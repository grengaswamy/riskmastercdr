﻿// All Common UI helper functions would be written here
using System;
using System.Web;
using System.Text;
using HtmlAgilityPack;
using System.IO;
using System.Collections.Specialized;
using System.Web.Configuration;
using System.Collections;
using System.Globalization;
using System.Text.RegularExpressions;
using Riskmaster.UI.CommonWCFService;
using System.Web.Security;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Riskmaster.RMXResourceManager;
using System.Runtime.Serialization.Json;
using System.Net;
using Riskmaster.Models;
using System.ServiceModel;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

//Add by kuladeep for rmA14.1 performance
/// <summary>
/// Provides Helper operations for UI classes
/// </summary>
public class AppHelper
{
    //Deb MITS 30897
    public static string DSNName = string.Empty;
    //Deb MITS 30897

    // akaushik5 Added for MITS 38073 Starts
    /// <summary>
    /// The language cache
    /// </summary>
    private static Dictionary<string, Dictionary<string, string>> languageCache = new Dictionary<string, Dictionary<string, string>>();
    // akaushik5 Added for MITS 38073 Ends
    
    /// <summary>
    /// Created By Deb
    /// For gettting the ClientId from the session/cookie
    /// </summary>
    public static int ClientId
    {
        get
        {
            try
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    return Convert.ToInt32(HttpContext.Current.Session["ClientId"]);
                }
                else
                {
                    return int.Parse(ReadCookieValue("ClientId"));
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        set
        {
            HttpContext.Current.Session["ClientId"] = value;
        }
    }
    public static string Token
    {
        get
        {
            return ReadCookieValue("SessionId");
        }
    }
    /// <summary>
    /// Created By Deb
    /// For Serializing object 
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string ConvertCharToHTML(string str)
    {
        if (str != null)
        {
            //For now its commented
            //str = str.Replace("&amp;", "&");
            //str = str.Replace("&lt;", "<");
            //str = str.Replace("&gt;", ">");
            //str = str.Replace("&#035;", "#");
            //str = str.Replace("&#039;", "'");
            //str = str.Replace("&quot;", "\"");
        }
        return str;
    }
    
    #region Session Handling
    /// <summary>
    /// Creates Session objects with all of the specified Keys and Values
    /// in the NameValueCollection
    /// </summary>
    /// <param name="dictSessionValues">StringDictionary containing all values
    /// to populate within each of the Session object instances</param>
    public static void CreateUserSession(StringDictionary dictSessionValues)
    {

        foreach (DictionaryEntry deSession in dictSessionValues)
        {
            HttpContext.Current.Session.Add(deSession.Key.ToString(), deSession.Value.ToString());
        }//foreach

        //Set the timeout value for Session
        TimeSpan tsTimeOut = new TimeSpan(0, GetAuthenticationTimeout(), 0);

        HttpContext.Current.Session.Timeout = (tsTimeOut.Days * 24 + tsTimeOut.Hours) * 60 + tsTimeOut.Minutes;

    }//method: CreateUserSession() 

    /// <summary>
    /// Gets the current Session ID from the Cookie rather than from a 
    /// Session state variable
    /// </summary>
    /// <returns>string containing the Session ID</returns>
    public static string GetSessionId()
    {
        string strSessionID = string.Empty;

        strSessionID = ReadCookieValue("SessionId");

        return strSessionID;
    }//method: GetSessionId
    #endregion

    #region Cookie Handling

    /// <summary>
    /// Reads the Cookie information containing all relevant 
    /// User Information
    /// </summary>
    /// <returns>NameValueCollection containing all of the Cookie keys and values</returns>
    public static NameValueCollection ReadCookieInfo()
    {
        NameValueCollection nvCookieValues = new NameValueCollection();

        HttpCookie objUserCookie = HttpContext.Current.Request.Cookies.Get("UserInfoCookie");

        //Verify that the cookie exists on the system
        if (objUserCookie != null)
        {
            nvCookieValues = objUserCookie.Values;
        } // if

        return nvCookieValues;
    }//method: ReadCookieInfo() 

    /// <summary>
    /// Creates a Cookie with all of the specified Keys and Values
    /// in the NameValueCollection
    /// </summary>
    /// <param name="nvCookieColl">NameValueCollection containing all
    /// values to populate within the Cookie</param>
    public static void CreateUserCookie(NameValueCollection nvCookieColl)
    {
        //Create cookie and populate it with values
        HttpCookie objCookie = new HttpCookie("UserInfoCookie");

        //Add the entire name value collection to the Cookie
        objCookie.Values.Add(nvCookieColl);
        objCookie.HttpOnly = true;
        //psarin2:start
        if (HttpContext.Current.Request.IsSecureConnection)
        objCookie.Secure = true;
        //psarin2:end
        objCookie.Expires = DateTime.Now.AddMinutes(GetAuthenticationTimeout());

        //Determine whether or not this needs to be set based on server and client workstation time zones
        //objCookie.Expires = DateTime.Now.AddYears(1); //Set the timeout to 1-year in the future

        //Store the cookie on the client side browser
        HttpContext.Current.Response.Cookies.Add(objCookie);
    }//method: CreateUserCookie()

    /// <summary>
    /// Reads the specified key from the cookie
    /// and returns the appropriate value
    /// </summary>
    /// <param name="strCookieKey">string containing the cookie key</param>
    /// <returns>string containing the value contained within the cookie</returns>
    public static string ReadCookieValue(string strCookieKey)
    {
        NameValueCollection nvCookieColl = new NameValueCollection();
        string strCookieValue = string.Empty;

        nvCookieColl = ReadCookieInfo();

        if (nvCookieColl.Count > 0)
        {
            strCookieValue = nvCookieColl[MatchCookieKey(strCookieKey)];
        } // if

        return strCookieValue;
    } // method: ReadCookieValue

    /// <summary>
    /// Removes the User Information cookie from the user's system
    /// </summary>
    public static void RemoveCookie()
    {
        //Create cookie and populate it with values
        HttpCookie objUserCookie = HttpContext.Current.Request.Cookies.Get("UserInfoCookie");
        HttpCookie objRMNetSessionCookie = HttpContext.Current.Request.Cookies.Get("RMNETSESSION");
        //Verify that the cookie still exists on the user's system
        if (objUserCookie != null)
        {
            objUserCookie.Expires = DateTime.Now.AddYears(-1); //Set the expiration date for a year in the past

            //Store the cookie on the client side browser
            HttpContext.Current.Response.Cookies.Add(objUserCookie);
        } // if

        if (objRMNetSessionCookie != null)
        {
            objRMNetSessionCookie.Expires = DateTime.Now.AddYears(-1); //Set the expiration date for a year in the past

            //Store the cookie on the client side browser
            HttpContext.Current.Response.Cookies.Add(objRMNetSessionCookie);
        } // if

    } // method: RemoveCookie


    /// <summary>
    /// Matches the Key in the NameValueCollection for the cookie
    /// regardless of the case-sensitivity in the specified key
    /// </summary>
    /// <param name="strNVCookieKey">string containing the requested NameValueCollection Key</param>
    /// <returns>string containing the correct case-sensitive key</returns>
    internal static string MatchCookieKey(string strNVCookieKey)
    {
        NameValueCollection nvCookieColl = new NameValueCollection();
        string strMatchedKey = string.Empty;
        nvCookieColl.Add("ClientId", string.Empty);
        nvCookieColl.Add("SessionId", string.Empty);
        nvCookieColl.Add("DsnId", string.Empty);
        nvCookieColl.Add("DsnName", string.Empty);
        nvCookieColl.Add("ViewId", string.Empty);
        nvCookieColl.Add("ViewName", string.Empty);
        nvCookieColl.Add("CustomViewNames", string.Empty);

        // Start MITS 26834, Debabrata Biswas
        nvCookieColl.Add("PortalDSNname", string.Empty);
        nvCookieColl.Add("PortalPVname", string.Empty);
        nvCookieColl.Add("PortalFDMName", string.Empty);
        nvCookieColl.Add("PortalClaimID", string.Empty);
        // End MITS 26834, Debabrata Biswas
        nvCookieColl.Add("LanguageCode", string.Empty);
        nvCookieColl.Add("BaseLanguageCode", string.Empty);
        nvCookieColl.Add("EnableVSS", string.Empty);//averma62 - VSS - rmA Integration.
        nvCookieColl.Add("EnableFAS", string.Empty);//asharma326 -FAS MITS 32386
        nvCookieColl.Add("FASServer", string.Empty);
        nvCookieColl.Add("FASUserId", string.Empty);
        nvCookieColl.Add("FASPassword", string.Empty);
        nvCookieColl.Add("FASFolder", string.Empty);
 		nvCookieColl.Add("CurrentDateSetting", string.Empty); //igupta3 Mits:32846 
        nvCookieColl.Add("AdjAssignmentAutoDiary", string.Empty);   //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        //Get all of the Keys in the NameValueCollection
        string[] arrKeys = nvCookieColl.AllKeys;

        //Loop through all of the Keys in the NameValueCollection
        foreach (string strNVKey in arrKeys)
        {
            int intCompareValue = string.Compare(strNVCookieKey, strNVKey, true);

            //verify that the key matches regardless of string case-sensitivity
            if (intCompareValue == 0)
            {
                strMatchedKey = strNVKey;
                break;
            } // if
        }

        return strMatchedKey;
    } // method: MatchCookieKey

    /// <summary>
    /// Gets the Forms Authentication Timeout value configured in the 
    /// Web.config file (in minutes)
    /// </summary>
    /// <returns>integer containing the Forms timeout configured
    /// in the Web.config file (in minutes)</returns>
    internal static int GetAuthenticationTimeout()
    {
        //Set a default timeout value of 30 mins
        const int DEFAULT_TIMEOUT = 30;

        AuthenticationSection authSection = WebConfigurationManager.GetWebApplicationSection("system.web/authentication") as AuthenticationSection;
        TimeSpan tsMinutes = new TimeSpan(0, DEFAULT_TIMEOUT, 0);
        int intTimeout = tsMinutes.Minutes;

        //Verify that the authentication section exists in the Web.config file
        if (authSection != null)
        {
            intTimeout = (authSection.Forms.Timeout.Days * 24 + authSection.Forms.Timeout.Hours) * 60 + authSection.Forms.Timeout.Minutes;
        } // if

        return intTimeout;
    } // method: GetAuthenticationTimeout()


    #endregion


    /// <summary>
    /// Creates the Forms Authentication Ticket and corresponding
    /// Forms Authentication Cookie
    /// </summary>
    /// <param name="strLoginName">string containing the user's login name</param>
    /// <param name="blnRememberMe">boolean indicating whether or not the user's cookie should be stored for future use</param>
    public static void CreateFormsAuthenticationTicket(string strLoginName, bool blnRememberMe)
    {
        DateTime dtExpirationDate = DateTime.Now.AddMinutes(GetAuthenticationTimeout());//make sure its same as the formsauthentication ticket expiry value

        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
            (1,
            strLoginName,
            DateTime.Now,
            dtExpirationDate, //should be same as cookie expiration
            blnRememberMe,
            string.Empty, //can contain user specific data
            FormsAuthentication.FormsCookiePath);

        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);

        HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

        authCookie.Expires = dtExpirationDate;
        authCookie.HttpOnly = true;

        //Create the FormsAuthentication Cookie
        HttpContext.Current.Response.Cookies.Add(authCookie);
    }//method: CreateFormsAuthenticationTicket



    #region Release Information Handling
    /// <summary>
    /// Gets the build version number from the Build Information XML file
    /// </summary>
    /// <returns>string containing the fully qualified string for the Build Version Information</returns>
    public static string GetReleaseBuildInfo()
    {
        string strBuildReleaseVersion = string.Empty;

        XDocument xDocBuildInfo = XDocument.Load(HttpContext.Current.Server.MapPath("~/App_Data/BuildInfo.xml"));
        XElement buildRoot = xDocBuildInfo.Element("build");
        XElement releaseVersion = buildRoot.Element("release_version");
        XElement patchsetVersion = buildRoot.Element("patchset_version");
        XElement buildNumber = buildRoot.Element("number");
        XElement changeset = buildRoot.Element("changeset");

        //Determine if the version includes any patchsets
        if (!string.IsNullOrEmpty(patchsetVersion.Value))
        {
            strBuildReleaseVersion = string.Format("{0}-{1} Build {2} Changeset {3}", releaseVersion.Value, patchsetVersion.Value, buildNumber.Value, changeset.Value);
        }//if
        else
        {
            strBuildReleaseVersion = string.Format("{0} Build {1} Changeset {2}", releaseVersion.Value, buildNumber.Value, changeset.Value);
        }//else

        //Clean up
        xDocBuildInfo = null;

        return strBuildReleaseVersion;
    } // method: GetReleaseBuildInfo 
    #endregion

    /// <summary>
    /// Returns the current user login name
    /// </summary>
    /// <returns></returns>
    public static string GetUserLoginName()
    {
        return HttpContext.Current.User.Identity.Name;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static string GetApplicationRootPath()
    {
        string sPath = HttpContext.Current.Request.Url.ToString();
        Regex rx = new Regex(@"^(?<rootpath>http(s){0,1}://[^/]+/[^/]+)/");
        string sRootPath = string.Empty;
        if (rx.IsMatch(sPath))
        {
            sRootPath = rx.Match(sPath).Result("${rootpath}");
        }

        return sRootPath;
    }
    
    public static string CallCWSService(string MessageTemplate)
    {
        string sSessionId = string.Empty, sReturn = string.Empty;
        CommonWCFServiceClient oWCF = null;
        try
        {
            sSessionId = ReadCookieValue("SessionId");
            string sClientId = AppHelper.ClientId.ToString();
            MessageTemplate = ChangeMessageValue(MessageTemplate, "//Message/Authorization", sSessionId);
            XElement xmlTree = XElement.Parse(MessageTemplate);
            xmlTree.Add(new XElement("ClientId", sClientId));
            oWCF = new CommonWCFServiceClient();
            sReturn = oWCF.ProcessRequest(xmlTree.ToString());
            XmlDocument xmlReturnErrorTest = new XmlDocument();
            xmlReturnErrorTest.LoadXml(sReturn);
            if (xmlReturnErrorTest.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
            {
                HttpContext.Current.Items["Error"] = sReturn;
            }
        }
        catch (Exception ex)
        {
            //RMA-2304 achouhan3 Ends : Check if Sesson ID is empty then ask user to login again else throw exception When SSO is enabled
            bool m_bUseSSO = false;
            Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["UseSSO"] ?? "false", out m_bUseSSO);
            if (m_bUseSSO && String.IsNullOrEmpty(sSessionId))
                HttpContext.Current.Response.Redirect("../UI/Home/Login.aspx", false);
            //RMA-2304 achouhan3 Ends : Check if Sesson ID is empty then ask user to login again else throw exception When SSO is enabled
            else
                throw new Exception(ex.Message, ex);
        } // catch

        finally
        {
            //Close the connection to the WCF Service
            if (oWCF != null)
            {
                oWCF.Close();
            } // if
        }
        return sReturn;
    }
    //PSARIN2 :R8 Perf Changes
    public static string GetBOBSession()
    {
        if (HttpContext.Current.Session["IsBOB"] == null)
        {
            //Riskmaster.UI.RMAuthentication.AuthenticationServiceClient authService=null;
            bool bIsBOB = false;
            try
            {
                //authService = new Riskmaster.UI.RMAuthentication.AuthenticationServiceClient();
                //bIsBOB = authService.GetBOBSetting(HttpContext.Current.User.Identity.Name, ReadCookieValue("DsnName"));//added by Amitosh for Adding carrier claim settings in session
                Riskmaster.Models.AuthData oAuthData = new Riskmaster.Models.AuthData();
                oAuthData.ClientId = AppHelper.ClientId;
                oAuthData.DsnName = ReadCookieValue("DsnName");
                oAuthData.UserName = HttpContext.Current.User.Identity.Name;
                bIsBOB = AppHelper.GetResponse<bool>("RMService/Authenticate/iscarrier",HttpVerb.POST,AppHelper.APPLICATION_JSON,oAuthData);
            }
            finally
            {
                //if (authService != null)
                //{
                //    authService.Close();
                //}
            }
            HttpContext.Current.Session["IsBOB"] = bIsBOB.ToString();
        }
        return HttpContext.Current.Session["IsBOB"].ToString();
    }
    //Deb: Multi Language Changes
    /// <summary>
    /// Get Language Code
    /// </summary>
    /// <returns></returns>
    public static string GetLanguageCode()
    {
        string sLangCode = ReadCookieValue("LanguageCode");
        sLangCode = sLangCode.Split('|')[0].ToString();
        string sDsn = ReadCookieValue("DsnName");
        if (string.IsNullOrEmpty(sLangCode))
        {
            //Riskmaster.UI.RMAuthentication.AuthenticationServiceClient authService = null;
            try
            {
                //authService = new Riskmaster.UI.RMAuthentication.AuthenticationServiceClient();
                AuthData oAuthData = new AuthData();
                //oAuthData.UserName = HttpContext.Current.Items["loginName"].ToString();
                oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                oAuthData.DsnName = DSNName;
                if ((string.IsNullOrEmpty(sDsn) || string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name)) && HttpContext.Current.Items["loginName"] != null)
                {
                    //sLangCode = authService.GetLanguageCode(HttpContext.Current.Items["loginName"].ToString(), DSNName);
                    oAuthData.UserName = HttpContext.Current.Items["loginName"].ToString();
                    sLangCode = AppHelper.GetResponse<string>("RMService/Authenticate/languagecode", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                }
                else
                {
                    oAuthData.UserName = HttpContext.Current.User.Identity.Name;
                    if (string.IsNullOrEmpty(sDsn))
                    {
                        //sLangCode = authService.GetLanguageCode(HttpContext.Current.User.Identity.Name, DSNName);
                        sLangCode = AppHelper.GetResponse<string>("RMService/Authenticate/languagecode", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                    }
                    else
                    {
                        //sLangCode = authService.GetLanguageCode(HttpContext.Current.User.Identity.Name, sDsn);
                        oAuthData.DsnName = sDsn;
                        sLangCode = AppHelper.GetResponse<string>("RMService/Authenticate/languagecode", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                    }
                }
                sLangCode = sLangCode.Split('|')[0].ToString();
                oAuthData = null;
            }
            finally
            {
                //if (authService != null)
                //{
                //    authService.Close();
                //}
            }
        }
        return sLangCode;
    }
    public static string GetBaseLanguageCodeWithCulture()
    {
        string sBaseLangCode = ReadCookieValue("BaseLanguageCode");
        if (string.IsNullOrEmpty(sBaseLangCode))
        {
            //Riskmaster.UI.RMAuthentication.AuthenticationServiceClient authService = null;
            try
            {
                //authService = new Riskmaster.UI.RMAuthentication.AuthenticationServiceClient();
                //sBaseLangCode = authService.GetBaseLanguageCode();
                sBaseLangCode = AppHelper.GetResponse<string>("RMService/Authenticate/baselanguagecode", AppHelper.HttpVerb.GET, AppHelper.APPLICATION_JSON, null);
            }
            finally
            {
                //if (authService != null)
                //{
                //    authService.Close();
                //}
            }
        }
        return sBaseLangCode;
    }
    /// <summary>
    /// Get Current Culture
    /// </summary>
    /// <returns></returns>
    public static string GetCulture()
    {
        string sCulture = ReadCookieValue("LanguageCode");
        sCulture = sCulture.Split('|')[1].ToString();
        if (string.IsNullOrEmpty(sCulture))
        {
            //Riskmaster.UI.RMAuthentication.AuthenticationServiceClient authService = null;
            try
            {
                //authService = new Riskmaster.UI.RMAuthentication.AuthenticationServiceClient();
                AuthData oAuthData = new AuthData();
                oAuthData.UserName = HttpContext.Current.User.Identity.Name;
                oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                oAuthData.DsnName = ReadCookieValue("DsnName");
                sCulture = AppHelper.GetResponse<string>("RMService/Authenticate/languagecode", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                //sCulture = authService.GetLanguageCode(HttpContext.Current.User.Identity.Name, ReadCookieValue("DsnName"));
                sCulture = sCulture.Split('|')[1].ToString();
                oAuthData = null;
            }
            finally
            {
                //if (authService != null)
                //{
                //    authService.Close();
                //}
            }
        }
        if (sCulture.Split('-').Length == 1)
        {
            sCulture = sCulture + "-" + sCulture.ToUpper().ToString();
        }
        return sCulture;
    }
    /// <summary>
    /// CalenderClientScript
    /// </summary>
    /// <param name="sCulture"></param>
    /// <returns></returns>
    public static void  CalenderClientScript(string sCulture,Page oPage)
    {
        string readContents = string.Empty;
        string searchContent = "dateFormat: '";
        int dateFormatIndex = 0;
        string currentDateformat = string.Empty;
        string path = string.Empty;
        string sScript = string.Empty;
        if (!oPage.ClientScript.IsStartupScriptRegistered(oPage.GetType(), "CalenderScript"))
        {
            //Rakhel - ML Changes - to pass cultures such as zh-CN,en-AU etc. and split the cultures which passed fr-FR to pass fr - start 
            if (sCulture.Split('-')[0].ToLower() == sCulture.Split('-')[1].ToLower())
            {
                sCulture = sCulture.Split('-')[0].ToLower();
            }
            //Rakhel - ML Changes - to pass cultures such as zh-CN,en-AU etc. and split the cultures which passed fr-FR to pass fr- end 
            path = HttpContext.Current.Server.MapPath(string.Format("../../Scripts/jquery/ui/i18n/jquery.ui.datepicker-{0}.js", sCulture));
            if (File.Exists(path))
            {
                using (StreamReader streamReader = new StreamReader(path, Encoding.UTF8))
                {
                    readContents = streamReader.ReadToEnd();
                }
                dateFormatIndex = readContents.IndexOf(searchContent);
                if (dateFormatIndex > 0)
                {
                    currentDateformat = readContents.Substring(dateFormatIndex + searchContent.Length, 8);
                    readContents = readContents.Replace(currentDateformat, GetDateFormat().ToLower().Replace("yyyy", "yy"));
                }
                sScript = (@"<script language='javascript' type='text/javascript' > " + readContents + " </script>");
            }
            else 
            {
                sScript = string.Format(@"<script language='javascript' type='text/javascript' src='../../Scripts/jquery/ui/i18n/jquery.ui.datepicker-{0}.js'></script>", sCulture);
            }
            oPage.ClientScript.RegisterStartupScript(oPage.GetType(), "CalenderScript", sScript, false);
        }
    }

    /// <summary>
    /// CalenderClientScript For non Fdm
    /// </summary>
    /// <param name="sCulture"></param>
    /// <param name="pageLayer"></param>
    /// <returns></returns>
    public static void CalenderClientScriptNonFDM(string sCulture, Page oPage, int pageLayer)
    {
        string readContents = string.Empty;
        string searchContent = "dateFormat: '";
        int dateFormatIndex = 0;
        string currentDateformat = string.Empty;
        string path = string.Empty;
        string sScript = string.Empty;
        string sSourcePath = string.Empty;
        if (!oPage.ClientScript.IsStartupScriptRegistered(oPage.GetType(), "CalenderScript"))
        {
            //Rakhel - ML Changes - to pass cultures such as zh-CN,en-AU etc. and split the cultures which passed fr-FR to pass fr - start 
            if (sCulture.Split('-')[0].ToLower() == sCulture.Split('-')[1].ToLower())
            {
                sCulture = sCulture.Split('-')[0].ToLower();
            }
            //Rakhel - ML Changes - to pass cultures such as zh-CN,en-AU etc. and split the cultures which passed fr-FR to pass fr- end 
            switch (pageLayer)
            {
                case 3:
                   sSourcePath = string.Format("../../../Scripts/jquery/ui/i18n/jquery.ui.datepicker-{0}.js", sCulture);
                   break;
                case 4:
                   sSourcePath = string.Format("../../../../Scripts/jquery/ui/i18n/jquery.ui.datepicker-{0}.js", sCulture);
                   break;
                default:
                   sSourcePath = string.Format("../../Scripts/jquery/ui/i18n/jquery.ui.datepicker-{0}.js", sCulture);
                   break;
        }
            path = HttpContext.Current.Server.MapPath(sSourcePath);
            if (File.Exists(path))
            {
                using (StreamReader streamReader = new StreamReader(path, Encoding.UTF8))
                {
                    readContents = streamReader.ReadToEnd();
                }
                dateFormatIndex = readContents.IndexOf(searchContent);
                if (dateFormatIndex > 0)
                {
                    currentDateformat = readContents.Substring(dateFormatIndex + searchContent.Length, 8);
                    readContents = readContents.Replace(currentDateformat, GetDateFormat().ToLower().Replace("yyyy", "yy"));
                }
                sScript = (@"<script language='javascript' type='text/javascript' > " + readContents + " </script>");
            }
            else
            {
                sScript = string.Format(@"<script language='javascript' type='text/javascript' src='" + sSourcePath + "'></script>", sCulture);
            }
            oPage.ClientScript.RegisterStartupScript(oPage.GetType(), "CalenderScript", sScript, false);
        }
    }

    /// <summary>
    /// TimeClientScript
    /// </summary>
    /// <param name="sCulture"></param>
    /// <returns></returns>
    public static void TimeClientScript(string sCulture,Page oPage)
    {
        if (!oPage.ClientScript.IsStartupScriptRegistered(oPage.GetType(), "TimeScript"))
        {
            DateTimeFormatInfo currentDateTimeFormat = (new CultureInfo(sCulture)).DateTimeFormat;
            Dictionary<string, string> resDict = resDict = new Dictionary<string, string>();
            resDict.Add("TimeSeparator", currentDateTimeFormat.TimeSeparator);
            resDict.Add("AMDesignator", currentDateTimeFormat.AMDesignator);
            resDict.Add("PMDesignator", currentDateTimeFormat.PMDesignator);
            resDict.Add("TimeFormat", currentDateTimeFormat.ShortTimePattern);
            //TimeFields variable name should be used in js files for further coding
            string sTimeScript = Riskmaster.RMXResourceManager.JavaScriptResourceHandler.SerializeResourceDictionary(resDict, "TimeFields");
            oPage.ClientScript.RegisterStartupScript(oPage.GetType(), "TimeScript", sTimeScript, true);
        }
    }
    /// <summary>
    /// RegionalFormatsScript
    /// </summary>
    /// <param name="sCountryId"></param>
    /// <param name="oPage"></param>
    public static void RegionalFormatsScript(string sCountryId, Page oPage)
    {
        Dictionary<string, string> resDict = null;
        //RMXResourceServiceClient oClient = null;
        RMResource oRMResource = null;
        try
        {
            //if (string.IsNullOrEmpty(sCountryId) || sCountryId == "0")
            //{
            //    resDict = new Dictionary<string, string>();
            //    resDict.Add("ZipCodeFormat", "#####");
            //    resDict.Add("PhoneFormat", "(###) ###-####");
            //    resDict.Add("SSNFormat", "###-##-####");
            //    resDict.Add("isSSNOnlyNumeric", "-1");
            //}
            //else
            //{
                oRMResource = new RMResource();
                oRMResource.ClientId = AppHelper.ClientId;
                oRMResource.DataSourceId = AppHelper.ReadCookieValue("DsnId");
               // oRMResource.CountryId = sCountryId;
                //oClient = new RMXResourceServiceClient();
                //resDict = oClient.GetFormats(int.Parse(AppHelper.ReadCookieValue("DsnId")), sCountryId);
                resDict = AppHelper.GetResponse<Dictionary<string, string>>("RMService/Resource/formats", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oRMResource);
            //}
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if(oRMResource != null)
               oRMResource = null;
            //if (oClient != null)
            //{
            //    oClient = null;
            //}
        }
        string sScript = Riskmaster.RMXResourceManager.JavaScriptResourceHandler.SerializeResourceDictionary(resDict, "RegionalFormats");
        oPage.ClientScript.RegisterStartupScript(oPage.GetType(), "RegionalFormatsScript", sScript, true);
    }
    /// <summary>
    /// Get the Riskmaster required date format: yyyymmdd from mm/dd/yyyy
    /// </summary>
    /// <param name="sValue"></param>
    /// <returns></returns>
    public static string GetRMDate(string sValue)
    {
        string sCulture = AppHelper.GetCulture().ToString();
        DateTimeFormatInfo fmt = (new CultureInfo(sCulture)).DateTimeFormat;
        string sRMDate = string.Empty;
        try
        {
            DateTime dt = new DateTime();
            bool bResult = DateTime.TryParse(sValue, fmt, DateTimeStyles.None, out dt);
            if (bResult)
            {
                sRMDate = dt.Year.ToString() + dt.ToString("MM") + dt.ToString("dd");
            }
            else
            {
                if (sValue.Length == 10)
                {
                    sValue = sValue.Replace("/", string.Empty);
                    sRMDate = sValue.Substring(4, 4) + sValue.Substring(0, 2) + sValue.Substring(2, 2);
                }
            }
        }
        catch
        {
            if (sValue.Length == 10)
            {
                sValue = sValue.Replace("/", string.Empty);
                sRMDate = sValue.Substring(4, 4) + sValue.Substring(0, 2) + sValue.Substring(2, 2);
            }
        }
        //if (sValue.Length == 10)
        //{
        // sValue = sValue.Replace("/", string.Empty);
        // sRMDate = sValue.Substring(4, 4) + sValue.Substring(0, 2) + sValue.Substring(2, 2);
        //}
        return sRMDate;
    }
    /// <summary>
    /// Get the time in format of hhmm from hh:mm
    /// </summary>
    /// <param name="sValue"></param>
    /// <returns></returns>
    public static string GetRMTime(string sValue)
    {
        string sRMTime = string.Empty;
        string sCulture = AppHelper.GetCulture().ToString();
        DateTimeFormatInfo fmt = (new CultureInfo(sCulture)).DateTimeFormat;
        if (sValue.Length == 8)
        {
            sRMTime = sValue.Replace(fmt.TimeSeparator, string.Empty);
        }

        return sRMTime;
    }

    //Deb: Multi Language Changes
    public static string GetQueryStringValue(string p_sControlName)
    {
        // npadhy MITS 17678 If there is & in the Querystring passed from the Javascript, then we repace the &
        // by the Character pair of "^@" in javascript and while retrieving the value from Querystring, we replace
        // the Character pair of "^@" by &
        if (HttpContext.Current.Request.QueryString[p_sControlName] == null)
            return string.Empty;
        else
            return HttpContext.Current.Request.QueryString[p_sControlName].ToString().Trim().Replace("^@", "&");
    }

    public static string GetFormValue(string p_sControlName)
    {
        if (HttpContext.Current.Request.Form[p_sControlName] == null)
            return string.Empty;
        else
            return HttpContext.Current.Request.Form[p_sControlName].ToString().Trim();
    }

    public static string ChangeMessageValue(string xml, string nodetochange, string nodevalue)
    {
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xml);
        if (doc.SelectSingleNode(nodetochange) != null)//MITS 27370
        doc.SelectSingleNode(nodetochange).InnerText = nodevalue;
        return doc.OuterXml;

    }
    public static string ChangeMessageXML(string xml, string nodetochange, string nodevalue)
    {
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xml);
        doc.SelectSingleNode(nodetochange).InnerXml = nodevalue;
        return doc.OuterXml;

    }

    public static string GetValueFromDOM(XmlDocument objDOM, string sValue)
    {
        string sReturn = string.Empty;
        if (objDOM.SelectSingleNode("//" + sValue) != null)
            sReturn = objDOM.SelectSingleNode("//" + sValue).InnerText;
        return sReturn;
    }

    public static string GetInnerXmlFromDOM(XmlDocument objDOM, string sValue)
    {
        string sReturn = string.Empty;
        if (objDOM.SelectSingleNode("//" + sValue) != null)
            sReturn = objDOM.SelectSingleNode("//" + sValue).InnerXml;
        return sReturn;
    }

    [Obsolete("Use GetDate() method")]
    public static string GetUIDate(string sValue)
    {
        string sUIDate = string.Empty;
        DateTimeFormatInfo currentDateTimeFormat = null;
        DateTime dtFormattedDate;

        dtFormattedDate = new DateTime();
        //Deb ML Changes
        //currentDateTimeFormat = new DateTimeFormatInfo();
        //currentDateTimeFormat.LongDatePattern = "yyyyMMdd";
        currentDateTimeFormat = (new CultureInfo(AppHelper.GetCulture().ToString())).DateTimeFormat;

        try
        {
            //Deb : Performance changes 
            if (string.IsNullOrEmpty(sValue))
            {
                return sUIDate;
            }
            //Deb : Performance changes
            
            // npadhy Check if the Parameter Passed is a valid Date. If it is a valid date then return it. Otherwise Format it
            DateTime dtValidDate;
            //srajindersin MITS 32606 dt:05/14/2013
            if ((!DateTime.TryParse(sValue, out dtValidDate)) && (!string.IsNullOrEmpty(sValue)))
            {
                //Aman MITS 29700
                if (sValue.Contains("/"))
                    sValue = GetRMDate(sValue);
                dtFormattedDate = DateTime.ParseExact(sValue, "yyyyMMdd", currentDateTimeFormat);
                //sUIDate = String.Format("{0:MM/dd/yyyy}", dtFormattedDate);
                if (currentDateTimeFormat.ShortDatePattern == "M" + currentDateTimeFormat.DateSeparator + "d" + currentDateTimeFormat.DateSeparator + "yyyy")
                {
                    currentDateTimeFormat.ShortDatePattern = "MM" + currentDateTimeFormat.DateSeparator + "dd" + currentDateTimeFormat.DateSeparator + "yyyy";
                }
                else if (currentDateTimeFormat.ShortDatePattern == "d" + currentDateTimeFormat.DateSeparator + "M" + currentDateTimeFormat.DateSeparator + "yyyy")
                {
                    currentDateTimeFormat.ShortDatePattern = "dd" + currentDateTimeFormat.DateSeparator + "MM" + currentDateTimeFormat.DateSeparator + "yyyy";
                }
                sUIDate = dtFormattedDate.ToString(currentDateTimeFormat.ShortDatePattern);
            }
            else
            {
                sUIDate = sValue;
            }
        }
        //Deb ML Changes
        catch
        {
            sUIDate = string.Empty;
        }
        return sUIDate;
    }
    public static string GetDateInenUS(string sValue)
    {
        string sTempDate=sValue;
        try
        {
            sTempDate = AppHelper.GetRMDate(sValue);
            sTempDate = AppHelper.GetUIDate(sTempDate, "en-US");
        }
        catch
        {
        }
        return sTempDate;
    }

    [Obsolete("Use GetDate() method")]
    public static string GetUIDate(string sValue,string sCulture)
    {
        string sUIDate = string.Empty;
        DateTimeFormatInfo currentDateTimeFormat = null;
        DateTime dtFormattedDate;
        dtFormattedDate = new DateTime();
        currentDateTimeFormat = (new CultureInfo(sCulture)).DateTimeFormat;
        try
        {
            if (string.IsNullOrEmpty(sValue))
            {
                return sUIDate;
            }
            DateTime dtValidDate;
            if (!DateTime.TryParse(sValue, out dtValidDate))
            {
                dtFormattedDate = DateTime.ParseExact(sValue, "yyyyMMdd", currentDateTimeFormat);
                if (currentDateTimeFormat.ShortDatePattern == "M" + currentDateTimeFormat.DateSeparator + "d" + currentDateTimeFormat.DateSeparator + "yyyy")
                {
                    currentDateTimeFormat.ShortDatePattern = "MM" + currentDateTimeFormat.DateSeparator + "dd" + currentDateTimeFormat.DateSeparator + "yyyy";
                }
                else if (currentDateTimeFormat.ShortDatePattern == "d" + currentDateTimeFormat.DateSeparator + "M" + currentDateTimeFormat.DateSeparator + "yyyy")
                {
                    currentDateTimeFormat.ShortDatePattern = "dd" + currentDateTimeFormat.DateSeparator + "MM" + currentDateTimeFormat.DateSeparator + "yyyy";
                }
                sUIDate = dtFormattedDate.ToString(currentDateTimeFormat.ShortDatePattern);
            }
            else
            {
                sUIDate = sValue;
            }
        }
        catch
        {
            sUIDate = string.Empty;
        }
        return sUIDate;
    }
    [Obsolete("Use GetTime() method")]
    public static string GetUITime(string sValue)
    {
        string sUITime = string.Empty;
        string sAMPM = string.Empty;

        if (sValue.Length == 6)
        {
            //Deb ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            DateTimeFormatInfo fmt = (new CultureInfo(sCulture)).DateTimeFormat;
            string sTime= sValue.Substring(0, 2)+fmt.TimeSeparator+sValue.Substring(2, 2);
            DateTime dt = DateTime.MinValue;
            bool bout = DateTime.TryParse(sTime,out dt);
            sUITime = dt.ToString(fmt.ShortTimePattern);

            //if (iHour < 12)
            //    sAMPM = "AM";
            //else
            //    sAMPM = "PM";

            //if (iHour > 12)
            //    iHour -= 12;

            //if (iHour == 0)
            //{
            //    sUITime = String.Format("12:{0} {1}", sValue.Substring(2, 2), sAMPM);
            //}
            //else if (iHour < 10)
            //{
            //    sUITime = String.Format("0{0}:{1} {2}", iHour, sValue.Substring(2, 2), sAMPM);
            //}
            //else if ((iHour >= 10) && (iHour <= 12))
            //{
            //    sUITime = String.Format("{0}:{1} {2}", iHour, sValue.Substring(2, 2), sAMPM);
            //}
            //else
            //{
            //    sUITime = String.Format(" {0}", sAMPM);
            //}
            //Deb ML Changes
        }
        else if (sValue.Length == 8  || sValue.Length == 7)//nnithiyanand 33974 - Added an extra condition to check the time length.
        {
            sUITime = sValue;
               
        }

        return sUITime;
    }

    public static string GetUIDatetTime(string sValue)
    {
        string sUIDateTime = string.Empty;  
        string sUIDate = string.Empty;
        string sUITime = string.Empty;

        if (sValue.Length == 14)
        {
		 	/* rkulavil : RMA-4726/MITS 37305 - start */
            //sUIDate = GetUIDate(sValue.Substring(0, 8));
            //sUITime = GetUITime(sValue.Substring(8, 6));
            sUIDate = GetDate(sValue);
            sUITime = GetTime(sValue);
		  	/* rkulavil : RMA-4726/MITS 37305 - end */
            sUIDateTime = String.Format("{0} {1}", sUIDate, sUITime);  
        }
        return sUIDateTime;
        
    }
	
	 /// <summary>
    /// Get System Date Separator: MITS 34253 hlv
    /// </summary>
    /// <returns>System date separator</returns>
    public static string GetUIDateSeparator()
    {
        string lsDateSeparator = string.Empty;
        string lsCulDate = GetDateFormat();

        if (lsCulDate.Length > 0)
        {
            int liMM = lsCulDate.IndexOf("MM");

            if (liMM == 0)
                lsDateSeparator = lsCulDate.Substring(2, 1);
            else
                lsDateSeparator = lsCulDate.Substring(liMM - 1, 1);
        }
        else
        {
            DateTimeFormatInfo fmt = (new CultureInfo(AppHelper.GetCulture().ToString())).DateTimeFormat;
            lsDateSeparator = fmt.DateSeparator;
        }

        return lsDateSeparator;
    }
	
/// <summary>
    /// Formats the SSN data:MITS 19296
/// </summary>
/// <param name="sValue">ssn data from database</param>
/// <param name="bValidFormat">If the format is valid</param>
/// <returns>formatted ssn</returns>
    public static string GetUISSNTaxID(string sValue,string sControltype,out bool bValidFormat)
    {
        int iParseResult = 0;
        StringBuilder sReturnWithHashes =  new StringBuilder();
        StringBuilder sCheck = new StringBuilder(sValue);
        sCheck = sCheck.Replace("-", ""); //stripping all the hashes
        if (Int32.TryParse(sCheck.ToString(), out iParseResult))
        {
            if (sCheck.Length != 9)//check for 9 digits
            {
                bValidFormat = false;
                return sValue;
            }
            if (sValue.Length == 11)//user entered ssn with dashes
            {
                if ((string.Compare(sValue.Substring(3, 1), "-") != 0) || (string.Compare(sValue.Substring(6, 1), "-") != 0 && string.Compare(sValue.Substring(7, 1), "-") != 0))
                {
                    bValidFormat = false;

                }
                else
                {
                    bValidFormat = true;
                }
                return sValue;
            }

            if (sValue.Length == 10)//user entered a tax id with dashes
            {
                if (string.Compare(sValue.Substring(2, 1), "-") != 0)
                {
                    bValidFormat = false;

                }
                else
                {
                    bValidFormat = true;
                }
                return sValue;
            }
            
            bValidFormat = true;
            if (string.Compare(sControltype, "ssn") == 0)//fall through:  if no dashes are present in database, default to SSN ###-##-####
            {
                sReturnWithHashes.Append(sCheck.ToString().Substring(0, 3));
                sReturnWithHashes.Append("-");
                sReturnWithHashes.Append(sCheck.ToString().Substring(3, 2));
                sReturnWithHashes.Append("-");
                sReturnWithHashes.Append(sCheck.ToString().Substring(5, 4));
            }
            else//fall through:  if no dashes are present in database, default to TaxId ##-#######
            {
                sReturnWithHashes.Append(sCheck.ToString().Substring(0, 2));
                sReturnWithHashes.Append("-");
                sReturnWithHashes.Append(sCheck.ToString().Substring(2, 7));
            }
            return sReturnWithHashes.ToString();

        }
        else
        {
            bValidFormat = false;
            return sValue;
        }

    }


    public static string GetValue(string p_sControlName)
    {
        if (HttpContext.Current.Request[p_sControlName] == null)
            return string.Empty;
        else
            return HttpContext.Current.Request[p_sControlName].ToString().Trim(); //MITS 28588 hlv 6/11/12 .Replace("[ampersand]", "&")
    }

    [Obsolete ("Use GetUIDateTime() method")]
    public static string GetDateTimeForUI(string p_sDataValue)
    {
        string sReturnString = string.Empty;
        string sDatePart = string.Empty;
        string sTimePart = string.Empty;
        string sTempDataValue = string.Empty;
        string p_sDateFormat = "d";
        string p_sTimeFormat = "t";
        DateTime datDataValue = DateTime.MinValue;
        try
        {
            if ((p_sDataValue == null) || (p_sDataValue.Trim() == ""))
                return string.Empty;
            else if (p_sDataValue.Trim() != "")
            {
                sTempDataValue = p_sDataValue;
                sTempDataValue = sTempDataValue.Substring(4, 2) + "-" + sTempDataValue.Substring(6, 2) + "-" + sTempDataValue.Substring(0, 4);
                datDataValue = System.DateTime.Parse(sTempDataValue);
                sDatePart = datDataValue.ToString(p_sDateFormat);

                sTempDataValue = p_sDataValue;
                sTempDataValue = sTempDataValue.Substring(8, 2) + ":" + sTempDataValue.Substring(10, 2);
                datDataValue = System.DateTime.Parse(sTempDataValue);
                sTimePart = datDataValue.ToString(p_sTimeFormat);
                sReturnString = sDatePart + " " + sTimePart;
            }
        }
        catch { return string.Empty; }
        return sReturnString;

    }
    /// <summary>
    /// Decompresses a compressed string data and returns the actual data in the form of string
    /// Created By- Parijat -Jurisdictionals
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string UnZip(string value)
    {
        //Transform string into byte[]
        byte[] byteArray = new byte[value.Length * 2];
        int indexBA = 0;
        foreach (char item in value.ToCharArray())
        {
            byteArray[indexBA++] = (byte)item;
        }

        //Prepare for decompress
        MemoryStream ms = new MemoryStream(byteArray);
        System.IO.Compression.GZipStream sr = new System.IO.Compression.GZipStream(ms,
            System.IO.Compression.CompressionMode.Decompress);

        //Reset variable to collect uncompressed result
        byteArray = new byte[byteArray.Length * 20];

        //Decompress
        int rByte = sr.Read(byteArray, 0, byteArray.Length);

        //Transform byte[] unzip data to string
        StringBuilder sB = new StringBuilder(rByte);
        //Read the number of bytes GZipStream red and do not a for each bytes in
        //resultByteArray;
        for (int i = 0; i < rByte; i++)
        {
            sB.Append((char)byteArray[i]);
        }
        sr.Close();
        ms.Close();
        sr.Dispose();
        ms.Dispose();
        return sB.ToString();
    }
    /// <summary>
    /// Compresses data in the form of string 
    /// Created By- Parijat -Jurisdictionals
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string Zip(string value)
    {
        //Transform string into byte[]  
        byte[] byteArray = new byte[2 * value.Length];
        int indexBA = 0;
        foreach (char item in value.ToCharArray())
        {
            byteArray[indexBA++] = (byte)item;
        }

        //Prepare for compress
        MemoryStream ms = new MemoryStream();
        System.IO.Compression.GZipStream sw = new System.IO.Compression.GZipStream(ms,
            System.IO.Compression.CompressionMode.Compress);

        //Compress
        sw.Write(byteArray, 0, byteArray.Length);
        //Close, DO NOT FLUSH cause bytes will go missing...
        sw.Close();

        //Transform byte[] zip data to string
        byteArray = ms.ToArray();
        StringBuilder sB = new StringBuilder(byteArray.Length);
        foreach (byte item in byteArray)
        {
            sB.Append((char)item);
        }
        ms.Close();
        sw.Dispose();
        ms.Dispose();
        return sB.ToString();
    }

    /// <summary>
    /// Provides Html Encoding for strings that need to be passed to Web Services
    /// </summary>
    /// <param name="strUnencoded">unencoded string</param>
    /// <returns>HtmlEncoded string</returns>
    /// <remarks>This method is used to encode strings to reduce the possibility
    /// of scripting or injection attacks on values passed to and from the UI and Service layer</remarks>
    public static string HtmlEncodeString(string strUnencoded)
    {
        return HttpContext.Current.Server.HtmlEncode(strUnencoded);
    }//method: HtmlEncode

    /// <summary>
    /// Takes HTML in the form of a string builder object and returns the extracted InnerHtml for a given Tag.
    /// Created by. Parijat - for jurisdictionals
    /// </summary>
    /// <param name="convertedAspx">Html in the form of string builder object from which the tag has to be extracted</param>
    ///<param name="tagName">Name of the tag whose Inner HTML has to be extracted</param>
    /// <returns>Extracted Inner Htl is returned in the form of string</returns>
    public static string HTMLExtractor(StringBuilder sourceHTML, string tagName)
    {
        HtmlDocument HtmlDocObject = new HtmlDocument();
        using (MemoryStream memoryStreamObject = new MemoryStream())
        {
            byte[] ByteArrayObject;
            ByteArrayObject = System.Text.Encoding.UTF8.GetBytes(sourceHTML.ToString());
            memoryStreamObject.Write(ByteArrayObject, 0, ByteArrayObject.Length);
            memoryStreamObject.Seek(0, SeekOrigin.Begin);
            HtmlDocObject.Load(memoryStreamObject);
        }
        return HtmlDocObject.GetElementbyId(tagName).InnerHtml;
    }
    /// <summary>
    /// Takes HTML in the form of a string builder object and returns the extracted HtmlNode for a given Tag.
    /// </summary>
    /// <param name="convertedAspx">Html in the form of string builder object from which the tag has to be extracted</param>
    ///<param name="tagName">Id of the element which needs to be extracted</param>
    /// <returns>Extracted HtmlNode is returned </returns>
    public static HtmlNode HTMLNodeExtractor(StringBuilder sourceHTML, string tagName)
    {
        HtmlDocument HtmlDocObject = new HtmlDocument();
        using (MemoryStream memoryStreamObject = new MemoryStream())
        {
            byte[] ByteArrayObject;
            ByteArrayObject = System.Text.Encoding.UTF8.GetBytes(sourceHTML.ToString());
            memoryStreamObject.Write(ByteArrayObject, 0, ByteArrayObject.Length);
            memoryStreamObject.Seek(0, SeekOrigin.Begin);
            HtmlDocObject.Load(memoryStreamObject);
        }
        return HtmlDocObject.GetElementbyId(tagName);
    }
    /// <summary>
    /// Takes HTML in the form of a string builder object and returns the given Tag.
    /// Created by. Parijat - Generic
    /// </summary>
    /// <param name="convertedAspx">Html in the form of string builder object from which a particular tag has to be extracted</param>
    ///<param name="tagName">Id of the element which needs to be extracted</param>
    /// <returns>Extracted element is returned in the form of string</returns>
    public static string HTMLElementExtractor(StringBuilder sourceHTML, string tagName)
    {
        HtmlDocument HtmlDocObject = new HtmlDocument();
        using (MemoryStream memoryStreamObject = new MemoryStream())
        {
            byte[] ByteArrayObject;
            ByteArrayObject = System.Text.Encoding.UTF8.GetBytes(sourceHTML.ToString());
            memoryStreamObject.Write(ByteArrayObject, 0, ByteArrayObject.Length);
            memoryStreamObject.Seek(0, SeekOrigin.Begin);
            HtmlDocObject.Load(memoryStreamObject);
        }
        return HtmlDocObject.GetElementbyId(tagName).OuterHtml;
    }  
    /// <summary>
    /// Returns the Type of the Control
    /// </summary>
    /// <param name="p_sFullControlType">The Full Control Type, ex - System.Web.UI.WebControls.TextBox</param>
    /// <returns>The Type of the Control ex TextBox</returns>
    public static string GetControlType(string p_sFullControlType)
    {
        int index = p_sFullControlType.LastIndexOf(".");
        return (p_sFullControlType.Substring(index + 1));
    }
	//Start:VACo changes
    /// <summary>
    /// Function for escape sequences.
    /// </summary>
    /// <param name="sCode">Input String</param>
    /// <returns>Output String</returns>
    public static string escapeStrings(string sCode)
    {
        sCode = sCode.Replace("'", "\\'");
        return sCode;
    }
	//end:VACo changes
    
    
    //rsolanki2: extensibility updates
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static bool isExtendedScreen(string p_sScreenName)
    {
        string sCustomViewList = AppHelper.ReadCookieValue("CustomViewNames");
        return sCustomViewList.ToLower()
                .Contains(string.Concat("|"
                    , p_sScreenName.ToLower()
                    , ".xml|"));      
        
    }

    //Yatharth: Added common function for stripping the XML/HTML tags from string. MITS 22273
    //This function will remove characters like &amp; from your string and convert to its original form.
    public static string StripHTMLTags(string InString) //Parijat :19880 --Strip HTML tags and creating Plain text
    {
        string cleanedbuffer = "";
        string PatternString = "\\<[^<>]+\\>";
        Regex re = new Regex(PatternString, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Multiline);
        cleanedbuffer = re.Replace(InString, "");

        //replace special characters
        PatternString = "(&nbsp;)|(&quot;)|(&ldquo;)|(&rdquo;)|(&lsquo;)|(&rsquo;)|(&bull;)";
        re = new Regex(PatternString, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Multiline);
        cleanedbuffer = re.Replace(cleanedbuffer, " ");
        cleanedbuffer = cleanedbuffer.Replace("&amp;", "&").Replace(@"&lt;", "<").Replace(@"&gt;", ">");//Parijat: replacing &amp; with &--19297
           
        return cleanedbuffer;
    }
    public static void CreateControl(System.Web.UI.Page objPage, XmlDocument XMLDocument)
    {
        XmlDocument xmlDoM;
        //String sComments = String.Empty;
        //String sFileName = String.Empty;
        String sType = String.Empty;
        String sCodeTable = String.Empty;
        String sName = String.Empty;
        String sTitle = String.Empty;

        ////added by rkaur7 on 09/10/2010 - MITS 22350
        //String sTableName = String.Empty;
        //String sParentFieldName = String.Empty;
        //String sChildFieldName = String.Empty;
        ////String sParent = String.Empty;
        //String sKey = String.Empty;
        ////String sAdmin = String.Empty;
        ////end rkaur7
        String sRequired = String.Empty;
        String sDefault = String.Empty;
        String sDisable = String.Empty;
        String shidden = String.Empty;
        String sGrouphidden = String.Empty;
        String sControlValue = String.Empty;
        //String sStamp = String.Empty; //Added by rkaur7 on 04/05/2009
        //string sFill = string.Empty; //07/09/2010: Sonam Pahariya MITS 21172
        //string sDirection = string.Empty;//09/07/2010 Debabrata Biswas
        //string sCustomQuery = string.Empty;//09/22/2010 Debabrata Biswas
        //String sParentOrgId = string.Empty;//09/22/2010 Debabrata Biswas            
        System.Web.UI.HtmlControls.HtmlGenericControl objDiv;
        System.Web.UI.HtmlControls.HtmlGenericControl objDivHeader;
        String sDisplayHideGroupName = String.Empty;
        String sDisplayHideGroupID = String.Empty;
        String sDisplayHideControlName = String.Empty;
        String sDisplayHideControlID = String.Empty;
        //String sChangeTitleControl = String.Empty;
        //String sChangeTitleControlID = String.Empty;
        //String sChangeTitle = String.Empty;
        //String sLevel = String.Empty;
        //String sInstruction = String.Empty;
        String sOnblur = String.Empty;
        String sOrderBy = String.Empty;
        //String sIgnored = String.Empty;
        //String sMsgShow = String.Empty;
        //String sMsgShowOn = String.Empty;
        //String sShowControls = String.Empty;
        //String sAddNewBtn = String.Empty;
        //String sParentCode = String.Empty;
        //String sParentLoB = String.Empty;//added for the code fields having parent lob  as a filter criteria--shilpi
        String sShortCode = String.Empty;
        String sCodeDesc = String.Empty;
        String sMode = String.Empty;
        String sIsSingleEntity = String.Empty;
        String sXMLScreens = String.Empty;
        String sAttachTo = String.Empty;
        String sAddNewEntityBtn = String.Empty;
        String sMaxLength = String.Empty;
        String[] sCodeIdList;
        //// 06/10/2009 Saurabh Walia: Added to include/exclude code 
        //String sInCode = String.Empty;
        //String sExCode = String.Empty;
        //// 06/10/2009 Saurabh Walia: Set one code value based on the other.
        //String sParentCodeField = String.Empty;
        ////09/16/2010 Neha Goel:MITS#22345(MITS#22916)
        //String sForeColor = String.Empty;
        //String sBackColor = String.Empty;
        //String sBorder = String.Empty;
        ////09/16/2010 Neha Goel:MITS#22345(MITS#22916)--end

        ////10/20/2010 Neha Goel:MITS#22775(MITS#22916)
        //String sTabOutSearchOn = String.Empty;
        ////10/20/2010 Neha Goel:MITS#22775(MITS#22916)--end

        ////09/04/2009 Saurabh Walia: Handling the Position of the control
        //String sControlPosition = String.Empty;
        //String sCPTitle = String.Empty;
        //String sCPControl = String.Empty;
        //String sCPInstruction = String.Empty;
        ////End 
        //sFileName = XMLFileName.ToString();
        ////Saurabh Walia 11/20/2009:Handling ids
        //String sConversion = string.Empty;

        //// 01/05/2010 Saurabh Walia: To add fixed decimal digits
        //String sFixed = string.Empty;

        //01/27/2010 Saurabh Walia: MITS 19598
        String sIgnoreFutureValidation = string.Empty;

        ////02/10/2010 Saurabh Walia: Handling HardCode Fields
        //String sHardCodeValues = string.Empty;
        //String sHardCodeIds = string.Empty;
        //05/20/2010 Saurabh Walia: Get Datamodel Object
        String sDataModelObject = string.Empty;

        ////02/02/2011 Neha Goel:(MITS#22916)--start--for changing the height and width of memo control on user's choice
        //String sRowSize = String.Empty;
        //String sColumnSize = String.Empty;
        ////02/02/2011 Neha Goel:(MITS#22916)--start

        //// Neha Goel---publix--04252011--start
        //String sSearchType = String.Empty;
        //String sClaimType = String.Empty;
        //String sPIPubType = String.Empty;
        String sOnChange = String.Empty;
        String sUnHide = String.Empty;
        //String sIsPERNR = String.Empty;
        ////ddhiman - Publix
        //string strClientScrptAction = string.Empty;
        //string strClientScrpt = string.Empty;
        ////ddhiman - Publix
        //String sCustomjsFunction = String.Empty;
        //String sCustomWidth = String.Empty; //spahariya to customize publix multientity list controls's width
        //String sValueID = String.Empty; //spahariya for type in PI other people
        ////09/21/2010 Neha Goel:MITS#22438(MITS#22916)---added grid for  publix
        //String sIsGridType = String.Empty;            
        ////09/21/2010 Neha Goel:MITS#22438(MITS#22916)--end
        //Neha Goel--end

        String sID = String.Empty;

        String sHelp = String.Empty; //spahariya MITS 26002 
        String sSelectedTextToBold = String.Empty;

        try
        {
            xmlDoM = new XmlDocument();
            xmlDoM = XMLDocument;

            foreach (XmlElement objGroup in xmlDoM.GetElementsByTagName("group"))
            {
                #region Group Element

                sGrouphidden = "";
                sTitle = "";
                sName = "";
                //sIgnored = "";

                // sComments = GetAttributeValue(objGroup, "comment", false);
                sTitle = GetAttributeValue(objGroup, "title", false);
                sGrouphidden = GetAttributeValue(objGroup, "hidden", true);
                sName = GetAttributeValue(objGroup, "name", false);
                //sLevel = GetAttributeValue(objGroup, "level", false);
                // sIgnored = GetAttributeValue(objGroup, "ignored", true);
                //09/16/2010 Neha Goel:MITS#22345(MITS#22916)   
                // sForeColor = GetAttributeValue(objGroup, "forecolor", true);
                // sBackColor = GetAttributeValue(objGroup, "backcolor", true);
                // sBorder = GetAttributeValue(objGroup, "border", false);
                //09/16/2010 Neha Goel:MITS#22345(MITS#22916)  
                //Check if the Group is ignored, then move to the next control
                //if (sIgnored.ToLower() == "yes")
                //{
                //    continue;
                //}

                objDiv = (System.Web.UI.HtmlControls.HtmlGenericControl)objPage.FindControl(sName);
                objDivHeader = (System.Web.UI.HtmlControls.HtmlGenericControl)objPage.FindControl(sName + "_header");
                if (objDiv != null)
                {
                    //if (sLevel != "")
                    //    objDiv.Attributes.Add("level", sLevel);

                    //if (sGrouphidden.ToLower() == "yes")
                    //{
                    //    objDiv.Style["DISPLAY"] = "none";
                    //    objDivHeader.Style["DISPLAY"] = "none";
                    //}                        

                    #region Adding Event Number
                    //if ((sName == "succsubmit" || sName == "succupd"))
                    //{
                    //    //if (sTitle.Contains("[EN]"))//changed code to add event number if required--shilpi
                    //    //{
                    //    //    string sEventNumber = string.Empty;
                    //    //    HashSet<object> hsObject = new HashSet<object>();
                    //    //    hsObject = (HashSet<object>)Session["objects"];
                    //    //    foreach (Object obj in hsObject)

                    //    //05262010 Saurabh Walia: Generic Messages.Start
                    //    ArrayList alDatamodel = null;
                    //    ArrayList alConversion = null;
                    //    ArrayList alOnlyShortCode = null;
                    //    ArrayList alOnlyDescription = null;

                    //    alDatamodel = new ArrayList();
                    //    alConversion = new ArrayList();
                    //    alOnlyShortCode = new ArrayList();
                    //    alOnlyDescription = new ArrayList();

                    //    alDatamodel.Add(ConfigurationManager.AppSettings["IspdfTitleClaim"]);

                    //    //sTitle = FetchDataModelValues(sTitle, alDatamodel, alConversion, alOnlyShortCode, alOnlyDescription);

                    //    //05262010 Saurabh Walia: Generic Messages.End
                    //}
                    #endregion

                    ////09/16/2010 Neha Goel:MITS#22345(MITS#22916)
                    //String sStyle = String.Empty;
                    //sStyle = "font-size: 15pt; font-family: Arial";
                    //if (!String.IsNullOrEmpty(sForeColor))
                    //{
                    //    sStyle = sStyle + ";color:" + sForeColor;
                    //}
                    //else
                    //{
                    //    sStyle = sStyle + ";color: black";
                    //}

                    //if (!String.IsNullOrEmpty(sBackColor))
                    //{
                    //    sStyle = sStyle + ";background-color:" + sBackColor;
                    //}

                    //if (!String.IsNullOrEmpty(sBorder) && sBorder.ToLower() == "yes")
                    //{
                    //    sStyle = sStyle + ";border:none thin";
                    //}                        

                    foreach (XmlElement objXmlNode in objGroup.ChildNodes)
                    {

                        //Reset variable values
                        //sComments = "";
                        sRequired = "";
                        sName = "";
                        sTitle = "";
                        sCodeTable = "";
                        sType = "";
                        sDefault = "";
                        sDisable = "";
                        shidden = "";
                        sDisplayHideGroupName = "";
                        sDisplayHideGroupID = "";
                        sDisplayHideControlName = "";
                        sDisplayHideControlID = "";
                        //sChangeTitle = "";
                        //sLevel = "";
                        //sChangeTitleControl = "";
                        //sChangeTitleControlID = "";
                        //sInstruction = "";
                        sOnblur = "";
                        sOrderBy = "";
                        //sIgnored = "";
                        //sMsgShow = "";
                        //sMsgShowOn = "";
                        //sShowControls = "";
                        //sAddNewBtn = "";
                        //sParentCode = "";
                        //sParentLoB = "";
                        sControlValue = "";
                        sShortCode = "";
                        sCodeDesc = "";
                        sMode = "";
                        sIsSingleEntity = "";
                        sAddNewEntityBtn = "";
                        sMaxLength = "";
                        //// 06/10/2009 Saurabh Walia: Added to include/exclude code 
                        //sInCode = "";
                        //sExCode = "";
                        //// 06/10/2009 Saurabh Walia: Set one code value based on the other.
                        //sParentCodeField = "";

                        ////added by rkaur7 on 09/10/2010 - MITS #22350
                        //// sAdmin = "";
                        //sTableName = "";
                        //sParentFieldName = "";
                        //sChildFieldName = "";
                        ////sParent = "";
                        //sKey = "";
                        ////end rkaur7
                        ////09/04/2009 Saurabh Walia: Handling the Position of the control
                        //sControlPosition = "";
                        //sCPTitle = "";
                        //sCPControl = "";
                        //sCPInstruction = "";
                        ////End

                        ////Saurabh Walia 11/20/2009:Handling ids
                        //sConversion = "";

                        //// 01/05/2010 Saurabh Walia: To add fixed decimal digits
                        //sFixed = "";

                        //01/27/2010 Saurabh Walia: MITS 19598
                        sIgnoreFutureValidation = "";

                        ////02/10/2010 Saurabh Walia: Handling HardCode Fields
                        //sHardCodeValues = "";
                        //sHardCodeIds = "";

                        //05/20/2010 Saurabh Walia: Get Datamodel Object
                        sDataModelObject = "";
                        ////10/20/2010 Neha Goel:MITS#22775(MITS#22916)--start
                        //sTabOutSearchOn = "";
                        ////10/20/2010 Neha Goel:MITS#22775(MITS#22916)--end
                        ////02/02/2011 Neha Goel:(MITS#22916)--start---for changing the height and width of memo control on user's choice
                        //sRowSize = "";
                        //sColumnSize = "";
                        //sCustomWidth = "";
                        ////02/02/2011 Neha Goel:(MITS#22916)--start

                        ////neha goel--publix---04252011---start
                        //sSearchType = "";
                        //sClaimType = "";
                        //sPIPubType = "";
                        sOnChange = "";
                        sUnHide = "";
                        //sIsPERNR = "";
                        //sCustomjsFunction = "";
                        //sValueID = "";
                        ////09/21/2010 Neha Goel:MITS#22438(MITS#22916)---added grid func from adventist to publix 
                        //sIsGridType = "";
                        ////09/21/2010 Neha Goel:MITS#22438(MITS#22916)--end
                        ////neha goel--end

                        sID = "";

                        sHelp = "";//spahariya MITS 26002
                        sSelectedTextToBold = "";
                        //Get the ignored field
                        //sIgnored = GetAttributeValue(objXmlNode, "ignored", true);



                        ////Check if the field is ignored, then move to the next control
                        //if (sIgnored.ToLower() == "yes")
                        //{
                        //    continue;
                        //}

                        //Getting Control Type
                        sType = GetAttributeValue(objXmlNode, "type", true);


                        //Get to know control is required
                        sRequired = GetAttributeValue(objXmlNode, "required", true);


                        //Get to know the title for the control
                        sTitle = GetAttributeValue(objXmlNode, "title", false);

                        ////Get to know the stamp for the control //Added by rkaur7 on 04/05/2009
                        //sStamp = GetAttributeValue(objXmlNode, "stamp", false);

                        ////Get to know the control should fill by default //07/09/2010: Sonam Pahariya MITS 21172
                        //sFill = GetAttributeValue(objXmlNode, "fill", false);
                        //Get to know the name for the control
                        sName = GetAttributeValue(objXmlNode, "name", false);
                        sID = GetAttributeValue(objXmlNode, "id", false);
                        //sName = "Main$" + sName;

                        //Get to know the codetable for the control
                        sCodeTable = GetAttributeValue(objXmlNode, "codetable", false);

                        //Get the default Date
                        sDefault = GetAttributeValue(objXmlNode, "default", false);

                        //Get the Control Disabled
                        sDisable = GetAttributeValue(objXmlNode, "disabled", true);

                        //Get the field hidden
                        shidden = GetAttributeValue(objXmlNode, "hidden", true);


                        //Get the display Hide Group
                        sDisplayHideGroupName = GetAttributeValue(objXmlNode, "displaygroupname", false);


                        //Get the display Hide Group ID
                        sDisplayHideGroupID = GetAttributeValue(objXmlNode, "displaygroupid", false);


                        //Get the display Control Name
                        sDisplayHideControlName = GetAttributeValue(objXmlNode, "displaycontrol", false);

                        //Get the display Control ID
                        sDisplayHideControlID = GetAttributeValue(objXmlNode, "displaycontrolid", false);

                        ////Get the display Control Title
                        //sChangeTitleControl = GetAttributeValue(objXmlNode, "changetitlecontrol", false);

                        ////Get the display Control Title ID
                        //sChangeTitleControlID = GetAttributeValue(objXmlNode, "changetitleid", false);

                        ////Get the display Change Title
                        //sChangeTitle = GetAttributeValue(objXmlNode, "changetitle", false);

                        ////Get the display Change Title
                        //sLevel = GetAttributeValue(objXmlNode, "level", false);

                        ////Get the Instruction 
                        //sInstruction = GetAttributeValue(objXmlNode, "instruction", false);

                        //sw Get the value of onblur field
                        sOnblur = GetAttributeValue(objXmlNode, "onblur", false);

                        //Get the value of orderby field
                        sOrderBy = GetAttributeValue(objXmlNode, "orderby", false);

                        ////Get the value of msgshow field
                        //sMsgShow = GetAttributeValue(objXmlNode, "msgshow", false);

                        ////Get the value of msgshowon field
                        //sMsgShowOn = GetAttributeValue(objXmlNode, "msgshowon", false);

                        ////Get the value of addcontrols field
                        //sShowControls = GetAttributeValue(objXmlNode, "addcontrols", false);

                        ////Get the value of addnewbtn field
                        //sAddNewBtn = GetAttributeValue(objXmlNode, "addnewbtn", true);

                        ////Get the value of parentcode field
                        //sParentCode = GetAttributeValue(objXmlNode, "parentcode", false);
                        ////Get the value of parentlob field--shilpi
                        //sParentLoB = GetAttributeValue(objXmlNode, "parentlob", false);

                        //Get the value of value field
                        sControlValue = GetAttributeValue(objXmlNode, "value", false);

                        //Get the value of mode field
                        sMode = GetAttributeValue(objXmlNode, "mode", true);

                        //Get the value of issingle field
                        sIsSingleEntity = GetAttributeValue(objXmlNode, "issingle", true);

                        //Get the value of xmlscreens field
                        sXMLScreens = GetAttributeValue(objXmlNode, "xmlscreens", false);

                        //Get the value of attachto field
                        sAttachTo = GetAttributeValue(objXmlNode, "attachto", false);

                        //Get the value of AddNewEntityBtn field
                        sAddNewEntityBtn = GetAttributeValue(objXmlNode, "addnewentitybtn", false);

                        //Get the value of Length
                        sMaxLength = GetAttributeValue(objXmlNode, "maxlength", false);

                        //// 06/10/2009 Saurabh Walia: Added to include/exclude code 
                        //sInCode = GetAttributeValue(objXmlNode, "incode", false);
                        //sExCode = GetAttributeValue(objXmlNode, "excode", false);

                        //// 06/10/2009 Saurabh Walia: Set one code value based on the other.
                        //sParentCodeField = GetAttributeValue(objXmlNode, "parentcodefield", false);

                        ////Get the comments
                        //sComments = GetAttributeValue(objXmlNode, "comments", false);

                        //Added by Abhinav -09 Sept 2010- Memo field as per Adventist
                        String sNotShowSavedValueMemo = string.Empty;
                        String sNumDaysForLockMemo = string.Empty;
                        String sDateToCompareWith = string.Empty;
                        String sMemoEditableForMultipleUpdates = string.Empty;
                        Boolean bLockEnabledMemo = false;

                        sNotShowSavedValueMemo = GetAttributeValue(objXmlNode, "notshowsavedvaluememo", false);

                        sNumDaysForLockMemo = GetAttributeValue(objXmlNode, "numdaystolockformemo", false);

                        sDateToCompareWith = GetAttributeValue(objXmlNode, "datetocomparewithformemo", false);

                        sMemoEditableForMultipleUpdates = GetAttributeValue(objXmlNode, "memoeditformultupdate", false);

                        //sCustomWidth = GetAttributeValue(objXmlNode, "customwidth", false);

                        //if ((!String.IsNullOrEmpty(sNumDaysForLockMemo) && (!String.IsNullOrEmpty(sDateToCompareWith)) && (!String.IsNullOrEmpty(sMemoEditableForMultipleUpdates))))
                        //{
                        //    bLockEnabledMemo = ValidateControlForlock(objXmlNode);

                        //}
                        //Abhinav 09 Sept 2010-End


                        ////09/04/2009 Saurabh Walia: Handling the Position of the control
                        //sControlPosition = GetAttributeValue(objXmlNode, "ctrlposition", false);
                        ////added by rkaur7 on 09/10/2010 - MITS #22350                     
                        ////Animesh Inserted
                        //strClientScrptAction = GetAttributeValue(objXmlNode, "clientscriptaction", false);
                        //strClientScrpt = GetAttributeValue(objXmlNode, "clientscriptvalue", false);
                        ////Animesh Insertion ends
                        //sTableName = GetAttributeValue(objXmlNode, "admintablename", false);
                        //sParentFieldName = GetAttributeValue(objXmlNode, "parentfieldname", false);
                        //sChildFieldName = GetAttributeValue(objXmlNode, "childfieldname", false);
                        ////sParent = GetAttributeValue(objXmlNode, "parent", false);
                        //sKey = GetAttributeValue(objXmlNode, "keyname", false);
                        ////end rkaur7

                        ////02/02/2011 Neha Goel:(MITS#22916)--start---for changing the height and width of memo control on user's choice
                        //sRowSize = GetAttributeValue(objXmlNode, "rowsize", false); ;
                        //sColumnSize = GetAttributeValue(objXmlNode, "columnsize", false); ;
                        ////02/02/2011 Neha Goel:(MITS#22916)--start

                        //if (!String.IsNullOrEmpty(sControlPosition))
                        //{
                        //    sCPTitle = sControlPosition.Split(',')[0].ToString();
                        //    sCPControl = sControlPosition.Split(',')[1].ToString();
                        //    sCPInstruction = sControlPosition.Split(',')[2].ToString();
                        //}
                        ////End

                        //shilpi-05/06/09--added a condition to check for the first enable control and store its name and type
                        //if ((sDisable.ToLower() != "yes") && (shidden.ToLower() != "yes") && (bEnable == false))
                        //{
                        //    bEnable = true;
                        //    sEnaControlName = sName;
                        //    sEnaControlType = sType;
                        //}
                        //ends here

                        ////Saurabh Walia 11/20/2009:Handling ids
                        //sConversion = GetAttributeValue(objXmlNode, "conversion", false);

                        //// 01/05/2010 Saurabh Walia: To add fixed decimal digits
                        //sFixed = GetAttributeValue(objXmlNode, "fixedto", false);

                        //01/27/2010 Saurabh Walia: MITS 19598
                        sIgnoreFutureValidation = GetAttributeValue(objXmlNode, "ignorefuturevalidation", true);

                        ////02/10/2010 Saurabh Walia: Handling HardCode Fields
                        //sHardCodeValues = GetAttributeValue(objXmlNode, "hardcodevalues", false);
                        //sHardCodeIds = GetAttributeValue(objXmlNode, "hardcodeids", false);

                        //05/20/2010 Saurabh Walia: Get Datamodel Object
                        sDataModelObject = GetAttributeValue(objXmlNode, "datamodelobject", false);

                        ////10/20/2010 Neha Goel:MITS#22775(MITS#22916)--start
                        //sTabOutSearchOn = GetAttributeValue(objXmlNode, "taboutsearchon", false);
                        ////10/20/2010 Neha Goel:MITS#22775(MITS#22916)--end
                        ////Debabrata Biswas
                        //sDirection = GetAttributeValue(objXmlNode, "direction", false);
                        //sCustomQuery = GetAttributeValue(objXmlNode, "customquery", false);
                        //sParentOrgId = GetAttributeValue(objXmlNode, "parentorgid", false);

                        ////neha goel---publix---04252011---start
                        //sSearchType = GetAttributeValue(objXmlNode, "searchtype", false);
                        //sClaimType = GetAttributeValue(objXmlNode, "claimtype", false);
                        //sPIPubType = GetAttributeValue(objXmlNode, "pipubtype", false);
                        sOnChange = GetAttributeValue(objXmlNode, "onchange", false);
                        sUnHide = GetAttributeValue(objXmlNode, "unhide", false);
                        //sIsPERNR = GetAttributeValue(objXmlNode, "isPERNR", false);
                        //sValueID = GetAttributeValue(objXmlNode, "valueid", false);
                        //sCustomjsFunction = GetAttributeValue(objXmlNode, "iscustomjsfunction", false);
                        ////09/21/2010 Neha Goel:MITS#22438(MITS#22916)---added for grid func from adventist to Publix
                        //sIsGridType = GetAttributeValue(objXmlNode, "isgridtype", false);
                        ////09/21/2010 Neha Goel:MITS#22438(MITS#22916)--end
                        ////neha goel---end

                        //Get Help text //spahariya MITS 26002 start- 09/05/11
                        sHelp = GetAttributeValue(objXmlNode, "help", false);
                        sSelectedTextToBold = GetAttributeValue(objXmlNode, "selectedtexttobold", false);
                        switch (sType)
                        {
                            #region Creating DropDown Control
                            //case "dropdown":
                            //    {
                            //        //start:nitin goel,23519,01/18/2011
                            //        //TextBox objTextControl = new TextBox();
                            //        //end nitin goel,23519,01/18/2011
                            //        //DropDownList objddControl = new DropDownList();

                            //        XmlNodeList xDropdownList = null;
                            //        ListItem DropdownItem;
                            //        ArrayList alParams = new ArrayList();
                            //        string sTempValue = string.Empty;
                            //        //Added by rkaur on 04/27/2009
                            //        DropDownList objddControl = (DropDownList)objPage.Master.FindControl(sName);
                            //        if (objddControl != null)
                            //        {
                            //            objddControl.ForeColor = System.Drawing.Color.Black;
                            //            //objddControl.Width = "300px";                                      
                            //            //objddControl.Attributes.Add("runat", "server");
                            //            //objddControl.Attributes.Add("PageXml", GlobalClass.xmlDocument.InnerXml.ToString());                                    
                            //            //                                    objddControl.Attributes.Add("PageXml", ((XmlDocument)Session["xmldoc"]).InnerXml.ToString()); 
                            //            //objddControl.Attributes.Add("PageXml", xmlDoM.InnerXml.ToString());

                            //            //neha goel: added if for hardcoded values: (MITS#22916)
                            //            if (sCodeTable != "")
                            //            {
                            //                //sMsgShowOn = GetCodeIdList(sMsgShowOn, sCodeTable);
                            //                if (sMsgShowOn == "0")
                            //                    sMsgShowOn = "";

                            //               // sDisplayHideControlID = GetCodeIdList(sDisplayHideControlID, sCodeTable);
                            //                if (sDisplayHideControlID == "0")
                            //                    sDisplayHideControlID = "";

                            //                //sDisplayHideGroupID = GetCodeIdList(sDisplayHideGroupID, sCodeTable);
                            //                if (sDisplayHideGroupID == "0")
                            //                    sDisplayHideGroupID = "";

                            //               // sChangeTitleControlID = GetCodeIdList(sChangeTitleControlID, sCodeTable);
                            //                if (sChangeTitleControlID == "0")
                            //                    sChangeTitleControlID = "";
                            //            }//end neha goel

                            //            objddControl.Attributes.Add("DisplayHideGroupName", sDisplayHideGroupName);
                            //            objddControl.Attributes.Add("DisplayHideGroupID", sDisplayHideGroupID);
                            //            objddControl.Attributes.Add("DisplayHideFieldName", sDisplayHideControlName);
                            //            objddControl.Attributes.Add("DisplayHideFieldID", sDisplayHideControlID);
                            //            objddControl.Attributes.Add("ChangeTitleControlName", sChangeTitleControl);
                            //            objddControl.Attributes.Add("ChangeTitleControlID", sChangeTitleControlID);
                            //            objddControl.Attributes.Add("ChangeTitle", sChangeTitle);
                            //            objddControl.Attributes.Add("Level", sLevel);
                            //            objddControl.Attributes.Add("MsgShow", sMsgShow);
                            //            objddControl.Attributes.Add("MsgShowOn", sMsgShowOn);
                            //            objddControl.Attributes.Add("ParentCode", sParentCode);
                            //            objddControl.Attributes.Add("Comment", sComments); //added by Ravneet Kaur
                            //            objddControl.Attributes.Add("CodeTable", sCodeTable);//Debabrata Biswas
                            //            //neha goel--publix---05032011
                            //            objddControl.Attributes.Add("OrderBy", sOrderBy);
                            //            //12/14/2010:Neha Goel: (MITS#22916) added a parameter to dropdownonchange to call a custom function from customfunction.js to do any custom work on dropdownonchange
                            //            objddControl.Attributes.Add("onchange", "setValues(this.DisplayHideGroupName,this.DisplayHideGroupID,this.DisplayHideFieldName,this.DisplayHideFieldID,this.ChangeTitleControlName,this.ChangeTitleControlID,this.ChangeTitle,this.Level,this.OrderBy,this.MsgShow,this.MsgShowOn,'','','',this.PageXml,'');DropDownOnChange(this.options[this.selectedIndex].value,this.name,'" + sCustomjsFunction + "');"); //Saurabh Walia: username needed for memo.

                            //            //string sPageXml = objddControl.Attributes["PageXml"];  
                            //            string sPageXml = String.Empty;
                            //            //objddControl.Attributes.Add("onchange", "setValues(this.DisplayHideGroupName,this.DisplayHideGroupID,this.DisplayHideFieldName,this.DisplayHideFieldID,this.ChangeTitleControlName,this.ChangeTitleControlID,this.ChangeTitle,this.Level,this.OrderBy,this.MsgShow,this.MsgShowOn,'','','','','');DropDownOnChange(this.options[this.selectedIndex].value,this.name)");
                            //            //Animesh inserted
                            //            bool blnAttributeExists = false;
                            //            string strTempAtt = string.Empty;
                            //            if (!string.IsNullOrEmpty(strClientScrptAction))
                            //            {
                            //                IEnumerator keys = objddControl.Attributes.Keys.GetEnumerator();
                            //                while (keys.MoveNext())
                            //                {
                            //                    string strTempKey = (String)keys.Current;
                            //                    if (strTempKey == strClientScrptAction)
                            //                    {
                            //                        blnAttributeExists = true;
                            //                        strTempAtt = objddControl.Attributes[strTempKey];
                            //                        break;
                            //                        //strTempAtt = objddControl.Attributes.Keys[strTempKey];  
                            //                    }
                            //                }

                            //                if (blnAttributeExists)
                            //                {
                            //                    objddControl.Attributes.Remove(strClientScrptAction);
                            //                    objddControl.Attributes.Add(strClientScrptAction, strTempAtt + strClientScrpt);
                            //                }
                            //                else
                            //                {
                            //                    objddControl.Attributes.Add(strClientScrptAction, strClientScrpt);
                            //                }
                            //            }
                            //            //Animesh Insertion ends
                            //            //objddControl.Attributes.Add("style", "width:auto;");
                            //            // 06/10/2009 Saurabh Walia: Added to include/exclude code 
                            //            objddControl.Attributes.Add("incode", sInCode);
                            //            objddControl.Attributes.Add("excode", sExCode);
                            //            //
                            //            //Animesh inserted
                            //            //bool blnAttributeExists = false;
                            //            //string strTempAtt = string.Empty;
                            //            //IEnumerator keys = objddControl.Attributes.Keys.GetEnumerator();
                            //            //while (keys.MoveNext())
                            //            //{
                            //            //    string strTempKey = (String)keys.Current;
                            //            //    if (string.Compare(strTempKey,"onchange",true)==0)
                            //            //    {
                            //            //        blnAttributeExists = true;
                            //            //        strTempAtt = objddControl.Attributes[strTempKey];
                            //            //        break;
                            //            //        //strTempAtt = objddControl.Attributes.Keys[strTempKey];  
                            //            //    }
                            //            //}

                            //            //if (blnAttributeExists)
                            //            //{
                            //            //    objddControl.Attributes.Remove("onchange");
                            //            //    objddControl.Attributes.Add("onchange", "setValues(this.DisplayHideGroupName,this.DisplayHideGroupID,this.DisplayHideFieldName,this.DisplayHideFieldID,this.ChangeTitleControlName,this.ChangeTitleControlID,this.ChangeTitle,this.Level,this.OrderBy,this.MsgShow,this.MsgShowOn,'','','',this.PageXml,'');DropDownOnChange(this.options[this.selectedIndex].value,this.name,'" + Common.GetConfigValue("username") + "')" + strTempAtt);
                            //            //}
                            //            //else
                            //            //{
                            //            //    objddControl.Attributes.Add("onchange", "setValues(this.DisplayHideGroupName,this.DisplayHideGroupID,this.DisplayHideFieldName,this.DisplayHideFieldID,this.ChangeTitleControlName,this.ChangeTitleControlID,this.ChangeTitle,this.Level,this.OrderBy,this.MsgShow,this.MsgShowOn,'','','',this.PageXml,'');DropDownOnChange(this.options[this.selectedIndex].value,this.name,'" + Common.GetConfigValue("username") + "')");
                            //            //}
                            //            //Animesh Insertion ends


                            //            //Start:Commented by Nitin Goel,23519,01/18/2011
                            //            //if (sDisable.ToLower() == "yes")
                            //            //{
                            //            //    objddControl.Enabled = false;
                            //            //    //Added by rkaur7
                            //            //    objddControl.BackColor = System.Drawing.Color.Silver;
                            //            //}
                            //            //end:Commented by Nitin Goel,23519,01/18/2011

                            //            //objGroupRow = new TableRow();
                            //            //objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            //            //if (shidden.ToLower() == "yes")
                            //            //    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");

                            //            //neha goel--publix--05122011
                            //            if (sUnHide.ToLower() == "yes")
                            //            {
                            //                HtmlTableRow tablerow = (HtmlTableRow)objPage.Master.FindControl(sName).Parent.Parent;
                            //                tablerow.Attributes.Add("style", "display:inline");
                            //            }
                            //            //objGroupCell = new TableCell();

                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPTitle);
                            //            ////End
                            //            Control objCntrlLabel = objPage.Master.FindControl(sName + "_Title");
                            //            if (objCntrlLabel != null)
                            //            {
                            //                if (sRequired == "yes")
                            //                {
                            //                    ((Label)objCntrlLabel).Font.Bold = true;
                            //                }
                            //                else
                            //                    ((Label)objCntrlLabel).Font.Bold = false;
                            //            }

                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();
                            //            //09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPControl);
                            //            //End
                            //            //start:added by nitin goel,23519,01/18/2011
                            //            //if (sDisable.ToLower() == "yes")
                            //            //{

                            //            //objTextControl.ForeColor = System.Drawing.Color.Black;
                            //            //objTextControl.ID = objddControl.ID; 
                            //            //objTextControl.Attributes.Add("runat", "server");
                            //            //objTextControl.Attributes.Add("readonly", "readonly");
                            //            //objTextControl.BackColor = System.Drawing.Color.Silver;
                            //            //objTextControl.Columns = 30;
                            //            //    objGroupCell.Controls.Add(objTextControl);
                            //            //}
                            //            //else
                            //            //{
                            //            //    objGroupCell.Controls.Add(objddControl);
                            //            //}
                            //            //end:added by nitin goel,23519,01/18/2011

                            //            if (sRequired == "yes")
                            //            {
                            //                RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                            //                objReqCtl.ControlToValidate = objddControl.ID;
                            //                if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //                //if (objddControl.Visible == false || objddControl.Enabled == false)
                            //                {
                            //                    objReqCtl.Enabled = false;
                            //                }
                            //                objReqCtl.ID = objddControl.ID + "_Req";
                            //                objReqCtl.Text = "*";
                            //                objReqCtl.ErrorMessage = ((Label)objPage.Master.FindControl(sName + "_Title")).Text + " is a Required Field.";
                            //                objReqCtl.EnableClientScript = true;
                            //                if (objPage.Master.FindControl(sName).Parent is HtmlTableCell)
                            //                {
                            //                    ((HtmlTableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //                else if (objPage.Master.FindControl(sName).Parent is TableCell)
                            //                {
                            //                    ((TableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //            }

                            //            #region Filling parameter arrayList
                            //            alParams.Add(sCodeTable);
                            //            alParams.Add(sParentCode);
                            //            alParams.Add(sParentLoB);
                            //            alParams.Add("");
                            //            alParams.Add("");
                            //            alParams.Add(sInCode);
                            //            alParams.Add(sExCode);
                            //            alParams.Add(sOrderBy);
                            //            #endregion

                            //            //GetDropDownValues(ref objddControl, alParams);

                            //            xDropdownList = objXmlNode.ChildNodes;
                            //            //Append Static Values

                            //            foreach (XmlNode rad in xDropdownList)
                            //            {
                            //                DropdownItem = new ListItem();
                            //                DropdownItem.Text = ((XmlElement)rad).GetAttribute("title");

                            //                sTempValue = ((XmlElement)rad).GetAttribute("value");

                            //                if (sTempValue.StartsWith("[") && sTempValue.EndsWith("]"))
                            //                {
                            //                    DropdownItem.Value = sTempValue;
                            //                    //DropdownItem.Value = sTempValue.Substring(1, sTempValue.Length - 2);
                            //                }
                            //                //else
                            //                //{
                            //                //    if (sCodeTable != "")
                            //                //        //DropdownItem.Value = (GlobalClass.DataModelObject.Context.LocalCache.GetCodeId(sTempValue.Trim(), sCodeTable)).ToString();
                            //                //}

                            //                objddControl.Items.Add(DropdownItem);
                            //            }

                            //            //Debabrata Biswas Handle custom query
                            //            if (sCustomQuery != "")
                            //            {
                            //                //ExecuteCustomQuery(objddControl, sCustomQuery, sParentOrgId, xmlDoM);
                            //            }

                            //            //objGroupRow.Cells.Add(objGroupCell);

                            //            //objGroupCell = new TableCell();

                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "30%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPInstruction);
                            //            ////End
                            //            ////objGroupCell.Attributes.Add("width", "2%");
                            //            //objGroupCell.Text = sInstruction;

                            //            //objGroupRow.Controls.Add(objGroupCell);
                            //            //objGroupTable.Rows.Add(objGroupRow);

                            //            //03/8/2010: Saurabh Walia:Setting the default value
                            //            //if (!String.IsNullOrEmpty(sDefault) && (String.IsNullOrEmpty(sControlValue) || sControlValue == "0"))
                            //            //{
                            //            //    foreach (ListItem lstItem in objddControl.Items)
                            //            //    {
                            //            //        if (lstItem.Value == GlobalClass.DataModelObject.Context.LocalCache.GetCodeId(sDefault, sCodeTable).ToString())
                            //            //        {
                            //            //            lstItem.Selected = true;
                            //            //        }
                            //            //    }
                            //            //}

                            //            //Setting the value if any                                    
                            //            if (!String.IsNullOrEmpty(sControlValue)) //03/08/2010 Saurabh Walia: Only enters if has a value
                            //            {
                            //                foreach (ListItem lstItem in objddControl.Items)
                            //                {
                            //                    if (lstItem.Value == sControlValue)
                            //                    {
                            //                        lstItem.Selected = true;
                            //                    }
                            //                }
                            //            }
                            //            //Added by RKaur
                            //            //Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                            //            //
                            //            //start:Added by nitin goel,23519,01/18/2011
                            //            //if (sDisable.ToLower() == "yes")
                            //            //{
                            //            //    objTextControl.Text = objddControl.SelectedItem.ToString();
                            //            //    objControls.Add(objTextControl);
                            //            //}
                            //            ////end:nitin goel,23519,01/18/2011
                            //            //else
                            //            //{
                            //            //    objControls.Add(objddControl);
                            //            //}
                            //        }
                            //        break;
                            //    }
                            #endregion
                            #region Creating TextBox Control
                            case "text":
                                {
                                    //TextBox objControl = new TextBox();
                                    //objControl.ID = sName;
                                    //objControl.Columns = 30;
                                    TextBox objControl = (TextBox)objPage.FindControl(sName);
                                    if (objControl != null)
                                    {
                                        //if (sMaxLength != "")
                                        //    objControl.MaxLength = Conversion.ConvertStrToInteger(sMaxLength);

                                        // objControl.EnableViewState = false;
                                        //if (sMode == "password")
                                        //    objControl.TextMode = TextBoxMode.Password;

                                        //sw adding the onblur function, if present
                                        if (sOnblur != String.Empty)
                                            objControl.Attributes.Add("onblur", sOnblur);

                                        //neha goel--publix--05092011-- adding the ontextchanged function, if present
                                        if (sOnChange != String.Empty)
                                            objControl.Attributes.Add("onchange", sOnChange);

                                        if (sDisable.ToLower() == "yes")
                                        {
                                            objControl.Attributes.Add("readonly", "readonly");
                                            //Added by rkaur7
                                            objControl.BackColor = System.Drawing.Color.Silver;
                                        }

                                        //neha goel--publix--05122011
                                        if (sUnHide.ToLower() == "yes")
                                        {
                                            //************************************
                                            Label objlbl1 = (Label)objPage.Master.FindControl(sName + "_Title");
                                            if (objlbl1 != null)
                                            {
                                                HtmlTableCell tblCell = (HtmlTableCell)objPage.Master.FindControl(sName + "_Title").Parent;
                                                tblCell.Attributes.Add("style", "display:inline");
                                            }
                                            HtmlTableCell tblCell2 = (HtmlTableCell)objPage.Master.FindControl(sName).Parent;
                                            tblCell2.Attributes.Add("style", "display:inline");
                                            //************************************
                                            HtmlTableRow tablerow = (HtmlTableRow)objPage.Master.FindControl(sName).Parent.Parent;
                                            tablerow.Attributes.Add("style", "display:inline");
                                        }

                                        //spahariya MITS 26002 start- 09/05/11**********
                                        //modified this for PSO architecture--neha goel
                                        if (sHelp != "")
                                        {
                                            Image objImage = (Image)objPage.FindControl(sID + "_Image");
                                            if (objImage != null)
                                            {
                                                objImage.Attributes.Add("style", "visibility:visible;display:inline");
                                                objImage.Attributes.Add("onMouseOver", "displayPopupDiv('" + sHelp + "','" + sSelectedTextToBold + "');");
                                                objImage.Attributes.Add("onMouseOut", "hidePopupDiv();");
                                            }

                                        }
                                        //*********************************************   

                                        if (sRequired == "yes")
                                        {
                                            RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                                            objReqCtl.ControlToValidate = objControl.ID;
                                            objReqCtl.ValidationGroup = "btnSubmit";
                                            //if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                                            ////if (objControl.Visible == false || objControl.Enabled == false)
                                            //{
                                            //    objReqCtl.Enabled = false;
                                            //}
                                            objReqCtl.ID = objControl.ID + "_Req";
                                            objReqCtl.Text = "*";
                                            //objReqCtl.InitialValue = "0";
                                            Label objLblCntl = (Label)objPage.FindControl(sID + "_Title");
                                            objReqCtl.ErrorMessage = objLblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                                            objReqCtl.EnableClientScript = true;
                                            if (objPage.FindControl(sName).Parent is HtmlTableCell)
                                            {
                                                ((HtmlTableCell)objPage.FindControl(sName).Parent).Controls.Add(objReqCtl);
                                            }
                                            else if (objPage.FindControl(sName).Parent is TableCell)
                                            {
                                                ((TableCell)objPage.FindControl(sName).Parent).Controls.Add(objReqCtl);
                                            }
                                            //Anu Tennyson moved to javascript
                                            objReqCtl.Enabled = false;
                                            //Anu Tennyson Ends
                                        }



                                        //objGroupRow.Cells.Add(objGroupCell);
                                        //objGroupCell = new TableCell();

                                        //09/04/2009 Saurabh Walia: Handling the Position of the control
                                        //if (String.IsNullOrEmpty(sControlPosition))
                                        //    objGroupCell.Attributes.Add("width", "30%");
                                        //else
                                        //    objGroupCell.Attributes.Add("width", sCPInstruction);
                                        ////End
                                        //objGroupCell.Text = sInstruction;
                                        //objGroupRow.Controls.Add(objGroupCell);

                                        //objGroupTable.Rows.Add(objGroupRow);
                                        //Added by RKaur
                                        //Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                                        //
                                        //Setting the default value if any   -Added by Ravneet Kaur 
                                        //if (!(String.IsNullOrEmpty(sDefault)))
                                        //    objControl.Text = sDefault;
                                        ////Setting the value if any    
                                        //if (!(String.IsNullOrEmpty(sControlValue)))
                                        //    objControl.Text = sControlValue;

                                        //objControls.Add(objControl);
                                    }
                                    break;
                                }
                            #endregion
                            #region Creating Code Control
                            case "code":
                                Control objCodeControl;
                                objCodeControl = objPage.FindControl(sName);
                                Button objbtnControl = (Button)objPage.FindControl(sName + "btn");
                                TextBox objtxtControl = (TextBox)objPage.FindControl(sName);
                                if (objCodeControl != null)
                                {

                                    objbtnControl.Attributes.Add("DisplayHideFieldName", sDisplayHideControlName);
                                    objtxtControl.Attributes.Add("DisplayHideFieldName", sDisplayHideControlName);
                                    objbtnControl.Attributes.Add("DisplayHideFieldID", sDisplayHideControlID);
                                    objtxtControl.Attributes.Add("DisplayHideFieldID", sDisplayHideControlID);


                                    objbtnControl.Attributes.Add("DisplayHideGroupName", sDisplayHideGroupName);
                                    objtxtControl.Attributes.Add("DisplayHideGroupName", sDisplayHideGroupName);
                                    objbtnControl.Attributes.Add("DisplayHideGroupID", sDisplayHideGroupID);
                                    objtxtControl.Attributes.Add("DisplayHideGroupID", sDisplayHideGroupID);

                                    String sOnClick = String.Empty;
                                    sOnClick = objbtnControl.Attributes["onclick"];

                                    //objbtnControl.Attributes.Add("onclick", "setValues(this.DisplayHideGroupName,this.DisplayHideGroupID,this.DisplayHideFieldName,this.DisplayHideFieldID,'','','','','','','','','','','');" + sOnClick);
                                    //MITS 38289/RMA-10825 - PSO related issue msampathkuma
                                    objbtnControl.Attributes.Add("onclick", "setValues(this.getAttribute('DisplayHideGroupName'),this.getAttribute('DisplayHideGroupID'),this.getAttribute('DisplayHideFieldName'),this.getAttribute('DisplayHideFieldID'),'','','','','','','','','','','');" + sOnClick);
                                    
                                    //spahariya MITS 26002 start- 09/05/11**********
                                    //modified this for PSO architecture--neha goel
                                    if (sHelp != "")
                                    {
                                        Image objImage = (Image)objPage.FindControl(sID + "_Image");
                                        if (objImage != null)
                                        {
                                            objImage.Attributes.Add("style", "visibility:visible;display:inline");
                                            objImage.Attributes.Add("onMouseOver", "displayPopupDiv('" + sHelp + "','" + sSelectedTextToBold + "');");
                                            objImage.Attributes.Add("onMouseOut", "hidePopupDiv();");
                                        }

                                    }
                                    //*********************************************                                        

                                    if (sRequired == "yes")
                                    {
                                        Label lblCntlName = (Label)objPage.FindControl(sID + "_Title");
                                        RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                                        //objReqCtl.Visible = true;                                        
                                        objReqCtl.ControlToValidate = sID + "$codelookup_cid";
                                        objReqCtl.ValidationGroup = "btnSubmit";
                                        //if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                                        ////if (((Code)objCodeControl).Visible == false )
                                        //{                                            
                                        //        objReqCtl.Enabled = false;

                                        //}
                                        objReqCtl.ID = sID + "_Req";
                                        objReqCtl.Text = "*";
                                        objReqCtl.InitialValue = "0";

                                        //objReqCtl.Attributes.Add("width", "6px");
                                        objReqCtl.ErrorMessage = lblCntlName.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                                        if (objPage.FindControl(sName).Parent.Parent is HtmlTableCell)
                                        {
                                            ((HtmlTableCell)objPage.FindControl(sName).Parent.Parent).Controls.Add(objReqCtl);
                                        }
                                        else if (objPage.FindControl(sName).Parent.Parent is TableCell)
                                        {
                                            ((TableCell)objPage.FindControl(sName).Parent.Parent).Controls.Add(objReqCtl);
                                        }
                                        //Anu Tennyson moved to javascript
                                        objReqCtl.Enabled = false;

                                    }



                                    //Setting default value
                                    //if (sDefault != "")
                                    //{
                                    //    StringBuilder sSQL = new StringBuilder();

                                    //    //neha goel:MITS 23539  added all org hierarchy level-01/11/2011
                                    //    if (sCodeTable == "DEPARTMENT" || sCodeTable == "COMPANY" || sCodeTable == "FACILITY" || sCodeTable == "LOCATION" || sCodeTable == "REGION" || sCodeTable == "OPERATION" || sCodeTable == "COMPANY" || sCodeTable == "CLIENT")
                                    //    {
                                    //        //neha goel:MITS 23539  commented and replaced with new query-01/11/2011
                                    //        sSQL.Append("SELECT DISTINCT ENTITY.ENTITY_ID as 'ID',ENTITY.LAST_NAME as 'Description',ENTITY.ABBREVIATION as 'Code' FROM ORG_HIERARCHY INNER JOIN ENTITY ON ORG_HIERARCHY.").Append(sCodeTable).Append("_EID = ENTITY.ENTITY_ID WHERE ");
                                    //        sSQL.Append(" ENTITY_TABLE_ID = ").Append(GlobalClass.DataModelObject.Context.LocalCache.GetTableId(sCodeTable));
                                    //        //neha goel:end

                                    //        //sSQL.Append("SELECT ENTITY.ENTITY_ID as 'ID',ENTITY.LAST_NAME as 'l1',ENTITY.ABBREVIATION as 'f1' FROM ORG_HIERARCHY INNER JOIN ENTITY ON ORG_HIERARCHY.DEPARTMENT_EID = ENTITY.ENTITY_ID INNER JOIN ENTITY_SUPP ON ENTITY.ENTITY_ID = ENTITY_SUPP.ENTITY_ID WHERE ");
                                    //        //sSQL.Append(" ENTITY_TABLE_ID = ").Append(GlobalClass.DataModelObject.Context.LocalCache.GetTableId("DEPARTMENT"));

                                    //        sSQL.Append(" AND ENTITY.ABBREVIATION = '").Append(sDefault.Split(',')[0].ToString()).Append("'");
                                    //        sSQL.Append(" AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL)");   //ABHAY-Added deleted condition

                                    //        ((CodeLookUp)objCodeControl).codeid = GlobalClass.DataModelObject.Context.DbConn.ExecuteString(sSQL.ToString());
                                    //        sCodeDesc = GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(((Code)objCodeControl).codeid)) + " " + GlobalClass.DataModelObject.Context.LocalCache.GetEntityLastName(Conversion.ConvertStrToInteger(((Code)objCodeControl).codeid));
                                    //    }
                                    //    else if (sCodeTable == "STATES")
                                    //    {
                                    //        String sStateId = string.Empty;
                                    //        String sStateName = string.Empty;
                                    //        sSQL.Append("SELECT S.STATE_ROW_ID AS 'ID' FROM STATES S WHERE STATE_NAME<>''");
                                    //        sSQL.Append(" AND S.STATE_ID ='").Append(sDefault.Split(',')[0].ToString()).Append("'");

                                    //        ((Code)objCodeControl).codeid = GlobalClass.DataModelObject.Context.DbConn.ExecuteString(sSQL.ToString());
                                    //        GlobalClass.DataModelObject.Context.LocalCache.GetStateInfo(Conversion.ConvertStrToInteger(((Code)objCodeControl).codeid), ref sStateId, ref sStateName);
                                    //        sCodeDesc = sStateId + "    " + sStateName;
                                    //    }
                                    //    else
                                    //    {
                                    //        ((Code)objCodeControl).codeid = GlobalClass.DataModelObject.Context.LocalCache.GetCodeId(sDefault.Split(',')[0].ToString(), sCodeTable).ToString();
                                    //        sCodeDesc = GlobalClass.DataModelObject.Context.LocalCache.GetShortCode(Conversion.ConvertStrToInteger(((Code)objCodeControl).codeid)) + "  " + GlobalClass.DataModelObject.Context.LocalCache.GetCodeDesc(Conversion.ConvertStrToInteger(((Code)objCodeControl).codeid));
                                    //    }
                                    //    ((Code)objCodeControl).codedesc = sCodeDesc;
                                    //}



                                }
                                break;
                            #endregion
                            #region Creating MultiCode Control
                            case "multicode":
                                Control objMultiControl;
                                //objMultiControl = LoadControl("multicode.ascx");
                                //objMultiControl.ID = sName;
                                //objMultiControl = objPage.Master.FindControl(sName);
                                Button objMultibtnControl = (Button)objPage.FindControl(sName + "btn");
                                ListBox objMultilstControl = (ListBox)objPage.FindControl(sName);
                                TextBox objMultitxtControl = (TextBox)objPage.FindControl(sName + "_lst");
                                if (objMultilstControl != null)
                                {
                                    objMultibtnControl.Attributes.Add("DisplayHideFieldName", sDisplayHideControlName);
                                    objMultilstControl.Attributes.Add("DisplayHideFieldName", sDisplayHideControlName);
                                    objMultitxtControl.Attributes.Add("DisplayHideFieldName", sDisplayHideControlName);

                                    objMultibtnControl.Attributes.Add("DisplayHideFieldID", sDisplayHideControlID);
                                    objMultilstControl.Attributes.Add("DisplayHideFieldID", sDisplayHideControlID);
                                    objMultitxtControl.Attributes.Add("DisplayHideFieldID", sDisplayHideControlID);

                                    //spahariya MITS 26002 start- 09/05/11**********
                                    //modified this for PSO architecture--neha goel
                                    if (sHelp != "")
                                    {
                                        Image objImage = (Image)objPage.FindControl(sID + "_Image");
                                        if (objImage != null)
                                        {
                                            objImage.Attributes.Add("style", "visibility:visible;display:inline");
                                            objImage.Attributes.Add("onMouseOver", "displayPopupDiv('" + sHelp + "','" + sSelectedTextToBold + "');");
                                            objImage.Attributes.Add("onMouseOut", "hidePopupDiv();");
                                        }

                                    }
                                    //*********************************************   

                                    if (sRequired == "yes")
                                    {
                                        Label lblCntrl = (Label)objPage.FindControl(sID + "_Title");
                                        RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                                        //objReqCtl.Visible = true;
                                        objReqCtl.ControlToValidate = sID + "$multicode_lst";
                                        objReqCtl.ValidationGroup = "btnSubmit";
                                        //if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                                        //{
                                        //    objReqCtl.Enabled = false;
                                        //}
                                        objReqCtl.ID = sID + "_Req";
                                        objReqCtl.Text = "*";
                                        objReqCtl.ErrorMessage = lblCntrl.Text + " is a Required Field.";//01/27/2010 Saurabh Walia: MITS 19600
                                        if (objPage.FindControl(sName).Parent.Parent is HtmlTableCell)
                                        {
                                            ((HtmlTableCell)objPage.FindControl(sName).Parent.Parent).Controls.Add(objReqCtl);
                                        }
                                        else if (objPage.FindControl(sName).Parent.Parent is TableCell)
                                        {
                                            ((TableCell)objPage.FindControl(sName).Parent.Parent).Controls.Add(objReqCtl);
                                        }
                                        //Anu Tennyson moved to javascript
                                        objReqCtl.Enabled = false;
                                    }

                                    //    //objGroupRow.Cells.Add(objGroupCell);

                                    //    //objGroupCell = new TableCell();

                                    //    //                                objGroupCell.Attributes.Add("width", "30%");
                                    //    //09/04/2009 Saurabh Walia: Handling the Position of the control
                                    //    //if (String.IsNullOrEmpty(sControlPosition))
                                    //    //    objGroupCell.Attributes.Add("width", "1%");
                                    //    //else
                                    //    //    objGroupCell.Attributes.Add("width", sCPInstruction);
                                    //    ////End
                                    //    //objGroupCell.Text = sInstruction;
                                    //    //objGroupRow.Controls.Add(objGroupCell);

                                    //    //objGroupTable.Rows.Add(objGroupRow);


                                    //    //Ravneet Kaur
                                    //    if (!(String.IsNullOrEmpty(sDefault)))
                                    //    {
                                    //        ((MultiCode)objMultiControl).codeidlist = sDefault.Replace(' ', ',');
                                    //        sDefault = sDefault.Replace(' ', ',');
                                    //        sCodeIdList = sDefault.Split(',');
                                    //        ListBox lstBox = (ListBox)objMultiControl.FindControl("txtMultiName");

                                    //        for (int i = 0; i < sCodeIdList.Length; i++)
                                    //        {
                                    //            ListItem lstitem = new ListItem();
                                    //            lstitem.Value = sCodeIdList[i];
                                    //            lstitem.Text = GlobalClass.DataModelObject.Context.LocalCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //            lstBox.Items.Add(lstitem);
                                    //            if (sCodeDesc == "")
                                    //            {
                                    //                sCodeDesc = GlobalClass.DataModelObject.Context.LocalCache.GetShortCode(Conversion.ConvertStrToInteger(sCodeIdList[i])) + " " + GlobalClass.DataModelObject.Context.LocalCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //            }
                                    //            else
                                    //            {
                                    //                //multicode fill error
                                    //                // sCodeDesc = sCodeDesc + "," + GlobalClass.DataModelObject.Context.LocalCache.GetShortCode(Conversion.ConvertStrToInteger(sCodeIdList[i])) + "   " + GlobalClass.DataModelObject.Context.LocalCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //                sCodeDesc = sCodeDesc + "%#" + GlobalClass.DataModelObject.Context.LocalCache.GetShortCode(Conversion.ConvertStrToInteger(sCodeIdList[i])) + " " + GlobalClass.DataModelObject.Context.LocalCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //            }
                                    //        }
                                    //        ((MultiCode)objMultiControl).codedesclist = sCodeDesc;
                                    //    }


                                    //    //ABHAY
                                    //    if (!(String.IsNullOrEmpty(sControlValue)))
                                    //    {
                                    //        ((MultiCode)objMultiControl).codeidlist = sControlValue.Replace(' ', ',');
                                    //        sControlValue = sControlValue.Replace(' ', ',');
                                    //        sCodeIdList = sControlValue.Split(',');
                                    //        ListBox lstBox = (ListBox)objMultiControl.FindControl("txtMultiName");

                                    //        for (int i = 0; i < sCodeIdList.Length; i++)
                                    //        {
                                    //            ListItem lstitem = new ListItem();
                                    //            lstitem.Value = sCodeIdList[i];
                                    //            if (sCodeTable != null && sCodeTable.ToUpper() == "DEPARTMENT")
                                    //            {
                                    //                lstitem.Text = GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(sCodeIdList[i])) + " " + GlobalClass.DataModelObject.Context.LocalCache.GetEntityLastName(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //            }
                                    //            else
                                    //                lstitem.Text = GlobalClass.DataModelObject.Context.LocalCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //            lstBox.Items.Add(lstitem);
                                    //            if (sCodeTable != null && sCodeTable.ToUpper() == "DEPARTMENT")
                                    //            {
                                    //                if (sCodeDesc == "")
                                    //                {
                                    //                    sCodeDesc = GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(sCodeIdList[i])) + " " + GlobalClass.DataModelObject.Context.LocalCache.GetEntityLastName(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //                }
                                    //                else
                                    //                {
                                    //                    //Commented and added by Abhinav on 10 Jan 2011- MITS#23506
                                    //                    //sCodeDesc = sCodeDesc + "," + GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(sCodeIdList[i])) + "   " + GlobalClass.DataModelObject.Context.LocalCache.GetEntityLastName(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //                    sCodeDesc = sCodeDesc + "%#" + GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(sCodeIdList[i])) + "   " + GlobalClass.DataModelObject.Context.LocalCache.GetEntityLastName(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //                    //End - MITS#23506
                                    //                }
                                    //            }
                                    //            else
                                    //            {
                                    //                if (sCodeDesc == "")
                                    //                {
                                    //                    sCodeDesc = GlobalClass.DataModelObject.Context.LocalCache.GetShortCode(Conversion.ConvertStrToInteger(sCodeIdList[i])) + " " + GlobalClass.DataModelObject.Context.LocalCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //                }
                                    //                else
                                    //                {
                                    //                    //multicode fill error
                                    //                    // sCodeDesc = sCodeDesc + "," + GlobalClass.DataModelObject.Context.LocalCache.GetShortCode(Conversion.ConvertStrToInteger(sCodeIdList[i])) + "   " + GlobalClass.DataModelObject.Context.LocalCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //                    sCodeDesc = sCodeDesc + "%#" + GlobalClass.DataModelObject.Context.LocalCache.GetShortCode(Conversion.ConvertStrToInteger(sCodeIdList[i])) + " " + GlobalClass.DataModelObject.Context.LocalCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                                    //                }
                                    //            }
                                    //        }
                                    //        ((MultiCode)objMultiControl).codedesclist = sCodeDesc;
                                    //    }
                                }

                                break;
                            #endregion
                            #region Creating MultiEntity Control
                            //case "multientity":
                            // Control objMultiEntControl;
                            //objMultiEntControl = LoadControl("multientity.ascx");
                            //objMultiEntControl.ID = sName;
                            //objMultiEntControl = objPage.Master.FindControl(sName);                                    
                            //if (objMultiEntControl != null)
                            //{
                            ////((MultiEntity)objMultiEntControl).CodeTable = sCodeTable;
                            //((MultiEntity)objMultiEntControl).OrderBy = sOrderBy;
                            //((MultiEntity)objMultiEntControl).ShowControls = sShowControls;
                            //((MultiEntity)objMultiEntControl).AddNewBtn = sAddNewBtn;
                            //((MultiEntity)objMultiEntControl).DisplayHideFieldName = sDisplayHideControlName;
                            //((MultiEntity)objMultiEntControl).DisplayHideFieldID = sDisplayHideControlID;
                            ////                                ((MultiEntity)objMultiEntControl).PageXml = GlobalClass.xmlDocument.InnerXml.ToString();
                            ////((MultiEntity)objMultiEntControl).PageXml = ((XmlDocument)Session["xmldoc"]).InnerXml.ToString();
                            ////((MultiEntity)objMultiEntControl).PageXml = xmlDoM.InnerXml.ToString();
                            //((MultiEntity)objMultiEntControl).IsSingleEntity = sIsSingleEntity;
                            //((MultiEntity)objMultiEntControl).XmlScreens = sXMLScreens;
                            //((MultiEntity)objMultiEntControl).AttachTo = sAttachTo;
                            //((MultiEntity)objMultiEntControl).Type = sType;
                            //((MultiEntity)objMultiEntControl).AddNewEntityButton = sAddNewEntityBtn;
                            ////neha goel---publix-04252011--start
                            //((MultiEntity)objMultiEntControl).ClaimType = sClaimType;
                            //((MultiEntity)objMultiEntControl).PIPubType = sPIPubType;
                            //((MultiEntity)objMultiEntControl).SearchType = sSearchType;
                            ////neha goel---end


                            ////if (sIsSingleEntity == "yes")
                            ////{
                            ////    ListBox li = (ListBox)objMultiEntControl.FindControl("txtMultiName");
                            ////    li.Rows  = 1;
                            ////    li.SelectionMode = ListSelectionMode.Multiple;
                            ////}

                            ////objGroupRow = new TableRow();
                            ////objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            ////if (shidden.ToLower() == "yes")
                            ////    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");

                            ////objGroupCell = new TableCell();
                            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            ////if (String.IsNullOrEmpty(sControlPosition))
                            ////    objGroupCell.Attributes.Add("width", "10%");
                            ////else
                            ////    objGroupCell.Attributes.Add("width", sCPTitle);
                            //////End
                            ////objGroupCell.ID = sName + "_ctlTitle";

                            //Label lblCntl = (Label)objPage.Master.FindControl(sName + "_Title");
                            ////neha goel--publix--05242011---added if as code breaks if label control not found
                            //if (lblCntl != null)
                            //{
                            //    if (sRequired == "yes")
                            //    {
                            //        lblCntl.Font.Bold = true;
                            //    }
                            //    else
                            //        lblCntl.Font.Bold = false;
                            //}

                            ////objGroupRow.Cells.Add(objGroupCell);
                            ////objGroupCell = new TableCell();
                            //////09/04/2009 Saurabh Walia: Handling the Position of the control
                            ////if (String.IsNullOrEmpty(sControlPosition))
                            ////    objGroupCell.Attributes.Add("width", "80%");
                            ////else
                            ////    objGroupCell.Attributes.Add("width", sCPControl);
                            //////End
                            ////objGroupCell.Controls.Add(objMultiEntControl);
                            //if (sRequired == "yes")
                            //{
                            //    RequiredFieldValidator objReqCtl = (RequiredFieldValidator)objMultiEntControl.FindControl("RequiredFieldValidator1");
                            //    objReqCtl.Visible = true;
                            //    if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //    {
                            //        objReqCtl.Enabled = false;
                            //    }
                            //    //objReqCtl.ID = "Req";
                            //    objReqCtl.ErrorMessage = lblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                            //    //if (objPage.Master.FindControl(sName).Parent is HtmlTableCell)
                            //    //{
                            //    //    ((HtmlTableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //    //}
                            //    //else if (objPage.Master.FindControl(sName).Parent is TableCell)
                            //    //{
                            //    //    ((TableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //    //}
                            //    // objGroupCell.Controls.Add(objReqCtl);
                            //}
                            ////objGroupRow.Cells.Add(objGroupCell);

                            ////objGroupCell = new TableCell();
                            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            ////if (String.IsNullOrEmpty(sControlPosition))
                            ////    objGroupCell.Attributes.Add("width", "10%");
                            ////else
                            ////    objGroupCell.Attributes.Add("width", sCPInstruction);
                            //////End
                            ////objGroupCell.Text = sInstruction;
                            ////objGroupRow.Controls.Add(objGroupCell);

                            ////objGroupTable.Rows.Add(objGroupRow);

                            ////Ravneet Kaur
                            //if (!(String.IsNullOrEmpty(sDefault)))
                            //{
                            //    ((MultiEntity)objMultiEntControl).codeidlist = sDefault.Replace(' ', ',');
                            //    sCodeIdList = sDefault.Split(' ');
                            //    ListBox lstBox1 = (ListBox)objMultiEntControl.FindControl("txtMultiName");

                            //    //for (int i = 0; i < sCodeIdList.Length; i++)
                            //    //{
                            //    //    ListItem lstitem1 = new ListItem();
                            //    //    lstitem1.Value = sCodeIdList[i];
                            //    //    lstitem1.Text = GlobalClass.DataModelObject.Context.LocalCache.GetEntityLastName(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                            //    //    lstBox1.Items.Add(lstitem1);
                            //    //    if (sCodeDesc == "")
                            //    //    {
                            //    //        sCodeDesc = GlobalClass.DataModelObject.Context.LocalCache.GetEntityLastName(Conversion.ConvertStrToInteger(sCodeIdList[i])) + ' ' + GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                            //    //    }
                            //    //    else
                            //    //    {
                            //    //        sCodeDesc = sCodeDesc + "," + GlobalClass.DataModelObject.Context.LocalCache.GetEntityLastName(Conversion.ConvertStrToInteger(sCodeIdList[i])) + ' ' + GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                            //    //    }
                            //    //}
                            //    for (int i = 0; i < sCodeIdList.Length; i++)
                            //    {
                            //        ListItem lstitem1 = new ListItem();
                            //        String sLName = String.Empty;
                            //        String sFName = String.Empty;
                            //        GlobalClass.DataModelObject.Context.LocalCache.GetEntityInfo(Conversion.ConvertStrToLong(sCodeIdList[i]), ref sFName, ref sLName);
                            //        lstitem1.Value = sCodeIdList[i];
                            //        lstitem1.Text = sFName + ' ' + sLName;
                            //        lstBox1.Items.Add(lstitem1);
                            //        if (sCodeDesc == "")
                            //        {
                            //            // sCodeDesc = GlobalClass.DataModelObject.Context.LocalCache.GetEntit(Conversion.ConvertStrToInteger(sCodeIdList[i])) + ' ' + GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                            //            sCodeDesc = sFName + ' ' + sLName;
                            //        }
                            //        else
                            //        {
                            //            //sCodeDesc = sCodeDesc + "," + GlobalClass.DataModelObject.Context.LocalCache.GetEntityLastName(Conversion.ConvertStrToInteger(sCodeIdList[i])) + ' ' + GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                            //            sCodeDesc = sCodeDesc + "," + sFName + ' ' + sLName;
                            //        }
                            //    }
                            //    ((MultiEntity)objMultiEntControl).codedesclist = sCodeDesc;
                            //}
                            ////ABHAY
                            //if (!(String.IsNullOrEmpty(sControlValue)) && sControlValue != "0")
                            //{
                            //    ((MultiEntity)objMultiEntControl).codeidlist = sControlValue.Replace(' ', ',');
                            //    sCodeIdList = sControlValue.Split(' ');
                            //    ListBox lstBox1 = (ListBox)objMultiEntControl.FindControl("txtMultiName");

                            //    for (int i = 0; i < sCodeIdList.Length; i++)
                            //    {
                            //        ListItem lstitem1 = new ListItem();
                            //        String sLName = String.Empty;
                            //        String sFName = String.Empty;
                            //        if (sDataModelObject != "" && sDataModelObject.Split('.')[0] == "Vehicle")
                            //        {
                            //            String sQuery = String.Empty;
                            //            DbReader dbVehicle = null;
                            //            sQuery = " SELECT VIN,VEHICLE_MAKE FROM VEHICLE WHERE VIN = '";
                            //            sQuery = sQuery + sCodeIdList[i] + "'";
                            //            dbVehicle = GlobalClass.DataModelObject.Context.DbConn.ExecuteReader(sQuery);
                            //            while (dbVehicle.Read())
                            //            {
                            //                sLName = dbVehicle.GetString(0);
                            //                sFName = dbVehicle.GetString(1);
                            //            }
                            //            dbVehicle.Close();
                            //        }
                            //        else
                            //        {
                            //            GlobalClass.DataModelObject.Context.LocalCache.GetEntityInfo(Conversion.ConvertStrToLong(sCodeIdList[i]), ref sFName, ref sLName);
                            //        }
                            //        lstitem1.Value = sCodeIdList[i];
                            //        lstitem1.Text = sFName + ' ' + sLName;
                            //        lstBox1.Items.Add(lstitem1);
                            //        if (sCodeDesc == "")
                            //        {
                            //            // sCodeDesc = GlobalClass.DataModelObject.Context.LocalCache.GetEntit(Conversion.ConvertStrToInteger(sCodeIdList[i])) + ' ' + GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                            //            sCodeDesc = sFName + ' ' + sLName;
                            //        }
                            //        else
                            //        {
                            //            //sCodeDesc = sCodeDesc + "," + GlobalClass.DataModelObject.Context.LocalCache.GetEntityLastName(Conversion.ConvertStrToInteger(sCodeIdList[i])) + ' ' + GlobalClass.DataModelObject.Context.LocalCache.GetEntityAbbreviation(Conversion.ConvertStrToInteger(sCodeIdList[i]));
                            //            sCodeDesc = sCodeDesc + "," + sFName + ' ' + sLName;
                            //        }
                            //    }
                            //    ((MultiEntity)objMultiEntControl).codedesclist = sCodeDesc;
                            //}
                            ////spahariya-- publix customer injury to change width of multi entity list control
                            //if (!string.IsNullOrEmpty(sCustomWidth))
                            //{
                            //    ListBox lstBox1 = (ListBox)objMultiEntControl.FindControl("txtMultiName");
                            //    lstBox1.Attributes.Add("style", "width:" + sCustomWidth + ";");
                            //}
                            ////Added by RKaur
                            ////Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                            ////
                            //if (sDisable.ToLower() == "yes")
                            //    ((MultiEntity)objMultiEntControl).Disabled(true);

                            ////ABHAY
                            ////objControls.Add(objMultiEntControl);
                            // }
                            //}
                            //break;
                            #endregion
                            #region Creating Time Control
                            // case "time":
                            // Control objTimeControl;
                            //objTimeControl = LoadControl("time.ascx");
                            //objTimeControl.ID = sName;
                            // objTimeControl = objPage.Master.FindControl(sName);                    
                            // if (objTimeControl != null)
                            // {
                            //((Time)objTimeControl).Title = sTitle;
                            //((Time)objTimeControl).OnBlur = "timeLostFocus(this.name);";

                            //if (sOnblur != String.Empty)
                            //    ((Time)objTimeControl).OnBlur = sOnblur;

                            //if (sDisable.ToLower() == "yes")
                            //    ((Time)objTimeControl).Disabled(true);

                            ////objGroupRow = new TableRow();
                            ////objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            ////if (shidden.ToLower() == "yes")
                            ////objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");

                            ////objGroupCell = new TableCell();
                            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            ////if (String.IsNullOrEmpty(sControlPosition))
                            ////    objGroupCell.Attributes.Add("width", "35%");
                            ////else
                            ////    objGroupCell.Attributes.Add("width", sCPTitle);
                            //////End
                            ////objGroupCell.ID = sName + "_ctlTitle";
                            //Label lblCntl = (Label)objPage.Master.FindControl(sName + "_Title");
                            //if (lblCntl != null)
                            //{
                            //    if (sRequired == "yes")
                            //    {
                            //        lblCntl.Font.Bold = true;
                            //    }
                            //    else
                            //        lblCntl.Font.Bold = false;
                            //}
                            ////objGroupRow.Cells.Add(objGroupCell);
                            ////objGroupCell = new TableCell();
                            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            ////if (String.IsNullOrEmpty(sControlPosition))
                            ////    objGroupCell.Attributes.Add("width", "35%");
                            ////else
                            ////    objGroupCell.Attributes.Add("width", sCPControl);
                            //////End
                            ////objGroupCell.Controls.Add(objTimeControl);


                            //if (sRequired == "yes")
                            //{
                            //    RequiredFieldValidator objReqCtl = (RequiredFieldValidator)objTimeControl.FindControl("RequiredFieldValidator1");
                            //    objReqCtl.Visible = true;
                            //    if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //    {
                            //        objReqCtl.Enabled = false;
                            //    }
                            //    //objReqCtl.ID = "Req";
                            //    objReqCtl.ErrorMessage = lblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                            //    // objGroupCell.Controls.Add(objReqCtl);
                            //    //if (objPage.Master.FindControl(sName).Parent is HtmlTableCell)
                            //    //{
                            //    //    ((HtmlTableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //    //}
                            //    //else if (objPage.Master.FindControl(sName).Parent is TableCell)
                            //    //{
                            //    //    ((TableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //    //}
                            //}
                            ////objGroupRow.Cells.Add(objGroupCell);

                            ////objGroupCell = new TableCell();
                            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            ////if (String.IsNullOrEmpty(sControlPosition))
                            ////    objGroupCell.Attributes.Add("width", "30%");
                            ////else
                            ////    objGroupCell.Attributes.Add("width", sCPInstruction);
                            ////End
                            ////objGroupCell.Text = sInstruction;
                            ////objGroupRow.Controls.Add(objGroupCell);

                            ////objGroupTable.Rows.Add(objGroupRow);
                            //if (sDefault == "currenttime")
                            //    ((Time)objTimeControl).codeid = DateTime.Now.ToString("hh:mm tt");
                            ////Setting the value, if any
                            //if (!String.IsNullOrEmpty(sControlValue))
                            //{
                            //    if (sControlValue.IndexOf(":") == -1 && sControlValue.Length == 6)
                            //    {
                            //        sControlValue = Conversion.GetDBTimeFormat(sControlValue, "hh:mm tt");
                            //        //String sHours = String.Empty;
                            //        //String sMinutes = String.Empty;
                            //        //sHours = sControlValue.Substring(0, 2);
                            //        //sMinutes = sControlValue.Substring(2, 2);
                            //        //sControlValue = sHours + ":" + sMinutes;
                            //    }
                            //    ((Time)objTimeControl).codeid = sControlValue;
                            //}

                            ////Added by RKaur
                            ////Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                            ////
                            ////objControls.Add(objTimeControl);
                            //}
                            //}
                            // break;
                            #endregion
                            #region Creating Date(javascript) Control
                            case "date":
                                Control objDateNewControl;
                                objDateNewControl = objPage.FindControl(sName);
                                if (objDateNewControl != null)
                                {
                                    if (sRequired == "yes")
                                    {
                                        RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                                        objReqCtl.ControlToValidate = objDateNewControl.ID + "_ValidateDate";
                                        objReqCtl.ValidationGroup = "btnSubmit";
                                        //if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                                        //{
                                        //    objReqCtl.Enabled = false;
                                        //}
                                        objReqCtl.ID = sID + "_Req";
                                        objReqCtl.Text = "*";
                                        Label lblCntrl = (Label)objPage.FindControl(sID + "_Title");
                                        objReqCtl.ErrorMessage = lblCntrl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                                        if (objPage.FindControl(sName).Parent is HtmlTableCell)
                                        {
                                            ((HtmlTableCell)objPage.FindControl(sName).Parent).Controls.Add(objReqCtl);
                                        }
                                        else if (objPage.FindControl(sName).Parent is TableCell)
                                        {
                                            ((TableCell)objPage.FindControl(sName).Parent).Controls.Add(objReqCtl);
                                        }
                                        //Anu Tennyson moved to javascript
                                        objReqCtl.Enabled = false;
                                    }
                                    //spahariya MITS 26002 start- 09/05/11**********
                                    //modified this for PSO architecture--neha goel
                                    if (sHelp != "")
                                    {
                                        Image objImage = (Image)objPage.FindControl(sID + "_Image");
                                        if (objImage != null)
                                        {
                                            objImage.Attributes.Add("style", "visibility:visible;display:inline");
                                            objImage.Attributes.Add("onMouseOver", "displayPopupDiv('" + sHelp + "','" + sSelectedTextToBold + "');");
                                            objImage.Attributes.Add("onMouseOut", "hidePopupDiv();");
                                        }

                                    }
                                    //*********************************************   


                                }

                                break;
                            #endregion
                            #region Creating Phone Control
                            //case "phone":
                            //    {
                            //        TextBox objPhoneControl = (TextBox)objPage.Master.FindControl(sName);
                            //        //objPhoneControl.ID = sName;
                            //        //Saurabh Walia: Added handling for maxlength.
                            //        //if (sMaxLength != "")
                            //        //    objPhoneControl.MaxLength = Conversion.ConvertStrToInteger(sMaxLength);
                            //        //else//End
                            //        //    objPhoneControl.MaxLength = 30;

                            //        objPhoneControl.Attributes.Add("onblur", "phoneLostFocus(this)");

                            //        if (sDisable.ToLower() == "yes")
                            //        {
                            //            objPhoneControl.Attributes.Add("readonly", "readonly");
                            //            objPhoneControl.Attributes.Add("Style", "BACKGROUND-COLOR: silver");
                            //        }

                            //        //objGroupRow = new TableRow();
                            //        //objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            //        //if (shidden.ToLower() == "yes")
                            //        //    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");

                            //        //neha goel--publix--05122011
                            //        if (sUnHide.ToLower() == "yes")
                            //        {
                            //            HtmlTableRow tablerow = (HtmlTableRow)objPage.Master.FindControl(sName).Parent.Parent;
                            //            tablerow.Attributes.Add("style", "display:inline");
                            //        }
                            //        //objGroupCell = new TableCell();
                            //        //09/04/2009 Saurabh Walia: Handling the Position of the control
                            //        //if (String.IsNullOrEmpty(sControlPosition))
                            //        //    objGroupCell.Attributes.Add("width", "35%");
                            //        //else
                            //        //    objGroupCell.Attributes.Add("width", sCPTitle);
                            //        ////End
                            //        //objGroupCell.ID = sName + "_ctlTitle";
                            //        Label lblCntl = (Label)objPage.Master.FindControl(sName + "_Title");
                            //        if (lblCntl != null)
                            //        {
                            //            if (sRequired == "yes")
                            //            {
                            //                lblCntl.Font.Bold = true;
                            //            }
                            //            else
                            //                lblCntl.Font.Bold = false;
                            //        }
                            //        //objGroupRow.Cells.Add(objGroupCell);
                            //        //objGroupCell = new TableCell();
                            //        ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //        //if (String.IsNullOrEmpty(sControlPosition))
                            //        //    objGroupCell.Attributes.Add("width", "35%");
                            //        //else
                            //        //    objGroupCell.Attributes.Add("width", sCPControl);
                            //        ////End
                            //        //objGroupCell.Controls.Add(objPhoneControl);


                            //        if (sRequired == "yes")
                            //        {
                            //            RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                            //            objReqCtl.ControlToValidate = objPhoneControl.ID;
                            //            if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //            {
                            //                objReqCtl.Enabled = false;
                            //            }
                            //            objReqCtl.ID = objPhoneControl.ID + "_Req";
                            //            objReqCtl.Text = "*";
                            //            objReqCtl.ErrorMessage = lblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                            //            objReqCtl.EnableClientScript = true;
                            //            if (objPage.Master.FindControl(sName).Parent is HtmlTableCell)
                            //            {
                            //                ((HtmlTableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //            }
                            //            else if (objPage.Master.FindControl(sName).Parent is TableCell)
                            //            {
                            //                ((TableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //            }
                            //        }

                            //        //objGroupRow.Cells.Add(objGroupCell);
                            //        //objGroupCell = new TableCell();
                            //        ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //        //if (String.IsNullOrEmpty(sControlPosition))
                            //        //    objGroupCell.Attributes.Add("width", "30%");
                            //        //else
                            //        //    objGroupCell.Attributes.Add("width", sCPInstruction);
                            //        ////End
                            //        //objGroupCell.Text = sInstruction;
                            //        //objGroupRow.Controls.Add(objGroupCell);


                            //        //objGroupTable.Rows.Add(objGroupRow);
                            //        //Setting the defaultvalue if any    
                            //        objPhoneControl.Text = sDefault;
                            //        //Setting the value if any    
                            //        objPhoneControl.Text = sControlValue;

                            //        //Added by RKaur
                            //        //Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                            //        //
                            //        //objControls.Add(objPhoneControl);
                            //        break;
                            //    }
                            #endregion
                            #region Creating Email Control
                            //case "email":
                            //    {
                            //        //TextBox objEmlControl = new TextBox();
                            //        //objEmlControl.ID = sName;
                            //        TextBox objEmlControl = (TextBox)objPage.Master.FindControl(sName);
                            //        if (objEmlControl != null)
                            //        {
                            //            //Abhay-03/20/2009-Added case for maxlength
                            //            // objEmlControl.MaxLength = 17;
                            //            //if (sMaxLength != "")
                            //            //    objEmlControl.MaxLength = Conversion.ConvertStrToInteger(sMaxLength);
                            //            //Abhay-03/20/2009-End


                            //            //Abhinav MITS 22363- 10/04/2010--To call the onblurr function if specified in the xml onblur tag
                            //            if (String.IsNullOrEmpty(sOnblur))
                            //                objEmlControl.Attributes.Add("onblur", "echeck(this)");
                            //            else
                            //                objEmlControl.Attributes.Add("onblur", "echeck(this);" + sOnblur);
                            //            //Abhinav - MITS 22363- End

                            //            if (sDisable.ToLower() == "yes")
                            //            {
                            //                objEmlControl.Attributes.Add("readonly", "readonly");
                            //                objEmlControl.Attributes.Add("Style", "BACKGROUND-COLOR: silver");
                            //            }

                            //            //objGroupRow = new TableRow();
                            //            //objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            //            //if (shidden.ToLower() == "yes")
                            //            //    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");

                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPTitle);
                            //            ////End
                            //            //objGroupCell.ID = sName + "_ctlTitle";
                            //            Label lblCntlTitle = (Label)objPage.Master.FindControl(sName + "_Title");
                            //            if (lblCntlTitle != null)
                            //            {

                            //                if (sRequired == "yes")
                            //                {
                            //                    lblCntlTitle.Font.Bold = true;
                            //                }
                            //                else
                            //                    lblCntlTitle.Font.Bold = false;
                            //            }

                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPControl);
                            //            ////End
                            //            //objGroupCell.Controls.Add(objEmlControl);
                            //            if (sRequired == "yes")
                            //            {
                            //                RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                            //                objReqCtl.ControlToValidate = objEmlControl.ID;
                            //                if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //                {
                            //                    objReqCtl.Enabled = false;
                            //                }
                            //                objReqCtl.ID = objEmlControl.ID + "_Req";
                            //                objReqCtl.Text = "*";
                            //                objReqCtl.ErrorMessage = lblCntlTitle.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                            //                objReqCtl.EnableClientScript = true;
                            //                //objGroupCell.Controls.Add(objReqCtl);
                            //                if (objPage.Master.FindControl(sName).Parent is HtmlTableCell)
                            //                {
                            //                    ((HtmlTableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //                else if (objPage.Master.FindControl(sName).Parent is TableCell)
                            //                {
                            //                    ((TableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //            }

                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "30%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPInstruction);
                            //            ////End
                            //            //objGroupCell.Text = sInstruction;
                            //            //objGroupRow.Controls.Add(objGroupCell);


                            //            //objGroupTable.Rows.Add(objGroupRow);
                            //            //Setting the value if any    
                            //            objEmlControl.Text = sDefault;
                            //            objEmlControl.Text = sControlValue;

                            //            //Added by RKaur
                            //            //Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                            //            //
                            //            //objControls.Add(objEmlControl);
                            //        }
                            //        break;
                            //    }
                            #endregion
                            #region Creating SSN Control
                            //case "ssn":
                            //    {
                            //        //TextBox objSSNControl = new TextBox();
                            //        TextBox objSSNControl = (TextBox)objPage.Master.FindControl(sName);
                            //        //objSSNControl.ID = sName;
                            //        objSSNControl.Attributes.Add("onblur", "ssnLostFocus(this)");

                            //        if (sMode == "password")
                            //            objSSNControl.TextMode = TextBoxMode.Password;

                            //        if (sDisable.ToLower() == "yes")
                            //        {
                            //            objSSNControl.Attributes.Add("readonly", "readonly");
                            //            objSSNControl.Attributes.Add("Style", "BACKGROUND-COLOR: silver");
                            //        }

                            //        //objGroupRow = new TableRow();
                            //        //objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            //        //if (shidden.ToLower() == "yes")
                            //        //    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");

                            //        //objGroupCell = new TableCell();
                            //        //09/04/2009 Saurabh Walia: Handling the Position of the control
                            //        //if (String.IsNullOrEmpty(sControlPosition))
                            //        //    objGroupCell.Attributes.Add("width", "35%");
                            //        //else
                            //        //    objGroupCell.Attributes.Add("width", sCPTitle);
                            //        ////End
                            //        //objGroupCell.ID = sName + "_ctlTitle";

                            //        Label lblCntl = (Label)objPage.Master.FindControl(sName + "_Title");
                            //        if (lblCntl != null)
                            //        {
                            //            if (sRequired == "yes")
                            //            {
                            //                lblCntl.Font.Bold = true;
                            //            }
                            //            else
                            //                lblCntl.Font.Bold = false;
                            //        }
                            //        //objGroupRow.Cells.Add(objGroupCell);
                            //        //objGroupCell = new TableCell();
                            //        ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //        //if (String.IsNullOrEmpty(sControlPosition))
                            //        //    objGroupCell.Attributes.Add("width", "35%");
                            //        //else
                            //        //    objGroupCell.Attributes.Add("width", sCPControl);
                            //        ////End
                            //        //objGroupCell.Controls.Add(objSSNControl);


                            //        if (sRequired == "yes")
                            //        {
                            //            RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                            //            objReqCtl.ControlToValidate = objSSNControl.ID;
                            //            if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //            {
                            //                objReqCtl.Enabled = false;
                            //            }
                            //            objReqCtl.ID = objSSNControl.ID + "_Req";
                            //            objReqCtl.Text = "*";
                            //            objReqCtl.ErrorMessage = lblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                            //            objReqCtl.EnableClientScript = true;
                            //            //objGroupCell.Controls.Add(objReqCtl);
                            //            if (objPage.Master.FindControl(sName).Parent is HtmlTableCell)
                            //            {
                            //                ((HtmlTableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //            }
                            //            else if (objPage.Master.FindControl(sName).Parent is TableCell)
                            //            {
                            //                ((TableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //            }
                            //        }

                            //        //objGroupRow.Cells.Add(objGroupCell);

                            //        //objGroupCell = new TableCell();
                            //        //09/04/2009 Saurabh Walia: Handling the Position of the control
                            //        //if (String.IsNullOrEmpty(sControlPosition))
                            //        //    objGroupCell.Attributes.Add("width", "30%");
                            //        //else
                            //        //    objGroupCell.Attributes.Add("width", sCPInstruction);
                            //        ////End
                            //        //objGroupCell.Text = sInstruction;
                            //        //objGroupRow.Controls.Add(objGroupCell);


                            //        //objGroupTable.Rows.Add(objGroupRow);
                            //        //Setting the default value if any    
                            //        objSSNControl.Text = sDefault;

                            //        //Setting the value if any    
                            //        objSSNControl.Text = sControlValue;

                            //        //Added by RKaur
                            //        //Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                            //        //


                            //        //objControls.Add(objSSNControl);
                            //        break;
                            //    }
                            #endregion
                            #region Creating ZipCode Control
                            //case "zipcode":
                            //    {
                            //        //TextBox objZipControl = new TextBox();
                            //        //objZipControl.ID = sName;
                            //        TextBox objZipControl = (TextBox)objPage.Master.FindControl(sName);
                            //        if (objZipControl != null)
                            //        {
                            //            objZipControl.Attributes.Add("onblur", "zipLostFocus(this)");
                            //            if (sDisable.ToLower() == "yes")
                            //            {
                            //                objZipControl.Attributes.Add("readonly", "readonly");
                            //                objZipControl.Attributes.Add("Style", "BACKGROUND-COLOR: silver");
                            //            }

                            //            if (sMode == "password")
                            //                objZipControl.TextMode = TextBoxMode.Password;

                            //            //objGroupRow = new TableRow();
                            //            //objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            //            //if (shidden.ToLower() == "yes")
                            //            //    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");

                            //            //neha goel--publix--05122011
                            //            if (sUnHide.ToLower() == "yes")
                            //            {
                            //                HtmlTableRow tablerow = (HtmlTableRow)objPage.Master.FindControl(sName).Parent.Parent;
                            //                tablerow.Attributes.Add("style", "display:inline");
                            //            }
                            //            //objGroupCell = new TableCell();
                            //            //09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPTitle);
                            //            ////End
                            //            //objGroupCell.ID = sName + "_ctlTitle";
                            //            Label lblCntl = (Label)objPage.Master.FindControl(sName + "_Title");
                            //            if (lblCntl != null)
                            //            {
                            //                if (sRequired == "yes")
                            //                {
                            //                    lblCntl.Font.Bold = true;
                            //                }
                            //                else
                            //                    lblCntl.Font.Bold = false;
                            //            }
                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPControl);
                            //            ////End
                            //            //objGroupCell.Controls.Add(objZipControl);
                            //            if (sRequired == "yes")
                            //            {
                            //                RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                            //                objReqCtl.ControlToValidate = objZipControl.ID;
                            //                if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //                {
                            //                    objReqCtl.Enabled = false;
                            //                }
                            //                objReqCtl.ID = objZipControl.ID + "_Req";
                            //                objReqCtl.Text = "*";
                            //                objReqCtl.ErrorMessage = lblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                            //                objReqCtl.EnableClientScript = true;
                            //                //objGroupCell.Controls.Add(objReqCtl);
                            //                if (objPage.Master.FindControl(sName).Parent is HtmlTableCell)
                            //                {
                            //                    ((HtmlTableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //                else if (objPage.Master.FindControl(sName).Parent is TableCell)
                            //                {
                            //                    ((TableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //            }

                            //            //objGroupRow.Cells.Add(objGroupCell);

                            //            //objGroupCell = new TableCell();
                            //            //09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "30%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPInstruction);
                            //            ////End
                            //            //objGroupCell.Text = sInstruction;
                            //            //objGroupRow.Controls.Add(objGroupCell);


                            //            //objGroupTable.Rows.Add(objGroupRow);

                            //            //Setting the default value if any    
                            //            objZipControl.Text = sDefault;
                            //            //Setting the value if any    
                            //            objZipControl.Text = sControlValue;

                            //            //Added by RKaur
                            //            //Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                            //            //

                            //            //objControls.Add(objZipControl);
                            //        }
                            //        break;
                            //    }
                            #endregion
                            #region Creating Currency Control
                            //case "currency":
                            //    {
                            //        //TextBox objCurControl = new TextBox();
                            //        //objCurControl.ID = sName;
                            //        TextBox objCurControl = (TextBox)objPage.Master.FindControl(sName);
                            //        if (objCurControl != null)
                            //        {
                            //            //01/05/2010 Saurabh Walia: Added to handle the Hourly rate etc calculations.
                            //            if (String.IsNullOrEmpty(sOnblur))
                            //                objCurControl.Attributes.Add("onblur", "currencyLostFocus(this)");
                            //            else
                            //                objCurControl.Attributes.Add("onblur", "currencyLostFocus(this);" + sOnblur);
                            //            //End
                            //            //11/26/2009 Saurabh Walia: Added maxlength
                            //            //if (sMaxLength != "")
                            //            //    objCurControl.MaxLength = Conversion.ConvertStrToInteger(sMaxLength);

                            //            if (sDisable.ToLower() == "yes")
                            //            {
                            //                objCurControl.Attributes.Add("readonly", "readonly");
                            //                objCurControl.Attributes.Add("Style", "BACKGROUND-COLOR: silver");
                            //            }

                            //            if (sMode == "password")
                            //                objCurControl.TextMode = TextBoxMode.Password;

                            //            //objGroupRow = new TableRow();
                            //            //objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            //            //if (shidden.ToLower() == "yes")
                            //            //    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");

                            //            //objGroupCell = new TableCell();
                            //            //09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPTitle);
                            //            ////End
                            //            //objGroupCell.ID = sName + "_ctlTitle";
                            //            Label lblCntl = (Label)objPage.Master.FindControl(sName + "_Title");
                            //            if (lblCntl != null)
                            //            {
                            //                if (sRequired == "yes")
                            //                {
                            //                    lblCntl.Font.Bold = true;
                            //                }
                            //                else
                            //                    lblCntl.Font.Bold = false;
                            //            }
                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPControl);
                            //            ////End
                            //            //objGroupCell.Controls.Add(objCurControl);
                            //            if (sRequired == "yes")
                            //            {
                            //                RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                            //                objReqCtl.ControlToValidate = objCurControl.ID;
                            //                if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //                {
                            //                    objReqCtl.Enabled = false;
                            //                }
                            //                objReqCtl.ID = objCurControl.ID + "_Req";
                            //                objReqCtl.Text = "*";
                            //                objReqCtl.ErrorMessage = lblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                            //                objReqCtl.EnableClientScript = true;
                            //                //objGroupCell.Controls.Add(objReqCtl);
                            //                if (objPage.Master.FindControl(sName).Parent is HtmlTableCell)
                            //                {
                            //                    ((HtmlTableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //                else if (objPage.Master.FindControl(sName).Parent is TableCell)
                            //                {
                            //                    ((TableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //            }

                            //            //objGroupRow.Cells.Add(objGroupCell);

                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "30%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPInstruction);
                            //            ////End
                            //            //objGroupCell.Text = sInstruction;
                            //            //objGroupRow.Controls.Add(objGroupCell);


                            //            //objGroupTable.Rows.Add(objGroupRow);
                            //            //Setting the default value if any    
                            //            objCurControl.Text = sDefault;
                            //            //Setting the value if any    
                            //            objCurControl.Text = sControlValue;

                            //            //Added by RKaur
                            //            //Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                            //            //

                            //            //objControls.Add(objCurControl);
                            //        }
                            //        break;
                            //    }
                            #endregion
                            #region Creating Memo Control
                            case "memo":
                                {

                                    TextBox objMemoControl = (TextBox)objPage.FindControl(sName);

                                    if (objMemoControl != null)
                                    {

                                        if (sDisable.ToLower() == "yes")
                                        {
                                            objMemoControl.Attributes.Add("readonly", "yes");
                                            //Added by rkaur7
                                            objMemoControl.Attributes.Add("Style", "BACKGROUND-COLOR: silver");
                                        }

                                        //neha goel--publix--07072011---added for auto web form ass occupnts edity functionality
                                        if (sUnHide.ToLower() == "yes")
                                        {
                                            HtmlTableRow tablerow = (HtmlTableRow)objPage.Master.FindControl(sName).Parent.Parent;
                                            tablerow.Attributes.Add("style", "display:inline");
                                        }

                                        //spahariya MITS 26002 start- 09/05/11**********
                                        //modified this for PSO architecture--neha goel
                                        if (sHelp != "")
                                        {
                                            Image objImage = (Image)objPage.FindControl(sID + "_Image");
                                            if (objImage != null)
                                            {
                                                objImage.Attributes.Add("style", "visibility:visible;display:inline");
                                                objImage.Attributes.Add("onMouseOver", "displayPopupDiv('" + sHelp + "','" + sSelectedTextToBold + "');");
                                                objImage.Attributes.Add("onMouseOut", "hidePopupDiv();");
                                            }

                                        }
                                        //*********************************************   

                                        if (sRequired == "yes")
                                        {
                                            RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                                            objReqCtl.ControlToValidate = objMemoControl.ID;
                                            objReqCtl.ValidationGroup = "btnSubmit";
                                            //if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                                            //{
                                            //    objReqCtl.Enabled = false;
                                            //}
                                            objReqCtl.ID = objMemoControl.ID + "_Req";
                                            objReqCtl.Text = "*";
                                            Label lblCntl = (Label)objPage.FindControl(sID + "_Title");
                                            objReqCtl.ErrorMessage = lblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                                            objReqCtl.EnableClientScript = true;
                                            //objGroupCell.Controls.Add(objReqCtl);
                                            if (objPage.FindControl(sName).Parent is HtmlTableCell)
                                            {
                                                ((HtmlTableCell)objPage.FindControl(sName).Parent).Controls.Add(objReqCtl);
                                            }
                                            else if (objPage.FindControl(sName).Parent is TableCell)
                                            {
                                                ((TableCell)objPage.FindControl(sName).Parent).Controls.Add(objReqCtl);
                                            }
                                            //Anu Tennyson moved to javascript
                                            objReqCtl.Enabled = false;
                                        }

                                        //Setting the value if any    
                                        if (sDefault.Trim() != "")
                                            objMemoControl.Text = sDefault;

                                        //Abhinav - Commented and added new block - 09 Sept 2010 ####
                                        ////Setting the value if any    
                                        //if (sControlValue.Trim() != "")
                                        //objMemoControl.Value = sControlValue;

                                        if ((sControlValue.Trim() != "") && (sNotShowSavedValueMemo.Trim() != "yes"))
                                            objMemoControl.Text = sControlValue;


                                        //Abhinav - Commented and added new block - 09 Sept 2010 ####-- End
                                    }
                                    break;
                                }
                            #endregion
                            #region Creating Numeric Control
                            case "numeric":
                                {

                                    TextBox objnumControl = (TextBox)objPage.FindControl(sName);
                                    if (objnumControl != null)
                                    {
                                        //01/05/2010 Saurabh Walia: Added to handle the Hourly rate etc calculations.
                                        if (String.IsNullOrEmpty(sOnblur))
                                            //neha goel--publix--05092011--added new parameter of maxlength to js function
                                            objnumControl.Attributes.Add("onblur", "numLostFocus(this)");
                                        else
                                            objnumControl.Attributes.Add("onblur", "numLostFocus(this);" + sOnblur);
                                        //End                                           

                                        if (sDisable.ToLower() == "yes")
                                        {
                                            objnumControl.Attributes.Add("readonly", "readonly");
                                            objnumControl.Attributes.Add("Style", "BACKGROUND-COLOR: silver");
                                        }

                                        //neha goel--publix--05092011-- adding the ontextchanged function, if present
                                        if (sOnChange != String.Empty)
                                            objnumControl.Attributes.Add("onchange", sOnChange);

                                        //spahariya MITS 26002 start- 09/05/11**********
                                        //modified this for PSO architecture--neha goel
                                        if (sHelp != "")
                                        {
                                            Image objImage = (Image)objPage.FindControl(sID + "_Image");
                                            if (objImage != null)
                                            {
                                                objImage.Attributes.Add("style", "visibility:visible;display:inline");
                                                objImage.Attributes.Add("onMouseOver", "displayPopupDiv('" + sHelp + "','" + sSelectedTextToBold + "');");
                                                objImage.Attributes.Add("onMouseOut", "hidePopupDiv();");
                                            }

                                        }
                                        //*********************************************   

                                        if (sRequired == "yes")
                                        {
                                            RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                                            objReqCtl.ControlToValidate = objnumControl.ID;
                                            objReqCtl.ValidationGroup = "btnSubmit";
                                            //if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                                            //{
                                            //    objReqCtl.Enabled = false;
                                            //}
                                            objReqCtl.ID = objnumControl.ID + "_Req";
                                            objReqCtl.Text = "*";
                                            Label lblCntl = (Label)objPage.FindControl(sID + "_Title");
                                            objReqCtl.ErrorMessage = lblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                                            objReqCtl.EnableClientScript = true;
                                            //objGroupCell.Controls.Add(objReqCtl);
                                            if (objPage.FindControl(sName).Parent is HtmlTableCell)
                                            {
                                                ((HtmlTableCell)objPage.FindControl(sName).Parent).Controls.Add(objReqCtl);
                                            }
                                            else if (objPage.FindControl(sName).Parent is TableCell)
                                            {
                                                ((TableCell)objPage.FindControl(sName).Parent).Controls.Add(objReqCtl);
                                            }
                                            //Anu Tennyson moved to javascript
                                            objReqCtl.Enabled = false;
                                        }

                                    }
                                    break;
                                }
                            #endregion
                            #region Creating Double Control
                            // FPH 4/23/2009 - Added for Double type
                            //case "double":
                            //    {
                            //        //TextBox objdblControl = new TextBox();
                            //        TextBox objdblControl = (TextBox)objPage.Master.FindControl(sName);
                            //        if (objdblControl != null)
                            //        {
                            //            //objdblControl.ID = sName;
                            //            //01/05/2010 Saurabh Walia: Added to handle the Hourly rate etc calculations.
                            //            if (String.IsNullOrEmpty(sOnblur))
                            //                objdblControl.Attributes.Add("onblur", "doubleLostFocus(this,'" + sFixed + "')");
                            //            else
                            //                objdblControl.Attributes.Add("onblur", "doubleLostFocus(this,'" + sFixed + "');" + sOnblur);
                            //            //End

                            //            //11/26/2009 Saurabh Walia: Added maxlength
                            //            //if (sMaxLength != "")
                            //            //    objdblControl.MaxLength = Conversion.ConvertStrToInteger(sMaxLength);

                            //            if (sDisable.ToLower() == "yes")
                            //            {
                            //                objdblControl.Attributes.Add("readonly", "readonly");
                            //                objdblControl.Attributes.Add("style", "background-color: silver");
                            //            }

                            //            if (sMode == "password")
                            //                objdblControl.TextMode = TextBoxMode.Password;

                            //            //objGroupRow = new TableRow();
                            //            //objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            //            //if (shidden.ToLower() == "yes")
                            //            //    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");

                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPTitle);
                            //            ////End
                            //            //objGroupCell.ID = sName + "_ctlTitle";
                            //            Label lblCntl = (Label)objPage.Master.FindControl(sName + "_Title");
                            //            if (lblCntl != null)
                            //            {
                            //                if (sRequired == "yes")
                            //                {
                            //                    lblCntl.Font.Bold = true;
                            //                }
                            //                else
                            //                    lblCntl.Font.Bold = false;
                            //            }
                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPControl);
                            //            ////End
                            //            //objGroupCell.Controls.Add(objdblControl);
                            //            if (sRequired == "yes")
                            //            {
                            //                RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                            //                objReqCtl.ControlToValidate = objdblControl.ID;
                            //                if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //                {
                            //                    objReqCtl.Enabled = false;
                            //                }
                            //                objReqCtl.ID = objdblControl.ID + "_Req";
                            //                objReqCtl.Text = "*";
                            //                objReqCtl.ErrorMessage = lblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                            //                objReqCtl.EnableClientScript = true;
                            //                //bjGroupCell.Controls.Add(objReqCtl);
                            //                if (objPage.Master.FindControl(sName).Parent is HtmlTableCell)
                            //                {
                            //                    ((HtmlTableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //                else if (objPage.Master.FindControl(sName).Parent is TableCell)
                            //                {
                            //                    ((TableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //            }

                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "30%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPInstruction);
                            //            ////End
                            //            //objGroupCell.Text = sInstruction;
                            //            //objGroupRow.Controls.Add(objGroupCell);
                            //            //objGroupTable.Rows.Add(objGroupRow);
                            //            //Setting the value if any    
                            //            objdblControl.Text = sControlValue;
                            //            //Added by RKaur
                            //            //Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                            //            //

                            //            //objControls.Add(objdblControl);
                            //        }
                            //        break;
                            //    }
                            #endregion
                            #region Creating Comment Control
                            //case "comments":
                            //    {
                            //        //objGroupRow = new TableRow();
                            //        //objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            //        //if (shidden.ToLower() == "yes")
                            //        //    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");


                            //        //objGroupCell = new TableCell();
                            //        //objGroupCell.Attributes.Add("width", "100%");
                            //        //objGroupCell.Attributes.Add("colspan", "3");
                            //        //objGroupCell.ID = sName + "_ctlTitle";

                            //        //if (sRequired == "yes")
                            //        //{
                            //        //    objGroupCell.Text = "<u>" + sTitle + "</u>";
                            //        //}
                            //        //else
                            //        //    objGroupCell.Text = "<p>" + sTitle + "</p>";
                            //        //objGroupRow.Cells.Add(objGroupCell);

                            //        //objGroupTable.Rows.Add(objGroupRow);

                            //        break;
                            //    }
                            #endregion
                            #region Creating CheckBox Control
                            //case "checkbox":
                            //    {
                            //        //CheckBox objControl = new CheckBox();
                            //        //objControl.ID = sName;
                            //        CheckBox objControl = (CheckBox)objPage.Master.FindControl(sName);
                            //        //sw adding the onblur function, if present
                            //        if (objControl != null)
                            //        {
                            //            if (sOnblur != String.Empty)
                            //                objControl.Attributes.Add("onblur", sOnblur);

                            //            if (sDisable.ToLower() == "yes")
                            //            {
                            //                objControl.Enabled = false;
                            //                objControl.BackColor = System.Drawing.Color.Silver;
                            //            }

                            //            //objGroupRow = new TableRow();
                            //            //objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            //            //if (shidden.ToLower() == "yes")
                            //            //    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");


                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPTitle);
                            //            ////End
                            //            //objGroupCell.ID = sName + "_ctlTitle";
                            //            Label lblCntl = (Label)objPage.Master.FindControl(sName + "_Title");
                            //            if (lblCntl != null)
                            //            {
                            //                if (sRequired == "yes")
                            //                {
                            //                    lblCntl.Font.Bold = true;
                            //                }
                            //                else
                            //                    lblCntl.Font.Bold = false;
                            //            }
                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "35%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPControl);
                            //            ////End

                            //            //objGroupCell.Controls.Add(objControl);
                            //            if (sRequired == "yes")
                            //            {
                            //                RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                            //                objReqCtl.ControlToValidate = objControl.ID;
                            //                if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //                {
                            //                    objReqCtl.Enabled = false;
                            //                }
                            //                objReqCtl.ID = objControl.ID + "_Req";
                            //                objReqCtl.Text = "*";
                            //                objReqCtl.ErrorMessage = lblCntl.Text + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                            //                objReqCtl.EnableClientScript = true;
                            //                //objGroupCell.Controls.Add(objReqCtl);
                            //                if (objPage.Master.FindControl(sName).Parent is HtmlTableCell)
                            //                {
                            //                    ((HtmlTableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //                else if (objPage.Master.FindControl(sName).Parent is TableCell)
                            //                {
                            //                    ((TableCell)objPage.Master.FindControl(sName).Parent).Controls.Add(objReqCtl);
                            //                }
                            //            }
                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();
                            //            ////09/04/2009 Saurabh Walia: Handling the Position of the control
                            //            //if (String.IsNullOrEmpty(sControlPosition))
                            //            //    objGroupCell.Attributes.Add("width", "30%");
                            //            //else
                            //            //    objGroupCell.Attributes.Add("width", sCPInstruction);
                            //            ////End
                            //            //objGroupCell.Text = sInstruction;
                            //            //objGroupRow.Controls.Add(objGroupCell);

                            //            //objGroupTable.Rows.Add(objGroupRow);
                            //            ////Added by RKaur
                            //            //Comments(sComments, objGroupTable, ref objGroupRow, ref objGroupCell);
                            //            //

                            //            if (sDefault.ToUpper() == "TRUE")//changed by shilpi for the reviewer case
                            //                objControl.Checked = true;
                            //            else
                            //                objControl.Checked = false;
                            //            if (sControlValue.ToUpper() == "TRUE")//changed by shilpi for the reviewer case
                            //                objControl.Checked = true;
                            //            else
                            //                objControl.Checked = false;

                            //            //Setting the value if any    
                            //            // objControl.Text   = sControlValue;
                            //            //objControls.Add(objControl);
                            //        }
                            //        break;
                            //    }
                            #endregion
                            #region Creating Hidden Control

                            //case "id":
                            //    {
                            //        //HiddenField objControl = new HiddenField();
                            //        //objControl.ID = sName;
                            //        HiddenField objControl = (HiddenField)objPage.Master.FindControl(sName);
                            //        if (objControl != null)
                            //        {
                            //            //objGroupRow = new TableRow();
                            //            //objGroupRow.Attributes.Add("id", sName + "_ctlRow");
                            //            //if (shidden.ToLower() == "yes")
                            //            //    objGroupRow.Attributes.Add("style", "visibility:hidden;display:none");


                            //            //objGroupCell = new TableCell();
                            //            //objGroupCell.Attributes.Add("width", "35%");
                            //            //objGroupCell.ID = sName + "_ctlTitle";

                            //            /* if (sRequired == "yes")
                            //             {
                            //                 objGroupCell.Text = "<u>" + sTitle + "</u>";
                            //             }
                            //             else
                            //                 objGroupCell.Text = sTitle;*/
                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();
                            //            //objGroupCell.Attributes.Add("width", "35%");

                            //            //objGroupCell.Controls.Add(objControl);
                            //            //if (sRequired == "yes")
                            //            //{
                            //            //    RequiredFieldValidator objReqCtl = new RequiredFieldValidator();
                            //            //    objReqCtl.ControlToValidate = objControl.ID;
                            //            //    if (shidden.ToLower() == "yes" || sGrouphidden.ToLower() == "yes" || sDisable.ToLower() == "yes")
                            //            //    {
                            //            //        objReqCtl.Enabled = false;
                            //            //    }
                            //            //    objReqCtl.ID = objControl.ID + "_Req";
                            //            //    objReqCtl.Text = "*";
                            //            //    objReqCtl.ErrorMessage = sTitle + " is a Required Field."; //01/27/2010 Saurabh Walia: MITS 19600
                            //            //    objReqCtl.EnableClientScript = true;
                            //            //    objGroupCell.Controls.Add(objReqCtl);
                            //            //}

                            //            //objGroupRow.Cells.Add(objGroupCell);
                            //            //objGroupCell = new TableCell();

                            //            //objGroupCell.Attributes.Add("width", "30%");
                            //            //objGroupCell.Text = sInstruction;
                            //            //objGroupRow.Controls.Add(objGroupCell);

                            //            //objGroupTable.Rows.Add(objGroupRow);

                            //            //Setting the default value if any    
                            //            //objControl.Value = sDefault;  //Saurabh Walia 11/20/2009: Commenting since NA.
                            //            //Setting the value if any    
                            //            //Saurabh Walia 11/20/2009: Handling the cases
                            //            if (!String.IsNullOrEmpty(sControlValue))
                            //            {
                            //                String sIdCodeTable = String.Empty;
                            //                if (!String.IsNullOrEmpty(sConversion))
                            //                {
                            //                    switch (sConversion.ToUpper())
                            //                    {
                            //                        case "STATES":
                            //                            //03/26/2010 Saurabh Walia: Handled incorrect syntax.Start
                            //                            String sStateId = string.Empty;
                            //                            String sStateName = string.Empty;
                            //                            StringBuilder sSQL = new StringBuilder();
                            //                            sSQL.Append("SELECT S.STATE_ROW_ID AS 'ID' FROM STATES S WHERE STATE_NAME<>''");
                            //                            sSQL.Append(" AND S.STATE_ID ='").Append(sControlValue.Replace("%", "").Split('#')[0].ToString().Trim()).Append("'");

                            //                            //sControlValue = GlobalClass.DataModelObject.Context.DbConn.ExecuteString(sSQL.ToString());
                            //                            //sControlValue = GlobalClass.DataModelObject.Context.LocalCache.GetStateRowID(sControlValue).ToString();
                            //                            //03/26/2010 Saurabh Walia: Handled incorrect syntax.End
                            //                            break;
                            //                        case "MULTICODE":
                            //                            {
                            //                                sIdCodeTable = sControlValue.Replace("%", "").Split('#')[1];
                            //                                String sFinalValue = String.Empty;
                            //                                for (int i = 0; i < sControlValue.Replace("%", "").Split('#')[0].Split(',').Length; i++)
                            //                                {
                            //                                    //sFinalValue = sFinalValue + "," + GlobalClass.DataModelObject.Context.LocalCache.GetCodeId(sControlValue.Replace("%", "").Split('#')[0].Split(',')[i], sIdCodeTable).ToString();
                            //                                }
                            //                                sControlValue = sFinalValue.Remove(0, 1);
                            //                            }
                            //                            break;
                            //                        case "CODE":
                            //                            sIdCodeTable = sControlValue.Replace("%", "").Split('#')[1];
                            //                            //sControlValue = GlobalClass.DataModelObject.Context.LocalCache.GetCodeId(sControlValue.Replace("%", "").Split('#')[0], sIdCodeTable).ToString();
                            //                            break;
                            //                        //03/16/2010 Saurabh Walia: Added to handle person involveds Table id.Start
                            //                        case "TABLE":
                            //                            //sControlValue = GlobalClass.DataModelObject.Context.LocalCache.GetTableId(sValueID).ToString();
                            //                            break;
                            //                        //03/16/2010 Saurabh Walia: Added to handle person involveds Table id.Start

                            //                    }
                            //                }

                            //                if (sControlValue.Trim() == "systemdate")
                            //                    sControlValue = DateTime.Now.ToString("MM/dd/yyyy");
                            //                if (sControlValue.Trim() == "systemtime")
                            //                    sControlValue = DateTime.Now.ToString("hh:mm tt");
                            //            }
                            //            //End
                            //            objControl.Value = sControlValue;

                            //            //objControls.Add(objControl);
                            //        }
                            //        break;
                            //    }
                            #endregion

                        }

                    }
                }
                #endregion
            }

        }
        catch (Exception ex)
        {
            //    String sTrace = Environment.StackTrace;
            //    sTrace = RiskMaster.CustomWeb.Common.GetFunctionName(sTrace);
            //    Log.Write(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt") + " : " + sTrace + " : " + ex.StackTrace + ex.Message, "default");
        }
        finally
        {
            xmlDoM = null;

        }

    }


    /// Name		    : GetAttributeValue
    /// Author		    : Saurabh Walia
    /// Date Created	: 	
    /// <summary>
    /// This function gets the passed attribute value of the input node
    /// </summary>
    /// <params name="objXmlNode">xml node whose attribute's value need to be read</params>
    /// <params name="sAttributeName">attribute name whose value need to be get</params>
    /// <params name="bLower">bool value indicating whether the value required in lower case or not</params>
    ///<returns type="string">return value</returns>
    public static string GetAttributeValue(XmlElement objXmlNode, String sAttributeName, bool bLower)
    {
        String sValue = String.Empty;
        try
        {
            if (objXmlNode.Attributes[sAttributeName] != null)
            {
                if (bLower)
                    sValue = objXmlNode.Attributes[sAttributeName].Value.Trim().ToLower();
                else
                    sValue = objXmlNode.Attributes[sAttributeName].Value.Trim();
            }
        }
        catch (Exception Ex)
        {
            String sTrace = Environment.StackTrace;
            // sTrace = Common.GetFunctionName(sTrace);
            //Log.Write(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt") + " : " + sTrace + " : " + Ex.StackTrace + Ex.Message, "default");
            return String.Empty;
        }
        return sValue;
    }

     
    /// <summary>
    /// Get the page name, such as, QuickLookup.aspx
    /// </summary>
    /// <param name="sFullOrigionalpath">Request.url value</param>
    /// <returns></returns>
    public static string GetPageName(string sFullOrigionalpath)
    {
        string sPageName = string.Empty;

        Regex rx = new Regex(@"/(?<pagename>[\w_-]+\.aspx)");
        if (rx.IsMatch(sFullOrigionalpath))
        {
            sPageName = rx.Match(sFullOrigionalpath).Result("${pagename}");
        }

        return sPageName;
    }

    //srajindersin - Pentesting - Cross site Scripting - MITS 27682
    public static string HTMLCustomEncode(string inputString)
    {
        string encodedString = string.Empty;

        //encodedString = inputString.Replace("<", "").Replace(">", "");

        //return Microsoft.Security.Application.Encoder.HtmlEncode(encodedString);
        encodedString = System.Web.HttpUtility.HtmlEncode(inputString);

        while (encodedString.Contains("&amp;"))
            encodedString = encodedString.Replace("&amp;", "&");

        if(encodedString.Contains("^@")) // ijha : MITS 28664 :use of & 
            encodedString = encodedString.Replace("^@", "&");
        if (encodedString.Contains("*@")) // asharma326 Use of # MITS 34294
            encodedString = encodedString.Replace("*@", "#");
        if (encodedString.Contains("@*^"))
            encodedString = encodedString.Replace("@*^", "'"); //ygoyal3, MITS 34895, DT:01/15/2014
        return encodedString;
    }
    //END srajindersin - Pentesting - Cross site Scripting - MITS 27682

    //bpaskova JIRA 10181 - start
    /// <summary>
    /// ensure special encoding for signs '&' and '#' in the input string
    /// </summary>
    /// <param name="inputString">input string</param>
    /// <returns>Returns input string with replaced spacial signs.</returns>
    public static string EncodeSpecialSigns(string inputString)
    {
        if (string.IsNullOrEmpty(inputString))
        {
            return String.Empty;
        }
        else
        {
            return inputString.Replace("&", "^@").Replace("#", "*@");
        }
    }
    //bpaskova JIRA 10181 - end

    public static string HTMLCustomDecode(string inputString)
    {
        return System.Web.HttpUtility.HtmlDecode(inputString);
    }

    //Rakhel ML Changes - to get date according to current culture.
    public static string GetDate(string sValue)
    {
        string sUIDate = string.Empty;
        //DateTimeFormatInfo currentDateTimeFormat = null;
        string svalueExpected =string.Empty;
        //currentDateTimeFormat = (new CultureInfo(AppHelper.GetCulture().ToString())).DateTimeFormat;
        DateTime dtvalue;
        try
        {
            if (string.IsNullOrEmpty(sValue))
            {
                return sUIDate;
            }
            Regex rgx = new Regex("[^a-z0-9]");
            if (rgx.IsMatch(sValue))
            {
                //true;
                dtvalue = DateTime.Parse(sValue);
            }
            else
            {
                // false;
                if (sValue.Length > 10)
                {
                    dtvalue = DateTime.ParseExact(sValue, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtvalue = DateTime.ParseExact(sValue, "yyyyMMdd", CultureInfo.InvariantCulture);
                }
            }
            //DateTime dtvalue = DateTime.Parse(sValue);
            //svalueExpected = dtvalue.ToString(currentDateTimeFormat.ShortDatePattern);
            svalueExpected = dtvalue.ToString(GetDateFormat());
            if (String.Compare(sValue, svalueExpected) != 0)
            {
                sUIDate = svalueExpected;
            }
            else
            {
                sUIDate = sValue;
            }
        }
        catch
        {
            sUIDate = sValue;
        }
        return sUIDate;
    }

    //Rakhel ML Changes - to get time according to current culture.
    public static string GetTime(string sValue)
    {
        string sUITime = string.Empty;
        DateTimeFormatInfo currentDateTimeFormat = null;
        string svalueExpected = string.Empty;
        currentDateTimeFormat = (new CultureInfo(AppHelper.GetCulture().ToString())).DateTimeFormat;
        DateTime dtvalue;
        //Raman MITS 31010
        if (string.IsNullOrEmpty(sValue))
        {
            return sUITime;
        }
        //if (sValue.Length < 7)
        //{
        //    sValue = "20010101" + sValue;
        //}
        //dtvalue = DateTime.ParseExact(sValue, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
        //as per rakhel suggestion.
        Regex rgx = new Regex("[^a-z0-9]");
        if (rgx.IsMatch(sValue))
        {
            //true;
            dtvalue = DateTime.Parse(sValue);
        }
        else
        {
            if (sValue.Length < 7)
            {
                sValue = "20010101" + sValue;
            }
            dtvalue = DateTime.ParseExact(sValue, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
        }
       // DateTime dtvalue = DateTime.Parse(sValue);
       
        try
        {
            //Raman MITS 31010 1/16/2013
            //Moving this null/blank check before initialising the datetime object
            //if (string.IsNullOrEmpty(sValue))
            //{
            //    return sUITime;
            //}
            svalueExpected = dtvalue.ToString(currentDateTimeFormat.ShortTimePattern);
            if (String.Compare(sValue, svalueExpected) != 0)
            {
                sUITime = svalueExpected;
            }
            else
            {
                sUITime = sValue;
            }
        }
        catch
        {
            sUITime = string.Empty;
        }
      
        return sUITime;
    }

    
	
	 public static string GetResourceValue(string pageID, string strResourceKey, string strResourceType)
    {
        return RMXResourceProvider.GetSpecificObject(strResourceKey, pageID, strResourceType).ToString();
    }

    //Rakhel: Multi Language Changes
    /// <summary>
    /// Get Date format for Language Code / User login details
    /// </summary>
    /// <returns>DateFormat</returns>
    public static string GetDateFormat()
     {
        // akaushik5 Changed for MITS 38073 Starts
        //string sDateFormat = string.Empty;
        //string sLangCode = ReadCookieValue("LanguageCode");
        //sLangCode = sLangCode.Split('|')[0].ToString();
        //string sDsn = ReadCookieValue("DsnName");
        ////Riskmaster.UI.RMAuthentication.AuthenticationServiceClient authService = null;
        //try
        //{
        //    AuthData oAuthData = new AuthData();
        //    oAuthData.UserName = HttpContext.Current.User.Identity.Name;
        //    oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
        //    oAuthData.DsnName = DSNName;
        //    //authService = new Riskmaster.UI.RMAuthentication.AuthenticationServiceClient();
        //    if (string.IsNullOrEmpty(sLangCode))
        //    {
               
        //        if ((string.IsNullOrEmpty(sDsn) || string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name)) && HttpContext.Current.Items["loginName"] != null)
        //        {
        //            sDateFormat = AppHelper.GetResponse<string>("RMService/Authenticate/dateformat", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
        //            //sDateFormat = authService.GetDateFormatFromLoginDetails(HttpContext.Current.Items["loginName"].ToString(), DSNName);
        //        }
        //        else
        //        {
        //            if (string.IsNullOrEmpty(sDsn))
        //            {
        //                sDateFormat = AppHelper.GetResponse<string>("RMService/Authenticate/dateformat", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
        //                //sDateFormat = authService.GetDateFormatFromLoginDetails(HttpContext.Current.User.Identity.Name, DSNName);
        //            }
        //            else
        //            {
        //                oAuthData.DsnName = sDsn;
        //                sDateFormat = AppHelper.GetResponse<string>("RMService/Authenticate/dateformat", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
        //                //sDateFormat = authService.GetDateFormatFromLoginDetails(HttpContext.Current.User.Identity.Name, sDsn);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        oAuthData.LangCode = int.Parse(sLangCode);
        //        sDateFormat = AppHelper.GetResponse<string>("RMService/Authenticate/dateformatbycode", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
        //        //sDateFormat = authService.GetDateFormatFromLangCode(sLangCode,AppHelper.ClientId);
        //    }
        //    oAuthData = null;
        //}
        //finally
        //{
        //    //if (authService != null)
        //    //{
        //    //    authService.Close();
        //    //}
        //}
        //return sDateFormat;
 
        return AppHelper.GetLanguageSpecficValue("DateFormat");
         // akaushik5 Changed for MITS 38073 Ends
     }
    
    public enum HttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    /// <summary>
    /// Created By Deb
    /// Content Type
    /// </summary>
    public const string APPLICATION_JSON = "application/json";
    public const string APPLICATION_XML = "application/xml";
    /// <summary>
    /// Created By Deb for calling REST service 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="uri"></param>
    /// <param name="oMethod"></param>
    /// <param name="ContentType"></param>
    /// <param name="oData"></param>
    /// <returns></returns>
    public static T GetResponse<T>(string uri, HttpVerb oMethod, string ContentType, object oData)
    {
        WebXClient client = null;
        string data = string.Empty;
        string responseString = string.Empty;
        try
        {
            client = new WebXClient(Riskmaster.Common.CommonFunctions.RetrieveTimeOut());
            uri = uri.Trim('/');
            if (HttpContext.Current != null)
                uri = Riskmaster.Common.CommonFunctions.RetrieveBaseURL() + uri;
            client.Encoding = Encoding.UTF8;
            client.Headers[HttpRequestHeader.ContentType] = ContentType; // working
            if (oMethod == HttpVerb.GET)
            {
                responseString = client.DownloadString(uri);
            }
            else
            {
                data = AppHelper.ConvertCharToHTML("{\"o" + oData.GetType().Name + "\": " + JSONSerializeDeserialize.Serialize(oData) + "}");
                responseString = client.UploadString(uri, oMethod.ToString(), data);
            }
            //string data = AppHelper.ConvertCharToHTML("{\"o" + oData.GetType().Name + "\": " + Newtonsoft.Json.JsonConvert.SerializeObject(oData) + "}");
            //return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseString);
            return JSONSerializeDeserialize.DeserializeJSon<T>(responseString);
        }
        catch (WebException wex)
        {
            string exMessage = string.Empty;
            if (wex.Response != null)
            {
                using (StreamReader r = new StreamReader(wex.Response.GetResponseStream()))
                {
                    exMessage = r.ReadToEnd();
                }
                // the fault xml 
                if (!string.IsNullOrEmpty(exMessage))
                {
                    XElement oXFault = XElement.Parse(exMessage);
                    XNamespace ns = oXFault.Name.Namespace.NamespaceName;
                    var namespaceManager = new XmlNamespaceManager(new NameTable());
                    namespaceManager.AddNamespace("rmaprefix", oXFault.Name.Namespace.NamespaceName);
                    XElement oReason = oXFault.XPathSelectElement(".//rmaprefix:Reason/rmaprefix:Text", namespaceManager);
                    RMException theFault = new RMException();
                    try
                    {
                        XElement oXFaultReason = XElement.Parse(oReason.Value);
                        theFault.Errors = oReason.Value;
                        throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
                    }
                    catch
                    {
                        throw new FaultException<RMException>(theFault, new FaultReason(oReason.Value), new FaultCode("Sender"));
                    }
                }
                else
                {
                    RMException theFault = new RMException();
                    throw new FaultException<RMException>(theFault, new FaultReason(wex.Message), new FaultCode("Sender"));
                }
            }
            return default(T);
        }
        catch (Exception ex)
        {
            return default(T);
        }
        finally
        {
            if (client != null)
            {
               client = null;
            }
        }
        #region Commented for now
        //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri((uri));
        //request.Method = oMethod.ToString();
        //request.ContentLength = 0;
        //request.ContentType = ContentType;
        //if (!string.IsNullOrEmpty(data) && oMethod == HttpVerb.POST)
        //{
        //    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
        //    byte[] bytes = encoding.GetBytes(data);
        //    request.ContentLength = bytes.Length;
        //    using (Stream requestStream = request.GetRequestStream())
        //    {
        //        // Send the data.
        //        requestStream.Write(bytes, 0, bytes.Length);
        //    }
        //}
        //using (var response = (HttpWebResponse)request.GetResponse())
        //{
        //    var responseValue = string.Empty;
        //    if (response.StatusCode != HttpStatusCode.OK)
        //    {
        //        var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
        //        throw new ApplicationException(message);
        //    }
        //    using (var responseStream = response.GetResponseStream())
        //    {
        //        if (responseStream != null)
        //            using (var reader = new StreamReader(responseStream))
        //            {
        //                responseValue = reader.ReadToEnd();
        //            }
        //    }
        //    return responseValue;
        //}
        //using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        //{
        //    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(object));
        //    return ser.ReadObject(response.GetResponseStream());
        //}
        #endregion
    }
    /// <summary>
    /// GetAsyncResponse
    /// </summary>
    /// <param name="uri"></param>
    /// <param name="oMethod"></param>
    /// <param name="ContentType"></param>
    /// <param name="oData"></param>
    public static void GetAsyncResponse(string uri, HttpVerb oMethod, string ContentType, object oData)
    {
        WebXClient client = null;
        string data = string.Empty;
        string responseString = string.Empty;
        try
        {
            client = new WebXClient(Riskmaster.Cache.CacheCommonFunctions.RetreiveValueFromCache<int>("TimeOut", 0));
            uri = uri.Trim('/');
            if (HttpContext.Current != null)
                uri = Riskmaster.Common.CommonFunctions.RetrieveBaseURL() + uri;
            client.Encoding = Encoding.UTF8;
            client.Headers[HttpRequestHeader.ContentType] = ContentType; // working
            if (oMethod == HttpVerb.GET)
            {
                responseString = client.DownloadString(uri);
            }
            else
            {
                data = AppHelper.ConvertCharToHTML("{\"o" + oData.GetType().Name + "\": " + JSONSerializeDeserialize.Serialize(oData) + "}");
                client.UploadStringAsync((new Uri(uri)), oMethod.ToString(), data);
            }
        }
        catch (WebException wex)
        {
            string exMessage = string.Empty;
            if (wex.Response != null)
            {
                using (StreamReader r = new StreamReader(wex.Response.GetResponseStream()))
                {
                    exMessage = r.ReadToEnd();
                }
                // the fault xml 
                if (!string.IsNullOrEmpty(exMessage))
                {
                    XElement oXFault = XElement.Parse(exMessage);
                    XNamespace ns = oXFault.Name.Namespace.NamespaceName;
                    var namespaceManager = new XmlNamespaceManager(new NameTable());
                    namespaceManager.AddNamespace("rmaprefix", oXFault.Name.Namespace.NamespaceName);
                    XElement oReason = oXFault.XPathSelectElement(".//rmaprefix:Reason/rmaprefix:Text", namespaceManager);
                    RMException theFault = new RMException();
                    try
                    {
                        XElement oXFaultReason = XElement.Parse(oReason.Value);
                        theFault.Errors = oReason.Value;
                        throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
                    }
                    catch
                    {
                        throw new FaultException<RMException>(theFault, new FaultReason(oReason.Value), new FaultCode("Sender"));
                    }
                }
                else
                {
                    RMException theFault = new RMException();
                    throw new FaultException<RMException>(theFault, new FaultReason(wex.Message), new FaultCode("Sender"));
                }
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (client != null)
            {
                client = null;
            }
        }
    }
    /// <summary>
    /// GetResponse
    /// </summary>
    /// <param name="uri"></param>
    /// <param name="oMethod"></param>
    /// <param name="ContentType"></param>
    /// <param name="oData"></param>
    public static void GetResponse(string uri, HttpVerb oMethod, string ContentType, object oData)
    {
        WebXClient client = null;
        string data = string.Empty;
        string responseString = string.Empty;
        try
        {
            client = new WebXClient(Riskmaster.Common.CommonFunctions.RetrieveTimeOut());
            uri = uri.Trim('/');
            if (HttpContext.Current != null)
                uri = Riskmaster.Common.CommonFunctions.RetrieveBaseURL() + uri;
            client.Encoding = Encoding.UTF8;
            client.Headers[HttpRequestHeader.ContentType] = ContentType; // working
            if (oMethod == HttpVerb.GET)
            {
                responseString = client.DownloadString(uri);
            }
            else
            {
                data = AppHelper.ConvertCharToHTML("{\"o" + oData.GetType().Name + "\": " + JSONSerializeDeserialize.Serialize(oData) + "}");
                responseString = client.UploadString(uri, oMethod.ToString(), data);
            }
        }
        catch (WebException wex)
        {
            string exMessage = string.Empty;
            if (wex.Response != null)
            {
                using (StreamReader r = new StreamReader(wex.Response.GetResponseStream()))
                {
                    exMessage = r.ReadToEnd();
                }
                // the fault xml 
                if (!string.IsNullOrEmpty(exMessage))
                {
                    XElement oXFault = XElement.Parse(exMessage);
                    XNamespace ns = oXFault.Name.Namespace.NamespaceName;
                    var namespaceManager = new XmlNamespaceManager(new NameTable());
                    namespaceManager.AddNamespace("rmaprefix", oXFault.Name.Namespace.NamespaceName);
                    XElement oReason = oXFault.XPathSelectElement(".//rmaprefix:Reason/rmaprefix:Text", namespaceManager);
                    RMException theFault = new RMException();
                    try
                    {
                        XElement oXFaultReason = XElement.Parse(oReason.Value);
                        theFault.Errors = oReason.Value;
                        throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
                    }
                    catch
                    {
                        throw new FaultException<RMException>(theFault, new FaultReason(oReason.Value), new FaultCode("Sender"));
                    }
                }
                else
                {
                    RMException theFault = new RMException();
                    throw new FaultException<RMException>(theFault, new FaultReason(wex.Message), new FaultCode("Sender"));
                }
            }
        }
        catch (Exception ex)
        {
            RMException theFault = new RMException();
            throw new FaultException<RMException>(theFault, new FaultReason(ex.Message), new FaultCode("Sender"));
        }
        finally
        {
            if (client != null)
            {
                client = null;
            }
        }
    }
    
    // akaushik5 Added for MITS 38073 Starts
    /// <summary>
    /// Gets the language specfic value.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <returns></returns>
    public static string GetLanguageSpecficValue(string key)
    {
        string languageCode = string.Empty;
        string resourceValue = string.Empty;
        Dictionary<string, string> resCacheByKey = null;

        try
        {
            if (string.IsNullOrEmpty(key))
            {
                return resourceValue;
            }

            languageCode = AppHelper.GetLanguageCode();
            key = key.ToLower();
            if (AppHelper.languageCache.ContainsKey(languageCode))
            {
                resCacheByKey = languageCache[languageCode];
                if (!object.ReferenceEquals(resCacheByKey, null) && resCacheByKey.ContainsKey(key))
                {
                    resourceValue = resCacheByKey[key];
                }
            }

            if (string.IsNullOrEmpty(resourceValue))
            {
                resourceValue = AppHelper.AddLanguageSpecficValue(languageCode, key);
            }
        }
        catch { }
        return resourceValue;
    }

    /// <summary>
    /// Adds the language specfic value.
    /// </summary>
    /// <param name="languageCode">The language code.</param>
    /// <param name="resourceKey">The resource key.</param>
    /// <returns>The resource value.</returns>
    private static string AddLanguageSpecficValue(string languageCode, string resourceKey)
    {
        Dictionary<string, string> resCacheByKey = null;
        string resourceValue = string.Empty;
        try
        {
            switch (resourceKey)
            {
                case "dateformat":
                    {
                        resourceValue = AppHelper.GetDateFormatByLanguageCode(languageCode);
                        break;
                    }
            }

            if (!string.IsNullOrEmpty(resourceValue))
            {
                if (AppHelper.languageCache.ContainsKey(languageCode))
                {
                    resCacheByKey = languageCache[languageCode];
                    if (object.ReferenceEquals(resCacheByKey, null))
                    {
                        resCacheByKey = new Dictionary<string, string>();
                    }

                    if (!resCacheByKey.ContainsKey(resourceKey))
                    {
                        resCacheByKey.Add(resourceKey, resourceValue);
                    }
                }
                else
                {
                    resCacheByKey = new Dictionary<string, string>();
                    resCacheByKey.Add(resourceKey, resourceValue);
                    languageCache.Add(languageCode, resCacheByKey);
                }
            }
        }
        catch
        { }
        return resourceValue;
    }

    /// <summary>
    /// Gets the date format by language code.
    /// </summary>
    /// <param name="languageCode">The language code.</param>
    /// <returns></returns>
    private static string GetDateFormatByLanguageCode(string languageCode)
    {
        string dateFormat = string.Empty;
        AuthData oAuthData = new AuthData();
        oAuthData.UserName = HttpContext.Current.User.Identity.Name;
        oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
        oAuthData.DsnName = DSNName;
        oAuthData.LangCode = int.Parse(languageCode);
        dateFormat = AppHelper.GetResponse<string>("RMService/Authenticate/dateformatbycode", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
        return dateFormat;
    }
    // akaushik5 Added for MITS 38073 Ends
}
/// <summary>
/// Created by Deb for JSON implementation
/// </summary>
public class JSONSerializeDeserialize
{
    /// <summary>
    /// Serialize the object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string Serialize<T>(T obj)
    {
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
        MemoryStream ms = new MemoryStream();
        serializer.WriteObject(ms, obj);
        string retVal = Encoding.UTF8.GetString(ms.ToArray());
        ms.Close();
        return retVal;
    }
    /// <summary>
    /// Deserialize the object with property exposed
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="json"></param>
    /// <returns></returns>
    public static T Deserialize<T>(string json)
    {
        T obj1 = Activator.CreateInstance<T>();
        MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj1.GetType());
        if (json.Length > 0 && json.Substring(0, 1) == "{")
        {
            obj1 = (T)serializer.ReadObject(ms);
        }
        ms.Close();
        return obj1;
    }
    /// <summary>
    /// Deserialize the return json
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="jsonString"></param>
    /// <returns></returns>
    public static T DeserializeJSon<T>(string jsonString)
    {
        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
        MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
        if (stream.Length == 0) return default(T);
        T obj = (T)ser.ReadObject(stream);
        stream.Close();
        return obj; 
    }
}
/// <summary>
/// Class to override/set the timeout for any request 
/// </summary>
public class WebXClient : WebClient
{
    private int _timeout;
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="p_timeout"></param>
    public WebXClient(int p_timeout)
    {
        this._timeout = p_timeout;
    }
    /// <summary>
    /// GetWebRequest
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    protected override WebRequest GetWebRequest(Uri address)
    {
        var request = base.GetWebRequest(address);
        request.Timeout = this._timeout;
        return request;
    }
}

