﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Xml;

//rsolanki2: extensibility updates 
//namespace Riskmaster.Common
//{
namespace Riskmaster.AppHelpers
{

    #region RMAdapterElement Class
    public class RMAdapterElement : ConfigurationElement
    {

        [ConfigurationProperty("name",
            IsRequired = true,
            IsKey = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("assembly",
            IsRequired = true)]
        public string Assembly
        {
            get
            {
                return (string)this["assembly"];
            }
            set
            {
                this["assembly"] = value;
            }
        }

        [ConfigurationProperty("class",
            IsRequired = true)]
        public string Class
        {
            get
            {
                return (string)this["class"];
            }
            set
            {
                this["class"] = value;
            }
        }

        [ConfigurationProperty("method",
            IsRequired = true)]
        public string Method
        {
            get
            {
                return (string)this["method"];
            }
            set
            {
                this["method"] = value;
            }
        }//property: Method

        [ConfigurationProperty("bypasssecurity",
            DefaultValue = false,
            IsRequired = false)]
        public bool BypassSecurity
        {
            get
            {
                return (bool)this["Bypasssecurity"];
            }
            set
            {
                this["Bypasssecurity"] = value;
            }
        }

        [ConfigurationProperty("trustedonly",
            DefaultValue = true,
            IsRequired = false)]
        public bool TrustedOnly
        {
            get
            {
                return (bool)this["Trustedonly"];
            }
            set
            {
                this["Trustedonly"] = value;
            }
        }



        protected override void DeserializeElement(
           System.Xml.XmlReader reader,
            bool serializeCollectionKey)
        {
            base.DeserializeElement(reader,
                serializeCollectionKey);
            // You can your custom processing code here.
        }


        protected override bool SerializeElement(
            System.Xml.XmlWriter writer,
            bool serializeCollectionKey)
        {
            bool ret = base.SerializeElement(writer,
                serializeCollectionKey);
            // You can enter your custom processing code here.
            return ret;

        }


        protected override bool IsModified()
        {
            bool ret = base.IsModified();
            // You can enter your custom processing code here.
            return ret;
        }
    }
    #endregion

    #region RMAdapterSection Class
    // Define a custom section containing an individual
    // element and a collection of elements.
    public class RMAdaptersSection : ConfigurationSection
    {



        // Declare a collection element represented 
        // in the configuration file by the sub-section
        // <Adaptors> <add .../> </Adaptors> 
        // Note: the "IsDefaultCollection = false" 
        // instructs the .NET Framework to build a nested 
        // section like <Adaptors> ...</Adaptors>.
        [ConfigurationProperty("Adaptors",
            IsDefaultCollection = false)]
        public RMAdaptersCollection RMAdapters
        {
            get
            {
                RMAdaptersCollection rmAdaptersCollection =
                (RMAdaptersCollection)base["Adaptors"];
                return rmAdaptersCollection;
            }
        }

        
        /// <summary>
        /// Handles the Deserialization of the ConfigurationSection
        /// </summary>
        /// <param name="reader">XmlReader</param>
        protected override void DeserializeSection(
            XmlReader reader)
        {
            base.DeserializeSection(reader);
            // You can add custom processing code here.
        }

        /// <summary>
        /// Handles the serialization of the ConfigurationSection
        /// </summary>
        /// <param name="parentElement"></param>
        /// <param name="name"></param>
        /// <param name="saveMode"></param>
        /// <returns></returns>
        protected override string SerializeSection(
            ConfigurationElement parentElement,
            string name, ConfigurationSaveMode saveMode)
        {
            string s =
                base.SerializeSection(parentElement,
                name, saveMode);
            // You can add custom processing code here.
            return s;
        }

    }
    #endregion

    #region RMAdaptersCollection Class
    public class RMAdaptersCollection : ConfigurationElementCollection
    {
        public RMAdaptersCollection()
        {

        }

        public override
            ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return

                    ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        protected override
            ConfigurationElement CreateNewElement()
        {
            return new RMAdapterElement();
        }


        protected override Object
            GetElementKey(ConfigurationElement element)
        {
            return ((RMAdapterElement)element).Name;
        }


        public new string AddElementName
        {
            get
            { return base.AddElementName; }

            set
            { base.AddElementName = value; }

        }

        public new string ClearElementName
        {
            get
            { return base.ClearElementName; }

            set
            { base.AddElementName = value; }

        }

        public new string RemoveElementName
        {
            get
            { return base.RemoveElementName; }
        }

        public new int Count
        {
            get { return base.Count; }
        }


        public RMAdapterElement this[int index]
        {
            get
            {
                return (RMAdapterElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public RMAdapterElement this[string Name]
        {
            get
            {
                return (RMAdapterElement)BaseGet(Name);
            }
        }

        public int IndexOf(RMAdapterElement rmAdapter)
        {
            return BaseIndexOf(rmAdapter);
        }

        public void Add(RMAdapterElement rmAdapter)
        {
            BaseAdd(rmAdapter);
            // Add custom code here.
        }

        protected override void
            BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
            // Add custom code here.
        }

        public void Remove(RMAdapterElement rmAdapter)
        {
            if (BaseIndexOf(rmAdapter) >= 0)
                BaseRemove(rmAdapter.Name);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
            // Add custom code here.
        }
    }
    #endregion

}
