﻿using System;
using System.Configuration;
using System.Xml;

//rsolanki2: extensibility updates 
namespace Riskmaster.AppHelpers
{

    #region RMExtensibilityElement Class
    public class RMExtensibilityElement : ConfigurationElement
    {

        [ConfigurationProperty("baseformname",
            IsRequired = true,
            IsKey = true)]
        public string BaseFormName
        {
            get
            {
                return (string)this["baseformname"];
            }
            set
            {
                this["baseformname"] = value;
            }
        }//property: BaseFormName

        //rsolanki2 : extensibility updates
        [ConfigurationProperty("pageloadmethod",
           IsRequired = true,
           IsKey = true)]
        public string PageLoadMethod
        {
            get
            {
                return (string)this["pageloadmethod"];
            }
            set
            {
                this["pageloadmethod"] = value;
            }
        }//property: pageloadmethod

        //rsolanki2 : extensibility updates
        [ConfigurationProperty("pageprerendermethod",
           IsRequired = true,
           IsKey = true)]
        public string PagePreRenderMethod
        {
            get
            {
                return (string)this["pageprerendermethod"];
            }
            set
            {
                this["pageprerendermethod"] = value;
            }
        }//property: pageprerendermethod 
        //rsolanki2 : extensibility updates
        [ConfigurationProperty("pagerendermethod",
           IsRequired = true,
           IsKey = true)]
        public string PageRenderMethod
        {
            get
            {
                return (string)this["pagerendermethod"];
            }
            set
            {
                this["pagerendermethod"] = value;
            }
        }//property: pagerendermethod
        //rsolanki2 : extensibility updates
        [ConfigurationProperty("pageinitmethod",
           IsRequired = true,
           IsKey = true)]
        public string PageInitMethod
        {
            get
            {
                return (string)this["pageinitmethod"];
            }
            set
            {
                this["pageinitmethod"] = value;
            }
        }//property: pageinitmethod

        [ConfigurationProperty("assembly",
            IsRequired = true)]
        public string Assembly
        {
            get
            {
                return (string)this["assembly"];
            }
            set
            {
                this["assembly"] = value;
            }
        }//property: Assembly

        [ConfigurationProperty("formname",
            IsRequired = true)]
        public string FormName
        {
            get
            {
                return (string)this["formname"];
            }
            set
            {
                this["formname"] = value;
            }
        }//property: FormName

        [ConfigurationProperty("typename",
            IsRequired = true)]
        public string TypeName
        {
            get
            {
                return (string)this["typename"];
            }
            set
            {
                this["typename"] = value;
            }
        }//property: TypeName


        protected override void DeserializeElement(
           System.Xml.XmlReader reader,
            bool serializeCollectionKey)
        {
            base.DeserializeElement(reader,
                serializeCollectionKey);
            // You can your custom processing code here.
        }


        protected override bool SerializeElement(
            System.Xml.XmlWriter writer,
            bool serializeCollectionKey)
        {
            bool ret = base.SerializeElement(writer,
                serializeCollectionKey);
            // You can enter your custom processing code here.
            return ret;

        }


        protected override bool IsModified()
        {
            bool ret = base.IsModified();
            // You can enter your custom processing code here.
            return ret;
        }
    }
    #endregion

    #region RMExtensibilitySection Class
    // Define a custom section containing an individual
    // element and a collection of elements.
    public class RMExtensibilitySection : ConfigurationSection
    {



        // Declare a collection element represented 
        // in the configuration file by the sub-section
        // <FormMapping> <add .../> </FormMapping> 
        // Note: the "IsDefaultCollection = false" 
        // instructs the .NET Framework to build a nested 
        // section like <FormMapping> ...</FormMapping>.
        [ConfigurationProperty("FormMapping",
            IsDefaultCollection = false)]
        public RMExtensibilityCollection RMFormMapping
        {
            get
            {
                RMExtensibilityCollection RMExtensibilityCollection =
                (RMExtensibilityCollection)base["FormMapping"];
                return RMExtensibilityCollection;
            }
        }

        // Declare a collection element represented 
        // in the configuration file by the sub-section
        // <Extensibility> <add .../> </Extensibility> 
        // Note: the "IsDefaultCollection = false" 
        // instructs the .NET Framework to build a nested 
        // section like <Extensibility> ...</Extensibility>.
        [ConfigurationProperty("Adaptors",
            IsDefaultCollection = false)]
        public RMAdaptersCollection RMExtensibilityAdapters
        {
            get
            {
                RMAdaptersCollection rmAdaptersCollection =
                (RMAdaptersCollection)base["Adaptors"];
                return rmAdaptersCollection;
            }
        } // property RMExtensibilityAdapters


        /// <summary>
        /// Handles the Deserialization of the ConfigurationSection
        /// </summary>
        /// <param name="reader">XmlReader</param>
        protected override void DeserializeSection(
            XmlReader reader)
        {
            base.DeserializeSection(reader);
            // You can add custom processing code here.
        }

        /// <summary>
        /// Handles the serialization of the ConfigurationSection
        /// </summary>
        /// <param name="parentElement"></param>
        /// <param name="name"></param>
        /// <param name="saveMode"></param>
        /// <returns></returns>
        protected override string SerializeSection(
            ConfigurationElement parentElement,
            string name, ConfigurationSaveMode saveMode)
        {
            string s =
                base.SerializeSection(parentElement,
                name, saveMode);
            // You can add custom processing code here.
            return s;
        }

    }
    #endregion

    #region RMExtensibilityCollection Class
    public class RMExtensibilityCollection : ConfigurationElementCollection
    {
        public RMExtensibilityCollection()
        {

        }

        public override
            ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return

                    ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        protected override
            ConfigurationElement CreateNewElement()
        {
            return new RMExtensibilityElement();
        }


        protected override Object
            GetElementKey(ConfigurationElement element)
        {
            return ((RMExtensibilityElement)element).FormName;
        }


        public new string AddElementName
        {
            get
            { return base.AddElementName; }

            set
            { base.AddElementName = value; }

        }

        public new string ClearElementName
        {
            get
            { return base.ClearElementName; }

            set
            { base.AddElementName = value; }

        }

        public new string RemoveElementName
        {
            get
            { return base.RemoveElementName; }
        }

        public new int Count
        {
            get { return base.Count; }
        }


        public RMExtensibilityElement this[int index]
        {
            get
            {
                return (RMExtensibilityElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public RMExtensibilityElement this[string Name]
        {
            get
            {
                return (RMExtensibilityElement)BaseGet(Name);
            }
        }

        public int IndexOf(RMExtensibilityElement rmAdapter)
        {
            return BaseIndexOf(rmAdapter);
        }

        public void Add(RMExtensibilityElement rmAdapter)
        {
            BaseAdd(rmAdapter);
            // Add custom code here.
        }

        protected override void
            BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
            // Add custom code here.
        }

        public void Remove(RMExtensibilityElement rmAdapter)
        {
            if (BaseIndexOf(rmAdapter) >= 0)
                BaseRemove(rmAdapter.FormName);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
            // Add custom code here.
        }
    }
    #endregion
}
