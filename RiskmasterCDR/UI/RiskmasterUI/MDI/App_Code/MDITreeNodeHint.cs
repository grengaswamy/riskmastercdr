﻿///********************************************************************
/// Amendment History
///********************************************************************
/// Date Amended   *            Amendment               *    Author
/// 02/10/2014         MITS 34260 - duplicate claim         Ngupta36
///********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Web;
using Riskmaster.Common;

namespace Riskmaster.MDI.App_Code
{
    public class MDITreeNodeHint //A hint contains parsed information of a navigation tree node.
    {
        public bool IsPopulated;
        public bool IsRefreshed;
        public bool IsReplaced;
        public bool IsRemoved;
        public bool IsDirtied;
        public bool IsToggled;
        public bool IsReloaded;
        public bool NotInNavTree;
        public bool UserSelectedFromNavTree; //it is the same as selecting from Breadcrumb
        public bool CouldPopulateOnDemand;
        public bool IsRoot;
        public bool AreChildrenCollapsed; //this property is only for roots.
        public bool IsClaim;
        public bool IsEvent;
        public bool IsOneToOne;
        public bool Presentable;
        public bool SearchMenuForIt;
        //public bool RunBusinessRule;
        public string Root;
        public string RootTitle;
        public string NodeScreen;
        public string NodeParam;
        public int NodeNewRecordId;
        public long NodeRecordId;
        public int NodeParentId;
        public string NodeMDIId;
        public string NodeLocalPath;
        public string NodeTitle;
        public string NodePlainText;
        public string NodeHTMLText;
        public bool ShowContextMenu;
        public string ParentPathValue;
        public string PlaceAt;
        public string NodePathValue;
        public string NodeInitialValue;
        public string Breadcrumb;
        public const char GroupSeparator = (char)29;
        public const char UnitSeparator = (char)31;
        public const char PathValueSeparator = (char)30;
        public const char TreeviewActionSeparator = (char)92;
        private const char SelectionIndicator = (char)115;
        private const char TogglingIndicator = (char)116;
        private const char ZeroNodeIdIndicator = (char)122; // it is used to create MDIId, if recordId is zero then the concatenation of this indicator and nodeScreen is used in order to keep uniqueness of MDIId instead of using zero(e.g. if nodeScreen is 1099 the concatenation would be z1099 so still the IsRecordIdZero() method can realize that RecordId is zero. In this example 1099 is an MDIMenu item with RecordId = 0).
        public bool IsBOB;
        private const char OneToOneIndicator = (char)111;
        public Dictionary<string, bool> PersonInvolved { get; set; } //Added by gbindra on 08/26/14 for WWIG GAP 30 MITS#35365

        public MDITreeNodeHint(string eventArg, bool searchMenuForIt)
        {
            this.SearchMenuForIt = searchMenuForIt;
            MDITreeNodeHintStart(eventArg);
        }

        private void MDITreeNodeHintStart(string eventArg)
        {
            this.IsPopulated = false;
            this.IsRefreshed = false;
            this.IsReplaced = false;
            this.IsRemoved = false;
            this.IsDirtied = false;
            this.IsToggled = false;
            this.IsReloaded = false;
            this.NotInNavTree = true;
            this.UserSelectedFromNavTree = false;
            this.CouldPopulateOnDemand = false;
            this.IsRoot = false;
            this.AreChildrenCollapsed = false; //this property is only for roots.
            this.IsClaim = false;
            this.IsEvent = false;
            this.IsOneToOne = false;
            this.Presentable = true;
            //this.RunBusinessRule = true;
            this.Root = string.Empty;
            this.NodeScreen = string.Empty;
            this.NodeParam = string.Empty;
            this.NodeRecordId = -1; //-1 means by default this is a new node which needs to be assigned a value.
            this.NodeNewRecordId = -1;
            this.NodeParentId = -1;
            this.NodeMDIId = string.Empty;
            this.NodeLocalPath = string.Empty;
            this.NodeTitle = string.Empty;
            this.NodePlainText = string.Empty;
            this.NodeHTMLText = string.Empty;
            this.ShowContextMenu = true;
            this.NodePathValue = string.Empty;
            this.NodeInitialValue = string.Empty;
            this.ParentPathValue = string.Empty;
            this.PlaceAt = string.Empty;
            this.Breadcrumb = string.Empty;
            this.IsBOB = false;
            Interpret(eventArg);
        }

        public MDITreeNodeHint(string eventArg, string relativeNode, bool searchMenuForIt)
        {
            string[] splitItems;
            char delimiter;
            this.SearchMenuForIt = searchMenuForIt;
            if (eventArg.IndexOf(TreeviewActionSeparator) != -1)
                delimiter = TreeviewActionSeparator;
            else
                delimiter = PathValueSeparator;
            switch (relativeNode)
            {
                case "DataModelParent":
                    {
                        do
                        {
                            splitItems = eventArg.Split(delimiter);
                            if (splitItems.Length > 1)
                            {
                                for (int i = 0; i < splitItems.Length - 1; i++)
                                {
                                    if (i == 0)
                                        eventArg = splitItems[i];
                                    else
                                        eventArg += delimiter + splitItems[i];
                                }
                                this.MDITreeNodeHintStart(eventArg);
                            }
                            else
                            {
                                break;
                            }
                        } while (this.NodeRecordId == 0);
                        break;
                    }
                case "MDIParent":
                    {
                        splitItems = eventArg.Split(delimiter);
                        for (int i = 0; i < splitItems.Length - 1; i++)
                        {
                            if (i == 0)
                                eventArg = splitItems[i];
                            else
                                eventArg += delimiter + splitItems[i];
                        }
                        this.MDITreeNodeHintStart(eventArg);
                        break;
                    }
                default:
                    this.MDITreeNodeHintStart(eventArg);
                    break;
            }
        }

        public string GetNodeValue()
        {
            string plainText;
            //Any MDIId consists of the parent MDIId + current NodeRecordId
            //Since it is a Root, and we don't have any screens for roots, so we simplify it and don't use any delimiter in it.
            //This way we can easily recognize Root nodes. Any NodeValue that does not contain GroupSeparator is a Root!
            //this simplification is also vital for result screens under Search root. Because we use MDIId as iframes' id and need to submit any criteria form into a different iframe for result screen. So we need to assign the iframe's id to the form's target property.
            //so far in IE7, forms' target property does not accept any expression that contains a special character(as we have delimiters). This is not an issue in Firefox!
            //hence we basically don't have any delimiter in MDIId for Search screens; they have two levels of hierarchy and easy to manage without delimiters.
            if (this.IsRoot)
                return this.NodeMDIId;
            else
            {
                plainText = this.NodePlainText;
                plainText = plainText.Replace(',', (char)32);
                plainText = plainText.Replace('\\', (char)32);
                plainText = plainText.Replace('\"', (char)32);//Deb MITS 25139 Removed " 

                return this.NodeMDIId + GroupSeparator + this.NodeScreen + UnitSeparator + this.NodeTitle + UnitSeparator + this.NodeLocalPath + UnitSeparator + this.NodeParam + UnitSeparator + this.PlaceAt + GroupSeparator + plainText + GroupSeparator + this.IsOneToOne.ToString() + GroupSeparator + this.IsBOB.ToString();// +GroupSeparator + this.ShowContextMenu.ToString();
            }
        }

        public void SetMDIId(string parentMDIId)
        {
            if (this.IsRoot)
            {
                this.NodeMDIId = this.Root;
            }
            else if (this.Root == "Search")
            {
                this.NodeMDIId = parentMDIId + this.NodeRecordId;
            }
            else {
                if (this.NodeRecordId == 0)
                {
                    //This is to make sure we have unique MDIIds when we have different nodes with zero recordId at the same level in navTree (like Litigation list node, Claimant list node, Adjuster list node under the same claim).
                        this.NodeMDIId = parentMDIId + UnitSeparator + ZeroNodeIdIndicator + this.NodeScreen;
                }
                else if (this.IsOneToOne) 
                {
                    /*
                     * This is to make sure MDIId is unique for OneToOne nodes. 
                     * There is a chance that OneToOne nodes have the same RecordId. 
                     * For example, as of now, OneToOne nodes under Event, all have the same EventId as their RecordIds; like Osha, MedWatch & etc.! 
                     * So to have unique MDIIds for OneToOne nodes, we concatenate recordId with screenName.
                     * this way screens like Osha and MedWatch can have each a unique MDIId.
                     * Now the reason we use OneToOneIndicator is to easily separate recordId from screenName.
                     * Also if RecordId is 126 and ScreenName is 1099 (in future we may have a screen/object having a number as its name) then the concatenation would be 126o1099.
                     * This way recordId can be safely separated from screenName.
                     * Note that having OneToOneIndicator in MDIId does not mean we can eliminate the boolean value of IsOneToOne from NodeValue (see GetNodeValue() method).
                     * Because if recordId of a OneToOne node (like reservelisting) is zero then MDIId is constructed differently. (see the above if block when this.NodeRecordId == 0).
                    */
                    this.NodeMDIId = parentMDIId + UnitSeparator + this.NodeRecordId + OneToOneIndicator + this.NodeScreen;
                }
                else
                {
                    /*
                     * It is possible that two Adjuster and Claimant records have the same recordId under the same claim.
                     * But since each of these records is under a different list node in navTree then MDIIds are each different and unique.
                     * So because of the presence of list nodes no special care is needed for these cases.
                    */
                    this.NodeMDIId = parentMDIId + UnitSeparator + this.NodeRecordId;
                }
            }
        }

        /*public string GetClaimMDIId()
        {
            string claimMDIId = string.Empty;
            string[] splitItems = null;

            splitItems = this.NodePathValue.Split(PathValueSeparator);
            if (splitItems.Length > 1)
            {
                splitItems = splitItems[2].Split(GroupSeparator);
                if (spliItems.)
            }
            return claimMDIId;
        }*/

        public string GetParentMDIId()
        {
            int i, l = 0;
            string parentMDIId = string.Empty;
            string[] splitItems;
            if (!string.IsNullOrEmpty(this.NodeMDIId))
            {
                splitItems = this.NodeMDIId.Split(UnitSeparator);
                l = splitItems.Length;
                if (l > 1)
                {
                    parentMDIId = splitItems[0];
                    for (i = 1; i < l-1; i++)
                    {
                        parentMDIId += UnitSeparator + splitItems[i];
                    }
                }
            }
            return parentMDIId;
        }

        public void ReadThisNarrowedProfile(string[] narrowedProfile)
        {
            string description = string.Empty;
            string recordTitle = string.Empty;
            string listTitle = string.Empty;

            this.Root = "Document"; // Right now this method is only used for Document root, for other roots we don't show hierarchy in navTree. Also because the root is Document, if it is One to One then we set ShowContextMenu to false (e.g. Financials/Reserves node must not show the context menu if RSW is disabled!).
            this.ShowContextMenu = true; // MITS 18402
            this.CouldPopulateOnDemand = false;

            if (narrowedProfile != null)
            {
                if (narrowedProfile.Length > 0)
                {
                    this.NodeRecordId = Convert.ToInt64(narrowedProfile[0]);
                }
                if (narrowedProfile.Length > 1)
                {
                    this.NodeScreen = narrowedProfile[1];
                }
                if (narrowedProfile.Length > 2)
                {
                    description = MDITreeNodeHint.EncodeSpecialChars(narrowedProfile[2]);
                }
                if (narrowedProfile.Length > 3)
                {
                    recordTitle = MDITreeNodeHint.EncodeSpecialChars(narrowedProfile[3]);
                }
                if (narrowedProfile.Length > 4)
                {
                    listTitle = MDITreeNodeHint.EncodeSpecialChars(narrowedProfile[4]);
                }
                if (narrowedProfile.Length > 5)
                {
                    this.Presentable = Convert.ToBoolean(narrowedProfile[5]);
                }
                if (narrowedProfile.Length > 6)
                {
                    this.IsOneToOne = Convert.ToBoolean(narrowedProfile[6]);
                }
                if (narrowedProfile.Length > 7)
                {
                    this.CouldPopulateOnDemand = Convert.ToBoolean(narrowedProfile[7]);
                }
                if (narrowedProfile.Length > 8)
                {
                    this.IsBOB = Convert.ToBoolean(narrowedProfile[8]);
                }
            }

            //smahajan6 11/26/09 MITS:18230 :Start
            if (this.NodeScreen == "claimgc" || this.NodeScreen == "claimwc" || this.NodeScreen == "claimva" || this.NodeScreen == "claimdi" || this.NodeScreen == "claimpc")
            //if (this.NodeScreen == "claimgc" || this.NodeScreen == "claimwc" || this.NodeScreen == "claimva" || this.NodeScreen == "claimdi")
            //smahajan6 11/26/09 MITS:18230 :End
            {
                this.IsClaim = true;
                this.CouldPopulateOnDemand = true;
               
            }
            else
            {
                this.IsClaim = false;
                if (this.NodeScreen == "event")
                    this.IsEvent = true;
                else
                    this.IsEvent = false;
            }

            if (this.IsEvent || this.IsClaim)
            {
                this.IsOneToOne = false;
                this.NodePlainText = recordTitle;// +" (" + description + ")";
                this.NodeTitle = recordTitle;
            }
            else if (this.PlaceAt != string.Empty) //another node injected out of back-end(e.g. Diry List(Filtered) called from form.js)
            {
                this.NodePlainText = listTitle;
                this.NodeTitle = this.NodePlainText;
            }

            else if (this.IsOneToOne)
            {
                this.NodePlainText = recordTitle;
                this.NodeTitle = recordTitle;
				this.ShowContextMenu = false;
            }
            else 
            {
                if (this.NodeRecordId == 0) //we show the list
                {
                    this.NodePlainText = listTitle;// +" (" + description + ")";
                    this.NodeTitle = listTitle;
                }
                else
                {
                    this.NodePlainText = recordTitle;// +" (" + description + ")";
                    this.NodeTitle = recordTitle;
                }
            }
            if (description != string.Empty)
            {
                //this.NodePlainText += " (" + description + ")";
                //JIRA RMA-11109 ajohari2: Start
                if ((this.NodeScreen.ToLower().Equals("adjuster") || this.NodeScreen.ToLower().Equals("claimant") || this.NodeScreen.ToLower().Equals("defendant")) && this.NodeRecordId > 0)
                    this.NodePlainText = description;
                else
                    this.NodePlainText += " (" + description + ")";
                //JIRA RMA-11109 ajohari2: End
                //Neha start
                if (this.NodeScreen == "casemanagementlist")
                {
                    if (string.Compare(description, "0") != 0)
                    {
                        this.ShowContextMenu = false;
                    }
                }
                //end
            }
            
            /*
            else if (description == "-1") // one to one with children
            {
                this.NodePlainText = recordTitle;
                this.NodeTitle = recordTitle;
                this.CouldPopulateOnDemand = true;
            }
            else if (description == "-2") //one to one with no children
            {
                this.NodePlainText = recordTitle;
                this.NodeTitle = recordTitle;
            }
            else //one to many; we show the list
            {
                this.IsOneToOne = false;
                if (this.NodeRecordId == 0) //we show the list
                {
                    this.NodePlainText = listTitle + " (" + description + ")";
                    this.NodeTitle = listTitle;
                }
                else
                {
                    this.NodePlainText = recordTitle + " (" + description + ")";
                    this.NodeTitle = recordTitle;
                }
            }*/
        }

        public void ReadThisProfileForListNode(List<string[]> siblingProfile, string[] node, bool allSiblingsIncluded)
        {
            int num = 0;
            if (allSiblingsIncluded)
            {
                num = siblingProfile.Count;
            }
            else
            {
                num = siblingProfile.Count + 1;
            }
            this.CouldPopulateOnDemand = false;
            this.IsOneToOne = false;
            //this.NodeTitle = node[4];
            this.NodePlainText = NodeTitle + " (" + (num).ToString() + ")";
            if (this.NodeScreen == "casemanagementlist")// removes add option on menu if the node adds a child.
            {
                if (string.Compare((num).ToString(), "0") != 0)
                {
                    this.ShowContextMenu = false;
                }
            }
        }

        public void PopulateHierarchyProperties()
        {
            string[] valueItems, pathValueItems = this.NodePathValue.Split(PathValueSeparator);
            string userPrompt;
            for (int i = 0; i < pathValueItems.Length; i++)
            {
                valueItems = pathValueItems[i].Split(GroupSeparator);
                if (valueItems.Length > 2)
                    userPrompt = valueItems[2];
                else
                    userPrompt = string.Empty;
                if (i == 0)
                {
                    //MITS 34415 starts
                    this.ParentPathValue = Root;
                    if (string.IsNullOrEmpty(this.RootTitle))
                    {
                        if (!string.IsNullOrEmpty(this.Root))
                        {
                            string sLangCode = AppHelper.GetLanguageCode();
                            if (MDISetting.dictRootNodes.ContainsKey(sLangCode))
                            {
                                if (MDISetting.dictRootNodes[sLangCode].ContainsKey(this.Root))
                                {
                                    this.RootTitle = MDISetting.dictRootNodes[sLangCode][this.Root].ToString();
                                }
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(this.RootTitle))
                    {
                        this.RootTitle = this.Root;
                    }
                 //MITS 34415 ends
                    BreadcrumbInitiate(RootTitle);
                }

                else if (i < pathValueItems.Length - 1)
                {
                    this.ParentPathValue += PathValueSeparator + pathValueItems[i];
                    BreadcrumbAddItem(ParentPathValue, userPrompt);
                }
                else
                {
                    //this.NodePathValue = ParentPathValue + PathValueSeparator + pathValueItems[i];
                    BreadcrumbAddItem(this.NodePathValue, userPrompt);
                }
            }
        }

        private void Interpret(string eventArg)
        {
            string[] splitItems = null;
            string[] splitItemsHelp = null;
            this.NodePathValue = "";
            this.IsRemoved = false;
            if (!string.IsNullOrEmpty(eventArg))
            {
                if (eventArg.Split('|').Length > 1)
                {
                    this.RootTitle = eventArg.Split('|')[0].ToString();
                    eventArg = eventArg.Split('|')[1].ToString();
                }
                
                int startingValue = 1;
                if (Convert.ToChar(eventArg.Substring(0, 1)) == TogglingIndicator || Convert.ToChar(eventArg.Substring(0, 1)) == SelectionIndicator)
                {
                    UserSelectedFromNavTree = true; //User either toggled or selected a tree node
                    if (Convert.ToChar(eventArg.Substring(0, 1)) == TogglingIndicator)
                        this.IsToggled = true;
                    eventArg = eventArg.Substring(1, eventArg.Length - 1);
                    eventArg = eventArg.Replace(TreeviewActionSeparator, PathValueSeparator);
                }
                //otherwise User selected from search results OR Menu bar OR this is a call by the application itself to parse other tree nodes' info


                splitItems = eventArg.Split(PathValueSeparator);
                if (splitItems.Length > 1)
                {
                    this.NodePathValue = eventArg; //if startingValue remains 1(it has to be set to 1 at the top by default) then NodePathValue is corrected in the following.
                    splitItemsHelp = splitItems[0].Split(UnitSeparator);
                    if (splitItemsHelp[0] == "Dirty")
                    {
                        this.IsDirtied = true;
                        this.Root = splitItems[1].Split(GroupSeparator)[0].Split(UnitSeparator)[0];
                    }
                    else if (splitItemsHelp[0] == "Populate")
                    {
                        this.IsPopulated = true;
                        this.Root = splitItems[1].Split(GroupSeparator)[0].Split(UnitSeparator)[0];
                    }
                    else if (splitItemsHelp[0] == "Refresh") //for Refresh we get: "Refresh(UnitSeparator)NewId(PathValueSeparator)Value"; it is to update navTree node user prompt.
                    {
                        this.IsRefreshed = true;
                        this.NodeNewRecordId = Convert.ToInt32(GetRecordNum(splitItemsHelp[1]));
                        this.Root = splitItems[1].Split(GroupSeparator)[0].Split(UnitSeparator)[0];
                    }
                    else if (splitItemsHelp[0] == "Replace") //for Replace we get: "Replace(UnitSeparator)NewId(PathValueSeparator)Value"
                    {
                        this.IsReplaced = true;
                        this.IsRemoved = true;
                        this.NodeNewRecordId = Convert.ToInt32(GetRecordNum(splitItemsHelp[1]));
                        this.Root = splitItems[1].Split(GroupSeparator)[0].Split(UnitSeparator)[0];
                    }
                    else if (splitItemsHelp[0] == "Remove") //for Remove we get: "Remove(PathValueSeparator)ValuePath"
                    {
                        this.IsRemoved = true;
                        this.Root = splitItems[1].Split(GroupSeparator)[0].Split(UnitSeparator)[0];
                    }
                    else if (splitItemsHelp[0] == "Collapse")
                    {
                        this.AreChildrenCollapsed = true;
                        this.Root = splitItems[1].Split(GroupSeparator)[0].Split(UnitSeparator)[0];
                    }
                    else if (splitItemsHelp[0] == "Reload") // This is to reload Screen with original content.
                    {
                        this.IsReloaded = true;
                        this.NodeNewRecordId = Convert.ToInt32(GetRecordNum(splitItemsHelp[1]));
                        this.Root = splitItems[1].Split(GroupSeparator)[0].Split(UnitSeparator)[0];
                        if (this.Root.Length > 5 && this.Root.Substring(0, 6) == "Search")
                        {
                            this.Root = "Search";
                        }
                    }
                    else
                    {
                        Root = splitItems[0];
                        startingValue = 0;
                    }

                    if (startingValue == 1)
                    {
                        this.NodePathValue = eventArg.Substring(splitItems[0].Length + 1, eventArg.Length - splitItems[0].Length - 1);
                        splitItems = NodePathValue.Split(PathValueSeparator); // we need to refresh splitItems and get rid of the first item (e.g. Remove) so that in case of "Remove(PathValueSeparator)Search", root info is correctly populated when it falls in the next if statement.
                    }

                    PopulateHierarchyProperties(); // it will populate ParentPathValue and Breadcrumb
                }
                if (eventArg.IndexOf(GroupSeparator) == -1) // it must not be else if; because in case of "Remove(PathValueSeparator)Search" it will not be recognized as root!
                {
                    this.IsRoot = true;
                    this.Root = splitItems[0];
                    this.NodePathValue = splitItems[0];
                }
                //if it falls in none of the above two if statementst hen eventArg is just the nodeValue(not the pathValue; so NodePathValue remains empty) and it could be called from a search result or a diary list, etc.

                splitItems = splitItems[splitItems.Length - 1].Split(GroupSeparator);
                if (splitItems.Length > 2)
                {
                    this.NodePlainText = splitItems[2];
                    this.IsOneToOne = Convert.ToBoolean(splitItems[3]);
                    //this.ShowContextMenu = Convert.ToBoolean(splitItems[4]);
                    this.NotInNavTree = false;
                }
                //Parag When BOB is true
                if (splitItems.Length > 4)
                {
                    this.IsBOB = Convert.ToBoolean(splitItems[4]);                    
                }
                if (splitItems.Length >= 1) //to make sure NodeMDIId is also set for roots ( =1)
                    this.NodeMDIId = splitItems[0];
                if (splitItems.Length > 1)
                {
                    splitItemsHelp = splitItems[1].Split(UnitSeparator);
                    this.NodeScreen = splitItemsHelp[0];
                    if (splitItemsHelp.Length > 1)
                    {
                        this.NodeTitle = splitItemsHelp[1];
                    }
                    if (splitItemsHelp.Length > 2)
                    {
                        this.NodeLocalPath = EncodeSpecialChars(splitItemsHelp[2]);
                    }
                    if (splitItemsHelp.Length > 3)
                    {
                        this.NodeParam = splitItemsHelp[3];
                    }
                    if (splitItemsHelp.Length > 4)
                    {
                        this.PlaceAt = splitItemsHelp[4].ToUpper(); //if not in navTRee, "SELECTED" means place it under Selected node (note that if selected node is under a different root then "TOP" will be considered); "TOP" means place it under its Root.
                    } //if later another parameter needs to be added, and the parameter is not necessary to be provided for injected nodes(like Filtered Diary lists or Enhanced Notes), just add it as parameter # 5, and always set the current parameter #5(root) as the last item. You also need to take care of this in MDIShowScreen() function (MDIBase.js). So that in case of any injected node, you include empty string in categoryArg after the "PlaceAt" item. And include the "Root" item as the last item in categoryArg.
                    if (splitItemsHelp.Length > 5)
                    {
                        if (!String.IsNullOrEmpty(splitItemsHelp[5]))
                        {
                            this.Root = splitItemsHelp[5];
                        }
                    }

                    //ipuri DIS User Verification Mits: 33285 Code Merge from RMA 13.1 CHP3 branch Start
                    if (this.NodeScreen == "TMView")
                    {
                        this.Root = "Utilities";
                    }
                    //ipuri DIS User Verification Mits: 33285 Code Merge from RMA 13.1 CHP3 branch End

                    //smahajan6 11/26/09 MITS:18230 :Start
                    if (this.NodeScreen == "claim" || this.NodeScreen == "claimgc" || this.NodeScreen == "claimwc" || this.NodeScreen == "claimva" || this.NodeScreen == "claimdi" || this.NodeScreen == "claimpc")
                    //if (this.NodeScreen == "claim" || this.NodeScreen == "claimgc" || this.NodeScreen == "claimwc" || this.NodeScreen == "claimva" || this.NodeScreen == "claimdi")
                    //smahajan6 11/26/09 MITS:18230 :End
                    {
                        this.IsClaim = true;
                       
                    }
                    else if (this.NodeScreen == "event")
                    {
                        this.IsEvent = true;
                    }
                    if (this.IsEvent || this.IsClaim)
                    {
                        if (this.NodeTitle != this.NodeScreen) // nodeTitle is NodeScreen while adding a new claim under an existing event; otherwise we don't waste the performance searching MDIMenu.
                        {
                            this.Root = "Document";
                            this.SearchMenuForIt = false;
                        }
                        else
                        {
                            this.NodeTitle = string.Empty;
                        }
                    }
                    splitItems = splitItems[0].Split(UnitSeparator);
                    if (splitItems.Length > 1)
                    {
                        this.NodeRecordId = 0;
                        if (!IsRecordIdZero(splitItems[splitItems.Length - 1]))
                        {
                            this.NodeRecordId = Convert.ToInt64(GetRecordNum(splitItems[splitItems.Length - 1]));
                        }
                        if (splitItems.Length > 2)
                        {
                            if (IsRecordIdZero(splitItems[splitItems.Length - 2]))
                            {
                                this.NodeParentId = 0;
                                if (splitItems.Length > 3 && !IsRecordIdZero(splitItems[splitItems.Length - 3]))//DataModel Parent of a node is not always the MDIParent; in that case it is the MDIParent of MDIParent (two levels upper). For example because of the presence of Litigation list node, from a litigation node you have to climb MDI-navTree two levels up in order to reach its claim parent!
                                    this.NodeParentId = Convert.ToInt32(GetRecordNum(splitItems[splitItems.Length - 3]));
                            }
                            else
                            {
                                this.NodeParentId = Convert.ToInt32(GetRecordNum(splitItems[splitItems.Length - 2]));
                            }
                        }
                    }

                    // Any MDIId consists of the parent MDIId + current NodeRecordId
                    // Since it is a Root, and we don't have any screens for roots, so we simplify it and don't use any delimiter in it.
                    // This way we can easily recognize Root nodes. Any NodeValue that does not contain GroupSeparator is a Root!
                    // this simplification is also vital for result screens under Search root. Because we use MDIId as iframes' id and need to submit any criteria form into a different iframe for result screen. So we need to assign the iframe's id to the form's target property.
                    // so far in IE7, forms' target property does not accept any expression that contains a special character(as we have delimiters). This is not an issue in Firefox!
                    // hence we basically don't have any delimiter in MDIId for Search screens; they have two levels of hierarchy and easy to manage without delimiters.
                    else if (this.NodeMDIId.Substring(0, this.Root.Length) == this.Root)
                    {
                        this.NodeRecordId = Convert.ToInt64(this.NodeMDIId.Substring(this.Root.Length, this.NodeMDIId.Length - this.Root.Length));
                    }

                    if (this.SearchMenuForIt && (this.Root == string.Empty || this.NodeTitle == string.Empty || this.NodeLocalPath == string.Empty))
                    {
                        SetKeyPropertiesFromMenu();
                    }

                    if (this.Root == string.Empty)
                    {
                        if (splitItems[0] != string.Empty)
                        {
                            this.Root = splitItems[0];
                        }
                    }

                    this.NodeInitialValue = this.GetNodeValue();
                }
            }
            else
            {
                SetAsUndefined();
            }
        }

        private string GetRecordNum(string sId)
        {
            string[] splitItems = sId.Split(OneToOneIndicator);
            if (splitItems.Length > 1)
            {
                return(splitItems[0]); //get the portion which is a number!
            }
            else 
            {
                return sId;
            }
        }

        private bool IsRecordIdZero(string sId)
        {
            /*
             * We could just simply check the first character to see if it is 0 or ZeroNodeIdIndicator character.
             * However, in order to avoid run-time errors, we perform further tests to make sure all characters in a non-zero recordId are numerical since we may later convert recordId to Int or pass it as Integer.
             * This is because MDIMenu.xml file is heavily updated overtime, and there is a chance that a non-numerical character is mistakenly included in recordId attributes.
             * in other places like MDIBase.js (see MDIGetScreenInfo function in MDIBase.js) we only check the first character of recordIds to check if they are zero; if the first character is 0 or ZeroNodeIdIndicator then we consider that as zero without further tests.
            */
            int i, l;
            bool isZero = true;
            string[] splitItems = sId.Split(OneToOneIndicator);
            if (splitItems.Length > 1)
            {
                sId = splitItems[0]; //get the portion which is supposed to be a number!
            }
            if (!string.IsNullOrEmpty(sId) && sId != "0")
            {
                l = sId.Length;
                for (i = 0; i < l; i++)
                {
                    if (i == 0 && (int)sId[i] == 45)
                    {
                        ; //still it can be a non-zero number if the first chatacter is minus!
                    }
                    else if ((int)sId[i] < 48 || (int)sId[i] > 57) //we check if all digits are numbers so that we won't have problems such as converting to Int/passing Int later.
                    {
                        break; //whenever we find a non-numerical character then we consider recordId as zero.
                    }
                }
                if (i == l)
                {
                    isZero = false; //since we didn't find a non-numerical character (minus is allowed as the first character) then we consider recordId as non-zero.
                }
            }
            return isZero;  
        }

        public string GetNodePathValue()
        {
            return this.ParentPathValue + PathValueSeparator + this.GetNodeValue();
        }

        private void BreadcrumbInitiate(string text)
        {
            Breadcrumb = "<span class='breadcrumbSolidItem'>" + text + "</span>";
        }

        private void BreadcrumbAddItem(string pathValue, string text)
        {
            Breadcrumb += "<span class='breadcrumbDelimiter'> => <span>";
            if (pathValue == string.Empty || text == "Claims")
            {
                Breadcrumb += "<span class='breadcrumbSolidItem'>" + text + "</span>";
            }
            else
            {
                Breadcrumb += "<a class='breadcrumbLinkItem' onclick=\"MDI('navTree', '" + pathValue + "')\">" + text + "</a>";
            }
        }

        public string GetNodeHTMLText()
        {
            string key = string.Empty;
            string HTMLText = string.Empty;
            string cssClass = string.Empty;
            const string defaultStart = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,null);\">";
            const string defaultEnd = "</span>";
            string MDIArgForChild = string.Empty;
            //Deb
            bool bBOB = false;
            //Deb
            if (this.ShowContextMenu)
            {
                if (this.IsRoot || this.NodeScreen == "Claims") // one of those which have not screens; including roots
                    key = this.NodePlainText;
                else if (this.Root == "Search")
                {
                    if (this.NodeScreen.ToLower() == "result")
                    {
                        key = "result";
                    }
                    else
                    {
                        key = "criteria";
                    }
                }
                else if (this.Root == "Document")
                {
                    if (this.IsClaim)
                        key = "claim";
                    else if (this.IsEvent)
                        key = "event";
                    else if (this.NodeScreen == "personinvolvedlist")
                        key = this.NodeScreen;
                    else if (this.NodeScreen == "reservelisting")
                        key = this.NodeScreen;
                    else if (this.NodeScreen == "otherunitloss") //rupal:start, we dont want context menu for otherunit loss
                        key = "otherunitloss";  
                    else if (this.NodeScreen == "siteloss") //we dont want context menu for site loss
                        key = "siteloss";  //rupal:end
                    //Start - Averma62 - MITS 28452 VSS Integration
                    else if (this.NodeScreen == "vssassignment")
                        key = "vssassignment";
                    //End - Averma62 - MITS 28452 VSS Integration
                    //MITS 34260  aahuja - to remove add new context menu from policy node start
                    else if (this.NodeScreen == "claimxpolicy")
                        key = "claimxpolicy";
                    //MITS 34260 aahuja - to remove add new context menu from policy node end
                    else if (this.NodeRecordId == 0 && !this.IsOneToOne)//we want the context menu off the list nodes!
                        key = "oneToMany";
                    
                }
                else if (this.NodeRecordId != 0)
                { 
                    if(this.NodeScreen == "OrgHierarchyRecord")
                        key = "other";
                    else  //mits 34199
                    key = "otherFDM";
                }
                //MITS 34260
                else if (this.NodeScreen == "claimxpolicy")
                    key = "claimxpolicy";
                //MITS 34260
                else
                {
                    key = "other";
                }

                if (this.PlaceAt != string.Empty)
                {
                    key = string.Empty;
                }
            }
            //npradeepshar 07/14/2011 added for case mangement injury
            if (this.NodeScreen == "casemanagement" || this.NodeScreen == "casemanagementinjury")
            {
                key = "tabInParent";
            }
            switch (key)
            {
                //Start - Averma62 - MITS 28452 VSS Integration
                case "vssassignment":
                    {
                        HTMLText = "<span>" + this.NodePlainText + "</span>";
                        break;
                    }
                //End - Averma62 - MITS 28452 VSS Integration
                    //rupal:start
                case "otherunitloss":
                    {
                        HTMLText = "<span>" + this.NodePlainText + "</span>";
                        break;
                    }
                case "siteloss":
                    {
                        HTMLText = "<span>" + this.NodePlainText + "</span>";
                        break;
                    }//rupal:end
                case "Document":
                    {
                        //HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV,Collapse All," + key + ",CLP');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV," + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCollapseAll") + "," + key + ",CLP');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
                case "Search":
                    {
                       // HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV,Collapse All," + key + ",CLP');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV," + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCollapseAll") + "," + key + ",CLP');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
                case "Maintenance":
                    {
                        //HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
                case "Utilities":
                    {
                        //HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
                case "User Documents":
                    {
                        //HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;</span>";
                        break;
                    }
                case "result":
                    {
                        //HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Close," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV');\">" + this.NodePlainText + "</span>";
                        HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeClose") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV');\">" + this.NodePlainText + "</span>";
                        break;
                    }
                case "criteria":
                    {
                        //HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV');\">" + this.NodePlainText + "</span>";
                        HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV');\">" + this.NodePlainText + "</span>";
                        break;
                    }
                case "event":
                    {
                        //PSARIN2 : R8 Perf Change 
                        bBOB = Conversion.ConvertStrToBool(AppHelper.GetBOBSession());  //added by Amitosh for for changing context menu when carrier claim is on
                        //Added by amitosh for change in context menu for carrier claim.
                        if (bBOB)
                        {
                            //smahajan6 11/26/09 MITS:18230 :Start
                            //pmittal5 Mits 21978 09/07/10 - Changed menu item name from 'Property' to 'Property Claim' 
                            //HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV,Add New:," + " " + ",NON,       General Claim," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimgc" + UnitSeparator + "claimgc" + ",ADD,      Workers’ Comp.," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimwc" + UnitSeparator + "claimwc"+ ",ADD');\">" + this.NodePlainText + "</span>";
                            HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV," + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("PrsnInvlvedLstAddNew") + "," + " " + ",NON,       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropGeneralClaim") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimgc" + UnitSeparator + "claimgc" + ",ADD,      " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropWorkersComp") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimwc" + UnitSeparator + "claimwc" + ",ADD');\">" + this.NodePlainText + "</span>";
                            //HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV,Add New:," + " " + ",NON,       General Claim," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimgc" + UnitSeparator + "claimgc" + ",ADD,      Workers’ Comp.," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimwc" + UnitSeparator + "claimwc" + ",ADD,      Vehicle Accident," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimva" + UnitSeparator + "claimva" + ",ADD,      Non-occupational," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimdi" + UnitSeparator + "claimdi" + ",ADD');\">" + this.NodePlainText + "</span>";
                            //smahajan6 11/26/09 MITS:18230 :End
                        }
                        else
                        {
                            //HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV,Add New:," + " " + ",NON,       General Claim," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimgc" + UnitSeparator + "claimgc" + ",ADD,      Workers’ Comp.," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimwc" + UnitSeparator + "claimwc" + ",ADD,      Vehicle Accident," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimva" + UnitSeparator + "claimva" + ",ADD,      Non-occupational," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimdi" + UnitSeparator + "claimdi" + ",ADD,      Property Claim," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimpc" + UnitSeparator + "claimpc" + ",ADD');\">" + this.NodePlainText + "</span>";
                            HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV," + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("PrsnInvlvedLstAddNew") + "," + " " + ",NON,       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropGeneralClaim") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimgc" + UnitSeparator + "claimgc" + ",ADD,      " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropWorkersComp") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimwc" + UnitSeparator + "claimwc" + ",ADD,      " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropVehicleAccident") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimva" + UnitSeparator + "claimva" + ",ADD,      " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropNonOccupational") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimdi" + UnitSeparator + "claimdi" + ",ADD,      " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropPropertyClaim") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimpc" + UnitSeparator + "claimpc" + ",ADD');\">" + this.NodePlainText + "</span>";
                        }
                        break;
                    }
                case "Claims":
                    {
                        //PSARIN2 : R8 Perf Change
                        bBOB = Conversion.ConvertStrToBool(AppHelper.GetBOBSession());  //added by Amitosh for for changing context menu when carrier claim is on
                        //Added by amitosh for change in context menu for carrier claim.
                        if (bBOB)
                        {
                            //smahajan6 11/26/09 MITS:18230 :Start
                            //pmittal5 Mits 21978 09/07/10 - Changed menu item name from 'Property' to 'Property Claim'
                            HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("PrsnInvlvedLstAddNew") + "," + " " + ",NON,       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropGeneralClaim") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimgc" + ",ADD,      " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropWorkersComp") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimwc" + ",ADD');\"><a class='parentTreeNode idleTreeNode' href=#>" + this.NodePlainText + "</a></span>";
                            //HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Add New:," + " " + ",NON,       General Claim," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimgc" + ",ADD,      Workers’ Comp.," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimwc" + ",ADD,      Vehicle Accident," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimva" + ",ADD,      Non-occupational," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimdi" + ",ADD');\"><a class='parentTreeNode idleTreeNode' href=#>" + this.NodePlainText + "</a></span>";
                            //smahajan6 11/26/09 MITS:18230 :End
                        }
                        else
                        {
                            HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("PrsnInvlvedLstAddNew") + "," + " " + ",NON,       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropGeneralClaim") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimgc" + ",ADD,      " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropWorkersComp") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimwc" + ",ADD,      " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropVehicleAccident") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimva" + ",ADD,      " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropNonOccupational") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimdi" + ",ADD,      " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePropPropertyClaim") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + "claimpc" + ",ADD');\"><a class='parentTreeNode idleTreeNode' href=#>" + this.NodePlainText + "</a></span>";
                        }
                        break;
                    }
                case "claim":
                    {
                        HTMLText = defaultStart + this.NodePlainText + defaultEnd;
                        break;
                    }
                case "oneToMany":
                    {
                        //HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Add New ," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + this.NodeScreen + UnitSeparator + this.NodeTitle + ",ADD');\">" + this.NodePlainText + "</span>";
                        HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiOneToManyAddNew") + " ," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + this.NodeScreen + UnitSeparator + this.NodeTitle + ",ADD');\">" + this.NodePlainText + "</span>";
                        break;
                    }
                case "Diaries":
                    {
                        //HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
                case "Funds":
                    {
                        //HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
                case "Reports":
                    {
                        //HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
                case "otherFDM":
                    {
                    ////Added by Amitosh for Policy Interface
                    //    if(this.NodeTitle == "Policy Tracking")
                    //        HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Close," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV,Add New ," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + this.NodeScreen + UnitSeparator + this.NodeTitle + ",ADD,Tag ,"+ this.NodeRecordId+" ,TAG');\">" + this.NodePlainText + "</span>";
                    //    else
                       // HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Close," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV,Add New ," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + this.NodeScreen + UnitSeparator + this.NodeTitle + ",ADD');\">" + this.NodePlainText + "</span>";
                        HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeClose") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV,Add New ," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator + this.NodeScreen + UnitSeparator + this.NodeTitle + ",ADD');\">" + this.NodePlainText + "</span>";
                        break;
                    }
                case "other":
                    {
                        //HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Close," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV');\">" + this.NodePlainText + "</span>";
                        HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeClose") + "," + this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + ",RMV');\">" + this.NodePlainText + "</span>";
                        break;
                    }
                case "Help":
                    {
                        //HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
                case "My Work":
                    {
                       // HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
                case "Other":
                    {
                        //HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
                case "tabInParent":
                    {
                        if (this.CouldPopulateOnDemand)
                        {
                            cssClass = "idleTreeNode";
                        }
                        else
                        {
                            cssClass = "idleTreeLeaf";
                        }
                        HTMLText = defaultStart + "<a class='" + cssClass + "' href=\"javascript:tabFocus('casemgt','" + this.NodeParentId + "');\">" + this.NodePlainText + "</a>" + defaultEnd;
                        break;
                    }
                case "personinvolvedlist":
                    {
                        //added by swati agarwal for WWIG Gap 30
                        MDIArgForChild = this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator;
                       // HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'Add Existing:," + " " + ",NON,";
                        HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,' " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("PrsnInvlvedLstAddExisting") + "," + " " + ",NON,";
                        if (this.PersonInvolved["AddExistingEmployee"])
                        {
                            HTMLText += "       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstEmployee") + "," + MDIArgForChild + "piemployee" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstEmployee") + ",PIE,";
                           // HTMLText += "       Employee," + MDIArgForChild + "piemployee" + UnitSeparator + "PI Employee" + ",PIE,";
                        }
                        if (this.PersonInvolved["AddExistingMedicalStaff"])
                        {
                            HTMLText += "       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstMedicalStaff") + "," + MDIArgForChild + "pimedstaff" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstMedicalStaff") + ",PIE,";
                            // HTMLText += "       Medical Staff," + MDIArgForChild + "pimedstaff" + UnitSeparator + "PI Medstaff" + ",PIE,";
                        }
                        if (this.PersonInvolved["AddExistingOtherPerson"])
                        {
                            HTMLText += "       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstOtherPerson") + "," + MDIArgForChild + "piother" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstOtherPerson") + ",PIE,";
                            //HTMLText += "       Other Person," + MDIArgForChild + "piother" + UnitSeparator + "PI Other" + ",PIE,";
                        //mbahl3 driver changes 
                        }
                        if (this.PersonInvolved["AddExistingDriver"])
                        {
                        //HTMLText += "       Driver," + MDIArgForChild + "pidriver" + UnitSeparator + "PI Driver" + ",PIE,";
                            HTMLText += "       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstDriver") + "," + MDIArgForChild + "pidriver" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstDriver") + ",PIE,";
                        }
                        //mbahl3 driver changes 
                        //HTMLText += "       Driver," + MDIArgForChild + "pidriver" + UnitSeparator + "PI Driver" + ",PIE,";
                        //mbahl3 driver changes 
                        //Neha 06/27/2011 added two new type in Person Involved
                       // HTMLText += "       Driver Insured," + MDIArgForChild + "pidriveri" + UnitSeparator + "PI Driver Insured" + ",PIE,";
                       // HTMLText += "       Driver Other," + MDIArgForChild + "pidrivero" + UnitSeparator + "PI Driver Other" + ",PIE,";
                        //Neha 06/27/2011 added two new type in Person Involved - End
                        if (this.PersonInvolved["AddExistingPatient"])
                        {
                        //HTMLText += "       Patient," + MDIArgForChild + "pipatient" + UnitSeparator + "PI Patient" + ",PIE,";
                            HTMLText += "       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstPatient") + "," + MDIArgForChild + "pipatient" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstPatient") + ",PIE,";
                        }
                        if (this.PersonInvolved["AddExistingPhysician"])
                        {
                       // HTMLText += "       Physician," + MDIArgForChild + "piphysician" + UnitSeparator + "PI Physician" + ",PIE,";
                            HTMLText += "       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstPhysician") + "," + MDIArgForChild + "piphysician" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstPhysician") + ",PIE,";
                        }
                        if (this.PersonInvolved["AddExistingWitness"])
                        {
                        //HTMLText += "       Witness," + MDIArgForChild + "piwitness" + UnitSeparator + "PI Witness" + ",PIE,";
                            HTMLText += "       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstWitness") + "," + MDIArgForChild + "piwitness" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstWitness") + ",PIE,";
                        }
                       // HTMLText +="Add New:," + "" + ",NON";
                        HTMLText += Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("PrsnInvlvedLstAddNew") + "," + "" + ",NON";
                        if (this.PersonInvolved["AddNewEmployee"])
                        {
                            //HTMLText += "       Employee," + MDIArgForChild + "piemployee" + UnitSeparator + "PI Employee" + ",ADD,";
                           // HTMLText += ",       Employee," + MDIArgForChild + "piemployee" + UnitSeparator + "PI Employee" + ",ADD"; 
                            HTMLText += ",       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstEmployee") + "," + MDIArgForChild + "piemployee" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstEmployee") + ",ADD";
                        }
                        if (this.PersonInvolved["AddNewMedicalStaff"])
                        {
                            //HTMLText += "       Medical Staff," + MDIArgForChild + "pimedstaff" + UnitSeparator + "PI Medstaff" + ",ADD,";
                           // HTMLText += ",       Medical Staff," + MDIArgForChild + "pimedstaff" + UnitSeparator + "PI Medstaff" + ",ADD";  
                            HTMLText += ",       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstMedicalStaff") + "," + MDIArgForChild + "pimedstaff" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstMedicalStaff") + ",ADD";
                        }
                        if (this.PersonInvolved["AddNewOtherPerson"])
                        {
                            //HTMLText += "       Other Person," + MDIArgForChild + "piother" + UnitSeparator + "PI Other" + ",ADD,";
                            //HTMLText += ",       Other Person," + MDIArgForChild + "piother" + UnitSeparator + "PI Other" + ",ADD";    
                            HTMLText += ",       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstOtherPerson") + "," + MDIArgForChild + "piother" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstOtherPerson") + ",ADD";
                        }
                        if (this.PersonInvolved["AddNewDriver"])
                        {
                            //HTMLText += "       Driver ," + MDIArgForChild + "pidriver" + UnitSeparator + "PI Driver " + ",ADD,";
                           // HTMLText += ",       Driver ," + MDIArgForChild + "pidriver" + UnitSeparator + "PI Driver " + ",ADD";   
                            HTMLText += ",       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstDriver") + "," + MDIArgForChild + "pidriver" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstDriver") + ",ADD";
                        }
                        //mbahl3 driver changes 
                        //HTMLText += "       Driver ," + MDIArgForChild + "pidriver" + UnitSeparator + "PI Driver " + ",ADD,";
                        //mbahl3 driver changes 
                        //Neha 06/27/2011 added two new type in Person Involved
                      //  HTMLText += "       Driver Insured," + MDIArgForChild + "piother" + UnitSeparator + "PI Driver Insured" + ",ADDDRIVERI,";
                      //  HTMLText += "       Driver Other," + MDIArgForChild + "piother" + UnitSeparator + "PI Driver Other" + ",ADDDRIVERO,";
                        //Neha 06/27/2011 added two new type in Person Involved  end
                        if (this.PersonInvolved["AddNewPatient"])
                        {
                            //HTMLText += "       Patient," + MDIArgForChild + "pipatient" + UnitSeparator + "PI Patient" + ",ADD,";
                           // HTMLText += ",       Patient," + MDIArgForChild + "pipatient" + UnitSeparator + "PI Patient" + ",ADD";   
                            HTMLText += ",       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstPatient") + "," + MDIArgForChild + "pipatient" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstPatient") + ",ADD";
                        }
                        if (this.PersonInvolved["AddNewPhysician"])
                        {
                            //HTMLText += "       Physician," + MDIArgForChild + "piphysician" + UnitSeparator + "PI Physician" + ",ADD,";
                          //  HTMLText += ",       Physician," + MDIArgForChild + "piphysician" + UnitSeparator + "PI Physician" + ",ADD";      
                            HTMLText += ",       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstPhysician") + "," + MDIArgForChild + "piphysician" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstPhysician") + ",ADD";
                        }
                        if (this.PersonInvolved["AddNewWitness"])
                        {
                            //HTMLText += "       Witness," + MDIArgForChild + "piwitness" + UnitSeparator + "PI Witness" + ",ADD');\">" + this.NodePlainText + defaultEnd;
                           // HTMLText += ",       Witness," + MDIArgForChild + "piwitness" + UnitSeparator + "PI Witness" + ",ADD";   
                            HTMLText += ",       " + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("AddPrsnInvlvedLstWitness") + "," + MDIArgForChild + "piwitness" + UnitSeparator + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodePrsnInvlvedLstWitness") + ",ADD";
                        }                        
                        HTMLText += "');\">" + this.NodePlainText + defaultEnd;
                        //change end here by swati
                        break;
                    }
                case "reservelisting":
                    {
                        MDIArgForChild = this.ParentPathValue + PathValueSeparator + this.GetNodeValue() + PathValueSeparator + this.NodeMDIId + UnitSeparator + "-1" + GroupSeparator;
                        HTMLText = "<span oncontextmenu=\"return&nbsp;startnodemenu(event,'New Reserve Current," + MDIArgForChild + "RESERVEWORKSHEET" + UnitSeparator + "Reserve Current" + ",ADD');\">" + this.NodePlainText + defaultEnd;
                        break;
                    }
                //Mridul. 11/24/09. MITS:18229
                case "Policy":
                    {
                        //HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'Close All," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        HTMLText = "<span class=t1 oncontextmenu=\"return&nbsp;startnodemenu(event,'" + Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("mdiNodeCloseAll") + "," + key + ",RMV');\">&nbsp;&nbsp;&nbsp;&nbsp;" + this.RootTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        break;
                    }
    	    	//MITS 34260 start
                case "claimxpolicy":
                    {
                        HTMLText = "<span>" + this.NodePlainText + "</span>";
                        break;
                    }
	    	    //MITS 34260 end
                default:
                    {
                        HTMLText = defaultStart + this.NodePlainText + defaultEnd;
                        break;
                    }
            }
            if (this.IsDirtied)
            {
                HTMLText = "<span class=notSaved>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>" + HTMLText;
            }
            return HTMLText;
        }

        private void SetKeyPropertiesFromMenu()
        {
            string root = string.Empty;
            //Deb ML Changes
            MDISetting mdObj = new MDISetting();
            XElement xml = XElement.Parse(mdObj.UpdateMDIMenu());
            //XElement xml = XElement.Load(System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\MDI\\App_Data\\MDIMenu.xml");
            //Deb ML Changes
            foreach (XElement el in xml.Descendants())
            {
                if (el.Parent.Attribute("title") == null)
                {
                    root = el.Attribute("title").Value;
                }
                if (el.Attribute("sysName") != null && el.Attribute("sysName").Value != string.Empty && el.Attribute("sysName").Value.ToLower() == this.NodeScreen.ToLower())
                {
                    if (this.Root == string.Empty)
                    {
                        //this.Root = root;
                        this.RootTitle = root;
                        switch (el.Parent.Attribute("sysName").Value)
                        {
                            case "DocumentRoot":
                                this.Root = "Document";
                                break;
                            case "PolicyRoot":
                                this.Root = "Policy";
                                break;
                            case "DiariesRoot":
                                this.Root = "Diaries";
                                break;
                            case "FundsRoot":
                                this.Root = "Funds";
                                break;
                            case "MaintenanceRoot":
                                this.Root = "Maintenance";
                                break;
                            case "MyWorkRoot":
                                this.Root = "My Work";
                                break;
                            case "ReportsRoot":
                                this.Root = "Reports";
                                break;
                            case "SearchRoot":
                                this.Root = "Search";
                                break;
                            case "SecurityRoot":
                                this.Root = "Security";
                                break;
                            case "UserDocumentsRoot":
                                this.Root = "User Documents";
                                break;
                            case "utilitiesRoot":
                                this.Root = "Utilities";
                                break;
                            case "HelpRoot":
                                this.Root = "Help";
                                break;
                            case "OtherRoot":
                                this.Root = "Other";
                                break;
                        }
                    }
                    if (this.NodeTitle == string.Empty)
                    {
                        this.NodeTitle = el.Attribute("title").Value;
                    }
                    if (this.NodeLocalPath == string.Empty)
                    {
                        this.NodeLocalPath = EncodeSpecialChars(el.Attribute("localPath").Value);
                    }
                    if (this.NodeParam == string.Empty)
                    {
                        this.NodeParam = el.Attribute("params").Value;
                    }
                    break;
                }
            }
            if (this.NodeTitle == string.Empty)
            {
                SetAsUndefined();
            }
            mdObj = null;//Deb ML Changes
        }

        public static string DecodeSpecialChars(string s)
        {
            s = s.Replace((char)2 + "92" + (char)3, "\\");
            return s;
        }

        public static string EncodeSpecialChars(string s)
        {
            s = s.Replace("'", "’");
            s = s.Replace("\\", (char)2 + "92" + (char)3);
            return s;
        }

        public string GetFinalizedNodeParam()
        {
            string param = string.Empty;
            param = this.NodeParam.Replace("(NODERECORDID)", this.NodeRecordId.ToString());
            param = param.Replace("(PARENTNODERECORDID)", this.NodeParentId.ToString());
            return param;
        }

        public bool NodeParamNeedsToBeFinalized()
        {
            if (this.NodeParam.IndexOf("(NODERECORDID)") != -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SetAsUndefined()
        {
            this.NodeTitle = "Undefined";
        }
    }
}
