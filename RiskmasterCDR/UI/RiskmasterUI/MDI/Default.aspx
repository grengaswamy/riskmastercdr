﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Riskmaster.MDI._Default" StylesheetTheme="RMX_MDI" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Riskmaster Accelerator</title>
    <style type="text/css">
        .nodemenu {
            height: auto!important;
        }

        #imgNodeTree {
            height: 18px;
        }
    </style>
</head>
<script type="text/javascript" src="../Scripts/jquery/jquery-1.8.0.js"></script>
<script type="text/javascript">
    function GetIEVersion() {
        var sAgent = window.navigator.userAgent;
        var Idx = sAgent.indexOf("MSIE");
        // If IE, return version number.
        if (Idx > 0)
            return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
            // If IE 11 then look for Updated user agent string.
        else if (!!navigator.userAgent.match(/Trident\/7\./))
            return 11;
        else
            return 0; //It is not IE
    }
</script>
<body id="MDI" onunload="MDIForceCloseAllPopUps();">
    <form id="frmMDIDefault" runat="server">
        <script type="text/javascript" src="Scripts/MDIBase.js"></script>
        <script type="text/javascript" src="Scripts/MDIContextMenu.js"></script>
        <script type="text/javascript" src="Scripts/MDILoadingSign.js"></script>
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="120" />
        <asp:HiddenField ID="relativePath" Value="" runat="server" />
        <asp:HiddenField ID="FDMLocalPath" Value="" runat="server" />
        <div id="RMXTab">
            <div id="navTreeOuter">
                <%--rgb(99, 143, 197)  OR #d6d7d7 OR #95B3D7--%>
                <div id="divimg" style="position: fixed; z-index: 99999; background-color: #d6d7d7; margin-left: 0;width:16%; /*height: 99.2%*/">
                    <input type="image" style="margin-left:85%" id="imgNodeTree" src="../Images/treearrowlt.png" onclick="toggleNodeTree(); return false;" />
                </div>
                <div id="navTreeInner">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" OnLoad="UpdatePanel1_Load">
                        <ContentTemplate>
                            <asp:HiddenField ID="previousNodeValuePath" Value="" runat="server" />
                            <asp:HiddenField ID="previousScript" Value="" runat="server" />
                            <asp:HiddenField ID="nextSearchResult" Value="" runat="server" />
                            <asp:HiddenField ID="searchResultRecordId" Value="1" runat="server" />
                            <asp:HiddenField ID="allowedSearchScreens" Value="" runat="server" />
                            <asp:HiddenField ID="allowedTotalScreens" Value="" runat="server" />
                            <asp:HiddenField ID="searchScreens" Value="" runat="server" />
                            <asp:HiddenField ID="screens" Value="" runat="server" />
                            <asp:HiddenField ID="ReachedMaxSearchScreens" Value="NO" runat="server" />
                            <asp:HiddenField ID="LastNumericId" Value="0" runat="server" />
                            <asp:HiddenField ID="selectedScreenId" Value="" runat="server" />
                            <asp:HiddenField ID="currentClaimMDIId" Value="" runat="server" />
                            <asp:HiddenField ID="taggedPolicyId" Value="" runat="server" />
                            <asp:HiddenField ID="isBOB" Value="" runat="server" />
                            <asp:HiddenField ID="hdnDocumenttitle" Value="" runat="server" />
                            <asp:HiddenField ID="hdnEventTitle" Value="" runat="server" />
                            <asp:HiddenField ID="hdnIsTreeHover" Value="true" runat="server" />
                            <asp:HiddenField ID="hdnIsNodeTreeShown" Value="true" runat="server" />
                            <asp:TreeView ID="navTree" runat="server" Height="100%" Width="100%" NodeIndent="6" ExpandDepth="0" BorderStyle="None" EnableClientScript="false">
                                <ParentNodeStyle Font-Bold="True" ForeColor="#005172" />
                                <HoverNodeStyle Font-Underline="False" />
                                <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" VerticalPadding="0px" />
                                <NodeStyle Font-Names="Arial" Font-Size="0.7em" ForeColor="#3572b0" HorizontalPadding="5px"
                                    NodeSpacing="0px" VerticalPadding="0px" />
                                <Nodes>
                                    <asp:TreeNode Expanded="True" Text="<span class=t1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Diary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>"
                                        Value="Diary">
                                        <asp:TreeNode Text="Diary List" Value="Diary List" Expanded="False" Selected="true"></asp:TreeNode>
                                        <asp:TreeNode Text="Diary Calendar" Value="Diary DCL" Expanded="False"></asp:TreeNode>
                                    </asp:TreeNode>
                                </Nodes>
                            </asp:TreeView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div id="menuBar" style="background-color: #638FC5"><%--#638FC5--%>
                <ignav:UltraWebMenu ID="MDIMenu" runat="server" JavaScriptFilename="" JavaScriptFileNameCommon=""
                    LeafItemImageUrl="" ParentItemImageUrl="" StyleSetName="" BackColor="#638FC5" 
                    DisabledClass="" FileUrl="" ForeColor="White" ItemPaddingSubMenus="1" SeparatorClass="" EnhancedRendering="True"
                    TargetFrame="" TargetUrl="" TopItemSpacing="Compact" TopSelectedClass="" WebMenuStyle="XPClient" Height="28px">
                    <Styles>
                        <ignav:Style BackColor="#64799C" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px"
                            CssClass="TopLevelHover" Cursor="Hand" Font-Names="Arial" Font-Size="8pt"
                            ForeColor="Black">
                            <BorderDetails WidthBottom="1px" WidthLeft="1px" WidthRight="1px" WidthTop="1px" />
                        </ignav:Style>
                        <ignav:Style BackColor="FloralWhite" BackgroundImage="None" BorderColor="#7288AC"
                            BorderStyle="Solid" BorderWidth="1px" CssClass="TopLevelStyle" Font-Names="Arial"
                            Font-Size="8pt" ForeColor="Black">
                            <BorderDetails WidthBottom="1px" WidthLeft="1px" WidthRight="1px" WidthTop="1px" />
                        </ignav:Style>
                    </Styles>
                    <IslandStyle BackColor="White" BorderStyle="Outset" BorderWidth="1px" Cursor="Hand"
                        Font-Names="Arial" Font-Size="8pt" ForeColor="Black">
                    </IslandStyle>
                    <TopSelectedStyle BackColor="#889DC2" BorderColor="Blue">
                        <BorderDetails WidthBottom="1px" WidthLeft="1px" WidthRight="1px" WidthTop="1px" />
                    </TopSelectedStyle>
                    <HoverItemStyle BackColor="#8babfe" Cursor="Hand" ForeColor="#003300">
                    </HoverItemStyle>
                    <Images>
                        <ScrollTopDisabledImage Url="ig_menu_scrollup_disabled.gif" />
                        <ScrollBottomDisabledImage Url="ig_menu_scrolldown_disabled.gif" />
                        <SubMenuImage Url="ig_menutri.gif" />
                        <ScrollBottomImage Url="ig_menu_scrolldown.gif" />
                        <ScrollTopImage Url="ig_menu_scrollup.gif" />
                        <XPSpacerImage Url="ig_menuCRMspacer1.gif" />
                    </Images>
                    <ItemStyle Font-Names="Arial" Font-Size="8pt" />
                    <DisabledStyle BorderStyle="Solid" ForeColor="LightGray">
                        <BorderDetails WidthBottom="1px" WidthLeft="1px" WidthRight="1px" WidthTop="1px" />
                    </DisabledStyle>
                    <Levels>
                        <ignav:Level Index="0" />
                        <ignav:Level Index="1" />
                        <ignav:Level Index="2" />
                        <ignav:Level Index="3" />
                    </Levels>
                    <SeparatorStyle BackgroundImage="ig_menuSep.gif" CssClass="SeparatorClass" CustomRules="background-repeat:repeat-x; " />
                    <ExpandEffects ShadowColor="Black" Type="Fade" RemovalDelay="100"></ExpandEffects>
                    <MenuClientSideEvents InitializeMenu="" ItemChecked="" ItemClick="" SubMenuDisplay=""
                        ItemHover=""></MenuClientSideEvents>
                </ignav:UltraWebMenu>
            </div>
            <div>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" OnLoad="UpdatePanel2_Load">
                    <ContentTemplate>
                        <div id="breadcrumb" runat="server">
                            <asp:Literal ID="litBreadcrumb" runat="server"></asp:Literal>
                        </div>
                        <div id="reloadButton">
                            <img src="Images/refresh.png" style="width: 16px; height: 16px" onclick="MDIRefreshByScreenId(document.getElementById('selectedScreenId').value);" runat="server" title='<%$ Resources:Reload %>' alt='Reload' />
                        </div>
                        <div id="closeButton">
                            <img src="Images/close.png" style="width: 16px; height: 16px" onclick="closeScreen(document.getElementById('selectedScreenId').value, false);" runat="server" title='<%$ Resources:Close %>' alt='Close' />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="scrollingFrame" runat="server" width="100%"></div>
        </div>
        <!-- closing screen -->
        <div id="big-frame">
        </div>
        <!-- closing RMXBody -->
        <div id="nodeMenu" onmouseover="keepnodemenu();" onmouseout="hidenodemenu();" runat="server"
            class="nodemenu">
        </div>
        <div id="pleaseWaitFrame" runat="server">
            <img onclick="pleaseWait('stop');" src="Images/loading.gif" alt="" />
        </div>
        <div id="right">
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                var rtclick = false;
                $("#navTreeOuter").hover(function () {
                    if (document.getElementById('hdnIsTreeHover').value == "false") {
                        $(this).css("width", "16%");
                        $(this).css("position", "fixed");
                        $(this).css("z-index", "99999");
                        $(this).css("height", "91%");
                        $(this).css("border", "1px solid #ccc");
                        $("#breadcrumb").css("left", "1.8%");
                        $("#menuBar").css("left", "1.5%");
                        $("#scrollingFrame").css("left", "1.5%");
                        $("#menuBar").css("width", "99.5%");
                        if (GetIEVersion() > 0) {
                            $("#scrollingFrame").css("width", "98.5%");
                        }
                        else {
                            if ($.browser.chrome) {
                                $("#scrollingFrame").css("width", "98.5%");
                            }
                            else if ($.browser.mozilla) {
                                $("#scrollingFrame").css("width", "99%");
                            }
                            else {
                                $("#scrollingFrame").css("width", "98.5%");
                            }
                        }
                        document.getElementById('divimg').style.width = "16%";
                        document.getElementById('divimg').style.height = "auto";
                        document.getElementById('imgNodeTree').style.marginLeft = "85%";
                    }
                }, function () {
                    if (!rtclick && document.getElementById('hdnIsTreeHover').value == "false") {
                        $(this).css("width", "1%");
                        $(this).css("position", "relative");
                        $(this).css("height", "99%");
                        //$(this).css("z-index", "0");
                        $(this).css("border", "0px solid #ccc");
                        $("#breadcrumb").css("left", "1.8%");
                        $("#menuBar").css("width", "98.5%");
                        $("#menuBar").css("left", "0.5%");
                        $("#scrollingFrame").css("left", "0.5%");
                        if (GetIEVersion() > 0) {
                            $("#scrollingFrame").css("width", "98.5%");
                        }
                        else {
                            if ($.browser.chrome) {
                                $("#scrollingFrame").css("width", "98.5%");
                            }
                            else if ($.browser.mozilla) {
                                $("#scrollingFrame").css("width", "99%");
                            }
                            else {
                                $("#scrollingFrame").css("width", "98.5%");
                            }
                        }
                        document.getElementById('hdnIsTreeHover').value = "false";
                        document.getElementById('divimg').style.width = "auto";
                        document.getElementById('divimg').style.height = "99%";
                        document.getElementById('imgNodeTree').style.marginLeft = "0";
                    }
                    rtclick = false;
                });
                document.oncontextmenu = function () { return false; };
                $(document).mousedown(function (e) {
                    if (e.button == 2) {
                        rtclick = true;
                        return false;
                    }
                    else {
                        rtclick = false;
                        return true;
                    }
                });
            });
            function toggleNodeTree() {
                if (document.getElementById('hdnIsNodeTreeShown').value == "false") {
                    $("#navTreeOuter").css("width", "16%");
                    $("#navTreeOuter").css("height", "99%");
                    $("#navTreeOuter").css("position", "relative");
                    $("#navTreeOuter").css("z-index", "0");
                    document.getElementById('divimg').style.width = "16%";
                    document.getElementById('divimg').style.height = "auto";
                    document.getElementById('imgNodeTree').style.marginLeft = "85%";
                    document.getElementById('imgNodeTree').src = "../Images/treearrowlt.png";
                    document.getElementById('hdnIsNodeTreeShown').value = "true";
                    $("#breadcrumb").css("left", "16.8%");
                    $("#menuBar").css("width", "83.3%");
                    $("#scrollingFrame").css("width", "83.3%");
                    $("#menuBar").css("left", ".5%");
                    $("#scrollingFrame").css("left", ".5%");
                    document.getElementById('hdnIsTreeHover').value = "true";
                }
                else {
                    $("#navTreeOuter").css("width", "1%");
                    document.getElementById('imgNodeTree').style.marginLeft = "0";
                    document.getElementById('divimg').style.width = "1%";
                    document.getElementById('divimg').style.height = "99%";
                    document.getElementById('imgNodeTree').src = "../Images/treearrowrt.png";
                    document.getElementById('hdnIsNodeTreeShown').value = "false";
                    document.getElementById('hdnIsTreeHover').value = "false";
                    $("#navTreeOuter").css("position", "relative");
                    $("#navTreeOuter").css("z-index", "0");
                    $("#navTreeOuter").css("height", "99.5%");
                    $(this).css("border", "0px solid #ccc");
                }
                return false;
            }
        </script>
    </form>
</body>
</html>
