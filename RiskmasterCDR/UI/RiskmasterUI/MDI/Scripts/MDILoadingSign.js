﻿/************************************************************
* Subject: Loading sign; instead of the "Please Wait!" message...
* Author: Nima Norouzi/FSG/CSC 
* Date: September 2008
************************************************************/

function loading1(type) {
    //do something before wait
    document.all.pleaseWaitFrame.style.visibility = "visible";

    loading2(0, type);
}

function loading2(i, type) {
    if (i < 10) {
        if (i == 7) //start something in the middle of waiting interval
        {
            __doPostBack("UpdatePanel1", "criteria " + type + "&0");
        }
        i++;
        window.setTimeout("loading2(" + i + ",'" + type + "')", 210);
    }
    else //do something after wait
        document.all.pleaseWaitFrame.style.visibility = "hidden";
}
