﻿/************************************************************
* Subject: Base MDI javascript functions
* Author: Nima Norouzi/FSG/CSC 
* Date: August 2008
************************************************************/
/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 11/04/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality
**********************************************************************************************/
var groupSeparator = String.fromCharCode(29);
var unitSeparator = String.fromCharCode(31);
var pathValueSeparator = String.fromCharCode(30);
var nodeActionSeparator = String.fromCharCode(92);
var nodeSelectionIndicator = String.fromCharCode(115);
var nodeTogglingIndicator = String.fromCharCode(116);
var zeroNodeIdIndicator = String.fromCharCode(122);
var oneToOneIndicator = String.fromCharCode(111);   //mbahl3 Jira [RMA-389]
var wereScreenPoolInitialized = false;
//Deb : MITS 31021
//var dirtyScreenReloadWarning = "This screen has unsaved data! Click on \"OK\" to discard changes and reload; otherwise click on \"Cancel\".";
//var dirtyScreenReloadWarning = parent.CommonValidations.dirtyScreenReloadWarning;
//rsushilaggar MITS 27964 Date 11/06/2012
//var dirtyScreenCloseWarning = "There is/are unsaved screen(s); including pop-ups! Click on \"OK\" to discard changes and close; otherwise click on \"Cancel\".";
//var dirtyScreenCloseWarning = "You are DISCARDING and closing unsaved screens; including pop-ups!";
//var dirtyScreenCloseWarning = parent.CommonValidations.dirtyScreenCloseWarning;
//var sharedInfoWarning = "Related Event and Claim screens contain shared information; so you need to edit and save them one at a time!"
//var sharedInfoWarning = parent.CommonValidations.sharedInfoWarning;
//var sharedPIInfoWarning = "PI Employee and Claim WC screens under the same event may contain shared information; so you need to edit and save them one at a time!"
//var sharedPIInfoWarning = parent.CommonValidations.sharedPIInfoWarning;
//var MDIMeterTooltipText = "MDI meter; a visual indication based on open screens. As it gets closer to red, the performance can be affected.";
//var MDIMeterTooltipText = parent.CommonValidations.MDIMeterTooltipText;
//Deb : MITS 31021
var MDIErrorMessage = "";
var m_Wnd = null;
var m_MDIArg = "";
var MDICommentsWinNamePrefix = "Comments";
var MDIPopUpWinType = new Object();
MDIPopUpWinType.comments = "Comments";
MDIPopUpWinType.progressNotes = "ProgressNotes";
MDIPopUpWinType.search = "Search";
MDIPopUpWinType.codeLookUp = "CodeLookUp";

var MDIPopUpArr = new Array(5);
var MDIMeterRatio = new Array(8);
MDIMeterRatio[0] = 0.05; // %5
MDIMeterRatio[1] = 0.11; //%11
MDIMeterRatio[2] = 0.18; //%18
MDIMeterRatio[3] = 0.26; //%26
MDIMeterRatio[4] = 0.37; //%37
MDIMeterRatio[5] = 0.52; //%52
MDIMeterRatio[6] = 0.72; //%72
MDIMeterRatio[7] = 1;   //%100
var MDIMeterIndex = -1;
var MDIMeterIndexChanged = true;
//Ashish Ahuja
var NextControl = null;
var win = null;
function updateMDIMeter() {
     
    var num = parseInt(document.getElementById('screens').value);
    var maxNum = parseInt(document.getElementById('allowedTotalScreens').value);
    var MDIMeter = document.getElementById('MDIMeter');
    var highColorLimit = 0;
    var lowColorLimit = 0;
    var k;

    if (MDIMeterIndex == -1) {
        highColorLimit = maxNum * MDIMeterRatio[0];
        lowColorLimit = maxNum * MDIMeterRatio[0];
    }
    else if (MDIMeterIndex == 7) {
        highColorLimit = maxNum * MDIMeterRatio[MDIMeterIndex];
        lowColorLimit = maxNum * MDIMeterRatio[MDIMeterIndex];
    }
    else {
        highColorLimit = maxNum * MDIMeterRatio[MDIMeterIndex + 1];
        lowColorLimit = maxNum * MDIMeterRatio[MDIMeterIndex];
    }

    //alert(num + " " + maxNum + " " + lowColorLimit + " " + highColorLimit + " " + MDIMeterIndex);
    if (num >= highColorLimit && MDIMeterIndex < 7) {
        MDIMeterIndex++;
        MDIMeterIndexChanged = true;
    }
    else if (num < lowColorLimit && MDIMeterIndex >= 0) {
        MDIMeterIndex--;
        MDIMeterIndexChanged = true;
    }
    //alert(MDIMeterIndex);

    if (MDIMeterIndexChanged) {
        MDIMeter.innerHTML = "&nbsp;";
        for (k = 1; k <= MDIMeterIndex + 1; k++) {
            MDIMeter.innerHTML += "<img src='Images/" + k + "on.gif'/>&nbsp;";
        }
        for (k = MDIMeterIndex + 2; k <= 8; k++) {
            MDIMeter.innerHTML += "<img src='Images/" + k + "off.gif'/>&nbsp;";
        }
        MDIMeterIndexChanged = false;
    }
}


function countSearchScreens() {
    var c = 0;
    var d = document.getElementById('scrollingFrame');
    var key = "Search";
    for (i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        if (child.nodeType === 1 && child.id.substr(0, key.length) == key) {
            c++;
        }
    }
    return c;
}

function tabFocus(tabName, screenId) {
     
    var found = false;
    var iFrameDoc = null;
    var objPW = document.getElementById('pleaseWaitFrame');
    var selectedId = document.getElementById('selectedScreenId').value;
    var selectedScreen = document.getElementById(selectedId);
    if (selectedScreen != null && screenId == MDIGetScreenInfo(selectedId, "RECORDID")) {
        iFrameDoc = (selectedScreen.contentDocument) ?
            selectedScreen.contentDocument : selectedScreen.contentWindow.document;
    }
    
    if (objPW.style.display == "none" && iFrameDoc != null) {
        var parwin = iFrameDoc.parentWindow;           //mjain8 Mozilla fix
        if (!parwin)
            parwin = iFrameDoc.defaultView;
        if (parwin.document.getElementById('FORMTAB' + tabName) != null) {
            found = true;
            parwin.tabChange(tabName);
        }
    }
    
    if (!found) {
        //alert("Please select the parent first or wait for it to be completely loaded!");
        alert(parent.CommonValidations.LetParentLoad);
    }
}

function getTimeStamp() {
    return (new Date().getTime());
}

function disposeScreen(screen) {
    screen.setAttribute('id', "Blank");
    screen.setAttribute('name', getTimeStamp());
    screen.style.display = 'none';
    screen.src = 'about:blank';
    var num = document.getElementById('screens');
    num.value--;
    //updateMDIMeter(); asharma326 MITS 34425 
}

function MDIGetCurrentPageName(currentLocation, correspondingScreen) {
     
    var splitItems = currentLocation.href.split('/');
    var name = splitItems[splitItems.length - 1].split('.')[0].toLowerCase();
    if (name == "lookupnavigation") {
        name = correspondingScreen;
    }
    return name;
}

function getBlankScreenFromPool() {
    var blankScreen = null;
    var currentLocation;
    var correspondingScreen;
    var d = document.getElementById('scrollingFrame');    
    for (i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        if (child.nodeType === 1 && child.id == "Blank") {
            blankScreen = child;
            var num = document.getElementById('screens');
            num.value++;
            //updateMDIMeter(); asharma326 MITS 34425 
            break;
        }
    }
    if (blankScreen == null) {
        for (i = 0; i < d.childNodes.length; i++) {
            child = d.childNodes[i];
            if (child.nodeType === 1 && child.name != "Dirty") {
                //currentLocation = self.frames[child.id].location;
                correspondingScreen = MDIGetScreenInfo(child.id, "SCREENTYPE").toLowerCase();
                if (self.frames[child.id] != null && MDIGetCurrentPageName(self.frames[child.id].location, correspondingScreen) == correspondingScreen) {
                    if (blankScreen == null) {
                        blankScreen = child;
                    }
                    else if (parseInt(blankScreen.name) > parseInt(child.name)) {
                        blankScreen = child;
                    }          
                }
            }
        }
    }
    //asharma326 MITS 34425 
    if (blankScreen != null) {
    //    //alert("You have reached the maximum number of allowed screens! To free up the memory, please close some of the existing ones...");
    //    alert(parent.CommonValidations.MaxAllowedScreen);
    //}
    //else {
        blankScreen.src = 'about:blank';    
        blankScreen.name = getTimeStamp();
    }
    return (blankScreen);
}

function initializeScreenPool() {
     
    var newScreen;
    var totalNum = document.getElementById('allowedTotalScreens').value;
    var d = document.getElementById('scrollingFrame');
    for (i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        if (child.nodeType === 1) {
            d.removeChild(child);
        }
    }
    for (i = 0; i < totalNum; i++) {
        newScreen = document.createElement('iframe');
        newScreen.setAttribute('id', "Blank");
        newScreen.setAttribute('name', getTimeStamp());
        newScreen.style.display = 'none';
        newScreen.src = 'about:blank';
        newScreen.style.border = '1px solid #EEEEEE';    
        newScreen.setAttribute('height', "99.5%");
        newScreen.setAttribute('width', "99.8%");
        newScreen.scrolling = 'auto';
        newScreen.overflow = 'auto';
        newScreen.frameBorder = 0;
        d.appendChild(newScreen);
    }
    wereScreenPoolInitialized = true;
}

function hideAll() {
    if (!wereScreenPoolInitialized) {
        initializeScreenPool();
    }
    else {
        var objcloseButton = document.getElementById('closeButton');
        objcloseButton.style.display = "none";
        var objreloadButton = document.getElementById('reloadButton');
        objreloadButton.style.display = "none";
        var objBreadcrumb = document.getElementById('breadcrumb');
        objBreadcrumb.style.display = "none";
        var d = document.getElementById('scrollingFrame');
        for (i = 0; i < d.childNodes.length; i++) {
            child = d.childNodes[i];
            if (child.nodeType === 1) {
                child.style.display = 'none';
            }
        }
        // rrachev JIRA RMA-501 (MITS 36502)
        try{            
            document.getElementById("menuBar").focus();
        } catch (e) {}
    }
    pleaseWait("stop");
}

function showListOfScreens() {
    var d = document.getElementById('scrollingFrame');
    var message = "";
    for (i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        if (child.nodeType === 1) {
            message += child.id + ", " + child.name + "\n\r";
        }
    }
    alert(message);
}

function MDIScreenLoaded() {
    pleaseWait("stop");
    var selectedId = document.getElementById('selectedScreenId').value;
    var selectedScreen = document.getElementById(selectedId);
    var iFrameDoc = (selectedScreen.contentDocument) ?
        selectedScreen.contentDocument : selectedScreen.contentWindow.document;
    if (iFrameDoc != null) {
        var parwin = iFrameDoc.parentWindow; //mjain8 Mozilla fix
        if (!parwin)
            parwin = iFrameDoc.defaultView;
        if (parwin.document.getElementById('hidden_DataChanged') != null) {
            if (parwin.document.getElementById('hidden_DataChanged').value != "true") {
                MDISetUnDirty(null, 0, false);
            }
        }
        else if (parwin.m_DataChanged != null) {
        if (parwin.m_DataChanged != true) {
                MDISetUnDirty(null, 0, false);
            }
        }
        else { // since DataChanged flag is not available we assume it is False!
            MDISetUnDirty(null, 0, false);
        }
    }
}

function MDIPassDirtinessCheck(screenObject) {
    if (screenObject == null)
        return true;
    else {
        if (screenObject.name == "Dirty") {
            if (confirm("Data has changed in the existing screen. Do you continue and discard changes?")) {
                return true;
            }
            else
                return false;
        }
    }
}

function MDIGetNodeId(nodeHref) {
    splitItems = nodeHref.split(nodeActionSeparator);
    if (splitItems.length == 1) {
        splitItems = nodeHref.split(pathValueSeparator);
    }
    return splitItems[splitItems.length - 1];
}

function MDICorrespondingNavTreeNodeExists(recordId, category) {
    var alreadyExists = false;
    var foundRoot = false;
    var checkRoot = false;
    var doContinue = true;
    var selectedId = document.getElementById('selectedScreenId').value;

    var root = MDIGetScreenInfo(selectedId, "ROOT");

    var navTreeId = "navTreet";
    var counter = 1;
    var node = document.getElementById(navTreeId + counter);
    var nodeId;
    while (doContinue && node != null) {
        if (node.href != null) {
            nodeId = MDIGetNodeId(node.href);
            nodeRoot = MDIGetScreenInfo(nodeId, "ROOT");
            nodeScreen = MDIGetScreenInfo(nodeId, "SCREENTYPE");
            nodeRecordId = MDIGetScreenInfo(nodeId, "RECORDID");
            if (nodeRoot == root) {
                foundRoot = true;
                if (category == nodeScreen && recordId == nodeRecordId) {
                    alreadyExists = true;
                    doContinue = false;
                }
            }
            else if (foundRoot) {
                doContinue = false;
            }
        }
        counter++;
        node = document.getElementById(navTreeId + counter);
    }
    return alreadyExists;
}

function MDIReplaceCurrentNodeByRecordId(recordId) {
    var selectedId = document.getElementById('selectedScreenId').value;
    __doPostBack("UpdatePanel1", "Refresh" + unitSeparator + recordId + pathValueSeparator + selectedId);
}

function MDIRefreshCurrentNode() { //Initially created for reserve worksheet
    var selectedId = document.getElementById('selectedScreenId').value;
    var recordId = MDIGetScreenInfo(selectedId, "RECORDID");
    __doPostBack("UpdatePanel1", "Refresh" + unitSeparator + recordId + pathValueSeparator + selectedId);
}

function MDIShowScreen(recordId, categoryArg) {
    var screen = "";
    var id = "";
    var d = document.getElementById('scrollingFrame');
    var selectedId = document.getElementById('selectedScreenId').value;
    var splitItems;
    var root = ""; //if this function is called from LookUpNavigation then root is later identified below in the js code based on the selected node.
    var titleExtension = "";
    splitItems = categoryArg.split(unitSeparator);
    screen = splitItems[0];
    /*if (splitItems.length > 2) {
        if (splitItems[2] == String.fromCharCode(32)) { //comparing with blank character (space)
            splitItems[2] = MDIGetScreenInfo(selectedId, "UserPrompt");
        }
    }*/
   
   
    for (i = 0; i < splitItems.length; i++) {
        if (i == 0) {
            categoryArg = screen;
        }
        else {

            categoryArg += unitSeparator + MDIEncodeSpecialChars(splitItems[i]);
        }
    }
    
    root = MDIGetScreenInfo(selectedId, "ROOT");
    
    if (id != null) {
        if (id == "") {
                __doPostBack("UpdatePanel1", root + unitSeparator + recordId + groupSeparator + categoryArg);
        }
        else
            __doPostBack("UpdatePanel1", id);
    }
}

//MITS:33574 START
function MDIShowScreenFromPolicy(recordId, categoryArg, PolicyId, PolicySystemID, Policynumber, DateOfLoss, LOB, LOBID, ClaimstatusId, ClaimReportedDate, sSource) {
    var screen = "";
    var id = "";
    var d = document.getElementById('scrollingFrame');
    var selectedId = document.getElementById('selectedScreenId').value;
    var splitItems;
    var root = ""; //if this function is called from LookUpNavigation then root is later identified below in the js code based on the selected node.
    var titleExtension = "";
    splitItems = categoryArg.split(unitSeparator);
    screen = splitItems[0];
    Policynumber = Policynumber.replace("|", "*");
    Policynumber = Policynumber.replace("|", "*");

    //RMA-10039
    var sSourceQueryParm = "";
    if (sSource != null && sSource != undefined && sSource !="")
        sSourceQueryParm = "&PolicySource=" + sSource;

    if (id != null) {
        if (id == "") {
            if (categoryArg == "claimwc") {
                __doPostBack("UpdatePanel1", 'Document|DocumentDocument-1claimwcWorkers’ Comp.' + "?Policynumber=" + Policynumber + "&DateOfLoss=" + DateOfLoss + "&LOB=" + LOB + "&PolicyId=" + PolicyId + "&PolicySystemID=" + PolicySystemID + "&LOBID=" + LOBID + "&ClaimstatusId=" + ClaimstatusId+"&ClaimReportedDate="+ClaimReportedDate + sSourceQueryParm); //RMA-10039
            }
            else {
                __doPostBack("UpdatePanel1", 'Document|DocumentDocument-1claimgcGeneral Claim' + "?Policynumber=" + Policynumber + "&DateOfLoss=" + DateOfLoss + "&LOB=" + LOB + "&PolicyId=" + PolicyId + "&PolicySystemID=" + PolicySystemID + "&LOBID=" + LOBID + "&ClaimstatusId=" + ClaimstatusId + "&ClaimReportedDate=" + ClaimReportedDate + sSourceQueryParm); //RMA-10039
            }
        }
        else
            __doPostBack("UpdatePanel1", id);
    }
}
//MITS:33574 END
function MDIAddExistingPersonForPI(arg) {
   
    var searchFormName = null;
    var datafrom = "";
    m_MDIArg = arg;
    splitItemes = arg.split(pathValueSeparator);
    switch (MDIGetScreenInfo(splitItemes[splitItemes.length - 1], "SCREENTYPE")) {
        case "piemployee":
            searchFormName = "employee";
            break;
        case "pipatient":
            // akaushik5 Changed for MITS 30805 Starts
            // akaushik5 Reverted the Changes for MITS 30805 Starts
            searchFormName = "patient";
//            searchFormName = "entity";
//            datafrom = "from=patient&";
            // akaushik5 Reverted the Changes for MITS 30805 Ends
            // akaushik5 Changed for MITS 30805 Ends
            // akaushik5 Added for MITS 30805 Starts
            datafrom = "from=MDI&";
           // akaushik5 Added for MITS 30805 Ends
            break;
        case "piphysician":
            searchFormName = "physician";
            break;
        case "piwitness":
            searchFormName = "witness";
            break;
        case "piother":
            searchFormName = "otherpeople";
            break;
        case "pimedstaff":
            searchFormName = "medstaff";
            break;
        case "pidriver":
            searchFormName = "driver";
            datafrom = "from=pidriver&";
            break;   //mbahl3
            //npradeepshar R8 CC injury 07/05/2011
        case "pidriveri":
            searchFormName = "otherpeople";
            datafrom = "from=pidriveri&";
            break;
        case "pidrivero":
            searchFormName = "otherpeople";
            datafrom = "from=pidrivero&";
            break;
        //npradeepshar R8 CC injury end  
    }
    if (searchFormName != null) {
        //npradeepshar R8 CC  Added datafrom
        //rsushilaggar JIRA 7767
        m_Wnd = window.open("../UI/Search/searchmain.aspx?"+datafrom+"formname=" + searchFormName + "&screenflag=2&showaddnew=true", searchFormName, "width=600,height=500,resizable=yes,scrollbars=yes,left=250,top=150,screenX=250,screenY=150");    
        self.lookupCallback = "MDIOnPersonSearchCallback";
    }
}

//avipinsrivas Start : Worked for Jira-340(entityroleids is ~ seperated combination of Entity_X_Roles --> Entity_ID & Er_RowID)
function MDIOnPersonSearchCallback(eid, searchcat) {
    var arg;
    if (m_Wnd != null) {
        m_Wnd.close();
        m_Wnd = null;
    } 
    if (m_MDIArg != "") {
        arg = m_MDIArg + unitSeparator + unitSeparator + "?pieid=" + eid + "&SearchType=" + searchcat + "&DataChange=true";
        switch (MDIGetScreenInfo(splitItemes[splitItemes.length - 1], "SCREENTYPE")) {
            //npradeepshar R8 CC injury on search replaced the pseudo name of the file with actual name;
            case "pidriveri":
                arg = arg.replace('pidriveri', 'piother');
                arg = arg + "&pagetypename=driver_insured";
                break;
            case "pidrivero":
                arg = arg.replace('pidrivero', 'piother');
                arg = arg + "&pagetypename=driver_other";
                break;
 
        }
        m_MDIArg = "";
        __doPostBack("UpdatePanel1", arg);
    }
}

function replaceAll(s, searchFor, replaceWith) {
    while (s.indexOf(searchFor) != -1) {
        s = s.replace(searchFor, replaceWith);
    }
    return s;
}

function MDIEncodeSpecialChars(s) {

    s = replaceAll(s, "\\", String.fromCharCode(2) + "92" + String.fromCharCode(3));
    s = replaceAll(s, "'", String.fromCharCode(2) + "39" + String.fromCharCode(3));    
    return s;
}

//function MDISubmitSearchQuery(form) {
function MDISearchResult(form) {
    //var reachedMaxSearch = document.getElementById('reachedMaxSearchScreens').value;
    var maxNum = document.getElementById('allowedSearchScreens');
    var numSearch = document.getElementById('searchScreens');

    if (parseInt(numSearch.value) >= parseInt(maxNum.value))
        //alert("You have reached the maximum number of allowed result screens! For more results please remove some of the existing ones...");
        alert(parent.CommonValidations.MaxAllowedScreen);
    else {

        d = document.getElementById('scrollingFrame');
        for (i = 0; i < d.childNodes.length; i++) {
            child = d.childNodes[i];
            if (child.nodeType === 1) {
                child.style.display = 'none';
            }
        }
        var blankScreen = getBlankScreenFromPool();
        if (blankScreen != null) {
            var resultName = document.getElementById('nextSearchResult').value;
            blankScreen.setAttribute('id', resultName);
            blankScreen.setAttribute('name', resultName);
            blankScreen.contentWindow.name = resultName;
        pleaseWait("start");

        numSearch.value++;

            //if (self.frames[resultName] != null && self.frames[resultName].name != resultName) { /* BUG FIX for IE */
            //    self.frames[resultName].name = resultName;
            //}

        form.target = resultName;
        //form.action = "SearchResults.aspx";//Deb Pen Testing
        form.action = "SearchMain.aspx?btnSubmitQry=true";
        form.submit();

        __doPostBack("UpdatePanel1", "Search" + unitSeparator + "0" + groupSeparator + "Result");
        }
    }
}

function MDISetDeleted() {
    var selectedId = document.getElementById('selectedScreenId').value;
    var selectedScreen = document.getElementById(selectedId);
    selectedScreen.name = "Deleted";
}

function MDISetWipedOut() {
    var selectedId = document.getElementById('selectedScreenId').value;
    var selectedScreen = document.getElementById(selectedId);
    selectedScreen.name = "WipedOut";
}

function MDIIsClaimAncestorDirty() {
    var isDirty = false;
    var selectedId = document.getElementById('selectedScreenId').value;
    var ClaimMDIId = MDIGetScreenInfo(selectedId, "CLAIMMDIID");
    var MDIId;
    var d = document.getElementById('scrollingFrame');
    for (i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        MDIId = MDIGetScreenInfo(child.id, "MDIID");
        if (child.nodeType === 1 && MDIId == ClaimMDIId) {
            if (child.name == "Dirty") {
                isDirty = true;
            }
            break;
        }
    }
    return isDirty;
}

function MDIAnyPopUpDirtyUnder(id) {
    var anyPopUpDirty = undefined;
    var popUpWin = undefined;
    var plainMDIId = MDIGetScreenInfo(id, "PLAINMDIID");
    for (var i = 0; i < MDIPopUpArr.length; i++) {
        if (MDIPopUpArr[i] != undefined && !MDIPopUpArr[i].closed) {
            if (MDIPopUpArr[i].name.length >= plainMDIId.length && MDIPopUpArr[i].name.substr(0, plainMDIId.length) == plainMDIId) {
                popUpWin = MDIPopUpArr[i];
                if (typeof(popUpWin.m_DataChanged) != undefined && popUpWin.m_DataChanged == true) {
                    anyPopUpDirty = popUpWin;
                    break;
                }
            }
        }
    }
    return anyPopUpDirty;
}

function MDIAnyPopUpUnder(id) {
    var popUpWin = undefined;
    var plainMDIId = MDIGetScreenInfo(id, "PLAINMDIID");
    for (var i = 0; i < MDIPopUpArr.length; i++) {
        if (MDIPopUpArr[i] != undefined && !MDIPopUpArr[i].closed) {
            if (MDIPopUpArr[i].name.length >= plainMDIId.length && MDIPopUpArr[i].name.substr(0, plainMDIId.length) == plainMDIId) {
                popUpWin = MDIPopUpArr[i];
                break;
            }
        }
    }
    return popUpWin;
}

function MDIRefreshByScreenId(id) {
    var stillReload = false;
    var screenFoundAndRefreshed = false;
    var d = document.getElementById('scrollingFrame');
    var MDIId = MDIGetScreenInfo(id, "MDIID");
    var childMDIId;
    for (var i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        childMDIId = MDIGetScreenInfo(child.id, "MDIID");
        if (child.nodeType === 1 && childMDIId == MDIId) { //screen is found!
            if (MDICheckDirtyScreensUnder(id, false, false)) {
                stillReload = confirm(parent.CommonValidations.dirtyScreenReloadWarning);
            }
            else
                stillReload = true;
            if (stillReload) {
                screenFoundAndRefreshed = MDIForceRefreshByObjScreen(child);
            }
            break;
        }
    }
    return screenFoundAndRefreshed;
}

// Gets ScreenId as input(id) and tries to find and refresh the screen no matter it is dirty or not.
// Returns true if the screen is found and refreshed; otherwise false.
function MDIForceRefreshByScreenId(id) { 
    var screenFoundAndRefreshed = false;
    var d = document.getElementById('scrollingFrame');
    var plainMDIId = MDIGetScreenInfo(id, "PLAINMDIID");
    var childMDIId;
    for (var i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        childMDIId = MDIGetScreenInfo(child.id, "PLAINMDIID");
        if (child.nodeType === 1 && childMDIId == plainMDIId) { //screen is found!
            screenFoundAndRefreshed = MDIForceRefreshByObjScreen(child);
            break;
        }
    }
    return screenFoundAndRefreshed;
}

function MDIForceRefreshByObjScreen(objScreen) {
    var screenRefreshed = false;
    var screenId = undefined;
    if (objScreen != undefined) {
        screenId = objScreen.id;
        var recordId = MDIGetScreenInfo(screenId, "RECORDID");
        if (screenId != undefined && isNaN(recordId) == false) {
            disposeScreen(objScreen); //dispose the screen from client memory and re-load the screen from server in order to discard the change user just made!
            __doPostBack("UpdatePanel1", "Reload" + unitSeparator + recordId + pathValueSeparator + screenId);
            screenRefreshed = true;
        }
    }
    return screenRefreshed;
}

function MDIForceCloseAnyPopUpUnder(id) {
    var plainMDIId = MDIGetScreenInfo(id, "PLAINMDIID");
    var popUpWin;
    for (var i = 0; i < MDIPopUpArr.length; i++) {
        if (MDIPopUpArr[i] != undefined && !MDIPopUpArr[i].closed) {
            if (MDIPopUpArr[i].name.length >= plainMDIId.length && MDIPopUpArr[i].name.substr(0, plainMDIId.length) == plainMDIId) {
                popUpWin = MDIPopUpArr[i];
                if (typeof (popUpWin.m_DataChanged) != undefined && popUpWin.m_DataChanged == true) {
                    popUpWin.m_DataChanged = false;
                }
                popUpWin.close();            
            }
        }
    }
}

function MDIForceCloseAllPopUps() {
    var popUpWin;
    for (var i = 0; i < MDIPopUpArr.length; i++) {
        if (MDIPopUpArr[i] != undefined && !MDIPopUpArr[i].closed) {
            popUpWin = MDIPopUpArr[i];
            if (typeof (popUpWin.m_DataChanged) != undefined && popUpWin.m_DataChanged == true) {
                popUpWin.m_DataChanged = false;
            }
            popUpWin.close();
        }
    }
}

function MDIGetPopUpWin(winName) {
    var alreadyOpen = false;
    for (var i = 0; i < MDIPopUpArr.length; i++) {
        if (MDIPopUpArr[i] != undefined && !MDIPopUpArr[i].closed) {
            if (MDIPopUpArr[i].name == winName) {
                alreadyOpen = true;
                break;
            }
        }
    }
    if (alreadyOpen) {
        return MDIPopUpArr[i];
    }
    else {
        return undefined;
    }
}

function MDICreatePopUpWin(link, popUpType, parentWin,width,height) {
    MDIErrorMessage = "";
    var id = parentWin.frameElement.id;
    var plainMDIId = MDIGetScreenInfo(id, "PLAINMDIID"); //IE 7 does not like special characters to be in "name" properties; but Ok with them in "id" properties.
    var winName = plainMDIId + popUpType;
    var popUpWin = MDIGetPopUpWin(winName); // see if already open
    if (popUpWin != undefined) {
        popUpWin.focus();
    }
    else {
        for (i = 0; i < MDIPopUpArr.length; i++) {// see if we can open a new window
            if (MDIPopUpArr[i] == undefined || MDIPopUpArr[i].closed) {
            //Changed by amitosh for mits 23691(05/11/2011)
                if (height == null && width == null) {
                MDIPopUpArr[i] = window.open(link, winName,
        'width=760,height=450' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 720) / 2 + ',resizable=yes,scrollbars=yes');
                } //MDIPopUpArr[i].opener = top;
                else {
                    MDIPopUpArr[i] = window.open(link, winName,
        'width='+width+',height='+height + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 720) / 2 + ',resizable=yes,scrollbars=yes');
                }
                popUpWin = MDIPopUpArr[i];
                popUpWin.opener = parentWin;
                break;
            }
            if (i == MDIPopUpArr.length - 1) {
                MDIErrorMessage = "You have reached the maximum allowed number of pop-ups(" + MDIPopUpArr.length + ")!";
            }
        }
    }
    return popUpWin;
}

function MDIRemoveClaimAncestorFromMemory() {
    var claimRemoved = false;
    var selectedId = document.getElementById('selectedScreenId').value;
    var ClaimMDIId = MDIGetScreenInfo(selectedId, "CLAIMMDIID");
    var MDIId;
    var d = document.getElementById('scrollingFrame');
    for (var i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        MDIId = MDIGetScreenInfo(child.id, "MDIID");
        if (child.nodeType === 1 && MDIId == ClaimMDIId) {
            disposeScreen(child);
            claimRemoved = true;
            break;
        }
    }
    return claimRemoved;
}
function MDIPersonInvovedListFromMemory() {
    var PIListRemoved = false;
    var selectedId = document.getElementById('selectedScreenId').value;
    var ParentMdIId = MDIGetScreenInfo(selectedId, "PARENTMDIID");
    var MDIId;
    var d = document.getElementById('scrollingFrame');
    for (var i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        MDIId = MDIGetScreenInfo(child.id, "MDIID");
        if (child.nodeType === 1 && MDIId == ParentMdIId) {
            disposeScreen(child);
            PIListRemoved = true;
            break;
        }
    }
    return PIListRemoved;
}
function MDIRemoveNotDirtyFinancialDescendantsFromMemory() {
    var succeeded = false;
    var selectedId = document.getElementById('selectedScreenId').value;
    var isEvent = MDIGetScreenInfo(selectedId, "ISEVENT");
    var currentRoot = MDIGetScreenInfo(selectedId, "ROOT");    
    if (currentRoot == "Document" && !isEvent) {
        var ClaimMDIId = MDIGetScreenInfo(selectedId, "CLAIMMDIID");
        var MDIId, screenType;
        var d = document.getElementById('scrollingFrame');
        var i = 0;
        while (i < d.childNodes.length) {
            child = d.childNodes[i];
            screenType = MDIGetScreenInfo(child.id, "SCREENTYPE");
            if (child.nodeType === 1 && screenType == "reservelisting" && child.id.substr(0, ClaimMDIId.length) == ClaimMDIId && child.name != "Dirty") {
                disposeScreen(child);
                i++;
            }
            else {
                i++;
            }
        }
        succeeded = true;
    }
    return succeeded;
}

function MDIGetScreenInfo(screenId, property) {
    var returnValue;
     
    if (screenId == null || property == null || property == "" || screenId == "")
        return false;
    var splitItems = screenId.split(groupSeparator);
    switch (property.toUpperCase()) {
        case "SCREENTYPE":
            if (splitItems.length > 1) {
                splitItems = splitItems[1].split(unitSeparator);
                returnValue = splitItems[0];
            }
            else {
                returnValue = "";
            }
            break;
        case "PARENTSCREENTYPE":
            var breadcrumb = document.getElementById('breadcrumb').innerHTML;
            splitItems = breadcrumb.split('&#' + pathValueSeparator.charCodeAt(0) + ';');
            if (splitItems.length > 1) {
                splitItems = splitItems[splitItems.length - 2].split('&#' + groupSeparator.charCodeAt(0) + ';');
                if (splitItems.length > 1) {
                    splitItems = splitItems[1].split('&#' + unitSeparator.charCodeAt(0) + ';');
                    returnValue = splitItems[0];
                }
                else {
                    returnValue = "ROOT";
                }
            }
            else {
                returnValue = "";
            }
            break;
        case "ROOT":
            splitItems = splitItems[0].split(unitSeparator);
            returnValue = splitItems[0];
            break;
        case "RECORDID":
            returnValue = "0";
            splitItems = splitItems[0].split(unitSeparator);
            if (splitItems.length >= 2) {
                if (splitItems[splitItems.length - 1].substr(0, 1) != zeroNodeIdIndicator) {
                    //mbahl3 Jira [RMA-389]
		    if (splitItems[splitItems.length - 1].indexOf(oneToOneIndicator) != -1) {
                        splitItems = splitItems[splitItems.length - 1].split(oneToOneIndicator); //mbahl3
                        returnValue = splitItems[0];
                    }
                    else {
                        returnValue = splitItems[splitItems.length - 1];
                    } 
		      //mbahl3 Jira [RMA-389]
                }
            }
            break;
        case "MDIPARENTRECORDID":
            returnValue = "0";
            splitItems = splitItems[0].split(unitSeparator);
            if (splitItems.length >= 3) {
                if (splitItems[splitItems.length - 2].substr(0, 1) != zeroNodeIdIndicator) {
                    returnValue = splitItems[splitItems.length - 2]
                }
            }
            else {
                returnValue = "";
            }
            break;                        
        case "MDIID":
            returnValue = splitItems[0];
            break;
        case "PLAINMDIID":
            splitItems = splitItems[0].split(unitSeparator);
            if (splitItems.length >= 2) {
                for (i = 0; i < splitItems.length; i++) {
                    if (i == 0) {
                        returnValue = splitItems[i];
                    }
                    else {
                        returnValue += splitItems[i];
                    }
                }
            }
            else {
                returnValue = splitItems[0];
            }
            break;             
        case "PARENTMDIID":
            splitItems = splitItems[0].split(unitSeparator);
            if (splitItems.length >= 2) {
                for (i = 0; i < splitItems.length - 1; i++) {
                    if (i == 0) {
                        returnValue = splitItems[i];
                    }
                    else {
                        returnValue += unitSeparator + splitItems[i];
                    }
                }
            }
            else {
                returnValue = "";
            }
            break;            
        case "EVENTMDIID":
            splitItems = splitItems[0].split(unitSeparator);
            if (splitItems.length >= 2) { //else it is a root (splitItems.length = 1), and EventMDIId can not be fetched because there might be none or multiple events under the root; so basically bad call to this function and an empty string will be returned.
                returnValue = splitItems[0] + unitSeparator + splitItems[1];
            }
            break;
        case "CLAIMMDIID":
            splitItems = splitItems[0].split(unitSeparator);
            if (splitItems.length >= 3) {
                returnValue = splitItems[0] + unitSeparator + splitItems[1] + unitSeparator + splitItems[2];
            }
            break;
        case "ISCLAIM":
            returnValue = false;
            if (splitItems.length > 1) {
                splitItems = splitItems[1].split(unitSeparator);
                if (splitItems[0] == "claimgc" || splitItems[0] == "claimwc" || splitItems[0] == "claimva" || splitItems[0] == "claimdi" || splitItems[0] == "claimpc") {
                    returnValue = true;
                }
            }
            break;
        case "ISEVENT":
            returnValue = false;
            if (splitItems.length > 1) {
                splitItems = splitItems[1].split(unitSeparator);
                if (splitItems[0] == "event") {
                    returnValue = true;
                }
            }
            break;
        case "ISCLAIMOREVENT":
            returnValue = false;
            if (splitItems.length > 1) {
                splitItems = splitItems[1].split(unitSeparator);
                if (splitItems[0] == "event" || splitItems[0] == "claimgc" || splitItems[0] == "claimwc" || splitItems[0] == "claimva" || splitItems[0] == "claimdi" || splitItems[0] == "claimpc") {

                    returnValue = true;
                }
            }
            break;
        case "ISCLAIMWCORPIE": // MITS 17554
            returnValue = false;
            if (splitItems.length > 1) {
                splitItems = splitItems[1].split(unitSeparator);
                if (splitItems[0] == "claimwc" || splitItems[0] == "piemployee") {
                    returnValue = true;
                }
            }
            break;            
        case "USERPROMPT":
            if (splitItems.length > 1) {
                returnValue = splitItems[2];
            }
            else {
                returnValue = "";
            }
            break;
        case "ISCLAIMRESERVES": //bpaskova JIRA 9419
            returnValue = false;
            if (splitItems.length > 1) {
                splitItems = splitItems[1].split(unitSeparator);
                if (splitItems[0] == "reservelisting") {
                    returnValue = true;
                }
            }
            break;
        default:
            returnValue = "";
            break;
    }
    return returnValue;
}

function MDICheckDirtyScreensUnder(selectedId, all, switchToDirtyScreen) {
    //If all is true then also check children; otherwise only check the screen itself.
    //If switchToDirtyScreen is true then switch to the first dirty screen(including pop-ups); otherwise do not switch
    //This function returns true if the screen itself is dirty or any dirty child is found(including pop-ups)
    var isDirty = false;
    var selectedScreen = document.getElementById(selectedId);
    var dirtyPopUp = undefined;
    d = document.getElementById('scrollingFrame');
    MDIId = MDIGetScreenInfo(selectedId, "MDIID");
    
    for (var i = 0; i < d.childNodes.length; i++) {
        if (!all) {
            child = selectedScreen;
            i = d.childNodes.length;
        }
        else {
            child = d.childNodes[i];
        }
        if (child.nodeType === 1 && child.id.substr(0, MDIId.length) == MDIId) {
            dirtyPopUp = MDIAnyPopUpDirtyUnder(child.id);
            if (child.name == "Dirty" || dirtyPopUp != undefined) {
                if (switchToDirtyScreen) {
                    if (dirtyPopUp != undefined)
                        dirtyPopUp.focus();
                    else
                        __doPostBack("UpdatePanel1", child.id);
                }
                isDirty = true;
                break;
            }        
        }
    }
    
    return isDirty;
}

function MDISetDirty() {
    var eventMDIId, eventMDIId2;
    var d;
    var i;
    var setItDirty;
    var selectedId = document.getElementById('selectedScreenId').value;
    var selectedScreen = document.getElementById(selectedId);
    var isClaimOrEvent = MDIGetScreenInfo(selectedId, "IsClaimOrEvent");
    var isClaimWCOrPIEmployee = MDIGetScreenInfo(selectedId, "IsClaimWCOrPIE");
    var screenType = MDIGetScreenInfo(selectedId, "ScreenType");
    var plainMDIId = MDIGetScreenInfo(selectedId, "PLAINMDIID");
    var popUpWin;
    if (selectedScreen.name != "Dirty") {
        popUpWin = MDIGetPopUpWin(plainMDIId + MDIPopUpWinType.comments);
        if (popUpWin != undefined && String(true) == popUpWin.document.getElementById('hdUseLegacyComments').value.toLowerCase()) {
            //alert("You must close the comment pop-up window before modifying this screen!");
            alert(parent.CommonValidations.ClosePopup);
            disposeScreen(selectedScreen); //dispose the screen from client memory and re-load the screen from server in order to discard the change user just made!
            __doPostBack("UpdatePanel1", selectedId);
        }
        else {
            setItDirty = true;
            if (isClaimOrEvent) {
                d = document.getElementById('scrollingFrame');
                eventMDIId = MDIGetScreenInfo(selectedId, "EventMDIId");
                for (i = 0; i < d.childNodes.length; i++) {
                    child = d.childNodes[i];
                    eventMDIId2 = MDIGetScreenInfo(child.id, "EventMDIId");
                    if (child.nodeType === 1 && eventMDIId2 == eventMDIId) {
                        if (child.name == "Dirty") { // to make sure we are checking against Dirty screens and also not against itself.
                            isClaimOrEvent = MDIGetScreenInfo(child.id, "IsClaimOrEvent");
                            if (isClaimOrEvent) {
                                setItDirty = false;
                                alert(parent.CommonValidations.sharedInfoWarning);
                                i = d.childNodes.length;
                                disposeScreen(selectedScreen); //dispose the screen from client memory and re-load the screen from server in order to discard the change user just made!
                                __doPostBack("UpdatePanel1", selectedId);
                                break;
                            }
                        }
                    }
                }
            }
            if (isClaimWCOrPIEmployee && setItDirty) { // MITS 17554
                d = document.getElementById('scrollingFrame');
                eventMDIId = MDIGetScreenInfo(selectedId, "EventMDIId");
                for (i = 0; i < d.childNodes.length; i++) {
                    child = d.childNodes[i];
                    eventMDIId2 = MDIGetScreenInfo(child.id, "EventMDIId");
                    if (child.nodeType === 1 && eventMDIId2 == eventMDIId) {
                        if (child.name == "Dirty") {
                            isClaimWCOrPIEmployee = MDIGetScreenInfo(child.id, "IsClaimWCOrPIE");
                            if (isClaimWCOrPIEmployee && screenType != MDIGetScreenInfo(child.id, "ScreenType")) {
                                setItDirty = false;
                                alert(parent.CommonValidations.sharedPIInfoWarning);
                                i = d.childNodes.length;
                                disposeScreen(selectedScreen); //dispose the screen from client memory and re-load the screen from server in order to discard the change user just made!
                                __doPostBack("UpdatePanel1", selectedId);
                                break;
                            }
                        }
                    }
                }
            }
            if (setItDirty) {
                selectedScreen.name = "Dirty";
                __doPostBack("UpdatePanel1", "Dirty" + unitSeparator + pathValueSeparator + selectedId);
            }
        }
    }
}

function MDISetUnDirty(FDMServiceStatus, recordId, isCorrespondingScreenWithNavTree) { //when a form is saved or deleted or basically loaded this function is called. So far the value of isCorrespondingScreenWithNavTree is important for us only for FDM pages.
    pleaseWait("stop");
    var eventMDIId, eventMDIId2, MDIId, parentMDIId;
    var screenType, screenType2;
    var d;
    var i;
    var selectedId = document.getElementById('selectedScreenId').value;
    var selectedScreen = document.getElementById(selectedId);
    var isClaimOrEvent = MDIGetScreenInfo(selectedId, "IsClaimOrEvent");
    var screenType = MDIGetScreenInfo(selectedId, "ScreenType");
    if (FDMServiceStatus == null) {
        if (selectedScreen.name == "Dirty") {
            selectedScreen.name = getTimeStamp();
            __doPostBack("UpdatePanel1", "Refresh" + unitSeparator + "0" + pathValueSeparator + selectedId);
        }
    }
    else {
        if (FDMServiceStatus == 1) {
            var MDIChildRecordId;
            var MDIParentRecordId = MDIGetScreenInfo(selectedId, "MDIPARENTRECORDID");
            var currentRoot = MDIGetScreenInfo(selectedId, "ROOT");    
            if (selectedScreen.name == "Dirty" && recordId > 0) {
                selectedScreen.name = getTimeStamp();
                if (isCorrespondingScreenWithNavTree == false) {
                    recordId = 0; //we just pass zero as the new recordId but it has to be greater than zero to be replaced with the actual recordId.
                }
                else {
                    var isClaimWCOrPIEmployee = MDIGetScreenInfo(selectedId, "isClaimWCOrPIE");
                    if (isClaimOrEvent) { // related claims and events have shared information
                        d = document.getElementById('scrollingFrame');
                        eventMDIId = MDIGetScreenInfo(selectedId, "EventMDIId");
                        i = 0;
                        while (i < d.childNodes.length) {// loop through screens in client memory
                            child = d.childNodes[i];
                            eventMDIId2 = MDIGetScreenInfo(child.id, "EventMDIId");
                            if (child.nodeType === 1 && eventMDIId2 == eventMDIId && child.id != selectedId) {// if the screen is a child of current event and it is not the selected screen (because selected screen just got refreshed and we don't want to refresh it again)
                                isClaimOrEvent = MDIGetScreenInfo(child.id, "IsClaimOrEvent");
                                if (isClaimOrEvent) {
                                    disposeScreen(child); //we dispose this from client memory so that once User clicks on corresponding node in navTree the screen is loaded again and refreshed.
                                }
                            }
                            i++;
                        }
                    }
                    if (isClaimWCOrPIEmployee) { // MITS 17554
                        d = document.getElementById('scrollingFrame');
                        eventMDIId = MDIGetScreenInfo(selectedId, "EventMDIId");
                        i = 0;
                        while (i < d.childNodes.length) {// loop through screens in client memory
                            child = d.childNodes[i];
                            eventMDIId2 = MDIGetScreenInfo(child.id, "EventMDIId");
                            if (child.nodeType === 1 && eventMDIId2 == eventMDIId && child.id != selectedId) {// if the screen is a child of current event and it is not the selected screen (because selected screen just got refreshed and we don't want to refresh it again)
                                isClaimWCOrPIEmployee = MDIGetScreenInfo(child.id, "isClaimWCOrPIE");
                                if (isClaimWCOrPIEmployee && screenType != MDIGetScreenInfo(child.id, "ScreenType")) {
                                    disposeScreen(child); //we dispose this from client memory so that once User clicks on corresponding node in navTree the screen is loaded again and refreshed.
                                }
                            }
                            i++;
                        }
                    }

                    if (currentRoot == "Document" && MDIParentRecordId == "0") { // MDI parent is a list node so it has to be removed from the memory as well, and refreshed later if User clicks on that.
                        parentMDIId = MDIGetScreenInfo(selectedId, "PARENTMDIID");
                        screenType = MDIGetScreenInfo(selectedId, "SCREENTYPE");
                        d = document.getElementById('scrollingFrame');
                        i = 0;
                        while (i < d.childNodes.length) {
                            child = d.childNodes[i];
                            MDIId = MDIGetScreenInfo(child.id, "MDIID");
                            screenType2 = MDIGetScreenInfo(child.id, "SCREENTYPE");
                            if (child.nodeType === 1 && MDIId == parentMDIId && screenType2 == screenType) {
                                disposeScreen(child);
                                break;
                            }
                            else {
                                i++;
                            }
                        }
                    }
                }
                //showListOfScreens();
                __doPostBack("UpdatePanel1", "Refresh" + unitSeparator + recordId + pathValueSeparator + selectedId);
            }
            else if (selectedScreen.name == "Deleted") {
                selectedScreen.name = getTimeStamp();
                if (MDIGetScreenInfo(selectedId, "SCREENTYPE") == "cmxvocrehab") {
                    isCorrespondingScreenWithNavTree = false;
                }
                //alert("The record has been successfully deleted!");
                alert(parent.CommonValidations.MDIDeleted);
                if (isCorrespondingScreenWithNavTree == false) {
                    __doPostBack("UpdatePanel1", "Refresh" + unitSeparator + "0" + pathValueSeparator + selectedId); //we just pass zero as the new recordId but it has to be greater than zero to be replaced with the actual recordId.
                }
                else {
                    d = document.getElementById('scrollingFrame');
                    if (currentRoot == "Document" && MDIParentRecordId == "0" && MDIGetScreenInfo(selectedId, "SCREENTYPE") != "RESERVEWORKSHEET") { // MDI parent is a list node so it has to be removed from the memory as well, and refreshed later if User clicks on that.
                        MDIId = MDIGetScreenInfo(selectedId, "PARENTMDIID");
                    }
                    else {
                        MDIId = MDIGetScreenInfo(selectedId, "MDIID");
                    }

                    i = 0;
                    while (i < d.childNodes.length) {
                        child = d.childNodes[i];
                        MDIChildRecordId = MDIGetScreenInfo(child.id, "RECORDID")
                        if (child.nodeType === 1 && child.id.substr(0, MDIId.length) == MDIId && (MDIChildRecordId == 0 || MDIChildRecordId == recordId)) {
                            disposeScreen(child);
                            i++;
                        }
                        else {
                            i++;
                        }
                    }
                    __doPostBack("UpdatePanel1", "Remove" + pathValueSeparator + selectedId);
                }
            }
            else if (selectedScreen.name == "WipedOut") {
                selectedScreen.name = getTimeStamp();

                d = document.getElementById('scrollingFrame');

                MDIId = MDIGetScreenInfo(selectedId, "PARENTMDIID");

                i = 0;
                if (MDIGetScreenInfo(selectedId, "SCREENTYPE") == "RESERVEWORKSHEET") {
                    while (i < d.childNodes.length) {
                        child = d.childNodes[i];
                        MDIChildRecordId = MDIGetScreenInfo(child.id, "RECORDID")
                        if (child.nodeType === 1 && child.id.substr(0, MDIId.length) == MDIId && MDIGetScreenInfo(child.id, "MDIID").length > MDIId.length && MDIChildRecordId == 0) {// we want to remove Reserve History list from memory, but not any of reservelisting and reserve history screens and Reserve Current(the node is replced with the history node in navTree but we still keep the iframe screen).
                            disposeScreen(child);
                            i++;
                        }
                        else {
                            i++;
                        }
                    }
                }
                else {
                    while (i < d.childNodes.length) {
                        child = d.childNodes[i];
                        if (child.nodeType === 1 && child.id.substr(0, MDIId.length) == MDIId) {
                            disposeScreen(child);
                            i++;
                        }
                        else {
                            i++;
                        }
                    }
                }
                __doPostBack("UpdatePanel1", "Replace" + unitSeparator + MDIGetScreenInfo(selectedId, "RECORDID") + pathValueSeparator + selectedId);

            }            
        }
        else if (selectedScreen.name == "Deleted" || selectedScreen.name == "WipedOut") {
            selectedScreen.name = getTimeStamp();
        }
    }
}

function pleaseWait(state) {
    var objPW = document.getElementById('pleaseWaitFrame');
    if (state == "start") {
        objPW.style.display = "block";
    }
    else { //stop
        objPW.style.display = "none";
    }
}

function closeScreen(closingId, closeAllChildrenInNavTree) {
    //hideAll();
    var d = document.getElementById('scrollingFrame');
    var stillClose = false;
    var closeChildScreensInClientMemory = false;
    var i = 0;

    var currentRoot = MDIGetScreenInfo(closingId, "ROOT");
    var MDIId = MDIGetScreenInfo(closingId, "MDIID");
    var screenType = MDIGetScreenInfo(closingId, "SCREENTYPE");
    var recordId = MDIGetScreenInfo(closingId, "RECORDID");
    var isClaimOrEvent = MDIGetScreenInfo(closingId, "IsClaimOrEvent");

    if (closeAllChildrenInNavTree || (currentRoot == "Document" && recordId != "0" && !isClaimOrEvent && screenType.toLowerCase() != "reservelisting"))
    {
        closeChildScreensInClientMemory = true;
    }

    if (MDICheckDirtyScreensUnder(closingId, closeChildScreensInClientMemory, false)) {
        stillClose = confirm(parent.CommonValidations.dirtyScreenCloseWarning);
        if (!stillClose)
            MDICheckDirtyScreensUnder(closingId, closeChildScreensInClientMemory, true);
    }
    else
        stillClose = true;

    if (stillClose) {
        i = 0;
        var numSearch = document.getElementById('searchScreens');

        if (closeChildScreensInClientMemory)
       {
            while (i < d.childNodes.length) {
                child = d.childNodes[i];
                if (child.nodeType === 1 && child.id.substr(0, MDIId.length) == MDIId) {
                    MDIForceCloseAnyPopUpUnder(child.id);
                    child.src = 'about:blank'; //this is to stop loading! Otherwise IE gets confused and we might get an error because "parent.MDIScreenLoaded();" can not be located.
                    disposeScreen(child);
                    i++;
                }
                else {
                    i++;
                }
            }
       }
       else {
            while (i < d.childNodes.length) {
                child = d.childNodes[i];
                if (child.nodeType === 1 && child.id == closingId) {
                    MDIForceCloseAnyPopUpUnder(closingId);
                    disposeScreen(child);
                    i = d.childNodes.length;
                }
                else {
                    i++;
                }
            }
        }

        numSearch.value = countSearchScreens();
        if (!closeAllChildrenInNavTree)
            __doPostBack('navTree', 'sDUMMY');
    }
    return stillClose;
}

function MDI(arg, action) {
    
    var selectedScreen = document.getElementById('selectedScreenId');
    //asharma326 MITS 34425 
    //objOuter = document.getElementById("navTreeOuter");
    //objMeter = document.getElementById("MDIMeter");
    
    //if (objMeter.style.width != objOuter.clientWidth) {
    //    objMeter.style.width = objOuter.clientWidth + "px";
    //    objMeter.title = parent.CommonValidations.MDIMeterTooltipText;
    //}
    if (arg != '') {
        pleaseWait("stop");
        var numSearch = document.getElementById('searchScreens');
        var num = document.getElementById('screens');
        var child;
        var d = document.getElementById('scrollingFrame');
        var doc = document.getElementById(arg);
        var doc2;
        var i;

        if (action == "Msg") {
            alert(arg);
        }
        else if (action == "ADDDRIVERI") {
            arg = arg + unitSeparator + unitSeparator + '?pagetypename=driver_insured'
            if (!sharedInfoDirty) {
                __doPostBack("UpdatePanel1", arg);
            }
        }
        else if (action == "ADDDRIVERO") {
            arg = arg + unitSeparator + unitSeparator + '?pagetypename=driver_other'
            if (!sharedInfoDirty) {
                __doPostBack("UpdatePanel1", arg);
            }
        }
        else if (action == "ADD") {
            var nodeId = MDIGetNodeId(arg);
            var isClaim = MDIGetScreenInfo(nodeId, "ISCLAIM");
            var isClaimOrEvent, eventMDIId, eventMDIId2;
            var sharedInfoDirty = false;
            if (isClaim) {
                eventMDIId = MDIGetScreenInfo(nodeId, "EVENTMDIID");
                for (i = 0; i < d.childNodes.length; i++) {
                    child = d.childNodes[i];
                    eventMDIId2 = MDIGetScreenInfo(child.id, "EVENTMDIID");
                    if (child.nodeType === 1 && eventMDIId2 == eventMDIId) {
                        if (child.name == "Dirty") {
                            isClaimOrEvent = MDIGetScreenInfo(child.id, "IsClaimOrEvent");
                            if (isClaimOrEvent) {
                                sharedInfoDirty = true;
                                alert(parent.CommonValidations.sharedInfoWarning);
                                break;
                            }
                        }
                    }
                }
            }
            if (!sharedInfoDirty) {
                __doPostBack("UpdatePanel1", arg);
            }
        }
        else if (action == "CLP") {
            __doPostBack("UpdatePanel1", "Collapse" + pathValueSeparator + arg);
        }
        else if (action == "RMV") {
            var splitItems = arg.split(pathValueSeparator);
            var closingId = splitItems[splitItems.length - 1];
            if (closeScreen(closingId, true)) {
                __doPostBack("UpdatePanel1", "Remove" + pathValueSeparator + arg);
            }
        }
        else if (arg == "navTree") {
            action = nodeSelectionIndicator + action.replace(new RegExp(pathValueSeparator, "g"), nodeActionSeparator); //replace all
            __doPostBack('navTree', action);
        }
        //Added by Amitosh for policy interface mits 25163
        else if (action == "TAG") {

            document.getElementById('taggedPolicyId').value = arg;
            return false;
        }
        //End Amitosh

        else if (doc == null) {
            doc2 = document.getElementById(action);
            if (doc2 != null) {
                doc2.id = arg;
                doc2.name = getTimeStamp();
                document.getElementById('selectedScreenId').value = arg;
            }
            else {
                hideAll();
                var objcloseButton = document.getElementById('closeButton');
                objcloseButton.style.display = "block";
                var objreloadButton = document.getElementById('reloadButton');
                objreloadButton.style.display = "block";
                var objBreadcrumb = document.getElementById('breadcrumb');
                objBreadcrumb.style.display = "block";
                var blankScreen = getBlankScreenFromPool();
                if (blankScreen != null) {
                    blankScreen.setAttribute('id', arg);

                    blankScreen.style.display = 'block';

                    blankScreen.src = action;

                    pleaseWait("start");

                    selectedScreen.value = arg;

                    //num.value = countScreens();
                    numSearch.value = countSearchScreens();
                }
                else {
                    __doPostBack('navTree', 'sDUMMY');
                }
            }
        }
        else if (doc.style.display == 'none') {
            hideAll();
            var objcloseButton = document.getElementById('closeButton');
            objcloseButton.style.display = "block";
            if (arg.indexOf(groupSeparator) > 0) {// no reload button for search result screens!
                var objreloadButton = document.getElementById('reloadButton');
                objreloadButton.style.display = "block";
            }
            var objBreadcrumb = document.getElementById('breadcrumb');
            objBreadcrumb.style.display = "block";
            doc.style.display = 'block';
            selectedScreen.value = arg;
        }
    }
    else {
        if (selectedScreen.value.indexOf(groupSeparator) == -1) {// no reload button for search result screens!
            var objreloadButton = document.getElementById('reloadButton');
            objreloadButton.style.display = "none";
        }
    }
    //showListOfScreens();
    if (win != null && typeof win.setFocus === "function") {
        win.setFocus();
    } 
}
//Sangeet - MITS 25163: Added method to find the object of any open window by passing its MDI screenId
function GetWindowObjectByMDIId(WindowMDIId) {
    var MDIId;
    var child;
    var d = document.getElementById('scrollingFrame');
    for (i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        MDIId = MDIGetScreenInfo(child.id, "MDIID");
        if (child.nodeType === 1 && MDIId == WindowMDIId) {
            return child.contentWindow;
        }
    }
    return null;
}

function ShowExistingScreenByMDIId(WindowMDIId) {
    var MDIId;
    var child;
    var d = document.getElementById('scrollingFrame');
    for (i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        MDIId = MDIGetScreenInfo(child.id, "MDIID");
        if (child.nodeType === 1 && MDIId == WindowMDIId) {
            __doPostBack("UpdatePanel1", child.id);
            break;
        }
    }
}
//End: Sangeet

//bpaskova JIRA 9419 start
function UnloadClaimOnNewReserve() {
    var selectedId = document.getElementById('selectedScreenId').value;
    var screenType = MDIGetScreenInfo(selectedId, "ScreenType");
    var isClaimReserveListing = MDIGetScreenInfo(selectedId, "IsClaimReserves");
    if (isClaimReserveListing) {
        var d = document.getElementById('scrollingFrame');
        var claimMDIId = MDIGetScreenInfo(selectedId, "ClaimMDIId");
        for (var i = 0; i < d.childNodes.length; i++) {
            child = d.childNodes[i];
            var claimMDIId2 = MDIGetScreenInfo(child.id, "ClaimMDIId");
            if (child.nodeType === 1 && claimMDIId2 == claimMDIId && child.id != selectedId) {// if the screen is a child of current claim and it is not the selected screen (because selected screen just got refreshed and we don't want to refresh it again)
                var isClaim = MDIGetScreenInfo(child.id, "IsClaim");
                if (isClaim && screenType != MDIGetScreenInfo(child.id, "ScreenType")) {
                    disposeScreen(child); // User clicks on corresponding node in navTree the screen is loaded again and refreshed.
                    __doPostBack("UpdatePanel1", selectedId);
                    break;
                }
            }
        }
    }
}
//bpaskova JIRA 9419 end
//bpaskova JIRA 10347 start
function GetRelatedClaimantTabId(selectedId, iClaimantRowId) {
    var claimMDIId = MDIGetScreenInfo(selectedId, "ClaimMDIId");
    return claimMDIId + unitSeparator + zeroNodeIdIndicator + "claimant" + unitSeparator + iClaimantRowId;
}

function MDIIsRelatedClaimantDirty(iClaimantRowId) {
    var isDirty = false;
    var selectedId = document.getElementById('selectedScreenId').value;
    var tagetID = GetRelatedClaimantTabId(selectedId, iClaimantRowId);
    var d = document.getElementById('scrollingFrame');
    for (i = 0; i < d.childNodes.length; i++) {
        child = d.childNodes[i];
        var MDIId = MDIGetScreenInfo(child.id, "MDIID");
        if (child.nodeType === 1 && MDIId == tagetID && child.id != selectedId) {
            if (child.name == "Dirty") {
                isDirty = true;
            }
            break;
        }
    }
    return isDirty;
}
function UnloadClaimantOnNewReserve(iClaimantRowId) {

    var selectedId = document.getElementById('selectedScreenId').value;
    var isClaimReserveListing = MDIGetScreenInfo(selectedId, "IsClaimReserves");
    if (isClaimReserveListing) {
        var screenType = MDIGetScreenInfo(selectedId, "ScreenType");
        var d = document.getElementById('scrollingFrame');
        var tagetID = GetRelatedClaimantTabId(selectedId, iClaimantRowId);
        for (var i = 0; i < d.childNodes.length; i++) {
            child = d.childNodes[i];
            var MDIId2 = MDIGetScreenInfo(child.id, "MDIId");
            if (child.nodeType === 1 && MDIId2 == tagetID && child.id != selectedId) {// if the screen is a child of current claim and it is not the selected screen (because selected screen just got refreshed and we don't want to refresh it again)
                if (screenType != MDIGetScreenInfo(child.id, "ScreenType")) {
                    disposeScreen(child); // User clicks on corresponding node in navTree the screen is loaded again and refreshed.
                    __doPostBack("UpdatePanel1", selectedId);
                    break;
                }
            }
        }
    }
}
//bpaskova JIRA 10347 end
