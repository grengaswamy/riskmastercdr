﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using Riskmaster.BusinessAdaptor;
using System.Collections.Generic;
using Riskmaster.MDI.App_Code;
using Riskmaster.Security.Encryption;
using Riskmaster.RMXResourceManager;
using System.Xml;

namespace Riskmaster.MDI
{
    public partial class _Default : System.Web.UI.Page
    {
        private List<string> landingArgs = null;
        private static Dictionary<string, bool> dPersonInv = null;
        
        // akaushik5 Added for RMA-10160 Starts 
        /// <summary>
        /// Gets the person involved configuration.
        /// </summary>
        /// <value>
        /// The person involved configuration.
        /// </value>
        private static Dictionary<string, bool> PersonInvolvedConfig
        {
            get
            {
                if (object.ReferenceEquals(_Default.dPersonInv, null))
                {
                    _Default.GetPersonInvolvedSetting();
                }
                return _Default.dPersonInv;
            }
        }
        // akaushik5 Added for RMA-10160 Ends

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.landingArgs = new List<string>();
                MDISetting clientSettings = new MDISetting(ref MDIMenu, ref landingArgs);
                allowedSearchScreens.Value = clientSettings.AllowedSearchScreens;
                allowedTotalScreens.Value = clientSettings.AllowedTotalScreens;
                //Ashish Ahuja Mits 33698
                //relativePath.Value = clientSettings.RelativePath;
                relativePath.Value = RMCryptography.DecryptString(clientSettings.RelativePath);
                FDMLocalPath.Value = clientSettings.FDMLocalPath;
                hdnDocumenttitle.Value = clientSettings.DocumentTitle;
                hdnEventTitle.Value = clientSettings.EventTile;
                navTree.PathSeparator = (char)30;
                navTree.ExpandImageToolTip = string.Empty;
                navTree.CollapseImageToolTip = string.Empty;
                navTree.Nodes.Clear();
                navTree.Nodes.Add(new TreeNode("", "DUMMY"));
                //Deb ML changes :register the scripts at the RMXportal default page
                //string sResources = JavaScriptResourceHandler.GlobalProcessRequest(this.Context, "CommonValidations");
                //ClientScript.RegisterStartupScript(this.GetType(), "ResourcesScript", sResources, true);
                //Deb ML changes
                // akaushik5 Commneted for RMA-10160 Starts 
                //this.GetPersonInvolvedSetting();//Added by gbindra WWIG Gap # 30 MITS # 35365 08/26/14
                // akaushik5 Commneted for RMA-10160 Ends 
            }
            if (Session["IsBOB"] != null)
                isBOB.Value = (Session["IsBOB"]).ToString().ToLower();
        }
        //Deb : MDI Menu hovering issue fixed for IE9
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (PageHelper.GetClientBrowserVersion() == "9.0")
            {
                Page.Header.Controls.AddAt(0, new HtmlMeta { HttpEquiv = "X-UA-Compatible", Content = "IE=EmulateIE8" });
            }
            //Deb: Enhance Rendering is required to be True always for all browsers
            //igupta3 Mits# 33301 starts
            //if (!Request.Browser.Type.Contains("IE"))
            //{
            //    MDIMenu.EnhancedRendering = false;
            //}
            //igupta3 Mits# 33301 ends
                
        }
        //Deb : MDI Menu hovering issue fixed for IE9
        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {
            string script = string.Empty, sPortalLandingParams = string.Empty ;

            if (this.landingArgs != null && this.landingArgs.Count > 0)
            {
                // MITS 26834 ; Debabrata Biswas
                // Get the portal landing Arguments and over ride any PV default landing page
                // Merge of Mits 26834:Amitosh
                sPortalLandingParams = getPortalLandingPage();

                if (string.IsNullOrEmpty(sPortalLandingParams))
                {
                for (int i = 0; i < landingArgs.Count; i++)
                {
                    script = DoNavTreeProcess(landingArgs[i]);
                    }
                }
                else
                {
                    script = sPortalLandingParams;
                }
                //End Amitosh
            }
            else
            {
                script = DoNavTreeProcess(Request.Params.Get("__EVENTARGUMENT"));
            }
            ScriptManager.RegisterStartupScript(this.UpdatePanel1, typeof(string), "handleMDI", script, true);
        }


        protected string DoNavTreeProcess(string navTreeArg)
        {
            try
            {
                MDINavigationTreeProcess navTreeProcess = new MDINavigationTreeProcess(relativePath.Value, FDMLocalPath.Value, previousNodeValuePath.Value, previousScript.Value, searchResultRecordId.Value, allowedSearchScreens.Value, ReachedMaxSearchScreens.Value, LastNumericId.Value, litBreadcrumb.Text, currentClaimMDIId.Value);
            	/*Added by gbindra WWIG Gap # 30 MITS # 35365 08/26/14*/
            	//navTree = navTreeProcess.UpdateNavTree(navTree, navTreeArg, hdnDocumenttitle.Value, hdnEventTitle.Value);
                // akaushik5 Changed for RMA-10160 Starts 
                //navTree = navTreeProcess.UpdateNavTree(navTree, navTreeArg, hdnDocumenttitle.Value, hdnEventTitle.Value, dPersonInv);
                navTree = navTreeProcess.UpdateNavTree(navTree, navTreeArg, hdnDocumenttitle.Value, hdnEventTitle.Value, _Default.PersonInvolvedConfig);
                // akaushik5 Changed for RMA-10160 Ends 
            	/*Added by gbindra WWIG Gap # 30 MITS # 35365 08/26/14*/
                searchResultRecordId.Value = navTreeProcess.SearchResultRecordId.ToString();
                previousNodeValuePath.Value = navTreeProcess.PreviousNodeValuePath;
                nextSearchResult.Value = navTreeProcess.NextSearchResultValue;
                ReachedMaxSearchScreens.Value = navTreeProcess.ReachedMaxSearchScreens;
                LastNumericId.Value = navTreeProcess.LastNumericId.ToString();
                litBreadcrumb.Text = navTreeProcess.Breadcrumb;
                currentClaimMDIId.Value = navTreeProcess.CurrentClaimMDIId;
                //previousScript.Value = navTreeProcess.Script;
                return navTreeProcess.Script;
            }
            catch (Exception ex)
            {
                //srajindersin MITS 34415 1/2/2014
                return "alert(" +ex.Message.ToString() + ")";
            }
        }

        /// <summary>
        /// Added by gbindra WWIG Gap # 30 MITS # 35365 08/26/14
        /// </summary>
        // akaushik5 Changed for RMA-10160 Starts 
        //private void GetPersonInvolvedSetting()
        private static void GetPersonInvolvedSetting()
        // akaushik5 Changed for RMA-10160 Ends
        {
            XElement messageElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;
            XmlNode menuConfigNode = null;
            Dictionary<string, bool> dPersonCheck = null;

            try
            {
                InitializePersonInvolvedDictionary();

                messageElement = XElement.Parse(@"<Message>
                                                   <Authorization></Authorization>
                                                   <Call>
                                                      <Function>PersonInvMenuConfigAdaptor.GetPersonInvMenuConfig</Function>
                                                   </Call>
                                                   <Document>
                                                      <PersonInvMenuConfig>
                                                         <AddExistingEmployee></AddExistingEmployee> 
                                                         <AddNewEmployee></AddNewEmployee> 
                                                         <AddExistingMedicalStaff></AddExistingMedicalStaff> 
                                                         <AddNewMedicalStaff></AddNewMedicalStaff>
                                                         <AddExistingOtherPerson></AddExistingOtherPerson>
                                                         <AddNewOtherPerson></AddNewOtherPerson> 
                                                         <AddExistingDriver></AddExistingDriver> 
                                                         <AddNewDriver></AddNewDriver> 
                                                         <AddExistingPatient></AddExistingPatient> 
                                                         <AddNewPatient></AddNewPatient> 
                                                         <AddExistingPhysician></AddExistingPhysician> 
                                                         <AddNewPhysician></AddNewPhysician> 
                                                         <AddExistingWitness></AddExistingWitness> 
                                                         <AddNewWitness></AddNewWitness>                           
                                                      </PersonInvMenuConfig>
                                                   </Document>
                                                </Message>");

                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);

                dPersonCheck = new Dictionary<string, bool>();
                //set obtained values of Config
                menuConfigNode = resultDoc.SelectSingleNode("//Document/PersonInvMenuConfig");

                if (menuConfigNode != null)
                {
                    foreach (string sKey in dPersonInv.Keys)
                    {
                        //null check handled by && as it searches for first false
                        dPersonCheck.Add(sKey, ((menuConfigNode.SelectSingleNode(sKey) != null &&
                                            (string.Compare(menuConfigNode.SelectSingleNode(sKey).InnerText, "True", true) == 0))) ?
                                                    true : false);
                    }
                }
                dPersonInv = new Dictionary<string,bool>(dPersonCheck);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (dPersonCheck != null)
                {
                    dPersonCheck.Clear();
                    dPersonCheck = null;
                }
            }
        }

        /// <summary>
        /// Added by gbindra WWIG Gap # 30 MITS # 35365 08/26/14
        /// </summary>
        // akaushik5 Changed for RMA-10160 Starts 
        //private void InitializePersonInvolvedDictionary()
        private static void InitializePersonInvolvedDictionary()
        // akaushik5 Changed for RMA-10160 Ends
        {
            dPersonInv = new Dictionary<string, bool>();
            dPersonInv.Add("AddExistingEmployee", false);
            dPersonInv.Add("AddNewEmployee", false);
            dPersonInv.Add("AddExistingMedicalStaff", false);
            dPersonInv.Add("AddNewMedicalStaff", false);
            dPersonInv.Add("AddExistingOtherPerson", false);
            dPersonInv.Add("AddNewOtherPerson", false);
            dPersonInv.Add("AddExistingDriver", false);
            dPersonInv.Add("AddNewDriver", false);
            dPersonInv.Add("AddExistingPatient", false);
            dPersonInv.Add("AddNewPatient", false);
            dPersonInv.Add("AddExistingPhysician", false);
            dPersonInv.Add("AddNewPhysician", false);
            dPersonInv.Add("AddExistingWitness", false);
            dPersonInv.Add("AddNewWitness", false);
        }

        protected void UpdatePanel2_Load(object sender, EventArgs e)
        {
            //RMA-13498
            if (hdnIsTreeHover.Value == "true")
                breadcrumb.Style.Value = "left:16.8%";
            else
                breadcrumb.Style.Value = "left:1.8%";
        }

        void Application_Error(object sender, EventArgs e)
        {
            //There should be some checking done so that not all the errors 
            //are cleared
            Context.ClearError();
        }
        /// <summary>
        /// Author: Debabrata Biswas
        /// MITS 26834
        /// Date: 12/02/2011
        /// </summary>
        /// <returns>sScript: Returns the JavaScript function to be called for initial landing Page Arguments passed from portal</returns>
        string getPortalLandingPage()
        {
            string sFDMName = string.Empty, sClaimID = string.Empty, sScript = string.Empty;
            HttpCookie objUserCookie = HttpContext.Current.Request.Cookies.Get("UserInfoCookie");

            if (objUserCookie != null)
            {
                if ((objUserCookie["PortalFDMName"] != null) && (objUserCookie["PortalClaimID"] != null))
                {
                    sFDMName = AppHelper.ReadCookieValue("PortalFDMName");
                    sClaimID = AppHelper.ReadCookieValue("PortalClaimID");

                    sFDMName = RMCryptography.DecryptString(sFDMName);
                    sClaimID = RMCryptography.DecryptString(sClaimID);

                    if (!string.IsNullOrEmpty(sFDMName) && !string.IsNullOrEmpty(sClaimID))
                    {
                        sScript = "MDIShowScreen('" + sClaimID + "','" + sFDMName + "');";
                    }
                }
            }
            return sScript;
        }
    }
}