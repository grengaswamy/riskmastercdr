﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA   | Programmer | Description                                            *
 **********************************************************************************************
 * 09/02/2014 | RMA-88  | achouhan3   | CSC SSO Implementation
 * 11/25/2014 | RMA-345 | achouhan3   | Angular Framwework ANG_Grid Implementation
 * 02/13/2015 | RMA-6405| vchouhan6   | Converting Claim Activity log grid to Angular Framework-NG Grid
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Xml.Linq;
using System.Web;
using System.Web.Security;
using System.Collections.Specialized;
using Riskmaster.VPP;
using System.Text;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Net;
using System.Net.Security;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI
{
    public class GlobalApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Event Handler which occurs at the start of the Application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Start(object sender, EventArgs e)
        {
            //Create VirtualPathProvier help object
            VPProvider vpp = new VPProvider();
            HostingEnvironment.RegisterVirtualPathProvider(vpp);
            
            //Create Page Cache helper object. MITS 24898
            PageOutputCacheHelper oPageOutputCacheHelper = new PageOutputCacheHelper();
            Application["PageOutpuCacheHelper"] = oPageOutputCacheHelper;

            //psarin2:secure start
             // Get the application configuration file.
           System.Configuration.Configuration config =System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/");
           System.Web.Configuration.OutputCacheSection cacheSection = (System.Web.Configuration.OutputCacheSection)config.GetSection("system.web/caching/outputCache");
           Application["IsCacheEnabled"] = cacheSection.EnableOutputCache.ToString().ToLower();
            //psarin2:secure end
           //Application["BaseServiceUrl"] = System.Configuration.ConfigurationManager.AppSettings["BaseServiceUrl"].ToString();
           //Riskmaster.Cache.CacheCommonFunctions.AddValue2Cache<string>("BaseServiceUrl", 0, Application["BaseServiceUrl"].ToString().EndsWith("/") ? Application["BaseServiceUrl"].ToString() : Application["BaseServiceUrl"].ToString() + "/");
           Application["TimeOut"] = System.Configuration.ConfigurationManager.AppSettings["TimeOut"].ToString();
           Riskmaster.Cache.CacheCommonFunctions.AddValue2Cache<int>("TimeOut", 0, int.Parse(Application["TimeOut"].ToString()));
        }
        //psarin2:secure start
        protected void Application_EndRequest(Object sender, EventArgs e)
        {
            // should work now

            if (Application["IsCacheEnabled"].ToString()=="false")
           {
               Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
               Response.AddHeader("Pragma", "no-cache");
               Response.Expires = -1;
           }
        }
        //psarin2:secure end

        /// <summary>
        /// Globally handles any application errors which may occur in the UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>retrieves the last exception thrown by the ASP.Net runtime
        /// and logs the error to the Error Log</remarks>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError().GetBaseException();

            string sHeader = HeaderTemplate();
            Response.Write(sHeader);

            //Display the error message information
            //Show error message as a title
            Response.Write(String.Format("<h1>Error message: {0}</h1>", ErrorHelper.UpdateErrorMessage(ex.Message)));
            // Show error details
            Response.Write("<h2>Error details:</h2>");
            Response.Write(String.Format("<p>Source:{0}</p>", ex.Source));
            Response.Write(String.Format("<p>Error in: {0}</p>", HttpUtility.UrlEncode(Request.Url.ToString())));//Deb:Pen Testing Encoded
            //Deb:Pen Testing issue: Show Trace for local access only
            if (Request.IsLocal)
             Response.Write(String.Format("<p>Stack Trace:{0}</p>", ex.StackTrace));

            //Log the error messages
            ErrorHelper.logErrors(ex);

            //Clear the last error
            Server.ClearError();
        }//event: Application_Error()

        /// <summary>
        /// Event Handler which occurs after the AuthenticateRequest event
        /// which verifies that the Url which is being accessed has
        /// been granted appropriated privileges/permissions to be accessed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_AuthorizeRequest(object sender, EventArgs e)
        {
            //Prefix the file name with DSNID_ViewID_FileName for powerview pages, 
            //such as 2_1_Event.aspx
            string sFullOrigionalpath = Request.Url.ToString();
            string sPageName = AppHelper.GetPageName(sFullOrigionalpath);
            //Deb :Pen testing for XSS  
            if (HttpContext.Current.Request.QueryString.Count > 0)
            {
                int index = sFullOrigionalpath.IndexOf("//");//srajindersin MITS 35100 1/28/2014
                string sNewPath = sFullOrigionalpath.Substring(index + 2);
                index = sNewPath.IndexOf("/");
                sNewPath = sNewPath.Substring(index);
                //tmalhotra2 :mits 26586
                //Ankit Start : Worked on MITS-33597 (Quick look up not working for Notetype code on enhanced notes)
                //tanwar2 - condition commented. We need to remove extra braces for request coming for these files as well.
                if (Request.Path != "/RiskmasterUI/UI/Codes/QuickLookup.aspx" && Request.Path != "/RiskmasterUI/UI/Search/searchrender.aspx" && Request.Path != "/RiskmasterUI/UI/Codes/CodesList.aspx")
                {
                    Regex rxReplace = new Regex(@"([<>""%;()＜＞])");
                    sNewPath = rxReplace.Replace(sNewPath, "");
                    //PSARIN2:Corrected the code as it was giving errors for Telerik controls.
                    //Neha Context.Rewrite should not be used for Webresource.axd and scriptResource.axd
                    if (!sNewPath.Contains(".axd"))
                    {
                        Context.RewritePath(sNewPath);
                    }
                }
            }
            //Deb :Pen testing for XSS
            //Check if the page name has already been prefixed with dsn and view id
            Regex rx = new Regex(@"^\d+_-?\d+_[\w_-]+\.aspx");
            if (!rx.IsMatch(sPageName))
            {
                int iDsnID = 0;
                if (IsPowerViewPage(sPageName) || (sPageName.StartsWith("admintracking") && (string.Compare(sPageName, "admintrackinglist.aspx", true) != 0)))
                {
                    if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("DsnId")))
                    {
                        iDsnID = int.Parse(AppHelper.ReadCookieValue("DsnId"));
                    }

                    //In the future, the view id value should be from the session instead of from url
                    string sViewID = "0";
                    if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("ViewId")))
                    {
                        sViewID = AppHelper.ReadCookieValue("ViewId");
                    }
                    int iClientId = AppHelper.ClientId;
                    //http://servername:portnumber should be removed from the new url
                    int index = sFullOrigionalpath.LastIndexOf("//");
                    string sNewPath = sFullOrigionalpath.Substring(index + 2);
                    index = sNewPath.IndexOf("/");
                    sNewPath = sNewPath.Substring(index);

                    //replace olb page name with the new page name
                    string sNewPageName = string.Empty;

                    //rsolanki2(may 23,2010): extensibility updates retriving custom views 
                    string sCustomViewList = string.Empty;
                    if (sViewID.Trim().Equals("0"))
                    {
                        //if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("CustomViewNames")))
                        //{
                        sCustomViewList =
                            //Session["CustomViewNames"].ToString();
                                AppHelper.ReadCookieValue("CustomViewNames");
                        //}

                        if (!string.IsNullOrEmpty(sCustomViewList)
                            && sCustomViewList.Contains(string.Concat("|", sPageName.Replace(".aspx", ".xml"), "|"))
                            )
                        {
                            sNewPageName = String.Format("{0}_-1_{1}_{2}", iDsnID, iClientId, sPageName);
                        }
                        else
                        {
                            sNewPageName = String.Format("{0}_{1}_{2}_{3}", iDsnID, sViewID, iClientId, sPageName);
                        }
                    }
                    else
                    {
                        sNewPageName = String.Format("{0}_{1}_{2}_{3}", iDsnID, sViewID, iClientId, sPageName);
                    }

                    index = sNewPath.IndexOf(sPageName);
                    sNewPath = String.Format("{0}{1}{2}", sNewPath.Substring(0, index), sNewPageName, sNewPath.Substring(index + sPageName.Length));

                    Context.RewritePath(sNewPath);

                    //set value which will be used in the ControlAdaptor for form's action attribute
                    Context.Items["OriginalPageName"] = sPageName;
                }
            }
        }

        /// <summary>
        /// Config setting implementation to bypass certificate validation. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            bool useUnsecureSSL = false;

            Boolean.TryParse(ConfigurationManager.AppSettings["UseUnsecureSSL"] ?? "false", out useUnsecureSSL);

            if (useUnsecureSSL)
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate(object source, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                {
                        return true;
                };
            }
        }
        /// <summary>
        /// Entry points to the application should not require forms authentication. CustomLogin.aspx (and in some cases login.aspx too) is used by many portals to 
        /// provide direct authentication into Riskmaster and hence we need to skip forms authentication on it too. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_AuthenticateRequest(object sender, System.EventArgs e)
        {

            Uri ReqUrl = HttpContext.Current.Request.Url;
            string[] ReqSegment = ReqUrl.Segments;
            string RequestPage = ReqSegment[ReqSegment.Length - 1];

            // if the requested page is "SkippedFromAuthorization.aspx" , we skip the Authenticatation 
            // you should replace the "SkippedFromAuthorization.aspx" with the page name that you want to skip 
            if ((String.Compare(RequestPage, "customlogin.aspx", true) == 0) || (String.Compare(RequestPage, "login.aspx", true) == 0) || (String.Compare(RequestPage, "riskmaster.aspx", true) == 0))
            {
                HttpContext.Current.SkipAuthorization = true;
            }
            // RMA-888 CSC SSO Implementation Starts
            bool m_bUseSSO = false;
            Boolean.TryParse(ConfigurationManager.AppSettings["UseSSO"] ?? "false", out m_bUseSSO);
            if (m_bUseSSO)
            {
                HttpContext.Current.SkipAuthorization = true;
            }
            // RMA-888 CSC SSO Implementation Ends

        }

        protected void FormsAuthentication_OnAuthenticate(Object sender, FormsAuthenticationEventArgs e)
        {
            //get the cookie object from the http request header 
            HttpCookie authCookie = e.Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (null == authCookie)
            {
                return;
            }

            FormsAuthenticationTicket authTicket = null;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch (Exception ex)
            {
                return;
            }
            //if ticket is expired or not found terminate the execution and return to the login page to authenticate 
            if ((null == authTicket) || authTicket.Expired)
            {
                return;
            }

            //to store the renewed ticket 
            FormsAuthenticationTicket authTicketNew = authTicket;

            if (FormsAuthentication.SlidingExpiration)
            {
                authTicketNew = FormsAuthentication.RenewTicketIfOld(authTicket);
            }

            //If the ticket get renewed, then re-issue the auth cookie
            if (authTicketNew != authTicket)
            {
                string encryptedTicket = FormsAuthentication.Encrypt(authTicketNew);
                authCookie.Value = encryptedTicket;
                authCookie.Expires = authTicketNew.Expiration;
                authCookie.HttpOnly = true;
                Response.Cookies.Add(authCookie);

                HttpCookie userCookie = HttpContext.Current.Request.Cookies.Get("UserInfoCookie");
                if (userCookie != null)
                {
                    userCookie.HttpOnly = true;
                    userCookie.Expires = DateTime.Now.AddMinutes(AppHelper.GetAuthenticationTimeout());
                    Response.Cookies.Add(userCookie);
                }
            }
            //psarin2:secure start
            string aspnet_cookie="ASP.NET_SessionId";
            if (Request.IsSecureConnection)
            {
                if (Response.Cookies[aspnet_cookie] != null)
                {
                    Response.Cookies[aspnet_cookie].Secure = true;
                    Response.Cookies[aspnet_cookie].HttpOnly = true;
                }
                else if (Response.Cookies[aspnet_cookie.ToLower()] != null)
                {
                    Response.Cookies[aspnet_cookie.ToLower()].Secure = true;
                    Response.Cookies[aspnet_cookie.ToLower()].HttpOnly = true;
                }
            }
            // srajindersin MITS 30223 - Session variables not able to persist data in non-SSL environment because of this code 
            //else
            //{
            //    if (Response.Cookies[aspnet_cookie] != null)
            //    {
            //        Response.Cookies[aspnet_cookie].HttpOnly = true;
            //    }
            //    else if (Response.Cookies[aspnet_cookie.ToLower()] != null)
            //    {
            //        Response.Cookies[aspnet_cookie.ToLower()].HttpOnly = true;
            //    }
            //}
            //psarin2:secure end
        }


        /// <summary>
        /// Set VaryByCustom for OutputCache. MITS 24898
        /// </summary>
        /// <param name="context"></param>
        /// <param name="custom"></param>
        /// <returns></returns>
        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            string sValue = string.Empty;
            //Deb: Remove ouput cached pages where caching is not needed
            if (context.Request.QueryString["Cache"] == "0")
            {
                HttpResponse.RemoveOutputCacheItem(context.Request.Path);
            }
            //Deb
            PageOutputCacheHelper oPageOutputCacheHelper = (PageOutputCacheHelper)Application["PageOutpuCacheHelper"];
            if (oPageOutputCacheHelper != null)
                sValue = oPageOutputCacheHelper.GetCustomParamValue(context, custom);
            else
                sValue = base.GetVaryByCustomString(context, custom);

            return sValue;
        }

        /// <summary>
        /// Get the list of pages which will be powerview. The list will be used to determine if
        /// the page should be retrieved from the virtual source.
        /// </summary>
        /// <param name="sPageName">string containing the Page Name</param>
        private bool IsPowerViewPage(string sPageName)
        {
            //IEnumerable<string> oPageList = null;
            //if (Application["PowerviewPageList"] == null)
            //{
            //    string sPowerviewPageListFile = ConfigurationManager.AppSettings["PowerviewPageList"];
            //    string sPath = String.Format(@"{0}\App_Data\{1}", Server.MapPath("~"), sPowerviewPageListFile);

            //    XElement pageListElement = XElement.Load(sPath);

            //    oPageList = from el in pageListElement.Descendants("Page")
            //                select el.Attribute("name").Value.ToLower();

            //    Application["PowerviewPageList"] = oPageList;
            //}
            //else
            //    oPageList = (IEnumerable<string>)Application["PowerviewPageList"];

            //return oPageList.Contains(sPageName.ToLower());

            //smishra25:Performance changes, the PowerViewPageList XMl is not customizable. So no need to keep a separate XMl
            //file for this. This can be maintained internally.
            if (PowerViewPages().IndexOf(sPageName.ToLower()) > -1 && (!String.IsNullOrEmpty(sPageName)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string HeaderTemplate()
        {
            StringBuilder sb = new StringBuilder();

            //css style
            sb.AppendLine(@"<style type='text/css'>
		                        body{
                                    background:#ffffff;
                                    color:#000000;
                                    font-family:Arial, Helvetica, sans-serif;
                                }
		                        h1{
                                    font-size:120%;
                                    color:#EE2827;
                                    font-weight:normal;
                                }
		                        h2{
                                    font-size:110%;
                                    font-weight:normal;
                                }
		                        .hidden{
                                    display:none;
                                }
		                        .shown{
                                    display:block;
                                }
		                        .trigger{
                                    background:#7BA7C7;
                                }
		                        .open{
                                    background:#7BA7C7;
                                }
		                        .hover{
                                    background:#7BA7C7;
                                }
	                        </style>");

            //javascript
            sb.AppendLine(@"<script type='text/javascript'>
		                        function collapse()
		                        {
			                        if(!document.createTextNode){return;}
			                        var p=document.createElement('p');
			                        p.appendChild(document.createTextNode('Click on the headlines to collapse and expand the section...'));
			                        var heads=document.getElementsByTagName('h2');
			                        for(var i=0;i<heads.length;i++)
			                        {
					                        var tohide=heads[i].nextSibling;
					                        while(tohide.nodeType!=1)
					                        {
						                        tohide=tohide.nextSibling;
					                        }
					                        cssjs('add',tohide,'hidden')
					                        cssjs('add',heads[i],'trigger')
					                        heads[i].tohide=tohide;
					                        heads[i].onmouseover=function()
					                        {
						                        cssjs('add',this,'hover');
					                        }
					                        heads[i].onmouseout=function()
					                        {
						                        cssjs('remove',this,'hover');
					                        }
					                        heads[i].onclick=function()
					                        {
						                        if(cssjs('check',this.tohide,'hidden'))
						                        {
							                        cssjs('swap',this,'trigger','open');			
							                        cssjs('swap',this.tohide,'hidden','shown');			
						                        } else {
							                        cssjs('swap',this,'open','trigger');			
							                        cssjs('swap',this.tohide,'shown','hidden');			
						                        }
					                        }
			                                document.body.insertBefore(p,document.getElementsByTagName('h2')[0]);
			                        }

			                        function cssjs(a,o,c1,c2)
			                        {
				                        switch (a){
					                        case 'swap':
						                        o.className=!cssjs('check',o,c1)?o.className.replace(c2,c1):o.className.replace(c1,c2);
					                        break;
					                        case 'add':
						                        if(!cssjs('check',o,c1)){o.className+=o.className?' '+c1:c1;}
					                        break;
					                        case 'remove':
						                        var rep=o.className.match(' '+c1)?' '+c1:c1;
						                        o.className=o.className.replace(rep,'');
					                        break;
					                        case 'check':
						                        return new RegExp('\\b'+c1+'\\b').test(o.className)
					                        break;
				                        }
			                        }
		                        }

		                        window.onload=collapse;
	                        </script>");

            return sb.ToString();
        } // method: HeaderTemplate
        /// <summary>
        /// Returns a string that contains list of all power view pages
        /// </summary>
        /// <returns></returns>
        private string PowerViewPages()
        {
            StringBuilder sb = new StringBuilder();

            //css style
            sb.AppendLine(@"adjuster.aspx
                        adjusterdatedtext.aspx
                        admintracking.aspx
                        autoclaimchecks.aspx
                        bankaccount.aspx
                        bankaccountsub.aspx
                        brs.aspx
                        casemgrnotes.aspx
                        claimant.aspx
                        claimdi.aspx
                        claimgc.aspx
                        claimpc.aspx
                        claims.aspx
                        claimva.aspx
                        claimwc.aspx
                        cmxaccommodation.aspx
                        cmxcmgrhist.aspx
                        cmxmedmgtsavings.aspx
                        cmxtreatmentpln.aspx
                        cmxvocrehab.aspx
                        concomitant.aspx
                        defendant.aspx
                        dependent.aspx
                        deposit.aspx
                        employee.aspx
                        entity.aspx
                        entityexposure.aspx
                        entitymaint.aspx
                        event.aspx
                        eventdatedtext.aspx
                        eventintervention.aspx
                        expert.aspx
                        fallinfo.aspx
                        jurisdictionlicensecodes.aspx
                        leave.aspx
                        leaveplan.aspx
                        litigation.aspx
                        medwatch.aspx
                        medwatchtest.aspx
                        modify.xsl
                        nonocc.aspx
                        osha.aspx
                        patient.aspx
                        patientprocedure.aspx
                        people.aspx
                        physician.aspx
                        physiciancertification.aspx
                        physicianeducation.aspx
                        physicianprevhospital.aspx
                        physicianprivilege.aspx
                        picertification.aspx
                        pidependent.aspx
                        pieducation.aspx
                        piemployee.aspx
                        piinjury.aspx
                        pimedstaff.aspx
                        pimedstaffcertification.aspx
                        pimedstaffprivilege.aspx
                        piother.aspx
                        pipatient.aspx
                        piphysician.aspx
                        piprevhospital.aspx
                        piprivilege.aspx
                        piprocedure.aspx
                        piqpatient.aspx
                        piqprocedure.aspx
                        pirestriction.aspx
                        piwitness.aspx
                        piworkloss.aspx
                        plan.aspx
                        planclasses.aspx
                        policy.aspx
                        policybilling.aspx
                        policycoverage.aspx
                        policyenh.aspx
                        policyinsurer.aspx
                        policymco.aspx
                        policymcoenh.aspx
                        providercontract.aspx
                        qmcommdeptreview.aspx
                        qminitialreview.aspx
                        qmphysadvreview.aspx
                        qmqualmgrreview.aspx
                        sentinel.aspx
                        staff.aspx
                        staffcertification.aspx
                        staffprivilege.aspx
                        unit.aspx
                        vehicle.aspx
                        vehicleinspections.aspx
                        leavedetail.aspx
                        entityxoperatingas.aspx
                        entityxcontactinfo.aspx
                        orghierarchymaint.aspx
                        entityexposure.aspx
                        violation.aspx
                        coverages.aspx
                        exposure.aspx
                        clientlimits.aspx
                        funds.aspx
                        split.aspx
                        policyreinsurer.aspx
                        futurepayments.aspx
                        thirdpartypayments.aspx
                        entityxaddresses.aspx
                        propertyunit.aspx
                        policyenhal.aspx
                        policyenhgl.aspx
                        policyenhpc.aspx
                        policyenhwc.aspx
                        exposureal.aspx
                        exposuregl.aspx
                        exposurepc.aspx
                        exposurewc.aspx
                        subrogation.aspx
                        demandoffer.aspx  
						arbitration.aspx  
                        propertyloss.aspx     
                        salvage.aspx
                        siteloss.aspx   
                        siteunit.aspx     
                        liabilityloss.aspx    
                        casemanagement.aspx
                        combinedpayment.aspx   
                        withholding.aspx//smishra54: R8 Withholding Enhancement, MITS 26019
                           bankinginfo.aspx
                        catastrophe.aspx
                         pidriver.aspx
                        driver.aspx
                        vssassignment.aspx
                        entityxentityidtype.aspx // MITS #34276 New FDM page for Entity ID Type and Entity ID Number
                        AsyncSearchResult.aspx // JIRA-345 to add new search result page for Angular grid
                        AsyncClaimActLog.aspx   //JIRA-6405 converting Claim Activity log grid to Angular grid
                        reservecurrent.aspx
                        creatediary.aspx
                        attachdiary.aspx
                        diarydetails.aspx
                        deductibledetail.aspx
                        address.aspx //RMA-8753 nshah28 "

                );
            

            return sb.ToString();
        } 
    }
}
