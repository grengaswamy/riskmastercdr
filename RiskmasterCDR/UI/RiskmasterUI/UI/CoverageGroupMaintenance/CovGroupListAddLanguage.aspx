﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CovGroupListAddLanguage.aspx.cs" Inherits="Riskmaster.UI.UI.CoverageGroupMaintenance.CovGroupListAddLanguage" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <title></title>

     
    <script type="text/javascript">
        ///Code from:
        ///http ://www.telerik.com/community/code-library/aspnet-ajax/grid/single-radiobutton-check-at-a-time-with-row-selection.aspx
        var m_CodeId = 0;
        var m_LangCode = 0;
        function selectSingleRadio(objRadioButton, grdName, groupId,langCode) {
            var i, obj, pageElements;

            if (navigator.userAgent.indexOf("MSIE") != -1) {
                //IE browser  
                pageElements = document.all;
            }
            else if (navigator.userAgent.indexOf("Mozilla") != -1 || navigator.userAgent.indexOf("Opera") != -1) {
                //FireFox/Opera browser  
                pageElements = document.documentElement.getElementsByTagName("input");
            }
            for (i = 0; i < pageElements.length; i++) {
                obj = pageElements[i];

                if (obj.type == "radio") {
                    if (objRadioButton.id.substr(0, grdName.length) == grdName) {
                        if (objRadioButton.id == obj.id) {
                            obj.checked = true;
                        }
                        else {
                            obj.checked = false;
                        }
                    }
                }
            }
            m_CodeId = groupId;
            m_LangCode = langCode;
        }

        function selectCodeforEdit() {
            var objCtrl = document.getElementById("hdnCodeId");
            var objCtrlLang = document.getElementById("hdnLangCode");
            
//            var objTotalRows = document.getElementById("hdTotalRows");
//            if (objTotalRows != null && objTotalRows.value == "0")
//                return false;
            if (m_CodeId == 0 || m_LangCode == 0) {
                alert(CovGroupListAddLanguageValidations.SelectCode);
                return false;
            }
            if (objCtrl != null)
                objCtrl.value = m_CodeId;
            if (objCtrlLang != null)
                objCtrlLang.value = m_LangCode;
            return true;
        }
       
    </script>
       
</head>
<body onload="parent.MDIScreenLoaded();">
   <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <form id="frmCoverageGroupGrid" runat="server">
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">      
       <div class="toolBarButton" runat="server" id="div_new" xmlns="">
            <asp:ImageButton ID="cmdNew" ImageUrl="~/Images/tb_new_active.png"
                class="bold" ToolTip="<%$ Resources:cmdNewTip %>" runat="server" OnClick="cmdNew_Click" />
        </div>
        <div class="toolBarButton" runat="server" id="div_edit" xmlns="">
            <asp:ImageButton ID="cmdEdit" ImageUrl="~/Images/tb_edit_active.png" class="bold"
                ToolTip="<%$ Resources:cmdEditTip %>" OnClientClick = "return selectCodeforEdit();" runat="server" OnClick="cmdEdit_Click" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
            <asp:ImageButton ID="cmdDelete" ImageUrl="~/Images/tb_delete_active.png" class="bold"
                ToolTip="<%$ Resources:cmdDeleteTip %>" runat="server" OnClientClick = "return selectCodeforEdit();" OnClick="cmdDelete_Click"
                 />
         <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:btnCancelTip %>"
                        runat="server" OnClick="btnCancel_Click" />
        </div>

        </div>
    <div>
    <table width="100%" cellspacing="0" cellpadding="0" border="0"><tr>
    <td colspan="10" class="msgheader" bgcolor="#D5CDA4">
                    <asp:Label ID="lblCoverageGroupGridHeader" runat="server" Text="<%$ Resources:lblCoverageGroupGridHeader %>"></asp:Label>
                </td></tr>
    <tr><td>
    
    <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="lstDed">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="lstDed" />
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
    </telerik:RadAjaxManager>

    <telerik:RadGrid runat="server" ID="gvCoverageGroupGrid"
        AllowPaging="true" PageSize="15" Skin="Office2007" AllowFilteringByColumn="true" AllowSorting="true"
        AutoGenerateColumns="false" OnNeedDataSource="gvCoverageGroupGrid_NeedDataSource" OnItemDataBound="gvCoverageGroupGrid_ItemDataBound" >
            <SelectedItemStyle CssClass="SelectedItem"  />
            <ClientSettings AllowKeyboardNavigation="true">
                     <ClientEvents  />
                      <Scrolling SaveScrollPosition="true" AllowScroll="true" ScrollHeight="500px" />
             </ClientSettings>
             <MasterTableView Width="100%" ClientDataKeyNames="CovGroupId">
             <PagerStyle AlwaysVisible="true" Position="Top" mode="NextPrevAndNumeric"/>
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                 <NoRecordsTemplate>
                  <div>
                    There are no records to display.
                  </div>
                </NoRecordsTemplate>
                <Columns>
                   <telerik:GridTemplateColumn UniqueName="grdRadio" AllowFiltering="false">
                        <ItemTemplate>
                            <asp:RadioButton ID="gdRadio" runat="server" AutoPostBack="false" OnCheckedChanged="grdRadio_CheckedChanged" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn  HeaderText="<%$ Resources:lblCovGroupId %>" DataField="CovGroupId" UniqueName="CovGroupId"
                        SortExpression="CovGroupId" Visible="false">
                    </telerik:GridBoundColumn>       
                    <telerik:GridBoundColumn DataField="CovGroupCode" HeaderText="<%$ Resources:lblCovGroupCode %>" UniqueName="CovGroupCode"
                        SortExpression="CovGroupCode">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CovGroupDesc" HeaderText="<%$ Resources:lblCovGroupDesc %>" UniqueName="CovGroupDesc"
                        SortExpression="CovGroupDesc">
                    </telerik:GridBoundColumn>
                       <telerik:GridBoundColumn DataField="languageCode" HeaderText="<%$ Resources:lblLanguageCode %>" UniqueName="languageCode"
                        SortExpression="languageCode" Visible = "false">
                    </telerik:GridBoundColumn>
                     <telerik:GridBoundColumn DataField="languageDesc" HeaderText="<%$ Resources:lblLanguage %>" UniqueName="languageDesc"
                        SortExpression="languageDesc">
                    </telerik:GridBoundColumn>
                                  
                </Columns>
             </MasterTableView>
    </telerik:RadGrid>
    </td></tr>
  </table>
    </div>
       <asp:textbox style="display: none" runat="server" id="hdnCodeId" text="" rmxtype="hidden" />
       <asp:textbox style="display: none" runat="server" id="hdnLangCode" text="" rmxtype="hidden" />
    </form>
</body>
</html>
