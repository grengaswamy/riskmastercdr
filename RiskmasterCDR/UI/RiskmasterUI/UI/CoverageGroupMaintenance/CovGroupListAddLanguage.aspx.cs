﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.UI.CoverageGroupMaintenance
{
    public partial class CovGroupListAddLanguage : NonFDMBasePageCWS
    {
        //string m_sCovGroupId = string.Empty;

        string m_sLangCode = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            string sreturnValue = String.Empty; 
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            int iCurrentPage = 0;
            int iPageSize = 0;
            int iTotalRows = 0;
            string typecode = String.Empty;
            string table = String.Empty;
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CovGroupListAddLanguage.aspx"), "CovGroupListAddLanguageValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CovGroupListAddLanguageValidations", sValidationResources, true);
            if(!IsPostBack)
            {
                try
                {
                    if (AppHelper.GetQueryStringValue("ID") != null)
                    {
                        hdnCodeId.Text  = AppHelper.GetQueryStringValue("ID");
                     
                    }
                  
                }
                catch(Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                   
                }
            }
           
               
        }
        
        private XElement GetMessageTemplate(string sCovGroupId) //XML template for CWS call
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<PassToWebService><CoverageGroupList>");
            sXml = sXml.Append("<listhead>");
            sXml = sXml.Append("<CovGroupCode>CovGroupCode</CovGroupCode>");
            sXml = sXml.Append("<CovGroupDesc>CovGroupDesc</CovGroupDesc>");
            sXml = sXml.Append("<languageCode>languageCode</languageCode>");
            sXml = sXml.Append("<languageDesc>languageDesc</languageDesc>");
            sXml = sXml.Append("<CovGroupId>");
            sXml = sXml.Append(sCovGroupId);
            sXml =  sXml.Append("</CovGroupId>");
            sXml = sXml.Append("</listhead></CoverageGroupList></PassToWebService>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
       

        /// <summary>
        /// cmdNew_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdNew_Click(object sender, EventArgs e)
        {
            Server.Transfer(string.Format("CoveGroupDetailAddLanguage.aspx?mode=new&selectedid={0}&langCode={1}",hdnCodeId.Text,0),false);
        }
        /// <summary>
        /// cmdEdit_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdEdit_Click(object sender, EventArgs e)
        {
            Server.Transfer(string.Format("CoveGroupDetailAddLanguage.aspx?mode=edit&selectedid={0}&langCode={1}", hdnCodeId.Text, hdnLangCode.Text), false);

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("CoverageGroupsMaintenance.aspx", false);
        }

        /// <summary>
        /// grdRadio_CheckedChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdRadio_CheckedChanged(object sender, EventArgs e)
        {
            GridDataItem item = (sender as RadioButton).Parent.Parent as GridDataItem;
            if (item != null)
            {
                hdnCodeId.Text = item["CovGroupId"].Text;
                hdnLangCode.Text = item["languageCode"].Text;
            }
        }
        /// <summary>
        /// cmdDelete_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdDelete_Click(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = string.Empty;
            string selectedRowId = string.Empty;
            XmlDocument XmlDoc = new XmlDocument();
            XmlNode xXmlError = null;
            try
            {
                if (!string.IsNullOrEmpty(hdnCodeId.Text))
                {
                    
                    XmlTemplate = GetMessageTemplate(hdnCodeId.Text,hdnLangCode.Text);
                    CallCWS("CoverageGroupMaintenanceAdaptor.DeleteAdditionalLangCode", XmlTemplate, out sCWSresponse, false, false);
                    XmlDoc.LoadXml(sCWSresponse);
                    xXmlError = XmlDoc.SelectSingleNode("//MsgStatusCd");
                    if (xXmlError != null)
                    {
                        if (string.Compare(xXmlError.InnerText, "Error", true) != 0)
                        {
                            gvCoverageGroupGrid.Rebind();
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        /// <summary>
        /// gvCoverageGroupGrid_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvCoverageGroupGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //if (e.Item is GridPagerItem)
            //{
            //    GridPagerItem pager = (GridPagerItem)e.Item;
            //    Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
            //    lbl.Visible = false;

            //    RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
            //    combo.Visible = false;
            //}
            //else 
            if (e.Item is GridDataItem)
            {
                RadioButton grdRadio = null;
                GridDataItem item = e.Item as GridDataItem;
                Control radioControl = item["grdRadio"];
                if (radioControl != null)
                {
                    grdRadio = radioControl.FindControl("gdRadio") as RadioButton;
                    if (grdRadio != null)
                    {
                        //grdRadio.Attributes.Add("OnClick", "selectSingleRadio(" + grdRadio.ClientID + ", " + "'gvCoverageGroupGrid'" + ") ");
                        grdRadio.Attributes.Add("OnClick", "selectSingleRadio(" + grdRadio.ClientID + ", " + "'gvCoverageGroupGrid'" + ", " + item["CovGroupId"].Text + ", " + item["languageCode"].Text + ") ");
                    }
                }
            }

        }
        /// <summary>
        /// gvCoverageGroupGrid_NeedDataSource
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvCoverageGroupGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
           
            BindData();
        }
        /// <summary>
        /// BindData
        /// </summary>
        /// <param name=""></param>
        private void BindData()
        {
            string sCWSResponse = string.Empty;
            XmlDocument xDoc = null;
            DataRow CovGroupRow;
            XElement XmlTemplate = null;
            XmlNodeList xCovGroupRows = null;
            XmlTemplate = GetMessageTemplate(hdnCodeId.Text);


            CallCWSFunctionBind("CoverageGroupMaintenanceAdaptor.GetAdditionalLangCode", out sCWSResponse, XmlTemplate);
            xDoc = new XmlDocument();
            xDoc.LoadXml(sCWSResponse);

            DataSet CovGroupDataSet = new DataSet();
            DataTable CovGroupTable = CovGroupDataSet.Tables.Add("CoverageGroup");

            CovGroupTable.Columns.Add("CovGroupId", typeof(string));
            CovGroupTable.Columns.Add("CovGroupCode", typeof(string));
            CovGroupTable.Columns.Add("CovGroupDesc", typeof(string));
            CovGroupTable.Columns.Add("languageCode", typeof(string));
            CovGroupTable.Columns.Add("languageDesc", typeof(string));
          
            xCovGroupRows = xDoc.SelectNodes("//listrow");

            foreach (XmlNode xCovGroupRow in xCovGroupRows)
            {
                CovGroupRow = CovGroupTable.NewRow();
                if (xCovGroupRow.SelectSingleNode("CovGroupId") != null)
                {
                    CovGroupRow["CovGroupId"] = xCovGroupRow.SelectSingleNode("CovGroupId").InnerText;
                }
                if (xCovGroupRow.SelectSingleNode("CovGroupCode") != null)
                {
                    CovGroupRow["CovGroupCode"] = xCovGroupRow.SelectSingleNode("CovGroupCode").InnerText;
                }
                if (xCovGroupRow.SelectSingleNode("CovGroupDesc") != null)
                {
                    CovGroupRow["CovGroupDesc"] = xCovGroupRow.SelectSingleNode("CovGroupDesc").InnerText;
                }
                if (xCovGroupRow.SelectSingleNode("languageCode") != null)
                {
                    CovGroupRow["languageCode"] = xCovGroupRow.SelectSingleNode("languageCode").InnerText;
                }
                if (xCovGroupRow.SelectSingleNode("languageDesc") != null)
                {
                    CovGroupRow["languageDesc"] = xCovGroupRow.SelectSingleNode("languageDesc").InnerText;
                }
              
                CovGroupTable.Rows.Add(CovGroupRow);
            }

            gvCoverageGroupGrid.DataSource = CovGroupDataSet.Tables["CoverageGroup"];


            CovGroupTable.Dispose();
            CovGroupDataSet.Dispose();
        }
        /// <summary>
        /// GetMessageTemplate
        /// </summary>
        /// <param name="selectedRowId"></param>
        private XElement GetMessageTemplate(string selectedRowId,string sLangCode)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><CoverageGroup>");
            sXml = sXml.Append("<control name='LanguageCode'>");
            sXml = sXml.Append(sLangCode);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='CoverageGroupGrid'>");
            sXml = sXml.Append(selectedRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</CoverageGroup></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
    }

}