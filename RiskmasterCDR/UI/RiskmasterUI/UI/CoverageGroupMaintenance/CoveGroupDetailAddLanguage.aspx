﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CoveGroupDetailAddLanguage.aspx.cs" Inherits="Riskmaster.UI.UI.CoverageGroupMaintenance.CoveGroupDetailAddLanguage" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Enter Code Detail</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="Stylesheet" href="../../Scripts/zapatec/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/TableMaint.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>   
   <script type ="text/javascript">
       function CovGroupDesc_onSave() {
          
           var covGroupDesc = document.getElementById('CovGroupDesc').value.trim();
           if (covGroupDesc == "") {
               alert(CoveGroupDetailAddLanguageValidations.ValidRequiredFields);
               return false;
           }
           return true;



       }


   </script>
    <style>
        .center
        {
            text-align: center;
            float:left;
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="frmData" runat="server">
     <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td colspan="7">
                    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
                  <td colspan="7">
                <asp:TextBox Style="display: none" runat="server" ID="CovGroupId" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='CovGroupId']" rmxignoreset='true'/>
                            </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_save_active.png" 
                        class="bold" ToolTip="<%$ Resources:btnSaveTip %>"
                        runat="server" OnClick="btnOk_Click" OnClientClick="return CovGroupDesc_onSave();" />
                    <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:btnCancelTip %>"
                        runat="server" OnClick="btnCancel_Click" />
                </td>
            </tr>
            <tr>
                <td><input type="text" name="" value="deductibleDetails" id="SysFormName"  style="display:none" /></td>
                <td><input type="text" name="" value="" id="SysPageDataChanged"  style="display:none" /></td>
            </tr>
            <tr>
                <td colspan="7" class="ctrlgroup">
                    <asp:Label ID="lblCoverageGroupHeader" runat="server" Text="<%$ Resources:lblCoverageGroupHeader %>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            
            <tr>
                <td align="left" class="required">
                    <asp:Label ID="lblCovGroupCode" runat="server" Text="<%$ Resources:lblCovGroupCode %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox id="CovGroupCode" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name = 'CovGroupCode']" Enabled = "false"></asp:TextBox>
                </td>
                </tr>

                 <tr>
                <td align="left" class="required">
                    <asp:Label ID="lblCovGroupDesc" runat="server" Text="<%$ Resources:lblCovGroupDesc %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox id="CovGroupDesc" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name = 'CovGroupDesc']" OnChange="setDataChanged(true);" ></asp:TextBox>
                </td>
                </tr>
                  <tr>
                <td align="left" class="">
                    <asp:Label ID="lblLanguage" runat="server" Text="<%$ Resources:lblLanguage %>"></asp:Label>
                </td>
                <td>
                     <asp:DropDownList OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="Language" type="combobox" runat="server" 
                        rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Language']/@value" 
                        ItemSetRef="/Instance/Document/form/group/displaycolumn/control[@name='Language']" />
                </td>
                </tr>
             
                               
             <tr><td>&nbsp;</td></tr>
           
                   
        </table>
    </div>
    <asp:textbox style="display: none" runat="server" id="hdnLangCode" text="" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="hdnCodeId" text="" rmxtype="hidden" />
    </form>
</body>
</html>
