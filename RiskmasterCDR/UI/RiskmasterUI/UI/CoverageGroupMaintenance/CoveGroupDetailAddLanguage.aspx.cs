﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.UI.CoverageGroupMaintenance
{
    public partial class CoveGroupDetailAddLanguage : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = string.Empty;
        string m_sCovGroupId = string.Empty;
        string m_sMode = string.Empty;
        string sSelected = string.Empty;
        XmlDocument xmlServiceDoc = null;
        
        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date:08/22/2013
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                
                m_sMode = AppHelper.GetQueryStringValue("mode");
                            
                hdnLangCode.Text = AppHelper.GetQueryStringValue("LangCode");
                hdnCodeId.Text = AppHelper.GetQueryStringValue("selectedid");
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CoveGroupDetailAddLanguage.aspx"), "CoveGroupDetailAddLanguageValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "CoveGroupDetailAddLanguageValidations", sValidationResources, true);
                if (!IsPostBack)
                {
                    //Changed by Nikhil on 09/22/14.Code Review changes
                    // if (m_sMode.ToLower() == "edit" || m_sMode.ToLower() == "new")
                    if (string.Compare(m_sMode, "edit", true) == 0 || string.Compare(m_sMode, "new", true) == 0)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("CoverageGroupMaintenanceAdaptor.GetSelectedAdditionalLangCode", XmlTemplate, out sCWSresponse, true, true);
                        /*Start: Neha Suresh Jain, 08/22/2013: To get the combo trigger_date selected value*/
                        xmlServiceDoc = new XmlDocument();
                        if (!string.IsNullOrEmpty(sCWSresponse))
                        {
                            xmlServiceDoc.LoadXml(sCWSresponse);
                            XmlNode xXMLNode = xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='Language']");
                            if (xXMLNode != null)
                            {
                                if (!string.IsNullOrEmpty(((XmlElement)xXMLNode).GetAttribute("value")))
                                {
                                    sSelected = ((XmlElement)xXMLNode).GetAttribute("value");
                                }

                                FillLanguageCombo(xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='Language']"));
                            }

                        }
                        //End: To get the combo trigger_date selected value
                    }
                    if (string.Compare(m_sMode, "edit", true) == 0)
                    {
                        Language.Enabled = false;

                    }
                    else
                    {
                        Language.Enabled = true;

                    }
                   
                }


            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date:08/22/2013
        /// btnOk_Click .to save coverage maintainance changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOk_Click(object sender, EventArgs e)
        {

            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("CoverageGroupMaintenanceAdaptor.SaveAdditionalLangDesc", XmlTemplate, out sCWSresponse, true, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                //Changed by Nikhil on 09/22/14.Code Review changes
                //  if (sMsgStatus == "Success")
                if (string.Compare(sMsgStatus, "Success", true) == 0)
                {
                    //  ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.frmData.submit();window.close();</script>");
                    Server.Transfer("CovGroupListAddLanguage.aspx?ID=" + hdnCodeId.Text, false);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date:08/22/2013
        /// GetMessageTemplate
        /// </summary>

        private XElement GetMessageTemplate()
        {
            string sRowId = string.Empty;
            string slangCode = string.Empty;
            string sCodeDesc = string.Empty;
            //Changed by Nikhil on 09/22/14.Code Review changes
            //if (m_sMode.ToLower() == "edit")
            if (string.Compare(m_sMode, "edit", true) == 0)
            {

                slangCode = AppHelper.GetQueryStringValue("LangCode");
            }
            else
            {
               slangCode =  AppHelper.GetBaseLanguageCodeWithCulture().ToString().Split('|')[0];
            }
            hdnLangCode.Text = slangCode;
            sRowId = hdnCodeId.Text;

            
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<form name='CoverageGroupSetup' title='CoverageGroup'>");
            sXml = sXml.Append("<group name='CoverageGroupSetup' title='CoverageGroup'>");
            sXml = sXml.Append("<displaycolumn>");
            sXml = sXml.Append("<control name='CovGroupId' type='id'>");
            sXml = sXml.Append(sRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='LangCode'>");
            sXml = sXml.Append(slangCode);
            sXml = sXml.Append("</control>");          
            sXml = sXml.Append("<control name='Language' value='' type='combobox'>");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</displaycolumn>");
            sXml = sXml.Append("</group></form>");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }

        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date:08/22/2013
        /// btnCancel_Click .to redirect back to coverage maintainance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("CovGroupListAddLanguage.aspx?ID=" + hdnCodeId.Text, false);
        }

        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date:08/22/2013
        /// Function to Fill trigger date combo
        /// </summary>
        /// <param name="xmlNode"></param>
        private void FillLanguageCombo(XmlNode xmlNode)
        {
            ListItem objListItem = null;
            Language.Items.Clear();
            if (xmlNode != null)
            {
                //Changed by Nikhil on 09/22/14.Code Review changes
                //  if (xmlNode.InnerXml != "0")
                if (string.Compare(xmlNode.InnerXml, "0") != 0)
                {
                    foreach (XmlElement objElement in xmlNode)
                    {
                        objListItem = new ListItem(Server.HtmlDecode(objElement.InnerText), objElement.GetAttribute("value"));
                        Language.Items.Add(objListItem);
                        if (string.Compare(objListItem.Value, hdnLangCode.Text, true) == 0)
                        {
                            Language.SelectedValue = hdnLangCode.Text;
                        }
                    }
                }
            }
        }
    }
}