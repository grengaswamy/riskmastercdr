﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CuteWebUI;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;
using System.ServiceModel;
//using Riskmaster.UI.XMLImport;
using Riskmaster.Common;
using Riskmaster.BusinessHelpers;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Text;
using System.Data;
using System.Drawing;
using System.Linq.Expressions;
using System.Xml;
using Riskmaster.Models;


namespace Riskmaster.UI.UI.XML_Import
{
    public partial class XMLTemplateGeneration : System.Web.UI.Page
    {
        string sSelectedNodeType = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                TreeView1.Attributes.Add("onclick", "return OnTreeClick(event)");
                DisableSelectionCriteria();
            }
        }
        protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
        {
            TreeView1.SelectedNode.Checked = true;
            ExpandWhenChecked(TreeView1.SelectedNode);
            //if (TreeView1.SelectedNode.ChildNodes.Count == 0)
            //{
            //    ExpandNode(TreeView1.SelectedNode);
            //}
        }
        protected void TreeView1_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            ExpandWhenChecked(e.Node);
        }

        private void ExpandWhenChecked(TreeNode oNode)
        {
            if (oNode.Checked)
            {
                if (int.Equals(oNode.ChildNodes.Count, 0) && oNode.SelectAction != TreeNodeSelectAction.None)
                {
                    oNode.Select();
                    ExpandNode(oNode);
                }
            }
        }

        private void ExpandNode(TreeNode oNode)
        {
            sSelectedNodeType = oNode.Text;
            this.lblSelectedNodeType.Text = oNode.Text;
            if (oNode.Parent != null)
            {
                lblSelectedNodeParentType.Text = oNode.Parent.Text;
            }

          
            switch (sSelectedNodeType)
            {
                case "Event":
                    oNode.ChildNodes.Add(new TreeNode("GeneralClaim", "1"));
                    oNode.ChildNodes.Add(new TreeNode("WorkersCompensationClaim", "1"));
                    oNode.ChildNodes.Add(new TreeNode("VehicleAccidentClaim", "1"));
                    oNode.ChildNodes.Add(new TreeNode("NonOccupationalClaim", "1"));
                    oNode.ChildNodes.Add(new TreeNode("PropertyClaim", "1"));
                    oNode.ChildNodes.Add(new TreeNode("DatedText", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Intervention", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Witness", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Employee", "1"));
                    oNode.ChildNodes.Add(new TreeNode("MedicalStaff", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Physician", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Patient", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Other People", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Driver Insured", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Driver Other", "1"));
                    break;
                case "GeneralClaim":
                    oNode.ChildNodes.Add(new TreeNode("Claimant", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Defendant", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Litigation", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Adjuster", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Subrogation", "1")); // igupta3 MITS:32002

                    break;
                case "WorkersCompensationClaim":
                    oNode.ChildNodes.Add(new TreeNode("Defendant", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Litigation", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Adjuster", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Subrogation", "1")); // igupta3 MITS:32002
                    break;
                case "VehicleAccidentClaim":
                    oNode.ChildNodes.Add(new TreeNode("Claimant", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Defendant", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Litigation", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Adjuster", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Subrogation", "1"));// igupta3 MITS:32002
                    oNode.ChildNodes.Add(new TreeNode("Unit", "1"));// igupta3 MITS:32002
                    break;
                case "NonOccupationalClaim":
                    oNode.ChildNodes.Add(new TreeNode("Defendant", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Litigation", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Adjuster", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Subrogation", "1"));// igupta3 MITS:32002
                    break;
                case "PropertyClaim":
                    oNode.ChildNodes.Add(new TreeNode("Claimant", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Defendant", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Litigation", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Adjuster", "1"));
                    oNode.ChildNodes.Add(new TreeNode("Subrogation", "1"));// igupta3 MITS:32002
                    break;

                case "Claim Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;

                case "Event Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Policy Tracking Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Patient Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Claimant Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Defendant Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Physician Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Funds Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Entity Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Disability Plan Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Leave Plan Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Staff Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Vehicle Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Property Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;
                case "Policy Management Records":
                    oNode.ChildNodes.Add(new TreeNode("Attachments", "1"));
                    break;

            }

            oNode.Expanded = true;
            EnableSelectionCriteria();
        }
        private void AddTemplate(ref XElement oParentEle, string p_sRecordType, int p_iRecordCount)
        {
            XElement oElement = null;
            switch (p_sRecordType)
            {
                case "Event":
                    oElement = XElement.Parse(@"
                    <Event>                    
                        <EventId value='-1'/>
                        <EventQM>
                          <EventId value='-1'></EventId>
                          <MedCaseNumber value=''></MedCaseNumber>
                          <MedFileCode codeid='' />
                          <MedIndCode codeid='' />
                        </EventQM>
                        <EventNumber value=''/>
                        <DateReported value=''/>
                        <EventTypeCode codeid=''/>
                        <TimeReported value=''/>
                        <DateOfEvent value=''/>
                        <InjuryFromDate value=''/>
                        <TimeOfEvent value=''/>
                        <InjuryToDate value=''/>
                        <EventStatusCode codeid=''/>
                        <EventIndCode codeid=''/>
                        <DeptEid codeid=''/>
                        <DeptInvolvedEid codeid=''/>
                        <LocationAreaDesc value=''/>
                        <LocationAreaDesc_HTMLComments value=''/>
                        <EventDescription value=''/>
                        <EventDescription_HTMLComments value=''/>
                        <Addr1 value=''/>
                        <OnPremiseFlag value=''/>
                        <Addr2 value=''/>
                        <Addr3 value=''/>
                        <Addr4 value=''/>
                        <PrimaryLocCode codeid=''/>
                        <City value=''/>
                        <LocationTypeCode codeid=''/>
                        <StateId codeid=''/>
                        <CauseCode codeid=''/>
                        <ZipCode value=''/>
                        <NoOfInjuries value=''/>
                        <CountyOfInjury value='' />
                        <NoOfFatalities value=''/>
                        <CountryCode codeid='' />
                        <ConfRecFlag value=''/>
                        <RptdByEid codeid='' />
                        <ReporterEntity>
                          <EntityId value='-1'/>
                          <LastName value=''/>
                          <Phone1 value=''/>
                          <FirstName value='' />
                          <Phone2 value=''/>
                          <Addr1 value=''/>
                          <FaxNumber value=''/>
                          <Addr2 value=''/>
                          <Addr3 value=''/>
                          <Addr4 value=''/>
                          <EmailAddress value=''/>
                          <City value=''/>
                          <StateId codeid=''/>
                          <ZipCode value=''/>
                          <CountryCode codeid=''/>
                          <TaxId value=''/>
                        </ReporterEntity>
                        <DateToFollowUp value='' />
                        <TreatmentGiven value=''/>
                        <DatePhysAdvised value='' />
                        <ReleaseSigned value=''/>
                        <TimePhysAdvised value='' />
                        <DeptHeadAdvised value=''/>
                        <DateCarrierNotif value='' />
                        <PhysNotes value=''/>
                        <EventXAction codeid='' />
                        <EventXOutcome codeid='' />
                        <RequireIntervention value=''/>
                      
                        <PiList committedcount='' />
                        <EventXDatedTextList committedcount='' />
                        <EventXInterventionList committedcount='' />
                        <ClaimList committedcount =''/>

                    </Event>
                    ");
                    XElement currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.Add(oElement);

                    oParentEle = currentPointer;
                    break;

                case "GeneralClaim":

                    oElement = XElement.Parse(@"
                        <Claim>
	                        <EventId value='-1'/>
                            <ClaimId value='-1'/>
                            <ClaimNumber value=''/>
                            <LineOfBusCode codeid='241'/>
                            <ClaimTypeCode codeid=''/>
                            <DateOfClaim value=''/>
                            <TimeOfClaim value=''/>
                            <ClaimStatusCode codeid=''/>
                            <DttmClosed  value=''/>
                            <MethodClosedCode codeid=''/>
                            <DateRptdToRm value=''/>
                            <EstCollection value=''/>
                            <PrimaryPolicyId value=''/>
                            <FileNumber value=''/>
                            <PaymntFrozenFlag value=''/>
                            <ServiceCode codeid=''/>
                            <LssClaimInd value=''/>
                            <EmailAddresses value=''/>
                            <PoliceAgencyEid codeid=''/>
                            <ClaimantList committedcount='' />
                            <DefendantList committedcount='' />
                            <AdjusterList committedcount='' />
                            <LitigationList committedcount='' />
                            <SubrogationList committedcount='' />
                            <PropertyLossList committedcount='' />
                            <UnitList committedcount='' />
                            <LiabilityLossList committedcount='' />
                            <ArbitrationList committedcount='' />
                          </Claim>
                    ");
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("ClaimList").Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "WorkersCompensationClaim":
                    oElement = XElement.Parse(@"
                            <Claim>
  	                            <EventId value='-1'/>
 	                            <PrimaryPiEmployee>
	                              <PiEid codeid='' /> 
	                              <EmployeeNumber value=''/>
	                              <WorkdayStartTime value='' /> 
  	                              <RegularJobFlag value=''/>
	                              <LostConscFlag value=''/>
	                              <OshaRecFlag value=''/>
	                              <OshaAccDesc value=''/> 
	                              <PiEntity>
  		                            <LastName value=''/>	
		                            <TaxId value=''/>
		                            <FirstName value=''/>
		                            <BirthDate value='' /> 
		                            <MiddleName value='' /> 
		                            <Abbreviation value=''/>	
		                            <Phone1 value='' /> 
		                            <Phone2 value='' /> 
		                            <AlsoKnownAs value='' /> 
		                            <Addr1 value=''/>
		                            <FaxNumber value='' /> 
		                            <Addr2 value='' /> 
                                    <Addr3 value='' /> 
                                    <Addr4 value='' /> 
		                            <SexCode codeid=''/>
		                            <City value=''/>
		                            <Title value='' /> 
  		                            <StateId codeid=''/>
  		                            <ZipCode value=''/>
  		                            <CostCenterCode codeid='' /> 
  		                            <County value='' /> 
 		                            <CountryCode codeid='0' /> 
  		                            <EntityId value='-1'/>
  	                              </PiEntity>
                                  <CustomFedTaxPer value=''/> 
                                  <CustomMedTaxPer value=''/> 
                                  <CustomSSTaxPer value=''/> 
                                  <CustomSTTaxPer value=''/> 
                                  <PiXDependentList count='' /> 
                                  <DateHired value=''/> 
                                  <DateOfDeath value='' /> 
                                  <TermDate value='' /> 
                                  <MaritalStatCode codeid='' /> 
                                  <PositionCode codeid=''/> 
                                  <WorkPermitDate value='' /> 
                                  <DeptAssignedEid codeid=''/> 
                                  <WorkPermitNumber value='' /> 
                                  <SupervisorEid codeid='' /> 
                                  <NcciClassCode codeid='' /> 
                                  <PayTypeCode codeid=''/>
                                  <JobClassCode codeid='' /> 
                                  <WorkSunFlag value=''/> 
                                  <WorkThuFlag value=''/> 
                                  <WorkMonFlag value=''/> 
                                  <WorkFriFlag value=''/> 
                                  <WorkTueFlag value=''/> 
                                  <WorkSatFlag value=''/> 
                                  <WorkWedFlag value=''/> 
                                  <PayAmount value=''/>
                                  <ActiveFlag value=''/> 
                                  <HourlyRate value=''/>
                                  <FullTimeFlag value=''/> 
                                  <WeeklyHours value=''/> 
                                  <ExemptStatusFlag value=''/> 
                                  <WeeklyRate value=''/> 
                                  <HiredInSteFlag value=''/> 
                                  <MonthlyRate value=''/> 
                                  <DisabilityOption codeid='' /> 
                                  <Hospitals codeid='' /> 
                                  <HrangeStartDate  value=''/> 
                                  <DisabilityCode codeid='' /> 
                                  <HrangeEndDate  value=''/> 
                                  <IllnessCode codeid='' /> 
                                  <InjuryCodes codeid='' /> 
                                  <Physicians codeid='' /> 
                                  <Treatments codeid='' /> 
                                  <OtherTreatments  value=''/> 
                                  <DiagnosisList codeid='' /> 
                                  <MedicalConditions codeid='' /> 
                                  <BodyParts codeid='' /> 
                                  <OtherMedcond  value=''/> 
                                  <MajorHandCode codeid='' /> 
                                  <StdDisabilityType codeid='' /> 
                                  <PiXRestrictList count='' /> 
                                  <PiXWorkLossList count='' /> 
                                  <PiRowId value='-1'/> 
                                </PrimaryPiEmployee>
                                <ClaimId  value='-1'/> 
                                <LineOfBusCode codeid='243'/>
                                <ClaimNumber value=''/> 
                                <DateOfClaim value=''/> 
                                <TimeOfClaim value=''/> 
                                <ClaimTypeCode codeid=''/> 
                                <DateRptdToRm value='' /> 
                                <DisabilFromDate value=''/> 
                                <ClaimStatusCode codeid=''/>
                                <DisabilToDate value='' /> 
                                <DttmClosed value='' /> 
                                <DisTypeCode codeid='0' /> 
                                <MethodClosedCode codeid='0' /> 
                                <FileNumber value='' /> 
                                <PlanId defaultdetail='' value=''/>
                                <FilingStateId codeid=''/>
                                <LeaveOnlyFlag value=''/>
                                <ServiceCode codeid='0' /> 
                                <PaymntFrozenFlag value=''/>
                                <LssClaimInd value=''/>
                                <LeavePlanList committedcount='' /> 
                                 <CaseManagement>
                                  <CaseMgrHistList count='' /> 
                                  <TreatmentPlanList count='' /> 
                                  <MedMgtSavingsList count='' /> 
                                  <CasemgtRowId>-1</CasemgtRowId> 
                                  <RtwStatusCode codeid='' /> 
                                  <EstRelRstDate value='' /> 
                                  <EstRetWorkDate value='' /> 
                                  <AccommodationList count='' /> 
                                 </CaseManagement>
                                  <AdjusterList committedcount='' /> 
                                  <LitigationList committedcount='' /> 
                                  <SubrogationList committedcount='' /> 
                                  <DefendantList committedcount='' /> 

                            </Claim>
 ");
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("ClaimList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "VehicleAccidentClaim":
                    oElement = XElement.Parse(@"
                             <Claim>
                                    <EventId value='-1'/>
                                     <ClaimId value='-1'/>
                                     <ClaimNumber value=''/>
	                                <LineOfBusCode codeid='242'/>
                                     <ClaimTypeCode codeid=''/>
                                    <DateOfClaim value=''/>
                                    <TimeOfClaim value=''/>
                                    <ClaimStatusCode codeid=''/>
                                    <DttmClosed value=''/> 
                                    <MethodClosedCode codeid='' /> 
                                    <DateRptdToRm value=''/> 
                                    <FileNumber value=''/> 
                                    <EstCollection value=''/>
                                    <PrimaryPolicyId value=''/>
                                    <PaymntFrozenFlag value=''/>
                                     <ServiceCode codeid='' /> 
                                      <EmailAddresses value=''/> 
                                    <LssClaimInd value=''/>
                                    <PoliceAgencyEid codeid='' /> 
                                    <AccidentTypeCode codeid='' /> 
                                    <InTrafficFlag value='False'/> 
                                    <AccidentDescCode codeid='' /> 
                                    <ReportableFlag value='False'/> 
                                    <DateFddotRpt value=''/> 
                                    <PreventableFlag value='False'/> 
                                    <DateStdotRpt value=''/> 
                                    <StDotRptId value='' />   
                                   <AdjusterList committedcount='' /> 
                                    <LitigationList committedcount='' /> 
                                    <SubrogationList committedcount='' /> 
                                    <DefendantList committedcount='' /> 
                                    <ClaimantList committedcount='' /> 
                                    <UnitList committedcount='' /> 

                            </Claim>
 ");
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("ClaimList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "NonOccupationalClaim":
                    oElement = XElement.Parse(@"
                          <Claim>
                                <EventId value='-1'/>
                                <PrimaryPiEmployee>
                                <PiEid codeid='' />
                                <EmployeeNumber value=''/>
                                <WorkdayStartTime value=''/>
                                <RegularJobFlag value=''/>
                                <LostConscFlag value=''/>
                                <OshaRecFlag value=''/>
                                <OshaAccDesc  value=''/>
                                <PiEntity>
                                        <LastName value=''/>
                                        <TaxId value=''/>
                                         <FirstName value=''/>
                                        <BirthDate value=''/>
                                        <MiddleName value=''/>
                                        <Abbreviation value=''/>
                                            <Phone1 value=''/>
                                    <Phone2 value=''/>
                                    <AlsoKnownAs value=''/>
                                    <Addr1 value=''/>
                                    <FaxNumber value=''/>
                                    <Addr2 value=''/>
                                    <Addr3 value=''/>
                                    <Addr4 value=''/>
                                    <SexCode codeid='' />
                                    <City value=''/>
      <Title value=''/>
      <StateId codeid='' />
      <ZipCode value=''/>
      <CostCenterCode codeid='' />
      <County value=''/>
      <CountryCode codeid=''/>
      <EntityId value=''/>
        </PiEntity>
    <CustomFedTaxPer value=''/>
    <CustomMedTaxPer value=''/>
    <CustomSSTaxPer value=''/>
    <CustomSTTaxPer value=''/>
    <PiXDependentList count='' />
    <DateHired  value=''/>
    <DateOfDeath  value=''/>
    <TermDate value=''/>
    <MaritalStatCode codeid='' />
    <PositionCode codeid='' />
    <WorkPermitDate value=''/>
    <DeptAssignedEid codeid=''/>
    <WorkPermitNumber value=''/>
    <SupervisorEid codeid='' />
    <NcciClassCode codeid='' />
    <PayTypeCode codeid='' />
    <JobClassCode codeid='' />
    <WorkSunFlag value=''/>
    <WorkThuFlag value=''/>
    <WorkMonFlag value=''/>
    <WorkFriFlag value=''/>
    <WorkTueFlag value=''/>
    <WorkSatFlag value=''/>
    <WorkWedFlag value=''/>
    <PayAmount value=''/>
    <ActiveFlag value=''/>
    <HourlyRate value=''/>
    <FullTimeFlag value=''/>
    <WeeklyHours value=''/>
    <ExemptStatusFlag value=''/>
    <WeeklyRate value=''/>
    <HiredInSteFlag value=''/>
    <MonthlyRate value=''/>
    <DisabilityOption codeid='' />
    <Hospitals codeid='' />
    <HrangeStartDate value=''/>
    <DisabilityCode codeid='' />
    <HrangeEndDate value=''/>
    <IllnessCode codeid='' />
    <InjuryCodes codeid='' />
    <Physicians codeid='' />
    <Treatments codeid='' />
    <OtherTreatments value=''/>
    <DiagnosisList codeid='' />
    <MedicalConditions codeid='' />
    <BodyParts codeid='' />
    <OtherMedcond value=''/>
    <MajorHandCode codeid='' />
    <StdDisabilityType codeid='' />
    <PiXRestrictList count='' />
    <PiXWorkLossList count='' />
    <PiRowId value=''/>
  </PrimaryPiEmployee>
  <LineOfBusCode codeid='844'/>
  <ClaimId value='-1'/>
  <ClaimNumber value=''/>
  <DateOfClaim value=''/>
  <TimeOfClaim value=''/>
  <ClaimTypeCode codeid=''/>
  <DateRptdToRm value=''/>
  <DisabilFromDate value=''/>
  <ClaimStatusCode codeid=''/>
  <DisabilToDate value=''/>
  <DttmClosed value=''/>
  <DisTypeCode codeid='' />
  <MethodClosedCode codeid='' />
  <FileNumber value=''/>
  <PlanId defaultdetail=''/>
  <FilingStateId codeid=''/>
  <LeaveOnlyFlag value=''/>
  <ServiceCode codeid='' />
  <PaymntFrozenFlag value=''/>
  <LssClaimInd value=''/>
  <LeavePlanList committedcount='' />
   <CaseManagement>
    <CaseMgrHistList count='' />
    <TreatmentPlanList count='' />
    <MedMgtSavingsList count='' />
    <CasemgtRowId value=''/>
    <RtwStatusCode codeid='' />
    <EstRelRstDate value=''/>
    <EstRetWorkDate value=''/>
    <AccommodationList count='' />
  </CaseManagement>
    <AdjusterList committedcount='' />
  <LitigationList committedcount='' />
  <SubrogationList committedcount='' />
  <DefendantList committedcount='' />

</Claim>
 ");
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("ClaimList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "PropertyClaim":
                    oElement = XElement.Parse(@"
                         <Claim>
  <EventId value='-1'/>
  <ClaimId value='-1'/>
  <ClaimNumber value=''/>
	<LineOfBusCode codeid='845'/>
  <ClaimTypeCode codeid=''/>
  <DateOfClaim value=''/>
  <TimeOfClaim value=''/>
  <ClaimStatusCode codeid=''/>
  <DttmClosed value=''/> 
  <MethodClosedCode codeid='' /> 
  <DateRptdToRm value=''/> 
  <EstCollection value=''/>
  <PrimaryPolicyId value=''/>
  <FileNumber  value=''/> 
  <PaymntFrozenFlag value=''/>
  <ServiceCode codeid='' /> 
  <LssClaimInd value=''/>
  <PoliceAgencyEid codeid='' /> 
 <PropertyClaim>
  <PropRowID value=''/>
  <PropertyID defaultdetail=''/>
  <TerritoryCode codeid='' /> 
  <Description value=''/> 
  <CategoryCode codeid='' /> 
  <Addr1 value=''/>
  <AppraisedValue value=''/>
  <Addr2 value=''/> 
  <Addr3 value=''/> 
  <Addr4 value=''/> 
  <AppraisedDate value=''/> 
  <City value=''/>
  <AppraisalSourceCode codeid='' /> 
  <StateId codeid=''/>
  <LandValue value=''/>
  <ZipCode value=''/>
  <ReplacementValue value=''/>
  <CountryCode codeid='' /> 
  <ClassOfConstruction codeid='' /> 
  <HeatingSysCode codeid='' /> 
  <YearOfConstruction value=''/>
  <CoolingSysCode codeid='' /> 
  <SquareFootage value='0'/> 
  <FireAlarmCode codeid='0' /> 
  <NoOfStories value='0'/> 
  <SprinklersCode codeid='0' /> 
  <AvgStoryHeight value='0'/> 
  <EntryAlarmCode codeid='0' /> 
  <WallConstructionCode codeid='0' /> 
  <RoofAnchoringCode codeid='0' /> 
  <RoofConstructionCode codeid='0' /> 
  <GlassStrengthCode codeid='0' /> 
  <PlotPlansCode codeid='0' /> 
  <GPSAltitude value='0'/> 
  <FloodZoneCertCode codeid='0' /> 
  <GPSLatitude value='0'/> 
  <EarthquakeZoneCode codeid='0' /> 
  <GPSLongitude value='0'/> 
  <Pin value=''/> 
  </PropertyClaim>
  <ClaimantList committedcount='' /> 
  <AdjusterList committedcount='' /> 
  <LitigationList committedcount='' /> 
  <SubrogationList committedcount='' /> 
  <DefendantList committedcount='' /> 

 </Claim>
 ");
                    currentPointer = oElement;

                    oParentEle.XPathSelectElement("ClaimList").Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "Intervention":
                    oElement = XElement.Parse(@"
                 <EventXIntervention>
  <EventId value=''/>
  <EvIntRowId value='-1'/>
  <EmpEid codeid=''/>
  <DeptEid codeid=''/>
  <SupEid codeid=''/>
  <CompCode codeid='' /> 
  <Status codeid='' /> 
  <EventXIntCodes codeid='' /> 
  <EvalEid codeid='' /> 
  <RefCode codeid='' /> 
  <DtRec value=''/> 
  <AntiCost value=''/>
  <DateComplete value=''/> 
  <DateSent value=''/> 
  <DateResp value=''/> 
  <NonComp codeid='' /> 
  <InvDesc value=''/> 
  <Rate value=''/> 
  <NoHours value=''/>
  <TotalFlatAmt value=''/>

  </EventXIntervention>
                ");
                    currentPointer = oElement;

                    oParentEle.XPathSelectElement("EventXInterventionList").Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "DatedText":
                    oElement = XElement.Parse(@"
                 <EventXDatedText>
		<EventId value=''/>
		<EvDtRowId value='-1'/>
		<DateEntered value=''/>
		<TimeEntered value=''/>
		<EnteredByUser value=''/>
		<DatedText value=''/>
		<DatedText_HTMLComments value=''/>
			</EventXDatedText>


                ");
                    currentPointer = oElement;

                    oParentEle.XPathSelectElement("EventXDatedTextList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Claimant":
                    oElement = XElement.Parse(@"
                    <Claimant>
                    <ClaimId value='-1'></ClaimId>
                    <ClaimantRowId value='-1'></ClaimantRowId>
                    <ClaimantEntity>
                      <EntityId value='-1'></EntityId>
                      <LastName value=''></LastName>
                      <FirstName  value=''/>
                      <MiddleName  value=''/>
                      <Title  value=''/>
                      <Addr1  value=''/>
                      <TaxId value=''></TaxId>
                      <Addr2  value=''/>
                      <Addr3  value=''/>
                      <Addr4  value=''/>
                      <BirthDate  value=''/>
                      <City  value=''/>
                      <SexCode codeid='' />
                      <StateId codeid='' />
                      <ZipCode value=''/>
                      <CountryCode codeid='' />
                      <Phone1 value=''></Phone1>
                      <Phone2 value=''></Phone2>
                      <FaxNumber value=''></FaxNumber>
                       </ClaimantEntity>
                    <PrimaryClmntFlag value=''></PrimaryClmntFlag>
                    <ClaimantTypeCode codeid='' />
                    <SolDate  value=''/>
                    <InsurerEid codeid='' />
                    <InjuryDescription value=''/>
                    <DamageDescription value=''/>
                    <AttorneyEntity>
                      <EntityId  value='-1'></EntityId>
                      <LastName value=''/>
                      <ParentEid codeid='' />
                      <FirstName value=''/>
                      <Title value=''/>
                      <MiddleName value=''/>
                      <EmailTypeCode codeid='' />
                      <Addr1 value=''/>
                      <EmailAddress value=''/>
                      <Addr2 value=''/>
                      <Addr3 value=''/>
                      <Addr4 value=''/>
                      <Phone1 value=''/>
                      <City value=''/>
                      <Phone2 value=''/>
                      <StateId codeid='' />
                      <FaxNumber value=''/>
                      <CountryCode codeid='' />
                      <ZipCode value=''/>
                    </AttorneyEntity>

                  </Claimant>
                
                ");
                    oElement.XPathSelectElement("ClaimantRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("ClaimantList").Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "Defendant":
                    oElement = XElement.Parse(@"
                          <Defendant>
                              <ClaimId value='-1'></ClaimId>
                              <DefendantRowId value='-1'></DefendantRowId>
                              <DefendantEid codeid='0' />
                              <DefendantEntity>
                                <EntityId value='-1'></EntityId>
                                <LastName value=''></LastName>
                                <FirstName value='' />
                                <Title value='' />
                                <MiddleName value='' />
                                <Addr1 value='' />
                                <Addr2 value='' />
                                <Addr3 value='' />
                                <Addr4 value='' />
                                <City value='' />
                                <StateId codeid='' />
                                <CountryCode codeid='' />
                                <ZipCode value='' />
                                <Phone1 value='' />
                                <Phone2 value='' />
                                <FaxNumber value='' />
                              </DefendantEntity>
                              <DfndntTypeCode codeid='' />
                              <PolicyNumber value=''/>
                              <PolicyTypeCode codeid='' />
                              <PolicyLimit value=''></PolicyLimit>
                              <PolExpireDate value='' />
                              <InsurerEid codeid='' />
                              <AttorneyEntity>
                                <LastName  value=''/>
                                <ParentEid codeid='-1' />
                                <FirstName value=''/>
                                <Title value='' />
                                <MiddleName value='' />
                                <Phone1 value='' />
                                <Addr1 value='' />
                                <Phone2 value='' />
                                <Addr2 value='' />
                                <Addr3 value='' />
                                <Addr4 value='' />
                                <FaxNumber value='' />
                                <City value='' />
                                <ZipCode value='' />
                                <StateId codeid='' />
                                <EntityId value='-1'></EntityId>
                                <EmailTypeCode codeid='' />
                                <CountryCode codeid='' />
                                <EmailAddress value='' />
                                 </AttorneyEntity>
                              <AttorneyEid codeid='' />

                            </Defendant>
");
                    oElement.XPathSelectElement("DefendantRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("DefendantList").Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "Litigation":
                    oElement = XElement.Parse(@"
                          <ClaimXLitigation>
                              <ClaimId value='-1'/>
                              <LitigationRowId value='-1'/>
                              <DocketNumber value=''/>
                                <JudgeEntity>
                                  <LastName value='' /> 
                                  <FirstName value='' /> 
                                  <EntityId value=''/>
                                  <Addr1  value=''/> 
                                  <Addr2  value=''/> 
                                  <Addr3  value=''/> 
                                  <Addr4  value=''/> 
                                  <City  value=''/> 
                                  <StateId codeid='' /> 
                                  <ZipCode value='' /> 
                                  <CountryCode codeid='' /> 
                                </JudgeEntity>
                              <SuitDate value='' /> 
                              <CourtDate value='' /> 
                              <VenueStateId codeid='' /> 
                              <County value='' /> 
                              <LitTypeCode codeid='' /> 
                              <LitStatusCode codeid='' /> 
                              <DemandAllegations value='' /> 
                              <CoAttorneyEntity>
                                  <EntityId value='-1'/>
                                  <LastName value='' /> 
                                  <ParentEid codeid='' /> 
                                  <FirstName value='' /> 
                                  <Phone1 value='' /> 
                                  <Addr1 value='' /> 
                                  <Phone2 value='' /> 
                                  <Addr2  value=''/> 
                                  <Addr3  value=''/> 
                                  <Addr4  value=''/> 
                                  <FaxNumber value='' /> 
                                  <City value='' /> 
                                  <StateId codeid='' /> 
                                  <ZipCode value='' /> 
                                  <CountryCode codeid='' /> 
                                </CoAttorneyEntity>
                              <ExpertList committedcount='' /> 
                              <DemandOfferList committedcount='' /> 

                            </ClaimXLitigation>
");
                    oElement.XPathSelectElement("LitigationRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("LitigationList").Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "Adjuster":
                    oElement = XElement.Parse(@"
                <ClaimAdjuster>
  <ClaimId value=''/>
  <AdjRowId value='-1'/>
 <AdjusterEid codeid=''/>
<AdjusterEntity>
  <EntityId value='-1'/>
  <LastName value='' />
  <RMUserId codeid=''/>
  <FirstName value=''/>
  <Title value=''/> 
  <MiddleName value=''/>
  <Addr1 value=''/>
  <Phone1 value=''/>
  <Addr2 value=''/> 
  <Addr3 value=''/>
  <Addr4 value=''/>
  <Phone2 value=''/>
  <City value=''/>
  <FaxNumber value=''/>
  <StateId codeid=''/>
  <ZipCode value=''/>
  <CountryCode codeid=''/>
 </AdjusterEntity>
  <CurrentAdjFlag value=''/>
<AdjustDatedTextList committedcount='' /> 

</ClaimAdjuster>

                ");
                    oElement.XPathSelectElement("AdjRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("AdjusterList").Add(oElement);
                    oParentEle = currentPointer;
                    break;

                //igupta3 MITS:32002 Changes starts
                case "Subrogation":
                    oElement = XElement.Parse(@"
<ClaimXSubrogation>
  <ClaimId value='-1'/>
  <SubrogationRowId value='-1'/>
  <SubTypeCode codeid=''/>
  <SubSpecialistEid codeid='' /> 
  <SubStatusCode codeid=''/>
  <SubStatusDescCode codeid='' /> 
  <StatusDate value=''/>
  <SubAdversePartyEid codeid='' /> 
  <SubAdverseInsCoEid codeid='' /> 
  <SubAdvAdjusterEid codeid='' /> 
  <SubAdverseClaimNum value=''/> 
  <SubAdversePolicyNum value=''/> 
   <NbOfYears value=''/>
  <AdvPartiesCovLimit value=''/> 
  <StatuteLimitationDate value=''/> 
  <AmountPaid value=''/>
  <SettlementPercent value=''/>
  <CovDedAmount value=''/>
  <PotPayoutAmt value=''/>
  <TotalToBeRecov value=''/>
  <PotDedAmt value=''/>
  <TotalPotRecov value=''/>
  <DemandOfferList committedcount='' /> 

</ClaimXSubrogation>
");
                    oElement.XPathSelectElement("SubrogationRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("SubrogationList").Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "Unit":
                    oElement = XElement.Parse(@"
<UnitXClaim>
  <ClaimId value='-1'/>
  <UnitRowId value='-1'/>
<Vehicle>
  <UnitId value='-1'/> 
  <Vin value=''/>
  <VehicleMake value=''/>
  <VehicleModel value=''/>
  <VehicleYear value=''/>
</Vehicle>
  <HomeDeptEid codeid=''/>
  <LicenseNumber value=''/>
  <StateRowId codeid=''/>
  <UnitTypeCode codeid=''/>
  <Damage value=''/> 
  <Insured value=''/>
  <Drivable value=''/>
  <NonVehicleDamage value=''/>
  <EstDamage value=''/>
  <DateReported value=''/> 
  <Owner codeid=''/> 
  <TimeReported value=''/> 
  <SeenAt value=''/> 
  <OtherInsurance value=''/>
  <PolholderIsPropertyOwner value=''/>
  <PolicyHolder codeid='' /> 
  <Company codeid='' /> 
  <PolicyNumber value=''/> 
  <Coverages value=''/> 
  <Limits value=''/> 
  <DemandOfferList committedcount='' /> 
  </UnitXClaim>
");
                    oElement.XPathSelectElement("UnitRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("UnitList").Add(oElement);
                    oParentEle = currentPointer;
                    break;

                //igupta3 MITS: 32002 Changes end

                case "Witness":
                    oElement = XElement.Parse(@"
                <PiWitness>
		<EventId value=''></EventId>
		<PiRowId value='0'></PiRowId>
		<PiEid codeid='0' />
		<PiTypeCode codeid='239' />
	 <PiEntity>
			<LastName value=''></LastName>
			<EntityId value='0'/>
			<TaxId value=''/>
			<FirstName value=''/>
			<BirthDate value=''/>
			<MiddleName value=''/>
			<Title value=''/>
			<Abbreviation value=''></Abbreviation>
			<Phone1 value=''/>
			<AlsoKnownAs value=''/>
			<Phone2 value=''/>
			<Addr1 value=''/>
			<FaxNumber value=''/>
			<Addr2 value=''/>
			<Addr3 value=''/>
			<Addr4 value=''/>
			<SexCode codeid='' />
			<City value=''/>
			<StateId codeid='' />
			<ZipCode value=''/>
			<County value=''/>
			<CountryCode codeid='' />
		</PiEntity>
		<MaritalStatCode codeid='' />
		<DescByWitness value=''/>
	
	</PiWitness>


                ");
                    oElement.XPathSelectElement("PiRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("PiList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Employee":
                    oElement = XElement.Parse(@"
                  <PiEmployee>
		<EventId value=''></EventId>
		<PiRowId value='0'/>
		<PiEid codeid='0' />
		<PiTypeCode codeid='237' />
		<EmployeeNumber value=''></EmployeeNumber>
		 <PiEntity>
			<TaxId value=''></TaxId>
			<LastName value=''></LastName>
			<BirthDate value=''/>
			<FirstName value=''/>
			<Title value=''/>
			<EntityId value='0'/>
			<MiddleName value=''/>
			<Phone1 value=''/>
			<AlsoKnownAs value=''/>
			<Phone2 value=''/>
			<Abbreviation value=''></Abbreviation>
			<FaxNumber value=''/>
			<Addr1 value=''/>
			<SexCode codeid='' />
			<Addr2 value=''/>
			<Addr3 value=''/>
			<Addr4 value=''/>
			<City value=''/>
			<CostCenterCode codeid='' />
			<StateId codeid='' />
			<ZipCode value=''/>
			<County  value=''/>
			<CountryCode codeid='' />
		</PiEntity>
		<MaritalStatCode codeid='' />
		<DateHired value='' />
		<TermDate value=''/>
		<PositionCode codeid='' />
		<ActiveFlag value=''></ActiveFlag>
		<DeptAssignedEid codeid=''></DeptAssignedEid>
		<FullTimeFlag value=''></FullTimeFlag>
		<SupervisorEid codeid='' />
		<ExemptStatusFlag value=''></ExemptStatusFlag>
		<PayTypeCode codeid='' />
		<NoOfExemptions value=''></NoOfExemptions>
		<PayAmount value=''></PayAmount>
		<HiredInSteFlag value=''></HiredInSteFlag>
		<HourlyRate value=''></HourlyRate>
		<WorkdayStartTime value=''/>
		<WeeklyHours value=''></WeeklyHours>
		<EligDisBenFlag value=''></EligDisBenFlag>
		<WeeklyRate value=''></WeeklyRate>
		<DisabilityOption codeid='' />
		<MonthlyRate value=''></MonthlyRate>
		<DailyHours value=''></DailyHours>
		<DailyRate value=''></DailyRate>
		<CollateralSources codeid='' />
		<CollateralOffset value=''></CollateralOffset>
		<WorkSunFlag value=''></WorkSunFlag>
		<WorkThuFlag value=''></WorkThuFlag>
		<WorkMonFlag value=''></WorkMonFlag>
		<WorkFriFlag value=''></WorkFriFlag>
		<WorkTueFlag value=''></WorkTueFlag>
		<WorkSatFlag value=''></WorkSatFlag>
		<WorkWedFlag value=''></WorkWedFlag>
		<EstLenDisability value=''></EstLenDisability>
		<RegularJobFlag value=''></RegularJobFlag>
		<EstRtwDate value='' />
		<DateOfDeath value=''/>
		<DisabilityCode codeid='' />
		<IllnessCode codeid='' />
		<InjuryCodes codeid='' />
		<Hospitals codeid='' />
		<BodyParts codeid='' />
		<Physicians codeid='' />
		<OshaRecFlag value=''></OshaRecFlag>
		<DiagnosisList codeid='' />
		<OshaAccDesc  value=''/>
		<Treatments codeid='' />
		<DriversLicNo value=''/>
		<WorkPermitNumber value=''/>
		<Driverslictypecode codeid='' />
		<WorkPermitDate value=''/>
		<DrivlicState codeid='' />
		<JobClassCode codeid='' />
		<DateDriverslicexp value=''/>
		<DrivlicRstrctcode codeid='' />
		<NcciClassCode codeid='' />
		<LastVerifiedDate value=''/>
		<InsurableFlag value=''></InsurableFlag>
		<LostConscFlag value=''></LostConscFlag>
		<InjuryCauseCode codeid='' />
		<PreExistingCondition codeid='' />
		<ActivityWhenInjured value=''/>
		<SymptomsDesc value='' />
		<WhereInjuryTaken value=''/>
		<Prognosis value=''/>
		<InjuryInconsistentWithPoliceRPTFlag value=''></InjuryInconsistentWithPoliceRPTFlag>
		<HospitalFlag value=''></HospitalFlag>
		<FileClosingdate value=''/>
		<Medications value=''/>
		<CatastrophicInjuryFlag value=''></CatastrophicInjuryFlag>
		<Impairment value=''></Impairment>
	<DemandOfferList committedcount='' />
	 <CaseManagementList committedcount='' />

	</PiEmployee>



                ");
                    oElement.XPathSelectElement("PiRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("PiList").Add(oElement);
                    oParentEle = currentPointer;
                    break;


                case "MedicalStaff":
                    oElement = XElement.Parse(@"<PiMedicalStaff>
		<EventId value=''></EventId>
		<PiRowId value='0'/>
		 <MedicalStaff>
		   <StaffEntity>
				<EntityId value='0'/>
				<TaxId value=''/>
				<LastName value=''></LastName>
				<FreezePayments value=''></FreezePayments>
				<FirstName value=''/>
				<MiddleName value=''/>
				<Addr1 value=''/>
				<Phone1 value=''/>
				<Addr2 value=''/>
				<Addr3 value=''/>
				<Addr4 value=''/>
				<Phone2 value=''/>
				<City value=''/>
				<StateId codeid='' />
				<ZipCode value=''/>
				<FaxNumber value=''/>
				<County value=''/>
				<CountryCode codeid='' />
<Abbreviation value=''></Abbreviation>
				<AlsoKnownAs value=''/>
				<SexCode codeid='' />
				<BirthDate value=''/>
			</StaffEntity>
 <MedicalStaffNumber value=''/>
    <BeeperNumber value=''/>
    <CellularNumber value=''/>
    <EmergencyContact value=''/>
    <PrimaryPolicyId value=''></PrimaryPolicyId>
    <HireDate value=''/>
    <MaritalStatCode codeid='' />
    <LicNum value=''/>
    <HomeAddr1 value=''/>
    <LicState codeid='' />
    <HomeAddr2 value=''/>
    <HomeAddr3 value=''/>
    <HomeAddr4 value=''/>
    <LicIssueDate value=''/>
    <HomeCity value=''/>
    <LicExpiryDate value=''/>
   <HomeStateId codeid='' />
    <LicDeaNum value=''/>
    <HomeZipCode value=''/>
    <LicDeaExpDate value=''/>
    <StaffStatusCode codeid='' />
    <StaffPosCode codeid='' />
 <StaffCatCode codeid='' />
    <DeptAssignedEid codeid='' />
    <PrivilegeList committedcount='' />
    <CertificationList committedcount='' />
		</MedicalStaff>
		<PiEid codeid='0' />
</PiMedicalStaff>");
                    oElement.XPathSelectElement("PiRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("PiList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Physician":
                    oElement = XElement.Parse(@"
                   <PiPhysician>
		<EventId  value=''></EventId>
		<PiRowId  value='0'/>
		 <Physician>
			<PhysEid  value='0'></PhysEid>
			 <PhysEntity>
				<EntityId  value='0'></EntityId>
				<EntityTableId  value='1086'/>
				<LastName  value=''></LastName>
				<TaxId  value=''/>
				<FirstName  value=''/>
				<Title  value=''/>
				<MiddleName  value=''/>
				<Phone1  value=''/>
				<Addr1  value=''/>
				<Phone2  value=''/>
				<Addr2  value=''/>
				<Addr3  value=''/>
				<Addr4  value=''/>
				<City  value=''/>
				<StateId codeid='' />
				<FaxNumber  value=''/>
				<ZipCode  value=''/>
				<County  value=''/>
				<Entity1099Reportable  value=''></Entity1099Reportable>
				<CountryCode codeid='' />
				<Parent1099EID codeid='' />
				<FreezePayments  value=''></FreezePayments>
				<Abbreviation  value=''></Abbreviation>
				<AlsoKnownAs  value=''/>
				<SexCode codeid='' />
				<BirthDate  value=''/>
				<EmailTypeCode codeid='' />
				<EmailAddress  value=''/>
			</PhysEntity>
			<PhysicianNumber  value=''></PhysicianNumber>
			<MedStaffNumber  value=''/>
			<BeeperNumber  value=''/>
			<CellularNumber  value=''/>
			<EmergencyContact  value=''/>
			<MedicareNumber  value=''/>
			<PrimarySpecialty codeid='' />
			<SubSpecialtyList codeid='' />
			<MaritalStatCode codeid='' />
			<MailingAddr1  value=''/>
			<HomeAddr1  value=''/>
			<MailingAddr2  value=''/>
			<MailingAddr3  value=''/>
			<MailingAddr4  value=''/>
			<HomeAddr2  value=''/>
			<HomeAddr3  value=''/>
			<HomeAddr4  value=''/>
			<MailingCity  value=''/>
			<HomeCity  value=''/>
			<MailingStateId codeid='' />
			<HomeStateId codeid='' />
			<MailingZipCode  value=''/>
			<HomeZipCode  value=''/>
			<PrimaryPolicyId  value=''></PrimaryPolicyId>
			<StaffStatusCode codeid='' />
			<AppointDate  value=''/>
			<StaffTypeCode codeid='' />
			<ReappointDate  value=''/>
			<StaffCatCode codeid='' />
			<InternalNumber  value=''/>
			<DeptAssignedEid codeid='' />
			<LicNum  value=''/>
			<LicState codeid='' />
			<LicIssueDate  value=''/>
			<LicExpiryDate  value=''/>
			<LicDeaNum  value=''/>
			<LicDeaExpDate  value=''/>
			<ContEducation  value=''/>
			<Membership  value=''/>
			<TeachingExp  value=''/>
			<PrivilegeList committedcount='' />
			<CertificationList committedcount='' />
			<EducationList committedcount='' />
			<PrevHospitalList committedcount='' />
		</Physician>
		<PiEid codeid='0' />
		<PiTypeCode codeid='3285' />
	
	</PiPhysician>



                ");
                    oElement.XPathSelectElement("PiRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("PiList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Claim Records":
                    oElement = XElement.Parse(@"
                  <Claim>
                    <ClaimId value='' /> 
                  </Claim>

                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "Patient":
                    oElement = XElement.Parse(@"
                <PiOther>
		<EventId value=''></EventId>
		<PiEntity>
			<EntityTableId value='1409'/>
			<LastName value=''></LastName>
			<EntityId value='0'></EntityId>
			<TaxId value=''/>
			<FirstName value=''/>
			<BirthDate value=''/>
			<MiddleName value=''/>
			<Phone1 value=''/>
			<Abbreviation value=''>h</Abbreviation>
			<Phone2 value=''/>
			<AlsoKnownAs value=''/>
			<FaxNumber value=''/>
			<Addr1 value=''/>
			<Title value=''/>
			<Addr2 value=''/>
			<Addr3 value=''/>
			<Addr4 value=''/>
			<SexCode codeid='0'/>
			<City value=''/>
			<StateId codeid='0' />
			<ZipCode value=''/>
			<County value=''/>
			<CountryCode codeid='0' />
		</PiEntity>
		<PiRowId value='0'></PiRowId>
		<PiEid codeid='0' />
		<PiTypeCode codeid='240' />
		<MaritalStatCode codeid='' />
	
	</PiOther>

                ");
                    oElement.XPathSelectElement("PiRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("PiList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Other People":
                    oElement = XElement.Parse(@"
                                         <PiOther>
		<EventId value=''></EventId>
		<PiEntity>
			<EntityTableId value='1409'/>
			<LastName value=''></LastName>
			<EntityId value='0'></EntityId>
			<TaxId value=''/>
			<FirstName value=''/>
			<BirthDate value=''/>
			<MiddleName value=''/>
			<Phone1 value=''/>
			<Abbreviation value=''>h</Abbreviation>
			<Phone2 value=''/>
			<AlsoKnownAs value=''/>
			<FaxNumber value=''/>
			<Addr1 value=''/>
			<Title value=''/>
			<Addr2 value=''/>
			<Addr3 value=''/>
			<Addr4 value=''/>
			<SexCode codeid='0'/>
			<City value=''/>
			<StateId codeid='0' />
			<ZipCode value=''/>
			<County value=''/>
			<CountryCode codeid='0' />
		</PiEntity>
		<PiRowId value='0'></PiRowId>
		<PiEid codeid='0' />
		<PiTypeCode codeid='240' />
		<MaritalStatCode codeid='' />
	
	</PiOther>

                ");
                    oElement.XPathSelectElement("PiRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("PiList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Driver Insured":
                    oElement = XElement.Parse(@"
                                 <PiOther>
		<EventId value=''></EventId>
		<PiEntity>
			<EntityTableId value='1409'/>
			<LastName value=''></LastName>
			<EntityId value='0'></EntityId>
			<TaxId value=''/>
			<FirstName value=''/>
			<BirthDate value=''/>
			<MiddleName value=''/>
			<Phone1 value=''/>
			<Abbreviation value=''>h</Abbreviation>
			<Phone2 value=''/>
			<AlsoKnownAs value=''/>
			<FaxNumber value=''/>
			<Addr1 value=''/>
			<Title value=''/>
			<Addr2 value=''/>
			<Addr3 value=''/>
			<Addr4 value=''/>
			<SexCode codeid='0'/>
			<City value=''/>
			<StateId codeid='0' />
			<ZipCode value=''/>
			<County value=''/>
			<CountryCode codeid='0' />
		</PiEntity>
		<PiRowId value='0'></PiRowId>
		<PiEid codeid='0' />
		<PiTypeCode codeid='240' />
		<MaritalStatCode codeid='' />
		
	</PiOther>

                ");
                    oElement.XPathSelectElement("PiRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("PiList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case " Driver Other":
                    oElement = XElement.Parse(@"
                                <PiOther>
		<EventId value=''></EventId>
		<PiEntity>
			<EntityTableId value='1409'/>
			<LastName value=''></LastName>
			<EntityId value='0'></EntityId>
			<TaxId value=''/>
			<FirstName value=''/>
			<BirthDate value=''/>
			<MiddleName value=''/>
			<Phone1 value=''/>
			<Abbreviation value=''>h</Abbreviation>
			<Phone2 value=''/>
			<AlsoKnownAs value=''/>
			<FaxNumber value=''/>
			<Addr1 value=''/>
			<Title value=''/>
			<Addr2 value=''/>
			<Addr3 value=''/>
			<Addr4 value=''/>
			<SexCode codeid='0'/>
			<City value=''/>
			<StateId codeid='0' />
			<ZipCode value=''/>
			<County value=''/>
			<CountryCode codeid='0' />
		</PiEntity>
		<PiRowId value='0'></PiRowId>
		<PiEid codeid='0' />
		<PiTypeCode codeid='240' />
		<MaritalStatCode codeid='' />
		
	</PiOther>

                ");
                    oElement.XPathSelectElement("PiRowId").Attribute("value").Value = (-1 * p_iRecordCount).ToString();
                    currentPointer = oElement;
                    oElement.Add(AddSupplements(p_sRecordType));
                    oParentEle.XPathSelectElement("PiList").Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Event Records":
                    oElement = XElement.Parse(@"
                  <Event>
                    <EventId value='' /> 
                  </Event>

                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "Policy Tracking Records":
                    oElement = XElement.Parse(@"
<Policy>
<PolicyId value= ''/>
                      </Policy>
                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Patient Records":
                    oElement = XElement.Parse(@"
                <Patient>
<PatientId value= ''/>
  </Patient>
                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Claimant Records":
                    oElement = XElement.Parse(@"
                 <Claimant>
<ClaimantId value= ''/>
</Claimant>

                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Defendant Records":
                    oElement = XElement.Parse(@"
  <Defendant>
                  <DefendantId value= ''/>
 </Defendant>
                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Physician Records":
                    oElement = XElement.Parse(@"
                 <Physician>
<PhysEid value= ''/>
</Physician>
                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Funds Records":
                    oElement = XElement.Parse(@"
                  <Funds>
    <TransId value=''></TransId>   
  </Funds>
               ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Entity Records":
                    oElement = XElement.Parse(@"
           <Entity>
<EntityId value= ''/>
  </Entity>
");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Disability Plan Records":
                    oElement = XElement.Parse(@"
                  <Event>
                    <EventId value='' /> 
                  </Event>

                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Leave Plan Records":
                    oElement = XElement.Parse(@"
         <DisabilityPlan>
<PlanId value= ''/>
  </DisabilityPlan>
                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);

                    oParentEle = currentPointer;
                    break;
                case "Staff Records":
                    oElement = XElement.Parse(@"
                    <Staff>
<StaffId value= ''/>
 </Staff>
                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Vehicle Records":
                    oElement = XElement.Parse(@"
             <Vehicle>
<UnitId value= ''/>
  </Vehicle>
                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Property Records":
                    oElement = XElement.Parse(@"
         <Property>
<PropertyRowId value= ''/>
   </Property>
                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
                case "Policy Management Records":
                    oElement = XElement.Parse(@"
        <PolicyEnh>
<PolicyId value= ''/>
</PolicyEnh>
                ");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "Attachments":
                    oElement = XElement.Parse(@"
                    <Attachment>
                        <Title value='' /> 
                        <FileName value='' /> 
                        <Base64Content value=''/> 
                    </Attachment>
                    
");
                    currentPointer = oElement;
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;

                case "WpaDiaryEntry":
                    oElement = XElement.Parse(@"
                     <WpaDiaryEntry>
                        <EntryId value ='0'/>
                        <CreateDate value =''/>
                        <CompleteDate value =''/>
                        <EntryName value =''/>
                        <EntryNotes value =''/>
                        <AssigningUser value=''/>
                        <AssignedUser value=''/>
                        <AttachTable value =''/>
                        <AttachRecordId value =''/>
                        <IsAttached value='True'/>
                        <StatusOpen value='1'/>
                        <Priority value='Optional'/>
                    </WpaDiaryEntry>
");
                    currentPointer = oElement;
                    //Diary Supplemental - atennyson3/agupta298 JIRA-4691/MITS#36804 - Start
                    oElement.Add(AddSupplements(p_sRecordType));
                    //Diary Supplemental - atennyson3/agupta298 JIRA-4691/MITS#36804 - End
                    oParentEle.Add(oElement);
                    oParentEle = currentPointer;
                    break;
            }

        }

        protected void btnXML_OnClick(object sender, EventArgs e)
        {
            XElement oTemplate = XElement.Parse(@"<ImportDocument><Header><DateCreated value=''/><TimeCreated value=''/><CreatedBy value='' /></Header></ImportDocument>");
            GenerateTemplate(oTemplate, TreeView1.Nodes[0]);
            Response.AppendHeader("content-disposition", "attachment; filename=XMLTemplate.XML");
            Response.Write(oTemplate.ToString());
            Response.End();
        }
        protected void Radio_CheckedChanged(object sender, EventArgs e)
        {
            string sTypeOfRecords = "";
            if (rdDataEntry.Checked) sTypeOfRecords = "DataEntry";
            if (rdAttachments.Checked) sTypeOfRecords = "Attachments";
            if (rdWPADiaries.Checked) sTypeOfRecords = "Diaries";
            TreeView1.Nodes[0].ChildNodes.Clear();
            switch (sTypeOfRecords)
            {
                case "DataEntry":
                    TreeNode oNode = new TreeNode("Event", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    oNode.Checked = true;
                    oNode.Selected = true;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    lblSelectedNodeType.Text = "Event";
                    lblSelectedNodeParentType.Text = "Import";
                    EnableSelectionCriteria();
                    break;

                case "Attachments":
                    oNode = new TreeNode("Claim Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    lblSelectedNodeType.Text = "Claim Records";

                    oNode = new TreeNode("Event Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    //Added by Amitosh 
                    oNode = new TreeNode("Policy Tracking Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Patient Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Claimant Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Defendant Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Physician Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Funds Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Entity Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Disability Plan Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Leave Plan Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Staff Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Vehicle Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Property Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    oNode = new TreeNode("Policy Management Records", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    lblSelectedNodeParentType.Text = "Import";
                    break;

                case "Diaries":
                    oNode = new TreeNode("WpaDiaryEntry", "1");
                    oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    oNode.Checked = true;
                    oNode.Selected = true;
                    TreeView1.Nodes[0].ChildNodes.Add(oNode);
                    lblSelectedNodeType.Text = "WpaDiaryEntry";
                    lblSelectedNodeParentType.Text = "Import";
                    EnableSelectionCriteria();
                    break;
            }
        }

        private void GenerateTemplate(XElement oTemplate, TreeNode oParentNode)
        {
            foreach (TreeNode oNode in oParentNode.ChildNodes)
            {
                if (oNode.Checked)
                {
                    AddTemplate(ref oTemplate, oNode.Text, Conversion.ConvertStrToInteger(oNode.Value));

                    if (oNode.ChildNodes.Count != 0)
                    {
                        GenerateTemplate(oTemplate, oNode);
                    }
                    if (rdDataEntry.Checked)
                    {
                        if (oTemplate.Parent.Parent != null)
                        {
                            oTemplate = oTemplate.Parent.Parent;
                        }
                        else
                        {
                            oTemplate = oTemplate.Parent;  //event case .. there is no eventlist
                        }
                    }
                    else if (rdAttachments.Checked)
                    {
                        oTemplate = oTemplate.Parent;
                    }
                    else if (rdWPADiaries.Checked)
                    {
                        oTemplate = oTemplate.Parent;
                    }
                }

            }
        }

        protected void btnRefresh_OnClick(object sender, EventArgs e)
        {
            int iSelectedNodeCount = Conversion.ConvertStrToInteger(txtSelectedNodeCount.Text);
            sSelectedNodeType = TreeView1.SelectedNode.Text;
            TreeNode oParent = TreeView1.SelectedNode.Parent;
            int iChildCount = oParent.ChildNodes.Count;
            for (int i = 0; i < iChildCount; i++)
            {
                if (oParent.ChildNodes[i].Text == sSelectedNodeType)
                {
                    oParent.ChildNodes.RemoveAt(i--);
                    iChildCount = iChildCount - 1;
                }
            } oParent.SelectAction = TreeNodeSelectAction.None;
            for (int i = 1; i <= iSelectedNodeCount; i++)
            {
                TreeNode oNode = new TreeNode(sSelectedNodeType, i.ToString());
                oNode.Checked = true;
                oNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                ExpandNode(oNode);
                oParent.ChildNodes.Add(oNode);
                oNode.SelectAction = TreeNodeSelectAction.None;
                DisableSelectionCriteria();
            }
            txtSelectedNodeCount.Text = "1";
        }

        private void EnableSelectionCriteria()
        {
            lbl2.Visible = true;
            lbl3.Visible = true;
            lblSelectedNodeType.Visible = true;
            txtSelectedNodeCount.Visible = true;
            btnRefresh.Visible = true;
            lblSelectedNodeParentType.Visible = true;
            lbl9.Visible = true;
        }
        private void DisableSelectionCriteria()
        {
            lbl2.Visible = false;
            lbl3.Visible = false;
            lblSelectedNodeType.Visible = false;
            txtSelectedNodeCount.Visible = false;
            btnRefresh.Visible = false;
            lblSelectedNodeParentType.Visible = false;
            lbl9.Visible = false;
        }

        private XElement AddSupplements(string p_sRecordType)
        {
            XElement oElement = null;
            XMLImportBusinessHelper objXMLHelper = null;
            XMLTemplate objXMLTemplate = null;
            XElement currentPointer = null;
            XMLTemplate objReturn = null;
            XslTransform xslt = null;
            string sresult = string.Empty;

            TextReader txtreader = null;
            XmlTextReader xmltxtreader = null;
            XPathDocument xPathDocument = null;
            StringBuilder sb = null;
            TextWriter txtwriter = null;
            try
            {
                objXMLTemplate = new XMLTemplate();
                objReturn = new XMLTemplate();
                objXMLHelper = new XMLImportBusinessHelper();
                objXMLTemplate.NodeName = p_sRecordType;
                objReturn = objXMLHelper.GetSuppTemplate(objXMLTemplate);
                currentPointer = objReturn.SuppXml.Element("Supplementals");
                currentPointer.RemoveAttributes();


                xslt = new XslTransform();
                xslt.Load(Server.MapPath("~/Content/Xsl/XmlImport.xsl"));



                txtreader = new StringReader(currentPointer.ToString());
                xmltxtreader = new XmlTextReader(txtreader);
                xPathDocument = new XPathDocument(xmltxtreader);

                sb = new StringBuilder();
                txtwriter = new StringWriter(sb);
                xslt.Transform(xPathDocument, null, txtwriter);

                currentPointer = XElement.Parse(sb.ToString());
            }

            catch (Exception e)
            {

            }
            return currentPointer;
        }

    }

}