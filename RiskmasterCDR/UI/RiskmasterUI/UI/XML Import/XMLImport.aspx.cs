﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;
using System.ServiceModel;
//using Riskmaster.UI.XMLImport;
using Riskmaster.Common;
using Riskmaster.BusinessHelpers;
using Riskmaster.Models;

namespace Riskmaster.UI.UI.XML_Import
{
    public partial class XMLImport : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {


        }
        //Added for MITS 29711 by bsharma33
        protected void btnDownLoadLog_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(ViewState["logFileContent"])) == false)
            {
                byte[] pdfbytes = Convert.FromBase64String(Conversion.ConvertObjToStr(ViewState["logFileContent"]));

                Response.Buffer = true;
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = "";
                Response.AppendHeader("Content-Encoding", "none;");
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", string.Format("attachment; filename=XMLImportLog.pdf"));
                Response.AddHeader("Accept-Ranges", "bytes");
                Response.BinaryWrite(pdfbytes);
                Response.Flush();
                Response.Close();
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            long lFileSize = 0;
            XMLImportBusinessHelper objXMLHelper = null;
            XMLInput objXMLInput = null;
            bool bsuccess = false;
            string sFileContent = string.Empty;
            byte[] byteArray;
            MemoryStream stream = null;
            XMLOutPut oXMLOutPut = null;
            StreamReader reader = null;
            try
            {
                byteArray = uploadFile.FileBytes;
                stream = new MemoryStream(byteArray);
                reader = new StreamReader(stream);
                sFileContent = reader.ReadToEnd();
                string logFileContent = string.Empty;//logFileContent Added for MITS 29711 by bsharma33
                lFileSize = byteArray.Length;
               
                if (lFileSize > 1288490188.8)
                {
                    throw new ApplicationException("Size of attached Files is larger than 1.2 GB");
                }
                else
                {
                    objXMLInput = new XMLInput();
                    objXMLHelper = new XMLImportBusinessHelper();
                    objXMLInput.GetXMLInput = sFileContent;
                    bsuccess = objXMLHelper.SaveXML(objXMLInput, out oXMLOutPut);//logFileContent Added for MITS 29711 by bsharma33

                    if (bsuccess)
                    {
                        lbSuccess.Visible = true;
                        ErrorControl1.Visible = false;
                        if (oXMLOutPut.bIsEvent)
                        {
                            ViewState["logFileContent"] = oXMLOutPut.FileContent;//logFileContent Added for MITS 29711 by bsharma33

                            btnDownLoadLog.Visible = true;//logFileContent Added for MITS 29711 by bsharma33
                        }                                
                    }
                }
            }
            catch (FaultException<RMException> ee)
            {
                // ijha: 26406 :Error not visible on the UI Page
                lbSuccess.Visible = false;
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                lbSuccess.Visible = false;
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            finally
            {
                if (objXMLHelper != null)
                {
                    objXMLHelper = null;
                }
                if (objXMLInput != null)
                {
                    objXMLInput = null;
                }
                if (reader != null)
                    reader = null;
                if (stream != null)
                    stream = null;
            }
        }
    }

}