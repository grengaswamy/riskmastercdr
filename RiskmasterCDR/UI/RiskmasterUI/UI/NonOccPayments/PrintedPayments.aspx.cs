﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;

namespace Riskmaster.UI.NonOccPayments
{
    public partial class PrintedPayments : NonFDMBasePageCWS
    {
        public IEnumerable result = null;       
        public XElement rootElement = null; 
        bool bReturnStatus = false;
        string sreturnValue = "";
        public string sClaimantTitle = "";
        public string sClaimNumber = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument objPrintedPayments = new XmlDocument();
            XElement objTitle = null;         
            string sClaimId = "";
            string sSysCmd = "";
            string sTaxArray = "";
          
            sClaimId = Convert.ToString(Request.QueryString["claimid"]);
            hdnclaimid.Text = sClaimId;

            sSysCmd = Convert.ToString(Request.QueryString["syscmd"]);
            hdnsyscmd.Text = sSysCmd;

            sTaxArray = Convert.ToString(Request.QueryString["taxarray"]);
            hdntaxarray.Text = sTaxArray;

            if (!IsPostBack)
            {
                try
                {
                    bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.GetPrintedPayments", out sreturnValue);
                    if (bReturnStatus)
                    {
                        objPrintedPayments.LoadXml(sreturnValue);
                        rootElement = XElement.Parse(objPrintedPayments.OuterXml);

                        objTitle = rootElement.XPathSelectElement("//table");
                        sClaimantTitle = objTitle.Attribute("claimant").Value;
                        sClaimNumber = objTitle.Attribute("claimnumber").Value;                       
                        result = from c in rootElement.XPathSelectElement("//table").Nodes()
                                 select c;
                    }
                }

                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
        }
      
    }
}
