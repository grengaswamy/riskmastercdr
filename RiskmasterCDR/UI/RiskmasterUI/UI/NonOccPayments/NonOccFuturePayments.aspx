﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NonOccFuturePayments.aspx.cs" Inherits="Riskmaster.UI.NonOccPayments.NonOccFuturePayments" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Disability Payments</title>
          <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css">
          <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css">
          <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
          <script language="JavaScript" src="../../Scripts/nonocc.js" type="text/javascript"></script>
          <script src="../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
          <script src="../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
          <script src="../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
          <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
          <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
          <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
          <script type="text/javascript" src="../../Scripts/calendar-alias.js"></script>  
   </head>
  <body>
    <form id="frmData" runat="server">  
        <input type="hidden" name="syscmd" id="syscmd" runat="server"/>          
            <input type="hidden" name="batchid" value="" id="batchid" runat="server"/>
            <input type="hidden" name="claimnumber" id="claimnumber" runat="server"/>
            <input type="hidden" name="claimid" id="claimid" runat="server"/>  
            <input type="hidden" name="taxarray" id="taxarray" runat="server"/>             
            <input type="hidden" name="processpaymentflag" id="processpaymentflag" runat="server"/>
            <input type="hidden" name="amountchange" value="" id="amountchange"/>
            <%--pmittal5 --%>
            <%if (sPaymntFrozenFlag != "")
              { %>
                <input type="hidden" name="paymntfrozen" id="paymntfrozen" value=<%=sPaymntFrozenFlag %> />
            <%} %>
                          
    <%if (rootElement.XPathSelectElement("//table").Attribute("zerorecords") != null)
    {
        if(rootElement.XPathSelectElement("//table").Attribute("zerorecords").Value == "True")
        {%>
            <br/>
		        <div class="errtextheader">Zero payments exist for this claim.</div>
	        <br />
	   <%}%>
    <%}
    else
    {%> 
        <%if (sBatchid != "" && sBatchRestrict == "true")
        {%>
            <div class="formtitle">New Future payments</div>
            <div class="formsubtitle">[<%=sClaimNumber%><%=sClaimantTitle%>]</div>                   	  
            <table cellspacing="1" cellpadding="1" border="0"></table>
            <table>
                <tr>
                 <td><b>Batch #:</b></td>
                 <td><%=sBatchid%></td>
                </tr>
                <tr>
                 <td><b>Payments Beginning:</b></td>
                 <td><%=sBencalcpaystart%></td>
                </tr>
                <tr>
                 <td><b>Payments Through:</b></td>
                 <td><%=sBencalcpayto%></td>
                </tr>
                <tr>
                 <td><b>Transaction Type:</b></td>
                    <td><%=sTranstype%></td>
                </tr>
                <tr>
                 <td><b>Account:</b></td>
                    <td><%=sAccount%></td>
                </tr>
            </table>
            <div id="div3" class="divScroll">
            <table width="100%" cellspacing="0" cellpadding="1" border="1">
            <%int i = 0; foreach (XElement item in result)
            {
                string rowclass = "";
                if (i == 0) rowclass = "msgheader";
                else if (i % 2 == 0) rowclass = "datatd";
                else rowclass = "datatd1";
                i++;                   
               %> 
               <!-- pmittal5 Mits 16214 06/11/09 - Separator for Supplement payment -->
               <%if (item.Attribute("class") == null)
                 {%>                                                                                                     
                   <tr class="">
                     <td colspan="11">
                         <b>Batch <%=item.Attribute("batchid").Value%> , <%=item.Attribute("bencalcpaystart").Value%> - <%=item.Attribute("bencalcpayto").Value%>, <%=item.Attribute("transtype").Value%>, <%=item.Attribute("account").Value%></b>                 
                     </td>
                   </tr>
               <%}%>
               <%else
                   {%> <!--End - pmittal5-->
               <tr class="<%=rowclass%>">    
               <%if (i > 1)
                 {%>
                 <td>
                     <a href="#" onclick="return EditPayment('<%=item.Attribute("paymentnumber").Value%>','<%=item.Attribute("batchid").Value%>','<%=item.Attribute("fromdate").Value%>','<%=item.Attribute("todate").Value%>','<%=item.Attribute("gross").Value%>','<%=item.Attribute("supppayment").Value%>','<%=item.Attribute("pension").Value%>','<%=item.Attribute("ssoffset").Value%>','<%=item.Attribute("oi").Value%>','<%=item.Attribute("othoffset1").Value%>','<%=item.Attribute("othoffset2").Value%>','<%=item.Attribute("othoffset3").Value%>','<%=item.Attribute("psttaxded1").Value%>','<%=item.Attribute("psttaxded2").Value%>','<%=item.Attribute("AutoSplitId").Value%>','<%=item.Attribute("printdate").Value%>')"><%=item.Attribute("paymentnumber").Value%></a>                                                    
                 </td>
               <%}%>
               <%else
                   {%>        
                    <td>
                        <%=item.Attribute("paymentnumber").Value%>
                    </td>
               <%}%>               
               <td>
                    <%=item.Attribute("fromdate").Value%>
               </td>
               <td>
                    <%=item.Attribute("todate").Value%>
               </td>
               <td>
                    <%=item.Attribute("printdate").Value%>
               </td>
               <td>
                    <%=item.Attribute("gross").Value%>
               </td>
               <td>
                     <%=item.Attribute("supppayment").Value%>
               </td>
               <td>
                     <%=item.Attribute("net").Value%>
               </td>  
               <td>
                     <%=item.Attribute("numofpayments").Value%>                
               </td>
               <td>
                     <%=item.Attribute("fed").Value%>  
               </td>   
               <td>
                     <%=item.Attribute("ss").Value%> 
               </td> 
               <td>
                     <%=item.Attribute("med").Value%> 
               </td> 
               <td>
                     <%=item.Attribute("state").Value%> 
               </td> 
               <td>
                     <%=item.Attribute("pension").Value%>  
               </td> 
               <td>
                     <%=item.Attribute("ssoffset").Value%> 
               </td>   
               <td>
                     <%=item.Attribute("oi").Value%>    
               </td>  
               <!-- pmittal5 Mits 14841 04/27/09 - GHS Enhancements-->
               <td>
                     <%=item.Attribute("othoffset1").Value%>    
               </td> 
               <td>
                     <%=item.Attribute("othoffset2").Value%>    
               </td> 
               <td>
                     <%=item.Attribute("othoffset3").Value%>    
               </td>    
               <td>
                     <%=item.Attribute("psttaxded1").Value%>    
               </td>    
               <td>
                     <%=item.Attribute("psttaxded2").Value%>    
               </td>           
               <!--End - pmittal5 -->                 
               <%if (i == 1)
                 {%>
                    <td>
                          Select
                    </td>
                <%}%>                    
                <%else
                   {%>                              
                    <td>
                        <input type="radio" name="chkDelete" value="<%=item.Attribute("paymentnumber").Value%>" batchid="<%=item.Attribute("batchid").Value%>" autosplitid="<%=item.Attribute("AutoSplitId").Value%>"/>
                    </td>
                <%}%>              
        </tr>  
        <%}%>  
    <%}%>
    </table>
    </div>
    <%}%>    
      <%else
      {%>
            <div class="formtitle" id="formtitle">Future payments</div>
            <div class="formsubtitle">[<%=sClaimNumber%><%=sClaimantTitle%>]</div>
            <table cellspacing="1" cellpadding="1" border="0"></table>
            <div id="divForms" class="divScroll">
            <table width="100%" cellspacing="0" cellpadding="1" border="1">
                <%int i = 0; foreach (XElement item in result)
                {
                    string rowclass = "";
                    string batchId = "";
                    
                    if (i == 0) rowclass = "colheader";
                    else rowclass = "rowlight";
                    i++;                   
                    %>                     
                    <%if (item.Attribute("class") == null)
                    {%>                                                                                                     
                        <tr class="">
                              <td colspan="11">
                                <b>Batch <%=item.Attribute("batchid").Value%> , <%=item.Attribute("bencalcpaystart").Value%> - <%=item.Attribute("bencalcpayto").Value%>, <%=item.Attribute("transtype").Value%>, <%=item.Attribute("account").Value%></b>                 
                              </td>
                        </tr>
                   <%}%>
                   <%else
                   {%>                                       
                        <tr class="<%=rowclass%>">                                                    
                        <%if (i > 1)
                        {%>
                            <td>                    
                                <a href="#" onclick="return EditPayment('<%=item.Attribute("paymentnumber").Value%>','<%=item.Attribute("batchid").Value%>','<%=item.Attribute("fromdate").Value%>','<%=item.Attribute("todate").Value%>','<%=item.Attribute("gross").Value%>','<%=item.Attribute("supppayment").Value%>','<%=item.Attribute("pension").Value%>','<%=item.Attribute("ssoffset").Value%>','<%=item.Attribute("oi").Value%>','<%=item.Attribute("othoffset1").Value%>','<%=item.Attribute("othoffset2").Value%>','<%=item.Attribute("othoffset3").Value%>','<%=item.Attribute("psttaxded1").Value%>','<%=item.Attribute("psttaxded2").Value%>','<%=item.Attribute("AutoSplitId").Value%>','<%=item.Attribute("printdate").Value%>')"><%=item.Attribute("paymentnumber").Value%></a>                                      
                            </td>
                       <%}
                       if (i == 1)
                       {%>
                           <td>
                                <%=item.Attribute("paymentnumber").Value%>
                           </td>     
                       <%}%>
                       <td>
                                <%=item.Attribute("fromdate").Value%>
                       </td>
                       <td>
                                <%=item.Attribute("todate").Value%>
                        </td>                    
                        <td>
                                <%=item.Attribute("printdate").Value%>
                        </td>
                        <td>
                                <%=item.Attribute("gross").Value%>
                        </td>
                        <td>
                                <%=item.Attribute("supppayment").Value%>
                        </td>
                        <td>
                                <%=item.Attribute("net").Value%>
                        </td>
                        <td>
                                <%=item.Attribute("numofpayments").Value%>
                        </td>
                        <td>
                                <%=item.Attribute("fed").Value%>
                        </td>
                        <td>
                                <%=item.Attribute("ss").Value%>
                        </td>
                        <td>
                                <%=item.Attribute("med").Value%>
                        </td>
                        <td>
                                <%=item.Attribute("state").Value%>
                        </td>
                        <td>
                                <%=item.Attribute("pension").Value%>
                        </td>
                        <td>
                                <%=item.Attribute("ssoffset").Value%>
                        </td>                    
                        <td>
                                <%=item.Attribute("oi").Value%>
                        </td>
                        <!-- pmittal5 Mits 14841 04/27/09 - GHS Enhancements-->
                        <td>
                                <%=item.Attribute("othoffset1").Value%>    
                        </td> 
                        <td>
                                <%=item.Attribute("othoffset2").Value%>    
                        </td> 
                        <td>
                                <%=item.Attribute("othoffset3").Value%>    
                        </td>    
                        <td>
                                <%=item.Attribute("psttaxded1").Value%>    
                        </td>    
                        <td>
                                <%=item.Attribute("psttaxded2").Value%>    
                        </td>           
                        <!--End - pmittal5 -->  
                        <%if (i == 1)
                        {%>
                            <td>
                                   Select
                            </td>
                        <%}%>                    
                        <%else
                        {%>                              
                            <td>
                                <input type="radio" name="chkDelete" value="<%=item.Attribute("paymentnumber").Value%>" batchid="<%=item.Attribute("batchid").Value%>" autosplitid="<%=item.Attribute("AutoSplitId").Value%>"/>
                            </td>
                        <%}%>
                    </tr>
                    <%}%>
                <%}%>
          </table>
          <%} %>
  <%}%></div><br/>
  
 <asp:Button class="button" runat="server" Text="Delete Selected Payment" ID="btnDelete" OnClientClick = "return deleteSelectedPayments();" onclick="btnDelete_Click"/>
 <asp:Button class="button" runat="server" Text="Delete Current Batch" ID="btnDeleteAll" OnClientClick = "return chkAll(true);" onclick="btnDeleteAll_Click"/>
 <asp:Button class="button" runat="server" Text="Process Manual Payment" ID="btnProcessManualPayment" OnClientClick = "return ManualPaymentDetails();" onclick="btnProcessManualPayment_Click"/>
 <input type="hidden" name="deletelist" value="" id="deletelist" runat="server"/>
 <input type="hidden" name="batchDelete" value="" id="batchDelete" runat="server"/>
 <input type="button" class="button" name="btnClose" value="Close" onclick="window.close();"/> 
 <br/><br/>
 <asp:textbox style="display:none" runat="server" id="hdnclaimid" rmxref="Instance/Document/NonOccPayments/claimid" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpieid" rmxref="Instance/Document/NonOccPayments/pieid" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnsyscmd" rmxref="Instance/Document/NonOccPayments/syscmd" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntaxarray" rmxref="Instance/Document/NonOccPayments/taxarray" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnacct" rmxref="Instance/Document/NonOccPayments/acct" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbatchid" rmxref="Instance/Document/NonOccPayments/batchid" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpaymentnum" rmxref="Instance/Document/NonOccPayments/paymentnum" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbatchrestrict" rmxref="Instance/Document/NonOccPayments/batchrestrict" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnrecalc" rmxref="Instance/Document/NonOccPayments/recalc" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnsysCmdQueue" rmxref="Instance/Document/NonOccPayments/sysCmdQueue" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbenst" rmxref="Instance/Document/NonOccPayments/benst" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbento" rmxref="Instance/Document/NonOccPayments/bento" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpayst" rmxref="Instance/Document/NonOccPayments/payst" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpayto" rmxref="Instance/Document/NonOccPayments/payto" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntranstype" rmxref="Instance/Document/NonOccPayments/transtype" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntaxflags" rmxref="Instance/Document/NonOccPayments/taxflags" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdndeletelist" rmxref="Instance/Document/NonOccPayments/deletelist" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbatchDelete" rmxref="Instance/Document/NonOccPayments/batchDelete" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdngross" rmxref="Instance/Document/NonOccPayments/gross" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnfromdate" rmxref="Instance/Document/NonOccPayments/fromdate" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntodate" rmxref="Instance/Document/NonOccPayments/todate" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnsave" rmxref="Instance/Document/NonOccPayments/save" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnsupppayment" rmxref="Instance/Document/NonOccPayments/supppayment" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnpensionoffset" rmxref="Instance/Document/NonOccPayments/pensionoffset" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnssoffset" rmxref="Instance/Document/NonOccPayments/ssoffset" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnoioffset" rmxref="Instance/Document/NonOccPayments/oioffset" rmxtype="hidden" />   
 <asp:textbox style="display:none" runat="server" id="hdnclaimnumber" rmxref="Instance/Document/NonOccPayments/claimnumber" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndailyamount" rmxref="Instance/Document/NonOccPayments/dailyamount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndailysuppamount" rmxref="Instance/Document/NonOccPayments/dailysuppamount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndPension" rmxref="Instance/Document/NonOccPayments/dPension" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndSS" rmxref="Instance/Document/NonOccPayments/dSS" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndOther" rmxref="Instance/Document/NonOccPayments/dOther" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniDaysWorkingInWeek" rmxref="Instance/Document/NonOccPayments/iDaysWorkingInWeek" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniDaysWorkingInMonth" rmxref="Instance/Document/NonOccPayments/iDaysWorkingInMonth" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniFederalTax" rmxref="Instance/Document/NonOccPayments/iFederalTax" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniSocialSecurityAmount" rmxref="Instance/Document/NonOccPayments/iSocialSecurityAmount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniMedicareAmount" rmxref="Instance/Document/NonOccPayments/iMedicareAmount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniStateAmount" rmxref="Instance/Document/NonOccPayments/iStateAmount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniNetPayment" rmxref="Instance/Document/NonOccPayments/iNetPayment" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniGrossCalculatedPayment" rmxref="Instance/Document/NonOccPayments/iGrossCalculatedPayment" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniGrossCalculatedSupplement" rmxref="Instance/Document/NonOccPayments/iGrossCalculatedSupplement" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniGrossTotalNetOffsets" rmxref="Instance/Document/NonOccPayments/iGrossTotalNetOffsets" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniOffsetCalc" rmxref="Instance/Document/NonOccPayments/iOffsetCalc" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnilblDaysIncluded" rmxref="Instance/Document/NonOccPayments/ilblDaysIncluded" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndSuppRate" rmxref="Instance/Document/NonOccPayments/dSuppRate" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndWeeklyBenefit" rmxref="Instance/Document/NonOccPayments/dWeeklyBenefit" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnautosplitid" rmxref="Instance/Document/NonOccPayments/autosplitid" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnamountchange" rmxref="Instance/Document/NonOccPayments/amountchange" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnprintdate" rmxref="Instance/Document/NonOccPayments/printdate" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdncheckmemo" rmxref="Instance/Document/NonOccPayments/checkmemo" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnselectpaymentrecord" rmxref="Instance/Document/NonOccPayments/selectpaymentrecord" rmxtype="hidden" />  
  <!-- pmittal5 Mits 14841 04/27/09 -GHS enhancements-->
 <asp:textbox style="display:none" runat="server" id="hdnothoffset1" rmxref="Instance/Document/NonOccPayments/othoffset1" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnothoffset2" rmxref="Instance/Document/NonOccPayments/othoffset2" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnothoffset3" rmxref="Instance/Document/NonOccPayments/othoffset3" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnpsttaxded1" rmxref="Instance/Document/NonOccPayments/psttaxded1" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnpsttaxded2" rmxref="Instance/Document/NonOccPayments/psttaxded2" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdndoffset1" rmxref="Instance/Document/NonOccPayments/doffset1" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndoffset2" rmxref="Instance/Document/NonOccPayments/doffset2" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndoffset3" rmxref="Instance/Document/NonOccPayments/doffset3" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndPostTaxDed1" rmxref="Instance/Document/NonOccPayments/dPostTaxDed1" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndPostTaxDed2" rmxref="Instance/Document/NonOccPayments/dPostTaxDed2" rmxtype="hidden" />
 <!-- End - pmittal5 --> 
 <uc1:ErrorControl ID="ErrorControl" runat="server" />  
  <input type="text" style="display:none" runat="server" id="transidfuture" /><!--igupta3 Mits 28566-->
  </form>
  </body>
</html>