﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;

namespace Riskmaster.UI.NonOccPayments
{
    public partial class EditPayment : NonFDMBasePageCWS
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        public string sClaimantTitle = "";
        public string sClaimNumber = "";
        public string sFromDate = "";
        public string sToDate = "";
        public string sSuppPayment = "";
        public string sPensionoffset = "";
        public string sPrintdate = "";
        public string sSsoffset = "";
        public string sOioffset = "";
        public string sGross = "";
        public string sGrossTotNetOffset = "";
        public string sBatchId = "";
        public string sPaymentNum = "";
        public string sAutoSplitId = "";
        public string sNetPaymentTitle = "";
        public string sNetPaymentVal = "";
        public string sFederalTaxTitle = "";
        public string sFederalTaxVal = "";
        public string sSocialSecurityTaxTitle = "";
        public string sSocialSecurityTaxVal = "";
        public string sMedicareTaxTitle = "";
        public string sMedicareTaxVal = "";
        public string sStateTaxTitle = "";
        public string sStateTaxVal = "";
        public string sCheckMemo = "";
        public bool sRecordExists = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            sBatchId = AppHelper.GetQueryStringValue("BatchId");
            hdnbatchid.Text = sBatchId;
            sPaymentNum = AppHelper.GetQueryStringValue("PaymentNumber");
            hdnpaymentnum.Text = sPaymentNum;
            sAutoSplitId = AppHelper.GetQueryStringValue("autosplitid");
            hdnautosplitid.Text = sAutoSplitId;
            hdnclaimnumber.Text = AppHelper.GetQueryStringValue("claimnumber");

            if (!IsPostBack)
            {
                try
                {
                    suppgross.Value = AppHelper.GetQueryStringValue("SuppPayment");
                    pensionoffset.Value = AppHelper.GetQueryStringValue("pensionoffset");
                    ssoffset.Value = AppHelper.GetQueryStringValue("ssoffset");
                    oioffset.Value = AppHelper.GetQueryStringValue("oioffset");
                    //pmittal5 Mits 14841 04/27/09 -GHS enhancements
                    othoffset1.Value = AppHelper.GetQueryStringValue("othoffset1");
                    othoffset2.Value = AppHelper.GetQueryStringValue("othoffset2");
                    othoffset3.Value = AppHelper.GetQueryStringValue("othoffset3");
                    psttaxded1.Value = AppHelper.GetQueryStringValue("psttaxded1");
                    psttaxded2.Value = AppHelper.GetQueryStringValue("psttaxded2");
                    //End - pmittal5
                    LoadEditPageData("PageLoad");
                }

                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }

        protected void btnRecalc_Click(object sender, EventArgs e)
        {
            hdnsyscmd.Text = "9";
            hdnbatchid.Text = sBatchId;
            hdnrecalc.Text = "-1";
            hdngross.Text = gross.Value;
            hdnsupppayment.Text = suppgross.Value;    //pmittal5 03/31/09
            hdnfromdate.Text = fromdate.Value;
            hdntodate.Text = todate.Value;
            hdnpensionoffset.Text = pensionoffset.Value;
            hdnssoffset.Text = ssoffset.Value;
            hdnoioffset.Text = oioffset.Value;
            //pmittal5 Mits 14841 04/27/09 -GHS enhancements
            hdnothoffset1.Text = othoffset1.Value;
            hdnothoffset2.Text = othoffset2.Value;
            hdnothoffset3.Text = othoffset3.Value;
            hdnpsttaxded1.Text = psttaxded1.Value;
            hdnpsttaxded2.Text = psttaxded2.Value;
            //End - pmittal5
            hdnautosplitid.Text = sAutoSplitId;
            hdnamountchange.Text = amountchange.Value;
            hdnprintdate.Text = printdate.Value;
            hdncheckmemo.Text = checkmemo.Value;

            LoadEditPageData("Save");

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            hdnsyscmd.Text = "9";
            hdnbatchid.Text = sBatchId;
            hdnrecalc.Text = "0 ";
            hdngross.Text = gross.Value;     
            hdnsupppayment.Text = suppgross.Value;  //pmittal5 03/31/09
            hdnfromdate.Text = fromdate.Value;
            hdntodate.Text = todate.Value;
            hdnpensionoffset.Text = pensionoffset.Value;
            hdnssoffset.Text = ssoffset.Value;
            hdnoioffset.Text = oioffset.Value;
            //pmittal5 Mits 14841 04/27/09 -GHS enhancements
            hdnothoffset1.Text = othoffset1.Value;
            hdnothoffset2.Text = othoffset2.Value;
            hdnothoffset3.Text = othoffset3.Value;
            hdnpsttaxded1.Text = psttaxded1.Value;
            hdnpsttaxded2.Text = psttaxded2.Value;
            //End - pmittal5
            hdnautosplitid.Text = sAutoSplitId;
            hdnamountchange.Text = amountchange.Value; 
            hdnprintdate.Text = printdate.Value;
            hdncheckmemo.Text = checkmemo.Value;

            LoadEditPageData("Save");

        }

        private void LoadEditPageData(string p_sPageAction)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            XElement objTitle = null;
            XmlDocument objFuturePayments = new XmlDocument();

            if (p_sPageAction == "PageLoad")
            {
                bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.EditPayment", out sreturnValue);
            }
            else
            {
                bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.SaveEdit", out sreturnValue);
            }

            if (bReturnStatus)
            {
                objFuturePayments.LoadXml(sreturnValue);
                rootElement = XElement.Parse(objFuturePayments.OuterXml);
                objTitle = rootElement.XPathSelectElement("//table");
                if (objTitle != null)
                {
                    sClaimantTitle = objTitle.Attribute("claimant").Value;
                    sClaimNumber = objTitle.Attribute("claimnumber").Value;
                    formsubtitle.InnerText = "[" + sClaimNumber + sClaimantTitle + "]";             //pmittal5 04/01/09
                    gross.Value = objTitle.Attribute("gross").Value;
                    if (objTitle.Attribute("supppayment").Value != "")
                        suppgross.Value = objTitle.Attribute("supppayment").Value;
                    sGrossTotNetOffset = objTitle.Attribute("grosstotalnetoffsets").Value;
                    GrossTotNetOffset.InnerText = objTitle.Attribute("grosstotalnetoffsets").Value; //pmittal5 04/01/09
                    checkmemo.Value = objTitle.Attribute("checkmemo").Value;
                    fromdate.Value = objTitle.Attribute("fromdate").Value;
                    todate.Value = objTitle.Attribute("todate").Value;
                    printdate.Value = objTitle.Attribute("printdate").Value;
                    if (rootElement.XPathSelectElement("//table").Nodes() != null)
                    {
                        result = from objNode in rootElement.XPathSelectElement("//table").Nodes()
                                 select objNode;
                        //pmittal5 04/01/09
                        DataTable objTable = new DataTable();
                        DataRow objrow;
                        objTable.Columns.Add("Title");
                        objTable.Columns.Add("Value");
                        objTable.Columns.Add("Id");
                        foreach (XElement item in result)
                        {
                            objrow = objTable.NewRow();
                            if (item.Attribute("title") != null && item.Attribute("value") != null && item.Attribute("id") != null)
                            {
                                objrow["Title"] = item.Attribute("title").Value;
                                objrow["Value"] = item.Attribute("value").Value;
                                objrow["Id"] = item.Attribute("id").Value;
                                objTable.Rows.Add(objrow);
                            }
                        }
                        GridViewPayment.Visible = true;
                        GridViewPayment.DataSource = objTable;
                        GridViewPayment.DataBind();
                        //pmittal5 04/01/09
                        sRecordExists = true;
                    }
                    else
                    {
                        sRecordExists = false;
                    }
                }
                else
                {
                    sRecordExists = false;
                }
            }

        }
    }
}
