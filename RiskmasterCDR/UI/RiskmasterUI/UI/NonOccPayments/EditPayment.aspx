﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditPayment.aspx.cs" Inherits="Riskmaster.UI.NonOccPayments.EditPayment" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head runat="server">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>   
  <title>Disability Payments</title>
  <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css"/>
  <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
  <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
  <script language="JavaScript" src="../../Scripts/nonocc.js" type="text/javascript"></script>
  <script src="../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
  <script src="../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
  <script src="../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
  <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
  <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
  <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
  <script type="text/javascript" src="../../Scripts/calendar-alias.js"></script>
</head>
 <body >
  <form name="frmData" id="frmData" runat="server">
  <input type="hidden" name="syscmd" value="" id="syscmd"/>
  <input type="hidden" name="claimnumber"  runat="server" id="claimnumber"/>
  <input type="hidden" runat="server" name="amountchange" value="" id="amountchange"/>
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />  
  <div class="formtitle" id="formtitle">Edit Payment</div>
  <%--pmittal5 04/01/09--%>
  <%if (sClaimNumber != "" && sClaimantTitle != "")
    { %>
   <div class="formsubtitle">[<%=sClaimNumber%><%=sClaimantTitle%>]</div>
   <%}
    else
    {%>
    <div class="formsubtitle" id="formsubtitle" runat="server"></div>
    <%} %>
    <%--pmittal5 04/01/09--%>
    <table>
                  <tr>
                    <td>
                      <b>Batch #:</b>
                    </td>
                    <td><%=sBatchId%></td>
                  </tr>
                  <tr>
                    <td>
                      <b>Payment Number:</b>
                    </td>
                    <td><%=sPaymentNum%></td>
                  </tr>
                  <tr>
                    <td>
                      <b>Beginning:</b>
                    </td>
                    <td>
                      <input type="text" name="fromdate" runat="server" id="fromdate" size="10" onblur="dateLostFocus(this.id);"/>
                        <input type="button" class="DateLookupControl" id="btnPickDate" value="..."/>
                          <script type="text/javascript">
                            Calendar.setup(
                            {
                            inputField : "fromdate",
                            ifFormat : "%m/%d/%Y",
                            button : "btnPickDate"
                            }
                            );
                          </script>
                    </td>
                    <td>
                      <b>Through:</b>
                    </td>
                    <td>
                      <input type="text" name="todate" runat="server" id="todate" size="10" onblur="dateLostFocus(this.id);"/>
                        <input type="button" class="DateLookupControl" id="btnPickDate2" value="..."/>
                          <script type="text/javascript">
                            Calendar.setup(
                            {
                            inputField : "todate",
                            ifFormat : "%m/%d/%Y",
                            button : "btnPickDate2"
                            }
                            );
                          </script>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <b>Gross Calculated Payment:</b>
                    </td>
                    <td>
                      <input name="gross" id="gross" runat="server" onblur="numLostFocus(this);" size="30" onchange ="setDataChanged_NonOccAsp(true , true);"/>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <b>Gross Calculated Supplement:</b>
                    </td>
                    <%if (suppgross.Value == "0" || suppgross.Value == "0.00" || suppgross.Value == "")
                    {%>
                    <td>0.00</td>
                    <%}
                    else
                    { %>
                    <td>
                      <input name="suppgross"  id="suppgross" runat="server" onblur="numLostFocus(this);" size="30" onchange ="setDataChanged_NonOccAsp(true , true);"/>
                    </td>
                    <%}%>      
                  </tr>
                  <tr>
                    <td>
                      <b>Pension Offset:</b>
                    </td>
                    <%if (pensionoffset.Value == "0" || pensionoffset.Value == "0.00" || pensionoffset.Value == "")
                    {%>
                    <td>0.00</td>
                    <%}
                    else
                    { %>
                    <td>
                      <input name="pensionoffset" runat="server" id="pensionoffset" onblur="numLostFocus(this);" size="30" onchange ="setDataChanged_NonOccAsp(true , true);"/>
                    </td>
                    <%}%>                    
                  </tr>
                  <tr>
                    <td>
                      <b>Social Security Offset:</b>
                    </td>
                    <%if (ssoffset.Value == "0" || ssoffset.Value == "0.00" || ssoffset.Value == "")
                    {%>
                    <td>0.00</td>
                    <%}
                    else
                    { %>
                    <td>
                      <input name="ssoffset" id="ssoffset" runat="server" onblur="numLostFocus(this);" size="30" onchange ="setDataChanged_NonOccAsp(true , true);"/>
                    </td>
                    <%}%>                      
                  </tr>
                  <tr>
                    <td>
                      <b>Other Income Offset:</b>
                    </td>
                     <%if (oioffset.Value == "0" || oioffset.Value == "0.00" || oioffset.Value == "")
                    {%>
                    <td>0.00</td>
                    <%}
                    else
                    { %>
                    <td>
                      <input name="oioffset" id="oioffset" runat="server"  onblur="numLostFocus(this);" size="30" onchange ="setDataChanged_NonOccAsp(true , true);"/>
                    </td>
                    <%}%>           
                  </tr>
                  <tr>
                  <!--pmittal5 Mits 14841 04/27/09 - GHS Enhancements -->
                  <tr>
                    <td>
                      <b>Other Offset #1:</b>
                    </td>
                     <%if (othoffset1.Value == "0" || othoffset1.Value == "0.00" || othoffset1.Value == "")
                    {%>
                    <td>0.00</td>
                    <%}
                    else
                    { %>
                    <td>
                      <input name="othoffset1" id="othoffset1" runat="server"  onblur="numLostFocus(this);" size="30" onchange ="setDataChanged_NonOccAsp(true , true);"/>
                    </td>
                    <%}%>           
                  </tr>
                  <tr>
                    <td>
                      <b>Other Offset #2:</b>
                    </td>
                     <%if (othoffset2.Value == "0" || othoffset2.Value == "0.00" || othoffset2.Value == "")
                    {%>
                    <td>0.00</td>
                    <%}
                    else
                    { %>
                    <td>
                      <input name="othoffset2" id="othoffset2" runat="server"  onblur="numLostFocus(this);" size="30" onchange ="setDataChanged_NonOccAsp(true , true);"/>
                    </td>
                    <%}%>           
                  </tr>
                  <tr>
                    <td>
                      <b>Other Offset #3:</b>
                    </td>
                     <%if (othoffset3.Value == "0" || othoffset3.Value == "0.00" || othoffset3.Value == "")
                    {%>
                    <td>0.00</td>
                    <%}
                    else
                    { %>
                    <td>
                      <input name="othoffset3" id="othoffset3" runat="server"  onblur="numLostFocus(this);" size="30" onchange ="setDataChanged_NonOccAsp(true , true);"/>
                    </td>
                    <%}%>           
                  </tr>
                  <tr>
                    <td>
                      <b>Post Tax Deduction #1:</b>
                    </td>
                     <%if (psttaxded1.Value == "0" || psttaxded1.Value == "0.00" || psttaxded1.Value == "")
                    {%>
                    <td>0.00</td>
                    <%}
                    else
                    { %>
                    <td>
                      <input name="psttaxded1" id="psttaxded1" runat="server"  onblur="numLostFocus(this);" size="30" onchange ="setDataChanged_NonOccAsp(true , true);"/>
                    </td>
                    <%}%>           
                  </tr>
                  <tr>
                    <td>
                      <b>Post Tax Deduction #2:</b>
                    </td>
                     <%if (psttaxded2.Value == "0" || psttaxded2.Value == "0.00" || psttaxded2.Value == "")
                    {%>
                    <td>0.00</td>
                    <%}
                    else
                    { %>
                    <td>
                      <input name="psttaxded2" id="psttaxded2" runat="server"  onblur="numLostFocus(this);" size="30" onchange ="setDataChanged_NonOccAsp(true , true);"/>
                    </td>
                    <%}%>           
                  </tr>
                  <!--End - pmittal5 Mits 14841-->
                  <tr>
                    <td>
                      <b>Gross Total Net Offsets:</b>
                    </td>
                    <%--pmittal5 04/01/09--%>
                    <%if (sGrossTotNetOffset != "")
                    {%> 
                    <td><%=sGrossTotNetOffset%></td> 
                    <%}
                    else
                    {%>
                    <td>
                      <label name="GrossTotNetOffset" id="GrossTotNetOffset" runat="server" />
                    </td>
                    <%}%> 
                     <%-- pmittal5 04/01/09--%>
                  </tr>  
                 <%if (sRecordExists)
                   { %>             
                  <%foreach (XElement item in result)
                    {
                        if (item.Attribute("title") != null && item.Attribute("value") != null && item.Attribute("id") != null)
                        {
                        %>
                            <tr>           
                             <td>
                                   <b><%=item.Attribute("title").Value%></b>
                             </td>
                             <td>
                                    <%=item.Attribute("value").Value%>
                             </td> 
                             <td>
                              <input type="hidden" id="<%=item.Attribute("id").Value%>"  value="<%=item.Attribute("value").Value%>"/>
                            </td>
                           </tr>
                        <%}%>
                   <%}%>
                   <%}%>
                   <%--pmittal5 04/01/09--%>
                  <%else
                    {%>
                      <tr>
                        <td colspan="2">
                        <asp:GridView ID="GridViewPayment" AutoGenerateColumns="False" runat="server" CellPadding="0"  CellSpacing="-1" 
                                      Width="100%" BorderWidth="0" ShowHeader="false" >
                            <Columns>
                                <asp:TemplateField ItemStyle-Font-Bold="true" ItemStyle-Width="42%">
                                    <ItemTemplate>
                                        <%# Eval("Title")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Font-Bold="false">
                                    <ItemTemplate>
                                        <%# Eval("Value")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <input type="hidden" id='<%# Eval("Id")%>' value='<%# Eval("Value")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView> 
                       </td>
                      </tr>                     
                   <%}%>
                   <%-- pmittal5 04/01/09--%>
                  <tr>
                    <td>
                      <b>Print Date:</b>
                    </td>
                    <td>
                      <input type="text" name="printdate" runat="server" id="printdate" size="10" onblur="dateLostFocus(this.id);"/>
                        <input type="button" class="DateLookupControl" id="btnPickDate3" value="..."/>
                          <script type="text/javascript">
                            Calendar.setup(
                            {
                            inputField : "printdate",
                            ifFormat : "%m/%d/%Y",
                            button : "btnPickDate3"
                            }
                            );
                          </script>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <b>Check Memo:</b>
                    </td>
                    <td>
                      <input type="text" name="checkmemo" id="checkmemo" runat="server" size="40"/>
                    </td>
                  </tr>
                </table>
                <br/>
                  
  <asp:Button class="button" runat="server" Text="Save" ID="btnSave" OnClientClick = "return doEdit('0','<%=sAutoSplitId%>')" onclick="btnSave_Click" />
  <asp:Button class="button" runat="server" Text="Recalculate Gross" ID="btnRecalc" OnClientClick = "return doEdit('-1','<%=sAutoSplitId%>')" onclick="btnRecalc_Click" />
  <input type="hidden" name="recalc" value="" id="recalc"/>
  <input type="submit" name="btnClose" value="Close" class="button" id="btnClose" onclick="return cancelEdit()"/>
 
	
 <asp:textbox style="display:none" runat="server" id="hdnclaimid" rmxref="Instance/Document/NonOccPayments/claimid" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpieid" rmxref="Instance/Document/NonOccPayments/pieid" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnsyscmd" rmxref="Instance/Document/NonOccPayments/syscmd" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntaxarray" rmxref="Instance/Document/NonOccPayments/taxarray" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnacct" rmxref="Instance/Document/NonOccPayments/acct" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbatchid" rmxref="Instance/Document/NonOccPayments/batchid" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpaymentnum" rmxref="Instance/Document/NonOccPayments/paymentnum" rmxtype="hidden" />           
  <asp:textbox style="display:none" runat="server" id="hdnbatchrestrict" rmxref="Instance/Document/NonOccPayments/batchrestrict" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnrecalc" rmxref="Instance/Document/NonOccPayments/recalc" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnsysCmdQueue" rmxref="Instance/Document/NonOccPayments/sysCmdQueue" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbenst" rmxref="Instance/Document/NonOccPayments/benst" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbento" rmxref="Instance/Document/NonOccPayments/bento" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpayst" rmxref="Instance/Document/NonOccPayments/payst" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpayto" rmxref="Instance/Document/NonOccPayments/payto" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntranstype" rmxref="Instance/Document/NonOccPayments/transtype" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntaxflags" rmxref="Instance/Document/NonOccPayments/taxflags" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdndeletelist" rmxref="Instance/Document/NonOccPayments/deletelist" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbatchDelete" rmxref="Instance/Document/NonOccPayments/batchDelete" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdngross" rmxref="Instance/Document/NonOccPayments/gross" rmxtype="hidden" />          
 <asp:textbox style="display:none" runat="server" id="hdnfromdate" rmxref="Instance/Document/NonOccPayments/fromdate" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntodate" rmxref="Instance/Document/NonOccPayments/todate" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnsave" rmxref="Instance/Document/NonOccPayments/save" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnsupppayment" rmxref="Instance/Document/NonOccPayments/supppayment" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnpensionoffset" rmxref="Instance/Document/NonOccPayments/pensionoffset" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnssoffset" rmxref="Instance/Document/NonOccPayments/ssoffset" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnoioffset" rmxref="Instance/Document/NonOccPayments/oioffset" rmxtype="hidden" />   
 <!-- pmittal5 Mits 14841 04/27/09 -GHS enhancements-->
 <asp:textbox style="display:none" runat="server" id="hdnothoffset1" rmxref="Instance/Document/NonOccPayments/othoffset1" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnothoffset2" rmxref="Instance/Document/NonOccPayments/othoffset2" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnothoffset3" rmxref="Instance/Document/NonOccPayments/othoffset3" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnpsttaxded1" rmxref="Instance/Document/NonOccPayments/psttaxded1" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnpsttaxded2" rmxref="Instance/Document/NonOccPayments/psttaxded2" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdndoffset1" rmxref="Instance/Document/NonOccPayments/doffset1" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndoffset2" rmxref="Instance/Document/NonOccPayments/doffset2" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndoffset3" rmxref="Instance/Document/NonOccPayments/doffset3" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndPostTaxDed1" rmxref="Instance/Document/NonOccPayments/dPostTaxDed1" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndPostTaxDed2" rmxref="Instance/Document/NonOccPayments/dPostTaxDed2" rmxtype="hidden" />
 <!-- End - pmittal5 --> 
 <asp:textbox style="display:none" runat="server" id="hdnclaimnumber" rmxref="Instance/Document/NonOccPayments/claimnumber" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndailyamount" rmxref="Instance/Document/NonOccPayments/dailyamount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndailysuppamount" rmxref="Instance/Document/NonOccPayments/dailysuppamount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndPension" rmxref="Instance/Document/NonOccPayments/dPension" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndSS" rmxref="Instance/Document/NonOccPayments/dSS" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndOther" rmxref="Instance/Document/NonOccPayments/dOther" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniDaysWorkingInWeek" rmxref="Instance/Document/NonOccPayments/iDaysWorkingInWeek" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniDaysWorkingInMonth" rmxref="Instance/Document/NonOccPayments/iDaysWorkingInMonth" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniFederalTax" rmxref="Instance/Document/NonOccPayments/iFederalTax" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniSocialSecurityAmount" rmxref="Instance/Document/NonOccPayments/iSocialSecurityAmount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniMedicareAmount" rmxref="Instance/Document/NonOccPayments/iMedicareAmount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniStateAmount" rmxref="Instance/Document/NonOccPayments/iStateAmount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniNetPayment" rmxref="Instance/Document/NonOccPayments/iNetPayment" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniGrossCalculatedPayment" rmxref="Instance/Document/NonOccPayments/iGrossCalculatedPayment" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniGrossCalculatedSupplement" rmxref="Instance/Document/NonOccPayments/iGrossCalculatedSupplement" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniGrossTotalNetOffsets" rmxref="Instance/Document/NonOccPayments/iGrossTotalNetOffsets" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniOffsetCalc" rmxref="Instance/Document/NonOccPayments/iOffsetCalc" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnilblDaysIncluded" rmxref="Instance/Document/NonOccPayments/ilblDaysIncluded" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndSuppRate" rmxref="Instance/Document/NonOccPayments/dSuppRate" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndWeeklyBenefit" rmxref="Instance/Document/NonOccPayments/dWeeklyBenefit" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnautosplitid" rmxref="Instance/Document/NonOccPayments/autosplitid" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnamountchange" rmxref="Instance/Document/NonOccPayments/amountchange" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnprintdate" rmxref="Instance/Document/NonOccPayments/printdate" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdncheckmemo" rmxref="Instance/Document/NonOccPayments/checkmemo" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnselectpaymentrecord" rmxref="Instance/Document/NonOccPayments/selectpaymentrecord" rmxtype="hidden" />  
 <%--<uc1:ErrorControl ID="ErrorControl" runat="server" />  --%>
  </form>
 </body>
</html>
