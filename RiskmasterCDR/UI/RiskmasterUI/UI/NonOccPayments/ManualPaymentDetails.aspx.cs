﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;

namespace Riskmaster.UI.NonOccPayments
{
    public partial class ManualPaymentDetails : NonFDMBasePageCWS
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        public bool bReturnStatus = false;
        public string sreturnValue = "";   
        public string sAutoSplitId = "";
        public string sCheckDate = "";
        public string sCheckNumber = "";
        public string sAccountName = "";
        XmlDocument objManualPayments = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {     
            sAutoSplitId = AppHelper.GetQueryStringValue("autosplitid");
            hdnautosplitid.Text = sAutoSplitId;          

            try
            {
                if (!Page.IsPostBack)
                {
                    LoadManualPayments();            
                }
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        protected void cmdProcess_Click(object sender, EventArgs e)
        {
            hdnchecknumber.Text = chknumber.Value;
            hdncheckdate.Text = chkDate.Value;
            hdnautotransid.Text = autotransid.Value;
            hdnaccountid.Text = accountid.Value;
            
            bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.ProcessManualPayments", out sreturnValue);
            objManualPayments.LoadXml(sreturnValue);

            if (bReturnStatus)           
            {
                manualpaymentprocessedflag.Value = objManualPayments.SelectSingleNode("//ManualPaymentProcessedFlag").InnerText;
                transiddetail.Value = objManualPayments.SelectSingleNode("//TransID").InnerText; //igupta3 Mits: 28566
            }           
        }


        private void LoadManualPayments()
        {
            bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.ManualPaymentDetails", out sreturnValue);

            if (bReturnStatus)
            {
                objManualPayments.LoadXml(sreturnValue);

                chksetupaccount.Value = objManualPayments.SelectSingleNode("//AccountName").InnerText;
                chknumber.Value = objManualPayments.SelectSingleNode("//CheckNumber").InnerText;
                chkDate.Value = objManualPayments.SelectSingleNode("//CheckDate").InnerText;
                autotransid.Value = objManualPayments.SelectSingleNode("//AutoTransID").InnerText; ;
                accountid.Value = objManualPayments.SelectSingleNode("//AccountID").InnerText;                
            }
        }
    }
}
