﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using System.Text;

namespace Riskmaster.UI.NonOccPayments
{
    public partial class NonOccFuturePayments : NonFDMBasePageCWS
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        public bool bReturnStatus = false;
        public string sreturnValue = "";
        public string sClaimantTitle = "";
        public string sClaimNumber = "";            
        public string sBatchRestrict = "";
        public string sBatchid = "";
        public string sBencalcpaystart = ""; 
        public string sBencalcpayto = "";             
        public string sTranstype = ""; 
        public string sAccount = "";
        public string sPaymntFrozenFlag = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hdnclaimid.Text = AppHelper.GetQueryStringValue("claimid");
                hdnsyscmd.Text = AppHelper.GetQueryStringValue("syscmd");
                hdntaxarray.Text = AppHelper.GetQueryStringValue("taxarray");
                taxarray.Value = hdntaxarray.Text;
                claimnumber.Value = AppHelper.GetQueryStringValue("claimnumber"); ;
                claimid.Value = AppHelper.GetQueryStringValue("claimid");
                hdnbatchid.Text = AppHelper.GetQueryStringValue("batchid");
                sBatchRestrict = AppHelper.GetQueryStringValue("batchrestrict");
                hdnbatchrestrict.Text = sBatchRestrict;
            }
 
            
            XElement XmlTemplate = null;
            try
            {
                if (!(Page.IsPostBack) || processpaymentflag.Value == "True")
                {
                    //igupta3 Mits :28566 Changes Starts
                    XmlTemplate = GetMessageTemplate();
                    if (transidfuture.Value!="")
                    {
                        bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.CreateWorkLossRestrictions",out sreturnValue, XmlTemplate);
                    }
                    //igupta3 Mits :28566 Changes ends
                  LoadFuturePayments();                 
                }
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }          
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DeletePayments();
            LoadFuturePayments();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            DeletePayments();
            LoadFuturePayments();
        }

        protected void btnProcessManualPayment_Click(object sender, EventArgs e)
        {
               
        }

        private void DeletePayments()
        {            
            hdndeletelist.Text = deletelist.Value;
            hdnbatchDelete.Text = batchDelete.Value;
            bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.DeletePayments", out sreturnValue); 
        }


        private void LoadFuturePayments()
        {
            XmlDocument objFuturePayments = new XmlDocument();
            XElement objTitle = null;

            bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.GetFuturePayments", out sreturnValue);
            
            if (bReturnStatus)
            {
                objFuturePayments.LoadXml(sreturnValue);
                rootElement = XElement.Parse(objFuturePayments.OuterXml);

                objTitle = rootElement.XPathSelectElement("//table");

                if (objTitle.Attribute("claimant") != null)
                {
                    sClaimantTitle = objTitle.Attribute("claimant").Value;
                }
                if (objTitle.Attribute("claimnumber") != null)
                {
                    sClaimNumber = objTitle.Attribute("claimnumber").Value;
                }
                
                if (objTitle.Attribute("batchrestrict") != null)
                {
                    sBencalcpaystart = objTitle.Attribute("bencalcpaystart").Value;
                    sBencalcpayto = objTitle.Attribute("bencalcpayto").Value;                                              
                    sTranstype = objTitle.Attribute("transtype").Value;
                    sAccount = objTitle.Attribute("account").Value;
                    sBatchid = objTitle.Attribute("batchid").Value;
                }
                //pmittal5
                if (objTitle.Attribute("paymntfrozenflag") != null)
                {
                    sPaymntFrozenFlag = objTitle.Attribute("paymntfrozenflag").Value;
                }

                result = from objNode in rootElement.XPathSelectElement("//table").Nodes()
                         select objNode;
            }
        }

        //igupta3 Mits :28566 Changes Starts
        private XElement GetMessageTemplate()
        {   
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><TransId>");
            sXml = sXml.Append(Convert.ToString(transidfuture.Value));
            sXml = sXml.Append("</TransId></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        //igupta3 Mits :28566 Changes ends
    }
}
