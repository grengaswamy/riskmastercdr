﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RePrintCheck.aspx.cs" Inherits="Riskmaster.UI.UI.ReCreateCheck.RePrintCheck" %>

<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
 <base target=_self>
    <title></title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/RecreateChecks.js")%>'></script>
    
    <script language='javascript' type="text/javascript">

        function checkReadyState() {

            if (document.forms[0].hdnFileRequest.value == "1") {
                //JIRA RMA-9120 - ajohari2 Start

                var strreadyState = null;
                if (document.readyState != null) {
                    strreadyState = document.readyState;
                }
                //while (document.all.file.document.readyState != "complete") {
                while (strreadyState != "complete") {
                    status += "."; // just wait ...
                }
                //JIRA RMA-9120 - ajohari2 End

                pleaseWait.pleaseWait('stop');
            }
        }


        function submitthisPage() {
            if (document.forms[0].hdnFirstTime != null) {
                if (document.forms[0].hdnFirstTime.value == "0") {
                    document.forms[0].hdnFirstTime.value = "1";
                    document.forms[0].hdnPrintRequest.value = "1";
                    document.forms[0].submit();
                    pleaseWait.Show();

                }
            }
        }   
    </script>
 <%-- aravi5 RMA-10227 Re-Create Check File -Show modal issue starts --%>
    <script type="text/javascript">
        function HideOverlaydiv() {
            var IEbrowser = false || !!document.documentMode; // At least IE6
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.opener.parent.parent.document.getElementById("overlaydiv").remove();
                }
                return false;
            }
        }
    </script>
    <%-- aravi5 RMA-10227 Re-Create Check File -Show modal issue ends --%>
</head>
<body onpagehide="HideOverlaydiv(); return false;" onload="javascript:submitthisPage();" > <%-- aravi5 RMA-10227 --%>
    <form id="frmData" runat="server">
                   <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />  
       				<table width="100%" cellspacing="0" cellpadding="2" border="0">  
   					<input type='hidden' id="StartNum" runat=server ></input>
					<input type='hidden' id="EndNum" runat=server ></input>
					<input type='hidden' id="hdnBatchPrint" runat=server ></input>
					<input type='hidden' id="hdnFileRequest" runat=server ></input>
					<input type='hidden' id="hdnPrintRequest" runat=server  value="0"></input>
					<input type='hidden' id="hdnFileName" runat=server ></input>
					<input type='hidden' id="hdnFileType" runat=server ></input>
					<input type='hidden' id="hdnModelXml" runat=server ></input>
					<input type='hidden' id="hdnPrintCheckDetails" runat=server ></input>
					<%try
       {
           %>
       
					<% if (Model.SelectSingleNode("//PrintCheckDetails") != null)
        {%>
					<tr>
						<td colspan="2">
							&#160;
						</td>
					</tr>
					<tr class="msgheader">
						<td colspan="2">
							<!--Checks and EOB Reports -->
						</td>
					</tr>
					
					<%if (Model.SelectSingleNode("//PrintChecks").Attributes["PrintDirectlyToPrinter"].Value == "false")
       { %>
						<%if (Model.SelectSingleNode("//ExportToFile").InnerText == "0" || Model.SelectSingleNode("//ExportToFile").InnerText == "2")
        {%>
							<tr>
					
								<td>
								<!--The Following Check and EOB reports are attached with this check 
								Batch:-->
                                    Re-printed to the file successfully.
								</td>
							</tr>
							<%foreach (XmlNode node in Model.SelectNodes("//File"))
         {
           
             int iCount = 0;%>
						<tr>
									<td>
										<div style="position:relative;width:420px;height:70px;overflow:auto" class="divScroll">
											<table>
												<ul>
													
														<tr>
															<td width="5%">
															</td>
															<td>
																<li>
																
																<a  id='<%=node.Attributes["Name"].Value %>' href="javascript:PrintSelectedReport('<%=node.Attributes["FileType"].Value %>','<%=node.Attributes["FileName"].Value %>')" >
																<%if (node.Attributes["Name"].Value != "AddlDetail" && node.Attributes["Name"].Value != "PrintCheck")
                                                                    {%> EOB Report For Claim Number : <%=node.Attributes["Name"].Value %><%} %>
                                                                <%else if (node.Attributes["Name"].Value == "AddlDetail") {%>  Additional Details for Check Batch<%} %>
                                                                <%else if (node.Attributes["Name"].Value == "PrintCheck") {%> <%#CheckSingleOrCheckBatch()%><%} %>
                                                                </a>
															
																</li>				
															</td>
															
														</tr>
													
												</ul>
											</table>
										</div>
									</td>
								</tr>
							<%iCount++;%>
<%         } %>
<%         } %>

        <%else
                        {%>
                            <%--npadhy JIRA RMA-10227 - As we do not have any distribution type for EFT, and we hard code a distribution type for EFT and use it in Recreate 
                 For EFT we do not have Distribution Type, but it will always be printed using file system. But we need to keep track of a print option which can be used while Reprint--%>
            <%if (Model.SelectSingleNode("//ExportToFile").InnerText == "1" || Model.SelectSingleNode("//ExportToFile").InnerText == "3" || Model.SelectSingleNode("//ExportToFile").InnerText == "4" || Model.SelectSingleNode("//ExportToFile").InnerText == "-999")
              {%>
            <tr>
								<td >
									<br></br>
									<%#CheckSingleOrCheckBatch()%>
									Re-printed to the file successfully.
									<br></br><br></br>
								</td>
							</tr>
             <%         } %>
        
        <%         } %>
			<%         } %>
			<%else
                        { %>	
				<tr>
					
								<td>
                                Re-printed to the file successfully.
								<!--Checks and EOB reports are printed sucessfully.-->
								</td>
								</tr>
			<%         } %>	
			
			<tr>
						<td >
						
							<asp:Button ID="btnOk" runat="server"  OnClientClick="return PrintBatchOk();"
             Text="   Ok   "  CssClass=button  onclick="btnOk_Click" UseSubmitBehavior="true"
              /> 														
						</td>							
					</tr>
					<tr>
					<%         } %>	
			<%        Model = null; %>	
					
					   <asp:HiddenField ID="hdnIsBatchPrint" runat="server" />
              <asp:HiddenField ID="hdnCheckId" runat="server" />
           <asp:HiddenField ID="hdnBatchId" runat="server" />
           <asp:HiddenField ID="hdnAccountId" runat="server" />
            <asp:HiddenField ID="hdnCheckDate" runat="server" />
        <asp:HiddenField ID="hdnIncludeAutoPayment" runat="server" />   
        <asp:HiddenField ID="hdnCheckStockId" runat="server" />   
        <asp:HiddenField ID="hdnFirstCheckNum" runat="server" />   
         <asp:HiddenField ID="hdnOrderBy" runat="server" />
          <asp:HiddenField ID="hdnNumAutoChecksToPrint" runat="server" />   
        <asp:HiddenField ID="hdnUsingSelection" runat="server" />   
        <asp:HiddenField ID="hdnType" runat="server" />   
        <asp:HiddenField ID="hdnIncludeClaimantInPayee" runat="server" />  
        <%--npadhy RMA-11632 Starts - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks--%> 
        <asp:HiddenField ID="hdnDistributionType" runat="server" />
        <%--npadhy RMA-11632 Ends - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks--%> 
         <asp:HiddenField ID="hdnFirstTime" runat="server"  Value="0"/>
         <asp:HiddenField ID="hdnFileNameAndType" runat="server"  Value="0"/>
         
         
					</td>
					</tr>
					<tr>
					<td>
			<iframe frameborder="0" width="0" height="0" src="" id="file"></iframe>
					
					 <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"   CustomMessage="Printing" />
					 </td>
					</tr>
				</table>
				<%}
       catch (Exception ee)
       {
           ErrorHelper.logErrors(ee);
           lblErrors.Text = ErrorHelper.FormatErrorsForUI("Error in printing checks");
       }%>	
        <asp:Label ID="lblErrors" runat=server></asp:Label>		
    </form>
</body>
</html>
