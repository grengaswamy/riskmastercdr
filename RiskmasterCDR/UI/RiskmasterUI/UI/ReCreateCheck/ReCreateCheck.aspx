<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false"  CodeBehind="ReCreateCheck.aspx.cs" Inherits="Riskmaster.UI.UI.ReCreateCheck.ReCreateCheck" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"  TagPrefix="uc2" %>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Re-Print Checks</title>
	<link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
	<link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
	<link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

	<script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>

	<script language="JavaScript" src="../../Scripts/drift.js" type="text/javascript"></script>

	
	<script language="javaScript" src="../../Scripts/ReCreateChecks.js" type="text/javascript"></script>

	<script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">	    { var i; }
	</script>

     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>


	<%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">	    { var i; }
	</script>
	<script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">	    { var i; }
	</script>
	<script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">	    { var i; }
	</script>
	<script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">	    { var i; }
	</script>
	<script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">	    { var i; }
	</script>
	<script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">	    { var i; }
	</script>
	<script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">	    { var i; }
	</script>
--%>
	<script language="JavaScript" src="../../Scripts/rmx-common-ref.js" type="text/javascript">	    { var i; }
	</script>
 
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="PrintCheckOnLoad1()" >
	<form id="frmData" name="frmData" method="post" runat="server">
	<div>
		<rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
<%--        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />--%>
		<asp:HiddenField runat="server" ID="HiddenField1" Value="" />
		<asp:TextBox Style="display: none" runat="server" name="hTabName" ID="TextBox1" />
		<div class="msgheader" id="div_formtitle" runat="server">
			<asp:Label ID="Label1" runat="server" Text="<%$ Resources:lblrecreatecheckfile %>" /> 
			<asp:Label ID="formsubtitle" runat="server" Text="" />
		</div>
		<div id="Div1" class="errtextheader" runat="server">
			<asp:Label ID="formdemotitle" runat="server" Text="" />
		</div>
	</div>
	<br />
	<table border="0">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="NotSelected" nowrap="true" name="TABSprintcheckbatch" id="TABSprintcheckbatch">
							<a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="printcheckbatch"
								id="LINKTABSprintcheckbatch"><span style="text-decoration: none">
                                <asp:Label ID="recreatechkfile" runat="server" Text="<%$ Resources:lblrecreatecheckfile1 %>" />
                                </span></a>
						</td>
						<td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
							border-top: none;">
							&nbsp;&nbsp;
						</td>
					
						<td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
							border-top: none;">
							&nbsp;&nbsp;
						</td>
						<td valign="top" nowrap="true">
						</td>
					</tr>
				</table>
				<div class="singletopborder" style="position: relative; left: 0; top: 0; width: 800px;
					height: 315px; overflow: auto">
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								
								<table border="0" cellspacing="0" cellpadding="0" name="FORMTABprintcheckbatch" id="FORMTABprintcheckbatch"
									style="display: none;">
									<tr id="">
										<td>
                                        <asp:Label ID="bankaccount" runat="server" Text="<%$ Resources:lblbankaccount %>" />

											&nbsp;&nbsp;
										</td>
										<td>
											<asp:DropDownList ID="bankaccountprint" runat="server" TabIndex="0" onchange="BankAccountChange_ReCreateCheck('print');" AutoPostBack="true" >
											</asp:DropDownList>
										</td>
									</tr>
                                    <%--npadhy RMA-11632 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks--%>  
                                    <tr id="Tr1">
                                        <td>
                                            Distribution Type:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDistributionTypePrint" runat="server" AutoPostBack ="true" onchange="BankAccountChange_ReCreateCheck('print');">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
									<%--npadhy JIRA 11632 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks--%>  
										<%--<tr id="Tr1">
										<td>
											Check Stock:&nbsp;&nbsp;
										</td>
										<td>
											<asp:DropDownList ID="checkstock" runat="server" onchange="setDataChanged(true);">
											</asp:DropDownList>
										</td>
									</tr>--%>
								
								
									<tr id="">
										<td>
                                         <asp:Label ID="chekdate" runat="server" Text="<%$ Resources:lblcheckdate %>" />
											&nbsp;&nbsp;
										</td>
										<td>
										<!--pmittal5 Mits 17988 10/07/09 - Removed Readonly attribute and added button to select date.
											Control should be readonly only if "Allow Post Date" is enabled. -->
												<%--<asp:TextBox runat="server" size="30" ID="checkdate" RMXRef="/Instance/Document//CheckDate"
												TabIndex="3" ReadOnly="true" Style="background-color:silver"/>--%>
												<asp:TextBox runat="server" size="30" ID="checkdate" RMXRef="/Instance/Document//CheckDate"
												TabIndex="3" onblur="dateLostFocus(this.id);" onchange="validateCheckDate(this.id)"/>
												<%--<input runat="server" class="button" id="checkdatebtn" type="button" value="..." />--%>
												
												<script type="text/javascript">
												    $(function () {
												        $("#checkdate").datepicker({
												            showOn: "button",
												            buttonImage: "../../Images/calendar.gif",
												            buttonImageOnly: true,
												            showOtherMonths: true,
												            selectOtherMonths: true,
												            changeYear: true
												        });
												    });
                                             </script>
										<!-- End - pmittal5-->
										</td>
									</tr>
									<tr id="">
										<td>
                                        <asp:Label ID="chkbatch" runat="server" Text="<%$ Resources:lblcheckbatch %>" />
											&nbsp;&nbsp;
										</td>
										<td>
											<div title="" style="padding: 0px; margin: 0px">
													 <asp:TextBox runat="server" size="30" RMXRef="/Instance/Document//CheckBatchChangedPre/CheckBatch"
													TabIndex="4" ID="checkbatchpre" onchange="setDataChanged(true);" onblur="CheckBatchChangedPre()" />
													</div>
										</td>
									</tr>
									<tr id="">
										<td>
                                        <asp:Label ID="prechekdate" runat="server" Text="<%$ Resources:lblprecheckdate %>" />
											&nbsp;&nbsp;
										</td>
										<td>
												<asp:TextBox runat="server" size="30" ID="precheckdate" RMXRef="/Instance/Document//DatePreCheck"
												TabIndex="5" ReadOnly="true" Style="background-color:silver"/>
										</td>
									</tr>
									<tr id="">
										<td>
                                         <asp:Label ID="numchecks" runat="server" Text="<%$ Resources:lblnumchecks %>" />
											&nbsp;&nbsp;
										</td>
										<td>
												<asp:TextBox runat="server" size="30" ID="numberofchecks" RMXRef="/Instance/Document//CheckBatchChangedPre/NumberOfChecks"
												TabIndex="6" ReadOnly="true" Style="background-color:silver"/>
										</td>
									</tr>
									<tr id="">
										<td>
                                         <asp:Label ID="orderfield" runat="server" Text="<%$ Resources:lblorderfield %>" />
											&nbsp;&nbsp;
										</td>
										<td>
												<asp:TextBox runat="server" size="30" ID="orderfieldbatch" RMXRef="/Instance/Document//OrderPreCheck"
												TabIndex="7" ReadOnly="true" Style="background-color:silver"/>
										</td>
									</tr>
									<tr id="">
										<td colspan="2">

											<script type="text/javascript" language="JavaScript" src="">{var i;}</script>

											<asp:Button runat="server" class="button" ID="printbatch" OnClientClick="PrintBatch()"
												Text="<%$ Resources:btnreprint %> "  TabIndex="9" />
										</td>
									</tr>
								</table>
								
							</td>
							<td valign="top">
							</td>
						</tr>
					</table>
				</div>
				<table>
					<tr>
						<td>
						</td>
					</tr>
				</table>
				   <asp:TextBox runat="server"  ID="txtIsPrinterSelected"  style="display: none"/>                                                
				<asp:TextBox runat="server" style="display: none" id="fromdate" rmxref="/Instance/Document//FromDate" />
				<asp:TextBox runat="server" style="display: none" id="todate" rmxref="/Instance/Document//ToDate" />
				<%-- Debabrata Biswas Batch Printing Filter R6 Retrofit MITS 19715/20050 Date: 03/17/2010--%>
<%--<asp:TextBox runat="server" style="display: none" id="orghierarchy" />--%>
				<asp:TextBox runat="server" value="" style="display: none" id="selectedchecksids"/>
				<asp:TextBox runat="server" value="" style="display: none" id="selectedautochecksids"/>

				<asp:TextBox runat="server" style="display: none" id="printpreflag" rmxref="/Instance/Document//PrintFlag" />
				<asp:TextBox runat="server" style="display: none" id="printbatchflag" rmxref="/Instance/Document//PrintBatch" />
				<asp:TextBox runat="server" style="display: none" id="printpostflag" rmxref="/Instance/Document//PrintPost" />

				<asp:TextBox runat="server" value="" style="display: none" id="insufficientfund" rmxref="/Instance/Document//InsufficientFund" />
				<asp:TextBox runat="server" value="False" style="display: none" id="usesubbankaccounts" rmxref="/Instance/Document//UseFundsSubAccounts"/>
				<asp:TextBox runat="server" value="" style="display: none" id="FirstFailedCheckNumber" />
				<asp:TextBox runat="server" value="" style="display: none" id="PrintCheckDetails" />
				<asp:TextBox runat="server" value="" style="display: none" id="FileNameAndType" />
				<asp:TextBox runat="server" value="" style="display: none" id="EOBFilesNames" />
				<!--pmittal5 Mits 17988 10/07/09 - Added control to store the value of CheckDateType node returned from webservice-->
				<asp:TextBox runat="server" value="" style="display: none" id="CheckDateType" rmxref="/Instance/Document//CheckDateType"/>
				<input type="text" runat="server" value="PrintChecksAdaptor.OnLoad" style="display: none"
					id="functiontocall" />
					<input type="hidden" name="hdnShowPleaseWait" id="hdnShowPleaseWait" />
					<asp:TextBox runat="server" style="display: none" id="fileandconsolidate"  rmxref="/Instance/Document//FileAndConsolidate"/>
					 <input type="hidden" name="hTabName" id="hTabName"  runat=server  value="printcheckbatch" />
			</td>
		</tr>
		<tr>
			<td>
			 <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"  CustomMessage=""/>
			</td>
			</tr>
			<tr>
			<td>
			<script language=javascript type="text/javascript">
				document.forms[0].onsubmit = showmessage;

				function showmessage() {

					if (document.forms[0].hdnShowPleaseWait.value == "")
						pleaseWait.Show();
				}
				function PrintCheckOnLoad1() {
					//    CloseProgressWindow();
					parent.MDIScreenLoaded();
					loadTabList();
					tabChange(window.document.forms[0].hTabName.value);
						 

					if (window.document.forms[0].printbatchflag.value == "True" )
						window.document.forms[0].printbatch.disabled = false;
					else
						window.document.forms[0].printbatch.disabled = true;

				   

			 

					if (window.document.forms[0].txtIsPrinterSelected.value == "false") {
						document.getElementById('formdemotitle').innerText = "You need to select a default printer in payment parameter setup or change the option 'Print Direct To Printer' off.";
						window.document.forms[0].printeob.disabled = true;
						window.document.forms[0].printbatch.disabled = true;
					}


					if (window.document.forms[0].functiontocall.value == "PrintChecksAdaptor.GetInsufficientFundsPreEditFlag") {
						if (window.document.forms[0].insufficientfund.value == "True") {
							OpenInsufficientFundWindow();
							window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.InsufficientFundsPreEdit";
						}
						else {
							OpenPreCheckDetailWindow();
							window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.PreCheckDetail";
						}
					}
					//pmittal5 Mits 17988 10/07/09 - Enable/disable Check Date depending on the Allow Post Date flag in Check Options
					if (document.forms[0].CheckDateType != null) {
						if (document.forms[0].CheckDateType.value == "readonly") {
							if (document.forms[0].checkdate != null) {
								document.forms[0].checkdate.readOnly = true;
								document.forms[0].checkdate.style.backgroundColor = "silver";
							}
							if (document.forms[0].checkdatebtn != null) {
								document.forms[0].checkdatebtn.style.display = "none";
							}
						}
					}
					//end - pmittal5

				}
	</script>
			</td>
			</tr>
	</table>
	</form>
</body>
</html>
