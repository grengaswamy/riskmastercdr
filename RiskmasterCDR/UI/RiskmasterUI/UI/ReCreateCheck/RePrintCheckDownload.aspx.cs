﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;

namespace Riskmaster.UI.UI.ReCreateCheck
{
    public partial class RePrintCheckDownload : System.Web.UI.Page
    {
        private string FileRequestMessageTemplate = "<Message><Authorization></Authorization><Call>" +
         "<Function>PrintChecksAdaptor.GetPrintCheckFile</Function></Call><Document><PrintChecks><FileName/><FileType/></PrintChecks></Document></Message>";
        private string DeletePostEobMessageTemplate = "<Message><Authorization></Authorization><Call>" +
"<Function>PrintEOBAdaptor.DeleteEobFiles</Function></Call><Document><PostCheckEOB><FileNames/></PostCheckEOB></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (AppHelper.GetQueryStringValue("deleteEOBFiles") == "true" && AppHelper.GetQueryStringValue("name") != "")
                    {
                        DeleteFile();
                    } // if
                    else
                    {
                        GetFile();
                    } // else
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            DataBind();
        }
        private void GetFile()
        {
            FileRequestMessageTemplate = AppHelper.ChangeMessageValue(FileRequestMessageTemplate, "//FileName", AppHelper.GetQueryStringValue("name"));
            FileRequestMessageTemplate = AppHelper.ChangeMessageValue(FileRequestMessageTemplate, "//FileType", AppHelper.GetQueryStringValue("type"));
            string sReturn = AppHelper.CallCWSService(FileRequestMessageTemplate.ToString());
            XmlDocument objFile = new XmlDocument();
            objFile.LoadXml(sReturn);

            byte[] pdfbytes = Convert.FromBase64String(objFile.SelectSingleNode("//File").InnerText);

            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            Response.AppendHeader("Content-Encoding", "none;");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=report.PDF");
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(pdfbytes);
            Response.Flush();
            Response.Close();
        }

        protected void DeleteFile()
        {
            DeletePostEobMessageTemplate = AppHelper.ChangeMessageValue(DeletePostEobMessageTemplate, "//FileNames", AppHelper.GetQueryStringValue("name"));
            string sReturn = AppHelper.CallCWSService(DeletePostEobMessageTemplate.ToString());
            XmlDocument objFile = new XmlDocument();
            objFile.LoadXml(sReturn);
        }
    }
}
