﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChecksFailedToReprint.aspx.cs" Inherits="Riskmaster.UI.UI.ReCreateCheck.ChecksFailedToReprint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Print Check</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/PrintChecks.js")%>'></script>
    
</head>
<body>
    <form id="frmData" runat="server">
    <input type="hidden" runat=server  id="StartNum"><input type="hidden"  runat=server  id="EndNum"><table width="100%" cellspacing="0" cellpadding="2" border="0">
    <tr>
     <td>
      								&nbsp;
      							
     </td>
    </tr>
    <tr class="msgheader">
     <td>
      								Checks Failed To Print
      							
     </td>
    </tr>
    <tr>
     <td><BR>The following checks were printed:
      								<asp:Label ID="lblStartNum" runat=server></asp:Label>
      								through
      								<asp:Label ID="lblEndNum" runat=server></asp:Label>
     </td>
    </tr>
    <tr>
     <td><BR>Enter the number of first check printed which failed
      								and press Ok. Choosing Cancel will mark all checks
      								as printed successfully.								
      							
     </td>
    </tr>
    <tr>
     <td><BR> First Failed Check Number: 
      								<input type="text"  runat=server  id="txtFirstFailedCheckNumber" size="20"></td>
    </tr>
   </table>
   <table width="17%">
    <tr>
     <td width="5%"><BR><input type="button"  value="   Ok   " class="button" id="Ok" onclick="return ChecksFailedToPrintOk(); "></td>
     <td width="5%"><BR><input type="button" value="Cancel" class="button" id="Cancel" onclick="return ChecksFailedToPrintCancel(); "></td>
    </tr>
   </table
    </form>
</body>
</html>
