﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.UI.UI.ReCreateCheck
{
    public partial class RePrintCheck : System.Web.UI.Page
    {
        public XmlDocument Model = null;
        //260e44e7-61ea-4324-ac7c-876a6621ef8b
        //34880aa1-ee69-4766-8c95-2afd5fe434d2
        // npadhy RMA-11632 Starts - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks
        private string MessageTemplate = "<Message><Authorization></Authorization><Call>" +
            "<Function>PrintChecksAdaptor.PrintChecksBatch</Function></Call><Document><PrintChecks><CheckId /><BatchNumber></BatchNumber><AccountId></AccountId><IncludeAutoPayment></IncludeAutoPayment><UsingSelection>false</UsingSelection><CheckStockId></CheckStockId><FirstCheckNum></FirstCheckNum><OrderBy></OrderBy><CheckDate></CheckDate><NumAutoChecksToPrint></NumAutoChecksToPrint><IncludeClaimantInPayee /><IsReCreate>true</IsReCreate><DistributionType></DistributionType></PrintChecks></Document></Message>";
        // npadhy RMA-11632 Ends - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks
        private string FileRequestMessageTemplate = "<Message><Authorization></Authorization><Call>" +
           "<Function>PrintChecksAdaptor.GetPrintCheckFile</Function></Call><Document><PrintChecks><FileName/><FileType/></PrintChecks></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            Model = new XmlDocument();
            try
            {
                if (!Page.IsPostBack)
                {
                    StoreQueryStringValues();
                }
                else
                {
                    if (hdnFileRequest.Value == "1")
                    {
                        hdnFileRequest.Value = "0";
                        GetFile();
                    }
                    if (hdnPrintRequest.Value == "1")
                    {
                        hdnPrintRequest.Value = "0";
                        MessageTemplate = CreateServiceCall();
                        string sReturn = AppHelper.CallCWSService(MessageTemplate.ToString());
                        Model.LoadXml(sReturn);
                        SetValuesFromService();
                        hdnModelXml.Value = Model.OuterXml;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            DataBind();

        }

        private void GetFile()
        {
            if (hdnFileRequest.Value == "1")
            {
                Model.LoadXml(hdnModelXml.Value);
            }

            FileRequestMessageTemplate = AppHelper.ChangeMessageValue(FileRequestMessageTemplate, "//FileName", hdnFileName.Value);
            FileRequestMessageTemplate = AppHelper.ChangeMessageValue(FileRequestMessageTemplate, "//FileType", hdnFileType.Value);
            string sReturn = AppHelper.CallCWSService(FileRequestMessageTemplate.ToString());
            XmlDocument objFile = new XmlDocument();
            objFile.LoadXml(sReturn);

            byte[] pdfbytes = Convert.FromBase64String(objFile.SelectSingleNode("//File").InnerText);

            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            Response.AppendHeader("Content-Encoding", "none;");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=report.PDF");
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(pdfbytes);
            Response.Flush();
            Response.Close();
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {

        }
        public string CheckSingleOrBatch()
        {
            if (hdnBatchPrint.Value == "SingleCheck")
            {
                return "Check";
            }
            else
            {
                return "Checks for Batch";
            }
        }
        public string CheckSingleOrCheckBatch()
        {
            if (hdnBatchPrint.Value == "SingleCheck")
            {
                return "Check";
            }
            else
            {
                return "Checks";
            }
        }
        public string ChangeTitle()
        {
            if (hdnBatchPrint.Value == "SingleCheck")
            {
                return "Print Check ";
            }
            else
            {
                return "Print Check Batch";
            }
        }
        private void SetValuesFromService()
        {
            StartNum.Value = AppHelper.GetValueFromDOM(Model, "StartNum");
            EndNum.Value = AppHelper.GetValueFromDOM(Model, "EndNum");
            hdnFileNameAndType.Value = AppHelper.GetValueFromDOM(Model, "@FileNameAndType");
            hdnPrintCheckDetails.Value = AppHelper.GetInnerXmlFromDOM(Model, "PrintCheckDetails");
        }
        private void StoreQueryStringValues()
        {
            hdnBatchPrint.Value = AppHelper.GetQueryStringValue("IsBatchPrint"); ;

            hdnCheckId.Value = AppHelper.GetQueryStringValue("CheckId");

            hdnBatchId.Value = AppHelper.GetQueryStringValue("BatchId");

            hdnAccountId.Value = AppHelper.GetQueryStringValue("AccountId");

            hdnCheckDate.Value = AppHelper.GetQueryStringValue("CheckDate");

            hdnIncludeAutoPayment.Value = AppHelper.GetQueryStringValue("IncludeAutoPayments");

            hdnCheckStockId.Value = AppHelper.GetQueryStringValue("CheckStockId");

            hdnFirstCheckNum.Value = AppHelper.GetQueryStringValue("FirstCheck");

            hdnOrderBy.Value = AppHelper.GetQueryStringValue("OrderBy");

            hdnNumAutoChecksToPrint.Value = AppHelper.GetQueryStringValue("NumAutoChecksToPrint");

            hdnUsingSelection.Value = AppHelper.GetQueryStringValue("UsingSelection");

            hdnIncludeClaimantInPayee.Value = AppHelper.GetQueryStringValue("IncludeClaimantInPayee");

            // npadhy RMA-11632 Starts - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks
            hdnDistributionType.Value = AppHelper.GetQueryStringValue("DistributionType");
            // npadhy RMA-11632 Ends - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks

        }
        private string CreateServiceCall()
        {
            string IsBatchPrint = AppHelper.GetFormValue("hdnBatchPrint");
            hdnBatchPrint.Value = IsBatchPrint;
            string CheckId = AppHelper.GetFormValue("hdnCheckId");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//CheckId", CheckId);
            string CheckBatch = AppHelper.GetFormValue("hdnBatchId");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//BatchNumber", CheckBatch);
            string BankAccount = AppHelper.GetFormValue("hdnAccountId");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//AccountId", BankAccount);
            string CheckDate = AppHelper.GetFormValue("hdnCheckDate");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//CheckDate", CheckDate);
            string IncludeAutoPaymentsFlag = AppHelper.GetFormValue("hdnIncludeAutoPayment");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//IncludeAutoPayment", IncludeAutoPaymentsFlag);
            string CheckStock = AppHelper.GetFormValue("hdnCheckStockId");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//CheckStockId", CheckStock);
            string FirstCheck = AppHelper.GetFormValue("hdnFirstCheckNum");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//FirstCheckNum", FirstCheck);
            string OrderBy = AppHelper.GetFormValue("hdnOrderBy");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//OrderBy", OrderBy);
            string NumAutoChecksToPrint = AppHelper.GetFormValue("hdnNumAutoChecksToPrint");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//NumAutoChecksToPrint", NumAutoChecksToPrint);
            string UsingSelection = AppHelper.GetFormValue("hdnUsingSelection");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//UsingSelection", UsingSelection);
            string IncludeClaimantInPayee = AppHelper.GetFormValue("hdnIncludeClaimantInPayee");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//IncludeClaimantInPayee", IncludeClaimantInPayee);
            // npadhy RMA-11632 Starts - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks
            string DistributionType = AppHelper.GetFormValue("hdnDistributionType");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//DistributionType", DistributionType);
            // npadhy RMA-11632 Ends - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks
            return MessageTemplate;
        }
    }
}