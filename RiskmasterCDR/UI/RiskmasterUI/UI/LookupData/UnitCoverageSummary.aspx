﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnitCoverageSummary.aspx.cs" Inherits="Riskmaster.UI.LookupData.UnitCoverageSummary" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Unit Coverage Summary</title>
	<link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
	<script language="javascript" src="../../Scripts/form.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      



    <asp:GridView ID="grdUnitCovSummary" runat="server" 
        CellPadding="4 " onrowdatabound="grdUnitCovSummary_RowDataBound" OnSorting="GridView_Sorting" AllowSorting="true" Width="100%" >
        <HeaderStyle CssClass="colheader6"/>


    </asp:GridView>


	<label id="lblNoRecord" runat="server"></label>				
    </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    <asp:HiddenField EnableViewState ="true" runat="server" ID="GridSortExpression" />
	<asp:HiddenField EnableViewState ="true" runat="server" ID="GridSortDirection" Value="ASC" />
    </form>
</body>
</html>
