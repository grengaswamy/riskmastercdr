﻿/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 11/04/2014 | 34279  | ajohari2   | Changes req for Claimant lookup
**********************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.LookupData
{
    public partial class LookUpFDM : NonFDMBasePageCWS
    {
        ArrayList arrIndex = null;
        string m_sData = "";
        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        string ParentValues;
        //Ankit End
		 
		 string PageID = string.Empty;
        //dbisht6 omig issue
        int PolicySysID = 0;
        //dbisht6 end
        override protected void OnInit(EventArgs e)
        {
            bool bReturnStatus = false;
            //dbisht6 omig issue
            bool success = false;
            //dbisht6 end        
            string sreturnValue = "";
            this.ddlSort.SelectedIndexChanged += new EventHandler(ddlSort_SelectedIndexChanged);
            this.ddlPage.SelectedIndexChanged += new EventHandler(ddlPage_SelectedIndexChanged);
            this.ddlOrderMode.SelectedIndexChanged += new EventHandler(ddlSort_SelectedIndexChanged);
            //this.lnkFirst.Click += new EventHandler(LinkCommand);
            //this.lnkNext.Click += new EventHandler(LinkCommand);
            //this.lnkLast.Click += new EventHandler(LinkCommand);
            //this.lnkPrev.Click += new EventHandler(LinkCommand);
            // this.lnkPrev.Click+=new EventHandler(lnkPrev_Click);

            try
            {
                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                if (Request.QueryString["parentvalues"] != null)
                {
                    ParentValues = Request.QueryString["parentvalues"];
                }
                //Ankit End
                if (!IsPostBack)
                {
                    if (Request.QueryString["form"] != null)
                    {
                        TextBox txtpageName = (TextBox)this.Form.FindControl("form");
                        if (txtpageName != null)
                            txtpageName.Text = Request.QueryString["form"];
                    }
                    if (Request.QueryString["fpid"] != null)
                    {
                        TextBox txtfpid = (TextBox)this.Form.FindControl("fpid");
                        if (txtfpid != null)
                            txtfpid.Text = Request.QueryString["fpid"];

                    }
                    if (Request.QueryString["psid"] != null)
                    {
                        TextBox txtpsid = (TextBox)this.Form.FindControl("psid");
                        if (txtpsid != null)
                            txtpsid.Text = Request.QueryString["psid"];
                    }
                    if (Request.QueryString["sid"] != null)
                    {
                        TextBox txtsid = (TextBox)this.Form.FindControl("sid");
                        if (txtsid != null)
                            txtsid.Text = Request.QueryString["sid"];
                    }
                    if (Request.QueryString["sys_ex"] != null)
                    {
                        TextBox txtsysex = (TextBox)this.Form.FindControl("sys_ex");
                        if (txtsysex != null)
                            txtsysex.Text = Request.QueryString["sys_ex"];
                    }
                    if (Request.QueryString["claimid"] != null)
                    {
                        TextBox txtclaimid = (TextBox)this.Form.FindControl("claimid");
                        if (txtclaimid != null)
                            txtclaimid.Text = Request.QueryString["claimid"];
                    }
                }
                //dbisht6 omig issue
                if (Request.QueryString["PolicySystemID"] != null)
                {
                    PolicySysID = Conversion.CastToType<int>(Request.QueryString["PolicySystemID"], out success);
                }
                //dbisht6 end

                for (int i = 0; i < Request.QueryString.Count; i++)
                {
                    if (Request.QueryString.AllKeys[i] != "form" && Request.QueryString.AllKeys[i] != "fpid"
                       && Request.QueryString.AllKeys[i] != "psid" && Request.QueryString.AllKeys[i] != "sid"
                       && Request.QueryString.AllKeys[i] != "claimid" && Request.QueryString.AllKeys[i] != "claimid"
                       && Request.QueryString.AllKeys[i] != "sys_ex" && Request.QueryString.AllKeys[i] != "ShowAll"
                        && Request.QueryString.AllKeys[i] != "recordcount" && Request.QueryString.AllKeys[i] != "pagecount"
                        && Request.QueryString.AllKeys[i] != "pagenumber" && Request.QueryString.AllKeys[i] != "fpidname"
                        && Request.QueryString.AllKeys[i] != "OrderBy" && Request.QueryString.AllKeys[i] != "OrderMode")    
                    {
                        TextBox txtAttributes = new TextBox();
                        txtAttributes.ID = Request.QueryString.AllKeys[i];
                        txtAttributes.Attributes["rmxref"] = "Instance/Document/Data/" + Request.QueryString.AllKeys[i] + "/@value";
                        txtAttributes.Attributes["rmxretainvalue"] = "true";
                        txtAttributes.Text = Request.QueryString[i];
                        txtAttributes.Visible = false;
                        this.Form.Controls.Add(txtAttributes);
                        txtAttributes.Dispose();


                    }
                }
                if (!IsPostBack)
                {
                    TextBox txtpagecount = (TextBox)this.Form.FindControl("pagecount");
                    bReturnStatus = CallCWSFunctionBind("LookupDataAdaptor.GetLookupData", out sreturnValue);
                    m_sData = sreturnValue;
                    if (bReturnStatus)
                    {
                        for (int intcount = 1; intcount <= Convert.ToInt32(txtpagecount.Text); intcount++)
                        {
                            ListItem LI = new ListItem();
                            LI.Value = Convert.ToString(intcount);

                            LI.Text = Convert.ToString(intcount) + " Of " + txtpagecount.Text;
                            ddlPage.Items.Add(LI);
                        }

                        BindGridToXml(sreturnValue);

                        XElement objLookupDoc = XElement.Parse(sreturnValue);

                        var objColXml = from cols in objLookupDoc.Descendants("ColHead")

                                        select cols;

                        foreach (XElement column in objColXml)
                        {
                            ListItem LI = new ListItem();
                            LI.Value = column.Attribute("DbFieldName").Value;
							
							//MITS 20135 un-escape Admin tracking field
                            string sTitle = column.Attribute("title").Value;
                            sTitle = HttpUtility.HtmlDecode(sTitle).Replace("&apos;", "'");
                            LI.Text = sTitle;
                            ddlSort.Items.Add(LI);
                        }
                    }
                    if (ddlSort.Items.Count > 0)
                    {   //MITS 16503
                        //It is not necessary that when page is loaded first time , data are sorted by first coloumn.
                        //orderBy.Text = ddlSort.Items[0].Value;
                        ordermode.Text = "ASC";
                    }
                    ddlSort.Visible = false;
                    ddlOrderMode.Visible = false;
                    ddlPage.Visible = false;

                    lnkFirst.Enabled = true;
                    lnkLast.Enabled = true;
                    lnkPrev.Enabled = true;
                    lnkNext.Enabled = true;
                    TextBox txtthispage = (TextBox)this.Form.FindControl("thispage");
                    if (txtthispage != null && txtthispage.Text == "1")
                    {
                        lnkFirst.Enabled = false;
                        lnkPrev.Enabled = false;
                    }
                    if (pagesize != null && recordcount != null && Convert.ToInt32(pagesize.Text) > Convert.ToInt32(recordcount.Text))
                    {
                        lnkLast.Enabled = false;
                        lnkNext.Enabled = false;
                    }
                    if (Request.QueryString["form"] != null && Request.QueryString["form"] == "Catastrophe")
                    {
                        PageID = RMXResourceProvider.PageId("CatastropheLookUp.aspx");
                        lnkFirst.Text = RMXResourceProvider.GetSpecificObject("lnkbtnFirst", PageID, ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
                        lnkPrev.Text = RMXResourceProvider.GetSpecificObject("lnkbtnPrevious", PageID, ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
                        lnkNext.Text = RMXResourceProvider.GetSpecificObject("lnkbtnNext", PageID, ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
                        lnkLast.Text = RMXResourceProvider.GetSpecificObject("lnkbtnLast", PageID, ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
                        //lblLookUpResults.Text = RMXResourceProvider.GetSpecificObject("lblLookUpResults", PageID, ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                    }
                    
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private void ddlPage_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
            if (txtpagenumber != null)
                txtpagenumber.Text = ddlPage.SelectedItem.Value;
            EnableLinks();

        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private BoundField CreateBoundColumn(DataColumn c)
        {

            BoundField column = new BoundField();
            column.DataField = c.ColumnName;
            column.SortExpression = c.ColumnName;
            column.HeaderText = c.ColumnName.Replace("_", " ");

            return column;


        }
        private HyperLinkField CreateHyperLinkColumn(DataColumn c)
        {

            HyperLinkField column = new HyperLinkField();
            column.SortExpression = c.ColumnName;
            column.HeaderText = c.ColumnName.Replace("_", " ");
            column.DataTextField = c.ColumnName;
            column.NavigateUrl = "OpenRecord({0},{0});";
            return column;



        }

        protected void LinkCommand_Click(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToUpper())
            {
                case "FIRST":
                    {
                        TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
                        if (txtpagenumber != null)
                            txtpagenumber.Text = "1";
                        ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;

                        break;
                    }
                case "LAST":
                    {

                        TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
                        TextBox txtpagecount = (TextBox)this.Form.FindControl("pagecount");
                        if (txtpagenumber != null && txtpagecount != null && txtpagecount.Text != "")
                            txtpagenumber.Text = txtpagecount.Text;
                        ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;

                        break;
                    }
                case "PREV":
                    {
                        TextBox txtthispage = (TextBox)this.Form.FindControl("thispage");
                        TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");

                        if (txtpagenumber != null && txtthispage != null && txtthispage.Text != "")
                            txtpagenumber.Text = Convert.ToString(Convert.ToInt32(txtthispage.Text) - 1);
                        ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;
                        break;
                    }
                case "NEXT":
                    {
                        TextBox txtthispage = (TextBox)this.Form.FindControl("thispage");
                        TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");

                        if (txtpagenumber != null && txtthispage != null && txtthispage.Text != "")
                            txtpagenumber.Text = Convert.ToString(Convert.ToInt32(txtthispage.Text) + 1);
                        ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;
                        break;
                    }
            }

            //grdLookUp.DataSource = (DataSet)ViewState["DS"];
            //grdLookUp.DataBind();
            //ddlPage.SelectedIndex = grdLookUp.CurrentPageIndex;
            EnableLinks();

        }
        void EnableLinks()
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            bReturnStatus = CallCWSFunctionBind("LookupDataAdaptor.GetLookupData", out sreturnValue);
            m_sData = sreturnValue;
            if (bReturnStatus)
            {
                BindGridToXml(sreturnValue);
            }
            lnkFirst.Enabled = true;
            lnkLast.Enabled = true;
            lnkPrev.Enabled = true;
            lnkNext.Enabled = true;
            TextBox txtthispage = (TextBox)this.Form.FindControl("thispage");
            TextBox txtpagecount = (TextBox)this.Form.FindControl("pagecount");
            if (txtthispage != null && txtthispage.Text == "1")
            {
                lnkFirst.Enabled = false;
                lnkPrev.Enabled = false;
            }
            if ((txtthispage != null && txtpagecount != null && txtthispage.Text == txtpagecount.Text) || (pagesize != null && recordcount != null && Convert.ToInt32(pagesize.Text) > Convert.ToInt32(recordcount.Text)))
            {
                lnkLast.Enabled = false;
                lnkNext.Enabled = false;
            }
        }
        private string AlignMatrix(string type)
        {
            string ret = "center";
            switch (type)
            {
                case "varchar":
                    ret = "left";
                    break;
                case "smallint":
                    ret = "center";
                    break;
                case "int":
                case "float":
                case "double":
                    ret = "right";
                    break;
            }
            return ret;
        }
        private string SetAlignment(string colname)
        {
            if (m_sData != "")
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(m_sData);
                XmlNode node = doc.SelectSingleNode("//ColHead[@title='" + colname + "']");
                if (node != null)
                {
                    if (colname.ToLower().IndexOf("date") != -1)
                    {
                        return "center";
                    }
                    return AlignMatrix(node.SelectSingleNode("@DbFieldType").Value);
                }

            }
            return "";

        }

 

        protected void grdLookUp_RowDataBound(object Sender, GridViewRowEventArgs e)
        {

            //if (grdLookUp.DataSource != null)
            //{
            //    if (e.Row.RowType == DataControlRowType.DataRow)
            //    {
            //        DataRowView drv = (DataRowView)e.Row.DataItem;
            //        TableCell nameCell = e.Row.Cells[0];
            //        HyperLink link = (HyperLink)nameCell.Controls[0];
            //        link.NavigateUrl = "javascript:if(OpenRecord(" + drv
            //     ["pid"].ToString() + "," + drv
            //     ["pid"].ToString() + "))";
            //    }
            //}
            int iTempIndex = 0;
            bool isSubBankAccount = false;
            bool isEntityaddr = false;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    string Header = ((System.Web.UI.WebControls.DataControlFieldCell)(cell)).ContainingField.SortExpression;
                    cell.Attributes.Add("align", SetAlignment(Header));

                    //for bank sub account:: SUB ROW ID comes for sub bank account only
                    //rsushilaggar MITS 24083 Date 03/11/2011
                    if (Header.ToUpper() == "SUB_ROW_ID")
                    {
                        isSubBankAccount = true;
                        cell.Attributes.CssStyle.Add("display", "none");
                    }
                    //amitosh
                    if (form.Text == "entityaddress")
                    {
                        isEntityaddr = true;
                     
                    }
                    if (Header.ToUpper() == "COUNTRYID")
                    {
                        cell.Attributes.CssStyle.Add("display", "none");
                    }
                }
                DataRowView objDataRow = (DataRowView)e.Row.DataItem;
                //Deb  MITS 34351
                //e.Row.ID = objDataRow["pid"].ToString();
                try
                {
                    e.Row.Attributes["id"] = "grdLookUp_" + objDataRow["pid"].ToString();
                }
                catch{}
                //Deb MITS 34351
                try
                {
                    if (objDataRow["pid2"] != null)
                        e.Row.Attributes.Add("pid2", objDataRow["pid2"].ToString());

                    if (objDataRow["pid3"] != null)
                        e.Row.Attributes.Add("pid3", objDataRow["pid3"].ToString());
                }
                catch { }
                //smahajan6 - MITS #18230 - 11/18/2009 : Start
                try
                {
                    //For Hidden Columns - Fetch Value for Hidden Columns
                    if (objDataRow["pid4"] != null)
                        e.Row.Attributes.Add("pid4", objDataRow["pid4"].ToString());
                }
                catch { }
                //smahajan6 - MITS #18230 - 11/18/2009 : End
                e.Row.Cells[0].Text = "<a href='#'>" + e.Row.Cells[0].Text + "</a>";
                //amitosh
                if (isEntityaddr)
                {
                    e.Row.Cells[0].Attributes["onclick"] = String.Format("BindDataToAddrCtrl('{0}',{1})", objDataRow["addr1"].ToString() + " " + objDataRow["addr2"].ToString() + " " + objDataRow["addr3"].ToString() + " " + objDataRow["addr4"].ToString() + " " + objDataRow["city"].ToString() + " " + objDataRow["state"].ToString() + " " + objDataRow["country"].ToString(), objDataRow["pid"].ToString());

                }
               else if (isSubBankAccount)
                {
                    //rsushilaggar MITS 24083 Date 03/11/2011
                    e.Row.Cells[0].Attributes["onclick"] = String.Format("OpenRecord({0},{1})", objDataRow["sub_row_id"].ToString(), objDataRow["pid"].ToString());

                }
                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                else if (string.Equals(form.Text, "claimadjuster", StringComparison.OrdinalIgnoreCase))
                {
                    string strFullName = objDataRow[0].ToString().Trim();
                    if (!string.IsNullOrEmpty(strFullName))
                    {
                        if (!string.IsNullOrEmpty(objDataRow[1].ToString().Trim()))
                            strFullName = string.Concat(strFullName, ", ", objDataRow[1].ToString().Trim());
                    }
                    else
                        strFullName = objDataRow[1].ToString().Trim();

                    e.Row.Cells[0].Attributes["onclick"] = String.Format("BindDatToAdjCtrl({0},{1},{2})", objDataRow["pid"].ToString(), string.Concat("'", strFullName.Trim(), "'"), string.Concat("'", ParentValues.Trim(), "'"));
                }
                //Ankit End
                else
                {
                    e.Row.Cells[0].Attributes["onclick"] = String.Format("OpenRecord({0},{1})", objDataRow["pid"].ToString(), objDataRow["pid"].ToString());
                }

             

                if (arrIndex != null)
                {
                    for (int i = 0; i < arrIndex.Count; i++)
                        e.Row.Cells[Conversion.ConvertObjToInt(arrIndex[i])].Visible = false;
                }
              }

            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    arrIndex = new ArrayList();

            //    foreach (TableCell cell in e.Row.Cells)
            //    {
            //        if ((cell.Text == "pid") || (cell.Text == "pid2") || (cell.Text == "pid3"))
            //        {
            //            cell.Visible = false;
            //            arrIndex.Add(iTempIndex);
            //        }
            //        iTempIndex++;
            //    }
            //    e.Row.CssClass = "colheader3";
            //}
            if (e.Row.RowType == DataControlRowType.Header)
            {
                arrIndex = new ArrayList();
                foreach (TableCell cell in e.Row.Cells)
                {
                    DataControlFieldCell gridViewCell = cell as DataControlFieldCell;
                    if (gridViewCell != null)
                    {
                        DataControlField cellsField = gridViewCell.ContainingField;
                        cellsField.HeaderStyle.ForeColor = System.Drawing.Color.White;
                        cell.Attributes.Add("align", SetAlignment(cellsField.SortExpression));
                        //smahajan6 - MITS #18230 - 11/18/2009 : Start
                        //if (cellsField != null && ((cellsField.SortExpression == "pid") || (cellsField.SortExpression == "pid2") || (cellsField.SortExpression == "pid3")))
                        if (cellsField != null && ((cellsField.SortExpression == "pid") || (cellsField.SortExpression == "pid2") || (cellsField.SortExpression == "pid3") || (cellsField.SortExpression == "pid4") || (cellsField.SortExpression == "pid5")))// MITS #34279 Starts : Added for extra Column pid5 in Datakeynames which was causing Object reference null
                        //smahajan6 - MITS #18230 - 11/18/2009 : End
                        {
                            cell.Visible = false;
                            arrIndex.Add(iTempIndex);
                        }
                        iTempIndex++;
                        
                        if (cellsField.HeaderText.ToUpper() == "COUNTRYID")
                        {
                            cell.Attributes.CssStyle.Add("display", "none");
                        }
                        //cellsField.HeaderText = HttpUtility.HtmlDecode(cellsField.HeaderText).Replace("&apos;", "'");

                        //MGaba2:MITS 15638:Sorting on Dated Text is not working:Start
                        //In lookup.cs, nosort attribute is set for field types text/ntext/image/clob
                        //We are preventing sorting on such columns by disabling them
                        if (m_sData != "")
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(m_sData);
                            XmlNode node = doc.SelectSingleNode("//ColHead[@DbFieldName='" + cellsField.SortExpression + "']");
                            if (node != null && cell.Controls.Count > 0)
                            {
								//MITS 20135 un-escape Admin tracking field
                                ((LinkButton)cell.Controls[0]).Text = HttpUtility.HtmlDecode(node.Attributes["title"].Value).Replace("&apos;", "'");
                                if (node.Attributes["nosort"] != null)
                                {
                                    ((LinkButton)cell.Controls[0]).Enabled = false;
                                }

                                //for bank account: hide the column
                                if (cellsField.SortExpression.ToUpper() == "SUB ROW ID")
                                {
                                    ((LinkButton)cell.Controls[0]).Style.Add("display", "none");
                                }
                            }
                        }
                        //MGaba2:MITS 15638:Sorting on Dated Text not working:End
                    }
                }
                
            }

        }

        private void ddlSort_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
            if (txtpagenumber != null)
                txtpagenumber.Text = "1";
            ddlPage.SelectedIndex = 0;
            TextBox txtordermode = (TextBox)this.Form.FindControl("ordermode");
            if (txtordermode != null)
            {
                txtordermode.Text = ddlOrderMode.SelectedItem.Value;
            }

            TextBox txtorderBy = (TextBox)this.Form.FindControl("orderBy");
            if (txtorderBy != null)
            {
                txtorderBy.Text = ddlSort.SelectedItem.Value;
            }
            EnableLinks();

        }
        protected void grdLookUp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            grdLookUp.PageIndex = e.NewPageIndex;
            bReturnStatus = CallCWSFunction("LookupDataAdaptor.GetLookupData", out sreturnValue);
            if (bReturnStatus)
            {
                BindGridToXml(sreturnValue);
            }

        }

        private void BindGridToXml(string sReturnValue)
        {
            DataRow drLookUpRow = null;
            DataTable dtLookUp = new DataTable();

            XElement objLookupDoc = XElement.Parse(sReturnValue);

            var objColXml = from cols in objLookupDoc.Descendants("ColHead")

                            select cols;

            if (objColXml.Count() != 0)
            {
                foreach (XElement column in objColXml)
                {
                    //excluding 
                    // if (column.Attribute("title").Value.ToUpper() != "SUB_ROW_ID")
                    //{
                    //For Admin tracking talbe, the title could have been encoded
                    //dbisht6 omig issue
                    if (string.Equals(column.Attribute("DbFieldName").Value, "COVERAGE_TEXT"))
                    {
                        if (PolicySysID > 0)
                            dtLookUp.Columns.Add(new DataColumn(column.Attribute("DbFieldName").Value));
                    }
                    else
                        dtLookUp.Columns.Add(new DataColumn(column.Attribute("DbFieldName").Value));
                    //dbisht6 end
                    // }
                }

                dtLookUp.Columns.Add("pid");


                var objRowXml = from rows in objLookupDoc.Descendants("Row")

                                select rows;


                if (objRowXml.Count() != 0)
                {
                    int iAttributeCount = 0;

                    foreach (XAttribute attribute in objRowXml.ElementAt(0).Attributes())
                    {
                        iAttributeCount++;
                        if (attribute != null)
                        {
                            if (attribute.Name.LocalName.ToLower() == "pid")
                                continue;

                            dtLookUp.Columns.Add(attribute.Name.LocalName.ToLower());
                        }
                    }

                    string[] arrPid = new string[iAttributeCount];

                    string sDescendant = string.Empty;
                    // npadhy MITS 15832 Handled an Extra comma coming while concatenating two fields with comma
                    string sValue = string.Empty;
                    string sTrimmedValue = string.Empty;

                    foreach (XElement row in objRowXml)
                    {
                        arrPid[0] = "pid";
                        if (row.Attribute("pid2") != null)
                            arrPid[1] = "pid2";

                        if (row.Attribute("pid3") != null)
                            arrPid[2] = "pid3";
                    //smahajan6 - MITS #18230 - 11/18/2009 : Start
                    //For Hidden Columns - Fetch Value for Hidden Columns
                    if (row.Attribute("pid4") != null)
                        arrPid[3] = "pid4";//Ashish Ahuja [JIRA] RMA-219 //MITS #34279 CDR ISSUE fix added
                    //smahajan6 - MITS #18230 - 11/18/2009 : End
                    // MITS #34279 Starts : Added for extra Column pid5 in Datakeynames which was causing Object reference null
                    if (row.Attribute("pid5") != null)
                        arrPid[iAttributeCount - 1] = "pid5";
                    // MITS #34279 Ends : Added for extra Column pid5 in Datakeynames which was causing Object reference null

                        grdLookUp.DataKeyNames = arrPid;

                        drLookUpRow = dtLookUp.NewRow();
                        foreach (XElement column in objColXml)
                        {
                            sDescendant = column.Attribute("DbFieldName").Value;
                            // npadhy MITS 15832 Handled an Extra comma coming while concatenating two fields with comma
                            sValue = row.Element(sDescendant).Value;
                            sTrimmedValue = sValue.Trim();
                            if (sTrimmedValue == ",")
                                sValue = "";
                            else if (sTrimmedValue != row.Element(sDescendant).Value)
                            {
                                if (sTrimmedValue.StartsWith(","))
                                {
                                    sValue = sTrimmedValue.Substring(1);
                                }
                                else if (sTrimmedValue.EndsWith(","))
                                {
                                    sValue = sTrimmedValue.Substring(0, sTrimmedValue.Length - 1);
                                }
                            }
                            //dbisht6 omig issue
                            if (string.Equals(sDescendant, "COVERAGE_TEXT"))
                            {
                                if (PolicySysID > 0)
                                    drLookUpRow[sDescendant] = sValue;
                            }
                            else
                                drLookUpRow[sDescendant] = sValue;
                            //dbisht6 end
                        }

                        drLookUpRow["pid"] = row.Attribute("pid").Value;

                        if (row.Attribute("pid2") != null)
                            drLookUpRow["pid2"] = row.Attribute("pid2").Value;

                        if (row.Attribute("pid3") != null)
                            drLookUpRow["pid3"] = row.Attribute("pid3").Value;
                    //smahajan6 - MITS #18230 - 11/18/2009 : Start
                    //For Hidden Columns - Fetch Value for Hidden Columns
                    if (row.Attribute("pid4") != null)
                        drLookUpRow["pid4"] = row.Attribute("pid4").Value;
                    //smahajan6 - MITS #18230 - 11/18/2009 : End
                    // MITS #34279 Starts : Added for extra Column pid5 in Datakeynames which was causing Object reference null
                    if (row.Attribute("pid5") != null)
                        drLookUpRow["pid5"] = row.Attribute("pid5").Value;
                    // MITS #34279 Ends : Added for extra Column pid5 in Datakeynames which was causing Object reference null

                        dtLookUp.Rows.Add(drLookUpRow);
                    }
                }
                else
                {
                    ddlOrderMode.Visible = false;
                    ddlPage.Visible = false;
                    ddlSort.Visible = false;
                    lblSort.Visible = false;
                    lblPage.Visible = false;
                    if (Request.QueryString["form"] != null && Request.QueryString["form"] == "Catastrophe")
                    {
                        lblNoRecord.InnerText = RMXResourceProvider.GetSpecificObject("lblNoRecordtoShow", RMXResourceProvider.PageId("CatastropheLookUp.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
                    }
                    else
                    {
                        lblNoRecord.InnerText = "There are no records to show.";
                    }
                }
            }//start rsushilaggar- MITS 17601
            else
            {
                ddlOrderMode.Visible = false;
                ddlPage.Visible = false;
                ddlSort.Visible = false;
                lblSort.Visible = false;
                lblPage.Visible = false;
                if (Request.QueryString["form"] != null && Request.QueryString["form"] == "Catastrophe")
                {
                    lblNoRecord.InnerText = RMXResourceProvider.GetSpecificObject("lblNoLookUpFieldDefined", RMXResourceProvider.PageId("CatastropheLookUp.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
                }
                else
                {
                    lblNoRecord.InnerText = "There are no lookup field defined for this table.";
                }
                
            }
            //End rsushilaggar- MITS 17601
            // Bind DataTable to GridView.
            grdLookUp.Visible = true;
            grdLookUp.PageSize = Convert.ToInt32(pagesize.Text);
            grdLookUp.DataSource = dtLookUp;
            grdLookUp.DataBind();
        }
        protected void grdLookUp_Sorting(object sender, GridViewSortEventArgs e)
        {
            //TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
            //if (txtpagenumber != null)
            //    txtpagenumber.Text = "1";
            //ddlPage.SelectedIndex = 0;

            string sPrevorderBy = "";
            string sPrevordermode = "";
            TextBox txtorderBy = (TextBox)this.Form.FindControl("orderBy");
            if (txtorderBy != null)
            {
                sPrevorderBy = txtorderBy.Text;
                //txtorderBy.Text = ddlSort.SelectedItem.Value;
                txtorderBy.Text = ddlSort.Items.FindByValue(e.SortExpression).Value;
            }
            TextBox txtordermode = (TextBox)this.Form.FindControl("ordermode");

            if (txtordermode != null)
            {
                sPrevordermode = txtordermode.Text;
                if (sPrevordermode == "")
                    sPrevordermode = "ASC";
                if ((sPrevorderBy == txtorderBy.Text))
                {
                    if (sPrevordermode == "ASC")
                        sPrevordermode = "DESC";
                    else
                        sPrevordermode = "ASC";
                }
                else
                {
                    sPrevordermode = "ASC";
                }
                txtordermode.Text = sPrevordermode;
                //txtordermode.Text = ddlOrderMode.SelectedItem.Value;
            }
            EnableLinks();

        }

 
    }
}
