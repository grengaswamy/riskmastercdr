﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Data;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.LookupData
{
    public partial class UnitCoverageSummary : System.Web.UI.Page
    {
        XElement objXml = null;
        XElement objUnitCoverageLst = null;
        protected void Page_Load(object sender, EventArgs e)
        {                    

            if (!IsPostBack)
            {
                objUnitCoverageLst = GetMessageTemplate();
                PopulateTemplate(ref objUnitCoverageLst);
                
                // Call CWS and get the result xml.
                objXml = GetUnitCoverageLst(objUnitCoverageLst);

                BindGridToXml(objXml, "");
            }
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <Message>
                    <Authorization></Authorization> 
                    <Call>
                        <Function>LookupDataAdaptor.GetUnitCoverageSummary</Function> 
                    </Call>
                    <Document>
                        <GetUnitCoverageLst>
                            <ClaimID></ClaimID> 
                            <FormName></FormName> 
                        </GetUnitCoverageLst>
                    </Document>
                </Message>
            ");

            return oTemplate;
        }
        /// <summary>
        /// This function is used to create the xml for payment transaction
        /// </summary>
        /// <param name="objUnitCoverageLst"></param>
        private void PopulateTemplate(ref XElement objUnitCoverageLst)
        {
            XElement objTempElement = null;
            if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("SessionId")))
            {
                objTempElement = objUnitCoverageLst.XPathSelectElement("./Authorization");
                objTempElement.Value = AppHelper.ReadCookieValue("SessionId");
            }

            objTempElement = objUnitCoverageLst.XPathSelectElement("./Document/GetUnitCoverageLst/ClaimID");
            if (Request.QueryString["ClaimID"] != null)
            {
                objTempElement.Value = Request.QueryString["ClaimID"];
            }
            objTempElement = objUnitCoverageLst.XPathSelectElement("./Document/GetUnitCoverageLst/FormName");
            if (Request.QueryString["Form"] != null)
            {
                objTempElement.Value = Request.QueryString["Form"];
            }
        }

        private XElement GetUnitCoverageLst(XElement p_objAutoCheckBatchesIn)
        {
            XmlDocument objXmlUnitCoverage = new XmlDocument();
            objXmlUnitCoverage.LoadXml(p_objAutoCheckBatchesIn.ToString());

            string sReturn = AppHelper.CallCWSService(objXmlUnitCoverage.InnerXml.ToString());

            //Binding Error Control
            ErrorControl.errorDom = sReturn;

            objXmlUnitCoverage.LoadXml(sReturn);

            XmlNode oInstanceNode = objXmlUnitCoverage.SelectSingleNode("/ResultMessage/Document");

            XElement objUnitCoverageOut = XElement.Parse(oInstanceNode.InnerXml.ToString());

            return objUnitCoverageOut;
        }


        /// <summary>
        /// Author:Sonam Pahariya
        /// This function is used to bind xml to the grid.
        /// </summary>
        /// <param name="p_objUnitCovXml"></param>
        /// <param name="sortExpression"></param>

        private void BindGridToXml(XElement p_objUnitCovXml,string sortExpression)
        {
            DataRow drUnitCoverageRow = null;
            DataTable dtUnitCoverageRow = new DataTable();
            int iPageSize = 0;
            int iheader = 0;
            var objUnitCoverageLstXml = from cols in p_objUnitCovXml.Descendants("UnitCoverage")

                                       select cols;

            if (objUnitCoverageLstXml.Count() != 0)
            {
                iPageSize = objUnitCoverageLstXml.Count();

                XElement objHeaderRow = objUnitCoverageLstXml.First();

                for (int iCount = 0; iCount < objHeaderRow.Elements().Count(); iCount++)
                {
                    dtUnitCoverageRow.Columns.Add(new DataColumn(objHeaderRow.Elements().ElementAt(iCount).Value));
                }
                if (objUnitCoverageLstXml.Count() == 1)
                {
                    drUnitCoverageRow = dtUnitCoverageRow.NewRow();
                    dtUnitCoverageRow.Rows.Add(drUnitCoverageRow);
                }
                else
                {
                    foreach (XElement UnitCoverage in objUnitCoverageLstXml)
                    {
                        if (iheader == 0)
                        {
                            iheader++;
                            continue;
                        }
                        drUnitCoverageRow = dtUnitCoverageRow.NewRow();

                        for (int iCount = 0; iCount < UnitCoverage.Elements().Count(); iCount++)
                        {
                            drUnitCoverageRow[objHeaderRow.Elements().ElementAt(iCount).Value] = UnitCoverage.Elements().ElementAt(iCount).Value;
                        }
                        dtUnitCoverageRow.Rows.Add(drUnitCoverageRow);
                    }
                }
            }
            else
            {
                lblNoRecord.InnerText = "There are no records to show.";
                return;
            }
            DataView view = new DataView(dtUnitCoverageRow);
            if (!string.IsNullOrEmpty(sortExpression))
            {
                view.Sort = sortExpression;
            }
            else
            {
                view = dtUnitCoverageRow.DefaultView;
            }

            // Bind DataTable to GridView.
            grdUnitCovSummary.Visible = true;
            grdUnitCovSummary.PageSize = iPageSize;
            grdUnitCovSummary.DataSource = view;
            grdUnitCovSummary.DataBind();
        }


        /// <summary>
        /// Author:Sonam Pahariya
        /// This function is used to bind the rows to the grid columns.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void grdUnitCovSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //string sBatchNumber = e.Row.Cells[0].Text;
                    //e.Row.Cells[0].Text = "<a href='#'>" + sBatchNumber + "</a>";
                    //e.Row.Cells[0].Attributes["onclick"] = String.Format("MoveToAutoBatchId({0})", sBatchNumber);

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                    else
                    {
                        e.Row.CssClass = "rowlight2";
                    }

                    for (int i = 0; i < e.Row.Cells.Count; i++)
                        e.Row.Cells[i].Wrap = false;
                }
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    foreach (TableCell cell in e.Row.Cells)
                    {
                        cell.Wrap = false;
                    }
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

      

        /// <summary>
        /// Author:Sonam Pahariya
        /// This function is used to sort the grid columns.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView_Sorting(Object sender, GridViewSortEventArgs e)
        {
            //IsSearchEnabled.Text = "true";


            try
            {
                objUnitCoverageLst = GetMessageTemplate();
                PopulateTemplate(ref objUnitCoverageLst);
                objXml = GetUnitCoverageLst(objUnitCoverageLst);
                if (ViewState["GridSortExpression"] != null)
                {
                    GridSortExpression.Value = ViewState["GridSortExpression"].ToString();
                }
                if (ViewState["GridSortDirection"] != null)
                {
                    GridSortDirection.Value = ViewState["GridSortDirection"].ToString();
                }

                if (!string.IsNullOrEmpty(GridSortExpression.Value) && GridSortExpression.Value == e.SortExpression)
                {
                    if (GridSortDirection.Value == "ASC")
                        GridSortDirection.Value = "DESC";
                    else
                        GridSortDirection.Value = "ASC";
                }
                else
                {
                    GridSortDirection.Value = "ASC";
                }

                BindGridToXml(objXml, e.SortExpression + " " + GridSortDirection.Value);
                ViewState["GridSortDirection"] = GridSortDirection.Value;
                ViewState["GridSortExpression"] = e.SortExpression;
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

    }
}