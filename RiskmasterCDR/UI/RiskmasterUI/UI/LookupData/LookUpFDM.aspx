﻿<!--**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 11/04/2014 | 34279  | ajohari2   | SRe Gap 21 Claimant lookup - Added changes in OpenRecord fun
**********************************************************************************************-->
<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="LookUpFDM.aspx.cs" Inherits="Riskmaster.UI.LookupData.LookUpFDM" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   
  <title>Lookup Data</title>
     <link href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" rel="stylesheet" /><link href="../../App_Themes/RMX_Default/round-button.css" type="text/css" rel="stylesheet" /><link href="../../App_Themes/RMX_Default/Tabs.css" type="text/css" rel="stylesheet" />
     <uc4:CommonTasks ID="CommonTasks1" runat="server" />
   <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">
  </script>
  <script type="text/javascript" language="javascript">
      function getPage(toPage) {
          document.getElementById('pagenumber').value = toPage;
          document.getElementById('hdnaction').value = 'GoToAnotherPage';
          //document.forms[0].submit();
          document.forms[0].submit();
          return false;
      }

      //Ankit Start : Worke on MITS - 29721 - Adjuster Enhancement
      function BindDatToAdjCtrl(pid, ptext, pcontrolid) {
          window.opener.SetClaimAdjusterData(pid, ptext, pcontrolid);
          window.close();
          return false;
      }
      //Ankit End

      function BindDataToAddrCtrl(sData, pid) {
          
          if ((window.opener.document.forms[0].SysFormName.value == 'autoclaimchecks') || (window.opener.document.forms[0].SysFormName.value == 'funds')) {
              var objClaimantRow = document.getElementById("grdLookUp_" + pid);
              if (objClaimantRow != null) {
                  if (objClaimantRow.childNodes != null) {
                      //change index for compatibility mode.9203
                      
                      if (objClaimantRow.childNodes[0].innerHTML == undefined && !window.ie)
                      {
                          for (var i = 0; i < 9; i++)
                          {
                              objClaimantRow.childNodes[i].innerHTML = objClaimantRow.childNodes[i + 1].innerHTML;
                          }
                      }
                      var addr1 = GetTextFromHTML(objClaimantRow.childNodes[0].innerHTML);
                      var addr2 = GetTextFromHTML(objClaimantRow.childNodes[1].innerHTML);
                      var addr3 = GetTextFromHTML(objClaimantRow.childNodes[2].innerHTML);
                      var addr4 = GetTextFromHTML(objClaimantRow.childNodes[3].innerHTML);
                      var city = GetTextFromHTML(objClaimantRow.childNodes[4].innerHTML);
                      var staterowid = objClaimantRow.attributes.getNamedItem("pid3").value;

                      var state = GetTextFromHTML(objClaimantRow.childNodes[6].innerHTML);
                      var country = GetTextFromHTML(objClaimantRow.childNodes[7].innerHTML);
                      var zip = GetTextFromHTML(objClaimantRow.childNodes[8].innerHTML);
                      var countryid = objClaimantRow.attributes.getNamedItem("pid2").value;

                      window.opener.AddrSelected(addr1, addr2,addr3,addr4, city, state, staterowid, zip,country,countryid)
                      window.close();
                  }


              }
          }
          else {
              window.opener.BindData(sData, pid);
              window.close();
          }
          return false;

      }
      function GetTextFromHTML(HTMLvalue)
      {
          var returnValue = "";
          var tmp = document.createElement("DIV");
          if (HTMLvalue.indexOf(">")!=-1)
              returnValue = HTMLvalue.substring((HTMLvalue.indexOf(">") + 1), HTMLvalue.indexOf("</"));
          else if (HTMLvalue == "&nbsp;")
              returnValue = "";
          else
              returnValue = HTMLvalue;
          tmp.innerHTML = returnValue;
          return tmp.textContent || tmp.innerText;
      }
      function OpenRecord(pid, rowid) {
        
          //choosing Url depending on whether its a fdm lookup , funds lookup , admin trackingc lookup etc..raman bhatia
          var iCount = 0;
          var formtype = document.forms[0].form.value;
          var form = formtype;
          var bIsAdmin = ('sysformid' == 'document.forms[0].fpid.value');
          window.opener.document.forms[0].SysCmd.value = "0";
          if (bIsAdmin) {
              formtype = 'admintracking';
              form = formtype + '|' + new String(form).toUpperCase();
          }
          var currentScreen = window.opener.m_CurrentScreen;
          var correspondingScreen = window.opener.m_CorrespondingScreen;
          
          //MITS:34279 ajohari2: Added a new condition for liabilityloss screen claimant lookup
          //if (currentScreen == correspondingScreen && window.opener.parent.MDICorrespondingNavTreeNodeExists(rowid, formtype)){
          if (currentScreen == correspondingScreen && window.opener.parent.MDICorrespondingNavTreeNodeExists(rowid, formtype) && (window.opener.document.forms[0].SysFormName.value != 'liabilityloss' && formtype != 'claimant')) {
              window.opener.document.forms[0].SysCmd.value = "";  //mbahl3 mits 31696
              window.opener.parent.MDIShowScreen(rowid, formtype);
          }
          else {
              switch (formtype) {
                  case "funds":
                      window.opener.document.forms[0].transid.value = pid;
                      //window.opener.document.forms[0].functiontocall.value = "FundManagementAdaptor.GetTransaction";
                      //window.opener.document.forms[0].ouraction.value = "gototransaction";
                      //pmahli 11/30/2007 MITS 10811 - Start
                      //window.opener.OpenProgressWindow();
                      //pmahli 11/30/2007 MITS 10811 - End
                      window.opener.document.forms[0].submit();
                      break;

                  case "admintracking":
                      var obj = eval('window.opener.document.forms[0].SysFormId');
                      if (obj != null)
                          obj.value = pid;
                      window.opener.document.forms[0].submit();
                      break;

                  //Raman Bhatia.. need claimant information for Autochecks  
                  case "claimant":
                  case "personinvolved": //added by rupal, r8 enh to add person involved as payee type
                      if ((window.opener.document.forms[0].SysFormName.value == 'autoclaimchecks') || (window.opener.document.forms[0].SysFormName.value == 'funds')) {
                          var objClaimantRow = document.getElementById("grdLookUp_" + pid);
                          
                          if (objClaimantRow.childNodes[0].innerHTML == undefined && !window.ie) {
                              //11599
                              var countCols = 0;

                              if (formtype == "claimant")
                              {
                                  countCols = 17;
                              }
                              else if (formtype == "personinvolved")
                              {
                                  countCols = 15;
                              }

                              for (var i = 0; i < countCols; i++) { //JIRA RMA-11475 ajohari2
                                  objClaimantRow.childNodes[i].innerHTML = objClaimantRow.childNodes[i+1].innerHTML;
                              }
                          }

                          //Ankit Starts : For MITS - 30882 (Prefix/Suffix Issue)
                          var prefix = '';
                          var suffixCommon = '';
                          //Ankit End
                          ////avipinsrivas Start : Worked for Jira-340
                          //var entityroleid = '';
                          ////avipinsrivas End
                          if (objClaimantRow != null) {
                              if (objClaimantRow.childNodes != null) {
                                  //var temp=GetTextFromHTML(objClaimantRow.childNodes[0].innerHTML);
                                  var lastname = GetTextFromHTML(objClaimantRow.childNodes[0].innerHTML);
                                  var firstname = GetTextFromHTML(objClaimantRow.childNodes[1].innerHTML);
                                  var middlename = GetTextFromHTML(objClaimantRow.childNodes[2].innerHTML);
                                  var taxid = GetTextFromHTML(objClaimantRow.childNodes[3].innerHTML);
                                  var addr1 = GetTextFromHTML(objClaimantRow.childNodes[4].innerHTML);
                                  var addr2 = GetTextFromHTML(objClaimantRow.childNodes[5].innerHTML);
                                  var addr3 = GetTextFromHTML(objClaimantRow.childNodes[6].innerHTML);
                                  var addr4 = GetTextFromHTML(objClaimantRow.childNodes[7].innerHTML);
                                  var city = GetTextFromHTML(objClaimantRow.childNodes[8].innerHTML);
                                  var staterowid = objClaimantRow.attributes.getNamedItem("pid3").value;
                                  var stateid = GetTextFromHTML(objClaimantRow.childNodes[9].innerHTML);
                                  var state = stateid + ' ' + GetTextFromHTML(objClaimantRow.childNodes[10].innerHTML);
                                  var zip = GetTextFromHTML(objClaimantRow.childNodes[11].innerHTML);
                                  var countryid = null;
                                  var country = null;//JIRA RMA-11898 ajohari2
                                  //11599
                                  if (formtype == "claimant") {
                                      countryid = GetTextFromHTML(objClaimantRow.childNodes[16].innerHTML);//68;//JIRA RMA-11475 ajohari2
                                      country = GetTextFromHTML(objClaimantRow.childNodes[14].innerHTML) + ' ' + GetTextFromHTML(objClaimantRow.childNodes[15].innerHTML);//JIRA RMA-11898 ajohari2
                                  }
                                  else if (formtype == "personinvolved")
                                  {
                                      countryid = GetTextFromHTML(objClaimantRow.childNodes[12].innerHTML);//JIRA RMA-11898 ajohari2
                                      country = GetTextFromHTML(objClaimantRow.childNodes[13].innerHTML) + ' ' + GetTextFromHTML(objClaimantRow.childNodes[14].innerHTML);//JIRA RMA-11898 ajohari2
                                  }
                                  //var country = GetTextFromHTML(objClaimantRow.childNodes[14].innerHTML) + ' ' + GetTextFromHTML(objClaimantRow.childNodes[15].innerHTML);
                                  var EFTInfo = objClaimantRow.attributes.getNamedItem("pid4").value.split("^*^*^"); //Amitosh
                                  //avipinsrivas Start : Worked for Jira-340
                                  //if (formtype == 'personinvolved') {
                                      //Ankit Starts : For MITS - 30882 (Prefix/Suffix Issue)
                                      if (EFTInfo[1] != null)
                                          prefix = EFTInfo[1];
                                      if (EFTInfo[2] != null)
                                          suffixCommon = EFTInfo[2];
                                      //Ankit End
                                      //if (EFTInfo[3] != null)
                                      //    entityroleid = EFTInfo[3];
                                  //}
                                  //else if (formtype == 'claimant') {
                                  //    if (EFTInfo[1] != null)
                                  //        entityroleid = EFTInfo[1];
                                  //}
                                  //avipinsrivas End
                              }

                          }
                          //Deb
                          var childnodelist = [];
                          var elements = document.getElementsByTagName("tr");
                          for (var i = 0; i < elements.length; i++) {
                              if (elements[i].id.indexOf("grdLookUp_") == 0)
                                  childnodelist.push(elements[i]);
                          }
                          //Deb
                          // Vaibhav has passed the ClaimentId in palce of ClaimantRowId.
                          window.opener.document.forms[0].SysCmd.value = "";
                          //Added by Amitosh For EFT Payment
                          window.opener.ClaimantSelected(objClaimantRow.attributes.getNamedItem("pid2").value, lastname, firstname, middlename, taxid, addr1, addr2, addr3, addr4, city, state, staterowid, zip, countryid, country, EFTInfo[0], childnodelist, prefix, suffixCommon);
                          window.close();
                      }
                      //MITS:34279 ajohari2:start
                      else if (window.opener.document.forms[0].SysFormName.value == 'liabilityloss') {
                          var objClaimantRow = document.getElementById("grdLookUp_" + rowid);
                          if (objClaimantRow != null) {
                              if (objClaimantRow.childNodes != null) {
                                  var lastname = objClaimantRow.childNodes[0].innerText;
                                  var firstname = objClaimantRow.childNodes[1].innerText;
                                  if (firstname != '' && lastname != '') {
                                      firstname = ", " + firstname;
                                  }
                              }
                          }
                          window.opener.document.forms[0].SysCmd.value = "";
                          window.opener.SelectEntityClaimant(objClaimantRow.attributes.getNamedItem("pid2").value, firstname, lastname);
                          window.close();
                      }
                      //MITS:34279 ajohari2:end
                      break;
                  case "unit":
                      if ((window.opener.document.forms[0].SysFormName.value == 'autoclaimchecks') || (window.opener.document.forms[0].SysFormName.value == 'funds')) {
                          var objUnitRow = document.getElementById("grdLookUp_" + pid);

                          if (objUnitRow != null) {
                              if (objUnitRow.childNodes != null) {
                                  var unitname = GetTextFromHTML(objUnitRow.childNodes[2].innerHTML);
                              }
                          }
                          window.opener.UnitSelected(pid, unitname);
                          window.close();
                      }
                      //smahajan6 - MITS #18644 - 11/18/2009 : Start
                      else if(window.opener.document.forms[0].SysFormName.value == 'unit')
                      {
                          var objUnitRow = document.getElementById("grdLookUp_" + pid);
                          if (objUnitRow != null) 
                          {
                              if (objUnitRow.childNodes != null) 
                              {
                                  var vehid = pid;
                                  var vehicle = GetTextFromHTML(objUnitRow.childNodes[0].innerHTML);
                                  var make = GetTextFromHTML(objUnitRow.childNodes[1].innerHTML);
                                  var year = GetTextFromHTML(objUnitRow.childNodes[2].innerHTML);
                                  var licensenumber = GetTextFromHTML(objUnitRow.childNodes[3].innerHTML);
                                  var arrHiddenFields= objUnitRow.attributes.getNamedItem("pid4").value.split("^*^*^");
                              }
                          }
                          window.opener.document.forms[0].SysCmd.value = "";
                          window.opener.SelectedUnit(vehid,vehicle,make,year,licensenumber,arrHiddenFields);
                          correspondingScreen=""
                          window.close();
                      }
                      //smahajan6 - MITS #18644 - 11/18/2009 : End
                      break;
                  //smahajan6 11/26/09 MITS:18230 :Start
                  //Populate fields
                  case "claimpc":
                      if (window.opener.document.forms[0].SysFormName.value == 'claimpc') {
                          var objPropRow = document.getElementById("grdLookUp_" + pid);
                          if (objPropRow != null) 
                          {
                              if (objPropRow.childNodes != null) 
							  {
                                  var propID = pid;
                                  var prop = GetTextFromHTML(objPropRow.childNodes[0].innerHTML);
                                  var state = GetTextFromHTML(objPropRow.childNodes[4].innerHTML);
                                  var zip = GetTextFromHTML(objPropRow.childNodes[5].innerHTML);
                                  var staterowid=objPropRow.attributes.getNamedItem("pid2").value;
                              }
                          }
                          window.opener.document.forms[0].SysCmd.value = "";
                          window.opener.SelectedProperty(propID,prop,state,staterowid,zip);
                          correspondingScreen=""
                          window.close();
                      }
                      break;
                  //smahajan6 11/26/09 MITS:18230 :End     
                  //rupal:start
                  case "siteunit":

                      var sitenumber = "";
                      var objSiteNumber = document.getElementById("grdLookUp_" + pid);
                      if (objSiteNumber != null) {
                          if (objSiteNumber.childNodes != null) {
                              //sitenumber = GetTextFromHTML(objSiteNumber.childNodes[0].innerHTML);
                              //Jira-9817 start msampathkuma
                              if (objSiteNumber.childNodes[0].innerHTML == undefined && !window.ie) {
                                  sitenumber = GetTextFromHTML(objSiteNumber.childNodes[1].innerHTML);
                              }
                              else {
                                  sitenumber = GetTextFromHTML(objSiteNumber.childNodes[0].innerHTML);
                              }
                              //Jira-9817 end msampathkuma
                              //window.opener.document.forms[0].slsiteid.value = sitenumber;
                          }
                      }

                      if (window.opener.document.forms[0].SysFormName.value == 'policy') {
                          window.opener.document.forms[0].SysCmd.value = "";
                          window.opener.SetUnitNameList("S_" + pid + "_0", "Site Unit: " + sitenumber);
                          window.close();

                      }
                      else {
                          window.opener.SiteIdSelected(pid, sitenumber);
                          window.opener.document.forms[0].submit(); //MITS 31063 : aahuja21
                          window.close();
                      }
                      break;
                      //rupal:end
                  // Anjaneya Changes Start : MITS 11639
                  case "piprivilege":
                  case "picertification":
                  case "pieducation":
                  case "piprevhospital":
                      // Anjaneya Changes End   
                
                  default:
                      var obj = eval('window.opener.document.forms[0].' + window.opener.document.forms[0].SysFormIdName.value);
                      if (obj != null)
                      {
                         obj.value = pid;
                      }
                      window.opener.document.forms[0].submit();
                      break;
              }
              if (currentScreen == correspondingScreen) {
                  //if ((formtype != "claimant") && (formtype != "unit")) {
                  if ((formtype != "claimant") && (formtype != "unit") && (formtype != "siteunit")) {                  
                      window.opener.parent.MDIReplaceCurrentNodeByRecordId(rowid);
                  }
              }
          }

          window.close();
          return false;
      }

      function changemode() {
          if (document.forms[0].ordermode.value == "" || document.forms[0].ordermode.value == "DESC")
              document.forms[0].ordermode.value = "ASC";
          else
              document.forms[0].ordermode.value = "DESC";

          return true;
      }
  </script>
</head>
<body>
    <form id="frmData" runat="server">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
    <tr class="msgheader">
     <td class="msgheader" colspan="">Lookup results</td>
    </tr>
   </table>
   
<%--    <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
     <tr class="headertext2">
      <td class="headertext2" align="left">
       										Page 1 of 1
      </td>
      <td class="headertext2" align="right">
       											|&nbsp;First&nbsp;|&nbsp;&lt;&nbsp;Prev&nbsp;
       										
       											&nbsp;Next&nbsp;&gt;&nbsp;|&nbsp;Last&nbsp;
       										
      </td>
     </tr>
    </table>--%>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
<tr class="headertext2">
<td align="left" class="headertext2"><label id="lblPage" runat="server" title="Page"></label>
<asp:dropdownlist id="ddlPage" runat="server" AutoPostBack="True"  Font-Size="Small" > </asp:dropdownlist>
</td>
<td align="right" class="headertext2">
  <asp:LinkButton id="lnkFirst" ForeColor="black" runat="server"  OnCommand = "LinkCommand_Click" CommandName="First">First|</asp:LinkButton> 
  <asp:LinkButton id="lnkPrev" runat="server" ForeColor="black" OnCommand = "LinkCommand_Click" CommandName="Prev">Previous</asp:LinkButton>
    <asp:LinkButton id="lnkNext" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next">|Next</asp:LinkButton>
    <asp:LinkButton id="lnkLast" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last">|Last</asp:LinkButton>
</td>
</tr>
</table>

    <asp:GridView ID="grdLookUp" runat="server"
        onpageindexchanging="grdLookUp_PageIndexChanging" 
        onrowdatabound="grdLookUp_RowDataBound" AllowSorting="true" onsorting="grdLookUp_Sorting"  Width="100%" 
        CellPadding="4 " 
         >
<PagerStyle CssClass="headertext2"  ForeColor=White />
            <HeaderStyle CssClass="colheader6" />
            <AlternatingRowStyle CssClass="data2" /> 
            <FooterStyle  CssClass ="colheader6" />

    </asp:GridView>

<label id="lblNoRecord" runat="server"></label>

     <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
<tr class="headertext2">
<td align="left" class="headertext2"><label id="lblSort" runat="server" title="Sort"></label>
<asp:dropdownlist id="ddlSort" runat="server" AutoPostBack="True"  Font-Size="Small" > </asp:dropdownlist>
<asp:dropdownlist id="ddlOrderMode" runat="server" AutoPostBack="True"  Font-Size="Small" > 
<asp:ListItem Text="ASCENDING" Value="ASC">
</asp:ListItem>
<asp:ListItem Text="DESCENDING" Value="DESC">
</asp:ListItem>
</asp:dropdownlist>
</td>
<td align="right" class="headertext2">
  
</td>
</tr>
</table>
     <%--    <table width="700" bgcolor="lightblue">
    <tr>
    <td align="right">
    <asp:LinkButton id="lnkFirst" runat="server"  OnCommand = "LinkCommand_Click" CommandName="First">First|</asp:LinkButton> <asp:LinkButton id="lnkPrev" runat="server" OnCommand = "LinkCommand_Click" CommandName="Prev">Previous</asp:LinkButton>
    <asp:LinkButton id="lnkNext" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next">|Next</asp:LinkButton>
    <asp:LinkButton id="lnkLast" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last">|Last</asp:LinkButton>
    </td>
    </tr>
    </table>
--%>
     <asp:textbox style="display: none" runat="server" id="orderBy" rmxretainvalue="true" rmxref="Instance/Document/Data/OrderBy"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="ordermode" rmxretainvalue="true" rmxref="Instance/Document/Data/OrderMode"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="showAll" rmxretainvalue="true" rmxref="Instance/Document/Data/ShowAll"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="form" rmxretainvalue="true" rmxref="Instance/Document/Data/@form"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="fpid" rmxretainvalue="true" rmxref="Instance/Document/Data/@fpid"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="recordcount"  rmxref="Instance/Document/Data/@recordcount"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="pagecount"  rmxref="Instance/Document/Data/@pagecount"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="fpidname" rmxretainvalue="true" rmxref="Instance/Document/Data/@fpidname"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="sid" rmxretainvalue="true" rmxref="Instance/Document/Data/@sid"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="psid" rmxretainvalue="true" rmxref="Instance/Document/Data/@psid"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="claimid" rmxretainvalue="true" rmxref="Instance/Document/Data/@claimid"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="sys_ex" rmxretainvalue="true" rmxref="Instance/Document/Data/@sys_ex"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="pagenumber" rmxref="Instance/Document/Data/@pagenumber"
                 Text=""   rmxtype="hidden" />
                  <asp:textbox style="display: none" runat="server" id="thispage" rmxref="Instance/Document/Data/@thispage"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="pagesize" rmxref="Instance/Document/Data/@pagesize"
                 Text=""   rmxtype="hidden" /> 
    </form>
</body>
</html>
