﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.LookupData
{
    public partial class LookUpNavigation : NonFDMBasePageCWS
    {
        ArrayList arrIndex = null;
        string m_sData = "";
        override protected void OnInit(EventArgs e)
        {
            bool bReturnStatus = false;
            
            string sreturnValue = "";
            this.ddlSort.SelectedIndexChanged += new EventHandler(ddlSort_SelectedIndexChanged);
            this.ddlPage.SelectedIndexChanged += new EventHandler(ddlPage_SelectedIndexChanged);
            this.ddlOrderMode.SelectedIndexChanged += new EventHandler(ddlSort_SelectedIndexChanged);
            
            try
            {

                if (!IsPostBack)
                {
                    form.Text = AppHelper.GetQueryStringValue("SysFormName");
                    parentid.Text = AppHelper.GetQueryStringValue("ParentID");
                    //10/09/2009 Raman Bhatia: Creating Lookup for Reserve History in Reserve Worksheet
                    parentsysformname.Text = AppHelper.GetQueryStringValue("parentsysformname");

                    TextBox txtpagecount = (TextBox)this.Form.FindControl("pagecount");
                    bReturnStatus = CallCWSFunctionBind("LookupDataAdaptor.GetLookupData", out sreturnValue);
                    ErrorControl1.errorDom = sreturnValue;
                    m_sData = sreturnValue;
                    if (bReturnStatus || !ErrorControl1.errorFlag)
                    {
                        for (int intcount = 1; intcount <= Convert.ToInt32(txtpagecount.Text); intcount++)
                        {
                            ListItem LI = new ListItem();
                            LI.Value = Convert.ToString(intcount);

                            LI.Text = Convert.ToString(intcount) + " Of " + txtpagecount.Text;
                            ddlPage.Items.Add(LI);
                        }
                      
                        BindGridToXml(sreturnValue);

                        XElement objLookupDoc = XElement.Parse(sreturnValue);

                        var objColXml = from cols in objLookupDoc.Descendants("ColHead")

                                        select cols;

                        foreach (XElement column in objColXml)
                        {
                            ListItem LI = new ListItem();
                            LI.Value = column.Attribute("DbFieldName").Value;
                            LI.Text = column.Attribute("title").Value;
                            ddlSort.Items.Add(LI);
                        }
                    }
                    if (ddlSort.Items.Count > 0)
                    {    //MITS 15945
                        //It is not necessary that when page is loaded first time , data are sorted by first coloumn.
                        //e.g. in case of bank account first time data get sorted by Account_Id but first Coloumn on page is Account_Number
                        // orderBy.Text = ddlSort.Items[0].Value;
                        ordermode.Text = "ASC";
                    }
                    ddlSort.Visible = false;
                    ddlOrderMode.Visible = false;
                    ddlPage.Visible = false;
                    lnkFirst.Enabled = true;
                    lnkLast.Enabled = true;
                    lnkPrev.Enabled = true;
                    lnkNext.Enabled = true;
                    TextBox txtthispage = (TextBox)this.Form.FindControl("thispage");
                    if (txtthispage != null && txtthispage.Text == "1")
                    {
                        lnkFirst.Enabled = false;
                        lnkPrev.Enabled = false;
                    }
                    if (pagesize != null && recordcount != null && Convert.ToInt32(pagesize.Text) > Convert.ToInt32(recordcount.Text))
                    {
                        lnkLast.Enabled = false;
                        lnkNext.Enabled = false;
                    }

                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private void ddlPage_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
            if (txtpagenumber != null)
                txtpagenumber.Text = ddlPage.SelectedItem.Value;
            EnableLinks();

        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private BoundField CreateBoundColumn(DataColumn c)
        {

            BoundField column = new BoundField();
            column.DataField = c.ColumnName;
            column.SortExpression = c.ColumnName;
            column.HeaderText = c.ColumnName.Replace("_", " ");

            return column;


        }
        private HyperLinkField CreateHyperLinkColumn(DataColumn c)
        {

            HyperLinkField column = new HyperLinkField();
            column.SortExpression = c.ColumnName;
            column.HeaderText = c.ColumnName.Replace("_", " ");
            column.DataTextField = c.ColumnName;
            column.NavigateUrl = "OpenRecord({0},{0});";
            return column;



        }

        protected void LinkCommand_Click(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToUpper())
            {
                case "FIRST":
                    {
                        TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
                        if (txtpagenumber != null)
                            txtpagenumber.Text = "1";
                        ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;

                        break;
                    }
                case "LAST":
                    {

                        TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
                        TextBox txtpagecount = (TextBox)this.Form.FindControl("pagecount");
                        if (txtpagenumber != null && txtpagecount != null && txtpagecount.Text != "")
                            txtpagenumber.Text = txtpagecount.Text;
                        ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;

                        break;
                    }
                case "PREV":
                    {
                        TextBox txtthispage = (TextBox)this.Form.FindControl("thispage");
                        TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");

                        if (txtpagenumber != null && txtthispage != null && txtthispage.Text != "")
                            txtpagenumber.Text = Convert.ToString(Convert.ToInt32(txtthispage.Text) - 1);
                        ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;
                        break;
                    }
                case "NEXT":
                    {
                        TextBox txtthispage = (TextBox)this.Form.FindControl("thispage");
                        TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");

                        if (txtpagenumber != null && txtthispage != null && txtthispage.Text != "")
                            txtpagenumber.Text = Convert.ToString(Convert.ToInt32(txtthispage.Text) + 1);
                        ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;
                        break;
                    }
            }

            //grdLookUp.DataSource = (DataSet)ViewState["DS"];
            //grdLookUp.DataBind();
            //ddlPage.SelectedIndex = grdLookUp.CurrentPageIndex;
            EnableLinks();

        }
        void EnableLinks()
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            bReturnStatus = CallCWSFunctionBind("LookupDataAdaptor.GetLookupData", out sreturnValue);
            m_sData = sreturnValue;
            ErrorControl1.errorDom = sreturnValue;
            if (bReturnStatus || !ErrorControl1.errorFlag)
            {
                BindGridToXml(sreturnValue);
            }
            lnkFirst.Enabled = true;
            lnkLast.Enabled = true;
            lnkPrev.Enabled = true;
            lnkNext.Enabled = true;
            TextBox txtthispage = (TextBox)this.Form.FindControl("thispage");
            TextBox txtpagecount = (TextBox)this.Form.FindControl("pagecount");
            if (txtthispage != null && txtthispage.Text == "1")
            {
                lnkFirst.Enabled = false;
                lnkPrev.Enabled = false;
            }
            if ((txtthispage != null && txtpagecount != null && txtthispage.Text == txtpagecount.Text) || (pagesize != null && recordcount != null && Convert.ToInt32(pagesize.Text) > Convert.ToInt32(recordcount.Text)))
            {
                lnkLast.Enabled = false;
                lnkNext.Enabled = false;
            }
        }
        private string AlignMatrix(string type)
        {
            string ret = "center";
            switch (type)
            {
                case "varchar":
                    ret = "left";
                    break;
                case "smallint":
                    ret = "center";
                    break;
                case "int":
                case "float":
                case "double":
                    ret = "right";
                    break;
            }
            return ret;
        }
        private string SetAlignment(string colname)
        {
            if (m_sData != "")
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(m_sData);
                XmlNode node = doc.SelectSingleNode("//ColHead[@title='" + colname + "']");
                if (node != null)
                {
                    if (colname.ToLower().IndexOf("date") != -1 || colname.ToLower().IndexOf("insured") != -1)
                    {
                        return "center";
                    }
                    return AlignMatrix(node.SelectSingleNode("@DbFieldType").Value);
                }

            }
            return "";

        }
        protected void grdLookUp_RowDataBound(object Sender, GridViewRowEventArgs e)
        {

            //if (grdLookUp.DataSource != null)
            //{
            //    if (e.Row.RowType == DataControlRowType.DataRow)
            //    {
            //        DataRowView drv = (DataRowView)e.Row.DataItem;
            //        TableCell nameCell = e.Row.Cells[0];
            //        HyperLink link = (HyperLink)nameCell.Controls[0];
            //        link.NavigateUrl = "javascript:if(OpenRecord(" + drv
            //     ["pid"].ToString() + "," + drv
            //     ["pid"].ToString() + "))";
            //    }
            //}
            int iTempIndex = 0;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    string Header = ((System.Web.UI.WebControls.DataControlFieldCell)(cell)).ContainingField.HeaderText;
                    cell.Attributes.Add("align", SetAlignment(Header));

                    if (Header.ToUpper() == "COUNTRYID")
                    {
                        cell.Attributes.CssStyle.Add("display", "none");
                    }
                }
                DataRowView objDataRow = (DataRowView)e.Row.DataItem;

                e.Row.ID = objDataRow["pid"].ToString();

                try
                {
                    if (objDataRow["pid2"] != null)
                        e.Row.Attributes.Add("pid2", objDataRow["pid2"].ToString());

                    if (objDataRow["pid3"] != null)
                        e.Row.Attributes.Add("pid3", objDataRow["pid3"].ToString());

                    if (objDataRow["pid4"] != null)
                        e.Row.Attributes.Add("pid4", objDataRow["pid4"].ToString());//Deb

                    //Added:Yukti, DT:05/22/2014, MITS 35772
                    if (objDataRow["pid5"] != null)
                        e.Row.Attributes.Add("pid5", objDataRow["pid5"].ToString());
                }
                catch { }

                e.Row.Cells[0].Text = "<a href='#'>" + e.Row.Cells[0].Text + "</a>";
                e.Row.Cells[0].Attributes["onclick"] = String.Format("OpenRecord({0},{1})", objDataRow["pid"].ToString(), objDataRow["pid"].ToString()) + ";return false;";


                if (((e.Row.RowIndex + 1) % 2) == 0)
                {
                    e.Row.CssClass = "rowdark2";
                }

                if (arrIndex != null)
                {
                    for (int i = 0; i < arrIndex.Count; i++)
                        e.Row.Cells[Conversion.ConvertObjToInt(arrIndex[i])].Visible = false;
                }
              }

            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    arrIndex = new ArrayList();

            //    foreach (TableCell cell in e.Row.Cells)
            //    {
            //        if ((cell.Text == "pid") || (cell.Text == "pid2") || (cell.Text == "pid3"))
            //        {
            //            cell.Visible = false;
            //            arrIndex.Add(iTempIndex);
            //        }
            //        iTempIndex++;
            //    }
            //    e.Row.CssClass = "colheader3";
            //}
            if (e.Row.RowType == DataControlRowType.Header)
            {
                arrIndex = new ArrayList();
                foreach (TableCell cell in e.Row.Cells)
                {
                    DataControlFieldCell gridViewCell = cell as DataControlFieldCell;
                    if (gridViewCell != null)
                    {
                        DataControlField cellsField = gridViewCell.ContainingField;
                        cellsField.HeaderStyle.ForeColor = System.Drawing.Color.White;
                        cell.Attributes.Add("align", SetAlignment(cellsField.SortExpression));
                        //ADded:Yukti, MITS 35772
                        if (cellsField != null && ((cellsField.SortExpression == "pid") || (cellsField.SortExpression == "pid2") || (cellsField.SortExpression == "pid3") || (cellsField.SortExpression == "pid4") || (cellsField.SortExpression == "pid5")))//Deb
                        {
                            cell.Visible = false;
                            arrIndex.Add(iTempIndex);                        
                        }
                        if (cellsField.HeaderText.ToUpper() == "COUNTRYID")
                        {
                            cell.Attributes.CssStyle.Add("display", "none");
                        }
                        iTempIndex++;
                        //MGaba2:MITS 15638:Sorting on Dated Text is not working:Start
                        //In lookup.cs, nosort attribute is set for field types text/ntext/image/clob
                        //We are preventing sorting on such columns by disabling them
                        if (m_sData != "")
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(m_sData);
                            XmlNode node = doc.SelectSingleNode("//ColHead[@title='" + cellsField.SortExpression + "']");
                            if (node != null)
                            {
                                if (node.Attributes["nosort"] != null)
                                {
                                    ((LinkButton)cell.Controls[0]).Enabled = false;                                   
                                }
                            }
                        }
                        //MGaba2:MITS 15638:Sorting on Dated Text not working:End
                    }
                }
               // e.Row.CssClass = "colheader3";
            }


        }

        private void ddlSort_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
            if (txtpagenumber != null)
                txtpagenumber.Text = "1";
            ddlPage.SelectedIndex = 0;
            TextBox txtordermode = (TextBox)this.Form.FindControl("ordermode");
            if (txtordermode != null)
            {
                txtordermode.Text = ddlOrderMode.SelectedItem.Value;
            }

            TextBox txtorderBy = (TextBox)this.Form.FindControl("orderBy");
            if (txtorderBy != null)
            {
                txtorderBy.Text = ddlSort.SelectedItem.Value;
            }
            EnableLinks();

        }
        protected void grdLookUp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            grdLookUp.PageIndex = e.NewPageIndex;
            bReturnStatus = CallCWSFunction("LookupDataAdaptor.GetLookupData", out sreturnValue);
            ErrorControl1.errorDom = sreturnValue;
            if (bReturnStatus || !ErrorControl1.errorFlag)
            {
                BindGridToXml(sreturnValue);
            }

        }

        private void BindGridToXml(string sReturnValue)
        {
            DataRow drLookUpRow = null;
            DataTable dtLookUp = new DataTable();

            XElement objLookupDoc = XElement.Parse(sReturnValue);

            var objColXml = from cols in objLookupDoc.Descendants("ColHead")

                            select cols;

            foreach (XElement column in objColXml)
            {
                dtLookUp.Columns.Add(new DataColumn(column.Attribute("title").Value));
            }

            dtLookUp.Columns.Add("pid");

            var objRowXml = from rows in objLookupDoc.Descendants("Row")

                            select rows;

            int iAttributeCount = 0;

            if (objRowXml.Count() != 0)
            {
                foreach (XAttribute attribute in objRowXml.ElementAt(0).Attributes())
                {
                    iAttributeCount++;
                    if (attribute != null)
                    {
                        if (attribute.Name.LocalName.ToLower() == "pid")
                            continue;

                        dtLookUp.Columns.Add(attribute.Name.LocalName.ToLower());
                    }
                }
            }

            string[] arrPid = new string[iAttributeCount];

            string sDescendant = string.Empty;
            // npadhy MITS 15832 Handled an Extra comma coming while concatenating two fields with comma
            string sValue = string.Empty;
            string sTrimmedValue = string.Empty;
            foreach (XElement row in objRowXml)
            {
                arrPid[0] = "pid";
                if (row.Attribute("pid2") != null)
                    arrPid[1] = "pid2";
                
                if (row.Attribute("pid3") != null)
                    arrPid[2] = "pid3";

                if (row.Attribute("pid4") != null)
                    arrPid[3] = "pid4";//Deb

                //Added:Yukti ,MITS 35772
                if (row.Attribute("pid5") != null)
                    arrPid[4] = "pid5";
                grdLookUp.DataKeyNames = arrPid;

                drLookUpRow = dtLookUp.NewRow();
                foreach (XElement column in objColXml)
                {
                    sDescendant = column.Attribute("DbFieldName").Value;
                    // npadhy MITS 15832 Handled an Extra comma coming while concatenating two fields with comma
                    sValue = row.Element(sDescendant).Value;
                    sTrimmedValue = sValue.Trim();
                    if (sTrimmedValue == ",")
                        sValue = "";
                    else if (sTrimmedValue != row.Element(sDescendant).Value)
                    {
                        if (sTrimmedValue.StartsWith(","))
                        {
                            sValue = sTrimmedValue.Substring(1);
                        }
                        else if (sTrimmedValue.EndsWith(","))
                        {
                            sValue = sTrimmedValue.Substring(0, sTrimmedValue.Length - 1);
                        }
                    }
                    drLookUpRow[column.Attribute("title").Value] = sValue;
                }

                drLookUpRow["pid"] = row.Attribute("pid").Value;

                if (row.Attribute("pid2") != null)
                    drLookUpRow["pid2"] = row.Attribute("pid2").Value;

                if (row.Attribute("pid3") != null)
                    drLookUpRow["pid3"] = row.Attribute("pid3").Value;

                if (row.Attribute("pid4") != null)
                    drLookUpRow["pid4"] = row.Attribute("pid4").Value;//Deb

                //Added:Yukti, MITS 35772
                if (row.Attribute("pid5") != null)
                    drLookUpRow["pid5"] = row.Attribute("pid5").Value;
                dtLookUp.Rows.Add(drLookUpRow);
            }

            // Bind DataTable to GridView.
            grdLookUp.Visible = true;
            grdLookUp.PageSize = Convert.ToInt32(pagesize.Text);
            grdLookUp.DataSource = dtLookUp;
            grdLookUp.DataBind();
        }
        protected void grdLookUp_Sorting(object sender, GridViewSortEventArgs e)
        {
            //TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
            //if (txtpagenumber != null)
            //    txtpagenumber.Text = "1";
            //ddlPage.SelectedIndex = 0;

            string sPrevorderBy = "";
            string sPrevordermode = "";
            TextBox txtorderBy = (TextBox)this.Form.FindControl("orderBy");
            if (txtorderBy != null)
            {
                sPrevorderBy = txtorderBy.Text;
                //txtorderBy.Text = ddlSort.SelectedItem.Value;
                txtorderBy.Text = ddlSort.Items.FindByText(e.SortExpression).Value;
            }
            TextBox txtordermode = (TextBox)this.Form.FindControl("ordermode");
            
            if (txtordermode != null)
            {
                sPrevordermode = txtordermode.Text;
                if (sPrevordermode == "")
                    sPrevordermode = "ASC";
                 if ((sPrevorderBy == txtorderBy.Text))
                 {
                     if (sPrevordermode=="ASC")
                         sPrevordermode = "DESC";
                     else
                         sPrevordermode = "ASC";
                 }
                 else
                 {
                     sPrevordermode = "ASC";
                 }
                 txtordermode.Text = sPrevordermode;
                //txtordermode.Text = ddlOrderMode.SelectedItem.Value;
            }
            EnableLinks();

        }
    }
}
