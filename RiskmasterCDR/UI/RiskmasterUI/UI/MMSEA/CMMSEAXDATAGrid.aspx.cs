﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;

namespace Riskmaster.UI.UI.MMSEA
{
    public partial class CMMSEAXDATAGrid : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            try
            {
                if (!Page.IsPostBack)
                {
                    entityid.Text = AppHelper.GetQueryStringValue("EntityId");
                    entitytableid.Text = AppHelper.GetQueryStringValue("EntityLevel");
                    //sgoel6 Medicare Entity Maintenance Form
                    callerformname.Text = AppHelper.GetQueryStringValue("formname");
                }
                if (CMMSEAXDataGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = CMMSEAXDataSelectedId.Text;
                    XmlTemplate = GetDeletionTemplate(selectedRowId);
                    CallCWS("CMMSEAXDataAdaptor.DeleteMMSEAXData", XmlTemplate, out sCWSresponse, false, false);
                    CMMSEAXDataGrid_RowDeletedFlag.Text = "false";
                }
                XmlTemplate = GetMessageTemplate(entityid.Text);
                CallCWSFunctionBind("CMMSEAXDataAdaptor.GetMMSEAXDataList", out sCWSresponse, XmlTemplate);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplate(string sEntityId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><MMSEAXDataDetails>");
            sXml = sXml.Append("<EntityEID>");
            sXml = sXml.Append(sEntityId);
            sXml = sXml.Append("</EntityEID>");
            sXml = sXml.Append("</MMSEAXDataDetails></Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
        private XElement GetDeletionTemplate(string selectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><MMSEAXDataDetails>");
            sXml = sXml.Append("<EntMMSEARowID>");
            sXml = sXml.Append(selectedRowId);
            sXml = sXml.Append("</EntMMSEARowID>");
            sXml = sXml.Append("</MMSEAXDataDetails></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
    }
}
