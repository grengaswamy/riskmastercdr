﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.FDM;

namespace Riskmaster.UI.UI.MMSEA
{
    public partial class TPOCData : GridPopupBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ////MITS 34108 - rkulavil 
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //MITS 34108 - rkulavil
            GridPopupPageload();
        }
    }
}
