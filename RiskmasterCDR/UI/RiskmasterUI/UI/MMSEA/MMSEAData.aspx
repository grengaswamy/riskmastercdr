<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MMSEAData.aspx.cs" ValidateRequest="false"
    Inherits="Riskmaster.UI.MMSEA.MMSEAData" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<%@ Register Src="~/UI/Shared/Controls/MultiCodeLookup.ascx" TagName="MultiCode"
    TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/SystemUsers.ascx" TagName="SystemUsers" TagPrefix="cul" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"
    TagPrefix="mc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Claimant MMSEA Data</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }  </script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js">        { var i; }  </script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }    </script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }    </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }    </script>
    <!--MITS 34107 : Rakhel ML-->
    <!--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }   </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }   </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }   </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }   </script> !-->

     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <!--MITS 34107 : Rakhel ML-->
    <script type="text/javascript" language="javascript">
        window.onbeforeunload =
             function () {
                 if ((window.event.clientX < 0) || (window.event.clientY < 0)) { //if close window
                     if (document.forms[0].SysPageDataChanged.value == "true") {
                         //return "You have not saved the recent changes. If you want to keep changes please save the changes. ";
                         return MMSEADataValidations.SaveChanges;
                     }
                 }

             }
        function CheckForFileDate() {

            var sRowPos = GetPositionForSelectedGridRow("TPOCGrid", document.forms[0].TPOCSelectedId.value, "false")

            if (sRowPos * 1 < 10) {
                sRowPos = "0" + sRowPos;
            }

            DateFiledId = "TPOCGrid_gvData_ctl" + sRowPos + "_hfGrid";
            svalue = document.getElementById(DateFiledId).value;

            if (Trim(svalue) != "") {
                //sgoel6 MITS 16899
                document.getElementById("TPOCGrid_Delete").style.setAttribute('display', 'none');
                document.getElementById("hdnTPOCGrid_Delete").value = "false";
            }
            else {
                //sgoel6 MITS 16899
                document.getElementById("TPOCGrid_Delete").style.setAttribute('display', '');
                document.getElementById("hdnTPOCGrid_Delete").value = "true";
            }

        }

        function setMMSEA() {

            var objMMSEAEdit = document.getElementById("MMSEAEditedFlag");
            if (objMMSEAEdit != null) {
                objMMSEAEdit.value = "1";
            }
        }

        function setValue(sFielMark) {

            document.getElementById(sFielMark + "lastfirstname_cid").value = document.getElementById(sFielMark + "entityid").value

        }

        function Validate() {

            var objIsBen = document.getElementById("IsBenificaryCode_codelookup");

            if (Trim(objIsBen.value) == "") {
                //alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
                alert(MMSEADataValidations.ValidationRequiredFields);
                tabChange('beneficiary');
                return false;
            }
            else {
                if (document.forms[0].SysPageDataChanged.value == "true") {
                    pleaseWait.Show();
                }
                else {
                    return false;
                }

            }

            return true;
        }
        //sgoel6 MITS 16899
        function SetUnDirty() {
            parent.MDISetUnDirty(null, '0', false);
        }
    
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:hiddenfield runat="server" id="wsrp_rewrite_action_1" value="" />
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:textbox style="display: none" runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager
        id="SMgr" runat="server" /><div id="toolbardrift" name="toolbardrift" class="toolbardrift"
            runat="server">
            <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                <asp:imagebutton runat="server" src="../../Images/tb_save_active.png" width="28"
                    height="28" border="0" id="save" alternatetext="<%$ Resources:imgbtnSave %>" onmouseover="this.src='../../Images/tb_save_mo.png';"
                    onmouseout="this.src='../../Images/tb_save_active.png';" onclientclick="return Validate();"
                    onclick="save_Click">
                </asp:imagebutton></div>
        </div>
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="<%$ Resources:lblFormTitle %>" /><asp:label
            id="formsubtitle" runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>
    <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSbeneficiary" id="TABSbeneficiary">
            <a class="Selected" href="#" onclick="tabChange(this.name);return false;" runat="server"
                rmxref="" name="beneficiary" id="LINKTABSbeneficiary">
                <asp:label id="lblBenAwardTPOC" runat="server" text="<%$ Resources:lblBenAwardTPOC %>"></asp:label></a></div>
        <div class="tabSpace" runat="server" id="TBSPbeneficiary">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSDiagnosis" id="TABSDiagnosis">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                rmxref="" name="Diagnosis" id="LINKTABSDiagnosis"><asp:label id="lblDiagPartiesToClaim" runat="server" text="<%$ Resources:lblDiagPartiesToClaim %>"></asp:label></a></div>
        <div class="tabSpace" runat="server" id="TBSPDiagnosis">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSProduct" id="TABSProduct">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                rmxref="" name="Product" id="LINKTABSProduct"><asp:label id="lblProdLiabMMSEAReturned" runat="server" text="<%$ Resources:lblProdLiabMMSEAReturned %>"></asp:label></a></div>
        <div class="tabSpace" runat="server" id="TBSPProduct">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABbeneficiary" id="FORMTABbeneficiary">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
            <asp:textbox runat="server" style="display: none" id="DttmRcdLastUpd" rmxref="/Instance/Document/ClaimantMMSEA/DttmRcdLastUpd">
            </asp:textbox>
            <asp:textbox runat="server" style="display: none" id="MMSEAEditedFlag" rmxref="/Instance/Document/ClaimantMMSEA/MMSEAEditedFlag"
                rmxtype="text" />
            <asp:textbox style="display: none" runat="server" id="DelMDCRERCDFlag" rmxref="/Instance/Document/ClaimantMMSEA/DelMDCRERCDFlag"
                rmxtype="text" />
            <asp:textbox style="display: none" runat="server" id="DelMDCRERCDDate" rmxref="/Instance/Document/ClaimantMMSEA/DelMDCRERCDDate"
                rmxtype="text" />
            <asp:textbox runat="server" id="txtGenerateXml" style="display: none" text="true">
            </asp:textbox>
            <input type="hidden" id="SysFormIdName" value="MMSEAData" />
            <input type="hidden" id="SysFormName" value="MMSEAData" />
            <input type="hidden" id="SysFormPIdName" value="" />
            <input type="hidden" id="SysCmd" value="" />
            <tr>
                <td>
                    <asp:textbox style="display: none" runat="server" id="claimantrowid" text="" rmxref="/Instance/Document/ClaimantMMSEA/ClaimantRowID"
                        rmxtype="text" />
						<!-- rkulavil - ML Changes - MITS 34107  -start !-->
                         <asp:textbox style="display: none" runat="server" id="langcodeid" text="" rmxref="/Instance/Document/ClaimantMMSEA/LangCodeID"
                        rmxtype="text"></asp:textbox>
                         <asp:textbox style="display: none" runat="server" id="pageinfoid" text="" rmxref="/Instance/Document/ClaimantMMSEA/PageID"
                        rmxtype="text"></asp:textbox>
						<!-- rkulavil - ML Changes - MITS 34107  -end !-->
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_IsBenificaryCode" xmlns="">
                        <!-- rsushilaggar 02/01/10 :MITS 18349 : Beneficiary is misspelled in two places in MMSEA Data window -->
                        <asp:label runat="server" width="55%" class="required" id="lbl_IsBenificaryCode"
                            text="<%$ Resources:lblIsBenificaryCode %>" /><span class="formw"><uc:CodeLookUp runat="server"
                                width="45%" ID="IsBenificaryCode" CodeTable="YES_NO" ControlName="IsBenificaryCode"
                                RMXRef="/Instance/Document/ClaimantMMSEA/IsBenificaryCode" RMXType="code" TabIndex="1"
                                Required="true" OnChange="setMMSEA();" ValidationGroup="vgSave" />
                            </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_IsPaymentNonMedical" xmlns="">
                        <asp:label runat="server" width="55%" class="label" id="lbl_IsPaymentNonMedical"
                            text="<%$Resources:lblIsPaymentNonMedical %>" /><span class="formw"><uc:CodeLookUp
                                runat="server" width="45%" ID="IsPaymentNonMedical" CodeTable="YES_NO" ControlName="IsPaymentNonMedical"
                                RMXRef="/Instance/Document/ClaimantMMSEA/IsPaymentNonMedical" OnChange="setMMSEA();"
                                RMXType="code" TabIndex="2" />
                            </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_ConfirmedDate" xmlns="">
                        <!-- rsushilaggar 02/01/10 :MITS 18349 : Beneficiary is misspelled in two places in MMSEA Data window -->
                        <asp:label runat="server" width="55%" class="label" id="lbl_ConfirmedDate" text="<%$ Resources:lblConfirmedDate %>" /><span
                            class="formw"><asp:textbox runat="server" width="45%" formatas="date" id="ConfirmedDate"
                                rmxref="/Instance/Document/ClaimantMMSEA/ConfirmedDate" rmxtype="date" tabindex="3"
                                onchange="dateLostFocus(this.id);setDataChanged(true);setMMSEA();" onblur="dateLostFocus(this.id);" />
                         <script type="text/javascript">
                             $(function () {
                                 $("#ConfirmedDate").datepicker({
                                     showOn: "button",
                                     buttonImage: "../../Images/calendar.gif",
                                     //buttonImageOnly: true,
                                     showOtherMonths: true,
                                     selectOtherMonths: true,
                                     changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "4");
                             });
                               </script>
                         </span>
                         </div>
                         
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_HealthClaimNumber" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_HealthClaimNumber" text="<%$ Resources:lblHealthClaimNumber %>" /><span
                            class="formw"><asp:textbox runat="server" width="45%" maxlength="12" id="HealthClaimNumber"
                                rmxref="/Instance/Document/ClaimantMMSEA/HealthClaimNumber" rmxtype="text" tabindex="5"
                                onchange="setMMSEA();setDataChanged(true);" /></span></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_ICDEventCode" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_ICDEventCode" text="<%$ Resources:lblICDEventCode %>">
                        </asp:label>
                        <span class="formw">
                            <!-- rsushilaggar MITS 25692 date 08/31/2011-->
                            <%--<uc:CodeLookUp runat="server" width="45%"  ID="ICDEventCode" CodeTable="CDC_EVENT_CODE" ControlName="ICDEventCode"
                                                    RMXRef="/Instance/Document/ClaimantMMSEA/ICDEventCode" RMXType="code" TabIndex="5" Required="true" OnChange="setMMSEA();"
                                                    ValidationGroup="vgSave" />--%>
                            <asp:textbox runat="server" size="3" id="CSCICDEventCode" rmxref="/Instance/Document/ClaimantMMSEA/ICDEventCode"
                                rmxtype="code" tabindex="9" />
                            <asp:button id="Button1" runat="server" class="CodeLookupControl" onclientclick="javascript: return callDiagSearch('CSCICDEventCode')"
                                value="..." tabindex="10" />
                            <asp:textbox id="CSCICDEventCode_cid" style="display: none" runat="server" rmxtype="code"
                                rmxref="/Instance/Document/ClaimantMMSEA/ICDEventCode/@codeid" />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_ICD10EventCode" xmlns="">
                        <asp:label runat="server" class="label" width="55%"  id="lbl_ICD10EventCode" 
                            text="<%$ Resources:lblICD10EventCode  %>" ></asp:label>
                            <span class="formw">
                           <asp:TextBox runat="server" size="3" id="CSCICD10EventCode" RMXRef="/Instance/Document/ClaimantMMSEA/ICD10EventCode" RMXType="code" Tabindex="11" />
                          <asp:button id="Button2" runat="server" class="CodeLookupControl" onclientclick="javascript: return callDiagSearch('CSCICD10EventCode')" value="..." tabindex="12" />
                          <asp:TextBox ID="CSCICD10EventCode_cid" style="display:none" runat ="server" RMXType="code" RMXRef="/Instance/Document/ClaimantMMSEA/ICD10EventCode/@codeid"/>
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_LastExtractDate" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_LastExtractDate" text="<%$ Resources:lblLastExtractDate %>" /><span
                            class="formw"><asp:textbox runat="server" width="45%" formatas="date" id="LastExtractDate"
                                rmxref="/Instance/Document/ClaimantMMSEA/LastExtractDate" rmxtype="date" tabindex="13"
                                onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                                     <script type="text/javascript">
                                         $(function () {
                                             $("#LastExtractDate").datepicker({
                                                 showOn: "button",
                                                 buttonImage: "../../Images/calendar.gif",
                                                 //buttonImageOnly: true,
                                                 showOtherMonths: true,
                                                 selectOtherMonths: true,
                                                 changeYear: true
                                             }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "14");
                                         });
                                    </script>
                                     </span></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div2" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lblDeleteRec" text="<%$ Resources:lblDeleteRec %>" /><span
                            class="formw"><asp:dropdownlist id="ddlDeleteRcd" runat="server" onchange="setDataChanged(true);setMMSEA();">
                                <asp:listitem value="1" text="<%$ Resources:liYes %>" ></asp:listitem>
                                <asp:listitem value="0" selected="True" text="<%$ Resources:liNo %>"></asp:listitem>
                            </asp:dropdownlist></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <!-- asingh263 mits 30525-->
            <% if (bIsCarrier.Text == "-1")
               {%>
            <tr>
                <td align="left" colspan="2">
                    <b>
                        <asp:label id="lblMMSEANoFault" runat="server" text="<%$ Resources:lblMMSEANoFault %>"></asp:label></b>
                    <hr />
                </td>
            </tr>
            <td>
                <div runat="server" class="completerow" id="div_NoFaultIndicatorfirst" xmlns="">
                    <asp:label runat="server" class="label" width="55%" id="lbl_NoFaultIndicatorfirst"
                        text="<%$ Resources:lblNoFaultIndicatorfirst %>" /><span class="formw"><uc:CodeLookUp runat="server" width="45%"
                            ID="NoFaultIndicatorfirst" CodeTable="YES_NO" ControlName="NoFaultIndicatorfirst"
                            RMXRef="/Instance/Document/ClaimantMMSEA/CarrierNoFaultIndicator" OnChange="setMMSEA();"
                            RMXType="code" TabIndex="15" />
                        </span>
                </div>
            </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_NoFaultInsuranceLimitfirst" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_NoFaultInsuranceLimitfirst"
                            text="<%$ Resources:lblNoFaultInsuranceLimitfirst %>" /><span class="formw">
                                <mc:CurrencyTextbox runat="server" Width="45%" ID="NoFaultInsuranceLimitfirst" RMXRef="/Instance/Document/ClaimantMMSEA/CarrierNoFaultInsuranceLimit"
                                    rmxtype="currency" TabIndex="16" onChange="setDataChanged(true);" MaxLength="40" />
                            </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_NoFaultExhaustDatefirst" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_NoFaultExhaustDatefirst"
                            text="<%$ Resources:lblNoFaultExhaustDatefirst %>" /><span class="formw"><asp:textbox runat="server" width="45%"
                                formatas="date" id="NoFaultExhaustDatefirst" rmxref="/Instance/Document/ClaimantMMSEA/CarrierNoFaultExhaustDate"
                                rmxtype="date" tabindex="17" onchange="dateLostFocus(this.id);setDataChanged(true);"
                                onblur="dateLostFocus(this.id);" />
                                    <script type="text/javascript">
                                        $(function () {
                                            $("#NoFaultExhaustDatefirst").datepicker({
                                                showOn: "button",
                                                buttonImage: "../../Images/calendar.gif",
                                                //buttonImageOnly: true,
                                                showOtherMonths: true,
                                                selectOtherMonths: true,
                                                changeYear: true
                                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "18");
                                        });
                                    </script>
                                    </span></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <%} %>
            <!--mits 30525 end-->
            <tr>
                <td align="left" colspan="2">
                    <b>
                        <asp:label id="lblAwardJudgementPayment" runat="server" text="<%$ Resources:lblAwardJudgementPayment %>"></asp:label></b>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_ResponsiblityForMedicalsCode" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_ResponsiblityForMedicalsCode"
                            text="<%$ Resources:lblResponsiblityForMedicalsCode %>" /><span class="formw"><uc:CodeLookUp
                                runat="server" width="45%" ID="ResponsiblityForMedicalsCode" CodeTable="YES_NO"
                                ControlName="ResponsiblityForMedicalsCode" RMXRef="/Instance/Document/ClaimantMMSEA/ResponsiblityForMedicalsCode"
                                RMXType="code" TabIndex="19" />
                            </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_ResponsibilityTerminationDate" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_ResponsibilityTerminationDate"
                            text="<%$ Resources:lblResponsibilityTerminationDate %>" /><span class="formw"><asp:textbox runat="server" width="45%"
                                formatas="date" id="ResponsibilityTerminationDate" rmxref="/Instance/Document/ClaimantMMSEA/ResponsibilityTerminationDate"
                                rmxtype="date" tabindex="20" onchange="dateLostFocus(this.id);setDataChanged(true);setMMSEA();"
                                onblur="dateLostFocus(this.id);" />
                                    <script type="text/javascript">
                                        $(function () {
                                            $("#ResponsibilityTerminationDate").datepicker({
                                                showOn: "button",
                                                buttonImage: "../../Images/calendar.gif",
                                                //buttonImageOnly: true,
                                                showOtherMonths: true,
                                                selectOtherMonths: true,
                                                changeYear: true
                                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "21");
                                        });
                                    </script>
                                    </span></div>
                </td>
            </tr>
            <tr>
                <td>
                    <dg:UserControlDataGrid runat="server" ID="TPOCGrid" GridName="TPOCGrid" TextColumn="<%$ Resources:gvhdrFilMdcreRcdDATE %>"
                        GridTitle="<%$ Resources:HdrTitle %>" Target="Document/ClaimantMMSEA/TPOC"
                        Ref="Document/ClaimantMMSEA/TPOC" Unique_Id="TpocRowID" ShowRadioButton="true"
                        Width="" Height="" HideNodes="|TpocRowID|ClaimantRowID|TpocEditFlag|DTTM_RCD_LAST_UPD|AddedByUser|UpdatedByUser|DttmRcdAdded|"
                        ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="250" Type="GridAndButtons"
                        OnClick="KeepRowForEdit('TPOCGrid');CheckForFileDate();" ImgNewToolTip="<%$ Resources:ttNew %>" ImgEditToolTip="<%$ Resources:ttEdit %>" ImgDeleteToolTip="<%$ Resources:ttDelete %>"  /> <!-- rkulavil - ML Changes - MITS 34107 !-->
                </td>
            </tr>
        </table>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABDiagnosis"
        id="FORMTABDiagnosis">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
            <tr>
                <td align="left" colspan="2">
                    <b>
                        <asp:label id="lblCDCDiagCodes" runat="server" text="<%$ Resources:lblCDCDiagCodes %>"></asp:label></b>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_CSCDiagnosisList" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_CSCDiagnosisList" text="<%$ Resources:lblCSCDiagnosisList %>" />
                        <%--<span class="formw"><uc:MultiCode runat="server" width="45%" ID="CSCDiagnosisList" CodeTable="CDC_DIAGNOSIS_CODE" ControlName="CSCDiagnosisList"
                                RMXRef="/Instance/Document/ClaimantMMSEA/CSCDiagnosisList/DiagnosisList" ItemsetRef="/Instance/Document/ClaimantMMSEA/CSCDiagnosisList/DiagnosisList/Item"  RMXType="codelist" TabIndex="9" />
                        </span>--%>
                        <%--gagnihotri MITS 19359 01/13/2010--%>
                        <span class="formw">
                          <asp:Listbox runat="server" size="3" id="CSCDiagnosisList" RMXRef="/Instance/Document/ClaimantMMSEA/CSCDiagnosisList/DiagnosisList" RMXType="codedeslist" Tabindex="22" />
                          <asp:button runat="server" class="CodeLookupControl" onclientclick="javascript: return callDiagSearch('CSCDiagnosisList')" value="..." tabindex="23" />
                          <asp:button runat="server" class="BtnRemove" id="diagnosislist1btndel" tabindex="24" onclientclick="return deleteSelCode('CSCDiagnosisList')" Text="-" />
                          <asp:Textbox runat="server" style="display:none" RMXRef="/Instance/Document/ClaimantMMSEA/CSCDiagnosisList/DiagnosisList/@codeid" id="CSCDiagnosisList_lst" />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <div runat="server" class="completerow" id="div_CSCICD10DiagnosisList" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_CSCICD10DiagnosisList" text="<%$ Resources:lblCSCICD10DiagnosisList  %>" />
                        <span class="formw">
                          <asp:Listbox runat="server" size="3" id="CSCICD10DiagnosisList" RMXRef="/Instance/Document/ClaimantMMSEA/CSCICD10DiagnosisList/DiagnosisList" RMXType="codedeslist" Tabindex="25" />
                          <asp:button runat="server" class="CodeLookupControl" onclientclick="javascript: return callDiagSearch('CSCICD10DiagnosisList')" value="..." tabindex="26" />
                          <asp:button runat="server" class="BtnRemove" id="diagnosislisticd10btndel" tabindex="27" onclientclick="return deleteSelCode('CSCICD10DiagnosisList')" Text="-" />
                          <asp:Textbox runat="server" style="display:none" RMXRef="/Instance/Document/ClaimantMMSEA/CSCICD10DiagnosisList/DiagnosisList/@codeid" id="CSCICD10DiagnosisList_lst" />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;
                </td>
            </tr>
            <%--kkaur8 MITS 24985 02/22/2013--%>
            <tr>
                <td align="left" colspan="2">
                    <b>
                        <asp:label id="lblClaimantRep" runat="server" text="<%$ Resources:lblClaimantRep %>"></asp:label></b>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_Claimantsrepresentativetype" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_Claimantsrepresentativetype"
                            text="<%$ Resources:lblClaimantsrepresentativetype %>" />
                        <span class="formw">
                            <uc:CodeLookUp runat="server" width="45%" ID="ClaimantsRepresentativeCode" CodeTable="MMSEA_CLPYREP_CODE"
                                ControlName="ClaimantsRepresentativeCode" RMXRef="/Instance/Document/ClaimantMMSEA/ClaimantsRepresentativeCode"
                                OnChange="setMMSEA();" RMXType="code" TabIndex="28" />
                        </span>
                    </div>
                    <div runat="server" class="completerow" id="div_ClaimantsrepresentativeName" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_ClaimantsrepresentativeName"
                            text="<%$ Resources:lblClaimantsrepresentativeName %>" />
                        <span class="formw">
                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" tabindex="29"
                                onblur="lookupLostFocus(this);" rmxref="/Instance/Document/ClaimantMMSEA/ClaimantRepresentativeName"
                                id="clmtreplastfirstname" enabled="false" />
                            <input type="button" class="EllipsisControl" tabindex="30" id="clmtreplastfirstnamebtn"
                                value="..." onblur="setValue('clmtrep');" runat="server" disabled="disabled" />
                            <asp:textbox runat="server" style="display: none" id="clmtrepentityid" />
                            <asp:textbox runat="server" style="display: none" id="clmtreplastfirstname_cid" rmxref="/Instance/Document/ClaimantMMSEA/ClaimantRepresentativeName/@codeid" />
                        </span>
                    </div>
                </td>
            </tr>
            <%--Kiran End--%>
            <tr>
                <td>
                    <dg:UserControlDataGrid runat="server" ID="MMSEAPartyGrid" GridName="MMSEAPartyGrid"
                        GridTitle="<%$ Resources:HdrMMSEAParties %>" Target="Document/ClaimantMMSEA/MMSEAParty"
                        Ref="Document/ClaimantMMSEA/MMSEAParty" Unique_Id="MMSEAClaimantRowID" ShowRadioButton="true"
                        Width="" Height="" HideNodes="|MMSEAClaimantRowID|ClaimantRowID|DttmRcdAdded|DttmRcdLastUpd|AddedByUser|UpdatedByUser|"
                        ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="250" Type="GridAndButtons"
                        OnClick="KeepRowForEdit('MMSEAPartyGrid');" ImgNewToolTip="<%$ Resources:ttNew %>" ImgEditToolTip="<%$ Resources:ttEdit %>" ImgDeleteToolTip="<%$ Resources:ttDelete %>" />
                </td>
            </tr>
        </table>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABProduct"
        id="FORMTABProduct">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
            <tr>
                <td align="left" colspan="2">
                    <b><asp:label id="lblProdLiabCode" runat="server" text="<%$ Resources:lblProdLiabCode %>"></asp:label></b>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_ProductLiabilityCode" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_ProductLiabilityCode"
                            text="<%$ Resources:lblProductLiabilityCode %>" /><span class="formw"><uc:CodeLookUp runat="server"
                                width="45%" ID="ProductLiabilityCode" CodeTable="MMSEA_PRO_LIA_CODE" ControlName="ProductLiabilityCode"
                                RMXRef="/Instance/Document/ClaimantMMSEA/ProductLiabilityCode" OnChange="setMMSEA();"
                                RMXType="code" TabIndex="31" />
                            </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_ProductGenericName" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_ProductGenericName" text="<%$ Resources:lblProductGenericName %>" /><span
                            class="formw"><asp:textbox runat="server" width="45%" id="ProductGenericName" rmxref="/Instance/Document/ClaimantMMSEA/ProductGenericName"
                                rmxtype="text" tabindex="32" onchange=";setDataChanged(true);setMMSEA();" maxlength="40" /></span></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_ProductBrandName" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_ProductBrandName" text="<%$ Resources:lblProductBrandName %>" /><span
                            class="formw"><asp:textbox runat="server" width="45%" id="ProductBrandName" rmxref="/Instance/Document/ClaimantMMSEA/ProductBrandName"
                                rmxtype="text" tabindex="33" onchange=";setDataChanged(true);setMMSEA();" maxlength="40" /></span></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_ProductManufacturer" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_ProductManufacturer"
                            text="<%$ Resources:lblProductManufacturer %>" /><span class="formw"><asp:textbox runat="server" width="45%"
                                maxlength="40" id="ProductManufacturer" rmxref="/Instance/Document/ClaimantMMSEA/ProductManufacturer"
                                rmxtype="text" tabindex="34" onchange=";setDataChanged(true);setMMSEA();" /></span></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_ProductHarmText" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_ProductHarmText" text="<%$ Resources:lblProductHarmText %>" /><span
                            class="formw"><asp:textbox runat="Server" width="45%" id="ProductHarmText" rmxref="/Instance/Document/ClaimantMMSEA/ProductHarmText"
                                rmxtype="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);setMMSEA();"
                                tabindex="35" textmode="MultiLine" columns="30" rows="3" maxlength="200" /><asp:button
                                    runat="server" class="MemoButton" name="ProductHarmTextbtnMemo" id="ProductHarmTextbtnMemo"
                                    onclientclick="return EditMemo('ProductHarmText','');" /></span></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <!--mits 30525 -->
            <% if (bIsCarrier.Text == "0")
               { %>
            <tr>
                <%--Added by Amitosh for Mits 24380 (03/16/2011)--%>
                <td align="left" colspan="2">
                    <b><asp:label id="lblMMSEANoFault1" runat="server" text="<%$ Resources:lblMMSEANoFault %>"></asp:label></b>
                    <hr />
                </td>
            </tr>
            <td>
                <div runat="server" class="completerow" id="div_LabelNoFault" xmlns="">
                    <asp:label runat="server" class="label" width="55%" id="lbl_NoFaultIndicator" text="<%$ Resources:lblNoFaultIndicatorfirst %>" /><span
                        class="formw"><uc:CodeLookUp runat="server" width="45%" ID="NoFaultIndicator" CodeTable="YES_NO"
                            ControlName="NoFaultIndicator" RMXRef="/Instance/Document/ClaimantMMSEA/NoFaultIndicator"
                            OnChange="setMMSEA();" RMXType="code" TabIndex="36" />
                    </span>
                </div>
            </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_NoFaultInsuranceLimit" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_NoFaultInsuranceLimit"
                            text="<%$ Resources:lblNoFaultInsuranceLimitfirst %>" /><span class="formw">
                                <mc:CurrencyTextbox runat="server" Width="45%" ID="NoFaultInsuranceLimit" value="0"
                                    rmxref="/Instance/Document/ClaimantMMSEA/NoFaultInsuranceLimit" rmxtype="currency"
                                    TabIndex="37" onChange="setDataChanged(true);" MaxLength="40" />
                            </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_NoFaultExhaustDate" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_NoFaultExhaustDate" text="<%$ Resources:lblNoFaultExhaustDatefirst %>" /><span
                            class="formw"><asp:textbox runat="server" width="45%" formatas="date" id="NoFaultExhaustDate"
                                rmxref="/Instance/Document/ClaimantMMSEA/NoFaultExhaustDate" rmxtype="date" tabindex="38"
                                onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                                        <script type="text/javascript">
                                            $(function () {
                                                $("#NoFaultExhaustDate").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "../../Images/calendar.gif",
                                                    //buttonImageOnly: true,
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    changeYear: true
                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "39");
                                            });
                                    </script>
                                        </span></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <%} %>
            <!--mits 30525 ends-->
            <%--end Amitosh--%>
            <tr>
                <td align="left" colspan="2">
                    <b><asp:label id="lblMMSEAReturnedApplied" runat="server" text="<%$ Resources:lblMMSEAReturnedApplied %>"></asp:label></b>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_MSPEffectiveDate" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_MSPEffectiveDate" text="<%$ Resources:lblMSPEffectiveDate %>" /><span
                            class="formw"><asp:textbox runat="server" width="45%" formatas="date" id="MSPEffectiveDate"
                                rmxref="/Instance/Document/ClaimantMMSEA/MSPEffectiveDate" rmxtype="date" tabindex="40"
                                onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                                        <script type="text/javascript">
                                            $(function () {
                                                $("#MSPEffectiveDate").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "../../Images/calendar.gif",
                                                    //buttonImageOnly: true,
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    changeYear: true
                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "41");
                                            });
                                    </script>
                                        </span></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_MSPTerminationDate" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_MSPTerminationDate" text="<%$ Resources:lblMSPTerminationDate %>" /><span
                            class="formw"><asp:textbox runat="server" width="45%" formatas="date" id="MSPTerminationDate"
                                rmxref="/Instance/Document/ClaimantMMSEA/MSPTerminationDate" rmxtype="date" tabindex="42"
                                onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                                        <script type="text/javascript">
                                            $(function () {
                                                $("#MSPTerminationDate").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "../../Images/calendar.gif",
                                                    //buttonImageOnly: true,
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    changeYear: true
                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "43");
                                            });
                                    </script>
                                        </span></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_MSPTypeIndicator" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_MSPTypeIndicator" text="<%$ Resources:lblMSPTypeIndicator %>" /><span
                            class="formw"><uc:CodeLookUp runat="server" width="45%" ID="MSPTypeIndicator" CodeTable=""
                                ControlName="MSPTypeIndicator" RMXRef="/Instance/Document/ClaimantMMSEA/MSPTypeIndicator"
                                RMXType="code" TabIndex="44" />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_DispositionCode" xmlns="">
                        <asp:label runat="server" class="label" width="55%" id="lbl_DispositionCode" text="<%$ Resources:lblDispositionCode %>" /><span><!--Removed class="formw" for Mits 20074-->
                            <uc:CodeLookUp runat="server" width="45%" ID="DispositionCode" CodeTable="" ControlName="DispositionCode"
                                RMXRef="/Instance/Document/ClaimantMMSEA/DispositionCode" RMXType="code" TabIndex="45" />
                        </span>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    <asp:textbox style="display: none" runat="server" id="TPOCSelectedId" rmxtype="id" />
    <asp:textbox style="display: none" runat="server" id="TPOCGrid_RowDeletedFlag" rmxtype="id"
        text="false" />
    <asp:textbox style="display: none" runat="server" id="TPOCGrid_Action" rmxtype="id" />
    <asp:textbox style="display: none" runat="server" id="TPOCGrid_RowAddedFlag" rmxtype="id"
        text="false" />
    <%-- akaushik5 Added for MITS 24985 Starts --%>
    <asp:textbox style="display: none" runat="server" id="Representative_AddedEditedFlag" rmxtype="id"
        text="false" />
    <%-- akaushik5 Added for MITS 24985 Ends --%>
    <asp:textbox style="display: none" runat="server" id="Sender" rmxtype="id" />
    <asp:textbox style="display: none" runat="server" id="Representative" rmxtype="id" />
    <asp:textbox style="display: none" runat="server" id="Representative_cid" rmxtype="id" />
    <asp:textbox style="display: none" runat="server" id="MMSEAPartySelectedId" rmxtype="id" />
    <asp:textbox style="display: none" runat="server" id="MMSEAPartyGrid_RowDeletedFlag"
        rmxtype="id" text="false" />
    <asp:textbox style="display: none" runat="server" id="MMSEAPartyGrid_Action" rmxtype="id" />
    <asp:textbox style="display: none" runat="server" id="MMSEAPartyGrid_RowAddedFlag"
        rmxtype="id" text="false" />
    <asp:textbox style="display: none" runat="server" id="PartyNameData" rmxtype="id" />
    <asp:textbox id="bIsCarrier" runat="server" style="display: none" rmxref="/Instance/Document/ClaimantMMSEA/IsCarrierClaimOn">
    </asp:textbox>
    <asp:textbox runat="server" style="display: none" id="hdnTPOCDeletedValues" rmxref="/Instance/Document/ClaimantMMSEA/TPOCDeletedValues">
    </asp:textbox>
    <asp:textbox runat="server" style="display: none" id="hdnPartyDeletedValues" rmxref="/Instance/Document/ClaimantMMSEA/PartyDeletedValues">
    </asp:textbox>
    <asp:textbox runat="server" style="display: none" id="SelectedId" rmxref="/Instance/Document/ClaimantMMSEA/SelectedId">
    </asp:textbox>
    <!--<asp:TextBox runat="server" style="display:none" id="IsNew" RMXRef="/Instance/Document/ClaimantMMSEA/IsNew"></asp:TextBox>-->
    <asp:textbox runat="server" style="display: none" id="SysPageDataChanged">
    </asp:textbox>
    <asp:textbox style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
        rmxtype="hidden" text="MMSEAData" />
    <asp:textbox style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" name="SysFocusFields" id="SysFocusFields"
        value="IsBenificaryCode_codelookup|CSCDiagnosisList_multicodebtn|ProductLiabilityCode_codelookup|" />
    <asp:textbox style="display: none" runat="server" name="txtHdnList" id="txtHdnList">
    </asp:textbox>
    <asp:textbox style="display: none" runat="server" name="txtHdnListICD10" id="txtHdnListICD10" ></asp:textbox>
    <asp:textbox style="display: none" runat="server" id="hdnTPOCGrid_Delete">
    </asp:textbox>
    <asp:textbox style="display: none" runat="server" id="hdnProductHarmText">
    </asp:textbox>
    <asp:textbox style="display: none" runat="server" id="setHidden">
    </asp:textbox>
    </form>
</body>
</html>
