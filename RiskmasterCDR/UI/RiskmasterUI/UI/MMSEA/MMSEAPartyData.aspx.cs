﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.FDM;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.MMSEA
{
    public partial class MMSEAPartyData : GridPopupBase
    {
        private string PageID = RMXResourceManager.RMXResourceProvider.PageId("MMSEAPartyData.aspx");//MITS 34109 - rkulavil

        protected void Page_Load(object sender, EventArgs e)
        {
            string sRelationToBeneficiary = string.Empty;
            string sTypeOfRepresentative = string.Empty;
            string sType = string.Empty;
            bool bIndividual = false;
            string sRepresentative = string.Empty;
            string sMode = string.Empty;

            //MITS 34109 - rkulavil
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, PageID, "MMSEAPartyDataValidations",
               ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "MMSEAPartyDataValidations", sValidationResources, true);
            //MITS 34109 - rkulavil
            if (!Page.IsPostBack)
            {
                PartyNameData.Text = AppHelper.GetQueryStringValue("PartyNameData");//PartyNames Existing in Grid.
                Sender.Text = AppHelper.GetQueryStringValue("mode");
                GridPopupPageload(); //For Empty Row at the time of load.
            }
            else //Changed for Mits 17762 and Mits 17760
            {
                sMode = Sender.Text;
                switch (sMode.ToLower())
                {
                    case "edit":
                        GridPopupPageload();
                        PartyName.Text = att1lastfirstname.Text; //Keeping PartyName in case of Edit.

                        //Added for Mits 17762:RelationToBeneficary is required before filling EntityName-Start
                        sRelationToBeneficiary = ((TextBox)RelationToBenificiary.FindControl("codelookup")).Text;
                        if (sRelationToBeneficiary == "")
                        {
                            att1lastfirstname.Enabled = false;
                            att1lastfirstnamebtn.Disabled = true;

                        }
                        else
                        {
                            att1lastfirstname.Enabled = true;
                            att1lastfirstnamebtn.Disabled = false;
                            if (sRelationToBeneficiary != "")
                                sType = sRelationToBeneficiary.Substring(0, 1);
                            switch (sType.ToUpper())
                            {
                                case "E":
                                case "F":
                                case "O":
                                    bIndividual = true;
                                    att1lastfirstnamebtn.Attributes.Add("onclick", "lookupData('att1lastfirstname','MMSEA_CLMPRTY_TYPE','-1','att1','1')");
                                    break;
                                case "X":
                                case "Y":
                                case "Z":
                                    bIndividual = false;
                                    att1lastfirstnamebtn.Attributes.Add("onclick", "lookupData('att1lastfirstname','MMSEA_PRTYENT_TYPE','-1','att1','1')");
                                    break;
                            }
                            ViewState["Individual"] = bIndividual;
                        }
                        //Added for Mits 17762:RelationToBeneficary is required before filling EntityName-End

                        //Added for Mits 17760:MMSEA Parties to the Claim-Start
                        sTypeOfRepresentative = ((TextBox)TypeOfRepresentative.FindControl("codelookup")).Text;
                        if (sTypeOfRepresentative == "")
                        {
                            attlastfirstname.Enabled = false;
                            attlastfirstnamebtn.Disabled = true;

                        }
                        else
                        {
                            attlastfirstname.Enabled = true;
                            attlastfirstnamebtn.Disabled = false;
                            if (sTypeOfRepresentative != "")
                                sType = sTypeOfRepresentative.Substring(0, 2);
                            switch (sType.ToUpper().Trim())
                            {
                                case "A":
                                    attlastfirstnamebtn.Attributes.Add("onclick", "lookupData('attlastfirstname','ATTORNEYS','-1','att','1')");
                                    sRepresentative = "A";
                                    break;
                                case "G":
                                    attlastfirstnamebtn.Attributes.Add("onclick", "lookupData('attlastfirstname','GUARDIAN_TYPE','-1','att','1')");
                                    sRepresentative = "G";
                                    break;
                                case "P":
                                    attlastfirstnamebtn.Attributes.Add("onclick", "lookupData('attlastfirstname','POWOFATTORNEY_TYPE','-1','att','1')");
                                    sRepresentative = "P";
                                    break;
                                case "O":
                                    attlastfirstnamebtn.Attributes.Add("onclick", "lookupData('attlastfirstname','OTHER_PEOPLE','-1','att','1')");
                                    sRepresentative = "O";
                                    break;
                                case "NA":
                                    attlastfirstname.Enabled = false;
                                    attlastfirstnamebtn.Disabled = true;
                                    sRepresentative = "NA";
                                    break;
                                case "F":
                                    attlastfirstnamebtn.Attributes.Add("onclick", "lookupData('attlastfirstname','ATTORNEY_FIRMS','-1','att','1')");
                                    sRepresentative = "F";
                                    break;//Deb : MITS 25637
                            }
                            ViewState["Representative"] = sRepresentative;
                        }
                        Sender.Text = "";
                        break;
                    //Added for Mits 17760:MMSEA Parties to the Claim-End

                    //Added for Mits 17762:RelationToBeneficary is required before filling EntityName
                    case "beneficiary":
                        att1lastfirstname.Enabled = true;
                        att1lastfirstnamebtn.Disabled = false;
                        sRelationToBeneficiary = ((TextBox)RelationToBenificiary.FindControl("codelookup")).Text;
                        if (sRelationToBeneficiary != "")
                            sType = sRelationToBeneficiary.Substring(0, 1);
                        switch (sType.ToUpper())
                        {
                            case "E":
                            case "F":
                            case "O":
                                bIndividual = true;
                                att1lastfirstnamebtn.Attributes.Add("onclick", "lookupData('att1lastfirstname','MMSEA_CLMPRTY_TYPE','-1','att1','1')");
                                break;
                            case "X":
                            case "Y":
                            case "Z":
                                bIndividual = false;
                                att1lastfirstnamebtn.Attributes.Add("onclick", "lookupData('att1lastfirstname','MMSEA_PRTYENT_TYPE','-1','att1','1')");
                                break;
                        }
                        if (ViewState["Individual"] != null)
                        {
                            if (Convert.ToBoolean(ViewState["Individual"]) != bIndividual)
                            {
                                att1lastfirstname.Text = "";
                            }
                            else
                            {
                                if (att1lastfirstname.Text == "" && att1lastfirstname_cid.Text=="0")
                                {
                                    att1lastfirstname.Text = Beneficiary.Text;
                                    att1lastfirstname_cid.Text = Beneficiary_cid.Text;
                                }
                            }
                        }
                        ViewState["Individual"] = bIndividual;  
                        //for case when Representative is NA  but ... is getting enabled on postback of Beneficiary.
                        sTypeOfRepresentative = ((TextBox)TypeOfRepresentative.FindControl("codelookup")).Text;
                        if (sTypeOfRepresentative != "")
                            sType = sTypeOfRepresentative.Substring(0, 2);
                        if (sType.ToUpper().Trim()=="NA")
                        {
                            
                                    attlastfirstname.Enabled = false;
                                    attlastfirstnamebtn.Disabled = true;
                                    ViewState["Representative"] = "NA";
                        }
                        //for case when Representative is NA but ... is getting enabled on postback of Beneficiary.
                        Sender.Text = "";
                        break;
                    //Added for Mits 17762:RelationToBeneficary is required before filling EntityName

                    //Added for Mits 17760:MMSEA Parties to the Claim
                    case "representative":
                        attlastfirstname.Enabled = true;
                        attlastfirstnamebtn.Disabled = false;
                        sTypeOfRepresentative = ((TextBox)TypeOfRepresentative.FindControl("codelookup")).Text;
                        if (sTypeOfRepresentative != "")
                            sType = sTypeOfRepresentative.Substring(0, 1);
                        switch (sType.ToUpper())
                        {
                            case "A":
                                attlastfirstnamebtn.Attributes.Add("onclick", "lookupData('attlastfirstname','ATTORNEYS','-1','att','1')");
                                sRepresentative = "A";
                                break;
                            case "G":
                                attlastfirstnamebtn.Attributes.Add("onclick", "lookupData('attlastfirstname','GUARDIAN_TYPE','-1','att','1')");
                                sRepresentative = "G";
                                break;
                            case "P":
                                attlastfirstnamebtn.Attributes.Add("onclick", "lookupData('attlastfirstname','POWOFATTORNEY_TYPE','-1','att','1')");
                                sRepresentative = "P";
                                break;
                            case "O":
                                attlastfirstnamebtn.Attributes.Add("onclick", "lookupData('attlastfirstname','OTHER_PEOPLE','-1','att','1')");
                                sRepresentative = "O";
                                break;
                            case "F":
                                attlastfirstnamebtn.Attributes.Add("onclick", "lookupData('attlastfirstname','ATTORNEY_FIRMS','-1','att','1')");
                                sRepresentative = "F";
                                break;//Deb : MITS 25637
                        }
                        if (ViewState["Representative"] != null)
                        {
                            if (ViewState["Representative"].ToString() != sRepresentative)
                            {
                                attlastfirstname.Text = "";
                            }
                            else
                            {
                                if (attlastfirstname.Text == "" && attlastfirstname_cid.Text == "0")
                                {
                                    attlastfirstname.Text = Representative.Text;
                                    attlastfirstname_cid.Text = Representative_cid.Text;
                                }
                            }
                        }
                        ViewState["Representative"] = sRepresentative;
                        //for case when Beneficiary is NA  but ... is getting enabled on postback of Representative.
                        sRelationToBeneficiary = ((TextBox)RelationToBenificiary.FindControl("codelookup")).Text;
                        if (sRelationToBeneficiary != "")
                            sType = sRelationToBeneficiary.Substring(0, 2);
                        if (sType.ToUpper().Trim() == "NA")
                        {

                            att1lastfirstname.Enabled = false;
                            att1lastfirstnamebtn.Disabled = true;
                            ViewState["Individual"] = false; 
                        }
                        //for case when Beneficiary is NA  but ... is getting enabled on postback of Representative.
                        Sender.Text = "";
                        break;
                }
                //Added for Mits 17760:MMSEA Parties to the Claim
            }
            
        }
    }
}
