﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TPOCData.aspx.cs" Inherits="Riskmaster.UI.UI.MMSEA.TPOCData" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="CodeLookUp" Src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>TPOC Data</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/utilities.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>

     <!--MITS 34108 : Rakhel ML-->
    <!--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script> !-->
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <!--MITS 34108 : Rakhel ML-->

</head>
<body onload="CopyGridRowDataToPopup();">
    <form id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="<%$ Resources:lblFormTitle %>" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <asp:TextBox style="display:none" runat="server" id="TpocRowID" RMXRef="//TpocRowID" RMXType="id" rmxignoreset="true"/>
        <asp:TextBox style="display:none" runat="server" id="ClaimantRowID" RMXRef="//ClaimantRowID" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="FilMdcreRcdDATE" RMXRef="//FilMdcreRcdDATE" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="DTTM_RCD_LAST_UPD" RMXRef="//DTTM_RCD_LAST_UPD" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="TpocEditFlag" RMXRef="//TpocEditFlag" RMXType="id" Text="-1" rmxignoreset="true" />
        <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="UniqueId" RMXRef="" RMXType="id" Text="TpocRowID" />
        <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDelayedBeyongTPOCStartDate" Text="<%$ Resources:lblDelayedBeyongTPOCStartDate %>"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>   
                  <asp:TextBox runat="server" FormatAs="date" ID="DelayedBeyongTPOCStartDate" RMXRef="//DelayedBeyongTPOCStartDate"
                TabIndex="1" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <!--<asp:Button class="DateLookupControl" runat="server" ID="btnDelayedBeyongTPOCStartDate" TabIndex="2" />

            <script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "DelayedBeyongTPOCStartDate",
					        ifFormat: "%m/%d/%Y",
					        button: "btnDelayedBeyongTPOCStartDate"
					    }
					    );
            </script> !-->
            <script type="text/javascript">
                $(function () {
                    $("#DelayedBeyongTPOCStartDate").datepicker({
                        showOn: "button",
                        buttonImage: "../../Images/calendar.gif",
                        //buttonImageOnly: true,
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        changeYear: true
                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");
                });
            </script>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:label runat="server" id="lblPaymentObligationAmount" Text="<%$ Resources:lblPaymentObligationAmount %>" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" id="PaymentObligationAmount" RMXRef="//PaymentObligationAmount" RMXType="currency" onblur="currencyLostFocus(this);" rmxforms:as="currency" onchange="setDataChanged(true);" tabindex="3" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDateofWrittenAgreement" Text="<%$ Resources:lblDateofWrittenAgreement %>"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>   
                  <asp:TextBox runat="server" FormatAs="date" ID="DateofWrittenAgreement" RMXRef="//DateofWrittenAgreement"
                TabIndex="4" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <!--<asp:Button class="DateLookupControl" runat="server" ID="btnDateofWrittenAgreement" TabIndex="5" />

            <script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "DateofWrittenAgreement",
					        ifFormat: "%m/%d/%Y",
					        button: "btnDateofWrittenAgreement"
					    }
					    );
            </script> !-->
            <script type="text/javascript">
                $(function () {
                    $("#DateofWrittenAgreement").datepicker({
                        showOn: "button",
                        buttonImage: "../../Images/calendar.gif",
                        //buttonImageOnly: true,
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        changeYear: true
                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "5");
                });
            </script>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDateofCourtApproval" Text="<%$ Resources:lblDateofCourtApproval %>"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>   
                  <asp:TextBox runat="server" FormatAs="date" ID="DateofCourtApproval" RMXRef="//DateofCourtApproval"
                TabIndex="6" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <!--<asp:Button class="DateLookupControl" runat="server" ID="btnDateofCourtApproval" TabIndex="7" />

            <script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "DateofCourtApproval",
					        ifFormat: "%m/%d/%Y",
					        button: "btnDateofCourtApproval"
					    }
					    );
            </script> !-->
            <script type="text/javascript">
                $(function () {
                    $("#DateofCourtApproval").datepicker({
                        showOn: "button",
                        buttonImage: "../../Images/calendar.gif",
                        //buttonImageOnly: true,
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        changeYear: true
                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "7");
                });
            </script>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDateofPaymentIssue" Text="<%$ Resources:lblDateofPaymentIssue %>"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>   
                  <asp:TextBox runat="server" FormatAs="date" ID="DateofPaymentIssue" RMXRef="//DateofPaymentIssue"
                TabIndex="8" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
           <!-- <asp:Button class="DateLookupControl" runat="server" ID="btnDateofPaymentIssue" TabIndex="9" />

            <script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "DateofPaymentIssue",
					        ifFormat: "%m/%d/%Y",
					        button: "btnDateofPaymentIssue"
					    }
					    );
            </script> !-->
             <script type="text/javascript">
                 $(function () {
                     $("#DateofPaymentIssue").datepicker({
                         showOn: "button",
                         buttonImage: "../../Images/calendar.gif",
                         //buttonImageOnly: true,
                         showOtherMonths: true,
                         selectOtherMonths: true,
                         changeYear: true
                     }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "9");
                 });
            </script>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDeletedFlag" Text="<%$ Resources:lblDeletedFlag %>"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>   
                  <asp:checkbox runat="server"  ID="DeletedFlag" RMXRef="//DeletedFlag" DeletedFlagYesNo="Yes" />
                </td>
            </tr>
            
        </tbody>
    </table>
    <div>
        <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" />
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnOk">
          <asp:button class="button" runat="server" id="btnOk" Text="<%$ Resources:btnOk %>" width="75px" onClientClick="return TPOCData_onOk();" onclick="btnOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
          <asp:button class="button" runat="server" id="btnCancel"  Text="<%$ Resources:btnCancel %>" width="75px" onClientClick="return TPOCData_onCancel();" onclick="btnCancel_Click" />
        </div>
      </div>
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="<%$Resources:PleaseWaitDialog1 %>" />
    </form>
  </body>
</html>