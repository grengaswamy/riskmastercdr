﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CMMSEAXDATA.aspx.cs" Inherits="Riskmaster.UI.UI.MMSEA.CMMSEAXDATA" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="CodeLookUp" Src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MMSEA Data Details</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/utilities.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>

</head>
<body onload="CopyGridRowDataToPopup();">
    <form id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="<%$ Resources:lblFormTitle %>" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <asp:TextBox style="display:none" runat="server" id="EntMMSEARowID" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/EntMMSEARowID" RMXType="id" rmxignoreset="true"/>
        <asp:TextBox style="display:none" runat="server" id="EntityEID" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/EntityEID" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="DttmRcdAdded" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/DttmRcdAdded" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="DttmRcdLastUpd" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/DttmRcdLastUpd" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="UpdatedByUser" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/UpdatedByUser" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="AddedByUser" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/AddedByUser" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="UniqueId" RMXRef="" RMXType="id" Text="EntMMSEARowID" />
        <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblLob" Text="<%$ Resources:lblLob %>"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>   
                  <asp:DropDownList runat="server" id="LobCode" tabindex="1" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/LobCode" RMXType="combobox" onchange="DisableSecondLOBCode();setDataChanged(true);">
                  <asp:ListItem Value="0" Text="" />
                  <asp:ListItem Value="241" Text="<%$ Resources:liGC %>" />
                  <asp:ListItem Value="242" Text="<%$ Resources:liVA %>" />
                  <asp:ListItem Value="243" Text="<%$ Resources:liWC %>" />
                  <asp:ListItem Value="-3" Text="<%$ Resources:liAll %>" />
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
            <td>
                    <asp:Label runat="server"  ID="lblSecondaryLob" Text="<%$ Resources:lblSecondaryLob %>"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
             <td>   
                  <asp:DropDownList runat="server" id="SecondLobCode" tabindex="2" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/SecondLobCode" RMXType="combobox" onchange="setDataChanged(true);">
                  <asp:ListItem Value="0" Text="" />
                  <asp:ListItem Value="241" Text="<%$ Resources:liGC %>" />
                  <asp:ListItem Value="242" Text="<%$ Resources:liVA %>" />
                  <asp:ListItem Value="243" Text="<%$ Resources:liWC %>" />
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:label runat="server" class="required" id="lbl_ReporterId" Text="<%$ Resources:lblReportId %>" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" id="ReporterId" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/ReporterId" RMXType="text" tabindex="3" onchange="setDataChanged(true);" MaxLength="9"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="required" id="lbl_SiteId" Text="<%$ Resources:lblSiteId %>" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" id="SiteId" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/SiteId" RMXType="text" tabindex="4" onchange="setDataChanged(true);" MaxLength="9" />
                </td>
            </tr>
             <tr>
                <td>
                    <asp:label runat="server"  id="lblScheduleCode" Text="<%$ Resources:lblScheduleCode %>" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" id="ScheduleCode" RMXRef="/Instance/Document/MMSEAXDataDetails/MMSEAXData/ScheduleCode" RMXType="code" tabindex="5" ControlName="ScheduleCode" CodeTable="REPORT_SCHED_CODE"/>
                </td>
            </tr>
            
        </tbody>
    </table>
    <div>
        <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" />
        
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnOk">
          <asp:button class="button" runat="server" id="btnOk" RMXRef="" Text="<%$ Resources:btnOk %>" width="75px" onClientClick="return CMMSEAXData_onOk();" onclick="btnOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
          <asp:button class="button" runat="server" id="btnCancel" RMXRef="" Text="<%$ Resources:btnCancel %>" width="75px" onClientClick="return CMMSEAXData_onCancel();" onclick="btnCancel_Click" />
        </div>
      </div>
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>