﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.Models;
using CuteWebUI;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessHelpers;

namespace Riskmaster.UI.UploadFiles
{
    public partial class UploadFiles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UploadDocumentAttachments.RemoveButtonText = "Remove";
            UploadDocumentAttachments.CancelText = "Cancel";
            UploadDocumentAttachments.UploadingMsg = "Uploading...";
            UploadDocumentAttachments.TableHeaderTemplate = UploadDocumentAttachments.TableHeaderTemplate.Substring(0, 29) +"Files to upload"+ "</td>";
            UploadDocumentAttachments.CancelAllMsg = "Cancel all Uploads";
        }
       protected void SaveData_Click(object sender, EventArgs e)
        {
            int iUploadedFiles = 0;
            
            DataStreamingService.StreamedUploadDocumentType oStreamedUploadDocumentType = null;
            DocumentBusinessHelper dc = new DocumentBusinessHelper();
           // RMServiceType returnVal = null;
            try
            {
                lbSuccess.Visible = false;
                if (UploadDocumentAttachments.Items.Count > 0)
                {
                    oStreamedUploadDocumentType = new DataStreamingService.StreamedUploadDocumentType();
                    oStreamedUploadDocumentType.Token = AppHelper.GetSessionId();
               try
                {
                foreach (AttachmentItem item in UploadDocumentAttachments.Items)
                {
                    Stream data = item.OpenStream();

                    oStreamedUploadDocumentType.fileStream = data;// Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                    oStreamedUploadDocumentType.FileName = item.FileName;
                    oStreamedUploadDocumentType.ClientId = AppHelper.ClientId;

                    //AppHelper.GetResponse("RMService/DataStream/addfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oStreamedUploadDocumentType);
                    dc.UploadFile(oStreamedUploadDocumentType);
                    if (!string.IsNullOrEmpty(filename.Value))
                    {
                        filename.Value += ",";
                    }
                    filename.Value += item.FileName;

                    iUploadedFiles++;
                }
                }
                finally
                {
                    oStreamedUploadDocumentType = null;
                }
                if (iUploadedFiles == UploadDocumentAttachments.Items.Count)
                {
                    lbSuccess.Visible = true;
                }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        
       protected void OnAttachmentRemoveClicked_Click(object sender, CuteWebUI.AttachmentItemEventArgs e)
        {
            e.Item.Remove();
        }

  
    }
}