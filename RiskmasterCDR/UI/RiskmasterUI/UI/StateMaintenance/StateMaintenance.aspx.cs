﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.UI.StateMaintenance
{
    public partial class StateMaintenance : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    /*To get the combo lstWorkloss filled*/
                   if(mode.Text.ToLower()=="add")
                   {
                    XmlTemplate = GetMessageTemplate();
                    CallCWS("StateMaintenanceAdaptor.GetSelectedStateInfo", XmlTemplate, out sCWSresponse, true, true);
                   }

                    /*To get the combo lstWorkloss filled*/
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("StateMaintenanceAdaptor.GetSelectedStateInfo", XmlTemplate, out sCWSresponse, true, true);
                        XmlDocument objReturnXml = new XmlDocument();
                        objReturnXml.LoadXml(sCWSresponse);
                        string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                        if (sMsgStatus == "Success")
                        {
                            XElement rootElement = XElement.Parse(sCWSresponse);
                            if (rootElement.XPathSelectElement("//displaycolumn/control[@name='StateAb']") != null)
                            {
                                StateAbLoadvalue.Text = rootElement.XPathSelectElement("//displaycolumn/control[@name='StateAb']").Value;
                            }
                        }
                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {

            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("StateMaintenanceAdaptor.Save", XmlTemplate, out sCWSresponse, true, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplate()
        {
            string sRowId = string.Empty;

            if (mode.Text.ToLower() == "edit")
            {
                sRowId = AppHelper.GetQueryStringValue("selectedid");
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<form name='StatesSetup' title='States'>");
            sXml = sXml.Append("<group name='StatesSetup' title='States'>");
            sXml = sXml.Append("<displaycolumn>");
            sXml = sXml.Append("<control name='RowId' type='id'>");
            sXml = sXml.Append(sRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</displaycolumn>");
            sXml = sXml.Append("</group></form>");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }
    }

}
