﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StatesMaintenance.aspx.cs"
    Inherits="Riskmaster.UI.UI.StateMaintenance.StatesMaintenance"  ValidateRequest="false" %>  <!-- validate request=false :MITS 23657-->

<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove"
xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>States Setup</title>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }
    </script>

</head>
<body onload="parent.MDIScreenLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <div>
            <span>
                <dg:UserControlDataGrid runat="server" ID="StatesGrid" GridName="StatesGrid" GridTitle="States Setup"
                    Target="/Document/PassToWebService/StatesList" Ref="/Instance/Document/form//control[@name='StatesGrid']"
                    Unique_Id="RowId" ShowRadioButton="true" Width=""  HideNodes="|RowId|"
                    ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="350" Type="GridAndButtons"
                    RowDataParam="listrow" OnClick="KeepRowForEdit('StatesGrid');" TextColumn="Abbreviation"/>
            </span>
        </div>
        <asp:TextBox Style="display: none" runat="server" ID="StatesSelectedId" RMXType="id" />
        <asp:TextBox Style="display: none" runat="server" ID="StatesGrid_RowDeletedFlag"
            RMXType="id" Text="false" />
        <asp:TextBox Style="display: none" runat="server" ID="StatesGrid_Action" RMXType="id" />
        <asp:TextBox Style="display: none" runat="server" ID="StatesGrid_RowAddedFlag" RMXType="id"
            Text="false" />
    </div>
    </form>
</body>
</html>
