﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeErrorResponse.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeErrorResponse"%>
<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>





<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
      <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body>
 <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
       <form id="frmData" runat="server">
   
    <p>
						<h3><asp:Label ID="lblHeading" runat="server" Text="<%$ Resources:lblHeadingResrc %>"></asp:Label></h3>
						<asp:Label ID="lblFollowMsg" runat="server" Text="<%$ Resources:lblFollowMsgResrc %>"></asp:Label>
						<br/>
						<asp:Label ID="lblResolveIssues" runat="server" Text="<%$ Resources:lblResolveIssuesResrc %>"></asp:Label>
						<br/><br/>
						<hr/>
						<asp:Label ID="lblCompleteWizard" runat="server" Text="<%$ Resources:lblCompleteWizardResrc %>"></asp:Label>
						<br/><br/>
						<asp:Label ID="lblInitializeScript" runat="server" Text="<%$ Resources:lblInitializeScriptResrc %>"></asp:Label>
						<br/><br/>
						<asp:Label ID="lblEnabled" runat="server" Text="<%$ Resources:lblEnabledResrc %>"></asp:Label>
						<br/><br/>
						<asp:Label ID="lblRefreshPage" runat="server" Text="<%$ Resources:lblRefreshPageResrc %>"></asp:Label>
					</p>
					<hr/>
					<center>
					 <asp:Button ID="Button1" onclick="btnBack_Click"  runat="server" Text="<%$ Resources:btnRetryResrc %>" UseSubmitBehavior="true" CssClass=button />
			                <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnBackResrc %>" CssClass=button onclick="btnCancel_Click"/>
             </center>
         <asp:HiddenField ID="hdnisallowallperms" runat="server" />
     <asp:HiddenField ID="lettername" runat="server" />
        <asp:HiddenField ID="lob" runat="server" />
        <asp:HiddenField ID="letterdesc" runat="server" />
        <asp:HiddenField ID="allstatesselected" runat="server" />
        <asp:HiddenField ID="lettertype" runat="server" />
        <asp:HiddenField ID="txtState" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumentformat" runat="server" />
         <asp:HiddenField ID="txtmergedocumentformat_cid" runat="server" />
           <asp:HiddenField ID="prefab" runat="server" />
    <asp:HiddenField ID="hdnSelectedFields" runat="server" />
    <asp:HiddenField ID="selectedfields" runat="server" />
    <asp:HiddenField ID="fieldcategory" runat="server" />
    <asp:HiddenField ID="txtState_cid" runat="server" />
    <asp:HiddenField ID="hdnSelectedFieldsID" runat="server" />
      <asp:HiddenField ID="hdnselectedpermsusers" runat="server" />
               <asp:HiddenField ID="hdnselectedperms" runat="server" />
           <asp:HiddenField ID="hdnselectedpermsids" runat="server" />
    <asp:HiddenField ID="hdnfilename" runat="server" />
        <asp:HiddenField ID="hdnformfilecontent" runat="server" />
            <asp:HiddenField ID="hdnselectedpermsgroups" runat="server" />
    <asp:HiddenField ID="TemplateId" runat="server" />
    </form>
</body>
</html>
