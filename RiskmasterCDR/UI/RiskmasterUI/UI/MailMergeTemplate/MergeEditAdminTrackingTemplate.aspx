﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeEditAdminTrackingTemplate.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeEditAdminTrackingTemplate"%>

<%@ Import Namespace="System.Xml" %>

<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="javascript" src='../../Scripts/merge.js'></script>
</head>
<body onload="InitPageSettings_MergeTemplate();">
    <form id="frmData" runat="server">
    <div>
    	<p class="formtitle">
						<font size="5">
							<b><asp:Label ID="lblEditMergeLetter" runat="server" Text="<%$ Resources:lblEditMergeLetterResrc %>"></asp:Label></b>
						</font>
					</p>
					<rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
      <table width="100%" border="0">
    <tr>
     <td colspan="3" class="ctrlgroup"><asp:Label ID="lblDataFields" runat="server" Text="<%$ Resources:lblDataFieldsResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td nowrap="1"><asp:Label ID="lblAvailableFields" runat="server" Text="<%$ Resources:lblAvailableFieldsResrc %>"></asp:Label></td>
     <td></td>
     <td><asp:Label ID="lblFieldsToMerge" runat="server" Text="<%$ Resources:lblFieldsToMergeResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td> <asp:DropDownList ID="fieldcategory" runat="server">
         </asp:DropDownList>
   
    <br />
          <asp:ListBox ID="fields_" runat="server" Rows=8></asp:ListBox>
    <%  foreach (XmlElement ele in Model.SelectNodes("//Field"))
        { %>
        <select ondblclick='AddSingle(this.options[this.selectedIndex]);' multiple="true" style='display:none' size="8" id='fields_<%=ele.Attributes["DisplayCat"].Value %>'>
            <%  foreach (XmlElement eleChild in ele.SelectNodes("./Value"))
                {
                    string fieldvalue = "";
                    fieldvalue = ele.Attributes["DisplayCat"].Value + "_" + eleChild.Attributes["FieldId"].Value + "_" + eleChild.Attributes["FieldName"].Value;
                          
                    %>
            <option value='<%=fieldvalue%>' > <%=eleChild.Attributes["FieldName"].Value%></option>
             <%} %>
        </select>
        <%} %>
    </td>
     <td width="1"><input type="button"  value=">>" class="button" onClick="Javascript:return AddSelected();"><input type="button"  value="<<" class="button" onClick="Javascript:return RemoveSelected();"></td>
     <td nowrap="1" valign="top" align="left" width="100%"><br><select runat="server" id="selectedfields" multiple="true" style="width:162px" ondblclick="Javascript:return RemoveSingle(this.options[this.selectedIndex]);" size="10"></select></td>
    </tr>
    <tr class="ctrlgroup">
     <td colspan="3">&nbsp;</td>
    </tr>
     <tr>
     <td colspan="3">    <asp:Button ID="Button1" onclick="btnBack_Click" runat="server" UseSubmitBehavior="true" Text="<%$ Resources:btnBackResrc %>"  CssClass=button OnClientClick="return Validate_MergeCreateTemplate2Back();"/>
         <asp:Button ID="Button2" runat="server" Text="<%$ Resources:btnNextResrc %>"  onclick="btnNext_Click" UseSubmitBehavior="true"  CssClass=button OnClientClick="return Validate_MergeCreateTemplate2();"/>
         <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass=button 
             onclick="btnCancel_Click" /></td>
    </tr>
    <asp:HiddenField ID="hdnisallowallperms" runat="server" />
        <asp:HiddenField ID="lettername" runat="server" />
        <asp:HiddenField ID="lob" runat="server" />
        <asp:HiddenField ID="letterdesc" runat="server" />
        <asp:HiddenField ID="allstatesselected" runat="server" />
        <asp:HiddenField ID="lettertype" runat="server" />
        <asp:HiddenField ID="txtState" runat="server" />
        <asp:HiddenField ID="txtState_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumentformat" runat="server" />
         <asp:HiddenField ID="txtmergedocumentformat_cid" runat="server" />
           <asp:HiddenField ID="prefab" runat="server" />
                 <asp:HiddenField ID="hdnselectedperms" runat="server" />
           <asp:HiddenField ID="hdnselectedpermsids" runat="server" />
         <asp:HiddenField ID="hdnfilename" runat="server" />
         <asp:HiddenField ID="hdnselectedpermsusers" runat="server" />
            <asp:HiddenField ID="hdnselectedpermsgroups" runat="server" />
             <asp:HiddenField ID="hdnSelectedFieldsID" runat="server" />
            <asp:HiddenField ID="hdnSelectedFields" runat="server" />
             <asp:HiddenField ID="TemplateId" runat="server" />
            <asp:HiddenField ID="hdncatid" runat="server" />
              <asp:HiddenField ID="catname" runat="server" />
              <asp:HiddenField ID="hdnformfilecontent" runat="server" />
                 <asp:HiddenField ID="hdnexecutepipeline" runat="server" Value=false />
                  <input type="hidden"  value="" id="hdnaction"/>
   </table>
    </form>
</body>
</html>

