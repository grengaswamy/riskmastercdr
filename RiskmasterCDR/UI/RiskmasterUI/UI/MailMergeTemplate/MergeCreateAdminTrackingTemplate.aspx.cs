﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.MailMerge
{
    public partial class MergeCreateAdminTrackingTemplate : System.Web.UI.Page
    {
        protected XmlDocument Model = new XmlDocument();
        string MergeMessageTemplate1 = "<Message> " +
          "<Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization>" +
           " <Call>" +
              "<Function>MailMergeAdaptor.GetAvailableFieldList</Function>" +
            "</Call>" +
            "<Document>" +
              "<Template><CategoryId>20</CategoryId></Template>" +
            "</Document>" +
          "</Message>";
        string MergeMessageTemplate2 = " <Message><Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization><Call>" +
            "<Function>MailMergeAdaptor.GetAvailableAdminTableList</Function>" +
            "</Call><Document><Template><TemplateId>1</TemplateId></Template></Document>" +
            "</Message>";

        private void SaveValuesFromPreviousScreen()
        {
            if (!Page.IsPostBack)
            {
                lettername.Value = AppHelper.GetFormValue("lettername");
                lob.Value = AppHelper.GetFormValue("lob");
                letterdesc.Value = AppHelper.GetFormValue("letterdesc");
                allstatesselected.Value = AppHelper.GetFormValue("allstatesselected");
                lettertype.Value = AppHelper.GetFormValue("lettertype");
                txtState.Value = AppHelper.GetFormValue("txtState");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
                prefab.Value = AppHelper.GetFormValue("prefab");
                hdnSelectedFieldsID.Value = AppHelper.GetFormValue("hdnSelectedFieldsID");
                hdnSelectedFields.Value = AppHelper.GetFormValue("hdnSelectedFields");
                hdnselectedperms.Value = AppHelper.GetFormValue("hdnselectedperms");
                hdnselectedpermsids.Value = AppHelper.GetFormValue("hdnselectedpermsids");
                hdnfilename.Value = AppHelper.GetFormValue("hdnfilename");
                hdnisallowallperms.Value = AppHelper.GetFormValue("hdnisallowallperms");
                hdnselectedpermsusers.Value = AppHelper.GetFormValue("hdnselectedpermsusers");
                hdnselectedpermsgroups.Value = AppHelper.GetFormValue("hdnselectedpermsgroups");
                fieldcategory.Value = AppHelper.GetFormValue("fieldcategory");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SaveValuesFromPreviousScreen();
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeCreateAdminTrackingTemplate.aspx"), "MergeCreateAdminTrackingTemplateValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeCreateAdminTrackingTemplateValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end

                if (!Page.IsPostBack)
                {
                    MergeMessageTemplate2 = AppHelper.ChangeMessageValue(MergeMessageTemplate2, "//Template/TemplateId", "1");
                    string sReturn = AppHelper.CallCWSService(MergeMessageTemplate2.ToString());

                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    PopulateFieldcategory(Model);

                    sReturn = AppHelper.CallCWSService(MergeMessageTemplate1.ToString());
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }

        private void PopulateFieldcategory(XmlDocument doc)
        {
            ListItem item = new ListItem();
            //item.Text = "Select a Category"; //Amandeep ML Changes
            item.Text = RMXResourceProvider.GetSpecificObject("cmbSelectCategory", RMXResourceProvider.PageId("MergeCreateAdminTrackingTemplate.aspx"), "0");               
            item.Value = "";
            fieldcategory.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//Table"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["CategoryName"].Value;
                item.Value = ele.Attributes["CategoryDesc"].Value;
                if (fieldcategory.Value.Replace(" Merge", "") == item.Text)
                    item.Selected = true;
                fieldcategory.Items.Add(item);
                fieldcategory.Attributes.Add("onchange", "Javascript:return LoadFieldCategory()");
            }
            fieldcategory.Attributes.Add("onchange", "if(VerifyCatChange()){LoadFieldCategory()}");
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeCreateTemplate3.aspx");
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeCreateTemplate1.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplates.aspx");
        }

     
    }
}
