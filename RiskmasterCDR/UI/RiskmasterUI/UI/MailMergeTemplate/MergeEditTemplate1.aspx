﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeEditTemplate1.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeEditTemplate1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>





<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
        <script  type="text/javascript" language="javascript" src='../../Scripts/merge.js'></script>
      <!-- rkapoor29  changes JIRA 6992 start-->

    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.min.js"></script>
         <script type="text/javascript">
             function Sort() {
                 var $r = $("#lstMailRecipient option");
                 $r.sort(function (a, b) {
                     if (a.text < b.text) return -1;
                     if (a.text == b.text) return 0;
                     return 1;
                 });
                 $($r).remove();
                 $("#lstMailRecipient").append($($r));
                 $("#lstMailRecipient").val('');
             }
     </script>

    <!-- rkapoor29 changes JIRA 6992 end -->
    <script type="text/javascript">
        //added by swati for MITS # 36930
        var m_DataChanged = false;
        function DeleteUserGroup() {
            if (document.getElementById("selecteduserfields").selectedIndex < 0) {
                alert("Please select the Group you would like to delete.");
                return false;
            }

            var i;
            var ListBox;
            var bDoIt;

            ListBox = document.getElementById('selecteduserfields');

            // rkapoor29 changes Gap 8 JIRA 6992 start
            // for loop added by rkapoor29 JIRA 7499
            var stext;
            var svalue;
            for (var j = 0; j < ListBox.options.length; j++) {
                if (ListBox.options[j].selected) {
                    stext = ListBox.options[j].text;
                    svalue = ListBox.options[j].value;
                    if (stext.substring(stext.length - 1, stext.length) == "*") {
                        stext = stext.replace(/\*/g, '');
                    }
                    document.getElementById('lstMailRecipient').options.add(new Option(stext, svalue));
                }
            }

            //rkapoor29 changes JIRA 6992 end

            for (i = ListBox.options.length - 1; i >= 0; i--)
                if (ListBox.options[i].selected == true) {
                    bDoIt = true;
                    if (String(ListBox.options[i].value).charAt(0) == "_")
                        bDoIt = window.confirm("The field you wish to remove (" + ListBox.options[i].text + ") may already be in use within the document.  Are you sure you wish to remove it?");
                    if (bDoIt)
                        ListBox.options[i] = null;
                }
            //return true;
            Sort();  //rkapoor29 changes JIRA 6992
            return false;

            //document.getElementById("selecteduserfields").options[document.getElementById("selecteduserfields").selectedIndex] = null;
            //m_DataChanged = true;
        }

        function AddUserGroup() {
            //debugger;
            if (document.getElementById("lstMailRecipient").selectedIndex < 0)
                return false;

            var sIndex = document.getElementById("lstMailRecipient").selectedIndex;   //rkapoor29 changes JIRA 6992  
            var sValue = document.getElementById("lstMailRecipient").options[document.getElementById("lstMailRecipient").selectedIndex].value;
            var sText = document.getElementById("lstMailRecipient").options[document.getElementById("lstMailRecipient").selectedIndex].text;

            // rkapoor29 changes JIRA 6992 
            if (document.getElementById("chkCurrent") != null) {
                if (document.getElementById("chkCurrent").checked == true) {
                    if (sValue != "" && sText != "") {
                        sValue = sValue + "*";
                        sText = sText + "*";
                    }
                }

            }

            for (var f = 0; f < document.getElementById("selecteduserfields").options.length; f++)
                //if (document.getElementById("selecteduserfields").options[f].value == sValue.replace("*",""))
                if (document.getElementById("selecteduserfields").options[f].value == sValue.replace("*", "") || document.getElementById("selecteduserfields").options[f].value == sValue || document.getElementById("selecteduserfields").options[f].value.replace("*", "") == sValue || document.getElementById("selecteduserfields").options[f].value == sText.replace("*", "") || document.getElementById("selecteduserfields").options[f].value == sText || document.getElementById("selecteduserfields").options[f].value.replace("*", "") == sText)
                    return false;

            var opt = new Option(sText, sValue, false, false);
            if (opt.value != "") {
                document.getElementById("selecteduserfields").options[document.getElementById("selecteduserfields").options.length] = opt;
            }
            m_DataChanged = true;

            //rkapoor29 changes JIRA 6992 start
            if (sIndex > 0) {
                document.getElementById("lstMailRecipient").options.remove(sIndex);
            }
            //rkapoor29 changes JIRA 6992 end

            return false;
        }

        function SetControls() {
            //debugger;

            var sText = document.getElementById("lstMailRecipient").options[document.getElementById("lstMailRecipient").selectedIndex].text;

            if (sText == "Claim Adjuster" || sText == "Claimant") {
                document.getElementById("chkCurrent").style.visibility = 'visible';
                document.getElementById('lblCurrentMsg').style.visibility = 'visible';
                if (sText == "Claim Adjuster")
                    document.getElementById("lblCurrentMsg").innerHTML = "Current Adjuster only";
                else if (sText == "Claimant")
                    document.getElementById("lblCurrentMsg").innerHTML = "Primary Claimant only";
            }
            else {
                document.getElementById("chkCurrent").style.visibility = "hidden";
                document.getElementById("lblCurrentMsg").style.visibility = "hidden";
                document.getElementById("chkCurrent").checked = false;
            }
        }
        //change end here by swati
        </script>
</head>
<body onload="setdefaultsforedit();InitPageSettings_MergeTemplate1();">
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
    <table width="100%">
    <tr class="msgheader">
     <td><asp:Label ID="lblMiscAdmin" runat="server" Text="<%$ Resources:lblMiscAdminResrc %>"></asp:Label></td>
    </tr>
    <tr class="ctrlgroup">
     <td><asp:Label ID="lblEditMergeLetter" runat="server" Text="<%$ Resources:lblEditMergeLetterResrc %>"></asp:Label><asp:Label ID="lbltitle" runat="server"></asp:Label></td>
    </tr>
   </table>
   <table width="100%" border="0">
    <tr>
     <td colspan="4" class="colheader3">
      				<asp:Label ID="lblBasicInfo" runat="server" Text="<%$ Resources:lblBasicInfoResrc %>"></asp:Label>
      									
     </td>
    </tr>
    <tr>
     <td nowrap="1" class="required">
      							<asp:Label ID="lblLetterName" runat="server" Text="<%$ Resources:lblLetterNameResrc %>"></asp:Label>			
      									
     </td>
     <td>
     <%--Ashish Ahuja Mits 32710--%>
     <%--<input type="text"  value="" id="lettername" maxlength="40" size="40" runat=server>--%>
     <input type="text"  value="" id="lettername" maxlength="50" size="40" runat=server>
     </td>
     <td nowrap="1">
      				<asp:Label ID="lblLOB" runat="server" Text="<%$ Resources:lblLOBResrc %>"></asp:Label>						
      									
     </td>
     <td>
         <asp:ListBox ID="lob" runat="server" Rows="1"></asp:ListBox>
      </td>
    </tr>
    <tr>
     <td nowrap="1">
      				<asp:Label ID="lblLetterDesc" runat="server" Text="<%$ Resources:lblLetterDescResrc %>"></asp:Label>								
      									
     </td>
     <td><input type="text"  runat=server value="" id="letterdesc" maxlength="40" size="40"></td>
     <td><input type="checkbox" runat=server  id="allstatesselected" onclick="alterstateselection();"><asp:Label ID="lblAllStates" runat="server" Text="<%$ Resources:lblAllStatesResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td nowrap="1">
      					<asp:Label ID="lblLetterDataSource" runat="server" Text="<%$ Resources:lblLetterDataSourceResrc %>"></asp:Label>					
      									
     </td>
     <td>
         <asp:ListBox ID="lettertype" runat="server" Rows="1"></asp:ListBox>
         </td>
     <td>
      										<asp:Label ID="lblPartState" runat="server" Text="<%$ Resources:lblPartStateResrc %>"></asp:Label>	
      									
     </td>
     <td>
     <input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);" name="txtState" id="txtState" cancelledvalue=""></input>
<input type="button" name="txtStatebtn"  class="CodeLookupControl"  id="txtStatebtn" onclick="return CodeSelect('states','txtState')" />

<input type="hidden" name="txtState_cid"  runat="server"  id="txtState_cid"  />
     </td>
   
    </tr>
    <tr>
     <td><asp:Label ID="lblTypeMergeDoc" runat="server" Text="<%$ Resources:lblTypeMergeDocResrc %>"></asp:Label></td>
       <td><input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="txtDisabledmergedocumenttype" cancelledvalue="" disabled><input type="hidden" size="30" runat="server"  onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" name="txtmergedocumenttype" id="txtmergedocumenttype" cancelledvalue="" readonly></input>


<input type="hidden" name="txtmergedocumenttype_cid"  runat="server"  id="txtmergedocumenttype_cid"  />
</td>
    
     <td><asp:Label ID="lblFormatMergeDoc" runat="server" Text="<%$ Resources:lblFormatMergeDocResrc %>"></asp:Label>	</td>
     <td>
     <input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);" name="txtmergedocumentformat" id="txtmergedocumentformat" cancelledvalue=""></input>
<input type="button" name="txtmergedocumentformatbtn"  class="CodeLookupControl"  id="txtmergedocumentformatbtn" onclick="return CodeSelect('MERGE_FORMAT_TYPE','txtmergedocumentformat')" />

<input type="hidden" name="txtmergedocumentformat_cid"  runat="server"  id="txtmergedocumentformat_cid"  />
     </td>
     
    </tr>
    <tr>
     <td nowrap="1">
      			<asp:Label ID="lblDocTemplate" runat="server" Text="<%$ Resources:lblDocTemplateResrc %>"></asp:Label>							
      									
     </td>
     <td>
         <asp:ListBox ID="prefab" runat="server" Rows="1" Enabled=false></asp:ListBox>
       </td>
        <td></td>
        <td></td>
        </tr>
       <tr>
            <td>
                <asp:Label ID="lblCopyAsEmail" runat="server" Text="<%$ Resources:lblCopyAsEmail %>"></asp:Label>							
            </td>            
            <td>
                <input type="checkbox" runat="server" value="true" checked id="EmailCheck"
                    onclick="alterCopyMailselection();">
            </td>
           <td></td>
           <td></td>
        </tr>
       <%--commented by swati and changed to below code--%>
        <%--<tr>
            <td >               
            </td>
            <td>                
            </td>
            <td class="required">
               <asp:Label ID="lblDefaultRecipient" runat="server" Text="<%$ Resources:lblDefaultRecipient %>"></asp:Label>
            </td>            
            <td>
                <asp:ListBox ID="lstMailRecipient" runat="server" Rows="1"></asp:ListBox>
            </td>
    </tr>--%>
       <%--changes made by swati start here MITS # 36930--%>
            <tr>            
            <td class="required">
               <asp:Label ID="lblDefaultRecipient" runat="server" Text="<%$ Resources:lblDefaultRecipient %>"></asp:Label>	
            </td>            
            <td>
                <asp:ListBox ID="lstMailRecipient" runat="server" Rows="1" onChange="SetControls();"  ></asp:ListBox>                
                <asp:Button ID="btnAdd"  UseSubmitBehavior="false" runat="server" Text="Add" class="button" onclientClick="AddUserGroup();return false;"/>  
                <input type="checkbox" runat="server" value="true" id="chkCurrent" style="visibility: hidden;" />      
                <asp:Label ID="lblCurrentMsg" runat="server"  style="visibility: hidden;" ></asp:Label>	        
            </td>
            <td></td>
            <td></td>
    </tr>
        <tr>
            <td></td>
            <td>
                <%--<asp:listbox runat="server" id="lstUserList" Rows="5" multiselect="true" />--%>
                <select id="selecteduserfields" runat="server" multiple="true" style="width:162px" ondblclick="javascript:return RemoveSingle(this.options[this.selectedIndex]);" size="5"></select>
                <asp:ImageButton ID="imageDelete2" runat="server" UseSubmitBehavior ="false" src="../../Images/delete3.gif" width="20" height="20" border="0" alt="Delete" title="Remove User/Group" OnClientClick = "DeleteUserGroup();return false;"  />
            </td>            
            <td></td>
            <td></td>
        </tr>
        <%--changes end here by swati--%>
    <tr class="ctrlgroup">
     <td colspan="8">&nbsp;</td>
    </tr>
    <tr>
     <td colspan="3">
         <asp:Button ID="Button1" runat="server" Text="<%$ Resources:btnBackResrc %>"  CssClass=button 
             onclick="Button1_Click"/>
         <asp:Button ID="Button2" onclick="btnNext_Click" UseSubmitBehavior="true"   runat="server" Text="<%$ Resources:btnNextResrc %>"  CssClass=button OnClientClick="return Validate_MergeCreateLetter1();"/>
         <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass=button 
             onclick="btnCancel_Click" />
        </tr>
        <tr>
        <td>
            <asp:HiddenField ID="fieldcategory" runat="server" />
            <asp:HiddenField ID="hdnisallowallperms" runat="server" />
        <asp:HiddenField ID="hdnselectedperms" runat="server" />
           <asp:HiddenField ID="hdnselectedpermsids" runat="server" />
            <asp:HiddenField ID="hdnSelectedFields" runat="server" />
            <asp:HiddenField ID="hdncatid" runat="server" />
            <asp:HiddenField ID="hdnSelectedFieldsID" runat="server" />
            <asp:HiddenField ID="hdnfilename" runat="server" />
            <asp:HiddenField ID="hdnselectedpermsusers" runat="server" />
            <asp:HiddenField ID="hdnselectedpermsgroups" runat="server" />
            <asp:HiddenField ID="catname" runat="server" />
             <asp:HiddenField ID="TemplateId" runat="server" />
             <asp:HiddenField ID="hdnformfilecontent" runat="server" />
                 <asp:HiddenField ID="hdnexecutepipeline" runat="server"  Value="false"/>
                  <asp:HiddenField ID="hdnTemplateNames" runat="server" />
           <%--added by swati MITS # 36930--%>
                <asp:HiddenField ID="hdnSelectedUser" runat="server" />
                <asp:HiddenField ID="hdnSelectedUserID" runat="server" />
        </td>
        </tr>
   </table>
    </form>
</body>
</html>
