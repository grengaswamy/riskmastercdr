﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.MailMerge
{
    public partial class MergeEditTemplate2 : System.Web.UI.Page
    {
        protected XmlDocument Model = new XmlDocument();
        string MergeMessageTemplate1 = "<Message> " +
          "<Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization>" +
           " <Call>" +
              "<Function>MailMergeAdaptor.GetAvailableFieldList</Function>" +
            "</Call>" +
            "<Document>" +
              "<Template><CategoryId>1</CategoryId></Template>" +
            "</Document>" +
          "</Message>";
        string MergeMessageTemplate2 = " <Message><Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization><Call>" +
            "<Function>MailMergeAdaptor.GetFieldDisplayCategoryList</Function>" +
            "</Call><Document><Template><CategoryId>1</CategoryId></Template></Document>" +
            "</Message>";

        string MergeMessageTemplate3 = "<Message><Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization><Call>" +
            "<Function>MailMergeAdaptor.GetUsedFieldList</Function></Call><Document>" +
            "<Template><TemplateId></TemplateId></Template></Document></Message>";


        private void SaveValuesFromPreviousScreen()
        {
            if (!Page.IsPostBack)
            {
                lettername.Value = AppHelper.GetFormValue("lettername");
                lbltitle.Text = lettername.Value;
                lob.Value = AppHelper.GetFormValue("lob");
                letterdesc.Value = AppHelper.GetFormValue("letterdesc");
                allstatesselected.Value = AppHelper.GetFormValue("allstatesselected");
                lettertype.Value = AppHelper.GetFormValue("lettertype");
                //spahariya MITS 28867
                lstMailRecipient.Value = AppHelper.GetFormValue("lstMailRecipient");
                EmailCheck.Value = AppHelper.GetFormValue("EmailCheck");
                //spahariya
                //added by swati for AIC MITS# 36930                
                hdnSelectedUser.Value = AppHelper.GetFormValue("hdnSelectedUser");
                hdnSelectedUserID.Value = AppHelper.GetFormValue("hdnSelectedUserID");
                //change end here
                txtState.Value = AppHelper.GetFormValue("txtState");
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
                prefab.Value = AppHelper.GetFormValue("prefab");
                hdnSelectedFieldsID.Value = AppHelper.GetFormValue("hdnSelectedFieldsID");
                hdnSelectedFields.Value = AppHelper.GetFormValue("hdnSelectedFields");
                hdnselectedperms.Value = AppHelper.GetFormValue("hdnselectedperms");
                hdnselectedpermsids.Value = AppHelper.GetFormValue("hdnselectedpermsids");
                hdnfilename.Value = AppHelper.GetFormValue("hdnfilename");
                hdnisallowallperms.Value = AppHelper.GetFormValue("hdnisallowallperms");
                hdnselectedpermsusers.Value = AppHelper.GetFormValue("hdnselectedpermsusers");
                hdnselectedpermsgroups.Value = AppHelper.GetFormValue("hdnselectedpermsgroups");
                fieldcategory.SelectedValue = AppHelper.GetFormValue("fieldcategory");
                catname.Value = AppHelper.GetFormValue("catname");
                TemplateId.Value = AppHelper.GetFormValue("TemplateId");
                hdncatid.Value = AppHelper.GetFormValue("hdncatid");
                hdnexecutepipeline.Value = AppHelper.GetFormValue("hdnexecutepipeline");
                hdnisallowallperms.Value = AppHelper.GetFormValue("hdnisallowallperms");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
                hdnformfilecontent.Value = AppHelper.GetFormValue("hdnformfilecontent");
                hdnfilename.Value = AppHelper.GetFormValue("hdnfilename");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SaveValuesFromPreviousScreen();
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeEditTemplate2.aspx"), "MergeEditTemplate2Validations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeEditTemplate2ValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end

                if (!Page.IsPostBack)
                {
                    MergeMessageTemplate2 = AppHelper.ChangeMessageValue(MergeMessageTemplate2, "//Template/CategoryId", AppHelper.GetFormValue("hdncatid"));
                    string sReturn = AppHelper.CallCWSService(MergeMessageTemplate2.ToString());

                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    PopulateFieldcategory(Model);


                    MergeMessageTemplate3 = AppHelper.ChangeMessageValue(MergeMessageTemplate3, "//Template/TemplateId", TemplateId.Value);
                    sReturn = AppHelper.CallCWSService(MergeMessageTemplate3.ToString());
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    PopulateSelectedFields(Model);

                    MergeMessageTemplate1 = AppHelper.ChangeMessageValue(MergeMessageTemplate1, "//Template/CategoryId", AppHelper.GetFormValue("hdncatid"));
                    sReturn = AppHelper.CallCWSService(MergeMessageTemplate1.ToString());
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        private void PopulateSelectedFields(XmlDocument doc)
        {
            if (hdnexecutepipeline.Value == "false")
            {
                foreach (XmlElement ele in doc.SelectNodes("//UsedFieldList/Fields/Field"))
                {
                    ListItem item = new ListItem();
                    item.Text = ele.Attributes["FieldDesc"].Value;
                    if (ele.Attributes["MergeParam"].Value == "")
                    {
                        item.Value = "_" + ele.Attributes["FieldId"].Value + "_" + ele.Attributes["FieldDesc"].Value;
                    }
                    else
                    {
                        item.Value = "_" + ele.Attributes["FieldId"].Value + "|" + ele.Attributes["MergeParam"].Value + "_" + ele.Attributes["FieldDesc"].Value;
                    }
                    selectedfields.Items.Add(item);
                }
            }
            XmlNode objNode = doc.SelectSingleNode("//UsedFieldList/OldPhoneFieldExists");
            if (objNode != null)
            {
                hdnOldPhoneFieldExists.Value = objNode.InnerText;
            }
        }
        private void PopulateFieldcategory(XmlDocument doc)
        {
            ListItem item = new ListItem();
            //item.Text = "Select a Category";//Amandeep ML Changes
            item.Text = RMXResourceProvider.GetSpecificObject("cmbSelectCategory", RMXResourceProvider.PageId("MergeEditTemplate2.aspx"), "0");               
            item.Value = "";
            fieldcategory.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//Category"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["CategoryName"].Value;
                item.Value = ele.Attributes["CategoryName"].Value;
                fieldcategory.Items.Add(item);
                fieldcategory.Attributes.Add("onchange", "Javascript:return LoadFieldCategory()");
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeEditTemplate3.aspx");
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeEditTemplate1.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplates.aspx");
        }

      
    }
}
