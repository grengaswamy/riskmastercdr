﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeEditTemplate3.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeEditTemplate3"%>

<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="javascript" src='../../Scripts/merge.js'></script>
    				<script type="text/javascript" language="JavaScript">
				function fncOnLoad()
                {
                     
                      if ( document.getElementById('hdnexecutepipeline').value=='true')
                      {
                          if ( document.getElementById('hdnisallowallperms').value=='false' ||  document.getElementById('hdnisallowallperms').value=='' )
                          {
                          document.getElementById('allowall').checked =false;
                          }
                          else
                          {
                          document.getElementById('allowall').checked =true;
                          }
                      ToggleCustomPerms();
                }
                }
				</script>
</head>
<body onload="javaScript:InitPageSettings_MergeTemplate3();fncOnLoad()">
    <form id="frmData" runat="server">
     <div id="maindiv" style="height:100%;width:99%;overflow:auto">
      <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
   <table width="100%" border="0">
    <tr class="msgheader">
     <td colspan="3"><asp:Label ID="lblMiscAdmin" runat="server" Text="<%$ Resources:lblMiscAdminResrc %>"></asp:Label></td>
    </tr>
    <tr class="ctrlgroup">
     <td colspan="3"><asp:Label ID="lblEditMergeLetter" runat="server" Text="<%$ Resources:lblEditMergeLetterResrc %>"></asp:Label><asp:Label ID="lbltitle" runat="server"></asp:Label></td>
    </tr>
    <tr>
     <td colspan="3" class="colheader3"><asp:Label ID="lblPermMergeLetter" runat="server" Text="<%$ Resources:lblPermMergeLetterResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td width="10%"></td>
     <td width="1%"></td>
     <td width="89%"></td>
    </tr>
    <tr>
     <td nowrap="1" valign="top" colspan="2"><input type="checkbox" runat=server id="allowall" onclick="ToggleCustomPerms();">
      		
      
      				<asp:Label ID="lblAllowUsers" runat="server" Text="<%$ Resources:lblAllowUsersResrc %>"></asp:Label>
      			
     </td>
     <td></td>
    </tr>
    <tr>
     <td colspan="3" class="ctrlgroup"></td>
    </tr>
    <tr>
     <td colspan="3"><asp:Label ID="lblAllowSelUsers" runat="server" Text="<%$ Resources:lblAllowSelUsersResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td nowrap="1"><em><asp:Label ID="lblAvailUsersGroups" runat="server" Text="<%$ Resources:lblAvailUsersGroupsResrc %>"></asp:Label></em><br>
     <select id="availperms" runat="server" ondblclick="AddSingle_MergeCreateTemplate3(this.options[this.selectedIndex]);" multiple="true"  size="12">
      </select><br><font size="-2"><asp:Label ID="lblUser" runat="server" Text="<%$ Resources:lblUserResrc %>"></asp:Label></font></td>
     <td align="left" nowrap="1" valign="center"><input type="button"  value="&gt;&gt;" id="add" class="button"  onclick="javascript:return AddSelected_MergeCreateTemplate3();"><br><input type="button"  value="<<" id="remove" class="button" onClick="javascript:return RemoveSelected_MergeCreateTemplate3();"><br></td>
     <td nowrap="1" align="left"><em><asp:Label ID="lblSelUsersGroups" runat="server" Text="<%$ Resources:lblSelUsersGroupsResrc %>"></asp:Label></em><br><select runat=server id="selectedperms"  ondblclick="RemoveSingle_MergeCreateTemplate3(this.options[this.selectedIndex]);" multiple="true" size="12"></select></td>
    </tr>
    <tr class="ctrlgroup">
     <td colspan="3">&nbsp;</td>
    </tr><input type="hidden" runat=server id="hdnselectedpermsusers"/><input type="hidden"  id="hdnselectedpermsgroups" runat=server/><input type="hidden" runat=server value="" id="hdnpreviousaction"/><input type="hidden" runat=server  id="hdnexecutepipeline"/><input type="hidden" value="true"  runat=server id="hdnisallowallperms"/><input type="hidden" runat=server  id="hdnselectedperms"/><input  runat="server" type="hidden"   id="hdnselectedpermsids"/><tr>
     <td colspan="3">
     <asp:Button ID="Button1"  onclick="btnBack_Click" runat="server" Text="<%$ Resources:btnBackResrc %>"  UseSubmitBehavior="true" CssClass=button OnClientClick="return backMergeTemplate2();"/>
         <asp:Button ID="Button2" runat="server"  onclick="btnNext_Click" UseSubmitBehavior="true"  Text="<%$ Resources:btnNextResrc %>"  CssClass=button OnClientClick="return Validate_MergeCreateTemplate3();"/>
         <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass=button 
             onclick="btnCancel_Click" /></td>
    
    </tr>
   </table>
   <ul class="gen">
    	<asp:Label ID="lblConfigPerm" runat="server" Text="<%$ Resources:lblConfigPermResrc %>"></asp:Label>
    
   </ul><br><ul class="gen">
    		<asp:Label ID="lblSpecialPerm" runat="server" Text="<%$ Resources:lblSpecialPermResrc %>"></asp:Label>,
    </ul>		
    <ul class="gen">
     <li><asp:Label ID="lblAllowOtherUsers" runat="server" Text="<%$ Resources:lblAllowOtherUsersResrc %>"></asp:Label></li>
     <li><asp:Label ID="lblMoveUsers" runat="server" Text="<%$ Resources:lblMoveUsersResrc %>"></asp:Label></li>
     <li><asp:Label ID="lblPressNext2" runat="server" Text="<%$ Resources:lblPressNextResrc1 %>"></asp:Label></li>
    </ul>
    		<asp:Label ID="lblOtherwise" runat="server" Text="<%$ Resources:lblOtherwiseResrc %>"></asp:Label>,
    		
    <ul class="gen">
     <li><asp:Label ID="lblLeaveAllowUsers" runat="server" Text="<%$ Resources:lblLeaveAllowUsersResrc %>"></asp:Label></li>
     <li><asp:Label ID="lblPressNext" runat="server" Text="<%$ Resources:lblPressNextResrc2 %>"></asp:Label></li>
    </ul> 
    <asp:HiddenField ID="fieldcategory" runat="server" />
     <asp:HiddenField ID="lettername" runat="server" />
        <asp:HiddenField ID="lob" runat="server" />
        <asp:HiddenField ID="letterdesc" runat="server" />
        <asp:HiddenField ID="allstatesselected" runat="server" />
        <asp:HiddenField ID="lettertype" runat="server" />
        <asp:HiddenField ID="txtState" runat="server" />
        <asp:HiddenField ID="txtState_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumentformat" runat="server" />
         <asp:HiddenField ID="txtmergedocumentformat_cid" runat="server" />
           <asp:HiddenField ID="prefab" runat="server" />
    <asp:HiddenField ID="hdnSelectedFields" runat="server" />
    <asp:HiddenField ID="hdnSelectedFieldsID" runat="server" />
   <asp:HiddenField ID="catname" runat="server" />
            <asp:HiddenField ID="TemplateId" runat="server" />
            <asp:HiddenField ID="hdncatid" runat="server" />
    <asp:HiddenField ID="hdnfilename" runat="server" />
    <asp:HiddenField ID="hdnformfilecontent" runat="server" />
  <asp:HiddenField ID="EmailCheck" runat="server" />
        <asp:HiddenField ID="lstMailRecipient" runat="server" />
    <%--added by swati MITS # 36930--%>
                <asp:HiddenField ID="hdnSelectedUser" runat="server" />
                <asp:HiddenField ID="hdnSelectedUserID" runat="server" />
    </div>

    </form>
</body>
</html>
