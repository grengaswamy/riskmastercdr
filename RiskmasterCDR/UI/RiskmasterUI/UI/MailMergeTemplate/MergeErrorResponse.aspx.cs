﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.UI.MailMerge
{
    public partial class MergeErrorResponse : System.Web.UI.Page
    {
        private void SaveValuesFromPreviousScreen()
        {

            if (!Page.IsPostBack)
            {
                lettername.Value = AppHelper.GetFormValue("lettername");
                lob.Value = AppHelper.GetFormValue("lob");
                letterdesc.Value = AppHelper.GetFormValue("letterdesc");
                allstatesselected.Value = AppHelper.GetFormValue("allstatesselected");
                lettertype.Value = AppHelper.GetFormValue("lettertype");
                txtState.Value = AppHelper.GetFormValue("txtState");
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
                prefab.Value = AppHelper.GetFormValue("prefab");
                hdnSelectedFieldsID.Value = AppHelper.GetFormValue("hdnSelectedFieldsID");
                hdnSelectedFields.Value = AppHelper.GetFormValue("hdnSelectedFields");
                selectedfields.Value = AppHelper.GetFormValue("hdnSelectedFields");
                hdnselectedperms.Value = AppHelper.GetFormValue("hdnselectedperms");
                hdnselectedpermsids.Value = AppHelper.GetFormValue("hdnselectedpermsids");
                hdnfilename.Value = AppHelper.GetFormValue("hdnfilename");
                hdnisallowallperms.Value = AppHelper.GetFormValue("hdnisallowallperms");
                hdnselectedpermsusers.Value = AppHelper.GetFormValue("hdnselectedpermsusers");
                hdnselectedpermsgroups.Value = AppHelper.GetFormValue("hdnselectedpermsgroups");
                fieldcategory.Value = AppHelper.GetFormValue("fieldcategory");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
                hdnformfilecontent.Value = AppHelper.GetFormValue("hdnformfilecontent");
                TemplateId.Value = AppHelper.GetFormValue("TemplateId");
            }



        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                    SaveValuesFromPreviousScreen();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (TemplateId.Value == "")
            {
                Server.Transfer("MergeCreateTemplate4.aspx");
            }
            else
            {
                Server.Transfer("MergeEditTemplate4.aspx");
            }
            
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplates.aspx");
        }
    }
}
