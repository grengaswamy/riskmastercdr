﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.MailMerge
{
    public partial class MergeTemplates : System.Web.UI.Page
    {
        string DeleteMessageTemplate = "<Message><Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization><Call><Function>MailMergeAdaptor.DeleteTemplate</Function></Call><Document><Template><TemplateId/></Template></Document></Message>";

        string MergeMessageTemplate = "<Message><Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization><Call><Function>MailMergeAdaptor.GetCustomTemplateList</Function></Call><Document><Template /></Document></Message>";
        public XmlDocument Model = new XmlDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
          
            try
            {
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeTemplates.aspx"), "MergeTemplatesValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeTemplatesValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end

                if (!Page.IsPostBack)
                {
                    string sReturn = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeCreateTemplate1.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteMessageTemplate = AppHelper.ChangeMessageValue(DeleteMessageTemplate, "//Template/TemplateId", hdnDeletedItems.Value);
                string sReturn = AppHelper.CallCWSService(DeleteMessageTemplate.ToString());
                hdnDeletedItems.Value = "";
                sReturn = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                Model = new XmlDocument();
                Model.LoadXml(sReturn);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            
        }
    }
}
