﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeEditTemplate4.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeEditTemplate4"%>


<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Riskmaster</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" language="javascript" src='../../Scripts/merge.js'></script>
    <script  type="text/javascript" language="javascript" src="../../Scripts/Silverlight.js"></script>
</head>
<body onload="InitPageSettings_CreateTemplate4();">
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <table width="100%" border="0">
        <b>
            <asp:Label ID="lblEditAttach" runat="server" Text="<%$ Resources:lblEditAttachResrc %>"></asp:Label><asp:Label
                ID="lblFilename" runat="server"></asp:Label></b><br>
        <tr class="ctrlgroup">
            <td colspan="2">
                <asp:Label ID="lblEditMergeTemplate" runat="server" Text="<%$ Resources:lblEditMergeTemplateResrc %>"></asp:Label>
            </td>
        </tr>
        <textarea cols="20" wrap="soft" style="display: none" name="formfilecontent"></textarea><tr>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <asp:Label ID="lblCmpltWrdMerge" runat="server" Text="<%$ Resources:lblCmpltWrdMergeResrc %>"></asp:Label><br>
                <br>
                <ul class="gen">
                    <li>
                        <asp:Label ID="lblClickLaunch" runat="server" Text="<%$ Resources:lblClickLaunchResrc %>"></asp:Label></li>
                    <li>
                        <asp:Label ID="lblClickOk" runat="server" Text="<%$ Resources:lblClickOkResrc %>"></asp:Label></li>
                    <li>
                        <asp:Label ID="lblOpenTemp" runat="server" Text="<%$ Resources:lblOpenTempResrc %>"></asp:Label></li>
                    <li><b>
                        <asp:Label ID="lblSaveDoc" runat="server" Text="<%$ Resources:lblSaveDocResrc %> "></asp:Label>&nbsp;</b>
                        <asp:Label ID="lblCloseDoc" runat="server" Text="<%$ Resources:lblCloseDocResrc %>"></asp:Label>
                    </li>
                    <li>
                        <asp:Label ID="lblClickFinish" runat="server" Text="<%$ Resources:lblClickFinishResrc %>"></asp:Label></li>
                </ul>
                <center>
      <input type="button" id="Button3" value="<%$ Resources:btnLaunchWordResrc %>" class="button" runat="server" onclick="Javascript:return StartWord_CreateTemplate4();" />
            </td>
        </tr>
        <tr>
            <td>
     
      <asp:Button ID="Button1"  onclick="btnBack_Click" runat="server" Text="<%$ Resources:btnBackResrc %>" UseSubmitBehavior="true" CssClass=button OnClientClick="return Finish_CreateTemplate4();"/>
         <asp:Button ID="Button2" runat="server"  onclick="btnFinish_Click" UseSubmitBehavior="true"  Text="<%$ Resources:btnFinishResrc %>"  CssClass=button OnClientClick="return Finish_CreateTemplate4();"/>
         <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass=button 
             onclick="btnCancel_Click"/>
            </td>
        </tr>
        <asp:HiddenField ID="hdnisallowallperms" runat="server" />
        <asp:HiddenField ID="lettername" runat="server" />
        <asp:HiddenField ID="lob" runat="server" />
        <asp:HiddenField ID="letterdesc" runat="server" />
        <asp:HiddenField ID="allstatesselected" runat="server" />
        <asp:HiddenField ID="lettertype" runat="server" />
        <asp:HiddenField ID="txtState" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumentformat" runat="server" />
        <asp:HiddenField ID="txtmergedocumentformat_cid" runat="server" />
        <asp:HiddenField ID="prefab" runat="server" />
        <asp:HiddenField ID="hdnSelectedFields" runat="server" />
        <asp:HiddenField ID="selectedfields" runat="server" />
        <asp:HiddenField ID="fieldcategory" runat="server" />
        <asp:HiddenField ID="txtState_cid" runat="server" />
        <asp:HiddenField ID="hdnSelectedFieldsID" runat="server" />
        <asp:HiddenField ID="hdnselectedpermsusers" runat="server" />
        <asp:HiddenField ID="hdnselectedperms" runat="server" />
        <asp:HiddenField ID="hdnselectedpermsids" runat="server" />
        <asp:HiddenField ID="hdnfilename" runat="server" />
        <asp:HiddenField ID="hdnformfilecontent" runat="server" />
        <asp:HiddenField ID="hdnselectedpermsgroups" runat="server" />
        <asp:HiddenField ID="TemplateId" runat="server" />
        <asp:HiddenField ID="hdncatid" runat="server" />
        <asp:HiddenField ID="hdnexecutepipeline" runat="server" />
          <asp:HiddenField ID="EmailCheck" runat="server" />
        <asp:HiddenField ID="lstMailRecipient" runat="server" />
		<asp:HiddenField ID="hdnSilver" runat="server" />
        <%--added by swati MITS # 36930--%>
                <asp:HiddenField ID="hdnSelectedUser" runat="server" />
                <asp:HiddenField ID="hdnSelectedUserID" runat="server" />
    </table>
    
   <div id="silverlightControlHost">
        <object data="data:application/x-silverlight-2," id="Silverlight" type="application/x-silverlight-2" width="100%" height="100%">
		  <param name="source" value="ClientBin/RiskmasterSL.xap"/>
		  <param name="onError" value="onSilverlightError" />
		  <param name="background" value="white" />
		  <param name="minRuntimeVersion" value="5.0.61118.0" />
		  <param name="autoUpgrade" value="true" />
           <param name="onload" value="silverlight_onload" />
		  <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=5.0.61118.0" style="text-decoration:none">
 			  <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style:none"/>
		  </a>
	    </object><iframe id="_sl_historyFrame" style="visibility:hidden;height:0px;width:0px;border:0px"></iframe></div>
    </form>
</body>
</html>
