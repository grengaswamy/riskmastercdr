﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.MailMerge
{
    public partial class MergeCreateTemplate3 : System.Web.UI.Page
    {
        protected XmlDocument Model = new XmlDocument();
        string MergeMessageTemplate = " <Message><Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization><Call>" +
           "<Function>MailMergeAdaptor.GetAvailablePermList</Function>" +
           "</Call><Document><Template/></Document>" +
           "</Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeCreateTemplate3.aspx"), "MergeCreateTemplate3Validations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeCreateTemplate3ValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end

                if (!Page.IsPostBack)
                {
                    string sReturn = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    PopulatePermissions(Model);
                    SaveValuesFromPreviousScreen();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        private void SaveValuesFromPreviousScreen()
        {
            if (!Page.IsPostBack)
            {
                lettername.Value = AppHelper.GetFormValue("lettername");
                lbltitle.Text = lettername.Value;
                lob.Value = AppHelper.GetFormValue("lob");
                letterdesc.Value = AppHelper.GetFormValue("letterdesc");
                allstatesselected.Value = AppHelper.GetFormValue("allstatesselected");
                lettertype.Value = AppHelper.GetFormValue("lettertype");
                //spahariya MITS 28867
                lstMailRecipient.Value = AppHelper.GetFormValue("lstMailRecipient");
                EmailCheck.Value = AppHelper.GetFormValue("EmailCheck");
                //spahariya
                //added by swati MITS # 36930
                hdnSelectedUser.Value = AppHelper.GetFormValue("hdnSelectedUser");
                hdnSelectedUserID.Value = AppHelper.GetFormValue("hdnSelectedUserID");
                txtState.Value = AppHelper.GetFormValue("txtState");
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
                prefab.Value = AppHelper.GetFormValue("prefab");
                hdnSelectedFieldsID.Value = AppHelper.GetFormValue("hdnSelectedFieldsID");
                hdnSelectedFields.Value = AppHelper.GetFormValue("hdnSelectedFields");
                hdnselectedperms.Value = AppHelper.GetFormValue("hdnselectedperms");
                hdnselectedpermsids.Value = AppHelper.GetFormValue("hdnselectedpermsids");
                hdnfilename.Value = AppHelper.GetFormValue("hdnfilename");
                hdnisallowallperms.Value = AppHelper.GetFormValue("hdnisallowallperms");
                hdnselectedpermsusers.Value = AppHelper.GetFormValue("hdnselectedpermsusers");
                hdnselectedpermsgroups.Value = AppHelper.GetFormValue("hdnselectedpermsgroups");
                fieldcategory.Value = AppHelper.GetFormValue("fieldcategory");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
                hdnformfilecontent.Value = AppHelper.GetFormValue("hdnformfilecontent");
            }
        }
        private void PopulatePermissions(XmlDocument doc)
        {
            foreach (XmlElement ele in doc.SelectNodes("//PermItem"))
            {
                ListItem item = new ListItem();
                if (ele.GetAttribute("GroupId") != "0")
                {
                    item.Text = "*" + ele.Attributes["DisplayName"].Value;
                    item.Value = "group_" + ele.Attributes["GroupId"].Value;
                }
                else
                {
                    item.Text = ele.Attributes["DisplayName"].Value;
                    item.Value = "user_" + ele.Attributes["UserId"].Value;
                }
                availperms.Items.Add(item);
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeCreateTemplate4.aspx");
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (lettertype.Value == "20")
            {
                Server.Transfer("MergeCreateAdminTrackingTemplate.aspx");
            }
            else
            Server.Transfer("MergeCreateTemplate2.aspx");
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplates.aspx");
        }

       
    }
}
