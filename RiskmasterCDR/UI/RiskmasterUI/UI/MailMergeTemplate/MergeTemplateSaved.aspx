﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeTemplateSaved.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeTemplateSaved" %>

<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>





<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
        <script  type="text/javascript" language="javascript" src='../../Scripts/merge.js'></script>
</head>
<body>
    <form id="frmData" runat="server">
     <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
    <table width="100%" border="0">
    
    <tr class="msgheader">
     <td colspan="2">
         <asp:Label ID="lblMiscAdminResrc" runat="server" Text="<%$ Resources:lblMiscAdminResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td colspan="2" class="ctrlgroup">
         <asp:Label ID="lblMergeLetterResrc" runat="server" Text="<%$ Resources:lblMergeLetterResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td>
      <p class="gen">
          <asp:Label ID="lblCreationCmpltResrc" runat="server" Text="<%$ Resources:lblCreationCmpltResrc %>"></asp:Label></p>
     </td>
    </tr>
    <tr>
     <td valign="top" colspan="2">
         <asp:Label ID="lblChangesStoredResrc" runat="server" Text="<%$ Resources:lblChangesStoredResrc %>"></asp:Label>  
      								<br>
         <asp:Label ID="lblClickDoneResrc" runat="server" Text="<%$ Resources:lblClickDoneResrc %>"></asp:Label>
      								<br><br></td>
    </tr>
    <tr>
     <td colspan="2" class="ctrlgroup">&nbsp;</td>
    </tr>
    <tr>
     <td colspan="2"><asp:Button ID="btnDone"  onclick="btnDone_Click" runat="server" Text="<%$ Resources:btnDoneResrc %>" UseSubmitBehavior="true" CssClass=button /></td>
    </tr>
   </table>
    </form>
</body>
</html>
