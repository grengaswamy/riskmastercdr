﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.MailMerge
{
    public partial class MergeEditTemplate1 : System.Web.UI.Page
    {
        string MergeMessageTemplate = "<Message> " +
          "<Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization>" +
           " <Call>" +
              "<Function>MailMergeAdaptor.GetCurrentTemplateInfo</Function>" +
            "</Call>" +
            "<Document>" +
              "<Template><TemplateId></TemplateId></Template>" +
            "</Document>" +
          "</Message>";
        private void PopulateOtherValues(XmlDocument Model)
        {
            if (Page.PreviousPage == null)
            {
                XmlNode node = Model.SelectSingleNode("//Template");
                if (node != null)
                {
                    if (node.Attributes["StateId"].Value == "-1")
                    {
                        allstatesselected.Checked = true;
                    }
                    else
                    {
                        if (node.Attributes["StateShortCode"].Value != "")
                        {
                            txtState.Value = node.Attributes["StateShortCode"].Value + " " + node.Attributes["StateDesc"].Value;
                            txtState_cid.Value = node.Attributes["StateId"].Value;
                        }
                    }
                }
            }
            else
            {
                if (AppHelper.GetFormValue("allstatesselected") == "on")
                    allstatesselected.Checked = true;
                txtState.Value = AppHelper.GetFormValue("txtState");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");

            }

            if (Page.PreviousPage == null)
            {
                XmlNode node = Model.SelectSingleNode("//Template");
                if (node != null)
                {
                    if (node.Attributes["MergeDocumentType"].Value != "0" &&  node.Attributes["MergeDocumentType"].Value != "")
                    {
                        if (node.Attributes["MergeDocumentTypeShortCode"].Value != "")
                        {
                            txtmergedocumenttype.Value = node.Attributes["MergeDocumentTypeShortCode"].Value + " " + node.Attributes["MergeDocumentTypeDesc"].Value;
                            txtmergedocumenttype_cid.Value = node.Attributes["MergeDocumentType"].Value;
                        }
                    }
                }
            }
            else
            {
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
            }

            if (Page.PreviousPage == null)
            {
                XmlNode node = Model.SelectSingleNode("//Template");
                if (node != null)
                {
                    if (node.Attributes["MergeDocumentFormat"].Value != "0" && node.Attributes["MergeDocumentFormat"].Value != "")
                    {
                        if (node.Attributes["MergeFormatShortCode"].Value != "")
                        {
                            txtmergedocumentformat.Value = node.Attributes["MergeFormatShortCode"].Value + " " + node.Attributes["MergeFormatDesc"].Value;
                            txtmergedocumentformat_cid.Value = node.Attributes["MergeDocumentFormat"].Value;
                        }
                    }
                }
            }
            else
            {
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
            }
            if (Page.PreviousPage == null)
            {
                XmlNode node = Model.SelectSingleNode("//Template");
                if (node != null)
                {
                    if (node.Attributes["FormFileName"].Value!= "")
                    {
                        hdnfilename.Value = node.Attributes["FormFileName"].Value;
                    }
                }
            }
            else
            {
                hdnfilename.Value = AppHelper.GetFormValue("hdnfilename");
            }
            if (Page.PreviousPage == null)
            {
                XmlNode node = Model.SelectSingleNode("//Template");
                if (node != null)
                {
                    if (node.Attributes["FormFileContent"].Value != "")
                    {
                        //hdnformfilecontent.Value = node.Attributes["FormFileContent"].Value;
                    }
                }
            }
            else
            {
                hdnformfilecontent.Value = AppHelper.GetFormValue("hdnformfilecontent");
            }

            //spahariya MITS 28867 - start
            if (Page.PreviousPage == null)
            {
                XmlNode node = Model.SelectSingleNode("//Template");
                if (node != null)
                {
                    if (node.Attributes["mailRecipient"].Value != "")
                    {
                        lstMailRecipient.SelectedValue = node.Attributes["mailRecipient"].Value;
                    }
                }
            }
            else
            {
                lstMailRecipient.SelectedValue = AppHelper.GetFormValue("lstMailRecipient");
            }

            if (Page.PreviousPage == null)
            {
                XmlNode node = Model.SelectSingleNode("//Template");
                if (node != null)
                {
                    if (node.Attributes["emailCheck"].Value == "-1")
                    {
                        EmailCheck.Checked = true;
                        lstMailRecipient.Enabled = true;
                    }
                    else
                    {
                        EmailCheck.Checked = false;
                    }
                }
            }
            else
            {
                if (AppHelper.GetFormValue("EmailCheck") == "true")
                {
                    EmailCheck.Checked = true;
                    lstMailRecipient.Enabled = true;
                }
                else
                {
                    EmailCheck.Checked = false;
                }
            }
            //added by swati for AIC Gap 7 MITS # 36930
            if (Page.PreviousPage == null)
            {
                ListItem item = new ListItem();                
                foreach (XmlElement ele in Model.SelectNodes("//GetUserRecipientList/Recipients/Recipient"))
                {
                    item = new ListItem();
                    item.Text = ele.Attributes["codedesc"].Value;
                    item.Value = ele.Attributes["codeid"].Value;
                    selecteduserfields.Items.Add(item);
                }
            }
            else
            {
                hdnSelectedUser.Value = AppHelper.GetFormValue("hdnSelectedUser");
                hdnSelectedUserID.Value = AppHelper.GetFormValue("hdnSelectedUserID");
            }
            //change end here by swati
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SaveValuesFromPreviousScreen();
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeEditTemplate1.aspx"), "MergeEditTemplate1Validations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeEditTemplate1ValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end

                if (!Page.IsPostBack)
                {
                    if (Page.PreviousPage == null)
                        MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Template/TemplateId", AppHelper.GetQueryStringValue("id"));
                    else
                        MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Template/TemplateId", AppHelper.GetFormValue("TemplateId"));
                    string sReturn = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                    XmlDocument Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    PopulateTemplateName(Model);
                    PopulateLetterDataSource(Model);
                    PopulateLOB(Model); 
                    //rkapoor commented 
                    //PopulateMailRecipient(Model);
                    PopulateDocumentTemplate(Model);
                    PopulateTemplateId(Model); 
                    PopulateOtherValues(Model);
                    PopulateMailRecipient(Model);     // rkapoor added JIRA 7515 gap 8
                    PopulateTemplateNames(Model);
                    hdnfilename.Value = AppHelper.GetFormValue("hdnfilename");
                    if (hdnfilename.Value == "")
                        PopulateFileName(Model);
                    if (Page.PreviousPage == null)
                    {
                        XmlNode node = Model.SelectSingleNode("//Template");
                        if (node != null)
                        {
                            lob.SelectedValue = node.Attributes["LOB"].Value;
                        }
                    }
                    else
                        lob.SelectedValue = AppHelper.GetFormValue("lob");


                    if (Page.PreviousPage == null)
                    {
                        XmlNode node = Model.SelectSingleNode("//Template");
                        if (node != null)
                        {
                            letterdesc.Value = node.Attributes["FormDesc"].Value;
                        }
                    }
                    txtDisabledmergedocumenttype.Value = txtmergedocumenttype.Value;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
           

            
        }
        
        private void PopulateTemplateNames(XmlDocument doc)
        {
            XmlNode node = doc.SelectSingleNode("//TemplateNames");
            if (node != null)
            {
                hdnTemplateNames.Value = node.InnerText;
            }
        }
       
        private void PopulateTemplateId(XmlDocument doc)
        {
            if (Page.PreviousPage == null)
            {
                XmlNode node = doc.SelectSingleNode("//Template");
                if (node != null)
                {
                    TemplateId.Value = node.Attributes["FormId"].Value;
                }
            }
            else
            {
                TemplateId.Value = AppHelper.GetFormValue("TemplateId");
            }

        }
        private void PopulateTemplateName(XmlDocument doc)
        {
            XmlNode node = doc.SelectSingleNode("//Template");
            if (node != null)
            {
                lettername.Value = node.Attributes["FormName"].Value;
                lbltitle.Text = lettername.Value;
            }

        }
        private void PopulateLetterDataSource(XmlDocument doc)
        {
            string catid = "";
            ListItem item = new ListItem();
            XmlNode node = doc.SelectSingleNode("//Template");
            if (node != null)
            {
                catid = node.Attributes["CatId"].Value;
                hdncatid.Value = catid;
            }
            if (catid == "20")
            {
                item.Text = "Administrative Tracking Merge";
            }
            else
            {
                item.Text = AppHelper.GetValue("catname");
            }

            //abansal23 on 3/25/2009 : To disable LOB if category is not Claim Merge - Start
            if (catid != "1")
            {
                lob.Enabled = false;
            }
            //abansal23 on 3/25/2009 : To disable LOB if category is not Claim Merge - Stop
            item.Value = "";
            lettertype.Items.Add(item);
            
            lettertype.Enabled = false;
        }
        //spahariya : MITS 28867 on 06/26/2012 Start
        private void PopulateMailRecipient(XmlDocument doc)
        {
            ListItem item = new ListItem();
            item.Text = "";
            item.Value = "";
            lstMailRecipient.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//GetMailRecipient/Recipients/Recipient"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["codedesc"].Value;
                item.Value = ele.Attributes["codeid"].Value;
                lstMailRecipient.Items.Add(item);
            }


            // rkapoor29 added JIRA 7515 gap 8 start 
            for (int i = 0; i < selecteduserfields.Items.Count; i++ )
            {
                ListItem item1 = new ListItem();
                item1.Text = selecteduserfields.Items[i].Text;
                item1.Value = selecteduserfields.Items[i].Value;
                if(item1.Text.Contains('*') && item1.Value.Contains('*'))
                { item1.Text = item1.Text.Replace("*","");
                item1.Value = item1.Value.Replace("*", "");
                }

                lstMailRecipient.Items.Remove(item1);
            }
            // rkapoor29 added JIRA 7515 gap 8 end

        }

        //spahariya : MITS 28867 on 06/26/2012 End
        
        private void SaveValuesFromPreviousScreen()
        {
            if (!Page.IsPostBack)
            {
                lettername.Value = AppHelper.GetFormValue("lettername");
                lob.SelectedValue = AppHelper.GetFormValue("lob");
                letterdesc.Value = AppHelper.GetFormValue("letterdesc");
                allstatesselected.Value = AppHelper.GetFormValue("allstatesselected");
                lettertype.SelectedValue = AppHelper.GetFormValue("lettertype");
                txtState.Value = AppHelper.GetFormValue("txtState");
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
                prefab.SelectedValue = AppHelper.GetFormValue("prefab");
                //spahariya 28867
                //commented by swati
                //lstMailRecipient.SelectedValue = AppHelper.GetFormValue("lstMailRecipient");
                if (AppHelper.GetFormValue("EmailCheck") == "true")
                {
                    EmailCheck.Checked = true;
                }
                else
                {
                    EmailCheck.Checked = false;
                    lstMailRecipient.Enabled = false;
                }
                //spahariya - end
                //added by swati for AIC MITS# 36930                
                hdnSelectedUser.Value = AppHelper.GetFormValue("hdnSelectedUser");
                hdnSelectedUserID.Value = AppHelper.GetFormValue("hdnSelectedUserID");
                //change end here
                hdnSelectedFieldsID.Value = AppHelper.GetFormValue("hdnSelectedFieldsID");
                hdnSelectedFields.Value = AppHelper.GetFormValue("hdnSelectedFields");
                hdnselectedperms.Value = AppHelper.GetFormValue("hdnselectedperms");
                hdnselectedpermsids.Value = AppHelper.GetFormValue("hdnselectedpermsids");
                fieldcategory.Value = AppHelper.GetFormValue("fieldcategory");
                hdnisallowallperms.Value = AppHelper.GetFormValue("hdnisallowallperms");
                hdnselectedpermsusers.Value = AppHelper.GetFormValue("hdnselectedpermsusers");
                hdnselectedpermsgroups.Value = AppHelper.GetFormValue("hdnselectedpermsgroups");
                catname.Value = AppHelper.GetQueryStringValue("catname");
                TemplateId.Value = AppHelper.GetFormValue("TemplateId");
                hdncatid.Value = AppHelper.GetFormValue("hdncatid");
                if (Page.PreviousPage != null)
                {
                    hdnexecutepipeline.Value = AppHelper.GetFormValue("hdnexecutepipeline");
                    hdnisallowallperms.Value = AppHelper.GetFormValue("hdnisallowallperms");
                }

            }
        }
        private void PopulateLOB(XmlDocument doc)
        {
            ListItem item = new ListItem();
            item.Text = "ALL - ALL LOBs";
            item.Value = "-1";
            lob.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//GetLOBList/LOBs/LOB"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["shortcode"].Value + " - " + ele.Attributes["codedesc"].Value;
                item.Value = ele.Attributes["codeid"].Value;
                lob.Items.Add(item);
            }

            
        }
        private void PopulateDocumentTemplate(XmlDocument doc)
        {
            ListItem item = new ListItem();
            item.Text = "N/A";
            item.Value = "n/a";
            prefab.Items.Add(item);
            
        }
        private void PopulateFileName(XmlDocument doc)
        {
            XmlNode node = doc.SelectSingleNode("//Template");
            if (node != null)
            {
                hdnfilename.Value = node.Attributes["FormFileName"].Value;
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (hdncatid.Value  == "20")
            {
                Server.Transfer("MergeEditAdminTrackingTemplate.aspx");
            }
            else
                Server.Transfer("MergeEditTemplate2.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplates.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplates.aspx");
        }
    }
}
