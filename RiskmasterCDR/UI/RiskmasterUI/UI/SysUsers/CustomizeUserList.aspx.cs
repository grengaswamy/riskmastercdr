﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.SysUsers
{
    public partial class CustomizeUserList : NonFDMBasePageCWS
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        public string sParent = "";
        public string sOptionType = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //Amandeep MultiLingual Changes--start
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CustomizeUserList.aspx"), "CustomizeUserListValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CustomizeUserListValidationsScripts", sValidationResources, true);
            //Amandeep MultiLingual Changes--end

            bool bReturnStatus = false;
            string sreturnValue = "";
            XmlDocument SysUsersXmlDoc = new XmlDocument();
            string sParentControl= "";            
            XmlNodeList sysUsersTypeList = null;
            XmlNodeList sysUserList = null;// Mits 35697 starts

            //sharishkumar Jira 6415 starts
            string sUserGroups = "";
            string smultiselect = "";
            //sharishkumar Jira 6415 ends
            bool blnCommaSeparator = false;

            //Pen testing :atavaragiri :mits 27831

            if (!String.IsNullOrEmpty(Request.QueryString["parentcontrolname"]))
            {    //sParentControl =Convert.ToString(Request.QueryString["parentcontrolname"]);
                sParentControl = AppHelper.HTMLCustomEncode(Convert.ToString(Request.QueryString["parentcontrolname"]));
                
             }
             //End :Pen testing :atavaragiri :mits 27831
             hdnParentControlname.Value = sParentControl;
            
            //Pen testing :atavaragiri :mits 27831
             if (!String.IsNullOrEmpty(Request.QueryString["parent"]))
            {
                //sParent = Convert.ToString(Request.QueryString["parent"]);
                sParent = AppHelper.HTMLCustomEncode(Convert.ToString(Request.QueryString["parent"]));
            }
                //End :Pen testing :atavaragiri :mits 27831
            hdnParentform.Value  = sParent;
            parentform.Text = sParent;

            if (Request.QueryString["userid"] != null)
            {   //Pen testing :atavaragiri :mits 27831
                //hdnUseridstr.Value = Convert.ToString(Request.QueryString["userid"]);
                hdnUseridstr.Value  = AppHelper.HTMLCustomEncode (Convert.ToString(Request.QueryString["userid"]));
                //End :Pen testing :atavaragiri :mits 27831
            }
            if (Request.QueryString["username"] != null)
            {    //Pen testing :atavaragiri :mits 27831
                //hdnUsernamestr.Value =Convert.ToString(Request.QueryString["username"]);
                hdnUsernamestr.Value  = AppHelper.HTMLCustomEncode(Convert.ToString(Request.QueryString["username"]));
                //End :Pen testing :atavaragiri :mits 27831
            }
            //Changed by Gagan for MITS 19725 : start
            if (Request.QueryString["lookup"] != null)
            {    //Pen testing :atavaragiri :mits 27831
                //hdnLookup.Value = (Convert.ToString(Request.QueryString["lookup"]);
                hdnLookup.Value = AppHelper.HTMLCustomEncode(Convert.ToString(Request.QueryString["lookup"]));
                //End :Pen testing :atavaragiri :mits 27831
            }
            else
            {
                hdnLookup.Value = "false";
            }
            //Changed by Gagan for MITS 19725 : End

            //sharishkumar Jira 6415 starts
            if (!String.IsNullOrEmpty(Request.QueryString["UserGroups"]))
            {
                sUserGroups = AppHelper.HTMLCustomEncode(Convert.ToString(Request.QueryString["UserGroups"]));
                hdnUserGroups.Value = sUserGroups;
            }
            if (!String.IsNullOrEmpty(Request.QueryString["multiselect"]))
            {
                smultiselect = AppHelper.HTMLCustomEncode(Convert.ToString(Request.QueryString["multiselect"]));
                hdnMultiSelect.Value = smultiselect;
            }
            else
            {
                hdnMultiSelect.Value = "1";
            }
            //sharishkumar Jira 6415 ends
            // npadhy Jira 6415
            if (!String.IsNullOrEmpty(Request.QueryString["commaseparator"]))
            {
                blnCommaSeparator = AppHelper.HTMLCustomEncode(Convert.ToString(Request.QueryString["commaseparator"])) == "true" ? true : false;
                hdnCommaSeparator.Value = blnCommaSeparator.ToString(); ;
            }
            else
            {
                hdnCommaSeparator.Value = "false";
            }
           

            if (!IsPostBack)
            {                
                try
                {
                    bReturnStatus = CallCWSFunction("CustomizeUserListAdaptor.GetUserList", out sreturnValue);
                    if (bReturnStatus)
                    {
                        SysUsersXmlDoc.LoadXml(sreturnValue);
                        rootElement = XElement.Parse(SysUsersXmlDoc.OuterXml);

                        //Fetch the list of System Users
                        result = from c in rootElement.XPathSelectElements("//SystemUserList/SystemUser")
                                 let xClaim = (string)Convert.ToString((c.Element("LoginName").Value))
                                 orderby xClaim
                                 select c;

                        sOptionType = rootElement.XPathSelectElement("//SystemUserList/OptionFlag").Value;

                        //Fetch the list of User Groups
                        sysUsersTypeList = SysUsersXmlDoc.SelectNodes("//UserTypes/GroupName");

                        lstUserGroups.Items.Add(new ListItem("", "0"));
                        for (int i = 0; i < sysUsersTypeList.Count; i++)
                        {
                            lstUserGroups.Items.Add(new ListItem(sysUsersTypeList[i].InnerText, (((XmlElement)sysUsersTypeList[i]).GetAttribute("groupid"))));                         
                        }                        
						// Mits 35697 starts
                        sysUserList = SysUsersXmlDoc.SelectNodes("//SystemUserList/SystemUser");

                        DataTable dtUser = ConvertXmlNodeListToDataTable(sysUserList);

                        gvUserList.DataSource = dtUser;
                        gvUserList.DataBind();
						// Mits 35697 ends
                    }
                }

                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }

     // Mits 35697 starts
        public static DataTable ConvertXmlNodeListToDataTable(XmlNodeList xnl)
        {
            DataTable dt = new DataTable();
            int TempColumn = 0;
 
            foreach (XmlNode node in xnl.Item(0).ChildNodes)
            {
                TempColumn++;
                DataColumn dc = new DataColumn(node.Name, System.Type.GetType("System.String"));
                if (dt.Columns.Contains(node.Name))
                {
                    dt.Columns.Add(dc.ColumnName = dc.ColumnName + TempColumn.ToString());
                }
                else
                {
                    dt.Columns.Add(dc);
                }
            }
 
            int ColumnsCount = dt.Columns.Count;
            for (int i = 0; i < xnl.Count; i++)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < ColumnsCount; j++)
                {
                    dr[j] = xnl.Item(i).ChildNodes[j].InnerText;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

// Mits 35697 ends
    }
}
