﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Sentinel
{
    public partial class SentinelReport : System.Web.UI.Page
    {
        private XElement oMessageElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = null;
            XElement rootElement = null;
            string sReturn = "";
            try
            {
                if (!Page.IsPostBack)
                {
                    oFDMPageDom = new XmlDocument();
                    //Preparing XML to send to service
                    oMessageElement = GetMessageTemplate();

                    //Modify XML 
                    ModifyTemplate();

                    //Calling Service to get all PreBinded Data 
                    sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                    ErrorControl1.errorDom = sReturn;
                    oFDMPageDom.LoadXml(sReturn);
                    XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                    XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                    oFDMPageDom.LoadXml(oInstanceNode.OuterXml);

                    rootElement = XElement.Parse(oFDMPageDom.OuterXml);

                    if (!ErrorControl1.errorFlag)
                    {
                        XElement oEle = rootElement.XPathSelectElement("//Sentinel/File");
                        if (oEle != null)
                        {
                            string sFileContent = oEle.Value;
                            string sFileName = oEle.Attribute("Name").Value;
                            
                            byte[] byteOrg = Convert.FromBase64String(sFileContent);

                            Response.Clear();
                            Response.Charset = "";
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-Disposition", "inline;");
                            Response.BinaryWrite(byteOrg);
                            Response.End();
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

            
           
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>97e7e805-0803-4caf-b947-90ddae85757f</Authorization> 
              <Call>
                <Function>SentinelAdaptor.GetRootCauseAnalysisReport</Function> 
              </Call>
              <Document>
                <Sentinel>
                    <EventId></EventId> 
                    <LangCode></LangCode> 
                </Sentinel>
              </Document>
            </Message>

            ");

            return oTemplate;
        }
        private void ModifyTemplate()
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/Sentinel/EventId");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("EventId");
            }

            oElement = oMessageElement.XPathSelectElement("./Document/Sentinel/LangCode");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetLanguageCode();
            }
        }
    }
}
