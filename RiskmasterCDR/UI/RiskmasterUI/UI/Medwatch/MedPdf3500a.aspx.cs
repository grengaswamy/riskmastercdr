﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.UI.pdf
{
    public partial class MedPdf3500a : System.Web.UI.Page
    {
        XElement XmlTemplate = null;
        string sreturnValue = string.Empty;
        XmlDocument XmlDoc = new XmlDocument();
        string sRequestHost = string.Empty;
        string sEventId = string.Empty;
        string sReturn = string.Empty;
        XElement rootElement = null;


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               sEventId = AppHelper.GetQueryStringValue("EventId");
                sRequestHost = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("RiskmasterUI") + 12);
                XmlTemplate = GetMessageTemPlatePDF3500A();
                //Calling Service to get all PreBinded Data 
                sReturn = AppHelper.CallCWSService(XmlTemplate.ToString());
                ErrorControl1.errorDom = sReturn;
                XmlDoc.LoadXml(sReturn);
                XmlNode oInstanceNode = XmlDoc.SelectSingleNode("/ResultMessage/Document");
                XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                XmlDoc.LoadXml(oInstanceNode.OuterXml);

                rootElement = XElement.Parse(XmlDoc.OuterXml);

                if (!ErrorControl1.errorFlag)
                {
                    XElement oEle = rootElement.XPathSelectElement("//MedWatch/File");
                    if (oEle != null)
                    {
                        string sFileContent = oEle.Value;
                        byte[] byteOrg = Convert.FromBase64String(sFileContent);

                        Response.Clear();
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.fdf";
                        Response.AddHeader("Content-Disposition", "inline;");
                        Response.BinaryWrite(byteOrg);
                        Response.End();
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            } 
        }

        private XElement GetMessageTemPlatePDF3500A()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization></Authorization>");
            sXml.Append("<Call><Function>FROIAdaptor.MedPdf3500a</Function></Call><Document><MedPDf3500><RequestHost>");
            sXml.Append(sRequestHost);
            sXml.Append("</RequestHost>");
            sXml.Append("<action /><value /><FunctionToCall /><EventId>");
            sXml.Append(sEventId);
            sXml.Append("</EventId><NoOfReports /></MedPDf3500></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }
    }
}
