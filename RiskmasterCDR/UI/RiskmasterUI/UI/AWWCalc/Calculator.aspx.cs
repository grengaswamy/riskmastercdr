﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Globalization;
using MultiCurrencyCustomControl;

namespace Riskmaster.UI.AWWCalc
{
    public partial class Calculator : NonFDMBasePageCWS
    {
        public IEnumerable resultWeeks = null;        
        public XElement rootElement = null;
        public XmlDocument awwCalcXmlDoc = new XmlDocument();
        public XmlElement objEleWeeks = null;
        public string strFormOption = "";
        public string strStateCode = "";
        public string strFormTitle = "";
        string strClaimId = "";
        bool blnReturnStatus = false;
        string strReturnValue = "";

        protected void Page_Load(object sender, EventArgs e)
        {            
            XElement xmlAWWTemplate = null;
            strClaimId = AppHelper.GetQueryStringValue("claimid");

            if (!IsPostBack)
            {
                try
                {
                    xmlAWWTemplate = GetAWWMessageTemplate(strClaimId);
                    blnReturnStatus = CallCWSFunction("AWWCalcAdaptor.Get", out strReturnValue, xmlAWWTemplate);

                    if (blnReturnStatus)
                    {
                        awwCalcXmlDoc.LoadXml(strReturnValue);
                        rootElement = XElement.Parse(awwCalcXmlDoc.OuterXml);
                        resultWeeks = from objWeekNodes in rootElement.XPathSelectElement("//Weeks").Nodes()
                                      select objWeekNodes;

                        strFormTitle = rootElement.XPathSelectElement("//FormTitle").Value;
                        strStateCode = rootElement.XPathSelectElement("//StateCode").Value;
                        strFormOption = rootElement.XPathSelectElement("//FormOption").Value;
                        Overtime.AmountInString = rootElement.XPathSelectElement("//Overtime").Value;  //Aman Multi Currency --Start
                        DailyEarn.AmountInString = rootElement.XPathSelectElement("//DailyEarn").Value;  
                        ConstantWage.AmountInString = rootElement.XPathSelectElement("//ConstantWage").Value;
                        LastWorkWeekDate.Value = AppHelper.GetUIDate(rootElement.XPathSelectElement("//LastWorkWeekDate").Value);
                        txtBonuses.AmountInString = rootElement.XPathSelectElement("//Bonuses").Value;
                        txtWeeksLastYear.Value = rootElement.XPathSelectElement("//EmployedWeeks").Value;
                        LastAWW.AmountInString = rootElement.XPathSelectElement("//LastAWW").Value;
                        AWW.AmountInString = rootElement.XPathSelectElement("//AWW").Value;
                        FormOption.Value = rootElement.XPathSelectElement("//FormOption").Value;
                        StateCode.Value = rootElement.XPathSelectElement("//StateCode").Value;
                        IncludeZero.Value = rootElement.XPathSelectElement("//IncludeZero").Value;
                        IfClose.Value = rootElement.XPathSelectElement("//IfClose").Value;
                        CompensationRate.Value = rootElement.XPathSelectElement("//CompensationRate").Value;
                        WorkDaysPerWeek.Value = rootElement.XPathSelectElement("//WorkDaysPerWeek").Value;
                        currencytype.Text = rootElement.XPathSelectElement("//BaseCurrencyType").Value;   //Aman Multi Currency
                        InitializeCulture();
                    }                    
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
            else
            {
                if(this.Action.Value == "Save")
                {
                    try
                    {
                        CallAWWCalcAdaptorSave();
                    } // try
                    catch (Exception ee)
                    {
                        ErrorHelper.logErrors(ee);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ee, BusinessAdaptorErrorType.SystemError);
                        ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                    }
                } // if

            } // else

        }

        private XElement GetAWWMessageTemplate(string strClaimId)
        {
            StringBuilder strAWWMessage = new StringBuilder("<Message>");
            XElement xmlAWWMsgTemplate = null;

            strAWWMessage = strAWWMessage.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            strAWWMessage = strAWWMessage.Append("<Call><Function>AWWCalcAdaptor.Get</Function></Call><Document><AWWCalculator><ClaimId>");
            strAWWMessage = strAWWMessage.Append(strClaimId);
            strAWWMessage = strAWWMessage.Append("</ClaimId><WorkDaysPerWeek /><DailyEarn /><ConstantWage /><Overtime /><EmployedWeeks />");
            strAWWMessage = strAWWMessage.Append("<LastWorkWeekDate /><IncludeZero /><Bonuses /><AWW /><LastAWW /><StateCode /><Weeks />");
            strAWWMessage = strAWWMessage.Append("<BaseCurrencyType />");
            strAWWMessage = strAWWMessage.Append("<FormTitle /><FormOption /><CloseFlag /><IfClose /><CompensationRate />");
            strAWWMessage = strAWWMessage.Append("</AWWCalculator></Document></Message>");

            xmlAWWMsgTemplate = XElement.Parse(strAWWMessage.ToString());

            return xmlAWWMsgTemplate;
        }

        protected void btnReturnAWW_Click(object sender, EventArgs e)
        {
            CallAWWCalcAdaptorSave();
        }
        /// <summary>
        /// 
        /// </summary>
        private void CallAWWCalcAdaptorSave()
        {
            int intWeekPerCol = 0;
            int intCounterWeek = 0;
            XElement xmlAWWTemplate = null;
            StringBuilder strAWWMessage = null;
            List<string> lstWeekElements = new List<string>();
            List<string> lstWageElements = new List<string>();
            List<string> lstDaysElements = new List<string>();
            List<string> lstHrsElements = new List<string>();
            List<string> lstWeekTitleElements = new List<string>();

            lstWeekElements = WeekName.Value.Split(',').ToList();
            lstWageElements = WeekWage.Value.Split('|').ToList();
            lstDaysElements = WeekDays.Value.Split('|').ToList();
            lstHrsElements = WeekHours.Value.Split('|').ToList();
            lstWeekTitleElements = WeekTitle.Value.Split(',').ToList();
            Action.Value = "Save";

            if (WeekCount.Value != "" && ColCount.Value != "")
            {
                intWeekPerCol = Convert.ToInt32((WeekCount.Value)) / Convert.ToInt32(ColCount.Value);
            }

            strAWWMessage = new StringBuilder("<Message>");
            strAWWMessage = strAWWMessage.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            strAWWMessage = strAWWMessage.Append("<Call><Function>AWWCalcAdaptor.Save</Function></Call><Document><AWWCalculator><ClaimId>");
            strAWWMessage = strAWWMessage.Append(strClaimId);
            strAWWMessage = strAWWMessage.Append("</ClaimId><WorkDaysPerWeek>");
            strAWWMessage = strAWWMessage.Append(WorkDaysPerWeek.Value);
            strAWWMessage = strAWWMessage.Append("</WorkDaysPerWeek>");
            strAWWMessage = strAWWMessage.Append("<DailyEarn>");
            strAWWMessage = strAWWMessage.Append(DailyEarn.AmountInString);   //Aman Multi Currency
            strAWWMessage = strAWWMessage.Append("</DailyEarn>");
            strAWWMessage = strAWWMessage.Append("<ConstantWage>");
            strAWWMessage = strAWWMessage.Append(ConstantWage.AmountInString);
            strAWWMessage = strAWWMessage.Append("</ConstantWage>");
            strAWWMessage = strAWWMessage.Append("<Overtime>");
            strAWWMessage = strAWWMessage.Append(Overtime.AmountInString);  //Aman Multi Currency
            strAWWMessage = strAWWMessage.Append("</Overtime>");
            strAWWMessage = strAWWMessage.Append("<EmployedWeeks>");
            strAWWMessage = strAWWMessage.Append(txtWeeksLastYear.Value);
            strAWWMessage = strAWWMessage.Append("</EmployedWeeks>");
            strAWWMessage = strAWWMessage.Append("<LastWorkWeekDate>");
            strAWWMessage = strAWWMessage.Append(LastWorkWeekDate.Value);
            strAWWMessage = strAWWMessage.Append("</LastWorkWeekDate>");
            strAWWMessage = strAWWMessage.Append("<IncludeZero>");
            strAWWMessage = strAWWMessage.Append(IncludeZero.Value);
            strAWWMessage = strAWWMessage.Append("</IncludeZero>");
            strAWWMessage = strAWWMessage.Append("<Bonuses>");
            strAWWMessage = strAWWMessage.Append(txtBonuses.AmountInString);  //Aman Multi Currency
            strAWWMessage = strAWWMessage.Append("</Bonuses>");
            strAWWMessage = strAWWMessage.Append("<AWW>");
            strAWWMessage = strAWWMessage.Append(AWW.AmountInString);    //Aman Multi Currency
            strAWWMessage = strAWWMessage.Append("</AWW>");
            strAWWMessage = strAWWMessage.Append("<LastAWW>");
            strAWWMessage = strAWWMessage.Append(LastAWW.AmountInString);     //Aman Multi Currency
            strAWWMessage = strAWWMessage.Append("</LastAWW>");
            strAWWMessage = strAWWMessage.Append("<StateCode>");
            strAWWMessage = strAWWMessage.Append(StateCode.Value);
            strAWWMessage = strAWWMessage.Append("</StateCode>");
            strAWWMessage = strAWWMessage.Append("<Weeks>");

            switch (FormOption.Value)
            {
                case "61":
                    for (int count = 0; count < Convert.ToInt32(WeekCount.Value); count++)
                    {
                        strAWWMessage.Append("<Week" + " name=" + '"' + lstWeekElements[intCounterWeek] + '"' + " days=" + '"' + lstDaysElements[intCounterWeek] + '"' + " hours=" + '"' + lstHrsElements[intCounterWeek] + '"' + " title=" + '"' + lstWeekTitleElements[intCounterWeek] + '"' + " wage=" + '"' + lstWageElements[intCounterWeek] + '"' + " />");
                        intCounterWeek = intCounterWeek + 1;
                    }
                    break;
                case "149":
                    for (int count = 0; count < Convert.ToInt32(WeekCount.Value); count++)
                    {
                        strAWWMessage.Append("<Week" + " name=" + '"' + lstWeekElements[intCounterWeek] + '"' + " days=" + '"' + "" + '"' + " hours=" + '"' + lstHrsElements[intCounterWeek] + '"' + " title=" + '"' + lstWeekTitleElements[intCounterWeek] + '"' + " wage=" + '"' + lstWageElements[intCounterWeek] + '"' + " />");
                        intCounterWeek = intCounterWeek + 1;
                    }
                    break;
                case "529":
                    if (StateCode.Value == "NY")
                    {
                        for (int counterCol = 0; counterCol < Convert.ToInt32(ColCount.Value); counterCol++)
                        {
                            strAWWMessage.Append("<Column>");
                            for (int count = 0; count < intWeekPerCol; count++)
                            {
                                strAWWMessage.Append("<Week" + " name=" + '"' + lstWeekElements[intCounterWeek] + '"' + " days=" + '"' + lstDaysElements[intCounterWeek] + '"' + " hours=" + '"' + "" + '"' + " title=" + '"' + lstWeekTitleElements[intCounterWeek] + '"' + " wage=" + '"' + lstWageElements[intCounterWeek] + '"' + " />");
                                intCounterWeek = intCounterWeek + 1;
                            }
                            strAWWMessage.Append("</Column>");
                        }
                    }
                    if (StateCode.Value == "MI")
                    {
                        intWeekPerCol = Convert.ToInt32((WeekCount.Value)) / Convert.ToInt32(ColCount.Value);
                        for (int counterCol = 0; counterCol < Convert.ToInt32(ColCount.Value); counterCol++)
                        {
                            strAWWMessage.Append("<Column>");
                            for (int count = 0; count < intWeekPerCol; count++)
                            {
                                strAWWMessage.Append("<Week" + " name=" + '"' + lstWeekElements[intCounterWeek] + '"' + " days=" + '"' + "" + '"' + " hours=" + '"' + "" + '"' + " title=" + '"' + "" + '"' + " wage=" + '"' + lstWageElements[intCounterWeek] + '"' + " />");
                                intCounterWeek = intCounterWeek + 1;
                            }

                            strAWWMessage.Append("</Column>");
                        }
                    } // if
                    break;
                default:
                    intWeekPerCol = Convert.ToInt32((WeekCount.Value)) / Convert.ToInt32(ColCount.Value);
                    for (int counterCol = 0; counterCol < Convert.ToInt32(ColCount.Value); counterCol++)
                    {
                        strAWWMessage.Append("<Column>");
                        for (int count = 0; count < intWeekPerCol; count++)
                        {
                            strAWWMessage.Append("<Week" + " name=" + '"' + lstWeekElements[intCounterWeek] + '"' + " days=" + '"' + "" + '"' + " hours=" + '"' + "" + '"' + " title=" + '"' + "" + '"' + " wage=" + '"' + lstWageElements[intCounterWeek] + '"' + " />");
                            intCounterWeek = intCounterWeek + 1;
                        }

                        strAWWMessage.Append("</Column>");
                    }
                    //Aman MITS 27893 --Start
                    if (intCounterWeek < Convert.ToInt32(WeekCount.Value))
                    {
                        for (int count = intCounterWeek; count < Convert.ToInt32(WeekCount.Value); count++)
                        {
                            strAWWMessage.Append("<Week" + " name=" + '"' + lstWeekElements[count] + '"' + " days=" + '"' + "" + '"' + " hours=" + '"' + "" + '"' + " title=" + '"' + "" + '"' + " wage=" + '"' + lstWageElements[count] + '"' + " />");
                            //intCounterWeek = intCounterWeek + 1;
                        }
                    }
                    //Aman MITS 27893 --End
                 break;
            }

            strAWWMessage = strAWWMessage.Append("</Weeks>");
            strAWWMessage = strAWWMessage.Append("<FormTitle>");
            strAWWMessage = strAWWMessage.Append("</FormTitle>");
            strAWWMessage = strAWWMessage.Append("<FormOption>");
            strAWWMessage = strAWWMessage.Append("</FormOption>");
            strAWWMessage = strAWWMessage.Append("<CloseFlag/>");
            strAWWMessage = strAWWMessage.Append("<IfClose>");
            strAWWMessage = strAWWMessage.Append(IfClose.Value);
            strAWWMessage = strAWWMessage.Append("</IfClose>");
            strAWWMessage = strAWWMessage.Append("<CompensationRate>");
            strAWWMessage = strAWWMessage.Append(CompensationRate.Value);
            strAWWMessage = strAWWMessage.Append("</CompensationRate>");
            strAWWMessage = strAWWMessage.Append("</AWWCalculator></Document></Message>");

            xmlAWWTemplate = XElement.Parse(strAWWMessage.ToString());
            blnReturnStatus = CallCWSFunction("AWWCalcAdaptor.Save", out strReturnValue, xmlAWWTemplate);

            if (blnReturnStatus)
            {
                IfClose.Value = "True";

                //Add  by igupta3 for mits:32200 Start
                awwCalcXmlDoc.LoadXml(strReturnValue);
                if (awwCalcXmlDoc.SelectSingleNode("//AWWCalculator/CompensationRate") != null)
                {
                    hdnCompData.Text = awwCalcXmlDoc.SelectSingleNode("//AWWCalculator/CompensationRate").InnerText;
                }
                if (awwCalcXmlDoc.SelectSingleNode("//AWWCalculator/AWW") != null)
                {
                    hdnAWWData.Text = awwCalcXmlDoc.SelectSingleNode("//AWWCalculator/AWW").InnerText;
                }
                //Add  by igupta3 for mits:32200 End
            }

        } // method: CallAWWCalcAdaptorSave
        //Aman Mult Currency --Start
        protected override void InitializeCulture()
        {
            if (Request.Form["currencytype"] != null)
            {
                if (!string.IsNullOrEmpty(Request.Form["currencytype"]))
                {
                    Culture = Request.Form["currencytype"];
                    UICulture = Request.Form["currencytype"];
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.Form["currencytype"]);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(Request.Form["currencytype"]);
                }
            }
            else if ((currencytype != null) && (!string.IsNullOrEmpty(currencytype.Text)))
            {
                Culture = currencytype.Text;
                UICulture = currencytype.Text;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(currencytype.Text);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(currencytype.Text);
            }
          //  base.InitializeCulture();
        }
        //Aman Mult Currency --End

    }
}
