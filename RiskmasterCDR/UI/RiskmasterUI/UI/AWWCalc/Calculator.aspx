﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculator.aspx.cs" Inherits="Riskmaster.UI.AWWCalc.Calculator" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl" TagPrefix="mc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title><%=strFormTitle%></title>
  <link rel="stylesheet" href = "../../Content/rmnet.css" type="text/css" />
  <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
  <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
  <script src="../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
  <script src="../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
  <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
  <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
  <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
  <script type="text/javascript" src="../../Scripts/calendar-alias.js"></script>
  <script language="JavaScript" src="../../Scripts/AWW.js" type="text/javascript"></script>
  <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">{var i;}</script>

<%--Jquery Calender--%>
<link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
<%--End Jquery calender--%>
</head>

<!--Igupta3 Mits :32200-->
<body onload="AWWCalc_PageLoad();" onunload="RefreshAWWData();">
<form id="frmData" runat="server">
<script language="JavaScript" type="text/javascript"></script>   
 <%
   if(strStateCode != "AZ" && strStateCode != "NV" && strStateCode != "WY") 
   { 
        if(strStateCode != "WA" && strStateCode != "VT")  
        {%> 
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>Wrk Days/Wk</td>
            <td></td>
            <td> Daily Earn</td>
            <td></td>
            <td>Constant Wage</td>
            <td></td>                
            <td align="right" colspan="3">
                <input type="checkbox" name="IncludeZero_Chk" onchange="OnCheckChange();ValueChange(this);" id="IncludeZero_Chk" runat="server"/>Include Zero Amts in Calc      							
            </td>
            <td align="center" colspan="3">
                Overtime: <mc:CurrencyTextbox runat="server" id="Overtime" onchange="return CurrencyValueChange(this);" size = "8" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" name="WorkDaysPerWeek" id="WorkDaysPerWeek" size="8" onblur="WrkDysLostFocus(this);" onkeyup="return setDataChangedKD();" runat="server"/>
            </td>
            <td>X</td>
            <td>      	        
                <mc:CurrencyTextbox runat="server" id="DailyEarn" onblur="DailyEarnLostFocus(this);" onchange="CurrencyValueChange(this);" size = "8"/>
                </td>
            <td>=</td>
            <td>     	       
                <mc:CurrencyTextbox runat="server" id="ConstantWage" onchange="CurrencyValueChange(this);" size = "8"/></td>
            <td><input type="button" class="button" id="btnSet" onclick="AWWCalc_SetAll()" value="Set All"/></td>
            <td align="right" colspan="2">
                Last Work Week:
                <input type="text" name="LastWorkWeekDate" size="8" runat="server" onblur="dateLostFocus(this.id);" onchange="AWWCalc_OnDateChange();ValueChange(this);" id="LastWorkWeekDate"/>
               <%-- <input type="button" class="EllipsisControl" id="btnLastWorkWeekDate" value="..."/>--%>
                <script type="text/javascript">
                    $(function () {
                        $("#LastWorkWeekDate").datepicker({
                            showOn: "button",
                            buttonImage: "../../Images/calendar.gif",
                            buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        });
                    });
            </script>
<%--                <script type="text/javascript">
	            Calendar.setup(
			    {
			        inputField : "LastWorkWeekDate",
			        ifFormat : "%m/%d/%Y",
			        button : "btnLastWorkWeekDate"
			    }
			    );--%>
			</script>
	    </td>	   
        <td colspan="2" align="right">All Bonuses:        
        <mc:CurrencyTextbox runat="server" id="txtBonuses" onchange="return CurrencyValueChange(this);" size = "8" /></td>
        <td align="center">Weeks in Last Year:<input type="text" name="txtWeeksLastYear" id="txtWeeksLastYear" onclick="" onblur="WeeksLostFocus(this);" runat="server"/></td>   
     </tr>
   </table>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
   <tr>
     <td class="group" colspan="2"></td>
    </tr>
   </table> 
   <%}%>
   
   <%if(strFormOption == "52" || strFormOption == "13" || strFormOption == "26" || strFormOption == "0" || strFormOption == "12" || strFormOption == "8" || strFormOption == "521" || strFormOption == "523" || strFormOption == "524" || strFormOption == "527" || strFormOption == "129" || strStateCode != "AZ")
   {%>       
   <table border="0" cellspacing="0" cellpadding="0" width="100%">        
   <tr>
      <%
           int colcounter = 0;           
           int weekcounter = 0;
           foreach (XmlNode objNodeCol in awwCalcXmlDoc.SelectNodes("//Column"))
           {
              ColCount.Value = Convert.ToString(colcounter + 1);
       %>
       <td>
       <table align="right">  
       <%foreach (XmlNode objNode in objNodeCol.ChildNodes)
         {
             WeekCount.Value = Convert.ToString(weekcounter + 1);
             objEleWeeks = (XmlElement)objNode;              
       %>    
       <tr>
             <td>           
              <%lbl.ID = "lbl" + objEleWeeks.GetAttribute("name");%>
              <asp:Label runat="server" ID="lbl"></asp:Label>     
              </td>
              <td></td>
              <td>
              <% WorkWeek.ID = objEleWeeks.GetAttribute("name");
                 WorkWeek.AmountInString = objEleWeeks.GetAttribute("wage");
                 if (WeekCount.Value == "1")
                     WeekName.Value = objEleWeeks.GetAttribute("name");
                 else
                     WeekName.Value = WeekName.Value + "," + objEleWeeks.GetAttribute("name");             
              %>             
             <mc:CurrencyTextbox runat="server" id="WorkWeek" onchange="return CurrencyValueChange(this);" size="8"/>        
           </td>
        </tr>
        <% weekcounter = weekcounter + 1;
         }%>
        </table>    
        <% colcounter = colcounter + 1;
        }%>  
       </td> 
       </tr>
      </table>  
    <%}%>    
   <%if(strFormOption == "61" || strFormOption == "149")
   {%>    
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr></tr>
   </table>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <table align="center">
      <tr>
       <td></td>
       <td></td>
       <td></td>
       <td>
       </td>
       <td>Earnings</td>
      <%if (strFormOption == "61")
        {%> <td>Days Worked</td><%}
      %>
      <td>Hours Worked</td>
      </tr>
      <%int weekcounter = 0; string sWeekName =""; foreach (XElement item in resultWeeks)            
      {%>      
      <tr>
       <td align="right"><%=item.Attribute("title").Value%></td>       
       <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td align="right">                
              <%            
                sWeekName = item.Attribute("name").Value;
                labelfl.ID = "lbl" + sWeekName ;
                WrkWeek.ID = sWeekName;
                WrkWeek.AmountInString = item.Attribute("wage").Value ;
                if (strFormOption == "61")
                {
                    WrkDays.ID = "dys" + sWeekName;
                    WrkDays.AmountInString = item.Attribute("days").Value;
                }
                                    
                WrkHrs.ID = "hrs" + sWeekName;
                WrkHrs.AmountInString = item.Attribute("hours").Value ;
            
                WeekCount.Value = Convert.ToString(weekcounter + 1);

                if (WeekCount.Value == "1")
                {
                    WeekName.Value = item.Attribute("name").Value ;
                    WeekTitle.Value = item.Attribute("title").Value;                                                       
                }
                else
                {
                    WeekName.Value = WeekName.Value + "," + item.Attribute("name").Value ;
                    WeekTitle.Value = WeekName.Value + "," + item.Attribute("title").Value;
                }            
              %>
              <asp:Label runat="server" ID="labelfl"></asp:Label>             
       &nbsp;&nbsp;        											
       </td>
       <td></td>
       <td>             
         <mc:CurrencyTextbox runat="server" id="WrkWeek" onchange="return CurrencyValueChange(this);" size = "8" />
       </td>
       <%if (strFormOption == "61")
       {%>
       <td>       
        <mc:CurrencyTextbox runat="server" id="WrkDays" onchange="return CurrencyValueChange(this);" size = "8" />
       </td>
       <%}%>
       <td>       
        <mc:CurrencyTextbox runat="server" id="WrkHrs" onchange="return CurrencyValueChange(this);" size = "8" />
       </td>
      </tr>                  
   <%
     weekcounter = weekcounter + 1;
   }%>    
    </table>
    </tr>
   </table> 
  <%}%> 
   
  <%
  if (strFormOption == "529" && strStateCode == "NY")
  {%>       
   <table border="0" cellspacing="0" cellpadding="0" width="100%">        
   <tr>
    <%      
       int colcounter = 0;     
       int weekcounter = 0;
        foreach (XmlNode objNodeCol in awwCalcXmlDoc.SelectNodes("//Column"))
        {
           ColCount.Value = Convert.ToString(colcounter + 1);
       %>
       <td>
       <table align="center">  
       <tr>
	       <td>Week</td>
           <td>P/E Date</td>
	       <td/>
	       <td>Earnings</td>
	       <td># Of Days</td>
	   </tr>
       <%foreach (XmlNode objNode in objNodeCol.ChildNodes)
       {
             WeekCount.Value = Convert.ToString(weekcounter + 1);
             objEleWeeks = (XmlElement)objNode;              
       %>    
       <tr>
             <td align="left">
			     <%=objEleWeeks.GetAttribute("title")%>&#160;&#160;&#160;&#160;&#160;
			 </td>
             <td align="right">         
              <%labelny.ID = "lbl" + objEleWeeks.GetAttribute("name");%>
              <asp:Label runat="server" ID="labelny"></asp:Label>     
              </td>
              <td></td>
              <td>
              <% WrkWeekNY.ID = objEleWeeks.GetAttribute("name");
                 WrkWeekNY.AmountInString = objEleWeeks.GetAttribute("wage");
                 WrkDaysNY.ID = "dys" + objEleWeeks.GetAttribute("name");
                 WrkDaysNY.AmountInString = objEleWeeks.GetAttribute("days");

                 if (WeekCount.Value == "1")
                 {
                     WeekName.Value = objEleWeeks.GetAttribute("name");
                     WeekTitle.Value = objEleWeeks.GetAttribute("title");             
                 }
                 else
                 {
                     WeekName.Value = WeekName.Value + "," + objEleWeeks.GetAttribute("name");
                     WeekTitle.Value = WeekTitle.Value + "," + objEleWeeks.GetAttribute("title");                     
                 }
              %>                                  
             <mc:CurrencyTextbox runat="server" id="WrkWeekNY" onchange="return CurrencyValueChange(this);" size = "8" />
           </td>
           <td>            
             <mc:CurrencyTextbox runat="server" id="WrkDaysNY" onchange="return CurrencyValueChange(this);" size = "8" />
           </td>           
        </tr>
        <% weekcounter = weekcounter + 1;
         }%>
        </table>    
        <% colcounter = colcounter + 1;
          }%>  
       </td>
       </tr>   
      </table>  
    <%}%>  
 <%}%>     
 <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tblDefaultWeeks" style="display:none;">        
   <tr>
      <%
           int colcount = 0;           
           int weekcount = 0;
           foreach (XmlNode objNodeCol in awwCalcXmlDoc.SelectNodes("//Column"))
           {
               ColCount.Value = Convert.ToString(colcount + 1);
       %>
       <td>
       <table align="right">  
       <%foreach (XmlNode objNode in objNodeCol.ChildNodes)
         {
             WeekCount.Value = Convert.ToString(weekcount + 1);
             objEleWeeks = (XmlElement)objNode;              
       %>    
       <tr>
             <td>           
              <%lbldefaultweeks.ID = "lbl" + objEleWeeks.GetAttribute("name");%>
              <asp:Label runat="server" ID="lbldefaultweeks"></asp:Label>     
              </td>
              <td></td>
              <td>
              <% WorkWeekDefault.ID = objEleWeeks.GetAttribute("name");
                 WorkWeekDefault.AmountInString = objEleWeeks.GetAttribute("wage");
                 if (WeekCount.Value == "1")
                     WeekName.Value = objEleWeeks.GetAttribute("name");
                 else
                     WeekName.Value = WeekName.Value + "," + objEleWeeks.GetAttribute("name");             
              %>             
             <mc:CurrencyTextbox runat="server" id="WorkWeekDefault" onchange="return CurrencyValueChange(this);" size = "8" />
           </td>
        </tr>
        <% weekcount = weekcount + 1;
         }%>
        </table>    
        <% colcount = colcount + 1;
        }%>  
       </td> 
       </tr>
 </table>    
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <td class="group" colspan="2"></td>
    </tr>
   </table>
 <tr></tr>
 <table border="0" cellspacing="0" cellpadding="0" width="100%">
   <tr>
    <td>
         Last AW:<mc:CurrencyTextbox runat="server" id="LastAWW" onchange="return CurrencyValueChange(this);" size = "8" /></td>
    <td>
      	 AW:<mc:CurrencyTextbox runat="server" id="AWW" onchange="return CurrencyValueChange(this);" size = "8" /></td>
    <td><input type="button" class="button" id="btnCalc" onclick="AWWCalc_Calculate();" value="Calculate"/></td>
    <td><input type="button" class="button" id="btnClear" onclick="return IfClear();" value="Clear"/></td>
    <td>     
        <asp:Button runat="server" class="button" ID="btnReturnAWW" 
             OnClientClick="return AWWCalc_Save();" onclick="btnReturnAWW_Click" Text="Return AWW" />     
    </td>
    <td><input type="button" class="button" id="btnClose" onclick="AskSave();" value="Close"/></td>
   </tr>
  </table> 

   <input type="text" name="FormOption" id="FormOption" style="display:none" runat="server"/>
   <input type="text" name="IncludeZero" id="IncludeZero" style="display:none" runat="server"/>
   <input type="text" name="StateCode" id="StateCode" style="display:none" runat="server"/>
   <input type="text" name="Action" id="Action" style="display:none" runat="server"/>
   <input type="hidden" name="IfClose" id="IfClose" runat="server"/>
   <input type="hidden" name="CompensationRate" value="0" id="CompensationRate" runat="server"/>
   <input id="SysFormIdName" onclick="" style="display:none" />
   <input id="SysFormPIdName" onclick="" style="display:none"/>
   <input id="SysFormName" onclick="" style="display:none"/>
   <input id="SysCmd" onclick="" style="display:none"/>
   <input id="SysRequired" onclick="" style="display:none"/>
   <input id="ColCount" type="text" style="display:none" runat="server"/>
   <input id="WeekCount" type="text" style="display:none" runat="server"/>   
   <input id="WeekDays" type="text" style="display:none" runat="server"/>   
   <input id="WeekHours" type="text" style="display:none" runat="server"/>   
   <input id="WeekName" type="text" style="display:none" runat="server"/>   
   <input id="WeekWage" type="text" style="display:none" runat="server"/>   
   <input id="WeekTitle" type="text" style="display:none" runat="server"/>   
   <uc1:ErrorControl ID="ErrorControl" runat="server" /> 		
   <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="_" />	  

   <asp:TextBox style="display:none" runat="server" name="hdnAWWData" id="hdnAWWData" Text="" /><!-- Add by igupta3 for mits:32200-->   
   <asp:TextBox style="display:none" runat="server" name="hdnCompData" id="hdnCompData" Text="" />
   <asp:TextBox style="display:none" runat="server" name="hdnCalcbutton" id="hdnCalcbutton" Text="" />

   <asp:TextBox style="display:none" id="currencytype" runat="server" />        <!-- Aman Multi Currency -->   
</form>
</body>
</html>
