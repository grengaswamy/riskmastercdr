﻿using System;
using System.Data;
using System.Security;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.TAndE
{
    public partial class TimeAndExpense : NonFDMBasePageCWS
    {
        #region variables
        private string sRateTable   = String.Empty;
        private string sRateTableId = String.Empty;

        public enum gridviewColumns  { billit, vendor, invoicenumber, invoicedate, invoiceamt, billamt, gvstatus, img, id, transid, payeeid, hinvoiceamt, msg, hstatus };
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus   = false;
            string sClaimId      = String.Empty;
            string sClaimantEid  = String.Empty;
            string sBilledVoid   = String.Empty;
            string sreturnValue  = String.Empty;
            string sUnitId       = String.Empty;
            XElement oMsgElement = null;

            if (String.IsNullOrEmpty(FrmName.Text))
            {
                FrmName.Text = "tandesummery";
            }//if

            //Read the values from query string
            sClaimId     = AppHelper.GetQueryStringValue("claimid");
            sRateTable   = AppHelper.GetQueryStringValue("RateTable");//MITS 15559 by MJP
            sRateTableId = AppHelper.GetQueryStringValue("RateTableId");//MITS 15559 by MJP

            //MITS 16608 - start - MJP
            sClaimantEid      = AppHelper.GetQueryStringValue("ClaimantEid");
            ClaimantEid.Value = sClaimantEid;
            sUnitId           = AppHelper.GetQueryStringValue("UnitId");
            UnitId.Value      = sUnitId;
            //MITS 16607 - end - MJP

            // MITS 15872 - MAC
            // -- START --
            string[] aRateTableId;
            if (sRateTableId != "" && (aRateTableId = sRateTableId.Split(',')).Length>1)
            {
                sRateTableId = aRateTableId[0];
            }
            // -- END --

            //Read the values from the form if nothing was parsed from the query
            sBilledVoid = AppHelper.GetFormValue("billed_void");

            if (!String.IsNullOrEmpty(sClaimId) && sClaimId != "0")
            {
                TableId1.Text = sClaimId;
            }//if
            else
            {
                //TODO: throw error if no claim id is found?
            }//else

            chkprintTE.Attributes.Add("onclick", "return checkVal();");

            if (!IsPostBack)
            {
                try
                {
                    oMsgElement   = GetMessageTemplate("GetXmlData");
                    CallCWSFunction("TimeExpenseAdaptor.GetXmlData", out sreturnValue, oMsgElement);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);

                    if (bReturnStatus)
                    {
                        BindGridData(sreturnValue);
                    }//if
                }//try
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch
            }//if
            else if (sBilledVoid == "1")
            {
                billed_void.Value     = String.Empty;
                InvoiceIds.Text       = hdnInvId.Value;
                AllowClosedClaim.Text = allowclsclm.Value;

                //TODO: for this to work as false statement, a MITS needs to be created for new functionality
                IsContinue.Text = "true";

                //TODO: assign from hidden variables rather then hardcode
                AllowClosedClaim.Text = "true";

                try
                {
                    oMsgElement = GetMessageTemplate("BillIt");
                    CallCWSFunction("TimeExpenseAdaptor.BillIt", out sreturnValue, oMsgElement);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);
                }//try
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch

                if (bReturnStatus)
                {
                    Response.Redirect(Page.Request.Url.ToString(), true);
                }//if
                else
                {
                    //CallCWSFunction("TimeExpenseAdaptor.GetXmlData", out sreturnValue, GetMessageTemplate("GetXmlData"));
                    //BindGridData(sreturnValue);

                    //throw new RMAppException(GetMyError(objFundManager.Confirmation(0), 2, m_sInvoiceIds, objInvoice.ClaimId, 1));
                    //throw new RMAppException(Globalization.GetString("InvoiceDriver.SetBill.ErrorSet"), p_objException);
                    //Duplicate Payment *4/13/2009|0000188|Safeway|No| - |Expert Witness Fees|$900.00|$900.00|GC6666000114|
                }//else
            }//else if
            else if (sBilledVoid == "0")
            {
                billed_void.Value = String.Empty;
                InvoiceIds.Text   = hdnInvId.Value;
                Status.Text       = h_status.Value;

                //TODO: for this to work as false statement, a MITS needs to be created for new functionality
                IsContinue.Text = "true";

                try
                {
                    oMsgElement = GetMessageTemplate("VoidIt");
                    CallCWSFunction("TimeExpenseAdaptor.VoidIt", out sreturnValue, oMsgElement);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);
                }//try
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch

                if (bReturnStatus)
                {
                    Response.Redirect(Page.Request.Url.ToString(), true);
                }//if
                else
                {
                    //TODO: don't redirect, but rather show the error
                }//else
            }//else if
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        private void BindGridData(string data)
        {
            string sClaimNum       = String.Empty;
            string sBilledTotal    = String.Empty;
            string sInvoiceTotal   = String.Empty;
            StringBuilder sbOnload = new StringBuilder();
            DataSet dsTandE        = null;
            DataSet dsTandEData    = null;
            DataTable tandedataDT  = null;
            XmlDocument xdTandE    = new XmlDocument();

            //XElement xmlTandESummary = XElement.Parse(data);

            try
            {
                //fill xmldocument from the string xml
                xdTandE.LoadXml(data);

                //fill xmlnode with a single node from the xmldocument
                XmlNode xnTandE      = xdTandE.SelectSingleNode("//Document/TandE");
                XmlNodeList xnlTandE = xnTandE.ChildNodes;

                //removes all the child nodes from the main t&e element
                int iChildCount = xnlTandE.Count;
                for (int ii = 0; ii < iChildCount; ii++)
                {
                    xnTandE.RemoveChild(xnlTandE[0]);
                }//for

                //fill dataset with xmlnode object
                dsTandE = new DataSet();
                dsTandE.ReadXml(new XmlNodeReader(xnTandE));

                //fill xmldocument from the string xml
                XmlDocument xdTandEData = new XmlDocument();
                xdTandEData.LoadXml(data);

                //fill xmlnode with a single node from the xmldocument
                XmlNode xnTandEData = xdTandEData.SelectSingleNode("//Document/TandE");

                //fill dataset with xmlnode object
                dsTandEData = new DataSet();
                dsTandEData.ReadXml(new XmlNodeReader(xnTandEData));

                //assign values from the main t&e element to the hidden variables
                claimid.Value = xnTandE.Attributes["claimid"].Value;

                //if no claim is found, do not continue any further and return
                if (String.IsNullOrEmpty(claimid.Value) || claimid.Value == "0")
                {
                    return;
                }//if

                //Start MITS 15559 by MJP
                if ((!String.IsNullOrEmpty(sRateTable)) && 
                    (!String.IsNullOrEmpty(sRateTableId)) &&
                    (String.IsNullOrEmpty(xnTandE.Attributes["ratetable"].Value)) &&
                    (String.IsNullOrEmpty(xnTandE.Attributes["ratetableid"].Value)))
                {
                    ratetable.Text      = sRateTable;
                    ratetableid.Value   = sRateTableId;
                    h_ratetable.Value   = sRateTable;
                    h_ratetableid.Value = sRateTableId;
                }//if
                else
                {
                    ratetable.Text      = xnTandE.Attributes["ratetable"].Value;
                    ratetableid.Value   = xnTandE.Attributes["ratetableid"].Value;
                    h_ratetableid.Value = xnTandE.Attributes["ratetableid"].Value;
                }//else
                //End MITS 15559 by MJP

                sBilledTotal     = xnTandE.Attributes["billtotal"].Value;
                sInvoiceTotal    = xnTandE.Attributes["invtotal"].Value;
                hcustid.Value    = xnTandE.Attributes["customerid"].Value;
                sClaimNum        = xnTandE.Attributes["claimnumber"].Value;
                lblClaimNum.Text = sClaimNum;
                hclmnum.Value    = sClaimNum;

                //create the onclick and onload attributes dynamically for the body html tag
                sbOnload.Length = 0;
                sbOnload.AppendFormat("PageLoaded('');setClaimTable('{0}'", SecurityElement.Escape(ratetable.Text));
                sbOnload.AppendFormat(",'{0}'",                             ratetableid.Value);
                sbOnload.AppendFormat(",'{0}'",                             hcustid.Value);
                sbOnload.AppendFormat(",'{0}');",                           sClaimNum);
                sbOnload.Append("if (parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();");
                
                bodyTandE.Attributes["onclick"] = "closeNewWin();";
                bodyTandE.Attributes["onload"]  = sbOnload.ToString();

                //create an onclientclick event for the claim rate table '...' button
                btnClaimRateTable.OnClientClick = String.Format("javascript:formHandler1('../../UI/TAndE/TimeAndExpenseRateAndUnits.aspx?custid={0}', '', '', 'childwindow', ''); return false;", hcustid.Value);

                //assign a postbackurl to the 'billed expenses' button
                btnBilledExpenses.PostBackUrl = String.Format("../../UI/TAndE/TimeAndExpenseBilledExpenses.aspx?claimid={0}", claimid.Value);

                if (dsTandEData.Tables["TandEData"] != null)
                {
                    tandedataDT = dsTandEData.Tables["TandEData"];

                    //add columns to the datatable manually so we can retrieve values from the gridview later with javascript
                    tandedataDT.Columns.Add("billit",      typeof(bool));
                    tandedataDT.Columns.Add("hinvoiceamt", typeof(string));
                    tandedataDT.Columns.Add("msg",         typeof(string));
                    tandedataDT.Columns.Add("hstatus",     typeof(string));
                    tandedataDT.Columns.Add("gvstatus",    typeof(string));

                    //loop through each invoice/node
                    foreach (DataRow d in tandedataDT.Rows)
                    {
                        StringBuilder sbVendorLink = new StringBuilder();

                        string sVendor      = d["vendor"].ToString();
                        string sInvoiceDate = d["invoicedate"].ToString();
                        DateTime idate      = Conversion.ToDate(sInvoiceDate);
                        d["invoicedate"]    = (!String.IsNullOrEmpty(sInvoiceDate)) ? String.Format("{0:MM/dd/yyyy}", idate) : String.Empty;
                        sbVendorLink.Append("<a href='#' onclick=\"; if (ValidateRateTableEntry()) formHandler1('");
                        sbVendorLink.AppendFormat("../../UI/TAndE/TimeAndExpenseInvoice.aspx?mode=edit&ratetableid={0}", ratetableid.Value);
                        sbVendorLink.AppendFormat("&claimid={0}&rowid={1}&claimnum={2}'", claimid.Value, d["id"].ToString(), sClaimNum);
                        sbVendorLink.AppendFormat(", '', 'saverateinfo', 'normal', ''); return false;\">{0}</a>", sVendor);
                        d["vendor"]         = sbVendorLink.ToString();
                        d["billit"]         = false;
                        d["hinvoiceamt"]    = d["invoiceamt"].ToString();
                        d["msg"]            = String.Format("{0}({1})", sVendor, d["invoicenumber"]);
                        d["gvstatus"]       = d["status"].ToString();
                        d["hstatus"]        = d["status"].ToString();
                        sVendor             = String.Empty;
                    }//foreach

                    //creates the 'vendor' boundfield dynamically and inserts it into the 1st position
                    BoundField bFldVendor                  = new BoundField();
                    bFldVendor.DataField                   = "vendor";
                    bFldVendor.HeaderText                  = "Vendor/Work Done By";
                    bFldVendor.HeaderStyle.CssClass        = "ctrlgroup2";
                    bFldVendor.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    bFldVendor.ItemStyle.HorizontalAlign   = HorizontalAlign.Left;
                    bFldVendor.FooterStyle.CssClass        = "ctrlgroup2";
                    bFldVendor.HtmlEncodeFormatString      = true;
                    bFldVendor.HtmlEncode                  = false;
                    gvExpenses.Columns.Insert(1, bFldVendor);

                    //creates the 'invoice amount' boundfield dynamically and inserts it into the 4th position
                    BoundField bFldInvoiceAmt                  = new BoundField();
                    bFldInvoiceAmt.DataField                   = "invoiceamt";
                    bFldInvoiceAmt.HeaderText                  = "Invoice Amount";
                    bFldInvoiceAmt.HeaderStyle.CssClass        = "ctrlgroup2";
                    bFldInvoiceAmt.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
                    bFldInvoiceAmt.ItemStyle.HorizontalAlign   = HorizontalAlign.Right;
                    bFldInvoiceAmt.FooterStyle.HorizontalAlign = HorizontalAlign.Right;
                    bFldInvoiceAmt.FooterStyle.CssClass        = "ctrlgroup2";
                    bFldInvoiceAmt.FooterText                  = sInvoiceTotal;
                    gvExpenses.Columns.Insert(4, bFldInvoiceAmt);

                    //creates the 'billed amount' boundfield dynamically and inserts it into the 5th position
                    BoundField bFldBillAmt                  = new BoundField();
                    bFldBillAmt.DataField                   = "billamt";
                    bFldBillAmt.HeaderText                  = "Billed Amount";
                    bFldBillAmt.HeaderStyle.CssClass        = "ctrlgroup2";
                    bFldBillAmt.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
                    bFldBillAmt.ItemStyle.HorizontalAlign   = HorizontalAlign.Right;
                    bFldBillAmt.FooterStyle.HorizontalAlign = HorizontalAlign.Right;
                    bFldBillAmt.FooterStyle.CssClass        = "ctrlgroup2";
                    bFldBillAmt.FooterText                  = sBilledTotal;
                    gvExpenses.Columns.Insert(5, bFldBillAmt);

                    //binds the datatable to the gridview
                    gvExpenses.Visible             = true;
                    gvExpenses.DataSource          = tandedataDT.DefaultView;
                    gvExpenses.AutoGenerateColumns = false;
                    gvExpenses.DataBind();
                }//if
            }//try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        }//method: BindGridData

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        private XElement GetMessageTemplate(string mode)
        {
            XElement oTemplate = null;

            if (mode == "GetXmlData")
            {
                #region GetXmlData template
                oTemplate = XElement.Parse(@"
                                            <Message>
                                                <Authorization></Authorization>
                                                <Call>
                                                    <Function></Function>
                                                </Call>
                                                <Document>
                                                    <TimeExpense>
                                                        <FrmName></FrmName>
                                                        <InputXml>
		                                                    <TandE/>
	                                                    </InputXml>
                                                        <TableId1/>
                                                        <TableId2/>
                                                        <TableId3/>
                                                    </TimeExpense>
                                                </Document>
                                            </Message>
                                            ");
                #endregion
            } // if
            else if (mode == "BillIt")
            {
                #region BillIt template
                oTemplate = XElement.Parse(@"
                                            <Message>
                                                <Authorization></Authorization>
                                                <Call>
                                                    <Function></Function>
                                                </Call>
                                                <Document>
                                                    <TimeExpense>
                                                        <InvoiceIds></InvoiceIds>
                                                        <IsContinue></IsContinue>
                                                        <AllowClosedClaim></AllowClosedClaim>
                                                    </TimeExpense>
                                                </Document>
                                            </Message>
                                            ");
                #endregion
            } // else if
            else if (mode == "VoidIt")
            {
                #region VoidIt template
                oTemplate = XElement.Parse(@"
                                            <Message>
                                                <Authorization></Authorization>
                                                <Call>
                                                    <Function></Function>
                                                </Call>
                                                <Document>
                                                    <TimeExpense>
                                                        <InvoiceIds></InvoiceIds>
                                                        <IsContinue></IsContinue>
                                                        <Status></Status>
                                                    </TimeExpense>
                                                </Document>
                                            </Message>
                                            ");
                #endregion
            } // else if
            else
            {
                return null;
            } // else

            return oTemplate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvExpenses_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string sRowId = String.Empty;
            StringBuilder sbImageAtt = new StringBuilder();

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //get reference to ImageButton control placed inside the Nth cell of gridview control
                //where N is the integer value of enum gridviewColumns.img
                ImageButton img = (ImageButton)e.Row.Cells[(int)gridviewColumns.img].FindControl("img");

                //create onlick event for the checkboxes within the gridview
                CheckBox cbox = (CheckBox)e.Row.Cells[(int)gridviewColumns.billit].FindControl("optBillit");
                cbox.InputAttributes.Add("onclick", "InvoiceSelected();");

                //gets the rowid from the hiddenfield and assigns it to a temp variable
                HiddenField hf  = (HiddenField)e.Row.Cells[(int)gridviewColumns.id].FindControl("rowid");
                sRowId = hf.Value.ToString();

                //add onclick event to the img to call the javascript function (formHandlerForTAndEDetails)
                sbImageAtt.Append("formHandlerForTAndEDetails('../../UI/TAndE/TimeAndExpenseInvoiceDetails.aspx',");
                sbImageAtt.AppendFormat("'{0}','{1}',", sRowId, claimid.Value);
                sbImageAtt.AppendFormat("'{0}','{1}'); return false;", ratetableid.Value, hclmnum.Value);
                img.Attributes.Add("onclick", sbImageAtt.ToString());

                //e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
                //e.Row.Attributes.Add("onmouseout",  "this.style.backgroundColor='#FFFBD6'");
            }//if
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvExpenses_DataBound(object sender, EventArgs e)
        {
            //this is to hide these columns from displaying on the client's UI
            gvExpenses.Columns[(int)gridviewColumns.id].ItemStyle.CssClass   = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.id].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.id].FooterStyle.CssClass = "hiderowcol";

            gvExpenses.Columns[(int)gridviewColumns.transid].ItemStyle.CssClass   = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.transid].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.transid].FooterStyle.CssClass = "hiderowcol";

            gvExpenses.Columns[(int)gridviewColumns.payeeid].ItemStyle.CssClass   = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.payeeid].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.payeeid].FooterStyle.CssClass = "hiderowcol";

            gvExpenses.Columns[(int)gridviewColumns.hinvoiceamt].ItemStyle.CssClass   = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.hinvoiceamt].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.hinvoiceamt].FooterStyle.CssClass = "hiderowcol";

            gvExpenses.Columns[(int)gridviewColumns.msg].ItemStyle.CssClass   = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.msg].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.msg].FooterStyle.CssClass = "hiderowcol";

            gvExpenses.Columns[(int)gridviewColumns.hstatus].ItemStyle.CssClass   = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.hstatus].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.hstatus].FooterStyle.CssClass = "hiderowcol";
        }
    }
}