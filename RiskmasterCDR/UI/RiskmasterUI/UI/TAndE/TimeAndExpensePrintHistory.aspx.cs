﻿using System;
using System.Data;
using System.Web.UI;
using System.Xml;
using System.Xml.Linq;

using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.TAndE
{
    public partial class TimeAndExpensePrintHistory : NonFDMBasePageCWS
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus   = false;
            string sreturnValue  = String.Empty;
            string sClaimId      = String.Empty;
            string sInvoiceId    = String.Empty;
            string sPrintMode    = String.Empty;
            XElement oMsgElement = null;

            // Read the values from query string
            sPrintMode = AppHelper.GetQueryStringValue("mode");

            if (sPrintMode == "history")
            {
                Page.Title = "Print History";

                sClaimId = AppHelper.GetQueryStringValue("claimid");

                // Read the values from the form if nothing was parsed from the query
                if (String.IsNullOrEmpty(sClaimId))
                {
                    sClaimId = AppHelper.GetFormValue("ClaimId");
                } // if

                if (!String.IsNullOrEmpty(sClaimId))
                {
                    ClaimId.Text = sClaimId;
                } // if
            } // if
            else if (sPrintMode == "report")
            {
                Page.Title = "Print Report";

                sInvoiceId = AppHelper.GetQueryStringValue("invoiceid");

                // Read the values from the form if nothing was parsed from the query
                if (String.IsNullOrEmpty(sInvoiceId))
                {
                    sInvoiceId = AppHelper.GetFormValue("invoiceid");
                } // if

                if (!String.IsNullOrEmpty(sInvoiceId))
                {
                    InvoiceId.Text = sInvoiceId;
                } // if
 
            } // else if

            pnlPrintHistory.Visible = false;
            pnlPrintReport.Visible  = false;

            if ((!IsPostBack) && (!String.IsNullOrEmpty(sPrintMode)))
            {
                try
                {
                    oMsgElement = GetMessageTemplate(sPrintMode);

                    if (sPrintMode == "history")
                    {
                        pnlPrintHistory.Visible = true;
                        pnlPrintReport.Visible  = false;

                        CallCWSFunction("TimeExpenseAdaptor.PrintHistory", out sreturnValue, oMsgElement);
                        bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);

                        if ((bReturnStatus) && (!String.IsNullOrEmpty(sClaimId)))
                        {
                            BindHistoryGridView(sreturnValue);
                        }//if
                    } // if
                    else if (sPrintMode == "report")
                    {
                        pnlPrintReport.Visible  = true;
                        pnlPrintHistory.Visible = false;

                        CallCWSFunction("TimeExpenseAdaptor.PrintReport", out sreturnValue, oMsgElement);
                        bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);

                        if ((bReturnStatus) && (!String.IsNullOrEmpty(sInvoiceId)))
                        {
                            BindReportGridView(sreturnValue);
                        }// if
                    } // else
                }// try
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }// catch
            } // if
        }// Page_Load

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        private void BindHistoryGridView(string data)
        {
            bool bDateIsNotEmpty      = false;
            DataSet invoiceRecordsSet = null;
            DataTable optionHistoryDT = null;
            XmlDocument invoiceXDoc   = new XmlDocument();
            //XElement xmlPrintData   = XElement.Parse(data);

            try
            {
                //fill xmldocument from the string xml
                invoiceXDoc.LoadXml(data);

                //fill dataset with xmldocument object
                invoiceRecordsSet = new DataSet();
                invoiceRecordsSet.ReadXml(new XmlNodeReader(invoiceXDoc.SelectSingleNode("//Document")));

                XmlNode xnControlData = invoiceXDoc.SelectSingleNode("//Document/print");

                string tdate = xnControlData.Attributes["tdate"].Value;
                DateTime dateToday = Conversion.ToDate(tdate);
                lblCurrentDate.Text = (!String.IsNullOrEmpty(tdate)) ? String.Format("{0:MM/dd/yyyy}", dateToday) : String.Format("{0:MM/dd/yyyy}", DateTime.Now);

                lblTitle.Text = xnControlData.Attributes["title"].Value;

                if (String.IsNullOrEmpty(lblTitle.Text))
                {
                    lblTitle.Text = "Time and Expense History";
                } // if

                lblBillTotal.Text   = xnControlData.Attributes["billtotal"].Value;
                lblInvTotal.Text    = xnControlData.Attributes["invtotal"].Value;
                lblCompanyName.Text = xnControlData.Attributes["name"].Value;

                XmlNode xnCopyRightData = invoiceXDoc.SelectSingleNode("//Document/print/CopyRightData");

                if (xnCopyRightData != null)
                {
                    lblCopyRightProp.Text      = xnCopyRightData.Attributes["CopyRightProp"].Value;
                    lblProductName.Text        = xnCopyRightData.Attributes["ProductName"].Value;
                    lblCopyRightDef.Text       = xnCopyRightData.Attributes["CopyRightDef"].Value;
                    lblCopyRightsReserved.Text = xnCopyRightData.Attributes["CopyRightsReserved"].Value;
                } // if
                else
                {
                    //TODO: May want to put these in a Global.asax or in App_GlobalResources file later
                    string sCurrentYear        = String.Format("{0:yyyy}", DateTime.Now);
                    lblCopyRightProp.Text      = "Proprietary &amp; Confidential Data";
                    lblProductName.Text        = "RISKMASTERX";
                    lblCopyRightDef.Text       = String.Format("Copyright {0}, Computer Sciences Corporation", sCurrentYear);
                    lblCopyRightsReserved.Text = "All Rights Reserved Worldwide";
                } // else

                if (invoiceRecordsSet.Tables["option"] != null)
                {
                    pnlGridview.Visible = true;
                    pnlNoData.Visible   = false;

                    optionHistoryDT = invoiceRecordsSet.Tables["option"];

                    //loop through each invoice/node
                    foreach (DataRow d in optionHistoryDT.Rows)
                    {
                        DateTime cDate;
                        DateTime iDate;

                        string sInvDate = d["invdate"].ToString();
                        iDate           = Conversion.ToDate(sInvDate);
                        d["invdate"]    = (!String.IsNullOrEmpty(sInvDate)) ? String.Format("{0:MM/dd/yyyy}", iDate) : String.Empty;

                        bDateIsNotEmpty = DateTime.TryParse(d["checkdate"].ToString(), out cDate);
                        d["checkdate"]  = (bDateIsNotEmpty) ? String.Format("{0:MM/dd/yyyy}", cDate) : String.Empty;
                    }//foreach

                    //binds the datatable to the gridview
                    gvPrintHistory.Visible             = true;
                    gvPrintHistory.DataSource          = optionHistoryDT.DefaultView;
                    gvPrintHistory.AutoGenerateColumns = false;
                    gvPrintHistory.DataBind();
                }//if
                else
                {
                    pnlNoData.Visible   = true;
                    pnlGridview.Visible = false;
                } // else
            }//try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        } // method: BindHistoryGridView

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        private void BindReportGridView(string data)
        {
            DataSet invoiceRecordsSet = null;
            DataTable optionReportDT  = null;
            XmlDocument invoiceXDoc   = new XmlDocument();
            //XElement xmlPrintData   = XElement.Parse(data);

            try
            {
                //fill xmldocument from the string xml
                invoiceXDoc.LoadXml(data);

                //fill dataset with xmldocument object
                invoiceRecordsSet = new DataSet();
                invoiceRecordsSet.ReadXml(new XmlNodeReader(invoiceXDoc.SelectSingleNode("//Document")));

                XmlNode xnControlData = invoiceXDoc.SelectSingleNode("//Document/print");

                string tDate = xnControlData.Attributes["tdate"].Value;
                DateTime dateToday = Conversion.ToDate(tDate);
                lblCurrentDate2.Text = (!String.IsNullOrEmpty(tDate)) ? String.Format("{0:MM/dd/yyyy}", dateToday) : String.Empty;

                string bDate = xnControlData.Attributes["DateBilled"].Value;
                DateTime dateBilled = Conversion.ToDate(bDate);
                lblDateBilled.Text = (!String.IsNullOrEmpty(bDate)) ? String.Format("{0:MM/dd/yyyy}", dateBilled) : String.Empty;

                if (String.IsNullOrEmpty(lblCurrentDate.Text))
                {
                    lblCurrentDate2.Text = String.Format("{0:MM/dd/yyyy}", DateTime.Now);
                } // if

                lblTitle2.Text = xnControlData.Attributes["title"].Value;

                if (String.IsNullOrEmpty(lblTitle.Text))
                {
                    lblTitle2.Text = "Time and Expense Report";
                } // if

                lblCompanyName2.Text = xnControlData.Attributes["name"].Value;
                lblClaimNumber.Text  = xnControlData.Attributes["ClaimNumber"].Value;
                lblClaimant.Text     = xnControlData.Attributes["Claimant"].Value;
                lblCity.Text         = xnControlData.Attributes["city1"].Value;
                lblPhone.Text        = xnControlData.Attributes["phone1"].Value;
                lblFax.Text          = xnControlData.Attributes["fax1"].Value;

                if (invoiceRecordsSet.Tables["option"] != null)
                {
                    optionReportDT = invoiceRecordsSet.Tables["option"];

                    //loop through each invoice/node
                    foreach (DataRow d in optionReportDT.Rows)
                    {
                        DateTime iDate;
                        bool bDateIsNotEmpty = DateTime.TryParse(d["invdate"].ToString(), out iDate);
                        d["invdate"] = (bDateIsNotEmpty) ? String.Format("{0:MM/dd/yyyy}", iDate) : String.Empty;
                        //rupal:start,multicurrency
                        //d["rate"]          = String.Format("${0}", d["rate"]);
                        //d["total"]         = String.Format("${0}", d["total"]);
                        d["rate"] = d["rate"].ToString();
                        d["total"] = d["total"].ToString();
                        //rupal:end
                    }//foreach

                    //binds the datatable to the gridview
                    gvPrintReport.Visible             = true;
                    gvPrintReport.DataSource          = optionReportDT.DefaultView;
                    gvPrintReport.AutoGenerateColumns = false;
                    gvPrintReport.DataBind();
                }//if
            }//try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        } // method: BindReportGridView

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <param name="printmode"></param>
        /// <returns></returns>
        private XElement GetMessageTemplate(string printmode)
        {
            XElement oTemplate = null;

            if (printmode == "history")
            {
                oTemplate = XElement.Parse(@"
                                            <Message>
                                                <Authorization></Authorization>
                                                <Call>
                                                    <Function></Function>
                                                </Call>
                                                <Document>
                                                    <TimeExpense>
                                                        <ClaimId></ClaimId>
                                                    </TimeExpense>
                                                </Document>
                                            </Message>
                                            ");
            } // if
            else if (printmode == "report")
            {
                oTemplate = XElement.Parse(@"
                                            <Message>
                                                <Authorization></Authorization>
                                                <Call>
                                                    <Function></Function>
                                                </Call>
                                                <Document>
                                                    <TimeExpense>
                                                        <InvoiceId></InvoiceId>
                                                    </TimeExpense>
                                                </Document>
                                            </Message>
                                            ");
            } // else if
            else
            {
                return null;
            } // else

            return oTemplate;
        }// GetMessageTemplate
    }
}