﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.TAndE
{
    public partial class TimeAndExpenseBilledExpenses : NonFDMBasePageCWS
    {
        #region public variables
        public enum tandeColumns     { invtotal, billtotal, ratetableid, ratetable, customerid, claimnumber, claimid, nopost, nopostnobill };
        public enum tandeDataColumns { bgcolor, id, vendor, invoicenumber, invoicedate, invoiceamt, billamt, status, chknum, transid, payeeid, chkdate };
        public enum gridviewColumns  { chkbox, vendor, invoicenumber, invoicedate, invoiceamt, billamt, gvstatus, chknum, chkdate, img, id, hstatus, transid, payeeid, hinvoiceamt };//MITS 15633 (added 3 columns to gridview)
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus   = false;
            string sreturnValue  = String.Empty;
            XElement oMsgElement = null;

            //read the values from the previous form
            string sRowId       = AppHelper.GetQueryStringValue("RowId");
            string sClaimId     = AppHelper.GetQueryStringValue("claimid");
            string sRateCodeId  = AppHelper.GetQueryStringValue("InvoiceId");
            string sClaimNumber = AppHelper.GetFormValue("hclmnum");
            string sBilledVoid  = AppHelper.GetFormValue("billed_void");

            TableId1.Text = sClaimId;

            if (String.IsNullOrEmpty(FrmName.Text))
            {
                FrmName.Text = "tandebillexpsummery";
            } // if

            if (!String.IsNullOrEmpty(sClaimNumber))
            {
                lblClaimNum.Text = sClaimNumber;
                claimid.Value    = sClaimId;
            } // if

            //dynamically create an onclientclick event for the 'print history' button
            btnPrintHistory.OnClientClick = "return PrintH('" + sClaimId + "');;";

            if (!IsPostBack)
            {
                try
                {
                    oMsgElement   = GetMessageTemplate("GetXmlData");
                    CallCWSFunction("TimeExpenseAdaptor.GetXmlData", out sreturnValue, oMsgElement);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);

                    if (bReturnStatus)
                    {
                        BindGridData(sreturnValue);
                    }//if
                }//try
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch
            } // if
            else if (sBilledVoid == "0")
            {
                billed_void.Value = String.Empty;
                InvoiceIds.Text   = hdnInvId.Value;
                Status.Text       = h_status.Value;

                //TODO: assign from hidden variables rather then hardcode
                IsContinue.Text = "false";

                try
                {
                    oMsgElement = GetMessageTemplate("VoidIt");
                    CallCWSFunction("TimeExpenseAdaptor.VoidIt", out sreturnValue, oMsgElement);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);
                }//try
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch
                Response.Redirect(Page.Request.Url.ToString(), true);
            } // else if
        }// Page_Load

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        private void BindGridData(string data)
        {
            string sInvoiceTotal  = String.Empty;
            string sBilledTotal   = String.Empty;
            DataSet dsTandE       = null;
            DataSet dsTandEData   = null;
            DataTable tandeDT     = null;
            DataTable tandedataDT = null;
            DataRow tandeDR       = null;
            XmlDocument xdTandE   = new XmlDocument();

            //XElement xmlTandEDetails = XElement.Parse(data);

            try
            {
                //fill xmldocument from the string xml
                xdTandE.LoadXml(data);

                //fill xmlnode with a single node from the xmldocument
                XmlNode xnTandE      = xdTandE.SelectSingleNode("//Document/TandE");
                XmlNodeList xnlTandE = xnTandE.ChildNodes;

                //removes all the child nodes from the main t&e element
                int iChildCount = xnlTandE.Count;
                for (int ii = 0; ii < iChildCount; ii++)
                {
                    xnTandE.RemoveChild(xnlTandE[0]);
                } // for

                //fill dataset with xmlnode object
                dsTandE = new DataSet();
                dsTandE.ReadXml(new XmlNodeReader(xnTandE));

                //fill xmldocument from the string xml
                XmlDocument xdTandEData = new XmlDocument();
                xdTandEData.LoadXml(data);

                //fill xmlnode with a single node from the xmldocument
                XmlNode xnTandEData     = xdTandEData.SelectSingleNode("//Document/TandE");

                //fill dataset with xmlnode object
                dsTandEData = new DataSet();
                dsTandEData.ReadXml(new XmlNodeReader(xnTandEData));

                tandeDT = dsTandE.Tables["TandE"];
                tandeDR = tandeDT.Rows[0];

                //assign values from the main t&e element to the hidden variables
                claimid.Value     = tandeDR.ItemArray.GetValue((int)tandeColumns.claimid).ToString();
                sBilledTotal      = tandeDR.ItemArray.GetValue((int)tandeColumns.billtotal).ToString();
                sInvoiceTotal     = tandeDR.ItemArray.GetValue((int)tandeColumns.invtotal).ToString();
                lblClaimNum.Text  = tandeDR.ItemArray.GetValue((int)tandeColumns.claimnumber).ToString();
                lblNPA.Text       = tandeDR.ItemArray.GetValue((int)tandeColumns.nopost).ToString();
                lblNBNPA.Text     = tandeDR.ItemArray.GetValue((int)tandeColumns.nopostnobill).ToString();
                hclmnum.Value     = tandeDR.ItemArray.GetValue((int)tandeColumns.claimnumber).ToString();
                ratetableid.Value = tandeDR.ItemArray.GetValue((int)tandeColumns.ratetableid).ToString();
                //hcustid.Value   = printHistoryDR.ItemArray.GetValue((int)tandeColumns.customerid).ToString();
                //ratetable.Text  = printHistoryDR.ItemArray.GetValue((int)tandeColumns.ratetable).ToString();

                //create an onclientclick event for the 'print report' button
                btnPrintReport.OnClientClick = "return printdoc_new();;";

                //assign a postbackurl to the 'available for billing' button
                btnAvailForBilling.PostBackUrl = "TimeAndExpense.aspx?claimid=" + claimid.Value;

                if (dsTandEData.Tables["TandEData"] != null)
                {
                    //displays panel so that the gridview w/header and footer IS displayed on the UI
                    pnlGridview.Visible = true;
                    pnlNoData.Visible   = false;

                    tandedataDT = dsTandEData.Tables["TandEData"];

                    //add columns to the datatable manually so we can retrieve values from the gridview later with javascript
                    tandedataDT.Columns.Add("gvstatus",    typeof(string));
                    tandedataDT.Columns.Add("hstatus",     typeof(string));
                    tandedataDT.Columns.Add("hinvoiceamt", typeof(string));//MITS 15633

                    //loop through each invoice/node
                    foreach (DataRow d in tandedataDT.Rows)
                    {
                        d["gvstatus"]    = d["status"].ToString();
                        d["hstatus"]     = d["status"].ToString();
                        d["hinvoiceamt"] = d["invoiceamt"].ToString();//MITS 15633

                        if (!String.IsNullOrEmpty(d["invoicedate"].ToString()))
                        {
                            DateTime date    = Conversion.ToDate(d["invoicedate"].ToString());
                            d["invoicedate"] = String.Format("{0:MM/dd/yyyy}", date);
                        } // if

                        if (!String.IsNullOrEmpty(d["chkdate"].ToString()))
                        {
                            DateTime date2 = Conversion.ToDate(d["chkdate"].ToString());
                            d["chkdate"]   = String.Format("{0:MM/dd/yyyy}", date2);
                        } // if
                    }//foreach

                    //creates the 'invoice amount' boundfield dynamically and inserts it into the 4th position
                    BoundField bFldInvoiceAmt                  = new BoundField();
                    bFldInvoiceAmt.DataField                   = "invoiceamt";
                    bFldInvoiceAmt.HeaderText                  = "Invoice Amount";
                    bFldInvoiceAmt.HeaderStyle.CssClass        = "ctrlgroup2";
                    bFldInvoiceAmt.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
                    bFldInvoiceAmt.ItemStyle.HorizontalAlign   = HorizontalAlign.Right;
                    bFldInvoiceAmt.FooterStyle.HorizontalAlign = HorizontalAlign.Right;
                    bFldInvoiceAmt.FooterStyle.CssClass        = "ctrlgroup2";
                    bFldInvoiceAmt.FooterText                  = sInvoiceTotal;
                    gvExpenses.Columns.Insert(4, bFldInvoiceAmt);

                    //creates the 'billed amount' boundfield dynamically and inserts it into the 5th position
                    BoundField bFldBillAmt                  = new BoundField();
                    bFldBillAmt.DataField                   = "billamt";
                    bFldBillAmt.HeaderText                  = "Billed Amount";
                    bFldBillAmt.HeaderStyle.CssClass        = "ctrlgroup2";
                    bFldBillAmt.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
                    bFldBillAmt.ItemStyle.HorizontalAlign   = HorizontalAlign.Right;
                    bFldBillAmt.FooterStyle.HorizontalAlign = HorizontalAlign.Right;
                    bFldBillAmt.FooterStyle.CssClass        = "ctrlgroup2";
                    bFldBillAmt.FooterText                  = sBilledTotal;
                    gvExpenses.Columns.Insert(5, bFldBillAmt);

                    //binds the datatable to the gridview
                    gvExpenses.Visible             = true;
                    gvExpenses.DataSource          = tandedataDT.DefaultView;
                    gvExpenses.AutoGenerateColumns = false;
                    gvExpenses.DataBind();
                }//if
                else
                {
                    //no invoices exist, so we do NOT display the gridview panel on the UI
                    pnlNoData.Visible   = true;
                    pnlGridview.Visible = false;
                } // else
            }//try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        } // method: BindGridData

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate(string mode)
        {
            XElement oTemplate = null;
            if (mode == "GetXmlData")
            {
                oTemplate = XElement.Parse(@"<Message>
                                                <Authorization></Authorization>
                                                <Call>
                                                    <Function></Function>
                                                </Call>
                                                <Document>
                                                    <TimeExpense>
                                                        <FrmName></FrmName>
                                                        <InputXml>
		                                                    <TandE/>
	                                                    </InputXml>
                                                        <TableId1/>
                                                        <TableId2/>
                                                        <TableId3/>
                                                    </TimeExpense>
                                                </Document>
                                            </Message>");
            } // if
            else if (mode == "VoidIt")
            {
                oTemplate = XElement.Parse(@"<Message>
                                                <Authorization></Authorization>
                                                <Call>
                                                    <Function></Function>
                                                </Call>
                                                <Document>
                                                    <TimeExpense>
                                                        <InvoiceIds></InvoiceIds>
                                                        <IsContinue></IsContinue>
                                                        <Status></Status>
                                                    </TimeExpense>
                                                </Document>
                                            </Message>");
            } // else if
            else
            {
                return null;
            } // else

            return oTemplate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvExpenses_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string sRowId = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // get reference to ImageButton control placed inside the Nth cell of gridview control
                // where N is the integer value of enum gridviewColumns.img
                ImageButton img = (ImageButton)e.Row.Cells[(int)gridviewColumns.img].FindControl("img");

                //create onlick event for the checkboxes within the gridview
                CheckBox cbox = (CheckBox)e.Row.Cells[(int)gridviewColumns.chkbox].FindControl("optBillit");
                cbox.InputAttributes.Add("onclick", "InvoiceSelected();");

                //gets the rowid from the hiddenfield and assigns it to a temp variable
                HiddenField hf  = (HiddenField)e.Row.Cells[(int)gridviewColumns.id].FindControl("rowid");
                sRowId = hf.Value.ToString();

                if (! String.IsNullOrEmpty(sRowId))
                {
                    // add onclick event to the img to call the javascript function (formHandlerForTAndEDetails)
                    img.Attributes.Add("onclick", "formHandlerForTAndEDetails('../../UI/TAndE/TimeAndExpenseInvoiceDetails.aspx','" + sRowId + "','" + claimid.Value.ToString() + "','" + ratetableid.Value.ToString() + "','" + hclmnum.Value.ToString() + "'); return false;");
                } // if

                //e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
                //e.Row.Attributes.Add("onmouseout",  "this.style.backgroundColor='#FFFBD6'");
            }//if
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvExpenses_DataBound(object sender, EventArgs e)
        {
            //this is to hide these columns from displaying on the client's UI
            gvExpenses.Columns[(int)gridviewColumns.id].ItemStyle.CssClass   = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.id].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.id].FooterStyle.CssClass = "hiderowcol";

            gvExpenses.Columns[(int)gridviewColumns.hstatus].ItemStyle.CssClass   = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.hstatus].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.hstatus].FooterStyle.CssClass = "hiderowcol";

            //Start - MITS 15633 - mpalinski
            gvExpenses.Columns[(int)gridviewColumns.transid].ItemStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.transid].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.transid].FooterStyle.CssClass = "hiderowcol";

            gvExpenses.Columns[(int)gridviewColumns.payeeid].ItemStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.payeeid].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.payeeid].FooterStyle.CssClass = "hiderowcol";

            gvExpenses.Columns[(int)gridviewColumns.hinvoiceamt].ItemStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.hinvoiceamt].HeaderStyle.CssClass = "hiderowcol";
            gvExpenses.Columns[(int)gridviewColumns.hinvoiceamt].FooterStyle.CssClass = "hiderowcol";
            //End - MITS 15633 - mpalinski
        }
    }
}
