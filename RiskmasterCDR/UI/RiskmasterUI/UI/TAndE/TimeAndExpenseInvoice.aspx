﻿<%@ page language="C#" Theme="RMX_Default" autoeventwireup="True" codebehind="TimeAndExpenseInvoice.aspx.cs" inherits="Riskmaster.UI.TAndE.TimeAndExpenseInvoice" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl" TagPrefix="mc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Time and Expense Invoice</title>
   <%-- <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    
    <script type="text/javascript" language="JavaScript" src="../../Scripts/TandE.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/form.js"></script>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <style type="text/css">
        .nolinehover { text-decoration: underline; color:blue; }
    </style>
    <script type="text/javascript" language="javascript">
        function SubmitForm() {
            document.forms[0].submit();
        }
    </script>
</head>

<body id="bodyTandEInvoice" class="Margin0" runat="server">
    <form id="frmData" runat="server">
        <div id="maindev" style="height:100%;width:99%;overflow:auto">
        <asp:scriptmanager id="ScriptManager1" runat="server" />
        
        <table width="100%" cellspacing="0" cellpadding="4" border="0">
            <tr>
                <td colspan="2">
                    <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
                </td>
            </tr>
        </table>
        
        <table width="100%" cellspacing="0" cellpadding="4" border="0">
            <tr>
                <td>Claim Number</td>
                <td><asp:textbox id="clmNumber" ReadOnly="true" runat="server" /></td>
            </tr>
            <tr>
                <td>Allocated</td>
                <td><asp:CheckBox runat="server" ID="optallocate" /></td>
            </tr>
            <tr>
                <td class="required"><asp:Label id="lblcurrtype" runat="server" Text="Currency Tupe:" Visible="false"></asp:Label> </td>
                <td>
                     <uc2:codelookup runat="server" ID="currencytypetext" CodeTable="CURRENCY_TYPE" ControlName="currencytypetext" OnChange="SubmitForm();"
                        type="code" required="true" Visible="false"  />
               </td>              
            </tr>
            <tr>
                <td class="required">Vendor:</td>
                <td>
                    <asp:textbox id="vendor" ReadOnly="true" runat="server" />
                    <asp:button id="btnVendorLookup" class="EllipsisControl" runat="server" Text="..." />
                </td>
            </tr>
            <tr>
                <td>Invoice Number</td>
                <td><asp:textbox id="invNumber" MaxLength="25" runat="server" />
                    
                </td>
            </tr>
            <tr>
                <td class="required">Invoice Amount</td>
                <td><mc:CurrencyTextbox runat="server" id="invAmt" OnBlur="setDataChangedInv('invAmt');" /></td>                
            </tr>
            <tr>
                <td class="required">Invoice Date</td>
                <td>
                    <asp:textbox id="invDate" runat="server" />
                    <%--<asp:button id="btGetInvDate" CssClass="DateLookupControl" runat="server" Text="..."  />
                    <script language="javascript" type="text/javascript">
                        Zapatec.Calendar.setup(
				            {
				                inputField: "invDate",
				                ifFormat: "%m/%d/%Y",
				                button: "btGetInvDate"
				            }
				        );
                    </script>--%>
                     <script type="text/javascript">
                         $(function () {
                             $("#invDate").datepicker({
                                 showOn: "button",
                                 buttonImage: "../../Images/calendar.gif",
                                // buttonImageOnly: true,
                                 showOtherMonths: true,
                                 selectOtherMonths: true,
                                 changeYear: true
                             }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                         });
                    </script>
                </td>
            </tr>
            <tr>
                <td><asp:label id="totinvoice" runat="server" /></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><asp:label id="totbill" runat="server" /></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Available:</td>
                <td><mc:CurrencyTextbox runat="server" id="totavailable" /></td>                
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
        
        <table width="60%" cellspacing="0" cellpadding="4" border="0">
            <tr>
                <td class="required">Invoice Details</td>
            </tr>
            <tr class="data1">
                <td>
                    <div id="div1" style="height:100px;overflow:auto;BORDER-RIGHT: darkblue 1px solid;
    BORDER-TOP: darkblue 1px solid;
    BORDER-LEFT: darkblue 1px solid;
    BORDER-BOTTOM: darkblue 1px solid;
    BACKGROUND-COLOR: #ffffff">
                    <asp:gridview id="gvInvoiceDetails" runat="server" 
                                  width="97%" 
                                  autogeneratecolumns="False" 
                                  GridLines="None"
                                  ShowHeader="true" 
                                  ShowFooter="false"
                                  BorderWidth="1px" 
                                  BorderStyle="Solid"
                                  BorderColor="#A7A6AA"
                                  AlternatingRowStyle-CssClass="data2" 
                                  onrowdatabound="gvInvoiceDetails_RowDataBound" 
                                  ondatabound="gvInvoiceDetails_DataBound">
                        <columns>
                            <asp:templatefield headerstyle-cssclass="ctrlgroup" 
                                               HeaderStyle-HorizontalAlign="Left" 
                                               ItemStyle-HorizontalAlign="Left" 
                                               headertext="Transaction Type">
                                <itemtemplate>
                                    <asp:hyperlink id="transtype_link" runat="server" 
                                                   text='<%# DataBinder.Eval(Container.DataItem, "transtype")%>' />
                                </itemtemplate>
                            </asp:templatefield>
                            <asp:boundfield datafield="invamt"  
                                            headerstyle-cssclass="ctrlgroup" 
                                            HeaderStyle-HorizontalAlign="Right"
                                            ItemStyle-HorizontalAlign="Right"
                                            headertext="Invoice Amount" />
                            <asp:boundfield datafield="billable" 
                                            headerstyle-cssclass="ctrlgroup" 
                                            HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center"
                                            headertext="Billable" />
                            <asp:boundfield datafield="billamt" 
                                            headerstyle-cssclass="ctrlgroup" 
                                            HeaderStyle-HorizontalAlign="Right"
                                            ItemStyle-HorizontalAlign="Right"
                                            headertext="Bill Amount" />
                            <asp:TemplateField headerstyle-cssclass="ctrlgroup" 
                                               HeaderStyle-HorizontalAlign="Center" 
                                               ItemStyle-HorizontalAlign="Center"
                                               headertext=" ">
                                <ItemTemplate>
                                    <asp:ImageButton id="deletedetail_link" runat="server" 
                                                     ImageUrl="~/Images/delete2.gif" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="transtype" runat="server" Value='<%# Eval("transtype") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="reservetype" runat="server" Value='<%# Eval("reservetype") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hInvamt" runat="server" Value='<%# Eval("hInvamt") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hBillable" runat="server" Value='<%# Eval("hBillable") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="unit" runat="server" Value='<%# Eval("unit") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="rate" runat="server" Value='<%# Eval("rate") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="nonbillreason" runat="server" Value='<%# Eval("nonbillreason") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="qty" runat="server" Value='<%# Eval("qty") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="postable" runat="server" Value='<%# Eval("postable") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hTrackId" runat="server" Value='<%# Eval("hTrackId") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="transtypecode" runat="server" Value='<%# Eval("transtypecode") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hBillAmt" runat="server" Value='<%# Eval("hBillAmt") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="reservetypecode" runat="server" Value='<%# Eval("reservetypecode") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="invtot" runat="server" Value='<%# Eval("invtot") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="fromdate" runat="server" Value='<%# Eval("fromdate") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="todate" runat="server" Value='<%# Eval("todate") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="fromdateFormatted" runat="server" Value='<%# Eval("fromdateFormatted") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="todateFormatted" runat="server" Value='<%# Eval("todateFormatted") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hOverRide" runat="server" Value='<%# Eval("hOverRide") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="nonbillreasoncode" runat="server" Value='<%# Eval("nonbillreasoncode") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="comment" runat="server" Value='<%# Eval("comment") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="precomment" runat="server" Value='<%# Eval("precomment") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="status" runat="server" Value='<%# Eval("status") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="reason" runat="server" Value='<%# Eval("reason") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="id" runat="server" Value='<%# Eval("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="pid" runat="server" Value='<%# Eval("pid") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="invnum" runat="server" Value='<%# Eval("invnum") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </columns>
                    </asp:gridview>
                    </div>
                </td>
                <asp:Panel ID="pnlNewInvoice" runat="server">
                    <td width="2%">&nbsp;</td>
                    <td width="5%">
                        <table>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                        </table>
                    </td>
                </asp:Panel>
            </tr>
        </table>
       
        <table width="100%" cellspacing="0" cellpadding="4" border="0">
            <tr>
                <td>
                    <asp:HyperLink runat="server" ID="hlAddDetail" CssClass="nolinehover" Text="Add Detail" />
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <asp:button id="SaveAndClose" CssClass="button" runat="server"
                                OnClientClick="javascript:return ValidateInvoiceAmount();"
                                text="Save and Close" 
                                onclick="SaveAndClose_Click" />
                    <asp:button id="Cancel" CssClass="button" runat="server" 
                                text="Cancel" OnClientClick="javascript:return BackToTandE();;" />
                </td>
            </tr>
        </table>
        
        <asp:placeholder id="PlaceHolder" runat="server" />
        
        <asp:HiddenField runat="server" ID="action" />
        <asp:HiddenField runat="server" ID="allocatedFlag" />
        <asp:HiddenField runat="server" ID="checkflag" />
        <asp:HiddenField runat="server" ID="claimid" />
        <asp:HiddenField runat="server" ID="deleteflag" />
        <asp:HiddenField runat="server" ID="effedate" />
        <asp:HiddenField runat="server" ID="effsdate1" />
        <asp:HiddenField runat="server" ID="ExistingRateDetailEdited" />
        <asp:HiddenField runat="server" ID="h_adjId" />
        <asp:HiddenField runat="server" ID="h_adjFirstName" />
        <asp:HiddenField runat="server" ID="h_adjLastName" />
        <asp:HiddenField runat="server" ID="h_allocated" />
        <asp:HiddenField runat="server" ID="h_available" />
        <asp:HiddenField runat="server" ID="h_billamt" />
        <asp:HiddenField runat="server" ID="h_billable" />
        <asp:HiddenField runat="server" ID="h_Billed" />
        <asp:HiddenField runat="server" ID="h_claimanteid" /><%-- MITS 16608 --%>
        <asp:HiddenField runat="server" ID="h_comment" />
        <asp:HiddenField runat="server" ID="h_fromdate" />
        <asp:HiddenField runat="server" ID="h_id" />
        <asp:HiddenField runat="server" ID="h_invnum" />
        <asp:HiddenField runat="server" ID="h_Invoiced" />
        <asp:HiddenField runat="server" ID="h_invoicetotal" />
        <asp:HiddenField runat="server" ID="h_itemAmt" />
        <asp:HiddenField runat="server" ID="h_maxtrackid" />
        <asp:HiddenField runat="server" ID="h_nonbillreason" />
        <asp:HiddenField runat="server" ID="h_nonbillreasoncode" />
        <asp:HiddenField runat="server" ID="h_override" />
        <asp:HiddenField runat="server" ID="h_pid" />
        <asp:HiddenField runat="server" ID="h_postable" />
        <asp:HiddenField runat="server" ID="h_postback" />
        <asp:HiddenField runat="server" ID="h_precomment" />
        <asp:HiddenField runat="server" ID="h_qty" />
        <asp:HiddenField runat="server" ID="h_rate" />
        <asp:HiddenField runat="server" ID="h_ratecode" />
        <asp:HiddenField runat="server" ID="h_ratetableid" />
        <asp:HiddenField runat="server" ID="h_ratetablename" />
        <asp:HiddenField runat="server" ID="h_reason" />
        <asp:HiddenField runat="server" ID="h_reservetype" />
        <asp:HiddenField runat="server" ID="h_reservetypecode" />
        <asp:HiddenField runat="server" ID="h_save" />
        <asp:HiddenField runat="server" ID="h_save_rowid" />
        <asp:HiddenField runat="server" ID="h_status" />    
        <asp:HiddenField runat="server" ID="h_todate" />
        <asp:HiddenField runat="server" ID="h_trackid" />
        <asp:HiddenField runat="server" ID="h_trackmode" />
        <asp:HiddenField runat="server" ID="h_transtype" />
        <asp:HiddenField runat="server" ID="h_transtypecode" />
        <asp:HiddenField runat="server" ID="h_unit" />
        <asp:HiddenField runat="server" ID="h_unitid" /><%-- MITS 16608 --%>
        <asp:HiddenField runat="server" ID="h_vendor" />
        <asp:HiddenField runat="server" ID="hclmnum" />
        <asp:HiddenField runat="server" ID="hCurrentBillableReason" />
        <asp:HiddenField runat="server" ID="hCurrentIsBillable" />
        <asp:HiddenField runat="server" ID="hCurrentItemAmount" />
        <asp:HiddenField runat="server" ID="hCurrentRate" />
        <asp:HiddenField runat="server" ID="hCurrentRowId" />
        <asp:HiddenField runat="server" ID="hCurrentTransType" />
        <asp:HiddenField runat="server" ID="hCurrentUnit" />
        <asp:HiddenField runat="server" ID="hRowId" />
        <asp:HiddenField runat="server" ID="hSaveMode" />
        <asp:HiddenField runat="server" ID="mode" />
        <asp:HiddenField runat="server" ID="NewRateDetailAdded" />
        <asp:HiddenField runat="server" ID="req_fields" />
        <asp:HiddenField runat="server" ID="SysFormName" />
        <asp:HiddenField runat="server" ID="trackid" />
        <asp:HiddenField runat="server" ID="user" />
        <asp:HiddenField runat="server" ID="validated" />
        <asp:HiddenField runat="server" ID="vendor_cid" />
        <asp:HiddenField runat="server" ID="vendor_tableid" />
        <asp:HiddenField runat="server" ID="FormName" Value="TimeAndExpense" />    
        <asp:HiddenField runat="server" ID="hUseMultiCurrency" Value="" />        
        <asp:textbox style="display: none" runat="server" id="FrmName" rmxref="Instance/Document/TimeExpense/FrmName" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId1" rmxref="Instance/Document/TimeExpense/TableId1" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId2" rmxref="Instance/Document/TimeExpense/TableId2" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId3" rmxref="Instance/Document/TimeExpense/TableId3" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="AllowClosedClaim" rmxref="Instance/Document/TimeExpense/AllowClosedClaim" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="RateTableid" rmxref="Instance/Document/TimeExpense/RateTableid" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="FirstTime" rmxref="Instance/Document/TimeExpense/FirstTime" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="currencytype" rmxtype="hidden" />
        </div>
    </form>
</body>
</html>