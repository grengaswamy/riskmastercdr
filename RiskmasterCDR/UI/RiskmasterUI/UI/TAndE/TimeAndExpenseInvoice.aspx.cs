﻿using System;
using System.Data;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using System.Threading;
using System.Globalization;
using MultiCurrencyCustomControl;

namespace Riskmaster.UI.TAndE
{
    public partial class TimeAndExpenseInvoice : NonFDMBasePageCWS
    {
        #region public variables
        private int iAdjusterId            = 0;
        private int iControlId             = 0;//MITS 16270
        private int CurrentInvoiceDetailId = 0;
        private decimal dTotalInvoiced     = 0.00m;
        private string sMode               = String.Empty;
        private string sRowId              = String.Empty;
        private string sInvNum             = String.Empty;
        private string sClaimId            = String.Empty;
        private string sClaimNum           = String.Empty;
        private string sClaimantEid        = String.Empty;//MITS 16608
        private string sUnitId             = String.Empty;//MITS 16608
        private string sRateTable          = String.Empty;
        private string sRateTableId        = String.Empty;
        private string sReturnValue        = String.Empty;
        private string sIsAllocated        = String.Empty;
        private string sTrackIdValue       = String.Empty;
        private string sEfDate             = String.Empty;
        private string sExDate             = String.Empty;
        private string sFirstName          = String.Empty;
        private string sLastName           = String.Empty;
        private string sCurrencyTypeText = String.Empty;//rupal: r8 multicurrency
        private string sCurrencyTypeCodeId = String.Empty;//rupal: r8 multicurrency


        public enum gridviewColumns { transtype_link, invamt, billable, billamt, deletedetail_link, transtype, reservetype, 
                                      hInvamt, hBillable, unit, rate, nonbillreason, qty, postable, hTrackId, transtypecode,
                                      hBillAmt, reservetypecode, invtot, fromdate, todate, fromdateFormatted, todateFormatted,
                                      hOverRide, nonbillreasoncode, comment, precomment, status, reason, id, pid, invnum };
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //MGaba2:MITS 16166:Clearing it so that no error should be shown when page is getting refreshed
            ErrorControl1.errorDom = "";
            StringBuilder sbAddDetailOnClick = new StringBuilder();

            // MITS 15624 - MAC
            //btnVendorLookup.OnClientClick = "javascript:lookupData('vendor', '', 4, 'vendor', 2); return false;";
            btnVendorLookup.OnClientClick = "CheckAllocatedForLookup(); return false;";

            //Start - Yukti-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }

            if (IsPostBack)
            {
                bodyTandEInvoice.Attributes["onload"] = "setInvoicePage(); CheckIsAllocated();";

                sClaimNum    = hclmnum.Value;
                sIsAllocated = h_allocated.Value;
                sRateTable   = h_ratetablename.Value;//MITS 15559 by MJP
                sRateTableId = h_ratetableid.Value;

                //if we are in postback mode, check if we're saving, otherwise, ignore...
                if (h_postback.Value == "savedetails")
                {
                    h_postback.Value = String.Empty;

                    if (deleteflag.Value == "true")
                    {
                        deleteflag.Value = String.Empty;
                        sTrackIdValue = trackid.Value;
                    }//if

                    XElement xmlDataRequest = GetXMLMessageTemplate("save");
                    XElement xmlUpdatedTemplate = UpdateDetailsXML(xmlDataRequest);

                    BindGridView(xmlUpdatedTemplate.ToString());

                    //hSaveMode.Value = String.Empty;
                    h_trackmode.Value = String.Empty;
                }//if
                //rupal:start,multicurrency
                else if (h_postback.Value == "currencytype")
                {

                }
                //rupal:end
            }//if
            else
            {
                sMode        = AppHelper.GetQueryStringValue("mode").ToLower();
                sRowId       = AppHelper.GetQueryStringValue("rowid");
                sClaimId     = AppHelper.GetQueryStringValue("claimid");
                sClaimNum    = AppHelper.GetQueryStringValue("claimnum");
                sClaimantEid = AppHelper.GetQueryStringValue("ClaimantEid");//MITS 16608
                sUnitId      = AppHelper.GetQueryStringValue("UnitId");//MITS 16608
                sRateTableId = AppHelper.GetQueryStringValue("ratetableid");

                // MITS 15872 - MAC
                // -- START --
                string[] aRateTableId;
                if ((!String.IsNullOrEmpty(sRateTableId)) && (aRateTableId = sRateTableId.Split(',')).Length > 1)
                {
                    sRateTableId = aRateTableId[0];
                    //Added for Mits 19138:RateTableId not getting passed in case like sRateTableId=",1"
                    //Happens when a Rate Table whose expiration date is less than current date is selected and we edit any invoice.
                    if (String.IsNullOrEmpty(sRateTableId))
                        sRateTableId = aRateTableId[1];
                    //Added for Mits 19138:RateTableId not getting passed in case like sRateTableId=",1"
                }//if
                // -- END --

                h_allocated.Value     = AppHelper.GetQueryStringValue("IsAllocated");
                h_ratetablename.Value = AppHelper.GetQueryStringValue("RateTable");//MITS 15559 by MJP
                claimid.Value         = sClaimId;
                hclmnum.Value         = sClaimNum;
                hSaveMode.Value       = sMode;
                h_trackmode.Value     = sMode;
                h_claimanteid.Value   = sClaimantEid;//MITS 16608
                h_unitid.Value        = sUnitId;//MITS 16608
                h_ratetableid.Value   = sRateTableId;
                sIsAllocated          = h_allocated.Value;

                if ((sMode == "edit" || sMode == "new"))
                {
                    try
                    {
                        if (sMode == "edit")
                        {
                            //h_invoicetotal.Value = 
                            bodyTandEInvoice.Attributes["onload"] = "setInvoicePage(); CheckIsAllocated();";

                            optallocate.Enabled = false;
                            sReturnValue = GetXMLData();
                        }//if
                        else
                        {
                            bodyTandEInvoice.Attributes["onload"] = "setInvoicePage(); CheckIsAllocated();";
                            optallocate.Checked = true;
                            optallocate.Enabled = true;
                          
                            sReturnValue = GetXMLDataAndDate();

                            XElement xeReturn            = XElement.Parse(sReturnValue);
                            XElement xeVerificationDates = xeReturn.XPathSelectElement("//TandEVerificationDates");
                            XElement xeCurrentAdjuster   = xeReturn.XPathSelectElement("//CurrentAdjuster");
                            //rupal:start, r8 multicurrency
                            XElement xeUseMultiCurrency = xeReturn.XPathSelectElement("//UseMultiCurrency");
                            if (xeUseMultiCurrency != null)
                            {
                                hUseMultiCurrency.Value = xeUseMultiCurrency.Attribute("value").Value;
                            }
                            XElement xeCurrencyType = xeReturn.XPathSelectElement("//CurrencyType");
                            if (xeCurrencyType != null)
                            {
                                sCurrencyTypeText = xeCurrencyType.Attribute("value").Value;
                                sCurrencyTypeCodeId = xeCurrencyType.Attribute("codeid").Value;
                                currencytype.Text = xeCurrencyType.Attribute("culture").Value;//culture
                                InitializeCulture();
                            }
                            
                            //totbill.Text        = "Billed : $0.00";
                            //totinvoice.Text     = "Invoiced : $0.00";
                            totbill.Text = "Billed : " + ConvertDoubleValueToCurrencyFormat("0");
                            totinvoice.Text = "Invoiced : " + ConvertDoubleValueToCurrencyFormat("0");
                            
                            //rupal:end
                            if (xeVerificationDates != null)
                            {
                                sEfDate = xeVerificationDates.Attribute("efdate").Value;
                                sExDate = xeVerificationDates.Attribute("exdate").Value;
                            } // if

                            if (xeCurrentAdjuster != null)
                            {
                                int.TryParse(xeCurrentAdjuster.Attribute("Id").Value, out iAdjusterId);
                                sFirstName = xeCurrentAdjuster.Attribute("FirstName").Value;
                                sLastName = xeCurrentAdjuster.Attribute("LastName").Value;
                            } // if
                            
                            sReturnValue = GetXMLMessageTemplate("new").ToString();
                        }//else sMode is new

                        vendor.Enabled          = (String.IsNullOrEmpty(vendor.Text)) ? false : true;
                        clmNumber.Enabled       = false;
                        totavailable.Enabled    = false;
                        btnVendorLookup.Enabled = true;
                        //invAmt.Attributes.Add("onblur", ";currencyLostFocus(this);setDataChangedInv('invAmt');");

                        BindGridView(sReturnValue);
                    }//try
                    catch (Exception objException)
                    {
                        ErrorHelper.logErrors(objException);
                        BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                        baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                    }//catch
                }//if
            }//else

            //this builds the hyperlink for the 'Add Detail' link
            sbAddDetailOnClick.Append("javascript:window.open('../../UI/TAndE/TimeAndExpenseDetails.aspx?ctlnumber=add");
            sbAddDetailOnClick.AppendFormat("&maxtrackid={0}&ratetableid={1}&ClaimantEid={2}&UnitId={3}", h_maxtrackid.Value, sRateTableId, h_claimanteid.Value, h_unitid.Value);//MITS 16608
            sbAddDetailOnClick.AppendFormat("&IsAllocated={0}&claimnum={1}&invtot={2}", sIsAllocated, sClaimNum, invAmt.Text);
            //rupal:start,r8 multicurrency
            sbAddDetailOnClick.AppendFormat("&culture={0}", currencytype.Text);
            //rupal:end
            sbAddDetailOnClick.Append("','TandE','width=820,height=550,resizable=yes,scrollbars=yes');void(0);");
            hlAddDetail.Attributes.Add("onmouseover", "this.style.cursor='hand'");
            hlAddDetail.Attributes.Add("onmouseout", "this.style.cursor='default'");
            hlAddDetail.NavigateUrl = sbAddDetailOnClick.ToString();
            //rupal:multicurrency
            if (gvInvoiceDetails.Rows.Count == 1)
            {
                currencytypetext.Enabled = true;
                totbill.Text = "Billed : " + ConvertDoubleValueToCurrencyFormat("0");
                totinvoice.Text = "Invoiced : " + ConvertDoubleValueToCurrencyFormat("0");
            }
            else
            {
                currencytypetext.Enabled = false;
            }
            currencytypetext.Visible = false;
            /*
            if (hUseMultiCurrency.Value != "0")
            {
                currencytypetext.Visible = true;
            }
            else
            {
                currencytypetext.Visible = false;
            }
             * */
            //rupal:end
        } // method: Page_Load

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sReturnValue"></param>
        private void BindGridView(string sReturnValue)
        {
            int iMaxId = 0;
            DataTable dtOption = null;

            //fill xmldocument from the string xml
            XmlDocument xdocInvoices = new XmlDocument();
            xdocInvoices.LoadXml(sReturnValue);
            //rupal:start,multicurrency
            XmlNode xnGetCurrencyType = xdocInvoices.SelectSingleNode("//group[@name='Invoice']/control[@name='currencytypetext']");
            if (xnGetCurrencyType != null)
            {
                if (xnGetCurrencyType.Attributes["value"] != null)
                {
                    sCurrencyTypeText = xnGetCurrencyType.Attributes["value"].Value;
                }
                if (xnGetCurrencyType.Attributes["codeid"] != null)
                {
                    sCurrencyTypeCodeId = xnGetCurrencyType.Attributes["codeid"].Value;
                }
                if (xnGetCurrencyType.Attributes["culture"] != null)
                {
                    currencytype.Text = xnGetCurrencyType.Attributes["culture"].Value;//culture
                }
                InitializeCulture();
            }
            //rupal:end

            XmlNode xnGetInvAmt = xdocInvoices.SelectSingleNode("//group[@name='Invoice']/control[@name='invAmt']");
            h_invoicetotal.Value = xnGetInvAmt.Attributes["value"].Value;

            XElement xeReturnValue = XElement.Parse(sReturnValue);
            XElement xeInvDetails = xeReturnValue.XPathSelectElement("//control[@name='invDetail']");

            XmlNode xnGetMaxTrackId = xdocInvoices.SelectSingleNode("//control[@name='invDetail']");
            int.TryParse(xnGetMaxTrackId.Attributes["maxtrackid"].Value, out iMaxId);

            h_maxtrackid.Value = iMaxId.ToString();

            //if the attribute /transtype=''/ does NOT exist, insert a blank option row
            XElement xeCheckIfValueExists = xeInvDetails.XPathSelectElement("//option[@transtype='']");
            if (xeCheckIfValueExists == null)
            {
                //insert blank row
                StringBuilder sbBlankOption = new StringBuilder();
                sbBlankOption.AppendFormat(@"<option bgcolor='' claimnum='' trackid='{0}' id='' transtype='' ", h_maxtrackid.Value);
                sbBlankOption.Append(@"transtypecode='' invamt='' billamt='' billable='' reason='' reservetype='' ");
                sbBlankOption.Append(@"reservetypecode='' invtot='' invnum='' fromdate='' fromdateFormatted='' ");
                sbBlankOption.Append(@"todateFormatted='' todate='' unit='' rate='' override='' nonbillreason='' ");
                sbBlankOption.AppendFormat(@"nonbillreasoncode='' comment='' precomment='' qty='' pid='{0}' postable='' ", h_pid.Value);
                sbBlankOption.Append(@"status='N' invtotavail=''/>");

                xeInvDetails.Add(XElement.Parse(sbBlankOption.ToString()));
            }//if
            
            xdocInvoices.LoadXml(xeReturnValue.ToString());

            //bind aspx controls with xml response
            BindInvoiceControls(xdocInvoices);

            //fill xmlnode with a single node from the xmldocument
            XmlNode xnFormGroupNode = xdocInvoices.SelectSingleNode("//Document//form/group");
            string sGridData = xnFormGroupNode.NextSibling.OuterXml;

            //fill xmldocument from the string xml
            XmlDocument xdGroupControlOption = new XmlDocument();
            xdGroupControlOption.LoadXml(sGridData);

            //fill xmlnode with a single node from the xmldocument
            XmlNode xnGroupControlOptionNode = xdGroupControlOption.SelectSingleNode("//group");

            //fill dataset with xmlnode object
            DataSet dsOptionSet = new DataSet();
            dsOptionSet.ReadXml(new XmlNodeReader(xnGroupControlOptionNode));

            #region xml for debug
            //<control col1="Transaction Type"              //  col2="invoice Amount" 
            //         col3="billable"                      //  col4="Bill Amount" 
            //         islink="yes"                         //  isrequ="yes" 
            //         linkpage="TimeAndExpensesDetails"    //  maxtrackid="11" 
            //         name="invDetail"                     //  title="Invoice Details" 
            //         type="invDetail"                     //  visible="1">
            #endregion
            DataTable dtControl = dsOptionSet.Tables["control"];

            if (dsOptionSet.Tables["option"] != null)
            {
                decimal dBilled   = 0.00m;
                decimal dInvoiced = 0.00m;

                dtOption = dsOptionSet.Tables["option"];

                #region xml for debug
                //<option bgcolor="data2"                   //  claimnum="GC6666000114" 
                //        trackid="1"                       //  id="20" 
                //       *transtype="Expert Witness Fees"   //  transtypecode="2357" 
                //       *invamt="26"                       // *billamt="1" 
                //       *billable="True"                   //  reason="" 
                //        reservetype="E Expense"           //  reservetypecode="368" 
                //        invtot="26"                       //  invnum="" 
                //        fromdate=""                       //  fromdateFormatted="" 
                //        todateFormatted=""                //  todate="" 
                //        unit="HOUR Hour"                  //  rate="1" 
                //        override="False"                  //  nonbillreason="&quot; &quot;" 
                //        nonbillreasoncode="0"             //  comment="" 
                //        precomment=""                     //  qty="1" 
                //        pid="10"                          //  postable="1" 
                //        status="E"                        //  invtotavail=""/>
                #endregion

                //add columns to the datatable manually so we can retrieve values from the gridview later with javascript
                dtOption.Columns.Add("hInvamt",   typeof(string));
                dtOption.Columns.Add("hBillable", typeof(string));
                dtOption.Columns.Add("hTrackId",  typeof(string));
                dtOption.Columns.Add("hBillAmt",  typeof(string));
                dtOption.Columns.Add("hOverRide", typeof(string));

                int iCurRow = 0;//MITS 16270

                //loop through each invoice/node
                foreach (DataRow d in dtOption.Rows)
                {
                    bool isBillAmt   = false;
                    bool isBillable  = false;
                    string sBillAmt  = String.Empty;
                    string sBillable = String.Empty;

                    //d["trackid"]   = (sMode == "new") ? h_maxtrackid.Value : d["trackid"].ToString();
                    d["trackid"] = (sMode == "new") ? h_maxtrackid.Value : iCurRow.ToString();
                    d["hTrackId"]  = d["trackid"].ToString();
                    iCurRow++;//MITS 16270

                    d["hInvamt"]   = d["invamt"].ToString();
                    d["hBillable"] = d["billable"].ToString().ToLower();
                    d["hBillAmt"]  = d["billamt"].ToString();
                    d["hOverRide"] = d["override"].ToString();
                    d["billable"]  = convertBillableToYesNo(d["billable"].ToString().ToLower());
                    sBillable      = d["billable"].ToString();
                    isBillable     = (sBillable == "No") ? false : true;

                    //rupal:start, multicurrency
                    //d["invamt"]    = ConvertToUSD(d["invamt"].ToString(), false, false);
                    //d["billamt"]   = ConvertToUSD(d["billamt"].ToString(), true, isBillable);

                    d["invamt"] = ConvertDoubleValueToCurrencyFormat(d["invamt"].ToString());
                    d["billamt"] = ConvertDoubleValueToCurrencyFormat(d["billamt"].ToString());
                    //rupal:end
                    
                    if ((!String.IsNullOrEmpty(d["hBillAmt"].ToString())) && isBillable)
                    {
                        decimal dCurBillAmt = 0.00m;
                        //rupal
                        decimal.TryParse(ConvertToDouble(d["hBillAmt"].ToString()).ToString(), out dCurBillAmt);
                        dBilled += dCurBillAmt;
                    }//if

                    if (!String.IsNullOrEmpty(d["hInvamt"].ToString()))
                    {
                        decimal dCurInvAmt = 0.00m;
                        //rupal
                        //decimal.TryParse(d["hInvamt"].ToString(), out dCurInvAmt);
                        decimal.TryParse(ConvertToDouble(d["hInvamt"].ToString()).ToString(), out dCurInvAmt);
                        dInvoiced += dCurInvAmt;
                    }//if
                }//foreach
                                
                totbill.Text    = String.Format("Billed : {0:C}",   dBilled);
                totinvoice.Text = String.Format("Invoiced : {0:C}", dInvoiced);

                //binds the datatable to the gridview
                gvInvoiceDetails.Visible             = true;
                gvInvoiceDetails.DataSource          = dtOption.DefaultView;
                gvInvoiceDetails.AutoGenerateColumns = false;
                gvInvoiceDetails.DataBind();

                pnlNewInvoice.Visible = (sMode == "new") ? true : false;
            }
        } // method: BindGridView

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveAndClose_Click(object sender, EventArgs e)
        {
            bool bXmlSaved = false;
            bool bIsValidated = (validated.Value.ToLower() == "true") ? true : false;

            if (bIsValidated)
            {
                bXmlSaved = SaveXML();
            }//if

            if (bXmlSaved)
            {
                hSaveMode.Value = String.Empty;
                h_trackmode.Value = String.Empty;

                //TODO: not sure if Server.Transfer method is better or worse than the Response.Redirect...
                //Server.Transfer(String.Format("../../UI/TAndE/TimeAndExpense.aspx?ClaimId={0}&ClaimantEid={1}", claimid.Value, h_claimanteid.Value), false);

                //MITS 16608 added ClaimantEid and UnitId to pass back to the main T&E page - MJP
                Response.Redirect(String.Format("../../UI/TAndE/TimeAndExpense.aspx?ClaimId={0}&ClaimantEid={1}&UnitId={2}", claimid.Value, h_claimanteid.Value, h_unitid.Value), true);
            }//if
        } // method: SaveAndClose_Click

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool SaveXML()
        {
            bool bIsSaved = false;

            try
            {
                sReturnValue = SaveXMLData();
                //MGaba2:MITS 16166: No alert for frozen payments
                //Checking whether there is any error from business layer or not
//Depending on some error is thrown from business layer or not,bsaved should have value accordingly
                bIsSaved= ErrorHelper.IsCWSCallSuccess(sReturnValue);                
               // bIsSaved = true;
            }//try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch

            return bIsSaved;
        } // method: SaveXML

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string SaveXMLData()
        {
            string sRequestReturn       = String.Empty;
            XElement xmlDataRequest     = null;
            XElement xmlUpdatedTemplate = null;

            FrmName.Text = "save";
            AllowClosedClaim.Text = "true"; //TODO: where does this get set from?

            try
            {
                //update xml template
                xmlDataRequest = GetXMLMessageTemplate("save");
                xmlUpdatedTemplate = UpdateDetailsXML(xmlDataRequest);

                //delete the blank option node if it exists
                XElement xeCheckIfValueExists = xmlUpdatedTemplate.XPathSelectElement("//option[@transtype='']");
                if (xeCheckIfValueExists != null)
                {
                    xeCheckIfValueExists.Remove();
                }//if

                //execute CallCWS function with updated template xml and parameters
                CallCWS("TimeExpenseAdaptor.SaveInvoiceXml", xmlUpdatedTemplate, out sRequestReturn, false, false);
            }//try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch

            return sRequestReturn;
        } // method: SaveXMLData

        /// <summary>
        /// 
        /// </summary>
        /// <param name="XMLTandEDetails"></param>
        /// <returns></returns>
        private XElement UpdateDetailsXML(XElement XMLTandEDetails)
        {
            bool bDeleteFlag = false;

            try
            {
                XElement xeTable = XMLTandEDetails.XPathSelectElement("//Document/TimeExpense/InputXml/form/group[@name='InvDetail']/control[@name='invDetail']");

                foreach (GridViewRow gvrInvoiceDetail in gvInvoiceDetails.Rows)
                {
                    #region initialize local variables
                    int iMaxIdValue                = 0;
                    int iTrackIdValue              = 0;
                    double dInvTotValue            = 0.0;
                    double dInvAmtValue            = 0.0;
                    double dInvTotAvailValue       = 0.0;
                    string sIdValue                = String.Empty;
                    string sPidValue               = String.Empty;
                    string sCommentValue           = String.Empty;
                    string sPreCommentValue        = String.Empty;
                    string sInvNumValue            = String.Empty;
                    string sNonBillReasonValue     = String.Empty;
                    string sQtyValue               = String.Empty;
                    string sRateValue              = String.Empty;
                    string sUnitValue              = String.Empty;
                    string sReserveTypeValue       = String.Empty;
                    string sReserveTypeCodeValue   = String.Empty;
                    string sTransTypeValue         = String.Empty;
                    string sTransTypeCodeValue     = String.Empty;
                    string sReasonValue            = String.Empty;
                    string sNonBillReasonCodeValue = String.Empty;
                    string sPostableValue          = String.Empty;
                    string sOverrideValue          = String.Empty;
                    string sBillAmtValue           = String.Empty;
                    string sBillableValue          = String.Empty;
                    string sToDateValue            = String.Empty;
                    string sFromDateValue          = String.Empty;
                    string sToDateFormattedValue   = String.Empty;
                    string sFromDateFormattedValue = String.Empty;
                    string sInvTotValue            = String.Empty;
                    string sInvAmtValue            = String.Empty;
                    string sHTrackIdValue          = String.Empty;
                    #endregion

                    XElement xeRow = new XElement("option");

                    sPostableValue = ((HiddenField)gvrInvoiceDetail.FindControl("postable")).Value.ToLower();
                    switch (sPostableValue)
                    {
                        case "0":
                        case "false":
                            sPostableValue = "0";
                            break;
                        default:
                            sPostableValue = "1";
                            break;
                    }//switch

                    sOverrideValue = ((HiddenField)gvrInvoiceDetail.FindControl("hOverRide")).Value.ToLower();
                    switch (sOverrideValue)
                    {
                        case "0":
                        case "false":
                            sOverrideValue = "0";
                            break;
                        default:
                            sOverrideValue = "1";
                            break;
                    }//switch

                    sBillableValue = ((HiddenField)gvrInvoiceDetail.FindControl("hBillable")).Value.ToLower();
                    switch (sBillableValue)
                    {
                        case "0":
                        case "false":
                            sBillableValue = "0";//sBillableValue = "False";
                            break;
                        default:
                            sBillableValue = "1";//sBillableValue = "True";
                            break;
                    }//switch

                    sBillAmtValue = ((HiddenField)gvrInvoiceDetail.FindControl("hBillAmt")).Value;
                    //rupal:multicurrency
                    //sBillAmtValue = Regex.Replace(sBillAmtValue, @"[\$,]+", String.Empty);

                    sToDateFormattedValue = ((HiddenField)gvrInvoiceDetail.FindControl("todateFormatted")).Value;
                    sToDateValue = sToDateFormattedValue;
                    if (!String.IsNullOrEmpty(sToDateValue))
                    {
                        sToDateValue = Regex.Replace(sToDateValue, @"^(\d{2})/(\d{2})/(\d{4})$", "$3$1$2");
                    }//if

                    sFromDateFormattedValue = ((HiddenField)gvrInvoiceDetail.FindControl("fromdateFormatted")).Value;
                    sFromDateValue = sFromDateFormattedValue;
                    if (!String.IsNullOrEmpty(sFromDateValue))
                    {
                        sFromDateValue = Regex.Replace(sFromDateValue, @"^(\d{2})/(\d{2})/(\d{4})$", "$3$1$2");
                    }//if

                    sInvTotValue = ((HiddenField)gvrInvoiceDetail.FindControl("invtot")).Value;
                    if (!String.IsNullOrEmpty(sInvTotValue))
                    {
                        //rupal:,ulticurrency
                        //dInvTotValue = Convert.ToDouble(sInvTotValue);
                        dInvTotValue = Convert.ToDouble(ConvertToDouble(sInvTotValue));
                    }//if

                    sInvAmtValue = ((HiddenField)gvrInvoiceDetail.FindControl("hInvamt")).Value;
                    if (!String.IsNullOrEmpty(sInvAmtValue))
                    {
                        //rupal:multicurrency
                        //dInvAmtValue = Convert.ToDouble(sInvAmtValue);
                        dInvAmtValue = Convert.ToDouble(ConvertToDouble(sInvAmtValue));
                    }//if
                   
                    dInvTotAvailValue = (dInvTotValue - dInvAmtValue);

                    if (!String.IsNullOrEmpty(h_maxtrackid.Value))
                    {
                        iMaxIdValue = Convert.ToInt16(h_maxtrackid.Value);
                    }//if

                    sHTrackIdValue = ((HiddenField)gvrInvoiceDetail.FindControl("hTrackId")).Value;
                    if (!String.IsNullOrEmpty(sHTrackIdValue))
                    {
                        iTrackIdValue = Convert.ToInt16(sHTrackIdValue);
                    }//if

                    if (bDeleteFlag)
                    {
                        iTrackIdValue--;
                    }//if

                    sIdValue                = ((HiddenField)gvrInvoiceDetail.FindControl("id")).Value;
                    sPidValue               = ((HiddenField)gvrInvoiceDetail.FindControl("pid")).Value;
                    sInvNumValue            = ((HiddenField)gvrInvoiceDetail.FindControl("invnum")).Value;
                    sCommentValue           = ((HiddenField)gvrInvoiceDetail.FindControl("comment")).Value;
                    sPreCommentValue        = ((HiddenField)gvrInvoiceDetail.FindControl("precomment")).Value;
                    sNonBillReasonValue     = ((HiddenField)gvrInvoiceDetail.FindControl("nonbillreason")).Value;
                    sNonBillReasonCodeValue = ((HiddenField)gvrInvoiceDetail.FindControl("nonbillreasoncode")).Value;
                    sQtyValue               = ((HiddenField)gvrInvoiceDetail.FindControl("qty")).Value;
                    sRateValue              = ((HiddenField)gvrInvoiceDetail.FindControl("rate")).Value;
                    sUnitValue              = ((HiddenField)gvrInvoiceDetail.FindControl("unit")).Value;
                    sReasonValue            = ((HiddenField)gvrInvoiceDetail.FindControl("reason")).Value;
                    sReserveTypeValue       = ((HiddenField)gvrInvoiceDetail.FindControl("reservetype")).Value;
                    sReserveTypeCodeValue   = ((HiddenField)gvrInvoiceDetail.FindControl("reservetypecode")).Value;
                    sTransTypeValue         = ((HiddenField)gvrInvoiceDetail.FindControl("transtype")).Value;
                    sTransTypeCodeValue     = ((HiddenField)gvrInvoiceDetail.FindControl("transtypecode")).Value;

                    xeRow.SetAttributeValue("bgcolor",           String.Empty);//unimportant and no longer used in R5 code
                    xeRow.SetAttributeValue("billable",          sBillableValue);
                    xeRow.SetAttributeValue("billamt",           sBillAmtValue);
                    xeRow.SetAttributeValue("claimnum",          hclmnum.Value);
                    xeRow.SetAttributeValue("comment",           sCommentValue);
                    xeRow.SetAttributeValue("fromdate",          sFromDateValue);
                    xeRow.SetAttributeValue("fromdateFormatted", sFromDateFormattedValue);
                    xeRow.SetAttributeValue("id",                sIdValue);
                    xeRow.SetAttributeValue("invamt",            sInvAmtValue);
                    xeRow.SetAttributeValue("invnum",            sInvNumValue);
                    xeRow.SetAttributeValue("invtot",            sInvTotValue);
                    xeRow.SetAttributeValue("invtotavail",       dInvTotAvailValue.ToString());
                    xeRow.SetAttributeValue("nonbillreason",     sNonBillReasonValue);
                    xeRow.SetAttributeValue("nonbillreasoncode", sNonBillReasonCodeValue);
                    xeRow.SetAttributeValue("override",          sOverrideValue);
                    xeRow.SetAttributeValue("pid",               sPidValue);
                    xeRow.SetAttributeValue("postable",          sPostableValue);
                    xeRow.SetAttributeValue("precomment",        sPreCommentValue);
                    xeRow.SetAttributeValue("qty",               sQtyValue);
                    xeRow.SetAttributeValue("rate",              sRateValue);
                    xeRow.SetAttributeValue("reason",            sReasonValue);
                    xeRow.SetAttributeValue("reservetype",       sReserveTypeValue);
                    xeRow.SetAttributeValue("reservetypecode",   sReserveTypeCodeValue);
                    xeRow.SetAttributeValue("status",            "N");
                    xeRow.SetAttributeValue("todate",            sToDateValue);
                    xeRow.SetAttributeValue("todateFormatted",   sToDateFormattedValue);
                    xeRow.SetAttributeValue("trackid",           iTrackIdValue.ToString());
                    xeRow.SetAttributeValue("transtype",         sTransTypeValue);
                    xeRow.SetAttributeValue("transtypecode",     sTransTypeCodeValue);
                    xeRow.SetAttributeValue("unit",              sUnitValue);

                    if ((!bDeleteFlag) && (xeRow.Attribute("trackid").Value.Equals(sTrackIdValue)))
                    {
                        //by doing nothing, we are essentially deleting a row from the table...
                        bDeleteFlag = true;
                        sTrackIdValue = String.Empty;
                    }//if
                    else
                    {
                        bDeleteFlag = false;
                        xeTable.Add(xeRow);
                    }//else
                }//foreach
            }//try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch

            return XMLTandEDetails;
        } // method: UpdateDetailsXML

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sMoney"></param>
        private string ConvertToUSD(string sMoney, bool isBillAmt, bool isBillable)
        {
            decimal dMoney = 0.00m;
            string sConvertedMoney = String.Empty;

            if (String.IsNullOrEmpty(sMoney))
            {
                sMoney = "0.00";
            }//if

            sMoney = Regex.Replace(sMoney, @"[,]+", String.Empty);
            Decimal.TryParse(sMoney, out dMoney);

            if (isBillAmt && !isBillable)
            {
                sMoney = String.Format("{0:C}", dMoney).TrimStart('$');
                sConvertedMoney = String.Format("$ [{0}]", sMoney);
            }//if
            else
            {
                sConvertedMoney = String.Format("{0:C}", dMoney);
            }//else

            return sConvertedMoney;
        } // method: ConvertToUSD

        //rupal:start,multicurrency
        private string ConvertDoubleValueToCurrencyFormat(string sValue)
        {
            string sConvertedValue = String.Empty;
            if (string.IsNullOrEmpty(sValue))
            {
                sValue = "0";
            }

            Double dValue = ConvertToDouble(sValue);
            return dValue.ToString("c");
        }
        //rupal:end

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string convertBillableToYesNo(string value)
        {
            switch (value)
            {
                case "0":
                case "false":
                    value = "No";
                    break;
                default:
                    value = "Yes";
                    break;
            }//switch

            return value;
        } // method: convertBillableToYesNo

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xdocInvoices"></param>
        private void BindInvoiceControls(XmlDocument xdocInvoices)
        {
            foreach (Control oCtrl in this.Form.Controls)
            {
                string sType = oCtrl.GetType().Name.ToLower();
                
                //get Invoice group data
                XmlNode xnControlData = xdocInvoices.SelectSingleNode("//Document/form/group[@name='Invoice']/control[@name='" + oCtrl.ID + "']");

                //if null, get InvDetail group data
                if (xnControlData == null)
                {
                    xnControlData = xdocInvoices.SelectSingleNode("//Document/form//control[@name='" + oCtrl.ID + "']");
                } // if

                if (xnControlData != null)
                {
                    switch (sType)
                    {
                        case "textbox":
                            {
                                TextBox tbField = Page.FindControl(oCtrl.ID) as TextBox;
                                tbField.ID      = oCtrl.ID;
                                tbField.Enabled = true;

                                if (xnControlData.Attributes["value"] != null)
                                {
                                    if (oCtrl.ID == "invDate")
                                    {
                                        if (sMode != "new")
                                        {
                                            DateTime dtInvDate = Conversion.ToDate(xnControlData.Attributes["value"].Value);
                                            tbField.Text = String.Format("{0}/{1}/{2}", dtInvDate.Month, dtInvDate.Day, dtInvDate.Year);
                                        } // if
                                        
                                        tbField.Attributes.Add("onblur", "dateLostFocus(this.id);");
                                    } // if
                                    else if (oCtrl.ID == "vendor")
                                    {
                                        tbField.Text   = xnControlData.Attributes["value"].Value.TrimEnd(',');
                                        h_vendor.Value = xnControlData.Attributes["value"].Value.TrimEnd(',');
                                        vendor_cid.Value = xnControlData.Attributes["id"].Value;

                                        btnVendorLookup.Enabled = true;
                                        tbField.Enabled = false;

                                        //MITS 15931
                                        //if (xnControlData.Attributes["lock"] != null)
                                        //{
                                        //    if (xnControlData.Attributes["lock"].Value == "true")
                                        //    {
                                        //        btnVendorLookup.Enabled = false;
                                        //    } // if
                                        //} // if
                                    } // else if
                                    else if (oCtrl.ID == "invAmt")
                                    {
                                        tbField.Text = xnControlData.Attributes["value"].Value;
                                        //rupal:multicurrency
                                        tbField.Text = ConvertToUSD(tbField.Text, false, false); //code added by yatharth : MITS 17500 :                                        
                                        //tbField.Attributes.Add("onblur", ";currencyLostFocus(this);setDataChangedInv('invAmt');"); //Function added by yatharth : MITS 17500 :
                                        //rupal:end

                                    } // else if
                                    else if (oCtrl.ID == "invNumber")
                                    {
                                        string sInvNumber = xnControlData.Attributes["value"].Value;
                                        tbField.MaxLength = 25;//field in db is varchar(25), so we need to limit end-user input
                                        tbField.Text      = sInvNumber;
                                        h_invnum.Value    = sInvNumber;
                                    } // else if
                                    else if (oCtrl.ID == "clmNumber")
                                    {
                                        tbField.Text = xnControlData.Attributes["value"].Value;

                                        if (String.IsNullOrEmpty(hclmnum.Value))
                                        {
                                            hclmnum.Value = xnControlData.Attributes["value"].Value;
                                        } // if

                                        if (xnControlData.Attributes["lock"] != null)
                                        {
                                            if (xnControlData.Attributes["lock"].Value == "true")
                                            {
                                                tbField.Enabled = false;
                                            } // if
                                        } // if
                                    }//else if
                                    else
                                    {
                                        tbField.Text = xnControlData.Attributes["value"].Value;

                                        if (xnControlData.Attributes["lock"] != null)
                                        {
                                            if (xnControlData.Attributes["lock"].Value == "true")
                                            {
                                                tbField.Enabled = false;
                                            } // if
                                        } // if
                                    } // else
                                } // if
                            }//case: textbox
                            break;
                        case "checkbox":
                            {
                                CheckBox cbField = Page.FindControl(oCtrl.ID) as CheckBox;
                                cbField.ID       = oCtrl.ID;
                                cbField.Checked  = false;
                                cbField.Enabled  = true;

                                #region xml for debug
                                //<control Disabled = '1' 
                                //         name     = 'optallocate' 
                                //         onclick  = 'allocationSettings();' 
                                //         title    = 'Allocated' 
                                //         type     = 'checkbox' 
                                //         value    = '1' 
                                //         visible  = '1'/>
                                #endregion

                                if (xnControlData.Attributes["value"] != null)
                                {
                                    if (xnControlData.Attributes["value"].Value != "0")
                                    {
                                        cbField.Checked = true;
                                    } // if
                                } // if

                                if (xnControlData.Attributes["Disabled"] != null)
                                {
                                    if ((xnControlData.Attributes["Disabled"].Value == "1") ||  
                                        (xnControlData.Attributes["Disabled"].Value == String.Empty))
                                    {
                                        cbField.Enabled = false;
                                    } // else if
                                } // if

                                if (xnControlData.Attributes["onclick"] != null)
                                {
                                    if (!String.IsNullOrEmpty(xnControlData.Attributes["onclick"].Value))
                                    {
                                        cbField.Attributes.Add("onclick", xnControlData.Attributes["onclick"].Value);
                                    } // if
                                } // if
                            }//case: checkbox
                            break;
                        case "label":
                            {
                                Label lField = Page.FindControl(oCtrl.ID) as Label;
                                lField.ID = oCtrl.ID;
                                lField.Enabled = true;

                                if (xnControlData.Attributes["value"] != null)
                                {
                                    if (String.IsNullOrEmpty(xnControlData.Attributes["value"].Value))
                                    {
                                        lField.Text = xnControlData.InnerText;
                                    } // if
                                    else
                                    {
                                        lField.Text = xnControlData.Attributes["value"].Value;
                                    } // else
                                } // if
                                else
                                {
                                    lField.Text = xnControlData.InnerText;
                                } // else

                                if (xnControlData.Attributes["lock"] != null)
                                {
                                    if (xnControlData.Attributes["lock"].Value == "true")
                                    {
                                        lField.Enabled = false;
                                    } // if
                                } // if
                            }//case: label
                            break;
                        case "hiddenfield":
                            {
                                HiddenField cHiddenField = Page.FindControl(oCtrl.ID) as HiddenField;
                                cHiddenField.ID = oCtrl.ID;

                                if (xnControlData.Attributes["value"] != null)
                                {
                                    if (oCtrl.ID != "h_maxtrackid" && oCtrl.ID != "h_Billed")
                                    {
                                        if (String.IsNullOrEmpty(cHiddenField.Value))
                                        {
                                            cHiddenField.Value = xnControlData.Attributes["value"].Value;
                                        }//if
                                    }//if
                                }//if
                            }//case: hiddenfield                            
                            break;
                        //rupal:start, r8 multicurrency                        
                        case "ui_shared_controls_codelookup_ascx":
                            {
                                CodeLookUp objCodeLookup = (CodeLookUp)Page.FindControl(oCtrl.ID);
                                if (oCtrl.ID == "currencytypetext")
                                {
                                    if (xnControlData.Attributes["value"] != null && xnControlData.Attributes["codeid"] != null)
                                    {
                                        objCodeLookup.CodeId = xnControlData.Attributes["codeid"].Value;
                                        objCodeLookup.CodeText = xnControlData.Attributes["value"].Value;
                                    }
                                }
                            }                            
                            break;
                        case "currencytextbox":
                            {
                                CurrencyTextbox tbField = Page.FindControl(oCtrl.ID) as CurrencyTextbox;
                                tbField.ID = oCtrl.ID;
                                if (xnControlData.Attributes["value"].Value == string.Empty)
                                {
                                    tbField.AmountInString = "0";
                                }
                                else
                                {
                                    tbField.AmountInString = xnControlData.Attributes["value"].Value;
                                }
                                break;
                            }
                        //rupal:end
                        default:
                            break;
                    }//switch
                }//if
            }
        } // method: BindInvoiceControls

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetXMLData()
        {
            string sRequestReturn       = String.Empty;
            TableId1.Text               = String.Empty;
            TableId2.Text               = String.Empty;
            TableId3.Text               = String.Empty;
            XElement xmlDataRequest     = null;
            XElement xmlUpdatedTemplate = null;

            //Setup parameters for XML transmission
            FrmName.Text = "invoice";
            TableId1.Text = sRowId;
            TableId2.Text = sClaimId;
            TableId3.Text = sRateTableId;

            try
            {
                //Execute CallCWSFunction with template xml and parameters
                xmlDataRequest = GetXMLMessageTemplate("invoice");
                CallCWSFunction("TimeExpenseAdaptor.GetXmlData", out sRequestReturn, xmlDataRequest);

                XElement xeReturn = XElement.Parse(sRequestReturn);
                XElement xeInvDetails = xeReturn.XPathSelectElement("//control[@name='invDetail']");
                foreach (XElement xeOption in xeInvDetails.Elements("option"))
                {
                    int iTid = 0;
                    int.TryParse(xeOption.Attribute("trackid").Value, out iTid);
                    iTid++;
                    xeOption.Attribute("trackid").Value = iTid.ToString();
                }//foreach
                sRequestReturn = xeReturn.ToString();
            }//try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch

            return sRequestReturn;
        } // method: GetXMLData

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetXMLDataAndDate()
        {
            string sRequestReturn       = String.Empty;
            RateTableid.Text            = String.Empty;
            FirstTime.Text              = String.Empty;
            XElement xmlDataRequest     = null;
            XElement xmlUpdatedTemplate = null;

            //Setup parameters for XML transmission
            RateTableid.Text = sRateTableId;
            FirstTime.Text   = "true";

            try
            {
                //Execute CallCWSFunction with template xml and parameters
                xmlDataRequest = GetXMLMessageTemplate("firsttime");
                CallCWSFunction("TimeExpenseAdaptor.GetXmlDataAndDate", out sRequestReturn, xmlDataRequest);
            }//try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch

            return sRequestReturn;
        } // method: GetXMLDataAndDate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        private XElement GetXMLMessageTemplate(string mode)
        {
            
            StringBuilder sbTemplate = new StringBuilder();
            XElement oTemplate = null;

            if (mode == "invoice")
            {
                //rupal:start,multicurrency
                //<control isrequ='yes' name='invAmt' title='Invoice Amount' type='text' value='0.00' visible='1'/>
                #region invoice xml template
                StringBuilder sb = new StringBuilder();
                sb = sb.Append(@"<Message>
	                        <Authorization></Authorization>
	                        <Call>
		                        <Function></Function>
	                        </Call>
	                        <Document>
		                        <TimeExpense>
			                        <FrmName>invoice</FrmName>
			                        <InputXml>
				                        <form actionname='invoice.asp' bcolor='white' headingtype='main' name='frmData' sid='150' title='Time and Expense Invoice' topbuttons='1'>
					                        <body1 func='setInvoicePage()' req_func='yes'/>
					                        <group name='Invoice' title='Invoice'>
						                        <error desc='' value='0'/>
						                        <control lock='true' name='clmNumber' title='Claim Number' type='text' value='' visible='1'/>
						                        <control Disabled='1' name='optallocate' onclick='allocationSettings();' title='Allocated' type='checkbox' value='1' visible='1'/>
						                        <control buttonlink='vendor.asp' id='' isrequ='yes' name='vendor' title='Vendor' type='textandlist' value='' visible='1'/>
						                        <control name='invNumber' title='Invoice Number' type='text' value='' visible='1'/>
						                        <control isrequ='yes' name='invAmt' title='Invoice Amount' type='text' value='0' visible='1'/>
						                        <control isrequ='yes' name='invDate' title='Invoice Date' type='date' value='' visible='1'/>
						                        <control name='effsdate1' type='id' value='' visible='1'/>
						                        <control name='effedate' type='id' value='' visible='1'/>
						                        <control name='action' type='id' value='' visible='1'/>
						                        <control name='h_id' type='id' value='' visible='1'/>
						                        <control name='h_trackid' type='id' value='' visible='1'/>
						                        <control name='h_maxtrackid' type='id' value='' visible='1'/>
						                        <control name='h_trackmode' type='id' value='' visible='1'/>
						                        <control name='mode' type='id' value='new' visible='1'/>
						                        <control name='claimid' type='id' value='' visible='1'/>
						                        <control name='h_available' type='id' value='' visible='1'/>
						                        <control name='h_billamt' type='id' value='' visible='1'/>
						                        <control name='h_billable' type='id' value='' visible='1'/>
						                        <control name='h_reservetype' type='id' value='' visible='1'/>
						                        <control name='h_reservetypecode' type='id' value='' visible='1'/>
						                        <control name='h_transtype' type='id' value='' visible='1'/>
						                        <control name='h_transtypecode' type='id' value='' visible='1'/>
						                        <control name='h_itemAmt' type='id' value='' visible='1'/>
						                        <control name='h_invnum' type='id' value='' visible='1'/>
						                        <control name='h_fromdate' type='id' value='' visible='1'/>
						                        <control name='h_todate' type='id' value='' visible='1'/>
						                        <control name='user' type='id' value='' visible='1'/>
						                        <control name='h_unit' type='id' value='' visible='1'/>
						                        <control name='h_rate' type='id' value='' visible='1'/>
						                        <control name='h_override' type='id' value='' visible='1'/>
						                        <control name='h_nonbillreason' type='id' value='' visible='1'/>
						                        <control name='h_nonbillreasoncode' type='id' value='' visible='1'/>
						                        <control name='h_comment' type='id' value='' visible='1'/>
						                        <control name='h_precomment' type='id' value='' visible='1'/>
						                        <control name='h_qty' type='id' value='' visible='1'/>
						                        <control name='h_pid' type='id' value='' visible='1'/>
						                        <control name='h_reason' type='id' value='' visible='1'/>
						                        <control name='h_postable' type='id' value='' visible='1'/>
						                        <control name='h_allocated' type='id' value='1' visible='1'/>
						                        <control name='req_fields' type='id' value='vendor' visible='1'/>
						                        <control name='h_save' type='id' value='' visible='1'/>");
                sb = sb.Append(@"<control name='totinvoice' title='Invoiced' type='textlable' visible='1'>Invoiced : " + ConvertDoubleValueToCurrencyFormat("0") + "</control>");
                sb = sb.Append(@"<control name='totbill' title='Billed' type='textlable' value='' visible='1'>Billed : " + ConvertDoubleValueToCurrencyFormat("0") + "</control>");
                sb = sb.Append(@"<control name='currencytypetext' title='Currency Type:' type='text' visible='1' value='" + currencytypetext.CodeTextValue + "' codeid='" + currencytypetext.CodeIdValue + "' />");
                sb = sb.Append(@"<control name='hUseMultiCurrency' type='id' visible='1' value=''/>");
                sb = sb.Append(@"<control lock='true' name='totavailable' title='Available:' type='text' value='0' visible='1'/>
					                                            </group>
					                                            <group name='InvDetail' title=''>
						                                            <control col1='Transaction Type' col2='invoice Amount' col3='billable' col4='Bill Amount' islink='yes' isrequ='yes' linkpage='TimeAndExpensesDetails' maxtrackid='1' name='invDetail' title='Invoice Details' type='invDetail' visible='1'/>
						                                            <control name='h_Invoiced' type='id' value='0' visible='1'/>
						                                            <control name='h_Billed' type='id' value='0' visible='1'/>
						                                            <control name='h_ratecode' type='id' value='125' visible='1'/>
					                                            </group>
					                                            <button disable='' enablefornew='invoice' linkto='Invoice.asp' name='btnsave' param='' title='Save and Close' type='save' visible='1'/>
					                                            <button disable='' enablefornew='invoice' linkto='Invoice.asp' name='btncancel' title='Cancel' type='normal' visible='1'/>
				                                            </form>
			                                            </InputXml>
			                                            <TableId1></TableId1>
			                                            <TableId2></TableId2>
			                                            <TableId3></TableId3>
		                                            </TimeExpense>
	                                            </Document>
                                            </Message>");
                oTemplate = XElement.Parse(sb.ToString());
                #region commented
                /*
                oTemplate = XElement.Parse(@"<Message>
	                                            <Authorization></Authorization>
	                                            <Call>
		                                            <Function></Function>
	                                            </Call>
	                                            <Document>
		                                            <TimeExpense>
			                                            <FrmName>invoice</FrmName>
			                                            <InputXml>
				                                            <form actionname='invoice.asp' bcolor='white' headingtype='main' name='frmData' sid='150' title='Time and Expense Invoice' topbuttons='1'>
					                                            <body1 func='setInvoicePage()' req_func='yes'/>
					                                            <group name='Invoice' title='Invoice'>
						                                            <error desc='' value='0'/>
						                                            <control lock='true' name='clmNumber' title='Claim Number' type='text' value='' visible='1'/>
						                                            <control Disabled='1' name='optallocate' onclick='allocationSettings();' title='Allocated' type='checkbox' value='1' visible='1'/>
						                                            <control buttonlink='vendor.asp' id='' isrequ='yes' name='vendor' title='Vendor' type='textandlist' value='' visible='1'/>
						                                            <control name='invNumber' title='Invoice Number' type='text' value='' visible='1'/>
						                                            <control isrequ='yes' name='invAmt' title='Invoice Amount' type='text' value='0' visible='1'/>
						                                            <control isrequ='yes' name='invDate' title='Invoice Date' type='date' value='' visible='1'/>
						                                            <control name='effsdate1' type='id' value='' visible='1'/>
						                                            <control name='effedate' type='id' value='' visible='1'/>
						                                            <control name='action' type='id' value='' visible='1'/>
						                                            <control name='h_id' type='id' value='' visible='1'/>
						                                            <control name='h_trackid' type='id' value='' visible='1'/>
						                                            <control name='h_maxtrackid' type='id' value='' visible='1'/>
						                                            <control name='h_trackmode' type='id' value='' visible='1'/>
						                                            <control name='mode' type='id' value='new' visible='1'/>
						                                            <control name='claimid' type='id' value='' visible='1'/>
						                                            <control name='h_available' type='id' value='' visible='1'/>
						                                            <control name='h_billamt' type='id' value='' visible='1'/>
						                                            <control name='h_billable' type='id' value='' visible='1'/>
						                                            <control name='h_reservetype' type='id' value='' visible='1'/>
						                                            <control name='h_reservetypecode' type='id' value='' visible='1'/>
						                                            <control name='h_transtype' type='id' value='' visible='1'/>
						                                            <control name='h_transtypecode' type='id' value='' visible='1'/>
						                                            <control name='h_itemAmt' type='id' value='' visible='1'/>
						                                            <control name='h_invnum' type='id' value='' visible='1'/>
						                                            <control name='h_fromdate' type='id' value='' visible='1'/>
						                                            <control name='h_todate' type='id' value='' visible='1'/>
						                                            <control name='user' type='id' value='' visible='1'/>
						                                            <control name='h_unit' type='id' value='' visible='1'/>
						                                            <control name='h_rate' type='id' value='' visible='1'/>
						                                            <control name='h_override' type='id' value='' visible='1'/>
						                                            <control name='h_nonbillreason' type='id' value='' visible='1'/>
						                                            <control name='h_nonbillreasoncode' type='id' value='' visible='1'/>
						                                            <control name='h_comment' type='id' value='' visible='1'/>
						                                            <control name='h_precomment' type='id' value='' visible='1'/>
						                                            <control name='h_qty' type='id' value='' visible='1'/>
						                                            <control name='h_pid' type='id' value='' visible='1'/>
						                                            <control name='h_reason' type='id' value='' visible='1'/>
						                                            <control name='h_postable' type='id' value='' visible='1'/>
						                                            <control name='h_allocated' type='id' value='1' visible='1'/>
						                                            <control name='req_fields' type='id' value='vendor' visible='1'/>
						                                            <control name='h_save' type='id' value='' visible='1'/>
						                                            <control name='totinvoice' title='Invoiced' type='textlable' visible='1'>Invoiced : $0.00</control>
						                                            <control name='totbill' title='Billed' type='textlable' value='' visible='1'>Billed : $0.00</control>
						                                            <control lock='true' name='totavailable' title='Available:' type='text' value='0' visible='1'/>
					                                            </group>
					                                            <group name='InvDetail' title=''>
						                                            <control col1='Transaction Type' col2='invoice Amount' col3='billable' col4='Bill Amount' islink='yes' isrequ='yes' linkpage='TimeAndExpensesDetails' maxtrackid='1' name='invDetail' title='Invoice Details' type='invDetail' visible='1'/>
						                                            <control name='h_Invoiced' type='id' value='0' visible='1'/>
						                                            <control name='h_Billed' type='id' value='0' visible='1'/>
						                                            <control name='h_ratecode' type='id' value='125' visible='1'/>
					                                            </group>
					                                            <button disable='' enablefornew='invoice' linkto='Invoice.asp' name='btnsave' param='' title='Save and Close' type='save' visible='1'/>
					                                            <button disable='' enablefornew='invoice' linkto='Invoice.asp' name='btncancel' title='Cancel' type='normal' visible='1'/>
				                                            </form>
			                                            </InputXml>
			                                            <TableId1></TableId1>
			                                            <TableId2></TableId2>
			                                            <TableId3></TableId3>
		                                            </TimeExpense>
	                                            </Document>
                                            </Message>");
                */
                 #endregion
                #endregion
                //rupal:end
            }//if
            else if (mode == "save")
            {
                #region save xml template
                sbTemplate.Append(@"<Message>
	                                    <Authorization></Authorization>
	                                    <Call>
		                                    <Function>TimeExpenseAdaptor.SaveInvoiceXml</Function>
	                                    </Call>
	                                    <Document>
		                                    <TimeExpense>
			                                    <InputXml>
				                                    <form actionname='invoice.asp' bcolor='white' headingtype='main' name='frmData' sid='150' title='Time and Expense Invoice' topbuttons='1'>
					                                    <body1 func='setInvoicePage()' req_func='yes'/>
					                                    <group name='Invoice' title='Invoice'>
						                                    <error desc='' value='0'/>");
                sbTemplate.AppendFormat(@"<control lock='true' name='clmNumber' title='Claim Number' type='text' value='{0}' visible='1'/>", hclmnum.Value);
                string sAllocValue    = (optallocate.Checked) ? "1" : "0";
                sbTemplate.AppendFormat(@"<control Disabled='1' name='optallocate' onclick='allocationSettings();' title='Allocated' type='checkbox' value='{0}' visible='1'/>", sAllocValue);
                sbTemplate.AppendFormat(@"<control buttonlink='vendor.asp' id='{0}' isrequ='yes' name='vendor' title='Vendor' type='textandlist' value='{1}' visible='1'/>", vendor_cid.Value, SecurityElement.Escape(vendor.Text));
                sbTemplate.AppendFormat(@"<control name='invNumber' title='Invoice Number' type='text' value='{0}' visible='1'/>", SecurityElement.Escape(invNumber.Text)); //h_invnum.Value
                //rupal:multicurrency
                //sbTemplate.AppendFormat(@"<control isrequ='yes' name='invAmt' title='Invoice Amount' type='text' value='{0}' visible='1'/>", invAmt.Text);
                sbTemplate.AppendFormat(@"<control isrequ='yes' name='invAmt' title='Invoice Amount' type='text' value='{0}' visible='1'/>", invAmt.AmountInString);                
                sbTemplate.AppendFormat(@"<control isrequ='yes' name='invDate' title='Invoice Date' type='date' value='{0}' visible='1'/>", invDate.Text);
                sbTemplate.AppendFormat(@"<control name='effsdate1' type='id' value='{0}' visible='1'/>", effsdate1.Value);
                sbTemplate.AppendFormat(@"<control name='effedate' type='id' value='{0}' visible='1'/>", effedate.Value);
                sbTemplate.Append(@"<control name='action' type='id' value='' visible='1'/>");
                sbTemplate.Append(@"<control name='h_id' type='id' value='' visible='1'/>");
                sbTemplate.Append(@"<control name='h_trackid' type='id' value='' visible='1'/>");
                sbTemplate.Append(@"<control name='h_maxtrackid' type='id' value='' visible='1'/>");
                sbTemplate.AppendFormat(@"<control name='h_trackmode' type='id' value='{0}' visible='1'/>", h_trackmode.Value);
                sbTemplate.AppendFormat(@"<control name='mode' type='id' value='{0}' visible='1'/>", hSaveMode.Value);
                sbTemplate.AppendFormat(@"<control name='claimid' type='id' value='{0}' visible='1'/>", claimid.Value);
                //rupal:multicurrency
                //sbTemplate.AppendFormat(@"<control name='h_available' type='id' value='{0}' visible='1'/>", totavailable.Text);
                sbTemplate.AppendFormat(@"<control name='h_available' type='id' value='{0}' visible='1'/>", totavailable.AmountInString);

                sbTemplate.Append(@"<control name='h_billamt' type='id' value='' visible='1'/>
						            <control name='h_billable' type='id' value='' visible='1'/>
						            <control name='h_reservetype' type='id' value='' visible='1'/>
						            <control name='h_reservetypecode' type='id' value='' visible='1'/>
            						<control name='h_transtype' type='id' value='' visible='1'/>
						            <control name='h_transtypecode' type='id' value='' visible='1'/>
						            <control name='h_itemAmt' type='id' value='' visible='1'/>
						            <control name='h_invnum' type='id' value='' visible='1'/>
						            <control name='h_fromdate' type='id' value='' visible='1'/>
						            <control name='h_todate' type='id' value='' visible='1'/>");
                sbTemplate.AppendFormat(@"<control name='user' type='id' value='{0}' visible='1'/>", AppHelper.GetUserLoginName());
                sbTemplate.Append(@"<control name='h_unit' type='id' value='' visible='1'/>
						            <control name='h_rate' type='id' value='' visible='1'/>
						            <control name='h_override' type='id' value='' visible='1'/>
						            <control name='h_nonbillreason' type='id' value='' visible='1'/>
						            <control name='h_nonbillreasoncode' type='id' value='' visible='1'/>
						            <control name='h_comment' type='id' value='' visible='1'/>
						            <control name='h_precomment' type='id' value='' visible='1'/>
						            <control name='h_qty' type='id' value='' visible='1'/>");
                sbTemplate.AppendFormat(@"<control name='h_pid' type='id' value='{0}' visible='1'/>", h_pid.Value);
                sbTemplate.Append(@"<control name='h_reason' type='id' value='' visible='1'/>
						            <control name='h_postable' type='id' value='' visible='1'/>");
                sbTemplate.AppendFormat(@"<control name='h_allocated' type='id' value='{0}' visible='1'/>", h_allocated.Value);
                sbTemplate.Append(@"<control name='req_fields' type='id' value='vendor' visible='1'/>
						            <control name='h_save' type='id' value='' visible='1'/>");                
                sbTemplate.AppendFormat(@"<control name='totinvoice' title='Invoiced' type='textlable' visible='1'>{0}</control>", totinvoice.Text);
                sbTemplate.AppendFormat(@"<control name='totbill' title='Billed' type='textlable' value='' visible='1'>{0}</control>", totbill.Text);
                //rupal:multicurrency
                //sbTemplate.AppendFormat(@"<control lock='true' name='totavailable' title='Available:' type='text' value='{0}' visible='1'/>", totavailable.Text);
                sbTemplate.AppendFormat(@"<control lock='true' name='totavailable' title='Available:' type='text' value='{0}' visible='1'/>", totavailable.AmountInString);
                sbTemplate.Append(@"<control name='currencytypetext' title='Currency Type:' type='text' visible='1' value='" + currencytypetext.CodeTextValue + "' codeid='" + currencytypetext.CodeIdValue + "' />");
                //rupal:end
                sbTemplate.Append(@"</group>
					                <group name='InvDetail' title=''>");
                sbTemplate.AppendFormat(@"<control col1='Transaction Type' col2='invoice Amount' col3='billable' col4='Bill Amount' islink='yes' isrequ='yes' linkpage='TimeAndExpensesDetails' maxtrackid='{0}' name='invDetail' title='Invoice Details' type='invDetail' visible='1'>", h_maxtrackid.Value);
                sbTemplate.Append(@"</control>");
                sbTemplate.AppendFormat(@"<control name='h_Invoiced' type='id' value='{0}' visible='1'/>", h_Invoiced.Value);
                sbTemplate.AppendFormat(@"<control name='h_Billed' type='id' value='{0}' visible='1'/>", h_Billed.Value);
                sbTemplate.AppendFormat(@"<control name='h_ratecode' type='id' value='{0}' visible='1'/>", h_ratetableid.Value);
                sbTemplate.Append(@"</group>
					                <button disable='' enablefornew='invoice' linkto='Invoice.asp' name='btnsave' param='' title='Save and Close' type='save' visible='1'/>
					                <button disable='' enablefornew='invoice' linkto='Invoice.asp' name='btncancel' title='Cancel' type='normal' visible='1'/>
				                </form>
			                </InputXml>
			                <AllowClosedClaim>True</AllowClosedClaim>
		                </TimeExpense>
	                </Document>
                </Message>");

                //rupal:start, r8 multicurrency                
                //sbTemplate.Append(@"<control name='currencytypetext' title='Currency Type:' type='text' visible='1' value='" + sCurrencyTypeText + "' codeid='" + sCurrencyTypeCodeId + "' />");

                oTemplate = XElement.Parse(sbTemplate.ToString());
                #endregion
            }//else if
            else if (mode == "new")
            {
                #region new invoice xml template
                sbTemplate.Append(@"<ResultMessage>
                                        <MsgStatus>
                                            <MsgStatusCd>Success</MsgStatusCd>
                                        </MsgStatus>
                                        <Document>
                                            <form actionname='invoice.asp' bcolor='white' headingtype='main' name='frmData' sid='150' title='Time and Expense Invoice' topbuttons='1'>
	                                            <body1 func='setInvoicePage()' req_func='yes'/>
	                                            <group name='Invoice' title='Invoice'>
		                                            <error desc='' value='0'/>");
                sbTemplate.AppendFormat(@"<control lock='true' name='clmNumber' title='Claim Number' type='text' value='{0}' visible='1'/>", hclmnum.Value);
                string sAllocDisabled = (optallocate.Enabled) ? "0" : "1";
                string sAllocValue = (optallocate.Checked) ? "1" : "0";
                sbTemplate.AppendFormat(@"<control Disabled='{0}' name='optallocate' onclick='allocationSettings();' title='Allocated' type='checkbox' value='{1}' visible='1'/>", sAllocDisabled, sAllocValue);
                sbTemplate.Append(@"<control buttonlink='vendor.asp' id='' isrequ='yes' name='vendor' title='Vendor' type='textandlist' value='' visible='1' lock='' mid=''/>
		                            <control name='invNumber' title='Invoice Number' type='text' value='' visible='1'/>
		                            <control isrequ='yes' name='invAmt' title='Invoice Amount' type='text' value='' visible='1'/>
		                            <control isrequ='yes' name='invDate' title='Date' type='date' value='' visible='1'/>");
                sbTemplate.AppendFormat(@"<control name='effsdate1' type='id' visible='1' value='{0}' />", sEfDate);
                sbTemplate.AppendFormat(@"<control name='effedate' type='id' visible='1' value='{0}' />", sExDate);
                sbTemplate.Append(@"<control name='action' type='id' value='' visible='1'/>
		                            <control name='h_id' type='id' value='' visible='1'/>
		                            <control name='h_trackid' type='id' value='' visible='1'/>
		                            <control name='h_maxtrackid' type='id' value='' visible='1'/>");
                sbTemplate.AppendFormat(@"<control name='h_trackmode' type='id' value='{0}' visible='1'/>", h_trackmode.Value);
                sbTemplate.AppendFormat(@"<control name='mode' type='id' value='{0}' visible='1'/>", sMode);
                sbTemplate.AppendFormat(@"<control name='claimid' type='id' value='{0}' visible='1'/>", claimid.Value);
                sbTemplate.Append(@"<control name='h_available' type='id' value='' visible='1'/>
                                    <control name='h_billamt' type='id' value='' visible='1'/>
                                    <control name='h_billable' type='id' value='' visible='1'/>
                                    <control name='h_reservetype' type='id' value='' visible='1'/>
                                    <control name='h_reservetypecode' type='id' value='' visible='1'/>
                                    <control name='h_transtype' type='id' value='' visible='1'/>
                                    <control name='h_transtypecode' type='id' value='' visible='1'/>
                                    <control name='h_itemAmt' type='id' value='' visible='1'/>
                                    <control name='h_invnum' type='id' value='' visible='1'/>
                                    <control name='h_fromdate' type='id' value='' visible='1'/>
                                    <control name='h_todate' type='id' value='' visible='1'/>
                                    <control name='user' type='id' value='' visible='1'/>
                                    <control name='h_unit' type='id' value='' visible='1'/>
                                    <control name='h_rate' type='id' value='' visible='1'/>
                                    <control name='h_override' type='id' value='' visible='1'/>
                                    <control name='h_nonbillreason' type='id' value='' visible='1'/>
                                    <control name='h_nonbillreasoncode' type='id' value='' visible='1'/>
                                    <control name='h_comment' type='id' value='' visible='1'/>
                                    <control name='h_precomment' type='id' value='' visible='1'/>
                                    <control name='h_qty' type='id' value='' visible='1'/>");
                sbTemplate.AppendFormat(@"<control name='h_pid' type='id' value='' visible='1'/>", h_pid.Value);
                sbTemplate.Append(@"<control name='h_reason' type='id' value='' visible='1'/>
                                    <control name='h_postable' type='id' value='' visible='1'/>
                                    <control name='h_allocated' type='id' value='1' visible='1'/>
                                    <control name='req_fields' type='id' value='vendor' visible='1'/>
                                    <control name='h_save' type='id' value='' visible='1'/>");
                //<!-- pmittal5 Mits 14066 12/19/08 - Billed and Invoiced should be in 0.00 format-->
                //rupal:start.multicurrency
                sbTemplate.Append(@"<control name='totinvoice' title='Invoiced' type='textlable' visible='1'>Invoiced : " + ConvertDoubleValueToCurrencyFormat("0") + "</control>");
                sbTemplate.Append(@"<control name='totbill' title='Billed' type='textlable' value='' visible='1'>Billed :" + ConvertDoubleValueToCurrencyFormat("0") + "</control>");
                //rupal:end
                //<!-- For Auto Populating Current Adjuster -->
                sbTemplate.AppendFormat(@"<control name='h_adjLastName' type='id' visible='1' value='{0}' />", sLastName);
                sbTemplate.AppendFormat(@"<control name='h_adjFirstName' type='id' visible='1' value='{0}' />", sFirstName);
                sbTemplate.AppendFormat(@"<control name='h_adjId' type='id' visible='1' value='{0}' />", iAdjusterId);
                //<!-- End - pmittal5-->
                sbTemplate.Append(@"<control lock='true' name='totavailable' title='Available:' type='text' value='0' visible='1'/>
                                </group>
	                            <group name='InvDetail' title=''>
		                            <control col1='Transaction Type' col2='invoice Amount' col3='billable' col4='Bill Amount' islink='yes' isrequ='yes' linkpage='TimeAndExpensesDetails' maxtrackid='1' name='invDetail' title='Invoice Details' type='invDetail' visible='1'>");
                sbTemplate.Append(@"</control>
		                            <control name='h_Invoiced' type='id' value='' visible='1'/>
		                            <control name='h_Billed' type='id' value='' visible='1'/>
		                            <control name='h_ratecode' type='id' value='' visible='1'/>
                                </group>
	                            <button disable='' enablefornew='invoice' linkto='Invoice.asp' name='btnsave' param='' title='Save and Close' type='save' visible='1'/>
	                            <button disable='' enablefornew='invoice' linkto='Invoice.asp' name='btncancel' title='Cancel' type='normal' visible='1'/>");
                           
                
                //rupal:start, r8 multicurrency                
                sbTemplate.Append(@"<control name='currencytypetext' title='Currency Type:' type='text' visible='1' value='" + sCurrencyTypeText + "' codeid='" + sCurrencyTypeCodeId + "' />");
                sbTemplate.Append(@" </form>
                                    </Document>
                                    </ResultMessage>");
                //rupal:end


                oTemplate = XElement.Parse(sbTemplate.ToString());
                #endregion
            }//else if
            else if (mode == "firsttime")
            {
                //rupal:start, r8 multicurrency
                #region GetXmlDataAndDate template
                StringBuilder sbMessage = new StringBuilder();
                sbMessage = sbMessage.Append("<Message>");
                sbMessage = sbMessage.Append("<Authorization></Authorization>");
                sbMessage = sbMessage.Append("<Call><Function></Function></Call>");
                sbMessage = sbMessage.Append("<Document>");
                sbMessage = sbMessage.Append("<TimeExpense>");
                sbMessage = sbMessage.Append("<RateTableid></RateTableid>");
                sbMessage = sbMessage.Append("<FirstTime></FirstTime>");
                sbMessage = sbMessage.Append("<ClaimId>");
                sbMessage = sbMessage.Append(claimid.Value);
                sbMessage = sbMessage.Append("</ClaimId>");
                sbMessage = sbMessage.Append("</TimeExpense></Document></Message>");                                   
                oTemplate = XElement.Parse(sbMessage.ToString());
                #endregion
                //rupal:end
            } // else if

            return oTemplate;
        } // method: GetXMLMessageTemplate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvInvoiceDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int iTrackId       = 0;
            string sTransType  = String.Empty;
            string sRate       = String.Empty;
            string sUnit       = String.Empty;
            string sIsBillable = String.Empty;
            string sItemAmount = String.Empty;
            string sTrackId    = String.Empty;

            StringBuilder sbDetailsOnClick = new StringBuilder();

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                sIsAllocated = h_allocated.Value;
                sTransType   = DataBinder.Eval(e.Row.DataItem, "transtype").ToString();
                sRate        = DataBinder.Eval(e.Row.DataItem, "rate").ToString().TrimStart('$');
                sUnit        = DataBinder.Eval(e.Row.DataItem, "unit").ToString();
                sIsBillable  = DataBinder.Eval(e.Row.DataItem, "billable").ToString();
                sItemAmount  = DataBinder.Eval(e.Row.DataItem, "invamt").ToString();
                sTrackId     = DataBinder.Eval(e.Row.DataItem, "trackid").ToString();

                int.TryParse(sTrackId, out iTrackId);

                HyperLink hlDetails = (HyperLink)e.Row.Cells[0].FindControl("transtype_link");
                hlDetails.CssClass = "nolinehover";
                hlDetails.Attributes.Add("onmouseover", "this.style.cursor='hand';");
                hlDetails.Attributes.Add("onmouseout", "this.style.cursor='default';");

                sbDetailsOnClick.Append("javascript:window.open('../../UI/TAndE/TimeAndExpenseDetails.aspx?ctlnumber=edit");
                sbDetailsOnClick.AppendFormat("&maxtrackid={0}&IsAllocated={1}&claimnum={2}", iControlId, sIsAllocated, clmNumber.Text);//MITS 16270
                //MITS 16053 - removed invNumber.Text from query string (not needed)
                //rupal:comeback
                sbDetailsOnClick.AppendFormat("&invtot={0}&ratetableid={1}&ClaimantEid={2}&UnitId={3}", invAmt.Text, h_ratetableid.Value, h_claimanteid.Value, h_unitid.Value);//MITS 16608
                //rupal:start,r8 multicurrency
                sbDetailsOnClick.AppendFormat("&culture={0}", currencytype.Text);
                //rupal:end
                sbDetailsOnClick.Append("','TandE','width=820,height=550,resizable=yes,scrollbars=yes');void(0);");
                hlDetails.NavigateUrl = sbDetailsOnClick.ToString();

                ImageButton ibDelete = (ImageButton)e.Row.Cells[0].FindControl("deletedetail_link");
                ibDelete.Attributes.Add("onclick", String.Format("delitem('{0}'); ", iControlId));//MITS 16270
                iControlId++;
                //rupal:comeback
                decimal dCurInvAmt = 0.00m;
                //rupal:multicurrency
                //decimal.TryParse(DataBinder.Eval(e.Row.DataItem, "invamt").ToString().TrimStart('$'), out dCurInvAmt);
                HiddenField hhInvamt = (HiddenField)e.Row.FindControl("hInvamt");
                decimal.TryParse(ConvertToDouble(hhInvamt.Value).ToString(), out dCurInvAmt);
                dTotalInvoiced += dCurInvAmt;
            }//if
        } // method: gvInvoiceDetails_RowDataBound

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvInvoiceDetails_DataBound(object sender, EventArgs e)
        {
            //hide the last column from displaying
            gvInvoiceDetails.Rows[gvInvoiceDetails.Rows.Count - 1].CssClass = "hiderowcol";

            //hide columns from displaying hidden values on the client's UI
            gvInvoiceDetails.Columns[(int)gridviewColumns.transtype].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.transtype].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.reservetype].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.reservetype].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.hInvamt].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.hInvamt].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.hBillable].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.hBillable].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.unit].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.unit].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.rate].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.rate].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.nonbillreason].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.nonbillreason].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.qty].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.qty].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.postable].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.postable].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.hTrackId].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.hTrackId].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.transtypecode].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.transtypecode].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.hBillAmt].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.hBillAmt].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.reservetypecode].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.reservetypecode].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.invtot].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.invtot].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.fromdate].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.fromdate].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.todate].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.todate].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.fromdateFormatted].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.fromdateFormatted].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.todateFormatted].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.todateFormatted].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.hOverRide].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.hOverRide].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.nonbillreasoncode].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.nonbillreasoncode].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.comment].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.comment].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.precomment].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.precomment].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.status].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.status].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.reason].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.reason].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.id].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.id].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.pid].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.pid].HeaderStyle.CssClass = "hiderowcol";

            gvInvoiceDetails.Columns[(int)gridviewColumns.invnum].ItemStyle.CssClass = "hiderowcol";
            gvInvoiceDetails.Columns[(int)gridviewColumns.invnum].HeaderStyle.CssClass = "hiderowcol";
        } // method: gvInvoiceDetails_DataBound

        //rupal:start, r8 multicurrency
        protected override void InitializeCulture()
        {
            if (Request.Form["currencytype"] != null)
            {
                if (!string.IsNullOrEmpty(Request.Form["currencytype"]))
                {
                    Culture = Request.Form["currencytype"].Split('|')[1];
                    UICulture = Request.Form["currencytype"].Split('|')[1];
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.Form["currencytype"].Split('|')[1]);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(Request.Form["currencytype"].Split('|')[1]);
                }
            }
            else if ((currencytype != null) && (!string.IsNullOrEmpty(currencytype.Text)))
            {
                Culture = currencytype.Text.Split('|')[1];
                UICulture = currencytype.Text.Split('|')[1];
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(currencytype.Text.Split('|')[1]);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(currencytype.Text.Split('|')[1]);
            }
            base.InitializeCulture();
        }

        private double ConvertToDouble(string sDecimalValue)
        {
            if (sDecimalValue == string.Empty)
            {
                sDecimalValue = "0";
            }
            string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            double dAmt = Convert.ToDouble(sDecimalValue);
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
            return dAmt;
        }
        
        //rupal:end
    }
}