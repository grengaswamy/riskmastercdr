﻿using System;
using System.Data;
using System.Security;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Linq;

using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.TAndE
{
    public partial class TimeAndExpenseRateAndUnits : NonFDMBasePageCWS
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        //rupal:multicurrency
        string sPmtCurrCodeId = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            bool bReturnStatus      = false;
            string sreturnValue     = String.Empty;
            StringBuilder sbCode    = new StringBuilder();
            DataSet tableRecordsSet = null;
            XmlDocument tableXDoc   = new XmlDocument();
            XElement XmlTemplate    = null;

            // Read the Values from Querystring
            string sCustId = AppHelper.GetQueryStringValue("custid");
            hcustid.Text   = sCustId;

            if (String.IsNullOrEmpty(sCustId))
            {
                TableId1.Text   = AppHelper.GetQueryStringValue("rateid");
                TableId2.Text   = AppHelper.GetQueryStringValue("allocated");
                LookUpText.Text = AppHelper.GetQueryStringValue("txtLookUp").Trim('\'');

                Page.Title               = "Rates for TandE with Type Code";
                pnlTypeCode.Visible      = true;
                pnlCodeSelection.Visible = false;
                pnlZeroRecords.Visible   = false;
                FrmName.Text             = "ratesandunits";

                #region javascript
                sbCode.Append(@"
                                //resize the scrollable div tag when window get resized
                                function resizeWindow()
                                {
                                    var objDIV  = document.getElementById('div1');
                                    var iHeight = document.body.clientHeight;
                                    var iWidth  = document.body.clientWidth;

                                    if( objDIV != null )
                                    {
                                        objDIV.style.height = (iHeight * 0.90);
                                        objDIV.style.width  = (iWidth * 0.95);
                                    }
                                }

                                window.onresize = resizeWindow;

                                function handleOnload()
                                {
                                    document.forms[0].cmdClose.focus();
                                    return true;
                                }

                                function handleUnload()
                                {
                                    if(window.opener!=null)
                                    {
                                        window.opener.m_LookupBusy = false;                      
                                        window.opener.document.forms[0].txtTransType.value = '';
                                        window.opener.onCodeClose();
                                    }
                                    return false;
                                }
                               ");
                #endregion

                HtmlGenericControl hgcJavascript = new HtmlGenericControl("script");
                hgcJavascript.Attributes.Add("type", "text/javascript");
                hgcJavascript.Attributes.Add("language", "javascript");
                hgcJavascript.InnerHtml = sbCode.ToString();
                Head1.Controls.Add(hgcJavascript);
            }//if
            else
            {
                Page.Title                    = "Code Selection";
                bodyRates.Attributes["class"] = "Center32";
                pnlCodeSelection.Visible      = true;
                pnlTypeCode.Visible           = false;
                pnlZeroRecords.Visible        = false;
                FrmName.Text                  = "rates";
            }//else

            //rupal:multicurrency
            sPmtCurrCodeId = AppHelper.GetQueryStringValue("PmtCurrCodeId");

            if (!IsPostBack)
            {
                try
                {
                    XmlTemplate   = GetMessageTemplate(sCustId);
                    CallCWSFunction("TimeExpenseAdaptor.GetXmlData", out sreturnValue, XmlTemplate);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);

                    if (bReturnStatus)
                    {
                        //XElement xmlDataRequest = XElement.Parse(sreturnValue);

                        //fill xmldocument from the string xml
                        tableXDoc.LoadXml(sreturnValue);

                        //fill dataset with xmldocument object
                        tableRecordsSet = ConvertXmlDocToDataSet(tableXDoc);

                        if (String.IsNullOrEmpty(sCustId))
                        {
                            #region xml for debug
                            //<group name="Invoice" title="Invoice">
                            //  <error desc="" value="0" />
                            //  <control>
                            //      <rateandunit lookuptext="tan">
                            //          <option id1="2357"           // id2="368" 
                            //                  code1="EXF"          // desc1="Expert Witness Fees" 
                            //                  codedesc="E Expense" // unit="HOUR Hour" 
                            //                  rate="1" />
                            //      </rateandunit>
                            //  </control>
                            //</group>
                            #endregion

                            XmlNode xnLookUpText = tableXDoc.SelectSingleNode("//Document/form/group[@name='Invoice']/control/rateandunit");
                            htranstype.Value = xnLookUpText.Attributes["lookuptext"].Value;

                            if (tableRecordsSet.Tables["option"] != null)
                            {
                                tableRecordsSet.Tables["option"].Columns.Add("desclink", typeof(string));

                                foreach (DataRow row in tableRecordsSet.Tables["option"].Rows)
                                {
                                    string sDescValue        = String.Empty;
                                    string sCodeDescValue    = String.Empty;
                                    StringBuilder sbRateName = new StringBuilder();

                                    sDescValue     = row["desc1"].ToString();
                                    sDescValue = sDescValue.Replace("'", "&apos;");
                                    sCodeDescValue = SecurityElement.Escape(row["codedesc"].ToString());

                                    #region html for debug
                                    //<a href="/oxf/home" onclick="; 
                                    //  document.getElementById('wsrp_rewrite_action_1').name += '$action^'; 
                                    //  event.returnValue=false; 
                                    //  if (setCalValue('2357','368','EXF','E Expense','Expert Witness Fees','HOUR Hour','1')) 
                                    //      document.forms['wsrp_rewrite_form_1'].submit();
                                    //">Expert Witness Fees</a>
                                    #endregion

                                    //function setCalValue(id1, id2, code1, codedesc, desc1, unit, rate)
                                    sbRateName.Append("<a href='#' onclick=\"setCalValue(");
                                    sbRateName.AppendFormat("'{0}','{1}','{2}',", row["id1"], row["id2"], row["code1"]);
                                    sbRateName.AppendFormat("'{0}','{1}','{2}',", sCodeDescValue, sDescValue, row["unit"]);
                                    
                                    sbRateName.AppendFormat("'{0}')\" target='_self'>{1}</a>", row["rate"], SecurityElement.Escape(sDescValue));
                                    //sbRateName.AppendFormat("'{0}')\" target='_self'>{1}</a>", row["rateInPmtCurr"], SecurityElement.Escape(sDescValue));
                                    

                                    row["desclink"] = sbRateName.ToString();
                                }//foreach

                                //MITS 17610 - start
                                //abansal23 MITS 18880 : Transaction Types display in oracle db 1/12/2009
                                //tableRecordsSet.Tables["option"].DefaultView.Sort = "rate DESC, code1 ASC";
                                dlTypeCodes.DataSource = tableRecordsSet.Tables["option"].DefaultView;
                                //dlTypeCodes.DataSource = tableRecordsSet.Tables["option"];
                                //MITS 17610 - end

                                dlTypeCodes.DataBind();
                            }//if
                            else
                            {
                                bodyRates.Attributes.Add("onload", "return handleOnload();");
                                bodyRates.Attributes.Add("onUnload", "handleUnload();");
                                pnlZeroRecords.Visible   = true;
                                pnlCodeSelection.Visible = false;
                                pnlTypeCode.Visible      = false;
                                cmdClose.Attributes.Add("onclick", "self.close()");
                                cmdClose.Visible         = true;
                                lblNoData.Text           = String.Format("Zero records match the criteria '{0}'", htranstype.Value);
                                lblNoData.Visible        = true;
                            }//else
                        }//if
                        else
                        {
                            #region xml for debug
                            // <TandE>
                            //   <rateTableData ...
                            //      0 bgcolor   = "data1"        // 1 id        = "5" 
                            //      2 name      = "Tanuj Test"   // 3 custid    = "11"
                            //      4 efdate    = "20060627"     // 5 exdate    = "20100615"
                            //      6 custname  = "Division 13"  // 7 TandE_Id  = 
                            //      8 ratename  = 
                            // .../></TandE>
                            #endregion

                            if (tableRecordsSet.Tables["rateTableData"] != null)
                            {
                                tableRecordsSet.Tables["rateTableData"].Columns.Add("ratename", typeof(string));

                                foreach (DataRow row in tableRecordsSet.Tables["rateTableData"].Rows)
                                {
                                    string efdate            = String.Empty;
                                    string exdate            = String.Empty;
                                    string sNameValue        = String.Empty;
                                    StringBuilder ratename   = new StringBuilder();
                                    StringBuilder sbRateName = new StringBuilder();

                                    sNameValue = row["name"].ToString();

                                    ratename.AppendFormat("{0} - Effective Date ", sNameValue);

                                    efdate = row["efdate"].ToString();
                                    if (!String.IsNullOrEmpty(efdate))
                                    {
                                        ratename.AppendFormat("{0}/{1}/{2}", efdate.Substring(4, 2), efdate.Substring(6, 2), efdate.Substring(0, 4));
                                    }//if

                                    ratename.Append(" Expiration Date ");

                                    exdate = row["exdate"].ToString();
                                    if (!String.IsNullOrEmpty(exdate))
                                    {
                                        ratename.AppendFormat("{0}/{1}/{2}", exdate.Substring(4, 2), exdate.Substring(6, 2), exdate.Substring(0, 4));
                                    }//if
                                    //Ashish Ahuja : Mits 31897 Start
                                    if (sNameValue.Contains("'"))
                                    {
                                        sNameValue = sNameValue.Replace("'", "^#$");
                                    }
                                    //Ashish Ahuja : Mits 31897 End
                                    sbRateName.AppendFormat("<a href='#' onclick=\"setClaimRateTable('{0}',", SecurityElement.Escape(sNameValue));
                                    sbRateName.AppendFormat("'{0}')\" target='_self'>", row["id"].ToString());
                                    sbRateName.AppendFormat("{0}</a>", ratename.ToString());

                                    row["ratename"] = sbRateName.ToString();
                                }//foreach
                            
                                dlRateTables.DataSource = tableRecordsSet.Tables["rateTableData"];
                                dlRateTables.DataBind();
                            }//if
                            else
                            {
                                lblDesc.Text    = "No Codes Available";
                                lblDesc.Visible = true;
                            }//else
                        }//else
                    }//if
                    else
                    {

                    } // else
                }//try
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch
            }//if
            else
            {
                //if it is a postback, do nothing
            }//else
        }//method: Page_Load

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <param name="sCustId"></param>
        /// <returns></returns>
        private XElement GetMessageTemplate(string sCustId)
        {
            XElement oTemplate = null;
            //rupal:multicurrency
            StringBuilder sb;
            if (String.IsNullOrEmpty(sCustId))
            {
                #region commented by rupal
                sb = new StringBuilder();
              
                /*
                sb = sb.Append(@"<Message>
                                                <Authorization></Authorization>
                                                <Call>
                                                    <Function></Function>
                                                </Call>
                                                <Document>
                                                    <TimeExpense>
                                                        <FrmName></FrmName>
                                                        <InputXml>
                                                            <form actionname='invoice.asp' bcolor='white' headingtype='main' name='frmData' sid='150' title='Time and Expense Invoice' topbuttons='1'>
		                                                        <body1 func='setInvoicePage()' req_func='yes'/>
		                                                        <group name='Invoice' title='Invoice'>
			                                                        <error desc='' value='0'/>
			                                                        <control>
				                                                        <rateandunit/>
			                                                        </control>
		                                                        </group>
	                                                        </form>
                                                        </InputXml>
                                                        <TableId1></TableId1>
                                                        <TableId2></TableId2>
                                                        <TableId3></TableId3>
                                                        <LookUpText></LookUpText>
                                                        <PmtCurrCodeId>");
                sb = sb.Append(sPmtCurrCodeId);
                sb = sb.Append(@"</PmtCurrCodeId>
                                                    </TimeExpense>
                                                </Document>
                                            </Message>");
                oTemplate = XElement.Parse(sb.ToString());
                 */
                #endregion
                #region ratesandunits xml template
                oTemplate = XElement.Parse(@"<Message>
                                                <Authorization></Authorization>
                                                <Call>
                                                    <Function></Function>
                                                </Call>
                                                <Document>
                                                    <TimeExpense>
                                                        <FrmName></FrmName>
                                                        <InputXml>
                                                            <form actionname='invoice.asp' bcolor='white' headingtype='main' name='frmData' sid='150' title='Time and Expense Invoice' topbuttons='1'>
		                                                        <body1 func='setInvoicePage()' req_func='yes'/>
		                                                        <group name='Invoice' title='Invoice'>
			                                                        <error desc='' value='0'/>
			                                                        <control>
				                                                        <rateandunit/>
			                                                        </control>
		                                                        </group>
	                                                        </form>
                                                        </InputXml>
                                                        <TableId1></TableId1>
                                                        <TableId2></TableId2>
                                                        <TableId3></TableId3>
                                                        <LookUpText></LookUpText>                                                        
                                                    </TimeExpense>
                                                </Document>
                                            </Message>");
               
                #endregion
            }//if
            else
            {
                #region commented by rupal
                /*
                sb = new StringBuilder();
                sb=sb.Append(@"
                                <Message>
                                    <Authorization></Authorization>
                                    <Call>
                                        <Function></Function>
                                    </Call>
                                    <Document>
                                        <TimeExpense>
                                            <FrmName></FrmName>
                                            <InputXml>
                                                <TandE/>
                                            </InputXml>
                                            <TableId1></TableId1>
                                            <TableId2></TableId2>
                                            <TableId3></TableId3>
                                            <CustomerId></CustomerId>
                                            <PmtCurrCodeId>");
                sb = sb.Append(sPmtCurrCodeId);
                sb = sb.Append(@"</PmtCurrCodeId>
                                                    </TimeExpense>
                                                </Document>
                                            </Message>");
                oTemplate = XElement.Parse(sb.ToString());
                 */ 
                #endregion
                
                oTemplate = XElement.Parse(@"<Message>
                                                <Authorization></Authorization>
                                                <Call>
                                                    <Function></Function>
                                                </Call>
                                                <Document>
                                                    <TimeExpense>
                                                        <FrmName></FrmName>
                                                        <InputXml>
                                                            <TandE/>
                                                        </InputXml>
                                                        <TableId1></TableId1>
                                                        <TableId2></TableId2>
                                                        <TableId3></TableId3>
                                                        <CustomerId></CustomerId>                                                        
                                                    </TimeExpense>
                                                </Document>
                                            </Message>");
               
            }//else

            return oTemplate;
        }//method: GetMessageTemplate
    }
}
