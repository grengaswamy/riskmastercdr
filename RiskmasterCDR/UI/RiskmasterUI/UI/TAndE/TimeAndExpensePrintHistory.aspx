﻿<%@ Page Language="C#" Theme="RMX_Default" AutoEventWireup="true" CodeBehind="TimeAndExpensePrintHistory.aspx.cs" Inherits="Riskmaster.UI.TAndE.TimeAndExpensePrintHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Print History</title>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/TandE.js"></script>
    <script type="text/javascript" language="javascript">
        function printTest() {
            try {
                if (window != null) {
                    window.print();
                }
            }
            catch (e) {
                //alert(e);
            };
        }
    </script>
</head>

<body class="Margin0" onload="printTest()">
    <form id="frmData" runat="server">
    <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="divScroll">
            <tr>
                <td colspan="3">
                    <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
                </td>
            </tr>
        </table>
        
        <asp:Panel ID="pnlPrintHistory" runat="server">
        <table width="100%" cellspacing="2" cellpadding="0" border="0" class="divScroll">
            <tr>
                <td align="left" class="rowdark">
                    <font size="2" face="arial"><b><asp:label ID="lblTitle" runat="server" Text="" /></b></font>
                </td>
                <td align="center" class="rowdark">
                    <font size="2" face="arial"><b><asp:label ID="lblCompanyName" runat="server" Text="" /></b></font>
                </td>
                <td align="right" class="rowdark">
                    <asp:label ID="lblCurrentDate" runat="server" Text="" />
                </td>
            </tr>
            
            <asp:Panel ID="pnlGridview" runat="server">
            <tr>
                <td align="center" colspan="3">
                    <asp:GridView ID="gvPrintHistory" Width="100%" runat="server" AutoGenerateColumns="false"
                                  CellPadding="0" CellSpacing="0" GridLines="None" CssClass="divScroll"
                                  BorderStyle="None" BorderColor="White" BorderWidth="0">
                        <Columns>
                            <asp:boundfield DataField="vendor" 
                                            HeaderStyle-CssClass="colheader4"
                                            ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Left"
                                            HeaderText="Vendor/Work Done By">
                            </asp:boundfield>
                            <asp:boundfield DataField="invnumber" 
                                            HeaderStyle-CssClass="colheader4"
                                            HeaderText="Invoice Number" 
                                            HeaderStyle-HorizontalAlign="Right" 
                                            ItemStyle-HorizontalAlign="Right">
                            </asp:boundfield>
                            <asp:boundfield DataField="invdate" 
                                            HeaderStyle-CssClass="colheader4"
                                            HeaderText="Invoice Date" 
                                            HeaderStyle-HorizontalAlign="Center" 
                                            ItemStyle-HorizontalAlign="Center">
                            </asp:boundfield>
                            <asp:boundfield DataField="invamnt" 
                                            HeaderStyle-CssClass="colheader4"
                                            HeaderText="Invoice Amount" 
                                            HeaderStyle-HorizontalAlign="Right" 
                                            ItemStyle-HorizontalAlign="Right">
                            </asp:boundfield>
                            <asp:boundfield DataField="billedamnt" 
                                            HeaderStyle-CssClass="colheader4"
                                            HeaderText="Billed Amount" 
                                            HeaderStyle-HorizontalAlign="Right" 
                                            ItemStyle-HorizontalAlign="Right">
                            </asp:boundfield>
                            <asp:boundfield DataField="checkstatus" 
                                            HeaderStyle-CssClass="colheader4"
                                            HeaderText="Check Status"
                                            HeaderStyle-HorizontalAlign="Center" 
                                            ItemStyle-HorizontalAlign="Center">
                            </asp:boundfield>
                            <asp:boundfield DataField="checknumber" 
                                            HeaderStyle-CssClass="colheader4"
                                            HeaderText="Check Number" 
                                            HeaderStyle-HorizontalAlign="Right" 
                                            ItemStyle-HorizontalAlign="Right">
                            </asp:boundfield>
                            <asp:boundfield DataField="checkdate" 
                                            HeaderStyle-CssClass="colheader4"
                                            HeaderText="Check Date" 
                                            HeaderStyle-HorizontalAlign="Center" 
                                            ItemStyle-HorizontalAlign="Center">
                            </asp:boundfield>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            </asp:Panel>
            
            <asp:Panel ID="pnlNoData" runat="server">
            <tr>
                <td align="center" colspan="3">
                    <table width="100%">
                        <tr>
                            <td class="colheader4">Vendor/Work Done By</td>
                            <td class="colheader4">Invoice Number</td>
                            <td class="colheader4">Invoice Date</td>
                            <td class="colheader4">Invoice Amount</td>
                            <td class="colheader4">Billed Amount</td>
                            <td class="colheader4">Check Status</td>
                            <td class="colheader4">Check Number</td>
                            <td class="colheader4">Check Date</td>
                        </tr>
                    </table>
                </td>
            </tr>
            </asp:Panel>
            
            <tr>
                <td align="left" class="rowdark" width="50%">
                    <font size="2" face="arial"><b>Total Billed Amount: 
                    <asp:label ID="lblBillTotal" runat="server" Text="" /></b></font>
                </td>
                <td align="left" class="rowdark" width="50%">
                    <font size="2" face="arial"><b>Total Invoice Amount: 
                    <asp:label ID="lblInvTotal" runat="server" Text="" /></b></font>
                </td>
                <td class="rowdark">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="left" class="colheader2">Note:<br />
                    <asp:label ID="lblCopyRightProp" runat="server" Text="" /><br />
                    <asp:label ID="lblProductName" runat="server" Text="" /><br />
                    <asp:label ID="lblCopyRightDef" runat="server" Text="" /><br />
                    <asp:label ID="lblCopyRightsReserved" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td class="rowdark" colspan="3" height="10"></td>
            </tr>
        </table>
        </asp:Panel>
        
        <asp:Panel ID="pnlPrintReport" runat="server">
        <table width="100%" cellspacing="2" cellpadding="0" border="0" class="divScroll">
            <tr>
                <td align="center" class="rowdark">
                    <font size="2" face="arial"><b><asp:label ID="lblCompanyName2" runat="server" Text="" /></b></font>
                </td>
                <td align="right" class="rowdark">
                    <asp:label ID="lblCurrentDate2" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <table width="100%">
                        <tr align="left">
                            <td colspan="7" class="colheader2" align="left">
                                <asp:label ID="lblCity" runat="server" Text="" />&nbsp;&nbsp;&nbsp;
                                <asp:label ID="lblPhone" runat="server" Text="" />&nbsp;&nbsp;&nbsp;
                                <asp:label ID="lblFax" runat="server" Text="" />&nbsp;
                            </td>
                        </tr>
                        <tr><td colspan="7">&nbsp;</td></tr>
                        <tr><td colspan="7" align="center" class="headertext1"><asp:label ID="lblTitle2" runat="server" Text="" /></td></tr>
                        <tr align="left">
                            <td colspan="7" align="left">
                                <font size="2" face="arial"><b>Date Billed&nbsp:&nbsp;</b></font>
                                <asp:label ID="lblDateBilled" runat="server" Text="" />&nbsp;
                                <font size="2" face="arial"><b>Claim Number&nbsp:&nbsp;</b></font>
                                <asp:label ID="lblClaimNumber" runat="server" Text="" />&nbsp;
                                <font size="2" face="arial"><b>Claimant&nbsp:&nbsp;</b></font>
                                <asp:label ID="lblClaimant" runat="server" Text="" />&nbsp;
                            </td>
                        </tr>
                        <tr><td colspan="7" class="rowdark" height="10"></td></tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvPrintReport" Width="100%" runat="server" AutoGenerateColumns="false"
                                              CellPadding="0" CellSpacing="0" GridLines="None" CssClass="divScroll"
                                              BorderStyle="None" BorderColor="White" BorderWidth="0">
                                    <Columns>
                                        <asp:boundfield DataField="invdate" 
                                                        HeaderStyle-CssClass="colheader4"
                                                        HeaderText="Date" 
                                                        HeaderStyle-HorizontalAlign="Center" 
                                                        ItemStyle-HorizontalAlign="Center">
                                        </asp:boundfield>
                                        <asp:boundfield DataField="typecode" 
                                                        HeaderStyle-CssClass="colheader4"
                                                        HeaderText="Type" 
                                                        HeaderStyle-HorizontalAlign="Left" 
                                                        ItemStyle-HorizontalAlign="Left">
                                        </asp:boundfield>
                                        <asp:boundfield DataField="comment" 
                                                        HeaderStyle-CssClass="colheader4"
                                                        HeaderText="Description" 
                                                        HeaderStyle-HorizontalAlign="Left" 
                                                        ItemStyle-HorizontalAlign="Left">
                                        </asp:boundfield>
                                        <asp:boundfield DataField="qty" 
                                                        HeaderStyle-CssClass="colheader4"
                                                        HeaderText="Quantity" 
                                                        HeaderStyle-HorizontalAlign="Right" 
                                                        ItemStyle-HorizontalAlign="Right">
                                        </asp:boundfield>
                                        <asp:boundfield DataField="unit" 
                                                        HeaderStyle-CssClass="colheader4"
                                                        HeaderText="Unit" 
                                                        HeaderStyle-HorizontalAlign="Center" 
                                                        ItemStyle-HorizontalAlign="Center">
                                        </asp:boundfield>
                                        <asp:boundfield DataField="rate" 
                                                        HeaderStyle-CssClass="colheader4"
                                                        HeaderText="Rate" 
                                                        HeaderStyle-HorizontalAlign="Right" 
                                                        ItemStyle-HorizontalAlign="Right">
                                        </asp:boundfield>
                                        <asp:boundfield DataField="total" 
                                                        HeaderStyle-CssClass="colheader4"
                                                        HeaderText="Total" 
                                                        HeaderStyle-HorizontalAlign="Right" 
                                                        ItemStyle-HorizontalAlign="Right">
                                        </asp:boundfield>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td colspan="2" class="rowdark" height="10"></td></tr>
        </table>
        </asp:Panel>
    </div>
    
    <asp:HiddenField runat="server" ID="billed_void" />
    <asp:HiddenField runat="server" ID="status" Value="A" />
    <asp:HiddenField runat="server" ID="SysFormName" />
    
    <asp:textbox style="display: none" runat="server" id="ClaimId" rmxref="Instance/Document/TimeExpense/ClaimId" Text="" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="InvoiceId" rmxref="Instance/Document/TimeExpense/InvoiceId" Text="" rmxtype="hidden" />
    </form>
</body>
</html>