﻿<%@ Page Language="C#" Theme="RMX_Default" AutoEventWireup="true" CodeBehind="TimeAndExpenseBilledExpenses.aspx.cs" Inherits="Riskmaster.UI.TAndE.TimeAndExpenseBilledExpenses" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Time and Expense Billed Expenses</title>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/TandE.js"></script>
</head>
<body class="Margin0">
    <form id="frmData" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
                <td colspan="2">
                    <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
                </td>
            </tr>
            <tr>
                <td class="msgheader" colspan="13">Time and Expenses (<asp:Label ID="lblClaimNum" runat="server" Text="" />)</td>
            </tr>
        </table>
        
        <asp:Panel ID="pnlGridview" runat="server">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr class="data2">
                <td align="left">
                    <asp:GridView ID="gvExpenses" runat="server" Width="100%" CellPadding="4" CellSpacing="0" 
                                  autogeneratecolumns="False" 
                                  AlternatingRowStyle-CssClass="data1" 
                                  ShowFooter="true"
                                  onrowdatabound="gvExpenses_RowDataBound" 
                                  ondatabound="gvExpenses_DataBound">
                        <Columns>
                            <asp:templatefield headerstyle-cssclass="ctrlgroup2" 
                                               HeaderStyle-HorizontalAlign="Center"
                                               FooterStyle-CssClass="ctrlgroup2"
                                               ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" id="optBillit" Checked="false" />
                                </ItemTemplate>
                            </asp:templatefield>
                            <asp:boundfield DataField="vendor" 
                                            ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="ctrlgroup2" 
                                            HeaderStyle-HorizontalAlign="Left"
                                            HeaderText="Vendor/Work Done By" 
                                            FooterStyle-CssClass="ctrlgroup2">
                            </asp:boundfield>
                            <asp:boundfield DataField="invoicenumber" 
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderText="Invoice Number" 
                                            HeaderStyle-HorizontalAlign="Right" 
                                            ItemStyle-HorizontalAlign="Right"
                                            FooterStyle-CssClass="ctrlgroup2">
                            </asp:boundfield>
                            <asp:boundfield DataField="invoicedate" 
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderText="Invoice Date" 
                                            HeaderStyle-HorizontalAlign="Center" 
                                            ItemStyle-HorizontalAlign="Center"
                                            FooterStyle-HorizontalAlign="Right"
                                            FooterStyle-CssClass="ctrlgroup2" 
                                            FooterText="Total&nbsp;:&nbsp;">
                            </asp:boundfield>
                            <asp:boundfield DataField="gvstatus" 
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderText="Check Status"
                                            HeaderStyle-HorizontalAlign="Center" 
                                            ItemStyle-HorizontalAlign="Center"
                                            FooterStyle-CssClass="ctrlgroup2">
                            </asp:boundfield>
                            <asp:boundfield DataField="chknum" 
                                            ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Right"
                                            HeaderText="Check Number" 
                                            FooterStyle-CssClass="ctrlgroup2">
                            </asp:boundfield>
                            <asp:boundfield DataField="chkdate" 
                                            ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Check Date" 
                                            FooterStyle-CssClass="ctrlgroup2">
                            </asp:boundfield>
                            <asp:templatefield headerstyle-cssclass="ctrlgroup2" 
                                               Visible="true"
                                               ItemStyle-HorizontalAlign="Center"
                                               FooterStyle-CssClass="ctrlgroup2">
                                <ItemTemplate>
                                    <asp:imagebutton id="img" imageurl="~/Images/book02.gif" ImageAlign="Middle" runat="server" />
                                </ItemTemplate>
                            </asp:templatefield>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="rowid" runat="server" Value='<%# Eval("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hstatus" runat="server" Value='<%# Eval("hstatus") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- Start - MITS 15633 - mpalinski --%>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="transid" runat="server" Value='<%# Eval("transid") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="payeeid" runat="server" Value='<%# Eval("payeeid") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hinvoiceamt" runat="server" Value='<%# Eval("hinvoiceamt") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- End - MITS 15633 - mpalinski --%>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        </asp:Panel>
        
        <asp:Panel ID="pnlNoData" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr class="ctrlgroup2">
             <td class="colheader4" width="5%"></td>
             <td class="colheader4" align="right" width="20%">Vendor/Work Done By</td>
             <td class="colheader4" align="right" width="10%">Invoice Number</td>
             <td class="colheader4" align="right" width="10%">Invoice Date</td>
             <td class="colheader4" align="right" width="10%">Invoice Amount</td>
             <td class="colheader4" align="right" width="10%">Billed Amount</td>
             <td class="colheader4" align="right" width="10%">Check Status</td>
             <td class="colheader4" align="right" width="10%">Check Number</td>
             <td class="colheader4" align="right" width="10%">Check Date</td>
             <td class="colheader4" align="right" width="5%"></td>
            </tr>

            <tr class="ctrlgroup2">
              <td width="5%">&nbsp;</td>
              <td align="right" width="20%"></td>
              <td align="right" width="10%"></td>
              <td class="colheader4" align="right" width="10%">Total :</td>
              <td align="right" width="10%">$0.00</td>
              <td align="right" width="10%">$0.00</td>
              <td align="right" width="10%"></td>
              <td align="right" width="10%"></td>
              <td align="right" width="10%"></td>
              <td align="right" width="5%"></td>
            </tr>
        </table>
    </asp:Panel>
    
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td align="right" class="colheader4">Non Postable Amount :</td>
                        <td><asp:Label ID="lblNPA" runat="server" Text="" /></td>
                    </tr>
                    <tr>
                        <td align="right" class="colheader4">Non Billable Non Postable Amount : </td>
                        <td><asp:Label ID="lblNBNPA" runat="server" Text="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td></tr>
    </table>
        
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr class="data">
            <td class="data" align="center">
                <asp:button id="btnAvailForBilling" class="button" runat="server" text="Available For Billing" />
                &nbsp;
                <asp:button id="btnVoidIt" class="button" runat="server" text="Void It" OnClientClick="return voidit();;" />
                &nbsp;
                <asp:button id="btnGoToFunds" class="button" runat="server" text="Go to Funds" OnClientClick="return gotofunds();;"/>
                &nbsp;
                <asp:button id="btnPrintHistory" class="button" runat="server" text="  Print History  " OnClientClick="" />
                &nbsp;
                <asp:button id="btnPrintReport" class="button" runat="server" text="  T&amp;E Report  " />
            </td>
        </tr>
    </table>
   
    <asp:HiddenField ID="billed_void" runat="server" />
    <asp:HiddenField ID="claimid" runat="server" />
    <asp:HiddenField ID="h_status" runat="server" />
    <asp:HiddenField ID="hclmnum" runat="server" />
    <asp:HiddenField ID="hdnInvId" runat="server" />
    <asp:HiddenField ID="invoiceamt" runat="server" />
    <asp:HiddenField ID="ratetableid" runat="server" />
    <asp:HiddenField ID="SysFormName" runat="server" />
    
    <asp:textbox style="display: none" runat="server" id="FrmName" rmxref="Instance/Document/TimeExpense/FrmName" Text="tandebillexpsummery" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="TableId1" rmxref="Instance/Document/TimeExpense/TableId1" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="TableId2" rmxref="Instance/Document/TimeExpense/TableId2" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="TableId3" rmxref="Instance/Document/TimeExpense/TableId3" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="InvoiceIds" rmxref="Instance/Document/TimeExpense/InvoiceIds" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="IsContinue" rmxref="Instance/Document/TimeExpense/IsContinue" text="true" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="Status" rmxref="Instance/Document/TimeExpense/Status" rmxtype="hidden" />
   </form>
</body>
</html>