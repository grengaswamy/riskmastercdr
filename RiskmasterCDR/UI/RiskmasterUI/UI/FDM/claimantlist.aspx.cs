﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.FDM 
{
    public partial class ClaimantList : FDMBasePage
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        public bool sShowAddButton = false;
        public bool sShowDeleteButton = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadInitialPage();
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/ClaimantList/Claimant")
                         let xClaim = (int)Convert.ToInt64(c.Element("ClaimantRowId").Value)
                         orderby xClaim
                         select c;
                if (rootElement.XPathSelectElement("//Claimant/ClaimId") != null)
                {
                    claimid.Text = rootElement.XPathSelectElement("//Claimant/ClaimId").Value;
                }
                if (rootElement.XPathSelectElement("//Param/FormVariables/SysExData/Add") != null)
                {

                    sShowAddButton = Convert.ToBoolean(rootElement.XPathSelectElement("//Param/FormVariables/SysExData/Add").Attribute("type").Value);
                }
                if (rootElement.XPathSelectElement("//Param/FormVariables/SysExData/Delete") != null)
                {

                    sShowDeleteButton = Convert.ToBoolean(rootElement.XPathSelectElement("//Param/FormVariables/SysExData/Delete").Attribute("type").Value);
                }
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }

        protected void NavigateListDelete(object sender, EventArgs e)
        {
            try
            {
                base.NavigateListDelete(sender, e);
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/ClaimantList/Claimant")
                         let xClaim = (int)Convert.ToInt64(c.Element("ClaimantRowId").Value)
                         orderby xClaim
                         select c;
                if (rootElement.XPathSelectElement("//Claimant/ClaimId") != null)
                {
                    claimid.Text = rootElement.XPathSelectElement("//Claimant/ClaimId").Value;
                }

                //LoadInitialPage();

            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

    }
}
