﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;


namespace Riskmaster.UI.FDM
{
    //JIRA RMA-8753 nshah28 start
    public partial class Address : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            FDMPageLoad();
        }
        protected new void NavigateSave(object sender, EventArgs e)
        {
            base.NavigateSave(sender,e);
        }

        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);
        }
    }
}