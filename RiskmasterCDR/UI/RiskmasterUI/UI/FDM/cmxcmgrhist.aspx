<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cmxcmgrhist.aspx.cs" Inherits="Riskmaster.UI.FDM.Cmxcmgrhist"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Case Manager History</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="../../Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="attach()" src="../../Images/attach.gif"
                        width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                        onmouseover="this.src='../../Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/attach.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="FilteredDiary()" src="../../Images/filtereddiary.gif"
                        width="28" height="28" border="0" id="filtereddiary" alternatetext="View Record Diaries"
                        onmouseover="this.src='../../Images/filtereddiary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/filtereddiary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Diary()" src="../../Images/diary.gif"
                        width="28" height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='../../Images/diary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/diary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="../../Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Case Manager History" /><asp:label
            id="formsubtitle" runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSCaseMngHis" id="TABSCaseMngHis">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="CaseMngHis"
                id="LINKTABSCaseMngHis"><span style="text-decoration: none">Case Manager History</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABCaseMngHis" id="FORMTABCaseMngHis">
        <asp:textbox style="display: none" runat="server" id="cmcmhrowid" rmxref="/Instance/CmXCmgrHist/CmcmhRowId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="casemgtrowid"
                rmxref="/Instance/CmXCmgrHist/CasemgtRowId" rmxtype="id" /><div runat="server" class="half"
                    id="div_casemanagerlist" xmlns="">
                    <asp:label runat="server" class="required" id="lbl_casemanagerlist" text="Case Manager" /><span
                        class="formw"><asp:textbox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                            id="casemanagerlist" rmxref="/Instance/CmXCmgrHist/CaseMgrEid" rmxtype="eidlookup"
                            cancelledvalue="" tabindex="1" /><input type="button" class="button" value="..."
                                name="casemanagerlistbtn" tabindex="2" onclick="lookupData('casemanagerlist','CASE_MANAGER',4,'casemanagerlist',2)" /><asp:textbox
                                    style="display: none" runat="server" id="casemanagerlist_cid" rmxref="/Instance/CmXCmgrHist/CaseMgrEid/@codeid"
                                    cancelledvalue="" /><asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required"
                                        runat="server" controltovalidate="casemanagerlist" /></span></div>
        <div runat="server" class="half" id="div_casestatustype" xmlns="">
            <asp:label runat="server" class="label" id="lbl_casestatustype" text="Case Status" /><span
                class="formw"><asp:textbox runat="server" onchange="lookupTextChanged(this);" id="casestatustype"
                    rmxref="/Instance/CmXCmgrHist/CaseStatusCode" rmxtype="code" cancelledvalue=""
                    tabindex="11" onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl"
                        runat="Server" id="casestatustypebtn" onclientclick="return selectCode('CASE_STATUS','casestatustype');"
                        tabindex="12" /><asp:textbox style="display: none" runat="server" id="casestatustype_cid"
                            rmxref="/Instance/CmXCmgrHist/CaseStatusCode/@codeid" cancelledvalue="" /></span></div>
        <asp:textbox style="display: none" runat="server" id="eventnumber" rmxref="/Instance/UI/FormVariables/SysExData/EventNumber"
            rmxtype="id" /><div runat="server" class="half" id="div_reftype" xmlns="">
                <asp:label runat="server" class="label" id="lbl_reftype" text="Referral Type" /><span
                    class="formw"><asp:textbox runat="server" onchange="lookupTextChanged(this);" id="reftype"
                        rmxref="/Instance/CmXCmgrHist/RefTypeCode" rmxtype="code" cancelledvalue="" tabindex="3"
                        onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl" runat="Server"
                            id="reftypebtn" onclientclick="return selectCode('REF_TYPE','reftype');" tabindex="4" /><asp:textbox
                                style="display: none" runat="server" id="reftype_cid" rmxref="/Instance/CmXCmgrHist/RefTypeCode/@codeid"
                                cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_dateclosed" xmlns="">
            <asp:label runat="server" class="label" id="lbl_dateclosed" text="Date Closed" /><span
                class="formw"><asp:textbox runat="server" formatas="date" onchange="calculatedayswithcasemgr();;setDataChanged(true);&#xA;              "
                    id="dateclosed" rmxref="/Instance/CmXCmgrHist/DateClosed" rmxtype="datebuttonscript"
                    name="dateclosed" /><script language="JavaScript" src="">{var i;}</script><asp:button
                        runat="server" type="button" class="button" id="dateclosedbtn" text="..." tabindex="14"
                        onclientclick="calculatedayswithcasemgr();" /><cc1:CalendarExtender runat="server"
                            ID="dateclosed_ajax" TargetControlID="dateclosed" />
            </span>
        </div>
        <div runat="server" class="half" id="div_referraldate" xmlns="">
            <asp:label runat="server" class="label" id="lbl_referraldate" text="Referral Date" /><span
                class="formw"><asp:textbox runat="server" formatas="date" onchange="calculatedayswithcasemgr();;setDataChanged(true);&#xA;              "
                    id="referraldate" rmxref="/Instance/CmXCmgrHist/RefDate" rmxtype="datebuttonscript"
                    name="referraldate" /><script language="JavaScript" src="">{var i;}</script><asp:button
                        runat="server" type="button" class="button" id="referraldatebtn" text="..." tabindex="6"
                        onclientclick="calculatedayswithcasemgr();" /><cc1:CalendarExtender runat="server"
                            ID="referraldate_ajax" TargetControlID="referraldate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_dayswithcasemgr" xmlns="">
            <asp:label runat="server" class="label" id="lbl_dayswithcasemgr" text="Days with Case Mgr" /><span
                class="formw"><asp:textbox runat="Server" rmxref="/Instance/UI/FormVariables/SysExData/CaseMgrDays"
                    id="dayswithcasemgr" tabindex="15" style="background-color: silver;" readonly="true" /></span></div>
        <div runat="server" class="half" id="div_referredto" xmlns="">
            <asp:label runat="server" class="label" id="lbl_referredto" text="Referred To" /><span
                class="formw"><asp:textbox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                    id="referredto" rmxref="/Instance/CmXCmgrHist/RefToEid" rmxtype="eidlookup" cancelledvalue=""
                    tabindex="7" /><input type="button" class="button" value="..." name="referredtobtn"
                        tabindex="8" onclick="lookupData('referredto','',4,'referredto',2)" /><asp:textbox
                            style="display: none" runat="server" id="referredto_cid" rmxref="/Instance/CmXCmgrHist/RefToEid/@codeid"
                            cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_primarycmgrflag" xmlns="">
            <asp:label runat="server" class="label" id="lbl_primarycmgrflag" text="Primary  Case Manager" /><span
                class="formw"><asp:checkbox runat="server" onchange="setDataChanged(true);" id="primarycmgrflag"
                    rmxref="/Instance/CmXCmgrHist/PrimaryCmgrFlag" rmxtype="checkbox" tabindex="16" /></span></div>
        <div runat="server" class="half" id="div_refreasoncode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_refreasoncode" text="Referral Reason" /><span
                class="formw"><asp:textbox runat="server" onchange="lookupTextChanged(this);" id="refreasoncode"
                    rmxref="/Instance/CmXCmgrHist/RefReasonCode" rmxtype="code" cancelledvalue=""
                    tabindex="9" onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl"
                        runat="Server" id="refreasoncodebtn" onclientclick="return selectCode('REF_REASON','refreasoncode');"
                        tabindex="10" /><asp:textbox style="display: none" runat="server" id="refreasoncode_cid"
                            rmxref="/Instance/CmXCmgrHist/RefReasonCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_duedate" xmlns="">
            <asp:label runat="server" class="label" id="lbl_duedate" text="UR Due Date" /><span
                class="formw"><asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);"
                    id="duedate" rmxref="/Instance/CmXCmgrHist/DueDate" rmxtype="date" tabindex="17" /><cc1:CalendarExtender
                        runat="server" ID="duedate_ajax" TargetControlID="duedate" />
            </span>
        </div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup">
        <asp:textbox style="display: none" runat="server" id="supp_cmcmh_row_id" rmxref="/Instance/*/Supplementals/CMCMH_ROW_ID"
            rmxtype="id" /></div>
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnCMNotes">
            <asp:button class="button" runat="Server" id="btnCMNotes" text="Case Manager Notes"
                rmxref="Instance/CmXCmgrHist/CaseMgrNotesList/@committedcount" onclientclick="if(!( XFormHandler('SysFormPIdName=cmcmhrowid&SysFormName=CaseMgrNotes&SysCmd=1&SysFormIdName=cmgrnotesrowid&SysEx=/Instance/CmXCmgrHist/CmcmhRowId','','')))return false;"
                postbackurl="?SysFormPIdName=cmcmhrowid&SysFormName=CaseMgrNotes&SysCmd=1&SysFormIdName=cmgrnotesrowid&SysEx=/Instance/CmXCmgrHist/CmcmhRowId" /></div>
        <div class="formButton" runat="server" id="div_btnToCmXCmgrHist">
            <asp:button class="button" runat="Server" id="btnToCmXCmgrHist" text="Back to Case Manager History List"
                onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"
                postbackurl="?SysViewType=controlsonly&SysCmd=1" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="CmXCmgrHist" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" text="&lt;CmXCmgrHist&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/CmXCmgrHist&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysPSid"
                                    rmxref="Instance/UI/FormVariables/SysPSid" rmxtype="hidden" text="" /><asp:textbox
                                        style="display: none" runat="server" id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId"
                                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysSid"
                                            rmxref="Instance/UI/FormVariables/SysSid" rmxtype="hidden" text="" /><asp:textbox
                                                style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormName"
                                                    rmxref="Instance/UI/FormVariables/SysFormName" rmxtype="hidden" text="CmXCmgrHist" /><asp:textbox
                                                        style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
                                                        rmxtype="hidden" text="cmcmhrowid" /><asp:textbox style="display: none" runat="server"
                                                            id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden"
                                                            text="casemgtrowid" /><asp:textbox style="display: none" runat="server" id="SysFormPForm"
                                                                rmxref="Instance/UI/FormVariables/SysFormPForm" rmxtype="hidden" text="" /><asp:textbox
                                                                    style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                                                    rmxtype="hidden" text="eventnumber,claimnumber" /><asp:textbox style="display: none"
                                                                        runat="server" name="formname" value="CmXCmgrHist" /><asp:textbox style="display: none"
                                                                            runat="server" name="SysRequired" value="casemanagerlist|" /><asp:textbox style="display: none"
                                                                                runat="server" name="SysFocusFields" value="casemanagerlist|" /><input type="hidden"
                                                                                    id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible" style="display: none" /><asp:textbox
                                                                                        runat="server" id="SysLookupClass" style="display: none" /><asp:textbox runat="server"
                                                                                            id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server" id="SysLookupAttachNodePath"
                                                                                                style="display: none" /><asp:textbox runat="server" id="SysLookupResultConfig" style="display: none" /><input
                                                                                                    type="hidden" value="rmx-widget-handle-2" id="SysWindowId" /></form>
</body>
</html>
