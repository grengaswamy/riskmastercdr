﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using Riskmaster.UI.Shared.Controls;
//Anu Tennyson for MITS - 18229 STARTS 12/21/2009
using Riskmaster.Common;
//Anu Tennyson for MITS - 18229 ENDS
using MultiCurrencyCustomControl;

namespace Riskmaster.UI.FDM
{
    public partial class Coverages : NonFDMBasePageCWS
    {
        private const string m_sCodeLookup = "codelookup";
        private const string m_sMultiCode = "multicode";
        private const string m_sUserName = "rmsyslookup";
		//Anu Tennyson for MITS - 18229 STARTS 12/21/2009
        private const string m_FUNCTION_CALL_GET_DEDUCTIBLE = "EnhancePolicyAdaptor.CalculateDeductible";
        private const string sREF = "/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/PolicyXCvgUarAddedPC";
        private const string sREFCodeId = "Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/PolicyXCvgUarAddedPC/@codeid";
		//Anu Tennyson for MITS - 18229 ENDS

        protected void Page_Load(object sender, EventArgs e)
        {
             bool bReturnStatus = false;
            XElement XmlTemplate = null;
            string sreturnValue = "";
            //Anu Tennyson for MITS - 18229 STARTS 12/21/2009
            XmlDocument objAmount = null;
            bool bIsSuccess = false;
            bool bDeductiblebtn = false;
            XElement XmlDeductibleTemplate = null;
            string sShowDeductibleGLBtn = string.Empty;
            string sCalculateDeductible = string.Empty;
            string sChildOrgHSessionID = string.Empty;
            string sMode = string.Empty;
            string sFormName = string.Empty;
            string sLob = string.Empty;
            string sOrg = string.Empty;
            string sDeductible = string.Empty;
            string sDeductibleBtn = string.Empty;
            string sDeductibleWC = string.Empty;
            string sAmount = string.Empty;
            string sModifier = string.Empty;
            string sDedPresent = string.Empty;
            decimal iDeductible = 0.00m;
            decimal iDeductibleWC = 0.00m;
            decimal iAmount = 0.00m;
            //Start: Neha Suresh Jain, 05/11/2010,MITS 20772 variable declaration for policy Limit
            decimal iPolicyLimit = 0;
            //End: Neha Suresh Jain
            string sFlatOrPercent = string.Empty;
            int iFlatOrPercent = 0;
            int iRateRowId = 0;
            XmlDocument XmlDeductibleDocument = new XmlDocument();
            int iStateId = 0; 
            //Anu Tennyson for MITS - 18229 ENDS

            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes

            

            if (!IsPostBack)
            {
                // Read the Values from Querystring
                int iPolicyId = 0;
                if (AppHelper.GetQueryStringValue("policyid") != "")
                    iPolicyId = Int32.Parse(AppHelper.GetQueryStringValue("policyid"));

                int iCoverageId = 0;
                if (AppHelper.GetQueryStringValue("selectedid") != "")
                    iCoverageId = Int32.Parse(AppHelper.GetQueryStringValue("selectedid"));

                string sSessionId = string.Empty;
                if (AppHelper.GetQueryStringValue("sessionid") != "")
                    sSessionId = AppHelper.GetQueryStringValue("sessionid");

                int iTransId = 0;
                if (AppHelper.GetQueryStringValue("selectedtransactionid") != "")
                    iTransId = Int32.Parse(AppHelper.GetQueryStringValue("selectedtransactionid"));

                string sEffectiveDate = AppHelper.GetQueryStringValue("effectivedate");
                string sExpirationDate = AppHelper.GetQueryStringValue("expirationdate");

                //Anu Tennyson for MITS - 18229 STARTS 12/21/2009
                if (!(String.IsNullOrEmpty(AppHelper.GetQueryStringValue("state"))))
                {
                    //iStateId = Int32.Parse(AppHelper.GetQueryStringValue("state"));
                    iStateId = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("state"), out bIsSuccess);
                }
                TextBox txtState = (TextBox)this.FindControl("state");
                if (txtState != null)
                {
                    txtState.Text = iStateId.ToString();
                }
                sMode = AppHelper.GetQueryStringValue("mode");
                sFormName = AppHelper.GetQueryStringValue("sysformname");
                sLob = AppHelper.GetQueryStringValue("sLOB");
                TextBox txtLineOfBusiness = (TextBox)this.FindControl("lineofbusiness");
                if (txtLineOfBusiness != null)
                {
                    txtLineOfBusiness.Text = sLob;
                }
                sOrg = AppHelper.GetQueryStringValue("sOrg");
                //Anu Tennyson for MITS - 18229 ENDS
                TextBox sPrevFormName = (TextBox)this.FindControl("prvfrmname");
                if (sPrevFormName != null)
                {
                    sPrevFormName.Text = sFormName;
                }
                // These Controls are used when Set Coverage is Called. 
                // These controls should be different then the Controls meant for display or for Coverages.
                // Like when a Quote is saved though the DB has a Transaction Id Saved,but there will not be any record for it.
                TextBox txtPolicyId = (TextBox)this.FindControl("PolId");
                if (txtPolicyId != null)
                {
                    txtPolicyId.Text = iPolicyId.ToString();
                }
                TextBox txtTransId = (TextBox)this.FindControl("TransId");
                if (txtTransId != null)
                {
                    txtTransId.Text = iTransId.ToString();
                }
                TextBox txtEffectiveDate = (TextBox)this.FindControl("EffDate");
                if (txtEffectiveDate != null)
                {
                    txtEffectiveDate.Text = sEffectiveDate;
                }
                TextBox txtExpirationDate = (TextBox)this.FindControl("ExpDate");
                if (txtExpirationDate != null)
                {
                    txtExpirationDate.Text = sExpirationDate;
                }

                TextBox txtGridName = (TextBox)this.FindControl("gridname");
                if (txtGridName != null)
                {
                    txtGridName.Text = AppHelper.GetQueryStringValue("gridname");
                }
                TextBox txtmode = (TextBox)this.FindControl("mode");
                if (txtmode != null)
                {
                    txtmode.Text = AppHelper.GetQueryStringValue("mode");
                }
                TextBox txtselectedrowposition = (TextBox)this.FindControl("selectedrowposition");
                if (txtselectedrowposition != null)
                {
                    txtselectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                }
                TextBox txtCoverageId = (TextBox)this.FindControl("selectedid");
                if (txtCoverageId != null)
                {
                    txtCoverageId.Text = iCoverageId.ToString();
                }

                //Anu Tennyson for MITS - 18229 STARTS 12/21/2009
                //Anu Tennyson for MITS 18229 - UAR List Field Added STARTS
                TextBox txtUarAddedLst = (TextBox)this.FindControl("uaraddedlist_lst");
                if (txtUarAddedLst != null)
                {
                    if (txtUarAddedLst.Text == "")
                    {
                        if (this.FindControl("uaraddedlist") != null)
                        {
                            if (sLob == "PC")
                            {
                                WebControl UarAddedLst = (WebControl)this.FindControl("uaraddedlist");
                                UarAddedLst.Attributes["RMXRef"] = sREF;
                                WebControl UarAddedLst_lst = (WebControl)this.FindControl("uaraddedlist_lst");
                                UarAddedLst_lst.Attributes["RMXRef"] = sREFCodeId;
                            }
                            ListBox lstUarAdded = (ListBox)this.FindControl("uaraddedlist");
                            ListItem lstItemNoUarAdded = new ListItem("None Selected", "0");
                            lstUarAdded.Items.Add(lstItemNoUarAdded);
                        }
                    }

                }

                if (sFormName == "policyenhgl" || sFormName == "policyenhwc")
                {
                    HtmlContainerControl oUARListDivControl = (HtmlContainerControl)this.FindControl("div_uaraddedlist");

                    if (oUARListDivControl != null)
                    {
                        oUARListDivControl.Visible = false;
                    }
                }
                //Anu Tennyson for MITS 18229 - UAR List Field Added ENDS
                TextBox txtDeductiblebtnSet = (TextBox)this.FindControl("deductiblebtnset");
                Control oControl = this.FindControl("Deductiblebtn");
                if (txtDeductiblebtnSet != null)
                {
                    txtDeductiblebtnSet.Text = "UnSet";
                }
                if (oControl != null)
                {
                    ((Button)oControl).Visible = false;
                    ((Button)oControl).Enabled = true;
                }
                TextBox txtCalcDeductible = (TextBox)this.FindControl("calcdeductible");
                if ((sFormName == "policyenhpc" || sFormName == "policyenhal" || sFormName == "policyenhgl"))
                {
                    XmlDeductibleTemplate = GetLoadMessageTemplate(iCoverageId, sSessionId, iPolicyId, iTransId, sEffectiveDate, sExpirationDate, iStateId, sLob, sOrg, sMode);
                    bDeductiblebtn = CallCWS("EnhancePolicyAdaptor.CheckDeductiblebtn", XmlDeductibleTemplate, out sreturnValue, false, true);
                    XmlDeductibleDocument.LoadXml(sreturnValue);
                    if (XmlDeductibleDocument.SelectSingleNode("//Coverages/ShowDeductibleBtnGL/ShowDeductibleBtnGLFlag") != null)
                    {
                        sShowDeductibleGLBtn = XmlDeductibleDocument.SelectSingleNode("//Coverages/ShowDeductibleBtnGL/ShowDeductibleBtnGLFlag").InnerText;
                    }
                    if (XmlDeductibleDocument.SelectSingleNode("//Coverages/ShowDeductibleBtnGL/CalculateDeductible") != null)
                    {
                        sCalculateDeductible = XmlDeductibleDocument.SelectSingleNode("//Coverages/ShowDeductibleBtnGL/CalculateDeductible").InnerText;
                    }
                    if (XmlDeductibleDocument.SelectSingleNode("//Coverages/ShowDeductibleBtnGL/ChildOrgHierarchySessionID") != null)
                    {
                        sChildOrgHSessionID = XmlDeductibleDocument.SelectSingleNode("//Coverages/ShowDeductibleBtnGL/ChildOrgHierarchySessionID").InnerText;
                    }
                    TextBox txtOrgHierarchy = (TextBox)this.FindControl("orghierarchysessionid");
                    if (txtOrgHierarchy != null)
                    {
                        txtOrgHierarchy.Text = sChildOrgHSessionID;
                    }
                    if (txtCalcDeductible != null)
                    {
                        txtCalcDeductible.Text = sCalculateDeductible;
                    }
                    if (sShowDeductibleGLBtn == "true")
                    {
                        if (oControl != null)
                        {
                            ((Button)oControl).Visible = true;
                        }
                        if (txtDeductiblebtnSet != null)
                        {
                            txtDeductiblebtnSet.Text = "Set";
                        }
                    }
                    if (sShowDeductibleGLBtn == "false")
                    {
                        if (oControl != null)
                        {
                            ((Button)oControl).Enabled = false;
                        }
                    }
                }
                //Anu Tennyson for MITS - 18229 ENDS
                XmlTemplate = GetLoadMessageTemplate(iCoverageId, sSessionId, iPolicyId, iTransId, sEffectiveDate, sExpirationDate, iStateId, sLob, sChildOrgHSessionID, sMode);
                bReturnStatus = CallCWS("EnhancePolicyAdaptor.GetCoverages", XmlTemplate, out sreturnValue, false, true);
                //Anu Tennyson for MITS - 18229 STARTS 12/21/2009
                if ((sFormName == "policyenhpc" || sFormName == "policyenhal" || sFormName == "policyenhgl") && (sMode == "edit"))
                {
                    objAmount = new XmlDocument();
                    objAmount.LoadXml(sreturnValue);
                    if (objAmount.SelectSingleNode("//Coverages/DeductibleBtn") != null)
                    {
                        sDeductibleBtn = objAmount.SelectSingleNode("//Coverages/DeductibleBtn").InnerText;
                    }
                    if (objAmount.SelectSingleNode("//Coverages/FlatOrPercent") != null)
                    {
                        sFlatOrPercent = objAmount.SelectSingleNode("//Coverages/FlatOrPercent").InnerText;
                    }
                    if (objAmount.SelectSingleNode("//Coverages/DedPresent") != null)
                    {
                        sDedPresent = objAmount.SelectSingleNode("//Coverages/DedPresent").InnerText;
                        TextBox txtdeductiblepresent = (TextBox)this.FindControl("deductiblepresent");
                        if (txtdeductiblepresent != null)
                        {
                            txtdeductiblepresent.Text = "NotPresent";
                        }
                    }
                    //Start: Neha Suresh Jain, 05/14/2010,MITS 20772, to get Short code 
                    TextBox txtFlatPerS = (TextBox)this.FindControl("FlatOrPerS");
                    if (txtFlatPerS != null)
                    {
                        txtFlatPerS.Text = sFlatOrPercent;
                    }
                    //End: Neha Suresh Jain
                    if (sDeductibleBtn == "true")
                    {
                        Control oControl1 = this.FindControl("Deductiblebtn");
                        if (oControl1 != null)
                        {
                            ((Button)oControl1).Enabled = true;
                        }
                    }
                    if (sDeductibleBtn == "false")
                    {
                        Control oControl1 = this.FindControl("Deductiblebtn");
                        if (oControl1 != null)
                        {
                            ((Button)oControl1).Enabled = false;
                        }
                    }
                    CurrencyTextbox txtDeductible = (CurrencyTextbox)this.FindControl("Deductible");
                    TextBox txtDeductibleAmount = (TextBox)this.FindControl("DeductibleAmount");
                    //Start: Neha Suresh Jain,05/12/2010,MITS 20772 Code commented as deductible wil always be displayed as dollar amount
                    //if (sFlatOrPercent == "P")      
                    //{
                    //    txtDeductible.Text = txtDeductible.Text + "%";                            
                    //}
                    //else   
                    //{
                    if (txtDeductible != null && txtDeductibleAmount != null)
                    {
                        //rupal:start, MITS 27502
                        //sDeductible = txtDeductible.Text;
                        sDeductible = txtDeductible.Amount.ToString();
                        iDeductible = Conversion.CastToType<decimal>(sDeductible, out bIsSuccess);
                        txtDeductibleAmount.Text = String.Format("{0:C}", iDeductible);
                        //txtDeductible.Amount = String.Format("{0:C}", iDeductible);
                        txtDeductible.Amount = iDeductible;
                        //rupal:end
                    }
                    //}
                    //End: Neha Suresh Jain

                }
                if (sFormName == "policyenhwc" && sMode == "edit")
                {
                    CurrencyTextbox txtDeductible = (CurrencyTextbox)this.FindControl("Deductible");
                    if (txtDeductible != null)
                    {
                        //rupal:start, MITS 27502
                        //sDeductibleWC = txtDeductible.Text;
                        sDeductible = txtDeductible.Amount.ToString();
                        iDeductibleWC = Conversion.CastToType<decimal>(sDeductibleWC, out bIsSuccess);
                        //txtDeductible.Text = String.Format("{0:C}", iDeductibleWC);
                        txtDeductible.Amount = iDeductibleWC;
                    }
                }

            }

            else
            {
                TextBox txtFunctionToCall = (TextBox)this.FindControl("FunctionToCall");
                if (txtFunctionToCall != null)
                {
                    if (txtFunctionToCall.Text == m_FUNCTION_CALL_GET_DEDUCTIBLE)
                    {
                        XmlTemplate = null;
                        bReturnStatus = CallCWS(m_FUNCTION_CALL_GET_DEDUCTIBLE, XmlTemplate, out sreturnValue, true, true);
                        objAmount = new XmlDocument();
                        objAmount.LoadXml(sreturnValue);
                        XmlElement objRange = null;
                        objRange = (XmlElement)objAmount.SelectSingleNode("//Coverages/Deductible/Range");
                        if (objRange != null)
                        {
                            Control oControl = this.FindControl("Deductiblebtn");
                            if (oControl != null)
                            {
                                ((Button)oControl).Enabled = true;
                            }
                            CurrencyTextbox txtDeductible = (CurrencyTextbox)this.FindControl("Deductible");
                            //txtDeductible.BackColor = System.Drawing.Color.DarkBlue;
                            if (txtDeductible != null)
                            {
                                //rupal:mits 27555
                                //control should be set to readonly instead of enabled=fale so that it retains its value during postbacks
                                
                                //txtDeductible.Enabled = false;
                                //txtDeductible.Style.Remove(HtmlTextWriterStyle.BackgroundColor);
                                //txtDeductible.BackColor = System.Drawing.Color.Silver;
                                //txtDeductible.Style.Add(HtmlTextWriterStyle.BackgroundColor, "silver");
                                
                                txtDeductible.ReadOnly = true;
                                //txtDeductible.Style.Remove(HtmlTextWriterStyle.BackgroundColor);
                                //txtDeductible.BackColor = System.Drawing.Color.Silver;
                                //txtDeductible.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");                                
                                //rupal:end
                                
                            }
                            TextBox txtDeductibleAmount = (TextBox)this.FindControl("DeductibleAmount");
                            //start:Neha Suresh Jain, 05/13/2010,MITS 20772 to calculate deductible during postback
                            // iDeductible = 0;
                            //txtDeductible.Text = Convert.ToString(iDeductible);
                            TextBox txtSelectedDeductible = (TextBox)this.FindControl("SelectedDeductible");
                            CurrencyTextbox txtPolicyLimit = (CurrencyTextbox)this.FindControl("PolicyLimit");
                            iFlatOrPercent = Conversion.CastToType<int>(objAmount.SelectSingleNode("//Coverages/Deductible/FlatOrPercid").InnerText, out bIsSuccess);
                            TextBox txtFlatOrPercent = (TextBox)this.FindControl("FlatOrPercent");
                            if (txtFlatOrPercent != null)
                            {
                                txtFlatOrPercent.Text = iFlatOrPercent.ToString();
                            }
                            sFlatOrPercent = objAmount.SelectSingleNode("//Coverages/Deductible/FlatOrPerc").InnerText;
                            TextBox txtFlatPerS = (TextBox)this.FindControl("FlatOrPerS");
                            if (txtFlatPerS != null)
                            {
                                txtFlatPerS.Text = sFlatOrPercent;
                            }
                            if (sFlatOrPercent == "P")
                            {
                                if (txtSelectedDeductible != null && txtPolicyLimit != null)
                                {
                                    iDeductible = Conversion.CastToType<decimal>(txtSelectedDeductible.Text, out bIsSuccess);
                                    //rupal:start, mits 27502
                                    //this is actually multicurrency enhancement but I found this issue while doing the MITS 27502 
                                    //String sPolicyLimit = String.Join(null, System.Text.RegularExpressions.Regex.Split(txtPolicyLimit.Text, "[^\\d/.]")); ;
                                    String sPolicyLimit = String.Join(null, System.Text.RegularExpressions.Regex.Split(txtPolicyLimit.AmountInString, "[^\\d/.]")); ;
                                    //rupal:end
                                    iPolicyLimit = Conversion.CastToType<decimal>(sPolicyLimit, out bIsSuccess);
                                    
                                    iDeductible = (iDeductible * iPolicyLimit) / 100;
                                }
                            }
                            else
                            {
                                if (txtSelectedDeductible != null)
                                {
                                    iDeductible = Conversion.CastToType<decimal>(txtSelectedDeductible.Text, out bIsSuccess);
                                }
                            }
                            if (txtDeductible != null && txtDeductibleAmount != null)
                            {
                                txtDeductibleAmount.Text = String.Format("{0:C}", iDeductible);
                                //txtDeductible.Text = String.Format("{0:C}", iDeductible);
                                txtDeductible.Amount = iDeductible;
                            }
                            //End: Neha Suresh Jain
                        }
                        if (objRange == null)
                        {
                            Control oControl = this.FindControl("Deductiblebtn");
                            if (oControl != null)
                            {
                                ((Button)oControl).Enabled = false;
                            }
                            // Set the Value of Controls
                            // Amount Value
                            CurrencyTextbox txtDeductible = (CurrencyTextbox)this.FindControl("Deductible");
                            //rupal:start, mits 27555
                            if (txtDeductible != null)
                            {
                                txtDeductible.ReadOnly = false;
                            }
                            //rupal:end
                            TextBox txtDeductibleAmount = (TextBox)this.FindControl("DeductibleAmount");
                            if (objAmount.SelectSingleNode("//Coverages/Deductible/Amount") != null)
                            {
                                sAmount = objAmount.SelectSingleNode("//Coverages/Deductible/Amount").InnerText;
                            }
                            //Start: Neha Suresh Jain,MITS 20772 when no range is defined, clear selected deductible
                            TextBox txtSelectedDeductible = (TextBox)this.FindControl("SelectedDeductible");
                            if (txtSelectedDeductible != null)
                            {
                                txtSelectedDeductible.Text = "";
                            }
                            //End: Neha Suresh Jain
                            TextBox txtModifier = (TextBox)this.FindControl("Modifier");
                            sModifier = objAmount.SelectSingleNode("//Coverages/Deductible/Amount").InnerText;
                            if (txtModifier != null)
                            {
                                txtModifier.Text = sModifier;
                            }
                            TextBox txtRateRowId = (TextBox)this.FindControl("ExpRateRowId");
                            iRateRowId = Conversion.CastToType<int>(objAmount.SelectSingleNode("//Coverages/Deductible/RateRowId").InnerText, out bIsSuccess);
                            if (txtRateRowId != null)
                            {
                                txtRateRowId.Text = iRateRowId.ToString();
                            }
                            XmlElement objFlatPerc = null;
                            objFlatPerc = (XmlElement)objAmount.SelectSingleNode("//Coverages/Deductible/FlatOrPerc");
                            if (objFlatPerc != null)
                            {
                                iFlatOrPercent = Conversion.CastToType<int>(objAmount.SelectSingleNode("//Coverages/Deductible/FlatOrPercid").InnerText, out bIsSuccess);
                                sFlatOrPercent = objAmount.SelectSingleNode("//Coverages/Deductible/FlatOrPerc").InnerText;
                                //Start: Neha Suresh Jain, 05/14/2010,MITS 20772, to get Short code 
                                TextBox txtFlatPerS = (TextBox)this.FindControl("FlatOrPerS");
                                if (txtFlatPerS != null)
                                {
                                    txtFlatPerS.Text = sFlatOrPercent;
                                }
                                //End:Neha Suresh Jain
                                TextBox txtFlatOrPercent = (TextBox)this.FindControl("FlatOrPercent");
                                if (txtFlatOrPercent != null)
                                {
                                    txtFlatOrPercent.Text = iFlatOrPercent.ToString();
                                }
                                if (sFlatOrPercent == "F")
                                {
                                    iAmount = Conversion.CastToType<decimal>(sAmount, out bIsSuccess);
                                    if (txtDeductible != null && txtDeductibleAmount != null)
                                    {
                                        //txtDeductible.Text = String.Format("{0:C}", iAmount);
                                        txtDeductible.Amount = iAmount;
                                        txtDeductibleAmount.Text = String.Format("{0:C}", iAmount);
                                    }
                                }
                                else if (sFlatOrPercent == "P")
                                {
                                    //Start: Neha Suresh Jain, 05/11/2010,MITS 20772, Deductible is calculated as Percent of Policy Limit
                                    //txtDeductible.Text = sAmount + "%";                              
                                    iAmount = Conversion.CastToType<decimal>(sAmount, out bIsSuccess);
                                    CurrencyTextbox txtPolicyLimit = (CurrencyTextbox)this.FindControl("PolicyLimit");
                                    if (txtDeductibleAmount != null && txtDeductible != null && txtPolicyLimit != null)
                                    {
                                        txtDeductibleAmount.Text = String.Format("{0:C}", iAmount);
                                        //rupal:start, mits 27502
                                        //this is actually multicurrency enhancement but I found this issue while doing the MITS 27502 
                                        //String sPolicyLimit = String.Join(null, System.Text.RegularExpressions.Regex.Split(txtPolicyLimit.Text, "[^\\d/.]")); ;
                                        String sPolicyLimit = String.Join(null, System.Text.RegularExpressions.Regex.Split(txtPolicyLimit.AmountInString, "[^\\d/.]")); ;
                                        //rupal:end
                                        iPolicyLimit = Conversion.CastToType<decimal>(sPolicyLimit, out bIsSuccess);
                                        iAmount = (iAmount * iPolicyLimit) / 100;
                                        //txtDeductible.Text = String.Format("{0:C}", iAmount);
                                        txtDeductible.Amount = iAmount;
                                    }
                                    //End: Neha Suresh Jain

                                }
                            }
                            else if (objFlatPerc == null)
                            {
                                TextBox txtdeductiblepresent = (TextBox)this.FindControl("deductiblepresent");
                                if (txtdeductiblepresent != null)
                                {
                                    txtdeductiblepresent.Text = "NotPresent";
                                }
                                iAmount = Conversion.CastToType<decimal>(sAmount, out bIsSuccess);
                                TextBox txtDeductibleValue = (TextBox)this.FindControl("DeductibleAmount");
                                if (txtDeductibleValue != null && txtDeductible != null)
                                {
                                    txtDeductibleValue.Text = String.Format("{0:C}", iAmount);
                                    //txtDeductible.Text = Convert.ToString(iAmount);
                                    txtDeductible.Amount = iAmount;
                                }
                            }
                        }
                        //Anu Tennyson for MITS - 18229 ENDS
                    }
                }
            }
      
        }
     
        protected void btnCoveragesOk_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = "";
            TextBox txtSessionId;
            bReturnStatus = CallCWS("EnhancePolicyAdaptor.SetCoverages", XmlTemplate, out sreturnValue, true, false);
            if (bReturnStatus)
            {
                // Set the Session Id
                XmlDoc.LoadXml(sreturnValue);
                // npadhy Start MITS 20721 When user Audits a Policy - user can enter Effective data for exposure beyond the term of the Policy
                if (ErrorHelper.IsCWSCallSuccess(sreturnValue))
                {
                    txtSessionId = (TextBox)this.FindControl("SessionId");
                    if (txtSessionId != null)
                    {
                        txtSessionId.Text = XmlDoc.SelectSingleNode("//Document/SetCoverages/SessionId").InnerText;
                    }

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CoverageOkClick", "<script>fnRefreshParentCoverageOk();</script>");
                }
                // npadhy End MITS 20721 When user Audits a Policy - user can enter Effective data for exposure beyond the term of the Policy
            }
        }
        

        //Anu Tennyson for MITS - 18229 STARTS 12/21/2009
        private XElement GetLoadMessageTemplate(int iCoveragesId, string sSessionId, int iPolicyId, int iTransId, string sEffectiveDate, string sExpirationDate, int iStateId, string sLob, string sOrg, string sMode )
        //Anu Tennyson for MITS - 18229 ENDS
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.GetCoverages</Function></Call><Document><Coverages><CoveragesId>");
            sXml = sXml.Append(iCoveragesId);
            sXml = sXml.Append("</CoveragesId><SessionId>");
            sXml = sXml.Append(sSessionId);
            sXml = sXml.Append("</SessionId><PolId >");
            sXml = sXml.Append(iPolicyId);
            sXml = sXml.Append("</PolId><TransId>");
            sXml = sXml.Append(iTransId);
            sXml = sXml.Append("</TransId><ExpirationDate>");
            sXml = sXml.Append(sExpirationDate);
            sXml = sXml.Append("</ExpirationDate><EffectiveDate>");
            sXml = sXml.Append(sEffectiveDate);
            sXml = sXml.Append("</EffectiveDate>");
			//Anu Tennyson for MITS - 18229 STARTS 12/21/2009
            sXml = sXml.Append("<State>");
            sXml = sXml.Append(iStateId);
            sXml = sXml.Append("</State><LOB>");
            sXml = sXml.Append(sLob);
            sXml = sXml.Append("</LOB><Org>");
            sXml = sXml.Append(sOrg);
            sXml = sXml.Append("</Org>");
            sXml = sXml.Append("<Mode>");
            sXml = sXml.Append(sMode);
            sXml = sXml.Append("</Mode>");           
			//Anu Tennyson for MITS - 18229 ENDS
            sXml = sXml.Append("</Coverages></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        public override void ModifyControl(XElement Xelement)
        {
            IEnumerable<XElement> suppList = Xelement.XPathSelectElements("./Coverages/SuppView/group/displaycolumn/*");

            foreach (XElement xSupp in suppList)
            {
                string sRef = xSupp.Attribute("ref").Value;
                string sControlName = xSupp.Attribute("name").Value;
                string sOnChange = xSupp.Attribute("onchange").Value;

                
                Control oCtrl = (Control)this.FindControl(sControlName);
                if (oCtrl != null)
                {
                    if (oCtrl is WebControl)
                    {
                        AssignAttributes(oCtrl, "", xSupp);
                    }
                    else if (oCtrl is CodeLookUp)
                    {
                        AssignAttributes(oCtrl, m_sCodeLookup, xSupp);
                    }

                    else if (oCtrl is MultiCodeLookUp)
                    {
                        AssignAttributes(oCtrl, m_sMultiCode, xSupp);
                    }
                    else if (oCtrl is SystemUsers)
                    {
                        AssignAttributes(oCtrl, m_sUserName, xSupp);
                    }
                }

            }
            
        }

        private void AssignAttributes(Control p_oCtrl, string p_sExtender,XElement p_xSupp)
        {
            string sRef = p_xSupp.Attribute("ref").Value;
            string sControlName = p_xSupp.Attribute("name").Value;
            string sOnChange = p_xSupp.Attribute("onchange").Value;
            string sType = p_xSupp.Attribute("type").Value;
            string sControlId = string.Empty;
            WebControl oWebctrl;
            Control oCtrl;
            if (p_sExtender == "")
            {
                oWebctrl = (WebControl)p_oCtrl;
                oWebctrl.Attributes["RMXRef"] = sRef;
                oWebctrl.Attributes["onchange"] = sOnChange;

                if(sType == "eidlookup")
                {
                    sControlName = p_oCtrl.ID + "_cid";
                    oCtrl = (Control)this.FindControl(sControlName);
                    {
                        oWebctrl = (WebControl)oCtrl;
                        if (oWebctrl != null)
                        {
                            oWebctrl.Attributes["RMXRef"] = sRef + "/@codeid";
                        }
                    }
                }
                if (sType == "entitylist")
                {
                    sControlName = p_oCtrl.ID + "_lst";
                    oCtrl = (Control)this.FindControl(sControlName);
                    {
                        oWebctrl = (WebControl)oCtrl;
                        if (oWebctrl != null)
                        {
                            oWebctrl.Attributes["RMXRef"] = sRef + "/@codeid";
                        }
                    }
                }
            }
            else
            {
                oWebctrl = (WebControl)p_oCtrl.FindControl(p_sExtender);
                if (oWebctrl != null)
                {
                    oWebctrl.Attributes["RMXRef"] = sRef;
                    oWebctrl.Attributes["onchange"] = sOnChange;
                    if (p_sExtender == m_sMultiCode)
                    {
                        // Add the Item SetRef attribute with the Listbox
                        if (oWebctrl.Attributes["ItemSetRef"] == null)
                            oWebctrl.Attributes.Add("ItemSetRef", sRef);
                        else
                            oWebctrl.Attributes["ItemSetRef"] = sRef;
                    }
                }

                sControlId = GetControlName(p_sExtender);

                if (sControlId != string.Empty)
                {
                    oWebctrl = (WebControl)p_oCtrl.FindControl(sControlId);
                    if (oWebctrl != null)
                    {
                        oWebctrl.Attributes["RMXRef"] = sRef + "/@codeid";
                    }
                }
                else
                {
                    throw new Exception(p_sExtender + " Type of Supplemental field is not implemented in coverages");
                }
            }
        }

        private string GetControlName(string p_sExtender)
        {
            string sControlName = string.Empty;
            switch (p_sExtender)
            {
                case m_sMultiCode:
                    sControlName = p_sExtender + "_lst";
                    break;
                case m_sUserName:
                case m_sCodeLookup:
                    sControlName = p_sExtender + "_cid";
                    break;
                default:
                    sControlName = "";
                    break;
            }
            return sControlName;
        }
        //Anu Tennyson for MITS - 18229 STARTS 12/21/2009
        public override void ModifyXml(ref XElement Xelement)
        {
            bool bIsSuccess = false;
            TextBox txtFunctionToCall = (TextBox)this.FindControl("FunctionToCall");
            if (txtFunctionToCall != null)
            {
                if (txtFunctionToCall.Text == m_FUNCTION_CALL_GET_DEDUCTIBLE)
                {
                    XElement xmlExposure = Xelement.XPathSelectElement("./Document/Coverages");
                    XElement xmlTarget = new XElement("State");
                    int iStateId = 0;
                    if (!(String.IsNullOrEmpty(AppHelper.GetQueryStringValue("state"))))
                    {
                        iStateId = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("state"), out bIsSuccess);
                    }

                    xmlTarget.Value = iStateId.ToString();
                    if (xmlExposure != null)
                    {
                        xmlExposure.Add(xmlTarget);
                    }

                    XElement xmlLobTarget = new XElement("LOB");
                    string sLob = AppHelper.GetQueryStringValue("sLOB");

                    xmlLobTarget.Value = sLob;
                    if (xmlExposure != null)
                    {
                        xmlExposure.Add(xmlLobTarget);
                    }

                    XElement xmlOrgTarget = new XElement("ORG");

                    TextBox txtOrgHierarchy = (TextBox)this.FindControl("orghierarchysessionid");
                    string sOrg = string.Empty;
                    if (txtOrgHierarchy != null)
                    {
                        sOrg = txtOrgHierarchy.Text;
                    }
                    xmlOrgTarget.Value = sOrg;
                    if (xmlExposure != null)
                    {
                        xmlExposure.Add(xmlOrgTarget);
                    }
                }
            }
        }
        //Anu Tennyson for MITS - 18229 ENDS
    }
}
