<%@ Page Language="C#" AutoEventWireup="true" Debug="true" CodeBehind="staffprivilege.aspx.cs"
    Inherits="Riskmaster.UI.FDM.Staffprivilege" ValidateRequest="false" EnableViewStateMac="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Staff Privileges</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28"
                        height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28"
                        height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28"
                        height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28"
                        height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28"
                        height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doLookup()" src="Images/lookup.gif"
                        width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='Images/lookup2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/lookup.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="formtitle">
        Staff Privileges</div>
    <div class="errtextheader">
    </div>
    <div id="Div1" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSprivileges" id="TABSprivileges">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="privileges"
                id="LINKTABSprivileges"><span style="text-decoration: none">Privileges</span></a></div>
        <div id="Div2" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABprivileges" id="FORMTABprivileges">
        <asp:textbox style="display: none" runat="server" id="staffeid" rmxref="/Instance/MedicalStaffPrivilege/StaffEid"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="staffprivid"
                rmxref="/Instance/MedicalStaffPrivilege/StaffPrivId" rmxtype="id" /><div runat="server"
                    class="" id="div_categorycode" xmlns="">
                    <span class="label">Category</span><span class="formw"><asp:textbox runat="server"
                        size="30" onchange="lookupTextChanged(this);" id="categorycode" rmxref="/Instance/MedicalStaffPrivilege/CategoryCode"
                        rmxtype="code" cancelledvalue="" tabindex="1" onblur="codeLostFocus(this.id);" /><asp:button
                            class="CodeLookupControl" runat="Server" id="categorycodebtn" onclientclick="return selectCode('PHYS_PRIV_CAT','categorycode');"
                            tabindex="2" /><asp:textbox style="display: none" runat="server" id="categorycode_cid"
                                rmxref="/Instance/MedicalStaffPrivilege/CategoryCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="" id="div_typecode" xmlns="">
            <span class="label">Type</span><span class="formw"><asp:textbox runat="server" size="30"
                onchange="lookupTextChanged(this);" id="typecode" rmxref="/Instance/MedicalStaffPrivilege/TypeCode"
                rmxtype="code" cancelledvalue="" tabindex="3" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="typecodebtn" onclientclick="return selectCode('PHYS_PRIV_TYPE','typecode');"
                    tabindex="4" /><asp:textbox style="display: none" runat="server" id="typecode_cid"
                        rmxref="/Instance/MedicalStaffPrivilege/TypeCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="" id="div_statuscode" xmlns="">
            <span class="label">Status</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="lookupTextChanged(this);" id="statuscode" rmxref="/Instance/MedicalStaffPrivilege/StatusCode"
                rmxtype="code" cancelledvalue="" tabindex="5" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="statuscodebtn" onclientclick="return selectCode('PHYS_PRIV_STATUS','statuscode');"
                    tabindex="6" /><asp:textbox style="display: none" runat="server" id="statuscode_cid"
                        rmxref="/Instance/MedicalStaffPrivilege/StatusCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="" id="div_intdate" xmlns="">
            <span class="label">Start Date</span><span class="formw"><asp:textbox runat="server"
                size="30" formatas="date" onchange="setDataChanged(true);" id="intdate" rmxref="/Instance/MedicalStaffPrivilege/IntDate"
                rmxtype="date" tabindex="7" /><cc1:CalendarExtender runat="server" ID="intdate_ajax"
                    TargetControlID="intdate" />
            </span>
        </div>
        <div runat="server" class="" id="div_enddate" xmlns="">
            <span class="label">End Date</span><span class="formw"><asp:textbox runat="server"
                size="30" formatas="date" onchange="setDataChanged(true);" id="enddate" rmxref="/Instance/MedicalStaffPrivilege/EndDate"
                rmxtype="date" tabindex="9" /><cc1:CalendarExtender runat="server" ID="enddate_ajax"
                    TargetControlID="enddate" />
            </span>
        </div>
    </div>
    <div id="Div3" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
            <asp:button id="Button1" class="button" runat="Server" text="Back to Staff" onclientclick="&#xA;            XFormHandler('','SysFormName=staff&SysCmd=0','1','back')&#xA;          " /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="MedicalStaffPrivilege" /><asp:textbox style="display: none"
                            runat="server" id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" name="SysSerializationConfig" text="&lt;MedicalStaffPrivilege&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/MedicalStaffPrivilege&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
                                rmxtype="hidden" text="staff" /><asp:textbox style="display: none" runat="server"
                                    id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden"
                                    text="staffeid" /><asp:textbox style="display: none" runat="server" id="SysFormPId"
                                        rmxref="Instance/UI/FormVariables/SysFormPId" rmxtype="hidden" text="" /><asp:textbox
                                            style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
                                            rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysEx"
                                                rmxref="Instance/UI/FormVariables/SysEx" rmxtype="hidden" text="" /><asp:textbox
                                                    style="display: none" runat="server" id="SysNotReqNew" rmxref="Instance/UI/FormVariables/SysNotReqNew"
                                                    rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormName"
                                                        rmxref="Instance/UI/FormVariables/SysFormName" rmxtype="hidden" text="staffprivilege" /><asp:textbox
                                                            style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
                                                            rmxtype="hidden" text="staffprivid" /><asp:textbox style="display: none" runat="server"
                                                                id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden"
                                                                text="" /><asp:textbox style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                                                    rmxtype="hidden" text="21300" /><asp:textbox style="display: none" runat="server"
                                                                        id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType" rmxtype="hidden"
                                                                        text="" /><asp:textbox style="display: none" runat="server" name="formname" value="staffprivilege" /><asp:textbox
                                                                            style="display: none" runat="server" name="SysRequired" value="" /><asp:textbox style="display: none"
                                                                                runat="server" name="SysFocusFields" value="categorycode|" /><input type="hidden"
                                                                                    id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible" style="display: none" /><asp:textbox
                                                                                        runat="server" id="SysLookupClass" style="display: none" /><asp:textbox runat="server"
                                                                                            id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server" id="SysLookupAttachNodePath"
                                                                                                style="display: none" /><asp:textbox runat="server" id="SysLookupResultConfig" style="display: none" /><input
                                                                                                    type="hidden" value="rmx-widget-handle-2" id="SysWindowId" /></form>
</body>
</html>
