﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
namespace Riskmaster.UI.FDM
{
    public partial class Qminitialreview : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            XElement oSysCmd = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables//SysCmd");
            Control oEventId = this.FindControl("eventid");
            if (oSysCmd != null && oEventId != null)
            {
                if (oSysCmd.Value == "7")
                {
                    ((TextBox)oEventId).Attributes["rmxignoreget"] = "true";                   
                }
                else
                {
                    ((TextBox)oEventId).Attributes["rmxignoreget"] = "";
                }
            }
        }
    }
}
