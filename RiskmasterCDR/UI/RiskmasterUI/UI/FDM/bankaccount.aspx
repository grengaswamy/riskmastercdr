<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bankaccount.aspx.cs" Inherits="Riskmaster.UI.FDM.Bankaccount"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Bank Accounts</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="../../Images/new.gif"
                        width="28" height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='../../Images/new2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/new.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="../../Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="../../Images/first.gif"
                        width="28" height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='../../Images/first2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/first.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="../../Images/prev.gif"
                        width="28" height="28" border="0" id="moveprevious" alternatetext="Move Previous"
                        onmouseover="this.src='../../Images/prev2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/prev.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="../../Images/next.gif"
                        width="28" height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='../../Images/next2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/next.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="../../Images/last.gif"
                        width="28" height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='../../Images/last2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/last.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="../../Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='../../Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/delete.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doLookup()" src="../../Images/lookup.gif"
                        width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='../../Images/lookup2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/lookup.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="../../Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Bank Accounts" /><asp:label id="formsubtitle"
            runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSgeneralinfo" id="TABSgeneralinfo">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="generalinfo"
                id="LINKTABSgeneralinfo"><span style="text-decoration: none">General Information</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSaccountfilter" id="TABSaccountfilter">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="accountfilter"
                id="LINKTABSaccountfilter"><span style="text-decoration: none">Account Owner</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABgeneralinfo" id="FORMTABgeneralinfo">
        <asp:textbox style="display: none" runat="server" id="accountid" rmxref="Instance/Account/AccountId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="bankentityid"
                rmxref="Instance/Account/BankEid/@codeid" rmxtype="id" /><asp:textbox style="display: none"
                    runat="server" id="entity_id" rmxref="Instance/Account/BankEntity/EntityId" rmxtype="id" /><div
                        runat="server" class="half" id="div_banklastname" xmlns="">
                        <asp:label runat="server" class="required" id="lbl_banklastname" text="Bank Name" /><span
                            class="formw"><asp:textbox runat="server" onchange="lookupTextChanged(this);" tabindex="1"
                                onblur="lookupLostFocus(this);" rmxref="Instance/Account/BankEntity/LastName"
                                rmxtype="entitylookup" id="banklastname" /><asp:requiredfieldvalidator validationgroup="vgSave"
                                    errormessage="Required" runat="server" controltovalidate="banklastname" /><asp:textbox
                                        runat="server" style="display: none" text="1" id="banklastname_creatable" /><asp:button
                                            runat="server" class="button" tabindex="2" id="banklastnamebtn" onclientclick="lookupDataFDM('banklastname','BANKS',4,'bank',1,'Instance/Account/BankEntity')"
                                            text="..." /></span></div>
        <div runat="server" class="half" id="div_accountnumber" xmlns="">
            <asp:label runat="server" class="required" id="lbl_accountnumber" text="Account #" /><span
                class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" id="accountnumber"
                    rmxref="Instance/Account/AccountNumber" rmxtype="text" /><asp:requiredfieldvalidator
                        validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="accountnumber" /></span></div>
        <div runat="server" class="half" id="div_bankaddr1" xmlns="">
            <asp:label runat="server" class="label" id="lbl_bankaddr1" text="Address 1" /><span
                class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" id="bankaddr1"
                    rmxref="Instance/Account/BankEntity/Addr1" rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_accountname" xmlns="">
            <asp:label runat="server" class="required" id="lbl_accountname" text="Account Name" /><span
                class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" id="accountname"
                    rmxref="Instance/Account/AccountName" rmxtype="text" /><asp:requiredfieldvalidator
                        validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="accountname" /></span></div>
        <div runat="server" class="half" id="div_bankaddr2" xmlns="">
            <asp:label runat="server" class="label" id="lbl_bankaddr2" text="Address 2" /><span
                class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" id="bankaddr2"
                    rmxref="Instance/Account/BankEntity/Addr2" rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_accttypecode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accttypecode" text="Account Type" /><span
                class="formw"><asp:textbox runat="server" onchange="lookupTextChanged(this);" id="accttypecode"
                    rmxref="Instance/Account/AcctTypeCode" rmxtype="code" cancelledvalue="" tabindex="11"
                    onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl" runat="Server"
                        id="accttypecodebtn" onclientclick="return selectCode('ACCOUNT_TYPE','accttypecode');"
                        tabindex="14" /><asp:textbox style="display: none" runat="server" id="accttypecode_cid"
                            rmxref="Instance/Account/AcctTypeCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_bankcity" xmlns="">
            <asp:label runat="server" class="label" id="lbl_bankcity" text="City" /><span class="formw"><asp:textbox
                runat="server" onchange="setDataChanged(true);" id="bankcity" rmxref="Instance/Account/BankEntity/City"
                rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_nextchecknumber" xmlns="">
            <asp:label runat="server" class="required" id="lbl_nextchecknumber" text="Next Check #" /><span
                class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" id="nextchecknumber"
                    rmxref="Instance/Account/NextCheckNumber" rmxtype="text" /><asp:requiredfieldvalidator
                        validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="nextchecknumber" /></span></div>
        <div runat="server" class="half" id="div_bankstateid" xmlns="">
            <asp:label runat="server" class="label" id="lbl_bankstateid" text="State" /><span
                class="formw"><asp:textbox runat="server" onchange="lookupTextChanged(this);" id="bankstateid"
                    rmxref="Instance/Account/BankEntity/StateId" rmxtype="code" cancelledvalue=""
                    tabindex="6" onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl"
                        runat="Server" id="bankstateidbtn" onclientclick="return selectCode('states','bankstateid');"
                        tabindex="7" /><asp:textbox style="display: none" runat="server" id="bankstateid_cid"
                            rmxref="Instance/Account/BankEntity/StateId/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_priority" xmlns="">
            <asp:label runat="server" class="label" id="lbl_priority" text="Priority" /><span
                class="formw"><asp:dropdownlist runat="server" id="priority" tabindex="14" rmxref="Instance/Account/Priority"
                    rmxtype="combobox" onchange="setDataChanged(true);"><asp:listitem value="0" text="None" />
                    <asp:listitem value="1" text="Lowest" />
                    <asp:listitem value="2" text="Low" />
                    <asp:listitem value="3" text="Low Moderate" />
                    <asp:listitem value="4" text="Moderate" />
                    <asp:listitem value="5" text="High Moderate" />
                    <asp:listitem value="6" text="High" />
                    <asp:listitem value="7" text="Very High" />
                    <asp:listitem value="8" text="Highest" />
                </asp:dropdownlist></span></div>
        <div runat="server" class="half" id="div_bankzipcode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_bankzipcode" text="Zip/Postal Code" /><span
                class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" onblur="zipLostFocus(this);"
                    id="bankzipcode" rmxref="Instance/Account/BankEntity/ZipCode" rmxtype="zip" tabindex="8" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABaccountfilter"
        id="FORMTABaccountfilter">
        <div runat="server" class="half" id="div_ownereid" xmlns="">
            <asp:label runat="server" class="label" id="lbl_ownereid" text="Owner" /><span class="formw"><asp:textbox
                runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                id="ownereid" rmxref="Instance/Account/OwnerEid" rmxtype="eidlookup" cancelledvalue=""
                tabindex="14" /><input type="button" class="button" value="..." name="ownereidbtn"
                    tabindex="15" onclick="lookupData('ownereid','ORGANIZATIONS',4,'ownereid',2)" /><asp:textbox
                        style="display: none" runat="server" id="ownereid_cid" rmxref="Instance/Account/OwnerEid/@codeid"
                        cancelledvalue="" /></span></div>
        <div runat="server" class="full" id="div_LOB" xmlns="">
            <span class="label">
                <asp:label runat="server" id="LOB" rmxref="Instance/UI/MissingRef" rmxtype="labelonly"
                    text="Line Of Business " /></span></div>
        <div runat="server" class="full" id="div_OWNER_LOB1" xmlns="">
            <span class="formw">
                <asp:radiobutton runat="server" id="OWNER_LOB1" rmxref="Instance/Account/OwnerLob"
                    rmxtype="radio" onclientclick="setDataChanged(true);" text="All" checked="" value="0"
                    groupname="OWNER_LOB" /></span></div>
        <span xmlns="">&#160;</span><div runat="server" class="full" id="div_OWNER_LOB2"
            xmlns="">
            <span class="formw">
                <asp:radiobutton runat="server" id="OWNER_LOB2" rmxref="Instance/Account/OwnerLob"
                    rmxtype="radio" onclientclick="setDataChanged(true);" text="General Claim" value="241"
                    groupname="OWNER_LOB" /></span></div>
        <span xmlns="">&#160;</span><div runat="server" class="full" id="div_OWNER_LOB3"
            xmlns="">
            <span class="formw">
                <asp:radiobutton runat="server" id="OWNER_LOB3" rmxref="Instance/Account/OwnerLob"
                    rmxtype="radio" onclientclick="setDataChanged(true);" text="Vehicle Accident"
                    value="242" groupname="OWNER_LOB" /></span></div>
        <span xmlns="">&#160;</span><div runat="server" class="full" id="div_OWNER_LOB4"
            xmlns="">
            <span class="formw">
                <asp:radiobutton runat="server" id="OWNER_LOB4" rmxref="Instance/Account/OwnerLob"
                    rmxtype="radio" onclientclick="setDataChanged(true);" text="Worker's Compensation"
                    value="243" groupname="OWNER_LOB" /></span></div>
        <span xmlns="">&#160;</span><div runat="server" class="full" id="div_OWNER_LOB5"
            xmlns="">
            <span class="formw">
                <asp:radiobutton runat="server" id="OWNER_LOB5" rmxref="Instance/Account/OwnerLob"
                    rmxtype="radio" onclientclick="setDataChanged(true);" text="Short Term Disability"
                    value="844" groupname="OWNER_LOB" /></span></div>
        <span xmlns="">&#160;</span><div runat="server" class="half" id="div_linebreak1"
            xmlns="">
            <asp:label runat="server" class="label" id="lbl_linebreak1" text="" /><span class="formw"><tr>
                <td>
                    <br />
                </td>
            </tr>
            </span>
        </div>
        <div runat="server" class="half" id="div_labelTrigger" xmlns="">
            <span class="label">
                <asp:label runat="server" id="labelTrigger" rmxref="Instance/UI/MissingRef" rmxtype="labelonly"
                    text="Effective Trigger and Dates " /></span></div>
        <div runat="server" class="half" id="div_triggerdate" xmlns="">
            <asp:label runat="server" class="label" id="lbl_triggerdate" text="Effective Date Trigger" /><span
                class="formw"><asp:dropdownlist runat="server" id="triggerdate" tabindex="19" rmxref="Instance/Account/EffTrigger"
                    rmxtype="combobox" onchange="setDataChanged(true);"><asp:listitem value="" text="none" />
                    <asp:listitem value="Current System Date" text="Current System Date" />
                    <asp:listitem value="Event Date" text="Event Date" />
                    <asp:listitem value="Claim Date" text="Claim Date" />
                </asp:dropdownlist></span></div>
        <div runat="server" class="half" id="div_startdate" xmlns="">
            <asp:label runat="server" class="label" id="lbl_startdate" text="Effective Start Date" /><span
                class="formw"><asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);"
                    id="startdate" rmxref="Instance/Account/TriggerFromDate" rmxtype="date" tabindex="20" /><cc1:CalendarExtender
                        runat="server" ID="startdate_ajax" TargetControlID="startdate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_enddate" xmlns="">
            <asp:label runat="server" class="label" id="lbl_enddate" text="Effective End Date" /><span
                class="formw"><asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);"
                    id="enddate" rmxref="Instance/Account/TriggerToDate" rmxtype="date" tabindex="22" /><cc1:CalendarExtender
                        runat="server" ID="enddate_ajax" TargetControlID="enddate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_linebreak2" xmlns="">
            <asp:label runat="server" class="label" id="lbl_linebreak2" text="" /><span class="formw"><tr>
                <td>
                    <br />
                </td>
            </tr>
            </span>
        </div>
        <div runat="server" class="half" id="div_servicecodelist" xmlns="">
            <asp:label runat="server" class="label" id="lbl_servicecodelist" text="Service Code(s)" /><span
                class="formw"><asp:listbox runat="server" size="3" id="servicecodelist" rmxref="/Instance/Account/ServiceCodes"
                    rmxtype="codelist" style="" tabindex="24" /><input type="button" id="servicecodelistbtn"
                        tabindex="25" class=" CodeLookupControl" onclick="selectCode('SERVICE_CODE','servicecodelist')" /><input
                            type="button" class="button" id="servicecodelistbtndel" tabindex="26" onclick="deleteSelCode('servicecodelist')"
                            value="-" /><asp:textbox runat="server" style="display: none" rmxref="/Instance/Account/ServiceCodes/@codeid"
                                id="servicecodelist_lst" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup">
        <asp:textbox style="display: none" runat="server" id="supp_account_id" rmxref="/Instance/*/Supplementals/ACCOUNT_ID"
            rmxtype="id" /></div>
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnDeposit">
            <asp:button class="button" runat="Server" id="btnDeposit" text="Deposit" rmxref="/Instance/Account/FundsDepositList/@committedcount"
                onclientclick="if(!( XFormHandler('SysFormName=deposit&SysEx=/Instance/Account/AccountId&SysCmd=1','','')))return false;"
                postbackurl="?SysFormName=deposit&SysEx=/Instance/Account/AccountId&SysCmd=1" /></div>
        <div class="formButton" runat="server" id="div_btnSubAccounts">
            <asp:button class="button" runat="Server" id="btnSubAccounts" text="Sub Accounts"
                rmxref="Instance/Account/BankAccSubList/@committedcount" onclientclick="if(!( XFormHandler('SysFormName=bankaccountsub&syscmd=1&SysFormIdName=polcvgrowid&SysEx=/Instance/Account/AccountId','','')))return false;"
                postbackurl="?SysFormName=bankaccountsub&syscmd=1&SysFormIdName=polcvgrowid&SysEx=/Instance/Account/AccountId" /></div>
        <div class="formButton" runat="server" id="div_btnCheckStocks">

            <script language="JavaScript" src="">{var i;}
            </script>

            <asp:button class="button" runat="server" id="btnCheckStocks" rmxref="" text="Check Stocks"
                onclientclick="GoToCheckStocks();" /></div>
        <div class="formButton" runat="server" id="div_btnBalance">

            <script language="JavaScript" src="">{var i;}
            </script>

            <asp:button class="button" runat="server" id="btnBalance" rmxref="" text="Balance"
                onclientclick="AccountBalanceReconciliation();" /></div>
        <div class="formButton" runat="server" id="div_btnAccountBalance">

            <script language="JavaScript" src="">{var i;}
            </script>

            <asp:button class="button" runat="server" id="btnAccountBalance" rmxref="" text="Account Balance"
                onclientclick="LoadAccountBalance();" /></div>
        <div class="formButton" runat="server" id="div_btnAccBankReports">

            <script language="JavaScript" src="">{var i;}
            </script>

            <asp:button class="button" runat="server" id="btnAccBankReports" rmxref="" text="Reports"
                onclientclick="BankAccountReports();" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="Account" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" text="&lt;Account&gt;&lt;BankEntity&gt;&lt;/BankEntity&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/Account&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormPIdName"
                                    rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden" text="" /><asp:textbox
                                        style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
                                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysPSid"
                                            rmxref="Instance/UI/FormVariables/SysPSid" rmxtype="hidden" text="" /><asp:textbox
                                                style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysNotReqNew"
                                                    rmxref="Instance/UI/FormVariables/SysNotReqNew" rmxtype="hidden" text="" /><asp:textbox
                                                        style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                                        rmxtype="hidden" text="bankaccount" /><asp:textbox style="display: none" runat="server"
                                                            id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName" rmxtype="hidden"
                                                            text="accountid" /><asp:textbox style="display: none" runat="server" id="SysFormId"
                                                                rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden" text="" /><asp:textbox
                                                                    style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                                                    rmxtype="hidden" text="9800" /><asp:textbox style="display: none" runat="server"
                                                                        id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType" rmxtype="hidden"
                                                                        text="" /><asp:textbox style="display: none" runat="server" name="formname" value="bankaccount" /><asp:textbox
                                                                            style="display: none" runat="server" name="SysRequired" value="banklastname|accountnumber|accountname|nextchecknumber|" /><asp:textbox
                                                                                style="display: none" runat="server" name="SysFocusFields" value="banklastname|ownereid|" /><input
                                                                                    type="hidden" id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible"
                                                                                        style="display: none" /><asp:textbox runat="server" id="SysLookupClass" style="display: none" /><asp:textbox
                                                                                            runat="server" id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server"
                                                                                                id="SysLookupAttachNodePath" style="display: none" /><asp:textbox runat="server"
                                                                                                    id="SysLookupResultConfig" style="display: none" /><input type="hidden" value="rmx-widget-handle-2"
                                                                                                        id="SysWindowId" /></form>
</body>
</html>
