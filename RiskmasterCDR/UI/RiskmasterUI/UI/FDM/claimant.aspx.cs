﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.Common;

namespace Riskmaster.UI.FDM
{
    public partial class Claimant : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
        }

        /// <summary>
        /// Added by Amitosh for mits 23186(06/03/2011)
        /// </summary>
        public override void ModifyControls()
        {
            base.ModifyControls();

        //rkaur27(RMA-4039) : Code handled properly in BusinessAdaptor Layer
        //    string sbirthDate = null;
        //    if (this.Form.FindControl("clmntbirthdate") != null && this.Form.FindControl("clmntage") != null)
        //    {
        //        TextBox txtAge = (TextBox)this.Form.FindControl("clmntage");


        //        TextBox txtBirthDate = (TextBox)this.Form.FindControl("clmntbirthdate");
        //        if (txtBirthDate != null)
        //            sbirthDate = txtBirthDate.Text;

        //        if (!string.IsNullOrEmpty(sbirthDate))
        //        {
        //            //Code change done by Kuladeep for MITS:35380 Start
        //            //DateTime objDate = Conversion.ToDate(sbirthDate);
        //            DateTime objDate = Convert.ToDateTime(AppHelper.GetDateInenUS(sbirthDate));
        //            //Code change done by Kuladeep for MITS:35380 End

        //            int iDateOfBirth = Conversion.ConvertStrToInteger(objDate.Day.ToString());
        //            int iMonthOfBirth = Conversion.ConvertStrToInteger(objDate.Month.ToString());
        //            int iYearOfBirth = Conversion.ConvertStrToInteger(objDate.Year.ToString());

        //            //igupta3 Mits#34583 starts
        //            int iDate = 0;
        //            int iMonth = 0;
        //            int iYear = 0;
        //            DateTime evntDate;
        //            TextBox txtEventDate = (TextBox)this.Form.FindControl("ev_dateofevent");
        //            if (txtEventDate != null)
        //            {
        //                evntDate = Conversion.ToDate(txtEventDate.Text);
        //                iDate = Conversion.ConvertStrToInteger(evntDate.Day.ToString());
        //                iMonth = Conversion.ConvertStrToInteger(evntDate.Month.ToString());
        //                iYear = Conversion.ConvertStrToInteger(evntDate.Year.ToString());
        //            } //igupta3 Mits#34583 ends
        //            else
        //            {
        //                DateTime sysDateTime = DateTime.Now;
        //                iDate = Conversion.ConvertStrToInteger(sysDateTime.Day.ToString());
        //                iMonth = Conversion.ConvertStrToInteger(sysDateTime.Month.ToString());
        //                iYear = Conversion.ConvertStrToInteger(sysDateTime.Year.ToString());
        //            }
        //            int iYearsDiff = iYear - iYearOfBirth - 1;

        //            if (iMonth > iMonthOfBirth || (iMonth == iMonthOfBirth && iDate > (iDateOfBirth - 1)))
        //            {
        //                iYearsDiff++;
        //            }

        //            txtAge.Text = iYearsDiff.ToString();
        //        }
        //        else
        //        {
        //            txtAge.Text = string.Empty;
        //        }
        //    }



        }
    }
}
