<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="deposit.aspx.cs"  Inherits="Riskmaster.UI.FDM.Deposit" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head runat="server">
    <title>Bank Account Deposit</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <script language="JavaScript" src="../../Scripts/deposit.js">{var i;}</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();OnLoadDeposit();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_new" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateNew" src="../../Images/tb_new_active.png" width="28" height="28" border="0" id="new" AlternateText="New" onMouseOver="this.src='../../Images/tb_new_mo.png';" onMouseOut="this.src='../../Images/tb_new_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/tb_save_active.png" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/tb_save_mo.png';" onMouseOut="this.src='../../Images/tb_save_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateFirst" src="../../Images/tb_first_active.png" width="28" height="28" border="0" id="movefirst" AlternateText="Move First" onMouseOver="this.src='../../Images/tb_first_mo.png';" onMouseOut="this.src='../../Images/tb_first_active.png';" OnClientClick="" />
        </div>
        <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigatePrev" src="../../Images/tb_previous_active.png" width="28" height="28" border="0" id="moveprevious" AlternateText="Move Previous" onMouseOver="this.src='../../Images/tb_previous_mo.png';" onMouseOut="this.src='../../Images/tb_previous_active.png';" OnClientClick="" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateNext" src="../../Images/tb_next_active.png" width="28" height="28" border="0" id="movenext" AlternateText="Move Next" onMouseOver="this.src='../../Images/tb_next_mo.png';" onMouseOut="this.src='../../Images/tb_next_active.png';" OnClientClick="" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateLast" src="../../Images/tb_last_active.png" width="28" height="28" border="0" id="movelast" AlternateText="Move Last" onMouseOver="this.src='../../Images/tb_last_mo.png';" onMouseOut="this.src='../../Images/tb_last_active.png';" OnClientClick="" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" OnClientClick="return DeleteRecord();" src="../../Images/tb_delete_active.png" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/tb_delete_mo.png';" onMouseOut="this.src='../../Images/tb_delete_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="if(!doLookup()) return false;" src="../../Images/tb_lookup_active.png" width="28" height="28" border="0" id="lookup" AlternateText="Lookup Data" onMouseOver="this.src='../../Images/tb_lookup_mo.png';" onMouseOut="this.src='../../Images/tb_lookup_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return Diary();" src="../../Images/tb_diary_active.png" width="28" height="28" border="0" id="diary" AlternateText="Diary" onMouseOver="this.src='../../Images/tb_diary_mo.png';" onMouseOut="this.src='../../Images/tb_diary_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return recordSummary();" src="../../Images/tb_recordsummary_active.png" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='../../Images/tb_recordsummary_mo.png';" onMouseOut="this.src='../../Images/tb_recordsummary_active.png';" />
        </div>
      </div>
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Bank Account Deposit" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSdepositinfo" id="TABSdepositinfo">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="depositinfo" id="LINKTABSdepositinfo">Deposit Information</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPdepositinfo">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABdepositinfo" id="FORMTABdepositinfo">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hddepositinfo" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="accountid" RMXRef="/Instance/FundsDeposit/BankAccId|/Instance/UI/FormVariables/SysExData/AccountId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="depositid" RMXRef="/Instance/FundsDeposit/DepositId" RMXType="id" />
              <div runat="server" class="half" id="div_ctlnumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ctlnumber" Text="Control Number" />
                <span class="formw">
                  <asp:TextBox runat="server" id="ctlnumber" RMXRef="/Instance/FundsDeposit/CtlNumber" RMXType="text" tabindex="1" maxlength="50" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clearedflag" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clearedflag" Text="Cleared" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="clearedflag" RMXRef="/Instance/FundsDeposit/ClearedFlag" RMXType="checkbox" onclick="ClearDeposit();" tabindex="11" Height="24px" />
                </span>
              </div>
              <div runat="server" class="half" id="div_voidflag" xmlns="">
                <asp:label runat="server" class="label" id="lbl_voidflag" Text="Void" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="voidflag" RMXRef="/Instance/FundsDeposit/VoidFlag" RMXType="checkbox" onclick="VoidDeposit();" tabindex="12" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_deposittype" xmlns="">
                <asp:label runat="server" class="required" id="lbl_deposittype" Text="Transaction Type" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="deposittype" tabindex="2" RMXRef="/Instance/FundsDeposit/DepositType" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/deposittype" onchange="setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_voidcleardate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_voidcleardate" Text="On" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="voidcleardate" RMXRef="/Instance/FundsDeposit/VoidclearDate" RMXType="date" tabindex="13" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="voidcleardatebtn" tabindex="14" />
                  <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "voidcleardate",
					ifFormat : "%m/%d/%Y",
					button : "voidcleardatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_subbankaccount" xmlns="">
                <asp:label runat="server" class="required" id="lbl_subbankaccount" Text="" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="subbankaccount" isblank="true" tabindex="3" RMXRef="/Instance/FundsDeposit/SubAccId" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/AccountList" width="205" onchange="setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_btnClearMultDeposits" xmlns="">
                <script language="JavaScript" src="">{var i;}
        </script>
                <span class="formw">
                  <asp:button class="button" runat="server" id="btnClearMultDeposits" RMXRef="" RMXType="buttonscript" Text="Clear Multiple Deposits" onClientClick="return GetDeposits();" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_transdate" xmlns="">
                <asp:label runat="server" class="required" id="lbl_transdate" Text="Transaction Date" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="transdate" RMXRef="/Instance/FundsDeposit/TransDate" RMXType="date" tabindex="4" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="transdatebtn" tabindex="5" />
                  <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "transdate",
					ifFormat : "%m/%d/%Y",
					button : "transdatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_amount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_amount" Text="Amount" />
                <span class="formw">
                  <asp:TextBox runat="server" id="amount" RMXRef="/Instance/FundsDeposit/Amount" RMXType="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="6" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_description" xmlns="">
                <asp:label runat="server" class="label" id="lbl_description" Text="Description" />
                <span class="formw">
                  <asp:TextBox runat="Server" id="description" RMXRef="/Instance/FundsDeposit/Description" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="7" TextMode="MultiLine" Columns="30" rows="5" />
                  <asp:button runat="server" class="MemoButton" name="descriptionbtnMemo" id="descriptionbtnMemo" onclientclick="return EditMemo('description','');" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_adjustcode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_adjustcode" Text="Adjustment Type" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="adjustcode" CodeTable="ADJUSTMENT_TYPE" ControlName="adjustcode" RMXRef="/Instance/FundsDeposit/AdjustCode" RMXType="code" tabindex="9" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="hdDttmRcdAdded" RMXRef="/Instance/FundsDeposit/DttmRcdAdded" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="hdDttmRcdLastUpd" RMXRef="/Instance/FundsDeposit/DttmRcdLastUpd" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="hdupdatedbyuser" RMXRef="/Instance/FundsDeposit/UpdatedByUser" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="hdaddedbyuser" RMXRef="/Instance/FundsDeposit/AddedByUser" RMXType="id" />
            </td>
          </tr>
        </table>
      </div>
      <div class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
          <asp:Button class="button" runat="Server" id="BackToParent" Text="Back to Bank Account" OnClientClick="if(!( XFormHandler('','1','back')))return false;" PostBackUrl="?" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="FundsDeposit" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;FundsDeposit&gt;&lt;/FundsDeposit&gt;" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="bankaccount" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="accountid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="deposit" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="depositid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="10650" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="TabNameList" RMXRef="" RMXType="hidden" Text="TABSdepositinfo" />
      <asp:TextBox style="display:none" runat="server" id="GroupAssocFieldList" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="deposit" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="deposittype|subbankaccount|transdate|" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="ctlnumber|" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>