<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="policyinsurer.aspx.cs"
    Inherits="Riskmaster.UI.FDM.Policyinsurer" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Policy Insurer</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift">
                <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28"
                                height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                                src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                                validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28"
                                height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28"
                                height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28"
                                height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28"
                                height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                                width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="doLookup()" src="Images/lookup.gif"
                                width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='Images/lookup2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/lookup.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                                width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                                onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" />
                        </td>
                    </tr>
                </table>
            </div>
    <br />
    <div class="msgheader" id="formtitle">
        Policy Insurer</div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0">
                    <tr>
                        <td class="Selected" nowrap="true" name="TABSinsurerinfo" id="TABSinsurerinfo">
                            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="insurerinfo"
                                id="LINKTABSinsurerinfo"><span style="text-decoration: none">Insurer Information</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td valign="top" nowrap="true" />
                    </tr>
                </table>
                <div style="position: relative; left: 0; top: 0; width: 870px; height: 350px; overflow: auto"
                    class="singletopborder">
                    <table border="0" cellspacing="0" celpadding="0" width="95%" height="95%">
                        <tr>
                            <td valign="top">
                                <table border="0" name="FORMTABinsurerinfo" id="FORMTABinsurerinfo">
                                    <tr>
                                        <asp:textbox style="display: none" runat="server" id="policyid" rmxref="/Instance/PolicyXInsurer/PolicyId" /><asp:textbox
                                            style="display: none" runat="server" id="inrowid" rmxref="/Instance/PolicyXInsurer/InRowID" /><asp:textbox
                                                style="display: none" runat="server" id="insentityid" rmxref="/Instance/PolicyXInsurer/InsurerCode" /><td
                                                    nowrap="true" id="inslastname_ctlcol" xmlns="">
                                                    <b><u>Insurer</u></b>
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" tabindex="0"
                                                onblur="lookupLostFocus(this);" rmxref="/Instance/PolicyXInsurer/InsurerEntity/LastName"
                                                id="inslastname" /><asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required"
                                                    runat="server" controltovalidate="inslastname" /><input type="button" class="button"
                                                        tabindex="1" id="inslastnamebtn" onclick="lookupDataFDM('inslastname','INSURERS',4,'ins',1,'/Instance/PolicyXInsurer/InsurerEntity')"
                                                        value="..." />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="insphone1_ctlcol" xmlns="">
                                            Office Phone
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="insphone1" rmxref="/Instance/PolicyXInsurer/InsurerEntity/Phone1"
                                                tabindex="12" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="inscontact_ctlcol" xmlns="">
                                            Contact
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="inscontact"
                                                rmxref="/Instance/PolicyXInsurer/InsurerEntity/Contact" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="insphone2_ctlcol" xmlns="">
                                            Alt. Phone
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="insphone2" rmxref="/Instance/PolicyXInsurer/InsurerEntity/Phone2"
                                                tabindex="13" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="insaddr1_ctlcol" xmlns="">
                                            Address 1
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="insaddr1"
                                                rmxref="/Instance/PolicyXInsurer/InsurerEntity/Addr1" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="insfaxnumber_ctlcol" xmlns="">
                                            Fax
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="insfaxnumber" rmxref="/Instance/PolicyXInsurer/InsurerEntity/FaxNumber"
                                                tabindex="14" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="insaddr2_ctlcol" xmlns="">
                                            Address 2
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="insaddr2"
                                                rmxref="/Instance/PolicyXInsurer/InsurerEntity/Addr2" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="primaryinsurer_ctlcol" xmlns="">
                                            Primary Insurer
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="primaryinsurer"
                                                rmxref="/Instance/PolicyXInsurer/PrimaryInsurer" tabindex="15" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="inscity_ctlcol" xmlns="">
                                            City
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="inscity"
                                                rmxref="/Instance/PolicyXInsurer/InsurerEntity/City" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="insstateid_ctlcol" xmlns="">
                                            State
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="insstateid"
                                                rmxref="/Instance/PolicyXInsurer/InsurerEntity/StateId" cancelledvalue="" tabindex="6"
                                                onblur="codeLostFocus(this.id);" /><input type="button" class="CodeLookupControl"
                                                    name="insstateidbtn" onclick="selectCode('states','insstateid')" /><asp:textbox style="display: none"
                                                        runat="server" id="insstateid_cid" rmxref="/Instance/PolicyXInsurer/InsurerEntity/StateId/@codeid"
                                                        cancelledvalue="" tabindex="7" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="inszipcode_ctlcol" xmlns="">
                                            Zip
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);"
                                                id="inszipcode" rmxref="/Instance/PolicyXInsurer/InsurerEntity/ZipCode" tabindex="8" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="inscountrycode_ctlcol" xmlns="">
                                            Country
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="inscountrycode"
                                                rmxref="/Instance/PolicyXInsurer/InsurerEntity/CountryCode" cancelledvalue=""
                                                tabindex="9" onblur="codeLostFocus(this.id);" /><input type="button" class="CodeLookupControl"
                                                    name="inscountrycodebtn" onclick="selectCode('COUNTRY','inscountrycode')" /><asp:textbox
                                                        style="display: none" runat="server" id="inscountrycode_cid" rmxref="/Instance/PolicyXInsurer/InsurerEntity/CountryCode/@codeid"
                                                        cancelledvalue="" tabindex="10" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="respercentage_ctlcol" xmlns="">
                                            Responsible Percentage
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onblur="numLostFocus(this);" id="respercentage"
                                                rmxref="/Instance/PolicyXInsurer/ResPercentage" tabindex="11" min="0" onchange="setDataChanged(true);" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" />
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td>
                            <asp:button id="Button1" class="button" runat="Server" text="Back to Policy" onclientclick="&#xA;            XFormHandler('','SysCmd=0','1','back')&#xA;          " />
                        </td>
                    </tr>
                </table>
                <asp:textbox style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
                    text="policy" /><asp:textbox style="display: none" runat="server" id="SysFormPIdName"
                        rmxref="Instance/UI/FormVariables/SysFormPIdName" text="policyid" /><asp:textbox
                            style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
                            text="" /><asp:textbox style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
                                text="" /><asp:textbox style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                    text="" /><asp:textbox style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                        text="policyinsurer" /><asp:textbox style="display: none" runat="server" id="SysFormIdName"
                                            rmxref="Instance/UI/FormVariables/SysFormIdName" text="inrowid" /><asp:textbox style="display: none"
                                                runat="server" id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId" text="" /><asp:textbox
                                                    style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                                    text="9000" /><asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
                                                        text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
                                                            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" text="" /><asp:textbox style="display: none"
                                                                runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                                                                text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText" rmxref="Instance/UI/FormVariables/SysCmdText"
                                                                    text="Navigate" /><asp:textbox style="display: none" runat="server" id="SysClassName"
                                                                        rmxref="Instance/UI/FormVariables/SysClassName" text="PolicyXInsurer" /><asp:textbox
                                                                            style="display: none" runat="server" id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                                                                            name="SysSerializationConfig" text="&lt;PolicyXInsurer&gt;&lt;InsurerEntity&gt;&lt;/InsurerEntity&gt;&lt;/PolicyXInsurer&gt;" /><asp:textbox
                                                                                style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                                                                text="" /><asp:textbox style="display: none" runat="server" name="formname" value="policyinsurer" /><asp:textbox
                                                                                    style="display: none" runat="server" name="SysRequired" value="inslastname|" /><asp:textbox
                                                                                        style="display: none" runat="server" name="SysFocusFields" value="inslastname|" /><input
                                                                                            type="hidden" id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible"
                                                                                                style="display: none" /><asp:textbox runat="server" id="SysLookupClass" style="display: none" /><asp:textbox
                                                                                                    runat="server" id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server"
                                                                                                        id="SysLookupAttachNodePath" style="display: none" /><asp:textbox runat="server"
                                                                                                            id="SysLookupResultConfig" style="display: none" /><input type="hidden" value="rmx-widget-handle-2"
                                                                                                                id="SysWindowId" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
