<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="claimdi.aspx.cs"  Inherits="Riskmaster.UI.FDM.Claimdi" ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Non Occupational Claim</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_new" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateNew" src="../../Images/new.gif" width="28" height="28" border="0" id="new" AlternateText="New" onMouseOver="this.src='../../Images/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/new.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/save.gif" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/save.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateFirst" src="../../Images/first.gif" width="28" height="28" border="0" id="movefirst" AlternateText="Move First" onMouseOver="this.src='../../Images/first2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/first.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigatePrev" src="../../Images/prev.gif" width="28" height="28" border="0" id="moveprevious" AlternateText="Move Previous" onMouseOver="this.src='../../Images/prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/prev.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movenext" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateNext" src="../../Images/next.gif" width="28" height="28" border="0" id="movenext" AlternateText="Move Next" onMouseOver="this.src='../../Images/next2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/next.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movelast" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateLast" src="../../Images/last.gif" width="28" height="28" border="0" id="movelast" AlternateText="Move Last" onMouseOver="this.src='../../Images/last2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/last.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" src="../../Images/delete.gif" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/delete2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/delete.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_esumm" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return ShowExecSummary();" src="../../Images/esumm.gif" width="28" height="28" border="0" id="esumm" AlternateText="Executive Summary" onMouseOver="this.src='../../Images/esumm2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/esumm.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_attach" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return attach();" src="../../Images/attach.gif" width="28" height="28" border="0" id="attach" AlternateText="Attach Documents" onMouseOver="this.src='../../Images/attach2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/attach.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_search" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return doSearch();" src="../../Images/search.gif" width="28" height="28" border="0" id="search" AlternateText="Search" onMouseOver="this.src='../../Images/search2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/search.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_lookup" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="if(!doLookup()) return false;" src="../../Images/lookup.gif" width="28" height="28" border="0" id="lookup" AlternateText="Lookup Data" onMouseOver="this.src='../../Images/lookup2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/lookup.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return FilteredDiary();" src="../../Images/filtereddiary.gif" width="28" height="28" border="0" id="filtereddiary" AlternateText="View Record Diaries" onMouseOver="this.src='../../Images/filtereddiary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/filtereddiary.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_diary" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return Diary();" src="../../Images/diary.gif" width="28" height="28" border="0" id="diary" AlternateText="Diary" onMouseOver="this.src='../../Images/diary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/diary.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_comments" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return Comments();" src="../../Images/comments.gif" width="28" height="28" border="0" id="comments" AlternateText="Comments" onMouseOver="this.src='../../Images/comments2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/comments.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_enhancednotes" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return EnhancedNotes();" src="../../Images/progressnotes.gif" width="28" height="28" border="0" id="enhancednotes" AlternateText="Enhanced Notes" onMouseOver="this.src='../../Images/progressnotes2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/progressnotes.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_mailmerge" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return MailMerge();" src="../../Images/mailmerge.gif" width="28" height="28" border="0" id="mailmerge" AlternateText="Mail Merge" onMouseOver="this.src='../../Images/mailmerge2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/mailmerge.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return recordSummary();" src="../../Images/recordsummary.gif" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_commentsummary" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return CommentSummary();" src="../../Images/commentsummary.gif" width="28" height="28" border="0" id="commentsummary" AlternateText="Claim Comment Summary" onMouseOver="this.src='../../Images/commentsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/commentsummary.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_eventexplorer" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return EventExplorer();" src="../../Images/eventexplorer.gif" width="28" height="28" border="0" id="eventexplorer" AlternateText="Quick Summary" onMouseOver="this.src='../../Images/eventexplorer2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/eventexplorer.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_sendmail" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return SendMail();" src="../../Images/sendregarding.gif" width="28" height="28" border="0" id="sendmail" AlternateText="Send Mail" onMouseOver="this.src='../../Images/sendregarding2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/sendregarding.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_deletealldiaries" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return DeleteAllDiaries();" src="../../Images/diary_deleteall.gif" width="28" height="28" border="0" id="deletealldiaries" AlternateText="Delete All Diaries" onMouseOver="this.src='../../Images/diary_deleteall2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/diary_deleteall.gif';this.style.zoom='100%'" />
        </div>
      </div>
      <br />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Non Occupational Claim" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSclaiminfo" id="TABSclaiminfo">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="claiminfo" id="LINKTABSclaiminfo">Claim Info</a>
        </div>
        <div id="Div3" class="tabSpace" runat="server">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSeventdetail" id="TABSeventdetail">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="eventdetail" id="LINKTABSeventdetail">Event Info</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSemployeeinfo" id="TABSemployeeinfo">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="employeeinfo" id="LINKTABSemployeeinfo">Employee Info</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSemploymentinfo" id="TABSemploymentinfo">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="employmentinfo" id="LINKTABSemploymentinfo">Employment Info</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSmedicalinfo" id="TABSmedicalinfo">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="medicalinfo" id="LINKTABSmedicalinfo">Medical Info</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABScasemgt" id="TABScasemgt">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="casemgt" id="LINKTABScasemgt">Case Mgt/RTW</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" style="height:310px;overflow:auto" name="FORMTABclaiminfo" id="FORMTABclaiminfo">
        <asp:TextBox style="display:none" runat="server" id="eventid" RMXRef="/Instance/Claim/EventId|/Instance/UI/FormVariables/SysExData/EventId|/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventId" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="pieid" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEid/@codeid" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="primaryClaimant" RMXRef="/Instance/UI/FormVariables/SysExData/DiaryMessage" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="/Instance/Claim/ClaimId" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_ev_eventnumber" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_ev_eventnumber" Text="Event Number" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" id="ev_eventnumber" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventNumber" RMXType="eventlookup" tabindex="1" cancelledvalue="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_dateofevent" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_ev_dateofevent" Text="Date Of Event" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="ev_dateofevent" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DateOfEvent" RMXType="date" tabindex="21" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="ev_dateofeventbtn" tabindex="22" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "ev_dateofevent",
					ifFormat : "%m/%d/%Y",
					button : "ev_dateofeventbtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_AllCodes" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="AllCodes" RMXRef="Instance/UI/FormVariables/SysExData/AllCodes" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_labelonly1" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="labelonly1" Text="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_timeofevent" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_ev_timeofevent" Text="Time Of Event" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="time" onchange="setDataChanged(true);" id="ev_timeofevent" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/TimeOfEvent" RMXType="time" tabindex="23" />
            <cc1:MaskedEditExtender runat="server" Enabled="True" MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="ev_timeofevent" id="ev_timeofevent_ajax" />
          </span>
        </div>
        <div runat="server" class="half" id="div_claimnumber" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_claimnumber" Text="Claim Number" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="claimnumber" RMXRef="/Instance/Claim/ClaimNumber" RMXType="text" tabindex="3" />
          </span>
        </div>
        <div runat="server" class="half" id="div_dateofclaim" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_dateofclaim" Text="Date Of Claim" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="dateofclaim" RMXRef="/Instance/Claim/DateOfClaim" RMXType="date" tabindex="24" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="dateofclaimbtn" tabindex="25" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "dateofclaim",
					ifFormat : "%m/%d/%Y",
					button : "dateofclaimbtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_labelonly2" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="labelonly2" Text="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_timeofclaim" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_timeofclaim" Text="Time Of Claim" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="time" onchange="setDataChanged(true);" id="timeofclaim" RMXRef="/Instance/Claim/TimeOfClaim" RMXType="time" tabindex="26" />
            <cc1:MaskedEditExtender runat="server" Enabled="True" MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="timeofclaim" id="timeofclaim_ajax" />
          </span>
        </div>
        <div runat="server" class="half" id="div_claimtypecode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_claimtypecode" Text="Claim Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="claimtypecode" CodeTable="CLAIM_TYPE" ControlName="claimtypecode" RMXRef="/Instance/Claim/ClaimTypeCode" RMXType="code" tabindex="4" Required="true" ValidationGroup="vgSave" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_datereported" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_datereported" Text="Date Reported" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="ev_datereported" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DateReported" RMXType="date" tabindex="27" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="ev_datereportedbtn" tabindex="28" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "ev_datereported",
					ifFormat : "%m/%d/%Y",
					button : "ev_datereportedbtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_labelonly3" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="labelonly3" Text="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_timereported" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_timereported" Text="Time Reported" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="time" onchange="setDataChanged(true);" id="ev_timereported" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/TimeReported" RMXType="time" tabindex="29" />
            <cc1:MaskedEditExtender runat="server" Enabled="True" MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="ev_timereported" id="ev_timereported_ajax" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empemployeenumber" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_empemployeenumber" Text="Employee No." />
          <span class="formw">
            <asp:TextBox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" id="empemployeenumber" RMXRef="/Instance/Claim/PrimaryPiEmployee/EmployeeNumber" RMXType="employeelookup" name="empemployeenumber" cancelledvalue="" />
            <asp:TextBox style="display:none" runat="server" value="1" id="empemployeenumber_creatable" />
            <asp:button runat="server" class="button" Text="..." id="empemployeenumberbtn" onclientclick="lookupData('empemployeenumber','employees',3,'emp',4)" />
          </span>
        </div>
        <div runat="server" class="half" id="div_disfromdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_disfromdate" Text="Disability Start Date" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="disfromdate" RMXRef="/Instance/Claim/DisabilFromDate" RMXType="date" tabindex="30" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="disfromdatebtn" tabindex="31" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "disfromdate",
					ifFormat : "%m/%d/%Y",
					button : "disfromdatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_depteid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_ev_depteid" Text="Department" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="ev_depteid" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DeptEid" RMXType="orgh" name="ev_depteid" cancelledvalue="" tabindex="8" />
            <asp:button runat="server" class="CodeLookupControl" Text="..." id="ev_depteidbtn" onclientclick="return selectCode('orgh','ev_depteid','Department');" />
            <asp:button runat="server" class="button" Text="Instr" id="ev_depteidbtnInstructions" onclientclick="return ClaimInstructions();" />
            <asp:TextBox style="display:none" runat="server" id="ev_depteid_cid" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DeptEid/@codeid" cancelledvalue="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_distodate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_distodate" Text="Disability To Date" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="distodate" RMXRef="/Instance/Claim/DisabilToDate" RMXType="date" tabindex="32" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="distodatebtn" tabindex="33" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "distodate",
					ifFormat : "%m/%d/%Y",
					button : "distodatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_defaultdeptflag" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="defaultdeptflag" RMXRef="/Instance/UI/FormVariables/SysExData/DefaultDeptFlag" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_claimstatuscode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_claimstatuscode" Text="Claim Status" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="claimstatuscode" CodeTable="CLAIM_STATUS" ControlName="claimstatuscode" RMXRef="/Instance/Claim/ClaimStatusCode" RMXType="codewithdetail" tabindex="11" Required="true" ValidationGroup="vgSave" />
            <asp:button class="button" runat="Server" id="claimstatuscodedetailbtn" onclientclick="return selectCodeWithDetail('claimstatuscode_codelookup',1);" tabindex="13" Text="Detail" />
          </span>
        </div>
        <div runat="server" class="half" id="div_benefitdistypecode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_benefitdistypecode" Text="Benefit Disability Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="benefitdistypecode" CodeTable="BENEFIT_DIS_TYPE" ControlName="benefitdistypecode" RMXRef="/Instance/Claim/DisTypeCode" RMXType="code" tabindex="34" />
          </span>
        </div>
        <div runat="server" class="half" id="div_dttmclosed" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_dttmclosed" Text="Date Closed" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/Claim/DttmClosed" id="dttmclosed" tabindex="14" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_filenumber" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_filenumber" Text="File Number" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="filenumber" RMXRef="/Instance/Claim/FileNumber" RMXType="text" tabindex="36" />
          </span>
        </div>
        <div runat="server" class="half" id="div_methodclosedcode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_methodclosedcode" Text="Close Method" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="methodclosedcode" CodeTable="CLOSE_METHOD" ControlName="methodclosedcode" RMXRef="/Instance/Claim/MethodClosedCode" RMXType="code" tabindex="15" />
          </span>
        </div>
        <div runat="server" class="half" id="div_planname" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_planname" Text="Plan Name" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeydown="eatKeystrokes();" id="planname" RMXRef="/Instance/Claim/PlanId/@defaultdetail" RMXType="planlookup" Enabled="false" BackColor="Silver" />
            <input type="button" class="button" value="..." name="plannamebtn" onclientclick="lookupClaimPolicy('planname','LookUpClaimPlan')" />
            <asp:TextBox style="display:none" runat="server" id="planname_cid" RMXRef="/Instance/Claim/PlanId" />
            <asp:button runat="server" class="button" id="planname_open" value="Open" OnClientClick="if(!( XFormHandler('SysFormPIdName=claimid&SysFormName=plan&SysCmd=0&SysFormIdName=planid&SysFormId=%planname_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','1','planlookup')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=plan&SysCmd=0&SysFormIdName=planid&SysFormId=%planname_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber" Tabindex="39" />
          </span>
        </div>
        <div runat="server" class="half" id="div_currentadjuster" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_currentadjuster" Text="Current Adjuster" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/CurrentAdjuster" id="currentadjuster" tabindex="17" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_classname" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_classname" Text="Class Name" />
          <span class="formw">
            <asp:DropDownList runat="server" id="classname" tabindex="40" RMXRef="/Instance/UI/FormVariables/SysExData/ClassList/@codeid" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/ClassList" onchange="setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_jurisdiction" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_jurisdiction" Text="Jurisdiction" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="jurisdiction" CodeTable="states" ControlName="jurisdiction" RMXRef="/Instance/Claim/FilingStateId" RMXType="code" tabindex="18" Required="true" ValidationGroup="vgSave" />
          </span>
        </div>
        <div runat="server" class="half" id="div_servicecode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_servicecode" Text="Service Code" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="servicecode" CodeTable="SERVICE_CODE" ControlName="servicecode" RMXRef="/Instance/Claim/ServiceCode" RMXType="code" tabindex="41" />
          </span>
        </div>
        <div runat="server" class="half" id="div_leaveonlyflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_leaveonlyflag" Text="Leave Only" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="leaveonlyflag" RMXRef="/Instance/Claim/LeaveOnlyFlag" RMXType="checkbox" tabindex="20" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_paymntfrozenflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_paymntfrozenflag" Text="Payments Frozen" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="paymntfrozenflag" RMXRef="/Instance/Claim/PaymntFrozenFlag" RMXType="checkbox" tabindex="43" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_lssclaimind" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_lssclaimind" Text="LSS Claim" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="lssclaimind" RMXRef="/Instance/Claim/LssClaimInd" RMXType="checkbox" tabindex="200" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_statuschangeapprovedby" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="statuschangeapprovedby" RMXRef="/Instance/UI/FormVariables/SysExData/StatusApprovedBy" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_statuschangedate" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="statuschangedate" RMXRef="/Instance/UI/FormVariables/SysExData/StatusDateChg" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_statuschangereason" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="statuschangereason" RMXRef="/Instance/UI/FormVariables/SysExData/StatusReason" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_statusdiaryclose" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="statusdiaryclose" RMXRef="Instance/UI/FormVariables/SysExData/CloseDiary" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_containsopendiaries" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="containsopendiaries" RMXRef="Instance/UI/FormVariables/SysExData/ContainsOpenDiaries" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_deleteautocheck" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="deleteautocheck" RMXRef="Instance/UI/FormVariables/SysExData/DeleteAutoCheck" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_containsautochecks" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="containsautochecks" RMXRef="Instance/UI/FormVariables/SysExData/ContainsAutoChecks" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_initialclaimstatus" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="initialclaimstatus" RMXRef="Instance/UI/FormVariables/SysExData/ClaimStatusCode" RMXType="hidden" />
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABeventdetail" id="FORMTABeventdetail">
        <div runat="server" class="half" id="div_ev_onpremiseflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_onpremiseflag" Text="Event On Premise" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="ev_onpremiseflag" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/OnPremiseFlag" RMXType="checkbox" onclick="FillEventLocationDetails(this)" tabindex="43" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_countyofinjury" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_countyofinjury" Text="County of Injury" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="ev_countyofinjury" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CountyOfInjury" RMXType="text" tabindex="58" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_addr1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_addr1" Text="Location Address 1" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="ev_addr1" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/Addr1" RMXType="text" tabindex="44" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_countrycode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_countrycode" Text="Country" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="ev_countrycode" CodeTable="COUNTRY" ControlName="ev_countrycode" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CountryCode" RMXType="code" tabindex="59" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_addr2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_addr2" Text="Location Address 2" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="ev_addr2" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/Addr2" RMXType="text" tabindex="45" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_primaryloccode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_primaryloccode" Text="Primary Location" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="ev_primaryloccode" CodeTable="PRIMARY_LOCATION" ControlName="ev_primaryloccode" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/PrimaryLocCode" RMXType="code" tabindex="61" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_city" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_city" Text="City" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="ev_city" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/City" RMXType="text" tabindex="46" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_locationtypecode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_locationtypecode" Text="Location Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="ev_locationtypecode" CodeTable="LOCATION_TYPE" ControlName="ev_locationtypecode" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationTypeCode" RMXType="code" tabindex="63" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_stateid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_stateid" Text="State" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="ev_stateid" CodeTable="states" ControlName="ev_stateid" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/StateId" RMXType="code" tabindex="47" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_zipcode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_zipcode" Text="Zip" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="ev_zipcode" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/ZipCode" RMXType="zip" TabIndex="65" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_causecode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_causecode" Text="Cause Code" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="ev_causecode" CodeTable="CAUSE_CODE" ControlName="ev_causecode" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CauseCode" RMXType="code" tabindex="49" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_noofinjuries" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_noofinjuries" Text="Number of Injuries" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="ev_noofinjuries" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/NoOfInjuries" RMXType="numeric" tabindex="66" min="0" onChange="setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_eventdescription" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_eventdescription" Text="Event Description" />
          <span class="formw">
            <asp:TextBox runat="Server" id="ev_eventdescription" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventDescription" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="51" TextMode="MultiLine" Columns="30" rows="5" />
            <input type="button" class="button" value="..." name="ev_eventdescriptionbtnMemo" tabindex="52" id="ev_eventdescriptionbtnMemo" onclick=" EditHTMLMemo('ev_eventdescription','yes')" />
            <asp:TextBox style="display:none" runat="server" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventDescription_HTMLComments" id="ev_eventdescription_HTML" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_locationareadesc" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_locationareadesc" Text="Location Description" />
          <span class="formw">
            <asp:TextBox runat="Server" id="ev_locationareadesc" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationAreaDesc" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="67" TextMode="MultiLine" Columns="30" rows="5" />
            <input type="button" class="button" value="..." name="ev_locationareadescbtnMemo" tabindex="68" id="ev_locationareadescbtnMemo" onclick=" EditHTMLMemo('ev_locationareadesc','yes')" />
            <asp:TextBox style="display:none" runat="server" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationAreaDesc_HTMLComments" id="ev_locationareadesc_HTML" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_injfromdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_injfromdate" Text="Injury From" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="ev_injfromdate" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/InjuryFromDate" RMXType="date" tabindex="53" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="ev_injfromdatebtn" tabindex="54" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "ev_injfromdate",
					ifFormat : "%m/%d/%Y",
					button : "ev_injfromdatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_injtodate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_injtodate" Text="Injury To" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="ev_injtodate" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/InjuryToDate" RMXType="date" tabindex="69" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="ev_injtodatebtn" tabindex="70" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "ev_injtodate",
					ifFormat : "%m/%d/%Y",
					button : "ev_injtodatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_empworkdaystarttime" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empworkdaystarttime" Text="Time Workday Began" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="time" onchange="setDataChanged(true);" id="empworkdaystarttime" RMXRef="/Instance/Claim/PrimaryPiEmployee/WorkdayStartTime" RMXType="time" tabindex="55" />
            <cc1:MaskedEditExtender runat="server" Enabled="True" MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="empworkdaystarttime" id="empworkdaystarttime_ajax" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ev_nooffatalities" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ev_nooffatalities" Text="Number Of Fatalities" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="ev_nooffatalities" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/NoOfFatalities" RMXType="numeric" tabindex="71" min="0" onChange="setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empregularjobflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empregularjobflag" Text="Regular Job at Time of Event" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empregularjobflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/RegularJobFlag" RMXType="checkbox" tabindex="56" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emplostconsciousness" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emplostconsciousness" Text="Lost Consciousness" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="emplostconsciousness" RMXRef="/Instance/Claim/PrimaryPiEmployee/LostConscFlag" RMXType="checkbox" tabindex="72" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emposharecordable" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emposharecordable" Text="PI OSHA Recordable" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="emposharecordable" RMXRef="/Instance/Claim/PrimaryPiEmployee/OshaRecFlag" RMXType="checkbox" tabindex="57" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emposhaaccdesc" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emposhaaccdesc" Text="PI OSHA Accident Description" />
          <span class="formw">
            <asp:TextBox runat="Server" id="emposhaaccdesc" RMXRef="/Instance/Claim/PrimaryPiEmployee/OshaAccDesc" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="73" TextMode="MultiLine" Columns="30" rows="5" />
            <input type="button" class="button" value="..." name="emposhaaccdescbtnMemo" tabindex="74" id="emposhaaccdescbtnMemo" onclick="EditMemo('emposhaaccdesc','')" />
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABemployeeinfo" id="FORMTABemployeeinfo">
        <div runat="server" class="half" id="div_emplastname" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_emplastname" Text="Last Name" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="emplastname" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/LastName" RMXType="text" tabindex="75" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emptaxid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_emptaxid" Text="Soc.Sec No." />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="ssnLostFocus(this);" id="emptaxid" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/TaxId" RMXType="ssn" name="emptaxid" tabindex="94" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empfirstname" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empfirstname" Text="First Name" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empfirstname" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/FirstName" RMXType="text" tabindex="76" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empbirthdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empbirthdate" Text="Date of Birth" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="empbirthdate" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/BirthDate" RMXType="datebuttonscript" name="empbirthdate" />
            <script language="JavaScript" SRC="">{var i;}</script>
            <asp:button runat="server" type="button" class="button" id="empbirthdatebtn" Text="..." tabindex="96" onclientclick="calculateage(this);" />
            <cc1:CalendarExtender runat="server" id="empbirthdate_ajax" TargetControlID="empbirthdate" OnClientDateSelectionChanged="calculateage" />
          </span>
        </div>
        <div runat="server" class="half" id="div_entityage" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_entityage" Text="Age" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="Instance/UI/FormVariables/SysExData/EntityAge" id="entityage" tabindex="97" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empmiddlename" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empmiddlename" Text="Middle Name" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empmiddlename" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/MiddleName" RMXType="text" tabindex="78" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empabbreviation" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empabbreviation" Text="Initials" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empabbreviation" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/Abbreviation" RMXType="text" tabindex="98" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empphone1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empphone1" Text="Office Phone" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="empphone1" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/Phone1" RMXType="phone" tabindex="79" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empphone2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empphone2" Text="Home Phone" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="empphone2" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/Phone2" RMXType="phone" tabindex="99" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empalsoknownas" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empalsoknownas" Text="Also Known as" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empalsoknownas" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/AlsoKnownAs" RMXType="text" tabindex="80" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empaddr1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empaddr1" Text="Address 1" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empaddr1" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/Addr1" RMXType="text" tabindex="81" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empfaxnumber" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empfaxnumber" Text="Fax" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="empfaxnumber" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/FaxNumber" RMXType="phone" tabindex="100" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empaddr2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empaddr2" Text="Address 2" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empaddr2" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/Addr2" RMXType="text" tabindex="82" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empsexcode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empsexcode" Text="Sex" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="empsexcode" CodeTable="SEX_CODE" ControlName="empsexcode" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/SexCode" RMXType="code" tabindex="101" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empcity" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empcity" Text="City" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empcity" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/City" RMXType="text" tabindex="83" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emptitle" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emptitle" Text="Title" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="emptitle" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/Title" RMXType="text" tabindex="103" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empstateid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empstateid" Text="State" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="empstateid" CodeTable="states" ControlName="empstateid" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/StateId" RMXType="code" tabindex="84" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empzipcode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empzipcode" Text="Zip/Postal Code" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="empzipcode" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/ZipCode" RMXType="zip" TabIndex="104" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empcostcentercode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empcostcentercode" Text="Cost Center" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="empcostcentercode" CodeTable="COST_CENTER" ControlName="empcostcentercode" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/CostCenterCode" RMXType="code" tabindex="86" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empcounty" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empcounty" Text="County" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empcounty" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/County" RMXType="text" tabindex="88" />
          </span>
        </div>
        <div runat="server" class="half" id="div_claimantattorney" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_claimantattorney" Text="Attorney" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="claimantattorney" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantAttorney" RMXType="eidlookup" cancelledvalue="" tabindex="105" />
            <input type="button" class="button" value="..." name="claimantattorneybtn" tabindex="106" onclick="lookupData('claimantattorney','ATTORNEYS',4,'claimantattorney',2)" />
            <asp:TextBox style="display:none" runat="server" id="claimantattorney_cid" RMXref="/Instance/UI/FormVariables/SysExData/ClaimantAttorney/@codeid" cancelledvalue="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empcountrycode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empcountrycode" Text="Country" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="empcountrycode" CodeTable="COUNTRY" ControlName="empcountrycode" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/CountryCode" RMXType="code" tabindex="89" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="entity_id" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/EntityId" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="empentitydttmrcdadded" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/DttmRcdAdded" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="empentitydttmrcdupdated" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/DttmRcdLastUpd" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="empentityupdatedbyuser" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/UpdatedByUser" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="empentityaddedbyuser" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiEntity/AddedByUser" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_empcustomtaxper" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="empcustomtaxper" Text="Custom Tax Percentages" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empcustomfedtaxper" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empcustomfedtaxper" Text="Federal" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empcustomfedtaxper" RMXRef="/Instance/Claim/PrimaryPiEmployee/CustomFedTaxPer" RMXType="text" tabindex="92" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empcustommedtaxper" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empcustommedtaxper" Text="Medicare" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empcustommedtaxper" RMXRef="/Instance/Claim/PrimaryPiEmployee/CustomMedTaxPer" RMXType="text" tabindex="107" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empcustomsstaxper" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empcustomsstaxper" Text="SS" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empcustomsstaxper" RMXRef="/Instance/Claim/PrimaryPiEmployee/CustomSSTaxPer" RMXType="text" tabindex="93" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empcustomsttaxper" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empcustomsttaxper" Text="State" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empcustomsttaxper" RMXRef="/Instance/Claim/PrimaryPiEmployee/CustomSTTaxPer" RMXType="text" tabindex="109" />
          </span>
        </div>
        <div runat="server" class="half" id="div_linebreak1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_linebreak1" Text="" />
          <span class="formw">
            <tr>
              <td>
                <br />
              </td>
            </tr>
          </span>
        </div>
        <div runat="server" class="half" id="div_singlerow1" xmlns="" xmlns:cul="remove">
          <script language="JavaScript" src="">{var i;}
              </script>
          <span class="formw">
            <asp:button class="button" runat="server" id="employeeclaimhistory" RMXRef="" RMXType="buttonscript" Text="Employee Claim History" OnClientClick="displayclaimhistory('employee')" />
          </span>
          <span class="formw">
            <asp:button class="button" runat="Server" id="employeedependents" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiXDependentList/@count" RMXType="button" Text="Dependents" OnClientClick="if(!(XFormHandler('','SysFormPIdName=claimid&SysFormName=pidependent&SysCmd=1&SysFormIdName=empdeprowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/PrimaryPiEmployee/PiRowId | /Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventNumber | /Instance/Claim/PrimaryPiEmployee/PiEntity/LastName','','button')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=pidependent&SysCmd=1&SysFormIdName=empdeprowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/PrimaryPiEmployee/PiRowId | /Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventNumber | /Instance/Claim/PrimaryPiEmployee/PiEntity/LastName" />
          </span>
        </div>
        <div runat="server" class="half" id="div_linebreak2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_linebreak2" Text="" />
          <span class="formw">
            <tr>
              <td>
                <br />
              </td>
            </tr>
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABemploymentinfo" id="FORMTABemploymentinfo">
        <div runat="server" class="half" id="div_empdatehired" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empdatehired" Text="Date Hired" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="empdatehired" RMXRef="/Instance/Claim/PrimaryPiEmployee/DateHired" RMXType="date" tabindex="111" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="empdatehiredbtn" tabindex="112" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "empdatehired",
					ifFormat : "%m/%d/%Y",
					button : "empdatehiredbtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_empdateofdeath" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empdateofdeath" Text="Date of Death" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="empdateofdeath" RMXRef="/Instance/Claim/PrimaryPiEmployee/DateOfDeath" RMXType="date" tabindex="136" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="empdateofdeathbtn" tabindex="137" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "empdateofdeath",
					ifFormat : "%m/%d/%Y",
					button : "empdateofdeathbtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_emptermdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emptermdate" Text="Termination Date" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="emptermdate" RMXRef="/Instance/Claim/PrimaryPiEmployee/TermDate" RMXType="date" tabindex="113" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="emptermdatebtn" tabindex="114" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "emptermdate",
					ifFormat : "%m/%d/%Y",
					button : "emptermdatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_empmaritalstatcode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empmaritalstatcode" Text="Marital Status" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="empmaritalstatcode" CodeTable="MARITAL_STATUS" ControlName="empmaritalstatcode" RMXRef="/Instance/Claim/PrimaryPiEmployee/MaritalStatCode" RMXType="code" tabindex="138" />
          </span>
        </div>
        <div runat="server" class="half" id="div_dupeoverride" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="dupeoverride" RMXRef="/Instance/UI/FormVariables/SysExData/dupeoverride" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_NonOccClaimFutureDate" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="NonOccClaimFutureDate" RMXRef="/Instance/UI/FormVariables/SysExData/NonOccClaimFutureDate" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emppositioncode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emppositioncode" Text="Position Code" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="emppositioncode" CodeTable="POSITIONS" ControlName="emppositioncode" RMXRef="/Instance/Claim/PrimaryPiEmployee/PositionCode" RMXType="code" tabindex="115" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empworkpermitdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empworkpermitdate" Text="Work Permit Date" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="empworkpermitdate" RMXRef="/Instance/Claim/PrimaryPiEmployee/WorkPermitDate" RMXType="date" tabindex="140" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="empworkpermitdatebtn" tabindex="141" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "empworkpermitdate",
					ifFormat : "%m/%d/%Y",
					button : "empworkpermitdatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_empdeptassignedeid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_empdeptassignedeid" Text="Department" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="empdeptassignedeid" RMXRef="/Instance/Claim/PrimaryPiEmployee/DeptAssignedEid" RMXType="orgh" name="empdeptassignedeid" cancelledvalue="" tabindex="117" />
            <asp:button runat="server" class="CodeLookupControl" Text="..." id="empdeptassignedeidbtn" onclientclick="return selectCode('orgh','empdeptassignedeid','Department');" />
            <asp:TextBox style="display:none" runat="server" id="empdeptassignedeid_cid" RMXRef="/Instance/Claim/PrimaryPiEmployee/DeptAssignedEid/@codeid" cancelledvalue="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empworkpermitnumber" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empworkpermitnumber" Text="Work Permit #" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="empworkpermitnumber" RMXRef="/Instance/Claim/PrimaryPiEmployee/WorkPermitNumber" RMXType="text" tabindex="142" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empsupervisoreid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empsupervisoreid" Text="Supervisor" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="empsupervisoreid" RMXRef="/Instance/Claim/PrimaryPiEmployee/SupervisorEid" RMXType="eidlookup" cancelledvalue="" tabindex="119" />
            <input type="button" class="button" value="..." name="empsupervisoreidbtn" tabindex="120" onclick="lookupData('empsupervisoreid','EMPLOYEES',4,'empsupervisoreid',2)" />
            <asp:TextBox style="display:none" runat="server" id="empsupervisoreid_cid" RMXref="/Instance/Claim/PrimaryPiEmployee/SupervisorEid/@codeid" cancelledvalue="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empncciclasscode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empncciclasscode" Text="NCCI Class" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="empncciclasscode" CodeTable="NCCI_CLASS_CODE" ControlName="empncciclasscode" RMXRef="/Instance/Claim/PrimaryPiEmployee/NcciClassCode" RMXType="code" tabindex="143" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emppaytypecode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emppaytypecode" Text="Pay Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="emppaytypecode" CodeTable="PAY_TYPES" ControlName="emppaytypecode" RMXRef="/Instance/Claim/PrimaryPiEmployee/PayTypeCode" RMXType="code" tabindex="121" />
          </span>
        </div>
        <div runat="server" class="half" id="div_jobclassification" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_jobclassification" Text="Job Classification" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="jobclassification" CodeTable="JOB_CLASS" ControlName="jobclassification" RMXRef="/Instance/Claim/PrimaryPiEmployee/JobClassCode" RMXType="code" tabindex="145" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empwork" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="empwork" Text="Work Week" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empworksunflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empworksunflag" Text="Sunday" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empworksunflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/WorkSunFlag" RMXType="checkbox" tabindex="124" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empworkthuflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empworkthuflag" Text="Thursday" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empworkthuflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/WorkThuFlag" RMXType="checkbox" tabindex="128" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empworkmonflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empworkmonflag" Text="Monday" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empworkmonflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/WorkMonFlag" RMXType="checkbox" tabindex="125" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empworkfriflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empworkfriflag" Text="Friday" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empworkfriflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/WorkFriFlag" RMXType="checkbox" tabindex="129" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empworktueflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empworktueflag" Text="Tuesday" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empworktueflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/WorkTueFlag" RMXType="checkbox" tabindex="126" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empworksatflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empworksatflag" Text="Saturday" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empworksatflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/WorkSatFlag" RMXType="checkbox" tabindex="130" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empworkwedflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empworkwedflag" Text="Wednesday" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empworkwedflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/WorkWedFlag" RMXType="checkbox" tabindex="127" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emppayamount" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emppayamount" Text="Pay Amount" />
          <span class="formw">
            <asp:TextBox runat="server" id="emppayamount" RMXRef="/Instance/Claim/PrimaryPiEmployee/PayAmount" RMXType="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="131" onchange="javascript: return calculateMonthlyRate('claimdi');;setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empactiveflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empactiveflag" Text="Active" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empactiveflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/ActiveFlag" RMXType="checkbox" tabindex="147" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emphourlyrate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emphourlyrate" Text="Hourly Rate" />
          <span class="formw">
            <asp:TextBox runat="server" id="emphourlyrate" RMXRef="/Instance/Claim/PrimaryPiEmployee/HourlyRate" RMXType="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency_three" tabindex="132" onchange="javascript: return calculateWeeklyRate('claimdi');;setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empfulltimeflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empfulltimeflag" Text="Full Time Employee" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empfulltimeflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/FullTimeFlag" RMXType="checkbox" tabindex="148" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empweeklyhours" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empweeklyhours" Text="Hours Per Week" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="empweeklyhours" RMXRef="/Instance/Claim/PrimaryPiEmployee/WeeklyHours" RMXType="numeric" tabindex="133" onChange="javascript: return calculateWeeklyRate('claimdi');" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empexemptstatusflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empexemptstatusflag" Text="Exempt" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="empexemptstatusflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/ExemptStatusFlag" RMXType="checkbox" tabindex="149" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empweeklyrate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empweeklyrate" Text="Weekly Rate" />
          <span class="formw">
            <asp:TextBox runat="server" id="empweeklyrate" RMXRef="/Instance/Claim/PrimaryPiEmployee/WeeklyRate" RMXType="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="134" onchange=";setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emphiredinsteflag" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emphiredinsteflag" Text="Hired In Jurisdiction" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="emphiredinsteflag" RMXRef="/Instance/Claim/PrimaryPiEmployee/HiredInSteFlag" RMXType="checkbox" tabindex="150" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empmonthlyrate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empmonthlyrate" Text="Monthly Rate" />
          <span class="formw">
            <asp:TextBox runat="server" id="empmonthlyrate" RMXRef="/Instance/Claim/PrimaryPiEmployee/MonthlyRate" RMXType="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="135" onchange=";setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_empdisabilityoption" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empdisabilityoption" Text="Disability Option" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="empdisabilityoption" CodeTable="DIS_OPTION_CODE" ControlName="empdisabilityoption" RMXRef="/Instance/Claim/PrimaryPiEmployee/DisabilityOption" RMXType="code" tabindex="151" />
          </span>
        </div>
        <div runat="server" class="half" id="div_weekspermonth" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="weekspermonth" RMXRef="Instance/UI/FormVariables/SysExData/weekspermonth" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_singlerow4" xmlns="" xmlns:cul="remove">
          <script language="JavaScript" src="csc-Theme/riskmaster/common/javascript/WC.js">{var i;}
              </script>
          <span class="formw">
            <asp:button class="button" runat="server" id="calcaww" RMXRef="" RMXType="buttonscript" Text="AWW Rate Calculations" OnClientClick="awwratecalc()" />
          </span>
          <span class="formw">
            <asp:button class="button" runat="Server" id="leavemgmt" RMXRef="/Instance/Claim/LeavePlanList/@committedcount" RMXType="button" Text="Leave Mgt" OnClientClick="if(!(XFormHandler('','SysFormPIdName=claimid&SysFormName=leave&SysCmd=1&SysFormIdName=leaverowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/PrimaryPiEmployee/PiRowId | /Instance/Claim/DateOfClaim','','button')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=leave&SysCmd=1&SysFormIdName=leaverowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/PrimaryPiEmployee/PiRowId | /Instance/Claim/DateOfClaim" />
          </span>
        </div>
        <div runat="server" class="half" id="div_constantwage" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="constantwage" RMXRef="/Instance/UI/FormVariables/SysExData/ConstantWage" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_lastworkweek" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="lastworkweek" RMXRef="/Instance/UI/FormVariables/SysExData/LastWorkWeek" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_includezeros" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="includezeros" RMXRef="/Instance/UI/FormVariables/SysExData/IncludeZeros" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_bonuses" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="bonuses" RMXRef="/Instance/UI/FormVariables/SysExData/Bonuses" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_aww" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="aww" RMXRef="/Instance/UI/FormVariables/SysExData/Aww" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_basearraynumber" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="basearraynumber" RMXRef="/Instance/UI/FormVariables/SysExData/BaseArrayNumber" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_weekarray" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="weekarray" RMXRef="/Instance/UI/FormVariables/SysExData/WeekArray" RMXType="hidden" />
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABmedicalinfo" id="FORMTABmedicalinfo">
        <asp:TextBox style="display:none" runat="server" id="hdnAddedBy" RMXRef="/Instance/UI/FormVariables/SysExData/PiXMMIHistList/Instance/PiXMMIHist/AddedByUser" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnApprovedBy" RMXRef="/Instance/UI/FormVariables/SysExData/PiXMMIHistList/Instance/PiXMMIHist/ApprovedByUser" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnDisability" RMXRef="/Instance/UI/FormVariables/SysExData/PiXMMIHistList/Instance/PiXMMIHist/PercentDisability" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnExamDate" RMXRef="/Instance/UI/FormVariables/SysExData/PiXMMIHistList/Instance/PiXMMIHist/ExamDate" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnPIRowId" RMXRef="/Instance/UI/FormVariables/SysExData/PiXMMIHistList/Instance/PiXMMIHist/PiRowId" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnPhysician" RMXRef="/Instance/UI/FormVariables/SysExData/PiXMMIHistList/Instance/PiXMMIHist/PhysicianEid" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnPhysicianEId" RMXRef="/Instance/UI/FormVariables/SysExData/PiXMMIHistList/Instance/PiXMMIHist/PhysicianEid/@codeid" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnReason" RMXRef="/Instance/UI/FormVariables/SysExData/PiXMMIHistList/Instance/PiXMMIHist/Reason" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnDiagPIRowId" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/PiRowId" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnDiagReason" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/Reason" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnDateChanged" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/DateChanged" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnDiagApprovedByUser" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/ApprovedByUser" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnDiagChangedByUser" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/ChgdByUser" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_icd9primarydiagnosis_cid" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="icd9primarydiagnosis_cid" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/DiagnosisCode/@codeid" RMXType="hidden" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="hdnmdaminimumdays" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/DisMinDuration" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnmdaoptimumdays" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/DisOptDuration" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="hdnmdamaximumdays" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/DisMaxDuration" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_mdatopicid" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="mdatopicid" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/DisMdaId" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdaguidelineid" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="mdaguidelineid" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/DisGuidelineId" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hospitallist" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_hospitallist" Text="Hospital" />
          <span class="formw">
            <asp:Listbox runat="server" size="3" id="hospitallist" RMXRef="/Instance/Claim/PrimaryPiEmployee/Hospitals" RMXType="entitylist" style="" Tabindex="154" />
            <input type="button" id="hospitallistbtn" tabindex="155" onclick="lookupData('hospitallist','HOSPITAL',4,'hospitallist',3)" class=" button" value="..." />
            <input type="button" class="button" id="hospitallistbtndel" tabindex="156" onclick="deleteSelCode('hospitallist')" value="-" />
            <asp:Textbox runat="server" style="display:none" RMXRef="/Instance/Claim/PrimaryPiEmployee/Hospitals/@codeid" id="hospitallist_lst" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hospitaldatefrom" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_hospitaldatefrom" Text="Hospital Date Range: (From)" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="hospitaldatefrom" RMXRef="/Instance/Claim/PrimaryPiEmployee/HrangeStartDate" RMXType="date" tabindex="177" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="hospitaldatefrombtn" tabindex="178" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "hospitaldatefrom",
					ifFormat : "%m/%d/%Y",
					button : "hospitaldatefrombtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_empdisabilitycode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_empdisabilitycode" Text="Disability Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="empdisabilitycode" CodeTable="DISABILITY_CODE" ControlName="empdisabilitycode" RMXRef="/Instance/Claim/PrimaryPiEmployee/DisabilityCode" RMXType="code" tabindex="157" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hospitaldateto" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_hospitaldateto" Text=" (To)" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="hospitaldateto" RMXRef="/Instance/Claim/PrimaryPiEmployee/HrangeEndDate" RMXType="date" tabindex="179" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="hospitaldatetobtn" tabindex="180" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "hospitaldateto",
					ifFormat : "%m/%d/%Y",
					button : "hospitaldatetobtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_illnesscode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_illnesscode" Text="Illness Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="illnesscode" CodeTable="ILLNESS_TYPE" ControlName="illnesscode" RMXRef="/Instance/Claim/PrimaryPiEmployee/IllnessCode" RMXType="code" tabindex="159" />
          </span>
        </div>
        <div runat="server" class="half" id="div_injurieslist" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_injurieslist" Text="Injuries" />
          <span class="formw">
            <uc:MultiCode runat="server" ID="injurieslist" CodeTable="INJURY_TYPE" ControlName="injurieslist" RMXRef="/Instance/Claim/PrimaryPiEmployee/InjuryCodes" RMXType="codelist" Tabindex="181" />
          </span>
        </div>
        <div runat="server" class="half" id="div_icd9primarydiagnosis" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_icd9primarydiagnosis" Text="(ICD9) Primary Diagnosis" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="icd9primarydiagnosis" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/DiagnosisCode" RMXType="text" tabindex="161" />
            <input type="button" class="button" value="..." onClick="javascript: return callDiagHist()" />
          </span>
        </div>
        <div runat="server" class="half" id="div_physicianslist" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_physicianslist" Text="Physicians" />
          <span class="formw">
            <asp:Listbox runat="server" size="3" id="physicianslist" RMXRef="/Instance/Claim/PrimaryPiEmployee/Physicians" RMXType="entitylist" style="" Tabindex="184" />
            <input type="button" id="physicianslistbtn" tabindex="185" onclick="lookupData('physicianslist','PHYSICIANS',4,'physicianslist',3)" class=" button" value="..." />
            <input type="button" class="button" id="physicianslistbtndel" tabindex="186" onclick="deleteSelCode('physicianslist')" value="-" />
            <asp:Textbox runat="server" style="display:none" RMXRef="/Instance/Claim/PrimaryPiEmployee/Physicians/@codeid" id="physicianslist_lst" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdatopic" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdatopic" Text="MDA Topic" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="mdatopic" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/DisMdaTopic" RMXType="text" tabindex="163" />
            <input type="button" class="button" value="Disability Guideline" onClick="formHandler('','','','text')" />
            <input type="button" class="button" value="..." onClick="javascript: return callMDALookUp()" />
          </span>
        </div>
        <div runat="server" class="half" id="div_treatmentlist" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_treatmentlist" Text="Treatment" />
          <span class="formw">
            <uc:MultiCode runat="server" ID="treatmentlist" CodeTable="TREATMENT_TYPE" ControlName="treatmentlist" RMXRef="/Instance/Claim/PrimaryPiEmployee/Treatments" RMXType="codelist" Tabindex="163" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdafactor" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdafactor" Text="MDA Factor" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="mdafactor" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/DisFactor" RMXType="text" tabindex="165" />
          </span>
        </div>
        <div runat="server" class="half" id="div_othertreatmentlist" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_othertreatmentlist" Text="Other Treatment" />
          <span class="formw">
            <asp:TextBox runat="Server" id="othertreatmentlist" RMXRef="/Instance/Claim/PrimaryPiEmployee/OtherTreatments" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="166" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="othertreatmentlistbtnMemo" tabindex="167" id="othertreatmentlistbtnMemo" onclick="EditMemo('othertreatmentlist','')" />
          </span>
        </div>
        <div runat="server" class="half" id="div_diagnosislist" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_diagnosislist" Text="Diagnosis" />
          <span class="formw">
            <uc:MultiCode runat="server" ID="diagnosislist" CodeTable="DIAGNOSIS_CODE" ControlName="diagnosislist" RMXRef="/Instance/Claim/PrimaryPiEmployee/DiagnosisList" RMXType="codelist" Tabindex="168" />
          </span>
        </div>
        <div runat="server" class="half" id="div_medicalconditionslist" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_medicalconditionslist" Text="Medical Conditions" />
          <span class="formw">
            <uc:MultiCode runat="server" ID="medicalconditionslist" CodeTable="MED_COND" ControlName="medicalconditionslist" RMXRef="/Instance/Claim/PrimaryPiEmployee/MedicalConditions" RMXType="codelist" Tabindex="187" />
          </span>
        </div>
        <div runat="server" class="half" id="div_bodypartslist" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_bodypartslist" Text="Body Parts" />
          <span class="formw">
            <uc:MultiCode runat="server" ID="bodypartslist" CodeTable="BODY_PART" ControlName="bodypartslist" RMXRef="/Instance/Claim/PrimaryPiEmployee/BodyParts" RMXType="codelist" Tabindex="170" />
          </span>
        </div>
        <div runat="server" class="half" id="div_othermedicalconditionslist" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_othermedicalconditionslist" Text="Other Medical Conditions" />
          <span class="formw">
            <asp:TextBox runat="Server" id="othermedicalconditionslist" RMXRef="/Instance/Claim/PrimaryPiEmployee/OtherMedcond" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="190" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="othermedicalconditionslistbtnMemo" tabindex="191" id="othermedicalconditionslistbtnMemo" onclick="EditMemo('othermedicalconditionslist','')" />
          </span>
        </div>
        <div runat="server" class="half" id="div_majorhandcode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_majorhandcode" Text="Major Hand" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="majorhandcode" CodeTable="MAJOR_HAND" ControlName="majorhandcode" RMXRef="/Instance/Claim/PrimaryPiEmployee/MajorHandCode" RMXType="code" tabindex="173" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mmidate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mmidate" Text="MMI Date" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PiXMMIHistList/Instance/PiXMMIHist/MmiDate" id="mmidate" tabindex="192" style="background-color: silver;" readonly="true" />
            <input type="button" class="button" value="..." id="mmidatebtn&#xA;                      " tabindex="193" onclick="&#xA;                        lookupData('mmidate','event',2,'',11)&#xA;                      " />
          </span>
        </div>
        <div runat="server" class="half" id="div_stddistype" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_stddistype" Text="STD Disability Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="stddistype" CodeTable="STD_DISABIL_TYPE" ControlName="stddistype" RMXRef="/Instance/Claim/PrimaryPiEmployee/StdDisabilityType" RMXType="code" tabindex="175" />
          </span>
        </div>
        <div runat="server" class="half" id="div_linebreak3" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_linebreak3" Text="" />
          <span class="formw">
            <tr>
              <td>
                <br />
              </td>
            </tr>
          </span>
        </div>
        <div runat="server" class="half" id="div_singlerow2" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:button class="button" runat="Server" id="casemanagerhist1" RMXRef="/Instance/Claim/CaseManagement/CaseMgrHistList/@count" RMXType="button" Text="Case Managers" OnClientClick="if(!(XFormHandler('','SysFormPIdName=claimid&SysFormName=CmXCmgrHistList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=casemanagementid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId','','button')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=CmXCmgrHistList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=casemanagementid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId" />
          </span>
          <span class="formw">
            <asp:button class="button" runat="Server" id="treatmentplan" RMXRef="/Instance/Claim/CaseManagement/TreatmentPlanList/@count" RMXType="button" Text="Treatment Plan" OnClientClick="if(!(XFormHandler('','SysFormPIdName=claimid&SysFormName=CmXTreatmentPlnList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=casemanagementid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId','','button')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=CmXTreatmentPlnList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=casemanagementid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId" />
          </span>
          <span class="formw">
            <asp:button class="button" runat="Server" id="medicalmgmtsavings" RMXRef="/Instance/Claim/CaseManagement/MedMgtSavingsList/@count" RMXType="button" Text="Medical Mgt Savings" OnClientClick="if(!(XFormHandler('','SysFormPIdName=claimid&SysFormName=CmXMedmgtsavingsList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=casemanagementid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId','','button')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=CmXMedmgtsavingsList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=casemanagementid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId" />
          </span>
        </div>
        <div runat="server" class="half" id="div_linebreak4" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_linebreak4" Text="" />
          <span class="formw">
            <tr>
              <td>
                <br />
              </td>
            </tr>
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABcasemgt" id="FORMTABcasemgt">
        <asp:TextBox style="display:none" runat="server" id="casemgtrowid" RMXRef="/Instance/Claim/CaseManagement/CasemgtRowId" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_primarycasemanagersummary" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="primarycasemanagersummary" Text="Primary Case Manager Summary:" />
          </span>
        </div>
        <div runat="server" class="half" id="div_casemanager" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_casemanager" Text="Case Manager" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/CmXCmgrHistList/Instance/CmXCmgrHist/CaseMgrEid" id="casemanager" tabindex="204" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_referraldate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_referraldate" Text="Referral Date" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/CmXCmgrHistList/Instance/CmXCmgrHist/RefDate" id="referraldate" tabindex="227" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_casestatuscode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_casestatuscode" Text="Case Status" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/CmXCmgrHistList/Instance/CmXCmgrHist/CaseStatusCode" id="casestatuscode" tabindex="205" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_dayswithcasemgr" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_dayswithcasemgr" Text="Days With Case Mgr" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/CaseMgrDays" id="dayswithcasemgr" tabindex="228" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_rtwmanagement" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="rtwmanagement" Text="RTW Management:" />
          </span>
        </div>
        <div runat="server" class="half" id="div_rtwstatuscode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_rtwstatuscode" Text="RTW Status" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="rtwstatuscode" CodeTable="RTW_STATUS" ControlName="rtwstatuscode" RMXRef="/Instance/Claim/CaseManagement/RtwStatusCode" RMXType="code" tabindex="207" />
          </span>
        </div>
        <div runat="server" class="half" id="div_restrictedwork" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="restrictedwork" Text="Restricted Work:" />
          </span>
        </div>
        <div runat="server" class="half" id="div_currentrestricteddays" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_currentrestricteddays" Text="Current Restricted Days?" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="currentrestricteddays" RMXRef="/Instance/UI/FormVariables/SysExData/ChkCurrResDays" RMXType="checkbox" tabindex="210" disabled="true" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_estreleasefromrst" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_estreleasefromrst" Text="Est. Release From Rst." />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="estreleasefromrst" RMXRef="/Instance/Claim/CaseManagement/EstRelRstDate" RMXType="date" tabindex="229" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="estreleasefromrstbtn" tabindex="230" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "estreleasefromrst",
					ifFormat : "%m/%d/%Y",
					button : "estreleasefromrstbtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_actuallastrstday" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_actuallastrstday" Text="Actual Last Rst. Day" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PiXRestrictList/Instance/PiXRestrict/DateLastRestrct" id="actuallastrstday" tabindex="211" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_estlenofdis1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_estlenofdis1" Text="Est. Len.  of Disability" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/EstLenDis" id="estlenofdis1" tabindex="230" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_totalresdays" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_totalresdays" Text="Total Restricted Days" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/TotalResDays" id="totalresdays" tabindex="212" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdadurations1" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="mdadurations1" Text="MDA Durations - Days/Date:" />
          </span>
        </div>
        <div runat="server" class="half" id="div_rw_jobclassification" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_rw_jobclassification" Text="Job Classification" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/Claim/PrimaryPiEmployee/JobClassCode" id="rw_jobclassification" tabindex="214" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdaminimumdays1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdaminimumdays1" Text="Minimum Days" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PiXDiagHist_DisMinDuration" id="mdaminimumdays1" tabindex="215" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdaminimum1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdaminimum1" Text="Minimum" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/RW_MinDate" id="mdaminimum1" tabindex="231" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdaoptimumdays1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdaoptimumdays1" Text="Optimum Days" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PiXDiagHist_DisOptDuration" id="mdaoptimumdays1" tabindex="216" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdaoptimum1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdaoptimum1" Text="Optimum" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/RW_OptDate" id="mdaoptimum1" tabindex="232" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdamaximumdays1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdamaximumdays1" Text="Maximum Days" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PiXDiagHist_DisMaxDuration" id="mdamaximumdays1" tabindex="217" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdamaximum1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdamaximum1" Text="Maximum" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/RW_MaxDate" id="mdamaximum1" tabindex="233" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_workloss" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="workloss" Text="Work Loss:" />
          </span>
        </div>
        <div runat="server" class="half" id="div_currentworkloss" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_currentworkloss" Text="Current Work Loss?" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="currentworkloss" RMXRef="/Instance/UI/FormVariables/SysExData/ChkWorkLoss" RMXType="checkbox" tabindex="219" disabled="true" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_estrtwdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_estrtwdate" Text="Est. RTW Date" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="estrtwdate" RMXRef="/Instance/Claim/CaseManagement/EstRetWorkDate" RMXType="date" tabindex="234" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="estrtwdatebtn" tabindex="235" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "estrtwdate",
					ifFormat : "%m/%d/%Y",
					button : "estrtwdatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_actualrtwdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_actualrtwdate" Text="Actual RTW Date" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PiXWorkLossList/Instance/PiXWorkLoss/DateReturned" id="actualrtwdate" tabindex="220" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_estlenofdis2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_estlenofdis2" Text="Est. Len.  of Disability" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/EstDisability" id="estlenofdis2" tabindex="235" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_totallostdays" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_totallostdays" Text="Total Lost Days" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/TotalLostDays" id="totallostdays" tabindex="221" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdadurations2" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="mdadurations2" Text="MDA Durations - Days/Date:" />
          </span>
        </div>
        <div runat="server" class="half" id="div_wl_jobclassification" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_wl_jobclassification" Text="Job Classification" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/Claim/PrimaryPiEmployee/JobClassCode" id="wl_jobclassification" tabindex="223" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdaminimumdays2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdaminimumdays2" Text="Minimum Days" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PiXDiagHist_DisMinDuration" id="mdaminimumdays2" tabindex="224" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdaminimum2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdaminimum2" Text="Minimum" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/WL_MinDate" id="mdaminimum2" tabindex="236" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdaoptimumdays2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdaoptimumdays2" Text="Optimum Days" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PiXDiagHist_DisOptDuration" id="mdaoptimumdays2" tabindex="225" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdaoptimum2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdaoptimum2" Text="Optimum" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/WL_OptDate" id="mdaoptimum2" tabindex="237" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdamaximumdays2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdamaximumdays2" Text="Maximum Days" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PiXDiagHist_DisMaxDuration" id="mdamaximumdays2" tabindex="226" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mdamaximum2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mdamaximum2" Text="Maximum" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/WL_MaxDate" id="mdamaximum2" tabindex="238" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_linebreak5" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_linebreak5" Text="" />
          <span class="formw">
            <tr>
              <td>
                <br />
              </td>
            </tr>
          </span>
        </div>
        <div runat="server" class="half" id="div_singlerow3" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:button class="button" runat="Server" id="casemanagerhist2" RMXRef="/Instance/Claim/CaseManagement/CaseMgrHistList/@count" RMXType="button" Text="Case Managers" OnClientClick="if(!(XFormHandler('','SysFormPIdName=claimid&SysFormName=CmXCmgrHistList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=casemanagementid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId','','button')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=CmXCmgrHistList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=casemanagementid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId" />
          </span>
          <span class="formw">
            <asp:button class="button" runat="Server" id="employeerestrictions" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiXRestrictList/@count" RMXType="button" Text="Restrictions" OnClientClick="if(!(XFormHandler('','SysFormPIdName=claimid&SysFormName=pirestrictionlist&SysViewType=controlsonly&SysCmd=1&SysFormIdName=pirowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/PrimaryPiEmployee/PiRowId | /Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventNumber | /Instance/Claim/EventId','','button')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=pirestrictionlist&SysViewType=controlsonly&SysCmd=1&SysFormIdName=pirowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/PrimaryPiEmployee/PiRowId | /Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventNumber | /Instance/Claim/EventId" />
          </span>
          <span class="formw">
            <asp:button class="button" runat="Server" id="employeeworkloss" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiXWorkLossList/@count" RMXType="button" Text="Work Loss" OnClientClick="if(!(XFormHandler('','SysFormPIdName=claimid&SysFormName=piworklosslist&SysCmd=1&SysFormIdName=pirowid&SysViewType=controlsonly&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/PrimaryPiEmployee/PiRowId | /Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventNumber | /Instance/Claim/EventId | /Instance/Claim/PrimaryPiEmployee/DeptAssignedEid','','button')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=piworklosslist&SysCmd=1&SysFormIdName=pirowid&SysViewType=controlsonly&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/PrimaryPiEmployee/PiRowId | /Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventNumber | /Instance/Claim/EventId | /Instance/Claim/PrimaryPiEmployee/DeptAssignedEid" />
          </span>
          <span class="formw">
            <asp:button class="button" runat="Server" id="accommodationlist" RMXRef="/Instance/Claim/CaseManagement/AccommodationList/@count" RMXType="button" Text="Accommodation" OnClientClick="if(!(XFormHandler('','SysFormPIdName=claimid&SysFormName=CmXAccommodationList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=casemanagementid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId','','button')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=CmXAccommodationList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=casemanagementid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId" />
          </span>
          <span class="formw">
            <asp:button class="button" runat="Server" id="vocrehab" RMXType="button" Text="Vocational Rehabilitation" OnClientClick="if(!(XFormHandler('','SysFormName=CmXVocrehab&SysCmd=1&SysFormIdName=cmvrrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId','','button')))return false;" PostBackUrl="?SysFormName=CmXVocrehab&SysCmd=1&SysFormIdName=cmvrrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/CaseManagement/CasemgtRowId" style="&#xA;                      width:150px" />
          </span>
          <script language="JavaScript" src="">{var i;}
              </script>
          <span class="formw">
            <asp:button class="button" runat="server" id="disguidelines" RMXRef="" RMXType="buttonscript" Text="Medical Disability Guidelines" width="150px" OnClientClick="displaymedicalguideline();" />
          </span>
        </div>
        <div runat="server" class="half" id="div_linebreak6" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_linebreak6" Text="" />
          <span class="formw">
            <tr>
              <td>
                <br />
              </td>
            </tr>
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
        <asp:TextBox style="display:none" runat="server" id="supp_claim_id" RMXRef="/Instance/*/Supplementals/CLAIM_ID" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_supp_empl_enrl_mco_code" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_supp_empl_enrl_mco_code" Text="Employer MCO Enrolled" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="supp_empl_enrl_mco_code" CodeTable="YES_NO" ControlName="supp_empl_enrl_mco_code" RMXRef="/Instance/*/Supplementals/EMPL_ENRL_MCO_CODE" RMXType="code" tabindex="1896" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" name="supp_empl_enrl_mco_code_GroupAssoc" id="supp_empl_enrl_mco_code_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_supp_typeofreport_code" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_supp_typeofreport_code" Text="Type Of Report" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="supp_typeofreport_code" CodeTable="GA_REPORT_TYP_CODE" ControlName="supp_typeofreport_code" RMXRef="/Instance/*/Supplementals/TYPEOFREPORT_CODE" RMXType="code" tabindex="1897" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" name="supp_typeofreport_code_GroupAssoc" id="supp_typeofreport_code_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_supp_able_rtw_date" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_supp_able_rtw_date" Text="Date Able to RTW" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="supp_able_rtw_date" RMXRef="/Instance/*/Supplementals/ABLE_RTW_DATE" RMXType="date" tabindex="1898" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="supp_able_rtw_datebtn" tabindex="1899" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "supp_able_rtw_date",
					ifFormat : "%m/%d/%Y",
					button : "supp_able_rtw_datebtn"
					}
					);
				</script>
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" name="supp_able_rtw_date_GroupAssoc" id="supp_able_rtw_date_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_supp_perm_loss_numb" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_supp_perm_loss_numb" Text="Permanent Percent Loss" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="supp_perm_loss_numb" RMXRef="/Instance/*/Supplementals/PERM_LOSS_NUMB" RMXType="numeric" tabindex="1899" onChange="setDataChanged(true);" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" name="supp_perm_loss_numb_GroupAssoc" id="supp_perm_loss_numb_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_supp_perm_loss_text" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_supp_perm_loss_text" Text="Permanent Body Part Loss" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_perm_loss_text" RMXRef="/Instance/*/Supplementals/PERM_LOSS_TEXT" RMXType="text" tabindex="1900" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" name="supp_perm_loss_text_GroupAssoc" id="supp_perm_loss_text_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_supp_test_text" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_supp_test_text" Text="Test" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_test_text" RMXRef="/Instance/*/Supplementals/TEST_TEXT" RMXType="text" tabindex="2900" />
          </span>
        </div>
        <div runat="server" class="half" id="div_supp_ent_lookup_eid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_supp_ent_lookup_eid" Text="ENTITY LOOK UP" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="supp_ent_lookup_eid" RMXRef="/Instance/*/Supplementals/ENT_LOOKUP_EID" RMXType="eidlookup" cancelledvalue="" tabindex="2905" />
            <input type="button" class="button" value="..." name="supp_ent_lookup_eidbtn" tabindex="2906" onclick="lookupData('supp_ent_lookup_eid','ADJUSTERS',4,'supp_ent_lookup_eid',2)" />
            <asp:TextBox style="display:none" runat="server" id="supp_ent_lookup_eid_cid" RMXref="/Instance/*/Supplementals/ENT_LOOKUP_EID/@codeid" cancelledvalue="" />
          </span>
        </div>
      </div>
      <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnPI">
          <asp:Button class="button" runat="Server" id="btnPI" Text="Persons Involved" RMXRef="/Instance/UI/FormVariables/SysExData/PiList/@committedcount" OnClientClick="if(!( XFormHandler('SysFormPIdName=claimid&SysFormName=personinvolvedlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=pirowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/EventId | /Instance/Claim/EventNumber','','')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=personinvolvedlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=pirowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/EventId | /Instance/Claim/EventNumber" />
        </div>
        <div class="formButton" runat="server" id="div_btnAdjuster">
          <asp:Button class="button" runat="Server" id="btnAdjuster" Text="Adjuster" RMXRef="/Instance/Claim/AdjusterList/@committedcount" OnClientClick="if(!( XFormHandler('SysFormPIdName=claimid&SysFormName=adjuster&SysCmd=1&SysFormIdName=adjrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=adjuster&SysCmd=1&SysFormIdName=adjrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber" />
        </div>
        <div class="formButton" runat="server" id="div_btnLitigation">
          <asp:Button class="button" runat="Server" id="btnLitigation" Text="Litigation" RMXRef="/Instance/Claim/LitigationList/@committedcount" OnClientClick="if(!( XFormHandler('SysFormPIdName=claimid&SysFormName=litigation&SysCmd=1&SysFormIdName=litigationrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=litigation&SysCmd=1&SysFormIdName=litigationrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber" />
        </div>
        <div class="formButton" runat="server" id="div_btnDefendant">
          <asp:Button class="button" runat="Server" id="btnDefendant" Text="Defendant" RMXRef="/Instance/Claim/DefendantList/@committedcount" OnClientClick="if(!( XFormHandler('SysFormPIdName=claimid&SysFormName=defendant&SysCmd=1&SysFormIdName=defendantrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')))return false;" PostBackUrl="?SysFormPIdName=claimid&SysFormName=defendant&SysCmd=1&SysFormIdName=defendantrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber" />
        </div>
        <div class="formButton" runat="server" id="div_btnNonOcc">
          <asp:Button class="button" runat="Server" id="btnNonOcc" Text="Non-Occ Payments" OnClientClick="if(!( XFormHandler('SysFormPIdName=claimid&SysFormName=nonocc&SysCmd=1&SysFormIdName=claimid&SysEx=ClaimId|ClaimNumber|PiEid|Entity_Id|PiRowId&SysExMapCtl=ClaimId|ClaimNumber|PiEid|Entity_Id|PiRowId','','')))return false;" PostBackUrl="nonocc.aspx?SysFormPIdName=claimid&SysFormName=nonocc&SysCmd=1&SysFormIdName=claimid&SysEx=ClaimId|ClaimNumber|PiEid|Entity_Id|PiRowId&SysExMapCtl=ClaimId|ClaimNumber|PiEid|Entity_Id|PiRowId" />
        </div>
        <div class="formButton" runat="server" id="div_btnFinancials">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnFinancials" RMXRef="" Text="Financials" onClientClick="gotoreserves();" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="classlist" RMXRef="/Instance/UI/FormVariables/SysExData/ClassList/@codeid" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="pixmmirowid" RMXRef="/Instance/UI/FormVariables/SysExData/PiXMMIHistList/Instance/PiXMMIHist/PiXMmirowId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="pixdiagrowid" RMXRef="/Instance/UI/FormVariables/SysExData/PiXDiagHistList/Instance/PiXDiagHist/PiXDiagrowId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="pirowid" RMXRef="/Instance/Claim/PrimaryPiEmployee/PiRowId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="Claim" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;Claim&gt;&lt;PrimaryPiEmployee&gt;&lt;PiEntity/&gt;&lt;PiXDiagHistList/&gt;&lt;PiXMMIHistList/&gt;&lt;PiXRestrictList/&gt;&lt;PiXWorkLossList/&gt;&lt;/PrimaryPiEmployee&gt;&lt;CaseManagement&gt;&lt;CaseMgrHistList/&gt;&lt;/CaseManagement&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/Claim&gt;" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="eventid" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="claimnumber|ev_eventnumber|ev_timeofevent|timeofclaim" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="claimdi" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="claimid" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="60000" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="line_of_bus_code" RMXRef="" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="OH_FACILITY_EID" RMXRef="Instance/UI/FormVariables/SysExData/OH_FACILITY_EID" RMXType="hidden" Text="0" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="OH_LOCATION_EID" RMXRef="Instance/UI/FormVariables/SysExData/OH_LOCATION_EID" RMXType="hidden" Text="0" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="OH_DIVISION_EID" RMXRef="Instance/UI/FormVariables/SysExData/OH_DIVISION_EID" RMXType="hidden" Text="0" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="OH_REGION_EID" RMXRef="Instance/UI/FormVariables/SysExData/OH_REGION_EID" RMXType="hidden" Text="0" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="OH_OPERATION_EID" RMXRef="Instance/UI/FormVariables/SysExData/OH_OPERATION_EID" RMXType="hidden" Text="0" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="OH_COMPANY_EID" RMXRef="Instance/UI/FormVariables/SysExData/OH_COMPANY_EID" RMXType="hidden" Text="0" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="OH_CLIENT_EID" RMXRef="Instance/UI/FormVariables/SysExData/OH_CLIENT_EID" RMXType="hidden" Text="0" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="claimdi" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="ev_eventnumber|ev_dateofevent|ev_timeofevent|claimnumber|dateofclaim|timeofclaim|claimtypecode_codelookup_cid|empemployeenumber|disfromdate|ev_depteid_cid|claimstatuscode_codelookup_cid|planname|jurisdiction_codelookup_cid|emplastname|emptaxid|empdeptassignedeid_cid|supp_test_text|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="ev_eventnumber|ev_onpremiseflag|emplastname|empdatehired|" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>