﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.FDM
{
    public partial class Policybilling : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
        }

        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            HtmlContainerControl oDivControl = (HtmlContainerControl)this.FindControl("div_TransactionHistoryGrid");
            if (oDivControl != null)
            {
                oDivControl.Attributes["class"] = "completerow";
            }
        }

    }
}
