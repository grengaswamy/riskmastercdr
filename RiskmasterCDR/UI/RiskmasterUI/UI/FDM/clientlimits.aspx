<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="clientlimits.aspx.cs"  Inherits="Riskmaster.UI.FDM.Clientlimits" ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Client Limits</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <script language="JavaScript" src="../../Scripts/rmx-common-ref.js">{var i;}
  </script>
    <script language="JavaScript" src="../../Scripts/supportscreens.js">{var i;}</script>
    <script language="JavaScript" src="../../Scripts/grid.js">{var i;}</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="CopyGridRowDataToPopup();return checkPopUpInfo();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Client Limits" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSclientlimits" id="TABSclientlimits">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="clientlimits" id="LINKTABSclientlimits">Client Limits</a>
        </div>
        <div id="Div2" class="tabSpace" runat="server">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" style="height:310px;overflow:auto;position:relative;" name="FORMTABclientlimits" id="FORMTABclientlimits">
        <asp:TextBox style="display:none" runat="server" id="ClientLimitsRowId" RMXRef="//ClientLimitsRowId" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="ClientEid" RMXRef="//ClientEid" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="UniqueId" RMXRef="" RMXType="id" Text="ClientLimitsRowId" xmlns:cul="remove" />
        <div runat="server" class="full" id="div_PaymentFlag_Text" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_PaymentFlag_Text" Text="Type Of Limit" />
          <span class="formw">
            <asp:DropDownList runat="server" id="PaymentFlag_Text" tabindex="1" RMXRef="//PaymentFlag_Text" RMXType="combobox" onchange=" return CheckTypeofLimit(this);">
              <asp:ListItem Value="0" Text="Reserve" />
              <asp:ListItem Value="-1" Text="Payment" />
            </asp:DropDownList>
          </span>
        </div>
        <div runat="server" class="completerow" id="div_LobCode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_LobCode" Text="Line Of Business" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="LobCode" CodeTable="LINE_OF_BUSINESS" ControlName="LobCode" RMXRef="//LobCode" RMXType="code" tabindex="2" Required="true" ValidationGroup="vgSave" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_MaxAmount" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_MaxAmount" Text="Max Amount" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="MaxAmount" RMXRef="//MaxAmount" RMXType="text" tabindex="3" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_ReserveTypeCode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_ReserveTypeCode" Text="Reserve Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="ReserveTypeCode" CodeTable="RESERVE_TYPE" ControlName="ReserveTypeCode" RMXRef="//ReserveTypeCode" RMXType="code" tabindex="4" Required="true" ValidationGroup="vgSave" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="comboRelations" RMXRef="" RMXType="id" xmlns:cul="remove" />
      </div>
      <div id="Div3" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnOk">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnOk" RMXRef="" Text="OK" width="75px" onClientClick="return ClientLimits_onOk();" onclick="btnOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return ClientLimits_onCancel();" onclick="btnCancel_Click" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" name="formname" Text="clientlimits" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="PaymentFlag_Text|LobCode_codelookup_cid|MaxAmount|ReserveTypeCode_codelookup_cid|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>