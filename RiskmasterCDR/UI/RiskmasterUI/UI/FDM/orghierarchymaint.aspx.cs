﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.CommonWCFService;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.FDM
{
    public partial class Orghierarchymaint : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the Required Grid
            //added by Navdeep - Map on Grid from the Business Layer (Chubb Ack)
            Control cPostBackControl = null;
            UserControlDataGrid dgEntContactInfo = (UserControlDataGrid)this.FindControl("EntityXContactInfoGrid");
            cPostBackControl = Page.FindControl("SysPostBackAction");

            if (cPostBackControl != null)
            {
                if (((TextBox)cPostBackControl).Text == "Last")
                {
                    dgEntContactInfo.IncludeLastRecord = true;
                    ((TextBox)cPostBackControl).Text = string.Empty;
                }               
            }
            //End Navdeep - 

            //Added Rakhi forR7:Add Emp Data Elements
            TextBox txtUseMultipleAddresses = (TextBox)this.FindControl("UseMultipleAddresses");
            TextBox txtPrimaryAddressChanged = (TextBox)this.FindControl("PrimaryAddressChanged");
            if (txtUseMultipleAddresses != null && txtUseMultipleAddresses.Text.ToLower() == "true")
            {
                if (txtPrimaryAddressChanged != null)
                    IncludeLastRecord(txtPrimaryAddressChanged.Text);
            }
            //Added Rakhi forR7:Add Emp Data Elements

            FDMPageLoad();

            //Added Rakhi forR7:Add Emp Data Elements
            if (txtPrimaryAddressChanged != null)
                txtPrimaryAddressChanged.Text = "false"; //Setting it false after the processing is over.
            //Added Rakhi forR7:Add Emp Data Elements

            string sAction = AppHelper.GetQueryStringValue("action");
            if (sAction == "New")
            {
               Control oControl = this.FindControl("parentoperation");
               WebControl wb; 
               //MITS 22599: We need to check the type of control because if the form is readonly we only have labels on the screen
               //and no text boxes.

               if (oControl != null)
               {
                   wb = (WebControl)oControl;

                   if (wb.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                   {
                       ((TextBox)oControl).Text = AppHelper.GetQueryStringValue("parentname");
                   }
                   else if (wb.GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                   {
                       ((Label)oControl).Text = AppHelper.GetQueryStringValue("parentname");
                   }
               } 

               oControl = this.FindControl("parenteid");


               if (oControl != null)
               {
                   wb = (WebControl)oControl;
                   
                   if (wb.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                   {
                      ((TextBox)oControl).Text = AppHelper.GetQueryStringValue("parenteid");
                   }
                   else if (wb.GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                   {
                      ((Label)oControl).Text = AppHelper.GetQueryStringValue("parenteid");
                   }
               }
             }

            //Change done by kuladeep for mits:32073 Start

            //Animesh Inserted RMSC Bill Review //skhare7 RMSC Merge 28397
           // string sEntityLevel = AppHelper.GetQueryStringValue("entitylevel");    
            /*
            Control oControlTemp = this.FindControl("hdnBillRevActive");
            string strBillReviewFlag = string.Empty;
            string strBillReviewOrgLevel = string.Empty;
            //skhare7 MITS 28760
            string sEntityId = string.Empty;
            string sSysSettingEntityId = string.Empty;
            //skhare7 MITS 28760 End
            if (oControlTemp != null)
            {
                strBillReviewFlag = ((TextBox)oControlTemp).Text;
            }
            oControlTemp = this.FindControl("hdnBillRevOrg");
            if (oControlTemp != null)
            {
                strBillReviewOrgLevel = ((TextBox)oControlTemp).Text;
            }
            //skhare7 MITS 28760
            oControlTemp = this.FindControl("hdnBillRevOrgEId");
            if (oControlTemp != null)
            {
                sSysSettingEntityId = ((TextBox)oControlTemp).Text;
            }
            oControlTemp = this.FindControl("entityid");
            if (oControlTemp != null)
            {
                sEntityId = ((TextBox)oControlTemp).Text;
            }
            //skhare7 MITS 28760 End
            if (strBillReviewFlag.Equals("") || strBillReviewOrgLevel.Equals(""))
            {
                BillReviewSection(false);
            }
            else
            {
                //skhare7 MITS 28760 //skhare7 MITS 29217
              //  if (strBillReviewFlag.ToLower().Equals("true") && strBillReviewOrgLevel.Equals(sEntityLevel))
                if (strBillReviewFlag.ToLower().Equals("true") )
                {
                    if (sSysSettingEntityId.Equals("0"))
                        BillReviewSection(false);
                    else
                    {
                        if (sEntityId.Equals(sSysSettingEntityId))
                        
                            BillReviewSection(true);
                        else
                            BillReviewSection(false);
                    
                    }

                }
                else
                {
                    BillReviewSection(false);
                }
                //skhare7 MITS 28760 End
            }
            */
            string sEntityLevel = AppHelper.GetQueryStringValue("entitylevel");

            //mkaran2 : MITS 33657 : Start

            if (!Page.IsPostBack)
                ViewState["EntityLevel"] = sEntityLevel;
           
            if (ViewState["EntityLevel"] != null && !string.IsNullOrEmpty(ViewState["EntityLevel"].ToString()))
                sEntityLevel = ViewState["EntityLevel"].ToString();

            //mkaran2 : MITS 33657 : End
            //igupta3

            Control ocControlTracking = this.FindControl("timezonetracking");
            Control ocControlTimeZone = this.FindControl("timezonecode$codelookup");
            Control ocControlHidden = this.FindControl("timezonecode$codelookup_cid");
            Control ocControlLbl = this.FindControl("lbl_timezonecode");
            if (ocControlTracking != null && ocControlTimeZone != null)
            {
                if (((CheckBox)ocControlTracking).Checked == false)
                {
                    ((TextBox)ocControlTimeZone).Enabled = false;
                    ((TextBox)ocControlTimeZone).Text = "";
                    ((TextBox)ocControlHidden).Text = "";
                    ((Label)ocControlLbl).CssClass = "label";
                }
                else
                {
                    
                    ((Label)ocControlLbl).CssClass = "required";
                    //((TextBox)ocControlTimeZone).Enabled = true;
                    DatabindingHelper.EnableControls("timezonecode$codelookup", this);

                }
            }
            Control oControlTemp = this.FindControl("hdnBillRevActive");
            string strBillReviewFlag = string.Empty;
            string strBillReviewOrgLevel = string.Empty;
            if (oControlTemp != null)
            {
                strBillReviewFlag = ((TextBox)oControlTemp).Text;
            }
            oControlTemp = this.FindControl("hdnBillRevOrg");
            if (oControlTemp != null)
            {
                strBillReviewOrgLevel = ((TextBox)oControlTemp).Text;
            }
            if(string.IsNullOrEmpty(strBillReviewFlag) || string.IsNullOrEmpty(strBillReviewOrgLevel))
            {
                BillReviewSection(false);
            }
            else
            {
                if (strBillReviewFlag.ToLower().Equals("true") && strBillReviewOrgLevel.Equals(sEntityLevel))
                {
                    BillReviewSection(true);
                }
                else
                {
                    BillReviewSection(false);
                }
            }
            //Change done by kuladeep for mits:32073 End
            
        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            try
            {
                //Modifying the names of the first 2 tabs according to querystring inputs
                string sEntityLevel = AppHelper.GetQueryStringValue("entitylevel");
                string sOrgLevel = "";
                string sOrgLevelInfo = "";
                if (!String.IsNullOrEmpty(sEntityLevel))
                {
                    switch (sEntityLevel)
                    {
                        case "1":
                        case "1005":
                            sOrgLevel = "Client";
                            sOrgLevelInfo = "Client Info";

                            break;
                        case "2":
                        case "1006":
                            sOrgLevel = "Company";
                            sOrgLevelInfo = "Company Info";

                            break;
                        case "3":
                        case "1007":
                            sOrgLevel = "Operation";
                            sOrgLevelInfo = "Operation Info";

                            break;
                        case "4":
                        case "1008":
                            sOrgLevel = "Region";
                            sOrgLevelInfo = "Region Info";

                            break;
                        case "5":
                        case "1009":
                            sOrgLevel = "Division";
                            sOrgLevelInfo = "Division Info";

                            break;
                        case "6":
                        case "1010":
                            sOrgLevel = "Location";
                            sOrgLevelInfo = "Location Info";

                            break;
                        case "7":
                        case "1011":
                            sOrgLevel = "Facility";
                            sOrgLevelInfo = "Facility Info";

                            break;
                        case "8":
                        case "1012":
                            sOrgLevel = "Department";
                            sOrgLevelInfo = "Department Info";

                            break;
                        default:
                            sOrgLevel = "Department";
                            sOrgLevelInfo = "Department Info";
                            break;

                    }
                    Control oControl = this.FindControl("TABSdepartment");
                    if (oControl != null)
                    {
                        ((System.Web.UI.HtmlControls.HtmlAnchor)(oControl.FindControl("LINKTABSdepartment"))).InnerText = sOrgLevel;
                        //MITS 27866 hlv 4/20/2012 begin
                        if (this.FindControl("TABSdeptinfo") != null)
                        {
                            ((System.Web.UI.HtmlControls.HtmlAnchor)(this.FindControl("TABSdeptinfo").FindControl("LINKTABSdeptinfo"))).InnerText = sOrgLevelInfo;
                        }
                        //MITS 27866 hlv 4/20/2012 end
                    }
                    else //Mits 22789:Top down Layout
                    {
                        oControl = this.FindControl("department");
                        if (oControl != null)
                        {
                            ((System.Web.UI.HtmlControls.HtmlGenericControl)oControl).InnerText = sOrgLevel;
                            oControl = this.FindControl("deptinfo");
                            if (oControl != null)
                            {
                                ((System.Web.UI.HtmlControls.HtmlGenericControl)oControl).InnerText = sOrgLevelInfo;
                            }
                        }
                    }
                    XElement oEle = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/OrgHierarchy/EntityTableId");
                    if (oEle != null)
                    {
                        oEle.SetValue(sEntityLevel);
                    }


                }
                //int iCodeId = 0;
                //Parent is no longer a dropdown
                /*
                XElement oParentEle = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ParentEid");
                if (oParentEle == null)
                {
                    oParentEle = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/Entity/ParentEid");
                    iCodeId = Conversion.ConvertStrToInteger(oParentEle.Attribute("codeid").Value);

                }
                else
                {
                    XElement oEle = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/Entity/ParentEid");
                    if (oEle != null)
                    {
                        if(!String.IsNullOrEmpty(oParentEle.Value))
                        {
                            oEle.Attribute("codeid").Value = oParentEle.Value;
                        }
                    }
                    iCodeId = Conversion.ConvertStrToInteger(oParentEle.Value); 
                }
                if (oParentEle != null && ((!String.IsNullOrEmpty(oParentEle.Value) && oParentEle.Value != "0") || iCodeId != 0))
                {

                    //Calling web service to get Parent List
                    XElement oParentMessageElement = GetMessageTemplate();

                    XElement oEle = oParentMessageElement.XPathSelectElement("./Document/OrgHierarchy/child_eid");
                    oEle.Value = sEntityLevel;

                    //Call WCF wrapper for cws
                    string sReturn = AppHelper.CallCWSService(oParentMessageElement.ToString());
                    BindData2ErrorControl(sReturn);
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sReturn);
                    XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                    XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                    oFDMPageDom.LoadXml(oInstanceNode.OuterXml);

                    //Fill Parent dropdownlist

                    XmlNodeList cmbData = oFDMPageDom.SelectNodes("//xml/option");
                    ((DropDownList)this.FindControl("parentoperation")).Items.Clear();
                    foreach (XmlNode oNode in cmbData)
                    {
                        ListItem oItem = new ListItem(oNode.InnerText, oNode.Attributes["value"].Value);
                        ((DropDownList)this.FindControl("parentoperation")).Items.Add(oItem);

                    }
                }
                */
                    string sAction = Request.QueryString["action"];
                    sOrgLevel = "";
                    sOrgLevelInfo = "";
                    if (!String.IsNullOrEmpty(sAction))
                    {
                        if (sAction == "New" && String.IsNullOrEmpty(sEntityLevel))
                        {
                            Control oControl = this.FindControl("div_parentoperation");
                            if (oControl != null)
                            {
                                oControl.Visible = false;
                            }

                            oControl = this.FindControl("TABSdepartment");
                            if (oControl != null)
                            {
                                ((System.Web.UI.HtmlControls.HtmlAnchor)(oControl.FindControl("LINKTABSdepartment"))).InnerText = "Client";
                                ((System.Web.UI.HtmlControls.HtmlAnchor)(this.FindControl("TABSdeptinfo").FindControl("LINKTABSdeptinfo"))).InnerText = "Client Info";
                            }
                            else //Mits 22789:Top down Layout
                            {
                                oControl = this.FindControl("department");
                                if (oControl != null)
                                {
                                    ((System.Web.UI.HtmlControls.HtmlGenericControl)oControl).InnerText = "Client"; 
                                    oControl = this.FindControl("deptinfo");
                                    if (oControl != null)
                                    {
                                        ((System.Web.UI.HtmlControls.HtmlGenericControl)oControl).InnerText = "Client Info";
                                    }
                                }
                            }
                            oControl = this.FindControl("entitytableid");
                            if (oControl != null)
                            {
                                ((TextBox)oControl).Text = "1005";

                                //Update oMessageElement
                                XElement oEle = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/OrgHierarchy/EntityTableId");
                                if (oEle != null)
                                {
                                    oEle.SetValue("1005");
                                }
                            }
                        }
                        else
                        {
                            if (sAction == "Edit" && sEntityLevel == "1005")
                            {
                                Control oControl = this.FindControl("div_parentoperation");
                                if (oControl != null)
                                {
                                    oControl.Visible = false;
                                }
                            }
                        }

                        //Yatharth: Timezone Implementation
                        if (sAction == "New")
                        {
                            XElement oElement = oMessageElement.XPathSelectElement("//UtilTimeZoneLevel");

                            if (oElement.Value != sEntityLevel)
                            {
                                Control oControlTimeZone = this.FindControl("div_timezonetracking");
                                if (oControlTimeZone != null)
                                {
                                    oControlTimeZone.Visible = false;
                                }

                                oControlTimeZone = this.FindControl("div_timezonecode");
                                if (oControlTimeZone != null)
                                {
                                    oControlTimeZone.Visible = false;
                                }

                                oControlTimeZone = this.FindControl("div_daylightsavings");
                                if (oControlTimeZone != null)
                                {
                                    oControlTimeZone.Visible = false;
                                }

                            }
                            
                        }

                    }
               
                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                BindData2ErrorControl(ErrorHelper.formatUIErrorXML(err));

            }

            

        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>0e4fa7d9-9ebb-4467-9e47-fdac6c055b45</Authorization> 
             <Call>
              <Function>OrgHierarchyAdaptor.GetParentList</Function> 
              </Call>
             <Document>
             <OrgHierarchy>
              <child_eid></child_eid> 
              </OrgHierarchy>
              </Document>
              </Message>

            ");

            return oTemplate;
        }
        public override void ModifyControls()
        {
            //Added Rakhi For R7:Add Emp Data Elements
            TextBox txtUseMultipleAddresses = (TextBox)this.FindControl("UseMultipleAddresses");
            if (txtUseMultipleAddresses != null && txtUseMultipleAddresses.Text.ToLower() == "true")
            {
                SetPrimaryAddress();
            }
            //Added Rakhi For R7:Add Emp Data Elements
        }
        private void IncludeLastRecord(string sPrimaryAddressChanged)
        {
            UserControlDataGrid objGridControl = (UserControlDataGrid)this.FindControl("EntityXAddressesInfoGrid");
            if (objGridControl != null)
            {
                if (sPrimaryAddressChanged.ToLower() == "true")
                {

                    objGridControl.IncludeLastRecord = true;
                }
                else
                {
                    objGridControl.IncludeLastRecord = false;
                }
            }
        }
        private void SetPrimaryAddress()
        {
            UserControlDataGrid objGridControl = (UserControlDataGrid)this.FindControl("EntityXAddressesInfoGrid");//Capturing Primary Flag
            bool bPrimaryFlag = false;
            if (objGridControl != null)
            {
                GridView gvData = (GridView)objGridControl.GridView;
                int iGridRowCount = gvData.Rows.Count;
                int iPrimaryColIndex = 0;
                int iUniqueIdIndex = 0;
                if (iGridRowCount > 1)
                {
                    TextBox txtPrimaryRow = (TextBox)this.FindControl("PrimaryRow");
                    for (int i = 0; i < gvData.Columns.Count - 1; i++)
                    {
                        if (gvData.Columns[i].HeaderText.ToLower() == "primary address")
                        {
                            iPrimaryColIndex = i;
                            continue;
                        }
                        else if (gvData.Columns[i].HeaderText.ToLower() == "uniqueid_hidden")
                        {
                            iUniqueIdIndex = i;
                            continue;
                        }
                    }
                    for (int i = 0; i < iGridRowCount - 1; i++)
                    {
                        string sValue = gvData.Rows[i].Cells[iPrimaryColIndex].Text.ToLower();
                        if (sValue == "true")
                        {
                            bPrimaryFlag = true;
                            if (txtPrimaryRow != null)
                                txtPrimaryRow.Text = gvData.Rows[i].Cells[iUniqueIdIndex].Text;
                            break;
                        }
                    }
                    if (!bPrimaryFlag)
                    {
                        if (txtPrimaryRow != null)
                            txtPrimaryRow.Text = "";
                    }

                }
                else
                {
                    bPrimaryFlag = false;
                    TextBox txtPrimaryRow = (TextBox)this.FindControl("PrimaryRow");
                    if (txtPrimaryRow != null)
                        txtPrimaryRow.Text = "";
                }
            }
            TextBox txtPrimaryFlag = (TextBox)this.FindControl("PrimaryFlag");
            if (txtPrimaryFlag != null)
                txtPrimaryFlag.Text = bPrimaryFlag.ToString();
        }
		  private void BillReviewSection(bool blnValue)
        {
            Control oControl = this.FindControl("div_labelHBR");
            if (oControl != null)
            {
                oControl.Visible = blnValue;
            }
            oControl = this.FindControl("div_HBRAmt");
            if (oControl != null)
            {
                oControl.Visible = blnValue;
            }
            oControl = this.FindControl("div_HBRFeeLines");
            if (oControl != null)
            {
                oControl.Visible = blnValue;
            }
            oControl = this.FindControl("div_labelMBR");
            if (oControl != null)
            {
                oControl.Visible = blnValue;
            }
            oControl = this.FindControl("div_MBRAmt");
            if (oControl != null)
            {
                oControl.Visible = blnValue;
            }
            oControl = this.FindControl("div_MBRFeeLines");
            if (oControl != null)
            {
                oControl.Visible = blnValue;
            }
        }
//skhare7 RMSC merge
    }
}
