﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.FDM
{
    public partial class CmXTreatmentPlnList : FDMBasePage
    {
        public IEnumerable result = null;
        public XElement rootElement = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {

                LoadInitialPage();
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/CmXTreatmentPlnList/CmXTreatmentPln")
                         let xClaim = (int)Convert.ToInt64(c.Element("CmtpRowId").Value)
                         orderby xClaim descending
                         select c;
                if (rootElement.XPathSelectElement("//CmXTreatmentPlnList/CasemgtRowId") != null)
                {
                    casemgtrowid.Text = rootElement.XPathSelectElement("//CmXTreatmentPlnList/CasemgtRowId").Value;
                }
                
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }

        protected void NavigateListDelete(object sender, EventArgs e)
        {
            
            try
            {
                base.NavigateListDelete(sender, e);
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/CmXTreatmentPlnList/CmXTreatmentPln")
                         let xClaim = (int)Convert.ToInt64(c.Element("CmtpRowId").Value)
                         orderby xClaim descending
                         select c;
                if (rootElement.XPathSelectElement("//CmXTreatmentPlnList/CasemgtRowId") != null)
                {
                    casemgtrowid.Text = rootElement.XPathSelectElement("//CmXTreatmentPlnList/CasemgtRowId").Value;
                }
                
                //LoadInitialPage();

            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

    }
}
