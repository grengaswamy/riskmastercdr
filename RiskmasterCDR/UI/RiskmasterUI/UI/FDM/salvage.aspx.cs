﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.FDM
{
    public partial class Salvage : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();

        }
        protected override void OnUpdateForm(System.Xml.Linq.XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            HtmlContainerControl oDivControl = (HtmlContainerControl)this.FindControl("FORMTABsalvagehistory");
            if (oDivControl != null)
            {
                oDivControl.Attributes["class"] = "gridtopborder";
            }

            oDivControl = (HtmlContainerControl)this.FindControl("div_SalvageHistoryGrid");
            if (oDivControl != null)
            {
                oDivControl.Attributes["class"] = "completerow";
            }
        }
    }
}