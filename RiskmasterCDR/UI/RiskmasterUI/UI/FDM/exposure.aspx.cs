﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.FDM
{
    public partial class Exposure : NonFDMBasePageCWS
    {
        private const string m_FUNCTION_CALL_GET_RATE = "EnhancePolicyAdaptor.GetRate";

        private const string m_sCodeLookup = "codelookup";
        private const string m_sMultiCode = "multicode";
        private const string m_sUserName = "rmsyslookup";

        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument objRateDetails = null;
            string sreturnValue = "";
            if (!IsPostBack)
            {
                // Read the Values from Querystring
                int iPolicyId = 0;
                if (AppHelper.GetQueryStringValue("policyid") != "")
                    iPolicyId = Int32.Parse(AppHelper.GetQueryStringValue("policyid"));

                int iExposureId = 0;
                if (AppHelper.GetQueryStringValue("selectedid") != "")
                    iExposureId = Int32.Parse(AppHelper.GetQueryStringValue("selectedid"));

                string sSessionId = string.Empty;
                if (AppHelper.GetQueryStringValue("sessionid") != "")
                    sSessionId = AppHelper.GetQueryStringValue("sessionid");

                int iTransId = 0;
                if (AppHelper.GetQueryStringValue("selectedtransactionid") != "")
                    iTransId = Int32.Parse(AppHelper.GetQueryStringValue("selectedtransactionid"));

                string sEffectiveDate = AppHelper.GetQueryStringValue("effectivedate");
                string sExpirationDate = AppHelper.GetQueryStringValue("expirationdate");

                int iStateId = 0;
                if (AppHelper.GetQueryStringValue("state") != "")
                    iStateId = Int32.Parse(AppHelper.GetQueryStringValue("state"));

                /// These Controls are used when Set Coverage is Called. 
                // These controls should be different then the Controls meant for display or for Coverages.
                // Like when a Quote is saved though the DB has a Transaction Id Saved,but there will not be any record for it.
                TextBox txtPolicyId = (TextBox)this.FindControl("PolId");
                txtPolicyId.Text = iPolicyId.ToString();
                TextBox txtTransId = (TextBox)this.FindControl("TransId");
                txtTransId.Text = iTransId.ToString();
                TextBox txtEffectiveDate = (TextBox)this.FindControl("EffDate");
                txtEffectiveDate.Text = sEffectiveDate;
                TextBox txtExpirationDate = (TextBox)this.FindControl("ExpDate");
                txtExpirationDate.Text = sExpirationDate;

                TextBox txtGridName = (TextBox)this.FindControl("gridname");
                txtGridName.Text = AppHelper.GetQueryStringValue("gridname");
                TextBox txtmode = (TextBox)this.FindControl("mode");
                txtmode.Text = AppHelper.GetQueryStringValue("mode");
                TextBox txtselectedrowposition = (TextBox)this.FindControl("selectedrowposition");
                txtselectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                TextBox txtExposureId = (TextBox)this.FindControl("selectedid");
                txtExposureId.Text = iExposureId.ToString();


                XmlTemplate = GetLoadMessageTemplate(iExposureId, sSessionId, iPolicyId, iTransId, sEffectiveDate, sExpirationDate, iStateId);
                bReturnStatus = CallCWS("EnhancePolicyAdaptor.GetExposure", XmlTemplate, out sreturnValue, false, true);
            }
            else
            {
                // Get the Function Name
                TextBox txtFunctionToCall = (TextBox)this.FindControl("FunctionToCall");

                if (txtFunctionToCall.Text == m_FUNCTION_CALL_GET_RATE)
                {
                    XmlTemplate = null;
                    bReturnStatus = CallCWS(m_FUNCTION_CALL_GET_RATE, XmlTemplate, out sreturnValue, true, false);

                    objRateDetails = new XmlDocument();
                    objRateDetails.LoadXml(sreturnValue);

                    // Set the Value of Controls
                    // Rate Value
                    TextBox txtRate = (TextBox)this.FindControl("Rate");
                    txtRate.Text = objRateDetails.SelectSingleNode("//RateDetails/Rate").InnerText;
                    // Base Rate
                    TextBox txtBaseRate = (TextBox)this.FindControl("BaseRate");
                    txtBaseRate.Text = objRateDetails.SelectSingleNode("//RateDetails/BaseRate").InnerText;
                    // Premium Adjustment
                    MultiCurrencyCustomControl.CurrencyTextbox txtPremiumAdjustment = (MultiCurrencyCustomControl.CurrencyTextbox)this.FindControl("PremiumAdjustment");
                    if (txtPremiumAdjustment != null)
                    {
                        txtPremiumAdjustment.AmountInString = (objRateDetails.SelectSingleNode("//RateDetails/PremAdj").InnerText);
                    }
                    // Pro Rata Annual Premium
                    MultiCurrencyCustomControl.CurrencyTextbox txtProRataAnnualPremium = (MultiCurrencyCustomControl.CurrencyTextbox)this.FindControl("ProRataAnnualPremium");
                    if (txtProRataAnnualPremium != null)
                    {
                        txtProRataAnnualPremium.AmountInString = (objRateDetails.SelectSingleNode("//RateDetails/ProRataAnnualPrem").InnerText);
                    }
                    // Full Annual Premium
                    MultiCurrencyCustomControl.CurrencyTextbox txtFullAnnualPremium = (MultiCurrencyCustomControl.CurrencyTextbox)this.FindControl("FullAnnualPremium");
                    if (txtFullAnnualPremium != null)
                    {
                        txtFullAnnualPremium.AmountInString = (objRateDetails.SelectSingleNode("//RateDetails/FullAnnualPrem").InnerText);
                    }

                    // Date Amended
                    TextBox txtDateAmended = (TextBox)this.FindControl("DateAmended");
                    txtDateAmended.Text = Request.Form["DateAmended"];

                }
            }


        }

        protected void btnExposureOk_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = "";
            TextBox txtSessionId;
            bReturnStatus = CallCWS("EnhancePolicyAdaptor.SetExposure", XmlTemplate, out sreturnValue, true, false);
            if (bReturnStatus )
            {
                
                XmlDoc.LoadXml(sreturnValue);
                // npadhy Start MITS 16357 Check if Error is returned from the Business, then We will not get the SessionId
                if (ErrorHelper.IsCWSCallSuccess(sreturnValue))
                {
                    // Set the Session Id
                    txtSessionId = (TextBox)this.FindControl("SessionId");
                    txtSessionId.Text = XmlDoc.SelectSingleNode("//Document/SetExposure/SessionId").InnerText;

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ExposureOkClick", "<script>fnRefreshParentExposureOk();</script>");
                }
                // npadhy End MITS 16357 Check if Error is returned from the Business, then We will not get the SessionId
            }
        }

        private XElement GetLoadMessageTemplate(int iExposureId, string sSessionId, int iPolicyId, int iTransId, string sEffectiveDate, string sExpirationDate, int iStateId)
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.GetExposure</Function></Call><Document><Exposure><ExposureId>");
            sXml = sXml.Append(iExposureId);
            sXml = sXml.Append("</ExposureId><SessionId>");
            sXml = sXml.Append(sSessionId);
            sXml = sXml.Append("</SessionId><PolId >");
            sXml = sXml.Append(iPolicyId);
            sXml = sXml.Append("</PolId><TransId>");
            sXml = sXml.Append(iTransId);
            sXml = sXml.Append("</TransId><ExpirationDate>");
            sXml = sXml.Append(sExpirationDate);
            sXml = sXml.Append("</ExpirationDate><EffectiveDate>");
            sXml = sXml.Append(sEffectiveDate);
            sXml = sXml.Append("</EffectiveDate><State>");
            sXml = sXml.Append(iStateId);
            sXml = sXml.Append("</State></Exposure></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        public override void ModifyControl(XElement Xelement)
        {
            IEnumerable<XElement> suppList = Xelement.XPathSelectElements("./Exposure/SuppView/group/displaycolumn/*");

            foreach (XElement xSupp in suppList)
            {
                string sRef = xSupp.Attribute("ref").Value;
                string sControlName = xSupp.Attribute("name").Value;
                string sOnChange = xSupp.Attribute("onchange").Value;


                Control oCtrl = (Control)this.FindControl(sControlName);
                if (oCtrl != null)
                {
                    if (oCtrl is WebControl)
                    {
                        AssignAttributes(oCtrl, "", xSupp);
                    }
                    else if (oCtrl is CodeLookUp)
                    {
                        AssignAttributes(oCtrl, m_sCodeLookup, xSupp);
                    }

                    else if (oCtrl is MultiCodeLookUp)
                    {
                        AssignAttributes(oCtrl, m_sMultiCode, xSupp);
                    }
                    else if (oCtrl is SystemUsers)
                    {
                        AssignAttributes(oCtrl, m_sUserName, xSupp);
                    }
                }

            }

        }


        private void AssignAttributes(Control p_oCtrl, string p_sExtender, XElement p_xSupp)
        {
            string sRef = p_xSupp.Attribute("ref").Value;
            string sControlName = p_xSupp.Attribute("name").Value;
            string sOnChange = p_xSupp.Attribute("onchange").Value;
            string sType = p_xSupp.Attribute("type").Value;
            string sControlId = string.Empty;
            WebControl oWebctrl;
            Control oCtrl;
            if (p_sExtender == "")
            {
                oWebctrl = (WebControl)p_oCtrl;
                oWebctrl.Attributes["RMXRef"] = sRef;
                oWebctrl.Attributes["onchange"] = sOnChange;

                if (sType == "eidlookup")
                {
                    sControlName = p_oCtrl.ID + "_cid";
                    oCtrl = (Control)this.FindControl(sControlName);
                    {
                        oWebctrl = (WebControl)oCtrl;
                        if (oWebctrl != null)
                        {
                            oWebctrl.Attributes["RMXRef"] = sRef + "/@codeid";
                        }
                    }
                }
                if (sType == "entitylist")
                {
                    sControlName = p_oCtrl.ID + "_lst";
                    oCtrl = (Control)this.FindControl(sControlName);
                    {
                        oWebctrl = (WebControl)oCtrl;
                        if (oWebctrl != null)
                        {
                            oWebctrl.Attributes["RMXRef"] = sRef + "/@codeid";
                        }
                    }
                }
            }
            else
            {
                oWebctrl = (WebControl)p_oCtrl.FindControl(p_sExtender);
                if (oWebctrl != null)
                {
                    oWebctrl.Attributes["RMXRef"] = sRef;
                    oWebctrl.Attributes["onchange"] = sOnChange;
                    if (p_sExtender == m_sMultiCode)
                    {
                        // Add the Item SetRef attribute with the Listbox
                        if (oWebctrl.Attributes["ItemSetRef"] == null)
                            oWebctrl.Attributes.Add("ItemSetRef", sRef);
                        else
                            oWebctrl.Attributes["ItemSetRef"] = sRef;
                    }
                }

                sControlId = GetControlName(p_sExtender);

                if (sControlId != string.Empty)
                {
                    oWebctrl = (WebControl)p_oCtrl.FindControl(sControlId);
                    if (oWebctrl != null)
                    {
                        oWebctrl.Attributes["RMXRef"] = sRef + "/@codeid";
                    }
                }
                else
                {
                    throw new Exception(p_sExtender + " Type of Supplemental field is not implemented in coverages");
                }
            }
        }

        private string GetControlName(string p_sExtender)
        {
            string sControlName = string.Empty;
            switch (p_sExtender)
            {
                case m_sMultiCode:
                    sControlName = p_sExtender + "_lst";
                    break;
                case m_sUserName:
                case m_sCodeLookup:
                    sControlName = p_sExtender + "_cid";
                    break;
                default:
                    sControlName = "";
                    break;
            }
            return sControlName;
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            TextBox txtFunctionToCall = (TextBox)this.FindControl("FunctionToCall");

            if (txtFunctionToCall.Text == m_FUNCTION_CALL_GET_RATE)
            {
                XElement xmlExposure = Xelement.XPathSelectElement("./Document/Exposure");
                XElement xmlTarget = new XElement("State");
                int iStateId = 0;
                if (AppHelper.GetQueryStringValue("state") != "")
                    iStateId = Int32.Parse(AppHelper.GetQueryStringValue("state"));

                xmlTarget.Value = iStateId.ToString();
                if (xmlExposure != null)
                    xmlExposure.Add(xmlTarget);
            }
        }
    }
}
