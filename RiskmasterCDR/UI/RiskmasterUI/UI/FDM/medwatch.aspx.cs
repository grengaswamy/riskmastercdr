﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.FDM
{
    public partial class Medwatch : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
        }
        // Change for MITS No. 12273
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);
            
            XElement orepentityid = oMessageElement.XPathSelectElement("//EventMedwatch/RptdByEid");

            if (orepentityid != null)
            {
                if (this.Page.FindControl("initialRepEntityId") != null)
                    ((TextBox)this.Page.FindControl("initialRepEntityId")).Text = orepentityid.Attribute("codeid").Value;  
            }
        }
        
    }
}
