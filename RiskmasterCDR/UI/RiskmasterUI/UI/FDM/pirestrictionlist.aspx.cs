﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.FDM
{
    public partial class PiRestrictionList : FDMBasePage
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadInitialPage();
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/PiXRestrictList/PiXRestrict")
                         let xClaim = (int)Convert.ToInt64(c.Element("PiRestrictRowId").Value)
                         orderby xClaim
                         select c;               

                if (rootElement.XPathSelectElement("//PiXRestrict/PiRowId") != null)
                {
                   pirowid.Text = rootElement.XPathSelectElement("//PiXRestrict/PiRowId").Value;                   
                }
                //ijha:24987-OSHA 300 rules
                if(rootElement.XPathSelectElement("//SysExData/EventId") != null)
                {
                   eventid.Text = rootElement.XPathSelectElement("//SysExData/EventId").Value;
                }
                //rsushilaggar MITS 37986
                CalculateTotalOshaDays(result);
                
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }

        protected void NavigateListDelete(object sender, EventArgs e)
        {
            try
            {
                base.NavigateListDelete(sender, e);

                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/PiXRestrictList/PiXRestrict")
                         let xClaim = (int)Convert.ToInt64(c.Element("PiRestrictRowId").Value)
                         orderby xClaim
                         select c;
                if (rootElement.XPathSelectElement("//PiXRestrict/PiRowId") != null)
                {
                    pirowid.Text = rootElement.XPathSelectElement("//PiXRestrict/PiRowId").Value;
                }

                //LoadInitialPage();
                //rsushilaggar MITS 37986
                CalculateTotalOshaDays(result);
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }
        /// <summary>
        /// rsushilaggar MITS 37986
        /// </summary>
        /// <param name="p_xElement"></param>
        private void CalculateTotalOshaDays(IEnumerable p_xElement)
        {
            string sStartDate = string.Empty;
            string sStartOSHACount = string.Empty;
            string sEnddate = string.Empty;
            int iTotalDays = 0;
            string sEventDateFromDB = DateOfEvent.Text;
            bool bHavePreEventDates = false;
            int iRestrictedDays = 0;
            foreach (XElement item in p_xElement)
            {
                sStartDate = item.Element("DateFirstRestrct").Value;
                sStartOSHACount = sStartDate;

                if (item.Element("DateLastRestrct").Value == "")
                    sEnddate = System.DateTime.Today.Date.ToString();
                else
                    sEnddate = Conversion.ToDate(item.Element("DateLastRestrct").Value).ToShortDateString();

                if (Conversion.ToDate(sStartOSHACount) < Conversion.ToDate(sEventDateFromDB))
                {
                    bHavePreEventDates = true;
                    sStartOSHACount = Convert.ToDateTime(sEventDateFromDB).ToShortDateString();
                }
                else
                    sStartOSHACount = Conversion.ToDate(sStartOSHACount).ToShortDateString();

                if (Convert.ToDateTime(sStartOSHACount) <= Convert.ToDateTime(sEnddate))
                {
                    TimeSpan objTimeSpan = Convert.ToDateTime(sEnddate).Subtract(Convert.ToDateTime(sStartOSHACount));
                    iTotalDays = iTotalDays + objTimeSpan.Days + 1;
                }

                iRestrictedDays = iRestrictedDays+Convert.ToInt32(item.Element("Duration").Value);
            }
            if (Conversion.ToDate(sEventDateFromDB) >= Conversion.ToDate("20020101"))
            {
                if (bHavePreEventDates)
                    lblTotal.Text = "Total Restricted Days: "+ iRestrictedDays+  ", OSHA 300: " + iTotalDays.ToString() + ", First Dates before Event Date were adjusted for calculation.";
                else
                    lblTotal.Text = "Total Restricted Days: " + iRestrictedDays + ", OSHA 300: " + iTotalDays.ToString();
            }
            else
                lblTotal.Text = "OSHA 300 does not apply, Event is before OSHA 300 rules start.";


        }
    }
}
