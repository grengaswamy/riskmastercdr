<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cmxvocrehab.aspx.cs" Inherits="Riskmaster.UI.FDM.Cmxvocrehab"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Vocational Rehabilitation</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="attach()" src="Images/attach.gif"
                        width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                        onmouseover="this.src='Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/attach.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="FilteredDiary()" src="Images/filtereddiary.gif"
                        width="28" height="28" border="0" id="filtereddiary" alternatetext="View Record Diaries"
                        onmouseover="this.src='Images/filtereddiary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/filtereddiary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Diary()" src="Images/diary.gif" width="28"
                        height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='Images/diary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/diary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Vocational Rehabilitation" /><asp:label
            id="formsubtitle" runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>

    <script language="JavaScript" xmlns="">
    function onTextChange()
    {
      var iDiff = document.forms[0].estcostofplan.value - document.forms[0].actcostofplan.value;
      if(iDiff > 0)
        document.forms[0].difference.value = iDiff;
      else
        document.forms[0].difference.value = 0; 
      setDataChanged(true);
    }
    </script>

    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSVocRehab" id="TABSVocRehab">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="VocRehab"
                id="LINKTABSVocRehab"><span style="text-decoration: none">Vocational Rehabilitation</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABVocRehab" id="FORMTABVocRehab">
        <asp:textbox style="display: none" runat="server" id="cmvrrowid" rmxref="/Instance/CmXVocrehab/CmvrRowId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="casemgtrowid"
                rmxref="/Instance/CmXVocrehab/CasemgtRowId" rmxtype="id" /><div runat="server" class="half"
                    id="div_rehabtype" xmlns="">
                    <span class="label">Rehab Type</span><span class="formw"><asp:textbox runat="server"
                        onchange="lookupTextChanged(this);" id="rehabtype" rmxref="/Instance/CmXVocrehab/RehabTypeCode"
                        rmxtype="code" cancelledvalue="" tabindex="1" onblur="codeLostFocus(this.id);" /><asp:button
                            class="CodeLookupControl" runat="Server" id="rehabtypebtn" onclientclick="return selectCode('REHAB_TYPE','rehabtype');"
                            tabindex="2" /><asp:textbox style="display: none" runat="server" id="rehabtype_cid"
                                rmxref="/Instance/CmXVocrehab/RehabTypeCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_startdate" xmlns="">
            <span class="label">Start Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="startdate" rmxref="/Instance/CmXVocrehab/StartDate"
                rmxtype="date" tabindex="12" /><cc1:CalendarExtender runat="server" ID="startdate_ajax"
                    TargetControlID="startdate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_rehabstatus" xmlns="">
            <span class="label">Rehab Status</span><span class="formw"><asp:textbox runat="server"
                onchange="lookupTextChanged(this);" id="rehabstatus" rmxref="/Instance/CmXVocrehab/RehabStatusCode"
                rmxtype="code" cancelledvalue="" tabindex="3" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="rehabstatusbtn" onclientclick="return selectCode('REHAB_STATUS','rehabstatus');"
                    tabindex="4" /><asp:textbox style="display: none" runat="server" id="rehabstatus_cid"
                        rmxref="/Instance/CmXVocrehab/RehabStatusCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_enddate" xmlns="">
            <span class="label">End Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="enddate" rmxref="/Instance/CmXVocrehab/EndDate"
                rmxtype="date" tabindex="14" /><cc1:CalendarExtender runat="server" ID="enddate_ajax"
                    TargetControlID="enddate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_providergrouplist" xmlns="">
            <span class="label">Provider Group</span><span class="formw"><asp:textbox runat="server"
                onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="providergrouplist"
                rmxref="/Instance/CmXVocrehab/ProviderEid" rmxtype="eidlookup" cancelledvalue=""
                tabindex="5" /><input type="button" class="button" value="..." name="providergrouplistbtn"
                    tabindex="6" onclick="lookupData('providergrouplist','0',4,'providergrouplist',2)" /><asp:textbox
                        style="display: none" runat="server" id="providergrouplist_cid" rmxref="/Instance/CmXVocrehab/ProviderEid/@codeid"
                        cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_estcostofplan" xmlns="">
            <span class="label">Est. Cost Of Plan</span><span class="formw"><asp:textbox runat="server"
                onblur="numLostFocus(this);" id="estcostofplan" rmxref="/Instance/CmXVocrehab/EstCostOfPlan"
                rmxtype="numeric" tabindex="16" onchange="javascript:onTextChange();" /></span></div>
        <div runat="server" class="half" id="div_plandefinition" xmlns="">
            <span class="label">Plan Definition</span><span class="formw"><asp:textbox runat="Server"
                id="plandefinition" rmxref="/Instance/CmXVocrehab/PlanDefinition" rmxtype="memo"
                readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                tabindex="7" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                    value="..." name="plandefinitionbtnMemo" tabindex="8" id="plandefinitionbtnMemo"
                    onclick="EditMemo('plandefinition','')" /></span></div>
        <div runat="server" class="half" id="div_actcostofplan" xmlns="">
            <span class="label">Act. Cost Of Plan</span><span class="formw"><asp:textbox runat="server"
                onblur="numLostFocus(this);" id="actcostofplan" rmxref="/Instance/CmXVocrehab/ActCostOfPlan"
                rmxtype="numeric" tabindex="17" onchange="javascript:onTextChange();" /></span></div>
        <div runat="server" class="half" id="div_occproposed" xmlns="">
            <span class="label">Occupation Proposed</span><span class="formw"><asp:textbox runat="server"
                onchange="setDataChanged(true);" id="occproposed" rmxref="/Instance/CmXVocrehab/OccProposed"
                rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_difference" xmlns="">
            <span class="label">Difference</span><span class="formw"><asp:textbox runat="Server"
                rmxref="/Instance/CmXVocrehab/Difference" id="difference" tabindex="18" style="background-color: silver;"
                readonly="true" /></span></div>
        <div runat="server" class="half" id="div_outcomes" xmlns="">
            <span class="label">Outcomes</span><span class="formw"><asp:textbox runat="server"
                onchange="lookupTextChanged(this);" id="outcomes" rmxref="/Instance/CmXVocrehab/OutcomesCode"
                rmxtype="code" cancelledvalue="" tabindex="10" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="outcomesbtn" onclientclick="return selectCode('OUTCOME','outcomes');"
                    tabindex="11" /><asp:textbox style="display: none" runat="server" id="outcomes_cid"
                        rmxref="/Instance/CmXVocrehab/OutcomesCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_buyoutcost" xmlns="">
            <span class="label">Cost to Buy Out</span><span class="formw"><asp:textbox runat="server"
                onblur="numLostFocus(this);" id="buyoutcost" rmxref="/Instance/CmXVocrehab/BuyOutCost"
                rmxtype="numeric" tabindex="19" onchange="setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_labelonly1" style="display: none;" xmlns="">
            <span class="label">
                <asp:label runat="server" id="labelonly1" text="" /></span></div>
        <div runat="server" class="half" id="div_staterefund" xmlns="">
            <span class="label">State Refund</span><span class="formw"><asp:textbox runat="server"
                onblur="numLostFocus(this);" id="staterefund" rmxref="/Instance/CmXVocrehab/StateRefund"
                rmxtype="numeric" tabindex="20" onchange="setDataChanged(true);" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup">
        <asp:textbox style="display: none" runat="server" id="supp_cmvr_row_id" rmxref="/Instance/*/Supplementals/CMVR_ROW_ID"
            rmxtype="id" /></div>
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
            <asp:button class="button" runat="Server" id="btnBack" text="Back to Claim" onclientclick="return XFormHandler('','1','back','');" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="CmXVocrehab" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" name="SysSerializationConfig" text="&lt;CmXVocrehab&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/CmXVocrehab&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                rmxtype="hidden" text="CmXVocrehab" /><asp:textbox style="display: none" runat="server"
                                    id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId" rmxtype="hidden"
                                    text="" /><asp:textbox style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
                                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormId"
                                            rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden" text="" /><asp:textbox
                                                style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                                rmxtype="hidden" text="11150" /><asp:textbox style="display: none" runat="server"
                                                    id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType" rmxtype="hidden"
                                                    text="" /><asp:textbox style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
                                                        rmxtype="hidden" text="cmvrrowid" /><asp:textbox style="display: none" runat="server"
                                                            id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden"
                                                            text="casemgtrowid" /><asp:textbox style="display: none" runat="server" id="SysFormPForm"
                                                                rmxref="Instance/UI/FormVariables/SysFormPForm" rmxtype="hidden" text="" /><asp:textbox
                                                                    style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                                                    rmxtype="hidden" text="eventnumber,claimnumber" /><asp:textbox style="display: none"
                                                                        runat="server" name="formname" value="CmXVocrehab" /><asp:textbox style="display: none"
                                                                            runat="server" name="SysRequired" value="" /><asp:textbox style="display: none" runat="server"
                                                                                name="SysFocusFields" value="rehabtype|" /><input type="hidden" id="hdSaveButtonClicked" /><asp:textbox
                                                                                    runat="server" id="SysInvisible" style="display: none" /><asp:textbox runat="server"
                                                                                        id="SysLookupClass" style="display: none" /><asp:textbox runat="server" id="SysLookupRecordId"
                                                                                            style="display: none" /><asp:textbox runat="server" id="SysLookupAttachNodePath"
                                                                                                style="display: none" /><asp:textbox runat="server" id="SysLookupResultConfig" style="display: none" /><input
                                                                                                    type="hidden" value="rmx-widget-handle-2" id="SysWindowId" /></form>
</body>
</html>
