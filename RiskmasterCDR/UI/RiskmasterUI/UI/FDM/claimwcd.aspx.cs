﻿namespace Riskmaster.UI.FDM
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using MultiCurrencyCustomControl;
    using Riskmaster.AppHelpers;
    using Riskmaster.BusinessHelpers;
    using Riskmaster.UI.Shared.Controls;

    /// <summary>
    /// Claim WC Dynamic Class.
    /// </summary>
    public partial class Claimwcd : FDMBasePage
    {
        /// <summary>
        /// Is first tab loaded. 
        /// </summary>
        private bool isFirstTabLoaded = false;

        /// <summary>
        /// Control collection for required tab.
        /// </summary>
        private Control controlRequired = null;

        /// <summary>
        /// Required tab list.
        /// </summary>
        private List<string> requiredTabs = null;

        /// <summary>
        /// Modifies the XML.
        /// </summary>
        /// <param name="xmlElement">The xelement.</param>
        public override void ModifyXml(ref XElement xmlElement)
        {
            string viewID = "0";
            XElement objPiEid = xmlElement.XPathSelectElement("./Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/PrimaryPiEmployee/PiEid");
            XElement objEntityEid = xmlElement.XPathSelectElement("./Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/PrimaryPiEmployee/PiEntity/EntityId");
            if (objPiEid != null && objEntityEid != null)
            {
                if (objPiEid.Attribute("codeid").Value == "0")
                {
                    objPiEid.SetAttributeValue("codeid", objEntityEid.Value);
                }
            }

            if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("ViewId")))
            {
                viewID = AppHelper.ReadCookieValue("ViewId");
            }

            if (viewID != "0")
            {
                Control objhdnPersistInSession = this.FindControl("hdnPersistInSession");
                if (objhdnPersistInSession != null)
                {
                    string toBePersistInSession = ((TextBox)objhdnPersistInSession).Text;
                    if (toBePersistInSession != string.Empty)
                    {
                        AppendToXmlIn("claimwc", toBePersistInSession, xmlElement);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params.Get("__EVENTTARGET") != null)
            {
                this.controlRequired = this.Form.FindControl(Request.Params.Get("__EVENTTARGET"));
            }
            
            FDMPageLoad();
        }

        /// <summary>
        /// Bind data from CWS response to ASP.NET server controls
        /// </summary>
        /// <param name="messageElement">Response message.</param>
        /// <param name="controls">Control collection.</param>
        protected override void BindData2Control(XElement messageElement, ICollection controls)
        {
            string rmxReference = string.Empty;
            if (this.Form.FindControl("txtDynamicRequiredTabs") == null)
            {
                base.BindData2Control(messageElement, controls);
            }
            else
            {
                if (this.controlRequired != null)
                {
                    controls = this.controlRequired.Parent.Controls;
                    this.controlRequired = null;
                }
                else
                {
                    if (object.ReferenceEquals(this.requiredTabs, null) && !object.ReferenceEquals(this.Form.FindControl("txtRequiredTabs"), null))
                    {
                        this.requiredTabs = (this.Form.FindControl("txtRequiredTabs") as TextBox).Text.ToString().Split(',').ToList(); ;
                    }
                    
                    if (string.IsNullOrEmpty(((TextBox)this.Form.FindControl("txtDynamicRequiredTabs")).Text))
                    {
                        string parentTabs = string.Empty;
                        foreach (string field in this.GetRequiredFieldsTab(((TextBox)this.Form.FindControl("SysRequired")).Text))
                        {
                            string requiredField = field;
                            string parentTab = string.Empty;

                            if (requiredField.Contains("_codelookup_cid"))
                            {
                                requiredField = requiredField.Replace("_codelookup_cid", "$codelookup_cid");
                            }

                            if (this.Form.FindControl(requiredField) is WebControl)
                            {
                                rmxReference = ((WebControl)this.Form.FindControl(requiredField)).Attributes["RMXRef"];

                                if (string.IsNullOrEmpty(rmxReference))
                                {
                                    continue;
                                }

                                rmxReference = GetResponseRefPath(rmxReference);
                                if (rmxReference.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = rmxReference.Split(g_sXPathDelimiter.ToCharArray());
                                    rmxReference = lstRMXRef[0];
                                }

                                string requiredFieldValue = GetReturnValue(messageElement, rmxReference);

                                if (string.IsNullOrEmpty(requiredFieldValue) || (requiredField.Contains("_cid") && requiredFieldValue.Equals("0")))
                                {
                                    parentTab = this.GetTab(requiredField);
                                    if (!parentTabs.Contains(parentTab))
                                    {
                                        parentTabs += string.Format("{0}|", parentTab);
                                    }
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(parentTabs))
                        {
                            ((TextBox)this.Form.FindControl("txtDynamicRequiredTabs")).Text = parentTabs.TrimEnd('|');
                        }
                    }
                }

                foreach (Control ctrl in controls)
                {
                    if (ctrl is WebControl)
                    {
                        if (m_sFormReadOnly == "Enable")
                        {
                            if (ctrl.ClientID == ctrl.ID)
                            {
                                if (((WebControl)ctrl).Attributes["rmxtype"] != "id" && ((WebControl)ctrl).Attributes["rmxtype"] != "hidden" && ((WebControl)ctrl).Attributes["rmxtype"] != "textml" && ((WebControl)ctrl).Attributes["rmxtype"] != "readonlymemo")
                                {
                                    DatabindingHelper.EnableControls(ctrl.ClientID, this.Page);
                                }
                            }
                            else
                            {
                                string controlId = ctrl.ClientID.Substring(0, (ctrl.ClientID.Length - ctrl.ID.Length - 1));
                                DatabindingHelper.EnableControls(controlId, this.Page);
                            }
                        }

                        rmxReference = ((WebControl)ctrl).Attributes["RMXRef"];
                        if (string.IsNullOrEmpty(rmxReference))
                        {
                            continue;
                        }

                        if (((WebControl)ctrl).Attributes["rmxignorevalue"] != null)
                        {
                            continue;
                        }

                        string rmxIgnoreGet = ((WebControl)ctrl).Attributes["rmxignoreget"];
                        if (!string.IsNullOrEmpty(rmxIgnoreGet))
                        {
                            if (rmxIgnoreGet == "true")
                            {
                                continue;
                            }
                        }

                        Type controlType = ctrl.GetType();
                        string type = controlType.ToString();
                        int index = type.LastIndexOf(".");
                        type = type.Substring(index + 1);
                        string itemSetRef;
                        string rmxType;
                        string valueCollection;
                        switch (type)
                        {
                            case "DropDownList":
                                rmxReference = GetResponseRefPath(rmxReference);
                                itemSetRef = ((WebControl)ctrl).Attributes["ItemSetRef"];
                                if (rmxReference.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = rmxReference.Split(g_sXPathDelimiter.ToCharArray());
                                    rmxReference = lstRMXRef[0];
                                }

                                if (!string.IsNullOrEmpty(itemSetRef))
                                {
                                    itemSetRef = GetResponseRefPath(itemSetRef);
                                    if (itemSetRef.IndexOf(g_sXPathDelimiter) > 0)
                                    {
                                        string[] lstRMXRef = itemSetRef.Split(g_sXPathDelimiter.ToCharArray());
                                        itemSetRef = lstRMXRef[0];
                                    }
                                    
                                    if (itemSetRef.EndsWith("option"))
                                    {
                                        itemSetRef = itemSetRef.Replace("/option", string.Empty);
                                    }

                                    valueCollection = GetReturnValue(messageElement, itemSetRef);
                                }
                                else
                                {
                                    valueCollection = string.Empty;
                                }

                                string selectedValue = GetReturnValue(messageElement, rmxReference);
                                if (!string.IsNullOrEmpty(valueCollection))
                                {
                                    DatabindingHelper.SetValue2Control((WebControl)ctrl, selectedValue, valueCollection);
                                }
                                else
                                {
                                    DatabindingHelper.SetValue2Control((WebControl)ctrl, selectedValue);
                                }

                                break;
                            case "CheckBox":
                                ((CheckBox)ctrl).InputAttributes.Add("onchange", "return setDataChanged(true);");
                                rmxReference = GetResponseRefPath(rmxReference);
                                if (rmxReference.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = rmxReference.Split(g_sXPathDelimiter.ToCharArray());
                                    rmxReference = lstRMXRef[0];
                                }

                                string value = GetReturnValue(messageElement, rmxReference);
                                DatabindingHelper.SetValue2Control((WebControl)ctrl, value);
                                break;
                            case "CurrencyTextbox":
                                rmxReference = GetResponseRefPath(rmxReference);
                                if (rmxReference.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = rmxReference.Split(g_sXPathDelimiter.ToCharArray());
                                    rmxReference = lstRMXRef[0];
                                }

                                sCurrencyMode = ((WebControl)ctrl).Attributes["CurrencyMode"];
                                XElement xmlNode = messageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/BaseCurrencyType");
                                if (xmlNode != null)
                                {
                                    if (string.IsNullOrEmpty(sCurrencyMode))
                                    {
                                        ((CurrencyTextbox)ctrl).SetProperties(xmlNode.Value, xmlNode.Value);
                                    }
                                    else
                                    {
                                        ((CurrencyTextbox)ctrl).SetProperties(null, xmlNode.Value);
                                    }
                                }

                                value = GetReturnValue(messageElement, rmxReference);
                                DatabindingHelper.SetValue2Control((CurrencyTextbox)ctrl, value);
                                break;
                            default:
                                rmxReference = GetResponseRefPath(rmxReference);
                                if (rmxReference.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = rmxReference.Split(g_sXPathDelimiter.ToCharArray());
                                    rmxReference = lstRMXRef[0];
                                }

                                value = GetReturnValue(messageElement, rmxReference);
                                DatabindingHelper.SetValue2Control((WebControl)ctrl, value);
                                break;
                        }

                        rmxType = ((WebControl)ctrl).Attributes["RMXType"];
                        if (m_sCustomizablememofields.Contains(rmxType))
                        {
                            DatabindingHelper.UpdateCustomizedControls(messageElement, rmxType, ctrl);
                        }

                        if (m_smsSecurity && !m_smsSecurityForSupp)
                        {
                            if (ctrl.ClientID == ctrl.ID)
                            {
                                if (((WebControl)ctrl).Attributes["rmxtype"] != "id" && ((WebControl)ctrl).Attributes["rmxtype"] != "hidden" && ((WebControl)ctrl).GetType().Name != "Button" && !ctrl.ClientID.StartsWith("supp_"))
                                {
                                    DatabindingHelper.DisableControls(ctrl.ClientID, this.Page);
                                }
                            }
                            else
                            {
                                if (!ctrl.ClientID.StartsWith("supp_"))
                                {
                                    string controlId = ctrl.ClientID.Substring(0, (ctrl.ClientID.Length - ctrl.ID.Length - 1));
                                    DatabindingHelper.DisableControls(controlId, this.Page);
                                }
                            }
                        }
                        else if (m_smsSecurity && m_smsSecurityForSupp)
                        {
                            if (ctrl.ClientID == ctrl.ID)
                            {
                                if (((WebControl)ctrl).Attributes["rmxtype"] != "id" && ((WebControl)ctrl).Attributes["rmxtype"] != "hidden" && ((WebControl)ctrl).GetType().Name != "Button")
                                {
                                    DatabindingHelper.DisableControls(ctrl.ClientID, this.Page);
                                }
                            }
                            else
                            {
                                string controlId = ctrl.ClientID.Substring(0, (ctrl.ClientID.Length - ctrl.ID.Length - 1));
                                DatabindingHelper.DisableControls(controlId, this.Page);
                            }
                        }
                        else if (!m_smsSecurity && m_smsSecurityForSupp)
                        {
                            if (ctrl.ClientID == ctrl.ID)
                            {
                                if (ctrl.ClientID.StartsWith("supp_"))
                                {
                                    DatabindingHelper.DisableControls(ctrl.ClientID, this.Page);
                                }
                            }
                            else
                            {
                                if (ctrl.ClientID.StartsWith("supp_"))
                                {
                                    string controlId = ctrl.ClientID.Substring(0, (ctrl.ClientID.Length - ctrl.ID.Length - 1));
                                    DatabindingHelper.DisableControls(controlId, this.Page);
                                }
                            }
                        }
                        else
                        {
                            if (m_sFormReadOnly == "Disable")
                            {
                                if (ctrl.ClientID == ctrl.ID)
                                {
                                    if (((WebControl)ctrl).Attributes["rmxtype"] != "id" && ((WebControl)ctrl).Attributes["rmxtype"] != "hidden")
                                    {
                                        DatabindingHelper.DisableControls(ctrl.ClientID, this.Page);
                                    }
                                }
                                else
                                {
                                    string controlId = ctrl.ClientID.Substring(0, (ctrl.ClientID.Length - ctrl.ID.Length - 1));
                                    DatabindingHelper.DisableControls(controlId, this.Page);
                                }
                            }
                        }
                    }
                    else if (ctrl is UserControlDataGrid)
                    {
                        if (m_smsSecurity)
                        {
                            ((UserControlDataGrid)ctrl).HideButtons = "New|Edit|Delete|Clone";
                        }
                        else if (m_sFormReadOnly == "Disable")
                        {
                            ((UserControlDataGrid)ctrl).HideButtons = "New|Edit|Delete|Clone";
                        }

                        XmlDocument objXml = new XmlDocument();
                        XmlReader objReader;
                        objReader = messageElement.CreateReader();
                        objXml.Load(objReader);
                        ((UserControlDataGrid)ctrl).BindData(objXml);
                    }
                    else
                    {
                        if (ctrl.ID != null)
                        {
                            if (ctrl.ClientID.StartsWith("FORMTAB") && this.isFirstTabLoaded)
                            {
                                if (!ctrl.ClientID.Contains("pvjurisgroup") 
                                    && (object.ReferenceEquals(this.requiredTabs, null) || !this.requiredTabs.Contains(ctrl.ClientID.Replace("FORMTAB", string.Empty))))
                                {
                                    continue;
                                }
                            }
                            
                            if (!string.IsNullOrEmpty(Request.Params.Get("__EVENTTARGET")) && Request.Params.Get("__EVENTTARGET").Equals(ctrl.ClientID))
                            {
                                ctrl.Visible = true;
                            }

                            if (ctrl.ID.IndexOf("_RMXTable") != -1)
                            {
                                if (!object.ReferenceEquals(this.requiredTabs, null) && this.requiredTabs.Contains(ctrl.ClientID.Replace("_RMXTable", string.Empty)))
                                {
                                    ctrl.Visible = true;
                                }

                                if (ctrl.Visible == false)
                                {
                                    continue;
                                }
                            }
                        }

                        if (ctrl.Controls.Count > 0)
                        {
                            this.BindData2Control(messageElement, ctrl.Controls);
                            if (ctrl.ClientID.StartsWith("FORMTAB") && !this.isFirstTabLoaded)
                            {
                                this.isFirstTabLoaded = true;
                            }
                        }
                    }

                    if (bDoFieldMarkReadOnly)
                    {
                        updateControls(ctrl, oFieldMark);
                    }
                    
                    if (bKillFieldMarkNodes)
                    {
                        HideFieldMarkNodes(ctrl, oKillNodesFieldMark);
                    }
                }
            }
        }

        /// <summary>
        /// Override base and generates claim letter if claim is found eligible
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event argument.</param>
        protected new void NavigateSave(object sender, EventArgs e)
        {
            try
            {
                int claimId = 0;
                string claimLetterTmplId = string.Empty;
                string claimNo = string.Empty;
                base.NavigateSave(sender, e);
                claimId = Convert.ToInt32(((TextBox)this.Form.FindControl("claimid")).Text);
                claimLetterTmplId = Convert.ToString(((TextBox)this.Form.FindControl("ClaimLetterTmplId")).Text);
                claimNo = Convert.ToString(((TextBox)this.Form.FindControl("claimnumber")).Text);
                if (claimLetterTmplId == "ACK" || claimLetterTmplId == "CL")
                {
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClaimLetterMerge", "DoClaimLetterMerge(" + claimId + ",'" + claimLetterTmplId + "','" + claimNo + "');", true);
                }

                claimLetterTmplId = string.Empty;
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Restores view-state information from a previous page request that was saved by the <see cref="M:System.Web.UI.Control.SaveViewState"/> method.
        /// </summary>
        /// <param name="savedState">An <see cref="T:System.Object"/> that represents the control state to be restored.</param>
        protected override void LoadViewState(object savedState)
        {
           base.LoadViewState(savedState);
           if (IsPostBack)
           {
               int stateId = 0;
               if (ViewState["SelectedStateId"] != null)
               {
                   stateId = Convert.ToInt32(ViewState["SelectedStateId"]);
               }

               Control ctrl = this.Form.FindControl("FORMTABpvjurisgroup");
               if (ctrl == null)
               {
                   ctrl = this.FindControl("pvjurisgroup");
                   if (ctrl != null)
                   {
                       ctrl = this.FindControl("Tabpvjurisgroup");
                   }
               }

               if (ctrl != null && stateId != 0)
               {
                PowerViewUpgradeBusinessHelper pn = new PowerViewUpgradeBusinessHelper();
                string response = pn.FetchJurisdictionalData(stateId);
                Control ctrl2 = ParseControl(response);
                ctrl.Controls.Clear();
                ctrl.Controls.Add(ctrl2);
               }
           }
        }

        /// <summary>
        /// This function handles all customizition done in legacy OnUpdateForm of FDM classes
        /// </summary>
        /// <param name="messageElement">Response message.</param>
        protected override void OnUpdateForm(XElement messageElement)
        {
            string viewID = "0";
            base.OnUpdateForm(messageElement);

            XElement useAdvancedClaim = messageElement.XPathSelectElement("//UseAdvancedClaim");
            Control policyControl = this.FindControl("div_primarypolicyid");
            if (useAdvancedClaim != null)
            {
                if (useAdvancedClaim.Value == "-1")
                {
                    if (policyControl != null)
                    {
                        policyControl.Visible = false;
                    }
                }
                else
                {
                    policyControl = this.FindControl("div_multipolicyid");
                    if (policyControl != null)
                    {
                        policyControl.Visible = false;
                    }
                }
            }

            Control control = this.FindControl("FORMTABemploymentinfo");
            if (control != null)
            {
                if (control.Visible)
                {
                    Control otherControl = this.FindControl("FORMTABemploymentinfowocasemgmt");
                    if (otherControl != null)
                    {
                        if (otherControl.Visible == false)
                        {
                            SetIgnoreValueAttribute(otherControl);
                            RemoveIgnoreValueAttribute(control);
                        }
                    }
                }
                else
                {
                    Control otherControl = this.FindControl("FORMTABemploymentinfowocasemgmt");
                    if (otherControl != null)
                    {
                        if (otherControl.Visible)
                        {
                            RemoveIgnoreValueAttribute(otherControl);
                            SetIgnoreValueAttribute(control);
                        }
                    }
                }
            
                if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("ViewId")))
                {
                    viewID = AppHelper.ReadCookieValue("ViewId");
                }
                
                if (viewID != "0")
                {
                    Control objhdnPersistInSession = this.FindControl("hdnPersistInSession");
                
                    if (objhdnPersistInSession != null)
                    {
                        string toBePersistInSession = ((TextBox)objhdnPersistInSession).Text;
                    
                        if (toBePersistInSession != string.Empty)
                        {
                            ExtractFromXmlOut("claimwc", toBePersistInSession, messageElement);
                        }
                    }
                }
            }

            control = this.FindControl("FORMTABemployeeeventdetail");
            if (control != null)
            {
                if (control.Visible)
                {
                    Control otherControl = this.FindControl("FORMTABmedicalinfo");
            
                    if (otherControl != null)
                    {
                        if (otherControl.Visible == false)
                        {
                            SetIgnoreValueAttribute(otherControl);
                            RemoveIgnoreValueAttribute(control);
                        }
                    }
                }
                else
                {
                    Control otherControl = this.FindControl("FORMTABmedicalinfo");
                    if (otherControl != null)
                    {
                        if (otherControl.Visible)
                        {
                            RemoveIgnoreValueAttribute(otherControl);
                            SetIgnoreValueAttribute(control);
                        }
                    }
                }
            }

            int policyId = 0;
            int enhPolicyId = 0;
            Button btnPolicyOpen = this.Form.FindControl("primarypolicyid_open") as Button;

            if (btnPolicyOpen != null)
            {
                XElement policyIdNode = messageElement.XPathSelectElement("//PrimaryPolicyId");
                if (policyIdNode != null)
                {
                    policyId = Convert.ToInt32(policyIdNode.Value);
                }

                XElement policyEnhIdNode = messageElement.XPathSelectElement("//PrimaryPolicyIdEnh");
                if (policyEnhIdNode != null)
                {
                    enhPolicyId = Convert.ToInt32(policyEnhIdNode.Value);
                }

                XElement useEnhPolFlagNode = messageElement.XPathSelectElement("//UseEnhPolFlag");
                if (useEnhPolFlagNode != null)
                {
                    if (useEnhPolFlagNode.Value == "0")
                    {
                        btnPolicyOpen.OnClientClick = "fnOpenPolicy('policy'); return false;";
                    }
                    else if (useEnhPolFlagNode.Value == "-1")
                    {
                        btnPolicyOpen.OnClientClick = "fnOpenPolicy('policyenhwc'); return false;";
                    }
                }
            }
           
            btnPolicyOpen = this.Form.FindControl("multipolicyid_open") as Button;
            if (btnPolicyOpen != null)
            {
                if (btnPolicyOpen != null)
                {
                    btnPolicyOpen.OnClientClick = "fnOpenPolicy('policy'); return false;";
                }
            }

            Control singleRow = this.Form.FindControl("div_singlerow3");
            Control medDiabilityButton = this.Form.FindControl("disguidelines");

            if (singleRow != null && medDiabilityButton != null)
            {
                singleRow.Controls.Add(medDiabilityButton);
            }

            Control medDiabilityDiv = this.Form.FindControl("div_disguidelines");

            if (medDiabilityDiv != null)
            {
                medDiabilityDiv.Dispose();
            }
        }

        /// <summary>
        /// Gets the required fields tab.
        /// </summary>
        /// <param name="requiredFileds">The required fileds.</param>
        /// <returns>The required fileds</returns>
        private IEnumerable<string> GetRequiredFieldsTab(string requiredFileds)
        {
            List<string> lstRequiredFields = requiredFileds.TrimEnd('|').Split('|').ToList();
            foreach (string field in lstRequiredFields)
            {
                yield return field;
            }
        }

        /// <summary>
        /// Gets the tab.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns>The tab having required field.</returns>
        private string GetTab(string field)
        {
            if (this.Form.FindControl(field).ClientID.Contains("_RMXTable"))
            {
                return field.Replace("_RMXTable", string.Empty);
            }
            else
            {
                return this.GetTab(this.Form.FindControl(field).Parent.ClientID);
            }
        }
    }
}