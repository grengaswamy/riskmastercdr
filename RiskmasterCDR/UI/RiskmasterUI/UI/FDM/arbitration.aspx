<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="arbitration.aspx.cs"  Inherits="Riskmaster.UI.FDM.Arbitration" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Arbitration</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
    </script>
    <%-- <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
        <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    --%>
    <%--vkumar258 - RMA-6037 - Starts --%>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/tb_save_active.png" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/tb_save_mo.png';" onMouseOut="this.src='../../Images/tb_save_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" OnClientClick="return DeleteRecord();" src="../../Images/tb_delete_active.png" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/tb_delete_mo.png';" onMouseOut="this.src='../../Images/tb_delete_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_comments" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return Comments();" src="../../Images/tb_comments_active.png" width="28" height="28" border="0" id="comments" AlternateText="Comments" onMouseOver="this.src='../../Images/tb_comments_mo.png';" onMouseOut="this.src='../../Images/tb_comments_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return recordSummary();" src="../../Images/tb_recordsummary_active.png" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='../../Images/tb_recordsummary_mo.png';" onMouseOut="this.src='../../Images/tb_recordsummary_active.png';" />
        </div>
      </div>
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Arbitration" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSarbitrationinfo" id="TABSarbitrationinfo">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="arbitrationinfo" id="LINKTABSarbitrationinfo">Arbitration Info</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSParbitrationinfo">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
          <a class="NotSelected1" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPsuppgroup">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABarbitrationinfo" id="FORMTABarbitrationinfo">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdarbitrationinfo" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="/Instance/ClaimXArbitration/ClaimId|/Instance/UI/FormVariables/SysExData/ClaimId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="arbitrationrowid" RMXRef="/Instance/ClaimXArbitration/ArbitrationRowId" RMXType="id" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_arbtypecode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_arbtypecode" Text="Type" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="arbtypecode" CodeTable="ARBITRATION_TYPE" ControlName="arbtypecode" RMXRef="/Instance/ClaimXArbitration/ArbTypeCode" CodeFilter="" RMXType="code" tabindex="1" />
                </span>
              </div>
              <div runat="server" class="half" id="div_arbstatuscode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_arbstatuscode" Text="Status" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="arbstatuscode" CodeTable="ARBITRATION_STATUS" ControlName="arbstatuscode" RMXRef="/Instance/ClaimXArbitration/ArbStatusCode" CodeFilter="" RMXType="code" tabindex="2" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_arbadversepartyeid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_arbadversepartyeid" Text="Adverse Party" />
                <span class="formw">
                                <asp:textbox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="arbadversepartyeid" rmxref="/Instance/ClaimXArbitration/ArbAdversePartyEid" rmxtype="eidlookup" cancelledvalue="" tabIndex="3"  />
                                <asp:button runat="server" class="EllipsisControl" id="arbadversepartyeidbtn"  onclientclick="return lookupData('arbadversepartyeid','OTHER PEOPLE',4,'arbadversepartyeid',2)" />
                                <asp:textbox style="display: none" runat="server" id="arbadversepartyeid_cid" rmxref="/Instance/ClaimXArbitration/ArbAdversePartyEid/@codeid" cancelledvalue="" />
                            </span>
                        </div>
                        <div runat="server" class="half" id="div_arbpartycode" xmlns="">
                            <asp:label runat="server" class="label" id="lbl_arbpartycode" text="Arbitration Party" />
                            <span class="formw">
                                <uc:codelookup runat="server" onchange="setDataChanged(true);&#xA;&#x9;&#x9;  " id="arbpartycode" codetable="ARBITRATION_PARTY" controlname="arbpartycode" rmxref="/Instance/ClaimXArbitration/ArbPartyCode" codefilter="" rmxtype="code" tabIndex="4" />
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_arbadversecompany" xmlns="">
                            <asp:label runat="server" class="label" id="lbl_arbadversecompany" text="Company" />
                            <span class="formw">
                                <asp:textbox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="arbadversecompany" rmxref="/Instance/ClaimXArbitration/ArbAdverseCoEid" rmxtype="eidlookup" cancelledvalue=""  />
                                <asp:textbox style="display: none" runat="server" value="1" name="arbadversecompany_creatable" tabIndex="5" />
                  <asp:Button runat="server" class="EllipsisControl" id="arbadversecompanybtn" tabindex="6" onclientclick="&#xA;                        return lookupData('arbadversecompany','OTHER_PEOPLE',4,'adversecompany',1)&#xA;                      " />
                  <asp:TextBox style="display:none" runat="server" id="arbadversecompany_cid" RMXref="/Instance/ClaimXArbitration/ArbAdverseCoEid/@codeid" cancelledvalue="" />
                </span>
              </div>
              <div runat="server" class="half" id="div_datefiled" xmlns="">
                <asp:label runat="server" class="label" id="lbl_datefiled" Text="Date Filed" />
                <span class="formw">
                                <asp:textbox runat="server" tabIndex="7" formatas="date" id="datefiled" rmxref="/Instance/ClaimXArbitration/DateFiled" rmxtype="date"  onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                                <%--<asp:button class="DateLookupControl" runat="server" id="datefiledbtn" tabindex="6" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "datefiled",
					    ifFormat: "%m/%d/%Y",
					    button: "datefiledbtn"
					}
					);
				</script>--%>
                                <%--vkumar258 - RMA-6037 - Starts --%>
                                <script type="text/javascript">
                                    $(function () {
                                        $("#datefiled").datepicker({
                                            showOn: "button",
                                            buttonImage: "../../Images/calendar.gif",
                                           // buttonImageOnly: true,
                                            showOtherMonths: true,
                                            selectOtherMonths: true,
                                            changeYear: true
                                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "8");
                                    });
                                </script>
                                <%--vkumar258 - RMA_6037- End--%>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_arbadvadjuster" xmlns="">
                            <asp:label runat="server" class="label" id="lbl_arbadvadjuster" text="Adjuster" />
                            <span class="formw">
                                <asp:textbox runat="server" tabIndex="8" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="arbadvadjuster" rmxref="/Instance/ClaimXArbitration/ArbAdvAdjusterEid" rmxtype="eidlookup" cancelledvalue="" />
                                <asp:button runat="server" class="EllipsisControl" id="arbadvadjusterbtn"  onclientclick="return lookupData('arbadvadjuster','OTHER PEOPLE',4,'arbadvadjuster',2)" />
                                <asp:textbox style="display: none" runat="server" id="arbadvadjuster_cid" rmxref="/Instance/ClaimXArbitration/ArbAdvAdjusterEid/@codeid" cancelledvalue="" />
                            </span>
                        </div>
                        <div runat="server" class="half" id="div_datehearing" xmlns="">
                            <asp:label runat="server" class="label" id="lbl_datehearing" text="Hearing Date" />
                            <span class="formw">
                                <asp:textbox runat="server" tabIndex="9" formatas="date" id="datehearing" rmxref="/Instance/ClaimXArbitration/DateHearing" rmxtype="date" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                                <%--<asp:button class="DateLookupControl" runat="server" id="datehearingbtn" tabindex="6" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "datehearing",
					    ifFormat: "%m/%d/%Y",
					    button: "datehearingbtn"
					}
					);
				</script>--%>
                                <%--vkumar258 - RMA-6037 - Starts --%>
                                <script type="text/javascript">
                                    $(function () {
                                        $("#datehearing").datepicker({
                                            showOn: "button",
                                            buttonImage: "../../Images/calendar.gif",
                                           // buttonImageOnly: true,
                                            showOtherMonths: true,
                                            selectOtherMonths: true,
                                            changeYear: true
                                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "10");
                                    });
                                </script>
                                <%--vkumar258 - RMA_6037- End--%>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_arbclaimnum" xmlns="">
                            <asp:label runat="server" class="label" id="lbl_arbclaimnum" text="Claim Number" />
                            <span class="formw">
                                <asp:textbox runat="server" tabIndex="11" id="arbclaimnum" rmxref="/Instance/ClaimXArbitration/ArbAdversClaimNum" rmxtype="text"  maxlength="25" onchange=";setDataChanged(true);" />
                            </span>
                        </div>
                        <div runat="server" class="half" id="div_arbamount" xmlns="">
                            <asp:label runat="server" class="label" id="lbl_arbamount" text="Award Amount" />
                            <span class="formw">
                                <asp:textbox runat="server" tabIndex="12" id="arbamount" rmxref="/Instance/ClaimXArbitration/ArbAwardAmount" rmxtype="currency" rmxforms:as="currency"  onchange="currencyLostFocus(this);setDataChanged(true);" />
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:textbox style="display: none" runat="server" id="subdttmrcdadded" rmxref="/Instance/ClaimXArbitration/DttmRcdAdded" rmxtype="id" />
                        <asp:textbox style="display: none" runat="server" id="subdttmrcdlastupd" rmxref="/Instance/ClaimXArbitration/DttmRcdLastUpd" rmxtype="id" />
                        <asp:textbox style="display: none" runat="server" id="subupdatedbyuser" rmxref="/Instance/ClaimXArbitration/UpdatedByUser" rmxtype="id" />
                        <asp:textbox style="display: none" runat="server" id="subaddedbyuser" rmxref="/Instance/ClaimXArbitration/AddedByUser" rmxtype="id" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
            <table width="98%" border="0" cellspacing="0" celpadding="0">
                <tr>
                    <td>
                        <asp:hiddenfield runat="server" id="hdsuppgroup" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="Div2" class="formButtonGroup" runat="server">
            <div class="formButton" runat="server" id="div_btnDemandOffer">
                <asp:button class="button" runat="Server" id="btnDemandOffer" tabIndex="13" text="DemandOffer" visible="false" rmxref="/Instance/ClaimXArbitration/DemandOfferList/@committedcount" onclientclick="if(!( XFormHandler('SysFormPIdName=arbitrationrowid&SysFormName=demandoffer&SysCmd=1&SysFormIdName=demandofferrowid&SysEx=SubrogationRowId&SysExMapCtl=ArbitrationRowId','','')))return false;" postbackurl="demandoffer.aspx?SysFormPIdName=arbitrationrowid&SysFormName=demandoffer&SysCmd=1&SysFormIdName=demandofferrowid&SysEx=SubrogationRowId&SysExMapCtl=ArbitrationRowId" />
            </div>
        </div>
        <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave" rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="SysCmdText" rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" />
        <asp:textbox style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName" rmxtype="hidden" text="ClaimXArbitration" />
        <asp:textbox style="display: none" runat="server" id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig" rmxtype="hidden" text="&lt;ClaimXArbitration&gt;&lt;SubroSpecialist&gt;&lt;/SubroSpecialist&gt;&lt;AdverseParty&gt;&lt;/AdverseParty&gt;&lt;AdverseInsuranceCompany&gt;&lt;/AdverseInsuranceCompany&gt;&lt;AdverseAdjuster&gt;&lt;/AdverseAdjuster&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/ClaimXArbitration&gt;" />
        <asp:textbox style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm" rmxtype="hidden" text="claimwc" />
        <asp:textbox style="display: none" runat="server" id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden" text="claimid" />
        <asp:textbox style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx" rmxtype="hidden" text="claimid,arbitrationrowid" />
        <asp:textbox style="display: none" runat="server" id="SysNotReqNew" rmxref="Instance/UI/FormVariables/SysNotReqNew" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName" rmxtype="hidden" text="arbitration" />
        <asp:textbox style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName" rmxtype="hidden" text="arbitrationrowid" />
        <asp:textbox style="display: none" runat="server" id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid" rmxtype="hidden" text="3000" />
        <asp:textbox style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="line_of_bus_code" rmxref="Instance/UI/MissingRef" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="lineofbusinesscode" rmxref="/Instance/UI/FormVariables/SysExData/LINEOFBUSCODE" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="TabNameList" rmxref="" rmxtype="hidden" text="TABSarbitrationinfo|TABSsuppgroup" />
        <asp:textbox style="display: none" runat="server" id="GroupAssocFieldList" rmxref="" rmxtype="hidden" text="" />
        <asp:textbox style="display: none" runat="server" id="FormReadOnly" rmxref="" rmxtype="hidden" text="Enable" />
        <asp:textbox style="display: none" runat="server" name="formname" text="arbitratiom" />
        <asp:textbox style="display: none" runat="server" name="SysRequired" id="SysRequired" text="" />
        <asp:textbox style="display: none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" text="" />
        <asp:textbox style="display: none" runat="server" name="SysFocusFields" text="arbclaimnum|arbamount|" />
        <asp:textbox style="display: none" runat="server" name="SysReadonlyFields" id="SysReadonlyFields" text="" />
        <asp:textbox style="display: none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
        <input type="hidden" id="hdSaveButtonClicked" />
        <asp:textbox style="display: none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
        <asp:textbox runat="server" id="txtScreenFlowStack" style="display: none" />
        <asp:textbox runat="server" id="SysPageDataChanged" style="display: none" />
        <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
        <uc:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="Loading" />
    </form>
  </body>
</html>