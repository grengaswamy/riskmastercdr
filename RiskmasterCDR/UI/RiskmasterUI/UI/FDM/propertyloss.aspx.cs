﻿using System;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;




namespace Riskmaster.UI.FDM
{
    public partial class Propertyloss : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
            CheckBox chkvlInsured = (CheckBox)Page.FindControl("plInsured");
            if (chkvlInsured != null)
            {
                chkvlInsured.Enabled = false;
            }
        }
    }
}