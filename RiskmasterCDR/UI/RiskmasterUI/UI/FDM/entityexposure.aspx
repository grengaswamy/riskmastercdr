<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="entityexposure.aspx.cs"
    Inherits="Riskmaster.UI.FDM.Entityexposure" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<%@ Register Src="~/UI/Shared/Controls/MultiCodeLookup.ascx" TagName="MultiCode"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:uc="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Exposure Information</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="../../Images/new.gif"
                        width="28" height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='../../Images/new2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/new.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="../../Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="../../Images/first.gif"
                        width="28" height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='../../Images/first2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/first.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="../../Images/prev.gif"
                        width="28" height="28" border="0" id="moveprevious" alternatetext="Move Previous"
                        onmouseover="this.src='../../Images/prev2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/prev.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="../../Images/next.gif"
                        width="28" height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='../../Images/next2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/next.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="../../Images/last.gif"
                        width="28" height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='../../Images/last2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/last.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doLookup()" src="../../Images/lookup.gif"
                        width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='../../Images/lookup2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/lookup.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="../../Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='../../Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/delete.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Exposure Information" /><asp:label
            id="formsubtitle" runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSexposure" id="TABSexposure">
            <a class="Selected" href="#" onclick="tabChange(this.name);" runat="server" rmxref=""
                name="exposure" id="LINKTABSexposure">Exposure</a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" runat="server" rmxref=""
                name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABexposure" id="FORMTABexposure">
        <asp:textbox style="display: none" runat="server" id="entityid" rmxref="/Instance/UI/FormVariables/SysExData/EntityId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="entitytableid"
                rmxref="/Instance/UI/FormVariables/SysExData/EntityTableId" rmxtype="id" /><asp:textbox
                    style="display: none" runat="server" id="exposurerowid" rmxref="/Instance/EntityXExposure/ExposureRowId"
                    rmxtype="id" /><div runat="server" class="half" id="div_startdate" xmlns="">
                        <asp:label runat="server" class="required" id="lbl_startdate" text="Exposure Start Date" /><span
                            class="formw"><asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);"
                                id="startdate" rmxref="/Instance/EntityXExposure/StartDate" rmxtype="date" tabindex="1" /><asp:requiredfieldvalidator
                                    validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="startdate" /><cc1:CalendarExtender
                                        runat="server" ID="startdate_ajax" TargetControlID="startdate" />
                        </span>
                    </div>
        <div runat="server" class="half" id="div_enddate" xmlns="">
            <asp:label runat="server" class="required" id="lbl_enddate" text="Exposure End Date" /><span
                class="formw"><asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);"
                    id="enddate" rmxref="/Instance/EntityXExposure/EndDate" rmxtype="date" tabindex="9" /><asp:requiredfieldvalidator
                        validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="enddate" /><cc1:CalendarExtender
                            runat="server" ID="enddate_ajax" TargetControlID="enddate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_exposuretype" xmlns="">
            <asp:label runat="server" class="label" id="lbl_exposuretype" text="Exposure Type" /><span
                class="formw"><asp:textbox runat="Server" rmxref="/Instance/UI/FormVariables/SysExData/ExposureType"
                    id="exposuretype" tabindex="3" style="background-color: silver;" readonly="true" /></span></div>
        <div runat="server" class="half" id="div_noofemployees" xmlns="">
            <asp:label runat="server" class="label" id="lbl_noofemployees" text="No Of Employees" /><span
                class="formw"><asp:textbox runat="server" onblur="numLostFocus(this);" id="noofemployees"
                    rmxref="/Instance/EntityXExposure/NoOfEmployees" rmxtype="numeric" tabindex="4"
                    onchange="setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_vehiclecount" xmlns="">
            <asp:label runat="server" class="label" id="lbl_vehiclecount" text="Vehicle Count" /><span
                class="formw"><asp:textbox runat="server" onblur="numLostFocus(this);" id="vehiclecount"
                    rmxref="/Instance/EntityXExposure/VehicleCount" rmxtype="numeric" tabindex="11"
                    onchange="setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_noofworkhours" xmlns="">
            <asp:label runat="server" class="label" id="lbl_noofworkhours" text="No Of Work Hours" /><span
                class="formw"><asp:textbox runat="server" onblur="numLostFocus(this);" id="noofworkhours"
                    rmxref="/Instance/EntityXExposure/NoOfWorkHours" rmxtype="numeric" tabindex="5"
                    onchange="setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_totalrevenue" xmlns="">
            <asp:label runat="server" class="label" id="lbl_totalrevenue" text="Total Revenue" /><span
                class="formw"><asp:textbox runat="server" id="totalrevenue" rmxref="/Instance/EntityXExposure/TotalRevenue"
                    rmxtype="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency"
                    tabindex="12" onchange=";setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_payrollamount" xmlns="">
            <asp:label runat="server" class="label" id="lbl_payrollamount" text="Payroll Amount" /><span
                class="formw"><asp:textbox runat="server" id="payrollamount" rmxref="/Instance/EntityXExposure/PayrollAmount"
                    rmxtype="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency"
                    tabindex="6" onchange=";setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_otherbase" xmlns="">
            <asp:label runat="server" class="label" id="lbl_otherbase" text="Other Base" /><span
                class="formw"><asp:textbox runat="server" id="otherbase" rmxref="/Instance/EntityXExposure/OtherBase"
                    rmxtype="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency"
                    tabindex="13" onchange=";setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_assetvalue" xmlns="">
            <asp:label runat="server" class="label" id="lbl_assetvalue" text="Asset Value" /><span
                class="formw"><asp:textbox runat="server" id="assetvalue" rmxref="/Instance/EntityXExposure/AssetValue"
                    rmxtype="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency"
                    tabindex="7" onchange=";setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_rmoverhead" xmlns="">
            <asp:label runat="server" class="label" id="lbl_rmoverhead" text="RM Overhead" /><span
                class="formw"><asp:textbox runat="server" id="rmoverhead" rmxref="/Instance/EntityXExposure/RiskMgmtOverhead"
                    rmxtype="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency"
                    tabindex="14" onchange=";setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_squarefootage" xmlns="">
            <asp:label runat="server" class="label" id="lbl_squarefootage" text="Square Footage" /><span
                class="formw"><asp:textbox runat="server" onblur="numLostFocus(this);" id="squarefootage"
                    rmxref="/Instance/EntityXExposure/SquareFootage" rmxtype="numeric" tabindex="8"
                    onchange="setDataChanged(true);" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup">
        <asp:textbox style="display: none" runat="server" id="supp_exposure_row_id" rmxref="/Instance/*/Supplementals/EXPOSURE_ROW_ID"
            rmxtype="id" /><div runat="server" class="half" id="div_supp_temp_supp_text" xmlns="">
                <asp:label runat="server" class="label" id="lbl_supp_temp_supp_text" text="TEMP_SUPP" /><span
                    class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" id="supp_temp_supp_text"
                        rmxref="/Instance/*/Supplementals/TEMP_SUPP_TEXT" rmxtype="text" /></span></div>
    </div>
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">

            <script language="JavaScript" src="">{var i;}
            </script>

            <asp:button class="button" runat="server" id="btnBack" rmxref="" text="Back" onclientclick="return BackToOrgAddEdit();" /></div>
        <div class="formButton" runat="server" id="div_btnRollUp">

            <script language="JavaScript" src="">{var i;}
            </script>

            <asp:button class="button" runat="server" id="btnRollUp" rmxref="" text="Roll Up"
                onclientclick="return SetRollUpAttribute();" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="EntityXExposure" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" text="&lt;EntityXExposure&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/EntityXExposure&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
                                rmxtype="hidden" text="entitymaint" /><asp:textbox style="display: none" runat="server"
                                    id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden"
                                    text="entityid" /><asp:textbox style="display: none" runat="server" id="SysFormPId"
                                        rmxref="Instance/UI/FormVariables/SysFormPId" rmxtype="hidden" text="" /><asp:textbox
                                            style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                            rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysNotReqNew"
                                                rmxref="Instance/UI/FormVariables/SysNotReqNew" rmxtype="hidden" text="" /><asp:textbox
                                                    style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                                    rmxtype="hidden" text="entityexposure" /><asp:textbox style="display: none" runat="server"
                                                        id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName" rmxtype="hidden"
                                                        text="exposurerowid" /><asp:textbox style="display: none" runat="server" id="SysFormId"
                                                            rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden" text="" /><asp:textbox
                                                                style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
                                                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysSid"
                                                                    rmxref="Instance/UI/FormVariables/SysSid" rmxtype="hidden" text="16500" /><asp:textbox
                                                                        style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                                                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" name="formname"
                                                                            value="entityexposure" /><asp:textbox style="display: none" runat="server" name="SysRequired"
                                                                                value="startdate|enddate|" /><asp:textbox style="display: none" runat="server" name="SysFocusFields"
                                                                                    value="" /><input type="hidden" id="hdSaveButtonClicked" /><asp:textbox runat="server"
                                                                                        id="SysInvisible" style="display: none" /><asp:textbox runat="server" id="SysLookupClass"
                                                                                            style="display: none" /><asp:textbox runat="server" id="SysLookupRecordId" style="display: none" /><asp:textbox
                                                                                                runat="server" id="SysLookupAttachNodePath" style="display: none" /><asp:textbox
                                                                                                    runat="server" id="SysLookupResultConfig" style="display: none" /><input type="hidden"
                                                                                                        value="rmx-widget-handle-2" id="SysWindowId" /></form>
</body>
</html>
