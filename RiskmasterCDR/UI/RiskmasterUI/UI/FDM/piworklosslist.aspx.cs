﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.FDM
{
    public partial class PiworklossList : FDMBasePage
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadInitialPage();
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/PiXWorkLossList/PiXWorkLoss")
                         let xClaim = (int)Convert.ToInt64(c.Element("PiWlRowId").Value)
                         orderby xClaim
                         select c;
                if (rootElement.XPathSelectElement("//PiXWorkLoss/PiRowId") != null)
                {
                    pirowid.Text = rootElement.XPathSelectElement("//PiXWorkLoss/PiRowId").Value;
                }

                //rsushilaggar MITS 37986
                CalculateTotalOshaDays(result);

            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }

        protected void NavigateListDelete(object sender, EventArgs e)
        {
            try
            {
                base.NavigateListDelete(sender, e);

                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/PiXWorkLossList/PiXWorkLoss")
                         let xClaim = (int)Convert.ToInt64(c.Element("PiWlRowId").Value)
                         orderby xClaim
                         select c;
                if (rootElement.XPathSelectElement("//PiXWorkLoss/PiRowId") != null)
                {
                    pirowid.Text = rootElement.XPathSelectElement("//PiXWorkLoss/PiRowId").Value;
                }

                //LoadInitialPage();
                //rsushilaggar MITS 37986
                CalculateTotalOshaDays(result);
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }

        /// <summary>
        /// rsushilaggar MITS 37986
        /// </summary>
        /// <param name="p_xElement"></param>
        private void CalculateTotalOshaDays(IEnumerable p_xElement)
        {
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtEventDate;
            TimeSpan span;
            string sEndDate = string.Empty; 
            int iTotalOshaDays = 0;
            int iTotalWorkLossDays = 0;
            int iTotalStateDuration = 0;
            string sEventDate = string.Empty;
            foreach (XElement item in p_xElement)
            {
                dtStartDate = Conversion.ToDate(item.Element("DateLastWorked").Value);
                dtEventDate = Conversion.ToDate(DateOfEvent.Text);
                sEndDate = item.Element("DateReturned").Value;
                sEventDate = Conversion.ToDate(DateOfEvent.Text).ToString("MM/dd/yyyy");       
                if (sEndDate.Length > 0)
                    dtEndDate = Conversion.ToDate(sEndDate);
                else
                    dtEndDate = DateTime.Now;
                if (DateTime.Compare(dtStartDate, dtEventDate) < 0)
                {
                    dtStartDate = dtEventDate;
                }
                if (DateTime.Compare(dtStartDate, dtEndDate) < 0)
                {
                    span = dtEndDate - dtStartDate;
                    int iDays = span.Days - 1;
                    if (iDays > 0)
                        iTotalOshaDays += iDays;
                }

                iTotalWorkLossDays = iTotalWorkLossDays + Convert.ToInt32(item.Element("Duration").Value);
                iTotalStateDuration = iTotalStateDuration + Convert.ToInt32(item.Element("StateDuration").Value);
            }
            //MITS 37986 gmallick start
            if (!string.IsNullOrEmpty(sEventDate)) 
            {
                // If the event happened after Osha 300 rules came into effect
                if (Convert.ToDateTime(sEventDate) < Convert.ToDateTime("01/01/2002"))
                {
                    iTotalOshaDays = 0;
                }
            }
            //MITS 37986 gmallick End

            lblTotalDays.Text = "Total Work Loss Days, Duration: " + iTotalWorkLossDays + ", State Duration: " + iTotalStateDuration + ", OSHA 300: " + iTotalOshaDays;
           


        }
    }
}
