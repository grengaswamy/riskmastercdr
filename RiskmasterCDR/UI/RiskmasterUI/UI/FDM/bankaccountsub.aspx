<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bankaccountsub.aspx.cs"
    Inherits="Riskmaster.UI.FDM.Bankaccountsub" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/MultiCodeLookup.ascx" TagName="MultiCode"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:uc="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Sub Bank Accounts</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="../../Images/new.gif"
                        width="28" height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='../../Images/new2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/new.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="../../Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="../../Images/first.gif"
                        width="28" height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='../../Images/first2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/first.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="../../Images/prev.gif"
                        width="28" height="28" border="0" id="moveprevious" alternatetext="Move Previous"
                        onmouseover="this.src='../../Images/prev2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/prev.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="../../Images/next.gif"
                        width="28" height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='../../Images/next2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/next.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="../../Images/last.gif"
                        width="28" height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='../../Images/last2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/last.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="../../Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='../../Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/delete.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return doLookup();" src="../../Images/lookup.gif"
                        width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='../../Images/lookup2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/lookup.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return recordSummary();" src="../../Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Sub Bank Accounts" /><asp:label id="formsubtitle"
            runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSdisbursementaccount"
            id="TABSdisbursementaccount">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="disbursementaccount"
                id="LINKTABSdisbursementaccount"><span style="text-decoration: none">Disbursement Account</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSaccountfilter" id="TABSaccountfilter">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="accountfilter"
                id="LINKTABSaccountfilter"><span style="text-decoration: none">Account Owner</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABdisbursementaccount" id="FORMTABdisbursementaccount">
        <asp:textbox style="display: none" runat="server" id="accountid" rmxref="Instance/BankAccSub/AccountId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="subaccountid"
                rmxref="Instance/BankAccSub/SubRowId" rmxtype="id" /><div runat="server" class="half"
                    id="div_subaccountname" xmlns="">
                    <asp:label runat="server" class="required" id="lbl_subaccountname" text="Sub Account Name" /><span
                        class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" id="subaccountname"
                            rmxref="Instance/BankAccSub/SubAccName" rmxtype="text" /><asp:requiredfieldvalidator
                                validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="subaccountname" /></span></div>
        <div runat="server" class="half" id="div_minbalance" xmlns="">
            <asp:label runat="server" class="label" id="lbl_minbalance" text="Minimum Balance" /><span
                class="formw"><asp:textbox runat="server" id="minbalance" rmxref="Instance/BankAccSub/MinBalance"
                    rmxtype="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency"
                    tabindex="5" onchange=";setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_subaccountnumber" xmlns="">
            <asp:label runat="server" class="required" id="lbl_subaccountnumber" text="Sub Account #" /><span
                class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" id="subaccountnumber"
                    rmxref="Instance/BankAccSub/SubAccNumber" rmxtype="text" /><asp:requiredfieldvalidator
                        validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="subaccountnumber" /></span></div>
        <div runat="server" class="half" id="div_targetbalance" xmlns="">
            <asp:label runat="server" class="label" id="lbl_targetbalance" text="Target Balance" /><span
                class="formw"><asp:textbox runat="server" id="targetbalance" rmxref="Instance/BankAccSub/TargetBalance"
                    rmxtype="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency"
                    tabindex="6" onchange=";setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_checkstock" xmlns="">
            <asp:label runat="server" class="label" id="lbl_checkstock" text="Check Stock" /><span
                class="formw"><asp:dropdownlist runat="server" id="checkstock" tabindex="5" rmxref="Instance/BankAccSub/StockId"
                    rmxtype="combobox" itemsetref="/Instance/UI/FormVariables/SysExData/StockList"
                    width="205" onchange="setDataChanged(true);"><asp:listitem value="0" text="None" />
                    <asp:listitem value="1" text="Lowest" />
                    <asp:listitem value="2" text="Low" />
                    <asp:listitem value="3" text="Low Moderate" />
                    <asp:listitem value="4" text="Moderate" />
                    <asp:listitem value="5" text="High Moderate" />
                    <asp:listitem value="6" text="High" />
                    <asp:listitem value="7" text="Very High" />
                    <asp:listitem value="8" text="Highest" />
                </asp:dropdownlist></span></div>
        <div runat="server" class="half" id="div_priority" xmlns="">
            <asp:label runat="server" class="label" id="lbl_priority" text="Priority" /><span
                class="formw"><asp:dropdownlist runat="server" id="priority" tabindex="3" rmxref="Instance/BankAccSub/Priority"
                    rmxtype="combobox" width="205" onchange="setDataChanged(true);"><asp:listitem value="0"
                        text="None" />
                    <asp:listitem value="1" text="Lowest" />
                    <asp:listitem value="2" text="Low" />
                    <asp:listitem value="3" text="Low Moderate" />
                    <asp:listitem value="4" text="Moderate" />
                    <asp:listitem value="5" text="High Moderate" />
                    <asp:listitem value="6" text="High" />
                    <asp:listitem value="7" text="Very High" />
                    <asp:listitem value="8" text="Highest" />
                </asp:dropdownlist></span></div>
        <div runat="server" class="half" id="div_chkmma" xmlns="">
            <asp:label runat="server" class="label" id="lbl_chkmma" text="Use Additional Money Market Account" /><span
                class="formw"><asp:checkbox runat="server" onchange="setDataChanged(true);" id="chkmma"
                    rmxref="Instance/BankAccSub/UseMmFlag" rmxtype="checkbox" tabindex="4" /></span></div>
        <div runat="server" class="half" id="div_linebreak1" xmlns="">
            <asp:label runat="server" class="label" id="lbl_linebreak1" text="" /><span class="formw"><tr>
                <td>
                    <br />
                </td>
            </tr>
            </span>
        </div>
        <div runat="server" class="half" id="div_btnAccountBalance" xmlns="">

            <script language="JavaScript" src="">{var i;}
            </script>

            <span class="formw">
                <asp:button class="button" runat="server" id="btnAccountBalance" rmxref="" rmxtype="buttonscript"
                    text="Account Balance" onclientclick="LoadDisbursementAccountBalance();" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABaccountfilter"
        id="FORMTABaccountfilter">
        <div runat="server" class="half" id="div_ownereid" xmlns="">
            <asp:label runat="server" class="label" id="lbl_ownereid" text="Owner" /><span class="formw"><asp:textbox
                runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                id="ownereid" rmxref="Instance/BankAccSub/OwnerEid" rmxtype="eidlookup" cancelledvalue=""
                tabindex="7" /><input type="button" class="button" value="..." name="ownereidbtn"
                    tabindex="8" onclick="lookupData('ownereid','ORGANIZATIONS',4,'ownereid',2)" /><asp:textbox
                        style="display: none" runat="server" id="ownereid_cid" rmxref="Instance/BankAccSub/OwnerEid/@codeid"
                        cancelledvalue="" /></span></div>
        <div id="div4" class="half" xmlns="">
            <img src="../../Images/Blank.JPG" id="Img1" class="half" /></div>
        <div runat="server" class="half" id="div_LOB" xmlns="">
            <span class="label">
                <asp:label runat="server" id="LOB" rmxref="Instance/UI/MissingRef" rmxtype="labelonly"
                    text="Line Of Business " /></span></div>
        <div runat="server" class="half" id="div_OWNER_LOB1" xmlns="">
            <span class="formw">
                <asp:radiobutton runat="server" id="OWNER_LOB1" rmxref="Instance/BankAccSub/OwnerLob"
                    rmxtype="radio" onclientclick="setDataChanged(true);" text="All" checked="" value="0"
                    groupname="OWNER_LOB" /></span></div>
        <div id="div5" class="half" xmlns="">
            <img src="../../Images/Blank.JPG" id="Img1" class="half" /></div>
        <div runat="server" class="half" id="div_OWNER_LOB2" xmlns="">
            <span class="formw">
                <asp:radiobutton runat="server" id="OWNER_LOB2" rmxref="Instance/BankAccSub/OwnerLob"
                    rmxtype="radio" onclientclick="setDataChanged(true);" text="General Claim" value="241"
                    groupname="OWNER_LOB" /></span></div>
        <div id="div6" class="half" xmlns="">
            <img src="../../Images/Blank.JPG" id="Img1" class="half" /></div>
        <div runat="server" class="half" id="div_OWNER_LOB3" xmlns="">
            <span class="formw">
                <asp:radiobutton runat="server" id="OWNER_LOB3" rmxref="Instance/BankAccSub/OwnerLob"
                    rmxtype="radio" onclientclick="setDataChanged(true);" text="Vehicle Accident"
                    value="242" groupname="OWNER_LOB" /></span></div>
        <div id="div7" class="half" xmlns="">
            <img src="../../Images/Blank.JPG" id="Img1" class="half" /></div>
        <div runat="server" class="half" id="div_OWNER_LOB4" xmlns="">
            <span class="formw">
                <asp:radiobutton runat="server" id="OWNER_LOB4" rmxref="Instance/BankAccSub/OwnerLob"
                    rmxtype="radio" onclientclick="setDataChanged(true);" text="Worker's Compensation"
                    value="243" groupname="OWNER_LOB" /></span></div>
        <div id="div8" class="half" xmlns="">
            <img src="../../Images/Blank.JPG" id="Img1" class="half" /></div>
        <div runat="server" class="half" id="div_OWNER_LOB5" xmlns="">
            <span class="formw">
                <asp:radiobutton runat="server" id="OWNER_LOB5" rmxref="Instance/BankAccSub/OwnerLob"
                    rmxtype="radio" onclientclick="setDataChanged(true);" text="Short Term Disability"
                    value="844" groupname="OWNER_LOB" /></span></div>
        <div id="div9" class="half" xmlns="">
            <img src="../../Images/Blank.JPG" id="Img1" class="half" /></div>
        <div runat="server" class="half" id="div_labelTrigger" xmlns="">
            <span class="label">
                <asp:label runat="server" id="labelTrigger" rmxref="Instance/UI/MissingRef" rmxtype="labelonly"
                    text="Effective Trigger and Dates " /></span></div>
        <div id="div10" class="half" xmlns="">
            <img src="../../Images/Blank.JPG" id="Img1" class="half" /></div>
        <div id="div11" class="half" xmlns="">
            <img src="../../Images/Blank.JPG" id="Img1" class="half" /></div>
        <div runat="server" class="half" id="div_triggerdate" xmlns="">
            <asp:label runat="server" class="label" id="lbl_triggerdate" text="Effective Date Trigger" /><span
                class="formw"><asp:dropdownlist runat="server" id="triggerdate" tabindex="12" rmxref="Instance/BankAccSub/EffTrigger"
                    rmxtype="combobox" onchange="setDataChanged(true);"><asp:listitem value="" text="none" />
                    <asp:listitem value="Current System Date" text="Current System Date" />
                    <asp:listitem value="Event Date" text="Event Date" />
                    <asp:listitem value="Claim Date" text="Claim Date" />
                </asp:dropdownlist></span></div>
        <div id="div12" class="half" xmlns="">
            <img src="../../Images/Blank.JPG" id="Img1" class="half" /></div>
        <div id="div13" class="half" xmlns="">
            <img src="../../Images/Blank.JPG" id="Img1" class="half" /></div>
        <div runat="server" class="half" id="div_startdate" xmlns="">
            <asp:label runat="server" class="label" id="lbl_startdate" text="Effective Start Date" /><span
                class="formw"><asp:textbox runat="server" formatas="date" id="startdate" rmxref="Instance/BankAccSub/TriggerFromDate"
                    rmxtype="date" tabindex="13" /><cc1:CalendarExtender runat="server" ID="startdate_ajax"
                        TargetControlID="startdate" OnClientDateSelectionChanged="setDataChangedTrue" />
            </span>
        </div>
        <div runat="server" class="half" id="div_enddate" xmlns="">
            <asp:label runat="server" class="label" id="lbl_enddate" text="Effective End Date" /><span
                class="formw"><asp:textbox runat="server" formatas="date" id="enddate" rmxref="Instance/BankAccSub/TriggerToDate"
                    rmxtype="date" tabindex="15" /><cc1:CalendarExtender runat="server" ID="enddate_ajax"
                        TargetControlID="enddate" OnClientDateSelectionChanged="setDataChangedTrue" />
            </span>
        </div>
        <div runat="server" class="half" id="div_servicecodelist" xmlns="">
            <asp:label runat="server" class="label" id="lbl_servicecodelist" text="Service Code(s)" /><span
                class="formw"><uc:MultiCode runat="server" ID="servicecodelist" CodeTable="SERVICE_CODE"
                    ControlName="servicecodelist" RMXRef="/Instance/BankAccSub/ServiceCodes" RMXType="codelist" />
            </span>
        </div>
    </div>
    <div id="Div14" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
            <asp:button class="button" runat="Server" id="btnBack" text="Back to Accounts" onclientclick="if(!( XFormHandler('SysCmd=0','1','back')))return false;"
                postbackurl="?SysCmd=0" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="BankAccSub" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" text="&lt;SubAccount&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/SubAccount&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
                                rmxtype="hidden" text="bankaccount" /><asp:textbox style="display: none" runat="server"
                                    id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden"
                                    text="accountid" /><asp:textbox style="display: none" runat="server" id="SysFormPId"
                                        rmxref="Instance/UI/FormVariables/SysFormPId" rmxtype="hidden" text="" /><asp:textbox
                                            style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
                                            rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysEx"
                                                rmxref="Instance/UI/FormVariables/SysEx" rmxtype="hidden" text="" /><asp:textbox
                                                    style="display: none" runat="server" id="SysNotReqNew" rmxref="Instance/UI/FormVariables/SysNotReqNew"
                                                    rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormName"
                                                        rmxref="Instance/UI/FormVariables/SysFormName" rmxtype="hidden" text="bankaccountsub" /><asp:textbox
                                                            style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
                                                            rmxtype="hidden" text="subaccountid" /><asp:textbox style="display: none" runat="server"
                                                                id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden"
                                                                text="" /><asp:textbox style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                                                    rmxtype="hidden" text="10550" /><asp:textbox style="display: none" runat="server"
                                                                        id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType" rmxtype="hidden"
                                                                        text="" /><asp:textbox style="display: none" runat="server" name="formname" value="bankaccount" /><asp:textbox
                                                                            style="display: none" runat="server" name="SysRequired" value="subaccountname|subaccountnumber|" /><asp:textbox
                                                                                style="display: none" runat="server" name="SysFocusFields" value="subaccountname|ownereid|triggerdate|" /><input
                                                                                    type="hidden" id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible"
                                                                                        style="display: none" /><asp:textbox runat="server" id="SysLookupClass" style="display: none" /><asp:textbox
                                                                                            runat="server" id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server"
                                                                                                id="SysLookupAttachNodePath" style="display: none" /><asp:textbox runat="server"
                                                                                                    id="SysLookupResultConfig" style="display: none" /><input type="hidden" value="rmx-widget-handle-2"
                                                                                                        id="SysWindowId" /></form>
</body>
</html>
