<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="leave.aspx.cs"  Inherits="Riskmaster.UI.FDM.Leave" ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Leave Management</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_new" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateNew" src="../../Images/new.gif" width="28" height="28" border="0" id="new" AlternateText="New" onMouseOver="this.src='../../Images/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/new.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/save.gif" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/save.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateFirst" src="../../Images/first.gif" width="28" height="28" border="0" id="movefirst" AlternateText="Move First" onMouseOver="this.src='../../Images/first2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/first.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigatePrev" src="../../Images/prev.gif" width="28" height="28" border="0" id="moveprevious" AlternateText="Move Previous" onMouseOver="this.src='../../Images/prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/prev.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateNext" src="../../Images/next.gif" width="28" height="28" border="0" id="movenext" AlternateText="Move Next" onMouseOver="this.src='../../Images/next2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/next.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateLast" src="../../Images/last.gif" width="28" height="28" border="0" id="movelast" AlternateText="Move Last" onMouseOver="this.src='../../Images/last2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/last.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" src="../../Images/delete.gif" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/delete2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/delete.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="if(!doLookup()) return false;" src="../../Images/lookup.gif" width="28" height="28" border="0" id="lookup" AlternateText="Lookup Data" onMouseOver="this.src='../../Images/lookup2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/lookup.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return attach();" src="../../Images/attach.gif" width="28" height="28" border="0" id="attach" AlternateText="Attach Documents" onMouseOver="this.src='../../Images/attach2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/attach.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return Diary();" src="../../Images/diary.gif" width="28" height="28" border="0" id="diary" AlternateText="Diary" onMouseOver="this.src='../../Images/diary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/diary.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return recordSummary();" src="../../Images/recordsummary.gif" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" />
        </div>
      </div>
      <br />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Leave Management" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSLeave" id="TABSLeave">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="Leave" id="LINKTABSLeave">Leave Availability</a>
        </div>
        <div id="Div3" class="tabSpace" runat="server">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSleavedetail" id="TABSleavedetail">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="leavedetail" id="LINKTABSleavedetail">Leave Detail</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSleaveinformation" id="TABSleaveinformation">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="leaveinformation" id="LINKTABSleaveinformation">Leave Information</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABLeave" id="FORMTABLeave">
        <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="Instance/Leave/ClaimId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="pirowid" RMXRef="Instance/UI/FormVariables/SysExData/PiRowId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="pieid" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="leaverowid" RMXRef="Instance/Leave/LeaveRowId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="hdempid" RMXRef="Instance/UI/FormVariables/SysExData/PiRowId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="hdPostBack" RMXRef="Instance/UI/FormVariables/SysExData/IsPostBack" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="hdUpdateClick" RMXRef="Instance/UI/FormVariables/SysExData/IsUpdate" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="hdUseLeavesFlag" RMXRef="Instance/UI/FormVariables/SysExData/UseLeavesFlag" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="hdGridRowDeleteFlag" RMXRef="Instance/UI/FormVariables/SysExData/LeaveGridRowDeleteFlag" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="dateofclaim" RMXRef="Instance/UI/FormVariables/SysExData/DateOfClaim" RMXType="id" />
        <script language="JavaScript" xmlns="">
            if (document.forms[0].hdPostBack.value == "1")
        {       setDataChanged(true);
        document.forms[0].hdPostBack.value = "0";
        }
      </script>
        <div runat="server" class="half" id="div_labelheading" xmlns="">
          <span class="label">
            <asp:Label runat="server" id="labelheading" Text="Update" />
          </span>
        </div>
        <div runat="server" class="half" id="div_startdate" xmlns="">
          <asp:label runat="server" class="label" id="lbl_startdate" Text="Timeframe (From)" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/DateTFStart" id="startdate" tabindex="1" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_eligibilitydate" xmlns="">
          <asp:label runat="server" class="required" id="lbl_eligibilitydate" Text="Eligibility Date or End of Timeframe" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="eligibilitydate" RMXRef="/Instance/UI/FormVariables/SysExData/DateEligibility" RMXType="datebuttonscript" name="eligibilitydate" />
            <script language="JavaScript" SRC="">{var i;}</script>
            <asp:button runat="server" type="button" class="button" id="eligibilitydatebtn" Text="..." tabindex="3" onclientclick="CalcAgeforLeaveMgt(this,'EligibilityDate');" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="eligibilitydate" ID="rfv_eligibilitydate" />
            <cc1:CalendarExtender runat="server" id="eligibilitydate_ajax" TargetControlID="eligibilitydate" OnClientDateSelectionChanged="CalcAgeforLeaveMgt" />
          </span>
        </div>
        <div runat="server" class="half" id="div_enddate" xmlns="">
          <asp:label runat="server" class="label" id="lbl_enddate" Text="To" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/DateTFEnd" id="enddate" tabindex="6" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_leaveplan" xmlns="">
          <asp:label runat="server" class="required" id="lbl_leaveplan" Text="Leave Plan" />
          <span class="formw">
            <asp:Textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="codeLostFocus(this);" id="leaveplan" RMXRef="/Instance/UI/FormVariables/SysExData/LeavePlanName" name="leaveplan" TabIndex="3" readonly="true" style="background-color: silver;" />
            <asp:button runat="server" class="button" Text="..." id="leaveplanbtn" onclientclick="return lookupLeavePlanName('leaveplan','');" Tabindex="4" />
            <asp:TextBox style="display:none" runat="server" name="leaveplan_cid" value="" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="leaveplan" ID="rfv_leaveplan" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hoursavailable" xmlns="">
          <asp:label runat="server" class="label" id="lbl_hoursavailable" Text="Hours Available" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/HoursAvail" id="hoursavailable" tabindex="7" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_plancode" xmlns="">
          <asp:label runat="server" class="label" id="lbl_plancode" Text="Plan Code" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/LeavePlanCode" id="plancode" tabindex="4" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hourstaken" xmlns="">
          <asp:label runat="server" class="label" id="lbl_hourstaken" Text="Hours Taken" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/HoursUsed" id="hourstaken" tabindex="8" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hoursworked" xmlns="">
          <asp:label runat="server" class="label" id="lbl_hoursworked" Text="Hours Worked" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="hoursworked" RMXRef="/Instance/Leave/HoursWorked" RMXType="numeric" tabindex="5" maxlength="8" onChange="setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_maxduration" xmlns="">
          <asp:label runat="server" class="label" id="lbl_maxduration" Text="Maximum Duration" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/MaxDurHours" id="maxduration" tabindex="9" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_label1" xmlns="">
          <span class="label">
            <asp:Label runat="server" id="label1" Text="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_eligibilitystatus" xmlns="">
          <asp:label runat="server" class="label" id="lbl_eligibilitystatus" Text="Eligibility Status" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/EligStatusCode" id="eligibilitystatus" tabindex="10" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_btnUpdate" xmlns="">
          <script language="JavaScript" src="">{var i;}
        </script>
          <span class="formw">
            <asp:button class="button" runat="server" id="btnUpdate" RMXRef="" RMXType="buttonscript" Text="Update" onClientClick="UpdateLeave()" />
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABleavedetail" id="FORMTABleavedetail">
        <div runat="server" class="partial" id="div_LeaveDetailGrid" xmlns="">
          <asp:label runat="server" class="label" id="lbl_LeaveDetailGrid" Text="" />
          <span>
            <dg:UserControlDataGrid runat="server" ID="LeaveDetailGrid" GridName="LeaveDetailGrid" GridTitle="Leave Detail" Target="/Instance/UI/FormVariables/SysExData/LeaveDetail" Unique_Id="LDRowId" ShowRadioButton="" Width="680px" Height="150px" hidenodes="|LDRowId|LeaveRowId|" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="400" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="LeaveDetailSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/LeaveDetailSelectedId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="LeaveDetailGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/LeaveDetailGrid_Action" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="LeaveDetailGrid_RowAddedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/LeaveDetailGrid_RowAddedFlag" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="LeaveDetailGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/LeaveDetailGrid_RowDeletedFlag" RMXType="id" />
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABleaveinformation" id="FORMTABleaveinformation">
        <div runat="server" class="half" id="div_leavereason" xmlns="">
          <asp:label runat="server" class="label" id="lbl_leavereason" Text="Leave Reason" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="leavereason" CodeTable="LEAVE_REASON" ControlName="leavereason" RMXRef="/Instance/Leave/LeaveReasonCode" RMXType="code" tabindex="11" />
          </span>
        </div>
        <div runat="server" class="half" id="div_renewaldate" xmlns="">
          <asp:label runat="server" class="label" id="lbl_renewaldate" Text="Renewal Date" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="renewaldate" RMXRef="/Instance/Leave/DateRenew" RMXType="date" tabindex="22" />
            <cc1:CalendarExtender runat="server" id="renewaldate_ajax" TargetControlID="renewaldate" OnClientDateSelectionChanged="setDataChangedTrue" />
          </span>
        </div>
        <div runat="server" class="half" id="div_leavestatus" xmlns="">
          <asp:label runat="server" class="label" id="lbl_leavestatus" Text="Leave Status" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="leavestatus" CodeTable="LEAVE_STATUS" ControlName="leavestatus" RMXRef="/Instance/Leave/LeaveStatus" RMXType="code" tabindex="13" />
          </span>
        </div>
        <div runat="server" class="half" id="div_remindersent" xmlns="">
          <asp:label runat="server" class="label" id="lbl_remindersent" Text="Reminder Sent" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="remindersent" RMXRef="/Instance/Leave/DateRenewRemind" RMXType="date" tabindex="23" />
            <cc1:CalendarExtender runat="server" id="remindersent_ajax" TargetControlID="remindersent" OnClientDateSelectionChanged="setDataChangedTrue" />
          </span>
        </div>
        <div runat="server" class="half" id="div_authorization" xmlns="">
          <asp:label runat="server" class="label" id="lbl_authorization" Text="Authorization" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="authorization" CodeTable="LEAVE_AUTH_CODE" ControlName="authorization" RMXRef="/Instance/Leave/AuthorizationCode" RMXType="code" tabindex="15" />
          </span>
        </div>
        <div runat="server" class="half" id="div_relationship" xmlns="">
          <asp:label runat="server" class="label" id="lbl_relationship" Text="Relationship" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="relationship" CodeTable="RELATION_CODE" ControlName="relationship" RMXRef="/Instance/Leave/RelationshipCode" RMXType="code" tabindex="24" />
          </span>
        </div>
        <div runat="server" class="half" id="div_requestreceived" xmlns="">
          <asp:label runat="server" class="label" id="lbl_requestreceived" Text="Request Recieved" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="requestreceived" RMXRef="/Instance/Leave/DateReceived" RMXType="date" tabindex="16" />
            <cc1:CalendarExtender runat="server" id="requestreceived_ajax" TargetControlID="requestreceived" OnClientDateSelectionChanged="setDataChangedTrue" />
          </span>
        </div>
        <div runat="server" class="half" id="div_childdob" xmlns="">
          <asp:label runat="server" class="label" id="lbl_childdob" Text="Child's Date of Birth" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="childdob" RMXRef="/Instance/Leave/ChildDOB" RMXType="datebuttonscript" name="childdob" />
            <script language="JavaScript" SRC="">{var i;}</script>
            <asp:button runat="server" type="button" class="button" id="childdobbtn" Text="..." tabindex="27" onclientclick="CalcAgeforLeaveMgt(this,'ChildDOB');" />
            <cc1:CalendarExtender runat="server" id="childdob_ajax" TargetControlID="childdob" OnClientDateSelectionChanged="CalcAgeforLeaveMgt" />
          </span>
        </div>
        <div runat="server" class="half" id="div_labelage" xmlns="">
          <span class="label">
            <asp:Label runat="server" id="labelage" Text="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_Age" xmlns="">
          <asp:label runat="server" class="label" id="lbl_Age" Text="Age" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="Instance/UI/FormVariables/SysExData/ChildAge" id="Age" tabindex="27" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_requestreviewed" xmlns="">
          <asp:label runat="server" class="label" id="lbl_requestreviewed" Text="Request Reviewed" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="requestreviewed" RMXRef="/Instance/Leave/DateReviewed" RMXType="date" tabindex="18" />
            <cc1:CalendarExtender runat="server" id="requestreviewed_ajax" TargetControlID="requestreviewed" OnClientDateSelectionChanged="setDataChangedTrue" />
          </span>
        </div>
        <div runat="server" class="half" id="div_frequency" xmlns="">
          <asp:label runat="server" class="label" id="lbl_frequency" Text="Frequency" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="frequency" CodeTable="LEAVE_FREQ_CODE" ControlName="frequency" RMXRef="/Instance/Leave/FrequencyCode" RMXType="code" tabindex="28" />
          </span>
        </div>
        <div runat="server" class="half" id="div_reviewereid" xmlns="">
          <asp:label runat="server" class="label" id="lbl_reviewereid" Text="Reviewed By" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="reviewereid" RMXRef="/Instance/Leave/ReviewerEid" RMXType="eidlookup" cancelledvalue="" tabindex="20" />
            <input type="button" class="button" value="..." name="reviewereidbtn" tabindex="21" onclick="lookupData('reviewereid','LEAVE_RB_ENTITY',4,'reviewereid',2)" />
            <asp:TextBox style="display:none" runat="server" id="reviewereid_cid" RMXref="/Instance/Leave/ReviewerEid/@codeid" cancelledvalue="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_leavedescription" xmlns="">
          <asp:label runat="server" class="label" id="lbl_leavedescription" Text="Leave Description" />
          <span class="formw">
            <asp:TextBox runat="Server" id="leavedescription" RMXRef="/Instance/Leave/LeaveDesc" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="30" TextMode="MultiLine" Columns="30" rows="5" />
            <input type="button" class="button" value="..." name="leavedescriptionbtnMemo" tabindex="31" id="leavedescriptionbtnMemo" onclick=" EditHTMLMemo('leavedescription','yes')" />
            <asp:TextBox style="display:none" runat="server" RMXRef="/Instance/Leave/LeaveDesc_HTMLComments" id="leavedescription_HTML" />
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
        <asp:TextBox style="display:none" runat="server" id="supp_leave_row_id" RMXRef="/Instance/*/Supplementals/LEAVE_ROW_ID" RMXType="id" />
      </div>
      <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
          <asp:Button class="button" runat="Server" id="BackToParent" Text="Back to Claim" OnClientClick="if(!( XFormHandler('','1','back')))return false;" PostBackUrl="?" />
        </div>
        <div class="formButton" runat="server" id="div_btnLeaveHistory">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnLeaveHistory" RMXRef="" Text="Leave History" onClientClick="gotoLeaveHistory();" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="claimid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="claimid,claimnumber,hdempid,dateofclaim" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="leave" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="leaverowid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="60000" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="64000" />
      <asp:TextBox style="display:none" runat="server" id="LPRowId" RMXRef="/Instance/Leave/LPRowId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="Leave" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;Leave&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/Leave&gt;" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" name="formname" value="Leave" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" value="eligibilitydate|leaveplan|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" value="startdate|" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>