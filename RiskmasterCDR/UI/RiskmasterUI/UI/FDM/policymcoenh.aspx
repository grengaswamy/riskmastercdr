<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="policymcoenh.aspx.cs"  Inherits="Riskmaster.UI.FDM.Policymcoenh" ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Policy MCO</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <script language="JavaScript" src="../../Scripts/EnhPolicy.js">{var i;}</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_new" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateNew" src="../../Images/new.gif" width="28" height="28" border="0" id="new" AlternateText="New" onMouseOver="this.src='../../Images/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/new.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/save.gif" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/save.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateFirst" src="../../Images/first.gif" width="28" height="28" border="0" id="movefirst" AlternateText="Move First" onMouseOver="this.src='../../Images/first2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/first.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigatePrev" src="../../Images/prev.gif" width="28" height="28" border="0" id="moveprevious" AlternateText="Move Previous" onMouseOver="this.src='../../Images/prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/prev.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movenext" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateNext" src="../../Images/next.gif" width="28" height="28" border="0" id="movenext" AlternateText="Move Next" onMouseOver="this.src='../../Images/next2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/next.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movelast" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateLast" src="../../Images/last.gif" width="28" height="28" border="0" id="movelast" AlternateText="Move Last" onMouseOver="this.src='../../Images/last2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/last.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" src="../../Images/delete.gif" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/delete2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/delete.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_lookup" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="if(!doLookup()) return false;" src="../../Images/lookup.gif" width="28" height="28" border="0" id="lookup" AlternateText="Lookup Data" onMouseOver="this.src='../../Images/lookup2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/lookup.gif';this.style.zoom='100%'" />
        </div>
      </div>
      <br />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Policy MCO" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSpolicymcoinfo" id="TABSpolicymcoinfo">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="policymcoinfo" id="LINKTABSpolicymcoinfo">MCO Information</a>
        </div>
        <div id="Div3" class="tabSpace" runat="server">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" style="height:310px;overflow:auto" name="FORMTABpolicymcoinfo" id="FORMTABpolicymcoinfo">
        <asp:TextBox style="display:none" runat="server" id="policyid" RMXRef="/Instance/PolicyXMcoEnh/PolicyId" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_mcoeid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_mcoeid" Text="Policy MCO" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="mcoeid" RMXRef="/Instance/PolicyXMcoEnh/McoEid" RMXType="eidlookup" cancelledvalue="" tabindex="1" />
            <input type="button" class="button" value="..." name="mcoeidbtn" tabindex="2" onclick="lookupData('mcoeid','MCO',4,'mcoeid',2)" />
            <asp:TextBox style="display:none" runat="server" id="mcoeid_cid" RMXref="/Instance/PolicyXMcoEnh/McoEid/@codeid" cancelledvalue="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_mcobegindate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mcobegindate" Text="Begin Date" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="mcobegindate" RMXRef="/Instance/PolicyXMcoEnh/McoBeginDate" RMXType="date" tabindex="3" onchange="setDataChanged(true);ValidateBeginEndDate()" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="mcobegindatebtn" tabindex="4" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "mcobegindate",
					ifFormat : "%m/%d/%Y",
					button : "mcobegindatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_mcoenddate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_mcoenddate" Text="End Date" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="mcoenddate" RMXRef="/Instance/PolicyXMcoEnh/McoEndDate" RMXType="date" tabindex="5" onchange="setDataChanged(true);ValidateBeginEndDate()" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="mcoenddatebtn" tabindex="6" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "mcoenddate",
					ifFormat : "%m/%d/%Y",
					button : "mcoenddatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_origvalues" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="origvalues" RMXRef="/Instance/UI/FormVariables/SysExData/OrigValues" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_polxmcoenhrowid" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="polxmcoenhrowid" RMXRef="/Instance/PolicyXMcoEnh/PolXMcoEnhRowId" RMXType="hidden" />
          </span>
        </div>
      </div>
      <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
          <asp:Button class="button" runat="Server" id="BackToParent" Text="Back to Policy" OnClientClick="if(!( XFormHandler('SysCmd=0','1','back')))return false;" PostBackUrl="?SysCmd=0" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="policy" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="policyid" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="23000" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="policymcoenh" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="polxmcoenhrowid" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="9000" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="PolicyXMcoEnh" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;PolicyXMco&gt;&lt;/PolicyXMco&gt;" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="policymcoenh" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="mcoeid|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="mcoeid|" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>