<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="policycoverage.aspx.cs"
    Inherits="Riskmaster.UI.FDM.Policycoverage" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Policy Coverage</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift">
                <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28"
                                height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                                src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                                validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28"
                                height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28"
                                height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28"
                                height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28"
                                height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                                width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="doLookup()" src="Images/lookup.gif"
                                width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='Images/lookup2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/lookup.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="Diary()" src="Images/diary.gif" width="28"
                                height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='Images/diary2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/diary.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                                width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                                onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" />
                        </td>
                    </tr>
                </table>
            </div>
    <br />
    <div class="msgheader" id="formtitle">
        Policy Coverage</div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0">
                    <tr>
                        <td class="Selected" nowrap="true" name="TABScoverage" id="TABScoverage">
                            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="coverage"
                                id="LINKTABScoverage"><span style="text-decoration: none">Coverage Information</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSsuppgroup" id="TABSsuppgroup">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td valign="top" nowrap="true" />
                    </tr>
                </table>
                <div style="position: relative; left: 0; top: 0; width: 870px; height: 350px; overflow: auto"
                    class="singletopborder">
                    <table border="0" cellspacing="0" celpadding="0" width="95%" height="95%">
                        <tr>
                            <td valign="top">
                                <table border="0" name="FORMTABcoverage" id="FORMTABcoverage">
                                    <tr>
                                        <asp:textbox style="display: none" runat="server" id="policyid" rmxref="/Instance/PolicyXCvgType/PolicyId" /><asp:textbox
                                            style="display: none" runat="server" id="polcvgrowid" rmxref="/Instance/PolicyXCvgType/PolcvgRowId" /><td
                                                nowrap="true" id="coveragetypecode_ctlcol" xmlns="">
                                                <b><u>Coverage Type</u></b>
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="coveragetypecode"
                                                rmxref="/Instance/PolicyXCvgType/CoverageTypeCode" cancelledvalue="" tabindex="1"
                                                onblur="codeLostFocus(this.id);" /><input type="button" class="CodeLookupControl"
                                                    name="coveragetypecodebtn" onclick="selectCode('COVERAGE_TYPE','coveragetypecode')" /><asp:textbox
                                                        style="display: none" runat="server" id="coveragetypecode_cid" rmxref="/Instance/PolicyXCvgType/CoverageTypeCode/@codeid"
                                                        cancelledvalue="" tabindex="2" /><asp:requiredfieldvalidator validationgroup="vgSave"
                                                            errormessage="Required" runat="server" controltovalidate="coveragetypecode" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="brokername_ctlcol" xmlns="">
                                            Broker Last Name
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="brokername"
                                                rmxref="/Instance/PolicyXCvgType/BrokerName" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <asp:textbox style="display: none" runat="server" id="hdCusListLookup" rmxref="Instance/UI/FormVariables/SysExData/IsUpdate" /></tr>
                                    <tr>
                                        <td nowrap="true" id="policylimit_ctlcol" xmlns="">
                                            Policy Limit
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" id="policylimit" rmxref="/Instance/PolicyXCvgType/PolicyLimit"
                                                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="4" onchange=";setDataChanged(true);" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="occurrencelimit_ctlcol" xmlns="">
                                            Event/Occurrence Limit
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" id="occurrencelimit" rmxref="/Instance/PolicyXCvgType/OccurrenceLimit"
                                                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="5" onchange=";setDataChanged(true);" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="claimlimit_ctlcol" xmlns="">
                                            Claim Limit
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" id="claimlimit" rmxref="/Instance/PolicyXCvgType/ClaimLimit"
                                                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="6" onchange=";setDataChanged(true);" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="totalpayments_ctlcol" xmlns="">
                                            Total Payments
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" id="totalpayments" rmxref="/Instance/PolicyXCvgType/TotalPayments"
                                                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="7" onchange=";setDataChanged(true);" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="selfinsurededuct_ctlcol" xmlns="">
                                            SIR/Deductible
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" id="selfinsurededuct" rmxref="/Instance/PolicyXCvgType/SelfInsureDeduct"
                                                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="8" onchange=";setDataChanged(true);" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="nextpolicyid_ctlcol" xmlns="">
                                            Next Policy
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onkeydown="eatKeystrokes();"
                                                id="nextpolicyid" rmxref="/Instance/PolicyXCvgType/NextPolicyId/@defaultdetail"
                                                tabindex="9" /><asp:textbox runat="server" style="display: none" rmxref="/Instance/PolicyXCvgType/NextPolicyId"
                                                    id="nextpolicyid_cid" /><input type="button" class="button" value="..." name="nextpolicyidbtn"
                                                        onclick="lookupData('nextpolicyid','policy',6,'nextpolicyid',9)" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="cancelnoticedays_ctlcol" xmlns="">
                                            Cancellation Notice (Days)
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onblur="numLostFocus(this);" id="cancelnoticedays"
                                                rmxref="/Instance/PolicyXCvgType/CancelNoticeDays" tabindex="11" onchange="setDataChanged(true);" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="notificationuid_ctlcol" xmlns="">
                                            Notification User
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox size="30" runat="server" onchange="CULFieldTextChanged(this,'fdmpage');"
                                                rmxref="/Instance/UI/FormVariables/SysExData/RMSysUserName" id="notificationuid"
                                                tabindex="12" onblur="&#xA;                  CULFieldTextChanged(this,'fdmpage');&#xA;                " /><script
                                                    language="JavaScript" src="">{var i;}
                                                </script><input type="button" class="button" id="notificationuidbtn" value="..."
                                                    tabindex="13" onclick="&#xA;                OpenCustomizedUserList('notificationuid','fdmpage')&#xA;              " />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="exceptions_ctlcol" xmlns="">
                                            Exceptions
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" id="exceptions" rmxref="/Instance/PolicyXCvgType/Exceptions"
                                                readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                                                tabindex="14" cols="30" rows="5">
                                            </asp:textbox><input type="button" class="button" value="..." name="exceptionsbtnMemo"
                                                tabindex="15" id="exceptionsbtnMemo" onclick="EditMemo('exceptions','')" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="remarks_ctlcol" xmlns="">
                                            Remarks
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" id="remarks" rmxref="/Instance/PolicyXCvgType/Remarks"
                                                readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                                                tabindex="16" cols="30" rows="5">
                                            </asp:textbox><input type="button" class="button" value="..." name="remarksbtnMemo"
                                                tabindex="17" id="remarksbtnMemo" onclick="EditMemo('remarks','')" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" style="display: none;" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
                                    <tr>
                                        <asp:textbox style="display: none" runat="server" id="supp_polcvg_row_id" rmxref="/Instance/*/Supplementals/POLCVG_ROW_ID" /></tr>
                                </table>
                            </td>
                            <td valign="top" />
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td>
                            <asp:button id="Button1" class="button" runat="Server" text="Back to Policy" onclientclick="&#xA;            XFormHandler('','','1','back')&#xA;          " />
                        </td>
                    </tr>
                </table>
                <asp:textbox style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
                    text="policy" /><asp:textbox style="display: none" runat="server" id="SysFormPIdName"
                        rmxref="Instance/UI/FormVariables/SysFormPIdName" text="policyid" /><asp:textbox
                            style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
                            text="" /><asp:textbox style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
                                text="" /><asp:textbox style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                    text="" /><asp:textbox style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                        text="policycoverage" /><asp:textbox style="display: none" runat="server" id="SysFormIdName"
                                            rmxref="Instance/UI/FormVariables/SysFormIdName" text="polcvgrowid" /><asp:textbox
                                                style="display: none" runat="server" id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId"
                                                text="" /><asp:textbox style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                                    text="9150" /><asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
                                                        text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
                                                            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" text="" /><asp:textbox style="display: none"
                                                                runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                                                                text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText" rmxref="Instance/UI/FormVariables/SysCmdText"
                                                                    text="Navigate" /><asp:textbox style="display: none" runat="server" id="SysClassName"
                                                                        rmxref="Instance/UI/FormVariables/SysClassName" text="PolicyXCvgType" /><asp:textbox
                                                                            style="display: none" runat="server" id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                                                                            name="SysSerializationConfig" text="&lt;PolicyXCvgType&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/PolicyXCvgType&gt;" /><asp:textbox
                                                                                style="display: none" runat="server" id="hdnUserId" rmxref="/Instance/PolicyXCvgType/NotificationUid"
                                                                                text="" /><asp:textbox style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                                                                    text="" /><asp:textbox style="display: none" runat="server" name="formname" value="policycoverage" /><asp:textbox
                                                                                        style="display: none" runat="server" name="SysRequired" value="coveragetypecode_cid|" /><asp:textbox
                                                                                            style="display: none" runat="server" name="SysFocusFields" value="coveragetypecode|" /><input
                                                                                                type="hidden" id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible"
                                                                                                    style="display: none" /><asp:textbox runat="server" id="SysLookupClass" style="display: none" /><asp:textbox
                                                                                                        runat="server" id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server"
                                                                                                            id="SysLookupAttachNodePath" style="display: none" /><asp:textbox runat="server"
                                                                                                                id="SysLookupResultConfig" style="display: none" /><input type="hidden" value="rmx-widget-handle-2"
                                                                                                                    id="SysWindowId" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
