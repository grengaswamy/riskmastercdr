﻿<%@ Page="" Language="C#" AutoEventWireup="true" CodeBehind="fallinfo.aspx.cs"  Inherits="Riskmaster.UI.FDM.Fallinfo" ValidateRequest="false" %>
  <%@ Register="" assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
      <head runat="server">
        <title>Fall Information</title>
        <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
        <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
        <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />
        <script language="JavaScript" src="Scripts/form.js">
          {var i;}
        </script>
        <script language="JavaScript" src="Scripts/drift.js">
          {var i;}
        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
      </head>
      <body class="10pt" onload="pageLoaded();">
        <form name="frmData" id="frmData" runat="server">
          <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
          <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
          <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
          <asp:ScriptManager ID="SMgr" runat="server" />
          <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
            <div class="toolBarButton" runat="server" id="div_save" xmlns="">
              <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="Images/save.gif" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/save.gif';this.style.zoom='100%'" />
            </div>
            <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
              <asp:ImageButton runat="server" OnClientClick="Diary()" src="Images/diary.gif" width="28" height="28" border="0" id="diary" AlternateText="Diary" onMouseOver="this.src='Images/diary2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/diary.gif';this.style.zoom='100%'" />
            </div>
            <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
              <asp:ImageButton runat="server" OnClientClick="recordSummary()" src="Images/recordsummary.gif" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/recordsummary.gif';this.style.zoom='100%'" />
            </div>
          </div>
          <br />
          <div class="msgheader" id="div_formtitle" runat="server">
            <asp:label id="formtitle" runat="server" Text="Fall Information" />
            <asp:label id="formsubtitle" runat="server" Text="" />
          </div>
          <div class="errtextheader" runat="server">
            <asp:label id="formdemotitle" runat="server" Text="" />
          </div>
          <div class="tabGroup" runat="server">
            <div class="Selected" nowrap="true" runat="server" name="TABSfalldetails" id="TABSfalldetails">
              <a class="Selected" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="falldetails" id="LINKTABSfalldetails">
                <span style="text-decoration:none">Fall Details</span>
              </a>
            </div>
            <div class="tabSpace" runat="server">&nbsp;&nbsp;</div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
              <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">
                <span style="text-decoration:none">Supplementals</span>
              </a>
            </div>
            <div class="tabSpace">&nbsp;&nbsp;</div>
          </div>
          <div class="singletopborder" runat="server" name="FORMTABfalldetails" id="FORMTABfalldetails">
            <asp:TextBox style="display:none" runat="server" id="eventid" RMXRef="/Instance/FallIndicator/EventId" RMXType="id" />
            <asp:TextBox style="display:none" runat="server" id="eventnumber" RMXRef="/Instance/UI/FormVariables/SysExData/EventNumber" RMXType="id" />
            <div runat="server" class="half" id="div_condpriorcode" xmlns="">
              <span class="label">Condition Prior</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="condpriorcode" RMXRef="/Instance/FallIndicator/CondPriorCode" RMXType="code" cancelledvalue="" tabindex="1" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="condpriorcodebtn" onclientclick="return selectCode('CONDITION_PRIOR','condpriorcode');" tabindex="2" />
                <asp:TextBox style="display:none" runat="server" id="condpriorcode_cid" RMXref="/Instance/FallIndicator/CondPriorCode/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_railpositioncode" xmlns="">
              <span class="label">Side Rail Position</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="railpositioncode" RMXRef="/Instance/FallIndicator/RailPositionCode" RMXType="code" cancelledvalue="" tabindex="3" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="railpositioncodebtn" onclientclick="return selectCode('SIDE_RAIL_POSITION','railpositioncode');" tabindex="4" />
                <asp:TextBox style="display:none" runat="server" id="railpositioncode_cid" RMXref="/Instance/FallIndicator/RailPositionCode/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_restraintcode" xmlns="">
              <span class="label">Restraints</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="restraintcode" RMXRef="/Instance/FallIndicator/RestraintCode" RMXType="code" cancelledvalue="" tabindex="5" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="restraintcodebtn" onclientclick="return selectCode('RESTRAINTS','restraintcode');" tabindex="6" />
                <asp:TextBox style="display:none" runat="server" id="restraintcode_cid" RMXref="/Instance/FallIndicator/RestraintCode/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_bedpositioncode" xmlns="">
              <span class="label">Bed Position</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="bedpositioncode" RMXRef="/Instance/FallIndicator/BedPositionCode" RMXType="code" cancelledvalue="" tabindex="7" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="bedpositioncodebtn" onclientclick="return selectCode('BED_POSITION_CODES','bedpositioncode');" tabindex="8" />
                <asp:TextBox style="display:none" runat="server" id="bedpositioncode_cid" RMXref="/Instance/FallIndicator/BedPositionCode/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_calllightcode" xmlns="">
              <span class="label">Call Light Within Reach</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="calllightcode" RMXRef="/Instance/FallIndicator/CallLightCode" RMXType="code" cancelledvalue="" tabindex="9" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="calllightcodebtn" onclientclick="return selectCode('YES_NO','calllightcode');" tabindex="10" />
                <asp:TextBox style="display:none" runat="server" id="calllightcode_cid" RMXref="/Instance/FallIndicator/CallLightCode/@codeid" cancelledvalue="" />
              </span>
            </div>
          </div>
          <div class="singletopborder" style="display:none;" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
            <asp:TextBox style="display:none" runat="server" id="supp_event_id" RMXRef="/Instance/*/Supplementals/EVENT_ID" RMXType="id" />
            <div runat="server" class="half" id="div_supp_fallinfo_text" xmlns="">
              <span class="label">FallInfo</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_fallinfo_text" RMXRef="/Instance/*/Supplementals/FALLINFO_TEXT" RMXType="text" />
              </span>
            </div>
          </div>
          <div class="formButtonGroup" runat="server">
            <div class="formButton" runat="server" id="div_btnBack">
              <asp:Button class="button" runat="Server" id="btnBack" Text="Back to Event" OnClientClick="if(!( XFormHandler('','1','back')))return false;" PostBackUrl="?" />
            </div>
          </div>
          <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
          <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="FallIndicator" />
          <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;FallIndicator&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/FallIndicator&gt;" />
          <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="fallinfo" />
          <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="11900" />
          <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="11000" />
          <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="eventid" />
          <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="eventid" />
          <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="event" />
          <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="eventnumber" />
          <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysSupplemental" RMXRef="Instance/UI/FormVariables/SysSupplemental" RMXType="hidden" Text="formname=fall_ind_supp&sys_formidname=event_id&event_id=%eventid%&sys_formpform=fallinfo&sys_formpidname=event_id" />
          <asp:TextBox style="display:none" runat="server" name="formname" value="fallinfo" />
          <asp:TextBox style="display:none" runat="server" name="SysRequired" value="" />
          <asp:TextBox style="display:none" runat="server" name="SysFocusFields" value="condpriorcode|" />
          <input type="hidden" id="hdSaveButtonClicked" />
          <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
          <asp:TextBox runat="server" id="SysLookupClass" style="display:none" />
          <asp:TextBox runat="server" id="SysLookupRecordId" style="display:none" />
          <asp:TextBox runat="server" id="SysLookupAttachNodePath" style="display:none" />
          <asp:TextBox runat="server" id="SysLookupResultConfig" style="display:none" />
          <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
        </form>
      </body>
    </html>