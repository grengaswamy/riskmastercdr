﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.AppHelpers;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using Riskmaster.AppHelpers;
using System.Text;
using System.Xml.XPath;
using Riskmaster.Common;
using Riskmaster.UI.Diaries;

namespace Riskmaster.UI.FDM
{
    public partial class Diarydetails : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    TextBox txtNotRouteableFlag = (TextBox)this.FindControl("txtNotRouteableFlag");
                    TextBox txtNotRollableFlag = (TextBox)this.FindControl("txtNotRollableFlag");
                    TextBox hdnOtherDiariesEditPerm = (TextBox)this.FindControl("hdnOtherDiariesEditPerm");

                    #region Saving QueryString values into View State
                    if (Request.QueryString["perm"] != null)
                    {
                        if (hdnOtherDiariesEditPerm != null)
                            hdnOtherDiariesEditPerm.Text = Request.QueryString["perm"];
                    }
                    else
                    {
                        if (hdnOtherDiariesEditPerm != null)
                            hdnOtherDiariesEditPerm.Text = "0";
                    }
                    if (Request.QueryString["Routeableflag"] != null)
                    {
                        if (txtNotRouteableFlag != null)
                            txtNotRouteableFlag.Text = Request.QueryString["Routeableflag"];
                    }
                    else
                    {
                        if (txtNotRouteableFlag != null)
                            txtNotRouteableFlag.Text = "";
                    }
                    if (Request.QueryString["Rollableflag"] != null)
                    {
                        if (txtNotRollableFlag != null)
                            txtNotRollableFlag.Text = Request.QueryString["Rollableflag"];
                    }
                    else
                    {
                        if (txtNotRollableFlag != null)
                            txtNotRollableFlag.Text = "";
                    }


                    if (Request.QueryString["entryid"] != null)
                    {
                        ViewState["entryid"] = Request.QueryString["entryid"];
                    }
                    else
                    {
                        ViewState["entryid"] = "";
                    }


                    if (Request.QueryString["assigninguser"] != null)
                    {
                        ViewState["assigninguser"] = Request.QueryString["assigninguser"];
                    }
                    else
                    {
                        ViewState["assigninguser"] = "";
                    }


                    if (Request.QueryString["attachprompt"] != null)
                    {
                        ViewState["attachprompt"] = Request.QueryString["attachprompt"];
                    }
                    else
                    {
                        ViewState["attachprompt"] = "";
                    }

                    if (Request.QueryString["creationdate"] != null)
                    {
                        ViewState["creationdate"] = Request.QueryString["creationdate"];
                    }
                    else
                    {
                        ViewState["creationdate"] = "";
                    }

                    if (Request.QueryString["assignedusername"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assignedusername"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }

                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";
                    }

                    if (Request.QueryString["attachrecord"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecord"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }
                    #endregion

                    //##DiaryDetails
                    string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DiaryDetails.aspx"), "DiaryDetailsValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                    ClientScript.RegisterStartupScript(this.GetType(), "DiaryDetailsValidations", sValidationResources, true);
                }

                FDMPageLoad();
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

        public override void ModifyControls()
        {
            XmlNode TempNode = null;
            XmlNodeList workActivitiesNodeList = null;
            Label WrkAct = (Label)this.FindControl("WrkAct");
            Label Priority = (Label)this.FindControl("Priority");
            HtmlImage img_Priority = (HtmlImage)this.FindControl("img_Priority");
            base.ModifyControls();
            XmlDocument oXmlData = base.Data;
            try
            {
                if (oXmlData != null)
                {

                    TempNode = oXmlData.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/LoginUser");
                    if (TempNode != null && String.IsNullOrEmpty(ViewState["assigneduser"].ToString()))
                    {
                        if (!String.IsNullOrEmpty(TempNode.InnerText))
                        {
                            ViewState["assigneduser"] = TempNode.InnerText;
                        }

                    }

                    TempNode = oXmlData.SelectSingleNode("//Document/ParamList/Param[@name='SysPropertyStore']/WpaDiaryEntry/Priority");
                    if (TempNode != null)
                    {
                        if (TempNode.InnerText == "2")
                        {
                            img_Priority.Src = "~/Images/important.gif";
                            Priority.Text = RMXResourceProvider.GetSpecificObject("Important", RMXResourceProvider.PageId("DiaryDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                        }
                        else if (TempNode.InnerText == "3")
                        {
                            img_Priority.Src = "~/Images/required.gif";
                            Priority.Text = RMXResourceProvider.GetSpecificObject("Required", RMXResourceProvider.PageId("DiaryDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                        }
                        else
                        {
                            img_Priority.Visible = false;
                            Priority.Text = RMXResourceProvider.GetSpecificObject("Optional", RMXResourceProvider.PageId("DiaryDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                        }

                    }

                    workActivitiesNodeList = oXmlData.SelectNodes("//ActivityList/option");
                    foreach (XmlNode activityNode in workActivitiesNodeList)
                    {
                        if (WrkAct != null)
                            WrkAct.Text = WrkAct.Text + activityNode.InnerText + "<br/>";
                    }

                }
            }
            finally
            {
                if (workActivitiesNodeList != null)
                    workActivitiesNodeList = null;
                if (TempNode != null)
                    TempNode = null;
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                //RMA - 4691
                //Server.Transfer("/RiskmasterUI/UI/Diaries/EditDiary.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&attachprompt=" + ViewState["attachprompt"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
                Response.Redirect("/RiskmasterUI/UI/FDM/creatediary.aspx?entryid=" + ViewState["entryid"].ToString() + "&recordID=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&attachprompt=" + ViewState["attachprompt"].ToString() + "&assignedusername=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecord=" + ViewState["attachrecordid"].ToString() + "&CalledBy=creatediary", false);
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("/RiskmasterUI/UI/Diaries/CompleteDiary.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

        protected void btnRoute_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("/RiskmasterUI/UI/Diaries/RouteDiary.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

        protected void btnRoll_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("/RiskmasterUI/UI/Diaries/RollDiary.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&creationdate=" + ViewState["creationdate"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("/RiskmasterUI/UI/Diaries/DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("/RiskmasterUI/UI/Diaries/DiaryHistory.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                //Server.Transfer("PrintListing.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&creationdate=" + ViewState["creationdate"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() +"&screen=selected", false);

                string script = "/RiskmasterUI/UI/Diaries/PrintListing.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&creationdate=" + ViewState["creationdate"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&screen=selected";
                ClientScript.RegisterStartupScript(this.GetType(), "popup", "window.open('" + script + "','DiaryList','width=800,height=800' + ',top=' + (screen.availHeight - 800) / 2 + ',left=' + (screen.availWidth - 800) / 2 + ',resizable=yes,scrollbars=yes')", true);
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }
    }
}