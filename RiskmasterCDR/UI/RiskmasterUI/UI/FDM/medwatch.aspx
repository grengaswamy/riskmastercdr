﻿<%@ Page="" Language="C#" AutoEventWireup="true" CodeBehind="medwatch.aspx.cs"  Inherits="Riskmaster.UI.FDM.Medwatch" ValidateRequest="false" %>
  <%@ Register="" assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
      <head runat="server">
        <title>MedWatch</title>
        <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
        <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
        <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />
        <script language="JavaScript" src="Scripts/form.js">
          {var i;}
        </script>
        <script language="JavaScript" src="Scripts/drift.js">
          {var i;}
        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
      </head>
      <body class="10pt" onload="pageLoaded();">
        <form name="frmData" id="frmData" runat="server">
          <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
          <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
          <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
          <asp:ScriptManager ID="SMgr" runat="server" />
          <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
            <div class="toolBarButton" runat="server" id="div_save" xmlns="">
              <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="Images/save.gif" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/save.gif';this.style.zoom='100%'" />
            </div>
            <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
              <asp:ImageButton runat="server" OnClientClick="Diary()" src="Images/diary.gif" width="28" height="28" border="0" id="diary" AlternateText="Diary" onMouseOver="this.src='Images/diary2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/diary.gif';this.style.zoom='100%'" />
            </div>
            <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
              <asp:ImageButton runat="server" OnClientClick="recordSummary()" src="Images/recordsummary.gif" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/recordsummary.gif';this.style.zoom='100%'" />
            </div>
          </div>
          <br />
          <div class="msgheader" id="div_formtitle" runat="server">
            <asp:label id="formtitle" runat="server" Text="MedWatch" />
            <asp:label id="formsubtitle" runat="server" Text="" />
          </div>
          <div class="errtextheader" runat="server">
            <asp:label id="formdemotitle" runat="server" Text="" />
          </div>
          <div class="tabGroup" runat="server">
            <div class="Selected" nowrap="true" runat="server" name="TABSmedwatch" id="TABSmedwatch">
              <a class="Selected" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="medwatch" id="LINKTABSmedwatch">
                <span style="text-decoration:none">MedWatch Info</span>
              </a>
            </div>
            <div class="tabSpace" runat="server">&nbsp;&nbsp;</div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSequipmentindicator" id="TABSequipmentindicator">
              <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="equipmentindicator" id="LINKTABSequipmentindicator">
                <span style="text-decoration:none">Equipment Indicator</span>
              </a>
            </div>
            <div class="tabSpace">&nbsp;&nbsp;</div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSmedicationindicator" id="TABSmedicationindicator">
              <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="medicationindicator" id="LINKTABSmedicationindicator">
                <span style="text-decoration:none">Medication Indicator</span>
              </a>
            </div>
            <div class="tabSpace">&nbsp;&nbsp;</div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
              <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">
                <span style="text-decoration:none">Supplementals</span>
              </a>
            </div>
            <div class="tabSpace">&nbsp;&nbsp;</div>
          </div>
          <div class="singletopborder" runat="server" name="FORMTABmedwatch" id="FORMTABmedwatch">
            <asp:TextBox style="display:none" runat="server" id="eventid" RMXRef="/Instance/EventMedwatch/EventId" RMXType="id" />
            <asp:TextBox style="display:none" runat="server" id="eventnumber" RMXRef="Instance/UI/FormVariables/SysExData/EventNumber" RMXType="id" />
            <div runat="server" class="half" id="div_replastname" xmlns="">
              <span class="required">Reporter's Last Name</span>
              <span class="formw">
                <asp:Textbox runat="server" onchange="lookupTextChanged(this);" tabindex="1" onblur="lookupLostFocus(this);" RMXRef="/Instance/EventMedwatch/RptdByEntity/LastName" RMXType="entitylookup" id="replastname" />
                <asp:RequiredFieldValidator ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="replastname" />
                <asp:Textbox runat="server" style="display:none" Text="1" id="replastname_creatable" />
                <asp:button runat="server" class="button" tabindex="2" id="replastnamebtn" onclientclick="lookupDataFDM('replastname','',4,'rep',1,'/Instance/EventMedwatch/RptdByEntity')" Text="..." />
              </span>
            </div>
            <div runat="server" class="half" id="div_rptdbyproflag" xmlns="">
              <span class="label">Health Professional</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="rptdbyproflag" RMXRef="/Instance/EventMedwatch/RptdByProFlag" RMXType="checkbox" tabindex="17" />
              </span>
            </div>
            <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="/Instance/Claim/ClaimId" RMXType="id" />
            <asp:TextBox style="display:none" runat="server" id="claimnumber" RMXRef="/Instance/Claim/ClaimNumber" RMXType="id" />
            <asp:TextBox style="display:none" runat="server" id="RptdByEntityId" RMXRef="/Instance/EventMedwatch/RptdByEntity/EntityId" RMXType="id" />
            <asp:TextBox style="display:none" runat="server" id="ManufacturerEntityId" RMXRef="/Instance/EventMedwatch/ManufacturerEntity/EntityId" RMXType="id" />
            <div runat="server" class="half" id="div_repfirstname" xmlns="">
              <span class="label">First Name</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="repfirstname" RMXRef="/Instance/EventMedwatch/RptdByEntity/FirstName" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_relevanthistory" xmlns="">
              <span class="label">Relevant History</span>
              <span class="formw">
                <asp:TextBox runat="Server" id="relevanthistory" RMXRef="/Instance/EventMedwatch/RelevantHistory" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="18" TextMode="MultiLine" Columns="30" rows="5" />
                <input type="button" class="button" value="..." name="relevanthistorybtnMemo" tabindex="19" id="relevanthistorybtnMemo" onclick="EditMemo('relevanthistory','')" />
              </span>
            </div>
            <div runat="server" class="half" id="div_repaddr1" xmlns="">
              <span class="label">Address 1</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="repaddr1" RMXRef="/Instance/EventMedwatch/RptdByEntity/Addr1" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_daterptdtomfg" xmlns="">
              <span class="label">Date Reported To Manufacturer</span>
              <span class="formw">
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);" id="daterptdtomfg" RMXRef="/Instance/EventMedwatch/DateRptdToMfg" RMXType="date" tabindex="20" />
                <cc1:CalendarExtender runat="server" id="daterptdtomfg_ajax" TargetControlID="daterptdtomfg" />
              </span>
            </div>
            <div runat="server" class="half" id="div_repaddr2" xmlns="">
              <span class="label">Address 2</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="repaddr2" RMXRef="/Instance/EventMedwatch/RptdByEntity/Addr2" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_daterptdtofda" xmlns="">
              <span class="label">Date Reported To FDA</span>
              <span class="formw">
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);" id="daterptdtofda" RMXRef="/Instance/EventMedwatch/DateRptdToFda" RMXType="date" tabindex="22" />
                <cc1:CalendarExtender runat="server" id="daterptdtofda_ajax" TargetControlID="daterptdtofda" />
              </span>
            </div>
            <div runat="server" class="half" id="div_repcity" xmlns="">
              <span class="label">City</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="repcity" RMXRef="/Instance/EventMedwatch/RptdByEntity/City" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_lastreportdate" xmlns="">
              <span class="label">Last Reported Date</span>
              <span class="formw">
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);" id="lastreportdate" RMXRef="/Instance/EventMedwatch/LastReportDate" RMXType="date" tabindex="24" />
                <cc1:CalendarExtender runat="server" id="lastreportdate_ajax" TargetControlID="lastreportdate" />
              </span>
            </div>
            <div runat="server" class="half" id="div_repstateid" xmlns="">
              <span class="label">State</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="repstateid" RMXRef="/Instance/EventMedwatch/RptdByEntity/StateId" RMXType="code" cancelledvalue="" tabindex="7" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="repstateidbtn" onclientclick="return selectCode('states','repstateid');" tabindex="8" />
                <asp:TextBox style="display:none" runat="server" id="repstateid_cid" RMXref="/Instance/EventMedwatch/RptdByEntity/StateId/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_followupcount" xmlns="">
              <span class="label">Follow Up Count</span>
              <span class="formw">
                <asp:TextBox runat="server" onblur="numLostFocus(this);" id="followupcount" RMXRef="/Instance/EventMedwatch/FollowUpCount" RMXType="numeric" tabindex="26" onChange="setDataChanged(true);" />
              </span>
            </div>
            <div runat="server" class="half" id="div_repzipcode" xmlns="">
              <span class="label">Zip</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="repzipcode" RMXRef="/Instance/EventMedwatch/RptdByEntity/ZipCode" RMXType="zip" TabIndex="9" />
              </span>
            </div>
            <div runat="server" class="half" id="div_repcountrycode" xmlns="">
              <span class="label">Country</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="repcountrycode" RMXRef="/Instance/EventMedwatch/RptdByEntity/CountryCode" RMXType="code" cancelledvalue="" tabindex="10" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="repcountrycodebtn" onclientclick="return selectCode('COUNTRY','repcountrycode');" tabindex="11" />
                <asp:TextBox style="display:none" runat="server" id="repcountrycode_cid" RMXref="/Instance/EventMedwatch/RptdByEntity/CountryCode/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_reptaxid" xmlns="">
              <span class="label">Soc. Sec No</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="ssnLostFocus(this);" id="reptaxid" RMXRef="/Instance/EventMedwatch/RptdByEntity/TaxId" RMXType="ssn" name="reptaxid" />
              </span>
            </div>
            <div runat="server" class="half" id="div_rptdbyposcode" xmlns="">
              <span class="label">Occupation</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="rptdbyposcode" RMXRef="/Instance/EventMedwatch/RptdByPosCode" RMXType="code" cancelledvalue="" tabindex="13" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="rptdbyposcodebtn" onclientclick="return selectCode('POSITIONS','rptdbyposcode');" tabindex="14" />
                <asp:TextBox style="display:none" runat="server" id="rptdbyposcode_cid" RMXref="/Instance/EventMedwatch/RptdByPosCode/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_repphone1" xmlns="">
              <span class="label">Office Phone</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="repphone1" RMXRef="/Instance/EventMedwatch/RptdByEntity/Phone1" RMXType="phone" tabindex="15" />
              </span>
            </div>
            <div runat="server" class="half" id="div_repphone2" xmlns="">
              <span class="label">Home Phone</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="repphone2" RMXRef="/Instance/EventMedwatch/RptdByEntity/Phone2" RMXType="phone" tabindex="16" />
              </span>
            </div>
            <div runat="server" class="half" id="div_productprobflag" xmlns="">
              <span class="label">Product Problem</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="productprobflag" RMXRef="/Instance/EventMedwatch/ProductProbFlag" RMXType="checkbox" tabindex="27" />
              </span>
            </div>
            <div runat="server" class="half" id="div_adverseeventflag" xmlns="">
              <span class="label">Adverse Event</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="adverseeventflag" RMXRef="/Instance/EventMedwatch/AdverseEventFlag" RMXType="checkbox" tabindex="31" />
              </span>
            </div>
            <div runat="server" class="half" id="div_hospitalizflag" xmlns="">
              <span class="label">Hospitalized</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="hospitalizflag" RMXRef="/Instance/EventMedwatch/HospitalizFlag" RMXType="checkbox" tabindex="28" />
              </span>
            </div>
            <div runat="server" class="half" id="div_lifethreatflag" xmlns="">
              <span class="label">Life Threatening</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="lifethreatflag" RMXRef="/Instance/EventMedwatch/LifeThreatFlag" RMXType="checkbox" tabindex="32" />
              </span>
            </div>
            <div runat="server" class="half" id="div_reqintervflag" xmlns="">
              <span class="label">Required Intervention</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="reqintervflag" RMXRef="/Instance/EventMedwatch/ReqdIntervFlag" RMXType="checkbox" tabindex="29" />
              </span>
            </div>
            <div runat="server" class="half" id="div_congenitalflag" xmlns="">
              <span class="label">Congenital</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="congenitalflag" RMXRef="/Instance/EventMedwatch/CongenitalFlag" RMXType="checkbox" tabindex="33" />
              </span>
            </div>
            <div runat="server" class="half" id="div_rptmandatory" xmlns="">
              <span class="label">Mandatory Report</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="rptmandatory" RMXRef="/Instance/EventMedwatch/RptMandatory" RMXType="checkbox" tabindex="30" />
              </span>
            </div>
            <div runat="server" class="half" id="div_disabilityflag" xmlns="">
              <span class="label">Disability</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="disabilityflag" RMXRef="/Instance/EventMedwatch/DisabilityFlag" RMXType="checkbox" tabindex="34" />
              </span>
            </div>
            <div runat="server" class="half" id="div_repentityid" style="display:none;" xmlns="">
              <span class="formw">
                <asp:TextBox style="display:none" runat="server" id="repentityid" RMXRef="/Instance/EventMedwatch/RptdByEid/@codeid" RMXType="hidden" />
              </span>
            </div>
          </div>
          <div class="singletopborder" style="display:none;" runat="server" name="FORMTABequipmentindicator" id="FORMTABequipmentindicator">
            <div runat="server" class="half" id="div_manulastname" xmlns="">
              <span class="label">Manufacturer Name</span>
              <span class="formw">
                <asp:Textbox runat="server" onchange="lookupTextChanged(this);" tabindex="35" onblur="lookupLostFocus(this);" RMXRef="/Instance/EventMedwatch/ManufacturerEntity/LastName" RMXType="entitylookup" id="manulastname" />
                <asp:Textbox runat="server" style="display:none" Text="1" id="manulastname_creatable" />
                <asp:button runat="server" class="button" tabindex="36" id="manulastnamebtn" onclientclick="lookupDataFDM('manulastname','MANUFACTURERS',4,'manu',1,'/Instance/EventMedwatch/ManufacturerEntity')" Text="..." />
              </span>
            </div>
            <div runat="server" class="half" id="div_modelnumber" xmlns="">
              <span class="label">Model Number</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="modelnumber" RMXRef="/Instance/EventMedwatch/ModelNumber" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_manuaddr1" xmlns="">
              <span class="label">Address 1</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="manuaddr1" RMXRef="/Instance/EventMedwatch/ManufacturerEntity/Addr1" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_catalognumber" xmlns="">
              <span class="label">Catalog Number</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="catalognumber" RMXRef="/Instance/EventMedwatch/CatalogNumber" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_manuaddr2" xmlns="">
              <span class="label">Address 2</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="manuaddr2" RMXRef="/Instance/EventMedwatch/ManufacturerEntity/Addr2" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_serialnumber" xmlns="">
              <span class="label">Serial Number</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="serialnumber" RMXRef="/Instance/EventMedwatch/SerialNumber" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_manucity" xmlns="">
              <span class="label">City</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="manucity" RMXRef="/Instance/EventMedwatch/ManufacturerEntity/City" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_eqlotnumber" xmlns="">
              <span class="label">Lot Number</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="eqlotnumber" RMXRef="/Instance/EventMedwatch/EqLotNumber" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_manustateid" xmlns="">
              <span class="label">State</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="manustateid" RMXRef="/Instance/EventMedwatch/ManufacturerEntity/StateId" RMXType="code" cancelledvalue="" tabindex="40" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="manustateidbtn" onclientclick="return selectCode('states','manustateid');" tabindex="41" />
                <asp:TextBox style="display:none" runat="server" id="manustateid_cid" RMXref="/Instance/EventMedwatch/ManufacturerEntity/StateId/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_othernumber" xmlns="">
              <span class="label">Other Number</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="othernumber" RMXRef="/Instance/EventMedwatch/OtherNumber" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_manuzipcode" xmlns="">
              <span class="label">Zip</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="manuzipcode" RMXRef="/Instance/EventMedwatch/ManufacturerEntity/ZipCode" RMXType="zip" TabIndex="42" />
              </span>
            </div>
            <div runat="server" class="half" id="div_brandname" xmlns="">
              <span class="label">Brand Name</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="brandname" RMXRef="/Instance/EventMedwatch/BrandName" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_deviceopercode" xmlns="">
              <span class="label">Device Operator</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="deviceopercode" RMXRef="/Instance/EventMedwatch/DeviceOperCode" RMXType="code" cancelledvalue="" tabindex="53" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="deviceopercodebtn" onclientclick="return selectCode('DEVICE_OPER_CODES','deviceopercode');" tabindex="54" />
                <asp:TextBox style="display:none" runat="server" id="deviceopercode_cid" RMXref="/Instance/EventMedwatch/DeviceOperCode/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_equiptypecode" xmlns="">
              <span class="label">Equipment Type</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="equiptypecode" RMXRef="/Instance/EventMedwatch/EquipTypeCode" RMXType="code" cancelledvalue="" tabindex="44" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="equiptypecodebtn" onclientclick="return selectCode('EQUIP_TYPE_CODE','equiptypecode');" tabindex="45" />
                <asp:TextBox style="display:none" runat="server" id="equiptypecode_cid" RMXref="/Instance/EventMedwatch/EquipTypeCode/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_eqexpirationdate" xmlns="">
              <span class="label">Expiration Date</span>
              <span class="formw">
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);" id="eqexpirationdate" RMXRef="/Instance/EventMedwatch/EqExpirationDate" RMXType="date" tabindex="55" />
                <cc1:CalendarExtender runat="server" id="eqexpirationdate_ajax" TargetControlID="eqexpirationdate" />
              </span>
            </div>
            <div runat="server" class="half" id="div_ageofdevice" xmlns="">
              <span class="label">Age of Device</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="ageofdevice" RMXRef="/Instance/EventMedwatch/AgeOfDevice" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_implantdate" xmlns="">
              <span class="label">Implant Date</span>
              <span class="formw">
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);" id="implantdate" RMXRef="/Instance/EventMedwatch/ImplantDate" RMXType="date" tabindex="57" />
                <cc1:CalendarExtender runat="server" id="implantdate_ajax" TargetControlID="implantdate" />
              </span>
            </div>
            <div runat="server" class="half" id="div_spacer" style="display:none;" xmlns="">
              <span class="label">
                <asp:Label runat="server" id="spacer" Text="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_explantdate" xmlns="">
              <span class="label">Explant Date</span>
              <span class="formw">
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);" id="explantdate" RMXRef="/Instance/EventMedwatch/ExplantDate" RMXType="date" tabindex="59" />
                <cc1:CalendarExtender runat="server" id="explantdate_ajax" TargetControlID="explantdate" />
              </span>
            </div>
            <div runat="server" class="half" id="div_devavailforeval" xmlns="">
              <span class="label">Device Available for Evaluation</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="devavailforeval" RMXRef="/Instance/EventMedwatch/DevAvailForEval" RMXType="checkbox" tabindex="47" />
              </span>
            </div>
            <div runat="server" class="half" id="div_devicereturndate" xmlns="">
              <span class="label">Device Return Date</span>
              <span class="formw">
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);" id="devicereturndate" RMXRef="/Instance/EventMedwatch/DeviceReturnDate" RMXType="date" tabindex="61" />
                <cc1:CalendarExtender runat="server" id="devicereturndate_ajax" TargetControlID="devicereturndate" />
              </span>
            </div>
            <div runat="server" class="half" id="div_manuentityid" style="display:none;" xmlns="">
              <span class="formw">
                <asp:TextBox style="display:none" runat="server" id="manuentityid" RMXRef="/Instance/EventMedwatch/ManufacturerEid/@codeid" RMXType="hidden" />
              </span>
            </div>
          </div>
          <div class="singletopborder" style="display:none;" runat="server" name="FORMTABmedicationindicator" id="FORMTABmedicationindicator">
            <div runat="server" class="half" id="div_medtype" xmlns="">
              <span class="label">Medication Type</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="medtype" RMXRef="/Instance/EventMedwatch/MedType" RMXType="code" cancelledvalue="" tabindex="63" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="medtypebtn" onclientclick="return selectCode('MEDICATION_TYPE','medtype');" tabindex="64" />
                <asp:TextBox style="display:none" runat="server" id="medtype_cid" RMXref="/Instance/EventMedwatch/MedType/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_duration" xmlns="">
              <span class="label">Duration</span>
              <span class="formw">
                <asp:TextBox runat="server" onblur="numLostFocus(this);" id="duration" RMXRef="/Instance/EventMedwatch/Duration" RMXType="numeric" tabindex="73" onChange="setDataChanged(true);" />
              </span>
            </div>
            <div runat="server" class="half" id="div_medname" xmlns="">
              <span class="label">Medication Name</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="medname" RMXRef="/Instance/EventMedwatch/MedName" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_lotnumber" xmlns="">
              <span class="label">Lot Number</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="lotnumber" RMXRef="/Instance/EventMedwatch/LotNumber" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_dose" xmlns="">
              <span class="label">Dose</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="dose" RMXRef="/Instance/EventMedwatch/Dose" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_ndcnumber" xmlns="">
              <span class="label">NDC Number</span>
              <span class="formw">
                <asp:TextBox runat="server" onblur="numLostFocus(this);" id="ndcnumber" RMXRef="/Instance/EventMedwatch/NdcNumber" RMXType="numeric" tabindex="75" onChange="setDataChanged(true);" />
              </span>
            </div>
            <div runat="server" class="half" id="div_frequency" xmlns="">
              <span class="label">Frequency</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="frequency" RMXRef="/Instance/EventMedwatch/Frequency" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_expirationdate" xmlns="">
              <span class="label">Expiration Date</span>
              <span class="formw">
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);" id="expirationdate" RMXRef="/Instance/EventMedwatch/ExpirationDate" RMXType="date" tabindex="76" />
                <cc1:CalendarExtender runat="server" id="expirationdate_ajax" TargetControlID="expirationdate" />
              </span>
            </div>
            <div runat="server" class="half" id="div_route" xmlns="">
              <span class="label">Route</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="route" RMXRef="/Instance/EventMedwatch/Route" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_therapyfromdate" xmlns="">
              <span class="label">Therapy From</span>
              <span class="formw">
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);" id="therapyfromdate" RMXRef="/Instance/EventMedwatch/TherapyFromDate" RMXType="date" tabindex="69" />
                <cc1:CalendarExtender runat="server" id="therapyfromdate_ajax" TargetControlID="therapyfromdate" />
              </span>
            </div>
            <div runat="server" class="half" id="div_therapytodate" xmlns="">
              <span class="label">Therapy To</span>
              <span class="formw">
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);" id="therapytodate" RMXRef="/Instance/EventMedwatch/TherapyToDate" RMXType="date" tabindex="71" />
                <cc1:CalendarExtender runat="server" id="therapytodate_ajax" TargetControlID="therapytodate" />
              </span>
            </div>
            <div runat="server" class="half" id="div_mweventabated" xmlns="">
              <span class="label">Event Abated After Dose Stopped</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="mweventabated" RMXRef="/Instance/EventMedwatch/MwEventAbated" RMXType="checkbox" tabindex="78" />
              </span>
            </div>
            <div runat="server" class="half" id="div_mwevntreappeared" xmlns="">
              <span class="label">Event Reappeared After Re-Introduction</span>
              <span class="formw">
                <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="mwevntreappeared" RMXRef="/Instance/EventMedwatch/MwEvntReappeared" RMXType="checkbox" tabindex="79" />
              </span>
            </div>
          </div>
          <div class="singletopborder" style="display:none;" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
            <asp:TextBox style="display:none" runat="server" id="supp_event_id" RMXRef="/Instance/*/Supplementals/EVENT_ID" RMXType="id" />
            <div runat="server" class="half" id="div_supp_patint_code_a_text" xmlns="">
              <span class="label">F-10 Patient Code 1</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_patint_code_a_text" RMXRef="/Instance/*/Supplementals/PATINT_CODE_A_TEXT" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_supp_patint_code_b_text" xmlns="">
              <span class="label">F-10 Patient Code 2</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_patint_code_b_text" RMXRef="/Instance/*/Supplementals/PATINT_CODE_B_TEXT" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_supp_patint_code_c_text" xmlns="">
              <span class="label">F-10 Patient Code 3</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_patint_code_c_text" RMXRef="/Instance/*/Supplementals/PATINT_CODE_C_TEXT" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_supp_device_code_a_text" xmlns="">
              <span class="label">F-10 Device Code 1</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_device_code_a_text" RMXRef="/Instance/*/Supplementals/DEVICE_CODE_A_TEXT" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_supp_device_code_b_text" xmlns="">
              <span class="label">F-10 Device Code 2</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_device_code_b_text" RMXRef="/Instance/*/Supplementals/DEVICE_CODE_B_TEXT" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_supp_device_code_c_text" xmlns="">
              <span class="label">F-10 Device Code 3</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_device_code_c_text" RMXRef="/Instance/*/Supplementals/DEVICE_CODE_C_TEXT" RMXType="text" />
              </span>
            </div>
            <div runat="server" class="half" id="div_supp_oth_outcome_code" xmlns="">
              <span class="label">Other Serious Important Medical Events?</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="supp_oth_outcome_code" RMXRef="/Instance/*/Supplementals/OTH_OUTCOME_CODE" RMXType="code" cancelledvalue="" tabindex="2864" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="supp_oth_outcome_codebtn" onclientclick="return selectCode('YES_NO','supp_oth_outcome_code');" tabindex="2865" />
                <asp:TextBox style="display:none" runat="server" id="supp_oth_outcome_code_cid" RMXref="/Instance/*/Supplementals/OTH_OUTCOME_CODE/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_supp_sing_use_code" xmlns="">
              <span class="label">Single-use Medical Device Reprocessed and Reused o</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="lookupTextChanged(this);" id="supp_sing_use_code" RMXRef="/Instance/*/Supplementals/SING_USE_CODE" RMXType="code" cancelledvalue="" tabindex="2865" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="supp_sing_use_codebtn" onclientclick="return selectCode('YES_NO','supp_sing_use_code');" tabindex="2866" />
                <asp:TextBox style="display:none" runat="server" id="supp_sing_use_code_cid" RMXref="/Instance/*/Supplementals/SING_USE_CODE/@codeid" cancelledvalue="" />
              </span>
            </div>
            <div runat="server" class="half" id="div_supp_repro_name_addr_text" xmlns="">
              <span class="label">Reprocessor Name Address</span>
              <span class="formw">
                <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_repro_name_addr_text" RMXRef="/Instance/*/Supplementals/REPRO_NAME_ADDR_TEXT" RMXType="text" />
              </span>
            </div>
          </div>
          <div class="formButtonGroup" runat="server">
            <div class="formButton" runat="server" id="div_btnBack">
              <asp:Button class="button" runat="Server" id="btnBack" Text="Back to Event" OnClientClick="if(!( XFormHandler('','1','back')))return false;" PostBackUrl="?" />
            </div>
            <div class="formButton" runat="server" id="div_btnMedwatchTest">
              <asp:Button class="button" runat="Server" id="btnMedwatchTest" Text="MedWatch Test" RMXRef="/Instance/EventMedwatch/TestList/@committedcount" OnClientClick="if(!( XFormHandler('SysFormPIdName=eventid&SysFormName=medwatchtest&SysCmd=1&SysFormIdName=evmwtestrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/EventMedwatch/EventId | /Instance/UI/FormVariables/SysExData/EventNumber','1','')))return false;" PostBackUrl="?SysFormPIdName=eventid&SysFormName=medwatchtest&SysCmd=1&SysFormIdName=evmwtestrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/EventMedwatch/EventId | /Instance/UI/FormVariables/SysExData/EventNumber" />
            </div>
            <div class="formButton" runat="server" id="div_btnConcomProd">
              <asp:Button class="button" runat="Server" id="btnConcomProd" Text="Concomitant Products" RMXRef="/Instance/EventMedwatch/ConcomProdList/@committedcount" OnClientClick="if(!( XFormHandler('SysFormPIdName=eventid&SysFormName=concomitant&SysCmd=1&SysFormIdName=evconcomrowid&SysEx=ClaimId|ClaimNumber|EventId|EventNumber&SysExMapCtl=ClaimId|ClaimNumber|EventId|EventNumber','1','')))return false;" PostBackUrl="concomitant.aspx?SysFormPIdName=eventid&SysFormName=concomitant&SysCmd=1&SysFormIdName=evconcomrowid&SysEx=ClaimId|ClaimNumber|EventId|EventNumber&SysExMapCtl=ClaimId|ClaimNumber|EventId|EventNumber" />
            </div>
            <div class="formButton" runat="server" id="div_btnPrintMedwatch">
              <script language="JavaScript" src="">
                {var i;}
              </script>
              <asp:button class="button" runat="server" id="btnPrintMedwatch" RMXRef="" Text="Print MedWatch" onClientClick="printmedwatchreport();" />
            </div>
          </div>
          <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
          <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="EventMedwatch" />
          <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;EventMedwatch&gt;&lt;ManufacturerEntity&gt;&lt;/ManufacturerEntity&gt;&lt;RptdByEntity&gt;&lt;/RptdByEntity&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/EventMedwatch&gt;" />
          <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="event" />
          <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="eventid" />
          <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="11000" />
          <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="medwatch" />
          <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="eventid" />
          <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="12050" />
          <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
          <asp:TextBox style="display:none" runat="server" id="SysSupplemental" RMXRef="Instance/UI/FormVariables/SysSupplemental" RMXType="hidden" Text="formname=event_x_medw_supp&sys_formidname=event_id&event_id=%eventid%&sys_formpform=medwatch&sys_formpidname=event_id" />
          <asp:TextBox style="display:none" runat="server" name="formname" value="medwatch" />
          <asp:TextBox style="display:none" runat="server" name="SysRequired" value="replastname|" />
          <asp:TextBox style="display:none" runat="server" name="SysFocusFields" value="replastname|manulastname|medtype|" />
          <input type="hidden" id="hdSaveButtonClicked" />
          <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
          <asp:TextBox runat="server" id="SysLookupClass" style="display:none" />
          <asp:TextBox runat="server" id="SysLookupRecordId" style="display:none" />
          <asp:TextBox runat="server" id="SysLookupAttachNodePath" style="display:none" />
          <asp:TextBox runat="server" id="SysLookupResultConfig" style="display:none" />
          <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
        </form>
      </body>
    </html>