<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Subrogation.aspx.cs"  Inherits="Riskmaster.UI.FDM.Subrogation" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head runat="server">
    <title>Subrogation</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/tb_save_active.png" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/tb_save_mo.png';" onMouseOut="this.src='../../Images/tb_save_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" OnClientClick="return DeleteRecord();" src="../../Images/tb_delete_active.png" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/tb_delete_mo.png';" onMouseOut="this.src='../../Images/tb_delete_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return recordSummary();" src="../../Images/tb_recordsummary_active.png" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='../../Images/tb_recordsummary_mo.png';" onMouseOut="this.src='../../Images/tb_recordsummary_active.png';" />
        </div>
      </div>
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Subrogation" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSsibrogationinfo" id="TABSsibrogationinfo">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="sibrogationinfo" id="LINKTABSsibrogationinfo">Subrogation Info</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPsibrogationinfo">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsubrogationfinancial" id="TABSsubrogationfinancial">
          <a class="NotSelected1" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="subrogationfinancial" id="LINKTABSsubrogationfinancial">Subrogation Financial</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPsubrogationfinancial">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
          <a class="NotSelected1" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPsuppgroup">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABsibrogationinfo" id="FORMTABsibrogationinfo">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdsibrogationinfo" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="/Instance/ClaimXSubro/ClaimId|" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="claimnumber" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimNumber" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="subrogationrowid" RMXRef="/Instance/ClaimXSubro/SubrogationRowId" RMXType="id" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_subtypecode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_subtypecode" Text="Subrogation Type" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="subtypecode" CodeTable="SUBROGATION_TYPE" ControlName="subtypecode" RMXRef="/Instance/ClaimXSubro/SubTypeCode" CodeFilter="" RMXType="code" tabindex="11" />
                </span>
              </div>
              <div runat="server" class="half" id="div_subspecialist" xmlns="">
                <asp:label runat="server" class="label" id="lbl_subspecialist" Text="Subro Specialist" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="lookupTextChanged(this);" tabindex="15" onblur="lookupLostFocus(this);" RMXRef="/Instance/ClaimXSubro/SubroSpecialistEntity/LastName" RMXType="entitylookup" id="subspecialist" />
                  <asp:Textbox runat="server" style="display:none" Text="1" id="subspecialist_creatable" />
                  <asp:button runat="server" class="EllipsisControl" id="subspecialistbtn" tabindex="16" onclientclick="return lookupData('subspecialist','SUBRO_SPECIALIST',4,'subrospecialist',1);" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="subspecialistid" RMXRef="/Instance/ClaimXSubro/SubroSpecialistEntity/EntityId" RMXType="id" />
              <div runat="server" class="half" id="div_substatus" xmlns="">
                <asp:label runat="server" class="label" id="lbl_substatus" Text="Subrogation Status" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="substatus" CodeTable="SUBROGATION_STATUS" ControlName="substatus" RMXRef="/Instance/ClaimXSubro/SubStatus" CodeFilter="" RMXType="code" tabindex="11" />
                </span>
              </div>
              <div runat="server" class="half" id="div_substatusdesc" xmlns="">
                <asp:label runat="server" class="label" id="lbl_substatusdesc" Text="Subrogation Status" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="substatusdesc" CodeTable="SUBROGATION_STATUS_DESC" ControlName="substatusdesc" RMXRef="/Instance/ClaimXSubro/SubStatusDesc" CodeFilter="" RMXType="code" tabindex="11" />
                </span>
              </div>
              <div runat="server" class="half" id="div_statusdate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_statusdate" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="statusdate" RMXRef="/Instance/ClaimXSubro/StatusDate" RMXType="date" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="statusdatebtn" />
                  <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "statusdate",
					ifFormat : "%m/%d/%Y",
					button : "statusdatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_subadversepartyeid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_subadversepartyeid" Text="Advers Party" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="lookupTextChanged(this);" tabindex="15" onblur="lookupLostFocus(this);" RMXRef="/Instance/ClaimXSubro/SubAdversePartyEntity/LastName" RMXType="entitylookup" id="subadversepartyeid" />
                  <asp:Textbox runat="server" style="display:none" Text="1" id="subadversepartyeid_creatable" />
                  <asp:button runat="server" class="EllipsisControl" id="subadversepartyeidbtn" tabindex="16" onclientclick="return lookupData('subadversepartyeid','OTHER_PEOPLE',4,'adverseparty',1);" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="subadversepartyid" RMXRef="/Instance/ClaimXSubro/SubAdversePartyEntityEid@codeid/Instance/ClaimXSubro/EntityId" RMXType="id" />
              <div runat="server" class="half" id="div_subadverseinscoeid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_subadverseinscoeid" Text="Insurance Company" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="lookupTextChanged(this);" tabindex="15" onblur="lookupLostFocus(this);" RMXRef="/Instance/ClaimXSubro/SubAdverseInsCoEntity" RMXType="entitylookup" id="subadverseinscoeid" />
                  <asp:Textbox runat="server" style="display:none" Text="1" id="subadverseinscoeid_creatable" />
                  <asp:button runat="server" class="EllipsisControl" id="subadverseinscoeidbtn" tabindex="16" onclientclick="return lookupData('subadverseinscoeid','INSURERS',4,'insurancecompany',1);" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="subadverseinscoid" RMXRef="/Instance/ClaimXSubro/SubAdverseInsCoEid@codeid/Instance/ClaimXSubro/EntityId" RMXType="id" />
              <div runat="server" class="half" id="div_subadverseclaimnum" xmlns="">
                <asp:label runat="server" class="label" id="lbl_subadverseclaimnum" Text="Claim Number" />
                <span class="formw">
                  <asp:TextBox runat="server" id="subadverseclaimnum" RMXRef="/Instance/ClaimXSubro/SubvAdversClaimNum" RMXType="text" tabindex="7" maxlength="25" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_subadversepolicynum" xmlns="">
                <asp:label runat="server" class="label" id="lbl_subadversepolicynum" Text="Policy Number" />
                <span class="formw">
                  <asp:TextBox runat="server" id="subadversepolicynum" RMXRef="/Instance/ClaimXSubro/SubAdversePolicyNum" RMXType="text" tabindex="7" maxlength="25" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_subadvadjustereid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_subadvadjustereid" Text="Adjuster" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="lookupTextChanged(this);" tabindex="15" onblur="lookupLostFocus(this);" RMXRef="/Instance/ClaimXSubro/AdjusterEntity/LastName" RMXType="entitylookup" id="subadvadjustereid" />
                  <asp:Textbox runat="server" style="display:none" Text="1" id="subadvadjustereid_creatable" />
                  <asp:button runat="server" class="EllipsisControl" id="subadvadjustereidbtn" tabindex="16" onclientclick="return lookupData('subadvadjustereid','ADJUSTER',4,'other_people',1);" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="subadvadjusterid" RMXRef="/Instance/ClaimXSubro/SubAdversePartyEntityEid@codeid/Instance/ClaimXSubro/EntityId" RMXType="id" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="subdttmrcdadded" RMXRef="/Instance/ClaimXSubro/DttmRcdAdded" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="subdttmrcdlastupd" RMXRef="/Instance/ClaimXSubro/DttmRcdLastUpd" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="subupdatedbyuser" RMXRef="/Instance/ClaimXSubro/UpdatedByUser" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="subaddedbyuser" RMXRef="/Instance/ClaimXSubro/AddedByUser" RMXType="id" />
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABsubrogationfinancial" id="FORMTABsubrogationfinancial">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdsubrogationfinancial" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="threecontrols" id="div_nbofyears" xmlns="">
                <asp:label runat="server" class="label" id="lbl_nbofyears" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" id="nbofyears" RMXRef="/Instance/ClaimXSubro/NbOfYears" RMXType="text" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="threecontrols" id="div_advpartiescovlimit" xmlns="">
                <asp:label runat="server" class="label" id="lbl_advpartiescovlimit" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" id="advpartiescovlimit" RMXRef="/Instance/ClaimXSubro/AdvPartiesCovLimit" RMXType="text" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="threecontrols" id="div_statutelimitationdate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_statutelimitationdate" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="statutelimitationdate" RMXRef="/Instance/ClaimXSubro/StatuteLimitationDate" RMXType="date" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="statutelimitationdatebtn" />
                  <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "statutelimitationdate",
					ifFormat : "%m/%d/%Y",
					button : "statutelimitationdatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_amountpaid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_amountpaid" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" id="amountpaid" RMXRef="/Instance/ClaimXSubro/AmountPaid" RMXType="text" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_settlementpercent" xmlns="">
                <asp:label runat="server" class="label" id="lbl_settlementpercent" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" id="settlementpercent" RMXRef="/Instance/ClaimXSubro/SettlementPercent" RMXType="text" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_covdedamount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_covdedamount" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="covdedamount" RMXRef="/Instance/ClaimXSubro/CovDedAmount" RMXType="date" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="covdedamountbtn" />
                  <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "covdedamount",
					ifFormat : "%m/%d/%Y",
					button : "covdedamountbtn"
					}
					);
				</script>
                </span>
              </div>
              <div runat="server" class="half" id="div_potpayoutamt" xmlns="">
                <asp:label runat="server" class="label" id="lbl_potpayoutamt" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" id="potpayoutamt" RMXRef="/Instance/ClaimXSubro/PotPayoutAmt" RMXType="text" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_totaltoberecov" xmlns="">
                <asp:label runat="server" class="label" id="lbl_totaltoberecov" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" id="totaltoberecov" RMXRef="/Instance/ClaimXSubro/TotalToBeRecov" RMXType="text" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_potdedamt" xmlns="">
                <asp:label runat="server" class="label" id="lbl_potdedamt" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="potdedamt" RMXRef="/Instance/ClaimXSubro/PotDedAmt" RMXType="date" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="potdedamtbtn" />
                  <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "potdedamt",
					ifFormat : "%m/%d/%Y",
					button : "potdedamtbtn"
					}
					);
				</script>
                </span>
              </div>
              <div runat="server" class="half" id="div_totalpotrecov" xmlns="">
                <asp:label runat="server" class="label" id="lbl_totalpotrecov" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" id="totalpotrecov" RMXRef="/Instance/ClaimXSubro/TotalPotRecov" RMXType="text" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdsuppgroup" />
            </td>
          </tr>
        </table>
      </div>
      <div class="formButtonGroup" runat="server" />
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="ClaimXSubro" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;ClaimXSubro&gt;&lt;SubroSpecialistEntity&gt;&lt;/SubroSpecialistEntity&gt;&lt;SubAdversePartyEntity&gt;&lt;/SubAdversePartyEntity&gt;&lt;SubAdverseInsCoEntity&gt;&lt;/SubAdverseInsCoEntity&gt;&lt;SubAdvAdusterEntity&gt;&lt;/SubAdvAdusterEntity&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/ClaimXSubro&gt;" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="claimwc" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="claimid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="claimid,claimnumber,subrogationrowid" />
      <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="subrogation" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="subrogationrowid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="3000" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="line_of_bus_code" RMXRef="Instance/UI/MissingRef" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="lineofbusinesscode" RMXRef="/Instance/UI/FormVariables/SysExData/LINEOFBUSCODE" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="TabNameList" RMXRef="" RMXType="hidden" Text="TABSsibrogationinfo|TABSsubrogationfinancial|TABSsuppgroup" />
      <asp:TextBox style="display:none" runat="server" id="GroupAssocFieldList" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="FormReadOnly" RMXRef="" RMXType="hidden" Text="Enable" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="subrogation" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="subadverseclaimnum|subadversepolicynum|" />
      <asp:TextBox style="display:none" runat="server" name="SysReadonlyFields" id="SysReadonlyFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>