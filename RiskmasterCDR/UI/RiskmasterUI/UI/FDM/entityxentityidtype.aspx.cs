﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/10/2014 | 34276  | achouhan3  | Added page for Entity ID Type and ID Type Number functionality
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.FDM
{
    public partial class Entityxentityidtype : GridPopupBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridPopupPageload();
        }
    }
}