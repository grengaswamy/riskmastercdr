﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.FDM
{
    public partial class Vssassignment : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
            //Asharma326 MITS 33994 Starts
            DropDownList varbankaccount = (DropDownList)this.FindControl("bankaccount");
            if (varbankaccount != null)
            {
                varbankaccount.Attributes.Remove("onchange");
                varbankaccount.Attributes.Add("onfocus", "setDataChanged(true);");
            }  
            //Asharma326 MITS 33994 Ends
        }
    }
}