﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.FDM
{
    public partial class Policy : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
        }

        //smishra54: MITS 28004
        public override void ModifyControls()
        {
            
            Control lbUnitName = this.Form.FindControl("multiunitid");
            Control dlUnitType = this.Form.FindControl("unittype");
            //apeykov MITs 37801 start
            if (dlUnitType != null)
                ((DropDownList)dlUnitType).Items.Insert(0, new ListItem(String.Empty, String.Empty));
            //apeykov MITs 37801 end

            if (lbUnitName != null && dlUnitType != null)
            {
                if (((ListBox)lbUnitName).Items.Count > 0 && ((ListBox)lbUnitName).Items[0].Value.Contains("E_"))
                {
                    ((DropDownList)dlUnitType).SelectedValue = "E";
                }
            }
          
            // dbisht6 start for mits35331
            Control dlopenentity = this.Form.FindControl("btnopeninsuredentity");
            if (dlopenentity != null)
            {
                DatabindingHelper.EnableControl(dlopenentity);
            }
           
            // dbisht6 end
        }
        //smishra54:end

        protected void ClonePolicy(object sender, EventArgs e)
        {

            TextBox txtPostBackAction = (TextBox)this.Form.FindControl("hdnPostBackAction");
            txtPostBackAction.Text = "CLONE";
            
            NavigateSubmit(sender,e);

            //XElement oMessageElement = GetMessageTemplate();
            //GetFormVariables(oMessageElement);

            //XElement oCmdElement = oMessageElement.XPathSelectElement("./Call/Function");

            //oCmdElement.Value = "FormDataAdaptor.Navigate";

            //oCmdElement = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysCmd");

            //oCmdElement.Value = "7";

            //oCmdElement = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName");

            //oCmdElement.Value = "policy";

            //BindControlCollection2Data(this.Form.Controls, oMessageElement);
            
            //string sSessionID = HttpContext.Current.Request.QueryString["SessionID"];
            //if (!string.IsNullOrEmpty(sSessionID))
            //{
            //    XElement oSessionCmdElement = oMessageElement.XPathSelectElement("./Authorization");
            //    oSessionCmdElement.Value = sSessionID;
            //}

            //Riskmaster.cws.CommonWebService oCWS = new Riskmaster.cws.CommonWebService();
            //oCWS.Url = ConfigurationManager.AppSettings["CWSURL"];
            //XmlDocument oClonePolicy = new XmlDocument();
            //oClonePolicy.LoadXml(oMessageElement.ToString());
            //oCWS.Timeout = 900000;
            //XmlNode oReturnNode = oCWS.ProcessRequest(oClonePolicy);
            //string sReturn = oReturnNode.InnerXml;
        }        
    }
}
