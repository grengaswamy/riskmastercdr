﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adjustdatedtextlist.aspx.cs" Inherits="Riskmaster.UI.FDM.AdjustDatedTextList" ValidateRequest="false" EnableViewStateMac="false"%>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>Adjuster Dated Text List</title>
     <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" src="Scripts/drift.js"></script>
	<script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
  <script type="text/javascript" src="../../Scripts/pagingsorting.js"></script> 
</head>
<body onload="pageLoaded()" class="10pt">
    <form id="frmData" runat="server">
    <div>
    <asp:Label ID="lblError" runat="server" Text="" ForeColor=Red></asp:Label>
    <h2>Lookup results</h2>
    <table border="0" width="95%" cellspacing="0" cellpadding="2" id="tbllist">
    <tr>
    <td class="ctrlgroup"><a href="#"  id="HeaderColumn0" class="ctrlgroup" onclick="SortList(document.all.tbllist,0,'lnkAdjDate',true );">Date Entered</a></td>
	<td class="ctrlgroup" nowrap="1">Dated Text</td>
	<td class="ctrlgroup" nowrap="1"><a href="#"  id="HeaderColumn2" class="ctrlgroup" onclick="SortList(document.all.tbllist,2,'lnkAdjDate',true );">Dated Text Type</a></td>
	
    </tr>
     
    <%
if (RecordExists.Value.ToLower() == "true")
{
    int i = 0; foreach (XElement item in result)
    {
        string rowclass = "";
        if ((i % 2) == 1) rowclass = "datatd1";
        else rowclass = "datatd";
        i++;
        %>
        <tr>
            <td class="<%=rowclass%>">                          
            <%lnkAdjDate.Text = AppHelper.GetUIDate(item.Element("DateEntered").Value);%> 
               <%
                   lnkAdjDate.ID = "lnkAdjDate" + i;
                lnkAdjDate.PostBackUrl = "adjusterdatedtext.aspx?SysFormName=adjusterdatedtext&SysCmd=0&SysViewType=controlsonly&SysFormIdName=adjdttextrowid&SysFormId=" + item.Element("AdjDttextRowId").Value + "&SysFormPIdName=adjrowid&SysEx=AdjRowId&SysExMapCtl=AdjRowId";
                %>
              <asp:LinkButton runat="server" id="lnkAdjDate"></asp:LinkButton> 
            </td>
            <td class="<%=rowclass%>">
                   <% 
                        DatedTextField.Value = item.Element("DatedText").Value;
                        if (DatedTextField.Value.Length > 400)
                        {
                            DatedTextField.Value = DatedTextField.Value.Substring(0, 400);
                        }
                        DatedTextField.Value = DatedTextField.Value + "...";
                     %> 
                              
            <%=DatedTextField.Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("TextTypeCode").Value%>
                              
            </td>  
            <td style="display:none">                           
                </td>
                <td style="display:none">                           
                   <%=lnkAdjDate.PostBackUrl%>
                </td>    
                <td style="display:none">
                  <%=item.Element("AdjDttextRowId").Value%>
                </td>               
                    
       </tr>  
                           
 <% }
}%> 
  <%else
{%>
     <tr>
     <td align="center"><br /><br /><br /><br />
            There are no records to show.
      <br /><br /><br /><br /><br /></td>
    </tr>
     <% }%>
 </table>
 
 <table>
 <tr>
 <td colspan="2">
    Text Type Filter:
      <uc:CodeLookUp runat="server" ID="adjtexttypecode" CodeTable="ADJ_TEXT_TYPE" ControlName="adjtexttypecode" RMXRef="/Instance/AdjustDatedTextList/AdjustDatedText/TextTypeCode" RMXType="code" />
 </td>
 <td>
  <input type="button" value="Clear Filter" name="btnClearFilter" onclick="return ClearFilter();"/>
 </td>
 </tr>
 </table>
 
    <center><div id="pageNavPosition"></div></center> 
    <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
    <asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
    <asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
    <asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
    <asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
    <asp:TextBox runat="server" id="SysClassName" style="display:none" Text="AdjustDatedTextList" RMXRef="Instance/UI/FormVariables/SysClassName"/>						
    <asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" Text="&lt;AdjustDatedTextList&gt;&lt;AdjustDatedText /&gt;&lt;/AdjustDatedTextList&gt;"	RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>	
    <asp:TextBox runat="server" id="SysSerializationConfigIgnore" style="display:none" Text="Claim.Comments"	RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore"/>						                             
    <asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm"/>
    <asp:TextBox runat="server" id="SysFormPIdName" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormIdName" Text="adjrowid"/>
    <asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
    <asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
    <asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>
    <asp:TextBox runat="server" id="SysNotReqNew" style="display:none" RMXRef="Instance/UI/FormVariables/SysNotReqNew" />
    <asp:TextBox runat="server" id="SysFormName" style="display:none" Text="adjustdatedtextlist" RMXRef="Instance/UI/FormVariables/SysFormName" />
    <asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="adjrowid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId"/>
    <asp:TextBox runat="server" id="SysSid" style="display:none" Text="11000" RMXRef="Instance/UI/FormVariables/SysSid"/>
    <asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
    <asp:TextBox ID="paramstore" runat="server" style="display:none"></asp:TextBox>	
    <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none"/>
    <asp:TextBox runat="server" id="claimid" RMXRef="Instance/UI/FormVariables/SysExData/ClaimId" style="display:none"/>
    <asp:TextBox runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" style="display:none"/>
    <asp:TextBox runat="server" id="adjrowid" RMXRef="Instance/UI/FormVariables/SysExData/AdjRowId"   style="display:none"/>    
   <asp:TextBox runat="server" id="texttypecode" RMXRef="Instance/UI/FormVariables/SysExData/TextTypeCode"   style="display:none"/>
   <asp:TextBox runat="server" id="codeid" RMXRef="Instance/UI/FormVariables/SysExData/CodeId"   style="display:none"/>                               
    <asp:button class="button" runat="server" id="btnAdd" Text="Add New" postbackurl="adjusterdatedtext.aspx?SysFormName=adjusterdatedtext&SysCmd=&SysViewType=tab&SysEx=AdjRowId&SysExMapCtl=AdjRowId" />
    <asp:Button class="button" runat="server" ID="BackToParent" Text="Back" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>    
    <asp:HiddenField ID="DatedTextField" runat="server" />
    <asp:HiddenField Id="RecordExists" runat="server" />
    <input type="hidden" id="PageName" value="adjustdatedtextlist" />
    </div>
	<uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
     <script type="text/javascript">
        var listpaging = new ListPaging('tbllist', 3); 
        listpaging.init(); 
        listpaging.showPageNavigation('listpaging', 'pageNavPosition'); 
        listpaging.showPage(1);
    </script>  
</body>
</html>
