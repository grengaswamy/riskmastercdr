﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using Riskmaster.AppHelpers;
using System.Text;
using System.Xml.XPath;
using Riskmaster.Common;

namespace Riskmaster.UI.FDM
{
    public partial class Attachdiary : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //Moving it before FDMPageLoad , so that Selected Value can be set to control
                    //##Bind Priority Dropdown
                    BindData();
                }

                FDMPageLoad();
                if (!IsPostBack)
                {
                    TextBox txtEntryName = (TextBox)this.FindControl("EntryName");
                    TextBox txtAttachRecordid_cid = (TextBox)this.FindControl("AttachRecordid_cid");

                    #region Saving QueryString values into View State
                    if (Request.QueryString["AttachTable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["AttachTable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";
                    }

                    if (Request.QueryString["AttachRecord"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["AttachRecord"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";
                    }

                    if (!string.IsNullOrWhiteSpace(ViewState["attachrecordid"].ToString()))
                    {
                        if (txtAttachRecordid_cid != null)
                            txtAttachRecordid_cid.Text = Convert.ToString(ViewState["attachrecordid"]);
                    }
                    
                    if (Request.QueryString["AttSecRecId"] != null)
                    {
                        ViewState["attsecrecid"] = Request.QueryString["AttSecRecId"];
                    }
                    else
                    {
                        ViewState["attsecrecid"] = "";
                    }

                    if (Request.QueryString["AttachRecordName"] != null)
                    {
                        ViewState["AttachRecordName"] = Request.QueryString["AttachRecordName"];
                    }
                    else
                    {
                        ViewState["AttachRecordName"] = "";
                    }

                    if (Request.QueryString["AttachSubject"] != null)
                    {
                        ViewState["AttachSubject"] = Request.QueryString["AttachSubject"];
                    }
                    else
                    {
                        ViewState["AttachSubject"] = string.Empty;
                    }

                    if (ViewState["AttachSubject"].ToString() == string.Empty)
                    {
                        if (txtEntryName != null)
                        {
                            if (ViewState["AttachRecordName"].ToString() == string.Empty)
                            {
                                txtEntryName.Text = ViewState["attachtable"].ToString() + " " + ViewState["attachrecordid"].ToString();
                            }
                            else
                            {
                                txtEntryName.Text = ViewState["attachtable"].ToString() + " : " + ViewState["AttachRecordName"].ToString();
                            } 
                        }
                    }
                    else
                    {
                        if (txtEntryName != null)
                        {
                            if (ViewState["AttachRecordName"].ToString() == string.Empty)
                            {
                                txtEntryName.Text = ViewState["AttachSubject"].ToString() + " " + ViewState["attachrecordid"].ToString();
                            }
                            else
                            {
                                txtEntryName.Text = ViewState["AttachSubject"].ToString() + " : " + ViewState["AttachRecordName"].ToString();
                            } 
                        }
                    }
                    #endregion

                    //##Attach
                    string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CreateDiary.aspx"), "CreateDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                    ClientScript.RegisterStartupScript(this.GetType(), "CreateDiaryValidations", sValidationResources, true);
                    
                    TextBox txtDueDate = (TextBox)this.FindControl("CompleteDate_date");
                    if (txtDueDate != null && string.IsNullOrEmpty(txtDueDate.Text))
                    {
                        txtDueDate.Text = AppHelper.GetDate(System.DateTime.Today.ToString());
                    }

                    TextBox txtDueTime = (TextBox)this.FindControl("CompleteDate_time");
                    if (txtDueTime != null && string.IsNullOrEmpty(txtDueTime.Text))
                    {
                        txtDueTime.Text = AppHelper.GetTime(System.DateTime.Now.TimeOfDay.ToString());
                    }
                }
            
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }      
        }

        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            XElement oUseWpaTask = oMessageElement.XPathSelectElement("//UseWpaTask");
            if (oUseWpaTask != null && oUseWpaTask.Value == "-1")
            {
                this.Page.ClientScript.RegisterStartupScript(typeof(string), "WpaTaskIsSelect", "_isSelect = true;", true);
            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            TextBox txthdnDiaryLstGroups = (TextBox)this.FindControl("hdnDiaryLstGroups");//pkandhari Jira 6412
            TextBox txthdnDiaryLstUsers = (TextBox)this.FindControl("hdnDiaryLstUsers");//pkandhari Jira 6412
            TextBox actstring = (TextBox)this.FindControl("actstring");
            XmlNode TempNode = null;
            XmlNode TempNodeToAdd = null;
            bool bIsSuccess = false;
            int iEntryId = 0;

            using (XmlReader reader = Xelement.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            XElement objEntryId = Xelement.XPathSelectElement("//Document/ParamList/Param[@name='SysPropertyStore']/Instance/WpaDiaryEntry/EntryId");
            if (objEntryId != null)
                iEntryId = Conversion.CastToType<int>(objEntryId.Value, out bIsSuccess);

            TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/CalledBy");
            if (TempNode != null)
                TempNode.InnerText = "attachdiary";

            TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ActivityList");
            if (TempNode != null)
                if (actstring != null)
                    TempNode.InnerText = actstring.Text;

            if (iEntryId == 0)
            {
                TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/UserNameList");
                if (TempNode != null)
                    if (txthdnDiaryLstUsers != null)
                        TempNode.InnerText = txthdnDiaryLstUsers.Text;

                TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/GroupIdList");
                if (TempNode != null)
                    if (txthdnDiaryLstGroups != null)
                        TempNode.InnerText = txthdnDiaryLstGroups.Text;
            }

            TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/AssigningUser");
            if (TempNode != null)
            {
                TempNode.InnerText = "currentuser";
                if (iEntryId > 0 && ViewState["assigninguser"] != null)
                    TempNode.InnerText = Convert.ToString(ViewState["assigninguser"]);
            }

            TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/StatusOpen");
            if (TempNode != null)
                TempNode.InnerText = iEntryId > 0 ? "" : "1";

            if (!string.IsNullOrWhiteSpace(ViewState["attachrecordid"].ToString()))
            {
                TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysPropertyStore']/Instance/WpaDiaryEntry/AttachRecordid"); 
                if (TempNode != null)
                    TempNode.InnerText = Convert.ToString(ViewState["attachrecordid"]);

                TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysPropertyStore']/Instance/WpaDiaryEntry/AttachTable");
                if (TempNode != null)
                    TempNode.InnerText = Convert.ToString(ViewState["attachtable"]);

                TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/AttachTable");
                if (TempNode != null)
                    TempNode.InnerText = Convert.ToString(ViewState["attachtable"]);
            }

            Xelement = XElement.Parse(xmlNodeDoc.InnerXml);
        }

        public override void ModifyControls()
        {
            TextBox txtAttachRecord = (TextBox)this.FindControl("AttachRecordid");
            TextBox actstring = (TextBox)this.FindControl("actstring");
            TextBox txtActivities = (TextBox)this.FindControl("txtActivities");
            TextBox LoginID = (TextBox)((UserControl)this.FindControl("AssignedUser")).FindControl("LoginID");
            TextBox LoginUserName = (TextBox)((UserControl)this.FindControl("AssignedUser")).FindControl("LoginUserName");
            TextBox DefaultAssignedTo = (TextBox)((UserControl)this.FindControl("AssignedUser")).FindControl("DefaultAssignedTo");
            TextBox UserStr = (TextBox)((UserControl)this.FindControl("AssignedUser")).FindControl("UserName");
            TextBox UserIdStr = (TextBox)((UserControl)this.FindControl("AssignedUser")).FindControl("UserId");
            TextBox txthdnDiaryLstGroups = (TextBox)this.FindControl("hdnDiaryLstGroups");
            TextBox txthdnDiaryLstUsers = (TextBox)this.FindControl("hdnDiaryLstUsers");

            base.ModifyControls();
            XmlDocument oXmlData = base.Data;
            if (oXmlData != null)
            {
                if (txtActivities != null && actstring != null)
                {
                    if (oXmlData.SelectSingleNode("//ActivityListOption/sActs") != null)
                    {
                        txtActivities.Text = oXmlData.SelectSingleNode("//ActivityListOption/sActs").InnerText;
                        actstring.Text = txtActivities.Text;
                    }
                }

                if (txthdnDiaryLstUsers != null && oXmlData.SelectSingleNode("//UserNameList") != null)
                    txthdnDiaryLstUsers.Text = oXmlData.SelectSingleNode("//UserNameList").InnerText;

                if (txthdnDiaryLstGroups != null && oXmlData.SelectSingleNode("//GroupIdList") != null)
                    txthdnDiaryLstGroups.Text = oXmlData.SelectSingleNode("//GroupIdList").InnerText;

                if (DefaultAssignedTo != null && LoginUserName != null && LoginID != null)
                {
                    if (oXmlData.SelectSingleNode("//LoginID") != null)
                        LoginID.Text = oXmlData.SelectSingleNode("//LoginID").InnerText;
                    if (oXmlData.SelectSingleNode("//LoginUser") != null)
                        LoginUserName.Text = oXmlData.SelectSingleNode("//LoginUser").InnerText;
                    if (oXmlData.SelectSingleNode("//DefaultAssignedTo") != null)
                        DefaultAssignedTo.Text = oXmlData.SelectSingleNode("//DefaultAssignedTo").InnerText;

                    if (!string.IsNullOrEmpty(DefaultAssignedTo.Text) && DefaultAssignedTo.Text == "True" && LoginUserName.Text != "" && LoginID.Text != "")
                    {
                        if (UserStr != null)
                            UserStr.Text = LoginUserName.Text;
                        if (UserIdStr != null)
                            UserIdStr.Text = LoginID.Text;
                    }
                }
            }
        }

        private void BindData()
        {
            #region Populating Priority Dropdown
            DropDownList ddlPriority = (DropDownList)this.FindControl("Priority");
            string sPriorityOptional = RMXResourceProvider.GetSpecificObject("Optional", RMXResourceProvider.PageId("CreateDiary.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            string sPriorityImportant = RMXResourceProvider.GetSpecificObject("Important", RMXResourceProvider.PageId("CreateDiary.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            string sPriorityRequired = RMXResourceProvider.GetSpecificObject("Required", RMXResourceProvider.PageId("CreateDiary.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            if (ddlPriority != null)
            {
                ddlPriority.Items.Clear();
                ddlPriority.Items.Insert(0, new ListItem(sPriorityOptional, "1"));
                ddlPriority.Items.Insert(1, new ListItem(sPriorityImportant, "2"));
                ddlPriority.Items.Insert(2, new ListItem(sPriorityRequired, "3"));
            }
            #endregion
        }

        protected new void NavigateSave(object sender, EventArgs e)
        {
            TextBox oSysIsServiceError = (TextBox)this.FindControl("SysIsServiceError");

            try
            {
                base.NavigateSave(sender, e);

                if (oSysIsServiceError != null && oSysIsServiceError.Text != "0")
                {
                    this.Page.ClientScript.RegisterStartupScript(typeof(string), "AttachedDiary", " if(window.opener.document.getElementById('containsopendiaries')!=null) window.opener.document.getElementById('containsopendiaries').value = true; if(window.opener.document.forms[0].id=='frmDiaryList') {window.opener.document.forms[0].__EVENTTARGET.value = 'attachdiary';window.opener.document.forms[0].submit();} self.close();", true);
                }
            }
            catch (ApplicationException ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }
    }
}