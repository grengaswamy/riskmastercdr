﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.Shared.Controls;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Generic;

namespace Riskmaster.UI.FDM
{
    public partial class Policyenh :FDMBasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
        }

        protected override void LoadViewState(object savedState)
        {

            base.LoadViewState(savedState);
            if (IsPostBack)
            {
                if (ViewState["FORMTABpremcalc_controls"] != null)
                {

                    Control ctrl = this.Form.FindControl("FORMTABpremcalc");
                    if (ctrl != null)
                    {
                        string response = AppHelper.UnZip((string)ViewState["FORMTABpremcalc_controls"]);
                        Control ctrl2 = ParseControl(response);
                        ctrl.Controls.Clear();
                        ctrl.Controls.Add(ctrl2);
                    }

                }
            }

        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            Control oControl = this.FindControl("rdoEventMadeCoverage");
            if (oControl != null)
            {
                SetIgnoreValueAttribute(oControl);
            }
            oControl = this.FindControl("rdoClaimMadeCoverage");
            if (oControl != null)
            {
                SetIgnoreValueAttribute(oControl);
            }

            HtmlContainerControl oDivControl = (HtmlContainerControl)this.FindControl("FORMTABtransactionhistory");
            if (oDivControl != null)
            {
                oDivControl.Attributes["class"] = "gridtopborder";
            }

            oDivControl = (HtmlContainerControl)this.FindControl("div_TransactionHistoryGrid");
            if (oDivControl != null)
            {
                oDivControl.Attributes["class"] = "completerow";
            }

            oDivControl = (HtmlContainerControl)this.FindControl("div_ExposureListGrid");
            if (oDivControl != null)
            {
                oDivControl.Attributes["class"] = "completerow";
                //MITS 20517
                oDivControl.Attributes["style"] = "position: absolute;overflow: auto;left: 5px;right: 5px;top: 5px;bottom: 5px";
                //MITS 20517
            }
            //MITS 20517:Positioning div_ExposureListGrid as absolute disturbed the layout of ExpsoureSum and ExposureManualPremium 
            oDivControl = (HtmlContainerControl)this.FindControl("div_ExposureSumAndPremium");
            if (oDivControl != null)
            {

                oDivControl.Attributes["style"] = "position: absolute;top:250px"; //Mits 23001

            }
            //MITS 20517:Positioning div_ExposureListGrid as absolute disturbed the layout of ExpsoureSum and ExposureManualPremium 
            oDivControl = (HtmlContainerControl)this.FindControl("div_CoveragesGrid");
            if (oDivControl != null)
            {
                oDivControl.Attributes["class"] = "completerow";
                //Start:Neha Suresh Jain,05/25/2010,MITS 20517
                oDivControl.Attributes["style"] = "position: absolute;overflow: auto;left: 5px;right: 5px;top: 5px;bottom: 5px";
                //End:Neha Suresh Jain, MITS 20517
            }
            //Mona: all levels of org hierarchy can be selected.Earlier it was a orghalllinks type of control
            oControl = this.FindControl("newlastnamebtn");
            if (oControl != null)
            {
                ((Button)oControl).OnClientClick = "return selectCode('orgh','newlastname','ALL');";
            }
            //Sumit-MITS#18251 -10/13/2009 -Hide Policy Comments Memo
            oDivControl = (HtmlContainerControl)this.FindControl("div_PolicyComments");
            if (oDivControl != null)
            {
                oDivControl.Attributes["style"] = "display:none";
            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            // Check if this needs to be executed or not
            TextBox txtCvgEditSessionId = (TextBox) this.FindControl("CoveragesGrid_EditRowSessionId");
            TextBox txtExpEditSessionId = (TextBox)this.FindControl("ExposureListGrid_EditRowSessionId");
            TextBox txtPostBackAction = (TextBox)this.FindControl("PostBackAction");
            TextBox txtCvgAction = (TextBox)this.FindControl("CoveragesGridAction");
            TextBox txtExpAction = (TextBox)this.FindControl("ExposureListGridAction");
            if (txtPostBackAction.Text == "COVERAGES_EDIT" && txtCvgAction.Text == "edit")
            {
                string[] sCoverageSessions = txtCvgEditSessionId.Text.Split(new char[] { '|' });

                XElement objSession = Xelement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/Coverages/option/CoveragesId[.=\"" + sCoverageSessions[0] + "\"]/../SessionId");
                if (objSession != null && sCoverageSessions[1] != "")
                    objSession.Value = sCoverageSessions[1];

            }
            if (txtPostBackAction.Text == "EXPOSURE_EDIT" && txtExpAction.Text == "edit")
            {
                string[] sExposureSessions = txtExpEditSessionId.Text.Split(new char[] { '|' });

                XElement objSession = Xelement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ExposureList/option/ExposureId[.=\"" + sExposureSessions[0] + "\"]/../SessionId");
                if (objSession != null && sExposureSessions[1] != "")
                    objSession.Value = sExposureSessions[1];

            }            
        }

        private int GetIndexFromColumnName(GridView gvData, string p_sColumnName, bool bHidden)
        {
            try
            {
                string sColumnName = p_sColumnName + (bHidden ? "_Hidden" : "");
                for (int i = gvData.Columns.Count - 1; i > 0; i--)
                {
                    if (gvData.Columns[i].HeaderText == sColumnName)
                        return i;
                }
                return 0;
            }
            catch
            {
                return 0;
            }


        }

    }
}
