﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cmxtreatmentplnlist.aspx.cs" Inherits="Riskmaster.UI.FDM.CmXTreatmentPlnList" ValidateRequest="false" EnableViewStateMac="false"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>


<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>Untitled Page</title>
     <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <link href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" src="Scripts/drift.js"></script> 
	<script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script> 
  <script type="text/javascript" src="../../Scripts/pagingsorting.js"></script>   
</head>
<body onload="pageLoaded()" class="10pt">
    <form id="frmData" runat="server">
    <%-- Mits 28937  --%> 
    <asp:ImageButton runat="server" src="../../Images/tb_new_active.png" onMouseOver="this.src='../../Images/tb_new_mo.png';" onMouseOut="this.src='../../Images/tb_new_active.png';"  Height="28" Width="28" BorderWidth="0" runat="server" id="lblAddNew" tooltip="<%$ Resources:imgbtnAddNew %>"  postbackurl="cmxtreatmentpln.aspx?SysFormName=CmXTreatmentPln&SysCmd=&SysViewType=tab&SysFormIdName=cmtprowid&SysFormPIdName=casemgtrowid&SysEx=CasemgtRowId|EventNumber&SysExMapCtl=CasemgtRowId|EventNumber" />
    <asp:ImageButton runat="server" ID="imgBtnDelete" Text="<%$ Resources:imgbtnDelete %>" OnClick="NavigateListDelete"  OnClientClick="return DeleteList();" src="../../Images/tb_delete_active.png" onMouseOver="this.src='../../Images/tb_delete_mo.png';" onMouseOut="this.src='../../Images/tb_delete_active.png';"  Height="28" Width="28" BorderWidth="0" Tooltip ="<%$ Resources:imgbtnDelete %>" />
    <asp:ImageButton runat="server" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;" src="../../Images/tb_backrecord_active.png" width="28" height="28" border="0" id="BackToParent" AlternateText="<%$ Resources:imgbtnBack %>" Tooltip ="<%$ Resources:imgbtnBack %>"  onMouseOver="this.src='../../Images/tb_backrecord_mo.png';" onMouseOut="this.src='../../Images/tb_backrecord_active.png';" />
    <%-- End of Mits 28937--%>
    <div>
    <asp:Label ID="lblError" runat="server" Text="" ForeColor=Red></asp:Label>
            <%--<h2>Treatment Plan / Medical Chronology</h2>--%>
            <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
                <tr class="msgheader">
                    <td class="msgheader" colspan=""><asp:Label id="lbltreatmentplnlist" runat="server" Class="required" Text="<%$ Resources:lbltreatmentplnlist %>" /></td>
                </tr>
            </table>
            </br>
    <table border="0" width="98%" cellspacing="0" cellpadding="2" id="tbllist">
    <tr>
     <td class="ctrlgroup" width="50px"></td>
     <td class="ctrlgroup" width="50px"></td>
      <td class="ctrlgroup" style="width:14%" ><asp:HyperLink href="#" runat="server" id="HeaderColumn2" class="ctrlgroup" onclick="SortList(document.all.tbllist,2,'lnkEdit',true );" Text='<%$ Resources:hyplnkprovidergroup %>'></asp:HyperLink></td>
     <td class="ctrlgroup" style="width:10%" ><asp:HyperLink href="#" runat="server"   id="HeaderColumn3" class="ctrlgroup" onclick="SortList(document.all.tbllist,3,'lnkEdit',true );" Text='<%$ Resources:hyplnkdaterequested %>'></asp:HyperLink></td>
     <td class="ctrlgroup" style="width:10%"><asp:HyperLink href="#" runat="server"  id="HeaderColumn4" class="ctrlgroup" onclick="SortList(document.all.tbllist,4,'lnkEdit',true );" Text='<%$ Resources:hyplnktreatmenttype %>'></asp:HyperLink></td>
     <td class="ctrlgroup" style="width:10%" ><asp:HyperLink href="#" runat="server"  id="HeaderColumn5" class="ctrlgroup" onclick="SortList(document.all.tbllist,5,'lnkEdit',true );" Text='<%$ Resources:hyplnkrequestedtreatment %>'></asp:HyperLink></td>
     <td class="ctrlgroup" style="width:7%" ><asp:HyperLink href="#" runat="server"  id="HeaderColumn6" class="ctrlgroup" onclick="SortList(document.all.tbllist,6,'lnkEdit',true );" Text='<%$ Resources:hyplnkreason %>'></asp:HyperLink></td>
     <td class="ctrlgroup" style="width:10%" ><asp:HyperLink href="#" runat="server"  id="HeaderColumn7" class="ctrlgroup" onclick="SortList(document.all.tbllist,7,'lnkEdit',true );" Text='<%$ Resources:hyplnkCPTprocedure %>'></asp:HyperLink></td>
     <td class="ctrlgroup" style="width:10%" ><asp:HyperLink href="#" runat="server"  id="HeaderColumn8" class="ctrlgroup" onclick="SortList(document.all.tbllist,8,'lnkEdit',true );" Text='<%$ Resources:hyplnkapprovedtreatment %>'></asp:HyperLink></td>
     <td class="ctrlgroup" ><asp:HyperLink href="#" runat="server"  id="HeaderColumn9" class="ctrlgroup" onclick="SortList(document.all.tbllist,9,'lnkEdit',true );" Text='<%$ Resources:hyplnkauthstartdate %>'></asp:HyperLink></td>
     <td class="ctrlgroup" ><asp:HyperLink href="#" runat="server"  id="HeaderColumn10" class="ctrlgroup" onclick="SortList(document.all.tbllist,10,'lnkEdit',true );" Text='<%$ Resources:hyplnkauthenddate %>' ></asp:HyperLink></td>
    </tr>
    <%int i = 0; foreach (XElement item in result)
    {
        string rowclass = "";
        if ((i % 2) == 1) rowclass = "datatd1";
        else rowclass = "datatd";
        i++;
        %>
        <tr>                          
            <td class="<%=rowclass%>">
            <%
                PageId.ID = "PageId" + i;
                PageId.Value = item.Element("CmtpRowId").Value;
           %>                              
                <input type ="checkbox" name="PageId" id="PageId" title="Treatment Plan / Medical Chronology" runat="server"/></td>                            
            <td class="<%=rowclass%>">
               <%
                   lnkEdit.ID = "lnkEdit" + i;
                lnkEdit.PostBackUrl = "cmxtreatmentpln.aspx?SysFormName=CmXTreatmentPln&SysCmd=0&SysViewType=tab&SysFormIdName=cmtprowid&SysFormId=" + item.Element("CmtpRowId").Value + "&SysFormPIdName=casemgtrowid&SysEx=CasemgtRowId|EventNumber&SysExMapCtl=CasemgtRowId|EventNumber";
                %>
              <asp:LinkButton runat="server" id="lnkEdit" Text="<%$ Resources:hyplnkEdit %>"></asp:LinkButton> 
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("ProviderEid").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=AppHelper.GetDate(item.Element("DateRequested").Value)%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("TreatmentTypeCode").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("RequestedTreatment").Value%> 
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("Reason").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("ProcedureCode").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("ApprovedTreatment").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=AppHelper.GetDate(item.Element("DateAuthStart").Value)%>
            </td>
            <td class="<%=rowclass%>">
                <%=AppHelper.GetDate(item.Element("DateAuthEnd").Value)%>
            </td>
            <td style="display:none">                           
            </td>
            <td style="display:none">                           
               <%=lnkEdit.PostBackUrl%>
            </td>    
            <td style="display:none">
              <%=item.Element("CmtpRowId").Value%>
            </td> 
       </tr>                          
 <% }%> 
    
    </table>
    <center><div id="pageNavPosition"></div></center>  
    <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
    <asp:TextBox runat="server" id="TextBox1" style="display:none" />
    <asp:TextBox runat="server" id="DeleteListTemplate"  style="display:none" Text="Instance/CmXTreatmentPlnList/CmXTreatmentPln/CmtpRowId"/>				
    <asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
    <asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
    <asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
    <asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
    <asp:TextBox runat="server" id="SysClassName" style="display:none" Text="CmXTreatmentPlnList" RMXRef="Instance/UI/FormVariables/SysClassName"/>						
    <asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" Text="&lt;CmXTreatmentPlnList&gt;&lt;CmXTreatmentPln /&gt;&lt;/CmXTreatmentPlnList&gt;"	RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>	
    <asp:TextBox runat="server" id="SysSerializationConfigIgnore" style="display:none" Text="Claim.Comments"	RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore"/>						                             
    <asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm"/>
    <asp:TextBox runat="server" id="SysFormPIdName" Text="casemanagementid" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
    <asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
    <asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>
    <asp:TextBox runat="server" id="SysNotReqNew" style="display:none" RMXRef="Instance/UI/FormVariables/SysNotReqNew" />
    <asp:TextBox runat="server" id="SysFormName" style="display:none" Text="CmXTreatmentPlnList" RMXRef="Instance/UI/FormVariables/SysFormName"/>
    <asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="casemanagementid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId"/>
    <%--  <asp:TextBox runat="server" id="SysSid" style="display:none" Text="11000" RMXRef="Instance/UI/FormVariables/SysSid"/>--%> <%--zmohammad merging MITS 32104 from 12.2 to 13.1--%>
    <asp:TextBox runat="server" id="SysSid" style="display:none" Text="" RMXRef="Instance/UI/FormVariables/SysSid"/> <%--zmohammad merging MITS 32104 from 12.2 to 13.1--%>
    <asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
    <asp:TextBox ID="paramstore" runat="server" style="display:none"></asp:TextBox>	
    <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none"/>
    <asp:TextBox runat="server" id="eventnumber" RMXRef="Instance/UI/FormVariables/SysExData/EventNumber" style="display:none"/>
    <asp:TextBox runat="server" id="claimid" RMXRef="Instance/UI/FormVariables/SysExData/ClaimId" style="display:none"/>
    <asp:TextBox runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" style="display:none"/>
    <asp:TextBox runat="server" id="casemanagementid" RMXRef="Instance/UI/FormVariables/SysExData/CasemgtRowId" style="display:none"/>
    <asp:TextBox runat="server" id="casemgtrowid" RMXRef="Instance/UI/FormVariables/SysExData/CasemgtRowId" style="display:none"/>
    <asp:TextBox runat="server" id="cmtprowid" RMXRef="Instance/UI/FormVariables/SysExData/CmtpRowId"   style="display:none"/>   
    <%-- Mits 28937  --%>        
    <%--<asp:button class="button" runat="server" id="btnAddNew" Text="Add New" postbackurl="cmxtreatmentpln.aspx?SysFormName=CmXTreatmentPln&SysCmd=&SysViewType=tab&SysFormIdName=cmtprowid&SysFormPIdName=casemgtrowid&SysEx=CasemgtRowId|EventNumber&SysExMapCtl=CasemgtRowId|EventNumber" />
    <asp:Button class="button" runat="server" ID="btnDelete" Text="Delete" OnClick="NavigateListDelete"  OnClientClick="return DeleteList();"/>
    <asp:Button class="button" runat="server" ID="Back" Text="Back" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>    --%>
    <%-- End of Mits 28937  --%>
    <input type="text" runat="server" id="PageIds" style="display:none"/>
    </div>
     <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
     <script type="text/javascript">
        var listpaging = new ListPaging('tbllist', 3); 
        listpaging.init(); 
        listpaging.showPageNavigation('listpaging', 'pageNavPosition'); 
        listpaging.showPage(1);
    </script>
</body>
</html>
