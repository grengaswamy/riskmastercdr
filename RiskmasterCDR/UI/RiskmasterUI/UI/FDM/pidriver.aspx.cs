﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Riskmaster.UI.FDM
{
    public partial class Pidriver : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();

            //If existing driver has been added then datachange flag has to be true
            if (AppHelper.GetQueryStringValue("DataChange") == "true")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "datachangescript", "<script type='text/javascript'>setDataChanged(true);</script>");
            }
        }
    }
}
