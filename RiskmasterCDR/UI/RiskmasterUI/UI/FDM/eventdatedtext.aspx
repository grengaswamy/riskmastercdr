<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventDatedText.aspx.cs" Inherits="Riskmaster.UI.FDM.EventDatedText" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
    <head id="Head1" runat="server">
        <title>Event Dated Text</title>
        <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
        <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
        <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />
        <script language="JavaScript" type="text/javascript" src="Scripts/form.js">{var i;}  </script>
        <script language="JavaScript" type="text/javascript" src="Scripts/drift.js">{var i;}  </script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    </head>
    
    <body class="10pt" onload="pageLoaded();">
        <form name="frmData" id="frmData" runat="server">
            <asp:label id="lblError" runat="server" text="" forecolor="Red" />
            <asp:hiddenfield runat="server" id="wsrp_rewrite_action_1" value="" />
            <asp:textbox style="display: none" runat="server" name="hTabName" id="hTabName" />
            <asp:scriptmanager id="SMgr" runat="server" />
            
            <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28" height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" />
                </div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave" src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save" validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" />
                </div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28" height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" />
                </div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28" height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" />
                </div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28" height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" />
                </div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28" height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" />
                </div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif" width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" />
                </div>
                <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doLookup()" src="Images/lookup.gif" width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='Images/lookup2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/lookup.gif';this.style.zoom='100%'" />
                </div>
                <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="attach()" src="Images/attach.gif" width="28" height="28" border="0" id="attach" alternatetext="Attach Documents" onmouseover="this.src='Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/attach.gif';this.style.zoom='100%'" />
                </div>
                <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Diary()" src="Images/diary.gif" width="28" height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='Images/diary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/diary.gif';this.style.zoom='100%'" />
                </div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif" width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary" onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" />
                </div>
            </div>
            <br />
            
            <div class="msgheader" id="div_formtitle" runat="server">
                <asp:label id="formtitle" runat="server" text="Event Dated Text" />
                <asp:label id="formsubtitle" runat="server" text="" />
            </div>
            
            <div id="Div1" class="errtextheader" runat="server">
                <asp:label id="formdemotitle" text="" />
            </div>
            
            <div id="Div2" class="tabGroup" runat="server">
                <div class="Selected" nowrap="true" runat="server" name="TABSdatedtextgroup" id="TABSdatedtextgroup">
                    <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="datedtextgroup" id="LINKTABSdatedtextgroup"><span style="text-decoration: none">Dated Text</span></a>
                </div>
                <div id="Div3" class="tabSpace" runat="server">&nbsp;&nbsp;</div>
            </div>
            
            <div class="singleTopborder" runat="server" name="FORMTABdatedtextgroup" id="FORMTABdatedtextgroup">
            
                <asp:textbox style="display: none" runat="server" id="eventid" rmxref="/Instance/EventXDatedText/EventId" rmxtype="id" />
                <asp:textbox style="display: none" runat="server" id="evdtrowid" rmxref="/Instance/EventXDatedText/EvDtRowId" rmxtype="id" />
                <asp:textbox style="display: none" runat="server" id="eventnumber" rmxref="Instance/UI/FormVariables/SysExData/EventNumber" rmxtype="id" />
                
                <div runat="server" class="row" id="div_dateentered" xmlns="">
                    <span class="required">Date Entered</span>
                    <span class="formw">
                        <asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);" id="dateentered" rmxref="/Instance/EventXDatedText/DateEntered" rmxtype="date" tabindex="1" />
                        <asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="dateentered" />
                        <cc1:CalendarExtender runat="server" ID="dateentered_ajax" TargetControlID="dateentered" />
                    </span>
                </div>
                
                <div runat="server" class="row" id="div_timeentered" xmlns="">
                    <span class="required">Time Entered</span>
                    <span class="formw">
                        <asp:textbox runat="server" formatas="time" onchange="setDataChanged(true);" id="timeentered" rmxref="/Instance/EventXDatedText/TimeEntered" rmxtype="time" tabindex="3" />
                        <cc1:MaskedEditExtender runat="server" Enabled="True" MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="timeentered" ID="timeentered_ajax" />
                        <cc1:MaskedEditValidator ValidationGroup="vgSave" runat="server" AcceptAMPM="true" InvalidValueMessage="Time is invalid" Display="Dynamic" TooltipMessage="" ID="timeentered_ajaxvalidator" ControlExtender="timeentered_ajax" ControlToValidate="timeentered" IsValidEmpty="False" EmptyValueMessage="Required" />
                    </span>
                </div>
                
                <div runat="server" class="row" id="div_enteredbyuser" xmlns="">
                    <span class="required">Entered By User</span>
                    <span class="formw">
                        <asp:textbox runat="server" onchange="setDataChanged(true);" id="enteredbyuser" rmxref="/Instance/EventXDatedText/EnteredByUser" rmxtype="text" />
                        <asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="enteredbyuser" />
                    </span>
                </div>
                
                <div runat="server" class="row" id="div_datedtext" xmlns="">
                    <span class="label">Dated Text</span>
                    <span class="formw">
                        <asp:textbox runat="Server" id="datedtext" rmxref="/Instance/EventXDatedText/DatedText" rmxtype="textml" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="5" textmode="MultiLine" columns="30" rows="5" />
                        <asp:textbox style="display: none" runat="server" id="datedtext_HTML" rmxref="/Instance/EventXDatedText/DatedText_HTMLComments" text="" />
                        <input type="button" class="button" value="..." name="datedtextbtnMemo" onclick="EditHTMLMemo('datedtext','')" />
                    </span>
                </div>
                
            </div>
            
            <div id="Div4" class="formButtonGroup" runat="server">
                <div class="formButton" runat="server" id="div_btnBack">
                    <asp:button class="button" runat="Server" id="btnBack" text="Back to Event" onclientclick="return XFormHandler('','1','back','');" />
                </div>
             </div>
             
             <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd" rmxtype="hidden" text="" />
             <asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave" rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" />
             <asp:textbox style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue" rmxtype="hidden" text="" />
             <asp:textbox style="display: none" runat="server" id="SysCmdText" rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" />    
             <asp:textbox style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName" rmxtype="hidden" text="EventXDatedText" />
             <asp:textbox style="display: none" runat="server" id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig" rmxtype="hidden" name="SysSerializationConfig" text="&lt;EventXDatedText&gt;&lt;/EventXDatedText&gt;" />
             <asp:textbox style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName" rmxtype="hidden" text="eventdatedtext" />
             <asp:textbox style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId" rmxtype="hidden" text="" />
             <asp:textbox style="display: none" runat="server" id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden" text="" />
             <asp:textbox style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid" rmxtype="hidden" text="12650" />
             <asp:textbox style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType" rmxtype="hidden" text="" />
             <asp:textbox style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName" rmxtype="hidden" text="evdtrowid" />
             <asp:textbox style="display: none" runat="server" id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden" text="eventid" />
             <asp:textbox style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm" rmxtype="hidden" text="event" />
             <asp:textbox style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx" rmxtype="hidden" text="eventnumber" />
             <asp:textbox style="display: none" runat="server" id="SysNotReqNew" rmxref="Instance/UI/FormVariables/SysNotReqNew" rmxtype="hidden" text="" />
             <asp:textbox style="display: none" runat="server" name="formname" value="eventdatedtext" />
             <asp:textbox style="display: none" runat="server" name="SysRequired" value="dateentered|timeentered|enteredbyuser|" />
             <asp:textbox style="display: none" runat="server" name="SysFocusFields" value="dateentered|" />
             <input type="hidden" id="hdSaveButtonClicked" />
             <asp:textbox runat="server" id="SysInvisible" style="display: none" />
             <asp:textbox runat="server" id="SysLookupClass" style="display: none" />
             <asp:textbox runat="server" id="SysLookupRecordId" style="display: none" />
             <asp:textbox runat="server" id="SysLookupAttachNodePath" style="display: none" />
             <asp:textbox runat="server" id="SysLookupResultConfig" style="display: none" />
             <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
        </form>
    </body>
</html>
