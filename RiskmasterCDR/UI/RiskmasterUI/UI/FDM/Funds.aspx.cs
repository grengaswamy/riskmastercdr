﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Xml.XPath;
using Riskmaster.Common;
using Riskmaster.AppHelpers;
using Riskmaster.Common.Extensions;
using Riskmaster.UI.Shared.Controls;
using System.Data;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.FDM
{
    public partial class Funds : FDMBasePage
    { // skhare7 MITS 23664
        string sPrintMode = string.Empty;
        // skhare7 MITS 23664 End
        //Deb : MITS 31211
        string sBRSSplit = string.Empty;
        string sSplitTotal = string.Empty;
        //Deb : MITS 31211
       
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bKeyFound = false;
            //Amitosh for EFT Payments

            DropDownList bankaccount = (DropDownList)Page.FindControl("bankaccount");
            TextBox EftBank = (TextBox)Page.FindControl("EFTBanks");
            TextBox PmtcurrencyType = (TextBox)Page.FindControl("currencytype");
            TextBox CombPayeeCurrtype = (TextBox)Page.FindControl("CombPayeeCurrtype");
            ListBox entitylist = (ListBox)Page.FindControl("entitylist");
            TextBox hdnIsEFTPayment = (TextBox)Page.FindControl("hdnIsEFTPayment"); //JIRA:438 : ajohari2

            string[] arrCurrtype = null;

            //JIRA:438 START: ajohari2
            //if (((CheckBox)Page.FindControl("IsEFTPayment")) != null)
            //{
            //    if (EftBank != null)
            //    {
            //        string[] seftbanks = EftBank.Text.Split(',');
            //        foreach (string sbank in seftbanks)
            //        {
            //            if (sbank.Equals(bankaccount.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
            //            {
            //                ((CheckBox)Page.FindControl("IsEFTPayment")).Checked = true;
            //                break;

            //            }
            //            else
            //            {
            //                ((CheckBox)Page.FindControl("IsEFTPayment")).Checked = false;
            //            }
            //        }
            //    }
            //}
			//pgupta93: RA-4608 START            
            if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("FormName")))
            {
                if (Page.FindControl("FDHButton") != null)
                {
                    if (Page.FindControl("FDHButton").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("FDHButton")).Text = AppHelper.GetQueryStringValue("FormName");
                    }
                    else if (Page.FindControl("FDHButton").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        ((Label)Page.FindControl("FDHButton")).Text = AppHelper.GetQueryStringValue("FormName");
                    }
                }
            }
            //pgupta93: RA-4608 END
			
			// npadhy JIRA 6418 Starts As we have removed the EFTPayment control from Funds page, we remove any refernce of the control from the code behind as well
            //if (((CheckBox)Page.FindControl("IsEFTPayment")) != null)
            //{
                if (EftBank != null)
                {
                    string[] seftbanks = EftBank.Text.Split(',');
                    foreach (string sbank in seftbanks)
                    {

                        if (sbank.Equals(bankaccount.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
                        {

                            if (sPrintMode.Trim().CompareTo("P Printed") == 0)
                            {
                                //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = false;
                                // If the Print mode is printed the we should not allow Distribution Type to Change
                                ((CodeLookUp)Page.FindControl("distributiontype")).Enabled = false;
                            }
                            else
                            {
                                //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = true;
                                //((CodeLookUp)Page.FindControl("distributiontype")).Enabled = true;
                                DatabindingHelper.EnableControls("distributiontype", this);
                            }
                            break;
                        }
                        else
                        {
                            //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = false;
                            //this validation to be added in save, that if the Bank is not an EFT bank and we are saving the Distribution Type as EFT, then it should throw error
                        }

                    }
                }


            //}
            //JIRA:438 End:
			// npadhy JIRA 6418 Ends As we have removed the EFTPayment control from Funds page, we remove any refernce of the control from the code behind as well

            //End Amitosh
            //skhare7 Combined Payment
            TextBox CombPayeeAccountId = (TextBox)Page.FindControl("CombPayeeAccountId");
            if ((CombPayeeAccountId != null) && (Page.FindControl("combinedpayflag") != null))
            {
                if (((CheckBox)Page.FindControl("combinedpayflag")).Checked)
                {

                    if (CombPayeeAccountId.Text.Equals(bankaccount.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
                    {
                        ((CheckBox)Page.FindControl("combinedpayflag")).Checked = true;

                    }
                    else
                    {
                        ((CheckBox)Page.FindControl("combinedpayflag")).Checked = false;
                    }
                }
            }


            //Amitosh for Combined Payment
            int iIndexId = 0;
            string[] array = null;

            if (IsPostBack)
            {
                //Start - averma62 - 31440 - Issue was fixed for MITS #28337 but no condition was put for labels.

                //Control paytotheorder = Page.FindControl("paytotheorder");
                //if (paytotheorder != null && Request.Form["entitylist"] != null)
                //{
                //    TextBox paytotheorderTxt = (TextBox)paytotheorder;
                //    paytotheorderTxt.Text = Request.Form["paytotheorder"];
                //}
                if (Page.FindControl("paytotheorder") != null)
                {
                    if (Page.FindControl("paytotheorder").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("paytotheorder")).Text = Request.Form["paytotheorder"];
                    }
                    else if (Page.FindControl("paytotheorder").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        ((Label)Page.FindControl("paytotheorder")).Text = Request.Form["paytotheorder"];
                    }
                }


                if (Page.FindControl("transactionnotes") != null)
                {
                    if (Page.FindControl("transactionnotes").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("transactionnotes")).Text = Request.Form["transactionnotes"];
                    }
                    else if (Page.FindControl("transactionnotes").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        ((Label)Page.FindControl("transactionnotes")).Text = Request.Form["transactionnotes"];
                    }
                }
                //pgupta93: RMA-7112 START
                if (Page.FindControl("collectionflag") != null)
                {
                    if (Page.FindControl("collectionflag").GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                    {
                        if (((CheckBox)Page.FindControl("collectionflag")).Checked)
                        {
                            if (Page.FindControl("lbl_cbopayeetype") != null)
                            {
                                if (Page.FindControl("lbl_cbopayeetype").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_cbopayeetype")).Text = RMXResourceProvider.GetSpecificGlobalObject("PayorType");
                                }
                            }

                            if (Page.FindControl("lbl_entitylist") != null)
                            {
                                if (Page.FindControl("lbl_entitylist").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {

                                    ((Label)Page.FindControl("lbl_entitylist")).Text = RMXResourceProvider.GetSpecificGlobalObject("Payor");
                                }
                            }
                           
                        }
                    }
                }
                if (Page.FindControl("paymentflag") != null)
                {
                    if (Page.FindControl("paymentflag").GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                    {
                        if (((CheckBox)Page.FindControl("paymentflag")).Checked)
                       
                        {
                            if (Page.FindControl("lbl_cbopayeetype") != null)
                            {
                                if (Page.FindControl("lbl_cbopayeetype").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_cbopayeetype")).Text = RMXResourceProvider.GetSpecificGlobalObject("PayeeType");
                                }
                            }

                            if (Page.FindControl("lbl_entitylist") != null)
                            {
                                if (Page.FindControl("lbl_entitylist").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {

                                    ((Label)Page.FindControl("lbl_entitylist")).Text = RMXResourceProvider.GetSpecificGlobalObject("Payees");
                                }
                            }
                            
                        }
                    }
                }
                //pgupta93: RMA-7112 END
                //Changes for MITS 28337 sgupta243
                //Control transactionnotes = Page.FindControl("transactionnotes");
                //if (transactionnotes != null)
                //{
                //    TextBox transactionnotesTxt = (TextBox)transactionnotes;
                //    transactionnotesTxt.Text = Request.Form["transactionnotes"];
                //}
                //END for MITS 28337 sgupta243

                //Ankit Start : Changes for MITS 30936
                //Control checkmemo = Page.FindControl("checkmemo");
                //if (checkmemo != null)
                //{
                //    TextBox checkmemoTxt = (TextBox)checkmemo;
                //    checkmemoTxt.Text = Request.Form["checkmemo"];
                //}

                if (Page.FindControl("checkmemo") != null)
                {
                    if (Page.FindControl("checkmemo").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("checkmemo")).Text = Request.Form["checkmemo"];
                    }
                    else if (Page.FindControl("checkmemo").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        ((Label)Page.FindControl("checkmemo")).Text = Request.Form["checkmemo"];
                    }
                }

                //Ankit End

                //End - averma62 - 31440 - Issue was fixed for MITS #28337 but no condition was put for labels.

                if ((((Label)Page.FindControl("pmtcurrencytypetext")) != null && (((Label)Page.FindControl("pmtcurrencytypetext")).Visible == true)))
                {
                    if (CombPayeeCurrtype != null)
                    {
                        if (Page.FindControl("combinedpayflag") != null)//Added by bsharma33 MITS 29613
                        {
                            if (((CheckBox)Page.FindControl("combinedpayflag")).Checked)
                            {

                                if (((TextBox)Page.FindControl("currencytypetext$codelookup_cid")) != null)
                                {
                                    if (CombPayeeCurrtype.Text.Equals(((TextBox)Page.FindControl("currencytypetext$codelookup_cid")).Text, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        ((CheckBox)Page.FindControl("combinedpayflag")).Checked = true;

                                    }
                                    else
                                    {
                                        ((CheckBox)Page.FindControl("combinedpayflag")).Checked = false;

                                    }
                                }

                            }
                        }
                    }
                }
                //skhare7 MITS 28081End 
                //skhare7 changed by shobhana as text was added to entity list but value was not there
                if (entitylist != null && Request.Form["entitylist"] != null)
                {
                    string[] payeeText = Request.Form["entitylist"].Split(',');
                    entitylist.Items.Clear();

                    DataTable dt = new DataTable();

                    dt.Columns.Add("PayeeText");

                    dt.Columns.Add("PayeeValue");
                    foreach (string payee in payeeText)
                    {
                        if (payee.Contains("_"))
                        {
                            array = payee.Split("_".ToCharArray()[0]);

                            if (array[0].Trim() != "" && array[1].Trim() != "")
                            {

                                DataRow dr = dt.NewRow();

                                dr["PayeeText"] = array[0].ToString();

                                dr["PayeeValue"] = array[0].ToString() + "_" + array[1].ToString();

                                dt.Rows.Add(dr);

                                dt.AcceptChanges();

                            }
                        }
                    }
                    entitylist.DataTextField = dt.Columns[0].ToString();
                    entitylist.DataValueField = dt.Columns[1].ToString();

                    entitylist.DataSource = dt;

                    entitylist.DataBind();
                }
            }
            //End Amitosh
            string dToday = DateTime.Now.ConvertToRiskmasterDate();


            if (Page.FindControl("BRSSplitTotals") != null)
            {
                //Deb : MITS 31211
                if (((Label)Page.FindControl("BRSSplitTotals")).Text.IndexOf(":") > -1)
                {
                    sBRSSplit = ((Label)Page.FindControl("BRSSplitTotals")).Text.Substring(0, ((Label)Page.FindControl("BRSSplitTotals")).Text.IndexOf(":"));
                }
                else
                {
                    sBRSSplit = ((Label)Page.FindControl("BRSSplitTotals")).Text;
                }
                //Deb : MITS 31211
                ((Label)Page.FindControl("BRSSplitTotals")).Text = string.Empty;
            }

            if (Page.FindControl("SplitTotals") != null)
            {
                //Deb : MITS 31211
                if (((Label)Page.FindControl("SplitTotals")).Text.IndexOf(":") > -1)
                {
                    sSplitTotal = ((Label)Page.FindControl("SplitTotals")).Text.Substring(0, ((Label)Page.FindControl("SplitTotals")).Text.IndexOf(":"));
                }
                else
                {
                    sSplitTotal = ((Label)Page.FindControl("SplitTotals")).Text;
                }
                //Deb : MITS 31211
                ((Label)Page.FindControl("SplitTotals")).Text = string.Empty;
            }
               

            //Raman : 08/17/2009 : MITS 17294
            //Removed the dependency on designer as control types can change in a readonly powerview
            //Start by Shivendu for MITS 19278:resetting the hidden field
            if (Page.FindControl("InsuffReserve") != null)
            {
                if (Page.FindControl("InsuffReserve").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    ((TextBox)Page.FindControl("InsuffReserve")).Text = "false"; ;
                }
            }
            //End by Shivendu for MITS 19278
            Label olblError = (Label)Page.FindControl("lblError");
            if (olblError != null)
            {
                if (olblError.Text != string.Empty)
                {
                    if (Page.FindControl("Skipreservetypes") != null)
                    {
                        if (Page.FindControl("Skipreservetypes").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                        {
                            ((TextBox)Page.FindControl("Skipreservetypes")).Text = ""; ;
                        }
                    }
                }
            }
            //if (Page.FindControl("pye_lastname") != null)
            //{
            //    if (Page.FindControl("pye_lastname").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
            //    {
            //        ((TextBox)Page.FindControl("pye_lastname")).Attributes.Add("maxlength", "100");
            //    }
            //}
            if (Page.FindControl("pye_taxid") != null)
            {
                if (Page.FindControl("pye_taxid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    ((TextBox)Page.FindControl("pye_taxid")).Attributes.Add("readonly", "readonly");
                }
            }
            if ((Page.FindControl("isClaimNumberReadOnly") != null) && (Page.FindControl("claimnumber") != null))
            {
                if (Page.FindControl("isClaimNumberReadOnly").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("isClaimNumberReadOnly")).Text == "True")
                    {
                        if (Page.FindControl("claimnumber").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                        {
                            ((TextBox)Page.FindControl("claimnumber")).Attributes.Add("readonly", "readonly");
                        }
                    }
                }
            }

            if (HandleGridData())
            {
                BindSuppleGridDataOnPostBack();// Added by Shivendu for MITS 17520
                //rsushilaggar MITS 22829 Date 11/03/2010
                // This function is for persisting the data for the disabled control b/w postbacks.
                PersistDisabledControlData();

                return;
            }

            if (HandleNewToolbarButton())
            {
                return;
            }

            if (Page.FindControl("backtotande") != null)
            {
                Page.FindControl("backtotande").Visible = false;
            }
            //Pass ClaimId, ClaimantRowId, UnitRowId, FundsType ('collection' or 'payment' or 'brs')
            foreach (string sKey in Request.QueryString)
            {
                switch (sKey)
                {
                    case "ClaimId":
                        if (AppHelper.GetQueryStringValue("ClaimID") != null)
                        {
                            if (Page.FindControl("claimid") != null)
                            {
                                if (Page.FindControl("claimid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("claimid")).Text = AppHelper.GetQueryStringValue("ClaimID");
                                }
                                else if (Page.FindControl("claimid").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("claimid")).Text = AppHelper.GetQueryStringValue("ClaimID");
                                }

                            }
                        }
                        bKeyFound = true;
                        break;
                    case "PolicyID":
                        if (AppHelper.GetQueryStringValue("PolicyID") != null)
                        {
                            if (Page.FindControl("PolicyID") != null)
                            {
                                if (Page.FindControl("PolicyID").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("PolicyID")).Text = AppHelper.GetQueryStringValue("PolicyID");
                                }
                                else if (Page.FindControl("PolicyID").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("PolicyID")).Text = AppHelper.GetQueryStringValue("PolicyID");
                                }

                            }
                        }
                        bKeyFound = true;
                        break;
                    case "RcRowId":
                        if (AppHelper.GetQueryStringValue("RcRowId") != null)
                        {
                            if (Page.FindControl("RcRowID") != null)
                            {
                                if (Page.FindControl("RcRowID").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("RcRowID")).Text = AppHelper.GetQueryStringValue("RcRowId");
                                }
                                else if (Page.FindControl("RcRowID").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("RcRowID")).Text = AppHelper.GetQueryStringValue("RcRowId");
                                }

                            }
                        }
                        bKeyFound = true;
                        break;
                    case "PolCvgId":
                        if (AppHelper.GetQueryStringValue("PolCvgId") != null)
                        {
                            if (Page.FindControl("PolCvgID") != null)
                            {
                                if (Page.FindControl("PolCvgID").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("PolCvgID")).Text = AppHelper.GetQueryStringValue("PolCvgId");
                                }
                                else if (Page.FindControl("PolCvgID").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("PolCvgID")).Text = AppHelper.GetQueryStringValue("PolCvgId");
                                }

                            }
                        }
                        bKeyFound = true;
                        break;
                    case "ResTypeCode":
                        if (AppHelper.GetQueryStringValue("ResTypeCode") != null)
                        {
                            if (Page.FindControl("ResTypeCode") != null)
                            {
                                if (Page.FindControl("ResTypeCode").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("ResTypeCode")).Text = AppHelper.GetQueryStringValue("ResTypeCode");
                                }
                                else if (Page.FindControl("ResTypeCode").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("ResTypeCode")).Text = AppHelper.GetQueryStringValue("ResTypeCode");
                                }

                            }
                        }
                        bKeyFound = true;
                        break;
                    case "ClaimantRowId":
                        if (AppHelper.GetQueryStringValue("ClaimantRowId") != null)
                        {
                            if (Page.FindControl("claimantrowid") != null)
                            {
                                if (Page.FindControl("claimantrowid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("claimantrowid")).Text = AppHelper.GetQueryStringValue("ClaimantRowId");
                                }
                                else if (Page.FindControl("claimantrowid").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("claimantrowid")).Text = AppHelper.GetQueryStringValue("ClaimantRowId");
                                }
                            }
                        }
                        bKeyFound = true;
                        break;
                    case "ClaimantEid":
                        if (AppHelper.GetQueryStringValue("ClaimantEid") != null)
                        {
                            if (Page.FindControl("clm_entityid") != null)
                            {
                                if (Page.FindControl("clm_entityid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("clm_entityid")).Text = AppHelper.GetQueryStringValue("ClaimantEid");
                                }
                                else if (Page.FindControl("clm_entityid").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("clm_entityid")).Text = AppHelper.GetQueryStringValue("ClaimantEid");
                                }

                            }
                        }
                        bKeyFound = true;
                        break;
                    case "UnitId":
                        if (AppHelper.GetQueryStringValue("UnitId") != null)
                        {
                            if (Page.FindControl("unitid") != null)
                            {
                                if (Page.FindControl("unitid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("unitid")).Text = AppHelper.GetQueryStringValue("UnitId");
                                }
                                else if (Page.FindControl("unitid").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("unitid")).Text = AppHelper.GetQueryStringValue("UnitId");
                                }
                            }
                        }
                        bKeyFound = true;
                        break;
                    case "UnitRowId":
                        if (AppHelper.GetQueryStringValue("UnitRowId") != null)
                        {
                            if (Page.FindControl("unitrowid") != null)
                            {
                                if (Page.FindControl("unitrowid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("unitrowid")).Text = AppHelper.GetQueryStringValue("UnitRowId");
                                }
                                else if (Page.FindControl("unitrowid").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("unitrowid")).Text = AppHelper.GetQueryStringValue("UnitRowId");
                                }
                            }
                        }
                        bKeyFound = true;
                        break;
                    case "FromTandE":
                        string sTandE = string.Empty;
                        if (AppHelper.GetQueryStringValue("FromTandE") != null)
                        {
                            sTandE = AppHelper.GetQueryStringValue("FromTandE");
                        }
                        if (sTandE == "True")
                        {
                            if (Page.FindControl("backtotande") != null)
                            {
                                Page.FindControl("backtotande").Visible = true;
                            }
                        }
                        else
                        {
                            if (Page.FindControl("backtotande") != null)
                            {
                                Page.FindControl("backtotande").Visible = false;
                            }
                        }
                        bKeyFound = true;
                        break;
                    case "FundsType":
                        string sFundsType = string.Empty;
                        if (AppHelper.GetQueryStringValue("FundsType") != null)
                        {
                            sFundsType = AppHelper.GetQueryStringValue("FundsType");
                        }
                        if (sFundsType == "collection")
                        {
                            if (Page.FindControl("collectionflag") != null)
                            {
                                if (Page.FindControl("collectionflag").GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                                {
                                    ((CheckBox)Page.FindControl("collectionflag")).Checked = true;
                                }
                            }
                            if (Page.FindControl("paymentflag") != null)
                            {
                                if (Page.FindControl("paymentflag").GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                                {
                                    ((CheckBox)Page.FindControl("paymentflag")).Checked = false;
                                }
                            }
                            if (Page.FindControl("brspayment") != null)
                            {
                                if (Page.FindControl("brspayment").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("brspayment")).Text = "false";
                                }
                                else if (Page.FindControl("brspayment").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("brspayment")).Text = "false";
                                }
                            }
                        }
                        else if (sFundsType == "payment")
                        {
                            if (Page.FindControl("collectionflag") != null)
                            {
                                if (Page.FindControl("collectionflag").GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                                {
                                    ((CheckBox)Page.FindControl("collectionflag")).Checked = false;
                                }
                            }
                            if (Page.FindControl("paymentflag") != null)
                            {
                                if (Page.FindControl("paymentflag").GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                                {
                                    ((CheckBox)Page.FindControl("paymentflag")).Checked = true;
                                }
                            }
                            if (Page.FindControl("brspayment") != null)
                            {
                                if (Page.FindControl("brspayment").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("brspayment")).Text = "false";
                                }
                                else if (Page.FindControl("brspayment").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("brspayment")).Text = "false";
                                }
                            }
                        }
                        else if (sFundsType == "brs")
                        {
                            if (Page.FindControl("collectionflag") != null)
                            {
                                if (Page.FindControl("collectionflag").GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                                {
                                    ((CheckBox)Page.FindControl("collectionflag")).Checked = false;
                                }
                            }
                            if (Page.FindControl("paymentflag") != null)
                            {
                                if (Page.FindControl("paymentflag").GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                                {
                                    ((CheckBox)Page.FindControl("paymentflag")).Checked = true;
                                }
                            }
                            if (Page.FindControl("brspayment") != null)
                            {
                                if (Page.FindControl("brspayment").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("brspayment")).Text = "true";
                                }
                                else if (Page.FindControl("brspayment").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("brspayment")).Text = "true";
                                }
                            }
                        }
                        bKeyFound = true;
                        break;
                    //start:rupal for First & Final Payment
                    //if user came from claim navigation tree w/o selecting any reserve, FirstFinal flag is passed in querys string which will be true
                    //if user came from funds menu>transation, this key will be null
                    case "IsFirstFinalQueryString":
                        if (AppHelper.GetQueryStringValue("IsFirstFinalQueryString") != null)
                        {
                            //IsFirstFinalQueryString - First & Final Payment Flag passed in quersystring from previous page
                            if (Page.FindControl("IsFirstFinalQueryString") != null)
                            {
                                if (Page.FindControl("IsFirstFinalQueryString").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("IsFirstFinalQueryString")).Text = AppHelper.GetQueryStringValue("IsFirstFinalQueryString");
                                }
                            }
                        }
                        bKeyFound = true;
                        break;
                    //if user came from claim navigation tree , First & Final Payment Checkbox will always be readonly
                    //but if user come from fund->transaction, the user can check or uncheck the firstfinal control
                    case "IsFirstFinalControlReradOnlyQueryString":
                        if (AppHelper.GetQueryStringValue("IsFirstFinalControlReradOnlyQueryString") != null)
                        {
                            //IsFirstFinalControlReradOnlyQueryString - First & Final Payment controls visibility passed in quersystring from previous page
                            if (Page.FindControl("IsFirstFinalControlReradOnlyQueryString") != null)
                            {
                                if (Page.FindControl("IsFirstFinalControlReradOnlyQueryString").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("IsFirstFinalControlReradOnlyQueryString")).Text = AppHelper.GetQueryStringValue("IsFirstFinalControlReradOnlyQueryString");
                                }
                            }
                        }
                        bKeyFound = true;
                        break;
                    //end:rupal for First & Final Payment
                    //skhare7 R8 Combined Payment
                    case "EnitytIdPassedIn":
                        if (AppHelper.GetQueryStringValue("EnitytIdPassedIn") != null)
                        {
                            if (Page.FindControl("enitytIdPassedIn") != null)
                            {
                                if (Page.FindControl("enitytIdPassedIn").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("enitytIdPassedIn")).Text = AppHelper.GetQueryStringValue("EnitytIdPassedIn");
                                }
                            }
                        }
                        bKeyFound = true;
                        break;

                    //rupal:start
                    case "CovgSeqNum":
                        if (AppHelper.GetQueryStringValue("CovgSeqNum") != null)
                        {
                            if (Page.FindControl("CovgSeqNum") != null)
                            {
                                if (Page.FindControl("CovgSeqNum").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("CovgSeqNum")).Text = AppHelper.GetQueryStringValue("CovgSeqNum");
                                }
                            }
                        }
                        bKeyFound = true;
                        break;
                    //rupal:end
                    case "RsvStatusParent":
                        if (AppHelper.GetQueryStringValue("RsvStatusParent") != null)
                        {
                            if (Page.FindControl("ResStatusParent") != null)
                            {
                                if (Page.FindControl("ResStatusParent").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("ResStatusParent")).Text = AppHelper.GetQueryStringValue("RsvStatusParent");
                                }
                            }
                        }
                        bKeyFound = true;
                        break;
                    case "TransSeqNum":
                        if (AppHelper.GetQueryStringValue("TransSeqNum") != null)
                        {
                            if (Page.FindControl("TransSeqNum") != null)
                            {
                                if (Page.FindControl("TransSeqNum").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("TransSeqNum")).Text = AppHelper.GetQueryStringValue("TransSeqNum");
                                }
                            }
                        }
                        bKeyFound = true;
                        break;
                    case "CoverageKey":         //Ankit Start : Worked on MITS - 34297
                        if (AppHelper.GetQueryStringValue("CoverageKey") != null)
                        {
                            if (Page.FindControl("CoverageKey") != null)
                            {
                                if (Page.FindControl("CoverageKey").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("CoverageKey")).Text = AppHelper.GetQueryStringValue("CoverageKey");
                                }
                            }
                        }
                        bKeyFound = true;
                        break;
                    case "IsMDed":
                        if (AppHelper.GetQueryStringValue("IsMDed") != null)
                        {
                            if (Page.FindControl("IsManualDed") != null)
                            {
                                if (Page.FindControl("IsManualDed").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                {
                                    ((TextBox)Page.FindControl("IsManualDed")).Text = AppHelper.GetQueryStringValue("IsMDed");
                                }
                            }
                        }
                        break;
                }
            }
            if (bKeyFound != true)
            {
                if (Page.FindControl("brspayment") != null)
                {
                    if (Page.FindControl("brspayment").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("brspayment")).Text = "none";
                    }
                    else if (Page.FindControl("brspayment").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        ((Label)Page.FindControl("brspayment")).Text = "none";
                    }
                }
            }
            //if (changedchecknumber.Text == "true")
            //{
            //    checkdate.Text = dToday;
            //}
            FDMPageLoad();

            //Deb : MITS 31211
            if (Page.FindControl("BRSSplitTotals") != null)
            {
                ((Label)Page.FindControl("BRSSplitTotals")).Text = ((Label)Page.FindControl("BRSSplitTotals")).Text.Replace("Total", sBRSSplit);
            }

            if (Page.FindControl("SplitTotals") != null)
            {
                ((Label)Page.FindControl("SplitTotals")).Text = ((Label)Page.FindControl("SplitTotals")).Text.Replace("Total", sSplitTotal);
            }
            //Deb : MITS 31211
            //Deb : MITS 35042
            if (Page.FindControl("LoadEntitybtn") != null)
            {
                //((Button)Page.FindControl("LoadEntitybtn")).Enabled = true;
                DatabindingHelper.EnableControls("LoadEntitybtn", this);
            }
            //Deb : MITS 35042



            //Ankit Start : Financial Enhancements - Payee Phrase Change
            if (Page.FindControl("DefaultPayeePhrase") != null)
            {
                if (base.Data != null)
                {
                    XmlNode oPayeePhraseHiddenField = base.Data.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/PayeePhraseList");
                    if (oPayeePhraseHiddenField != null)
                        ((TextBox)Page.FindControl("DefaultPayeePhrase")).Text = base.Data.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/PayeePhraseList").InnerText;
                }
            }
            //Ankit End
			// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //Start MITS Issue 26928 Anshul - Check EFT flag  in order to check or uncheck void check box
            //asharma326 jira 11591 handle page readonly case
            if (Page.FindControl("distributiontype") !=null 
                && Page.FindControl("distributiontype").GetType().ToString().Equals("System.Web.UI.WebControls.Label")) 
            {
                CheckBox chkVoid = (CheckBox)Page.FindControl("voidflag");
                if (chkVoid != null)
                {
                    chkVoid.Enabled = false;
                }
            }
            else
            {
                if (((CheckBox)Page.FindControl("paymentflag")) != null && ((CheckBox)Page.FindControl("paymentflag")).Checked && Page.FindControl("distributiontype") != null
                    && Page.FindControl("EFTDistributionType") != null &&
                    //                ((CheckBox)Page.FindControl("IsEFTPayment") != null) && ((CheckBox)Page.FindControl("IsEFTPayment")).Checked)
                     ((TextBox)((CodeLookUp)Page.FindControl("distributiontype")).FindControl("codelookup_cid")).Text == ((TextBox)Page.FindControl("EFTDistributionType")).Text)
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                {
                    CheckBox chkVoid = (CheckBox)Page.FindControl("voidflag");
                    if (chkVoid != null)
                    {
                        chkVoid.Enabled = false;
                    }
                }
            }
            //End MITS--26928 Anshul




            //Start by Shivendu for MITS 18794: this is temporary, this should be incorporated in binding engine
            //Readonly property of textbox should not be made true,instead do Attributes.Add("readonly", "readonly")
            //This makes sure that client side scripting changes on these text boxes are reflected on postbacks
            if (Page.FindControl("pye_firstname") != null)
            {
                if (Page.FindControl("pye_firstname").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("pye_firstname")).ReadOnly)
                    {
                        ((TextBox)Page.FindControl("pye_firstname")).ReadOnly = false;
                        ((TextBox)Page.FindControl("pye_firstname")).Attributes.Remove("readonly");
                        ((TextBox)Page.FindControl("pye_firstname")).Attributes.Add("readonly", "readonly");
                    }
                }
            }
            if (Page.FindControl("pye_taxid") != null)
            {
                if (Page.FindControl("pye_taxid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("pye_taxid")).ReadOnly)
                    {
                        ((TextBox)Page.FindControl("pye_taxid")).ReadOnly = false;
                        ((TextBox)Page.FindControl("pye_taxid")).Attributes.Remove("readonly");
                        ((TextBox)Page.FindControl("pye_taxid")).Attributes.Add("readonly", "readonly");
                    }
                }
            }
            if (Page.FindControl("pye_addr2") != null)
            {
                if (Page.FindControl("pye_addr2").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("pye_addr2")).ReadOnly)
                    {
                        ((TextBox)Page.FindControl("pye_addr2")).ReadOnly = false;
                        ((TextBox)Page.FindControl("pye_addr2")).Attributes.Remove("readonly");
                        ((TextBox)Page.FindControl("pye_addr2")).Attributes.Add("readonly", "readonly");
                    }
                }
            }
            if (Page.FindControl("pye_addr1") != null)
            {
                if (Page.FindControl("pye_addr1").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("pye_addr1")).ReadOnly)
                    {
                        ((TextBox)Page.FindControl("pye_addr1")).ReadOnly = false;
                        ((TextBox)Page.FindControl("pye_addr1")).Attributes.Remove("readonly");
                        ((TextBox)Page.FindControl("pye_addr1")).Attributes.Add("readonly", "readonly");
                    }
                }
            }
            if (Page.FindControl("pye_addr3") != null)
            {
                if (Page.FindControl("pye_addr3").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("pye_addr3")).ReadOnly)
                    {
                        ((TextBox)Page.FindControl("pye_addr3")).ReadOnly = false;
                        ((TextBox)Page.FindControl("pye_addr3")).Attributes.Remove("readonly");
                        ((TextBox)Page.FindControl("pye_addr3")).Attributes.Add("readonly", "readonly");
                    }
                }
            }
            if (Page.FindControl("pye_addr4") != null)
            {
                if (Page.FindControl("pye_addr4").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("pye_addr4")).ReadOnly)
                    {
                        ((TextBox)Page.FindControl("pye_addr4")).ReadOnly = false;
                        ((TextBox)Page.FindControl("pye_addr4")).Attributes.Remove("readonly");
                        ((TextBox)Page.FindControl("pye_addr4")).Attributes.Add("readonly", "readonly");
                    }
                }
            }
            if (Page.FindControl("pye_city") != null)
            {
                if (Page.FindControl("pye_city").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("pye_city")).ReadOnly)
                    {
                        ((TextBox)Page.FindControl("pye_city")).ReadOnly = false;
                        ((TextBox)Page.FindControl("pye_city")).Attributes.Remove("readonly");
                        ((TextBox)Page.FindControl("pye_city")).Attributes.Add("readonly", "readonly");
                    }
                }
            }
            //NULL Check added by Shivendu
            if (Page.FindControl("pye_countrycode") != null && Page.FindControl("pye_countrycode").Controls.Count >= 1 && Page.FindControl("pye_countrycode").Controls[0] != null)
            {
                if (Page.FindControl("pye_countrycode").Controls[0].GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("pye_countrycode").Controls[0]).ReadOnly)
                    {
                        ((TextBox)Page.FindControl("pye_countrycode").Controls[0]).ReadOnly = false;
                        ((TextBox)Page.FindControl("pye_countrycode").Controls[0]).Attributes.Remove("readonly");
                        ((TextBox)Page.FindControl("pye_countrycode").Controls[0]).Attributes.Add("readonly", "readonly");
                    }
                }
            }
            if (Page.FindControl("pye_zipcode") != null)
            {
                if (Page.FindControl("pye_zipcode").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("pye_zipcode")).ReadOnly)
                    {
                        ((TextBox)Page.FindControl("pye_zipcode")).ReadOnly = false;
                        ((TextBox)Page.FindControl("pye_zipcode")).Attributes.Remove("readonly");
                        ((TextBox)Page.FindControl("pye_zipcode")).Attributes.Add("readonly", "readonly");
                    }
                }
            }
            //NULL Check added by Shivendu
            if (Page.FindControl("pye_stateid") != null && Page.FindControl("pye_stateid").Controls.Count >= 1 && Page.FindControl("pye_stateid").Controls[0] != null)
            {
                if (Page.FindControl("pye_stateid").Controls[0].GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("pye_stateid").Controls[0]).ReadOnly)
                    {
                        ((TextBox)Page.FindControl("pye_stateid").Controls[0]).ReadOnly = false;
                        ((TextBox)Page.FindControl("pye_stateid").Controls[0]).Attributes.Remove("readonly");
                        ((TextBox)Page.FindControl("pye_stateid").Controls[0]).Attributes.Add("readonly", "readonly");
                    }
                }
            }


            //End by Shivendu for MITS 18794: this is temporary, this should be incorporated in binding engine

            StoreSuppleGridDataForPostBack();//Added by Shivendu for MITS 17520


            //put the BRS split xml into hidden field
            if (base.Data != null)
            {
                //srajindersin 1/2/42014 MITS 35040
                XmlNode oTaxId = base.Data.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/Update_tax_id");
                if (oTaxId != null)
                {
                    if (Page.FindControl("pye_taxid") != null)
                    {
                        if (Page.FindControl("pye_taxid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                        {
                            ((TextBox)Page.FindControl("pye_taxid")).Text = oTaxId.InnerText;
                        }
                        else if (Page.FindControl("pye_taxid").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                        {
                            ((Label)Page.FindControl("pye_taxid")).Text = oTaxId.InnerText;
                        }
                    }
                    
                }
                

                XmlNode oBRSSplitNode = base.Data.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/FundsBRSSplits");
                if (oBRSSplitNode != null)
                {
                    string sNewRSData = oBRSSplitNode.OuterXml;
                    if (!string.IsNullOrEmpty(sNewRSData))
                    {
                        //sNewRSData = sNewRSData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                        if (Page.FindControl("FundsBRSSplitsGrid_Data") != null)
                        {
                            if (Page.FindControl("FundsBRSSplitsGrid_Data").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                            {
                                ((TextBox)Page.FindControl("FundsBRSSplitsGrid_Data")).Text = sNewRSData;
                            }
                            else if (Page.FindControl("FundsBRSSplitsGrid_Data").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                            {
                                ((Label)Page.FindControl("FundsBRSSplitsGrid_Data")).Text = sNewRSData;
                            }
                        }
                    }
                }
            }


            if (Page.FindControl("lbl_bankaccount") != null)
            {
                if (((Label)Page.FindControl("lbl_bankaccount")).Text == "Sub Bank Account")
                {
                    ((Label)Page.FindControl("lbl_bankaccount")).Text = RMXResourceProvider.GetSpecificGlobalObject("SubBankAccount");
                }
                else if (((Label)Page.FindControl("lbl_bankaccount")).Text == "Bank Account")
                {
                    ((Label)Page.FindControl("lbl_bankaccount")).Text = RMXResourceProvider.GetSpecificGlobalObject("BankAccount");
                }
            }

                if (Page.FindControl("collectionflag") != null)
                {
                    if (Page.FindControl("collectionflag").GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                    {
                        if (((CheckBox)Page.FindControl("collectionflag")).Checked)
                        {
                            if (Page.FindControl("lbl_cbopayeetype") != null)
                            {
                                if (Page.FindControl("lbl_cbopayeetype").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {

                                    ((Label)Page.FindControl("lbl_cbopayeetype")).Text = RMXResourceProvider.GetSpecificGlobalObject("PayorType"); 
                                }
                            }
                            //pgupta93: RMA-7112 START
                            if (Page.FindControl("lbl_entitylist") != null)
                            {
                                if (Page.FindControl("lbl_entitylist").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {

                                    ((Label)Page.FindControl("lbl_entitylist")).Text = RMXResourceProvider.GetSpecificGlobalObject("Payor");
                                }
                            }
                            //pgupta93: RMA-7112 END
                            if (Page.FindControl("lbl_checknumber") != null)
                            {
                                if (Page.FindControl("lbl_checknumber").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_checknumber")).Text = RMXResourceProvider.GetSpecificGlobalObject("DepositNumber"); 
                                   
                                }
                            }
                            //rsharma220 MITS 33044 Start
                            if (Page.FindControl("lbl_finalpaymentflag") != null)
                            {
                                if (Page.FindControl("lbl_finalpaymentflag").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_finalpaymentflag")).Text = RMXResourceProvider.GetSpecificGlobalObject("FinalTransaction");

                                }
                            }
                            //rsharma220 MITS 33044 End
                        }
                        else
                        {
                            if (Page.FindControl("lbl_cbopayeetype") != null)
                            {
                                if (Page.FindControl("lbl_cbopayeetype").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_cbopayeetype")).Text = RMXResourceProvider.GetSpecificGlobalObject("PayeeType");
                                }
                            }
                            //pgupta93: RMA-7112 START
                            if (Page.FindControl("lbl_entitylist") != null)
                            {
                                if (Page.FindControl("lbl_entitylist").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {

                                    ((Label)Page.FindControl("lbl_entitylist")).Text = RMXResourceProvider.GetSpecificGlobalObject("Payees");
                                }
                            }
                            //pgupta93: RMA-7112 END
                            if (Page.FindControl("lbl_checknumber") != null)
                            {
                                if (Page.FindControl("lbl_checknumber").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_checknumber")).Text = RMXResourceProvider.GetSpecificGlobalObject("CheckNumber");
                                }
                            }
                            //rsharma220 MITS 33044 Start
                            if (Page.FindControl("lbl_finalpaymentflag") != null)
                            {
                                if (Page.FindControl("lbl_finalpaymentflag").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_finalpaymentflag")).Text = RMXResourceProvider.GetSpecificGlobalObject("FinalPayment");

                                }
                            }
                            //rsharma220 MITS 33044 End
                        }
                    }
                    if (Page.FindControl("collectionflag").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        if (((Label)Page.FindControl("collectionflag")).Text.ToLower() == "true")
                        {
                            if (Page.FindControl("lbl_cbopayeetype") != null)
                            {
                                if (Page.FindControl("lbl_cbopayeetype").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_cbopayeetype")).Text = RMXResourceProvider.GetSpecificGlobalObject("PayorType");
                                }
                            }
                            //pgupta93: RMA-7112 START
                            if (Page.FindControl("lbl_entitylist") != null)
                            {
                                if (Page.FindControl("lbl_entitylist").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {

                                    ((Label)Page.FindControl("lbl_entitylist")).Text = RMXResourceProvider.GetSpecificGlobalObject("Payor");
                                }
                            }
                            //pgupta93: RMA-7112 END
                            if (Page.FindControl("lbl_checknumber") != null)
                            {
                                if (Page.FindControl("lbl_checknumber").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_checknumber")).Text = RMXResourceProvider.GetSpecificGlobalObject("DepositNumber");
                                }
                            }
                            //rsharma220 MITS 33044 Start
                            if (Page.FindControl("lbl_finalpaymentflag") != null)
                            {
                                if (Page.FindControl("lbl_finalpaymentflag").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_finalpaymentflag")).Text = RMXResourceProvider.GetSpecificGlobalObject("FinalTransaction");

                                }
                            }
                            //rsharma220 MITS 33044 End
                        }
                        else
                        {
                            if (Page.FindControl("lbl_cbopayeetype") != null)
                            {
                                if (Page.FindControl("lbl_cbopayeetype").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_cbopayeetype")).Text = RMXResourceProvider.GetSpecificGlobalObject("PayeeType");
                                }
                            }
                            //pgupta93: RMA-7112 START
                            if (Page.FindControl("lbl_entitylist") != null)
                            {
                                if (Page.FindControl("lbl_entitylist").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {

                                    ((Label)Page.FindControl("lbl_entitylist")).Text = RMXResourceProvider.GetSpecificGlobalObject("Payees");
                                }
                            }
                            //pgupta93: RMA-7112 END
                            if (Page.FindControl("lbl_checknumber") != null)
                            {
                                if (Page.FindControl("lbl_checknumber").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_checknumber")).Text = RMXResourceProvider.GetSpecificGlobalObject("CheckNumber");
                                
                                }
                            }
                            //rsharma220 MITS 33044 Start
                            if (Page.FindControl("lbl_finalpaymentflag") != null)
                            {
                                if (Page.FindControl("lbl_finalpaymentflag").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                {
                                    ((Label)Page.FindControl("lbl_finalpaymentflag")).Text = RMXResourceProvider.GetSpecificGlobalObject("FinalPayment");

                                }
                            }
                            //rsharma220 MITS 33044 End
                        }
                    }
                }

            //if (isDetailLevelTrackingOn.Text == "True")
            //{
            //    if (lob.Text == "242")
            //    {
            //        if (lbl_clm_lastname != null)
            //        {
            //            lbl_clm_lastname.Text = "Detail Level Tracking Unit or Claimant";
            //        }
            //    }
            //    else
            //    {
            //        if (lbl_clm_lastname != null)
            //        {
            //            lbl_clm_lastname.Text = "Detail Level Tracking Claimant";
            //        }
            //    }
            //}
            //else
            //{
            //    if (lbl_clm_lastname != null)
            //    {
            //        lbl_clm_lastname.Text = "Claimant";
            //    }
            //}
            if (Page.FindControl("isTandE") != null)
            {
                if (Page.FindControl("isTandE").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("isTandE")).Text == "True")
                    {
                        if (Page.FindControl("backtotande") != null)
                        {
                            Page.FindControl("backtotande").Visible = true;
                        }
                    }
                }
                else if (Page.FindControl("isTandE").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                {
                    if (((Label)Page.FindControl("isTandE")).Text == "True")
                    {
                        if (Page.FindControl("backtotande") != null)
                        {
                            Page.FindControl("backtotande").Visible = true;
                        }
                    }
                }
            }

            //Raman Bhatia: MITS 17294 : Disabling bank account and Payee type for readonly powerviews
            if (Page.FindControl("lbl_bankaccount") != null)
            {
                if (Page.FindControl("lbl_bankaccount").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                {
                    if (((Label)Page.FindControl("lbl_bankaccount")).Attributes["DataRunTime"] != null)
                    {
                        if (Page.FindControl("bankaccount") != null)
                        {
                            if (Page.FindControl("bankaccount").GetType().ToString().Equals("System.Web.UI.WebControls.DropDownList"))
                            {
                                ((DropDownList)Page.FindControl("bankaccount")).Enabled = false;
                            }
                        }

                    }
                }
            }
            if (Page.FindControl("lbl_cbopayeetype") != null)
            {
                if (Page.FindControl("lbl_cbopayeetype").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                {
                    if (((Label)Page.FindControl("lbl_cbopayeetype")).Attributes["DataRunTime"] != null)
                    {
                        if (Page.FindControl("cbopayeetype") != null)
                        {
                            if (Page.FindControl("cbopayeetype").GetType().ToString().Equals("System.Web.UI.WebControls.DropDownList"))
                            {
                                ((DropDownList)Page.FindControl("cbopayeetype")).Enabled = false;
                            }
                        }

                    }
                }
            }
            //Debabrata Biswas Payee Data Exchange MITS# 20073 Payee Data Exchange
            if (Page.FindControl("supp_lss_invoice_id") != null)
            {
                //Start rsushilaggar added null check on the controls
                if (Page.FindControl("LSSInvoice") != null && Page.FindControl("lbl_LSSInvoice") != null)
                {
                    if (Page.FindControl("supp_lss_invoice_id").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        if (((TextBox)Page.FindControl("supp_lss_invoice_id")).Text.Trim() != "" && ((TextBox)Page.FindControl("supp_lss_invoice_id")).Text.Trim() != "0")
                        {
                            string sUrl = string.Empty;
                            ((HyperLink)Page.FindControl("LSSInvoice")).Visible = true;
                            ((Label)Page.FindControl("lbl_LSSInvoice")).Visible = true;

                            ((HyperLink)Page.FindControl("LSSInvoice")).Text = ((TextBox)Page.FindControl("supp_lss_invoice_id")).Text.Trim();
                            sUrl = "paramLSSInvoiceID=" + ((HyperLink)Page.FindControl("LSSInvoice")).Text;
                            ((HyperLink)Page.FindControl("LSSInvoice")).NavigateUrl = "#";
                            ((HyperLink)Page.FindControl("LSSInvoice")).Attributes.Add("onclick", "navigateToLSS('" + sUrl + "'); return false;");
                        }
                        else
                        {
                            ((HyperLink)Page.FindControl("LSSInvoice")).Visible = false;
                            ((Label)Page.FindControl("lbl_LSSInvoice")).Visible = false;
                        }
                    }
                    else
                    {
                        ((HyperLink)Page.FindControl("LSSInvoice")).Visible = false;
                        ((Label)Page.FindControl("lbl_LSSInvoice")).Visible = false;
                    }
                }
            }
            else
            {
                if (Page.FindControl("LSSInvoice") != null && Page.FindControl("lbl_LSSInvoice") != null)
                {
                    ((HyperLink)Page.FindControl("LSSInvoice")).Visible = false;
                    ((Label)Page.FindControl("lbl_LSSInvoice")).Visible = false;
                }
                //End rsushilaggar
            }
            //}

            //JIRA:438 START: ajohari2
            int itransid = 0;
            if (((TextBox)Page.FindControl("transid")) != null)
            {
                int.TryParse(((TextBox)Page.FindControl("transid")).Text, out itransid);
            }
			// npadhy JIRA 6418 Starts As we have removed the EFTPayment control from Funds page, we remove any refernce of the control from the code behind as well
            //if (((CheckBox)Page.FindControl("IsEFTPayment")) != null)
            //{
            // Below code is not required any more
                if (EftBank != null)
                {
                    string[] seftbanks = EftBank.Text.Split(',');
                    foreach (string sbank in seftbanks)
                    {

                        if (sbank.Equals(bankaccount.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (itransid > 0 || Page.IsPostBack)
                            {
                                if (hdnIsEFTPayment != null && hdnIsEFTPayment.Text.ToLower() == "false")
                                {
                                    //((CheckBox)Page.FindControl("IsEFTPayment")).Checked = false;
                                    hdnIsEFTPayment.Text = "false";
                                }
                                else
                                {
                                    //((CheckBox)Page.FindControl("IsEFTPayment")).Checked = true;
                                    if (hdnIsEFTPayment != null)
                                        hdnIsEFTPayment.Text = "true";
                                }
                            }
                            else
                            {
                                //((CheckBox)Page.FindControl("IsEFTPayment")).Checked = true;
                                if (hdnIsEFTPayment != null)
                                    hdnIsEFTPayment.Text = "true";
                            }

                            if (sPrintMode.Trim().CompareTo("P Printed") == 0)
                            {
                                //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = false;
                                // If the Print mode is printed the we should not allow Distribution Type to Change
                                ((CodeLookUp)Page.FindControl("distributiontype")).Enabled = false;
                            }
                            else
                            {
                                //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = true;
                                //((CodeLookUp)Page.FindControl("distributiontype")).Enabled = true;
                                DatabindingHelper.EnableControls("distributiontype", this);
                            }
                            break;


                        }
                        else
                        {
                            //((CheckBox)Page.FindControl("IsEFTPayment")).Checked = false;
                            //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = false;
                            if (hdnIsEFTPayment != null && hdnIsEFTPayment.Text.Trim() == "")
                                hdnIsEFTPayment.Text = "false";
                        }

                    }
                }


            //}
            //JIRA:438 END: ajohari2
			// npadhy JIRA 6418 Ends As we have removed the EFTPayment control from Funds page, we remove any refernce of the control from the code behind as well			
			// npadhy - the fix for RMA-8296 EFT Flag remains checked When Check number is manually entered on a Released Check is not merged intentionally, 
			// as with Distribution Type we are removing the concept of EFT and Manual Check box

            // npadhy JIRA 12885 Need to Disabled CtrlNumber, paymentflag and Collection Flag if we are creating Manual Deductible
                if (Page.FindControl("IsManualDed") != null && Page.FindControl("IsManualDed").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox") && ((TextBox)Page.FindControl("IsManualDed")).Text == "1")
                {
                    Control ctrlCtlNumber = Page.FindControl("ctlnumber");
                    if (ctrlCtlNumber != null)
                    {
                        DatabindingHelper.DisableControl(ctrlCtlNumber);
                    }

                    Control ctrlPaymentFlag = Page.FindControl("paymentflag");
                    if (ctrlPaymentFlag != null)
                    {
                        DatabindingHelper.DisableControl(ctrlPaymentFlag);
                    }

                    Control ctrlCollectionFlag = Page.FindControl("collectionflag");
                    if (ctrlCollectionFlag != null)
                    {
                        DatabindingHelper.DisableControl(ctrlCollectionFlag);
                    }
                }
        }

        //Debabrata Biswas Payee Data Exchange MITS# 20073 Payee Data Exchange
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);
            // skhare7 MITS 23664
            XElement oStatus = oMessageElement.XPathSelectElement("//StatusCode");
            if (oStatus != null)
            {
                sPrintMode = oStatus.Value;
            }

            if (sPrintMode.Trim().CompareTo("P Printed") == 0)
            {

                if (Page.FindControl("printpre") != null)
                {

                    if (Page.FindControl("printpre").GetType().ToString().Equals("System.Web.UI.WebControls.ImageButton"))
                    {
                        ((ImageButton)Page.FindControl("printpre")).Visible = false;
                        if (Page.FindControl("printedimage") != null)
                        {
                            if (Page.FindControl("printedimage").GetType().ToString().Equals("System.Web.UI.WebControls.ImageButton"))
                            {
                                ((ImageButton)Page.FindControl("printedimage")).Visible = true;
                            }
                        }
                        //((ImageButton)Page.FindControl("printpre")).Visible = true;
                        //((ImageButton)Page.FindControl("printpre")).Visible = true;
                        //((ImageButton)Page.FindControl("printpre")).Attributes.Remove("onclick");
                        //((ImageButton)Page.FindControl("printpre")).ID = "printedimage";

                        //((ImageButton)Page.FindControl("printedimage")).AlternateText = "Clone Image of Printed Check";
                        //((ImageButton)Page.FindControl("printedimage")).OnClientClick = "return Printedchecksclone();";
                        //(ImageButton)Page.FindControl("printedimage")).Attributes.Add("onclick", "return PrintedChecksImage();");
                    }
                }
            }
            else
            {
                if (Page.FindControl("printedimage") != null)
                {
                    if (Page.FindControl("printedimage").GetType().ToString().Equals("System.Web.UI.WebControls.ImageButton"))
                    {
                        ((ImageButton)Page.FindControl("printedimage")).Visible = false;
                    }
                }
            }
            // MITS 27242
            // Deb : MITS 27494
            //if (Page.FindControl("checknumber") != null)
            if (Page.FindControl("checknumber") != null && sPrintMode.Trim().CompareTo("P Printed") == 0) //mkaran2 : MITS 33326
            {
                if (Page.FindControl("checknumber").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    if (((TextBox)Page.FindControl("checknumber")).Text.Trim() != "" && ((TextBox)Page.FindControl("checknumber")).Text.Trim() != "0")
                    {
                        XElement oCheckNumber = oMessageElement.XPathSelectElement("//TransNumber");
                        if (oCheckNumber != null)
                        {
                            oCheckNumber.Value = ((TextBox)Page.FindControl("checknumber")).Text;
                        }
                    }
                }
            }
            // Deb : MITS 27494
            // MITS 27242 End
            // skhare7 MITS 23664 end
            string sUrl = string.Empty;
            if (Page.FindControl("supp_lss_invoice_id") != null)
            {
                //Start rsushilaggar added null check on the controls
                if (Page.FindControl("LSSInvoice") != null && Page.FindControl("lbl_LSSInvoice") != null)
                {
                    if (Page.FindControl("supp_lss_invoice_id").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        if (((TextBox)Page.FindControl("supp_lss_invoice_id")).Text.Trim() != "" && ((TextBox)Page.FindControl("supp_lss_invoice_id")).Text.Trim() != "0")
                        {
                            ((HyperLink)Page.FindControl("LSSInvoice")).Text = ((TextBox)Page.FindControl("supp_lss_invoice_id")).Text.Trim();
                            sUrl = "paramLSSInvoiceID=" + ((HyperLink)Page.FindControl("LSSInvoice")).Text;
                            ((HyperLink)Page.FindControl("LSSInvoice")).NavigateUrl = "#";
                            ((HyperLink)Page.FindControl("LSSInvoice")).Attributes.Add("onclick", "navigateToLSS('" + sUrl + "'); return false;");
                        }
                        else
                        {
                            ((HyperLink)Page.FindControl("LSSInvoice")).Visible = false;
                            ((Label)Page.FindControl("lbl_LSSInvoice")).Visible = false;
                        }
                    }
                    else
                    {
                        ((HyperLink)Page.FindControl("LSSInvoice")).Visible = false;
                        ((Label)Page.FindControl("lbl_LSSInvoice")).Visible = false;
                    }
                }
            }
            else
            {
                if (Page.FindControl("LSSInvoice") != null && Page.FindControl("lbl_LSSInvoice") != null)
                {
                    ((HyperLink)Page.FindControl("LSSInvoice")).Visible = false;
                    ((Label)Page.FindControl("lbl_LSSInvoice")).Visible = false;
                }
                //End rsushilaggar
            }

            
        }
        //End Debabrata 

        /// <summary>
        /// Handle grid data to avoid call application layer
        /// </summary>
        /// <returns></returns>
        private bool HandleGridData()
        {
            //For Split grids refresh postback, no need to call back end
            //The following scenario: 1. BRS add/clone/edit, SysSplitPostback = 'true'
            //2. BRS delete an item, FundsBRSSplitsGrid_Action != ''
            //3. Split add/edit/delete, FundsSplitsGrid_Action != ''
            string sSysSplitPostback = string.Empty;
            if (Page.FindControl("SysSplitPostback") != null)
            {
                if (Page.FindControl("SysSplitPostback").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    sSysSplitPostback = ((TextBox)Page.FindControl("SysSplitPostback")).Text;
                }
                else if (Page.FindControl("SysSplitPostback").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                {
                    sSysSplitPostback = ((Label)Page.FindControl("SysSplitPostback")).Text;
                }
            }

            string sBRSGridMode = string.Empty;
            if (Page.FindControl("SysSplitPostback") != null)
            {
                if (Page.FindControl("FundsBRSSplitsGrid_Action").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    sBRSGridMode = ((TextBox)Page.FindControl("FundsBRSSplitsGrid_Action")).Text;
                }
                else if (Page.FindControl("FundsBRSSplitsGrid_Action").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                {
                    sBRSGridMode = ((Label)Page.FindControl("FundsBRSSplitsGrid_Action")).Text;
                }
            }

            string sSplitGridMode = string.Empty;
            if (Page.FindControl("SysSplitPostback") != null)
            {
                if (Page.FindControl("FundsSplitsGrid_Action").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    sSplitGridMode = ((TextBox)Page.FindControl("FundsSplitsGrid_Action")).Text;
                }
                else if (Page.FindControl("FundsSplitsGrid_Action").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                {
                    sSplitGridMode = ((Label)Page.FindControl("FundsSplitsGrid_Action")).Text;
                }
            }
            //Deb : Reverting the code changes  of mits 33839
            //rsharma220 MITS 33839
            if (sSysSplitPostback == "true" || !string.IsNullOrEmpty(sSplitGridMode) || !string.IsNullOrEmpty(sBRSGridMode))// || IsPostBack)
            {
                BindGridData();

                Label olblError = (Label)Page.FindControl("lblError");
                if (olblError != null)
                {
                    olblError.Text = string.Empty;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Bind data for UserControlDataGrid
        /// </summary>
        private void BindGridData()
        {
            bool bFound = false;
            StringBuilder sbTemplate = null;
            string sTemp = string.Empty;
            string sTotalText = "Total: $0.00";

            string sBRSGridMode = string.Empty;
            //start -  Added by Nikhil. To store override processing status 
            bool bOverrideDedProcessing = true;
            //end -  Added by Nikhil. To store override processing status 
            if (Page.FindControl("SysSplitPostback") != null)
            {
                if (Page.FindControl("FundsBRSSplitsGrid_Action").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    sBRSGridMode = ((TextBox)Page.FindControl("FundsBRSSplitsGrid_Action")).Text;
                }
                else if (Page.FindControl("FundsBRSSplitsGrid_Action").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                {
                    sBRSGridMode = ((Label)Page.FindControl("FundsBRSSplitsGrid_Action")).Text;
                }
            }

            string sBeginXml = "<Document><ParamList><Param name='SysFormVariables'><FormVariables><SysExData>";
            string sEndXml = "</SysExData></FormVariables></Param></ParamList></Document>";
            string sBRSData = string.Empty;
            if (!string.IsNullOrEmpty(sBRSGridMode))
            {
                if (Page.FindControl("FundsBRSSplitsGrid") != null)
                {
                    if (Page.FindControl("FundsBRSSplitsGrid").GetType().ToString().Equals("ASP.ui_shared_controls_usercontroldatagrid_ascx"))
                    {
                        sBRSData = ((UserControlDataGrid)Page.FindControl("FundsBRSSplitsGrid")).GetXml();
                    }

                }
            }
            else
            {
                if (Page.FindControl("FundsBRSSplitsGrid_Data") != null)
                {
                    if (Page.FindControl("FundsBRSSplitsGrid_Data").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        sBRSData = ((TextBox)Page.FindControl("FundsBRSSplitsGrid_Data")).Text;
                    }

                }
            }

            if (!string.IsNullOrEmpty(sBRSData))
            {
                //sBRSData = sBRSData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                XmlDocument oBRSDataDoc = new XmlDocument();
                sbTemplate = new StringBuilder();
                sbTemplate.Append(sBeginXml);
                sbTemplate.Append(sBRSData);
                sbTemplate.Append(sEndXml);
                oBRSDataDoc.LoadXml(sbTemplate.ToString());
                //Deb MITS 27466 
                //When multicurrency will be implemented we have to remove these
                XmlNodeList objXmlNodeList = oBRSDataDoc.SelectNodes("//@CurrencyValue");
                foreach (XmlAttribute objElem in objXmlNodeList)
                {
                    objElem.Value = "";
                }
                //Deb MITS 27466 
                sTemp = string.Empty;
                sTotalText = string.Empty;
                double dThisBRSSplitAmount = 0;

                XElement xSplit = XElement.Parse(sbTemplate.ToString());

                foreach (XElement objOptionElement in xSplit.Descendants("option"))
                {
                    if (objOptionElement.Element("Amount") != null)
                    {
                        bFound = true;
                        sTemp = objOptionElement.Element("Amount").Value;
                        dThisBRSSplitAmount = dThisBRSSplitAmount + Conversion.ConvertStrToDouble(sTemp);
                    }
                }
                if (Page.FindControl("BRSSplitTotals") != null)
                {
                    if (bFound == true)
                    {
                        sTotalText = dThisBRSSplitAmount.ToString("c");
                        sTotalText = "Total: ".Replace("Total", sBRSSplit) + sTotalText;//Deb : MITS 31211
                    }
                    else
                    {
                        sTotalText = "Total: $0.00".Replace("Total", sBRSSplit);//Deb : MITS 31211
                    }
                    ((Label)Page.FindControl("BRSSplitTotals")).Text = sTotalText;
                }

                if (Page.FindControl("SysSplitPostback") != null)
                {
                    if (Page.FindControl("SysSplitPostback").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("SysSplitPostback")).Text = string.Empty;
                    }

                }

                if (Page.FindControl("FundsBRSSplitsGrid_Action") != null)
                {
                    if (Page.FindControl("FundsBRSSplitsGrid_Action").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("FundsBRSSplitsGrid_Action")).Text = string.Empty;
                    }

                }
                //Ashish Ahuja : Mits 30264
                //if (Page.FindControl("FundsBRSSplitsGrid_RowDeletedFlag") != null)
                //{
                //    if (Page.FindControl("FundsBRSSplitsGrid_RowDeletedFlag").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                //    {
                //        ((TextBox)Page.FindControl("FundsBRSSplitsGrid_RowDeletedFlag")).Text = string.Empty;
                //    }

                //}

                if (Page.FindControl("FundsBRSSplitsGrid") != null)
                {
                    if (Page.FindControl("FundsBRSSplitsGrid").GetType().ToString().Equals("ASP.ui_shared_controls_usercontroldatagrid_ascx"))
                    {
                        ((UserControlDataGrid)Page.FindControl("FundsBRSSplitsGrid")).BindData(oBRSDataDoc);
                    }

                }

                if (Page.FindControl("FundsBRSSplitsGrid_Data") != null)
                {
                    if (Page.FindControl("FundsBRSSplitsGrid_Data").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("FundsBRSSplitsGrid_Data")).Text = sBRSData;
                    }
                }

            }

            sbTemplate = new StringBuilder();
            sbTemplate.Append(sBeginXml);
            XmlDocument oGridDoc = new XmlDocument();
            string sSplitData = string.Empty;

            if (Page.FindControl("FundsSplitsGrid") != null)
            {
                if (Page.FindControl("FundsSplitsGrid").GetType().ToString().Equals("ASP.ui_shared_controls_usercontroldatagrid_ascx"))
                {
                    sSplitData = ((UserControlDataGrid)Page.FindControl("FundsSplitsGrid")).GetXml();
                }
            }


            if (!string.IsNullOrEmpty(sSplitData))
            {
                double dThisSplitAmount = 0;

                sbTemplate.Append(sSplitData);
                sbTemplate.Append(sEndXml);
                oGridDoc.LoadXml(sbTemplate.ToString());

                sTemp = string.Empty;
                sTotalText = string.Empty;

                XElement xSplit = XElement.Parse(sbTemplate.ToString());
                bFound = false;
                //Deb Multi Currency
                DatabindingHelper.DisableControls("currencytypetext", this);
                int iCount = 0;
                //Deb Multi Currency
                foreach (XElement objOptionElement in xSplit.Descendants("option"))
                {
                    if (objOptionElement.Element("Amount") != null)
                    {
                        bFound = true;
                        //Deb Multi Currency
                        sTemp = objOptionElement.Element("Amount").Value;
                        string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                        dThisSplitAmount = dThisSplitAmount + Conversion.ConvertStrToDouble(sTemp);
                        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                        iCount++;
                        //Deb Multi Currency
                    }
                    //Start -  Change by Nikhil. To store override processing status 
                    if (objOptionElement.Element("IsEligibleForDedProcessing") != null)
                    {
                        if (string.Compare(objOptionElement.Element("IsEligibleForDedProcessing").Value, "true", true) == 0 && Conversion.ConvertStrToInteger(objOptionElement.Element("TransTypeCode").Attribute("codeid").Value) > 0 && ((CheckBox)Page.FindControl("collectionflag")).Checked)
                        {
                            if (objOptionElement.Element("IsOverrideDedProcessing") != null && bOverrideDedProcessing)
                            {
                                //if (objOptionElement.Element("IsEligibleForDeductible") != null && string.Compare(objOptionElement.Element("IsEligibleForDeductible").Value, "true", true) == 0)
                                //{
                                sTemp = objOptionElement.Element("IsOverrideDedProcessing").Value;
                                if (string.Compare(sTemp, "false", true) == 0)
                                {
                                    bOverrideDedProcessing = false;

                                }
                                //}

                            }
                        }
                    }
                    

                    //End -    Change by Nikhil. To store override processing status 
                }
                if (Page.FindControl("SplitTotals") != null)
                {
                    if (bFound == true)
                    {
                        //Deb Multi Currency
                        //Commented the below line as we can have payment as zero any currency needs to be remain disabled
                        if (iCount == 1)
                            DatabindingHelper.EnableControls("currencytypetext", this);
                        //Deb Multi Currency
                        sTotalText = dThisSplitAmount.ToString("c");
                        sTotalText = "Total: ".Replace("Total", sSplitTotal) + sTotalText;//Deb : MITS 31211
                    }
                    else
                    {
                        //Deb Multi Currency
                        sTotalText = dThisSplitAmount.ToString("c");
                        sTotalText = "Total: ".Replace("Total", sSplitTotal) + sTotalText;//Deb : MITS 31211
                        //sTotalText = "Total: $0.00";
                        //Deb Multi Currency
                        DatabindingHelper.EnableControls("currencytypetext", this);
                        //Deb Multi Currency
                    }
                    ((Label)Page.FindControl("SplitTotals")).Text = sTotalText;
                }
                if (Page.FindControl("FundsSplitsGrid_Action") != null)
                {
                    if (Page.FindControl("FundsSplitsGrid_Action").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("FundsSplitsGrid_Action")).Text = string.Empty;
                    }

                }
                //Start -  Change by Nikhil. To store override processing status 
                if (Page.FindControl("OverideDedProcessing") != null &&  ((CheckBox)Page.FindControl("collectionflag")).Checked)
                {
                    if (Page.FindControl("OverideDedProcessing").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("OverideDedProcessing")).Text = bOverrideDedProcessing.ToString();
                    }

                }
                //END -  Change by Nikhil. To store override processing status 
                //Ashish ahuja : Mits 30264
                //if (Page.FindControl("FundsSplitsGrid_RowDeletedFlag") != null)
                //{
                //    if (Page.FindControl("FundsSplitsGrid_RowDeletedFlag").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                //    {
                //        ((TextBox)Page.FindControl("FundsSplitsGrid_RowDeletedFlag")).Text = string.Empty;
                //    }

                //}
                //mkaran2 - [JIRA] (RMA-4110) MITS 37158 -start                                                          
                XmlNodeList oListCoverageTypeCode = oGridDoc.SelectNodes("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/FundsSplits/option[@ref='/Instance/UI/FormVariables/SysExData/FundsSplits/option[1]']/CoverageTypeCode[@tablename='COVERAGE_TYPE']");
                foreach (XmlNode oCoverageTypeCode in oListCoverageTypeCode)
                {
                    oCoverageTypeCode.InnerText = AppHelper.HTMLCustomDecode(oCoverageTypeCode.InnerText);
                }

                XmlNodeList oListUnitID = oGridDoc.SelectNodes("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/FundsSplits/option[@ref='/Instance/UI/FormVariables/SysExData/FundsSplits/option[1]']/UnitID");
                foreach (XmlNode oUnitID in oListUnitID)
                {
                    oUnitID.InnerText = AppHelper.HTMLCustomDecode(oUnitID.InnerText);
                }
                //mkaran2 - [JIRA] (RMA-4110) MITS 37158 -end

                if (Page.FindControl("FundsSplitsGrid") != null)
                {
                    if (Page.FindControl("FundsSplitsGrid").GetType().ToString().Equals("ASP.ui_shared_controls_usercontroldatagrid_ascx"))
                    {
                        ((UserControlDataGrid)Page.FindControl("FundsSplitsGrid")).BindData(oGridDoc);
                    }

                }

            }
            DatabindingHelper.UpdateTabFocus(this);
        }

        private bool HandleNewToolbarButton()
        {
            //Special handling for new funds from the toolbar. 
            //If claimid is not 0, SysCmd is blank and new button is clicked, we can assume 
            //it's for new funds record
            Control oToolbarButton = DatabindingHelper.GetPostBackControl(this);
            string sToolbarButtonId = string.Empty;
            if (oToolbarButton != null)
            {
                sToolbarButtonId = oToolbarButton.ID.ToLower();
            }
            TextBox oClaimId = (TextBox)this.FindControl("claimid");
            string sClaimId = string.Empty;
            if (oClaimId != null)
            {
                sClaimId = oClaimId.Text;
            }
            TextBox oSysCmd = (TextBox)this.FindControl("SysCmd");
            string sSysCmd = string.Empty;
            if (oSysCmd != null)
            {
                sSysCmd = oSysCmd.Text;
            }
            if (string.IsNullOrEmpty(sSysCmd) && (sToolbarButtonId == "new") && (!string.IsNullOrEmpty(sClaimId) && sClaimId != "0"))
            {
                StringBuilder sbUrl = new StringBuilder();
                sbUrl.Append("funds.aspx?SysExternalParam=");

                string sclaimId = string.Empty;

                if (Page.FindControl("claimid") != null)
                {
                    if (Page.FindControl("claimid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        sclaimId = ((TextBox)Page.FindControl("claimid")).Text;
                    }
                    else if (Page.FindControl("claimid").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        sclaimId = ((Label)Page.FindControl("claimid")).Text;
                    }
                }

                sbUrl.Append(string.Format("<SysExternalParam><ClaimId>{0}</ClaimId>", sclaimId));

                string sClaimNumber = string.Empty;

                if (Page.FindControl("claimnumber") != null)
                {
                    if (Page.FindControl("claimnumber").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        sClaimNumber = ((TextBox)Page.FindControl("claimnumber")).Text;
                    }
                    else if (Page.FindControl("claimnumber").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        sClaimNumber = ((Label)Page.FindControl("claimnumber")).Text;
                    }
                }

                sbUrl.Append(string.Format("<ClaimNumber>{0}</ClaimNumber>", sClaimNumber));

                string sclaimantEIdPassedIn = string.Empty;

                if (Page.FindControl("claimantEIdPassedIn") != null)
                {
                    if (Page.FindControl("claimantEIdPassedIn").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        sclaimantEIdPassedIn = ((TextBox)Page.FindControl("claimantEIdPassedIn")).Text;
                    }
                    else if (Page.FindControl("claimantEIdPassedIn").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        sclaimantEIdPassedIn = ((Label)Page.FindControl("claimantEIdPassedIn")).Text;
                    }
                }

                sbUrl.Append(string.Format("<ClaimantEid>{0}</ClaimantEid>", sclaimantEIdPassedIn));

                string sIsCollection = "false";
                if (Page.FindControl("collectionflag") != null)
                {
                    if (Page.FindControl("collectionflag").GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                    {
                        if (((CheckBox)Page.FindControl("collectionflag")).Checked)
                        {
                            sIsCollection = "true";
                        }
                    }
                    else if (Page.FindControl("collectionflag").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        if (((Label)Page.FindControl("collectionflag")).Text.ToLower() == "true")
                        {
                            sIsCollection = "true";
                        }
                    }

                }


                sbUrl.Append(string.Format("<IsCollection>{0}</IsCollection>", sIsCollection));

                string sUnitId = string.Empty;

                if (Page.FindControl("unitid") != null)
                {
                    if (Page.FindControl("unitid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        sUnitId = ((TextBox)Page.FindControl("unitid")).Text;
                    }
                    else if (Page.FindControl("unitid").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        sUnitId = ((Label)Page.FindControl("unitid")).Text;
                    }
                }

                sbUrl.Append(string.Format("<UnitID>{0}</UnitID></SysExternalParam>", sUnitId));
                //Server.Transfer causes problem when adding a new funds split.
                Response.Redirect(sbUrl.ToString());
                return true;
            }

            return false;
        }
        /// <summary>
        /// Added by Shivendu for MITS 17520
        /// </summary>
        private void BindSuppleGridDataOnPostBack()
        {
            TextBox ctrlNewZapatecMinRows = new TextBox();
            TextBox ctrlExistingZapatecMinRows = new TextBox();
            ctrlExistingZapatecMinRows = (TextBox)Page.Form.FindControl("zapatecminrows");
            if (ctrlExistingZapatecMinRows == null)
            {
                ctrlNewZapatecMinRows.Attributes["style"] = "display:none";
                ctrlNewZapatecMinRows.ID = "zapatecminrows";
                if (ViewState["zapatecminrows"] != null)
                {
                    ctrlNewZapatecMinRows.Text = ViewState["zapatecminrows"].ToString();
                }
                Page.Form.Controls.Add(ctrlNewZapatecMinRows);
            }
            else
            {
                ctrlExistingZapatecMinRows.Attributes["style"] = "display:none";
                if (ViewState["zapatecminrows"] != null)
                {
                    ctrlExistingZapatecMinRows.Text = ViewState["zapatecminrows"].ToString();
                }
            }
            TextBox ctrlNewZapatecmaxRows = new TextBox();
            TextBox ctrlExistingZapatecmaxRows = new TextBox();
            ctrlExistingZapatecmaxRows = (TextBox)Page.Form.FindControl("zapatecmaxrows");
            if (ctrlExistingZapatecmaxRows == null)
            {
                ctrlNewZapatecmaxRows.Attributes["style"] = "display:none";
                ctrlNewZapatecmaxRows.ID = "zapatecmaxrows";
                if (ViewState["zapatecmaxrows"] != null)
                {
                    ctrlNewZapatecmaxRows.Text = ViewState["zapatecmaxrows"].ToString();
                }
                Page.Form.Controls.Add(ctrlNewZapatecmaxRows);
            }
            else
            {
                ctrlExistingZapatecmaxRows.Attributes["style"] = "display:none";
                if (ViewState["zapatecmaxrows"] != null)
                {
                    ctrlExistingZapatecmaxRows.Text = ViewState["zapatecmaxrows"].ToString();
                }
            }

            TextBox ctrlSuppleGridIds = (TextBox)Page.FindControl("txtSuppleGridIds");
            if (ctrlSuppleGridIds != null)
            {
                string[] sSuppleGridIds = ctrlSuppleGridIds.Text.Split('|');
                foreach (string s in sSuppleGridIds)
                {
                    if (s == "")
                        continue;

                    TextBox ctrlNewZapatecXml = new TextBox();
                    TextBox ctrlExistingZapatecXml = new TextBox();
                    ctrlExistingZapatecXml = (TextBox)Page.Form.FindControl(s);
                    if (ctrlExistingZapatecXml == null)
                    {
                        ctrlNewZapatecXml.Attributes["style"] = "display:none";
                        ctrlNewZapatecXml.ID = s;
                        if (ViewState[s] != null)
                        {
                            ctrlNewZapatecXml.Text = ViewState[s].ToString();
                        }
                        Page.Form.Controls.Add(ctrlNewZapatecXml);
                    }
                    else
                    {
                        ctrlExistingZapatecXml.Attributes["style"] = "display:none";
                        if (ViewState[s] != null)
                        {
                            ctrlExistingZapatecXml.Text = ViewState[s].ToString();
                        }
                    }
                }

            }


        }
        /// <summary>
        ///  Added by Shivendu for MITS 17520
        /// </summary>
        private void StoreSuppleGridDataForPostBack()
        {
            //Start by Shivendu for MITS 17520

            if (!IsPostBack)
            {
                TextBox txtZapatecMinRows = (TextBox)Page.FindControl("zapatecminrows");
                TextBox txtZapatecMaxRows = (TextBox)Page.FindControl("zapatecmaxrows");
                if (txtZapatecMinRows != null)
                    ViewState["zapatecminrows"] = txtZapatecMinRows.Text;

                if (txtZapatecMaxRows != null)
                    ViewState["zapatecmaxrows"] = txtZapatecMaxRows.Text;

                TextBox ctrlSuppleGridIds = (TextBox)Page.FindControl("txtSuppleGridIds");
                if (ctrlSuppleGridIds != null)
                {
                    string[] sSuppleGridIds = ctrlSuppleGridIds.Text.Split('|');
                    foreach (string s in sSuppleGridIds)
                    {

                        TextBox txtZapatecXml = (TextBox)Page.FindControl(s);
                        if (txtZapatecXml != null)
                        {
                            ViewState[s] = txtZapatecXml.Text;
                        }
                    }

                }




            }
            //End by Shivendu for MITS 17520
        }

        /// <summary>
        /// MITS 19067 After a payment is saved, the value for hidden field FundsBRSSplitsGrid_Data is not
        /// updated with the new value, so the SplitRowId is still could be 0 or negative so the Funds Trans
        /// Split record will be created again. When the old Trans Split record is deleted, the corresponding
        /// Invoice_Detail record is not deleted and in the orphan status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateSave(object sender, EventArgs e)
        {
            base.NavigateSave(sender, e);
            XmlDocument oXmlData = base.Data;
            if (oXmlData != null)
            {
                XmlNode oBrsSplits = oXmlData.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/FundsBRSSplits");
                if (oBrsSplits != null)
                {
                    if (Page.FindControl("FundsBRSSplitsGrid_Data") != null)
                    {
                        ((TextBox)Page.FindControl("FundsBRSSplitsGrid_Data")).Text = oBrsSplits.OuterXml;
                    }
                }
            }
            //Added by Ashutosh
            //if ((CheckBox)Page.FindControl("IsEFTPayment") !=null)
            //if (((CheckBox)Page.FindControl("IsEFTPayment")).Checked)
			// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            if (((CheckBox)Page.FindControl("paymentflag")) != null && ((CheckBox)Page.FindControl("paymentflag")).Checked && 
                //((CheckBox)Page.FindControl("IsEFTPayment") != null) && ((CheckBox)Page.FindControl("IsEFTPayment")).Checked)
                ((TextBox)((CodeLookUp)Page.FindControl("distributiontype")).FindControl("codelookup_cid")).Text ==  ((TextBox)Page.FindControl("EFTDistributionType")).Text)
				// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            {
                CheckBox chkVoid = (CheckBox)Page.FindControl("voidflag");
                if (chkVoid != null)
                {
                    chkVoid.Enabled = false;
                }
            }

            //JIRA:438 START: ajohari2
            TextBox EftBank = (TextBox)Page.FindControl("EFTBanks");
            DropDownList bankaccount = (DropDownList)Page.FindControl("bankaccount");
			// npadhy JIRA 6418 Starts As we have removed the EFTPayment control from Funds page, we remove any refernce of the control from the code behind as well
            //if (((CheckBox)Page.FindControl("IsEFTPayment")) != null)
            //{
                if (EftBank != null)
                {
                    string[] seftbanks = EftBank.Text.Split(',');
                    foreach (string sbank in seftbanks)
                    {

                        if (sbank.Equals(bankaccount.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
                        {

                            if (sPrintMode.Trim().CompareTo("P Printed") == 0)
                            {
                                //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = false;
                                ((CodeLookUp)Page.FindControl("distributiontype")).Enabled = false;

                            }
                            else
                            {
                                //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = true;
                                //((CodeLookUp)Page.FindControl("distributiontype")).Enabled = true;
                                DatabindingHelper.EnableControls("distributiontype", this);
                                //this validation to be added in save, that if the Bank is not an EFT bank and we are saving the Distribution Type as EFT, then it should throw error
                            }
                            break;
                        }
                        else
                        {
                            //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = false;
                            //this validation to be added in save, that if the Bank is not an EFT bank and we are saving the Distribution Type as EFT, then it should throw error
                        }

                    }
                }


            //}
            //JIRA:438 END: ajohari2
			// npadhy JIRA 6418 Ends As we have removed the EFTPayment control from Funds page, we remove any refernce of the control from the code behind as well

        }
        //Start: MITS Issue 26931 Anshul
        public override void ModifyControls()
        {
            base.ModifyControls();
            XmlDocument oXmlData = base.Data;
            Control oControl = this.Form.FindControl("SysCmd");
            //smishra54: MITS 26615
            string strEventTarget = Request.Params.Get("__EVENTTARGET");
            if (oXmlData != null && (oControl != null) && (oControl
                is WebControl) && (string.Equals(((TextBox)oControl).Text, "9") || string.Equals(((TextBox)oControl).Text, "5") || string.Equals(strEventTarget, "PrintCheck")))
            //smishra54: End
            {
                XmlNode oWithholdingPlaymentFlag = oXmlData.SelectSingleNode("/Document/ParamList/Param[@name='SysPropertyStore']/Funds/WithholdingPMTFlag");
                XmlNode oVoidFlag = oXmlData.SelectSingleNode("/Document/ParamList/Param[@name='SysPropertyStore']/Funds/VoidFlag");
                XmlNode oClearedFlag = oXmlData.SelectSingleNode("/Document/ParamList/Param[@name='SysPropertyStore']/Funds/ClearedFlag");
				// npadhy JIRA 6418 Starts As we have removed the EFTPayment control from Funds page, we remove any refernce of the control from the code behind as well
                //Check if the payment is EFT
                //XmlNode oEFTPaymentFlag = oXmlData.SelectSingleNode("/Document/ParamList/Param[@name='SysPropertyStore']/Funds/IsEFTPayment");
                XmlNode oDistributionType = oXmlData.SelectSingleNode("/Document/ParamList/Param[@name='SysPropertyStore']/Funds/DstrbnType[@codeid]");
                if (oWithholdingPlaymentFlag != null && string.Equals(oWithholdingPlaymentFlag.InnerText, "true", StringComparison.InvariantCultureIgnoreCase) &&
                    string.Equals(oVoidFlag.InnerText, "false", StringComparison.InvariantCultureIgnoreCase) &&
                    //(string.Equals(oEFTPaymentFlag.InnerText, "false", StringComparison.InvariantCultureIgnoreCase) ? string.Equals(oClearedFlag.InnerText, "false", StringComparison.InvariantCultureIgnoreCase) : true))
                    (string.Equals(oDistributionType.InnerText, ((TextBox)Page.FindControl("EFTDistributionType")).Text, StringComparison.InvariantCultureIgnoreCase) ? string.Equals(oClearedFlag.InnerText, "false", StringComparison.InvariantCultureIgnoreCase) : true))
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "WithholdingWarning", "alert('Payment amount is going to be reduced due to withholding');", true);
				// npadhy JIRA 6418 Ends As we have removed the EFTPayment control from Funds page, we remove any refernce of the control from the code behind as well	
            }
        }
        //End: MITS Issue 26931 Anshul
        /// <summary>
        /// This function is for persisting the data for the disabled control b/w postbacks.
        /// </summary>
        private void PersistDisabledControlData()
        {
            //Start - averma62 - 31440
            //Control voidReason = Page.FindControl("voidreason");
            Control bankaccount = Page.FindControl("bankaccount");
            if (Page.FindControl("voidreason") != null)
            {
                if (Page.FindControl("voidreason").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    ((TextBox)Page.FindControl("voidreason")).Text = Request.Form["voidreason"];
                }
                else if (Page.FindControl("voidreason").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                {
                    ((Label)Page.FindControl("voidreason")).Text = Request.Form["voidreason"];
                }
            }
            //if (voidReason != null)
            //{
            //    //TextBox VoidReasonTxt = (TextBox)voidReason;
            //    //VoidReasonTxt.Text = Request.Form["voidreason"];
            //}
            //End - averma62 - 31440
        }
    }
}
