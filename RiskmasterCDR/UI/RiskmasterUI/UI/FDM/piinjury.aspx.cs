﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.FDM
{
    public partial class Piinjury : FDMBasePage
    {
        /// <summary>
        /// npradeepshar 06/07/2011 Added injury screen for R8 enhancenment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
            if (AppHelper.GetQueryStringValue("DataChange") == "true")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "datachangescript", "<script type='text/javascript'>setDataChanged(true);</script>");
            }
        }
    }
}