﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cmxaccommodationlist.aspx.cs" Inherits="Riskmaster.UI.FDM.CmXAccommodationList" ValidateRequest="false" EnableViewStateMac="false" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>Untitled Page</title>
     <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>   
  <script type="text/javascript" src="../../Scripts/pagingsorting.js"></script>
</head>
<body onload="pageLoaded()" class="10pt">
    <form id="frmData" runat="server">
    <div>
    <asp:Label ID="lblError" runat="server" Text="" ForeColor=Red></asp:Label>
    <h2>Accommodations</h2>
    <table border="0" width="99%" cellspacing="0" cellpadding="2" id="tbllist">
    <tr>
     <td class="ctrlgroup" width="50px"></td>
     <td class="ctrlgroup" width="50px"></td>
     <td class="ctrlgroup"><a href="#"  id="HeaderColumn2" class="ctrlgroup" onclick="SortList(document.all.tbllist,2,'lnkEdit',true );">Accomm. Type</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#"  id="HeaderColumn3" class="ctrlgroup" onclick="SortList(document.all.tbllist,3,'lnkEdit',true );">Status</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#"  id="HeaderColumn4" class="ctrlgroup" onclick="SortList(document.all.tbllist,4,'lnkEdit',true );">Scope</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#"  id="HeaderColumn5" class="ctrlgroup" onclick="SortList(document.all.tbllist,5,'lnkEdit',true );">Accomm. Accepted</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#"  id="HeaderColumn6" class="ctrlgroup" onclick="SortList(document.all.tbllist,6,'lnkEdit',true );">Restriction</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#"  id="HeaderColumn7" class="ctrlgroup" onclick="SortList(document.all.tbllist,7,'lnkEdit',true );">Accommodation</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#"  id="HeaderColumn8" class="ctrlgroup" onclick="SortList(document.all.tbllist,8,'lnkEdit',true );">Cost</a></td>
    </tr>
    <%int i = 0; foreach (XElement item in result)
    {
        string rowclass = "";
        if ((i % 2) == 1) rowclass = "datatd1";
        else rowclass = "datatd";
        i++;
        %>
        <tr>                          
            <td class="<%=rowclass%>">
            <%
                PageId.ID = "PageId" + i;
                PageId.Value = item.Element("CmAccommRowId").Value;

           %>                              
                <input type ="checkbox" name="PageId" id="PageId" title="Accomodations" runat="server"/></td>
            <td class="<%=rowclass%>">
               <%
                   lnkEdit.ID = "lnkEdit" + i;
                   lnkEdit.PostBackUrl = "cmxaccommodation.aspx?SysFormName=CmXAccommodation&SysCmd=0&SysViewType=tab&SysFormIdName=cmaccommrowid&SysFormId=" + item.Element("CmAccommRowId").Value + "&SysFormPIdName=cmrowid&SysEx=CmRowId|EventNumber&SysExMapCtl=CmRowId|EventNumber";
                %>
              <asp:LinkButton runat="server" id="lnkEdit">Edit</asp:LinkButton> 
            </td>
            
            <td class="<%=rowclass%>">
                <%=item.Element("AccommType").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("AccommStatus").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("AccommScope").Value%>
            </td>
            <td class="<%=rowclass%>">
               <%=AppHelper.GetUIDate(item.Element("AccommAccepted").Value)%>
            </td>
            
            <td class="<%=rowclass%>">
                <%=item.Element("AccommRestriction").Value%> 
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("AccommText").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("NetAccommCost").Value%>
            </td>
            <td style="display:none">                           
            </td>
            <td style="display:none">                           
               <%=lnkEdit.PostBackUrl%>
            </td>    
            <td style="display:none">
              <%=item.Element("CmAccommRowId").Value%>
            </td>         
       </tr>                          
 <% }%>  
        
    </table>
    <center><div id="pageNavPosition"></div></center>   
    <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
    <asp:TextBox runat="server" id="TextBox1" style="display:none" />
    <asp:TextBox runat="server" id="DeleteListTemplate"  style="display:none" Text="Instance/CmXAccommodationList/CmXAccommodation/CmAccommRowId"/>				
    <asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
    <asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
    <asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
    <asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
    <asp:TextBox runat="server" id="SysClassName" style="display:none" Text="CmXAccommodationList" RMXRef="Instance/UI/FormVariables/SysClassName"/>						
    <asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" Text="&lt;CmXAccommodationList&gt;&lt;CmXAccommodation /&gt;&lt;/CmXAccommodationList&gt;"	RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>	
    <asp:TextBox runat="server" id="SysSerializationConfigIgnore" style="display:none" Text="Claim.Comments"	RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore"/>						                             
    <asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm"/>
    <asp:TextBox runat="server" id="SysFormPIdName" Text="casemanagementid" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
    <asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
    <asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>
    <asp:TextBox runat="server" id="SysNotReqNew" style="display:none" RMXRef="Instance/UI/FormVariables/SysNotReqNew" />
    <asp:TextBox runat="server" id="SysFormName" style="display:none" Text="CmXAccommodationList" RMXRef="Instance/UI/FormVariables/SysFormName"/>
    <asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="casemanagementid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId"/>
    <asp:TextBox runat="server" id="SysSid" style="display:none" Text="11000" RMXRef="Instance/UI/FormVariables/SysSid"/>
    <asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
    <asp:TextBox ID="paramstore" runat="server" style="display:none"></asp:TextBox>	
    <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none"/>
    <asp:TextBox runat="server" id="eventnumber" RMXRef="Instance/UI/FormVariables/SysExData/EventNumber" style="display:none"/>
    <asp:TextBox runat="server" id="claimid" RMXRef="Instance/UI/FormVariables/SysExData/ClaimId" style="display:none"/>
    <asp:TextBox runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" style="display:none"/>
    <asp:TextBox runat="server" id="casemgtrowid" RMXRef="Instance/UI/FormVariables/SysExData/CasemgtRowId" style="display:none"/>
    <asp:TextBox runat="server" id="casemanagementid" RMXRef="Instance/UI/FormVariables/SysExData/CasemgtRowId" style="display:none"/>
    <asp:TextBox runat="server" id="cmrowid" RMXRef="Instance/UI/FormVariables/SysExData/CmRowId"   style="display:none"/>           
    <asp:button class="button" runat="server" id="btnAddNew" Text="Add New" postbackurl="cmxaccommodation.aspx?SysFormName=CmXAccommodation&SysCmd=&SysViewType=tab&SysFormIdName=cmaccommrowid&SysFormPIdName=cmrowid&SysEx=CasemgtRowId|CmRowId|EventNumber&SysExMapCtl=CasemgtRowId|CmRowId|EventNumber" />
    <asp:Button class="button" runat="server" ID="btnDelete" Text="Delete" OnClick="NavigateListDelete"  OnClientClick="return DeleteList();"/>
    <asp:Button class="button" runat="server" ID="Back" Text="Back" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>      
    <input type="text" runat="server" id="PageIds" style="display:none"/>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
    <script type="text/javascript">
        var listpaging = new ListPaging('tbllist', 3); 
        listpaging.init(); 
        listpaging.showPageNavigation('listpaging', 'pageNavPosition'); 
        listpaging.showPage(1);
    </script>
</body>
</html>
