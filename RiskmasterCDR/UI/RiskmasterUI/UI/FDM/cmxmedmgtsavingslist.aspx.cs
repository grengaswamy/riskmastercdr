﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.FDM
{
    public partial class CmXMedmgtsavingsList : FDMBasePage
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        
        protected void Page_Load(object sender, EventArgs e)
       
        {
            double intTotalSavings = 0;
            try
            {

                LoadInitialPage();
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/CmXMedmgtsavingsList/CmXMedmgtsavings")
                         let xClaim = (int)Convert.ToInt64(c.Element("CmmsRowId").Value)
                         orderby xClaim descending
                         select c;
                if (rootElement.XPathSelectElement("//CmXMedmgtsavings/CasemgtRowId") != null)
                {
                    casemgtrowid.Text = rootElement.XPathSelectElement("//CmXMedmgtsavings/CasemgtRowId").Value;
                }
                foreach(XElement ele in rootElement.XPathSelectElements("//CmXMedmgtsavings/Savings"))
                {
                    intTotalSavings += Convert.ToDouble(ele.Value);
                }
                totalSavings.Value = intTotalSavings.ToString();
                    
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }

        protected void NavigateListDelete(object sender, EventArgs e)
        {
            double dTotalSavings = 0.0;//Milli MITS 18999 changing from Int to Double
            try
            {
                base.NavigateListDelete(sender, e);
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/CmXMedmgtsavingsList/CmXMedmgtsavings")
                         let xClaim = (int)Convert.ToInt64(c.Element("CmmsRowId").Value)
                         orderby xClaim descending
                         select c;
                if (rootElement.XPathSelectElement("//CmXMedmgtsavings/CasemgtRowId") != null)
                {
                    casemgtrowid.Text = rootElement.XPathSelectElement("//CmXMedmgtsavings/CasemgtRowId").Value;
                }
                foreach (XElement ele in rootElement.XPathSelectElements("//CmXMedmgtsavings/Savings"))
                {
                    dTotalSavings += Convert.ToDouble(ele.Value);
                }
                totalSavings.Value = dTotalSavings.ToString();

                //LoadInitialPage();

            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

    }
}
