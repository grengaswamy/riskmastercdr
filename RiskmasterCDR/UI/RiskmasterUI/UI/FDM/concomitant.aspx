<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="concomitant.aspx.cs" Inherits="Riskmaster.UI.FDM.Concomitant"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Concomitant Products</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28"
                        height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28"
                        height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28"
                        height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28"
                        height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28"
                        height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doLookup()" src="Images/lookup.gif"
                        width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='Images/lookup2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/lookup.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Diary()" src="Images/diary.gif" width="28"
                        height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='Images/diary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/diary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Concomitant Products" /><asp:label
            id="formsubtitle" runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSdetails" id="TABSdetails">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="details"
                id="LINKTABSdetails"><span style="text-decoration: none">Details</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABdetails" id="FORMTABdetails">
        <asp:textbox style="display: none" runat="server" id="eventid" rmxref="/Instance/EvXConcomProd/EventId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="evconcomrowid"
                rmxref="/Instance/EvXConcomProd/EvConcomRowId" rmxtype="id" /><div runat="server"
                    class="half" id="div_concomproductid" xmlns="">
                    <span class="required">Concomitant Product Number</span><span class="formw"><asp:textbox
                        runat="server" size="30" onblur="numLostFocus(this);" id="concomproductid" rmxref="/Instance/EvXConcomProd/ConcomProductId"
                        tabindex="1" maxlength="9" onchange="&#xA;                  AllowIntOnly(this);&#xA;                  setDataChanged(true);" /></span></div>
        <asp:textbox style="display: none" runat="server" id="claimid" rmxref="/Instance/EvXConcomProd/ClaimId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="claimnumber"
                rmxref="/Instance/EvXConcomProd/ClaimNumber" rmxtype="id" /><asp:textbox style="display: none"
                    runat="server" id="eventnumber" rmxref="/Instance/EvXConcomProd/EventNumber"
                    rmxtype="id" /><div runat="server" class="half" id="div_fromdate" xmlns="">
                        <span class="label">From Date</span><span class="formw"><asp:textbox runat="server"
                            formatas="date" onchange="setDataChanged(true);" id="fromdate" rmxref="/Instance/EvXConcomProd/FromDate"
                            rmxtype="date" tabindex="2" /><cc1:CalendarExtender runat="server" ID="fromdate_ajax"
                                TargetControlID="fromdate" />
                        </span>
                    </div>
        <div runat="server" class="half" id="div_todate" xmlns="">
            <span class="label">To Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="todate" rmxref="/Instance/EvXConcomProd/ToDate"
                rmxtype="date" tabindex="4" /><cc1:CalendarExtender runat="server" ID="todate_ajax"
                    TargetControlID="todate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_concomproduct" xmlns="">
            <span class="label">Concomitant Product Description</span><span class="formw"><asp:textbox
                runat="Server" id="concomproduct" rmxref="/Instance/EvXConcomProd/ConcomProduct"
                rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                tabindex="6" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                    value="..." name="concomproductbtnMemo" tabindex="7" id="concomproductbtnMemo"
                    onclick="EditMemo('concomproduct','')" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup" />
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
            <asp:button class="button" runat="Server" id="btnBack" text="Back to Medwatch" onclientclick="if(!( XFormHandler('','1','back')))return false;"
                postbackurl="?" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="EvXConcomProd" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" text="&lt;EvXConcomProd&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/EvXConcomProd&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormIdName"
                                    rmxref="Instance/UI/FormVariables/SysFormIdName" rmxtype="hidden" text="evconcomrowid" /><asp:textbox
                                        style="display: none" runat="server" id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName"
                                        rmxtype="hidden" text="eventid" /><asp:textbox style="display: none" runat="server"
                                            id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm" rmxtype="hidden"
                                            text="medwatch" /><asp:textbox style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                                rmxtype="hidden" text="eventid" /><asp:textbox style="display: none" runat="server"
                                                    id="SysNotReqNew" rmxref="Instance/UI/FormVariables/SysNotReqNew" rmxtype="hidden"
                                                    text="" /><asp:textbox style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                                        rmxtype="hidden" text="concomitant" /><asp:textbox style="display: none" runat="server"
                                                            id="SysSid" rmxref="Instance/UI/FormVariables/SysSid" rmxtype="hidden" text="12350" /><asp:textbox
                                                                style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
                                                                rmxtype="hidden" text="12050" /><asp:textbox style="display: none" runat="server"
                                                                    name="formname" value="concomitant" /><asp:textbox style="display: none" runat="server"
                                                                        name="SysRequired" value="concomproductid|" /><asp:textbox style="display: none"
                                                                            runat="server" name="SysFocusFields" value="concomproductid|" /><input type="hidden"
                                                                                id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible" style="display: none" /><asp:textbox
                                                                                    runat="server" id="SysLookupClass" style="display: none" /><asp:textbox runat="server"
                                                                                        id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server" id="SysLookupAttachNodePath"
                                                                                            style="display: none" /><asp:textbox runat="server" id="SysLookupResultConfig" style="display: none" /><input
                                                                                                type="hidden" value="rmx-widget-handle-2" id="SysWindowId" /></form>
</body>
</html>
