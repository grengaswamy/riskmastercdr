﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using Riskmaster.AppHelpers;
using System.Text;
using System.Xml.XPath;
using System.Collections.Generic;
using Riskmaster.Common;

namespace Riskmaster.UI.FDM
{
    public partial class Creatediary : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Moving it before FDMPageLoad , so that Selected Value can be set to control
                BindData();
            }

            FDMPageLoad();
            if (!IsPostBack)
            {
                #region Saving QueryString values into View State

                if (Request.QueryString["assignedusername"] != null)
                {
                    ViewState["assigneduser"] = Request.QueryString["assignedusername"];
                }
                else
                {
                    ViewState["assigneduser"] = "";
                }

                if (Request.QueryString["attachtable"] != null)
                {
                    ViewState["attachtable"] = Request.QueryString["attachtable"];
                }
                else
                {
                    ViewState["attachtable"] = "";
                }

                if (Request.QueryString["attachrecord"] != null)
                {
                    ViewState["attachrecordid"] = Request.QueryString["attachrecord"];
                }
                else
                {
                    ViewState["attachrecordid"] = "";
                }

                if (Request.QueryString["ispeekdiary"] != null)
                {
                    ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                }
                else
                {
                    ViewState["ispeekdiary"] = "false";
                }

                //##Edit
                if (!string.IsNullOrWhiteSpace(Request.QueryString["entryid"]))
                {
                    TextBox txtAttachRecord = (TextBox)this.FindControl("AttachRecordid");
                    if (Request.QueryString["attachprompt"] != null)
                    {
                        if ((Request.QueryString["attachprompt"]) != string.Empty)
                        {
                            if (txtAttachRecord != null)
                            {
                                txtAttachRecord.Text = Request.QueryString["attachprompt"];
                            }
                        }
                        else
                        {
                            if (txtAttachRecord != null)
                            {
                                txtAttachRecord.Text = "No";
                            }
                        }
                    }
                    else
                    {
                        if (txtAttachRecord != null)
                        {
                            txtAttachRecord.Text = "No";
                        }
                    }
                }

                if (Request.QueryString["assigninguser"] != null)
                {
                    ViewState["assigninguser"] = Request.QueryString["assigninguser"];
                }
                else
                {
                    ViewState["assigninguser"] = "";
                }

                if (Request.QueryString["entryid"] != null)
                {
                    ViewState["entryid"] = Request.QueryString["entryid"];
                }
                else
                {
                    ViewState["entryid"] = "";
                }
                //##Edit

                if (Request.QueryString["isdiarycalendar"] != null)
                {
                    ViewState["isdiarycalendar"] = Request.QueryString["isdiarycalendar"];
                }

                if (Request.QueryString["isdiarycalendarpeek"] != null)
                {
                    ViewState["isdiarycalendarpeek"] = Request.QueryString["isdiarycalendarpeek"];
                }
                else
                {
                    ViewState["isdiarycalendarpeek"] = "";
                }

                if (Request.QueryString["isdiarycalendarprocessingoffice"] != null)
                {
                    ViewState["isdiarycalendarprocessingoffice"] = Request.QueryString["isdiarycalendarprocessingoffice"];
                }
                else
                {
                    ViewState["isdiarycalendarprocessingoffice"] = "";
                }

                if (Request.QueryString["RtnClaimScreen"] != null)
                {
                    ViewState["RtnClaimScreen"] = Request.QueryString["RtnClaimScreen"];
                }

                #endregion

                //##Create
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CreateDiary.aspx"), "CreateDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "CreateDiaryValidations", sValidationResources, true);
                //##Edit
                sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("EditDiary.aspx"), "EditDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "EditDiaryValidations", sValidationResources, true);

                TextBox txtDueDate = (TextBox)this.FindControl("CompleteDate_date");
                if (txtDueDate != null && string.IsNullOrEmpty(txtDueDate.Text))
                {
                    txtDueDate.Text = AppHelper.GetDate(System.DateTime.Today.ToString());
                }

                TextBox txtDueTime = (TextBox)this.FindControl("CompleteDate_time");
                if (txtDueTime != null && string.IsNullOrEmpty(txtDueTime.Text))
                {
                    txtDueTime.Text = AppHelper.GetTime(System.DateTime.Now.TimeOfDay.ToString());
                }
            }
        }

        private void BindData()
        {
            #region Populating Priority Dropdown
            DropDownList ddlPriority = (DropDownList)this.FindControl("Priority");
            string sPriorityOptional = RMXResourceProvider.GetSpecificObject("Optional", RMXResourceProvider.PageId("CreateDiary.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            string sPriorityImportant = RMXResourceProvider.GetSpecificObject("Important", RMXResourceProvider.PageId("CreateDiary.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            string sPriorityRequired = RMXResourceProvider.GetSpecificObject("Required", RMXResourceProvider.PageId("CreateDiary.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            if (ddlPriority != null)
            {
                ddlPriority.Items.Clear();
                ddlPriority.Items.Insert(0, new ListItem(sPriorityOptional, "1"));
                ddlPriority.Items.Insert(1, new ListItem(sPriorityImportant, "2"));
                ddlPriority.Items.Insert(2, new ListItem(sPriorityRequired, "3"));
            }
            #endregion

            Label formtitle = (Label)this.FindControl("formtitle");
            if (string.IsNullOrWhiteSpace(Request.QueryString["entryid"]))
                formtitle.Text = RMXResourceProvider.GetSpecificObject("lblCreateDiary", RMXResourceProvider.PageId("CreateDiary.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());                
            else
                formtitle.Text = RMXResourceProvider.GetSpecificObject("lblEditDiary", RMXResourceProvider.PageId("EditDiary.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
        }

        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            XElement oUseWpaTask = oMessageElement.XPathSelectElement("//UseWpaTask");
            if (oUseWpaTask != null && oUseWpaTask.Value == "-1")
            {
                this.Page.ClientScript.RegisterStartupScript(typeof(string), "WpaTaskIsSelect", "_isSelect = true;", true);
            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            TextBox txthdnDiaryLstGroups = (TextBox)this.FindControl("hdnDiaryLstGroups");//pkandhari Jira 6412
            TextBox txthdnDiaryLstUsers = (TextBox)this.FindControl("hdnDiaryLstUsers");//pkandhari Jira 6412
            TextBox actstring = (TextBox)this.FindControl("actstring");
            XmlNode TempNode = null;
            XmlNode TempNodeToAdd = null;
            bool bIsSuccess = false;
            int iEntryId = 0;

            using (XmlReader reader = Xelement.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            XElement objEntryId = Xelement.XPathSelectElement("//Document/ParamList/Param[@name='SysPropertyStore']/Instance/WpaDiaryEntry/EntryId");
            if (objEntryId != null)
                iEntryId = Conversion.CastToType<int>(objEntryId.Value, out bIsSuccess);

            TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/CalledBy");
            if (TempNode != null)
                TempNode.InnerText = "creatediary";

            TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ActivityList");
            if (TempNode != null)
                if (actstring != null)
                    TempNode.InnerText = actstring.Text;

            if (iEntryId == 0)
            {
                TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/UserNameList");
                if (TempNode != null)
                    if (txthdnDiaryLstUsers != null)
                        TempNode.InnerText = txthdnDiaryLstUsers.Text;

                TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/GroupIdList");
                if (TempNode != null)
                    if (txthdnDiaryLstGroups != null)
                        TempNode.InnerText = txthdnDiaryLstGroups.Text;
            }

            TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/AssigningUser");
            if (TempNode != null)
            {
                TempNode.InnerText = "currentuser";
                if (iEntryId > 0 && ViewState["assigninguser"] != null)
                    TempNode.InnerText = Convert.ToString(ViewState["assigninguser"]);
            }


            XElement xAttachRecordId = Xelement.XPathSelectElement("//Document/ParamList/Param[@name='SysPropertyStore']/Instance/WpaDiaryEntry/AttachRecordid");
            if (xAttachRecordId != null)
            {
                int iAttachRecordId = Conversion.CastToType<int>(xAttachRecordId.Value, out bIsSuccess);
                TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/AttachTable");
                if (TempNode != null)
                {
                    TempNode.InnerText = iAttachRecordId > 0 ? (TempNode.InnerText == string.Empty ? "CLAIM" : TempNode.InnerText) : "";
                }
            }

            TempNode = xmlNodeDoc.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/StatusOpen");
            if (TempNode != null)
                TempNode.InnerText = iEntryId > 0 ? "" : "1";

            Xelement = XElement.Parse(xmlNodeDoc.InnerXml);
        }

        public override void ModifyControls()
        {
            TextBox txtAttachRecord = (TextBox)this.FindControl("AttachRecordid");
            TextBox actstring = (TextBox)this.FindControl("actstring");
            TextBox txtActivities = (TextBox)this.FindControl("txtActivities");
            TextBox LoginID = (TextBox)((UserControl)this.FindControl("AssignedUser")).FindControl("LoginID");
            TextBox LoginUserName = (TextBox)((UserControl)this.FindControl("AssignedUser")).FindControl("LoginUserName");
            TextBox DefaultAssignedTo = (TextBox)((UserControl)this.FindControl("AssignedUser")).FindControl("DefaultAssignedTo");
            TextBox UserStr = (TextBox)((UserControl)this.FindControl("AssignedUser")).FindControl("UserName");
            TextBox UserIdStr = (TextBox)((UserControl)this.FindControl("AssignedUser")).FindControl("UserId");
            TextBox txthdnDiaryLstGroups = (TextBox)this.FindControl("hdnDiaryLstGroups");
            TextBox txthdnDiaryLstUsers = (TextBox)this.FindControl("hdnDiaryLstUsers");

            base.ModifyControls();
            XmlDocument oXmlData = base.Data;
            if (oXmlData != null)
            {
                //XmlNode xAttachRecordInfo = oXmlData.SelectSingleNode("//Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/AttachRecordInfo");
                //if (txtAttachRecord != null && txtAttachRecord.Visible)
                //{
                //    if (xAttachRecordInfo != null)
                //        txtAttachRecord.Text = xAttachRecordInfo.InnerText;
                //    else
                //        txtAttachRecord.Text = "";
                //}

                if (txtActivities != null && actstring != null)
                {
                    if (oXmlData.SelectSingleNode("//ActivityListOption/sActs") != null)
                    {
                        txtActivities.Text = oXmlData.SelectSingleNode("//ActivityListOption/sActs").InnerText;
                        actstring.Text = txtActivities.Text;
                    }
                }

                if (txthdnDiaryLstUsers != null && oXmlData.SelectSingleNode("//UserNameList") != null)
                    txthdnDiaryLstUsers.Text = oXmlData.SelectSingleNode("//UserNameList").InnerText;

                if (txthdnDiaryLstGroups != null && oXmlData.SelectSingleNode("//GroupIdList") != null)
                    txthdnDiaryLstGroups.Text = oXmlData.SelectSingleNode("//GroupIdList").InnerText;

                if (DefaultAssignedTo != null && LoginUserName != null && LoginID != null)
                {
                    if (oXmlData.SelectSingleNode("//LoginID") != null)
                        LoginID.Text = oXmlData.SelectSingleNode("//LoginID").InnerText;
                    if (oXmlData.SelectSingleNode("//LoginUser") != null)
                        LoginUserName.Text = oXmlData.SelectSingleNode("//LoginUser").InnerText;
                    if (oXmlData.SelectSingleNode("//DefaultAssignedTo") != null)
                        DefaultAssignedTo.Text = oXmlData.SelectSingleNode("//DefaultAssignedTo").InnerText;

                    if (!string.IsNullOrEmpty(DefaultAssignedTo.Text) && DefaultAssignedTo.Text == "True" && LoginUserName.Text != "" && LoginID.Text != "")
                    {
                        if (UserStr != null)
                            UserStr.Text = LoginUserName.Text;
                        if (UserIdStr != null)
                            UserIdStr.Text = LoginID.Text;
                    }
                }
            }
        }

        protected new void NavigateSave(object sender, EventArgs e)
        {
            string isDiaryCalPeek = string.Empty;
            string isDiaryCalProssOffice = string.Empty;
            string diaryListUrl = string.Empty;
            string sDiaryError = string.Empty;
            TextBox oSysIsServiceError = (TextBox)this.FindControl("SysIsServiceError");

            try
            {
                base.NavigateSave(sender, e);

                if (oSysIsServiceError != null && oSysIsServiceError.Text != "0")
                {
                    if (ViewState["isdiarycalendar"] != null && Convert.ToString(ViewState["isdiarycalendar"]).ToLower() == "true")
                    {
                        isDiaryCalPeek = Convert.ToString(ViewState["isdiarycalendarpeek"]);
                        isDiaryCalProssOffice = Convert.ToString(ViewState["isdiarycalendarprocessingoffice"]);

                        diaryListUrl = "../Diaries/DiaryCalendar/DiaryCalendar.aspx?assigneduser=" + Convert.ToString(ViewState["assigneduser"]) + "&IsPeek=" + isDiaryCalPeek + "&IsProcessingOffice=" + isDiaryCalProssOffice;
                        Response.Redirect(diaryListUrl, false);
                    }
                    //nsachdeva2 15 Sep 2011: MITS 25643
                    else if (ViewState["RtnClaimScreen"] != null)
                    {
                        this.Page.ClientScript.RegisterStartupScript(typeof(string), "CreateDiary", " if(window.opener.document.getElementById('containsopendiaries')!=null) window.opener.document.getElementById('containsopendiaries').value = true; if(window.opener.document.forms[0].id='frmDiaryList') {window.opener.document.forms[0].__EVENTTARGET.value = 'creatediary';window.opener.document.forms[0].submit();} self.close();", true);
                    }
                    //End MITS: 25643
                    else
                    {
                        diaryListUrl = "../Diaries/DiaryList.aspx?assigneduser=" + Convert.ToString(ViewState["assigneduser"]) + "&ispeekdiary=" + Convert.ToString(ViewState["ispeekdiary"]) + "&attachtable=" + Convert.ToString(ViewState["attachtable"]) + "&attachrecordid=" + Convert.ToString(ViewState["attachrecordid"]);
                        Response.Redirect(diaryListUrl, false);
                    }
                }
            }
            catch (ApplicationException ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string isDiaryCalPeek = string.Empty;
            string isDiaryCalProssOffice = string.Empty;
            string diaryListUrl = string.Empty;
            Label lblError = (Label)this.FindControl("lblError");

            try
            {
                if (ViewState["isdiarycalendar"] != null && Convert.ToString(ViewState["isdiarycalendar"]).ToLower() == "true")
                {
                    isDiaryCalPeek = Convert.ToString(ViewState["isdiarycalendarpeek"]);
                    isDiaryCalProssOffice = Convert.ToString(ViewState["isdiarycalendarprocessingoffice"]);

                    diaryListUrl = "../Diaries/DiaryCalendar/DiaryCalendar.aspx?assigneduser=" + Convert.ToString(ViewState["assigneduser"]) + "&IsPeek=" + isDiaryCalPeek + "&IsProcessingOffice=" + isDiaryCalProssOffice;
                    Response.Redirect(diaryListUrl, false);

                }
                //nsachdeva2 15 Sep 2011: MITS 25643
                else if (ViewState["RtnClaimScreen"] != null)
                {
                    this.Page.ClientScript.RegisterStartupScript(typeof(string), "CreateDiary", " if(window.opener.document.getElementById('containsopendiaries')!=null) window.opener.document.getElementById('containsopendiaries').value = true; if(window.opener.document.forms[0].id='frmDiaryList') {window.opener.document.forms[0].__EVENTTARGET.value = 'creatediary';window.opener.document.forms[0].submit();} self.close();", true);
                }
                //End MITS: 25643
                else
                {
                    diaryListUrl = "../Diaries/DiaryList.aspx?assigneduser=" + Convert.ToString(ViewState["assigneduser"]) + "&ispeekdiary=" + Convert.ToString(ViewState["ispeekdiary"]) + "&attachtable=" + Convert.ToString(ViewState["attachtable"]) + "&attachrecordid=" + Convert.ToString(ViewState["attachrecordid"]);
                    Response.Redirect(diaryListUrl, false);
                }
            }
            catch (Exception ee)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }
    }
}
