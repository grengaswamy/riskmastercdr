﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="propertyuardetails.aspx.cs" Inherits="Riskmaster.UI.FDM.propertyuardetails" ValidateRequest="false"  %>

<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Property UAR Details</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script  type="text/javascript" language="javascript" src='../../Scripts/form.js'>   {var i;}</script>
    <script  type="text/javascript" language="javascript" src='../../Scripts/EnhPolicy.js'>   {var i;}</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">{var i;}</script>
    <style type="text/css">
        .style1
        {
            border-style: none;
            border-color: inherit;
            border-width: medium;
            FONT-WEIGHT: bold;
            PADDING-BOTTOM: 3px;
            COLOR: #ffffff;
            PADDING-TOP: 3px;
            BACKGROUND-COLOR: #95B3D7;
            float: left;
            width: 418px;
        }
    </style>
</head>

<%--Sumit - Start(04/27/2010) - MITS# 20483 - Updated Entire page to include the Schedule functionality.--%>
<body  onload="UarDetailsOnLoad();" >
    <form id="frmData" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Add/Modify UAR" /></div>
        <br />
        <table border="0" style="width: 98%">
                <tr>
                    <td>
                        <div class="tabGroup" id="TabsDivGroup" runat="server">
                            <div class="Selected" nowrap="true" runat="server" name="TABSUarDetail" id="TABSUarDetail">
                                <a class="Selected" href="#" onclick="tabChange(this.name);return false;" runat="server"
                                    rmxref="" name="UarDetail" id="LINKTABSUarDetail">UAR Details</a>
                            </div>
                            <div class="tabSpace" runat="server" id="TBSUarDetail">
                                <nbsp />
                                <nbsp />
                            </div>
                            <div class="NotSelected" nowrap="true" runat="server" name="TABSSchedule"
                                id="TABSSchedule">
                                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                                    rmxref="" name="Schedule" id="LINKTABSSchedule">Schedule</a>
                            </div>
                            
                            <div class="tabSpace" runat="server" id="TBSSchedule">
                                <nbsp />
                                <nbsp />
                            </div>
                        </div>
                    </td>
                </tr>
        </table>
        
    <div>
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABUarDetail" id="FORMTABUarDetail" width="100%">
            <tr>
                <td>Property ID</td>
                    <td>
                    <asp:TextBox runat ="server" size = "30" onblur=""  onchange="setDataChanged(true);" name="propertyidcode" id="propertyidcode" RMXRef = "Instance/Document/PolicyXUar/PIN"  ReadOnly ="true"/>
                    <%--Sumit - Start(10/19/2010) - MITS# 21996--%>
                    <input type="button" name="propertyidcodebtn"  class="CodeLookupControl"  id="propertyidcodebtn" runat="server"/>
                    <%--End:Sumit--%>
                 </td>
            </tr>
             <tr>
                 <td>Address 1</td>
                    <td>
                    <asp:TextBox runat ="server" size="30" onblur=""  onchange="setDataChanged(true);" name="address1" id="address1" ReadOnly ="true" RMXRef = "Instance/Document/PolicyXUar/Address1" class="grayline2" />
                    </td>
             </tr>
             <tr>
                 <td>Address 2</td>
                    <td>
                    <asp:TextBox size="30" runat="server"  onblur=""  onchange="setDataChanged(true);" name="address2" id="address2" ReadOnly ="true" RMXRef = "Instance/Document/PolicyXUar/Address2" class="grayline2"/>
                    </td>
             </tr>
             <tr>
                 <td>City</td>
                    <td>
                    <asp:TextBox size="30" runat="server"  onblur=""  onchange="setDataChanged(true);" name="city" id="city" ReadOnly ="true" RMXRef = "Instance/Document/PolicyXUar/City" class="grayline2"/>
                    </td>
             </tr>
             <tr>
                 <td>State</td>
                    <td>
                    <asp:TextBox size="30" runat="server"  onblur=""  onchange="setDataChanged(true);" name="state" id="state" ReadOnly ="true" RMXRef = "Instance/Document/PolicyXUar/State" class="grayline2"/>
                    </td>
             </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABSchedule" id="FORMTABSchedule">
            <tr>
                <td width="80%" style="height: 100%">
                    <div class="completerow" style="height: 100%">
                        <dg:UserControlDataGrid runat="server" ID="ScheduleListGrid" GridName="ScheduleListGrid"
                            GridTitle="" Target="/Document/PolicyXUar/ScheduleList" RMXRef="/Instance/Document/PolicyXUar/SelectedScheduleId"
                            Unique_Id="ScheduleRowId" ShowRadioButton="True" Width="99%" Height="80%"
                            HideNodes="|SessionId|ScheduleRowId|" TextColumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="500"
                            PopupHeight="400" HideButtons="" Type="GridAndButtons"  Editable="True" BindingRequired="true" />
                        <asp:TextBox Style="display: none" runat="server" ID="ScheduleListGridSelectedId" RMXType="id" RMXRef="/Instance/Document/PolicyXUar/ScheduleListSelectedId"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="ScheduleListGrid_NewRowSessionId" RMXType="id" RMXRef="/Instance/Document/PolicyXUar/NewAddedScheduleSessionId"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="ScheduleListGrid_EditRowSessionId" RMXType="id"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="ScheduleListGrid_DeleteScheduleRowId" RMXType="id" RMXRef="/Instance/Document/PolicyXUar/DeleteScheduleRowId"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="ScheduleListGrid_RowDeletedFlag" RMXType="id" Text="false"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="ScheduleListGridAction" RMXType="id" RMXRef="/Instance/Document/PolicyXUar/ScheduleListGridAction"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="ScheduleListGrid_ScheduleSessionId" RMXType="id" ></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="ScheduleListGrid_RowAddedFlag" RMXType="id" Text="false"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="ScheduleListGrid_SessionIds" RMXType="id" RMXRef="/Instance/Document/PolicyXUar/ScheduleListGridSessionIds"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="ScheduleListGrid_ScheduleRowIds" RMXType="id" RMXRef="/Instance/Document/PolicyXUar/ScheduleListGridScheduleRowIds"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="PCUARListGrid_SelectedSessionIds" RMXType="id" RMXRef="/Instance/Document/PolicyXUar/PCUARListGridSelectedSessionIds"></asp:TextBox>
                    </div>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                 <td>
                     <br />
                     <br />
                     <asp:button class="button" runat="server" id="btnUarOk" RMXRef="" Text="OK" width="75px" onClientClick="return fnUAROk();" onclick="btnUAROk_Click" />
                     
                     &nbsp;
                     
                     <asp:button class="button" runat="server" id="btnUarCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return fnUARCancel();" onclick="btnUAROk_Click" />
                </td>
            </tr> 
        </table> 
    </div>    

    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="txtData"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="txtGenerateXml" value="true"></asp:TextBox>
    <asp:TextBox style="display: none" runat="server" ID="formname" value="vehicleuardetails" />
    <asp:TextBox Style="display: none" runat="server" ID="PropertyID"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="FunctionToCall"></asp:TextBox>
    <asp:TextBox style="display:none" runat="server" id="SessionId" RMXRef="/Instance/Document/PolicyXUar/SessionId" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
    <asp:TextBox style="display:none" runat="server" id="UarId" RMXRef="/Instance/Document/PolicyXUar/UarId" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
    <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" />
    <asp:TextBox style="display:none" runat="server" id="hdnCodeID" RMXRef="" RMXType="id" />
    <asp:TextBox style="display:none" runat="server" id="hdnOkEnabled" value="false" />
    <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" />
    <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" /> 
    <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" />    
    <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="" RMXType="id" />  
    <asp:TextBox style="display:none" runat="server" id="PostBackAction" RMXRef="" RMXType="id" /> 
    <asp:TextBox Style="display: none" runat="server" ID="hTabName"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="HdnTabPress"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="HdnTab"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="ScheduleAddButtonEnabled"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="ScheduleEditButtonEnabled"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="ScheduleDeleteButtonEnabled"></asp:TextBox>
    <%--Sumit - End--%>
    </form>
</body>
</html>
