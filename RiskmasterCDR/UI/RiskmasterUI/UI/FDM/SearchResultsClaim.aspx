﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchResultsClaim.aspx.cs" Inherits="Riskmaster.UI.FDM.SearchResultsClaim" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head runat="server">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
  <title>Search Results</title>
   <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
  <script language="javaScript" src="/oxf/csc-Pages/riskmaster/Search/Common/Javascript/searchresults.js"></script><script src="csc-Theme/riskmaster/common/javascript/zapatec/utils/zapatec.js" type="text/javascript"></script><script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/window.js" type="text/javascript"></script><script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/dialog.js" type="text/javascript"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/lang/calendar-en.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar-setup.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/calendar-alias.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-xml.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-editable.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-query.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/rmx-common-ref.js"></script></head>
 <body class="Margin0">
  <form id="wsrp_rewrite_form_1" name="frmData" method="post" action="/oxf/home"><input type="hidden" id="wsrp_rewrite_action_1" name="" value=""><table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
     <td class="Arial" nowrap="">1 - 10 of 92 matches<br></td>
     <td class="Arial" align="right" width="50%" nowrap="">
      			Page 1 of 10<span class="disabled">&lt;&lt; First Previous </span><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26MoveToPage&amp;setvalue%26node-ids%26273%26content%262&amp;setvalue%26node-ids%26280%26content%26&amp;setvalue%26node-ids%26274%26content%2692';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="pagescroll"> Next </a><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26MoveToPage&amp;setvalue%26node-ids%26273%26content%2610&amp;setvalue%26node-ids%26280%26content%26&amp;setvalue%26node-ids%26274%26content%2692';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="pagescroll"> Last </a><span class="pagescrolldec">&gt;&gt;</span></td>
    </tr>
   </table>
   <table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td class="ctrlgroup4" nowrap="" colspan="9">Search 
      										
     </td>
    </tr>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26281%26content%26ascending&amp;setvalue%26node-ids%26279%26content%261&amp;setvalue%26node-ids%26280%26content%261';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Claim Number</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26281%26content%26ascending&amp;setvalue%26node-ids%26279%26content%262&amp;setvalue%26node-ids%26280%26content%262';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Claimant Last Name</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26281%26content%26ascending&amp;setvalue%26node-ids%26279%26content%263&amp;setvalue%26node-ids%26280%26content%263';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Claimant First Name</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26281%26content%26ascending&amp;setvalue%26node-ids%26279%26content%264&amp;setvalue%26node-ids%26280%26content%264';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Claimant SSN/Tax ID</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26281%26content%26ascending&amp;setvalue%26node-ids%26279%26content%265&amp;setvalue%26node-ids%26280%26content%265';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Claim Date</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26281%26content%26ascending&amp;setvalue%26node-ids%26279%26content%266&amp;setvalue%26node-ids%26280%26content%266';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Line of Business</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26281%26content%26ascending&amp;setvalue%26node-ids%26279%26content%267&amp;setvalue%26node-ids%26280%26content%267';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Claim Type</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26281%26content%26ascending&amp;setvalue%26node-ids%26279%26content%268&amp;setvalue%26node-ids%26280%26content%268';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Department</a></td>
    <tr class="rowdark2">
     <td class="Arial" nowrap=""><a href="#" onclick="parent.MDI('open', 'CGC&100');">GC6666000100</a></td>
     <td class="Arial" nowrap="">Dodich&nbsp;</td>
     <td class="Arial" nowrap="">Sandra&nbsp;</td>
     <td class="Arial" nowrap="">123-49-8765&nbsp;</td>
     <td class="Arial" nowrap="">2/1/2006&nbsp;</td>
     <td class="Arial" nowrap="">GC - General Claims&nbsp;</td>
     <td class="Arial" nowrap="">IO - Information Only&nbsp;</td>
     <td class="Arial" nowrap="">6760 - Store 6760&nbsp;</td>
    </tr>
    <tr class="rowlight2">
     <td class="Arial" nowrap=""><a href="#" onclick="parent.MDI('open', 'CGC&99');">GC6666000099</a></td>
     <td class="Arial" nowrap="">Ladd&nbsp;</td>
     <td class="Arial" nowrap="">Gary&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">2/1/2006&nbsp;</td>
     <td class="Arial" nowrap="">GC - General Claims&nbsp;</td>
     <td class="Arial" nowrap="">IO - Information Only&nbsp;</td>
     <td class="Arial" nowrap="">6760 - Store 6760&nbsp;</td>
    </tr>
    <tr class="rowdark2">
     <td class="Arial" nowrap=""><a href="#" onclick="parent.MDI('open', 'CGC&98');">GC6666000098</a></td>
     <td class="Arial" nowrap="">Walters&nbsp;</td>
     <td class="Arial" nowrap="">Barbara&nbsp;</td>
     <td class="Arial" nowrap="">123-45-6789&nbsp;</td>
     <td class="Arial" nowrap="">2/8/2006&nbsp;</td>
     <td class="Arial" nowrap="">WC - Workers' Compensation &nbsp;</td>
     <td class="Arial" nowrap="">M - Medical Only&nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr class="rowlight2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(3)">GCN6760000003</a></td>
     <td class="Arial" nowrap="">Hamann&nbsp;</td>
     <td class="Arial" nowrap="">Michael&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">2/23/2006&nbsp;</td>
     <td class="Arial" nowrap="">GC - General Claims&nbsp;</td>
     <td class="Arial" nowrap="">N - Normal&nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr class="rowdark2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(4)">GCN6666000004</a></td>
     <td class="Arial" nowrap="">Ladd&nbsp;</td>
     <td class="Arial" nowrap="">Gary&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">3/1/2006&nbsp;</td>
     <td class="Arial" nowrap="">GC - General Claims&nbsp;</td>
     <td class="Arial" nowrap="">N - Normal&nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr class="rowlight2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(5)">WCIND1027000005</a></td>
     <td class="Arial" nowrap="">Dodich&nbsp;</td>
     <td class="Arial" nowrap="">Sandra&nbsp;</td>
     <td class="Arial" nowrap="">123-49-8765&nbsp;</td>
     <td class="Arial" nowrap="">2/13/2006&nbsp;</td>
     <td class="Arial" nowrap="">WC - Workers' Compensation &nbsp;</td>
     <td class="Arial" nowrap="">IND - Indemnity&nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr class="rowdark2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(6)">GCN1621000006</a></td>
     <td class="Arial" nowrap="">Partin&nbsp;</td>
     <td class="Arial" nowrap="">James&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">3/1/2006&nbsp;</td>
     <td class="Arial" nowrap="">GC - General Claims&nbsp;</td>
     <td class="Arial" nowrap="">N - Normal&nbsp;</td>
     <td class="Arial" nowrap="">1621 - Store 1621&nbsp;</td>
    </tr>
    <tr class="rowlight2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(15)">TestWCNoYes1</a></td>
     <td class="Arial" nowrap="">Steinhoff&nbsp;</td>
     <td class="Arial" nowrap="">Laura&nbsp;</td>
     <td class="Arial" nowrap="">987-65-4321&nbsp;</td>
     <td class="Arial" nowrap="">3/1/2006&nbsp;</td>
     <td class="Arial" nowrap="">WC - Workers' Compensation &nbsp;</td>
     <td class="Arial" nowrap="">M - Medical Only&nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr class="rowdark2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(16)">TestGCNoYes1</a></td>
     <td class="Arial" nowrap="">Hopper&nbsp;</td>
     <td class="Arial" nowrap="">Carrie Ann&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">3/1/2006&nbsp;</td>
     <td class="Arial" nowrap="">GC - General Claims&nbsp;</td>
     <td class="Arial" nowrap="">IO - Information Only&nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr class="rowlight2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(16)">TestGCNoYes1</a></td>
     <td class="Arial" nowrap="">Ruiz&nbsp;</td>
     <td class="Arial" nowrap="">Debra&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">3/1/2006&nbsp;</td>
     <td class="Arial" nowrap="">GC - General Claims&nbsp;</td>
     <td class="Arial" nowrap="">IO - Information Only&nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr>
     <td class="ctrlgroup4" nowrap="" colspan="9">&nbsp;
      										
     </td>
    </tr>
   </table>
   <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
     <td class="Arial" nowrap="">1 - 10 of 92 matches<br></td>
     <td class="Arial" align="right" width="50%" nowrap="">
      			Page 1 of 10<span class="disabled">&lt;&lt; First Previous </span><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26MoveToPage&amp;setvalue%26node-ids%26273%26content%262&amp;setvalue%26node-ids%26280%26content%26&amp;setvalue%26node-ids%26274%26content%2692';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="pagescroll"> Next </a><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26MoveToPage&amp;setvalue%26node-ids%26273%26content%2610&amp;setvalue%26node-ids%26280%26content%26&amp;setvalue%26node-ids%26274%26content%2692';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="pagescroll"> Last </a><span class="pagescrolldec">&gt;&gt;</span></td>
    </tr>
   </table><input type="hidden" name="$node^21" value="" id="sys_ex"><input type="hidden" name="$node^18" value="claim" id="searchcat"><input type="hidden" name="$node^34" value="" id="admtable"><input type="hidden" name="$node^35" value="" id="entitytableid"><table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
     <td><br></td>
    </tr>
    <tr class="Arial">
     <td align="center"><input type="submit" name="$action^setvalue%26node-ids%267%26content%26Back&amp;setvalue%26node-ids%26279%26content%26&amp;setvalue%26node-ids%26278%26content%26True" value="Return To Search" id="btnBack" class="button" style="width:100px"><input type="submit" name="$action^setvalue%26node-ids%267%26content%26Print" value="Print" id="btnPrint" class="button" style="width:100px"></td>
    </tr>
   </table><input type="hidden" name="$node^5" value="rmx-widget-handle-3" id="SysWindowId"><input type="hidden" name="$instance" value="H4sIAAAAAAAAAO1aW3ObOBR+Tn9Fxu+NjC/JpkOYcQKeYde3MbjbPnkUkG0mIHmQXNv761dcDDgR&#xA;Fa497bTBL+hyjo70fQcdJB/VxJRB7KDrXeBj+mm3IGFAHxorxtafANhutzfb9g0Jl6DVbLZA0t3Q&#xA;PlxdXV2nPzVADMYtWVPWRZ0QIfzRczUQevQlgJShEFgIhs4qfUwR3fhMBbmoeKgNRWHU61BHBYdK&#xA;iVVEqUeiscDRTDOBrYddso0GUEFezrpBtqLculdiCzqMmwIlvQMPv/yD9mXddMUtozAkIdUW0KeI&#xA;w1BoEislnccr44h4xw1WgoGJF+SVqE6cTYAw0wqyERFD6GHtw2tzb3+quRgQ8jJba00VZOV4rm8m&#xA;m+vgSUgcPqdYKatItKzYJ/o+XGqKCgo1id5nD21NN9JJSxL5PvfqEQwQd4esWAWJg7CNgjWQGbHh&#xA;s4+4t7PQc5hU2tpTY8fnkzxlwogxDy9ptTmYutw62WCX2/0YwZ6WJSo6WkD+IqfvdkXphB6pdCQm&#xA;X9wTZAnpSUE2A4+ufbjve8h3D45YKp1pjUMXhY97BZyq0TpZoy1dMBCtoVQ6YYZjIx235waxo3D3&#xA;y4oSFQMzj+0T94p21eO6zCv3aySdVLLPVBJNVpqiUiaVSS8iuTT8Ods89vFAc+OQAEzzwPUveuaB&#xA;55vnIPBEgoDgRqrnIh8tYRQJMnUSPiOC49BJdgvA5UAuddCjBK4zDeqsEDd0w7ui9lg1KgCEvyGf&#xA;8IU3ruPJMg7CQ0NpXHvuQ6M/0JVmk5cp3az5YLyI+Zb00Hga9MzhfDQbPhrTxjWDzzzQoV2sxyJe&#xA;Uglei4djaMcaQrTU8RqF8bS1BxXklTdyIJ7dO0FcBPmgZ9nzUW9oFPFuHePdG9lzY2Sb9tca+R9D&#xA;XhEg3zenAujbNfSXhf5eAL3d+zI39SLsnRr2s2HvZLC3m50j1H3E5Qv468aE43vMQDdjwPjMoT/g&#xA;zm2uzsM9lp070clK3BWbFXX/mYw5xEVFX59bds+eWUdUtgtvUKc0UCeK86exfrSF3YqjdWT35zCp&#xA;xXZVkNXfKbH214lRSmu7lNZI7Q2pdzWpv3p/vSuwJ/qg0Hu2MR/35weCMu7+EnPnQlbCnYTPZ8S2&#xA;CGEZq8JRLAZDpnPLYrqFOgZ2q2n88S6gFEKs6Gs+Dp2CA9S9OLjWHzUnvHRt5e47L90B1/zM2hRj&#xA;Xr91v4cPFKLpwBzFHD/OLF6yyj+VWqIDdq78JqgqJRcbF4yq0eV1/BRLvLNv35JDYrvyzYgiuBrp&#xA;2fZ4OjLqo+IPgl/9ckQR3I7U6J+Bfqtb/VJQye9IdKNvjPT6huQs4E9w+26N/AWR71Z3+dvXwNe7&#xA;zXnQn+D0dzX2l8T+hL9/lPzCoKf/PbNsY1rvOGdBf4Lb39fYX/LzsntbHftW4cj+ZWJM6/+AzkC+&#xA;Uz3MtvJj8MSsMT8H8xOcvVWDfqEtpvr1QSs/wA7M+ux6Lu4neHvnFwOvgqMUrwrZkSRNOEwyGosN&#xA;4iRbUMiJLQyUJyynRiWmJ3CJRkSelkkY9KfIIaErT3Mcwl0yBbloZN7y/quaXFchE9NcPELnpUL+&#xA;aMieiL8JoszovL1KTm2qOoGUgkpJuHG6pAapg7Dr4SX3q7jhu7ymHCZNWUI0V4mSk5OEeO1/SKfa&#xA;dhsvAAA="></form>
 </body>
</html>