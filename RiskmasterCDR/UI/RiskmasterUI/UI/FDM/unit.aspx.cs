﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//smahajan6 - MITS #18644 - 11/18/2009 : Start
using System.Xml.XPath;
//smahajan6 - MITS #18644 - 11/18/2009 : End

namespace Riskmaster.UI.FDM
{
    public partial class Unit : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
            CheckBox chkvlInsured = (CheckBox)Page.FindControl("vlInsured");
            if (chkvlInsured != null)
            {
                chkvlInsured.Enabled = false;
            }
        }
        //bsharma33 - MITS 31087
        protected new void NavigateSave(object sender, EventArgs e)
        {
            base.NavigateSave(sender, e);
            CheckBox chkvlInsured = (CheckBox)Page.FindControl("vlInsured");
            if (chkvlInsured != null)
            {
                chkvlInsured.Enabled = false;
            }
        }
        //smahajan6 - MITS #18644 - 11/18/2009 : Start
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            Control oControl = null;

            XElement oUseEnhPolFlag = oMessageElement.XPathSelectElement("//UseEnhPolFlag");
            XElement oIsPolicyFilter = oMessageElement.XPathSelectElement("//IsPolicyFilter");

            if (oUseEnhPolFlag != null)
            {
                if (oUseEnhPolFlag.Value == "-1")
                {
                    //If Enhanced Policy is activated , set TextBox as readonly
                    oControl = this.FindControl("vehvin");
                    
                    if (oControl != null)
                    {
                        // npadhy Start MITS 22085 Readonly powerview used to Crash as TextBox Control was not Present
                        if (string.Compare(AppHelper.GetControlType(oControl.GetType().ToString()), "TextBox", true) == 0)
                        {
                            ((TextBox)oControl).Attributes.Add("readonly", "readonly");
                            ((TextBox)oControl).BackColor = System.Drawing.Color.FromName("#f2f2f2");
                        }
                        // npadhy End MITS 22085 Readonly powerview used to Crash as TextBox Control was not Present
                    }

                    //If Policy Filter is also checked , Modify Button OnClientClick event
                    if (oIsPolicyFilter != null)
                    {
                        if (oIsPolicyFilter.Value == "-1")
                        {
                            oControl = this.FindControl("vehvinbtn");
                            if (oControl != null)
                            {
                                ((Button)oControl).OnClientClick = "return UnitSelect();";
                                ((Button)oControl).CssClass = "CodeLookupControl";
                            }
                        }
                    }
                }
            }

          
        }
        //smahajan6 - MITS #18644 - 11/18/2009 : End
     
        //Code changes for Performance 14.1 Start
        /// <summary>
        /// 
        /// </summary>
        /// <param name="EntityId"></param>
        /// <param name="LookUpType"></param>
        /// <param name="FormName"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string GetDataForSelectedEntity(string EntityId, string LookUpType, string FormName)
        {
            string sReturn = string.Empty;
            XElement oMessageElement = null;
            XElement oSessionCmdElement = null;

            oMessageElement = GetMessageTemplate();

            if (!string.IsNullOrEmpty(EntityId))
            {              
                oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/EntityTableId");
                oSessionCmdElement.Value = EntityId;
            }
            if (!string.IsNullOrEmpty(LookUpType))
            {
                oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/LookUpType");
                oSessionCmdElement.Value = LookUpType;
            }
            if (!string.IsNullOrEmpty(FormName))
            {
                oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/FormName");
                oSessionCmdElement.Value = FormName;
            }
            if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("SessionId")))
            {
                oSessionCmdElement = oMessageElement.XPathSelectElement("./Authorization");
                oSessionCmdElement.Value = AppHelper.ReadCookieValue("SessionId");
            }
            sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            return sReturn;
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private static XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                    <Call>
                        <Function>SearchAdaptor.GetSearchResultsForSelectedEntity</Function> 
                    </Call>
                    <Document>
                        <GetEntityData>
                            <EntityTableId></EntityTableId> 
                            <Type></Type> 
                            <LookUpType></LookUpType> 
                            <FormName></FormName> 
                        </GetEntityData>
                    </Document>
            </Message>
            ");

            return oTemplate;
        }
        //Code changes for Performance 14.1 End
    }
}
