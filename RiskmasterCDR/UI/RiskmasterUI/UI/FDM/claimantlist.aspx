﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="claimantlist.aspx.cs" Inherits="Riskmaster.UI.FDM.ClaimantList"  ValidateRequest="false" EnableViewStateMac="false"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" src="Scripts/drift.js"></script> 
	<script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
  <script type="text/javascript" src="../../Scripts/pagingsorting.js"></script> 
</head>
<body class="10pt" onload="pageLoaded();">
    <form id="frmData" runat="server">    
    <div>
     <asp:Label ID="lblError" runat="server" Text="" ForeColor=Red></asp:Label>
            <h2>Claimants for Claim: <%=rootElement.XPathSelectElement("//ClaimNumber").Value%></h2>
                          	<table border="0" width="100%" cellspacing="0" cellpadding="2" id="tbllist">
                          <tr>
                            <td class="ctrlgroup" width="50px" />
                            <td class="ctrlgroup"><a href="#"  id="HeaderColumn1" class="ctrlgroup" onclick="SortList(document.all.tbllist,1,'lnkClaimant',true );">Claimant Name</a></td>                         
                            <td class="ctrlgroup" nowrap="1"><a href="#"  id="HeaderColumn2" class="ctrlgroup" onclick="SortList(document.all.tbllist,2,'lnkClaimant',true );">Claimant Type</a></td>
                            <td class="ctrlgroup" nowrap="1"><a href="#"  id="HeaderColumn3" class="ctrlgroup" onclick="SortList(document.all.tbllist,3,'lnkClaimant',true );">Primary Claimant</a></td>
                          </tr>
                          
                          <%int i = 0; foreach (XElement item in result)
                            {
                                string rowclass = "";
                                if ((i % 2) == 1) rowclass = "datatd1";
                                else rowclass = "datatd";
                                i++;
                                %>
                                
                          <tr>                          
                                 <td class="<%=rowclass%>">                              
                                <table border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                  <td class="<%=rowclass%>">
                                   <%
                                  if (sShowDeleteButton)
                                    {%>
                                  <%
                                      PageId.ID = "PageId" + i;
                                      PageId.Value = item.Element("ClaimantRowId").Value;
                                
                                 %>
                                    <input type ="checkbox" name="PageId" id="PageId"  title="Claimant List" runat="server"/>
                                    <%} %>
                                    </td>                       
                                  </tr>
                                </table>
                              </td>                                                            
                            <td class="<%=rowclass%>" nowrap="1">     
                            <%
                                lnkClaimant.ID = "lnkClaimant" + i;
                                lnkClaimant.Text = item.Element("ClaimantEid").Value;
                              lnkClaimant.PostBackUrl = "claimant.aspx?SysFormName=claimant&SysCmd=0&SysViewType=controlsonly&SysFormIdName=claimantrowid&SysFormId=" + item.Element("ClaimantRowId").Value + "&SysFormPIdName=claimid&SysEx=ClaimId&SysExMapCtl=ClaimId";%>                                                                                                                                                                             
                             <asp:LinkButton runat="server" id="lnkClaimant"></asp:LinkButton>                               
                            </td>
                           <td class="<%=rowclass%>">
                            <%=item.Element("ClaimantTypeCode").Value%>
                          </td>
                               <td class="<%=rowclass%>">
                            <%=item.Element("PrimaryClmntFlag").Value%>
                          </td> 
                          <td style="display:none">                           
                            </td>
                            <td style="display:none">                           
                               <%=lnkClaimant.PostBackUrl%>
                            </td>    
                            <td style="display:none">
                              <%=item.Element("ClaimantRowId").Value%>
                            </td>                                                
                          </tr>                          
                            <% }%>                      
                       
                    </table>
                        <center><div id="pageNavPosition"></div></center> 
                        <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
                        <asp:TextBox runat="server" id="TextBox1" style="display:none" />
                        <asp:TextBox runat="server" id="DeleteListTemplate"  style="display:none" Text="Instance/ClaimantList/Claimant/ClaimantRowId"/>					
						<asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
						<asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
						<asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
						<asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
						<asp:TextBox runat="server" id="SysClassName" style="display:none" Text="ClaimList" RMXRef="Instance/UI/FormVariables/SysClassName"/>						
						<asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" Text="&lt;ClaimantList&gt;&lt;Claimant/&gt;&lt;/ClaimantList&gt;"	RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>	
						<asp:TextBox runat="server" id="SysSerializationConfigIgnore" style="display:none" Text="Claim.Comments"	RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore"/>						                             
                        <asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm"/>
						<asp:TextBox runat="server" id="SysFormPIdName" Text="claimid" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPIdName"/>
						<asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
						<asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
						<asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>
						<asp:TextBox runat="server" id="SysNotReqNew" style="display:none" Text="eventnumber|timeofevent|timereported"/>
						<asp:TextBox runat="server" id="SysFormName" style="display:none" Text="claimantlist" RMXRef="Instance/UI/FormVariables/SysFormName"/>
						<asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="claimid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
						<asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId"/>
						<asp:TextBox runat="server" id="SysSid" style="display:none" Text="11000" RMXRef="Instance/UI/FormVariables/SysSid"/>
						<asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
                        <asp:TextBox ID="paramstore" runat="server" style="display:none"></asp:TextBox>	
                        <asp:TextBox runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" style="display:none"/>
                        <asp:TextBox runat="server" id="claimid" RMXRef="Instance/UI/FormVariables/SysExData/ClaimId"   style="display:none"/>                       
                        <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none"/>
           <%if (sShowAddButton)
             {%>          
        <asp:button class="button" runat="server" id="btnAddClaimant" Text="Add Claimant" postbackurl="claimant.aspx?SysFormName=claimant&SysCmd=&SysViewType=controlsonly&SysFormIdName=claimantrowid&SysFormPIdName=claimid&SysEx=ClaimId|ClaimNumber&SysExMapCtl=ClaimId|ClaimNumber" />                           
        <%} %>
        <%if (sShowDeleteButton)
          {%>
        <asp:Button class="button" runat="server" ID="btnDelete" Text="Delete" OnClick="NavigateListDelete"  OnClientClick="return DeleteList();"/>
        <%} %>
        <asp:Button class="button" runat="server" ID="BackToParent" Text="Back to Claim" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>  
        <input type="text" runat="server" id="PageIds" style="display:none"/>
    </div>
	 <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
  </form>
  <script type="text/javascript">
        var listpaging = new ListPaging('tbllist', 3); 
        listpaging.init(); 
        listpaging.showPageNavigation('listpaging', 'pageNavPosition'); 
        listpaging.showPage(1);
    </script>       
</body>
</html>
