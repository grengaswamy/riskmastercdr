<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="policy.aspx.cs" Inherits="Riskmaster.UI.FDM.Policy"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Policy Tracking</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift">
                <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28"
                                height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                                src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                                validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28"
                                height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28"
                                height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28"
                                height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28"
                                height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                                width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="doLookup()" src="Images/lookup.gif"
                                width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='Images/lookup2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/lookup.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="attach()" src="Images/attach.gif"
                                width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                                onmouseover="this.src='Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/attach.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="doSearch()" src="Images/search.gif"
                                width="28" height="28" border="0" id="search" alternatetext="Search" onmouseover="this.src='Images/search2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/search.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="Diary()" src="Images/diary.gif" width="28"
                                height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='Images/diary2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/diary.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="Comments()" src="Images/comments.gif"
                                width="28" height="28" border="0" id="comments" alternatetext="Comments" onmouseover="this.src='Images/comments2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/comments.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="MailMerge()" src="Images/mailmerge.gif"
                                width="28" height="28" border="0" id="mailmerge" alternatetext="Mail Merge" onmouseover="this.src='Images/mailmerge2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/mailmerge.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                                width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                                onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" />
                        </td>
                    </tr>
                </table>
            </div>
    <br />
    <div class="msgheader" id="formtitle">
        Policy Tracking</div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0">
                    <tr>
                        <td class="Selected" nowrap="true" name="TABSpolicyinfo" id="TABSpolicyinfo">
                            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="policyinfo"
                                id="LINKTABSpolicyinfo"><span style="text-decoration: none">Policy Information</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSinsurerinfo" id="TABSinsurerinfo">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="insurerinfo"
                                id="LINKTABSinsurerinfo"><span style="text-decoration: none">Insurer Information</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSbrokerinfo" id="TABSbrokerinfo">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="brokerinfo"
                                id="LINKTABSbrokerinfo"><span style="text-decoration: none">Broker Information</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSsuppgroup" id="TABSsuppgroup">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td valign="top" nowrap="true" />
                    </tr>
                </table>
                <div style="position: relative; left: 0; top: 0; width: 870px; height: 350px; overflow: auto"
                    class="singletopborder">
                    <table border="0" cellspacing="0" celpadding="0" width="95%" height="95%">
                        <tr>
                            <td valign="top">
                                <table border="0" name="FORMTABpolicyinfo" id="FORMTABpolicyinfo">
                                    <tr>
                                        <asp:textbox style="display: none" runat="server" id="policyid" rmxref="/Instance/Policy/PolicyId" /><td
                                            nowrap="true" id="policyname_ctlcol" xmlns="">
                                            <b><u>Policy Name</u></b>
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="policyname"
                                                rmxref="/Instance/Policy/PolicyName" /><asp:requiredfieldvalidator validationgroup="vgSave"
                                                    errormessage="Required" runat="server" controltovalidate="policyname" />
                                        </td>
                                        <td nowrap="true" id="issuedate_ctlcol" xmlns="">
                                            Issue Date
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="issuedate" rmxref="/Instance/Policy/IssueDate" tabindex="9" /><cc1:CalendarExtender
                                                    runat="server" ID="issuedate_ajax" TargetControlID="issuedate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="policynumber_ctlcol" xmlns="">
                                            Policy Number
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="policynumber"
                                                rmxref="/Instance/Policy/PolicyNumber" />
                                        </td>
                                        <td nowrap="true" id="reviewdate_ctlcol" xmlns="">
                                            Review Date
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="reviewdate" rmxref="/Instance/Policy/ReviewDate" tabindex="11" /><cc1:CalendarExtender
                                                    runat="server" ID="reviewdate_ajax" TargetControlID="reviewdate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="policystatuscode_ctlcol" xmlns="">
                                            <b><u>Policy Status</u></b>
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="policystatuscode"
                                                rmxref="/Instance/Policy/PolicyStatusCode" cancelledvalue="" tabindex="3" onblur="codeLostFocus(this.id);" /><asp:button
                                                    class="CodeLookupControl" runat="Server" id="policystatuscodebtn" onclientclick="return selectCode('POLICY_STATUS','policystatuscode');"
                                                    tabindex="4" /><asp:textbox style="display: none" runat="server" id="policystatuscode_cid"
                                                        rmxref="/Instance/Policy/PolicyStatusCode/@codeid" cancelledvalue="" /><asp:requiredfieldvalidator
                                                            validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="policystatuscode" />
                                        </td>
                                        <td nowrap="true" id="renewaldate_ctlcol" xmlns="">
                                            Renewal Date
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="renewaldate" rmxref="/Instance/Policy/RenewalDate" tabindex="13" /><cc1:CalendarExtender
                                                    runat="server" ID="renewaldate_ajax" TargetControlID="renewaldate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="premium_ctlcol" xmlns="">
                                            Premium
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" id="premium" rmxref="/Instance/Policy/Premium"
                                                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="5" onchange=";setDataChanged(true);" />
                                        </td>
                                        <td nowrap="true" id="effectivedate_ctlcol" xmlns="">
                                            <b><u>Effective Date</u></b>
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="effectivedate" rmxref="/Instance/Policy/EffectiveDate" tabindex="15" /><asp:requiredfieldvalidator
                                                    validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="effectivedate" /><cc1:CalendarExtender
                                                        runat="server" ID="effectivedate_ajax" TargetControlID="effectivedate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="bankaccount_ctlcol" xmlns="">
                                            Bank Account
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:dropdownlist runat="server" id="bankaccount" tabindex="6" rmxref="/Instance/Policy/BankAccId"
                                                itemsetref="/Instance/UI/FormVariables/SysExData/AccountList" width="205" onchange="setDataChanged(true);" />
                                        </td>
                                        <td nowrap="true" id="expirationdate_ctlcol" xmlns="">
                                            <b><u>Expiration Date</u></b>
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="expirationdate" rmxref="/Instance/Policy/ExpirationDate" tabindex="17" /><asp:requiredfieldvalidator
                                                    validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="expirationdate" /><cc1:CalendarExtender
                                                        runat="server" ID="expirationdate_ajax" TargetControlID="expirationdate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="primarypolicyflg_ctlcol" xmlns="">
                                            Primary Policy
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="primarypolicyflg"
                                                rmxref="/Instance/Policy/PrimaryPolicyFlg" tabindex="7" />
                                        </td>
                                        <td nowrap="true" id="canceldate_ctlcol" xmlns="">
                                            Cancel Date
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="canceldate" rmxref="/Instance/Policy/CancelDate" tabindex="19" /><cc1:CalendarExtender
                                                    runat="server" ID="canceldate_ajax" TargetControlID="canceldate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="triggerclaimflag_ctlcol" xmlns="">
                                            Claims-Made Coverage
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="triggerclaimflag"
                                                rmxref="/Instance/Policy/TriggerClaimFlag" tabindex="8" />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" style="display: none;" name="FORMTABinsurerinfo" id="FORMTABinsurerinfo">
                                    <tr>
                                        <asp:textbox style="display: none" runat="server" id="insentityid" rmxref="/Instance/Policy/InsurerEid/@codeid" /><td
                                            nowrap="true" id="inslastname_ctlcol" xmlns="">
                                            <b><u>Insurer</u></b>
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" tabindex="21"
                                                onblur="lookupLostFocus(this);" rmxref="/Instance/Policy/InsurerEntity/LastName"
                                                id="inslastname" /><input type="button" class="button" tabindex="22" id="inslastnamebtn"
                                                    onclick="lookupDataFDM('inslastname','INSURERS',4,'ins',1,'/Instance/Policy/InsurerEntity')"
                                                    value="..." />
                                        </td>
                                        <td nowrap="true" id="insphone1_ctlcol" xmlns="">
                                            Office Phone
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="insphone1" rmxref="/Instance/Policy/InsurerEntity/Phone1"
                                                tabindex="32" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="inscontact_ctlcol" xmlns="">
                                            Contact
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="inscontact"
                                                rmxref="/Instance/Policy/InsurerEntity/Contact" />
                                        </td>
                                        <td nowrap="true" id="insphone2_ctlcol" xmlns="">
                                            Alt. Phone
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="insphone2" rmxref="/Instance/Policy/InsurerEntity/Phone2"
                                                tabindex="33" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="insaddr1_ctlcol" xmlns="">
                                            Address 1
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="insaddr1"
                                                rmxref="/Instance/Policy/InsurerEntity/Addr1" />
                                        </td>
                                        <td nowrap="true" id="insfaxnumber_ctlcol" xmlns="">
                                            Fax
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="insfaxnumber" rmxref="/Instance/Policy/InsurerEntity/FaxNumber"
                                                tabindex="34" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="insaddr2_ctlcol" xmlns="">
                                            Address 2
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="insaddr2"
                                                rmxref="/Instance/Policy/InsurerEntity/Addr2" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="inscity_ctlcol" xmlns="">
                                            City
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="inscity"
                                                rmxref="/Instance/Policy/InsurerEntity/City" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="insstateid_ctlcol" xmlns="">
                                            State
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="insstateid"
                                                rmxref="/Instance/Policy/InsurerEntity/StateId" cancelledvalue="" tabindex="27"
                                                onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl" runat="Server"
                                                    id="insstateidbtn" onclientclick="return selectCode('states','insstateid');"
                                                    tabindex="28" /><asp:textbox style="display: none" runat="server" id="insstateid_cid"
                                                        rmxref="/Instance/Policy/InsurerEntity/StateId/@codeid" cancelledvalue="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="inszipcode_ctlcol" xmlns="">
                                            Zip
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);"
                                                id="inszipcode" rmxref="/Instance/Policy/InsurerEntity/ZipCode" tabindex="29" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="inscountrycode_ctlcol" xmlns="">
                                            Country
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="inscountrycode"
                                                rmxref="/Instance/Policy/InsurerEntity/CountryCode" cancelledvalue="" tabindex="30"
                                                onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl" runat="Server"
                                                    id="inscountrycodebtn" onclientclick="return selectCode('COUNTRY','inscountrycode');"
                                                    tabindex="31" /><asp:textbox style="display: none" runat="server" id="inscountrycode_cid"
                                                        rmxref="/Instance/Policy/InsurerEntity/CountryCode/@codeid" cancelledvalue="" />
                                        </td>
                                        <td nowrap="true" id="insuredlist_ctlcol" xmlns="">
                                            Insured
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:listbox runat="server" size="3" id="insuredlist" rmxref="/Instance/Policy/PolicyXInsured"
                                                style="" tabindex="35" /><input type="button" id="insuredlistbtn" tabindex="36" onclick="lookupData('insuredlist','ORGANIZATIONS',4,'insuredlist',3)"
                                                    class=" button" value="..." /><input type="button" class="button" id="insuredlistbtndel"
                                                        tabindex="37" onclick="deleteSelCode('insuredlist')" value="-" /><asp:textbox runat="server"
                                                            style="display: none" rmxref="/Instance/Policy/PolicyXInsured/@codeid" id="insuredlist_lst" />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" style="display: none;" name="FORMTABbrokerinfo" id="FORMTABbrokerinfo">
                                    <tr>
                                        <asp:textbox style="display: none" runat="server" id="brkentityid" rmxref="/Instance/Policy/BrokerEid/@codeid" /><td
                                            nowrap="true" id="brklastname_ctlcol" xmlns="">
                                            Last Name
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" tabindex="38"
                                                onblur="lookupLostFocus(this);" rmxref="/Instance/Policy/BrokerEntity/LastName"
                                                id="brklastname" /><input type="button" class="button" tabindex="39" id="brklastnamebtn"
                                                    onclick="lookupDataFDM('brklastname','BROKER',4,'brk',1,'/Instance/Policy/BrokerEntity')"
                                                    value="..." />
                                        </td>
                                        <td nowrap="true" id="brkphone1_ctlcol" xmlns="">
                                            Office Phone
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="brkphone1" rmxref="/Instance/Policy/BrokerEntity/Phone1"
                                                tabindex="49" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="brkfirstname_ctlcol" xmlns="">
                                            First Name
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="brkfirstname"
                                                rmxref="/Instance/Policy/BrokerEntity/FirstName" />
                                        </td>
                                        <td nowrap="true" id="brkphone2_ctlcol" xmlns="">
                                            Alt. Phone
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="brkphone2" rmxref="/Instance/Policy/BrokerEntity/Phone2"
                                                tabindex="50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="brkaddr1_ctlcol" xmlns="">
                                            Address 1
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="brkaddr1"
                                                rmxref="/Instance/Policy/BrokerEntity/Addr1" />
                                        </td>
                                        <td nowrap="true" id="brkfaxnumber_ctlcol" xmlns="">
                                            Fax
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="brkfaxnumber" rmxref="/Instance/Policy/BrokerEntity/FaxNumber"
                                                tabindex="51" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="brkaddr2_ctlcol" xmlns="">
                                            Address 2
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="brkaddr2"
                                                rmxref="/Instance/Policy/BrokerEntity/Addr2" />
                                        </td>
                                        <td nowrap="true" id="brktaxid_ctlcol" xmlns="">
                                            Tax ID
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="ssnLostFocus(this);"
                                                id="brktaxid" rmxref="/Instance/Policy/BrokerEntity/TaxId" name="brktaxid" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="brkcity_ctlcol" xmlns="">
                                            City
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="brkcity"
                                                rmxref="/Instance/Policy/BrokerEntity/City" />
                                        </td>
                                        <td nowrap="true" id="brkparenteid_ctlcol" xmlns="">
                                            Broker Firm
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                                                id="brkparenteid" rmxref="/Instance/Policy/BrokerEntity/ParentEid" cancelledvalue=""
                                                tabindex="53" /><input type="button" class="button" value="..." name="brkparenteidbtn"
                                                    tabindex="54" onclick="lookupData('brkparenteid','BROKER_FIRM',4,'brkparenteid',2)" /><asp:textbox
                                                        style="display: none" runat="server" id="brkparenteid_cid" rmxref="/Instance/Policy/BrokerEntity/ParentEid/@codeid"
                                                        cancelledvalue="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="brkstateid_ctlcol" xmlns="">
                                            State
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="brkstateid"
                                                rmxref="/Instance/Policy/BrokerEntity/StateId" cancelledvalue="" tabindex="44"
                                                onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl" runat="Server"
                                                    id="brkstateidbtn" onclientclick="return selectCode('states','brkstateid');"
                                                    tabindex="45" /><asp:textbox style="display: none" runat="server" id="brkstateid_cid"
                                                        rmxref="/Instance/Policy/BrokerEntity/StateId/@codeid" cancelledvalue="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="brkzipcode_ctlcol" xmlns="">
                                            Zip
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);"
                                                id="brkzipcode" rmxref="/Instance/Policy/BrokerEntity/ZipCode" tabindex="46" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="brkcountrycode_ctlcol" xmlns="">
                                            Country
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="brkcountrycode"
                                                rmxref="/Instance/Policy/BrokerEntity/CountryCode" cancelledvalue="" tabindex="47"
                                                onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl" runat="Server"
                                                    id="brkcountrycodebtn" onclientclick="return selectCode('COUNTRY','brkcountrycode');"
                                                    tabindex="48" /><asp:textbox style="display: none" runat="server" id="brkcountrycode_cid"
                                                        rmxref="/Instance/Policy/BrokerEntity/CountryCode/@codeid" cancelledvalue="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="hdnPostBackAction_ctlcol" xmlns="" />
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox style="display: none" runat="server" id="hdnPostBackAction" rmxref="/Instance/UI/FormVariables/SysExData/PostBackAction" />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" style="display: none;" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
                                    <tr>
                                        <asp:textbox style="display: none" runat="server" id="supp_policy_id" rmxref="/Instance/*/Supplementals/POLICY_ID" /></tr>
                                </table>
                            </td>
                            <td valign="top" />
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td>
                            <asp:button id="Button1" class="button" runat="Server" text="Back " onclientclick="&#xA;            XFormHandler('','','','back')&#xA;          " /><asp:button
                                id="Button2" class="button" runat="Server" text="Policy Coverages" onclientclick="&#xA;            XFormHandler('','SysFormName=policycoverage&syscmd=1&SysFormIdName=polcvgrowid&SysEx=/Instance/Policy/PolicyId','','')&#xA;          " /><asp:button
                                    id="Button3" class="button" runat="Server" text="Policy MCO" onclientclick="&#xA;            XFormHandler('','SysFormName=policymco&syscmd=1&SysFilter=0&SysFormIdName=origvalues&SysEx=/Instance/Policy/PolicyId','','')&#xA;          " /><asp:button
                                        id="Button4" class="button" runat="Server" text="Insurer" onclientclick="&#xA;            XFormHandler('','SysFormName=policyinsurer&syscmd=1&SysEx=/Instance/Policy/PolicyId','','')&#xA;          " /><script
                                            language="JavaScript" src="">{var i;}
                                        </script><asp:button class="button" runat="server" id="btnClonePolicy" rmxref=""
                                            text="Clone Policy" onClientClick="ClonePolicy1" onclick="ClonePolicy" />
                        </td>
                    </tr>
                </table>
                <asp:textbox style="display: none" runat="server" id="AddedByUser" rmxref="Instance/Policy/AddedByUser"
                    text="" /><asp:textbox style="display: none" runat="server" id="DttmRcdAdded" rmxref="Instance/Policy/DttmRcdAdded"
                        text="" /><asp:textbox style="display: none" runat="server" id="UpdatedByUser" rmxref="Instance/Policy/UpdatedByUser"
                            text="" /><asp:textbox style="display: none" runat="server" id="DttmRcdLastUpd" rmxref="Instance/Policy/DttmRcdLastUpd"
                                text="" /><asp:textbox style="display: none" runat="server" id="SubAccRowId" rmxref="Instance/Policy/SubAccRowId"
                                    text="" /><asp:textbox style="display: none" runat="server" id="EnteredComments"
                                        rmxref="Instance/Policy/Comments" text="" /><asp:textbox style="display: none" runat="server"
                                            id="HTMLComments" rmxref="Instance/Policy/HTMLComments" text="" /><asp:textbox style="display: none"
                                                runat="server" id="Parent" rmxref="Instance/Policy/Parent" text="" /><asp:textbox
                                                    style="display: none" runat="server" id="Insurer_EntityId" rmxref="Instance/Policy/InsurerEntity/EntityId"
                                                    text="" /><asp:textbox style="display: none" runat="server" id="Insurer_EntityTableId"
                                                        rmxref="Instance/Policy/InsurerEntity/EntityTableId" text="" /><asp:textbox style="display: none"
                                                            runat="server" id="Broker_EntityId" rmxref="Instance/Policy/BrokerEntity/EntityId"
                                                            text="" /><asp:textbox style="display: none" runat="server" id="Broker_EntityTableId"
                                                                rmxref="Instance/Policy/BrokerEntity/EntityTableId" text="" /><asp:textbox style="display: none"
                                                                    runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
                                                                    text="" /><asp:textbox style="display: none" runat="server" id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName"
                                                                        text="" /><asp:textbox style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
                                                                            text="" /><asp:textbox style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
                                                                                text="" /><asp:textbox style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                                                                    text="" /><asp:textbox style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                                                                        text="policy" /><asp:textbox style="display: none" runat="server" id="SysFormIdName"
                                                                                            rmxref="Instance/UI/FormVariables/SysFormIdName" text="policyid" /><asp:textbox style="display: none"
                                                                                                runat="server" id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId" text="" /><asp:textbox
                                                                                                    style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                                                                                    text="9000" /><asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
                                                                                                        text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
                                                                                                            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" text="" /><asp:textbox style="display: none"
                                                                                                                runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                                                                                                                text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText" rmxref="Instance/UI/FormVariables/SysCmdText"
                                                                                                                    text="Navigate" /><asp:textbox style="display: none" runat="server" id="SysClassName"
                                                                                                                        rmxref="Instance/UI/FormVariables/SysClassName" text="Policy" /><asp:textbox style="display: none"
                                                                                                                            runat="server" id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                                                                                                                            name="SysSerializationConfig" text="&lt;Policy&gt;&lt;BrokerEntity&gt;&lt;/BrokerEntity&gt;&lt;InsurerEntity&gt;&lt;/InsurerEntity&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/Policy&gt;" /><asp:textbox
                                                                                                                                style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                                                                                                                text="" /><asp:textbox style="display: none" runat="server" name="formname" value="policy" /><asp:textbox
                                                                                                                                    style="display: none" runat="server" name="SysRequired" value="policyname|policystatuscode_cid|effectivedate|expirationdate|inslastname|" /><asp:textbox
                                                                                                                                        style="display: none" runat="server" name="SysFocusFields" value="policyname|inslastname|brklastname|" /><input
                                                                                                                                            type="hidden" id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible"
                                                                                                                                                style="display: none" /><asp:textbox runat="server" id="SysLookupClass" style="display: none" /><asp:textbox
                                                                                                                                                    runat="server" id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server"
                                                                                                                                                        id="SysLookupAttachNodePath" style="display: none" /><asp:textbox runat="server"
                                                                                                                                                            id="SysLookupResultConfig" style="display: none" /><input type="hidden" value="rmx-widget-handle-2"
                                                                                                                                                                id="SysWindowId" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
