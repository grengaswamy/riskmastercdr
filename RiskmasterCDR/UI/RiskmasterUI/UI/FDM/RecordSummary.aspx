﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecordSummary.aspx.cs" Inherits="Riskmaster.UI.FDM.RecordSummary" %>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3"  %>
<head id="Head1" runat=server></head>
<%=Display %>
<table>

<tr>
       <td>
          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
      </td>
      </tr>
</table>
<script type="text/javascript">
    //MITS 27964 hlv 4/10/2012 begin
    if (window.opener != null) {
        window.opener.window.parent.parent.iwintype = "RecordSummary";
    }

    window.onunload = function () {
        if (window.opener != null) {
            window.opener.window.parent.parent.iwintype = "";
        }
    }
    //MITS 27964 hlv 4/10/2012 end
</script>