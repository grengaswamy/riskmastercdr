﻿using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using Riskmaster.UI.Shared.Controls;

//Author    :   Anu Tennyson
//Date      :   12/20/09
//MITS      :   MITS:18229
//Comments  :   Replica of exposure.aspx.cs. Now it is tied to LOB.
namespace Riskmaster.UI.FDM
{
    public partial class Exposurewc : NonFDMBasePageCWS
    {
        private const string m_FUNCTION_CALL_GET_RATE = "EnhancePolicyAdaptor.GetRate";

        private const string m_sCodeLookup = "codelookup";
        private const string m_sMultiCode = "multicode";
        private const string m_sUserName = "rmsyslookup";
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument objRateDetails = null;
            string sreturnValue = "";
            if (!IsPostBack)
            {
                // Read the Values from Querystring
                int iPolicyId = 0;
                if (AppHelper.GetQueryStringValue("policyid") != "")
                {
                    iPolicyId = Int32.Parse(AppHelper.GetQueryStringValue("policyid"));
                }

                int iExposureId = 0;
                if (AppHelper.GetQueryStringValue("selectedid") != "")
                {
                    iExposureId = Int32.Parse(AppHelper.GetQueryStringValue("selectedid"));
                }

                string sSessionId = string.Empty;
                if (AppHelper.GetQueryStringValue("sessionid") != "")
                {
                    sSessionId = AppHelper.GetQueryStringValue("sessionid");
                }
                int iTransId = 0;
                if (AppHelper.GetQueryStringValue("selectedtransactionid") != "")
                {
                    iTransId = Int32.Parse(AppHelper.GetQueryStringValue("selectedtransactionid"));
                }
                string sEffectiveDate = AppHelper.GetQueryStringValue("effectivedate");
                string sExpirationDate = AppHelper.GetQueryStringValue("expirationdate");

                int iStateId = 0;
                if (AppHelper.GetQueryStringValue("state") != "")
                {
                    iStateId = Int32.Parse(AppHelper.GetQueryStringValue("state"));
                }
                //Anu Tennyson For MITS 18229 Starts
                string sFormName = string.Empty;
                if (!(string.IsNullOrEmpty(AppHelper.GetQueryStringValue("sysformname"))))
                {
                    sFormName = AppHelper.GetQueryStringValue("sysformname");
                }
                string sOrgH = string.Empty;
                if (!(string.IsNullOrEmpty(AppHelper.GetQueryStringValue("sOrg"))))
                {
                    sOrgH = AppHelper.GetQueryStringValue("sOrg");
                }
                //Anu Tennyson For MITS 18229 Ends

                /// These Controls are used when Set Coverage is Called. 
                // These controls should be different then the Controls meant for display or for Coverages.
                // Like when a Quote is saved though the DB has a Transaction Id Saved,but there will not be any record for it.
                TextBox txtPolicyId = (TextBox)this.FindControl("PolId");
                if (txtPolicyId != null)
                {
                    txtPolicyId.Text = iPolicyId.ToString();
                }
                TextBox txtTransId = (TextBox)this.FindControl("TransId");
                if (txtTransId != null)
                {
                    txtTransId.Text = iTransId.ToString();
                }
                TextBox txtEffectiveDate = (TextBox)this.FindControl("EffDate");
                if (txtEffectiveDate != null)
                {
                    txtEffectiveDate.Text = sEffectiveDate;
                }
                TextBox txtExpirationDate = (TextBox)this.FindControl("ExpDate");
                if (txtExpirationDate != null)
                {
                    txtExpirationDate.Text = sExpirationDate;
                }

                TextBox txtGridName = (TextBox)this.FindControl("gridname");
                if (txtGridName != null)
                {
                    txtGridName.Text = AppHelper.GetQueryStringValue("gridname");
                }
                TextBox txtmode = (TextBox)this.FindControl("mode");
                if (txtmode != null)
                {
                    txtmode.Text = AppHelper.GetQueryStringValue("mode");
                }
                TextBox txtselectedrowposition = (TextBox)this.FindControl("selectedrowposition");
                if (txtselectedrowposition != null)
                {
                    txtselectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                }
                TextBox txtExposureId = (TextBox)this.FindControl("selectedid");
                if (txtExposureId != null)
                {
                    txtExposureId.Text = iExposureId.ToString();
                }
                //Anu Tennyson For MITS 18229 Starts will be changed by summit
                XmlTemplate = GetLoadMessageTemplate(iExposureId, sSessionId, iPolicyId, iTransId, sEffectiveDate, sExpirationDate, iStateId);
                //XmlTemplate = GetLoadMessageTemplate(iExposureId, sSessionId, iPolicyId, iTransId, sEffectiveDate, sExpirationDate, iStateId, sFormName);
                //Anu Tennyson For MITS 18229 Ends
                bReturnStatus = CallCWS("EnhancePolicyAdaptor.GetExposure", XmlTemplate, out sreturnValue, false, true);
            }
            else
            {
                // Get the Function Name
                TextBox txtFunctionToCall = (TextBox)this.FindControl("FunctionToCall");
                if (txtFunctionToCall != null)
                {
                    if (txtFunctionToCall.Text == m_FUNCTION_CALL_GET_RATE)
                    {
                        XmlTemplate = null;
                        bReturnStatus = CallCWS(m_FUNCTION_CALL_GET_RATE, XmlTemplate, out sreturnValue, true, false);

                        objRateDetails = new XmlDocument();
                        objRateDetails.LoadXml(sreturnValue);

                        // Set the Value of Controls
                        // Rate Value
                        TextBox txtRate = (TextBox)this.FindControl("Rate");
                        if (txtRate != null)
                        {
                            txtRate.Text = objRateDetails.SelectSingleNode("//RateDetails/Rate").InnerText;
                        }
                        // Base Rate
                        TextBox txtBaseRate = (TextBox)this.FindControl("BaseRate");
                        if (txtBaseRate != null)
                        {
                            txtBaseRate.Text = objRateDetails.SelectSingleNode("//RateDetails/BaseRate").InnerText;
                        }
                        // Premium Adjustment
                        MultiCurrencyCustomControl.CurrencyTextbox txtPremiumAdjustment = (MultiCurrencyCustomControl.CurrencyTextbox)this.FindControl("PremiumAdjustment");
                        if (txtPremiumAdjustment != null)
                        {
                            txtPremiumAdjustment.AmountInString = (objRateDetails.SelectSingleNode("//RateDetails/PremAdj").InnerText);
                        }
                        // Pro Rata Annual Premium
                        MultiCurrencyCustomControl.CurrencyTextbox txtProRataAnnualPremium = (MultiCurrencyCustomControl.CurrencyTextbox)this.FindControl("ProRataAnnualPremium");
                        if (txtProRataAnnualPremium != null)
                        {
                            txtProRataAnnualPremium.AmountInString = (objRateDetails.SelectSingleNode("//RateDetails/ProRataAnnualPrem").InnerText);
                        }
                        // Full Annual Premium
                        MultiCurrencyCustomControl.CurrencyTextbox txtFullAnnualPremium = (MultiCurrencyCustomControl.CurrencyTextbox)this.FindControl("FullAnnualPremium");
                        if (txtFullAnnualPremium != null)
                        {
                            txtFullAnnualPremium.AmountInString = (objRateDetails.SelectSingleNode("//RateDetails/FullAnnualPrem").InnerText);
                        }

                        // Date Amended
                        TextBox txtDateAmended = (TextBox)this.FindControl("DateAmended");
                        if (txtDateAmended != null)
                        {
                            txtDateAmended.Text = Request.Form["DateAmended"];
                        }

                    }
                }
            }

        }
        /// <summary>
        /// Exposure button click event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnExposureOk_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = "";
            TextBox txtSessionId;
            bReturnStatus = CallCWS("EnhancePolicyAdaptor.SetExposure", XmlTemplate, out sreturnValue, true, false);
            if (bReturnStatus)
            {
                // Set the Session Id
                XmlDoc.LoadXml(sreturnValue);
                // npadhy Start MITS 16357 Check if Error is returned from the Business, then We will not get the SessionId
                if (ErrorHelper.IsCWSCallSuccess(sreturnValue))
                {
                    // Set the Session Id
                    txtSessionId = (TextBox)this.FindControl("SessionId");
                    txtSessionId.Text = XmlDoc.SelectSingleNode("//Document/SetExposure/SessionId").InnerText;

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ExposureOkClick", "<script>fnRefreshParentExposureOk();</script>");
                }
                // npadhy End MITS 16357 Check if Error is returned from the Business, then We will not get the SessionId
            }
        }
        /// <summary>
        /// Method to load the message template
        /// </summary>
        /// <param name="iExposureId">Exposure Id</param>
        /// <param name="sSessionId">Session Id</param>
        /// <param name="iPolicyId">Policy Id</param>
        /// <param name="iTransId">Policy Transaction Id</param>
        /// <param name="sEffectiveDate">Effective Date</param>
        /// <param name="sExpirationDate">Expiration Date</param>
        /// <param name="iStateId">State Id</param>
        /// <returns>The message template</returns>
        private XElement GetLoadMessageTemplate(int iExposureId, string sSessionId, int iPolicyId, int iTransId, string sEffectiveDate, string sExpirationDate, int iStateId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.GetExposure</Function></Call><Document><Exposure><ExposureId>");
            sXml = sXml.Append(iExposureId);
            sXml = sXml.Append("</ExposureId><SessionId>");
            sXml = sXml.Append(sSessionId);
            sXml = sXml.Append("</SessionId><PolId >");
            sXml = sXml.Append(iPolicyId);
            sXml = sXml.Append("</PolId><TransId>");
            sXml = sXml.Append(iTransId);
            sXml = sXml.Append("</TransId><ExpirationDate>");
            sXml = sXml.Append(sExpirationDate);
            sXml = sXml.Append("</ExpirationDate><EffectiveDate>");
            sXml = sXml.Append(sEffectiveDate);
            sXml = sXml.Append("</EffectiveDate><State>");
            sXml = sXml.Append(iStateId);
            sXml = sXml.Append("</State></Exposure></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;


        }
        /// <summary>
        /// Override method to Modify Control.
        /// </summary>
        /// <param name="Xelement">The xml input</param>
        public override void ModifyControl(XElement Xelement)
        {
            IEnumerable<XElement> suppList = Xelement.XPathSelectElements("./Exposure/SuppView/group/displaycolumn/*");

            foreach (XElement xSupp in suppList)
            {
                string sRef = xSupp.Attribute("ref").Value;
                string sControlName = xSupp.Attribute("name").Value;
                string sOnChange = xSupp.Attribute("onchange").Value;


                Control oCtrl = (Control)this.FindControl(sControlName);
                if (oCtrl != null)
                {
                    if (oCtrl is WebControl)
                    {
                        AssignAttributes(oCtrl, "", xSupp);
                    }
                    else if (oCtrl is CodeLookUp)
                    {
                        AssignAttributes(oCtrl, m_sCodeLookup, xSupp);
                    }

                    else if (oCtrl is MultiCodeLookUp)
                    {
                        AssignAttributes(oCtrl, m_sMultiCode, xSupp);
                    }
                    else if (oCtrl is SystemUsers)
                    {
                        AssignAttributes(oCtrl, m_sUserName, xSupp);
                    }
                }

            }

        }

        /// <summary>
        /// Method to Assign Attributes
        /// </summary>
        /// <param name="p_oCtrl">Control to assign attributes</param>
        /// <param name="p_sExtender">Extender string</param>
        /// <param name="p_xSupp">Supp xml</param>
        private void AssignAttributes(Control p_oCtrl, string p_sExtender, XElement p_xSupp)
        {
            string sRef = p_xSupp.Attribute("ref").Value;
            string sControlName = p_xSupp.Attribute("name").Value;
            string sOnChange = p_xSupp.Attribute("onchange").Value;
            string sType = p_xSupp.Attribute("type").Value;
            string sControlId = string.Empty;
            WebControl oWebctrl;
            Control oCtrl;
            if (p_sExtender == "")
            {
                oWebctrl = (WebControl)p_oCtrl;
                oWebctrl.Attributes["RMXRef"] = sRef;
                oWebctrl.Attributes["onchange"] = sOnChange;

                if (sType == "eidlookup")
                {
                    sControlName = p_oCtrl.ID + "_cid";
                    oCtrl = (Control)this.FindControl(sControlName);
                    {
                        oWebctrl = (WebControl)oCtrl;
                        if (oWebctrl != null)
                        {
                            oWebctrl.Attributes["RMXRef"] = sRef + "/@codeid";
                        }
                    }
                }
                if (sType == "entitylist")
                {
                    sControlName = p_oCtrl.ID + "_lst";
                    oCtrl = (Control)this.FindControl(sControlName);
                    {
                        oWebctrl = (WebControl)oCtrl;
                        if (oWebctrl != null)
                        {
                            oWebctrl.Attributes["RMXRef"] = sRef + "/@codeid";
                        }
                    }
                }
            }
            else
            {
                oWebctrl = (WebControl)p_oCtrl.FindControl(p_sExtender);
                if (oWebctrl != null)
                {
                    oWebctrl.Attributes["RMXRef"] = sRef;
                    oWebctrl.Attributes["onchange"] = sOnChange;
                    if (p_sExtender == m_sMultiCode)
                    {
                        // Add the Item SetRef attribute with the Listbox
                        if (oWebctrl.Attributes["ItemSetRef"] == null)
                            oWebctrl.Attributes.Add("ItemSetRef", sRef);
                        else
                            oWebctrl.Attributes["ItemSetRef"] = sRef;
                    }
                }

                sControlId = GetControlName(p_sExtender);

                if (sControlId != string.Empty)
                {
                    oWebctrl = (WebControl)p_oCtrl.FindControl(sControlId);
                    if (oWebctrl != null)
                    {
                        oWebctrl.Attributes["RMXRef"] = sRef + "/@codeid";
                    }
                }
                else
                {
                    throw new Exception(p_sExtender + " Type of Supplemental field is not implemented in coverages");
                }
            }
        }
        /// <summary>
        /// Gets the Control Name
        /// </summary>
        /// <param name="p_sExtender">Extender String</param>
        /// <returns>Control Name</returns>
        private string GetControlName(string p_sExtender)
        {
            string sControlName = string.Empty;
            switch (p_sExtender)
            {
                case m_sMultiCode:
                    sControlName = p_sExtender + "_lst";
                    break;
                case m_sUserName:
                case m_sCodeLookup:
                    sControlName = p_sExtender + "_cid";
                    break;
                default:
                    sControlName = "";
                    break;
            }
            return sControlName;
        }
        /// <summary>
        /// Modifies the XML
        /// </summary>
        /// <param name="Xelement">The XMl to modify</param>
        public override void ModifyXml(ref XElement Xelement)
        {
            TextBox txtFunctionToCall = (TextBox)this.FindControl("FunctionToCall");

            if (txtFunctionToCall.Text == m_FUNCTION_CALL_GET_RATE)
            {
                XElement xmlExposure = Xelement.XPathSelectElement("./Document/Exposure");
                XElement xmlTarget = new XElement("State");
                int iStateId = 0;
                if (AppHelper.GetQueryStringValue("state") != "")
                {
                    iStateId = Int32.Parse(AppHelper.GetQueryStringValue("state"));
                }
                xmlTarget.Value = iStateId.ToString();
                if (xmlExposure != null)
                {
                    xmlExposure.Add(xmlTarget);
                }
                //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                XElement xmlPolicyTypeTarget = new XElement("PolicyType");
                xmlPolicyTypeTarget.Value = "WC";
                if (xmlExposure != null)
                {
                    xmlExposure.Add(xmlPolicyTypeTarget);
                }
                //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                //Anu Tennyson for MITS 18229 : Prem Calc STARTS
                XElement xmlOrg = new XElement("OrgHierarchy");
                string sOrg = string.Empty;
                if (AppHelper.GetQueryStringValue("sOrg") != "")
                {
                    sOrg = AppHelper.GetQueryStringValue("sOrg");
                }
                xmlOrg.Value = sOrg;
                if (xmlExposure != null)
                {
                    xmlExposure.Add(xmlOrg);
                }
                //Anu Tennyson for MITS 18229 : Prem Calc ENDS
            }
        }
    }
}
