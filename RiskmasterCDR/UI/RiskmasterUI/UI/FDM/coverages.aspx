<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="coverages.aspx.cs"  Inherits="Riskmaster.UI.FDM.Coverages" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Coverages</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <script language="JavaScript" src="../../Scripts/EnhPolicy.js">{var i;}</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="EnhancePolicyCoveragesOnLoad();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Coverages" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="/Instance/Document/selectedid" RMXType="id" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="/Instance/Document/mode" RMXType="id" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="/Instance/Document/selectedrowposition" RMXType="id" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="/Instance/Document/gridname" RMXType="id" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" rmxignorevalue="true" xmlns:cul="remove" />
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSCoverages" id="TABSCoverages">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="Coverages" id="LINKTABSCoverages">Coverages</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPCoverages">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABCoverages" id="FORMTABCoverages">
        <div runat="server" class="completerow" id="div_CoveragesType" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_CoveragesType" Text="Coverages Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="CoveragesType" CodeTable="COVERAGE_TYPE" ControlName="CoveragesType" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/CoverageTypeCode" RMXType="code" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_CoveragesStatus" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_CoveragesStatus" Text="Coverages Status" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="CoveragesStatus" CodeTable="COVEXP_STATUS" ControlName="CoveragesStatus" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/Status" RMXType="code" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_EffectiveDate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_EffectiveDate" Text="Effective Date" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="EffectiveDate" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/EffectiveDate" RMXType="date" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <asp:button class="DateLookupControl" runat="server" id="EffectiveDatebtn" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "EffectiveDate",
					ifFormat : "%m/%d/%Y",
					button : "EffectiveDatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="completerow" id="div_ExpirationDate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ExpirationDate" Text="Expiration Date" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="ExpirationDate" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/ExpirationDate" RMXType="date" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <asp:button class="DateLookupControl" runat="server" id="ExpirationDatebtn" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "ExpirationDate",
					ifFormat : "%m/%d/%Y",
					button : "ExpirationDatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="completerow" id="div_PolicyLimit" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_PolicyLimit" Text="Policy Limit" />
          <span class="formw">
            <asp:TextBox runat="server" id="PolicyLimit" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/PolicyLimit" RMXType="currency" onblur="CheckNegative(this);;currencyLostFocus(this);" rmxforms:as="currency" onchange="CoveragesEnableOk('EnableAmend',this);;setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_OccurenceLimit" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_OccurenceLimit" Text="Occurence Limit" />
          <span class="formw">
            <asp:TextBox runat="server" id="OccurenceLimit" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/OccurrenceLimit" RMXType="currency" onblur="CheckNegative(this);;currencyLostFocus(this);" rmxforms:as="currency" onchange="CoveragesEnableOk('EnableAmend',this);;setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_ClaimLimit" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ClaimLimit" Text="Claim Limit" />
          <span class="formw">
            <asp:TextBox runat="server" id="ClaimLimit" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/ClaimLimit" RMXType="currency" onblur="CheckNegative(this);;currencyLostFocus(this);" rmxforms:as="currency" onchange="CoveragesEnableOk('EnableAmend',this);;setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_TotalPayments" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_TotalPayments" Text="Total Payments" />
          <span class="formw">
            <asp:TextBox runat="server" id="TotalPayments" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/TotalPayments" RMXType="currency" onblur="CheckNegative(this);;currencyLostFocus(this);" rmxforms:as="currency" onchange="CoveragesEnableOk('EnableAmend',this);;setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_Deductible" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_Deductible" Text="Deductible" />
          <span class="formw">
            <asp:TextBox runat="server" id="Deductible" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/Deductible" RMXType="currency" onblur="CheckNegative(this);;currencyLostFocus(this);" rmxforms:as="currency" onchange="CoveragesEnableOk('EnableAmend',this);;setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_SelfInsuredRetention" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_SelfInsuredRetention" Text="Self Insured Retention" />
          <span class="formw">
            <asp:TextBox runat="server" id="SelfInsuredRetention" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/SelfInsRetention" RMXType="currency" onblur="CheckNegative(this);;currencyLostFocus(this);" rmxforms:as="currency" onchange="CoveragesEnableOk('EnableAmend',this);;setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_NextPolicy" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_NextPolicy" Text="Next Policy" />
          <span class="formw">
            <asp:Textbox runat="server" onchange="setDataChanged(true);" id="NextPolicy" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/NextPolicyName" RMXType="enhpolicylookup" name="NextPolicy" />
            <asp:button runat="server" class="button" Text="..." id="NextPolicybtn" onclientclick="return lookupData('NextPolicy','policyenh',-1,'NextPolicy',20);" />
            <asp:TextBox style="display:none" runat="server" name="NextPolicy_cid" value="" ref="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/NextPolicyId" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_CancelNotice" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_CancelNotice" Text="Cancel Notice(Days)" />
          <span class="formw">
            <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" id="CancelNotice" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/CancelNoticeDays" maxlength="3" onchange="&#xA;                  AllowIntOnly(this);&#xA;                  CoveragesEnableOk('EnableAmend',this); CheckNegative(this,true);" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_NotificationUser" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_NotificationUser" Text="Notification User" />
          <span class="formw">
            <cul:SystemUsers runat="server" ID="NotificationUser" CodeTable="" ControlName="NotificationUser" RMXRef="/Instance/Document/CoverageUIStatus/RMSysUserName" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_BrokerLastName" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_BrokerLastName" Text="Broker Last Name" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="BrokerLastName" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/BrokerName" RMXType="text" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_Exceptions" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_Exceptions" Text="Exceptions" />
          <span class="formw">
            <asp:TextBox runat="Server" id="Exceptions" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/Exceptions" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="ExceptionsbtnMemo" tabindex="NaN" id="ExceptionsbtnMemo" onclick="EditMemo('Exceptions','')" />
          </span>
        </div>
        <div runat="server" class="completerow" id="div_Remarks" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_Remarks" Text="Remarks" />
          <span class="formw">
            <asp:TextBox runat="Server" id="Remarks" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/Remarks" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="RemarksbtnMemo" tabindex="NaN" id="RemarksbtnMemo" onclick="EditMemo('Remarks','')" />
          </span>
        </div>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnCoveragesOk">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnCoveragesOk" RMXRef="" Text="OK" onClientClick="return fnCoveragesOk();" onclick="btnCoveragesOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCoveragesCancel">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnCoveragesCancel" RMXRef="" Text="Cancel" onClientClick="return fnCoveragesCancel();" onclick="btnCoveragesOk_Click" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="FunctionToCall" RMXRef="/Instance/Document/FunctionToCall" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SessionId" RMXRef="/Instance/Document/SessionId" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="" RMXType="hidden" Text="Coverages" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="AmendedDate" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/AmendedDate" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="ExceptionEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/ExceptionEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="RemarksEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/RemarksEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="CovTypeCodeEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/CovTypeCodeEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="CoverageStatusEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/CoverageStatusEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="EffDateEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/EffDateEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="ExpDateEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/ExpDateEnable" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="PolicyLimitEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/PolicyLimitEnable" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="OccuranceLimitEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/OccuranceLimitEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="ClaimLimitEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/ClaimLimitEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="TotalPaymentsEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/TotalPaymentsEnable" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="DeductibleEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/DeductibleEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SelfInsuredRetentionEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/SelfInsuredRetentionEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="NextPolicyIDEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/NextPolicyIDEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="CancelNoticeDaysEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/CancelNoticeDaysEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="NotificationEIdEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/NotificationEIdEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="BrokerLastNameEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/BrokerLastNameEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="OkEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/OkEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="CancelEnabled" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/CancelEnable" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="AmendDate" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/AmendDate" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="ControlType" RMXRef="/Instance/Document/ControlType" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="TransactionType" RMXRef="/Instance/Document/Coverages/CoverageUIStatus/TransactionType" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="hdnUserId" RMXRef="/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/NotificationUid" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="coverages" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>