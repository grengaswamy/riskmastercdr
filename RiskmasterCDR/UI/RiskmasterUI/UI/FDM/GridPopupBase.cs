﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 12/16/2013 | 34082   | pgupta93   | Changes req for MultiCurrency
 **********************************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
using System.Collections.Generic;
using Riskmaster.AppHelpers;
using Riskmaster.UI.Shared.Controls;
using MultiCurrencyCustomControl;

namespace Riskmaster.UI.FDM
{
    public class GridPopupBase : NonFDMBasePageCWS
    {


        public GridPopupBase()
        {
        }
        protected void GridPopupPageload()
        {
            if (!IsPostBack)
            {
                TextBox txtGridName = (TextBox)FindControlRecursive(this.Form, "gridname");
                txtGridName.Text = Request.QueryString["gridname"];
                TextBox txtmode = (TextBox)FindControlRecursive(this.Form, "mode");
                txtmode.Text = Request.QueryString["mode"];
                TextBox txtselectedrowposition = (TextBox)FindControlRecursive(this.Form, "selectedrowposition");
                txtselectedrowposition.Text = Request.QueryString["selectedrowposition"];

            }
            else
            {
                TextBox txtPostBackParent = (TextBox)FindControlRecursive(this.Form, "txtPostBack");
                txtPostBackParent.Text = "Done";

                #region Added Rakhi for R7:Add Emp Data Elements
                //TextBox txtChildGrid = (TextBox)FindControlRecursive(this.Form, "PopupGrid");
                //UserControlDataGrid objGridControl=null;
                //if (txtChildGrid != null && !String.IsNullOrEmpty(txtChildGrid.Text))
                //    objGridControl = (UserControlDataGrid)this.FindControl(txtChildGrid.Text);
                //Added Rakhi for R7:Add Emp Data Elements
                #endregion
                Control c = DatabindingHelper.GetPostBackControl(this.Page);
                //if (c == null || (c.ID.ToLower() == "delete" && objGridControl != null && objGridControl.PopupGrid && !objGridControl.SessionGrid))
                if (c == null)
                {
                    TextBox txtDataParent = (TextBox)FindControlRecursive(this.Form, "txtData");
                    string sXml = txtDataParent.Text;
                    sXml = sXml.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                    XmlDocument objXml = new XmlDocument();
                    objXml.LoadXml(sXml);

                    #region Added R7:Add Emp Data Elements
                    //TextBox txtPageLoad = (TextBox)FindControlRecursive(this.Form, "PageLoad");
                    //if(txtPageLoad != null && txtPageLoad.Text.ToLower() == "false" && objGridControl != null && objGridControl.PopupGrid && !objGridControl.SessionGrid)
                    //    BindTabData(objXml);
                    //Added R7:Add Emp Data Elements
                    #endregion
                    //added by Navdeep - Modify xml to Map Line of Business List Box to Pop Up from Grid (Chubb Ack)
                    ModifyXml(ref objXml);
                    sXml = objXml.InnerXml;
                    //end by Navdeep
                    XElement objElem = XElement.Parse(sXml);
                    //Deb Multi Currency
                    string sCurrCulture = AppHelper.GetQueryStringValue("pmtcurrencytype");
                    if (!string.IsNullOrEmpty(sCurrCulture))
                    {
                        Culture = sCurrCulture.Split('|')[1];
                        UICulture = sCurrCulture.Split('|')[1];
                        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(sCurrCulture.Split('|')[1]);
                        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(sCurrCulture.Split('|')[1]);
                        Label lblcurrencytype = (Label)this.FindControl("lblcurrencytype");
                        Control objDivlblCurrType = this.FindControl("div_lblcurrencytype");
                        Control objDivClaimCurrBal = this.FindControl("div_ClaimCurrReserveBal");
                        if (lblcurrencytype != null)
                        {
                            if ((objElem.XPathSelectElement("//UseMultiCurrency") != null))
                            {
                                if (string.Compare(objElem.XPathSelectElement("./UseMultiCurrency").Value, Boolean.TrueString) != 0)
                                {
                                    objDivClaimCurrBal.Visible = false;
                                    objDivlblCurrType.Visible = false;
                                }
                                else
                                {
                                    lblcurrencytype.Text = sCurrCulture.Split('|')[0].Substring(0, 3) + " " + System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol;
                                }
                            }
                        }
                    }
                    //Deb Multi Currency
                    //Aman MITS 31354 --start
                    string sAddresses = AppHelper.GetQueryStringValue("formentityxaddresses");
                    if (!string.IsNullOrEmpty(sAddresses) && sAddresses.ToLower().Equals("entityxaddresses"))
                    {
                        if (Request.QueryString["CountrySelectedId"] != null)
                        {
                            AppHelper.RegionalFormatsScript(Request.QueryString["CountrySelectedId"], this);
                            //PropertyInfo isreadonly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
                            //isreadonly.SetValue(this.Request.QueryString, false, null);
                            //this.Request.QueryString.Remove("CountrySelectedId");
                            //isreadonly.SetValue(this.Request.QueryString, true, null);
                        }
                        else
                        {
                            AppHelper.RegionalFormatsScript(string.Empty, this);
                        }
                    }
                    //Aman MITS 31354 --End
                    else
                    {
                    BindData2Control(objElem, this.Controls);
                    //Added Rakhi for R7:Add Emp Data Elements
                    //string sMode = AppHelper.GetQueryStringValue("mode");
                    //if (txtPageLoad != null && txtPageLoad.Text.ToLower() == "true")
                    //{

                    //    if (sMode.ToLower() != "edit")
                    //    {
                    //        txtPostBackParent.Text = "";
                    //    }
                    //    txtPageLoad.Text = "false";
                    //}
                    //Added Rakhi for R7:Add Emp Data Elements

                    //XmlNodeList objNodeList = objXml.SelectNodes("/option");
                    //XmlNode objNodes = objNodeList[0];

                    //foreach (XmlElement objElem in objNodes)
                    //{

                    //    if (objElem.Attributes.Count == 0)
                    //    {
                    //        TextBox txtDataRunTime = (TextBox)FindControlRecursive(this.Form, objElem.Name);
                    //        txtDataRunTime.Text = objElem.InnerText;
                    //    }
                    //    else
                    //    {
                    //        if (objElem.Attributes["codeid"] != null)
                    //        {
                    //            TextBox txtDataRunTimeCodeId = (TextBox)FindControlRecursive(this.Form, objElem.Name + "_codelookup_cid");
                    //            txtDataRunTimeCodeId.Text = objElem.Attributes["codeid"].InnerText;

                    //            TextBox txtDataRunTimeCode = (TextBox)FindControlRecursive(this.Form, objElem.Name + "_codelookup");
                    //            txtDataRunTimeCode.Text = objElem.InnerText;
                    //        }
                    //    }
                        //}
                    }
                }
            }
        }

        public static Control FindControlRecursive(Control root, string id)
        {
            if (root.ClientID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }

        //added by Navdeep - Modify xml to Map Line of Business List Box to Pop Up from Grid (Chubb Ack)
        public virtual void ModifyXml(ref XmlDocument p_objInputXml)
        {
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            XmlDocument objXml = new XmlDocument();



            TextBox txtDataParent = (TextBox)FindControlRecursive(this.Form, "txtData");
            string sData = txtDataParent.Text;
            sData = sData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
            objXml.LoadXml(sData);

            XmlNodeList objNodeList = objXml.SelectNodes(objXml.FirstChild.Name);
            XmlNode objNode = objNodeList[0];
            int iNewRecord = 0;
            foreach (XmlElement objElem in objNode)
            {

                if (objElem.Attributes.Count == 0)
                {
                    // Check if the Current Text Box is for Primary Key
                    TextBox txtPrimaryKey = (TextBox)FindControlRecursive(this.Form, "UniqueId");
                    if (txtPrimaryKey == null)
                        throw new Exception("Primary Key Not Implemeted on GridPopup Page");
                    else
                    {
                        Control oCtrl = (Control)FindControlRecursive(this.Form, objElem.Name);
                        if (oCtrl != null)
                        {
                            Type controlType = oCtrl.GetType();
                            string sType = controlType.ToString();
                            int index = sType.LastIndexOf(".");
                            sType = sType.Substring(index + 1);
                            switch (sType)
                            {
                                case "DropDownList":

                                    TextBox txtComboType = (TextBox)FindControlRecursive(this.Form, "txtComboType");
                                    if (txtComboType != null && txtComboType.Text == "comboRelations")
                                    {
                                        TextBox txtComboValueText = (TextBox)FindControlRecursive(this.Form, "comboRelations");
                                        if (txtComboValueText != null)
                                        {
                                            if (txtComboValueText.Text != "")
                                            {
                                                string[] sValueTextPair = new string[50];
                                                sValueTextPair = txtComboValueText.Text.Split(";".ToCharArray()[0]);//Each ValueTextPair is separated from other with a ;
                                                foreach (string sValueText in sValueTextPair)
                                                {
                                                    string[] sValueTextSplit = new string[2];
                                                    sValueTextSplit = sValueText.Split("|".ToCharArray()[0]);//Each ValueText is | separated.
                                                    if (sValueTextSplit.Length == 2)
                                                    {
                                                        if (objElem.Name == sValueTextSplit[1]) //Element is Text Element.
                                                        {
                                                            XmlNode objValueNode = objNode.SelectSingleNode(sValueTextSplit[0]);//Hide Node is Value Node
                                                            if (objValueNode != null)
                                                            {
                                                                objValueNode.InnerText = ((DropDownList)oCtrl).SelectedValue;
                                                                objElem.InnerText = ((DropDownList)oCtrl).SelectedItem.Text;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (txtComboType != null && txtComboType.Text == "combotext")
                                    {
                                        objElem.InnerText = ((DropDownList)oCtrl).SelectedItem.Text;
                                    }
                                    else
                                    {
                                        objElem.InnerText = ((DropDownList)oCtrl).SelectedValue;
                                    }
                                    break;
                                   
                                case "CurrencyTextbox":
                                    //changed Max Amount RmxType to Currency
                                    //Start MITS ID 31950 6/13/2013 psaiteja
                                    if(((CurrencyTextbox)oCtrl).AmountInString!= null)
                                     objElem.InnerText = ((CurrencyTextbox)oCtrl).AmountInString;
                                    //End MITS ID 31950 6/13/2013 psaiteja@csc.com
                                    break;
                                case "TextBox":
                                    TextBox txtDataRunTime = ((TextBox)oCtrl);
                                    //MGaba2:MITS 15469:value of TaxId was not retreived from thirdpartyscreen bcoz it was readonly:start
                                    bool bCtrlStatus = txtDataRunTime.ReadOnly;
                                    if (bCtrlStatus)
                                    {
                                     //Not handling check of code here bcoz codelookup is handled in the else section
                                            txtDataRunTime.Text = HttpContext.Current.Request.Form[oCtrl.ID];                                       
                                    }
                                    //MGaba2:MITS 15469:End
                                    if (txtPrimaryKey.Text != objElem.Name || (txtPrimaryKey.Text == objElem.Name && txtDataRunTime.Text != ""))
                                    {
                                        objElem.InnerText = txtDataRunTime.Text;
                                    }
                                    else if (txtPrimaryKey.Text == objElem.Name && txtDataRunTime.Text == "")
                                    {
                                        objElem.InnerText = (--iNewRecord).ToString();
                                    }
                                    break;
                                case "CheckBox":
                                    //MGaba2:MITS 15498:Value of Enclosure Flag was not retreived
                                    //rjhamb Customize grid to show Yes/No
                                    if (((CheckBox)oCtrl).Attributes["DeletedFlagYesNo"] != null)
                                    {
                                        objElem.InnerText = "No";
                                        if (((CheckBox)oCtrl).Checked)
                                        {
                                            objElem.InnerText = "Yes";
                                        }
                                    }
                                    else
                                    {
                                        objElem.InnerText = "False";
                                        if (((CheckBox)oCtrl).Checked)
                                        {
                                            objElem.InnerText = "True";
                                        }
                                    }
                                    break;
                            }
                        }
                        //Added for R7:Add Emp Data Elements
                        //else
                        //{
                        //    Control oControl = (Control)FindControlRecursive(this.Form, objElem.Name + "Grid");
                        //    if (oControl != null && oControl is UserControlDataGrid)
                        //    {
                        //        XmlDocument objXmlDoc = ((UserControlDataGrid)oControl).GenerateXml();
                        //        XmlNode objListNode = objXmlDoc.SelectSingleNode(objElem.Name);
                        //        if (objListNode != null)
                        //        {
                        //            objElem.InnerXml = objListNode.InnerXml;
                        //        }
                        //    }

                        //}
                        //Added for R7:Add Emp Data Elements
                    }
                }
                else
                {
                    if (objElem.Attributes["codeid"] != null || objElem.Attributes["value"] != null)
                    {
                        TextBox txtDataRunTimeCodeId = (TextBox)FindControlRecursive(this.Form, objElem.Name + "_codelookup_cid");
                        if (txtDataRunTimeCodeId != null)
                            objElem.Attributes["codeid"].InnerText = txtDataRunTimeCodeId.Text;

                        TextBox txtDataRunTimeCode = (TextBox)FindControlRecursive(this.Form, objElem.Name + "_codelookup");
                        if (txtDataRunTimeCode != null)
                        {
                            //MGaba2:MITS 15469:Adding handling of readonly controls:Start
                            bool bCtrlStatus = txtDataRunTimeCode.ReadOnly;
                            if (bCtrlStatus)
                            {
                                string sControlId = txtDataRunTimeCode.ClientID;
                                sControlId = sControlId.Replace('_', '$');
                                objElem.InnerText = HttpContext.Current.Request.Form[sControlId];
                                //MGaba2:MITS 15469:End
                            }
                            else
                            {
                                objElem.InnerText = txtDataRunTimeCode.Text;
                            }
                        }


                        //Added Rakhi for Search lookup
                        TextBox txtDataRunTimeLookUpId = (TextBox)FindControlRecursive(this.Form, objElem.Name + "_cid");
                        if (txtDataRunTimeLookUpId != null)
                        {
                            objElem.Attributes["codeid"].InnerText = txtDataRunTimeLookUpId.Text;
                            TextBox txtDataRunTimeLookUpCode = (TextBox)FindControlRecursive(this.Form, objElem.Name);
                            if (txtDataRunTimeLookUpCode != null)
                                objElem.InnerText = txtDataRunTimeLookUpCode.Text;
                        }
                        //Added Rakhi for Search lookup
                        //added by Navdeep for Multi Code  (Chubb Ack)                                              
                        TextBox lstDataRunTimeLookUpCode = (TextBox)FindControlRecursive(this.Form, objElem.Name + "_multicode_lst");
                        if (lstDataRunTimeLookUpCode != null)
                        {
                            objElem.Attributes["codeid"].InnerText = lstDataRunTimeLookUpCode.Text;
                        }
                        if (objElem.Name == "PolicyID")
                        {
                            /*
                             //commented by rupal, the PolicyID fild will always have the value passed from parent page since it is being set n javascript onload function
                             //but practically, user can change the policy and unit on split page, so we always need updated policy. instead of getting policyid from 
                             //hidden feilds, we will directlyget it from the dropdown itself
                            TextBox txtDataRunTimeId = (TextBox)FindControlRecursive(this.Form, objElem.Name);
                            objElem.Attributes["value"].InnerText = txtDataRunTimeId.Text;
                            for (int i = 0; i < ((DropDownList)this.FindControl("Policy")).Items.Count; i++)
                            {
                                if(string.Compare(((DropDownList)this.FindControl("Policy")).Items[i].Value,txtDataRunTimeId .Text,true) == 0)
                                 objElem.InnerText = ((DropDownList)this.FindControl("Policy")).Items[i].Text;
                            }*/

                            //rupal:start, r8 unit changes
                            //in case of futurepayments and third party payments, we want policy id from dropdown
                            DropDownList dPolicy = (DropDownList)this.FindControl("Policy");
                            if (dPolicy != null)
                            {
                                objElem.Attributes["value"].InnerText = dPolicy.SelectedValue;
                                objElem.InnerText = dPolicy.SelectedItem.Text.Trim();
                            }
                            //rupal:end
                        }
                        //rupal:start, r8 unit changes
                        if (objElem.Name == "UnitID")
                        {
                            //in case of futurepayments and third party payments, we want unit id from dropdown
                            DropDownList dUnit = (DropDownList)this.FindControl("Unit");
                            if (dUnit != null)
                            {
                                objElem.Attributes["value"].InnerText = dUnit.SelectedValue;
                                objElem.InnerText = dUnit.SelectedItem.Text.Trim();
                            }

                        }
                        //rupal:end
                        if (objElem.Name == "CoverageTypeCode")
                        {
                            objElem.Attributes["codeid"].InnerText = ((TextBox)FindControlRecursive(this.Form, "Coverage" + "_codelookup_cid")).Text;
                            objElem.InnerText = ((TextBox)FindControl("Coverage$codelookup")).Text;
                        }
                        //MITS:34082 MultiCurrency START
                        if (objElem.Name == "SplitCurrCode")
                        {
                            //ajohari2 FDM PowerView Fixes: Start
                            //objElem.Attributes["codeid"].InnerText = ((TextBox)FindControlRecursive(this.Form, "CurrencyTypeForClaim" + "_codelookup_cid")).Text;
                            //objElem.InnerText = ((TextBox)FindControl("CurrencyTypeForClaim$codelookup")).Text;
                            TextBox txtCurrencyTypeForClaimId = ((TextBox)FindControlRecursive(this.Form, "CurrencyTypeForClaim" + "_codelookup_cid"));
                            if (txtCurrencyTypeForClaimId != null)
                            {
                                objElem.Attributes["codeid"].InnerText = txtCurrencyTypeForClaimId.Text;
                            }
                            TextBox txtCurrencyTypeForClaimCode = ((TextBox)FindControl("CurrencyTypeForClaim$codelookup"));
                            if (txtCurrencyTypeForClaimCode != null)
                            {
                                objElem.InnerText = txtCurrencyTypeForClaimCode.Text;
                            }
                            //ajohari2 FDM PowerView Fixes: End
                        }
                        //MITS:34082 MultiCurrency END
                        if (objElem.Name == "CovgSeqNum")
                        {
                            //in case of futurepayments and third party payments, we want unit id from dropdown
                            TextBox    txtCovgSeqNum = (TextBox)this.FindControl("CovgSeqNum");
                            if (txtCovgSeqNum != null)
                            {
                                objElem.Attributes["value"].InnerText = txtCovgSeqNum.Text;
                                objElem.InnerText = txtCovgSeqNum.Text;
                            }

                        }
                        //rupal:end
                        if (objElem.Name == "DisabilityTypeCode")
                        {
                            objElem.Attributes["codeid"].InnerText = ((TextBox)FindControlRecursive(this.Form, "DisabilityLossType" + "_codelookup_cid")).Text;
                            objElem.InnerText = ((TextBox)FindControl("DisabilityLossType$codelookup")).Text;
                        }
                        if (objElem.Name == "LossTypeCode")
                        {
                            objElem.Attributes["codeid"].InnerText = ((TextBox)FindControlRecursive(this.Form, "LossType" + "_codelookup_cid")).Text;
                            objElem.InnerText = ((TextBox)FindControl("LossType$codelookup")).Text;
                        }
                    }
                    //Deb Multi Currency
                    else
                    {
                        TextBox txtPrimaryKey = (TextBox)FindControlRecursive(this.Form, "UniqueId");
                        if (txtPrimaryKey == null)
                            throw new Exception("Primary Key Not Implemeted on GridPopup Page");
                        Control oCtrl = (Control)FindControlRecursive(this.Form, objElem.Name);
                        if (oCtrl != null)
                        {
                            Type controlType = oCtrl.GetType();
                            string sType = controlType.ToString();
                            int index = sType.LastIndexOf(".");
                            sType = sType.Substring(index + 1);
                            switch (sType)
                            {
                                
                                case "CurrencyTextbox":
                                    CurrencyTextbox ctxtDataRunTime = ((CurrencyTextbox)oCtrl);
                                    if (txtPrimaryKey.Text != objElem.Name || (txtPrimaryKey.Text == objElem.Name && ctxtDataRunTime.Text != ""))
                                    {
                                        objElem.InnerText = ctxtDataRunTime.AmountInString;
                                        XmlAttribute objXmlAttribute = objXml.CreateAttribute("CurrencyValue");
                                        objXmlAttribute.Value = ctxtDataRunTime.Text;
                                        objElem.Attributes.Append(objXmlAttribute);
                                    }
                                    else if (txtPrimaryKey.Text == objElem.Name && ctxtDataRunTime.Text == "")
                                    {
                                        objElem.InnerText = (--iNewRecord).ToString();
                                        XmlAttribute objXmlAttribute = objXml.CreateAttribute("CurrencyValue");
                                        objXmlAttribute.Value = (--iNewRecord).ToString();
                                        objElem.Attributes.Append(objXmlAttribute);
                                    }
                                    break;
                            }
                        }
                    }
                    //Deb Multi Currency
                }
            }
            //mkaran2 - [JIRA] (RMA-4110) MITS 37158 -start                                                          
            XmlNode oCoverageTypeCode = objXml.SelectSingleNode("//CoverageTypeCode");
            if (oCoverageTypeCode != null)
                oCoverageTypeCode.InnerText = AppHelper.HTMLCustomEncode(oCoverageTypeCode.InnerText);

            XmlNode oUnitID = objXml.SelectSingleNode("//UnitID");
            if (oUnitID != null)
                oUnitID.InnerText = AppHelper.HTMLCustomEncode(oUnitID.InnerText);
            //mkaran2 - [JIRA] (RMA-4110) MITS 37158 -end
            txtDataParent.Text = objXml.OuterXml;

            string script = "<script>GetDataFromOpener();</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", script);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }


        private void BindData2Control(XElement oMessageElement, ControlCollection oControls)
        {
            foreach (Control oCtrl in oControls)
            {
                if (oCtrl is WebControl)
                {
                    string sRMXRef = ((WebControl)oCtrl).Attributes["RMXRef"];
                    if (string.IsNullOrEmpty(sRMXRef))
                        continue;


                    Type controlType = oCtrl.GetType();
                    string sType = controlType.ToString();
                    int index = sType.LastIndexOf(".");
                    sType = sType.Substring(index + 1);

                    switch (sType)
                    {
                        case "DropDownList":
                        case "ComboBox": //MGaba2:R7:Added case for Ajax Combo used on Purge History PopUp
                            string sSelectedValue = "";
                            TextBox txtComboType = (TextBox)FindControlRecursive(this.Form, "txtComboType");
                            if (txtComboType != null && txtComboType.Text == "comboRelations")
                            {
                                TextBox txtComboValueText = (TextBox)FindControlRecursive(this.Form, "comboRelations");
                                if (txtComboValueText != null)
                                {
                                    if (txtComboValueText.Text != "")
                                    {
                                        string[] sValueTextPair = new string[50];

                                        XElement oElement = null;
                                        sValueTextPair = txtComboValueText.Text.Split(";".ToCharArray()[0]);
                                        foreach (string sValueText in sValueTextPair)
                                        {
                                            string[] sValueTextSplit = new string[2];
                                            sValueTextSplit = sValueText.Split("|".ToCharArray()[0]);
                                            oElement = oMessageElement.XPathSelectElement(sValueTextSplit[0]);
                                            if (oElement != null)
                                            {
                                                sSelectedValue = oElement.Value;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                sSelectedValue = GetReturnValue(oMessageElement, sRMXRef);
                            }

                            DatabindingHelper.SetValue2Control((WebControl)oCtrl, sSelectedValue);
                            break;
                        case "CheckBox":
                            //MGaba2: setDataChanged was not called in case of checkbox 
                            ((CheckBox)oCtrl).InputAttributes.Add("onchange", "return setDataChanged(true);");
                            string sValue = GetReturnValue(oMessageElement, sRMXRef);
                            DatabindingHelper.SetValue2Control((WebControl)oCtrl, sValue);
                            break;
                        default:
                            sValue = GetReturnValue(oMessageElement, sRMXRef);
                            DatabindingHelper.SetValue2Control((WebControl)oCtrl, sValue);
                            break;
                    }

                    //string sValue = GetReturnValue(oMessageElement, sRMXRef);
                    //DatabindingHelper.SetValue2Control((WebControl)oCtrl, sValue);

                }
                //Added Rakhi for R7:Add Emp Data Elements
                //else if (oCtrl is UserControlDataGrid)
                //{
                //    XmlDocument objXml = new XmlDocument();
                //    XmlReader objReader;
                //    objReader = oMessageElement.CreateReader();
                //    objXml.Load(objReader);
                //    ((UserControlDataGrid)oCtrl).BindData(objXml);
                //}
                //Added Rakhi for R7:Add Emp Data Elements
                else
                {
                    if (oCtrl.Controls.Count > 0)
                        BindData2Control(oMessageElement, oCtrl.Controls);
                }
            }
        }

        private string GetReturnValue(XElement oMessageElement, string sPath)
        {
            //check if XPath is for an attribute
            string sValue = string.Empty;
            XElement oElement = null;
            int index = sPath.LastIndexOf("/");
            string sName = sPath.Substring(index + 1);
            if (sName.StartsWith("@"))
            {
                sName = sName.Substring(1);
                string sElementName = sPath.Substring(0, index);
                oElement = oMessageElement.XPathSelectElement(sElementName);
                if (oElement != null)
                {
                    sValue = oElement.Attribute(sName).Value;
                }
            }
            else
            {
                oElement = oMessageElement.XPathSelectElement(sPath);
                if (oElement != null)
                {
                    //For muti-code data type
                    if (oElement.HasElements)
                    {
                        string sChildName = ((XElement)(oElement.FirstNode)).Name.ToString();
                        switch (sChildName)
                        {
                            // npadhy Added a case for option to cater the combobox control, 
                            // which has the item collection in option node.
                            case "Item":
                            case "option":
                                sValue = oElement.ToString();
                                break;
                            default:
                                sValue = oElement.FirstNode.ToString();
                                break;
                        }
                    }
                    else
                        sValue = oElement.Value;
                }

            }
            return sValue;
        }
        #region BindTabData
        //private void BindTabData(XmlDocument objXml)
        //{
        //    XmlNodeList objNodeList = objXml.SelectNodes(objXml.FirstChild.Name);
        //    XmlNode objNode = objNodeList[0];
        //    foreach (XmlElement objElem in objNode)
        //    {
        //        if (objElem.Attributes.Count == 0)
        //        {
        //            Control oCtrl = (Control)FindControlRecursive(this.Form, objElem.Name);
        //            if (oCtrl != null)
        //            {
        //                Type controlType = oCtrl.GetType();
        //                string sType = controlType.ToString();
        //                int index = sType.LastIndexOf(".");
        //                sType = sType.Substring(index + 1);
        //                switch (sType)
        //                {
        //                    case "DropDownList":
        //                        objElem.InnerText = ((DropDownList)oCtrl).SelectedValue;
        //                        break;
        //                    case "TextBox":
        //                        objElem.InnerText = ((TextBox)oCtrl).Text;
        //                        break;
        //                    case "CheckBox":
        //                        objElem.InnerText = "False";
        //                        if (((CheckBox)oCtrl).Checked)
        //                        {
        //                            objElem.InnerText = "True";
        //                        }
        //                        break;
        //                }
        //            }
        //        }

        //        else
        //        {
        //            if (objElem.Attributes["codeid"] != null)
        //            {
        //                TextBox txtDataRunTimeCodeId = (TextBox)FindControlRecursive(this.Form, objElem.Name + "_codelookup_cid");
        //                if (txtDataRunTimeCodeId != null)
        //                {
        //                    if (!String.IsNullOrEmpty(txtDataRunTimeCodeId.Text))
        //                        objElem.Attributes["codeid"].InnerText = txtDataRunTimeCodeId.Text;
        //                    else
        //                        objElem.Attributes["codeid"].InnerText = "-1";
        //                }

        //                TextBox txtDataRunTimeCode = (TextBox)FindControlRecursive(this.Form, objElem.Name + "_codelookup");
        //                if (txtDataRunTimeCode != null)
        //                {
        //                    objElem.InnerText = txtDataRunTimeCode.Text;
        //                }

        //                TextBox txtDataRunTimeLookUpId = (TextBox)FindControlRecursive(this.Form, objElem.Name + "_cid");
        //                if (txtDataRunTimeLookUpId != null)
        //                {
        //                    if (!String.IsNullOrEmpty(txtDataRunTimeCodeId.Text))
        //                    {
        //                        objElem.Attributes["codeid"].InnerText = txtDataRunTimeLookUpId.Text;
        //                    }
        //                    else
        //                    {
        //                        objElem.Attributes["codeid"].InnerText = "-1";
        //                    }
        //                    TextBox txtDataRunTimeLookUpCode = (TextBox)FindControlRecursive(this.Form, objElem.Name);
        //                    if (txtDataRunTimeLookUpCode != null)
        //                        objElem.InnerText = txtDataRunTimeLookUpCode.Text;
        //                }


        //                TextBox lstDataRunTimeLookUpCode = (TextBox)FindControlRecursive(this.Form, objElem.Name + "_multicode_lst");
        //                if (lstDataRunTimeLookUpCode != null)
        //                {
        //                    if (!String.IsNullOrEmpty(txtDataRunTimeCodeId.Text))
        //                    {
        //                        objElem.Attributes["codeid"].InnerText = lstDataRunTimeLookUpCode.Text;
        //                    }
        //                    else
        //                    {
        //                        objElem.Attributes["codeid"].InnerText = "-1";
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}
        #endregion 
    }
}
