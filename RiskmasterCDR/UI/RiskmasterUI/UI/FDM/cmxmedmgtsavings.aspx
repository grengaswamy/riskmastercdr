<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cmxmedmgtsavings.aspx.cs"
    Inherits="Riskmaster.UI.FDM.Cmxmedmgtsavings" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Medical Management Savings</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="../../Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="attach()" src="../../Images/attach.gif"
                        width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                        onmouseover="this.src='../../Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/attach.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="FilteredDiary()" src="../../Images/filtereddiary.gif"
                        width="28" height="28" border="0" id="filtereddiary" alternatetext="View Record Diaries"
                        onmouseover="this.src='../../Images/filtereddiary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/filtereddiary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Diary()" src="../../Images/diary.gif"
                        width="28" height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='../../Images/diary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/diary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="../../Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Medical Management Savings" /><asp:label
            id="formsubtitle" runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>

    <script language="JavaScript" xmlns="">
    function onTextChange()
    {
     
      var iDiff = document.forms[0].quotedamount.value - document.forms[0].agreedamount.value;
      if(iDiff > 0)
        document.forms[0].savings.value = iDiff;
      else
        document.forms[0].savings.value = 0; 
      setDataChanged(true);
      
    }
    </script>

    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSMedMgmtSavings" id="TABSMedMgmtSavings">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="MedMgmtSavings"
                id="LINKTABSMedMgmtSavings"><span style="text-decoration: none">Medical Management Savings</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABMedMgmtSavings" id="FORMTABMedMgmtSavings">
        <asp:textbox style="display: none" runat="server" id="cmmsrowid" rmxref="/Instance/CmXMedmgtsavings/CmmsRowId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="casemgtrowid"
                rmxref="/Instance/CmXMedmgtsavings/CasemgtRowId" rmxtype="id" /><asp:textbox style="display: none"
                    runat="server" id="eventnumber" rmxref="/Instance/UI/FormVariables/SysExData/EventNumber"
                    rmxtype="id" /><div runat="server" class="half" id="div_providergrouplist" xmlns="">
                        <asp:label runat="server" class="label" id="lbl_providergrouplist" text="Provider Group" /><span
                            class="formw"><asp:textbox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                                id="providergrouplist" rmxref="/Instance/CmXMedmgtsavings/ProviderEid" rmxtype="eidlookup"
                                cancelledvalue="" tabindex="1" /><input type="button" class="button" value="..."
                                    name="providergrouplistbtn" tabindex="2" onclick="lookupData('providergrouplist','0',4,'providergrouplist',2)" /><asp:textbox
                                        style="display: none" runat="server" id="providergrouplist_cid" rmxref="/Instance/CmXMedmgtsavings/ProviderEid/@codeid"
                                        cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_quotedamount" xmlns="">
            <asp:label runat="server" class="label" id="lbl_quotedamount" text="Quoted Amount" /><span
                class="formw"><asp:textbox runat="server" onblur="numLostFocus(this);" id="quotedamount"
                    rmxref="/Instance/CmXMedmgtsavings/QuotedAmount" rmxtype="numeric" tabindex="12"
                    onchange="javascript:onTextChange();" /></span></div>
        <div runat="server" class="half" id="div_treatmentprovider" xmlns="">
            <asp:label runat="server" class="label" id="lbl_treatmentprovider" text="Treatment Provider" /><span
                class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" id="treatmentprovider"
                    rmxref="/Instance/CmXMedmgtsavings/TreatmentProvider" rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_agreedamount" xmlns="">
            <asp:label runat="server" class="label" id="lbl_agreedamount" text="Agreed Amount" /><span
                class="formw"><asp:textbox runat="server" onblur="numLostFocus(this);" id="agreedamount"
                    rmxref="/Instance/CmXMedmgtsavings/AgreedAmount" rmxtype="numeric" tabindex="13"
                    onchange="javascript:onTextChange();" /></span></div>
        <div runat="server" class="half" id="div_casemgrlist" xmlns="">
            <asp:label runat="server" class="label" id="lbl_casemgrlist" text="Case Manager" /><span
                class="formw"><asp:textbox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                    id="casemgrlist" rmxref="/Instance/CmXMedmgtsavings/CaseMgrEid" rmxtype="eidlookup"
                    cancelledvalue="" tabindex="4" /><input type="button" class="button" value="..."
                        name="casemgrlistbtn" tabindex="5" onclick="lookupData('casemgrlist','CASE_MANAGER',4,'casemgrlist',2)" /><asp:textbox
                            style="display: none" runat="server" id="casemgrlist_cid" rmxref="/Instance/CmXMedmgtsavings/CaseMgrEid/@codeid"
                            cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_savings" xmlns="">
            <asp:label runat="server" class="label" id="lbl_savings" text="Savings" /><span class="formw"><asp:textbox
                runat="server" rmxref="/Instance/CmXMedmgtsavings/Savings" id="savings" tabindex="14"
                style="background-color: silver;" readonly="true"  /></span></div>
        <div runat="server" class="half" id="div_savingstype" xmlns="">
            <asp:label runat="server" class="label" id="lbl_savingstype" text="Savings Type" /><span
                class="formw"><asp:textbox runat="server" onchange="lookupTextChanged(this);" id="savingstype"
                    rmxref="/Instance/CmXMedmgtsavings/SavingsTypeCode" rmxtype="code" cancelledvalue=""
                    tabindex="6" onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl"
                        runat="Server" id="savingstypebtn" onclientclick="return selectCode('SAV_TYPE','savingstype');"
                        tabindex="7" /><asp:textbox style="display: none" runat="server" id="savingstype_cid"
                            rmxref="/Instance/CmXMedmgtsavings/SavingsTypeCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_transactiontype" xmlns="">
            <asp:label runat="server" class="label" id="lbl_transactiontype" text="Transaction Type" /><span
                class="formw"><asp:textbox runat="server" onchange="lookupTextChanged(this);" id="transactiontype"
                    rmxref="/Instance/CmXMedmgtsavings/TransactionTypeCode" rmxtype="code" cancelledvalue=""
                    tabindex="8" onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl"
                        runat="Server" id="transactiontypebtn" onclientclick="return selectCode('TRANS_TYPES','transactiontype');"
                        tabindex="9" /><asp:textbox style="display: none" runat="server" id="transactiontype_cid"
                            rmxref="/Instance/CmXMedmgtsavings/TransactionTypeCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_summary" xmlns="">
            <asp:label runat="server" class="label" id="lbl_summary" text="Summary" /><span class="formw"><asp:textbox
                runat="Server" id="summary" rmxref="/Instance/CmXMedmgtsavings/Summary" rmxtype="memo"
                readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                tabindex="10" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                    value="..." name="summarybtnMemo" tabindex="11" id="summarybtnMemo" onclick="EditMemo('summary','')" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup">
        <asp:textbox style="display: none" runat="server" id="supp_cmms_row_id" rmxref="/Instance/*/Supplementals/CMMS_ROW_ID"
            rmxtype="id" /></div>
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnToCmXMedmgtsavings">
            <asp:button class="button" runat="Server" id="btnToCmXMedmgtsavings" text="Back to Medical Management Savings List"
                onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"
                postbackurl="?SysViewType=controlsonly&SysCmd=1" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="CmXMedmgtsavings" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" text="&lt;CmXMedmgtsavings&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/CmXMedmgtsavings&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysPSid"
                                    rmxref="Instance/UI/FormVariables/SysPSid" rmxtype="hidden" text="" /><asp:textbox
                                        style="display: none" runat="server" id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId"
                                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysSid"
                                            rmxref="Instance/UI/FormVariables/SysSid" rmxtype="hidden" text="11150" /><asp:textbox
                                                style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormName"
                                                    rmxref="Instance/UI/FormVariables/SysFormName" rmxtype="hidden" text="CmXMedmgtsavings" /><asp:textbox
                                                        style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
                                                        rmxtype="hidden" text="cmmsrowid" /><asp:textbox style="display: none" runat="server"
                                                            id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden"
                                                            text="casemgtrowid" /><asp:textbox style="display: none" runat="server" id="SysFormPForm"
                                                                rmxref="Instance/UI/FormVariables/SysFormPForm" rmxtype="hidden" text="" /><asp:textbox
                                                                    style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                                                    rmxtype="hidden" text="eventnumber,claimnumber" /><asp:textbox style="display: none"
                                                                        runat="server" name="formname" value="CmXMedmgtsavings" /><asp:textbox style="display: none"
                                                                            runat="server" name="SysRequired" value="" /><asp:textbox style="display: none" runat="server"
                                                                                name="SysFocusFields" value="providergrouplist|" /><input type="hidden" id="hdSaveButtonClicked" /><asp:textbox
                                                                                    runat="server" id="SysInvisible" style="display: none" /><asp:textbox runat="server"
                                                                                        id="SysLookupClass" style="display: none" /><asp:textbox runat="server" id="SysLookupRecordId"
                                                                                            style="display: none" /><asp:textbox runat="server" id="SysLookupAttachNodePath"
                                                                                                style="display: none" /><asp:textbox runat="server" id="SysLookupResultConfig" style="display: none" /><input
                                                                                                    type="hidden" value="rmx-widget-handle-2" id="SysWindowId" /></form>
</body>
</html>
