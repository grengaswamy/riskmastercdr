<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="eventintervention.aspx.cs"
    Inherits="Riskmaster.UI.FDM.Eventintervention" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Intervention/Evaluation</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">        { var i; }
    </script>

    <script language="JavaScript" src="Scripts/drift.js">        { var i; }
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28"
                        height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28"
                        height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28"
                        height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28"
                        height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28"
                        height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Intervention/Evaluation" /><asp:label
            id="formsubtitle" runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" text="" runat="server"/></div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSemployee" id="TABSemployee">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="employee"
                id="LINKTABSemployee"><span style="text-decoration: none">Employee</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSevaluation" id="TABSevaluation">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="evaluation"
                id="LINKTABSevaluation"><span style="text-decoration: none">Evaluation/Intervention</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABStande" id="TABStande">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="tande"
                id="LINKTABStande"><span style="text-decoration: none">Time & Expense</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABemployee" id="FORMTABemployee">
        <asp:textbox style="display: none" runat="server" id="eventid" rmxref="/Instance/EventXIntervention/EventId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="evinterrowid"
                rmxref="/Instance/EventXIntervention/EvIntRowId" rmxtype="id" /><asp:textbox style="display: none"
                    runat="server" id="eventnumber" rmxref="Instance/UI/FormVariables/SysExData/EventNumber"
                    rmxtype="id" />
                    
        <div runat="server" class="half" id="div_deptassignedeid" xmlns="">
        <span class="label">Name</span><span class="formw">
        <asp:textBox runat="server" onchange="lookupTextChanged(this);" onblur="codeLostFocus(this.name);" rmxref="/Instance/EventXIntervention/EmpEid" id="lastfirstname" tabindex="1" cancelledvalue="" />
                    <%--<asp:Button class="button" runat="Server" value="..." tabindex="2" id="lastfirstnamebtn" onclick="lookupData('lastfirstname','EMPLOYEE.LASTNAME',3,'',4)" />--%>
        <input type="button" class="button" Text="..." tabindex="2" id="lastfirstnamebtn" onclick="lookupData('lastfirstname','EMPLOYEE.LASTNAME',3,'',4)" />
        <asp:textbox runat="server" style="display:none" rmxref="/Instance/EventXIntervention/EmpEid/@codeid" id="lastfirstname_cid" cancelledvalue="" />
                    
        
        </span>
        
        <span class="label">Department</span><span class="formw"><asp:textbox runat="server"
                                        onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="deptassignedeid"
                                        rmxref="Instance/EventXIntervention/DeptEid" rmxtype="orgh" name="deptassignedeid"
                                        cancelledvalue="" /><asp:button runat="server" class="CodeLookupControl" text="..."
                                            id="deptassignedeidbtn" onclientclick="return selectCode('orgh','deptassignedeid','Department');" /><asp:textbox
                                                style="display: none" runat="server" id="deptassignedeid_cid" rmxref="Instance/EventXIntervention/DeptEid/@codeid"
                                                cancelledvalue="" />
        </span></div>
                                                
        <div runat="server" class="half" id="div_supervisoreid" xmlns="">
            <span class="label">Supervisor</span><span class="formw"><asp:textbox runat="server"
                onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="supervisoreid"
                rmxref="/Instance/EventXIntervention/SupEid" rmxtype="eidlookup" cancelledvalue=""
                tabindex="3" />
                <input type="button" class="button" value="..." name="supervisoreidbtn"
                    tabindex="4" onclick="lookupData('supervisoreid','EMPLOYEES',4,'supervisoreid',2)" /><asp:textbox
                        style="display: none" runat="server" id="supervisoreid_cid" rmxref="/Instance/EventXIntervention/SupEid/@codeid"
                        cancelledvalue="" />
        </span><%--</div>--%>
        <%--<div runat="server" class="half" id="div_chfcomplaint" xmlns="">--%>
            <span class="label">Chief Complaint</span><span class="formw"><asp:textbox runat="server"
                onchange="lookupTextChanged(this);" id="chfcomplaint" rmxref="Instance/EventXIntervention/CompCode"
                rmxtype="code" cancelledvalue="" tabindex="4" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="chfcomplaintbtn" onclientclick="return selectCode('COMPLAINT_CODE','chfcomplaint');"
                    tabindex="5" /><asp:textbox style="display: none" runat="server" id="chfcomplaint_cid"
                        rmxref="Instance/EventXIntervention/CompCode/@codeid" cancelledvalue="" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABevaluation"
        id="FORMTABevaluation">
        <div runat="server" class="half" id="div_status" xmlns="">
            <span class="label">Status</span><span class="formw"><asp:textbox runat="server"
                onchange="lookupTextChanged(this);" id="status" rmxref="Instance/EventXIntervention/Status"
                rmxtype="code" cancelledvalue="" tabindex="5" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="statusbtn" onclientclick="return selectCode('INTERVENT_STATUS','status');"
                    tabindex="6" /><asp:textbox style="display: none" runat="server" id="status_cid"
                        rmxref="Instance/EventXIntervention/Status/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_interventionlist" xmlns="">
            <span class="label">Interventions</span><span class="formw"><asp:listbox runat="server"
                size="3" id="interventionlist" rmxref="/Instance/EventXIntervention/EventXIntCodes"
                rmxtype="codelist" style="" tabindex="71" /><input type="button" id="interventionlistbtn"
                    tabindex="72" class=" CodeLookupControl" onclick="selectCode('INTERVENTION_CODE','interventionlist')" /><input
                        type="button" class="button" id="interventionlistbtndel" tabindex="73" onclick="deleteSelCode('interventionlist')"
                        value="-" /><asp:textbox runat="server" style="display: none" rmxref="/Instance/EventXIntervention/EventXIntCodes/@codeid"
                            id="interventionlist_lst" /></span></div>
        <div runat="server" class="half" id="div_evalentityid" xmlns="">
            <span class="label">Evaluator</span><span class="formw"><asp:textbox runat="server"
                onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="evalentityid"
                rmxref="/Instance/EventXIntervention/EvalEid" rmxtype="eidlookup" cancelledvalue=""
                tabindex="1" /><input type="button" class="button" value="..." name="evalentityidbtn"
                    tabindex="2" onclick="lookupData('evalentityid','ANY',4,'evalentityid',2)" /><asp:textbox
                        style="display: none" runat="server" id="evalentityid_cid" rmxref="/Instance/EventXIntervention/EvalEid/@codeid"
                        cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_referal" xmlns="">
            <span class="label">Referral Source</span><span class="formw"><asp:textbox runat="server"
                onchange="lookupTextChanged(this);" id="referal" rmxref="Instance/EventXIntervention/RefCode"
                rmxtype="code" cancelledvalue="" tabindex="5" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="referalbtn" onclientclick="return selectCode('REFERRAL_SOURCE','referal');"
                    tabindex="6" /><asp:textbox style="display: none" runat="server" id="referal_cid"
                        rmxref="Instance/EventXIntervention/RefCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_dateentered" xmlns="">
            <span class="label">Request Received</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="dateentered" rmxref="/Instance/EventXIntervention/DtRec"
                rmxtype="date" tabindex="2" /><cc1:CalendarExtender runat="server" ID="dateentered_ajax"
                    TargetControlID="dateentered" />
            </span>
        </div>
        <div runat="server" class="half" id="div_antcost" xmlns="">
            <span class="label">Anticipated Cost</span><span class="formw"><asp:textbox runat="server"
                id="antcost" rmxref="/Instance/EventXIntervention/AntiCost" rmxtype="currency"
                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="2" onchange=";setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_evalcomp" xmlns="">
            <span class="label">Evaluation Completed</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="evalcomp" rmxref="/Instance/EventXIntervention/DateComplete"
                rmxtype="date" tabindex="2" /><cc1:CalendarExtender runat="server" ID="evalcomp_ajax"
                    TargetControlID="evalcomp" />
            </span>
        </div>
        <div runat="server" class="half" id="div_recsent" xmlns="">
            <span class="label">Recommendation Sent</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="recsent" rmxref="/Instance/EventXIntervention/DateSent"
                rmxtype="date" tabindex="2" /><cc1:CalendarExtender runat="server" ID="recsent_ajax"
                    TargetControlID="recsent" />
            </span>
        </div>
        <div runat="server" class="half" id="div_recresp" xmlns="">
            <span class="label">Recommendation Response</span><span class="formw"><asp:textbox
                runat="server" formatas="date" onchange="setDataChanged(true);" id="recresp"
                rmxref="/Instance/EventXIntervention/DateResp" rmxtype="date" tabindex="2" /><cc1:CalendarExtender
                    runat="server" ID="recresp_ajax" TargetControlID="recresp" />
            </span>
        </div>
        <div runat="server" class="half" id="div_noncmpres" xmlns="">
            <span class="label">Non-Compliance reason</span><span class="formw"><asp:textbox
                runat="server" onchange="lookupTextChanged(this);" id="noncmpres" rmxref="Instance/EventXIntervention/NonComp"
                rmxtype="code" cancelledvalue="" tabindex="5" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="noncmpresbtn" onclientclick="return selectCode('NONCOMPLIANCE_CODE','noncmpres');"
                    tabindex="6" /><asp:textbox style="display: none" runat="server" id="noncmpres_cid"
                        rmxref="Instance/EventXIntervention/NonComp/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_intervendescription" xmlns="">
            <span class="label">Intervention Description</span><span class="formw"><asp:textbox
                runat="Server" id="intervendescription" rmxref="Instance/EventXIntervention/InvDesc"
                rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                tabindex="33" textmode="MultiLine" columns="30" rows="3" /><input type="button" class="button"
                    value="..." name="intervendescriptionbtnMemo" tabindex="34" id="intervendescriptionbtnMemo"
                    onclick="EditHTMLMemo('intervendescription','')" /><asp:textbox style="display: none"
                        runat="server" rmxref="Instance/EventXIntervention/InvDesc_HTMLComments" id="intervendescription_HTML" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABtande"
        id="FORMTABtande">
        <div runat="server" class="half" id="div_rate" xmlns="">
            <span class="label">Rate</span><span class="formw"><asp:textbox runat="server" id="rate"
                rmxref="/Instance/EventXIntervention/Rate" rmxtype="currency" onblur=";currencyLostFocus(this);"
                rmxforms:as="currency" tabindex="2" onchange="CalculateFlatFee();;setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_hours" xmlns="">
            <span class="label">Hours</span><span class="formw"><asp:textbox runat="server" onblur="numLostFocus(this);"
                id="hours" rmxref="/Instance/EventXIntervention/NoHours" rmxtype="numeric" tabindex="2"
                onchange="CalculateFlatFee();" /></span></div>
        <div runat="server" class="half" id="div_totalflatfee" xmlns="">
            <span class="label">Total/Flat Fee</span><span class="formw"><asp:textbox runat="server"
                id="totalflatfee" rmxref="/Instance/EventXIntervention/TotalFlatAmt" rmxtype="currency"
                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="2" onchange=";setDataChanged(true);" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup">
        <asp:textbox style="display: none" runat="server" id="supp_intervent_row_id" rmxref="/Instance/*/Supplementals/INTERVENT_ROW_ID"
            rmxtype="id" /></div>
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
            <asp:button class="button" runat="Server" id="btnBack" text="Back to Event" onclientclick="return XFormHandler('','1','back','');" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="EventXIntervention" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" name="SysSerializationConfig" text="&lt;EventXIntervention&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/EventXIntervention&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                rmxtype="hidden" text="eventintervention" /><asp:textbox style="display: none" runat="server"
                                    id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId" rmxtype="hidden"
                                    text="" /><asp:textbox style="display: none" runat="server" id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId"
                                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysSid"
                                            rmxref="Instance/UI/FormVariables/SysSid" rmxtype="hidden" text="12650" /><asp:textbox
                                                style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
                                                rmxtype="hidden" text="evinterrowid" /><asp:textbox style="display: none" runat="server"
                                                    id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden"
                                                    text="eventid" /><asp:textbox style="display: none" runat="server" id="SysFormPForm"
                                                        rmxref="Instance/UI/FormVariables/SysFormPForm" rmxtype="hidden" text="event" /><asp:textbox
                                                            style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                                            rmxtype="hidden" text="eventnumber" /><asp:textbox style="display: none" runat="server"
                                                                id="SysNotReqNew" rmxref="Instance/UI/FormVariables/SysNotReqNew" rmxtype="hidden"
                                                                text="" /><asp:textbox style="display: none" runat="server" name="formname" value="eventintervention" /><asp:textbox
                                                                    style="display: none" runat="server" name="SysRequired" value="" /><asp:textbox style="display: none"
                                                                        runat="server" name="SysFocusFields" value="dateentered|antcost|evalcomp|recsent|recresp|rate|hours|totalflatfee|" /><input
                                                                            type="hidden" id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible"
                                                                                style="display: none" /><asp:textbox runat="server" id="SysLookupClass" style="display: none" /><asp:textbox
                                                                                    runat="server" id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server"
                                                                                        id="SysLookupAttachNodePath" style="display: none" /><asp:textbox runat="server"
                                                                                            id="SysLookupResultConfig" style="display: none" /><input type="hidden" value="rmx-widget-handle-2"
                                                                                                id="SysWindowId" /></form>
</body>
</html>
