﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.FDM
{
    public partial class Entityxcontactinfo : GridPopupBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
//(Chubb Ack)
            // akaushik5 Added for MITS 38020 Starts
            if (!object.ReferenceEquals(Page.FindControl("TABSautonotification"), null))
            {
                // akaushik5 Added for MITS 38020 Ends
                if (AppHelper.GetQueryStringValue("ParentName").Contains("orghierarchymaint.aspx"))
                {
                    Page.FindControl("TABSautonotification").Visible = true;
                }
                else
                {
                    Page.FindControl("TABSautonotification").Visible = false;
                }
                // akaushik5 Added for MITS 38020 Starts
            }
            // akaushik5 Added for MITS 38020 Ends

            GridPopupPageload();
            //pmittal5 Mits 18623 11/24/09 - Set Onchange attribute to checkboxes
            if (!IsPostBack)
            {
                if (Page.FindControl("EmailAck")!= null)
                    ((CheckBox)Page.FindControl("EmailAck")).InputAttributes.Add("onchange", "return setDataChanged(true);");
                if (Page.FindControl("EmailFroi") != null)
                    ((CheckBox)Page.FindControl("EmailFroi")).InputAttributes.Add("onchange", "return setDataChanged(true);");
                if (Page.FindControl("EmailAcord") != null)
                    ((CheckBox)Page.FindControl("EmailAcord")).InputAttributes.Add("onchange", "return setDataChanged(true);");
            }
            //End - pmittal5
        }
        //added By NAVDEEP -(Chubb Ack)
        //Add the codes to List Box while opening it from Grid
        
        public override void ModifyXml(ref XmlDocument p_objInputXml)
        {
            XmlElement objElement = null;
            XmlNode objNode = null;
            string sNodeText = "";
            if (((TextBox)FindControlRecursive(this.Form, "gridname")).Text == "EntityXContactInfoGrid")
            {
                objNode = p_objInputXml.SelectSingleNode("//LineofBusList");                
                if (objNode != null)
                {
                    sNodeText = objNode.InnerText;
                    objNode.InnerText = "";
                    if (sNodeText.Contains("GC"))
                    {
                        objElement = p_objInputXml.CreateElement("Item");
                        objElement.SetAttribute("value", "241");
                        objElement.InnerText = "GC General Claims";
                        objNode.AppendChild(objElement);
                        objNode.Attributes.Item(0).Value = objNode.Attributes.Item(0).Value + (objNode.Attributes.Item(0).Value.Contains("241")?"":"241 ");
                    }
                    if (sNodeText.Contains("VA"))
                    {
                        objElement = p_objInputXml.CreateElement("Item");
                        objElement.SetAttribute("value", "242");
                        objElement.InnerText = "VA Vehicle Accident Claims";
                        objNode.AppendChild(objElement);
                        objNode.Attributes.Item(0).Value = objNode.Attributes.Item(0).Value + (objNode.Attributes.Item(0).Value.Contains("242") ? "" : "242 ");
                    }
                    if (sNodeText.Contains("WC"))
                    {
                        objElement = p_objInputXml.CreateElement("Item");
                        objElement.SetAttribute("value", "243");
                        objElement.InnerText = "WC Workers' Compensation";
                        objNode.AppendChild(objElement);
                        objNode.Attributes.Item(0).Value = objNode.Attributes.Item(0).Value + (objNode.Attributes.Item(0).Value.Contains("243") ? "" : "243 ");
                    }
                    if (sNodeText.Contains("DI"))
                    {
                        objElement = p_objInputXml.CreateElement("Item");
                        objElement.SetAttribute("value", "844");
                        objElement.InnerText = "DI Non-Occupational Claims";
                        objNode.AppendChild(objElement);
                        objNode.Attributes.Item(0).Value = objNode.Attributes.Item(0).Value + (objNode.Attributes.Item(0).Value.Contains("844") ? "" : "844 ");
                    }
                    //Start: Neha Suresh Jain, MITS 20811, 06/03/2010, to show property claim PC in contact info>Froi/ACCORD/Letters tab
                    if (sNodeText.Contains("PC"))
                    {
                        objElement = p_objInputXml.CreateElement("Item");
                        objElement.SetAttribute("value", "845");
                        objElement.InnerText = "PC Property Claims";
                        objNode.AppendChild(objElement);
                        objNode.Attributes.Item(0).Value = objNode.Attributes.Item(0).Value + (objNode.Attributes.Item(0).Value.Contains("845") ? "" : "845 ");
                    }
                    //End:Neha Suresh Jain
                    if (objNode.Attributes.Item(0).Value != "")
                    {
                        objNode.Attributes.Item(0).Value = objNode.Attributes.Item(0).Value.TrimEnd();
                    }
                }

            }
        }
        // End by NAVDEEP
    }
}
