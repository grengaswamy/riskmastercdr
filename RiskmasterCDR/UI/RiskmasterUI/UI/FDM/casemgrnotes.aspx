<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="casemgrnotes.aspx.cs" Inherits="Riskmaster.UI.FDM.Casemgrnotes"
    ValidateRequest="false" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Case Manager Notes</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="../../Images/new.gif"
                        width="28" height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='../../Images/new2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/new.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="../../Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="../../Images/first.gif"
                        width="28" height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='../../Images/first2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/first.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="../../Images/prev.gif"
                        width="28" height="28" border="0" id="moveprevious" alternatetext="Move Previous"
                        onmouseover="this.src='../../Images/prev2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/prev.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="../../Images/next.gif"
                        width="28" height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='../../Images/next2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/next.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="../../Images/last.gif"
                        width="28" height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='../../Images/last2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/last.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="../../Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='../../Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/delete.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="attach()" src="../../Images/attach.gif"
                        width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                        onmouseover="this.src='../../Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/attach.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="FilteredDiary()" src="../../Images/filtereddiary.gif"
                        width="28" height="28" border="0" id="filtereddiary" alternatetext="View Record Diaries"
                        onmouseover="this.src='../../Images/filtereddiary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/filtereddiary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Diary()" src="../../Images/diary.gif"
                        width="28" height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='../../Images/diary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/diary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="../../Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Case Manager Notes" /><asp:label id="formsubtitle"
            runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSCMgrNotes" id="TABSCMgrNotes">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="CMgrNotes"
                id="LINKTABSCMgrNotes"><span style="text-decoration: none">Case Manager Notes</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABCMgrNotes" id="FORMTABCMgrNotes">
        <asp:textbox style="display: none" runat="server" id="cmgrnotesrowid" rmxref="/Instance/CaseMgrNotes/CmgrNotesRowId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="casemgrrowid"
                rmxref="/Instance/CaseMgrNotes/CasemgrRowId" rmxtype="id" /><div runat="server" class="half"
                    id="div_enteredbyuser" xmlns="">
                    <asp:label runat="server" class="label" id="lbl_enteredbyuser" text="Entered By User" /><span
                        class="formw"><asp:textbox runat="server" onchange="setDataChanged(true);" id="enteredbyuser"
                            rmxref="/Instance/CaseMgrNotes/EnteredByUser" rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_dateentered" xmlns="">
            <asp:label runat="server" class="label" id="lbl_dateentered" text="Date Entered" /><span
                class="formw"><asp:textbox runat="Server" rmxref="/Instance/CaseMgrNotes/DateEntered"
                    id="dateentered" tabindex="2" style="background-color: silver;" readonly="true" /></span></div>
        <div runat="server" class="half" id="div_TimeEntered" xmlns="">
            <asp:label runat="server" class="label" id="lbl_TimeEntered" text="Time Entered" /><span
                class="formw"><asp:textbox runat="Server" rmxref="/Instance/CaseMgrNotes/TimeEntered"
                    id="TimeEntered" tabindex="7" style="background-color: silver;" readonly="true" /></span></div>
        <div runat="server" class="half" id="div_notestypecode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_notestypecode" text="Dated Notes Type" /><span
                class="formw"><asp:textbox runat="server" onchange="lookupTextChanged(this);" id="notestypecode"
                    rmxref="/Instance/CaseMgrNotes/NotesTypeCode" rmxtype="code" cancelledvalue=""
                    tabindex="3" onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl"
                        runat="Server" id="notestypecodebtn" onclientclick="return selectCode('CASE_NOTES_TYPE','notestypecode');"
                        tabindex="4" /><asp:textbox style="display: none" runat="server" id="notestypecode_cid"
                            rmxref="/Instance/CaseMgrNotes/NotesTypeCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_cmagernotes" xmlns="">
            <asp:label runat="server" class="label" id="lbl_cmagernotes" text="Case Manager Notes" /><span
                class="formw"><asp:textbox runat="Server" id="cmagernotes" rmxref="/Instance/CaseMgrNotes/CmgrNotes"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="5" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                        value="..." name="cmagernotesbtnMemo" tabindex="6" id="cmagernotesbtnMemo" onclick="EditMemo('cmagernotes','')" /></span></div>
    </div>
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
            <asp:button class="button" runat="Server" id="btnBack" text="Back to Case Manager History"
                onclientclick="if(!( XFormHandler('','1','back')))return false;" postbackurl="?" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="CaseMgrNotes" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" text="&lt;CaseMgrNotes&gt;&lt;/CaseMgrNotes&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                rmxtype="hidden" text="CaseMgrNotes" /><asp:textbox style="display: none" runat="server"
                                    id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden"
                                    text="" /><asp:textbox style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
                                        rmxtype="hidden" text="cmgrnotesrowid" /><asp:textbox style="display: none" runat="server"
                                            id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId" rmxtype="hidden"
                                            text="" /><asp:textbox style="display: none" runat="server" id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName"
                                                rmxtype="hidden" text="cmcmhrowid" /><asp:textbox style="display: none" runat="server"
                                                    id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid" rmxtype="hidden" text="" /><asp:textbox
                                                        style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                                        rmxtype="hidden" text="11150" /><asp:textbox style="display: none" runat="server"
                                                            id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType" rmxtype="hidden"
                                                            text="" /><asp:textbox style="display: none" runat="server" name="formname" value="CaseMgrNotes" /><asp:textbox
                                                                style="display: none" runat="server" name="SysRequired" value="" /><asp:textbox style="display: none"
                                                                    runat="server" name="SysFocusFields" value="enteredbyuser|" /><input type="hidden"
                                                                        id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible" style="display: none" /><asp:textbox
                                                                            runat="server" id="SysLookupClass" style="display: none" /><asp:textbox runat="server"
                                                                                id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server" id="SysLookupAttachNodePath"
                                                                                    style="display: none" /><asp:textbox runat="server" id="SysLookupResultConfig" style="display: none" /><input
                                                                                        type="hidden" value="rmx-widget-handle-2" id="SysWindowId" /><asp:textbox style="display: none" runat="server" id="cmcmhrowid" rmxref="/Instance/UI/FormVariables/SysExData/CmcmhRowId"
            rmxtype="id" /></form>
</body>
</html>
