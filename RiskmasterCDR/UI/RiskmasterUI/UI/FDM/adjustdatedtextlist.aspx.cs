﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.FDM
{
    public partial class AdjustDatedTextList : FDMBasePage
    {
        public IEnumerable result = null;
        public XElement rootElement = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                LoadInitialPage();
                rootElement = XElement.Parse(Data.OuterXml);
                if (rootElement.XPathSelectElement("//Param/AdjustDatedTextList/AdjustDatedText") != null)
                {
                    result = from c in rootElement.XPathSelectElements("//Param/AdjustDatedTextList/AdjustDatedText")
                             let xClaim = (int)Convert.ToInt64(c.Element("AdjDttextRowId").Value)
                             orderby xClaim descending
                             select c;
                   
                    if (rootElement.XPathSelectElement("//AdjustDatedText/AdjRowId") != null)
                    {
                        adjrowid.Text = rootElement.XPathSelectElement("//AdjustDatedText/AdjRowId").Value;
                    }
                    RecordExists.Value = "True";
                }
                else
                {
                    RecordExists.Value = "False";
                }
                if (rootElement.XPathSelectElement("//SysExData/TextTypeCode") != null)
                {
                    adjtexttypecode.CodeText = rootElement.XPathSelectElement("//SysExData/TextTypeCode").Value;
                }
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }

    }
}
