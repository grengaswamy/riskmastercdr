﻿//Author    : Mridul Bansal
//Date      : 10/19/09
//MITS      : 18230
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster;
namespace Riskmaster.UI.FDM
{
    public partial class Propertyunit : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
           
        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

        }

        protected void Page_PreRender()
        {
            if (this.Page.FindControl("squarefootage") != null)
            {
                if (this.Page.FindControl("squarefootage").GetType().Name == "TextBox")
                {
                    if (((TextBox)this.Page.FindControl("squarefootage")).Text == "0")
                    {
                        ((TextBox)this.Page.FindControl("squarefootage")).Text = "";
                    }
                }
                else
                {
                    if (((Label)this.Page.FindControl("squarefootage")).Text == "0")
                    {
                        ((Label)this.Page.FindControl("squarefootage")).Text = "";
                    }
                }
            }

            if (this.Page.FindControl("noofstories") != null)
            {
                if (this.Page.FindControl("noofstories").GetType().Name == "TextBox")
                {
                    if (((TextBox)this.Page.FindControl("noofstories")).Text == "0")
                    {
                        ((TextBox)this.Page.FindControl("noofstories")).Text = "";
                    }
                }
                else
                {
                    if (((Label)this.Page.FindControl("noofstories")).Text == "0")
                    {
                        ((Label)this.Page.FindControl("noofstories")).Text = "";
                    }
                }
            }

            if (this.Page.FindControl("avgstoryheight") != null)
            {
                if (this.Page.FindControl("avgstoryheight").GetType().Name == "TextBox")
                {
                    if (((TextBox)this.Page.FindControl("avgstoryheight")).Text == "0")
                    {
                        ((TextBox)this.Page.FindControl("avgstoryheight")).Text = "";
                    }
                }
                else
                {
                    if (((Label)this.Page.FindControl("avgstoryheight")).Text == "0")
                    {
                        ((Label)this.Page.FindControl("avgstoryheight")).Text = "";
                    }
                }
            }

            if (this.Page.FindControl("yearofconstruction") != null)
            {
                if (this.Page.FindControl("yearofconstruction").GetType().Name == "TextBox")
                {
                    if (((TextBox)this.Page.FindControl("yearofconstruction")).Text == "0")
                    {
                        ((TextBox)this.Page.FindControl("yearofconstruction")).Text = "";
                    }
                }
                else
                {
                    if (((Label)this.Page.FindControl("yearofconstruction")).Text == "0")
                    {
                        ((Label)this.Page.FindControl("yearofconstruction")).Text = "";
                    }
                }
            }

            if (this.Page.FindControl("gpsaltitude") != null)
            {
                if (this.Page.FindControl("gpsaltitude").GetType().Name == "TextBox")
                {
                    if (((TextBox)this.Page.FindControl("gpsaltitude")).Text == "0")
                    {
                        ((TextBox)this.Page.FindControl("gpsaltitude")).Text = "";
                    }
                }
                else
                {
                    if (((Label)this.Page.FindControl("gpsaltitude")).Text == "0")
                    {
                        ((Label)this.Page.FindControl("gpsaltitude")).Text = "";
                    }
                }
            }

            if (this.Page.FindControl("gpslongitude") != null)
            {
                if (this.Page.FindControl("gpslongitude").GetType().Name == "TextBox")
                {
                    if (((TextBox)this.Page.FindControl("gpslongitude")).Text == "0")
                    {
                        ((TextBox)this.Page.FindControl("gpslongitude")).Text = "";
                    }
                }
                else
                {
                    if (((Label)this.Page.FindControl("gpslongitude")).Text == "0")
                    {
                        ((Label)this.Page.FindControl("gpslongitude")).Text = "";
                    }
                }
            }

            if (this.Page.FindControl("gpslatitude") != null)
            {
                if (this.Page.FindControl("gpslatitude").GetType().Name == "TextBox")
                {
                    if (((TextBox)this.Page.FindControl("gpslatitude")).Text == "0")
                    {
                        ((TextBox)this.Page.FindControl("gpslatitude")).Text = "";
                    }
                }
                else
                {
                    if (((Label)this.Page.FindControl("gpslatitude")).Text == "0")
                    {
                        ((Label)this.Page.FindControl("gpslatitude")).Text = "";
                    }
                }
            }
        }

    }
}
