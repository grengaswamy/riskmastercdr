﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.FDM
{
    public partial class ClaimAdjusterList :FDMBasePage
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
     
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadInitialPage();
                rootElement = XElement.Parse(Data.OuterXml);
                if (rootElement.XPathSelectElement("//Param/ClaimAdjusterList/ClaimAdjuster") != null)
                {
                    result = from c in rootElement.XPathSelectElements("//Param/ClaimAdjusterList/ClaimAdjuster")
                             let xClaim = (int)Convert.ToInt64(c.Element("AdjRowId").Value)
                             orderby xClaim descending
                             select c;
                    if (rootElement.XPathSelectElement("//ClaimAdjuster/ClaimId") != null)
                    {
                        claimid.Text = rootElement.XPathSelectElement("//ClaimAdjuster/ClaimId").Value;
                    }
                    RecordExists.Value = "True";
                }
                else
                {
                    RecordExists.Value = "False";
                }
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }
        
            
    }
}
