﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
//Sumit - Start(04/27/2010) - MITS# 20483
using System.Text;
//Sumit - End

namespace Riskmaster.UI.FDM
{
    public partial class propertyuardetails : GridPopupBase
    {
        private const string m_FUNCTION_CALL_POPULATE_UAR = "EnhancePolicyAdaptor.PopulateSearchPropUAR";
        //Sumit - Start(04/27/2010) - MITS# 20483
        private const string m_FUNCTION_CALL_SET_SCHEDULE = "EnhancePolicyAdaptor.SetSchedule";
        private const string m_FUNCTION_CALL_UPDATE_SCHEDULE = "EnhancePolicyAdaptor.UpdateScheduleList";
        //Sumit - End
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            string sreturnValue = string.Empty;

            //Sumit: Start(10/19/2010) - MITS# 21996
            int iCategoryFieldID = 0;
            if (AppHelper.GetQueryStringValue("CategoryFieldID") != "")
                iCategoryFieldID = Int32.Parse(AppHelper.GetQueryStringValue("CategoryFieldID"));

            HtmlInputButton btnpropertyidcode = (HtmlInputButton)this.FindControl("propertyidcodebtn");
            if(btnpropertyidcode!=null)
                btnpropertyidcode.Attributes.Add("onclick","return searchUAR('propertyunit','" + iCategoryFieldID.ToString()+"')");
            //End:Sumit

            if (!IsPostBack)
            {
                int iUARId = 0;
                if (AppHelper.GetQueryStringValue("selectedid") != "")
                    iUARId = Int32.Parse(AppHelper.GetQueryStringValue("selectedid"));
                TextBox txtScheduleUARId = (TextBox)this.FindControl("ScheduleListGrid_EditedRowIds");
                if (txtScheduleUARId != null)
                {
                    txtScheduleUARId.Text = iUARId.ToString();
                }
                string sSessionId = string.Empty;
                if (AppHelper.GetQueryStringValue("sessionid") != "")
                    sSessionId = AppHelper.GetQueryStringValue("sessionid");
                string sdeletedrowid = string.Empty;
                if (AppHelper.GetQueryStringValue("deletedrowid") != "")
                    sdeletedrowid = AppHelper.GetQueryStringValue("deletedrowid");

                string sScheduleAddButtonEnabled = string.Empty;
                if (AppHelper.GetQueryStringValue("scheduleAddButtonEnabled") != "")
                    sScheduleAddButtonEnabled = AppHelper.GetQueryStringValue("scheduleAddButtonEnabled");
                TextBox txtScheduleAddButtonEnabled = (TextBox)this.FindControl("ScheduleAddButtonEnabled");
                txtScheduleAddButtonEnabled.Text = sScheduleAddButtonEnabled;

                string sScheduleEditButtonEnabled = string.Empty;
                if (AppHelper.GetQueryStringValue("scheduleEditButtonEnabled") != "")
                    sScheduleEditButtonEnabled = AppHelper.GetQueryStringValue("scheduleEditButtonEnabled");
                TextBox txtScheduleEditButtonEnabled = (TextBox)this.FindControl("ScheduleEditButtonEnabled");
                txtScheduleEditButtonEnabled.Text = sScheduleEditButtonEnabled;

                string sScheduleDeleteButtonEnabled = string.Empty;
                if (AppHelper.GetQueryStringValue("scheduleDeleteButtonEnabled") != "")
                    sScheduleDeleteButtonEnabled = AppHelper.GetQueryStringValue("scheduleDeleteButtonEnabled");
                TextBox txtScheduleDeleteButtonEnabled = (TextBox)this.FindControl("ScheduleDeleteButtonEnabled");
                txtScheduleDeleteButtonEnabled.Text = sScheduleDeleteButtonEnabled;
               //Anu Tennyson For MITS 18229 Starts
                if (!String.IsNullOrEmpty(sSessionId))
                {
                    string[] sScheduleSessionId = sSessionId.Split('^');
                    TextBox txtScheduleSessionId = (TextBox)this.FindControl("ScheduleListGrid_ScheduleSessionId");
                    if (txtScheduleSessionId != null && sScheduleSessionId.Length > 1)
                    {
                        txtScheduleSessionId.Text = sScheduleSessionId[1];
                    }
                    TextBox txtSelectedSessionIds = (TextBox)this.FindControl("PCUARListGrid_SelectedSessionIds");
                    if (txtSelectedSessionIds != null)
                    {
                        txtSelectedSessionIds.Text = sSessionId;
                    }
                }
                //Anu Tennyson For MITS 18229 Ends
                TextBox txtGridName = (TextBox)this.FindControl("gridname");
                if (txtGridName != null)
                {
                    txtGridName.Text = AppHelper.GetQueryStringValue("gridname");
                }

                TextBox txtmode = (TextBox)this.FindControl("mode");
                if (txtmode != null)
                {
                    txtmode.Text = AppHelper.GetQueryStringValue("mode");
                }

                TextBox txtselectedrowposition = (TextBox)this.FindControl("selectedrowposition");
                if (txtselectedrowposition != null)
                {
                    txtselectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                }

                TextBox txtUARId = (TextBox)this.FindControl("selectedid");
                if (txtUARId != null)
                {
                    txtUARId.Text = iUARId.ToString();
                }

                string shdnCodeId = string.Empty;
                if (AppHelper.GetQueryStringValue("CodeID") != "")
                {
                    shdnCodeId = AppHelper.GetQueryStringValue("CodeID");
                    hdnCodeID.Text = shdnCodeId;
                }
                //Sumit - Start(04/27/2010) - MITS# 20483
                XmlTemplate = GetLoadMessageTemplate(iUARId, sSessionId, sdeletedrowid);
                bReturnStatus = CallCWS(m_FUNCTION_CALL_POPULATE_UAR, XmlTemplate, out sreturnValue, false, true);
                //Sumit - End
            }
            else
            {
                TextBox txtFunctionToCall = (TextBox)this.FindControl("FunctionToCall");
                bool bGetConrolData = false;

                if (txtFunctionToCall.Text.Trim() != "")
                {
                    if (txtFunctionToCall.Text != m_FUNCTION_CALL_POPULATE_UAR)
                        bGetConrolData = true;

                    XmlTemplate = null;
                    if (txtFunctionToCall.Text != m_FUNCTION_CALL_SET_SCHEDULE)
                        bReturnStatus = CallCWS(txtFunctionToCall.Text, XmlTemplate, out sreturnValue, bGetConrolData, true);
                }

                TextBox txthdnOkEnabled = (TextBox)this.FindControl("hdnOkEnabled");
                if (txthdnOkEnabled != null)
                {
                    txthdnOkEnabled.Text = "true";
                }
            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            TextBox txtFunctionToCall = (TextBox)this.FindControl("FunctionToCall");

            if (txtFunctionToCall.Text == m_FUNCTION_CALL_POPULATE_UAR)
            {
                XElement xmlPropertyUAR = Xelement.XPathSelectElement("./Document");
                XElement xmlTarget = new XElement("PropertyID");

                TextBox txtPropertyID = (TextBox)this.FindControl("PropertyID");
                string sPropertyID = txtPropertyID.Text;

                xmlTarget.Value = sPropertyID;

                if (xmlPropertyUAR != null)
                    xmlPropertyUAR.Add(xmlTarget);
            }

            //Sumit - Start(04/27/2010) - MITS# 20483

            if (txtFunctionToCall.Text == m_FUNCTION_CALL_UPDATE_SCHEDULE)
            {
                XElement xmlProperty = Xelement.XPathSelectElement("./Document/PolicyXUar");
                XElement xmlTarget = new XElement("UarRowID");

                TextBox txtUarRowID = (TextBox)this.FindControl("selectedid");
                string sUarRowID = txtUarRowID.Text;

                xmlTarget.Value = sUarRowID;

                if (xmlProperty != null)
                    xmlProperty.Add(xmlTarget);
            }


            TextBox txtSchdEditSessionId = (TextBox)this.FindControl("ScheduleListGrid_EditRowSessionId");
            TextBox txtPostBackAction = (TextBox)this.FindControl("PostBackAction");
            TextBox txtSchdAction = (TextBox)this.FindControl("ScheduleListGridAction");
            if (string.Compare(txtPostBackAction.Text, "SCHEDULE_EDIT", StringComparison.OrdinalIgnoreCase) == 0 && string.Compare(txtSchdAction.Text, "edit", StringComparison.OrdinalIgnoreCase) == 0)
            {
                string[] sScheduleSessions = txtSchdEditSessionId.Text.Split(new char[] { '|' });
                XElement objSession = Xelement.XPathSelectElement("./Document/PolicyXUar/ScheduleList/option/ScheduleRowId[.=\"" + sScheduleSessions[0] + "\"]/../SessionId");
                if (objSession != null && sScheduleSessions[1] != "")
                    objSession.Value = sScheduleSessions[1];

            }
            //Sumit - End
        }

        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 03/16/2010
        /// MITS : 18229
        /// Description : This function will set the Session ID and refresh the parent page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUAROk_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = "";
            TextBox txtSessionId;
            bReturnStatus = CallCWS("EnhancePolicyAdaptor.SetUAR", XmlTemplate, out sreturnValue, true, false);
            if (bReturnStatus)
            {
                // Set the Session Id
                XmlDoc.LoadXml(sreturnValue);
                txtSessionId = (TextBox)this.FindControl("SessionId");
                txtSessionId.Text = XmlDoc.SelectSingleNode("//Document/SetUAR/SessionId").InnerText;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "UAROkClick", "<script>fnRefreshParentUAROk();</script>");
            }
        }


        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 04/27/2010
        /// MITS : 20483
        /// Description : This function will set the template.
        /// </summary>
        /// <param name="iPropertyId"></param>
        /// <param name="sSessionId"></param>
        /// <returns></returns>
        private XElement GetLoadMessageTemplate(int iPropertyId, string sSessionId, string sdeletedrowid)
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.PopulateSearchPropUAR</Function></Call><Document><PolicyXUar><PropertyID>");
            sXml = sXml.Append(iPropertyId);
            sXml = sXml.Append("</PropertyID><SessionId>");
            sXml = sXml.Append(sSessionId);
            sXml = sXml.Append("</SessionId><DelRowID>");
            sXml = sXml.Append(sdeletedrowid);
            sXml = sXml.Append("</DelRowID></PolicyXUar></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
