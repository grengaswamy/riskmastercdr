<%@ Page="" Language="C#" AutoEventWireup="true" CodeBehind="adjuster.aspx.cs"  Inherits="Riskmaster.UI.FDM.Adjuster" ValidateRequest="false" %>
  <%@ Register="" assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
      <head id="Head1" runat="server">
        <title>Adjuster</title>
        <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
        <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
        <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />
        <script language="JavaScript" src="Scripts/form.js">
          {var i;}
        </script>
        <script language="JavaScript" src="Scripts/drift.js">
          {var i;}
        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
      </head>
      <body class="10pt" onload="pageLoaded();">
        <form name="frmData" id="frmData" runat="server">
          <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
          <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
          <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
          <asp:ScriptManager ID="SMgr" runat="server" />
          <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
            <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
              <tr>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateNew" src="Images/new.gif" width="28" height="28" border="0" id="new" AlternateText="New" onMouseOver="this.src='Images/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/new.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="Images/save.gif" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/save.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateFirst" src="Images/first.gif" width="28" height="28" border="0" id="movefirst" AlternateText="Move First" onMouseOver="this.src='Images/first2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/first.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigatePrev" src="Images/prev.gif" width="28" height="28" border="0" id="moveprevious" AlternateText="Move Previous" onMouseOver="this.src='Images/prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/prev.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateNext" src="Images/next.gif" width="28" height="28" border="0" id="movenext" AlternateText="Move Next" onMouseOver="this.src='Images/next2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/next.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateLast" src="Images/last.gif" width="28" height="28" border="0" id="movelast" AlternateText="Move Last" onMouseOver="this.src='Images/last2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/last.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateDelete" src="Images/delete.gif" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='Images/delete2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/delete.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="doLookup()" src="Images/lookup.gif" width="28" height="28" border="0" id="lookup" AlternateText="Lookup Data" onMouseOver="this.src='Images/lookup2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/lookup.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="Diary()" src="Images/diary.gif" width="28" height="28" border="0" id="diary" AlternateText="Diary" onMouseOver="this.src='Images/diary2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/diary.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="recordSummary()" src="Images/recordsummary.gif" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/recordsummary.gif';this.style.zoom='100%'" />
                </td>
              </tr>
            </table>
          </div>
          <br />
          <div class="msgheader" id="formtitle">Adjuster</div>
          <div class="errtextheader"></div>
          <table border="0">
            <tr>
              <td>
                <table border="0" cellspacing="0" celpadding="0">
                  <tr>
                    <td class="Selected" nowrap="true" name="TABSadjuster" id="TABSadjuster">
                      <a class="Selected" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="adjuster" id="LINKTABSadjuster">
                        <span style="text-decoration:none">Claim Adjuster Information</span>
                      </a>
                    </td>
                    <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
                    <td class="NotSelected" nowrap="true" name="TABSsuppgroup" id="TABSsuppgroup">
                      <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">
                        <span style="text-decoration:none">Supplementals</span>
                      </a>
                    </td>
                    <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
                    <td valign="top" nowrap="true" />
                  </tr>
                </table>
                <div style="position:relative;left:0;top:0;width:870px;height:350px;overflow:auto" class="singletopborder">
                  <table border="0" cellspacing="0" celpadding="0" width="95%" height="95%">
                    <tr>
                      <td valign="top">
                        <table border="0" name="FORMTABadjuster" id="FORMTABadjuster">
                          <tr>
                            <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="Instance/ClaimAdjuster/ClaimId" />
                            <asp:TextBox style="display:none" runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" />
                            <asp:TextBox style="display:none" runat="server" id="adjrowid" RMXRef="Instance/ClaimAdjuster/AdjRowId" />
                            <asp:TextBox style="display:none" runat="server" id="adjentityid" RMXRef="Instance/ClaimAdjuster/AdjusterEid/@codeid" />
                            <asp:TextBox style="display:none" runat="server" id="primaryClaimant" RMXRef="Instance/UI/FormVariables/SysExData/DiaryMessage" />
                            <td nowrap="true" id="adjlastname_ctlcol" xmlns="">
                              <b>
                                <u>Adjusters Last Name</u>
                              </b>
                            </td>
                            <td nowrap="true" xmlns="">
                              <asp:Textbox runat="server" size="30" onchange="lookupTextChanged(this);" tabindex="1" onblur="lookupLostFocus(this);" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/LastName" id="adjlastname" />
                              <asp:Textbox runat="server" style="display:none" Text="1" id="adjlastname_creatable" />
                              <input type="button" class="button" tabindex="2" id="adjlastnamebtn" onclick="lookupDataFDM('adjlastname','ADJUSTERS',4,'adj',1,'Instance/ClaimAdjuster/AdjusterEntity')" value="..." />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <asp:TextBox style="display:none" runat="server" id="hdCusListLookup" RMXRef="Instance/UI/FormVariables/SysExData/IsUpdate" />
                            <td nowrap="true" id="rmsysuser_ctlcol" xmlns="">RISKMASTER Login</td>
                            <td nowrap="true" xmlns="">Unknown Element Type: customizedlistlookup</td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="adjfirstname_ctlcol" xmlns="">First Name</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="adjfirstname" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/FirstName" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="adjtitle_ctlcol" xmlns="">Title</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="adjtitle" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/Title" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="adjmiddlename_ctlcol" xmlns="">Middle Name</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="adjmiddlename" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/MiddleName" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="currentadjflag_ctlcol" xmlns="">Current Adjuster</td>
                            <td nowrap="true" xmlns="">
                              <asp:CheckBox onchange="setDataChanged(true);" runat="Server" id="currentadjflag" RMXRef="Instance/ClaimAdjuster/CurrentAdjFlag" tabindex="15" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="adjaddr1_ctlcol" xmlns="">Address 1</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="adjaddr1" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/Addr1" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="adjphone1_ctlcol" xmlns="">Office Phone</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="adjphone1" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/Phone1" tabindex="16" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="adjaddr2_ctlcol" xmlns="">Address 2</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="adjaddr2" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/Addr2" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="adjphone2_ctlcol" xmlns="">Alt. Phone</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="adjphone2" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/Phone2" tabindex="17" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="adjcity_ctlcol" xmlns="">City</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="adjcity" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/City" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="adjfaxnumber_ctlcol" xmlns="">Fax</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="adjfaxnumber" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/FaxNumber" tabindex="18" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="adjstateid_ctlcol" xmlns="">State</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="adjstateid" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/StateId" cancelledvalue="" tabindex="8" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="adjstateidbtn" onclick="selectCode('states','adjstateid')" />
                              <asp:TextBox style="display:none" runat="server" id="adjstateid_cid" RMXref="Instance/ClaimAdjuster/AdjusterEntity/StateId/@codeid" cancelledvalue="" tabindex="9" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="adjzipcode_ctlcol" xmlns="">Zip/Postal Code</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="adjzipcode" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/ZipCode" TabIndex="19" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="adjcountrycode_ctlcol" xmlns="">Country</td>
                            <td nowrap="true" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="adjcountrycode" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/CountryCode" cancelledvalue="" tabindex="10" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="adjcountrycodebtn" onclick="selectCode('COUNTRY','adjcountrycode')" />
                              <asp:TextBox style="display:none" runat="server" id="adjcountrycode_cid" RMXref="Instance/ClaimAdjuster/AdjusterEntity/CountryCode/@codeid" cancelledvalue="" tabindex="11" />
                            </td>
                            <td xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                        </table>
                        <table border="0" style="display:none;" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
                          <tr>
                            <asp:TextBox style="display:none" runat="server" id="supp_adj_row_id" RMXRef="/Instance/*/Supplementals/ADJ_ROW_ID" />
                          </tr>
                        </table>
                      </td>
                      <td valign="top" />
                    </tr>
                  </table>
                </div>
                <table>
                  <tr>
                    <td>
                      <asp:Button id="Button1" class="button" runat="Server" Text="Dated Text" OnClientClick="&#xA;            XFormHandler('','SysFormPIdName=adjrowid&SysFormName=adjusterdatedtext&SysCmd=1&SysFormIdName=adjdttextrowid&SysEx=/Instance/ClaimAdjuster/AdjRowId','','')&#xA;          " />
                      <asp:Button id="Button2" class="button" runat="Server" Text="Back to Claim" OnClientClick="&#xA;            XFormHandler('','','1','back')&#xA;          " />
                    </td>
                  </tr>
                </table>
                <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" Text="Navigate" />
                <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" Text="ClaimAdjuster" />
                <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" name="SysSerializationConfig" Text="&lt;ClaimAdjuster&gt;&lt;AdjusterEntity&gt;&lt;/AdjusterEntity&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/ClaimAdjuster&gt;" />
                <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" Text="claimwc" />
                <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" Text="claimid" />
                <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" Text="claimid,claimnumber,adjrowid" />
                <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" Text="adjuster" />
                <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" Text="adjrowid" />
                <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" Text="3000" />
                <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" Text="3900" />
                <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" Text="" />
                <asp:TextBox style="display:none" runat="server" id="hdnUserId" RMXRef="Instance/ClaimAdjuster/AdjusterEntity/RMUserId" Text="" />
                <asp:TextBox style="display:none" runat="server" name="formname" value="adjuster" />
                <asp:TextBox style="display:none" runat="server" name="SysRequired" value="adjlastname|" />
                <asp:TextBox style="display:none" runat="server" name="SysFocusFields" value="adjlastname|" />
                <input type="hidden" id="hdSaveButtonClicked" />
                <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
                <asp:TextBox runat="server" id="SysLookupClass" style="display:none" />
                <asp:TextBox runat="server" id="SysLookupRecordId" style="display:none" />
                <asp:TextBox runat="server" id="SysLookupAttachNodePath" style="display:none" />
                <asp:TextBox runat="server" id="SysLookupResultConfig" style="display:none" />
                <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
              </td>
            </tr>
          </table>
        </form>
      </body>
    </html>