﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using System.IO;
using Riskmaster.Common;
using Riskmaster.AppHelpers;
using System.Threading;
using System.Globalization;
using HtmlAgilityPack;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.FDM
{
    public partial class RecordSummary : System.Web.UI.Page
    {
        private bool IsAdminTracking()
        {
            if (AppHelper.GetQueryStringValue("formname") != "")
            {
                if (AppHelper.GetQueryStringValue("formname").IndexOf("admintracking") >= 0)
                {
                    return true;
                }
            }
            return false;
        }
        internal static string SafeGetAttString(XmlNode objNode, string attName)
        {
            string ret = "";
            if (objNode.Attributes.GetNamedItem(attName) != null)
                ret = objNode.Attributes.GetNamedItem(attName).Value;
            return ret;
        }
        public string Display = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            SaxonImpl obj =null;
            XsltArgumentList xslArg =null;
            XslCompiledTransform xslt=null;
            XmlDocument docInstance = null;
            HtmlDocument htm = null;
            string Print = string.Empty;
            try
            {
                XElement oMessageElement = GetMessageTemplate();

                if (oMessageElement.XPathSelectElement("/ClientId") != null)//sharishkumar Jira 816
                oMessageElement.XPathSelectElement("/ClientId").Value = AppHelper.ClientId.ToString();//sharishkumar Jira 816
                //if (Session["PiRowId"] != null)
                //{
                //    oMessageElement.XPathSelectElement(".//ParamList/Param/FormVariables/SysExData/PiRowId").Value = Session["PiRowId"].ToString();
                //    Session["PiRowId"] = null;
                //}
                ModifyXml1(oMessageElement);
                XmlDocument doc = CallService(oMessageElement);
                
                //rupal:start, r8 multicurrency
               
                XmlElement xElm = (XmlElement)doc.SelectSingleNode("//SysExData/BaseCurrencyType");
                if (xElm != null)
                {
                    Culture = xElm.InnerText;
                    UICulture = xElm.InnerText;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(xElm.InnerText);
                    System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(xElm.InnerText);
                    TextBox currencytype = new TextBox();
                    currencytype.ID = "currencytype";
                    currencytype.Text = "|" + xElm.InnerText;
                    currencytype.Style.Add("display", "none");                   
                    
                }               
                //rupal:end


                OnUpdateForm(doc);
                XmlNode view = doc.SelectSingleNode("//SysView/form");
                
                XmlNodeList SysViewSection = doc.SelectNodes("//SysViewSection/section");

                // akaushik5 Added for MITS 36479 Starts
                XmlNodeList objOptions = view.OwnerDocument.SelectNodes(".//option");

                foreach (XmlNode objOption in objOptions)
                {
                    if (RecordSummary.SafeGetAttString(objOption, "type").Equals("new"))
                    {
                        objOption.RemoveAll();
                    }
                }
                // akaushik5 Added for MITS 36479 Ends

                foreach (XmlNode section in SysViewSection)
                {
                    if (section.FirstChild != null && view.SelectSingleNode("//form/section[@name='" + section.Attributes["name"].Value + "']") != null && view.SelectSingleNode("//form/section[@name='" + section.Attributes["name"].Value + "']").NodeType != XmlNodeType.Comment)
                    {
                        XmlNode nodeSection = doc.ImportNode(section.FirstChild, true);
                        if (nodeSection != null)
                        {
                            view.AppendChild(nodeSection);
                            view.SelectSingleNode("//form/section[@name='" + section.Attributes["name"].Value + "']").RemoveAll();
                        }
                    }
                        // npadhy MITS 15514 The Data for Premium Calculation in Enhanced Policy was not coming in Recordsummary
                    else if (section.FirstChild != null && view.SelectSingleNode("//form/section[@name='" + section.Attributes["name"].Value + "']") == null)
                    {
                        if (AppHelper.GetQueryStringValue("formname").ToString() == "policyenh")
                        {
                            XmlNode nodeSection = doc.ImportNode(section.FirstChild, true);
                            if (nodeSection != null)
                            {
                                XmlNode objNode = view.SelectSingleNode("//form/group[@name='" + section.Attributes["name"].Value + "']");
                                if (objNode != null)
                                {
                                    objNode.InnerXml = nodeSection.InnerXml;
                                }
                            }
                        }
                    }
                }

                string dataformat = "<Instance><UI><FormVariables/></UI></Instance>";
                XmlDocument Data = new XmlDocument();
                Data.LoadXml(dataformat);
                Data.SelectSingleNode("//FormVariables").InnerXml = doc.SelectSingleNode("//FormVariables").InnerXml;
                XmlNode node = null;
                //if (!IsAdminTracking())
                //{
                    if (doc.SelectSingleNode("//Param[@name='SysPropertyStore']/Instance") != null)
                    {
                        node = Data.ImportNode(doc.SelectSingleNode("//Param[@name='SysPropertyStore']/Instance").FirstChild, true);

                    }
                    else
                    {
                        node = Data.ImportNode(doc.SelectSingleNode("//Param[@name='SysPropertyStore']").FirstChild, true);
                    }
                //}
                //else
                //{
                //    node = Data.ImportNode(doc.SelectSingleNode("//Param[@name='SysPropertyStore']/Instance").FirstChild, true);
                //}
                XmlNode SysViewSuppSection = view.SelectSingleNode("//form/group[@name='suppgroup']");
                XmlNodeList objSuppNodes = null;
                if (SysViewSuppSection != null)
                {
                    objSuppNodes = SysViewSuppSection.SelectNodes("./displaycolumn//control");
                }
                XmlNodeList objNodes = view.SelectNodes(".//control");
                string KillNodes = doc.SelectSingleNode("//SysKilledNodes").InnerText;
                string[] nodes = KillNodes.Split("|".ToCharArray()[0]);
                foreach (string enode in nodes)
                {
                    foreach (XmlNode objNode in objNodes)
                        if (SafeGetAttString(objNode, "name") == enode)
                            objNode.ParentNode.RemoveChild(objNode);

                    if (objSuppNodes != null)
                    {
                        foreach (XmlNode objNode in objSuppNodes)
                            if (SafeGetAttString(objNode, "name") == enode)
                                objNode.ParentNode.RemoveChild(objNode);
                    }
                }
                //Added by Amitosh for R8 enhancement of LiabilityLoss
                string KillFieldMarkNodes = doc.SelectSingleNode("//SysKillFieldMarkNode").InnerText;
                string[] fieldmarknodes = KillFieldMarkNodes.Split("|".ToCharArray()[0]);
                foreach (string fieldmarknode in fieldmarknodes)
                {
                    foreach (XmlNode objNode in objNodes)
                        if (SafeGetAttString(objNode, "name").StartsWith(fieldmarknode) && fieldmarknode != "")
                            objNode.ParentNode.RemoveChild(objNode);

                }
                //end Amitosh

                
                XmlNodeList objGroupNodes = view.SelectNodes("//group");
                foreach (string enode in nodes)
                    foreach (XmlNode objNode in objGroupNodes)
                        if (SafeGetAttString(objNode, "name") == enode)
                            objNode.ParentNode.RemoveChild(objNode);

                Data.DocumentElement.AppendChild(node);
                StringWriter sw = new StringWriter();
                XmlNode nodeEmpty = Data.CreateElement("BlankNode");
                nodeEmpty.InnerText = "";
                Data.DocumentElement.FirstChild.AppendChild(nodeEmpty);
                // Create the XslCompiledTransform and load the stylesheet.
                xslt = new XslCompiledTransform(); //Remove true : payal
                xslt.Load(Server.MapPath("~/Content/Xsl/recordsummary.xsl"));
                // Load the View.
     
                XmlDocument docview = new XmlDocument();
                docview.LoadXml(view.OuterXml);

                //Load the Formatted instance Data 
                docInstance = new XmlDocument();
                docInstance.LoadXml(Data.OuterXml);

       
                
                XmlNodeList objNodesList = docview.SelectNodes("//form//group[@name='suppgroup']//control[@type='ZapatecGrid']");

                //MGaba2:MITS 22180:Record Summary not working for Sentinel Screen-null check was missing
                //pmittal5 Mits 20679 08/18/10 - Display Zapatec Grid in case of Admin Tracking tables               
                if((docview.SelectSingleNode("//form").Attributes["name"] != null) && (docview.SelectSingleNode("//form").Attributes["name"].Value == "admintracking"))
                {
                    objNodesList = docview.SelectNodes("//form//group[@name='admgroup']//control[@type='ZapatecGrid']");
                }
                //End - pmittal5

                // Loop through all the Zapatec Grid records and Get the XML in GridXML attribute
                foreach (XmlNode objTemp in objNodesList)
                {

                    XmlDocument objTempGridXml = new XmlDocument();

                    XmlAttribute objTempAtt = objTemp.Attributes["gridXML"];

                    // Load the XML from Grid Xml Attribute
                    objTempGridXml.LoadXml(objTempAtt.Value);

                    // Loop through the Headers
                    XmlNodeList objNodeList = objTempGridXml.SelectNodes("//fields/field/title");
                    
                    // Create an Attribute of name Header in SysView.
                    XmlAttribute objAtt = docview.CreateAttribute("Headers");
                    foreach (XmlNode objNodeTemp in objNodeList)
                    {
                        objAtt.InnerXml += objNodeTemp.InnerText + "|";
                    }
                    if (objAtt != null && !string.IsNullOrEmpty(objAtt.InnerXml))
                    {
                        objAtt.InnerXml = objAtt.InnerXml.Substring(0, objAtt.InnerXml.Length - 1);
                    }
                        objTemp.Attributes.Append(objAtt);
                    

                    // Create an attribute of name Data in SysView
                    objAtt = docview.CreateAttribute("Data");
                    XmlNodeList objNodeListData = objTempGridXml.SelectNodes("//rows/row");
                    bool bContainsRow = false;
                    foreach (XmlNode objNodeTemp in objNodeListData)
                    {
                        foreach (XmlElement objElem in objNodeTemp)
                        {
                            if (objElem.InnerText != "")
                                bContainsRow = true;

                            //   if(objElem.InnerText != "") bkumar33-1\Mits 14316
                                objAtt.InnerXml += objElem.InnerText + "|";
                        }
                        // if(objAtt.InnerXml != "") bkumar33-1\Mits 14316
                        if (objAtt != null && !string.IsNullOrEmpty(objAtt.InnerXml))
                        {
                            objAtt.InnerXml = objAtt.InnerXml.Substring(0, objAtt.InnerXml.Length - 1);
                        }
                        //if(bContainsRow) bkumar33-1\Mits 14316
                            objAtt.InnerXml += ";";
                    }
                    if (bContainsRow)  //bkumar33-1\Mits 14316
                    objTemp.Attributes.Append(objAtt);
                }

                //Mits 35083 sharishkumar
                XmlNode objNodeMast = docview.SelectSingleNode("//control[@name='masterbankacc']");
                XmlNode objNodeMastList = docInstance.SelectSingleNode("//MastBankAcctList");
                if (objNodeMast != null && objNodeMastList != null)
                {
                    foreach (XmlNode objNode in objNodeMastList.ChildNodes)
                    {
                        if (objNode.InnerText != "")
                        {
                            XmlNode importNode = docview.ImportNode(objNode, true);
                            objNodeMast.AppendChild(importNode);
                        }
                    }
                }

                //End Mits 35083

                // Create an XsltArgumentList.
                xslArg = new XsltArgumentList();

                // Add an object to convert the book price.
                obj = new SaxonImpl(docInstance);
                xslArg.AddExtensionObject("urn:SaxonImpl", obj);

                // Transform the file.
        
                xslt.Transform(docview, xslArg, sw);
                //Start add ttumula2 for RMA-7055
                htm = new HtmlDocument();
                htm.LoadHtml(sw.ToString());
                Print = Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificObject("printRecordSmry",RMXResourceProvider.PageId("RecordSummary.aspx"),"2");
                if (!string.IsNullOrEmpty(Print) && Print != "printRecordSmry")
                {
                    foreach (var xb in htm.DocumentNode.Descendants("img"))
                    {
                        xb.Attributes["title"].Value = Print;
                        break;
                    }
                }
               
                //Display = sw.ToString();
                Display = htm.DocumentNode.InnerHtml;
                //End add ttumula2 for RMA-7055
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                obj = null;
                xslt = null;
                xslArg = null;
                docInstance = null;
            }   
        }

        // npadhy Change the Implementation of this function to cater to all the Pages and not EnhancedPolicy Only
        private void OnUpdateForm(XmlDocument objDoc)
        {
            XElement oMessageElement = XElement.Parse(objDoc.InnerXml);
            XmlNode objControl;
            bool bIsGroup;
            XElement objAttListNode = null;
            XmlElement objControlElem = null;


            XElement oControlAppendAttributeList = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ControlAppendAttributeList");

            if (oControlAppendAttributeList != null && oControlAppendAttributeList.Nodes() != null)
            {
                foreach (XElement objNode in oControlAppendAttributeList.Nodes())
                {
                    bIsGroup = false;
                    // Get the Xml Node from the Xml
                    objControl = objDoc.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysView/form//displaycolumn/control[@name= '" + objNode.Name.LocalName + "']");

                    if (objControl == null)
                    {
                        // This Might be a Group. Check for existence of group.
                        objControl = objDoc.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysView/form/group[@name= '" + objNode.Name.LocalName + "']");

                        bIsGroup = true;
                    }

                    objAttListNode = (XElement)objNode.FirstNode;
                    if (objAttListNode == null || objControl == null)
                        continue;


                    switch (objAttListNode.Name.LocalName)
                    {
                        case "title":
                        case "ref":
                            if (objControl.Attributes[objAttListNode.Name.LocalName] != null)
                                objControl.Attributes[objAttListNode.Name.LocalName].Value = objAttListNode.Attribute("value").Value;
                            else
                            {
                                objControlElem = (XmlElement)objControl;
                                objControlElem.SetAttribute(objAttListNode.Name.LocalName, objAttListNode.Attribute("value").Value);
                            }
                            break;
                    }
                }
            }

            //rsushilaggar  MITS 25351 Date 07/06/2011
            XElement oUseAdvancedClaim = oMessageElement.XPathSelectElement("//UseAdvancedClaim");
            if (oUseAdvancedClaim!= null && oUseAdvancedClaim.Value != "-1")
            {
                objControl = objDoc.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysView/form//displaycolumn/control[@name= 'multipolicyid']");
                if (objControl != null)
                    objControl.ParentNode.RemoveChild(objControl);
            }
            else
            {
                objControl = objDoc.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysView/form//displaycolumn/control[@name= 'primarypolicyid']");
                if(objControl != null)
                    objControl.ParentNode.RemoveChild(objControl);
            }
            //End rsushilaggar
            
            //DatabindingHelper.UpdateControlAttributes(oControlAppendAttributeList, this);
        }
        /// <summary>
        ///  The function used when we are required to add some additional Tag in the standard xml of Record Summary.
        /// </summary>
        /// <param name="oMessageElement"></param>
		//Bijender Mits 14124
        public virtual void ModifyXml1(XElement oMessageElement)
        {
            switch (AppHelper.GetQueryStringValue("formname").ToString ())
            {
                case "piworkloss":
                    oMessageElement = oMessageElement.XPathSelectElement(".//ParamList/Param/FormVariables/SysExData");
                    //oMessageElement.Add("PiRowId", AppHelper.GetQueryStringValue("SysParms").ToString());            
                    oMessageElement.SetElementValue("PiRowId", AppHelper.GetQueryStringValue("SysParms").ToString());
                    break;
                //Added Rakhi for R7:Add Emp Data Elements
                case "employee":
                    oMessageElement = oMessageElement.XPathSelectElement(".//ParamList/Param/FormVariables/SysExData");
                    if(oMessageElement!=null)
                        oMessageElement.SetElementValue("IsRecordSummary", true);
                    break;
                case "physician":
                    oMessageElement = oMessageElement.XPathSelectElement(".//ParamList/Param/FormVariables/SysExData");
                    if (oMessageElement != null)
                        oMessageElement.SetElementValue("IsRecordSummary", true);
                    break;
                case "staff":
                    oMessageElement = oMessageElement.XPathSelectElement(".//ParamList/Param/FormVariables/SysExData");
                    if (oMessageElement != null)
                        oMessageElement.SetElementValue("IsRecordSummary", true);
                    break;
                case "patient":
                    oMessageElement = oMessageElement.XPathSelectElement(".//ParamList/Param/FormVariables/SysExData");
                    if (oMessageElement != null)
                        oMessageElement.SetElementValue("IsRecordSummary", true);
                    break;
                //Added Rakhi for R7:Add Emp Data Elements

                // npadhy - RMA-6380. Policy System Data-tab of coverages screen should be included in record summary
                // Policy System Data is displayed only if the Policy is from External System.
                // Thus the Record Summary need to know while opening if the Coverge is corresponding to External System or Internal System
                case "policycoverage":
                    oMessageElement = oMessageElement.XPathSelectElement(".//ParamList/Param/FormVariables/SysExData");
                    oMessageElement.SetElementValue("hdnPolicySystemId", AppHelper.GetQueryStringValue("hdnPolicySystemId").ToString());
                    break;
            }
            
        }
        
        private XmlDocument  CallService(XElement oMessageElement)
        {
            XElement oSysFormName = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName");
            if (AppHelper.GetQueryStringValue("formname") == "")
            {
                //oSysFormName.Value = AppHelper.GetQueryStringValue("formname");
                oSysFormName.Value = "claimwc";
            }
            else
            {
                
                oSysFormName.Value = AppHelper.GetQueryStringValue("formname");
            }
            //Set SysFormId value
            XElement oSysFormId = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormId']");
            if (AppHelper.GetQueryStringValue("recordID") == "")
            {
            //oSysFormIdName.Value = AppHelper.GetQueryStringValue("recordID");
            oSysFormId.Value = "2";
            }
            else
                oSysFormId.Value = AppHelper.GetQueryStringValue("recordID");
             //Set SysFormId value
            XElement oSysViewId = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysViewId']");
            //aaggarwal29 : MITS 37713/ JIRA 7548 start , set ViewId from Cookie as it was not coming from Querystring
            if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("ViewId")))
            {
            oSysViewId.Value = AppHelper.ReadCookieValue("ViewId"); //add ttumula2 for RMA-7055
            }

            //oSysViewId.Value = AppHelper.GetQueryStringValue("viewid");
            //aaggarwal29 : MITS 37713/ JIRA 7548 end
            
            //Raman Bhatia : 07/31/2008
            //Setting SessionID from QueryString into Input XML for Web-Service 
            //This is needed to ensure that correct data source is picked
            //string sSessionID = HttpContext.Current.Request.QueryString["SessionID"];
            //Mjain8 Doing it temporarily later on it will be changed.I did it for handling child window where we don't get session id from querystring 
            //Right now we'll read it in application_begingrequest from web.config a hardcoded session and will be removed once login page completes.
            if (! string.IsNullOrEmpty(AppHelper.ReadCookieValue("SessionId")))
            {
                XElement oSessionCmdElement = oMessageElement.XPathSelectElement("./Authorization");
                oSessionCmdElement.Value = AppHelper.ReadCookieValue("SessionId");
            }
            if (AppHelper.GetQueryStringValue("session") != "")
            {
                XElement oSessionCmdElement = oMessageElement.XPathSelectElement("./Authorization");
                oSessionCmdElement.Value = AppHelper.GetQueryStringValue("session");
            }
            //nkaranam2 - MITS : 34643
            if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("SysSid")))
            {
                XElement oSysSid = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysSid");
                oSysSid.Value = AppHelper.GetQueryStringValue("SysSid");
            }

            //Call WCF wrapper for cws
            CommonWCFService.CommonWCFServiceClient oWCF = new CommonWCFServiceClient();
            string sReturn = oWCF.ProcessRequest(oMessageElement.ToString());
            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sReturn);
            XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
            
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oFDMPageDom.LoadXml(oInstanceNode.OuterXml);
            return oFDMPageDom;
        }

        private XElement GetMessageTemplate()
        {//Added by Amitosh To Remove nodes starting with fieldmark
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization>
                <Call>
                    <Function>FormDataAdaptor.NavigateRecordSummary</Function>
                </Call>
                <Document>
                    <ParamList>
                        <Param name='SysFormVariables'>
	                        <FormVariables>
		                        <SysCmd>0</SysCmd>
		                        <SysViewType>recordsummary.xsl</SysViewType>
		                        <SysInvisible/>
		                        <SysFocusFields/>
		                        <SysCmdText/>
		                        <SysCmdConfirmSave/>
		                        <SysCmdQueue/>
		                        <SysFormName></SysFormName>
		                        <SysRequired/>
		                        <SysFormIdName/>
		                        <SysFormId/>
		                        <SysFormPId/>
		                        <SysFormPIdName/>
		                        <SysFormPForm/>
		                        <SysNotReqNew/>
		                        <SysSupplemental/>
		                        <SysSerializationConfig/>
		                        <SysClassName/>
		                        <SysFilter/>
		                        <SysEx/>
                                <SysExData>    
                                <Parent>
                                <Instance>
                                <PiEmployee>
                                <PiRowId /> 
                                </PiEmployee>
                                </Instance>
                                </Parent>
                                <PiRowId /> 
		                        </SysExData>
		                        <SysSid/>
		                        <SysPSid/>
		                        <SysLookupClass/>
		                        <SysLookupRecordId/>
		                        <SysLookupAttachNodePath/>
		                        <SysLookupResultConfig/>
		                        <SysViewSection/>
		                        <SysView/>
		                        <SysSelectedTab/>
                                <SysKilledNodes/>
                                <SysKillFieldMarkNode/>   
                                <SysReadOnlyNodes/>
	                        </FormVariables>
                        </Param>
                        <Param name='SysScreenAction'>
	                        <SysScreenAction/>
                        </Param>
                        <Param name='SysFormId'/>
                        <Param name='SysPropertyStore'>
	                        <Instance/>
                        </Param>
                        <Param name='ScreenFlowStack'>
	                        <ScreenFlowStack>
		                        <ScreenFlow>
			                        <SysFormId/>
			                        <SysFormName></SysFormName>
		                        </ScreenFlow>
	                        </ScreenFlowStack>
                        </Param>
                        <Param name='SysViewId'/>
                    </ParamList>
                </Document>
                <ClientId>0</ClientId>
            </Message>
            ");
            // MITS 27191: Ishan - Added nodes to populate data in the Record Summary
            return oTemplate;
        }
    } 
}