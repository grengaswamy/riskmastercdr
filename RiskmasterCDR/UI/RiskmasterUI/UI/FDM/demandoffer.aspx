<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="demandoffer.aspx.cs"  Inherits="Riskmaster.UI.FDM.DemandOffer" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Demand Offer</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/tb_save_active.png" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/tb_save_mo.png';" onMouseOut="this.src='../../Images/tb_save_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" OnClientClick="return DeleteRecord();" src="../../Images/tb_delete_active.png" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/tb_delete_mo.png';" onMouseOut="this.src='../../Images/tb_delete_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_comments" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return Comments();" src="../../Images/tb_comments_active.png" width="28" height="28" border="0" id="comments" AlternateText="Comments" onMouseOver="this.src='../../Images/tb_comments_mo.png';" onMouseOut="this.src='../../Images/tb_comments_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return recordSummary();" src="../../Images/tb_recordsummary_active.png" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='../../Images/tb_recordsummary_mo.png';" onMouseOut="this.src='../../Images/tb_recordsummary_active.png';" />
        </div>
      </div>
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Demand Offer" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSdemandofferinfo" id="TABSdemandofferinfo">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="demandofferinfo" id="LINKTABSdemandofferinfo">Demand/Offer Info</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPdemandofferinfo">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
          <a class="NotSelected1" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPsuppgroup">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABdemandofferinfo" id="FORMTABdemandofferinfo">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hddemandofferinfo" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="demandofferrowid" RMXRef="/Instance/DemandOffer/DemandOfferRowId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="parentid" RMXRef="/Instance/DemandOffer/ParentId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="parentname" RMXRef="/Instance/DemandOffer/ParentName" RMXType="id" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_activitycode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_activitycode" Text="Demand/Offer" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="activitycode" CodeTable="DEM_OFFER_ACTV" ControlName="activitycode" RMXRef="/Instance/DemandOffer/ActivityCode" CodeFilter="" RMXType="code" tabindex="1" />
                </span>
              </div>
              <div runat="server" class="half" id="div_demandofferamount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_demandofferamount" Text="Amount" />
                <span class="formw">
                  <asp:TextBox runat="server" id="demandofferamount" RMXRef="/Instance/DemandOffer/DemandOfferAmount" RMXType="text" tabindex="2" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_decisioncode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_decisioncode" Text="Decision" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="decisioncode" CodeTable="DEM_OFFER_DECIS" ControlName="decisioncode" RMXRef="/Instance/DemandOffer/DemandOfferDecision" CodeFilter="" RMXType="code" tabindex="3" />
                </span>
              </div>
              <div runat="server" class="half" id="div_demandofferdate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_demandofferdate" Text="Date" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="demandofferdate" RMXRef="/Instance/DemandOffer/DemandOfferDate" RMXType="date" tabindex="4" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="demandofferdatebtn" tabindex="5" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "demandofferdate",
					    ifFormat: "%m/%d/%Y",
					    button: "demandofferdatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_resultcode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_resultcode" Text="Result" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="resultcode" CodeTable="DEM_OFFER_RSLT" ControlName="resultcode" RMXRef="/Instance/DemandOffer/DemandOfferResult" CodeFilter="" RMXType="code" tabindex="5" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="dttmrcdadded" RMXRef="/Instance/DemandOffer/DttmRcdAdded" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="dttmrcdlastupd" RMXRef="/Instance/DemandOffer/DttmRcdLastUpd" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="updatedbyuser" RMXRef="/Instance/DemandOffer/UpdatedByUser" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="addedbyuser" RMXRef="/Instance/DemandOffer/AddedByUser" RMXType="id" />
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdsuppgroup" />
            </td>
          </tr>
        </table>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server" />
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="DemandOffer" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;Demand&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/Demand&gt;" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="claimwc" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="claimid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="claimid,subrogationrowid" />
      <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="subrogation" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="subrogationrowid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="3000" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="line_of_bus_code" RMXRef="Instance/UI/MissingRef" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="lineofbusinesscode" RMXRef="/Instance/UI/FormVariables/SysExData/LINEOFBUSCODE" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="TabNameList" RMXRef="" RMXType="hidden" Text="TABSdemandofferinfo|TABSsuppgroup" />
      <asp:TextBox style="display:none" runat="server" id="GroupAssocFieldList" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="FormReadOnly" RMXRef="" RMXType="hidden" Text="Enable" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="demandoffer" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysReadonlyFields" id="SysReadonlyFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>