﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vehicleuardetails.aspx.cs" Inherits="Riskmaster.UI.FDM.vehicleuardetails" ValidateRequest="false" %>

<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Vehicle UAR Details</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script  type="text/javascript" language="javascript" src='../../Scripts/form.js'>   {var i;}</script>
    <script  type="text/javascript" language="javascript" src='../../Scripts/EnhPolicy.js'>   {var i;}</script>
    <script language="JavaScript" src="../../Scripts/grid.js">{var i;}</script>
    <style type="text/css">
        .style1
        {
            border-style: none;
            border-color: inherit;
            border-width: medium;
            FONT-WEIGHT: bold;
            PADDING-BOTTOM: 3px;
            COLOR: #ffffff;
            PADDING-TOP: 3px;
            BACKGROUND-COLOR: #95B3D7;
            float: left;
            width: 418px;
        }
    </style>
</head>
<body  onload="UarDetailsOnLoad();" >
    <form id="frmData" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Add/Modify UAR" /></div>
        <table><tr><td class="style1" colSpan="2">UAR Details</td></tr></table>
        <br />
     <table>
        <tr>
        <%--Sumit - Start(03/16/2010) - MITS# 18229 - Updated entire refs--%>
        <td>Vehicle ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td>
            <asp:TextBox runat ="server" size = "30" onblur=""  onchange="setDataChanged(true);" name="vehicleidcode" id="vehicleidcode" RMXRef = "Instance/Document/PolicyXUar/VIN"  ReadOnly ="true"/>
            <%--Sumit - Start(10/19/2010) - MITS# 21996--%>
            <input type="button" name="vehicleidcodebtn"  class="CodeLookupControl"  id="vehicleidcodebtn" runat="server"/>
             <%--End:Sumit--%>
                </td>
            </tr>
         <tr>
         <td>Vehicle Make</td>
            <td>
            <asp:TextBox runat ="server" size="30" onblur=""  onchange="setDataChanged(true);" name="vehiclemake" id="vehiclemake" ReadOnly ="true" RMXRef = "Instance/Document/PolicyXUar/VehicleMake" class="grayline2" />
            </td>
         </tr>
         <tr>
         <td>Vehicle Model</td>
            <td>
            <asp:TextBox size="30" runat="server"  onblur=""  onchange="setDataChanged(true);" name="vehiclemodel" id="vehiclemodel" ReadOnly ="true" RMXRef = "Instance/Document/PolicyXUar/VehicleModel" class="grayline2"/>
            </td>
         </tr>
         <tr>
         <td>Vehicle Year</td>
            <td>
            <asp:TextBox size="30" runat="server"  onblur=""  onchange="setDataChanged(true);" name="vehicleyear" id="vehicleyear" ReadOnly ="true" RMXRef = "Instance/Document/PolicyXUar/VehicleYear" class="grayline2"/>
            </td>
         </tr>
         <tr>
         <td>State</td>
            <td>
            <asp:TextBox size="30" runat="server"  onblur=""  onchange="setDataChanged(true);" name="state" id="state" ReadOnly ="true" RMXRef = "Instance/Document/PolicyXUar/State" class="grayline2"/>
            </td>
         </tr>
         <tr>
         <td>License Number</td>
            <td>
            <asp:TextBox size="30" runat="server"  onblur=""  onchange="setDataChanged(true);" name="licensenumber" id="licensenumber" ReadOnly ="true" RMXRef = "Instance/Document/PolicyXUar/LicenseNumber" class="grayline2"/>
            </td>
         </tr>
<%--         <tr>
         <td>Unit Type</td>
            <td>
            <asp:TextBox size="30" runat="server"  onblur=""  onchange="setDataChanged(true);" name="unittype" id="unittype" ReadOnly ="true" RMXRef = "Instance/Document/PolicyXUar/UnitTypeCode" />
            </td>
         </tr> --%>
         <tr>
         <td>
             <br />
             <br />
           <%--Sumit - End--%>
         <asp:button class="button" runat="server" id="btnUarOk" RMXRef="" Text="OK" width="75px" onClientClick="return fnUAROk();" onclick="btnUAROk_Click" />
         
         &nbsp;
         
         <asp:button class="button" runat="server" id="btnUarCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return fnUARCancel();" onclick="btnUAROk_Click" />
         </td>
         </tr>  
        </table>
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
    <%--Sumit - Start(03/16/2010) - MITS# 18229--%>
    <asp:TextBox Style="display: none" runat="server" ID="txtData"></asp:TextBox>
    <%--Sumit - End--%>
    <asp:TextBox style="display: none" runat="server" ID="formname" value="vehicleuardetails" />
    <asp:TextBox Style="display: none" runat="server" ID="VehicleIDEnabled"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="VehicleMakeEnabled"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="VehicleModelEnabled"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="VehicleYearEnabled"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="StateEnabled"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="LicenceNumberEnabled"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="UnitTypeEnabled"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="UnitID"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="FunctionToCall"></asp:TextBox>
    <%--Sumit - Start(03/16/2010) - MITS# 18229--%>
    <asp:TextBox style="display:none" runat="server" id="SessionId" RMXRef="/Instance/Document/PolicyXUar/SessionId" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
    <asp:TextBox style="display:none" runat="server" id="UarId" RMXRef="/Instance/Document/PolicyXUar/UarId" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
    <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" />
    <asp:TextBox style="display:none" runat="server" id="hdnCodeID" RMXRef="" RMXType="id" />
    <asp:TextBox style="display:none" runat="server" id="hdnOkEnabled" value="false" />
    <%--Sumit - End--%>
    <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" />
    <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" /> 
    <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" />               
    </form>
</body>
</html>
