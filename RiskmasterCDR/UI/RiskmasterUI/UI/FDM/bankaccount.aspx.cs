﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.AppHelpers;
namespace Riskmaster.UI.FDM
{
    public partial class Bankaccount : FDMBasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
           

        }

        //Changed for MITS 15171 : Start
        public override void ModifyXml(ref XElement Xelement)
        {
            //XElement objNextCheckNumber = Xelement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/Coverages/option/CoveragesId[.=\"" + sCoverageSessions[0] + "\"]/../SessionId");
            XElement objNextCheckNumber = Xelement.XPathSelectElement("./Document/ParamList/Param[@name='SysPropertyStore']/Instance/Account/NextCheckNumber");
            if (objNextCheckNumber != null)
            {
                if (objNextCheckNumber.Value == "0")
                {
                    objNextCheckNumber.Value = "1";
                }
            }
        }
        //Changed for MITS 15171 : End
        //Added by Amitosh  for EFT Payments
        public override void ModifyControls()
        {
            CheckBox objCtrl = (CheckBox)this.FindControl("isEftAccount");
            if ((objCtrl != null) && objCtrl.Checked)
            {
              Label lbl_routingnumber = (Label)this.FindControl("lbl_routingnumber");
                if (lbl_routingnumber != null)
              lbl_routingnumber.CssClass = "required"; 


              Label lbl_accttypecode = (Label)this.FindControl("lbl_accttypecode");
              if (lbl_accttypecode != null)
                 lbl_accttypecode.CssClass = "required"; 
            }
            //Ankit Start : Worked on MITS - 33066
            else
            {
                Label lbl_routingnumber = (Label)this.FindControl("lbl_routingnumber");
                if (lbl_routingnumber != null)
                    lbl_routingnumber.CssClass = "";


                Label lbl_accttypecode = (Label)this.FindControl("lbl_accttypecode");
                if (lbl_accttypecode != null)
                    lbl_accttypecode.CssClass = "";
            }
            //Ankit End
            XmlDocument oXmlData = base.Data;
            //mona: Making Policy Lob on Bank Account as multicode and non mandatory
            if (oXmlData.SelectSingleNode("./Document/ParamList/Param[@name='SysPropertyStore']/Account/PolicyLOBCodes").ChildNodes.Item(0).Attributes["value"].Value == "0")
            {
                RadioButton objAllPolLob = (RadioButton)(this.FindControl("AllPolLOB"));
                if (objAllPolLob != null)
                    objAllPolLob.Checked = true;

                 RadioButton objSpecificPolLOB = (RadioButton)(this.FindControl("SpecificPolLOB"));
                 if (objSpecificPolLOB != null)
                    objSpecificPolLOB.Checked = false;

                // akaushik5 Commented for RMA-9706 Starts
                //MultiCodeLookUp objPolLoB = (MultiCodeLookUp)(this.FindControl("policyLOBCode"));
                //if (objPolLoB != null)
                //{
                //    objPolLoB.Enabled = false;
                //}
                // akaushik5 Commented for RMA-9706 Ends
            }
            else
            {
                RadioButton objSpecificPolLOB = (RadioButton)(this.FindControl("SpecificPolLOB"));
                if (objSpecificPolLOB != null)
                    objSpecificPolLOB.Checked = true;

                // akaushik5 Commented for RMA-9706 Starts
                //MultiCodeLookUp objPolLoB = (MultiCodeLookUp)(this.FindControl("policyLOBCode"));
                //if (objPolLoB != null)
                //{
                //    objPolLoB.Enabled = true;
                //}
                // akaushik5 Commented for RMA-9706 Ends
            }
        }
        //End amitosh
    }
}
