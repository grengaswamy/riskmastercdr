﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.UI.FDM
{
    public partial class Adjusterdatedtext : FDMBasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);
            //MGaba2: MITS 15467:start       
            //In case we do not have view permission as well as no permission to see attachment            
            TextBox txtSId = (TextBox)(this.Form.FindControl("SysSid"));
            if (txtSId != null)
            {
                XElement oSecId = oMessageElement.XPathSelectElement("//SysSid");
                if (oSecId != null)
                {
                    txtSId.Text = oSecId.Value;
                }
            }
            //MGaba2: MITS 15467:end
        }
    }
}
