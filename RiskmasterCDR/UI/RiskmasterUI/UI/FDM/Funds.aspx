<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="funds.aspx.cs"  Inherits="Riskmaster.UI.FDM.Funds" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"  TagPrefix="mc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Funds</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_new" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClick="NavigateNew" src="../../Images/tb_new_active.png" width="28" height="28" border="0" id="new" AlternateText="New" onMouseOver="this.src='../../Images/tb_new_mo.png';" onMouseOut="this.src='../../Images/tb_new_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/tb_save_active.png" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/tb_save_mo.png';" onMouseOut="this.src='../../Images/tb_save_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_lookup" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="if(!doLookup()) return false;" src="../../Images/tb_lookup_active.png" width="28" height="28" border="0" id="lookup" AlternateText="Lookup Data" onMouseOver="this.src='../../Images/tb_lookup_mo.png';" onMouseOut="this.src='../../Images/tb_lookup_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return FilteredDiary();" src="../../Images/tb_diaryviewrecord_active.png" width="28" height="28" border="0" id="filtereddiary" AlternateText="View Record Diaries" onMouseOver="this.src='../../Images/tb_diaryviewrecord_mo.png';" onMouseOut="this.src='../../Images/tb_diaryviewrecord_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_diary" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return Diary();" src="../../Images/tb_diary_active.png" width="28" height="28" border="0" id="diary" AlternateText="Diary" onMouseOver="this.src='../../Images/tb_diary_mo.png';" onMouseOut="this.src='../../Images/tb_diary_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_comments" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return Comments();" src="../../Images/tb_comments_active.png" width="28" height="28" border="0" id="comments" AlternateText="Comments" onMouseOver="this.src='../../Images/tb_comments_mo.png';" onMouseOut="this.src='../../Images/tb_comments_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_attach" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return attach();" src="../../Images/tb_attach_active.png" width="28" height="28" border="0" id="attach" AlternateText="Attach Documents" onMouseOver="this.src='../../Images/tb_attach_mo.png';" onMouseOut="this.src='../../Images/tb_attach_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_mailmerge" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return MailMerge();" src="../../Images/tb_mailmerge_active.png" width="28" height="28" border="0" id="mailmerge" AlternateText="Mail Merge" onMouseOver="this.src='../../Images/tb_mailmerge_mo.png';" onMouseOut="this.src='../../Images/tb_mailmerge_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_sendmail" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return SendMail();" src="../../Images/tb_sendmail_active.png" width="28" height="28" border="0" id="sendmail" AlternateText="Send Mail" onMouseOver="this.src='../../Images/tb_sendmail_mo.png';" onMouseOut="this.src='../../Images/tb_sendmail_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_fundspaymenthistory" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return BackToPaymentHistory();" src="../../Images/tb_paymenthistory_active.png" width="28" height="28" border="0" id="fundspaymenthistory" AlternateText="Go To Payment History" onMouseOver="this.src='../../Images/tb_paymenthistory_mo.png';" onMouseOut="this.src='../../Images/tb_paymenthistory_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_backtofinancials" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return BackToReserves();" src="../../Images/tb_backtofinancials_active.png" width="28" height="28" border="0" id="backtofinancials" AlternateText="Back To Financials" onMouseOver="this.src='../../Images/tb_backtofinancials_mo.png';" onMouseOut="this.src='../../Images/tb_backtofinancials_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_loadclaim" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return LoadClaim();" src="../../Images/tb_loadclaim_active.png" width="28" height="28" border="0" id="loadclaim" AlternateText="Load Claim" onMouseOver="this.src='../../Images/tb_loadclaim_mo.png';" onMouseOut="this.src='../../Images/tb_loadclaim_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_backtotande" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return BackToTimeandExpense();" src="../../Images/tb_tande_active.png" width="28" height="28" border="0" id="backtotande" AlternateText="Back To Time and Expense" onMouseOver="this.src='../../Images/tb_tande_mo.png';" onMouseOut="this.src='../../Images/tb_tande_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_payeeexp" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return DisplayPayeeExperience();" src="../../Images/tb_payeeexperience_active.png" width="28" height="28" border="0" id="payeeexp" AlternateText="Payee Experience" onMouseOver="this.src='../../Images/tb_payeeexperience_mo.png';" onMouseOut="this.src='../../Images/tb_payeeexperience_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_printpre" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return PrintPreview();" src="../../Images/tb_printpreview_active.png" width="28" height="28" border="0" id="printpre" AlternateText="Print Preview" onMouseOver="this.src='../../Images/tb_printpreview_mo.png';" onMouseOut="this.src='../../Images/tb_printpreview_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_printedimage" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return Printedchecksclone();" src="../../Images/tb_clonecheck_active.png" width="28" height="28" border="0" id="printedimage" AlternateText="Clone Image of Printed Check" onMouseOver="this.src='../../Images/tb_clonecheck_mo.png';" onMouseOut="this.src='../../Images/tb_clonecheck_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_printcheck" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return PrintCheck();" src="../../Images/tb_printcheck_active.png" width="28" height="28" border="0" id="printcheck" AlternateText="Print Check" onMouseOver="this.src='../../Images/tb_printcheck_mo.png';" onMouseOut="this.src='../../Images/tb_printcheck_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_printeob" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return PrintEOB();" src="../../Images/tb_printeob_active.png" width="28" height="28" border="0" id="printeob" AlternateText="Print EOB" onMouseOver="this.src='../../Images/tb_printeob_mo.png';" onMouseOut="this.src='../../Images/tb_printeob_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_printsummary" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return PrintTrans();" src="../../Images/tb_print_active.png" width="28" height="28" border="0" id="printsummary" AlternateText="Print Summary" onMouseOver="this.src='../../Images/tb_print_mo.png';" onMouseOut="this.src='../../Images/tb_print_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_voidreissue" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return VoidReissue();" src="../../Images/tb_voidreissue_active.png" width="28" height="28" border="0" id="voidreissue" AlternateText="Void And Reissue" onMouseOver="this.src='../../Images/tb_voidreissue_mo.png';" onMouseOut="this.src='../../Images/tb_voidreissue_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_ofac" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return OFACLookUp();" src="../../Images/tb_ofac_active.png" width="28" height="28" border="0" id="ofac" AlternateText="OFAC Check" onMouseOver="this.src='../../Images/tb_ofac_mo.png';" onMouseOut="this.src='../../Images/tb_ofac_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_enhancednotes" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return EnhancedNotes();" src="../../Images/tb_enhancednotes_active.png" width="28" height="28" border="0" id="enhancednotes" AlternateText="Enhanced Notes" onMouseOver="this.src='../../Images/tb_enhancednotes_mo.png';" onMouseOut="this.src='../../Images/tb_enhancednotes_active.png';" />
        </div>
      </div>
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Funds" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABStransaction" id="TABStransaction">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="transaction" id="LINKTABStransaction">Transaction</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPtransaction">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABStransactiondetail" id="TABStransactiondetail">
          <a class="NotSelected1" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="transactiondetail" id="LINKTABStransactiondetail">Transaction Detail</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPtransactiondetail">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSbrstransactiondetail" id="TABSbrstransactiondetail">
          <a class="NotSelected1" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="brstransactiondetail" id="LINKTABSbrstransactiondetail">BRS Transaction Detail</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPbrstransactiondetail">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
          <a class="NotSelected1" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPsuppgroup">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABtransaction" id="FORMTABtransaction">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdtransaction" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_ctlnumber" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_ctlnumber" Text="Control Number" />
                <span class="formw">
                  <asp:TextBox runat="server" id="ctlnumber" RMXRef="Instance/Funds/CtlNumber" RMXType="text" tabindex="1" maxlength="25" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div id="div2" class="half" xmlns="" xmlns:mc="remove">
                <img src="../../Images/Blank.JPG" id="Img1" class="half" />
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_paymentflag" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_paymentflag" Text="Payment" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="paymentflag" RMXRef="Instance/Funds/PaymentFlag" RMXType="checkbox" onclick="PaymentFlagCheck()" tabindex="2" Height="24px" />
                </span>
              </div>
              <div runat="server" class="half" id="div_voidflag" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_voidflag" Text="Void" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="voidflag" RMXRef="Instance/Funds/VoidFlag" RMXType="checkbox" onclick="return VoidPaymentCheck()" tabindex="3" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_collectionflag" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_collectionflag" Text="Collection" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="collectionflag" RMXRef="Instance/Funds/CollectionFlag" RMXType="checkbox" onclick="CollectionFlagCheck()" tabindex="4" Height="24px" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clearedflag" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_clearedflag" Text="Cleared" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="clearedflag" RMXRef="Instance/Funds/ClearedFlag" RMXType="checkbox" onclick="return ClearPaymentCheck()" tabindex="5" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_resubmitflag" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_resubmitflag" Text="Resubmit" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="resubmitflag" RMXRef="Instance/Funds/ResubmitFlag" RMXType="checkbox" tabindex="6" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_claimnumber" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_claimnumber" Text="Claim Number" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" id="claimnumber" RMXRef="/Instance/Funds/ClaimNumber" RMXType="claimnumberlookup" name="claimnumber" value="" cancelledvalue="" tabindex="8" />
                  <asp:button runat="server" class="EllipsisControl" id="claimnumberbtn" tabindex="9" onclientclick="return lookupData('claimnumber','claim',-1,'claimnumber',6);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_finalpaymentflag" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_finalpaymentflag" Text="Final Payment" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="finalpaymentflag" RMXRef="/Instance/Funds/FinalPaymentFlag" RMXType="checkbox" tabindex="9" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div id="div3" class="half" xmlns="" xmlns:mc="remove">
                <img src="../../Images/Blank.JPG" id="Img1" class="half" />
              </div>
              <div runat="server" class="half" id="div_manualcheckflag" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_manualcheckflag" Text="Manual Check" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="manualcheckflag" RMXRef="Instance/Funds/ManualCheckFlag" RMXType="checkbox" onclick="return false;" tabindex="7" style="background-color: #F2F2F2;" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_cboclaimant" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_cboclaimant" Text="Detailed Tracking Claimant" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="cboclaimant" tabindex="8" RMXRef="/Instance/Funds/ClaimantEid/@codeid" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/ClaimantList" onchange="setDataChanged(true);ClaimantChanged(this)" />
                </span>
              </div>
              <div runat="server" class="half" id="div_cbounit" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_cbounit" Text="Detailed Tracking Unit" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="cbounit" tabindex="9" RMXRef="/Instance/UnitId/@codeid" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/UnitList" onchange="setDataChanged(true);UnitChanged(this)" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_LSSInvoice" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_LSSInvoice" Text="LSS Invoice" />
                <span class="formw">
                  <asp:HyperLink id="LSSInvoice" runat="server" RMXRef="" RMXType="hyperlink" name="LSSInvoice" text="LSS Invoice" target="_blank" />
                </span>
              </div>
              <div id="div4" class="half" xmlns="" xmlns:mc="remove">
                <img src="../../Images/Blank.JPG" id="Img1" class="half" />
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_bankaccount" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="required" id="lbl_bankaccount" Text="Bank Account" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="bankaccount" tabindex="10" RMXRef="" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/AccountList" width="234px" onchange="setDataChanged(true);AccountChanged()" />
                </span>
              </div>
              <div runat="server" class="half" id="div_transdate" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="required" id="lbl_transdate" Text="Transaction Date" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="transdate" RMXRef="Instance/Funds/TransDate" RMXType="date" tabindex="11" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <script type="text/javascript">
                      $(function () {
                          $("#transdate").datepicker({
                              showOn: "button",
                              buttonImage: "../../Images/calendar.gif",
                              buttonImageOnly: true,
                              showOtherMonths: true,
                              selectOtherMonths: true,
                              changeYear: true
                          });
                      });
            </script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_cbopayeetype" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="required" id="lbl_cbopayeetype" Text="Payee Type" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="cbopayeetype" tabindex="12" RMXRef="/Instance/UI/FormVariables/SysExData/PayeeTypeCode" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/PayeeTypeList" onchange="setDataChanged(true);PayeeChanged()" />
                </span>
              </div>
              <div runat="server" class="half" id="div_payeephrase" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_payeephrase" Text="Payee Phrase" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="payeephrase" tabindex="13" RMXRef="/Instance/UI/FormVariables/SysExData/PayeePhrase" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/PayeePhraseList" width="234px" onchange="setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_lastname" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="required" id="lbl_pye_lastname" Text="Last Name" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" RMXRef="Instance/Funds/LastName" RMXType="financiallastname" id="pye_lastname" />
                  <asp:Textbox runat="server" style="display:none" Text="1" id="pye_lastname_creatable" />
                  <asp:button runat="server" class="EllipsisControl" id="pye_lastnamebtn" tabindex="15" onclientclick="return OnLastNameClick();" />
                </span>
              </div>
              <div runat="server" class="half" id="div_pye_firstname" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_pye_firstname" Text="First Name" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_firstname" RMXRef="Instance/Funds/FirstName" RMXType="text" tabindex="15" maxlength="50" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_entitylist" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_entitylist" Text="Payees" />
                <span class="formw">
                  <asp:Listbox runat="server" multiple="multiple" id="entitylist" RMXRef="/Instance/UI/FormVariables/SysExData/FundsXPayeeList" RMXType="multientitylist" style="" width="200" Tabindex="16" />
                  <asp:button runat="server" class="BtnRemove" ToolTip="Remove" id="entitylistbtndel" tabindex="18" onclientclick="&#xA;                return deleteSelCode('entitylist')&#xA;              " Text="-" />
                  <asp:ImageButton runat="server" src="../../Images/up.gif" width="20" height="20" title="Move Up" id="entitylistup" tabindex="18" onclientclick="&#xA;                   MoveListBoxField(0,' entitylist');return false;&#xA;                " />
                  <asp:ImageButton runat="server" src="../../Images/down.gif" width="20" height="20" title="Move Down" id="entitylistdown" tabindex="18" onclientclick="&#xA;                 MoveListBoxField(1,' entitylist');return false;&#xA;              " />
                  <asp:Textbox runat="server" style="display:none" RMXRef="/Instance/UI/FormVariables/SysExData/FundsXPayeeList/@codeid" id="entitylist_lst" />
                </span>
              </div>
              <div runat="server" class="half" id="div_paytotheorder" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_paytotheorder" Text="Pay To The Order Of" />
                <span class="formw">
                  <asp:TextBox runat="Server" id="paytotheorder" RMXRef="Instance/Funds/PayToTheOrderOf" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="17" TextMode="MultiLine" Columns="30" rows="3" />
                  <asp:button runat="server" class="MemoButton" name="paytotheorderbtnMemo" id="paytotheorderbtnMemo" onclientclick="return EditMemo('paytotheorder','');" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_taxid" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_pye_taxid" Text="Tax ID" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="ssnLostFocus(this);" onblur="lookupLostFocus(this);" tabindex="18" RMXRef="/Instance/UI/FormVariables/SysExData/FundsPayeeTaxId/PayeeTaxId" RMXType="editpayeetaxid" id="pye_taxid" />
                  <asp:button runat="server" ToolTip="Edit Tax ID" class="MemoButton" id="pye_taxidbtn" onclientclick="return OnEditPayeeTaxId();" />
                  <asp:button runat="server" ToolTip="Load Entity" class="EllipsisControl" id="LoadEntitybtn" onclientclick="return OnLoadPayeeEntity();" />
                </span>
              </div>
              <div runat="server" class="half" id="div_pye_addr1" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_pye_addr1" Text="Address 1" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_addr1" RMXRef="Instance/Funds/Addr1" RMXType="text" tabindex="19" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_addr2" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_pye_addr2" Text="Address 2" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_addr2" RMXRef="Instance/Funds/Addr2" RMXType="text" tabindex="20" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_pye_city" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_pye_city" Text="City" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_city" RMXRef="Instance/Funds/City" RMXType="text" tabindex="21" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_stateid" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_pye_stateid" Text="State" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="pye_stateid" CodeTable="states" ControlName="pye_stateid" RMXRef="Instance/Funds/StateId" CodeFilter="" RMXType="code" tabindex="22" />
                </span>
              </div>
              <div runat="server" class="half" id="div_pye_zipcode" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_pye_zipcode" Text="Zip" />
                <span class="formw">
                  <asp:TextBox runat="server" onblur="zipLostFocus(this);" onchange="setDataChanged(true);" id="pye_zipcode" RMXRef="Instance/Funds/ZipCode" RMXType="zip" TabIndex="23" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_pye_countrycode" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_pye_countrycode" Text="Country" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="pye_countrycode" CodeTable="COUNTRY" ControlName="pye_countrycode" RMXRef="Instance/Funds/CountryCode" CodeFilter="" RMXType="code" tabindex="24" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_enclosureflag" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_enclosureflag" Text="Enclosure" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="enclosureflag" RMXRef="Instance/Funds/EnclosureFlag" RMXType="checkbox" tabindex="25" Height="24px" />
                </span>
              </div>
              <div runat="server" class="half" id="div_combinedpayflag" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_combinedpayflag" Text="Combined Payment?" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="combinedpayflag" RMXRef="Instance/Funds/CombinedPayFlag" RMXType="checkbox" onclick="CombinedFlagclicked()" tabindex="26" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_autocheckflag" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_autocheckflag" Text="Auto Check?" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="autocheckflag" RMXRef="Instance/Funds/AutoCheckFlag" RMXType="checkbox" tabindex="27" Height="24px" />
                </span>
              </div>
              <div runat="server" class="half" id="div_billtypecode" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_billtypecode" Text="Type of Bill" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="billtypecode" CodeTable="BILL_TYPE" ControlName="billtypecode" RMXRef="Instance/Funds/BillTypeCode" CodeFilter="" RMXType="code" tabindex="28" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_IsEFTPayment" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_IsEFTPayment" Text="EFT Payment" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="IsEFTPayment" RMXRef="Instance/Funds/IsEFTPayment" RMXType="checkbox" tabindex="29" Height="24px" />
                </span>
              </div>
              <div runat="server" class="half" id="div_cbodormancystatus" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_cbodormancystatus" Text="Dormancy Status" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="cbodormancystatus" tabindex="30" RMXRef="/Instance/Funds/DormancyStatus" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/DormancyStatusList" onchange="setDataChanged(true);DormancyStatusChanged(this)" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_checkdate" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_checkdate" Text="Check Date" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="checkdate" RMXRef="Instance/Funds/DateOfCheck" RMXType="date" tabindex="31" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <script type="text/javascript">
                      $(function () {
                          $("#checkdate").datepicker({
                              showOn: "button",
                              buttonImage: "../../Images/calendar.gif",
                              buttonImageOnly: true,
                              showOtherMonths: true,
                              selectOtherMonths: true,
                              changeYear: true
                          });
                      });
            </script>
                </span>
              </div>
              <div runat="server" class="half" id="div_checkstatuscode" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_checkstatuscode" Text="Check Status" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="checkstatuscode" CodeTable="CHECK_STATUS" ControlName="checkstatuscode" RMXRef="Instance/Funds/StatusCode" CodeFilter="" RMXType="code" tabindex="32" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_memophrase" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_memophrase" Text="Memo Phrase" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="memophrase" tabindex="33" RMXRef="/Instance/UI/FormVariables/SysExData/memophrase" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/MemoPhraseList" onchange="setDataChanged(true);MemoPhraseChanged(this)" />
                </span>
              </div>
              <div runat="server" class="half" id="div_checknumber" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_checknumber" Text="Check Number" />
                <span class="formw">
                  <asp:TextBox runat="server" id="checknumber" RMXRef="Instance/Funds/TransNumber" RMXType="text" tabindex="34" maxlength="18" onchange="checknumberchange();setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_checkmemo" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_checkmemo" Text="Check Memo" />
                <span class="formw">
                  <asp:TextBox runat="Server" id="checkmemo" RMXRef="Instance/Funds/CheckMemo" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="35" TextMode="MultiLine" Columns="30" rows="3" />
                  <asp:button runat="server" class="MemoButton" name="checkmemobtnMemo" id="checkmemobtnMemo" onclientclick="return EditHTMLMemo('checkmemo','yes');" />
                  <asp:TextBox style="display:none" runat="server" RMXRef="Instance/Funds/CheckMemo_HTMLComments" id="checkmemo_HTML" />
                </span>
              </div>
              <div runat="server" class="half" id="div_voidcodereason" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_voidcodereason" Text="Void Reason Code" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="voidcodereason" tabindex="36" RMXRef="/Instance/UI/FormVariables/SysExData/voidcodereason" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/VoidCodeReasonList" onchange="setDataChanged(true);VoidCodeReasonChanged(this)" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_transactionnotes" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_transactionnotes" Text="Transaction Notes" />
                <span class="formw">
                  <asp:TextBox runat="Server" id="transactionnotes" RMXRef="Instance/Funds/Notes" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="37" TextMode="MultiLine" Columns="30" rows="3" />
                  <asp:button runat="server" class="MemoButton" name="transactionnotesbtnMemo" id="transactionnotesbtnMemo" onclientclick="return EditHTMLMemo('transactionnotes','yes');" />
                  <asp:TextBox style="display:none" runat="server" RMXRef="Instance/Funds/Notes_HTMLComments" id="transactionnotes_HTML" />
                </span>
              </div>
              <div runat="server" class="half" id="div_voidreason" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_voidreason" Text="Void Check Reason" />
                <span class="formw">
                  <asp:TextBox runat="Server" id="voidreason" RMXRef="Instance/Funds/VoidReason" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="38" TextMode="MultiLine" Columns="30" rows="3" />
                  <asp:button runat="server" class="MemoButton" name="voidreasonbtnMemo" id="voidreasonbtnMemo" onclientclick="return EditHTMLMemo('voidreason','yes');" />
                  <asp:TextBox style="display:none" runat="server" RMXRef="Instance/Funds/VoidReason_HTMLComments" id="voidreason_HTML" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="implements_split" RMXRef="" RMXType="id" xmlns:mc="remove" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_currencytypetext" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_currencytypetext" Text="Currency Type" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="currencytypetext" CodeTable="CURRENCY_TYPE" ControlName="currencytypetext" RMXRef="/Instance/Funds/PmtCurrencyType" CodeFilter="" RMXType="code" tabindex="40" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="hdDttmRcdAdded" RMXRef="/Instance/Funds/DttmRcdAdded" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="hdDttmRcdLastUpd" RMXRef="/Instance/Funds/DttmRcdLastUpd" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="hdupdatedbyuser" RMXRef="/Instance/Funds/UpdatedByUser" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="hdaddedbyuser" RMXRef="/Instance/Funds/AddedByUser" RMXType="id" xmlns:mc="remove" />
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABtransactiondetail" id="FORMTABtransactiondetail">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdtransactiondetail" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="partial" id="div_FundsSplitsGrid" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_FundsSplitsGrid" Text="" />
                <span>
                  <dg:UserControlDataGrid runat="server" ID="FundsSplitsGrid" GridName="FundsSplitsGrid" GridTitle="Transaction Details for Funds" Target="/Instance/UI/FormVariables/SysExData/FundsSplits" DynamicHideNodes="" Ref="/Instance/UI/FormVariables/SysExData/FundsSplits" Unique_Id="SplitRowId" ShowCloneButton="False" ShowRadioButton="True" ShowCheckBox="False" RefImplementation="False" OnClick="KeepRowForEdit('FundsSplitsGrid');FundsKeepRowForEdit('FundsSplitsGrid');;" Width="700px" Height="180px" hidenodes="" ShowHeader="True" ShowFooter="False" LinkColumn="" TextColumn="Discounted Transaction" PopupWidth="600" PopupHeight="600" Type="GridAndButtons" Align="True" CenterAlignColumns="From Date|To Date" RightAlignColumns="Amount|Inv. Amount" IncludeLastRecord="False" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="FundsSplitsGrid_RowAddedFlag" RMXRef="" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="FundsSplitsGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/FundsSplitsGrid_RowDeletedFlag" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="FundsSplitsGrid_RowEditFlag" RMXRef="" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="FundsSplitsSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/FundsSplitsSelectedId" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="FundsSplitsGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/FundsSplitsGrid_Action" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="FundsSplitsGrid_Data" RMXRef="" RMXType="id" xmlns:mc="remove" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_pmtcurrencytypetext" xmlns="" xmlns:mc="remove">
                <asp:Label runat="server" class="label" id="pmtcurrencytypetext" RMXRef="/Instance/Funds/PmtCurrencyType" RMXType="labelonly" Text="Currency Type:" DataRunTime="true" StaticValue="Currency Type:" />
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_SplitTotals" xmlns="" xmlns:mc="remove">
                <asp:Label runat="server" class="label" id="SplitTotals" RMXRef="/Instance/UI/FormVariables/SysExData/splitTotals" RMXType="labelonly" Text="Split Totals" StaticValue="Split Totals" />
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_" xmlns="" xmlns:mc="remove">
                <span class="formw">
                  <br />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABbrstransactiondetail" id="FORMTABbrstransactiondetail">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdbrstransactiondetail" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="partial" id="div_FundsBRSSplitsGrid" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_FundsBRSSplitsGrid" Text="" />
                <span>
                  <dg:UserControlDataGrid runat="server" ID="FundsBRSSplitsGrid" GridName="FundsBRSSplitsGrid" GridTitle="Transaction Details for BRS Funds Payments" Target="/Instance/UI/FormVariables/SysExData/FundsBRSSplits" DynamicHideNodes="" Ref="/Instance/UI/FormVariables/SysExData/FundsBRSSplits" Unique_Id="SplitRowId" ShowCloneButton="True" ShowRadioButton="True" ShowCheckBox="False" RefImplementation="False" OnClick="KeepRowForEdit('FundsBRSSplitsGrid');;" Width="680px" Height="180px" hidenodes="" ShowHeader="True" ShowFooter="False" LinkColumn="" PopupWidth="600" PopupHeight="600" Type="GridAndButtons" Align="True" CenterAlignColumns="From Date|To Date" RightAlignColumns="Amount|InvoiceAmount" IncludeLastRecord="False" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="FundsBRSSplitsGrid_RowAddedFlag" RMXRef="" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="FundsBRSSplitsGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/FundsBRSSplitsGrid_RowDeletedFlag" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="FundsBRSSplitsGrid_RowEditFlag" RMXRef="" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="FundsBRSSplitsSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/FundsBRSSplitsSelectedId" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="FundsBRSSplitsGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/FundsBRSSplitsGrid_Action" RMXType="id" xmlns:mc="remove" />
              <asp:TextBox style="display:none" runat="server" id="FundsBRSSplitsGrid_Data" RMXRef="" RMXType="id" xmlns:mc="remove" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_BRSSplitTotals" xmlns="" xmlns:mc="remove">
                <asp:Label runat="server" class="label" id="BRSSplitTotals" RMXRef="/Instance/UI/FormVariables/SysExData/bRSSplitTotals" RMXType="labelonly" Text="BRS Split Totals" StaticValue="BRS Split Totals" />
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdsuppgroup" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="supp_trans_id" RMXRef="/Instance/*/Supplementals/TRANS_ID" RMXType="id" xmlns:mc="remove" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_supp_int_claim_text" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_supp_int_claim_text" Text="Internal Claim" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" id="supp_int_claim_text" RMXRef="/Instance/*/Supplementals/INT_CLAIM_TEXT" RMXType="claimnumberlookup" name="supp_int_claim_text" value="" cancelledvalue="" tabindex="2258" />
                  <asp:button runat="server" class="EllipsisControl" id="supp_int_claim_textbtn" tabindex="2259" onclientclick="return lookupData('supp_int_claim_text','claim',-1,'supp_int_claim_text',6);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_supp_lss_invoice_id" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_supp_lss_invoice_id" Text="Invoice ID" />
                <span class="formw">
                  <asp:TextBox runat="server" id="supp_lss_invoice_id" RMXRef="/Instance/*/Supplementals/LSS_INVOICE_ID" RMXType="numeric" tabindex="2974" onChange="numLostFocus(this);setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_supp_lss_history_id" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_supp_lss_history_id" Text="LSS History Id" />
                <span class="formw">
                  <asp:TextBox runat="server" id="supp_lss_history_id" RMXRef="/Instance/*/Supplementals/LSS_HISTORY_ID" RMXType="numeric" tabindex="3006" onChange="numLostFocus(this);setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_supp_lss_user_id" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_supp_lss_user_id" Text="LSS User Id" />
                <span class="formw">
                  <asp:TextBox runat="server" id="supp_lss_user_id" RMXRef="/Instance/*/Supplementals/LSS_USER_ID" RMXType="text" tabindex="3007" maxlength="50" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_supp_netwrk_srvc_code" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_supp_netwrk_srvc_code" Text="Network Service Code" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="supp_netwrk_srvc_code" CodeTable="NETWRK_SRVCE_CODE" ControlName="supp_netwrk_srvc_code" RMXRef="/Instance/*/Supplementals/NETWRK_SRVC_CODE" CodeFilter="" RMXType="code" tabindex="3068" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_supp_bill_rcvd_date" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_supp_bill_rcvd_date" Text="Date Bill Received" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="supp_bill_rcvd_date" RMXRef="/Instance/*/Supplementals/BILL_RCVD_DATE" RMXType="date" tabindex="3069" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <script type="text/javascript">
                      $(function () {
                          $("#supp_bill_rcvd_date").datepicker({
                              showOn: "button",
                              buttonImage: "../../Images/calendar.gif",
                              buttonImageOnly: true,
                              showOtherMonths: true,
                              selectOtherMonths: true,
                              changeYear: true
                          });
                      });
            </script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_supp_bill_rcdmbr_date" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_supp_bill_rcdmbr_date" Text="Date Bill Rcvd By Review Co" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="supp_bill_rcdmbr_date" RMXRef="/Instance/*/Supplementals/BILL_RCDMBR_DATE" RMXType="date" tabindex="3070" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <script type="text/javascript">
                      $(function () {
                          $("#supp_bill_rcdmbr_date").datepicker({
                              showOn: "button",
                              buttonImage: "../../Images/calendar.gif",
                              buttonImageOnly: true,
                              showOtherMonths: true,
                              selectOtherMonths: true,
                              changeYear: true
                          });
                      });
            </script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_supp_oth_ven_doc_text" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_supp_oth_ven_doc_text" Text="Other Vendor Document Number" />
                <span class="formw">
                  <asp:TextBox runat="server" id="supp_oth_ven_doc_text" RMXRef="/Instance/*/Supplementals/OTH_VEN_DOC_TEXT" RMXType="text" tabindex="3071" maxlength="25" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_supp_bill_doc_no_text" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_supp_bill_doc_no_text" Text="Bill Document Number" />
                <span class="formw">
                  <asp:TextBox runat="server" id="supp_bill_doc_no_text" RMXRef="/Instance/*/Supplementals/BILL_DOC_NO_TEXT" RMXType="text" tabindex="3072" maxlength="25" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_supp_bill_rev_cm_eid" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="label" id="lbl_supp_bill_rev_cm_eid" Text="Bill Review Company" />
                <span class="formw">
                  <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="supp_bill_rev_cm_eid" RMXRef="/Instance/*/Supplementals/BILL_REV_CM_EID" RMXType="eidlookup" cancelledvalue="" tabindex="3073" />
                  <asp:Button runat="server" class="EllipsisControl" id="supp_bill_rev_cm_eidbtn" tabindex="3074" onclientclick="return lookupData('supp_bill_rev_cm_eid','',4,'supp_bill_rev_cm_eid',2)" />
                  <asp:TextBox style="display:none" runat="server" id="supp_bill_rev_cm_eid_cid" RMXref="/Instance/*/Supplementals/BILL_REV_CM_EID/@codeid" cancelledvalue="" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div id="Div5" class="formButtonGroup" runat="server" />
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="Funds" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;Funds&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/Funds&gt;" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="controlnumber|claimnumber" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="funds" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="transid" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="9650" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSplitPostback" RMXRef="" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="splitrowid" RMXRef="" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="Instance/Funds/ClaimId" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="unitid" RMXRef="Instance/Funds/UnitId" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="unitrowid" RMXRef="/Instance/UI/FormVariables/SysExData/UnitRowId" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="amount" RMXRef="Instance/Funds/Amount" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="reissueparentid" RMXRef="/Instance/UI/FormVariables/SysExData/reissueparentid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="ReissuePaymentFlag" RMXRef="/Instance/UI/FormVariables/SysExData/ReissuePaymentFlag" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isReadOnly" RMXRef="/Instance/UI/FormVariables/SysExData/IsReadOnly" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="allowVoidClearedTrans" RMXRef="/Instance/UI/FormVariables/SysExData/AllowVoidClearedTrans" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isReissuePayment" RMXRef="/Instance/UI/FormVariables/SysExData/IsReissuePayment" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="processReissuePayment" RMXRef="/Instance/UI/FormVariables/SysExData/ProcessReissuePayment" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="displayReissueRollUpPayments" RMXRef="/Instance/UI/FormVariables/SysExData/DisplayReissueRollUpPayments" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="htmlReissueRollUpPayments" RMXRef="/Instance/UI/FormVariables/SysExData/HTMLReissueRollUpPayments" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="RollUpCount" RMXRef="/Instance/UI/FormVariables/SysExData/RollUpCount" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="RollUpId" RMXRef="/Instance/UI/FormVariables/SysExData/RollUpId" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="TransNumber" RMXRef="/Instance/UI/FormVariables/SysExData/TransNumber" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="reissuereasoncode" RMXRef="/Instance/UI/FormVariables/SysExData/ReissueReasonCode" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="reissueby" RMXRef="/Instance/UI/FormVariables/SysExData/ReissueBy" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="reissuedate" RMXRef="/Instance/UI/FormVariables/SysExData/ReissueDate" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="changedchecknumber" RMXRef="/Instance/UI/FormVariables/SysExData/Changedchecknumber" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isDoNotSaveDups" RMXRef="/Instance/UI/FormVariables/SysExData/IsDoNotSaveDups" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isClaimClosed" RMXRef="/Instance/UI/FormVariables/SysExData/IsClaimClosed" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isDetailLevelTrackingOn" RMXRef="/Instance/UI/FormVariables/SysExData/IsDetailLevelTrackingOn" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="reservetracking" RMXRef="/Instance/UI/FormVariables/SysExData/Reservetracking" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isAutoClose" RMXRef="/Instance/UI/FormVariables/SysExData/IsAutoClose" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="autoCloseMessage" RMXRef="/Instance/UI/FormVariables/SysExData/AutoCloseMessage" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isFLMaxComp" RMXRef="/Instance/UI/FormVariables/SysExData/IsFLMaxComp" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isFLMaxCompMessage" RMXRef="/Instance/UI/FormVariables/SysExData/IsFLMaxCompMessage" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="lob" RMXRef="/Instance/UI/FormVariables/SysExData/LOB" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="claimIsClosed" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimIsClosed" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isNavToNewFunds" RMXRef="/Instance/UI/FormVariables/SysExData/IsNavToNewFunds" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isDuplicatePayment" RMXRef="/Instance/UI/FormVariables/SysExData/IsDuplicatePayment" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="allow_update_taxid" RMXRef="/Instance/UI/FormVariables/SysExData/Allow_update_taxid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="do_update" RMXRef="/Instance/UI/FormVariables/SysExData/Do_update" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="update_eid" RMXRef="/Instance/UI/FormVariables/SysExData/Update_eid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="update_tax_id" RMXRef="/Instance/UI/FormVariables/SysExData/Update_tax_id" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="loadPayeeEntity" RMXRef="/Instance/UI/FormVariables/SysExData/LoadPayeeEntity" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="payeeEntityType" RMXRef="/Instance/UI/FormVariables/SysExData/PayeeEntityType" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="addNewEntity" RMXRef="/Instance/UI/FormVariables/SysExData/AddNewEntity" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isCheckStatusQueued" RMXRef="/Instance/UI/FormVariables/SysExData/IsCheckStatusQueued" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isCheckStatusHold" RMXRef="/Instance/UI/FormVariables/SysExData/IsCheckStatusHold" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="thisClaimNumber" RMXRef="/Instance/Funds/ClaimNumber" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isClaimNumberReadOnly" RMXRef="/Instance/UI/FormVariables/SysExData/IsClaimNumberReadOnly" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isClearedAllowed" RMXRef="/Instance/UI/FormVariables/SysExData/IsClearedAllowed" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isTandE" RMXRef="/Instance/UI/FormVariables/SysExData/IsTandE" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="tandEClaimID" RMXRef="/Instance/UI/FormVariables/SysExData/TandEClaimID" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="htmlDuplicatePayment" RMXRef="/Instance/UI/FormVariables/SysExData/HTMLDuplicatePayment" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="bCreateHoldDiary" RMXRef="/Instance/UI/FormVariables/SysExData/BCreateHoldDiary" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="createHoldText" RMXRef="/Instance/UI/FormVariables/SysExData/CreateHoldText" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isNewVoidPayment" RMXRef="/Instance/UI/FormVariables/SysExData/IsNewVoidPayment" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isVoidRollup" RMXRef="/Instance/UI/FormVariables/SysExData/IsVoidRollup" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="claimantrowid" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantRowId" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="claimantEIdPassedIn" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantEIdPassedIn" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="unitIdPassedIn" RMXRef="/Instance/UI/FormVariables/SysExData/UnitIdPassedIn" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="unitrowIdPassedIn" RMXRef="/Instance/UI/FormVariables/SysExData/UnitrowIdPassedIn" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="brspayment" RMXRef="/Instance/UI/FormVariables/SysExData/BrsPayment" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="htmlVoidRollup" RMXRef="/Instance/UI/FormVariables/SysExData/HTMLVoidRollup" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isNewClearPayment" RMXRef="/Instance/UI/FormVariables/SysExData/IsNewClearPayment" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isAllowEditPayee" RMXRef="/Instance/UI/FormVariables/SysExData/IsAllowEditPayee" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isClearRollup" RMXRef="/Instance/UI/FormVariables/SysExData/IsClearRollup" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="skipreservetypes" RMXRef="/Instance/UI/FormVariables/SysExData/Skipreservetypes" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="htmlClearRollup" RMXRef="/Instance/UI/FormVariables/SysExData/HTMLClearRollup" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="payeeExperienceData" RMXRef="/Instance/UI/FormVariables/SysExData/PayeeExperienceData" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="payeeExperienceDataFound" RMXRef="/Instance/UI/FormVariables/SysExData/PayeeExperienceDataFound" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="getPayeeExperienceData" RMXRef="/Instance/UI/FormVariables/SysExData/GetPayeeExperienceData" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="CompRate" RMXRef="/Instance/UI/FormVariables/SysExData/CompRate" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="ValInsuffReserve" RMXRef="/Instance/UI/FormVariables/SysExData/ValInsuffReserve" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="InsuffReserve" RMXRef="/Instance/UI/FormVariables/SysExData/InsuffReserve" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="InsuffReserveNonNeg" RMXRef="/Instance/UI/FormVariables/SysExData/InsuffReserveNonNeg" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="AutoCrtWrkLossRest" RMXRef="/Instance/UI/FormVariables/SysExData/AutoCrtWrkLossRest" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="Reason" RMXRef="/Instance/UI/FormVariables/SysExData/Reason" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="MultipleCoverages" RMXRef="/Instance/UI/FormVariables/SysExData/MultipleCoverages" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="readonly" RMXRef="" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="orgEid" RMXRef="/Instance/UI/FormVariables/SysExData/DeptEid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="transid" RMXRef="Instance/Funds/TransId" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="ClaimantInformation" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantInformation" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="ClaimantLastNameFirstName" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantLastNameFirstName" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="ClaimantCount" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantCount" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="PersonInvolvedCount" RMXRef="/Instance/UI/FormVariables/SysExData/PersonInvolvedCount" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="PersonInvolvedInformation" RMXRef="/Instance/UI/FormVariables/SysExData/PersonInvolvedInformation" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="EFTBanks" RMXRef="/Instance/UI/FormVariables/SysExData/EFTBanks" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="pye_hasEFtbankInfo" RMXRef="/Instance/UI/FormVariables/SysExData/pye_hasEFtbankInfo" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="clm_entityid" RMXRef="/Instance/Funds/ClaimantEid/@codeid" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="payeetypecode" RMXRef="/Instance/Funds/PayeeTypeCode/@codeid" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="pye_entityid" RMXRef="Instance/Funds/PayeeEid/@codeid|Instance/Funds/PayeeEntity/EntityId" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="pye_middlename" RMXRef="Instance/Funds/MiddleName" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="AddNewPayee" RMXRef="" RMXType="hidden" Text="false" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="txtSuppleGridIds" RMXRef="" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="TabNameList" RMXRef="" RMXType="hidden" Text="TABStransaction|TABStransactiondetail|TABSbrstransactiondetail|TABSsuppgroup" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="PolicyID" RMXRef="" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="RcRowID" RMXRef="" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="PolCvgID" RMXRef="" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="ResTypeCode" RMXRef="" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="GroupAssocFieldList" RMXRef="" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="CovgSeqNum" RMXRef="" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="CvgLossID" RMXRef="" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="IsVoidReasonEnable" RMXRef="/Instance/UI/FormVariables/SysExData/IsVoidReasonEnable" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="IsFirstFinalQueryString" RMXRef="/Instance/UI/FormVariables/SysExData/IsFirstFinalQueryString" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="IsFirstFinalControlReradOnlyQueryString" RMXRef="/Instance/UI/FormVariables/SysExData/IsFirstFinalControlReradOnlyQueryString" RMXType="id" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="MaxPayee" RMXRef="/Instance/UI/FormVariables/SysExData/MaxPayees" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="enitytIdPassedIn" RMXRef="/Instance/UI/FormVariables/SysExData/EnitytIdPassedIn" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="CombPayeeAccountId" RMXRef="" RMXType="hidden" Text="/Instance/UI/FormVariables/SysExData/CombPayeeAccountId" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="IsCombPayee" RMXRef="" RMXType="hidden" Text="/Instance/UI/FormVariables/SysExData/IsCombPayee" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="CombPayeeCurrtype" RMXRef="" RMXType="hidden" Text="/Instance/UI/FormVariables/SysExData/CombPayeeCurrtype" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="addCombPayAccount" RMXRef="/Instance/UI/FormVariables/SysExData/AddCombPayAccount" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isDormancyRollup" RMXRef="/Instance/UI/FormVariables/SysExData/IsDormancyRollup" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="htmlDormancyRollup" RMXRef="/Instance/UI/FormVariables/SysExData/HTMLDormancyRollup" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="GetRolledUpChecksForDormancy" RMXRef="/Instance/UI/FormVariables/SysExData/GetRolledUpChecksForDormancy" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="SavedDormancyStatus" RMXRef="/Instance/UI/FormVariables/SysExData/SavedDormancyStatus" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="IsDormancyStatusChanged" RMXRef="/Instance/UI/FormVariables/SysExData/IsDormancyStatusChanged" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isVoidWithholding" RMXRef="/Instance/UI/FormVariables/SysExData/IsVoidWithholding" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="htmlGroupedWithholdingFund" RMXRef="/Instance/UI/FormVariables/SysExData/HTMLGroupedWithholdingFund" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="isClearWithholding" RMXRef="/Instance/UI/FormVariables/SysExData/IsClearWithholding" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="htmlClearWithholding" RMXRef="/Instance/UI/FormVariables/SysExData/HTMLClearWithholding" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="currencytype" RMXRef="/Instance/Funds/PmtCurrencyType" RMXType="hidden" Text="0" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="gettaxid" RMXRef="/Instance/UI/FormVariables/SysExData/Gettaxid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="foundeid" RMXRef="/Instance/UI/FormVariables/SysExData/Foundeid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="lookuptaxid" RMXRef="/Instance/UI/FormVariables/SysExData/Lookuptaxid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="allowtaxidlookup" RMXRef="/Instance/UI/FormVariables/SysExData/Allowtaxidlookup" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="funds" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="bankaccount|transdate|cbopayeetype|pye_lastname|" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="FundsSplitsGrid|FundsBRSSplitsGrid|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="ctlnumber|claimnumber|pye_lastname|pye_firstname|pye_taxid|pmtcurrencytypetext|SplitTotals|BRSSplitTotals|" />
      <asp:TextBox style="display:none" runat="server" name="SysReadonlyFields" id="SysReadonlyFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>