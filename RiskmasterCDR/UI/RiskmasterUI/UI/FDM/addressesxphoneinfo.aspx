<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addressesxphoneinfo.aspx.cs"  Inherits="Riskmaster.UI.FDM.Addressesxphoneinfo" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Phone Numbers</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <script language="JavaScript" src="../../Scripts/supportscreens.js">{var i;}</script>
    <script language="JavaScript" src="../../Scripts/grid.js">{var i;}</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Phone Numbers" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSaddressxphoneinfo" id="TABSaddressxphoneinfo">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="addressxphoneinfo" id="LINKTABSaddressxphoneinfo">Phone Number</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPaddressxphoneinfo">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="gridpopupborder" runat="server" name="FORMTABaddressxphoneinfo" id="FORMTABaddressxphoneinfo">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdaddressxphoneinfo" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="ContactId" RMXRef="//ContactId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="PhoneId" RMXRef="//PhoneId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="UniqueId" RMXRef="" RMXType="id" Text="ContactId" />
              <div runat="server" class="full" id="div_PhoneType" xmlns="">
                <asp:label runat="server" class="required" id="lbl_PhoneType" Text="Phone Type" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="PhoneType" CodeTable="phones_codes" ControlName="PhoneType" RMXRef="//PhoneType" RMXType="code" tabindex="1" Required="true" ValidationGroup="vgSave" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="full" id="div_PhoneNo" xmlns="">
                <asp:label runat="server" class="required" id="lbl_PhoneNo" Text="Phone No." />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="PhoneNo" RMXRef="//PhoneNo" RMXType="phone" tabindex="2" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="PopupGrid" RMXRef="" RMXType="id" Text="true" />
              <asp:TextBox style="display:none" runat="server" id="SessionGrid" RMXRef="" RMXType="id" Text="false" />
            </td>
          </tr>
        </table>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnOk">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnOk" RMXRef="" Text="OK" width="75px" onClientClick="return AddressesXPhoneInfo_onOk();" onclick="btnOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return AddressesXPhoneInfo_onCancel();" onclick="btnCancel_Click" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" name="formname" Text="addressxphoneinfo" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="PhoneType_codelookup_cid|PhoneNo|" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>