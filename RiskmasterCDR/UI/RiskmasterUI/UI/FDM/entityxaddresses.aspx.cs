﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 02/27/2015 | RMA-6865        | nshah28   | Swiss Re - Ability to set Effective Date_or_Expiration Date on Address
 **********************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.Shared.Controls;
using System.Xml;

namespace Riskmaster.UI.FDM
{
    public partial class Entityxaddresses : GridPopupBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            GridPopupPageload();
            if (!Page.IsPostBack) //rjhamb 21591:To prevent setting off PrimaryFlag on click of Ok Button
            {
                string sIsFirstRecord = AppHelper.GetQueryStringValue("IsFirstRecord");
                string sPrimaryFlag = AppHelper.GetQueryStringValue("PrimaryFlag");
                if (sIsFirstRecord.ToLower() == "true" && sPrimaryFlag.ToLower() == "false")
                {
                    CheckBox chkPrimaryAddress = (CheckBox)this.FindControl("PrimaryAddress");
                    if (chkPrimaryAddress != null)
                    {
                        chkPrimaryAddress.Checked = true;
                    }
                }

                //JIRA RMA-6865 nshah28 START //Setting effective date as today's date if blank.
                //JIRA RMA-9040(Suggestion on RMA-6865) - ajohari2 Start
                Control ceffectivedate = this.FindControl("effectivedate");
                TextBox txteffectivedate = null;
                if (ceffectivedate != null && ceffectivedate.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    txteffectivedate = (TextBox)ceffectivedate;
                }
                    
                if (txteffectivedate != null && string.IsNullOrEmpty(txteffectivedate.Text))
                {
                    string sToday = string.Empty;
                    sToday = AppHelper.GetDate(DateTime.Today.ToShortDateString());
                    
                    txteffectivedate.Text = sToday;
                    //JIRA RMA-9040(Suggestion on RMA-6865) - ajohari2 End
                }
                //JIRA RMA-6865 nshah28 END
            }
			
			//mbahl3 mits 31307
            //JIRA RMA-6865 nshah28 START: Commenting below lines because now ExpirationDate & EffectiveDate will be available for all (people,employee,staff,physician,patient)
            /* if (string.Equals(AppHelper.GetQueryStringValue("ParentFormName"), "people", StringComparison.InvariantCultureIgnoreCase) || string.Equals(AppHelper.GetQueryStringValue("ParentFormName"), "employee", StringComparison.InvariantCultureIgnoreCase) || string.Equals(AppHelper.GetQueryStringValue("ParentFormName"), "staff", StringComparison.InvariantCultureIgnoreCase) || string.Equals(AppHelper.GetQueryStringValue("ParentFormName"), "physician", StringComparison.InvariantCultureIgnoreCase) || string.Equals(AppHelper.GetQueryStringValue("ParentFormName"), "patient", StringComparison.InvariantCultureIgnoreCase) || string.Equals(AppHelper.GetQueryStringValue("ParentFormName"), "orghierarchymaint", StringComparison.InvariantCultureIgnoreCase)) //mbahl3 mits 31656
            {
                Control ctrlTemp = this.FindControl("div_ExpirationDate");
                if (ctrlTemp != null)
                {
                    ctrlTemp.Visible = false;
                }
                ctrlTemp = null;
                ctrlTemp = this.FindControl("div_EffectiveDate");
                if (ctrlTemp != null)
                {
                    ctrlTemp.Visible = false;
                }
                ctrlTemp = null;
            } */

			//mbahl3 mits 31307
            #region PhoneInfoDetails
            //UserControlDataGrid objGridControl = (UserControlDataGrid)this.FindControl("AddressXPhoneInfoGrid"); //Capturing Primary Flag
            //bool bPrimaryFlag = false;
            //if (objGridControl != null)
            //{
            //    GridView gvData = (GridView)objGridControl.GridView;
            //    int iGridRowCount = gvData.Rows.Count;
            //    int iPhoneTypeIndex = 0;
            //    int iPhoneNoIndex = 0;
            //    string sPhoneData = string.Empty;
            //    int iUniqueIdIndex = 0;
            //    if (iGridRowCount > 1)
            //    {
            //        for (int i = 0; i < gvData.Columns.Count - 1 ; i++)
            //        {
            //            if (gvData.Columns[i].HeaderText.ToLower() == "phone code")
            //            {
            //                iPhoneTypeIndex = i;
            //                continue;
            //            }
            //            else if (gvData.Columns[i].HeaderText.ToLower() == "phone number")
            //            {
            //                iPhoneNoIndex = i;
            //                continue;
            //            }
            //            else if (gvData.Columns[i].HeaderText.ToLower() == "uniqueid_hidden")
            //            {
            //                iUniqueIdIndex = i;
            //            }
            //        }
                
            //        for (int i = 0; i < iGridRowCount - 1; i++)
            //        {
            //            string sPhoneType = gvData.Rows[i].Cells[iPhoneTypeIndex].Text.ToLower();
            //            string sPhoneNo = gvData.Rows[i].Cells[iPhoneNoIndex].Text.ToLower();
            //            string sRowId = gvData.Rows[i].Cells[iUniqueIdIndex].Text.ToLower();
            //            if (String.IsNullOrEmpty(sPhoneData))
            //            {
            //                sPhoneData = sPhoneType + "|" + sPhoneNo + "|" + sRowId;
            //            }
            //            else
            //            {
            //                sPhoneData = sPhoneData + "," + sPhoneType + "|" + sPhoneNo + "|" + sRowId;
            //            }
            //        }
            //        TextBox txtPhoneData = (TextBox)this.FindControl("PhoneData");
            //        if (txtPhoneData != null)
            //        {
            //            txtPhoneData.Text = sPhoneData;
            //        }
           
            //    }

            //}


            //Added Rakhi For R7:Add Emp Data Elements
            #endregion 
        }
        //JIRA RMA-9040(Suggestion on RMA-6865) - ajohari2 Start
        protected void btnOk_Click(object sender, EventArgs e)
        {
            Control cPrimaryAddress = this.FindControl("PrimaryAddress");
            Control cExpirationDate = this.FindControl("ExpirationDate");
            CheckBox chkPrimaryAddress = null;
            TextBox txtExpirationDate = null;
            
            if (cPrimaryAddress != null && cPrimaryAddress.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
            {
                chkPrimaryAddress = (CheckBox)cPrimaryAddress;
            }

            if (cExpirationDate != null && cExpirationDate.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
            {
                txtExpirationDate = (TextBox)cExpirationDate;
            }

            if (txtExpirationDate != null && chkPrimaryAddress != null && !(string.IsNullOrEmpty(txtExpirationDate.Text)))
            {
                if (chkPrimaryAddress.Checked)
                {
                    DateTime ExpirationDate = Riskmaster.Common.Conversion.ToDate(txtExpirationDate.Text);
                    if (ExpirationDate != DateTime.MinValue && ExpirationDate < DateTime.Today)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "expirevalidation", "<script>alert(parent.CommonValidations.ExpirePrimaryAddress);</script>");
                        return;
                    }
                }
            }
            //RMA-8753 nshah28 for Dup popup(RMA-14262) start
            Control cDupOverride = this.FindControl("txtDupOverride");
            TextBox txtDupOverride = null;
            string sDupOverride="";
            if (cDupOverride != null && cDupOverride.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
            {
                txtDupOverride = (TextBox)cDupOverride;
                sDupOverride=txtDupOverride.Text;
            }
            XmlDocument objXml = new XmlDocument();
            TextBox txtDataParent = (TextBox)FindControlRecursive(this.Form, "txtData");
            if (txtDataParent != null)
            {
                string sData = txtDataParent.Text;
                sData = sData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                objXml.LoadXml(sData);
                XmlNodeList objNodeList = objXml.SelectNodes(objXml.FirstChild.Name);
                XmlNode objNode = objNodeList[0];
                if (objNode != null && objNode.SelectSingleNode("//dupeoverrideForAddress")!=null)
                {
                    objNode.SelectSingleNode("//dupeoverrideForAddress").InnerText = sDupOverride;
                }
                
                txtDataParent.Text = objXml.InnerXml.ToString();
            }
            //RMA-8753 nshah28 for Dup popup(RMA-14262) end
            base.btnOk_Click(sender, e);
        }
        //JIRA RMA-9040(Suggestion on RMA-6865) - ajohari2 End
    }
}
