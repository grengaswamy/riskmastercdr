<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="nonocc.aspx.cs"  Inherits="Riskmaster.UI.FDM.Nonocc" ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Non-Occ Payments</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/save.gif" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/save.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return recordSummary();" src="../../Images/recordsummary.gif" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" />
        </div>
      </div>
      <br />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Non-Occ Payments" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSclaiminfo" id="TABSclaiminfo">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="claiminfo" id="LINKTABSclaiminfo">Claim Info</a>
        </div>
        <div id="Div3" class="tabSpace" runat="server">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSbenperiod" id="TABSbenperiod">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="benperiod" id="LINKTABSbenperiod">Benefit Period</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSpaycalc" id="TABSpaycalc">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="paycalc" id="LINKTABSpaycalc">Payments Calculator</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" style="height:310px;overflow:auto" name="FORMTABclaiminfo" id="FORMTABclaiminfo">
        <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimId" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="pieid" RMXRef="/Instance/UI/FormVariables/SysExData/PiEid" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="pirowid" RMXRef="/Instance/UI/FormVariables/SysExData/PiRowId" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="entity_id" RMXRef="/Instance/UI/FormVariables/SysExData/Entity_Id" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="claimnumber" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimNumber" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_ballowcalc" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="ballowcalc" RMXRef="/Instance/UI/FormVariables/SysExData/AllowCalc" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_taxarray" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="taxarray" RMXRef="/Instance/UI/FormVariables/SysExData/TaxArray" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_iseligible" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="iseligible" RMXRef="/Instance/UI/FormVariables/SysExData/IsEligible" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_planeligdate" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="planeligdate" RMXRef="/Instance/UI/FormVariables/SysExData/EligDate" RMXType="labelonly" Text="" DataRunTime="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_disfromdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_disfromdate" Text="Disability Start Date" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/DisabilityFromDate" id="disfromdate" tabindex="2" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_benstartdatereadonly" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_benstartdatereadonly" Text="Benefits Start Date" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/BenefitStartDateReadOnly" id="benstartdatereadonly" tabindex="3" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_planname" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_planname" Text="Plan Name" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PlanName" id="planname" tabindex="4" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_calcmethod" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_calcmethod" Text="Calculation Method" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/CalcMethod" id="calcmethod" tabindex="5" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_wage" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_wage" Text="Hourly Wage" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/Wage" id="wage" tabindex="6" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_weeklybenefit" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_weeklybenefit" Text="Weekly Benefit" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/WeeklyBenefit" id="weeklybenefit" tabindex="7" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_supplement" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_supplement" Text="Supplement" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/Supplement" id="supplement" tabindex="7" style="background-color: silver;" readonly="true" />
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABbenperiod" id="FORMTABbenperiod">
        <div runat="server" class="half" id="div_benstartdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_benstartdate" Text="Benefits Start" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="benstartdate" RMXRef="/Instance/UI/FormVariables/SysExData/BenefitStartDate" RMXType="date" tabindex="8" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="benstartdatebtn" tabindex="9" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "benstartdate",
					ifFormat : "%m/%d/%Y",
					button : "benstartdatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_benthroughdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_benthroughdate" Text="Benefits Through" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="benthroughdate" RMXRef="/Instance/UI/FormVariables/SysExData/BenefitsThrough" RMXType="date" tabindex="10" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="benthroughdatebtn" tabindex="11" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "benthroughdate",
					ifFormat : "%m/%d/%Y",
					button : "benthroughdatebtn"
					}
					);
				</script>
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABpaycalc" id="FORMTABpaycalc">
        <div runat="server" class="half" id="div_paymentsstartdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_paymentsstartdate" Text="Payments Beginning" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="paymentsstartdate" RMXRef="/Instance/UI/FormVariables/SysExData/BenCalcPayStart" RMXType="date" tabindex="12" onchange="NonOccPaymentsRefreshScreen(this);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="paymentsstartdatebtn" tabindex="13" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "paymentsstartdate",
					ifFormat : "%m/%d/%Y",
					button : "paymentsstartdatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_paymentsthroughdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_paymentsthroughdate" Text="Payments Through" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="paymentsthroughdate" RMXRef="/Instance/UI/FormVariables/SysExData/BenCalcPayTo" RMXType="date" tabindex="14" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="paymentsthroughdatebtn" tabindex="15" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "paymentsthroughdate",
					ifFormat : "%m/%d/%Y",
					button : "paymentsthroughdatebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_chkfederaltax" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_chkfederaltax" Text="Deduct Federal Tax" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="chkfederaltax" RMXRef="/Instance/UI/FormVariables/SysExData/ChkFederalTax" RMXType="checkbox" onclick="NonOccPaymentsRefreshScreen(this);" tabindex="16" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_FederalTax" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_FederalTax" Text="" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/FederalTax" id="FederalTax" tabindex="22" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_chksstax" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_chksstax" Text="Deduct Soc Sec Tax" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="chksstax" RMXRef="/Instance/UI/FormVariables/SysExData/ChkSSTax" RMXType="checkbox" onclick="NonOccPaymentsRefreshScreen(this);" tabindex="17" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_SocialSecurityAmount" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_SocialSecurityAmount" Text="" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/SocialSecurityAmount" id="SocialSecurityAmount" tabindex="22" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_chkmedtax" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_chkmedtax" Text="Deduct Medicare Tax" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="chkmedtax" RMXRef="/Instance/UI/FormVariables/SysExData/ChkMedTax" RMXType="checkbox" onclick="NonOccPaymentsRefreshScreen(this);" tabindex="18" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_MedicareAmount" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_MedicareAmount" Text="" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/MedicareAmount" id="MedicareAmount" tabindex="22" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_chkstatetax" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_chkstatetax" Text="Deduct State Tax" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="chkstatetax" RMXRef="/Instance/UI/FormVariables/SysExData/ChkStateTax" RMXType="checkbox" onclick="NonOccPaymentsRefreshScreen(this);" tabindex="19" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_StateAmount" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_StateAmount" Text="" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/StateAmount" id="StateAmount" tabindex="22" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_transactiontypecode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_transactiontypecode" Text="Transaction Type" />
          <span class="formw">
            <asp:DropDownList runat="server" id="transactiontypecode" tabindex="21" RMXRef="/Instance/UI/FormVariables/SysExData/TransactionType/@codeid" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/TransactionType" onchange="setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_bankaccount" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_bankaccount" Text="" />
          <span class="formw">
            <asp:DropDownList runat="server" id="bankaccount" tabindex="20" width="234px" RMXRef="/Instance/UI/FormVariables/SysExData/AccountList/@codeid" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/AccountList" onchange="setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_reservetype" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_reservetype" Text="Reserve Type" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/ReserveType" id="reservetype" tabindex="22" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_grosscalculatedpayment" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_grosscalculatedpayment" Text="Gross Calculated Payment" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/GrossCalculatedPayment" id="grosscalculatedpayment" tabindex="25" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_pensionoffset" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_pensionoffset" Text="Pension Offset" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="pensionoffset" RMXRef="/Instance/UI/FormVariables/SysExData/PensionOffset" RMXType="numeric" tabindex="24" onChange="NonOccPaymentsRefreshScreen(this);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_grosscalculatedsupplement" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_grosscalculatedsupplement" Text="Gross Calculated Supplement" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/GrossCalculatedSupplement" id="grosscalculatedsupplement" tabindex="25" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_socialsecurityoffset" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_socialsecurityoffset" Text="Social Security Offset" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="socialsecurityoffset" RMXRef="/Instance/UI/FormVariables/SysExData/SocialSecurityOffset" RMXType="numeric" tabindex="24" onChange="NonOccPaymentsRefreshScreen(this);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_grosstotalnetoffsets" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_grosstotalnetoffsets" Text="Gross Total Net Offsets" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/GrossTotalNetOffsets" id="grosstotalnetoffsets" tabindex="27" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_otherincomeoffset" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_otherincomeoffset" Text="Other Income Offset" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="otherincomeoffset" RMXRef="/Instance/UI/FormVariables/SysExData/OtherIncomeOffset" RMXType="numeric" tabindex="24" onChange="NonOccPaymentsRefreshScreen(this);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_netpayment" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_netpayment" Text="Net Payment" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/NetPayment" id="netpayment" tabindex="28" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_reservetypearray" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="reservetypearray" RMXRef="/Instance/UI/FormVariables/SysExData/ReserveTypeArray" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_dailyamount" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="dailyamount" RMXRef="/Instance/UI/FormVariables/SysExData/DailyAmount" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_dailysuppamount" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="dailysuppamount" RMXRef="/Instance/UI/FormVariables/SysExData/DailySuppAmount" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_dPension" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="dPension" RMXRef="/Instance/UI/FormVariables/SysExData/dPension" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_dSS" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="dSS" RMXRef="/Instance/UI/FormVariables/SysExData/dSS" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_dOther" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="dOther" RMXRef="/Instance/UI/FormVariables/SysExData/dOther" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_iDaysWorkingInWeek" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="iDaysWorkingInWeek" RMXRef="/Instance/UI/FormVariables/SysExData/iDaysWorkingInWeek" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_iDaysWorkingInMonth" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="iDaysWorkingInMonth" RMXRef="/Instance/UI/FormVariables/SysExData/iDaysWorkingInMonth" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_iOffsetCalc" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="iOffsetCalc" RMXRef="/Instance/UI/FormVariables/SysExData/iOffsetCalc" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_ilblDaysIncluded" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="ilblDaysIncluded" RMXRef="/Instance/UI/FormVariables/SysExData/ilblDaysIncluded" RMXType="hidden" />
          </span>
        </div>
      </div>
      <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
          <asp:Button class="button" runat="Server" id="BackToParent" Text="Back to Claim" OnClientClick="if(!( XFormHandler('','1','back')))return false;" PostBackUrl="?" />
        </div>
        <div class="formButton" runat="server" id="div_collection">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="collection" RMXRef="" Text="Collection" onClientClick="nonocccollection()" />
        </div>
        <div class="formButton" runat="server" id="div_printedpaym">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="printedpaym" RMXRef="" Text="Printed Payments" onClientClick="printedPayments()" />
        </div>
        <div class="formButton" runat="server" id="div_futurepaym">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="futurepaym" RMXRef="" Text="Future Payments" onClientClick="futurePayments()" />
        </div>
        <div class="formButton" runat="server" id="div_calculatepaym">
          <script language="JavaScript" src="../../Scripts/nonocc.js">{var i;}
          </script>
          <asp:button class="button" runat="server" id="calculatepaym" RMXRef="" Text="Calculate Payments" onClientClick="calcPayments()" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="Claim" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;Claim&gt;&lt;PrimaryPiEmployee&gt;&lt;PiEntity/&gt;&lt;/PrimaryPiEmployee&gt;&lt;/Claim&gt;" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="claimdi" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="claimid" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="claimnumber" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="nonocc" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="claimid" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="60000" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="62500" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="nonocc" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="benstartdate|benthroughdate|paymentsstartdate|paymentsthroughdate|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="benstartdate|paymentsstartdate|" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>