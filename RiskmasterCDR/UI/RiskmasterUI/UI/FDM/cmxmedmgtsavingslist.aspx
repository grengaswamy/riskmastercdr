﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cmxmedmgtsavingslist.aspx.cs" Inherits="Riskmaster.UI.FDM.CmXMedmgtsavingsList" ValidateRequest="false" EnableViewStateMac="false" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>Untitled Page</title>
     <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <%--<link href="../../App_Themes/RMX_Default/dhtml-combo.css" type="text/css" rel="stylesheet" />
    <link href="../../App_Themes/RMX_Default/IEWin.css" type="text/css" rel="stylesheet" />
    <link href="../../App_Themes/RMX_Default/RiskmastercomStyles.css" type="text/css" rel="stylesheet" />--%>
    <link href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" rel="stylesheet" />
    <%--<link href="../../App_Themes/RMX_Default/round-button.css" type="text/css" rel="stylesheet" />
    <link href="../../App_Themes/RMX_Default/styles.css" type="text/css" rel="stylesheet" />
    <link href="../../App_Themes/RMX_Default/Tabs.css" type="text/css" rel="stylesheet" />--%>
    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" src="Scripts/drift.js"></script>
	<script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
  <script type="text/javascript" src="../../Scripts/pagingsorting.js"></script>     
</head>
<body onload="pageLoaded()" class="10pt">
    <form id="frmData" runat="server">
    <%-- Mits 28937        --%>     
    <asp:ImageButton runat="server" id="imgBtnAddNew" src="../../Images/tb_new_active.png" onMouseOver="this.src='../../Images/tb_new_mo.png';" onMouseOut="this.src='../../Images/tb_new_active.png';"  Height="28" Width="28" BorderWidth="0" runat="server" tooltip="<%$ Resources:imgbtnAddNew %>" Text="<%$ Resources:imgbtnAddNew %>" postbackurl="cmxmedmgtsavings.aspx?SysFormName=CmXMedmgtsavings&SysCmd=&SysViewType=tab&SysFormIdName=cmmsrowid&SysFormPIdName=casemgtrowid&SysEx=CasemgtRowId|EventNumber&SysExMapCtl=CasemgtRowId|EventNumber" />
    <asp:ImageButton runat="server" ID="imgBtnDelete" src="../../Images/tb_delete_active.png" onMouseOver="this.src='../../Images/tb_delete_mo.png';" onMouseOut="this.src='../../Images/tb_delete_active.png';"  Height="28" Width="28" BorderWidth="0" Text="<%$ Resources:imgbtnDelete %>" Tooltip ="<%$ Resources:imgbtnDelete %>"  OnClick="NavigateListDelete"  OnClientClick="return DeleteList();"/>
    <asp:ImageButton runat="server" src="../../Images/tb_backrecord_active.png" width="28" height="28" border="0" id="BackToParent" AlternateText="<%$ Resources:imgbtnBack %>" onMouseOver="this.src='../../Images/tb_backrecord_mo.png';" onMouseOut="this.src='../../Images/tb_backrecord_active.png';" Text="<%$ Resources:imgbtnBack %>" Tooltip ="<%$ Resources:imgbtnBack %>"  onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>      
    <%-- End of Mits 28937 --%>
    <div>
    <asp:Label ID="lblError" runat="server" Text="" ForeColor=Red></asp:Label>
    <%--<h2>Medical Management Savings List</h2>--%>
       <%-- <div id="div_formtitle" class="msgheader"><span id="formtitle">Medical Management Savings List</span><span id="formsubtitle"></span></div>--%>
        <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
    <tr class="msgheader">
     <td class="msgheader" colspan=""><asp:Label id="lblHeader" runat="server" Class="required" Text="<%$ Resources:lblHeader %>"></asp:Label></td>
    </tr>
   </table>
        </br>
    <table border="0" width="95%" cellspacing="0" cellpadding="2" id="tbllist">
    <tr>
     <td class="ctrlgroup" width="50px"></td>
     <td class="ctrlgroup" width="50px"></td>
     <td class="ctrlgroup"><asp:HyperLink href="#" runat="server" id="HeaderColumn2" class="ctrlgroup" onclick="SortList(document.all.tbllist,2,'lnkEdit',true );" Text='<%$ Resources:hyplnkprovidergroup %>'></asp:HyperLink></td>
     <td class="ctrlgroup" nowrap="1"><asp:HyperLink href="#" runat="server" id="HeaderColumn3" class="ctrlgroup" onclick="SortList(document.all.tbllist,3,'lnkEdit',true );" Text='<%$ Resources:hyplnksavingstype %>'></asp:HyperLink></td>
     <td class="ctrlgroup" nowrap="1"><asp:HyperLink href="#" runat="server" id="HeaderColumn4" class="ctrlgroup" onclick="SortList(document.all.tbllist,4,'lnkEdit',true );" Text='<%$ Resources:hyplnktransactiontype %>'></asp:HyperLink></td>
     <td class="ctrlgroup" nowrap="1"><asp:HyperLink href="#" runat="server" id="HeaderColumn5" class="ctrlgroup" onclick="SortList(document.all.tbllist,5,'lnkEdit',true );" Text='<%$ Resources:hyplnksavings %>'></asp:HyperLink></td>
     <td class="ctrlgroup" nowrap="1"><asp:HyperLink href="#" runat="server" id="HeaderColumn6" class="ctrlgroup" onclick="SortList(document.all.tbllist,6,'lnkEdit',true );" Text='<%$ Resources:hyplnksummary %>'></asp:HyperLink></td>
    </tr>
    <%int i = 0; foreach (XElement item in result)
    {
        string rowclass = "";
        if ((i % 2) == 1) rowclass = "datatd1";
        else rowclass = "datatd";
        i++;
        %>
        <tr>                          
            <td class="<%=rowclass%>">
            <%
                PageId.ID = "PageId" + i;
                PageId.Value=item.Element("CmmsRowId").Value;
           %>                              
                <input type ="checkbox" name="PageId" id="PageId" title="Medical Management Savings List" runat="server"/></td>                            
            
            <td class="<%=rowclass%>">
               <%
                   lnkEdit.ID = "lnkEdit" + i;
                   lnkEdit.PostBackUrl = "cmxmedmgtsavings.aspx?SysFormName=CmXMedmgtsavings&SysCmd=0&SysViewType=tab&SysFormIdName=cmmsrowid&SysFormId=" + item.Element("CmmsRowId").Value + "&SysFormPIdName=casemgtrowid&SysEx=CasemgtRowId|EventNumber&SysExMapCtl=CasemgtRowId|EventNumber";
                %>
              <asp:LinkButton runat="server" id="lnkEdit" Text="<%$ Resources:hyplnkEdit %>"></asp:LinkButton> 
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("ProviderEid").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("SavingsTypeCode").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("TransactionTypeCode").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("Savings").Value%> 
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("Summary").Value%>
            </td>
            <td style="display:none">                           
            </td>
            <td style="display:none">                           
               <%=lnkEdit.PostBackUrl%>
            </td>    
            <td style="display:none">
              <%=item.Element("CmmsRowId").Value%>
            </td>
       </tr>                          
 <% }%>
 </table>
 <table border="0" width="95%" cellspacing="0" cellpadding="2">
 <tr>
     <td colspan="6">&nbsp;</td>
    </tr> 
 <tr>
 <td></td>
 <td colspan="3">
	<asp:Label id="lblTotalSavings" runat="server" Class="required" Text="<%$ Resources:lblTotalSavings %>" style="font-weight:bold" ></asp:Label>
  </td>
	<td>
		<%=totalSavings.Value%>	
	</td>			
 
 </tr>
   <tr>
     <td colspan="6"></td>
   </tr>  
 
    </table>
    <center><div id="pageNavPosition"></div></center>
    <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
    <asp:TextBox runat="server" id="TextBox1" style="display:none" />
    <asp:TextBox runat="server" id="DeleteListTemplate"  style="display:none" Text="Instance/CmXMedmgtsavingsList/CmXMedmgtsavings/CmmsRowId"/>				
    <asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
    <asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
    <asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
    <asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
    <asp:TextBox runat="server" id="SysClassName" style="display:none" Text="CmXMedmgtsavingsList" RMXRef="Instance/UI/FormVariables/SysClassName"/>						
    <asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" Text="&lt;CmXMedmgtsavingsList&gt;&lt;CmXMedmgtsavings /&gt;&lt;/CmXMedmgtsavingsList&gt;"	RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>	
    <asp:TextBox runat="server" id="SysSerializationConfigIgnore" style="display:none" Text="Claim.Comments"	RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore"/>						                             
    <asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm"/>
    <asp:TextBox runat="server" id="SysFormPIdName" Text="casemanagementid" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
    <asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
    <asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>
    <asp:TextBox runat="server" id="SysNotReqNew" style="display:none" RMXRef="Instance/UI/FormVariables/SysNotReqNew" />
    <asp:TextBox runat="server" id="SysFormName" style="display:none" Text="CmXMedmgtsavingsList" RMXRef="Instance/UI/FormVariables/SysFormName"/>
    <asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="casemanagementid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId"/>
    <asp:TextBox runat="server" id="SysSid" style="display:none" Text="11000" RMXRef="Instance/UI/FormVariables/SysSid"/>
    <asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
    <asp:TextBox ID="paramstore" runat="server" style="display:none"></asp:TextBox>	
    <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none"/>
    <asp:TextBox runat="server" id="eventnumber" RMXRef="Instance/UI/FormVariables/SysExData/EventNumber" style="display:none"/>
    <asp:TextBox runat="server" id="claimid" RMXRef="Instance/UI/FormVariables/SysExData/ClaimId" style="display:none"/>
    <asp:TextBox runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" style="display:none"/>
    <asp:TextBox runat="server" id="casemgtrowid" RMXRef="Instance/UI/FormVariables/SysExData/CasemgtRowId" style="display:none"/>
    <asp:TextBox runat="server" id="casemanagementid" RMXRef="Instance/UI/FormVariables/SysExData/CasemgtRowId" style="display:none"/>
    <asp:TextBox runat="server" id="cmmsrowid" RMXRef="Instance/UI/FormVariables/SysExData/CmmsRowId"   style="display:none"/>   
    <%-- Mits 28937        --%>         
    <%--<asp:button class="button" runat="server" id="btnAddNew" Text="Add New" postbackurl="cmxmedmgtsavings.aspx?SysFormName=CmXMedmgtsavings&SysCmd=&SysViewType=tab&SysFormIdName=cmmsrowid&SysFormPIdName=casemgtrowid&SysEx=CasemgtRowId|EventNumber&SysExMapCtl=CasemgtRowId|EventNumber" />
    <asp:Button class="button" runat="server" ID="btnDelete" Text="Delete" OnClick="NavigateListDelete"  OnClientClick="return DeleteList();"/>
    <asp:Button class="button" runat="server" ID="Back" Text="Back" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>   --%>
    <%-- End of Mits 28937        --%>    
    <input type="hidden" runat="server" id="totalSavings" value=0 />
    <input type="text" runat="server" id="PageIds" style="display:none"/>
    </div>
     <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
    <script type="text/javascript">
        var listpaging = new ListPaging('tbllist', 3); 
        listpaging.init(); 
        listpaging.showPageNavigation('listpaging', 'pageNavPosition'); 
        listpaging.showPage(1);
    </script>
</body>
</html>
