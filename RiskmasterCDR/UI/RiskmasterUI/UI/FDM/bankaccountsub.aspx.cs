﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.Shared.Controls;
namespace Riskmaster.UI.FDM
{
    public partial class Bankaccountsub : FDMBasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
        }
        public override void ModifyControls()
        {
            //mona: Making Policy Lob on Bank Account as multicode and non mandatory
            XmlDocument oXmlData = base.Data;
            //mona: Making Policy Lob on Bank Account as multicode and non mandatory
            if (oXmlData.SelectSingleNode("./Document/ParamList/Param[@name='SysPropertyStore']/BankAccSub/PolicyLOBCodes").ChildNodes.Item(0).Attributes["value"].Value == "0")
            {
                RadioButton objAllPolLob = (RadioButton)(this.FindControl("AllPolLOB"));
                if (objAllPolLob != null)
                    objAllPolLob.Checked = true;

                 RadioButton objSpecificPolLOB = (RadioButton)(this.FindControl("SpecificPolLOB"));
                 if (objSpecificPolLOB != null)
                    objSpecificPolLOB.Checked = false;

                MultiCodeLookUp objPolLoB = (MultiCodeLookUp)(this.FindControl("policyLOBCode"));
                if (objPolLoB != null)
                {
                    objPolLoB.Enabled = false;
                }
            }
            else
            {
                RadioButton objSpecificPolLOB = (RadioButton)(this.FindControl("SpecificPolLOB"));
                if (objSpecificPolLOB != null)
                    objSpecificPolLOB.Checked = true;

                MultiCodeLookUp objPolLoB = (MultiCodeLookUp)(this.FindControl("policyLOBCode"));
                if (objPolLoB != null)
                {
                    objPolLoB.Enabled = true;
                }
            }
        }
    }
}
