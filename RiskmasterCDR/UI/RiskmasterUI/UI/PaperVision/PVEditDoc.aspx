<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PVEditDoc.aspx.cs" Inherits="Riskmaster.UI.PaperVision.EditDoc" EnableViewStateMac="false" %>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3" %>



<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Riskmaster</title>
    <uc4:CommonTasks  ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="javascript" src='/RiskmasterUI/Scripts/EditDoc.js'></script>
    <script language='javascript'>

    //Bharani - MITS : 34451 - Change 1/2 - Start
function CallPaperVision()
{
    var bIsAtPaperVision;
    var strPVUrl;
    if(eval("document.forms[0].hdnIsAtPaperVision")!=null)
    {
        bIsAtPaperVision=document.forms[0].hdnIsAtPaperVision.value;        
    }
    if(eval("document.forms[0].PVDocUrl")!=null)
    {
        strPVUrl=document.forms[0].PVDocUrl.value;         
    }
    if (strPVUrl != "" && bIsAtPaperVision == "-1" )
    {
    window.open(strPVUrl,'','width=500,height=300' + ',top=' + (screen.availHeight - 300) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }    
}

function ReleasePVSession() //MITS 19654
{
      var bIsAtPaperVision;
      var strPVUrl;  
      if(eval("document.forms[0].hdnIsAtPaperVision")!=null)
      {
        bIsAtPaperVision=document.forms[0].hdnIsAtPaperVision.value;        
      }
      if(eval("document.forms[0].PVDocUrl")!=null)
      {
        strPVUrl=document.forms[0].PVDocUrl.value;         
      }
      if (strPVUrl != "" && bIsAtPaperVision == "-1" )
      {
        document.forms[0].hdnAction.value="close"; 
        document.forms[0].submit();
      }
}

    function DoNotSubmit()
    {
        document.getElementsByTagName("BODY")[0].onunload = "";
    }
    //Bharani - MITS : 34451 - Change 1/2 - End

    </script>
</head> 

<body>
<form id="frmData"  method="post" runat="server"> 
<p>
<!-- Bharani - MITS : 34451 - Change 2/2 - Start -->
<asp:HiddenField ID="hdnIsAtPaperVision" runat="server"/>
<asp:HiddenField ID="PVDocUrl" runat="server"/>
<!-- Bharani - MITS : 34451 - Change 2/2 - End -->
<table class="singleborder" align="center">

 <tr>
      <td colspan="2">
          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
      </td>
     </tr>
     <tr>
      <td colspan="2" class="ctrlgroup">Edit Document Properties</td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b><u>Document Folder:</u></b></td>
      <td class="datatd">
           <b><u><asp:Label ID="lblFolderName" runat=server></asp:Label></u></b>     
      </td>
     </tr>
     
    <tr>
      <td class="datatd" nowrap=""><b><u>Title:</u></b></td>
      <td class="datatd"><input type="text" id="Title" runat="server" size="32" maxlength="32" style="border: 1px solid #104A7B;" ></td>
     </tr>
     <tr>
      <td class="datatd" nowrap=""><b>Subject:</b></td>
      <td class="datatd"><input type="text" id="Subject" runat="server"  size="50" maxlength="50" style="border: 1px solid #104A7B;"></td>
     </tr>
     <tr>
      <td class="datatd" nowrap=""><b>Type:</b></td>
        <td class="datatd">
                
            <input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);" name="documenttypecode" id="documenttypecode" cancelledvalue=""></input>
<input type="button" name="documenttypecodebtn"  class="CodeLookupControl"  id="documenttypecodebtn" onclick="return selectCode('DOCUMENT_TYPE','documenttypecode')" />

<input type="hidden" name="documenttypecode_cid"  runat="server"  id="documenttypecode_cid"  />
                
         </td>
      </tr>
     <tr>
      <td class="datatd" nowrap=""><b>Class:</b></td>
      <td class="datatd"><input type="text" size="30"  runat="server" onblur="codeLostFocus(this.id);"   onchange="lookupTextChanged(this);"  id="documentclasscode" cancelledvalue=""></input>
<input type="button" name="documentclasscodebtn"    class="CodeLookupControl"  id="documentclasscodebtn" onclick="return selectCode('DOCUMENT_CLASS','documentclasscode')" />

<input type="hidden" name="documentclasscode_cid"    runat="server"  id="documentclasscode_cid"  />
              
                        </td>
      
     </tr>
     <tr>
      <td class="datatd" nowrap=""><b>Category:</b></td>
         <td class="datatd"> <input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);" name="documentcategorycode" id="documentcategorycode" cancelledvalue=""></input>
<input type="button" name="documentcategorycodebtn"  class="CodeLookupControl"  id="documentcategorycodebtn" onclick="return selectCode('DOCUMENT_CATEGORY','documentcategorycode')" />

<input type="hidden" runat="server"  name="documentcategorycode_cid"  id="documentcategorycode_cid"  /></td>
     </tr>
     <tr>
      <td class="datatd" nowrap=""><b>Keywords:</b></td>
      
      <td class="datatd"><input type="text" runat="server" id="Keywords" value="" size="50" maxlength="200" style="border: 1px solid #104A7B;"></td>
     </tr>
     <tr>
      <td class="datatd" nowrap=""><b>Notes:</b></td>
      <td class="datatd"><textarea cols="38" runat="server" wrap="soft" id="Notes" rows="6" size="" style="border: 1px solid #104A7B;"></textarea></td>
     </tr>
     <tr>
      <td class="datatd"><b>File Name:</b></td>
     <!-- Bharani - MITS : 33886 - Change 1 - Start -->       
     <%if (IfOfficeDocumentOrTextType())
        { %>
        <td><a href="#" onclick="javaScript:CallPaperVision();"><%= hdnFileName.Value %></a></td>
      <%} %>
      <%else{ %>
      <td><input type="text" runat="server" id="FileName" readonly size="50" style="border: 0px solid #104A7B;" /></td>
      <%} %>
	  <!-- Bharani - MITS : 33886 - Change 1 - End -->
     </tr>
     
     <tr>
      <td nowrap="" class="datatd"><b>Created on:</b></td>
      <td class="datatd"><input type="text" runat="server" readonly id="CreateDate"  size="17" style="border: 0px solid #104A7B;" /></td>
     </tr>
    
     <tr>
      
     <td  colspan="2" align="center">
         <asp:Button ID="btnSave" runat="server"  Text="  Save  " CssClass="button" OnClick ="btnSave_Click"/>&nbsp;
            
    
            
         <asp:Button ID="btnCancel" runat="server" 
             Text="   Cancel   " CssClass="button"  OnClick="btnCancel_Click"/>
             </td>
     </tr>
     <asp:HiddenField ID="Regarding" runat="server" Value=""/>
     <!-- Bharani - MITS : 33886 - Change 2 - Start -->  
     <asp:HiddenField ID="hdnFileName" runat="server" Value=""/>
     <!-- Bharani - MITS : 33886 - Change 2 - End -->  
      <asp:HiddenField ID="FolderId" runat="server" />
     <asp:HiddenField ID="hdocId" runat="server" />
      <asp:HiddenField ID="Psid" runat="server" />
      <asp:HiddenField ID="flag" runat="server" />
      <!-- Bharani - MITS : 33886 - Change 3 - Start -->  
      <input type="hidden" id="hdnTempFileName" runat=server />
      <!-- Bharani - MITS : 33886 - Change 3 - End -->  
      <asp:HiddenField ID="hdnPageNumber" runat="server" />
      <asp:HiddenField ID="hdnSortExpression" runat="server" />
      <asp:HiddenField ID="AttachTable" runat="server" />
      <asp:HiddenField ID="FormName" runat="server" />
      <asp:HiddenField ID="AttachRecordId" runat="server" />
      <asp:HiddenField ID="FolderName" runat="server" />
      <asp:HiddenField ID="AttachTableName" runat="server" />
      <!-- Bharani - MITS : 33886 - Change 4 - Start -->  
      <input type="hidden" id="hdnContent" runat="server" />
      <input type="hidden" id="hdnRTFContent" runat="server" />
      <!-- Bharani - MITS : 33886 - Change 4 - End -->  
      <asp:HiddenField ID="NonMCMFormName" runat="server" />
    </table>
    
    </p>
    </form>
    
   </body>
   </html> 
