﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Riskmaster.UI.DocumentService;
using System.Web.UI.WebControls;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using CuteWebUI;
using Riskmaster.Models;
namespace Riskmaster.UI.PaperVision
{
    public partial class AddDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strPaperVisionFlag = string.Empty ; //Animesh Inserted MITS 18345
            if (!Page.IsPostBack)
            {
                SetInitialValues();
                FolderId.Value = AppHelper.GetQueryStringValue("folderid");
                Psid.Value = AppHelper.GetQueryStringValue("psid");
                //Animesh Inserted MITS 18345
                strPaperVisionFlag = AppHelper.GetQueryStringValue("papervisionactive").ToLower();
                if (strPaperVisionFlag == "true")
                {
                    UploadDocumentAttachments.ValidateOption.MaxSizeKB = 102400; //Setting upload limit to 100 MB
                }
                else
                {
                    UploadDocumentAttachments.ValidateOption.MaxSizeKB = 35000;   //Setting upload limit to default RMX limit.
                }
                //Animesh insertion ends
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
       
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            Psid.Value = AppHelper.GetValue("Psid");
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
            NonMCMFormName.Value = AppHelper.GetValue("NonMCMFormName");
        }
        protected void SaveData_Click(object sender, EventArgs e)
        {
            DocumentBusinessHelper dc = null;
            bool isError = false;
            try
            {
                dc = new DocumentBusinessHelper();
                dc.AddMultiple(UploadDocumentAttachments.Items);               
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                isError = true;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                isError = true;
            }           
            if (!isError)
                Server.Transfer("/RiskmasterUI/UI/PaperVision/PVDocumentList.aspx");
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("/RiskmasterUI/UI/PaperVision/PVDocumentList.aspx");
        }
    }
}
