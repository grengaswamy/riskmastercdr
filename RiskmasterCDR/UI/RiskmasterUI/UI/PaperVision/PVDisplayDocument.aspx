﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PVDisplayDocument.aspx.cs" Inherits="Riskmaster.UI.PaperVision.DisplayDocument"  EnableViewStateMac ="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3"  %>



<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title>Riskmaster</title>
<uc4:CommonTasks ID="CommonTasks1" runat="server" />
<script language='javascript'>

function EditDocument()
{
   //Animesh Inserted MITS 19654
   var bIsAtPaperVision;
   var strPVUrl;  
   if(eval("document.forms[0].hdnIsAtPaperVision")!=null)
   {
        bIsAtPaperVision=document.forms[0].hdnIsAtPaperVision.value;        
   }
   if(eval("document.forms[0].PVDocUrl")!=null)
   {
        strPVUrl=document.forms[0].PVDocUrl.value;         
   }
   if (strPVUrl != "" && bIsAtPaperVision == "-1" )
   {
        document.getElementsByTagName("BODY")[0].onunload="";   
        document.forms[0].hdnAction.value="editdocument"; 
        document.forms[0].submit();
   } ////Animesh Insertion Ends MITS 19654
   else
   {
       document.forms[0].action='EditDoc.aspx';
       document.forms[0].method="post";
       document.forms[0].submit();
   }
}
function CopyDoc()
{
      var screen='CopyDocument.aspx';
   
      document.forms[0].action=screen;
      document.forms[0].method="post";
      document.forms[0].submit();          
}
function MoveDoc()
{          
      var screen='MoveDocuments.aspx';
      document.forms[0].action=screen;
      document.forms[0].method="post";
      document.forms[0].submit();          
}

function DeleteDoc()
{   
      //Animesh Inserted MITS 19654
      var bIsAtPaperVision;
      var strPVUrl;  
      if(eval("document.forms[0].hdnIsAtPaperVision")!=null)
      {
        bIsAtPaperVision=document.forms[0].hdnIsAtPaperVision.value;        
      }
      if(eval("document.forms[0].PVDocUrl")!=null)
      {
        strPVUrl=document.forms[0].PVDocUrl.value;         
      }
      if (strPVUrl != "" && bIsAtPaperVision == "-1" )
      {
         document.getElementsByTagName("BODY")[0].onunload="";   
         document.forms[0].hdnAction.value="confirmdelete"; 
         document.forms[0].submit();
      } ////Animesh Insertion Ends MITS 19654
      else
      {   
         document.forms[0].action="ConfirmDelete.aspx";
	     document.forms[0].method="post";
	     document.forms[0].submit();
	 }
}

function TransferDoc(type)
{
     var screen='TransferDocument.aspx';
     document.forms[0].action=screen;
     document.forms[0].method="post";
     document.forms[0].submit();
}
function EmailDoc(type)
{
     var screen='EmailDocuments.aspx';
     document.forms[0].action=screen;
     document.forms[0].method="post";
     document.forms[0].submit();
}
//Animesh Inserted 
function CallPaperVision()
{
    var bIsAtPaperVision;
    var strPVUrl;
    if(eval("document.forms[0].hdnIsAtPaperVision")!=null)
    {
        bIsAtPaperVision=document.forms[0].hdnIsAtPaperVision.value;        
    }
    if(eval("document.forms[0].PVDocUrl")!=null)
    {
        strPVUrl=document.forms[0].PVDocUrl.value;         
    }
    if (strPVUrl != "" && bIsAtPaperVision == "-1" )
    {
    window.open(strPVUrl,'','width=500,height=300' + ',top=' + (screen.availHeight - 300) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }    
}

function ReleasePVSession() //MITS 19654
{
      var bIsAtPaperVision;
      var strPVUrl;  
      if(eval("document.forms[0].hdnIsAtPaperVision")!=null)
      {
        bIsAtPaperVision=document.forms[0].hdnIsAtPaperVision.value;        
      }
      if(eval("document.forms[0].PVDocUrl")!=null)
      {
        strPVUrl=document.forms[0].PVDocUrl.value;         
      }
      if (strPVUrl != "" && bIsAtPaperVision == "-1" )
      {
        document.forms[0].hdnAction.value="close"; 
        document.forms[0].submit();
      }
}

function DoNotSubmit() //MITS 19654
{
    document.getElementsByTagName("BODY")[0].onunload="";
}
//Animesh Insertion ends

function EditDoNotSubmit() 
{
    DoNotSubmit();
    EditDocument();
}

</script>
</head>
<body onunload="javascript:ReleasePVSession();"> <!--MITS 19654 -->
    <form id="frmData" runat="server">
    <div>
    <asp:HiddenField ID="FolderId" runat="server" />
     <asp:HiddenField ID="hdocId" runat="server" />
      <asp:HiddenField ID="Psid" runat="server" />
      <asp:HiddenField ID="flag" runat="server" />
      <asp:HiddenField ID="hdnPageNumber" runat="server" />
      <asp:HiddenField ID="hdnSortExpression" runat="server" />
       <asp:HiddenField ID="FolderName" runat="server" />
         <asp:HiddenField ID="AttachTableName" runat="server" />
     <asp:HiddenField ID="AttachRecordId" runat="server" />
      <asp:HiddenField ID="FormName" runat="server"/>
      <asp:HiddenField ID="Regarding" runat="server" Value="Files"/>
      <!-- Animesh Inserted -->
      <asp:HiddenField ID="hdnIsAtPaperVision" runat="server"/>
      <asp:HiddenField ID="PVDocUrl" runat="server"/>
      <asp:HiddenField ID="PVLoginUrl" runat="server"/>
      <asp:HiddenField ID="hdnAction" runat="server" /> <!-- MITS 19654 -->
      <!--Animesh Insertion ends -->
      <table width="100%" class="singleborder">
 <tr>
      <td colspan="3">
          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
      </td>
    
     </tr>
     <tr>
      <td colspan="3" class="ctrlgroup">Document Properties</td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>Document Folder:</b></td>
      <td class="datatd">
           <b><u><asp:Label ID="lblFolderName" runat=server></asp:Label></u></b>     
      </td>
     
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>Title:</b></td>
      <td width="100%" class="datatd"><input type="text"  runat="server" id="Title" value="<%#ViewData.Title%>" size="50" style="border: 0px solid #104A7B;" readonly /></td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>Subject:</b></td>
      <td width="100%" class="datatd"><input type="text" runat="server" id="Subject" value="<%#ViewData.Subject%>" size="50" style="border: 0px solid #104A7B;" readonly/></td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>Type:</b></td>
      <td width="100%" class="datatd"><input type="text" runat="server" id="Type" value="<%#ViewData.Type.Desc%>" size="50" style="border: 0px solid #104A7B;" readonly/></td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>Class:</b></td>
      <td width="100%" class="datatd"><input type="text" runat="server"  id="Class" value="<%#ViewData.Class.Desc%>" size="50" style="border: 0px solid #104A7B;" readonly/></td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>Category:</b></td>
      <td width="100%" class="datatd"><input type="text" runat="server" id="Category" value="<%#ViewData.Category.Desc%>" size="50" style="border: 0px solid #104A7B;" readonly /></td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>Notes:</b></td>
      <td width="100%" class="datatd"><input type="text" runat="server" id="Notes" value="<%#ViewData.Notes%>" size="50" style="border: 0px solid #104A7B;" readonly/></td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>Keywords:</b></td>
      <td width="100%" class="datatd"><input type="text" runat="server" id="Keywords" value="<%#ViewData.Keywords%>" size="50" style="border: 0px solid #104A7B;" readonly/></td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>File Name:</b></td>
      <td width="100%" class="datatd"><input type="text" runat="server" id="FileName" value="<%#ViewData.FileName%>" size="50" style="border: 0px solid #104A7B;" readonly/></td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>Created on:</b></td>
      <td width="100%" class="datatd"><input type="text" runat="server" id="CreateDate" value="<%#ViewData.CreateDate%>" size="17" style="border: 0px solid #104A7B;" readonly/></td>
     </tr>
     <tr>
      <td colspan="3" class1="vspacer1">&nbsp;</td>
     </tr>
     <tr>
      <td colspan="3" align="center" nowrap=""><br>
          
          
        <% if (ViewData.Download == 1){%>
     <%if (ViewData.DocInternalType!="3") {%>
      &nbsp;<asp:Button ID="btnDownload" runat="server" Text="Download" CssClass="button"
              onclick="btnDownload_Click"/>
      <%} %>
      <%else if (ViewData.DocInternalType == "3") { %>
      &nbsp;<asp:HyperLink ID="lnkDownloadLink" runat="server" Text="Download" Target="_blank" NavigateUrl="<%#ViewData.FilePath %>"/>
      <%} %>
       <%} %>
       
       <!-- Animesh Inserted  -->
     <% if (ViewData.IsAtPaperVision == "-1"){%>
      &nbsp;<input type="button" onclick="javascript:CallPaperVision();" name="View" value="View" class="button">
     <%} %>

       <!-- Animesh Insertion Ends -->
      
         <% if (ViewData.Email == 1){%>
     <%if (ViewData.DocInternalType!="3") {%>
      
      &nbsp;<input type="button" onclick="javascript:EmailDoc('c');" name="E-Mail" value="Email" class="button">
      <%} %>
       <%} %>
       <% if (ViewData.Edit == 1){%>
     <%if (ViewData.DocInternalType!="3") {%>
      &nbsp;<input type="button" onclick="javascript:EditDoNotSubmit();" name="Edit" value="Edit" class="button">
      <%} %>
       <%} %>
      
        <%if (ViewData.ScreenFlag =="Files") {%>
        <% if (ViewData.Move == 1){%>
     <%if (ViewData.DocInternalType!="3") {%>
     
      &nbsp;<input type="button" name="Move" value="  Move  " class="button" onclick="javascript:MoveDoc();"/>
        <%} %>
       <%} %>
        <%} %>
         <% if (ViewData.Delete  == 1){%>
     <%if (ViewData.DocInternalType!="3") {%>
      &nbsp;<input type="button" name="Delete" value="  Delete  " class="button" onclick="javascript:DeleteDoc();"/>
        <%} %>
       <%} %>
           <% if (ViewData.Transfer  == 1){%>
     <%if (ViewData.DocInternalType!="3") {%>
      <%if (ViewData.ScreenFlag =="Files") {%>
      &nbsp;<input type="button" name="Transfer" value="  Transfer  " class="button" onclick="javascript:TransferDoc('t');"/>
          <%} %>
       <%} %>
       <%} %>
          <% if (ViewData.Copy  == 1){%>
     <%if (ViewData.DocInternalType!="3") {%>
       <%if (ViewData.ScreenFlag =="Files") {%>
      &nbsp;<input type="button" name="Copy" value="  Copy  " class="button" onclick="javascript:CopyDoc();" />&nbsp;
         <%} %>
       <%} %>
     <%} %>
&nbsp;
<asp:HiddenField ID="NonMCMFormName" runat="server" />
      <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="button"
         OnClientClick="javascript:DoNotSubmit();" onclick="btnBack_Click"/> <!-- MITS 19654 -->
      </td> 
     </tr>
    </div>
    </form>
</body>
</html>
