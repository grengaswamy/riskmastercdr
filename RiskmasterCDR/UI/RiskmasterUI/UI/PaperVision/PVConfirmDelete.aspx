﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PVConfirmDelete.aspx.cs" Inherits="Riskmaster.UI.PaperVision.ConfirmDelete" EnableViewStateMac="false"%>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" Tagname="ErrorControl" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="~/Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link href="~/Content/system.css" rel="stylesheet" type="text/css" />
</head>
<body>
  
    <form id="form1" runat="server">
    <div>
    <table class="singleborder" align="center">
      <tr>
      <td colspan="2">
          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
             </td>
     </tr>
    <tr>
     <td class="ctrlgroup">Confirm File/Folder Delete</td>
    </tr>
    <tr>
     <td nowrap="" class="datatd"><b>Are you sure you want to delete selected items?<br><br>Data will be permanently deleted.</b><br></td>
    </tr>
    <tr>
     <td align="center" nowrap="">
        
         <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
             Text="    Delete   " CssClass="button" />
         <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
             Text="   Cancel   " CssClass="button" />
        
         <asp:HiddenField ID="FolderId" runat="server" />
         <asp:HiddenField ID="hfolderId" runat="server" />
          <asp:HiddenField ID="hdocId" runat="server" />
         <asp:HiddenField ID="Psid" runat="server" />
         <asp:HiddenField ID="flag" runat="server" />
         <asp:HiddenField ID="hdnPageNumber" runat="server" />
         <asp:HiddenField ID="hdnSortExpression" runat="server" />
           <asp:HiddenField ID="AttachTableName" runat="server" />
     <asp:HiddenField ID="AttachRecordId" runat="server" />
      <asp:HiddenField ID="FormName" runat="server"/>
      <asp:HiddenField ID="Regarding" runat="server"/>
        <asp:HiddenField ID="NonMCMFormName" runat="server" />
        </td>
        </tr>
   </table>
   
  
    </div>
    </form>
</body>
</html>
