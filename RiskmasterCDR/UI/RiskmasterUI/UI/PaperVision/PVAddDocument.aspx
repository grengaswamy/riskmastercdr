<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PVAddDocument.aspx.cs" Inherits="Riskmaster.UI.PaperVision.AddDocument"
    EnableViewStateMac="false" %>

<%@ Register Assembly="CuteWebUI.AjaxUploader" Namespace="CuteWebUI" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Riskmaster</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />

    <script type="text/javascript" language="JavaScript">
		function SaveForm()
		{
		    if (ValidateForm())
		    {
			        document.forms[0].action='';
			        document.forms[0].method="post";
			        document.forms[0].submit();
			        //This is all we need to do to Show Please Wait Box.......
			       
			        pleaseWait.Show();
			      
			        return true;
	        }	
	        
	        return false;        
		}
		function CancelForm()
		{
          				    
		}
		function ValidateForm()
		{
			if(document.forms[0].Title.value=="")
			{
				alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
				return false;
            }
            var arr = document.getElementsByTagName("input");
            var isAttached = false;
            for (i = 0; i < document.getElementsByTagName("input").length; i++) {
                if (arr[i].type == "checkbox") {
                    if (arr[i].checked == true) {
                        return true;
                    }                               
                }                
            }
            alert("Please select a file to attach");
            return false;          			
		}
    </script>

</head>
<body>
    <form id="frmData" runat="server">
    <div id="maindiv" style="height: 100%; width: 99%; overflow: auto">
        <p align="center">
            <table class="singleborder">
                <tr>
                    <td colspan="2">
                        <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="ctrlgroup">
                        Add New Document
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="errortext">
                    </td>
                </tr>
                <tr>
                    <td class="required" nowrap="">
                        Document Folder:
                    </td>
                    <td class="required">
                    </td>
                </tr>
                <tr>
                    <td class="datatd" nowrap="">
                        <b><u>Title:</u></b>
                    </td>
                    <td class="datatd">
                        <input type="text" id="Title" runat="server" size="32" maxlength="32" style="border: 1px solid #104A7B;">
                    </td>
                </tr>
                <tr>
                    <td class="datatd" nowrap="">
                        <b>Subject:</b>
                    </td>
                    <td class="datatd">
                        <input type="text" id="Subject" runat="server" size="50" maxlength="50" style="border: 1px solid #104A7B;">
                    </td>
                </tr>
                <tr>
                    <td class="datatd" nowrap="">
                        <b>Type:</b>
                    </td>
                    <td class="datatd">
                        <input type="text" size="30" runat="server" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);"
                            name="documenttypecode" id="documenttypecode" cancelledvalue=""></input>
                        <input type="button" name="documenttypecodebtn" class="CodeLookupControl" id="documenttypecodebtn"
                            onclick="return selectCode('DOCUMENT_TYPE','documenttypecode')" />
                        <input type="hidden" name="documenttypecode_cid" runat="server" id="documenttypecode_cid" />
                    </td>
                </tr>
                <tr>
                    <td class="datatd" nowrap="">
                        <b>Class:</b>
                    </td>
                    <td class="datatd">
                        <input type="text" size="30" runat="server" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);"
                            id="documentclasscode" cancelledvalue=""></input>
                        <input type="button" name="documentclasscodebtn" class="CodeLookupControl" id="documentclasscodebtn"
                            onclick="return selectCode('DOCUMENT_CLASS','documentclasscode')" />
                        <input type="hidden" name="documentclasscode_cid" runat="server" id="documentclasscode_cid" />
                    </td>
                </tr>
                <tr>
                    <td class="datatd" nowrap="">
                        <b>Category:</b>
                    </td>
                    <td class="datatd">
                        <input type="text" size="30" runat="server" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);"
                            name="documentcategorycode" id="documentcategorycode" cancelledvalue=""></input>
                        <input type="button" name="documentcategorycodebtn" class="CodeLookupControl" id="documentcategorycodebtn"
                            onclick="return selectCode('DOCUMENT_CATEGORY','documentcategorycode')" />
                        <input type="hidden" runat="server" name="documentcategorycode_cid" id="documentcategorycode_cid" />
                    </td>
                </tr>
                <td class="datatd" nowrap="">
                    <b>Keywords:</b>
                </td>
                <td class="datatd">
                    <input type="text" runat="server" id="Keywords" value="" size="50" maxlength="200"
                        style="border: 1px solid #104A7B;">
                </td>
                </tr>
                <tr>
                    <td class="datatd" nowrap="">
                        <b>Notes:</b>
                    </td>
                    <td class="datatd">
                        <textarea cols="38" runat="server" wrap="soft" id="Notes" rows="6" size="" style="border: 1px solid #104A7B;"></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <cc1:UploadAttachments ID="UploadDocumentAttachments" runat="server" InsertText="Click here to select files for attachment">
                            <InsertButtonStyle />                            
                        </cc1:UploadAttachments>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="Button1" runat="server" Text="Save" CssClass="button" OnClick="SaveData_Click"
                            UseSubmitBehavior="true" OnClientClick="return SaveForm()" />
                        &nbsp;
                        <asp:Button ID="Button2" runat="server" Text="Cancel" OnClick="Cancel_Click" CssClass="button" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="Psid" runat="server" />
                        <asp:HiddenField ID="FolderId" runat="server" />
                        <asp:HiddenField ID="Regarding" runat="server" Value="Files" />
                        <asp:HiddenField ID="AttachTableName" runat="server" />
                        <asp:HiddenField ID="AttachRecordId" runat="server" />
                        <asp:HiddenField ID="FormName" runat="server" />
                        <asp:HiddenField ID="flag" runat="server" />
                        <asp:HiddenField ID="hdnPageNumber" runat="server" />
                        <asp:HiddenField ID="hdnSortExpression" runat="server" />
                        <asp:HiddenField ID="NonMCMFormName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </p>
        <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading..." />
    </div>
    </form>
</body>
</html>
