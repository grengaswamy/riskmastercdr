﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Riskmaster.UI.DocumentService;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.Xml.Linq;
using System.IO;
using System.Net;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;

namespace Riskmaster.UI.PaperVision
{
    public partial class EditDoc : System.Web.UI.Page
    {
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            Psid.Value = AppHelper.GetValue("Psid");
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
            NonMCMFormName.Value = AppHelper.GetValue("NonMCMFormName");
            hdnIsAtPaperVision.Value = AppHelper.GetValue("IsAtPaperVision"); //Bharani - MITS : 34451 - Change 1/3
            
        }
        private void BindDataValues(DocumentType ViewData)
        {
            lblFolderName.Text = AppHelper.GetFormValue("FolderName");
            Title.Value = ViewData.Title;
            Subject.Value = ViewData.Subject;
            CreateDate.Value = ViewData.CreateDate;
            documenttypecode.Value = ViewData.Type.Desc;
            documenttypecode_cid.Value = ViewData.Type.Id.ToString();
            documentclasscode.Value = ViewData.Class.Desc;
            documentclasscode_cid.Value = ViewData.Class.Id.ToString();
            documentcategorycode.Value = ViewData.Category.Desc;
            documentcategorycode_cid.Value = ViewData.Category.Id.ToString();
            Keywords.Value = ViewData.Keywords;
            Notes.Value = ViewData.Notes;
            FileName.Value = ViewData.FileName;
            hdnFileName.Value = ViewData.FileName; //Bharani - MITS : 33886 - Change 1
              

        }
        //Bharani - MITS : 33886 - Change 2 - Start

        private void GetDocument()
        {
            DocumentType ViewData = null;
            DocumentBusinessHelper dc = new DocumentBusinessHelper();
            try
            {
                if (hdocId.Value != "")
                {

                    ViewData = dc.Download(Conversion.ConvertStrToInteger(hdocId.Value));
                    hdnContent.Value = Convert.ToBase64String(ViewData.FileContents);
                    hdnTempFileName.Value = hdnFileName.Value;

                }

            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }

        //Bharani - MITS : 33886 - Change 2 - End

        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("EditDoc.aspx"), "EditDocValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "EditDocValidationScripts", sValidationResources, true);
            //Animesh Inserted 
            string strPVLoginUrl = string.Empty;
            string strPVDocUrl = string.Empty;  
            //Animesh Insertion Ends
            if (!Page.IsPostBack)
            {
                SetInitialValues();
                DocumentBusinessHelper dc = new DocumentBusinessHelper();
                try
                {
                    AttachTable.Value = AppHelper.GetValue("AttachTable");
                    FormName.Value = AppHelper.GetValue("FormName");
                    AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
                    string shdocId = AppHelper.GetFormValue("hdocId");
                    FolderId.Value = AppHelper.GetFormValue("FolderId").ToString();
                    hdocId.Value = shdocId;
                    Psid.Value = AppHelper.GetFormValue("Psid").ToString();
                    flag.Value = AppHelper.GetFormValue("flag").ToString();
                    hdnPageNumber.Value = AppHelper.GetFormValue("hdnPageNumber").ToString();
                    hdnSortExpression.Value = AppHelper.GetFormValue("hdnSortExpression").ToString();
                    FolderName.Value = AppHelper.GetFormValue("FolderName");
                    DocumentType ViewData = dc.Edit(Conversion.ConvertStrToInteger(hdocId.Value), flag.Value, Psid.Value, FormName.Value,0);
                    BindDataValues(ViewData);
                    //Animesh Inserted for PaperVision
                    if (Conversion.ConvertObjToStr(ViewData.PaperVisionUrl).Trim() != "")
                    {
                        string[] strPVUrlArr = ViewData.PaperVisionUrl.Split(new char[] { '@' });
                        //PVDocUrl.Value = ViewData.PaperVisionUrl;
                        strPVDocUrl = strPVUrlArr[0].Trim();
                        if (strPVUrlArr.Length > 1)
                            strPVLoginUrl = strPVUrlArr[1].Trim();
                        //Bharani - MITS : 34451 - Change 2/3 - Start
                        PVDocUrl.Value = strPVDocUrl;
                        hdnIsAtPaperVision.Value = ViewData.IsAtPaperVision;
                        //Bharani - MITS : 34451 - Change 2/3 - End
                    }
                    //Bharani - MITS : 34451 - Change 3/3 - Start
                    //if (Conversion.ConvertObjToStr(strPVLoginUrl).Trim() != "" && Conversion.ConvertObjToStr(strPVDocUrl).Trim() != "")
                    //{
                    //  LogOff(strPVDocUrl.Trim(), strPVLoginUrl.Trim());
                    //}
                    //Animesh Insertion Ends                    
                    //Bharani - MITS : 34451 - Change 3/3 - End
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
        }

        //Bharani - MITS : 33886 - Change 3 - Start

        public bool IfOfficeDocumentOrTextType()
        {
            string sExtension = "";
            bool bIsOfficeDocumentOrText = false;
            if (FileName.Value != "")
            {
                string[] arrParseFileName = FileName.Value.Split(".".ToCharArray()[0]);
                if (arrParseFileName.Length > 1)
                {
                    int iExt = (arrParseFileName.Length - 1);
                    sExtension = arrParseFileName[iExt];
                }
            }
            if (sExtension.ToLower().Equals("doc") || sExtension.ToLower().Equals("docx"))
            {
                bIsOfficeDocumentOrText = true;
            }
            if (sExtension.ToLower().Equals("xls") || sExtension.ToLower().Equals("xlsx") || sExtension.ToLower().Equals("csv"))
            {
                bIsOfficeDocumentOrText = true;
            }
            if (sExtension.ToLower().Equals("ppt") || sExtension.ToLower().Equals("pptx"))
            {
                bIsOfficeDocumentOrText = true;
            }
            if (sExtension.ToLower().Equals("txt"))
            {
                bIsOfficeDocumentOrText = true;
            }
            return bIsOfficeDocumentOrText;
        }

        //Bharani - MITS : 33886 - Change 3 - End

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DocumentBusinessHelper dc = new DocumentBusinessHelper();
            bool isError = false;
            try
            {
                dc.EditDocument(Conversion.ConvertStrToInteger(hdocId.Value));
                //Bharani - MITS : 33886 - Change 4 - Start
                DocumentType dtype = new DocumentType();
                if (hdnRTFContent.Value.Trim() == "")
                {
                    dtype.FileContents = Convert.FromBase64String(hdnContent.Value);
                }
                else
                {
                    dtype.FileContents = Convert.FromBase64String(hdnRTFContent.Value);
                }
                dtype.DocumentId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("hdocId"));
                dtype.FileName = hdnTempFileName.Value;
                dtype.PsId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("Psid"));
                dtype.ScreenFlag = AppHelper.GetFormValue("flag");
                dc.UpdateDocumentFile(dtype);
                //Bharani - MITS : 33886 - Change 4 - End
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                isError = true;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                isError = true;

            }
            if (!isError)
                Server.Transfer("/RiskmasterUI/UI/PaperVision/PVDisplayDocument.aspx?id=" + hdocId.Value, true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("/RiskmasterUI/UI/PaperVision/PVDisplayDocument.aspx?id=" + hdocId.Value, true);
        }

        //Animesh Inserted to release the session as in case of Edit Client does not want ediitng on Papervision.
        protected bool LogOff(string strDocUrl, string strPVUrl)
        {
            WebRequest objReq;
            StreamWriter objWriter;
            string sURLParams;
            string strSessionID;
            string strEntityID;
            string strProjID;
            try
            {
                strSessionID = strDocUrl.Substring(strDocUrl.IndexOf("SessionID=") + 10, strDocUrl.IndexOf("&EntID=") - strDocUrl.IndexOf("SessionID=") - 10);
                strEntityID = strDocUrl.Substring(strDocUrl.IndexOf("EntID=") + 6, strDocUrl.IndexOf("&ProjID=") - strDocUrl.IndexOf("EntID=") - 6);
                strProjID = strDocUrl.Substring(strDocUrl.IndexOf("ProjID=") + 7, strDocUrl.IndexOf("&DocID=") - strDocUrl.IndexOf("ProjID=") - 7);
                sURLParams = "<?xml version=\"1.0\" standalone=\"yes\" ?>\r\n";
                sURLParams += "<MicroNetWrapper>";
                sURLParams += "	<FUNCTION>";
                sURLParams += "		<NAME>KillSession</NAME>";
                sURLParams += "		<PARAMETERS>";
                sURLParams += "			<ENTITYID>" + strEntityID.Trim() + "</ENTITYID>";
                sURLParams += "			<SESSIONID>" + strSessionID.Trim() + "</SESSIONID>";
                sURLParams += "         <PROJID>" + strProjID.Trim() + "</PROJID>";
                sURLParams += "		</PARAMETERS>";
                sURLParams += "	</FUNCTION>";
                sURLParams += "</MicroNetWrapper>";

                sURLParams = sURLParams.Replace("\t", "");
                objReq = WebRequest.Create(strPVUrl);
                objReq.Timeout = 900000; // fifteen minutes
                objReq.Credentials = CredentialCache.DefaultCredentials;
                objReq.ContentType = "application/x-www-form-urlencoded";
                objReq.ContentLength = sURLParams.Length;
                System.Net.IWebProxy iwp = System.Net.WebRequest.GetSystemWebProxy();
                objReq.Proxy = iwp;
                objReq.Method = "POST";
                objWriter = new StreamWriter(objReq.GetRequestStream());
            }
            catch
            {
                return false;
            }
            objWriter.Write(sURLParams);
            objWriter.Flush();
            objWriter.Close();
            objWriter = null;
            try
            {
                WebResponse objResp = objReq.GetResponse();
                StreamReader objSRead = new StreamReader(objResp.GetResponseStream());
                string cContent = objSRead.ReadToEnd();
                objSRead.Close();
                objSRead.Dispose();
                objSRead = null;
                objResp = null;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (objWriter != null)
                {
                    objWriter.Close();
                    objWriter.Dispose();
                    objWriter = null;
                }
                objReq = null;

            }
        }
        //Animesh Insertion Ends
      
    }
}
