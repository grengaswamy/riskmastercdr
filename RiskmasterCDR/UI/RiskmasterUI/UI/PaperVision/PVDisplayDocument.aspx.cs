﻿using System;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.IO;
using System.Net;
using Riskmaster.Models;

namespace Riskmaster.UI.PaperVision
{
    public partial class DisplayDocument : System.Web.UI.Page
    {
        public DocumentType ViewData = null;
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            Psid.Value = AppHelper.GetValue("Psid");
            flag.Value = AppHelper.GetValue("flag");
            //Animesh Inserted 
            hdnIsAtPaperVision.Value = AppHelper.GetValue("IsAtPaperVision");
            //PVDocUrl.Value = AppHelper.GetValue("PaperVisionUrl");    
            //Animesh Insertion ends
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetInitialValues();
            ViewData = new DocumentType();
            if (!Page.IsPostBack)
            {
                try
                {
                    DocumentBusinessHelper dc = new DocumentBusinessHelper();

                    if (AppHelper.GetQueryStringValue("id") != "")
                    {
                        ViewData = dc.Edit(Conversion.ConvertStrToInteger(AppHelper.GetQueryStringValue("id")), AppHelper.GetFormValue("flag").ToString(), AppHelper.GetFormValue("Psid").ToString(), FormName.Value,0);
                        string shdocId = Request.Form["hdocId"].ToString();
                        FolderId.Value = Request.Form["FolderId"].ToString();
                        hdocId.Value = AppHelper.GetQueryStringValue("id");
                        Psid.Value = Request.Form["Psid"].ToString();
                        flag.Value = Request.Form["flag"].ToString();
                        hdnPageNumber.Value = Request.Form["hdnPageNumber"].ToString();
                        hdnSortExpression.Value = Request.Form["hdnSortExpression"].ToString();
                        FolderName.Value = Request.Form["FolderName"].ToString();
                        lblFolderName.Text = AppHelper.GetFormValue("FolderName");
                        NonMCMFormName.Value = AppHelper.GetFormValue("NonMCMFormName");
                        //Animesh Inserted 
                        hdnIsAtPaperVision.Value = ViewData.IsAtPaperVision;
                        if (Conversion.ConvertObjToStr(ViewData.PaperVisionUrl).Trim() != "")
                        {
                            string[] strPVUrlArr = ViewData.PaperVisionUrl.Split(new char[] { '@' });
                            //PVDocUrl.Value = ViewData.PaperVisionUrl;
                            PVDocUrl.Value = strPVUrlArr[0].Trim();
                            if (strPVUrlArr.Length > 1)
                                PVLoginUrl.Value = strPVUrlArr[1].Trim();
                        }
                        //Animesh Insertion ends  
                        DataBind();
                    }
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
            else
            {
                //Animesh inserted  //MITS 19654
                try
                {
                    if ((hdnAction.Value.ToLower() == "close") || (hdnAction.Value.ToLower() == "confirmdelete") || hdnAction.Value.ToLower() == "editdocument")
                    {
                        if (Conversion.ConvertObjToStr(PVLoginUrl.Value).Trim() != "" && Conversion.ConvertObjToStr(PVDocUrl.Value).Trim() != "")
                        {
                            LogOff(PVDocUrl.Value.Trim(), PVLoginUrl.Value.Trim());
                        }
                    }
                }
                catch (Exception ex)
                {
                    //do nothing as we are just making a call to Papervision webservice to logout from the current session. 
                }
                if (hdnAction.Value == "confirmdelete")
                {
                    Server.Transfer("/RiskmasterUI/UI/PaperVision/PVConfirmDelete.aspx", true);
                }
                else if (hdnAction.Value == "editdocument")
                {
                    Server.Transfer("/RiskmasterUI/UI/PaperVision/PVEditDoc.aspx", true);
                }
                //Animesh Insertion Ends //MITS 19654
            }
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            //Animesh Inserted 
            if (Conversion.ConvertObjToStr(PVLoginUrl.Value).Trim() != "" && Conversion.ConvertObjToStr(PVDocUrl.Value).Trim()!="")
            {
                LogOff(PVDocUrl.Value.Trim(), PVLoginUrl.Value.Trim());       
            }
            //Animesh Insertion Ends 
            Server.Transfer("/RiskmasterUI/UI/PaperVision/PVDocumentList.aspx", true);
        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {

            DocumentBusinessHelper dc = new DocumentBusinessHelper();
            try
            {
                if (AppHelper.GetFormValue("hdocId") != "")
                {

                    ViewData = dc.Download(Conversion.ConvertStrToInteger(AppHelper.GetFormValue("hdocId")));


                    using (MemoryStream ms = new MemoryStream(ViewData.FileContents))
                    {
                        long dataLengthToRead = ms.Length;
                        int blockSize = dataLengthToRead >= 5000 ? 5000 : (int)dataLengthToRead;
                        byte[] buffer = new byte[dataLengthToRead];

                        Response.Clear();

                        // Clear the content of the response
                        Response.ClearContent();
                        Response.ClearHeaders();

                        // Buffer response so that page is sent
                        // after processing is complete.
                        Response.BufferOutput = true;

                        // Add the file name and attachment,
                        // which will force the open/cance/save dialog to show, to the header
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode(ViewData.FileName));

                        // bypass the Open/Save/Cancel dialog
                        //Response.AddHeader("Content-Disposition", "inline; fileid=" + doc.FileName);

                        // Add the file size into the response header
                        Response.AddHeader("Content-Length", ms.Length.ToString());

                        // Set the ContentType
                        Response.ContentType = "application/octet-stream";

                        // Write the document into the response
                        while (dataLengthToRead > 0 && Response.IsClientConnected)
                        {
                            Int32 lengthRead = ms.Read(buffer, 0, blockSize);
                            Response.OutputStream.Write(buffer, 0, lengthRead);
                            //Response.Flush(); // do not flush since BufferOutput = true
                            dataLengthToRead = dataLengthToRead - lengthRead;
                        }

                        Response.Flush();
                        Response.Close();
                    }

                    // End the response

                    Response.End();

                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }

        protected bool LogOff(string strDocUrl, string strPVUrl)
        {
            WebRequest objReq;
            StreamWriter objWriter;
            string sURLParams;
            string strSessionID;
            string strEntityID;
            string strProjID;
            try
            {
                strSessionID = strDocUrl.Substring(strDocUrl.IndexOf("SessionID=") + 10, strDocUrl.IndexOf("&EntID=") - strDocUrl.IndexOf("SessionID=") - 10);
                strEntityID = strDocUrl.Substring(strDocUrl.IndexOf("EntID=") + 6, strDocUrl.IndexOf("&ProjID=") - strDocUrl.IndexOf("EntID=") - 6);
                strProjID = strDocUrl.Substring(strDocUrl.IndexOf("ProjID=") + 7, strDocUrl.IndexOf("&DocID=") - strDocUrl.IndexOf("ProjID=") - 7);    
                sURLParams = "<?xml version=\"1.0\" standalone=\"yes\" ?>\r\n";
                sURLParams += "<MicroNetWrapper>";
                sURLParams += "	<FUNCTION>";
                sURLParams += "		<NAME>KillSession</NAME>";
                sURLParams += "		<PARAMETERS>";
                sURLParams += "			<ENTITYID>" + strEntityID.Trim()  + "</ENTITYID>";
                sURLParams += "			<SESSIONID>" + strSessionID.Trim()   + "</SESSIONID>";
                sURLParams += "         <PROJID>" + strProjID.Trim()  + "</PROJID>";
                sURLParams += "		</PARAMETERS>";
                sURLParams += "	</FUNCTION>";
                sURLParams += "</MicroNetWrapper>";

                sURLParams = sURLParams.Replace("\t", "");
                objReq = WebRequest.Create(strPVUrl);
                objReq.Timeout = 900000; // fifteen minutes
                objReq.Credentials = CredentialCache.DefaultCredentials;
                objReq.ContentType = "application/x-www-form-urlencoded";
                objReq.ContentLength = sURLParams.Length;
                System.Net.IWebProxy iwp = System.Net.WebRequest.GetSystemWebProxy();
                objReq.Proxy = iwp;
                objReq.Method = "POST";
                objWriter = new StreamWriter(objReq.GetRequestStream());
            }
            catch 
            {
                return false;
            }
            objWriter.Write(sURLParams);
            objWriter.Flush();
            objWriter.Close();
            objWriter = null;
            try
            {
                WebResponse objResp = objReq.GetResponse();
                StreamReader objSRead = new StreamReader(objResp.GetResponseStream());
                string cContent = objSRead.ReadToEnd();
                objSRead.Close();
                objSRead.Dispose();
                objSRead = null;
                objResp = null;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (objWriter != null)
                {
                    objWriter.Close();
                    objWriter.Dispose();
                    objWriter = null;
                }
                objReq = null;
                
            }
        }
    }
}
