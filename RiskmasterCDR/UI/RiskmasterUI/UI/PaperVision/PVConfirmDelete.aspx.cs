﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Riskmaster.UI.DocumentService;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.Xml.Linq;
using System.IO;
using Riskmaster.Models;
namespace Riskmaster.UI.PaperVision
{
    public partial class ConfirmDelete : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    SetInitialValues();
                    string sfolderId = AppHelper.GetFormValue("hfolderId").ToString();
                    string shdocId = AppHelper.GetFormValue("hdocId").ToString();
                    FolderId.Value = AppHelper.GetFormValue("FolderId").ToString();
                    hfolderId.Value = sfolderId;
                    hdocId.Value = shdocId;
                    Psid.Value = AppHelper.GetFormValue("Psid").ToString();
                    flag.Value = AppHelper.GetFormValue("flag").ToString();
                    hdnPageNumber.Value = AppHelper.GetFormValue("hdnPageNumber").ToString();
                    hdnSortExpression.Value = AppHelper.GetFormValue("hdnSortExpression").ToString();
                    NonMCMFormName.Value = AppHelper.GetValue("NonMCMFormName");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            Psid.Value = AppHelper.GetValue("Psid");
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
        }
        

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("/RiskmasterUI/UI/PaperVision/PVDocumentList.aspx", true);
        }

      
        protected void btnDelete_Click(object sender, EventArgs e)
        {
              DocumentBusinessHelper dc = new DocumentBusinessHelper();
              bool isError = false;
              try
              {
                  dc.Delete(hfolderId.Value, hdocId.Value, Psid.Value, flag.Value, "");
                 
              }
              catch (FaultException<RMException> ee)
              {
                  ErrorHelper.logErrors(ee);
                  ErrorControl1.errorDom = ee.Detail.Errors;
                  isError = true;

              }
              catch (Exception ee)
              {
                  ErrorHelper.logErrors(ee);
                  BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                  err.Add(ee, BusinessAdaptorErrorType.SystemError);
                  ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                  isError = true;
              }
              if (!isError)
                  Server.Transfer("/RiskmasterUI/UI/PaperVision/PVDocumentList.aspx", true);
        }

      

       
    }
}
