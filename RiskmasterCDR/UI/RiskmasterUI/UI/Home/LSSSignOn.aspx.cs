﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Text;


namespace Riskmaster.UI.Home
{
    public partial class LSSSignOn : System.Web.UI.Page
    {
        XElement m_objMessageElement = null;
        XElement objTempElement = null;
        XmlDocument objXml = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                m_objMessageElement = GetMessageTemplate();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                    objTempElement.Value = AppHelper.GetSessionId();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["paramLSSInvoiceID"]))
                {
                    this.invoice.Value = Request.QueryString["paramLSSInvoiceID"];
                }

                //Call WCF wrapper for cws
                string sReturn = AppHelper.CallCWSService(m_objMessageElement.ToString());

                XmlDocument oXmlOut = new XmlDocument();
                oXmlOut.LoadXml(sReturn);
                XmlNode oInstanceNode = oXmlOut.SelectSingleNode("/ResultMessage/Document");

                objXml.LoadXml(oInstanceNode.InnerXml);

                this.LSSUrl.Value = objXml.SelectSingleNode("//LSSUrl").InnerText;
                this.loginName.Value = objXml.SelectSingleNode("//loginName").InnerText;
                this.Pass.Value = objXml.SelectSingleNode("//Pass").InnerText;
                this.wsUser.Value = objXml.SelectSingleNode("//wsUser").InnerText;
                this.wsPass.Value = objXml.SelectSingleNode("//wsPass").InnerText;
                this.authMode.Value = objXml.SelectSingleNode("//authMode").InnerText;// MITS 26834 ; Debabrata Biswas
                this.EnableViewState = false;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>SingleSignonAdaptor.SingleSignonXML</Function></Call><Document>");
            sXml = sXml.Append("<form>");          
            sXml = sXml.Append("<LSSUrl />");
            sXml = sXml.Append("<loginName />");
            sXml = sXml.Append("<Pass />");
            sXml = sXml.Append("<wsUser />");
            sXml = sXml.Append("<wsPass />");
            sXml = sXml.Append("<authMode />");// MITS 26834 ; Debabrata Biswas
            sXml = sXml.Append("</form></Document></Message>");

            XElement oTemplateEOB = XElement.Parse(sXml.ToString());

            return oTemplateEOB;
        }
    }
}
