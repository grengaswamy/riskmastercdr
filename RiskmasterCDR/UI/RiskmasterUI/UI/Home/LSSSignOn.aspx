﻿<%@ Page Language="C#" AutoEventWireup="true" codebehind="LSSSignOn.aspx.cs" Inherits="Riskmaster.UI.Home.LSSSignOn" EnableViewState ="false" EnableViewStateMac="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LSS Login</title>
    
    <script language="javascript" type="text/javascript">
    
    function SingleSignon()
    {
       try
       {
		    //Submit the form to itself
		    var LSSUrl=document.getElementById('LSSUrl').value;
		    document.forms[0].action=LSSUrl;
		    document.forms[0].method="post";
		    document.forms[0].submit();
       }
       catch (e){}
    }

    </script>
</head>
<body onload="SingleSignon();">
    <form id="frmData" enableviewstate="false">       

        <input ID="LSSUrl" runat="server" enableviewstate="false" type="hidden"  />
        <input ID="loginName" runat="server" enableviewstate="false" type="hidden" />
        <input ID="Pass" runat="server" enableviewstate="false" type="hidden" />
        <input ID="wsUser" runat="server" enableviewstate="false" type="hidden" />
        <input ID="wsPass" runat="server" enableviewstate="false" type="hidden" />
        <input ID="invoice" runat="server" enableviewstate="false" type="hidden" />
        <input ID="authMode" runat="server" enableviewstate="false" type="hidden" /> <%--MITS 26834 ; Debabrata Biswas--%>
     </form>
</body>
</html>
