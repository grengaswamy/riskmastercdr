﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;     // MITS 26834, Debabrata Biswas. Included to Decrypt Common encryption Routine shared between #rd Party Portal and RMX
using Riskmaster.Security.Encryption;
using Riskmaster.Models;   // MITS 26834, Debabrata Biswas. Included to Encrypt values sent from portal.

namespace Riskmaster.UI.UI.Home
{
    public partial class CustomLogin : System.Web.UI.Page
    {
        //private RMAuthentication.AuthenticationServiceClient authService = new RMAuthentication.AuthenticationServiceClient();
        private delegate bool GetUserAuthentication(ref string strUserName);
        private static string[] sDataSeparator = { "|^^|" };
        private struct AdditionalPortalArguments
        {
            public string strUserName;
            public string strEncOn ;
            public string strDSNName ;
            public string sPVname ;
            public string sFDMName ;
            public string sClaimID ;
            public string sClaimNum;
            const string ENCRYPTION_KEYS_ON = "ON,ENCRYPTIONON";

            public bool IsEncryptionOn()
            {
                //if ((strEncOn == "ENCRYPTIONON") &&   !(string.IsNullOrEmpty(strEncOn)   || strEncOn == ""   || strEncOn == "ENCRYPTIONOFF"))
                if(ENCRYPTION_KEYS_ON.Contains(strEncOn))
                    return true; 
                else
                    return false; 
            }
        }
        private AdditionalPortalArguments structAdditionPortalParam;
        bool bMCICCookieKeysFound = false; //Added by Amitosh for mits 26834:.Flag to check for MCIC
        protected void Page_Load(object sender, EventArgs e)
        {
            const string DEFAULT_REDIRECT_URL = "Status.aspx";
            string strUserName = string.Empty;
            AdditionalPortalArguments structAdditionPortalParam;

            LoadAdditionalPortalParameters();
            if (IsUserAuthenticated(ref strUserName))
            {
                AppHelper.CreateFormsAuthenticationTicket(strUserName, false);
                //MITS: 26834
                //Debabrata Biswas
                //Custom/IC change for MCIC to save Additional Portal Parameters.
                if(bMCICCookieKeysFound)
                doSavePortalValues();
                Response.Redirect(DEFAULT_REDIRECT_URL);
            }//if
            else
            {
                Response.Write("The user credentials specified were not able to be authenticated.");
            }//else
            
        }//event: Page_Load()

        /// <summary>
        /// Determines if a user has passed authentication
        /// through various available authentication mechanisms
        /// </summary>
        /// <param name="strUserName">reference handle to the string containing the user name</param>
        /// <returns>boolean indicating whether or not the specified credential exists</returns>
        private bool IsUserAuthenticated(ref string strUserName)
        {
            bool blnIsAuthenticated = false, blnIsVerified = false;

            //Use a delegate to define mappings to methods with common signatures
            GetUserAuthentication[] delServerVarAuth = 
            
            {
                IsServerVariable, 
                IsRequestParameter
            };

            //Loop through each of the delegate operations
            foreach (var delOperation in delServerVarAuth)
            {
                //Check each operation to verify authentication
                blnIsAuthenticated = delOperation(ref strUserName);

                blnIsVerified = VerifyCredentials(blnIsAuthenticated, strUserName);

                //Determine if the user has been successfully authenticated
                if (blnIsVerified)
                {
                    return blnIsVerified;
                }//if
            }//foreach

            //return the final value if the user did not pass either of the existing credential checks
            return blnIsVerified;
        }//method: IsUserAuthenticated


        /// <summary>
        /// Verifies that the credentials exists and are considered valid
        /// </summary>
        /// <param name="blnIsAuthenticated">boolean indicating whether or not the user was 
        /// successfully authenticated</param>
        /// <param name="strUserName">containing the user name</param>
        /// <returns>boolean indicating whether or not the specified credential exists</returns>
        private bool VerifyCredentials(bool blnIsAuthenticated, string strUserName)
        {
            if (blnIsAuthenticated && !string.IsNullOrEmpty(strUserName))
            {
                return true;
            }//if
            else
            {
                return false;
            }//else
        }//method: VerifyCredentials();

        #region Delegate Methods
        /// <summary>
        /// Determines if the username has been passed in through
        /// the ServerVariables collection
        /// </summary>
        /// <param name="strUserName">reference handle to the string containing the user name</param>
        /// <returns>boolean indicating whether or not the specified credential exists</returns>
        private bool IsServerVariable(ref string strUserName)
        {
            bool blnIsUserCred = false;

            strUserName = Request.ServerVariables["HTTP_SCCHBCUSTOMFLD1"];

            if (!string.IsNullOrEmpty(strUserName))
            {
                AuthData oAuthData = new AuthData();
                oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                oAuthData.UserName = strUserName;
                blnIsUserCred = AppHelper.GetResponse<bool>("RMService/Authenticate/sso", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                //blnIsUserCred = authService.SingleSignOnUser(strUserName);
            }//if

            return blnIsUserCred;
        }//method: IsServerVariable()

        /// <summary>
        /// Determines if the username and password
        /// have been passed in through the QueryString variable or hidden fields
        /// collection
        /// </summary>
        /// <param name="strUserName">reference handle to the string containing the user name</param>
        /// <returns>boolean indicating whether or not the specified credential exists</returns>
        private bool IsRequestParameter(ref string strUserName)
        {
            string strPassword = string.Empty, strUserMessage = string.Empty, strDSN = string.Empty;
            bool blnIsUserCred = false;

            strUserName = Request.QueryString["loginname"];
            strPassword = Request.QueryString["password"];
            strDSN = Request.QueryString["datasource"];

            //If not in the QUeryString, try to get from Form hidden fields
            if (string.IsNullOrEmpty(strUserName) && string.IsNullOrEmpty(strPassword))
            {
                strUserName = Request.Form["loginname"];
                strPassword = Request.Form["password"];
                strDSN = structAdditionPortalParam.strDSNName;
            }

            if (!string.IsNullOrEmpty(strUserName) && !string.IsNullOrEmpty(strPassword))
            {
                //Added by Amitosh for mits 26834
                if (bMCICCookieKeysFound && structAdditionPortalParam.IsEncryptionOn())
                {
                    strUserName = GetDecryptedPortalValue(strUserName);
                    strPassword = GetDecryptedPortalValue(strPassword);
                    strDSN = GetDecryptedPortalValue(strDSN);
                }
                //End Amitosh
                try
                {
                    AuthData oAuthData = new AuthData();
                    oAuthData.UserName = AppHelper.HtmlEncodeString(strUserName);
                    oAuthData.Password = AppHelper.HtmlEncodeString(strPassword);
                    oAuthData.DsnName = AppHelper.HtmlEncodeString(strDSN);
                    oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    string sOutResult  = AppHelper.GetResponse<string>("RMService/Authenticate/IsAllowAutoLogin", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                    blnIsUserCred = Convert.ToBoolean(sOutResult.Split(sDataSeparator, StringSplitOptions.None)[0]);
                    strUserMessage = sOutResult.Split(sDataSeparator, StringSplitOptions.None)[1];
                    //blnIsUserCred = authService.AuthenticateUser( AppHelper.HtmlEncodeString(strUserName), AppHelper.HtmlEncodeString(strPassword), out strUserMessage);

                    //Added by Amitosh for mits 26834
                    if (bMCICCookieKeysFound && blnIsUserCred && !string.IsNullOrEmpty(strDSN))
                    {
                        oAuthData.DsnName = strDSN;
                        //if (string.IsNullOrEmpty(authService.GetUserSessionID(strUserName, strDSN)))
                        if(string.IsNullOrEmpty(AppHelper.GetResponse<string>("RMService/Authenticate/session", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData)))
                            blnIsUserCred = false;
                    }
                    //End Amitosh
                    oAuthData = null;
                }
                catch (Exception ex)
                {
                    blnIsUserCred = false;
                }
                //Close the authentication service
                //authService.Close();
            }//if

            return blnIsUserCred;
        }//method: IsQueryStringParameter() 
        #endregion
        /// <summary>
        /// MITS 26834, Debabrata Biswas
        /// This function will collect the values posted from portal
        /// The DSN name, POWERVIEW name and CLAIM ID will be passed from portal and will be 
        /// saved in cookies.
        /// </summary>
        private void doSavePortalValues()
        {
            string sDSNname = string.Empty, sPVname = string.Empty, sFDMName = string.Empty, sClaimID = string.Empty, sClaimNum = string.Empty, sUserName = string.Empty ;

            //The DSN should be a required value passed to have a Portal Cookie created.
            if (!string.IsNullOrEmpty(structAdditionPortalParam.strDSNName))
            {
                if (structAdditionPortalParam.IsEncryptionOn())
                {
                    sUserName = GetDecryptedPortalValue(FixDecryptText(structAdditionPortalParam.strUserName));
                    sDSNname = GetDecryptedPortalValue(FixDecryptText(structAdditionPortalParam.strDSNName));
                    sPVname = GetDecryptedPortalValue(FixDecryptText(structAdditionPortalParam.sPVname));
                    sFDMName = GetDecryptedPortalValue(FixDecryptText(structAdditionPortalParam.sFDMName));
                    sClaimID = GetDecryptedPortalValue(FixDecryptText(structAdditionPortalParam.sClaimID));                    
                    sClaimNum = GetDecryptedPortalValue(FixDecryptText(structAdditionPortalParam.sClaimNum));
                }
                else
                {
                    sUserName = structAdditionPortalParam.strUserName;
                    sDSNname = structAdditionPortalParam.strDSNName;
                    sPVname  = structAdditionPortalParam.sPVname;
                    sFDMName = structAdditionPortalParam.sFDMName;
                    sClaimID = structAdditionPortalParam.sClaimID;
                    sClaimNum = structAdditionPortalParam.sClaimNum;
                }

                if (sClaimNum != null && !string.IsNullOrEmpty(sClaimNum))
                {
                    AuthData oAuthData = new AuthData();
                    oAuthData.ClaimNum = sClaimNum;
                    oAuthData.UserName = sUserName;
                    oAuthData.DsnName = sDSNname;
                    oAuthData.ClientId = AppHelper.ClientId;
                    sClaimID = AppHelper.GetResponse<string>("RMService/CustomLogin/GetRecordID", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                    structAdditionPortalParam.sClaimID = sClaimID;
                }
                //Since these values are critical, It's best to have them encrypted by using Standard RM cryptography Lib.
                sDSNname = RMCryptography.EncryptString(sDSNname);
                sPVname  = RMCryptography.EncryptString(sPVname);
                sFDMName = RMCryptography.EncryptString(sFDMName);
                sClaimID = RMCryptography.EncryptString(sClaimID);
                
                System.Collections.Specialized.NameValueCollection nvCookieColl = new System.Collections.Specialized.NameValueCollection();

                nvCookieColl.Add("PortalDSNname", sDSNname);
                nvCookieColl.Add("PortalPVname", sPVname);
                nvCookieColl.Add("PortalFDMName", sFDMName);
                nvCookieColl.Add("PortalClaimID", sClaimID);

                AppHelper.CreateUserCookie(nvCookieColl);
            }
        }

        /// <summary>
        /// Author: Debabrata Biswas
        /// MITS: 26834
        /// Since RMX internal cryptography library is a Proprietary to CSC we can not provide this to end customer.
        /// This function is a common routine to be used for decrypting the cyphered text coming from a third party portal.
        /// </summary>
        /// <param name="sEncryptedValue">The String value to be Decrypted</param>
        /// <returns>Decrypted/Plain Text String</returns>
        private string GetDecryptedPortalValue(string sEncryptedValue)
        {
            if (string.IsNullOrEmpty(sEncryptedValue) || sEncryptedValue.Trim() =="")
                return string.Empty;

            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            string Passphrase = "r1$kM@5t3rP@55p4r@53";

            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;

            byte[] DataToDecrypt = Convert.FromBase64String(sEncryptedValue);

            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            return UTF8.GetString(Results);
        }
        /// This function will load Values to the structure
        /// </summary>
        void LoadAdditionalPortalParameters()
        {
                if (Request.QueryString != null)
                {
                    if ((Request.QueryString["loginname"] != null) && (Request.QueryString["encMode"] != null) && (Request.QueryString["datasource"] != null) && (Request.QueryString["apptargetFDMScreenName"] != null) && (Request.QueryString["apptargetClaimRecordID"] != null))
                    {
                        structAdditionPortalParam.strUserName = Request.QueryString["loginname"].Replace(' ', '+');
                        structAdditionPortalParam.strEncOn = Request.QueryString["encMode"].Replace(' ', '+');
                        structAdditionPortalParam.strDSNName = Request.QueryString["datasource"].Replace(' ', '+');
                        structAdditionPortalParam.sFDMName = Request.QueryString["apptargetFDMScreenName"].Replace(' ', '+');
                        structAdditionPortalParam.sClaimNum = Request.QueryString["apptargetClaimRecordID"].Replace(' ', '+');
                        bMCICCookieKeysFound = true;
                    }
                }
                else
                {
                    //if ((Request.Form["encMode"] != null) && (Request.Form["datasource"] != null) && (Request.Form["view"] != null) && (Request.Form["apptargetFDMScreenName"] != null) && (Request.Form["apptargetRecordID"] != null))
                    if ((Request.Form["encMode"] != null) && (Request.Form["datasource"] != null) && (Request.Form["view"] != null) && (Request.Form["apptargetFDMScreenName"] != null) && (Request.Form["apptargetRecordID"] != null) && !string.IsNullOrEmpty(Convert.ToString(Request.Form["apptargetRecordID"])))
            {
                        structAdditionPortalParam.strUserName = Request.Form["loginname"]; 
                structAdditionPortalParam.strEncOn = Request.Form["encMode"];
                structAdditionPortalParam.strDSNName = Request.Form["datasource"];
                structAdditionPortalParam.sPVname = Request.Form["view"];
                structAdditionPortalParam.sFDMName = Request.Form["apptargetFDMScreenName"];
                structAdditionPortalParam.sClaimID = Request.Form["apptargetRecordID"];
                bMCICCookieKeysFound = true;
            }
                    else if ((Request.Form["encMode"] != null) && (Request.Form["datasource"] != null) && !string.IsNullOrEmpty(Convert.ToString(Request.Form["datasource"])) && (Request.Form["view"] != null) && (Request.Form["apptargetFDMScreenName"] != null) && !string.IsNullOrEmpty(Convert.ToString(Request.Form["apptargetFDMScreenName"])))
                    {
                        if (Request.Form["apptargetClaimRecordID"] != null && !string.IsNullOrEmpty(Convert.ToString(Request.Form["apptargetClaimRecordID"])) && Request.Form["loginname"] != null && !string.IsNullOrEmpty(Convert.ToString(Request.Form["loginname"])))
                        {
                            structAdditionPortalParam.strUserName = Request.Form["loginname"]; 
                            structAdditionPortalParam.strEncOn = Request.Form["encMode"];
                            structAdditionPortalParam.strDSNName = Request.Form["datasource"];
                            structAdditionPortalParam.sPVname = Request.Form["view"];
                            structAdditionPortalParam.sFDMName = Request.Form["apptargetFDMScreenName"];
                            structAdditionPortalParam.sClaimNum = Request.QueryString["apptargetClaimRecordID"].Trim();
                            bMCICCookieKeysFound = true;
                        }
                    }
            else
                bMCICCookieKeysFound = false;
                }
        }
        string FixDecryptText(string Text)
        {
            if (Text != null && Text != string.Empty)
            {
                if (Text.Contains(' '))
                    return Text.Replace(' ', '+');
                else
                    return Text;
            }
            else
                return string.Empty;
        }
    }
}
