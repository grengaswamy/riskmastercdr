﻿<%@ page language="C#" autoeventwireup="true" codebehind="Login.aspx.cs" inherits="Riskmaster.UI.Home.Login"
    masterpagefile="~/App_Master/RMXMain.Master" title="CSC Authentication Service"
    stylesheettheme="RMX_Portal" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:content id="cHeader" contentplaceholderid="cphHeader" runat="server">
    <div id="banner" class="banner" runat="server">
    </div>

   <%-- <script>
        if (window != window.top) window.top.navigate(window.location.href);
    </script>--%>
    <%--asharma326 MITS 27586 Starts--%>
    <script type="text/javascript">
        function pageLoad() {
            if (self != top) window.open(window.location.href, "_top", "");
            $find("myModelapopupId").add_shown(onModalPopupShown);
      }
        function onModalPopupShown() {
            document.getElementById('cphMainBody_Login1_txtUserid').focus();
        }
       function HideMe()
        {
           $find("myModelapopupId").hide();
           document.getElementById('cphMainBody_Login1_UserName').focus()
           document.getElementById('cphMainBody_Login1_UserName').tabIndex = 1;
           document.getElementById('cphMainBody_Login1_Password').tabIndex = 2;
           document.getElementById('cphMainBody_Login1_Button1').tabIndex = 3;
           document.getElementById('cphMainBody_Login1_lnkFrgtPwd').tabIndex = 4;
           // document.getElementById('cphMainBody_Login1_btnhide').click();
        }
        function UseridValidations() {
            var varlblError = document.getElementById('cphMainBody_Login1_lblError');
            varlblError.innerText = "";
            var varuserid = document.getElementById('cphMainBody_Login1_txtUserid');
            if (varuserid.value == "") {
                varlblError.style.color = "Red";
                varlblError.innerText = "User ID field must be entered";
                return false;
            }
            else {
                return true;
            }

        }
        function HyperLinkClick() {
            document.getElementById('cphMainBody_Login1_lblError').innerText = "";
            document.getElementById('cphMainBody_Login1_txtUserid').value = "";
        }
      
    </script>
    <%--asharma326 MITS 27586 Starts--%>
</asp:content>
<asp:content id="Content3" contentplaceholderid="cphMainBody" runat="server">
    <asp:login id="Login1" runat="server" onauthenticate="OnAuthenticate" onloggedin="Login1_LoggedIn" onloginerror="Login1_LoginError">
        <layouttemplate>
            <div class="loginOuter" id="loginOuter">
                <div class="loginInner" id="loginInner">
                    <table border="0" class="loginControls">
                        <tr>
                            <td class="loginLabel">
                                <asp:label id="UserNameLabel" runat="server" associatedcontrolid="UserName" tabindex="0">Username:</asp:label>
                            </td>
                            <td>
                                <asp:textbox cssclass="loginTextBox" id="UserName" runat="server" borderstyle="None"
                                    tabindex="1" autocomplete="off"></asp:textbox>
                                <asp:requiredfieldvalidator id="UserNameRequired" runat="server" controltovalidate="UserName"
                                    errormessage="User Name is required." tooltip="User Name is required." validationgroup="Login1">*
                                </asp:requiredfieldvalidator>
                            </td>
                            <td rowspan="2" class="loginButton" align="center">
                                <asp:button id="Button1" runat="server" commandname="Login" text="Login" tabindex="3"
                                    validationgroup="Login1"/>
    <%--asharma326 MITS 27586 Starts--%>
                                <br />
                                <a href="#" runat="server" id="lnkFrgtPwd" onclick="javascript:HyperLinkClick();" tabindex="4" style="font-size: x-small; font-weight: normal; font-style: normal; font-variant: normal">Forgot Password?</a>
                                <asp:Panel ID="Panel1" runat="server" Style="display: none" CssClass="modalPopup" Height="160" BackColor="White" Width="400" DefaultButton="btnSendMail">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                <ContentTemplate>
                                    <img src="../../Images/close-round.png" id="btnCancel" runat="server" alt="Close" title="Close" style="cursor: pointer; width: 30px; height: 30px; position: absolute; right: -15px; top: -15px;" onclick="HideMe();"/>
                                    <div id="Sendmail" style="border: 0px solid black; background-color: whitesmoke; padding: 25px; height:110px; text-align: center;">
                                        Please enter a valid User ID to receive Temporary Password.<br />
                                        <br />
                                        <asp:TextBox ID="txtUserid" autocomplete="off" runat="server"  Width="180"></asp:TextBox>
                                        <br />
                                        <%--<asp:UpdateProgress runat="server" ID="updateprogress1">
                  <ProgressTemplate><strong>Processing...</strong></ProgressTemplate>
                  </asp:UpdateProgress>--%>
                                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                        <br />
                                          
                                        <br />
                                        <asp:Button ID="btnSendMail" runat="server" Text="Send Password" OnClientClick="return UseridValidations();" OnClick="btnSendMail_Click" />
                                        <asp:Button ID="btnhide" runat="server" Text="test" style="display:none" OnClick="btnhide_Click" />
                                          
                                    </div>
                     </ContentTemplate>
            </asp:UpdatePanel>
                                </asp:Panel>
                                <asp:ModalPopupExtender ID="ForgotPasswordModalPopup" runat="server" BehaviorID="myModelapopupId"
                                    PopupControlID="Panel1"
                                    BackgroundCssClass="modalBackground"
                                    RepositionMode="RepositionOnWindowResize"
                                    CancelControlID="btnCancel"
                                    DropShadow="true"
                                    TargetControlID="lnkFrgtPwd" />
    <%--asharma326 MITS 27586 Starts--%>
                                

                           </td>
                        </tr>
                        <tr>
                            <td class="loginLabel">
                                <asp:label id="PasswordLabel" runat="server" associatedcontrolid="Password">Password:</asp:label>
                            </td>
                            <td>
                                <asp:textbox cssclass="loginTextBox" id="Password" runat="server" borderstyle="None"
                                    textmode="Password" tabindex="2" autocomplete="off"></asp:textbox>
                                <asp:requiredfieldvalidator id="PasswordRequired" runat="server" controltovalidate="Password"
                                    errormessage="Password is required." tooltip="Password is required." validationgroup="Login1">*
                                </asp:requiredfieldvalidator>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="FailureText">
                <asp:literal id="FailureText" runat="server" enableviewstate="False"></asp:literal></div>
            <asp:checkbox id="RememberMe" runat="server" text="Remember me next time." visible="false" />
        </layouttemplate>
    </asp:login>
    <div align="center">
&nbsp;<iframe id="ifHomepage" class="Homepage"  frameborder="0" scrolling="auto" runat="server" /></div>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager><%--asharma326 MITS 27586--%>
</asp:content>
