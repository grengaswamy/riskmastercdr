﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ServiceModel;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.AppHelpers;
using System.Web;
using Riskmaster.Security.Encryption;
using System.Net;
using Riskmaster.Models;
using System.IO;
using Riskmaster.Cache;


namespace Riskmaster.UI.Home
{
    public partial class Status : Page
    {
        #region private member variables
        //Raman : R8 performance improvement : By using server.transfer we can save on a roundtrip
        //changed on 3/19/2012
        //private string loginUserName = HttpContext.Current.User.Identity.Name;
        //R8 Perf Imp
        private string strCustomViewNames = "";
        private bool isBOB  = false;
        //R8 Perf Imp
        private string loginUserName = ""; //HttpContext.Current.Items["loginName"].ToString();
        //Deb ML Changes these are fixed for existing RMX system
        private string sLanguageCode = "1033|en-US";
        private string sBaseLanguageCode = "1033|en-US";
        //Deb ML Changes
        private string sEnableVSS = string.Empty; //averma62 - rmA - VSS integration
		private string sCurrentDateSetting = string.Empty; //igupta3  Mits:32846 
        private string sAdjAssignmentAutoDiary = string.Empty;  //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        //Ankit Start : Worked on MITS - 32386 - FAS Integration
        private string sEnableFAS = string.Empty;
        private string sFileLocationSelection = string.Empty;
        private string sSharedLocation = string.Empty;
        private string sFASServer = string.Empty;
        private string sFASUserId = string.Empty;
        private string sFASPassword = string.Empty;
        private string sFASFolder = string.Empty;
        //Ankit End
        //private RMAuthentication.AuthenticationServiceClient authService = new RMAuthentication.AuthenticationServiceClient();
        //rsolanki2: extensibility updates.
        private bool m_bHasCustomViewsListLoaded = false; 

        #endregion
        bool bMCICCookieKeysFound = false; //Added by Amitosh for mits 26834:.Flag to check for MCIC
        public static string sDSN = string.Empty;//Deb MITS 30897
        #region Page Events
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = "RMX_Default";
        } // method: Page_PreInit

        /// <summary>
        /// Handles the initial loading of the Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["src"] != null)
            {
                LoadRMALogo();
                return;
            }
            string strViewList = string.Empty;

            //Raman : R8 performance improvement 3/20/2012

            if (!String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
            {
                loginUserName = HttpContext.Current.User.Identity.Name;
                tmploginname.Text = String.Empty;
            }
            else if (HttpContext.Current.Items["loginName"] != null)
            {
                loginUserName = HttpContext.Current.Items["loginName"].ToString();
                tmploginname.Text = loginUserName;
            }
            //RMA-88 CSC SSO Implemetation Starts
            else if (HttpContext.Current.Items["HTTP_USER"] != null)
            {
                loginUserName = HttpContext.Current.Items["HTTP_USER"].ToString();
                tmploginname.Text = loginUserName;
            }
            //RMA-88 CSC SSO Implemetation Ends
            //Asharma326 MITS 27586 Starts
            AuthData oAuthData = new AuthData();
            oAuthData.UserName = loginUserName;
            oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
            //if (authService.IsPasswordReset(loginUserName,AppHelper.ClientId))
            if (AppHelper.GetResponse<bool>("RMService/Authenticate/ispasswordreset", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData))
            {
                Response.Redirect("../SecurityMgtSystem/ChangePassword.aspx?forgotpassword=1");
            }
            oAuthData = null;
            //Asharma326 MITS 27586 Ends
            //Only perform bindings on the initial load of the Page
            if (!Page.IsPostBack)
            {
                
                ResetScreen();

                //try to add Auth cookie to the Response again. Sometime the cookies got lost
                ResetAuthenticationCookie();

                //TODO: These DropDown Lists are perfect candidates for the AJAX control for Cascading DropDownLists
                BindDSNs();
                sDSN = ddlDataSources.SelectedItem.Text;//Deb MITS 30897
                BindViews(ddlDataSources.SelectedItem.Text, ddlDataSources.SelectedItem.Value);
                //Debabrata Biswas
                if (bAutoPortalLogin() && bMCICCookieKeysFound)
                    PopulateUserInfo();


                //Thursday Feb 26 ..modified by Raman Bhatia
                //If only 1 Data Source is there and at max 1 PowerView is there then we can auto-login
                if (ddlDataSources.Items.Count == 1 && ddlViews.Items.Count <= 1)
                {
                    PopulateUserInfo();
                }
                else if (ddlDataSources.Items.Count > 1 && ddlViews.Items.Count > 1)
                {

                    try
                    {
                        string sBackgroundColor = string.Empty;

                        sBackgroundColor = RMXPortalHelper.GetBannerColorAsHTML();

                        banner.Style.Add("background-color", sBackgroundColor);

                    }
                    catch (Exception objException)
                    {
                        ErrorHelper.logErrors(objException);
                    }// try

                } // else
                //Asharma326 MITS 33691 Starts
                else
                {
                    try
                    {
                        string sBackgroundColor = string.Empty;

                        sBackgroundColor = RMXPortalHelper.GetBannerColorAsHTML();

                        banner.Style.Add("background-color", sBackgroundColor);

                    }
                    catch (Exception objException)
                    {
                        ErrorHelper.logErrors(objException);
                    }// try
                }
                //Asharma326 MITS 33691 Ends
                banner.Style.Add("background-image", "Status.aspx?src=rmx_logo.gif");
            } // if
        }
        /// <summary>
        /// Load Image
        /// </summary>
        private void LoadRMALogo()
        {
            try
            {
                byte[] filecontent = RMXPortalHelper.PortalHomeImage;
                if (filecontent == null)
                {
                    //banner.Style.Add("background-image", "../../Images/rmx_logo.gif");
                    string cacheimageKey = "RMXPortalImage";
                    FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\Images\rmx_logo.gif", FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    filecontent = br.ReadBytes((int)fs.Length);
                    CacheCommonFunctions.UpdateValue2Cache<byte[]>(cacheimageKey, AppHelper.ClientId, filecontent); 
                }
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "images/jpeg";
                Response.AddHeader("content-disposition", "attachment;filename=imagestatuslogo");
                Response.BinaryWrite(filecontent);
                Response.Flush();
                Response.End();
            }
            catch
            {
            }
        }
        /// <summary>
        /// MITS 26834, Debabrata Biswas
        /// This function will first check if Portal cookie has been created.
        /// Then will check if the supplied DSN and power view exists in the dropwown list.
        /// Then it will have them selected and will return true.
        /// </summary>
        /// <returns></returns>
        private bool bAutoPortalLogin()
        {
            bool bAutoLogin = false, bDSNExists=false,bPVExists=false;

            string sDSNname = string.Empty, sPVname = string.Empty, sClaimID = string.Empty, sEncMode = string.Empty ;
            ListItem lstDSN = null, lstPV = null;

            HttpCookie objUserCookie = HttpContext.Current.Request.Cookies.Get("UserInfoCookie");

            if (objUserCookie != null)
            {
                if ((objUserCookie["PortalDSNname"] != null) && (objUserCookie["PortalPVname"] != null))
                {
                    sDSNname = AppHelper.ReadCookieValue("PortalDSNname");
                    sPVname = AppHelper.ReadCookieValue("PortalPVname");

                    sDSNname = RMCryptography.DecryptString(sDSNname);
                    sPVname = RMCryptography.DecryptString(sPVname);

                    if (!string.IsNullOrEmpty(sDSNname))
                    {
                        lstDSN = FindByText(ddlDataSources, sDSNname);
                        if (lstDSN != null)
                            bDSNExists = true;
                    }

                    if (!string.IsNullOrEmpty(sPVname))
                    {
                        lstPV = FindByText(ddlViews, sPVname);
                        if (lstPV != null)
                            bPVExists = true;
                    }

                    bAutoLogin = bDSNExists && bPVExists;

                    if (bAutoLogin)
                    {
                        ddlDataSources.Text = lstDSN.Value;
                        ddlViews.Text = lstPV.Value;
                    }

                    bMCICCookieKeysFound = true;
                }
                else
                    bMCICCookieKeysFound = false;
            }
            return bAutoLogin;
        } // method: Page_Load

        /// <summary>
        /// After you select a new DSN, the error message and login button should be reset
        /// MITS 19942
        /// </summary>
        private void ResetScreen()
        {
            btnLogin.Enabled = true;
            ErrorControl1.Text = string.Empty;
        }

        /// <summary>
        /// MITS 17663 For Boeing IE8, the form authentication cookie got lost and reset
        /// resolve the issue.
        /// </summary>
        private void ResetAuthenticationCookie()
        {
            //try to add Auth cookie to the Response again. Sometime the cookies got lost
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null)
            {
                return;
            }

            FormsAuthenticationTicket authTicket = null;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch (Exception ex)
            {
                return;
            }
            //if ticket is expired or not found terminate the execution and return to the login page to authenticate 
            if ((authTicket == null) || authTicket.Expired)
            {
                return;
            }

            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            authCookie.Value = encryptedTicket;
            authCookie.Expires = authTicket.Expiration;
            authCookie.HttpOnly = true;//Deb Added For Pen Testing
            Response.Cookies.Add(authCookie);
        }



        /// <summary>
        /// Event to handle the unloading of the Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Unload(object sender, EventArgs e)
        {
            //Ensure that the call to the WCF Service is closed
            //authService.Close();

            ////Clean up the instance of the service
            //if (authService != null)
            //{
            //    authService = null;    
            //} // if
        } // method: Page_Unload 
        #endregion

        #region Binding Methods
        /// <summary>
        /// Creates a ListItemCollection based on a Generic Dictionary object
        /// </summary>
        /// <param name="objDictValues">Generic Dictionary object</param>
        /// <returns>ListItemCollection used for binding controls</returns>
        private ListItemCollection GetBindingCollection(Dictionary<string, string> objDictValues)
        {

            ListItemCollection objLiColl = new ListItemCollection();

            //Loop through each item in the Dictionary collection
            foreach (string strKey in objDictValues.Keys)
            {
                ListItem objLiItem = new ListItem();

                objLiItem.Text = strKey;
                objLiItem.Value = objDictValues[strKey];

                objLiColl.Add(objLiItem);
            } // foreach


            return objLiColl;
        } // method: GetBindingCollection

        /// <summary>
        /// Bind the Views based on the selected DSN
        /// </summary>
        /// <param name="strDSNName">string containing the name of the Data Source</param>
        private void BindViews(string strDSNName,string strDSNId)
        {
            try
            {
                //ListItemCollection objLiColl = GetBindingCollection(authService.GetUserViews(loginUserName, strDSNName, strDSNId));
                AuthData oAuthData = new AuthData();
                oAuthData.UserName = loginUserName;
                oAuthData.DsnName = strDSNName;
                oAuthData.DsnId = int.Parse(strDSNId);
                oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                ListItemCollection objLiColl = GetBindingCollection(AppHelper.GetResponse<Dictionary<string,string>>("RMService/Authenticate/View", AppHelper.HttpVerb.POST, "application/json", oAuthData));
                if (objLiColl.Count == 0)
                {
                    //Hide the relevant View related controls
                    lblViews.Visible = false;
                    ddlViews.Visible = false;

                    //Removing previous DropDownList items(if any) in order to return DEFAULT_VIEW
                    ddlViews.Items.Clear();
                } // if
                else
                {
                    //Ensure that the relevant View related controls are visible
                    lblViews.Visible = true;
                    ddlViews.Visible = true;

                    //Bind the User Views
                    ddlViews.DataSource = objLiColl;
                    ddlViews.DataTextField = "text";
                    ddlViews.DataValueField = "value";
                    ddlViews.DataBind();
                } // else
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                //Deb MITS 30897
                AppHelper.DSNName = sDSN;
                ErrorControl1.Text = ErrorHelper.UpdateErrorMessage(ex.Message);
                //Deb MITS 30897
                btnLogin.Enabled = false;
            }

        } // method: BindViews
        /// <summary>
        /// Binds the DSNs to the DSN Data Source selection
        /// </summary>
        private void BindDSNs()
        {

            //Perform a check to verify that the specified user has been assigned to a DSN
            //ListItemCollection objLiCollDSNs = GetBindingCollection(authService.GetUserDSNs(loginUserName));
            AuthData oAuthData = new AuthData();
            oAuthData.UserName = loginUserName;
            oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
            ListItemCollection objLiCollDSNs = GetBindingCollection(AppHelper.GetResponse<Dictionary<string,string>>("RMService/Authenticate/Dsn", AppHelper.HttpVerb.POST, "application/json", oAuthData));
            if (objLiCollDSNs.Count == 1)//rsolanki2: extensibility updates
            {
                System.Collections.IEnumerator objEnum =  objLiCollDSNs.GetEnumerator();
                objEnum.MoveNext();

                //rsolanki2(may 23,2010): extensibility updates start - retriving custom views 
                //Application.Add("CustomViewNames"
                //    , "|"
                //    + authService.GetCustomViews(((System.Web.UI.WebControls.ListItem)(objEnum.Current)).Value.ToString())
                //    + "|");                           

                //PSARIN2 : R8 Perf Improvement Commented as there is no need for it..
                //NameValueCollection nvCookieColl = new NameValueCollection();                
                //nvCookieColl.Add("CustomViewNames"
                //    , authService.GetCustomViews(((System.Web.UI.WebControls.ListItem)(objEnum.Current)).Value.ToString()));

                //AppHelper.CreateUserCookie(nvCookieColl);
                //m_bHasCustomViewsListLoaded = true;
            } 
            if (objLiCollDSNs.Count == 0)
            {
                throw new ApplicationException("No DSNs have been assigned to this user.  Please verify your settings with your system administrator.");
            } // if
            else
            {
                ddlDataSources.Visible = true;

                //Bind the User DSNs
                ddlDataSources.DataSource = objLiCollDSNs;
                ddlDataSources.DataTextField = "text";
                ddlDataSources.DataValueField = "value";
                ddlDataSources.DataBind();
            } // else

            
        } // method: BindDSNs 
        #endregion

        /// <summary>
        /// Handles the Selection Changed Event for the Data Source DropDownList
        /// DropDownlist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlDataSources_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetScreen();
            sDSN = ddlDataSources.SelectedItem.Text;//Deb MITS 30897
            BindViews(ddlDataSources.SelectedItem.Text,ddlDataSources.SelectedItem.Value);
        }

        /// <summary>
        /// Handles the OnClick event of the Login button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            PopulateUserInfo();
        }

        /// <summary>
        /// Populates all of the relevant User Information 
        /// required for redirecting to the Home Page
        /// </summary>
        private void PopulateUserInfo()
        {
            string strSessionID = string.Empty, strDSNID = string.Empty;
            sDSN = ddlDataSources.SelectedItem.Text;//Deb MITS 30897
            strDSNID = ddlDataSources.SelectedValue;
            //bool isBOB = false;
            Dictionary<string, string> dictSessionInfo = null;
            string sDSNname = string.Empty;
            ListItem lstDSN = null;
            try
            {

                //PSARIN2 : R8 Perf Improvement    
                //strSessionID = authService.GetUserSessionID(loginUserName, ddlDataSources.SelectedItem.Text);
                if (bMCICCookieKeysFound)
                {
                    sDSNname = AppHelper.ReadCookieValue("PortalDSNname");

                    sDSNname = RMCryptography.DecryptString(sDSNname);

                    if (!string.IsNullOrEmpty(sDSNname))
                    {
                        lstDSN = FindByText(ddlDataSources, sDSNname);
                    }
                    AuthData oAuthData = new AuthData();
                    oAuthData.UserName = loginUserName;
                    oAuthData.DsnId = int.Parse(lstDSN.Value);
                    oAuthData.DsnName = lstDSN.Text;
                    oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    dictSessionInfo = AppHelper.GetResponse<Dictionary<string, string>>("RMService/Authenticate/CurrentUser", AppHelper.HttpVerb.POST, "application/json", oAuthData);
                    //dictSessionInfo = authService.GetUserSessionInfo(loginUserName, lstDSN.Text, lstDSN.Value);
                    oAuthData = null;
                }
                else
                { 
                    //dictSessionInfo = authService.GetUserSessionInfo(loginUserName, ddlDataSources.SelectedItem.Text, strDSNID);
                    //string url = HttpContext.Current.Application["BaseServiceUrl"] + "RMService/Authenticate/CurrentUser";
                    //WebClient client = new WebClient();
                    //client.Headers[HttpRequestHeader.ContentType] = "application/json"; // working
                    AuthData oAuthData = new AuthData();
                    oAuthData.UserName = loginUserName;
                    oAuthData.DsnId = int.Parse(strDSNID);
                    oAuthData.DsnName = ddlDataSources.SelectedItem.Text;
                    oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    //string data = AppHelper.ConvertCharToHTML("{\"oAuthData\": " + JSONSerializeDeserialize.Serialize(oAuthData) + "}");
                    //string responseString = client.UploadString(url, "POST", data);
                    //dictSessionInfo = JSONSerializeDeserialize.Deserialize<Result>(responseString).GetUserSessionInfoResult;
                    dictSessionInfo = AppHelper.GetResponse<Dictionary<string, string>>("RMService/Authenticate/CurrentUser", AppHelper.HttpVerb.POST, "application/json", oAuthData);
                    oAuthData = null;
                }
                strSessionID = dictSessionInfo["SessionId"];
                isBOB = Riskmaster.Common.Conversion.ConvertStrToBool(dictSessionInfo["BOBSetting"]);
                strCustomViewNames = dictSessionInfo["CustomViews"];
                //isBOB = authService.GetBOBSetting(loginUserName, ddlDataSources.SelectedItem.Text);//added by Amitosh for Adding carrier claim settings in session
                //Populate the Session information
                //PSARIN2 : R8 Perf Improvement
                sLanguageCode = Convert.ToString(dictSessionInfo["LanguageCode"]);//Deb: Multi Language Changes
                sBaseLanguageCode = Convert.ToString(dictSessionInfo["BaseLanguageCode"]);//Deb: Multi Language Changes
                sEnableVSS = Convert.ToString(dictSessionInfo["EnableVSS"]);//averma62 - rmA-VSS integration
				sCurrentDateSetting = Convert.ToString(dictSessionInfo["CurrentDateSetting"]);//igupta3 Mits:32846 
                sAdjAssignmentAutoDiary = Convert.ToString(dictSessionInfo["AdjAssignmentAutoDiary"]);  //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                //Ankit Start : Worked on MITS - 32386 - FAS Integration
                sEnableFAS = Convert.ToString(dictSessionInfo["EnableFAS"]);
                sFileLocationSelection = Convert.ToString(dictSessionInfo["FileLocationSelection"]);
                sSharedLocation = Convert.ToString(dictSessionInfo["SharedLocation"]);
                sFASServer = Convert.ToString(dictSessionInfo["FASServer"]);
                sFASUserId = Convert.ToString(dictSessionInfo["FASUserId"]);
                sFASPassword = Convert.ToString(dictSessionInfo["FASPassword"]);
                sFASFolder = Convert.ToString(dictSessionInfo["FASFolder"]);
                //Ankit End
                PopulateSessionInfo(strSessionID);

                PopulateUserCookie(strSessionID);
                //Application.Add("CustomViewNames", authService.GetCustomViews(ddlDataSources.SelectedItem.Value));

                //Redirect to the default Forms Authentication Url
                Response.Redirect(FormsAuthentication.DefaultUrl);
            }

            catch (FaultException ee)
            {
                //ErrorControl1.Text = "Could not establish a new session. Please verify your Session and Security settings.";
                //Deb MITS 30897
                AppHelper.DSNName = sDSN;
                ErrorControl1.Text = ErrorHelper.UpdateErrorMessage(ee.Message.ToString());
                //Deb MITS 30897
                ErrorHelper.logErrors(ee);
            }//catch

        }//method: PopulateUserInfo()

        /// <summary>
        /// Stores relevant authorization information in Session variables
        /// </summary>
        /// <param name="strSessionID">string containing the user's Session ID</param>
        private void PopulateSessionInfo(string strSessionID)
        {
            StringDictionary dictSessionValues = new StringDictionary();

            dictSessionValues.Add("SessionId", strSessionID);
            dictSessionValues.Add("ViewId", GetViewID(ddlViews.SelectedValue));
            //PSARIN2 : R8 Perf Improvement
            dictSessionValues.Add("IsBOB", isBOB.ToString()); //added by Amitosh for Adding carrier claim settings in session
            //PSARIN2 : R8 Perf Improvement
            //dictSessionValues.Add("LanguageCode", sLanguageCode);
            AppHelper.CreateUserSession(dictSessionValues);
        }//method: PopulateSessionInfo

        /// <summary>
        /// Stores relevant authorization information in a User Coookie
        /// </summary>
        /// <param name="strSessionID">string containing the SessionId value</param>
        private void PopulateUserCookie(string strSessionID)    
        {
            NameValueCollection nvCookieColl = new NameValueCollection();
            nvCookieColl.Add("ClientId", Convert.ToString(AppHelper.ClientId));
            nvCookieColl.Add("SessionId", strSessionID);
            nvCookieColl.Add("DsnId", ddlDataSources.SelectedValue);
            nvCookieColl.Add("DsnName", ddlDataSources.SelectedItem.Text);
            nvCookieColl.Add("ViewId", GetViewID(ddlViews.SelectedValue));
            nvCookieColl.Add("ViewName", GetViewName(ddlViews.SelectedItem).Replace("&", "^@")); //Aman MITS 27767
            //PSARIN2 : R8 Perf Improvement
            nvCookieColl.Add("CustomViewNames", strCustomViewNames);
            //PSARIN2 : R8 Perf Improvement
            //Deb ML Changes
            nvCookieColl.Add("LanguageCode", sLanguageCode);
            nvCookieColl.Add("BaseLanguageCode", sBaseLanguageCode);
            //Deb ML Changes
            nvCookieColl.Add("EnableVSS", sEnableVSS);
			nvCookieColl.Add("CurrentDateSetting", sCurrentDateSetting); //igupta3 Mits:32846 
            nvCookieColl.Add("AdjAssignmentAutoDiary", sAdjAssignmentAutoDiary);    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            //Ankit Start : Worked on MITS - 32386 - FAS Integration
            nvCookieColl.Add("EnableFAS", sEnableFAS);
            nvCookieColl.Add("FileLocationSelection", sFileLocationSelection);
            nvCookieColl.Add("SharedLocation", sSharedLocation);
            nvCookieColl.Add("FASServer", sFASServer);
            nvCookieColl.Add("FASUserId", sFASUserId);
            nvCookieColl.Add("FASPassword", sFASPassword);
            nvCookieColl.Add("FASFolder", sFASFolder);
            //Ankit End
            //rsolanki2(may 23,2010): extensibility updates start - retriving custom views 
            //if (!m_bHasCustomViewsListLoaded )
            //PSARIN2 : R8 Perf Improvement Commented
            // Todo-rsolanki2 : we may want to add a check if the cookie CustomViewNames has already been set 
            //nvCookieColl.Add("CustomViewNames", authService.GetCustomViews(ddlDataSources.SelectedItem.Value));                 
            //else
            //    nvCookieColl.Add("CustomViewNames"
            //        , AppHelper.ReadCookieValue("CustomViewNames"));                 
            
            //m_bHasCustomViewsListLoaded = false;               
            //Debabrata Biswas. This is to get cookie value before its being recreated.
            if (bMCICCookieKeysFound)
            {
                nvCookieColl.Add("PortalFDMName", AppHelper.ReadCookieValue("PortalFDMName"));
                nvCookieColl.Add("PortalClaimID", AppHelper.ReadCookieValue("PortalClaimID"));
            }
            AppHelper.CreateUserCookie(nvCookieColl);
        }//method: PopulateUserCookie

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strViewID"></param>
        /// <returns></returns>
        private string GetViewID(string strViewID)
        {
            const string DEFAULT_VIEW_ID = "0";

            if (string.IsNullOrEmpty(strViewID))
            {
                return DEFAULT_VIEW_ID;    
            } // if
            else
            {
                return strViewID;
            } // else


        } // method: GetDefaultViewID

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strViewName"></param>
        /// <returns></returns>
        private string GetViewName(ListItem objViewName)
        {
            const string DEFAULT_VIEW_NAME = "DEFAULT_VIEW";

            if (objViewName == null)
            {
                return DEFAULT_VIEW_NAME;
            } // if
            else
            {
                return objViewName.Text;
            } // else
        } // method: GetViewName

        /// <summary>
        /// Debabrata Biswas. The string comparison should be Case Insensitive.
        /// MITS 26834
        /// </summary>
        /// <param name="lstBox"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private ListItem FindByText(DropDownList lstBox, string value)
        {
            ListItem liTemp = null;
            foreach (ListItem li in lstBox.Items)
            {
                if (li.Text.ToLower() == value.ToLower())
                {
                    liTemp = li;
                    break;
                }
            }
            
            return liTemp;
        } 
    }
}

