﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.WordMergeEmailSetup
{
    public partial class WordMergeEmailDetailSetup : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();
        XmlDocument Model = new XmlDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("WordMergeEmailDetailSetup.aspx"), "WordMergeEmailDetailSetupValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "WordMergeEmailDetailSetupValidations", sValidationResources, true);
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    if (mode.Text.ToLower() == "edit")
                    {
                        txtSelectedid.Text = AppHelper.GetQueryStringValue("selectedid");
                    }
                }
                if (txtReloadNA.Text.ToLower() == "true")
                {
                    XmlTemplate = GetMessageTemplate("reload");
                    CallCWSFunctionBind("MailMergeAdaptor.GetMergeEmailDetail", out sCWSresponse, XmlTemplate);
                    Model.LoadXml(sCWSresponse);
                    PopulateTemplates(Model);
                    PopulateEmailVariables(Model);
                    txtReloadNA.Text = "";
                }
                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";
                    Control c = DatabindingHelper.GetPostBackControl(this.Page);
                    if (c == null)
                    {
                        if (mode.Text.ToLower() == "edit")
                        {
                            XmlTemplate = GetMessageTemplate("edit");
                            CallCWS("MailMergeAdaptor.GetMergeEmailDetail", XmlTemplate, out sCWSresponse, false, true);
                            Model.LoadXml(sCWSresponse);
                            PopulateTemplates(Model);
                            PopulateEmailVariables(Model);
                        }
                        else
                        {
                            XmlTemplate = GetMessageTemplate("add");
                            CallCWS("MailMergeAdaptor.GetMergeEmailDetail", XmlTemplate, out sCWSresponse, true, true);
                            Model.LoadXml(sCWSresponse);
                            PopulateTemplates(Model);
                            PopulateEmailVariables(Model);

                        }
                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private void PopulateTemplates(XmlDocument doc)
        {
            XmlElement objElement = null;
            ListItem item = new ListItem();
            item.Text = "";
            item.Value = "";
            Template.Items.Add(item);
            objElement = (XmlElement)doc.SelectSingleNode("//control[@name='Template']");
            foreach (XmlElement ele in objElement.ChildNodes)
            {
                item = new ListItem();
                item.Text = ele.Attributes["tempname"].Value;
                item.Value = ele.Attributes["tempid"].Value;
                if (ele.Attributes["Selected"] != null && string.Compare(ele.Attributes["Selected"].Value, "true", true) == 0)
                {
                    item.Selected = true;
                    HdnTempIdEdit.Text = item.Value;
                }
                //if (ele.Attributes["InGrid"] != null && string.Compare(ele.Attributes["InGrid"].Value, "Yes", true) == 0)
                //{
                //    item.Attributes.Add("InGrid", "Yes");
                //}
                Template.Items.Add(item);                
            }
        }
        private void PopulateEmailVariables(XmlDocument doc)
        {
            XmlElement objElement = null;            
            //drop down SubVar1
            ListItem item = new ListItem();
            item.Text = "";
            item.Value = "";
            SubVar1.Items.Add(item);
            objElement = (XmlElement)doc.SelectSingleNode("//control[@name='SubVar1']//Variable");
            foreach (XmlElement ele in objElement.ChildNodes)
            {
                item = new ListItem();
                item.Text = ele.Attributes["vardesc"].Value;
                item.Value = ele.Attributes["varcode"].Value;
                if (ele.Attributes["Selected"] != null && string.Compare(ele.Attributes["Selected"].Value, "true", true) == 0)
                {
                    item.Selected = true;
                }
                SubVar1.Items.Add(item);
            }
            //Pradyumna 10/10/2014 MITS 36930 - Start
            ////drop down SubVar2
            //item = new ListItem();
            //item.Text = "";
            //item.Value = "";
            //SubVar2.Items.Add(item);
            //objElement = (XmlElement)doc.SelectSingleNode("//control[@name='SubVar2']//Variable");
            //foreach (XmlElement ele in objElement.ChildNodes)
            //{
            //    item = new ListItem();
            //    item.Text = ele.Attributes["vardesc"].Value;
            //    item.Value = ele.Attributes["varid"].Value;
            //    if (ele.Attributes["Selected"] != null && string.Compare(ele.Attributes["Selected"].Value, "true", true) == 0)
            //    {
            //        item.Selected = true;
            //    }
            //    SubVar2.Items.Add(item);
            //}
            //Pradyumna 10/10/2014 MITS 36930 - End
            //drop down SubVar1
            item = new ListItem();
            item.Text = "";
            item.Value = "";
            BodyVar1.Items.Add(item);
            objElement = (XmlElement)doc.SelectSingleNode("//control[@name='BodyVar1']//Variable");
            foreach (XmlElement ele in objElement.ChildNodes)
            {
                item = new ListItem();
                item.Text = ele.Attributes["vardesc"].Value;
                item.Value = ele.Attributes["varcode"].Value;
                if (ele.Attributes["Selected"] != null && string.Compare(ele.Attributes["Selected"].Value, "true", true) == 0)
                {
                    item.Selected = true;
                }
                BodyVar1.Items.Add(item);
            }

            //Pradyumna 10/10/2014 MITS 36930 - Start
            ////drop down BodyVar2
            //item = new ListItem();
            //item.Text = "";
            //item.Value = "";
            //BodyVar2.Items.Add(item);
            //objElement = (XmlElement)doc.SelectSingleNode("//control[@name='BodyVar2']//Variable");
            //foreach (XmlElement ele in objElement.ChildNodes)
            //{
            //    item = new ListItem();
            //    item.Text = ele.Attributes["vardesc"].Value;
            //    item.Value = ele.Attributes["varid"].Value;
            //    if (ele.Attributes["Selected"] != null && string.Compare(ele.Attributes["Selected"].Value, "true", true) == 0)
            //    {
            //        item.Selected = true;
            //    }
            //    BodyVar2.Items.Add(item);
            //} 
            //Pradyumna 10/10/2014 MITS 36930 - End
        }

         /// <summary>
        /// Method to generate the XML template for Adjuster set up screen.
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="strValue">Mode of the screen</param>
        /// <returns></returns>
        private XElement GetMessageTemplate(string strValue)
        {
            StringBuilder sXml = new StringBuilder("");
            try
            {
                switch (strValue.ToLower())
                {
                    case "edit":
                        sXml = sXml.Append("<Message>");
                        sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                        sXml = sXml.Append("<Call><Function></Function></Call>");
                        sXml = sXml.Append("<Document><MergeEmailDetail>");
                        sXml = sXml.Append("<control name='MergeEmailDetailID' type='id'>");
                        sXml = sXml.Append(txtSelectedid.Text);
                        sXml = sXml.Append("</control>");
                        sXml = sXml.Append("<control name='Template'></control>");
                        sXml = sXml.Append("<control name='EmailFrom'></control>");
                        sXml = sXml.Append("<control name='SubText1'></control>");
                        sXml = sXml.Append("<control name='SubVar1'></control>");
                        // Commented by Pradyumna 10/13/2014 MITS 36930
                        //sXml = sXml.Append("<control name='SubText2'></control>");
                        //sXml = sXml.Append("<control name='SubVar2'></control>");
                        sXml = sXml.Append("<control name='BodyText1'></control>");
                        sXml = sXml.Append("<control name='BodyVar1'></control>");
                        // Commented by Pradyumna 10/13/2014 MITS 36930
                        //sXml = sXml.Append("<control name='BodyText2'></control>"); 
                        //sXml = sXml.Append("<control name='BodyVar2'></control>");
                        //sXml = sXml.Append("<control name='BodyText3'></control>");
                        sXml = sXml.Append("</MergeEmailDetail>");
                        sXml = sXml.Append("</Document></Message>");
                        break;
                    case "add":
                        sXml = sXml.Append("<Message>");
                        sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                        sXml = sXml.Append("<Call><Function></Function></Call>");
                        sXml = sXml.Append("<Document><MergeEmailDetail>");
                        sXml = sXml.Append("</MergeEmailDetail>");
                        sXml = sXml.Append("</Document></Message>");
                        break;
                    case "normal":
                        sXml = sXml.Append("<Message>");
                        sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                        sXml = sXml.Append("<Call><Function>MailMergeAdaptor.GetMergeEmailDetail</Function></Call>");
                        sXml = sXml.Append("<Document><MergeEmailDetail>");
                        sXml = sXml.Append("</MergeEmailDetail>");
                        sXml = sXml.Append("</Document></Message>");
                        break;
                    //MITS 26266 Manish Jain ,Created reload template
                    case "reload":
                        sXml = sXml.Append("<Message>");
                        sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                        sXml = sXml.Append("<Call><Function>MailMergeAdaptor.GetMergeEmailDetail</Function></Call>");
                        sXml = sXml.Append("<Document><MergeEmailDetail>");
                        sXml = sXml.Append("<control name='AutoAssignID' type='id'>");
                        sXml = sXml.Append(txtSelectedid.Text);
                        sXml = sXml.Append("</control>");                        
                        sXml = sXml.Append("</MergeEmailDetail>");
                        sXml = sXml.Append("</Document></Message>");
                        break;

                }
                XElement oElement = XElement.Parse(sXml.ToString());
                return oElement;
            }
            finally
            {
                sXml = null; 
            }
        }
      

        /// <summary>
        /// Event handler for the click event of the button btnOK
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate("normal");
                CallCWSFunction("MailMergeAdaptor.SaveMergeEmailDetail", out sCWSresponse, XmlTemplate);
                if (ErrorControl1.Text == string.Empty)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        /// <summary>
        /// Event handler for the click event of the button btnCancel
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }

        protected void Template_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ListItem item = new ListItem();
            //item = Template.SelectedItem;
            //if (item.Attributes["InGrid"] != null)
            //{
            //    if(string.Compare(item.Attributes["InGrid"].ToString(),"Yes", true) == 0)
            //        Page.ClientScript.RegisterStartupScript(this.GetType(), "k", "<script>ExistInMergeGrid();</script>", false);  

            //}
        }
    }
}