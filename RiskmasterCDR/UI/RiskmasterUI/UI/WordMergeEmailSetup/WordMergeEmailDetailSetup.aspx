﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WordMergeEmailDetailSetup.aspx.cs" Inherits="Riskmaster.UI.WordMergeEmailSetup.WordMergeEmailDetailSetup"  ValidateRequest = "false" %>

<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Word Merge Email Detail Setup</title>
     <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/supportscreens.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>
       <script type="text/javascript" language="JavaScript" src="../../Scripts/merge.js">           { var i; }</script>
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; } </script>
    <script type="text/javascript">
        // Pradyumna 10/10/2014 MITS 36930 - Start
        //function AddSubVariable() {
        $(document).ready(function () {
            $('#SubText1').bind('blur', function (e) {
                var len = $('#SubText1').val().trim().length;
                if (len >= 250) {
                    e.preventDefault();
                    $('#SubText1').val($('#SubText1').val().trim().substring(0, 250));
                }
            });
            $('#SubText1').bind('keypress', function (e) {
                var len = $('#SubText1').val().trim().length;
                if (len >= 250) {
                    e.preventDefault();
                }
                if ((e.keyCode || e.which) == 13) {
                    alert(WordMergeEmailDetailSetupValidations.EnterNotAllowed);
                    e.preventDefault();
                }
            });
        });
        //}
        function AddSubVariable() {
            var sValue = $('#SubVar1 :selected').val();
            if (sValue !== null && sValue !== undefined) {
                if (sValue.trim()) {
                    var len = $('#SubText1').val().trim().length + ('<<' + sValue + '>>').length;
                    if (len <= 250) {
                        $('#SubText1').val($('#SubText1').val() + '<<' + sValue + '>>');
                    }
                }
            }
        }
        function AddBodyVariable() {
            var sBodyValue = $('#BodyVar1 :selected').val();
            if (sBodyValue !== null && sBodyValue !== undefined) {
                if (sBodyValue.trim()) {
                    $('#BodyText1').val($('#BodyText1').val() + '<<' + sBodyValue + '>>');
                }
            }
        }
        // Pradyumna 10/10/2014 MITS 36930 - End
    </script>
</head>
<body onload="MergeMailDetailLoad();CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />              
            </td>
        </tr>
    </table>
    
     <asp:TextBox Style="display: none" runat="server" ID="hTabName"></asp:TextBox>
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
            <asp:ImageButton runat="server" src="../../Images/tb_save_active.png" Width="28"
                Height="28" border="0" ID="save" AlternateText="Save" onmouseover="this.src='../../Images/tb_save_mo.png';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/tb_save_active.png';this.style.zoom='100%'"
                OnClick="btnOk_Click" OnClientClick="return OnMergeEmailDetailSave();" /></div>
    </div>
    <div class="msgheader" id="div_formtitle">
        <asp:Label runat="server" ID="formtitle" Text="<%$ Resources:lblWordEmailHeadResrc %>" />
    </div>
    <br />
    

    <asp:TextBox Style="display: none" runat="server" ID="txtRowId" RMXRef="/Instance/Document/MergeEmailDetail/control[@name ='MergeEmailDetailID']"/>
    <asp:TextBox Style="display: none" runat="server" ID="txtSelectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="txtReloadNA" Text="" />
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <asp:TextBox Style="display: none" runat="server" ID="FormMode" />

     <table border="0" style="width: 98%">
      <tr>
            <td>
                <div class="tabGroup" id="TabsDivGroup" runat="server">
                    <div class="Selected" nowrap="true" runat="server" name="TABSTemplate" id="TABSTemplate">
                        <a class="Selected" href="#" onclick="tabChange(this.name);return false;" runat="server"
                            rmxref="" name="Template" id="LINKTABSTemplate" ><asp:Label runat ="server" Text = "<%$ Resources:lblEmailTemplResrc %>" /></a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSTemplate">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSEmailSubject"
                        id="TABSEmailSubject">
                        <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                            rmxref="" name="EmailSubject" id="LINKTABSEmailSubject"><asp:Label runat ="server" Text = "<%$ Resources:lblEmailSubjLResrc %>" /></a>
                    </div>                    
                    <div class="tabSpace" runat="server" id="TBSEmailSubject">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSEmailBody"  id="TABSEmailBody">
                        <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                            rmxref="" name="EmailBody" id="LINKTABSEmailBody"><asp:Label runat ="server" Text = "<%$ Resources:lblEmailBodyLResrc %>" /></a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSEmailBody">
                        <nbsp />
                        <nbsp />
                    </div>                   
                </div>
            </td>
        </tr>        
    </table>
    <div>
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABTemplate" id="FORMTABTemplate">
        <tr>
    <td colspan="3">
    <table width="100%" align="left">
              <tr>
               <td class="ctrlgroup" width="100%"><asp:Label ID="lblEmailTempl" runat="server" Text="<%$ Resources:lblEmailTemplResrc %>"></asp:Label></td>
              </tr>
             </table>
              </td>
    </tr>
         <tr>
        <td class= "required">
	    <asp:Label runat="server" class="label" ID="lblTemplate" Text="<%$ Resources:lblTemplateResrc %>" Width = "120"/>
        </td>
        <td>
	    <asp:DropDownList runat="server" id="Template" 
                  rmxref="/Instance/Document/MergeEmailDetail/control[@name ='Template']" onChange="return ExistInMergeGrid();" ></asp:DropDownList>
        </td>       
    </tr>
    <tr>
    <td>
        <asp:label runat="server" class="label" id="lblEmailFrom" Text="<%$ Resources:lblEmailFromResrc %>" Width = "120"/>
        </td>
        <td>
        <span>
            <asp:TextBox runat="server" id="EmailFrom" MaxLength="100" RMXRef="/Instance/Document/MergeEmailDetail/control[@name ='EmailFrom']" RMXType="" onChange="setDataChanged(true);validateEmail();" />
        </span>
    </td>
    </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0" name="FORMTABEmailSubject" id="FORMTABEmailSubject">
    <tr>
    <td colspan="3">
    <table width="100%" align="left">
              <tr>
               <td class="ctrlgroup" width="100%"><asp:Label ID="Label1" runat="server" Text="<%$ Resources:lblEmailSubjResrc %>"></asp:Label></td>
              </tr>
             </table>
              </td>
    </tr>
    <%-- Pradyumna 10/10/2014 MITS 36930 - Added new controls and Commented old controls - Start --%>
     <%--<tr>
    <td class= "required">
        <asp:label runat="server" class="label" id="lblSubText1" Text="<%$ Resources:lblSubjText1Resrc %>" Width = "120"/>
        </td>
        <td>
        <span>
            <asp:TextBox runat="server" id="SubText1" MaxLength="100" RMXRef="/Instance/Document/MergeEmailDetail/control[@name ='SubText1']" RMXType="" onChange="setDataChanged(true);" />
        </span>
    </td>
    <td></td>
    </tr>--%>
    <tr>
        <td>
	    <asp:Label runat="server" class="label" ID="lblSubVar1" Text="<%$ Resources:lblVarResrc %>" Width = "120"/>
        </td>
        <td>
	    <asp:DropDownList runat="server" id="SubVar1" 
                  rmxref="/Instance/Document/MergeEmailDetail/control[@name ='SubVar1']" ></asp:DropDownList>
        <asp:Button runat="server" type="button" class="button" Text="<%$ Resources:btnAdd %>" name="btnAddToSub" tabindex="8" id="btnAddToSub" OnClientClick="AddSubVariable();"/>
        </td>
       <td></td>
    </tr>
    <tr>
    <td class= "required">
        <asp:label runat="server" class="label" id="lblSubText1" Text="<%$ Resources:lblSubjTextResrc %>" Width = "120"/>
        </td>
        <td>
        <span>
            <asp:TextBox runat="server" id="SubText1" Rows="4" TextMode="MultiLine" width="300px" RMXRef="/Instance/Document/MergeEmailDetail/control[@name ='SubText1']" RMXType="" onChange="setDataChanged(true);"  />
        </span>
    </td>
    <td></td>
    </tr>
<%--    <tr>
    <td>
        <asp:label runat="server" class="label" id="lblSubText2" Text="<%$ Resources:lblSubjText2Resrc %>" Width = "120"/>
        </td>
        <td>
        <span>
            <asp:TextBox runat="server" id="SubText2" MaxLength="100" RMXRef="/Instance/Document/MergeEmailDetail/control[@name ='SubText2']" RMXType="" onChange="setDataChanged(true);" />
        </span>
    </td>
    <td></td>
    </tr>
    <tr>
        <td>
	    <asp:Label runat="server" class="label" ID="lblSubVar2" Text="<%$ Resources:lblSubjVar2Resrc %>" Width = "120"/>
        </td>
        <td>
	    <asp:DropDownList runat="server" id="SubVar2" 
                  rmxref="/Instance/Document/MergeEmailDetail/control[@name ='SubVar2']" ></asp:DropDownList>
        </td>
       <td>
        
       </td>
    </tr>
    <tr>
    <td>&nbsp;
    </td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>
    </td>
     <td>
   <asp:Button runat="server"  type="button" class="button" Text="<%$ Resources:btnPreview %>" name="btnSubPreview" tabindex="8" id="btnSubPreview" OnClientClick="SubPreview();" />--%>
     <%-- <input type="button" runat="server" class="button" value="<%$ Resources:btnPreview %>" name="btnSubPreview" tabindex="8" id="btnSubPreview" onClick="SubPreview();" />--%>
    <%--</td>
    </tr>--%>
    <%-- Pradyumna 10/10/2014 MITS 36930 - Added new controls and Commented few controls - End --%>
    </table>
   <table border="0" cellspacing="0" cellpadding="0" name="FORMTABEmailBody" id="FORMTABEmailBody">
    <tr>
    <td colspan="3">
        <table width="100%" align="left">
              <tr>
               <td class="ctrlgroup" width="100%"><asp:Label ID="Label2" runat="server" Text="<%$ Resources:lblEmailBodyResrc %>"></asp:Label></td>
              </tr>
             </table>
             </td>
    </tr>
       <%--Pradyumna 10/10/2014 MITS 36930 - Added new controls and Commented old controls - Start--%>
<%--     <tr>
    <td class= "required">
        <asp:label runat="server" class="label" id="lblBodyText1" Text="<%$ Resources:lblBodyText1Resrc %>" Width = "120"/>
        </td>
        <td>
        <!--<span class="formw">
          <asp:TextBox runat="Server" id="BodyText1" RMXRef="/Instance/Document/AdjusterScreens/control[@name ='BodyText1']" RMXType="memo" readonly="true" 
          style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="7" TextMode="MultiLine" Columns="30" rows="5" />
           <input type="button" class="button" value="..." name="BodyText1_btnMemo" tabindex="8" id="BodyText1_btnMemo"
                    onclick="EditHTMLMemo('BodyText1','yes')" />
           <asp:textbox style="display: none" runat="server" rmxref="/Instance/Document/AdjusterScreens/control[@name ='BodyText1_HTMLComments']"
                        id="BodyText1_HTML" />
          </span>  -->    
          <span>
            <asp:TextBox runat="server" id="BodyText1" MaxLength="250" Columns="20" Rows="3" TextMode="MultiLine" RMXRef="/Instance/Document/MergeEmailDetail/control[@name ='BodyText1']" RMXType="" onChange="setDataChanged(true);" />
        </span>  
    </td>
    <td></td>
    </tr>--%>
    <tr>
        <td>
	    <asp:Label runat="server" class="label" ID="lblBodyVar1" Text="<%$ Resources:lblVarResrc %>" Width = "120"/>
        </td>
        <td>
	    <asp:DropDownList runat="server" id="BodyVar1" 
                  rmxref="/Instance/Document/MergeEmailDetail/control[@name ='BodyVar1']" ></asp:DropDownList>
        <asp:Button runat="server" type="button" class="button" Text="<%$ Resources:btnAdd %>" name="btnAddToBody" tabindex="8" id="btnAddToBody" OnClientClick="AddBodyVariable();"/>
        </td>
       <td></td>
    </tr>
    <tr>
    <td class= "required">
        <asp:label runat="server" class="label" id="lblBodyText1" Text="<%$ Resources:lblBodyTextResrc %>" Width = "120"/>
        </td>
        <td>  
          <span>
            <asp:TextBox runat="server" id="BodyText1" Columns="50" Rows="18" TextMode="MultiLine" RMXRef="/Instance/Document/MergeEmailDetail/control[@name ='BodyText1']" RMXType="" onChange="setDataChanged(true);" />
        </span>  
    </td>
    <td></td>
    </tr>
    <%--<tr>
    <td>
        <asp:label runat="server" class="label" id="lblBodyText2" Text="<%$ Resources:lblBodyText2Resrc %>" Width = "120"/>
        </td>
        <td>
        <span>
            <asp:TextBox runat="server" id="BodyText2" MaxLength="250" Columns="20" Rows="3" TextMode="MultiLine" RMXRef="/Instance/Document/MergeEmailDetail/control[@name ='BodyText2']" RMXType="" onChange="setDataChanged(true);" />
        </span>
    </td>
    <td></td>
    </tr>
    <tr>
        <td>
	    <asp:Label runat="server" class="label" ID="lblBodyVar2" Text="<%$ Resources:lblBodyVar2Resrc %>" Width = "120"/>
        </td>
        <td>
	    <asp:DropDownList runat="server" id="BodyVar2" 
                  rmxref="/Instance/Document/MergeEmailDetail/control[@name ='BodyVar2']" ></asp:DropDownList>
        </td>
       <td>   
       </td>
    </tr>
    <tr>
    <td>
        <asp:label runat="server" class="label" id="lblBodyText3" Text="<%$ Resources:lblBodyText3Resrc %>" Width = "120"/>
        </td>
        <td>
        <span>
            <asp:TextBox runat="server" id="BodyText3" MaxLength="250" Columns="20" Rows="3" TextMode="MultiLine" RMXRef="/Instance/Document/MergeEmailDetail/control[@name ='BodyText3']" RMXType="" onChange="setDataChanged(true);" />
        </span>
    </td>
    <td>    
    </td>
    </tr>
    <tr>
    <td>&nbsp;
    </td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>    
    </td>
    <td>
    <asp:Button runat="server" type="button" class="button" Text="<%$ Resources:btnPreview %>" name="btnBodyPreview" tabindex="8" id="btnBodyPreview" OnClientClick="BodyPreview();"/>
    </td>
    </tr>--%>
       <%--Pradyumna 10/10/2014 MITS 36930 - Added new controls and Commented old controls - End--%>
    <%--<tr>
    <td>         
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return OnMergeEmailDetailSave();"
                OnClick="btnOk_Click" />
     </td>
     <td>
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return MergeEmailDetail_onCancel();" OnClick="btnCancel_Click" />
      </td>
    </tr>--%>
    </table>

    <asp:TextBox Style="display: none" runat="server" ID="HdnTabPress" Text="" />
        <asp:TextBox Style="display: none" runat="server" ID="HdnTab" Text="" />
        <asp:TextBox Style="display: none" runat="server" ID="HdnTempIdEdit" Text="" />
    </div>
    </form>
</body>
</html>
