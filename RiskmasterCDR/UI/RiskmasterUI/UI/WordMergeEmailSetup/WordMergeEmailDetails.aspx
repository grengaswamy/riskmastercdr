﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WordMergeEmailDetails.aspx.cs" Inherits="Riskmaster.UI.WordMergeEmailSetup.WordMergeEmailDetails" ValidateRequest="false"%>
<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="dgt" TagName="UserControlDataGridTelerik" Src="~/UI/Shared/Controls/UserControlDataGridTelerik.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove"
xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Word Merge Email Details</title>
    <style type="text/css">
        .SelectedItem
        {
            background: none repeat scroll 0 0 #6699FF !important;
        }
        
        .GridFilterRow_Telerik 
        {
            font: normal 8pt Verdana, Arial, Sans-serif;
            height: 18px;
            background: green;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }</script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <div>
        <uc1:errorcontrol id="ErrorControl1" runat="server" />
        <div>
            <span>
                <asp:TextBox id="MergeEmailDetailGrid_OtherParams" style="display: none" runat="server"
                    Text="" />
                <%--Pradyumna 11/17/2014 MITS 36930: Added properties of allow Column resize and Column width in percentage--%>
                <dgt:usercontroldatagridtelerik runat="server" id="MergeEmailDetailGrid" gridname="MergeEmailDetailGrid"
                    gridtitle="Word Merge Email Details" target="/Document/MergeEmailDetailList" ref="/Instance/Document/form//control[@name='MergeEmailDetailGrid']"
                    unique_id="RowId" showradiobutton="true" width="" height="100%" hidenodes="|RowId|"
                    showheader="True" linkcolumn="" popupwidth="500" popupheight="510" type="GridAndButtons"
                    rowdataparam="listrow" AllowColumnResize="True" ColumnWidhtPercent="4||||||30||30"/>
            </span>
        </div>
        <asp:TextBox style="display: none" runat="server" id="MergeEmailDetailSelectedId"
            RMXType="id" />
        <asp:TextBox style="display: none" runat="server" id="MergeEmailDetailGrid_RowDeletedFlag"
            RMXType="id" Text="false" />
        <asp:TextBox style="display: none" runat="server" id="MergeEmailDetailGrid_Action"
            RMXType="id" />
        <asp:TextBox style="display: none" runat="server" id="MergeEmailDetailGrid_RowAddedFlag"
            RMXType="id" Text="false" />
            <asp:TextBox Style="display: none" runat="server" ID="HdnFormIdInGrid" Text="" />
        <asp:HiddenField id="hdnEmailSub1"  value="<%$ Resources:lblEmailSub1Resrc %>" runat="server"/>
    </div>
    </form>
</body>
</html>
