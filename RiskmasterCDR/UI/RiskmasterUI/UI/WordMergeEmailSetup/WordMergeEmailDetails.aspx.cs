﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
//Added by Swati Agarawl
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.WordMergeEmailSetup
{
    public partial class WordMergeEmailDetails : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        XmlDocument XmlResponse = new XmlDocument();
        XmlNodeList XmlHeadList = null;
        string sCWSresponse = "";
        protected void Page_Init(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// event handler for the Page load event
        /// Author : Sonam Pahariya
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            TextBox tControl = null;
            XmlDocument XmlResponse = new XmlDocument();
            XmlNodeList XmlRowList = null;            
            string sCWSresponse = "";
            try
            {
                //Swati Agarawl ML Changes
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), "WordMergeEmailDetailsValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "WordMergeEmailDetailsValidations", sValidationResources, true);
                //Swati Agarwal ML Changes
                if (MergeEmailDetailGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = MergeEmailDetailSelectedId.Text;
                    XmlTemplate = GetMessageTemplate(selectedRowId);
                    CallCWS("MailMergeAdaptor.DeleteWordMergeEmailDetail", XmlTemplate, out sCWSresponse, false, false);
                    MergeEmailDetailGrid_RowDeletedFlag.Text = "false";
                }

                XmlTemplate = GetMessageTemplate();
                CallCWSFunctionBind("MailMergeAdaptor.GetWordMergeEmailDetailList", out sCWSresponse, XmlTemplate);
                XmlResponse.LoadXml(sCWSresponse);
                
                XmlRowList = XmlResponse.SelectNodes("//listrow/FormName");
                foreach (XmlNode XmlElm in XmlRowList)
                {
                    if(string.IsNullOrEmpty(HdnFormIdInGrid.Text))
                        HdnFormIdInGrid.Text = ((XmlElement)XmlElm).GetAttribute("FormId");
                    else
                        HdnFormIdInGrid.Text = HdnFormIdInGrid.Text + "," + ((XmlElement)XmlElm).GetAttribute("FormId");
                }               
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                XmlTemplate = null;
            }
        }

        /// <summary>
        /// Method to generate the XML template for Merge Email Detail screen
        /// Author : Sonam Pahariya
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            string item = string.Empty;
            try
            {
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document>");
                sXml = sXml.Append("<MergeEmailDetailList><listhead>");
                //Changed by Swati Agarwal
                item = RMXResourceProvider.GetSpecificObject("lblRowID", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                sXml = sXml.Append("<RowId>");
                sXml = sXml.Append(item);
                sXml = sXml.Append("</RowId>");

                item = RMXResourceProvider.GetSpecificObject("lblTemplate", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                sXml = sXml.Append("<FormName>");
                sXml = sXml.Append(item);
                sXml = sXml.Append("</FormName>");
                
                item = RMXResourceProvider.GetSpecificObject("lblEmlAddress", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                sXml = sXml.Append("<From>");
                sXml = sXml.Append(item);
                sXml = sXml.Append("</From>");

                // Pradyumna 11/05/2014 MITS 36930 - Modified Template to modify Grid Display - Start
                item = RMXResourceProvider.GetSpecificObject("lblEmlSubTxt", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                sXml = sXml.Append("<SubText>");
                sXml = sXml.Append(item);
                sXml = sXml.Append("</SubText>");

                item = RMXResourceProvider.GetSpecificObject("lblEmlBdyTxt", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                sXml = sXml.Append("<BodyText>");
                sXml = sXml.Append(item);
                sXml = sXml.Append("</BodyText>");

                //item = RMXResourceProvider.GetSpecificObject("lblEmailSub1Resrc", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //sXml = sXml.Append("<SubText1>");
                //sXml = sXml.Append(item);
                //sXml =sXml.Append("</SubText1>");


                //item = RMXResourceProvider.GetSpecificObject("lblEmlSubVar1", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //sXml = sXml.Append("<SubVar1>");
                //sXml = sXml.Append(item);
                //sXml =sXml.Append("</SubVar1>");

                //item = RMXResourceProvider.GetSpecificObject("lblEmlSubText2", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //sXml = sXml.Append("<SubText2>");
                //sXml = sXml.Append(item);
                //sXml =sXml.Append("</SubText2>");

                //item = RMXResourceProvider.GetSpecificObject("lblEmlSubVar2", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //sXml = sXml.Append("<SubVar2>");
                //sXml = sXml.Append(item);
                //sXml = sXml.Append("</SubVar2>");

                //item = RMXResourceProvider.GetSpecificObject("lblEmlBdtTxt1", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //sXml = sXml.Append("<BodyText1>");
                //sXml = sXml.Append(item);
                //sXml = sXml.Append("</BodyText1>");


                //item = RMXResourceProvider.GetSpecificObject("lblEmlBdtVar1", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //sXml = sXml.Append("<BodyVar1>");
                //sXml = sXml.Append(item);
                //sXml = sXml.Append("</BodyVar1>");

                //item = RMXResourceProvider.GetSpecificObject("lblEmlBdtTxt2", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //sXml = sXml.Append("<BodyText2>");
                //sXml = sXml.Append(item);
                //sXml = sXml.Append("</BodyText2>");

                //item = RMXResourceProvider.GetSpecificObject("lblEmlBdtVar2", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //sXml = sXml.Append("<BodyVar2>");
                //sXml = sXml.Append(item);
                //sXml = sXml.Append("</BodyVar2>");

                //item = RMXResourceProvider.GetSpecificObject("lblEmlBdtTxt3", RMXResourceProvider.PageId("WordMergeEmailDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //sXml = sXml.Append("<BodyText3>");
                //sXml = sXml.Append(item);
                //sXml = sXml.Append("</BodyText3>");
                //Change end here
                // Pradyumna 11/05/2014 MITS 36930 - Modified Template to modify Grid Display - End
                sXml = sXml.Append("</listhead></MergeEmailDetailList>");
                sXml = sXml.Append("</Document></Message>");
                XElement oElement = XElement.Parse(sXml.ToString());
                return oElement;
            }
            finally
            {
                sXml = null;
            }

        }

        /// <summary>
        /// Method to generate the XML template for the selected record.
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="strSelectedRowId">selected record id</param>
        /// <returns></returns>
        private XElement GetMessageTemplate(string strSelectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            try
            {
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document><MergeEmailDetail>");
                sXml = sXml.Append("<control name='MergeEmailDetailID'>");
                sXml = sXml.Append(strSelectedRowId);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("</MergeEmailDetail></Document>");
                sXml = sXml.Append("</Message>");
                XElement oElement = XElement.Parse(sXml.ToString());
                return oElement;
            }
            finally
            {
                sXml = null;
            }
        }
    }
}