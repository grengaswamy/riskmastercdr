﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for DiaryListCurrentAction
/// </summary>
/// 
namespace Riskmaster.UI.Diaries
{
    public class DiaryListCurrentAction
    {
        public DiaryListCurrentAction()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string Activediarychecked
        {
            get;
            set;
        }

        //aanandpraka2:Start changes for MITS 27074
        public string Claimactivediarychecked
        {
            get;
            set;
        }
        public string Diarysource
        {
            get;
            set;
        }
        //aanandpraka2:End changes
        public string Attachrecordid
        {
            get;
            set;
        }

        public string Attachtable
        {
            get;
            set;
        }

        public string Diaryfunction
        {
            get;
            set;
        }

        public string Duedate
        {
            get;
            set;
        }

        public string Open
        {
            get;
            set;
        }

        public string Pagecount
        {
            get;
            set;
        }

        public string Pagenumber
        {
            get;
            set;
        }

        public string Pagesize
        {
            get;
            set;
        }

        public string Recordcount
        {
            get;
            set;
        }

        public string Reqactdiary
        {
            get;
            set;
        }

        public string Showallclaimdiaries
        {
            get;
            set;
        }


        public string Showalldiariesforactiverecord
        {
            get;
            set;
        }

        public string Shownotes
        {
            get;
            set;
        }

        public string Sortorder
        {
            get;
            set;
        }
        //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
        public string Usersortorder
        {
            get;
            set;
        }
        public string  User
        {
            get;
            set;
        }
        //Neha Mits 23586 05/16/2011--aaply filter on the page
        public string TaskNameFilter
        {
            get;
            set;
        }
        public string TaskNameText
        {
            get;
            set;
        
        }

        //Added by Amitosh for Terelik Grid
        public string Showregarding
        {
            get;
            set;
        }
        //End Amitosh
        //Added by Amitosh for Terelik Grid
        public string FilterExpression
        {
            get;
            set;
        }

        //End Amitosh
        //MITS 36519
        public string SortExpression
        {
            get;
            set;
        }
    }
}
