﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class VoidDiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("VoidDiary.aspx"), "VoidDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "VoidDiaryValidations", sValidationResources, true);
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);

                   
                }

                if (!IsPostBack)
                {
                    if (Request.QueryString["entryid"] != null)
                    {
                        ViewState["entryid"] = Request.QueryString["entryid"];
                    }
                    else
                    {
                        ViewState["entryid"] = "";
                    }

                    if (Request.QueryString["assingneduser"] != null)
                    {
                        ViewState["assingneduser"] = Request.QueryString["assingneduser"];
                    }
                    else
                    {
                        ViewState["assingneduser"] = "";
                    }

                    if (Request.QueryString["assigneduser"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }

                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";//"EVENT";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";// "1";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }

                    if (Request.QueryString["completedate"] != null)
                    {
                      //  txtFromDate.Value = Request.QueryString["completedate"];
                      //  txtToDate.Value = Request.QueryString["completedate"];
                        //ML changes
                        txtFromDate.Value = AppHelper.GetDate(Request.QueryString["completedate"]);
                        txtToDate.Value = AppHelper.GetDate(Request.QueryString["completedate"]);
                        //ML changes
                    }
                    else
                    {
                        txtFromDate.Value = "";
                        txtToDate.Value = "";
                    }

                    //Start: nsureshjain: 10/10/2012; MITS 29791 BRD 5.1.18; do not display task name
                    //if (Request.QueryString["taskname"] != null)
                    //{
                    //    lblVoidSelectedDiary.Text = "Void the Selected Diary (" + Request.QueryString["taskname"] + ")";
                    //}

                    lblVoidSelectedDiary.Text = "Void the Selected Diary(s)";
                    //End:MITS 29791                    
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlNode GetInputDocForVoidDiary()
        {
            XmlDocument voidDiaryDoc = null;
            XmlNode voidDiaryNode = null;
            XmlAttribute atbAttachRecordId = null;
            XmlAttribute atbAttachTable = null;
            XmlAttribute atbAssignedUser = null;
            XmlAttribute atbDiaryId = null;
            XmlAttribute atbFromDate = null;
            XmlAttribute atbToDate = null;

            voidDiaryDoc = new XmlDocument();

            voidDiaryNode = voidDiaryDoc.CreateElement("VoidDiary");

            atbAttachRecordId = voidDiaryDoc.CreateAttribute("AttachRecordId");
            atbAttachRecordId.Value = "";
            voidDiaryNode.Attributes.Append(atbAttachRecordId);

            atbAttachTable = voidDiaryDoc.CreateAttribute("AttachTable");
            atbAttachTable.Value = "";
            voidDiaryNode.Attributes.Append(atbAttachTable);

            atbAssignedUser = voidDiaryDoc.CreateAttribute("assignuser");
            atbAssignedUser.Value = ViewState["assingneduser"].ToString();
            voidDiaryNode.Attributes.Append(atbAssignedUser);

            atbDiaryId = voidDiaryDoc.CreateAttribute("diaryid");
            atbFromDate = voidDiaryDoc.CreateAttribute("fromdate");
            atbToDate = voidDiaryDoc.CreateAttribute("todate");
            
            if (chkVoidSelected.Checked == false)
            {
                atbFromDate.Value = txtFromDate.Value;
                atbToDate.Value = txtToDate.Value;
                atbDiaryId.Value = "0";
            }
            else
            {
                atbDiaryId.Value = ViewState["entryid"].ToString();
                atbFromDate.Value = "";
                atbToDate.Value = "";
            }

            voidDiaryNode.Attributes.Append(atbDiaryId);
            voidDiaryNode.Attributes.Append(atbFromDate);
            voidDiaryNode.Attributes.Append(atbToDate);

            voidDiaryDoc.AppendChild(voidDiaryNode);

            return voidDiaryNode;
        }

        private XmlDocument VoidCurrentDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "WPAAdaptor.VoidDiary";

                inputDocNode = GetInputDocForVoidDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        protected void btnVoid_Click(object sender, EventArgs e)
        {
            XmlDocument responseDoc = null;

            try
            {
                responseDoc = VoidCurrentDiary();

                if (responseDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                {
                    Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
                }
                else
                {
                    throw new ApplicationException(responseDoc.InnerXml);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
        }
    
    }
}
