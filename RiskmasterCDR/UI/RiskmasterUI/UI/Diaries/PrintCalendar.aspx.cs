﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.Globalization;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class PrintCalendar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string assignedUser = string.Empty;
            string dateRange = string.Empty;
            string diaryStyle = string.Empty;
            string fromDate = string.Empty;
            string selectedMonth = string.Empty;
            string selectedYear = string.Empty;
            string sortBy = string.Empty;
            string sortOrder = string.Empty;
            string toDate = string.Empty;

            XmlDocument diaryCalendarDoc = null;
            XmlNode inputNodeForPrint = null;
            string htmlScriptForDiaryList = string.Empty;

           try
           {
               if (!IsPostBack)
               {
                   if (Request.QueryString["assingneduser"] != null)
                   {
                       //Pentesting - XSS error fix - srajindersin - MITS 27822
                       //assignedUser = Request.QueryString["assingneduser"];
                       assignedUser = AppHelper.HTMLCustomEncode(Request.QueryString["assingneduser"]);
                       //END Pentesting - XSS error fix - srajindersin - MITS 27822
                   }

                   if (Request.QueryString["daterange"] != null)
                   {
                       dateRange = Request.QueryString["daterange"];
                   }

                   if (Request.QueryString["diarystyle"] != null)
                   {
                       diaryStyle = Request.QueryString["diarystyle"];
                   }

                   if (Request.QueryString["fromdate"] != null)
                   {
                       fromDate = Request.QueryString["fromdate"];
                   }

                   if (Request.QueryString["selectedmonth"] != null)
                   {
                       selectedMonth = Request.QueryString["selectedmonth"];
                   }

                   if (Request.QueryString["selectedyear"] != null)
                   {
                       selectedYear = Request.QueryString["selectedyear"];
                   }

                   if (Request.QueryString["sortby"] != null)
                   {
                       sortBy = Request.QueryString["sortby"];
                   }

                   if (Request.QueryString["sortorder"] != null)
                   {
                       sortOrder = Request.QueryString["sortorder"];
                   }

                   if (Request.QueryString["todate"] != null)
                   {
                       toDate = Request.QueryString["todate"];
                   }

                   inputNodeForPrint = InputDocForPirntCalendar(assignedUser, dateRange, diaryStyle, fromDate, selectedMonth, selectedYear,
                                                 sortBy, sortOrder, toDate);


                   diaryCalendarDoc = GetDiaryListDocForPrint(inputNodeForPrint);
                  
                   htmlScriptForDiaryList = GetHtmlScriptForDiaryList(diaryCalendarDoc, assignedUser);

                   diaryCalendarDiv.InnerHtml = htmlScriptForDiaryList;
               }   
            }
           catch (Exception ee)
           {
               ErrorHelper.logErrors(ee);
               BusinessAdaptorErrors err = new BusinessAdaptorErrors();
               err.Add(ee, BusinessAdaptorErrorType.SystemError);
               ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
           }
        }


        private XmlNode InputDocForPirntCalendar(string assignedUser, string dateRange, string diaryStyle, string fromDate,
            string selectedMonth,string selectedYear, string sortBy, string sortOrder, string toDate)
        {
            XmlDocument printCalendarDoc = null;
            XmlNode diaryCalendarNode = null;

            XmlAttribute atbAssignedUser = null;
            XmlAttribute atbDateRange = null;
            XmlAttribute atbDiaryStyle = null;
            XmlAttribute atbFromDate = null;
            XmlAttribute atbSelectedMonth = null;
            XmlAttribute atbselectedYear = null;
            XmlAttribute atbSortBy= null;
            XmlAttribute atbSortOrder = null;
            XmlAttribute atbToDate = null;
            XmlAttribute LangCode = null;
            XmlAttribute PageID = null;

            printCalendarDoc = new XmlDocument();

            diaryCalendarNode = printCalendarDoc.CreateElement("DiaryCalendar");

            atbAssignedUser = printCalendarDoc.CreateAttribute("assigneduser");
            atbAssignedUser.Value = assignedUser;
            diaryCalendarNode.Attributes.Append(atbAssignedUser);

            atbDateRange = printCalendarDoc.CreateAttribute("daterange");
            atbDateRange.Value = dateRange;
            diaryCalendarNode.Attributes.Append(atbDateRange);

            atbDiaryStyle = printCalendarDoc.CreateAttribute("diarystyle");
            atbDiaryStyle.Value = diaryStyle;
            diaryCalendarNode.Attributes.Append(atbDiaryStyle);

            atbFromDate = printCalendarDoc.CreateAttribute("fromdate");
            if (dateRange != "2") 
            {
                if (dateRange == "4") //month radio is selected then fromdate will blank
                {
                    atbFromDate.Value = "";
                }
                else
                {   //in case of daterange 0 and 1, fromdate will be current date
                    atbFromDate.Value = System.DateTime.Now.ToShortDateString();
                }
            }
            else
            {
                atbFromDate.Value = fromDate;
            }

            diaryCalendarNode.Attributes.Append(atbFromDate);

            atbSelectedMonth = printCalendarDoc.CreateAttribute("selectedmonth");
            atbSelectedMonth.Value = selectedMonth;
            diaryCalendarNode.Attributes.Append(atbSelectedMonth);

            atbselectedYear = printCalendarDoc.CreateAttribute("selectedyear");
            atbselectedYear.Value = selectedYear;
            diaryCalendarNode.Attributes.Append(atbselectedYear);

            atbSortBy = printCalendarDoc.CreateAttribute("sortby");
            atbSortBy.Value = sortBy;
            diaryCalendarNode.Attributes.Append(atbSortBy);

            atbSortOrder = printCalendarDoc.CreateAttribute("sortorder");
            atbSortOrder.Value = sortOrder;
            diaryCalendarNode.Attributes.Append(atbSortOrder);

            atbToDate = printCalendarDoc.CreateAttribute("todate");

            if (dateRange != "2")
            {
                if (dateRange == "4")
                {
                    atbToDate.Value = "";
                }
                else
                {
                    atbToDate.Value = System.DateTime.Now.ToShortDateString();
                }
            }
            else
            {
                atbToDate.Value = toDate;
            }
            
            diaryCalendarNode.Attributes.Append(atbToDate);

            LangCode = printCalendarDoc.CreateAttribute("LangCode");
            LangCode.Value = AppHelper.GetLanguageCode();
            diaryCalendarNode.Attributes.Append(LangCode);

            PageID = printCalendarDoc.CreateAttribute("PageID");
            PageID.Value = RMXResourceProvider.PageId("CreateDiary.aspx");
            diaryCalendarNode.Attributes.Append(PageID);

            printCalendarDoc.AppendChild(diaryCalendarNode);
            return diaryCalendarNode;
        }

        private string GetHtmlScriptForDiaryList(XmlDocument diaryListDoc, string assignedUser)
        {
            XmlNodeList diaryCalendarNodeList = null;
            XmlNodeList diaryNodeList  = null;

            System.Text.StringBuilder htmlScriptBuilder = null;

            diaryCalendarNodeList = diaryListDoc.SelectNodes("ResultMessage/Document/Diaries/DiaryCalendar");

            htmlScriptBuilder = new System.Text.StringBuilder();

            htmlScriptBuilder.Append("<table width='100%' border='0' cellspacing='0' cellpadding='4'>");

            foreach (XmlNode diaryCalenderNode in diaryCalendarNodeList)
            {
                htmlScriptBuilder.Append("<tr width='100%' class='ctrlgroup'>");
                htmlScriptBuilder.Append("<td  border='1' align='left' colspan='3'><h2>" + diaryCalenderNode.Attributes["Date"].Value + "</h2></td>");
                htmlScriptBuilder.Append("<td  align='center' width='100%' colspan='2'>");
                htmlScriptBuilder.Append(GetResourceValue("lblDiaryEntry","0") + assignedUser + "</td>"); //tmalhotra3-Changes for MITS 31130
                htmlScriptBuilder.Append("</tr>");

                htmlScriptBuilder.Append("<tr>");
                htmlScriptBuilder.Append("<td  width='10%'><B><U>");
                htmlScriptBuilder.Append(GetResourceValue("lblPriority","0")); 
                htmlScriptBuilder.Append("</U></B></td>");
                htmlScriptBuilder.Append("<td  width='10%'><B><U>");
                htmlScriptBuilder.Append(GetResourceValue("lblTimeDue","0")); 
                htmlScriptBuilder.Append("</U></B></td>"); 
                htmlScriptBuilder.Append("<td  width='10%'><B><U>");
                htmlScriptBuilder.Append(GetResourceValue("lblTimeEst","0")); 
                htmlScriptBuilder.Append("</U></B></td>"); 
                htmlScriptBuilder.Append("<td  width='20%'><B><U>");
                htmlScriptBuilder.Append(GetResourceValue("lblTaskName","0")); 
                htmlScriptBuilder.Append("</U></B></td>"); 
                htmlScriptBuilder.Append("<td  width='50%'><B><U>");
                htmlScriptBuilder.Append(GetResourceValue("lblRegarding","0")); //tmalhotra3-End Changes
                htmlScriptBuilder.Append("</U></B></td>"); 
                htmlScriptBuilder.Append("</tr>");

                diaryNodeList = diaryCalenderNode.SelectNodes("Diary");

                foreach (XmlNode diaryNode in diaryNodeList)
                {
                    htmlScriptBuilder.Append("<tr>");
                    htmlScriptBuilder.Append("<td>" + diaryNode.Attributes["Priority"].Value + "</td>");
                    htmlScriptBuilder.Append("<td>" + diaryNode.Attributes["Time_Due"].Value + "</td>");
                    htmlScriptBuilder.Append("<td>" + diaryNode.Attributes["Time_Estimated"].Value + "</td>");
                    htmlScriptBuilder.Append("<td>" + diaryNode.Attributes["Task_Name"].Value + "</td>");
                    htmlScriptBuilder.Append("<td>" + diaryNode.Attributes["Regarding"].Value + "</td>");
                    htmlScriptBuilder.Append("</tr>");
                }

                
            }
                
            htmlScriptBuilder.Append("</table>");

            return htmlScriptBuilder.ToString();
        }

        private XmlDocument GetDiaryListDocForPrint(XmlNode inputDocNode)
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;

            string serviceMethodToCall = string.Empty;
            
            try
            {
                serviceMethodToCall = "WPAAdaptor.DiaryCalender";
                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        //tmalhotra3-MITS 31130
        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("PrintCalendar.aspx"), strResourceType).ToString();
        } 
        //tmalhotra3-End Changes
    }
}
