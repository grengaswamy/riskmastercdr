﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RouteDiary.aspx.cs" Inherits="Riskmaster.UI.Diaries.RouteDiary" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Route Diary</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/cul.js" type="text/javascript"></script>
    <script src="../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function pageLoaded()
        {

	        //Set focus to the first field
	        var i;

	        for (i=0;i<document.forms[0].length;i++)			
	        {	 				
		        if((document.forms[0].item(i).type=="text") || (document.forms[0].item(i).type=="select-one")|| (document.forms[0].item(i).type=="textarea"))
		        {
			        document.forms[0].item(i).focus();
			        break;
		        }
	        }
	        var currentdate = new Date();
	        //alert((currentdate.getMonth() + 1) + '/' + (currentdate.getDate()) + '/' + currentdate.getYear());
	        document.forms[0].currentdate.value = (currentdate.getMonth() + 1) + '/' + (currentdate.getDate()) + '/' + currentdate.getYear();
	        //alert(document.forms[0].currentdate.value);
        }
			
        function ValForm()
        {
            //if (document.forms[0].txtAssignedUser.value == "") {
                //alert("Please select the user to route diary to."); Rakhel ML Change
            if (document.forms[0].lstUsers.options.length < 1) {
                alert(RouteDiaryValidations.SelectUser);
                return false;
            }
            return true;
        }
    </script> 
</head>
<body>
    <form id="frmData" runat="server">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <p align="center">
        <table border="0" align="center">        
            <tr>
                 <td colspan="2" align="left">
                   <asp:ImageButton ID="btnRoute" ImageUrl="~/Images/tb_diaryroute_active.png" class="bold" ToolTip="<%$ Resources:ttRoute %>"
                        runat="server" OnClientClick="return ValForm();; " OnClick="btnRoute_Click" />
                   <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                        runat="server" OnClick="btnCancel_Click" />
                 </td>
            </tr>
            <input type="hidden" value="" id="currentdate">
            <tr>
                <td align="left" class="ctrlgroup" width="100%" colspan="2"> 
                 <asp:Label ID="lblRouteDiaryResc" runat="server" Text="<%$ Resources:lblRouteDiaryResc %>"></asp:Label> </td>
               
            </tr>
            <tr>
                <td align="right" valign="top">
                <asp:Label ID="lblRouteDiarytoResc" class="required" Font-Underline="true" runat="server" Text="<%$ Resources:lblRouteDiarytoResc %>"></asp:Label>
               </td>
                <td class="required" align="left">
                
                <!-- Changed by Gagan for MITS 19725-->
                    <%--<input type="text"  runat="server" value="" id="txtAssignedUser" readonly="readonly" />--%>
                    <%--sharishkumar Jira 6412--%>
                    <select multiple="true" id="lstUsers" size="3" runat="server" style="width: 190px; height: 71px"></select>
                    <input type="button" class="CodeLookupControl" value="" id="cmdAddCustomizedUser" onclick="AddCustomizedListUser('routediary', 'lstUsers', 'UserIdStr', 'UserStr')"/>        
                    <input type="button" class="BtnRemove" value="" id="cmdDelCustomizedListUser" style="width:21px" onclick="DelCustomizedListUser('routediary', 'lstUsers', 'UserIdStr', 'UserStr')"/>
                     <input type="hidden"  runat="server" name="" value="" id="UserIdStr"/>
                    <input type="hidden"  runat="server" value="" id="UserStr"/>
                    <input type="text"  runat="server" value="" id="hdnAssigneduser" style="display:none"/> 
                    <input type="hidden"  runat="server" value="" id="hdnUserStr"/>
                    <input type="hidden"  runat="server" value="" id="hdnGroupStr" />
                    <input type="text" runat="server" style="display:none" id="hdnDiaryLstGroups"/>
                    <input type="text" runat="server" style="display:none" id="hdnDiaryLstUsers"/>
                    <%--sharishkumar Jira 6412--%>   
                </td>
            </tr>
             <!--07/26/2011 SMISHRA54: WPA Email Notification-->
            <tr>
                <td align="left" colspan ="2">
                    <input type="checkbox" value="true" runat="server" id="chkEmailNotification" />
                       <asp:Label ID="lblNotifyEmailResc" class="required" Font-Underline="true" runat="server" Text="<%$ Resources:lblNotifyEmailResc %>"></asp:Label>
                </td>
            </tr>
            <!--07/26/2011 SMISHRA54: End-->
            <%--<tr>
                <td colspan="2" align="center">
                   <asp:Button ID="btnRoute"  runat="server" Text="Route" CssClass="button" 
                        OnClientClick = "return ValForm();;" onclick="btnRoute_Click" />
                   <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" 
                        onclick="btnCancel_Click" />
                </td>
            </tr>--%>
        </table>
    </p>
    
    </form>
</body>
</html>
