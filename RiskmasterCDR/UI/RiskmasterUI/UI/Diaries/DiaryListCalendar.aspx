﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiaryListCalendar.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryListCalendar" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Diary List Calendar</title>
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    
    <script src="../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    <script src="../../Scripts/drift.js" type="text/javascript"></script>
    <script src="../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
     <!--Rakhel ML Changes!-->
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
     <!--Rakhel ML Changes!-->

    
    <script language="JavaScript" type="text/javascript">
					function SetFocus()
					{
						//Set focus to first field
						var i; 
						for (i=0;i<document.forms[0].length;i++)			
						{	 				
							if((document.forms[0][i].type=="text") || (document.forms[0][i].type=="select-one") || (document.forms[0][i].type=="textarea"))
							{
								document.forms[0][i].focus();
								break;
							}
						}
					}
					
					function StoreSelection()
					{
						var objSelStyle;
			
						objSelStyle = eval("document.forms[0].selStyle");
						if (objSelStyle!=null)
						{
							for(var i=0;i<objSelStyle.length;i++)
							{
								if (objSelStyle[i].checked)
								document.forms[0].iDiaryStyle.value=i;
							}
				
							var tmpSortBy, tmpSortOrder;
				
							tmpSortBy="";
							tmpSortOrder="";
				
							if (document.forms[0].selSortBy1.value!=0)
							{
								tmpSortBy = document.forms[0].selSortBy1.value;
								tmpSortOrder = document.forms[0].selSortOrder1.value;
							}
				
							if (document.forms[0].selSortBy2.value!=0)
							{
								if (tmpSortBy.match(document.forms[0].selSortBy2.value))
								{
								    //alert("Duplicate sort order is not allowed.");
								    alert(DiaryListCalendarValidations.DupSortOrder);
									return false;
								}
								else
								{
									tmpSortBy=tmpSortBy + "," + document.forms[0].selSortBy2.value;
									tmpSortOrder = tmpSortOrder + "," + document.forms[0].selSortOrder2.value;
								}
							}
				
							if (document.forms[0].selSortBy3.value!=0)
							{
								if (tmpSortBy.match(document.forms[0].selSortBy3.value))
								{
								    //alert("Duplicate sort order is not allowed.");
								    alert(DiaryListCalendarValidations.DupSortOrder);
									return false;
								}
								else
								{
									tmpSortBy=tmpSortBy + "," + document.forms[0].selSortBy3.value;
									tmpSortOrder = tmpSortOrder + "," + document.forms[0].selSortOrder3.value;
								}
							}

							document.forms[0].sSortBy.value=tmpSortBy;
							document.forms[0].sSortOrder.value=tmpSortOrder;
							
							var objSelRange;
				
							objSelRange=eval("document.forms[0].selRange");
							if (objSelRange!=null)
							{
								for(var i=0;i<objSelRange.length;i++)
								{
									if (objSelRange[i].checked)
									document.forms[0].iDiaryRange.value=i;
								}
							}
				
							objSelRange=eval("document.forms[0].selMonthlyRange");
							if (objSelRange!=null)
							{
								if (objSelRange.checked)
								document.forms[0].iDiaryRange.value=4;
							}
				
							if(document.forms[0].iDiaryRange.value==2){  //specific date, must have both
								document.forms[0].sFromDate.value = getdbDate(document.forms[0].txtFromDate.value);
							document.forms[0].sToDate.value = getdbDate(document.forms[0].txtToDate.value);
							if(document.forms[0].sFromDate.value=='' || document.forms[0].sFromDate.value==null){
							    //alert("You must enter a 'From' date.");
							    alert(DiaryListCalendarValidations.FromDateValidation);
								document.forms[0].txtFromDate.focus;
								return false;
							}
							if(document.forms[0].sToDate.value=='' || document.forms[0].sToDate.value==null){
								//alert("You must enter a 'To' date.");
							    alert(DiaryListCalendarValidations.ToDateValidation);
								document.forms[0].txtToDate.focus;
								return false;
							}
						}
						
						
						if(document.forms[0].iDiaryRange.value == '4')
						{
						      document.forms[0].sMonthSelected.value = document.forms[0].selMonth.value;
						      document.forms[0].sYearSelected.value = document.forms[0].selYear.value;
						      
//						      document.forms[0].sFromDate.value = "";
//						      document.forms[0].sToDate.value = "";
						      
						      document.forms[0].txtToDate.value = "";
						      document.forms[0].txtFromDate.value = "";
						}
						
						//retaining sortorder for the refreshed page
//						document.forms[0].sSortFirst.value = document.forms[0].selSortBy1.value;
//						document.forms[0].sSortSecond.value = document.forms[0].selSortBy2.value;
//						document.forms[0].sSortThird.value = document.forms[0].selSortBy3.value;
//						
//						document.forms[0].sSortOrder1.value = document.forms[0].selSortOrder1.value;
//						document.forms[0].sSortOrder2.value = document.forms[0].selSortOrder2.value;
//						document.forms[0].sSortOrder3.value = document.forms[0].selSortOrder3.value;
						
						
						window.open('PrintCalendar.aspx?assingneduser=' + document.forms[0].hdnAssignedUser.value + '&daterange=' + document.forms[0].iDiaryRange.value + '&diarystyle=' + document.forms[0].iDiaryStyle.value + '&fromdate=' + document.forms[0].txtFromDate.value + '&selectedmonth=' + document.forms[0].sMonthSelected.value + '&selectedyear=' + document.forms[0].sYearSelected.value + '&sortby=' + document.forms[0].sSortBy.value + '&sortorder=' + document.forms[0].sSortOrder.value + '&todate=' + document.forms[0].txtToDate.value,'','','');
						
						    return false;
					    }
					    }
						
						function selectRange(iRange)
						{
							var bSpecificDate=(iRange==3);
							document.forms[0].txtFromDate.disabled=!bSpecificDate;
							//document.forms[0].btnfromdate.disabled=!bSpecificDate;
							document.forms[0].txtToDate.disabled=!bSpecificDate;
							//document.forms[0].btntodate.disabled = !bSpecificDate;
							if (!bSpecificDate) {
							    var btnfrom = $("#txtFromDate"); var btnto = $("#txtToDate");
							    $.datepicker._disabledInputs = [btnfrom[0], btnto[0]];
                            }
							else {
							    $.datepicker._disabledInputs = [];
                            }
						}
		
						function selectStyle(iStyle)
						{
							var bMonthly=false;
								document.forms[0].txtFromDate.disabled=true;
								//document.forms[0].btnfromdate.disabled=true;
								document.forms[0].txtToDate.disabled=true;
								//document.forms[0].btntodate.disabled=true;
								var btnfrom = $("#txtFromDate"); var btnto = $("#txtToDate");
								$.datepicker._disabledInputs = [btnfrom[0], btnto[0]];
			
							switch(iStyle)
							{	
								case 1:  //daily
								
								document.forms[0].selRange[0].disabled=false;
								document.forms[0].selRange[1].disabled=true;
								document.forms[0].selRange[2].disabled=false;
								document.forms[0].selRange[0].checked=true;
								
								break;
								
								case 2: //weekly
								document.forms[0].selRange[0].disabled=true;
								document.forms[0].selRange[1].disabled=false;
								document.forms[0].selRange[2].disabled=false;
								document.forms[0].selRange[1].checked=true;
								
								break;
				
								case 3: //monthly
								bMonthly=true;
								for(var i=0;i<=2;i++){
									document.forms[0].selRange[i].disabled=true;
									document.forms[0].selRange[i].checked=false;
								}
								break;
							}
						
							document.forms[0].selMonthlyRange.disabled=!bMonthly;
							document.forms[0].selMonthlyRange.checked=bMonthly;
							document.forms[0].selMonth.disabled=!bMonthly;
							document.forms[0].selYear.disabled=!bMonthly;
						}
						
						function selectStyle1(iStyle)
						{
							var bMonthly=false;
							if(document.forms[0].PrintClick.value=='')
							{
								document.forms[0].txtFromDate.disabled=true;
								//document.forms[0].btnfromdate.disabled=true;
								document.forms[0].txtToDate.disabled=true;
								//document.forms[0].btntodate.disabled = true;
								var btnfrom = $("#txtFromDate"); var btnto = $("#txtToDate");
								$.datepicker._disabledInputs = [btnfrom[0], btnto[0]];
							}
			
							switch(iStyle)
							{	
								case 1:  //daily
								
								document.forms[0].selRange1.disabled=false;
								document.forms[0].selRange2.disabled=true;
								document.forms[0].selRange3.disabled=false;
								
								break;
								
								case 2: //weekly
								document.forms[0].selRange1.disabled=true;
								document.forms[0].selRange2.disabled=false;
								document.forms[0].selRange3.disabled=false;
								
								break;
				
								case 3: //monthly
								bMonthly=true;
								for(var i=0;i<=2;i++){
									document.forms[0].selRange[i].disabled=true;
									document.forms[0].selRange[i].checked=false;
								}
								break;
							}
						
							document.forms[0].selMonthlyRange.disabled=!bMonthly;
							document.forms[0].selMonthlyRange.checked=bMonthly;
							document.forms[0].selMonth.disabled=!bMonthly;
							document.forms[0].selYear.disabled=!bMonthly;
							
							document.forms[0].selSortBy1.value = document.forms[0].sSortFirst.value;
							document.forms[0].selSortBy2.value = document.forms[0].sSortSecond.value;
							document.forms[0].selSortBy3.value = document.forms[0].sSortThird.value;
							
							document.forms[0].selSortOrder1.value = document.forms[0].sSortOrder1.value;
							document.forms[0].selSortOrder2.value = document.forms[0].sSortOrder2.value;
							document.forms[0].selSortOrder3.value = document.forms[0].sSortOrder3.value;
							
						}
					
					    function check()
						{
					        
							var iY, iY2, i;
							
							iY = document.forms[0].currentyear.value;
							var objCtrl;
							var objOption;
							var begin = iY - 20;
							var end = begin + 40;
							
							for (i=begin;i<=end;i++)
							{
								objCtrl=document.forms[0].selYear;
								if (i==iY)
								{	
									objOption =	new Option(i, i);
									objCtrl.options[objCtrl.length] = objOption;
									objCtrl.value = i
								}
								else
								{
									objOption =	new Option(i, i);
									objCtrl.options[objCtrl.length] = objOption;
								}
								objCtrl = null;
								
							}
                            //Added:Yukti, DT:07/15/2014, MITS 34473
							//MGaba2: MITS 14638:Default value should be the current month
					        //var objCurMonth = document.forms[0]("hdnCurrentMonth").value;
					        //document.forms[0].selMonth.options[objCurMonth.value - 1].selected = true;
							var objCurMonth = document.getElementById("hdnCurrentMonth");
							if (objCurMonth != null) {
							    document.forms[0].selMonth.options[objCurMonth.value - 1].selected = true;
							}
					        //Ended:Yukti, DT:07/15/2014, MITS 34473
																	
						}

						function Disp() {
						    $.datepicker._disabledInputs = [];
						}
		
        				</script>
</head>
<body onunload="Disp();">
    <form id="frmData" runat="server" >
        <uc1:ErrorControl ID="ErrorControl" runat="server" />        
        <input type="hidden" runat="server" id="iDiaryStyle" value=""/>
        <input type="hidden"  runat="server" value="" id="sSortBy"/>
        <input type="hidden"  runat="server" value="" id="sSortOrder"/>
        <input type="hidden" runat="server" value="" id="sMonthSelected" />
        <input type="hidden" runat="server" value="" id="sYearSelected" />
        <input type="hidden" runat="server"  id="iDiaryRange" value=""/>
        <input type="hidden" id="sToDate" value="" />
        <input type="hidden" id="sFromDate" value="" />        
        <input type="hidden"  runat="server" id="currentyear" value=""/>
        <input type="hidden" runat="server" id="hdnCurrentMonth" value="" />   <%--MGaba2: MITS 14638--%>
        <input type="hidden" runat="server" id="hdnAssignedUser" value="" />
        <input type="hidden" runat="server" id="hdnAttachtable" value="" />
        <input type="hidden" runat="server" id="hdnAttachRecordId" value="" />
        <input type="hidden" runat="server" id="hdnIsPeek" value="" />
        
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
                 <td>
                 <asp:ImageButton ID="btnPrint" ImageUrl="~/Images/tb_print_active.png" class="bold" ToolTip="<%$ Resources:ttPrint %>"
                        runat="server" OnClientClick="return StoreSelection();" />  
                 <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                        runat="server" OnClick="btnCancel_Click" />
                  </td>
            </tr>
            <tr width="100%" class="ctrlgroup">
                <td width="100%" colspan="3">
                <asp:Label ID="lblDiaryLststyResrc"  runat="server" Text="<%$ Resources:lblDiaryLststyResrc %>"></asp:Label> 
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="radio" value="0"   name="rdoStyle"  id="selStyle"  runat="server" onclick="selectStyle(1)"/>
                      <asp:Label ID="lblDailyResrc"  runat="server" Text="<%$ Resources:lblDailyResrc %>"></asp:Label> 
                    
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="radio" value="1" name="rdoStyle" id="selStyle" onclick="selectStyle(2)" checked="true"/>
                     <asp:Label ID="lblWeeklyResrc"  runat="server" Text="<%$ Resources:lblWeeklyResrc %>"></asp:Label> 
                   
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="radio" value="2" name="rdoStyle" id="selStyle" onclick="selectStyle(3)"/>
                      <asp:Label ID="lblMonthlyResrc"  runat="server" Text="<%$ Resources:lblMonthlyResrc %>"></asp:Label> 
                   </td>
            </tr>
            <tr width="100%" class="ctrlgroup">
                <td width="100%" colspan="3">
                 <asp:Label ID="lblSortOrderResrc"  runat="server" Text="<%$ Resources:lblSortOrderResrc %>"></asp:Label> 
               </td>
            </tr>
            <tr>
                <td align="left" width="10%">
                   <asp:Label ID="lblSortFirstResrc"  runat="server" Text="<%$ Resources:lblSortFirstResrc %>"></asp:Label> 
               
                </td>
                 <td width="80%" align="left">
                     <asp:DropDownList ID="selSortBy1" runat="server">
                        <asp:ListItem Text ="<%$ Resources:(None) %>" Value="0"></asp:ListItem>
                        <asp:ListItem Text ="<%$ Resources:Priority %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text ="<%$ Resources:Task Name %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text ="<%$ Resources:Time Due %>" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                    
                     <asp:DropDownList ID="selSortOrder1" runat="server">
                        <asp:ListItem Text ="<%$ Resources:Asc %>" Value="ASC"></asp:ListItem>
                        <asp:ListItem Text ="<%$ Resources:Desc %>" Value="DESC"></asp:ListItem>
                     </asp:DropDownList>
                </td>
            </tr>
            <tr>
             <td align="left" width="10%">
               <asp:Label ID="lblSortSecResrc"  runat="server" Text="<%$ Resources:lblSortSecResrc %>"></asp:Label> 
               
            </td>
             <td width="80%" align="left">
                <asp:DropDownList ID="selSortBy2" runat="server">
                    <asp:ListItem Text ="<%$ Resources:(None) %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text ="<%$ Resources:Priority %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text ="<%$ Resources:Task Name %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text ="<%$ Resources:Time Due %>" Value="3"></asp:ListItem>
                </asp:DropDownList>
                
                <asp:DropDownList ID="selSortOrder2" runat="server">
                    <asp:ListItem Text ="<%$ Resources:Asc %>"  Value="ASC"></asp:ListItem>
                    <asp:ListItem Text ="<%$ Resources:Desc %>" Value="DESC"></asp:ListItem>
                </asp:DropDownList>
             </td>
            </tr>
            <tr>
             <td align="left" width="10%">

                <asp:Label ID="lblSortThirdResrc"  runat="server" Text="<%$ Resources:lblSortThirdResrc %>"></asp:Label> 
               
            </td>
             <td width="80%" align="left">
                <asp:DropDownList ID="selSortBy3" runat="server">
                    <asp:ListItem Text ="<%$ Resources:(None) %>"  Value="0"></asp:ListItem>
                    <asp:ListItem Text ="<%$ Resources:Priority %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text ="<%$ Resources:Task Name %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text ="<%$ Resources:Time Due %>" Value="3"></asp:ListItem>
                </asp:DropDownList>
                
                <asp:DropDownList ID="selSortOrder3" runat="server">
                    <asp:ListItem Text ="<%$ Resources:Asc %>" Value="ASC"></asp:ListItem>
                    <asp:ListItem Text ="<%$ Resources:Desc %>" Value="DESC"></asp:ListItem>
                </asp:DropDownList>
               </td>
            </tr>
            <tr class="ctrlgroup">
                <td width="100%" colspan="3">
                  <asp:Label ID="lblDiaryDateRgResrc"  runat="server" Text="<%$ Resources:lblDiaryDateRgResrc %>"></asp:Label> 
               </td>
            </tr>
            <tr>
             <td width="10%" colspan="2">
                <input type="radio" value="0"  name="rdoRange" id="selRange" onclick="selectRange(1)" disabled="true"/>
                   <asp:Label ID="lblTodayResrc"  runat="server" Text="<%$ Resources:lblTodayResrc %>"></asp:Label> 
                </td>
            </tr>
            <tr>
             <td width="10%" colspan="2">
                <input type="radio"  value="1" name="rdoRange" id="selRange" onclick="selectRange(2)" checked="true"/>
                 <asp:Label ID="lblCurrWeekResrc"  runat="server" Text="<%$ Resources:lblCurrWeekResrc %>"></asp:Label> 
               </td>
            </tr>
            <tr>
             <td width="10%" colspan="2">
                <input type="radio" value="2" name="rdoRange" id="selRange" onclick="selectRange(3)"/>
                 <asp:Label ID="lblSpeDateResrc"  runat="server" Text="<%$ Resources:lblSpeDateResrc %>"></asp:Label> 
                </td>
            </tr>
            <tr>
                <td width="10%" align="right">
                 <asp:Label ID="lblFromResrc"  runat="server" Text="<%$ Resources:lblFromResrc %>"></asp:Label> 
                </td>
                <td width="90%" align="left">
                    <input type="text" value="" id="txtFromDate" runat="server" size="30" onblur="dateLostFocus(this.id);" disabled="true"/>
                   
                    <!-- Rakhel ML Changes - start !-->
                    <script type="text/javascript">
                        $(function () {
                            $("#txtFromDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                       </script>
                    <!-- Rakhel ML Changes - end !-->
                </td>
            </tr>
            <tr>
                <td width="10%" align="right">
                 <%--Richa : Mits 30994 Start--%>
                <asp:Label ID="lblToResrc"  runat="server" Text="<%$ Resources:lblToResrc %>"></asp:Label> 
                <%--Richa : Mits 30994 End--%>
                </td>
                <td align="left">
                    <input type="text" value="" id="txtToDate" size="30" runat="server" onblur="dateLostFocus(this.id);" disabled="true"/>
                    
                     <!-- Rakhel ML Changes - start !-->
                     <script type="text/javascript">
                         $(function () {
                             $("#txtToDate").datepicker({
                                 showOn: "button",
                                 buttonImage: "../../Images/calendar.gif",
                                 buttonImageOnly: true,
                                 showOtherMonths: true,
                                 selectOtherMonths: true,
                                 changeYear: true
                             });
                         });
                       </script>
                    <!-- Rakhel ML Changes - end !-->
                </td>
            </tr>
            <tr>
                <td width="10%">
                    <input type="radio"  runat="server" value="4" id="selMonthlyRange" disabled="true"/>
                    <%--Richa : Mits 30994 Start--%>
                     <asp:Label ID="lblmonthyear"  runat="server" Text="<%$ Resources:lblmonthyear %>"></asp:Label> 
                    <%--Richa : Mits 30994 End--%>

                </td>
             <td width="80%" align="left">
                 <asp:DropDownList ID="selMonth" runat="server" disabled="true">
                     <asp:ListItem Text="<%$ Resources:January %>" Value="1"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:February %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:March %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:April %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:May %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:June %>" Value="6"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:July %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:August %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:September %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:October %>" Value="10"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:November %>" Value="11"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:December %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="selYear" runat="server" disabled="true"></asp:DropDownList>
                <script type="text/javascript">check();</script>
            </td>
        </tr>
       <%-- <tr>
            <td colspan="2" align="left">
                <asp:Button ID="btnPrint" runat="server" CssClass="button" OnClientClick="return StoreSelection();" Text="Print" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button"   Text="Cancel" onclick="btnCancel_Click" />
            </td>
        </tr>--%>
   </table>
    </form>
</body>
</html>
