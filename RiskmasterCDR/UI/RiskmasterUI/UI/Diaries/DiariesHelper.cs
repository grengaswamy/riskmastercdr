﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.Diaries
{
    public class DiariesHelper
    {
        #region Constructor
        public DiariesHelper()
        {
        }
        #endregion

        #region Public Methods
        public XmlNode InputDocForListAndCalender(DiaryListCurrentAction objCurrentAction)
        {
            XmlDocument xDoc = null;
            XmlNode xNode = null;
            XmlAttribute xAttribute1 = null;
            XmlAttribute xAttribute2 = null;
            XmlAttribute xAttribute3 = null;
            XmlAttribute xAttribute4 = null;
            XmlAttribute xAttribute5 = null;
            XmlAttribute xAttribute6 = null;
            XmlAttribute xAttribute7 = null;
            XmlAttribute xAttribute8 = null;
            XmlAttribute xAttribute9 = null;
            XmlAttribute xAttribute10 = null;
            XmlAttribute xAttribute11 = null;
            XmlAttribute xAttribute12 = null;
            XmlAttribute xAttribute13 = null;
            XmlAttribute xAttribute14 = null;
            XmlAttribute xAttribute15 = null;
            XmlAttribute xAttribute16 = null;
            XmlAttribute xAttribute17 = null;//Neha
            XmlAttribute xAttribute18 = null;
            XmlAttribute xAttribute19 = null;//Neha
            XmlAttribute xAttribute20 = null;            
            XmlAttribute xAttribute21 = null;//Ankit-MITS 27074
            XmlAttribute xAttribute22 = null;//Ankit-MITS 27074
            //MITS 36519
            XmlAttribute xAttribute23 = null;
            XmlAttribute xAttFilterExpr = null;
            
            try
            {
                xDoc = new XmlDocument();

                xNode = xDoc.CreateElement("GetDiaryDom");


                xAttribute1 = xDoc.CreateAttribute("activediarychecked");
                xAttribute1.Value = objCurrentAction.Activediarychecked;
                xNode.Attributes.Append(xAttribute1);

                xAttribute2 = xDoc.CreateAttribute("attachrecordid");
                xAttribute2.Value = objCurrentAction.Attachrecordid;
                xNode.Attributes.Append(xAttribute2);

                xAttribute3 = xDoc.CreateAttribute("attachtable");
                xAttribute3.Value = objCurrentAction.Attachtable;
                xNode.Attributes.Append(xAttribute3);



                xAttribute4 = xDoc.CreateAttribute("diaryfunction");
                xAttribute4.Value = objCurrentAction.Diaryfunction;
                xNode.Attributes.Append(xAttribute4);

                xAttribute5 = xDoc.CreateAttribute("duedate");
                xAttribute5.Value = objCurrentAction.Duedate;
                xNode.Attributes.Append(xAttribute5);

                xAttribute6 = xDoc.CreateAttribute("open");
                xAttribute6.Value = "1";//objCurrentAction.Open;
                xNode.Attributes.Append(xAttribute6);

                xAttribute7 = xDoc.CreateAttribute("pagecount");
                xAttribute7.Value = objCurrentAction.Pagecount;
                xNode.Attributes.Append(xAttribute7);

                xAttribute8 = xDoc.CreateAttribute("pagenumber");
                xAttribute8.Value = objCurrentAction.Pagenumber;
                xNode.Attributes.Append(xAttribute8);

                xAttribute9 = xDoc.CreateAttribute("pagesize");
                //Ankit-MITS 27074-Start changes for making number or records per page customizable
                //xAttribute9.Value = "10";//objCurrentAction.Pagesize;
                xAttribute9.Value = objCurrentAction.Pagesize;
                //Ankit-MITS 27074-End Changes
                xNode.Attributes.Append(xAttribute9);

                xAttribute10 = xDoc.CreateAttribute("recordcount");
                xAttribute10.Value = objCurrentAction.Recordcount;
                xNode.Attributes.Append(xAttribute10);

                xAttribute11 = xDoc.CreateAttribute("reqactdiary");
                //xAttribute11.Value = "0";
                xAttribute11.Value = objCurrentAction.Reqactdiary;
                xNode.Attributes.Append(xAttribute11);

                xAttribute12 = xDoc.CreateAttribute("showallclaimdiaries");
                xAttribute12.Value = objCurrentAction.Showallclaimdiaries;
                xNode.Attributes.Append(xAttribute12);

                xAttribute13 = xDoc.CreateAttribute("showalldiariesforactiverecord");
                xAttribute13.Value = objCurrentAction.Showalldiariesforactiverecord;
                xNode.Attributes.Append(xAttribute13);

                xAttribute14 = xDoc.CreateAttribute("shownotes");
                xAttribute14.Value = objCurrentAction.Shownotes;
                xNode.Attributes.Append(xAttribute14);

                xAttribute15 = xDoc.CreateAttribute("sortorder");
                xAttribute15.Value = objCurrentAction.Sortorder;
                xNode.Attributes.Append(xAttribute15);

                //neha MITS 23477 05/03/2011
                xAttribute16 = xDoc.CreateAttribute("usersortorder");
                xAttribute16.Value = objCurrentAction.Usersortorder;
                xNode.Attributes.Append(xAttribute16);
                //MITS 23477 End
                //Added by Amitosh fro Terelik Grid
                xAttribute20 = xDoc.CreateAttribute("showregarding");
                xAttribute20.Value = objCurrentAction.Showregarding;
                xNode.Attributes.Append(xAttribute20);
                xAttFilterExpr = xDoc.CreateAttribute("filterexpression");
                xAttFilterExpr.Value = objCurrentAction.FilterExpression;
                xNode.Attributes.Append(xAttFilterExpr);
                //End Amitosh
                xAttribute17 = xDoc.CreateAttribute("user");
                xAttribute17.Value = objCurrentAction.User;
                xNode.Attributes.Append(xAttribute17);
                xDoc.AppendChild(xNode);
                //Mits 23586 neha 05/16/2011
                xAttribute18 = xDoc.CreateAttribute("tasknamefilter");
                xAttribute18.Value = objCurrentAction.TaskNameFilter;
                xNode.Attributes.Append(xAttribute18);
                xDoc.AppendChild(xNode);

                xAttribute19 = xDoc.CreateAttribute("tasknametext");
                xAttribute19.Value = objCurrentAction.TaskNameText;
                xNode.Attributes.Append(xAttribute19);
                xDoc.AppendChild(xNode);
                //Ankit-MITS 27074-Start Changes
                xAttribute21 = xDoc.CreateAttribute("claimactivediarychecked");
                xAttribute21.Value = objCurrentAction.Claimactivediarychecked;
                xNode.Attributes.Append(xAttribute21);
                xDoc.AppendChild(xNode);

                xAttribute22 = xDoc.CreateAttribute("diarysource");
                xAttribute22.Value = objCurrentAction.Diarysource;
                xNode.Attributes.Append(xAttribute22);
                xDoc.AppendChild(xNode);
                //Ankit-MITS 27074 End changes
                xDoc.AppendChild(xNode);

                //MITS 36519
                xAttribute23 = xDoc.CreateAttribute("sortexpression");
                xAttribute23.Value = objCurrentAction.SortExpression;
                xNode.Attributes.Append(xAttribute23);
                xDoc.AppendChild(xNode);
                return xDoc.SelectSingleNode("/");
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
    }

        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(diaryDoc));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        
		 //Deb: MITS 32961
        public static string GetSortDirection(int iSortOrder)
        {
            string sDirection = "ASC";
            switch (iSortOrder)
            {
                case (int)DIARYSORTORDER.DEC_COMPLETE_DATE:
                case (int)DIARYSORTORDER.DEC_ATTACH_TABLE:
                case (int)DIARYSORTORDER.DEC_ENTRY_NAME:
                case (int)DIARYSORTORDER.DEC_PRIORITY:
                case (int)DIARYSORTORDER.DEC_CLAIMANT:
                case (int)DIARYSORTORDER.DEC_WORK_ACTIVITY:
                //tkatsarski: 06/18/14 Start changes for Mits 36512 
                //jira562 psharma206
                case (int)DIARYSORTORDER.DEC_DEPARTMENT:
                //tkatsarski: End changes for Mits 36512
                    sDirection = "DEC";
                    break;
                default:
                    break;
            }
            return sDirection;
        }
        public static int GetSortOrder(string sortExpression,int iSortOrder)
        {
            if (!string.IsNullOrEmpty(sortExpression))
            {
                switch (sortExpression)
                {
                    case "priority":
                        iSortOrder = (int)DIARYSORTORDER.ASC_PRIORITY;
                        break;
                    case "complete_date":
                        iSortOrder = (int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                        break;
                    case "tasksubject":
                        iSortOrder = (int)DIARYSORTORDER.ASC_ENTRY_NAME;
                        break;
                    case "attachrecord":
                    case "attachprompt":
                        iSortOrder = (int)DIARYSORTORDER.ASC_ATTACH_TABLE;
                        break;
                    case "work_activity":
                        iSortOrder = (int)DIARYSORTORDER.ASC_WORK_ACTIVITY;
                        break;
                    case "claimant":
                        iSortOrder = (int)DIARYSORTORDER.ASC_CLAIMANT;
                        break;
                        //MITS 36519
                    case "department":
                        iSortOrder = (int)DIARYSORTORDER.ASC_DEPARTMENT;
                        break;
                    case "claimstatus":
                        iSortOrder = (int)DIARYSORTORDER.ASC_CLAIM_STATUS;
                        break;
                    case "assigned_user":
                        iSortOrder = (int)DIARYSORTORDER.ASC_ASSIGNED_USER;
                        break;
                    case "assigning_user":
                        iSortOrder = (int)DIARYSORTORDER.ASC_ASSIGNING_USER;
                        break;
                    case "parentrecord":
                        iSortOrder = (int)DIARYSORTORDER.ASC_PARENTRECORD;
                        break;
                    case "notroutable":
                        iSortOrder = (int)DIARYSORTORDER.ASC_NOTROUTE;
                        break;
                    case "notrollable":
                        iSortOrder = (int)DIARYSORTORDER.ASC_NOTROLL;
                        break;
                    default:
                        iSortOrder = (int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                        break;
                }
            }
            else//srajindersin MITS 32355 01/06/2014
            {
                switch (Convert.ToInt32(iSortOrder))
                {
                    case (int)DIARYSORTORDER.DEC_COMPLETE_DATE:
                        iSortOrder = (int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                        break;
                    case (int)DIARYSORTORDER.ASC_ATTACH_TABLE:
                        iSortOrder = (int)DIARYSORTORDER.DEC_ATTACH_TABLE;
                        break;
                    case (int)DIARYSORTORDER.ASC_WORK_ACTIVITY:
                        iSortOrder = (int)DIARYSORTORDER.DEC_WORK_ACTIVITY;
                        break;
                    case (int)DIARYSORTORDER.ASC_ENTRY_NAME:
                        iSortOrder = (int)DIARYSORTORDER.DEC_ENTRY_NAME;
                        break;
                    case (int)DIARYSORTORDER.ASC_PRIORITY:
                        iSortOrder = (int)DIARYSORTORDER.DEC_PRIORITY;
                        break;
                    case (int)DIARYSORTORDER.ASC_CLAIMANT:
                        iSortOrder = (int)DIARYSORTORDER.DEC_CLAIMANT;
                        break;
                    //tkatsarski: 06/18/14 Mits 36512 Diary list does not sort properly on all fields
                    //tkatsarski: Changes for MITS 36512 start here
                    //jira562 psharma206
                    case (int)DIARYSORTORDER.ASC_DEPARTMENT:
                        iSortOrder = (int)DIARYSORTORDER.DEC_DEPARTMENT;
                        break;
                    case (int)DIARYSORTORDER.ASC_CLAIM_STATUS:
                        iSortOrder = (int)DIARYSORTORDER.DES_CLAIM_STATUS;
                        break;
                    case (int)DIARYSORTORDER.ASC_ASSIGNED_USER:
                        iSortOrder = (int)DIARYSORTORDER.DES_ASSIGNED_USER;
                        break;
                    case (int)DIARYSORTORDER.ASC_ASSIGNING_USER:
                        iSortOrder = (int)DIARYSORTORDER.DEC_ASSIGNING_USER;
                        break;
                    //tkatsarski: Changes for MITS 36512 end here
                    case (int)DIARYSORTORDER.DEC_ATTACH_TABLE:
                        iSortOrder = (int)DIARYSORTORDER.ASC_ATTACH_TABLE;
                        break;
                    case (int)DIARYSORTORDER.DEC_ENTRY_NAME:
                        iSortOrder = (int)DIARYSORTORDER.ASC_ENTRY_NAME;
                        break;
                    case (int)DIARYSORTORDER.DEC_PRIORITY:
                        iSortOrder = (int)DIARYSORTORDER.ASC_PRIORITY;
                        break;
                    case (int)DIARYSORTORDER.DEC_CLAIMANT:
                        iSortOrder = (int)DIARYSORTORDER.ASC_CLAIMANT;
                        break;
                    case (int)DIARYSORTORDER.DEC_WORK_ACTIVITY:
                        iSortOrder = (int)DIARYSORTORDER.ASC_WORK_ACTIVITY;
                        break;
                    //tkatsarski: 06/18/14 Mits 36512 Diary list does not sort properly on all fields
                    //MITS 36519
                    //tkatsarski: 06/18/14 Mits 36512 Diary list does not sort properly on all fields
                    //tkatsarski: Changes for MITS 36512 start here
                       // jira562 psharma206
                    case (int)DIARYSORTORDER.DEC_DEPARTMENT:
                        iSortOrder = (int)DIARYSORTORDER.ASC_DEPARTMENT;
                        break;
                    case (int)DIARYSORTORDER.DES_CLAIM_STATUS:
                        iSortOrder = (int)DIARYSORTORDER.ASC_CLAIM_STATUS;
                        break;
                    case (int)DIARYSORTORDER.DES_ASSIGNED_USER:
                        iSortOrder = (int)DIARYSORTORDER.ASC_ASSIGNED_USER;
                        break;
                    case (int)DIARYSORTORDER.DEC_ASSIGNING_USER:
                        iSortOrder = (int)DIARYSORTORDER.ASC_ASSIGNING_USER;
                        break;
                    //tkatsarski: Changes for MITS 36512 end here

                    
                    case (int)DIARYSORTORDER.ASC_PARENTRECORD:
                        iSortOrder = (int)DIARYSORTORDER.DEC_PARENTRECORD;
                        break;
                    case (int)DIARYSORTORDER.DEC_PARENTRECORD:
                        iSortOrder = (int)DIARYSORTORDER.ASC_PARENTRECORD;
                        break;
                    //igupta3 jira 439
                    case (int)DIARYSORTORDER.ASC_NOTROUTE:
                        iSortOrder = (int)DIARYSORTORDER.DEC_NOTROUTE;
                        break;
                    case (int)DIARYSORTORDER.DEC_NOTROUTE:
                        iSortOrder = (int)DIARYSORTORDER.ASC_NOTROUTE;
                        break;
                    case (int)DIARYSORTORDER.ASC_NOTROLL:
                        iSortOrder = (int)DIARYSORTORDER.DEC_NOTROLL;
                        break;
                    case (int)DIARYSORTORDER.DEC_NOTROLL:
                        iSortOrder = (int)DIARYSORTORDER.ASC_NOTROLL;
                        break;
                    default:
                        iSortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                        break;
                }
            }
            return iSortOrder;
        }
        //Deb: MITS 32961 
		
        public string CallCWSFunctionForDiaries(XmlNode documentNodeChild, string serviceMethodToCall)
        {
            XmlNode templateNode = null;
            string oMessageElement = string.Empty;
            string responseCWS = string.Empty;
            XmlDocument messageDoc = null;

            try
            {
                System.Collections.ArrayList arList = new System.Collections.ArrayList();
                oMessageElement = GetMessageTemplate();

                messageDoc = new XmlDocument();
                messageDoc.LoadXml(oMessageElement);

                if (documentNodeChild != null)
                {
                    templateNode = messageDoc.SelectSingleNode("Message/Document");
                    templateNode.InnerXml = documentNodeChild.OuterXml;
                }

                if (!string.IsNullOrEmpty(serviceMethodToCall))
                {
                    templateNode = messageDoc.SelectSingleNode("Message/Call/Function");
                    templateNode.InnerText = serviceMethodToCall;
                }

                responseCWS = AppHelper.CallCWSService(messageDoc.InnerXml);

                return responseCWS;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            
        }


        /// <summary>
        /// Gets the appropriate sort column index for the Diary screen
        /// </summary>
        /// <param name="sortExpression">string specifying the Diary sort order</param>
        /// <returns>integer containing the sort id</returns>
        public static int GetSortColumnIndex(string sortExpression)
        {
            
            const int PRIORITY = 1;
            const int DUE_DATE = 3;
            const int TASK_NAME = 4;
            const int ATTACHED_RECORD = 5;
            const int WORK_ACTIVITY = 6;
            const int CLAIMANT = 7;
            const int DEPARTMENT = 8;
            const int CLAIM_STATUS = 9;
            const int ASSIGNED_USER = 10;
            const int ASSIGNING_USER = 11;
            //MITS 36519
            const int PARENTRECORD = 12;
            //igupta3 jira 439
            const int NOTROUTE = 13;
            const int NOTROLL = 14;
            int intSortColumnIndex = 0;


            switch (sortExpression)
            {
                case "priority":
                    intSortColumnIndex = PRIORITY;
                    break;
                //case "duedate":
                case "complete_date": //hlv MITS 29594 11/6/12
                    intSortColumnIndex = DUE_DATE;
                    break;
                //case "taskname":
                case "tasksubject": //hlv MITS 29594 11/6/12
                    intSortColumnIndex = TASK_NAME;
                    break;
                //case "attachedrecord":
                case "attachprompt":  //hlv MITS 29594 11/6/12
                    intSortColumnIndex = ATTACHED_RECORD;
                    break;
                //case "workactivity":
                case "work_activity": //hlv MITS 29594 11/6/12
                    intSortColumnIndex = WORK_ACTIVITY;
                    break;
                case "claimant":
                    intSortColumnIndex = CLAIMANT;
                    break;
                // changes for MITS 29594 SGUPTA243
                //MITS 36519
                case "department":
                    intSortColumnIndex = DEPARTMENT;
                    break;

                case "claimstatus":
                    intSortColumnIndex = CLAIM_STATUS;
                    break;

                case "assigned_user":
                    intSortColumnIndex = ASSIGNED_USER;
                    break;

                case "assigning_user":
                    intSortColumnIndex = ASSIGNING_USER;
                    break;
                case "parentrecord":
                    intSortColumnIndex = PARENTRECORD;
                    break;
                // end changes for MITS 29594 SGUPTA243
                case "notroutable":
                    intSortColumnIndex = NOTROUTE;
                    break;
                case "notrollable":
                    intSortColumnIndex = NOTROLL;
                    break;
                default:
                    intSortColumnIndex = DUE_DATE;
                    break;
            }

            return intSortColumnIndex;
        }


        //tmalhotra3: MITS 25091
        /// <summary>
        /// Gets the appropriate sort column index for the Diary History Screen
        /// </summary>
        /// <param name="sortExpression">string specifying the Diary sort order</param>
        /// <returns>integer containing the sort id</returns>
        public static int GetSortColumnIndexforDiaryHistory(string sortExpression)
        {

            const int PRIORITY = 1;
            const int DUE_DATE = 3;
            const int TASK_NAME = 4;
            const int ATTACHED_RECORD = 5;
            const int ASSIGNED_USER = 7;
            const int ASSIGNING_USER = 8;
            int intSortColumnIndex = 0;


            switch (sortExpression)
            {
                case "priority":
                    intSortColumnIndex = PRIORITY;
                    break;
                case "complete_date": 
                    intSortColumnIndex = DUE_DATE;
                    break;
                case "tasksubject": 
                    intSortColumnIndex = TASK_NAME;
                    break;
                case "attachprompt": 
                    intSortColumnIndex = ATTACHED_RECORD;
                    break;
                case "assigned_user":
                    intSortColumnIndex = ASSIGNED_USER;
                    break;
                case "assigning_user":
                    intSortColumnIndex = ASSIGNING_USER;
                    break;
                default:
                    intSortColumnIndex = DUE_DATE;
                    break;
            }

            return intSortColumnIndex;
        }


        /// <summary>
        /// Gets the appropriate sort column index for the Diary screen
        /// </summary>
        /// <param name="sortExpression">string specifying the Diary sort order</param>
        /// <param name="sortOrder">string filter specifying ascending or descending sort order</param>
        /// <returns>integer containing the sort id</returns>
        public static int GetSortOrder(string sortExpression, string sortOrder)
        {
            int intDiarySortOrder = 0;

            switch (sortExpression)
            {
                case "priority":
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_PRIORITY;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DEC_PRIORITY;
                    }
                    break;
                case "complete_date":
                case "duedate":// changes for MITS 32358: aaggarwal29
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                    }
                    break;
                case "tasksubject":
                case "taskname": // changes for MITS 32358: aaggarwal29
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_ENTRY_NAME;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DEC_ENTRY_NAME;
                    }
                    break;
                case "attachprompt":
                case "attachedrecord":// changes for MITS 32358: aaggarwal29
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_ATTACH_TABLE;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DEC_ATTACH_TABLE;
                    }
                    break;
                case "work_activity":
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_WORK_ACTIVITY;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DEC_WORK_ACTIVITY;
                    }
                    break;
                case "claimant":
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_CLAIMANT;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DEC_CLAIMANT;
                    }
                    break;

                // changes for MITS 29594 SGUPTA243

                case "department":
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_DEPARTMENT;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DEC_DEPARTMENT;
                    }
                    break;


                case "claimstatus":
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_CLAIM_STATUS;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DES_CLAIM_STATUS;
                    }
                    break;

                case "assigned_user":
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_ASSIGNED_USER;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DES_ASSIGNED_USER;
                    }
                    break;


                case "assigning_user":
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_ASSIGNING_USER;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DEC_ASSIGNING_USER;
                    }
                    break;

                // end changes for MITS 29594 SGUPTA243
                //MITS 36519
                case "parentrecord":
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_PARENTRECORD;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DEC_PARENTRECORD;
                    }
                    break;

                default:
                    //Ankit Start : MITS 31149
                    if (sortOrder == "ASC")
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.ASC_COMPLETE_DATE;
                    }
                    else
                    {
                        intDiarySortOrder = (int)DIARYSORTORDER.DEC_COMPLETE_DATE;
                    }
                    //Ankit End
                    break;
            }

            return intDiarySortOrder;
        }
        #endregion

        #region Private Methods
        private string GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function></Function>
              </Call>
              <Document>
              </Document>
            </Message>
            ");

            return oTemplate.ToString();
        }
        #endregion 

        private enum DIARYSORTORDER : int
        {
            NONE,
            ASC_COMPLETE_DATE,
            ASC_ATTACH_TABLE,
            ASC_ENTRY_NAME,
            ASC_PRIORITY,
            DEC_COMPLETE_DATE,
            DEC_ATTACH_TABLE,
            DEC_ENTRY_NAME,
            DEC_PRIORITY,
            ASC_WORK_ACTIVITY,
            ASC_CLAIMANT,//Merged by Nitin for R6 Diary List - sorting
            DEC_WORK_ACTIVITY = 13,
            DEC_CLAIMANT, //Merged by Nitin for R6 Diary List - sorting
            //Added  changes for MITS 29594 SGUPTA243
            ASC_DEPARTMENT,
            DEC_DEPARTMENT,
            ASC_CLAIM_STATUS,
            DES_CLAIM_STATUS,
            ASC_ASSIGNED_USER,
            DES_ASSIGNED_USER,
            ASC_ASSIGNING_USER,
            DEC_ASSIGNING_USER,
            //end   changes for MITS 29594 SGUPTA243
            //MITS 36519
            ASC_PARENTRECORD,
            DEC_PARENTRECORD,
            //igupta3 jira 439
            ASC_NOTROUTE,
            DEC_NOTROUTE,
            ASC_NOTROLL,
            DEC_NOTROLL

        }//enum

    }//class

    
}
