﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class PrintListing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlNode inputDocForPrint = null;
            XmlDocument diaryListDoc = null;

            string assignedUser = string.Empty;
            string diarySel = string.Empty;
            string fromDate = string.Empty;
            string sortBy = string.Empty;
            string taskDesc = string.Empty;
            string toDate = string.Empty;
            string useTaskDesc = string.Empty;
            string htmlScriptForDiaryList = string.Empty;
            //MITS 34874 asingh263
            string screen = string.Empty;
            string entryid = string.Empty;
            //MITS 34874 asingh263

            string sortOrder = string.Empty;

            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["assigneduser"] != null)
                    {
                        assignedUser = Request.QueryString["assigneduser"];
                    }

                    if (Request.QueryString["diarysel"] != null)
                    {
                        diarySel = Request.QueryString["diarysel"];
                    }

                    if (Request.QueryString["fromdate"] != null)
                    {
                        fromDate = Request.QueryString["fromdate"];
                    }

                    if (Request.QueryString["sortby"] != null)
                    {
                        sortBy = Request.QueryString["sortby"];
                    }

                    if (Request.QueryString["taskdesc"] != null)
                    {
                        taskDesc = Request.QueryString["taskdesc"];
                    }

                    if (Request.QueryString["todate"] != null)
                    {
                        toDate = Request.QueryString["todate"];
                    }

                    if (Request.QueryString["usetaskdesc"] != null)
                    {
                        useTaskDesc = Request.QueryString["usetaskdesc"];
                    }
                    //asingh263 MITS 34874 STARTS 
                    if (Request.QueryString["screen"] != null)
                    {
                        screen = Request.QueryString["screen"];
                    }
                    if (Request.QueryString["entryid"] != null)
                    {
                        entryid=Request.QueryString["entryid"];
                    }
                    //asingh263 MITS 34874 ends
                    //aaggarwal29 : MITS 36691 sortorder changes
                    if (Request.QueryString["sortorder"] != null)
                    {
                        sortOrder = Request.QueryString["sortorder"];
                    }
                    inputDocForPrint = GetInputDocForPrintDiary(entryid, assignedUser, diarySel, fromDate, sortBy, taskDesc, toDate, useTaskDesc, screen, sortOrder);//MITS 34874

                    diaryListDoc = GetDiaryListDocForPrint(inputDocForPrint);

                    ShowPDFReport(diaryListDoc);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlDocument GetDiaryListDocForPrint(XmlNode inputDocNode)
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            
            string serviceMethodToCall = string.Empty;
            
            try
            {
                serviceMethodToCall = "WPAAdaptor.DiaryList";
                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        ////asingh263 MITS 34874 starts

        private XmlDocument GetDiaryDocForPrintDetails(XmlNode inputDocNode)
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;

            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "WPAAdaptor.DiaryList";
                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        ////asingh263 MITS 34874 ends
        private XmlNode GetInputDocForPrintDiary(string entryid,string assignedUser, string diarySel, string fromDate,
                string sortBy, string taskDesc, string toDate, string useTaskDesc, string screen, string sortOrder)
        {
            XmlDocument printDiaryDoc = null;
            XmlNode diaryListNode = null;

            XmlAttribute atbAssignedUser = null;
            XmlAttribute atbDiarySel = null;
            XmlAttribute atbFromdate = null;
            XmlAttribute atbSortBy = null;
            XmlAttribute atbTaskDesc = null;
            XmlAttribute atbToDate = null;
            XmlAttribute atbUseTaskDesc = null;
            XmlAttribute atbscreen = null;//asingh263 MITS 34874
            XmlAttribute atbentryid = null;//asingh263 MITS 34874
			XmlAttribute atbLangCode = null; //MITS 34392 hlv
            XmlAttribute atbPageId = null; //MITS 34392 hlv
            XmlAttribute atbCurrentPageId = null; //MITS 34392 hlv
            XmlAttribute atbSortOrder = null;

            printDiaryDoc = new XmlDocument();
            diaryListNode = printDiaryDoc.CreateElement("DiaryList");
            //asingh263 MITS 34874 starts
            atbentryid = printDiaryDoc.CreateAttribute("entryid");
            atbentryid.Value = entryid;
            diaryListNode.Attributes.Append(atbentryid);
            //asingh263 MITS 34874 ends
            atbAssignedUser = printDiaryDoc.CreateAttribute("assigneduser");
            atbAssignedUser.Value = assignedUser;
            diaryListNode.Attributes.Append(atbAssignedUser);

            atbDiarySel = printDiaryDoc.CreateAttribute("diarysel");
            atbDiarySel.Value = diarySel;
            diaryListNode.Attributes.Append(atbDiarySel);
            
            atbFromdate = printDiaryDoc.CreateAttribute("fromdate");
            atbFromdate.Value = fromDate;
            diaryListNode.Attributes.Append(atbFromdate);

            atbSortBy = printDiaryDoc.CreateAttribute("sortby");
            atbSortBy.Value = sortBy;
            diaryListNode.Attributes.Append(atbSortBy);

            atbTaskDesc = printDiaryDoc.CreateAttribute("taskdesc");
            atbTaskDesc.Value = taskDesc;
            diaryListNode.Attributes.Append(atbTaskDesc);

            atbToDate = printDiaryDoc.CreateAttribute("todate");
            atbToDate.Value = toDate;
            diaryListNode.Attributes.Append(atbToDate);

            atbUseTaskDesc = printDiaryDoc.CreateAttribute("usetaskdesc");
            atbUseTaskDesc.Value = useTaskDesc;
            diaryListNode.Attributes.Append(atbUseTaskDesc);
            //asingh263 MITS 34874 starts
            atbscreen = printDiaryDoc.CreateAttribute("screen");
            atbscreen.Value = screen;
            diaryListNode.Attributes.Append(atbscreen);
            //asingh263 MITS 34874 ends
			
			 //MITS 34392 hlv begin
            atbLangCode = printDiaryDoc.CreateAttribute("langcode");
            atbLangCode.Value = AppHelper.GetLanguageCode();
            diaryListNode.Attributes.Append(atbLangCode);

            atbPageId = printDiaryDoc.CreateAttribute("CreateDiaryPageId");
            atbPageId.Value = RMXResourceProvider.PageId("CreateDiary.aspx");
            diaryListNode.Attributes.Append(atbPageId);

            atbCurrentPageId = printDiaryDoc.CreateAttribute("CurrentPageId");
            atbCurrentPageId.Value = RMXResourceProvider.PageId("PrintListing.aspx");
            diaryListNode.Attributes.Append(atbCurrentPageId);
            //MITS 34392 hlv end 

            //aaggarwal29 : MITS 36691 added new attribute describing sort order/direction
            atbSortOrder = printDiaryDoc.CreateAttribute("sortorder");
            atbSortOrder.Value = sortOrder;
            diaryListNode.Attributes.Append(atbSortOrder);

            printDiaryDoc.AppendChild(diaryListNode);

            return diaryListNode;
        }

        private void ShowPDFReport(XmlDocument diaryListDoc)
        {
            XmlNode fileNode = null;

            fileNode = diaryListDoc.SelectSingleNode("//File");
            string sFileContent = fileNode.InnerText;

            //commented by Nitin on 23-Apr-2009 for Mits 15800
            //byte[] byteOrg = Convert.FromBase64String(sFileContent);
            //Response.Clear();
            //Response.Charset = "";
            //Response.ContentType = "application/vnd.fdf";
            //Response.AddHeader("Content-Disposition", "inline;");
            //Response.BinaryWrite(byteOrg);
            //Response.End();


            //added by Nitin on 23-Apr-2009 for Mits 15800
            byte[] pdfbytes = Convert.FromBase64String(sFileContent);

            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            Response.AppendHeader("Content-Encoding", "none;");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "inline");
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(pdfbytes);
            Response.Flush();
            Response.Close();
        }
    }
}
