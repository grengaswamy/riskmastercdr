﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class CreateDiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument diaryDoc = null;
            XmlNodeList wpaTaskNodeList = null;

            //Deb: ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //Aman MITS 31001 
                if (string.IsNullOrEmpty(txtDueDate.Value))
                {
                    txtDueDate.Value = AppHelper.GetDate(System.DateTime.Today.ToString());
                   
                }
                if(string.IsNullOrEmpty(txtDueTime.Value))
                {
                    txtDueTime.Value = AppHelper.GetTime(System.DateTime.Now.TimeOfDay.ToString());
                }
                //Aman MITS 31001 
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CreateDiary.aspx"), "CreateDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "CreateDiaryValidations", sValidationResources, true);
            //Deb: ML Changes
            try
            {
                if (!IsPostBack)
                {
                    diaryDoc = LoadDiary();

                    wpaTaskNodeList = diaryDoc.SelectNodes("ResultMessage/Document/GetUsersResult/WpaTaskList/WpaTask");

                    if (wpaTaskNodeList.Count > 0)
                    {
                        foreach (XmlNode wpaTaskNode in wpaTaskNodeList)
                        {
                            txtSubjectDcboBox.Items.Add(wpaTaskNode.Attributes["code_desc"].Value);
                        }

                        this.Page.ClientScript.RegisterStartupScript(typeof(string), "WpaTaskIsSelect", "_isSelect = true;", true);
                    }
                    else
                    {
                        txtSubjectDcboBox.Visible = false;
                    }
                    //07/26/2011 SMISHRA54: WPA Email Notification
                    //chkEmailNotification.Checked = true;      // Ishan : As per discussion with parag- By default unchecked
                    //07/26/2011 SMISHRA54: WPA Email Notification

                    if (Request.QueryString["assigneduser"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }

                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";//"EVENT";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";// "1";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }
                    //Aman MITS 31001
                    if (!IsPostBack)
                    {
                        if (string.IsNullOrEmpty(txtDueDate.Value))
                        { txtDueDate.Value = AppHelper.GetDate(System.DateTime.Today.ToString()); }
                        if (string.IsNullOrEmpty(txtDueTime.Value))
                        { txtDueTime.Value = AppHelper.GetTime(System.DateTime.Now.TimeOfDay.ToString()); }
                    }
                    //Aman MITS 31001
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlDocument LoadDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            XmlElement activities = null;
            string serviceMethodToCall = string.Empty;
            int iIterator = 0;
            try
            {
                serviceMethodToCall = "WPAAdaptor.GetUsers";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForLoadDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                LoginID.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@LoginID").InnerText;
                LoginUserName.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@LoginUser").InnerText;
                DefaultAssignedTo.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@DefaultAssignedTo").InnerText;

                //zmohammad MITs 33655 Diary Initialisation script : Start
                if (!string.IsNullOrEmpty(DefaultAssignedTo.Value) && DefaultAssignedTo.Value == "True" && LoginUserName.Value != "")
                {
                    UserStr.Value = LoginUserName.Value;
                    UserIdStr.Value = LoginID.Value;
                }

                // rrachev JIRA 4607
                //XmlNode mailDisabled = diaryDoc.SelectSingleNode("//GetUsersResult/@MAIL_DISABLED");
                //if (mailDisabled != null && (mailDisabled.InnerText.ToLower() == "true"))
                //{
                //    chkAutoConfrim.Disabled = true;//dvatsa
                //}
                //dvatsa JIRA 11627(start)
                XmlNode mailEnabled = diaryDoc.SelectSingleNode("//GetUsersResult/@MAIL_ENABLED");
                if (mailEnabled != null && (mailEnabled.InnerText.ToLower() == "true"))
                {
                    
                    chkAutoConfrim.Disabled = false;
                    chkEmailNotification.Disabled = false;
                    chkDiaryOverDueNotify.Disabled = false;
                }
                else if (mailEnabled != null && (mailEnabled.InnerText.ToLower() == "false"))
                {
                    chkAutoConfrim.Disabled = true;
                    chkEmailNotification.Disabled = true;
                    chkDiaryOverDueNotify.Disabled = true;
                }
                //dvatsa JIRA 11627(end)
                if (diaryDoc.SelectSingleNode("//GetUsersResult/@ScriptFired").InnerText == "true")
                {
                    txtSubject.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@TaskSubject").InnerText;
                    txtNotes.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@TaskNotes").InnerText;
                    txtDueDate.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@CompleteDate").InnerText;
                    txtDueTime.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@CompleteTime").InnerText;
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@AttachRecord").InnerText.IndexOf('(') != 0)
                    { txtClaimNumber.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@AttachRecord").InnerText; }
                    ddlPriority.SelectedValue = diaryDoc.SelectSingleNode("//GetUsersResult/@Priority").InnerText;

                    if (!string.IsNullOrEmpty(diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUser").InnerText))
                    {

                        if (string.IsNullOrEmpty(UserStr.Value))
                        {
                            UserStr.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUser").InnerText;
                        }
                        else
                        {
                            UserStr.Value = UserStr.Value + ' ' + diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUser").InnerText;
                        }

                    }
                    if (!string.IsNullOrEmpty(diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUserID").InnerText))
                    {
                        if (string.IsNullOrEmpty(UserIdStr.Value))
                        {
                            UserIdStr.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUserID").InnerText;
                        }
                        else
                        {
                            UserIdStr.Value = UserIdStr.Value + ' ' + diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUserID").InnerText;
                        }
                    }
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@EstimateTime").InnerText != "0")
                    {
                        txtEstTime.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@EstimateTime").InnerText;
                    }
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@NotifyFlag").InnerText.ToLower() == "true")
                    {
                        chkEmailNotification.Checked = true;
                    }
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@AutoConfirm").InnerText.ToLower() == "true")
                    {
                        chkAutoConfrim.Checked = true;
                    }
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@OverDueDiaryNotify").InnerText.ToLower() == "true")
                    {
                        chkDiaryOverDueNotify.Checked = true;
                    }
                    //igupta3 Jira-439
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@NonRoutableFlag").InnerText.ToLower() == "true")
                    {
                        chkNotRoutable.Checked = true;
                    }
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@NonRollableFlag").InnerText.ToLower() == "true")
                    {
                        chkNotRollable.Checked = true;
                    }
                }

                if (!string.IsNullOrEmpty(UserStr.Value))
                {
                    string[] arr = UserStr.Value.Split(new char[] { ' ' });
                    for (iIterator = 0; iIterator < arr.Length; iIterator++)
                    {
                        lstUsers.Items.Add(arr[iIterator]);
                    }
                }
                //zmohammad MITs 33655 Diary Initialisation script : End
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlDocument SaveDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            
            try
            {
                serviceMethodToCall = "WPAAdaptor.SaveDiary";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForSaveDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetInputDocForSaveDiary()
        {
            XmlDocument saveDiaryDoc = null;
            XmlNode saveDiaryNode = null;
            
            XmlNode entryIdNode = null;
            XmlNode taskSubjectNode = null;
            XmlNode assigningUserNode = null;
            XmlNode assignedUserNode = null;
            XmlNode statusOpenNode = null;
            XmlNode dueDateNode = null;
            XmlNode completeTimeNode = null;
            XmlNode responseDateNode = null;
            XmlNode responseNode = null;
            XmlNode teTrackedNode = null;
            XmlNode teTotalHoursNode = null;
            XmlNode teExpensesNode = null;
            XmlNode teEndTimeNode = null;
            XmlNode startTimeNode = null;
            XmlNode taskNotesNode = null;
            XmlNode actionNode = null;
            XmlNode estimateTimeNode = null;
            XmlNode priorityNode = null;
            XmlNode issueCompletionNode = null;
            XmlNode issueDateTextNode = null;
            XmlNode notifyFlagNode = null;
            XmlNode attachRecordIdNode = null;
            XmlNode attachTableNode = null;
            
            XmlNode autoConfirmNode = null;
            XmlNode activityStringNode = null;
            //07/07/2011 MDHAMIJA : WPA Email Notification
            XmlNode notifyByMailNode = null;
            //07/07/2011 MDHAMIJA : WPA Email Notification End
            XmlNode sendOverDueDiaryNotification = null;
            //igupta3 jira 439
            XmlNode nonRoutable = null;
            XmlNode nonRollable = null;

            saveDiaryDoc = new XmlDocument();
            saveDiaryNode = saveDiaryDoc.CreateElement("SaveDiary");

            entryIdNode = saveDiaryDoc.CreateElement("EntryId");
            entryIdNode.InnerText = "";
            saveDiaryNode.AppendChild(entryIdNode);

            taskSubjectNode = saveDiaryDoc.CreateElement("TaskSubject");
            taskSubjectNode.InnerText = txtSubject.Value;
            saveDiaryNode.AppendChild(taskSubjectNode);

            assigningUserNode = saveDiaryDoc.CreateElement("AssigningUser");
            assigningUserNode.InnerText = "currentuser";
            saveDiaryNode.AppendChild(assigningUserNode);

            assignedUserNode = saveDiaryDoc.CreateElement("AssignedUser");
            assignedUserNode.InnerText = UserStr.Value.ToString();  
            saveDiaryNode.AppendChild(assignedUserNode);

            statusOpenNode = saveDiaryDoc.CreateElement("StatusOpen");
            statusOpenNode.InnerText = "1";
            saveDiaryNode.AppendChild(statusOpenNode);


            teTrackedNode = saveDiaryDoc.CreateElement("TeTracked");
            teTrackedNode.InnerText = "";
            saveDiaryNode.AppendChild(teTrackedNode);

            teTotalHoursNode = saveDiaryDoc.CreateElement("TeTotalHours");
            teTotalHoursNode.InnerText = "";
            saveDiaryNode.AppendChild(teTotalHoursNode);

            teExpensesNode = saveDiaryDoc.CreateElement("TeExpenses");
            teExpensesNode.InnerText = "";
            saveDiaryNode.AppendChild(teExpensesNode);

            startTimeNode = saveDiaryDoc.CreateElement("TeStartTime");
            startTimeNode.InnerText = "";
            saveDiaryNode.AppendChild(startTimeNode);
            
            teEndTimeNode = saveDiaryDoc.CreateElement("TeEndTime");
            teEndTimeNode.InnerText = "";
            saveDiaryNode.AppendChild(teEndTimeNode);

            responseDateNode = saveDiaryDoc.CreateElement("ResponseDate");
            responseDateNode.InnerText = "";
            saveDiaryNode.AppendChild(responseDateNode);

            responseNode = saveDiaryDoc.CreateElement("Response");
            responseNode.InnerText = "";
            saveDiaryNode.AppendChild(responseNode);

            taskNotesNode = saveDiaryDoc.CreateElement("TaskNotes");
            taskNotesNode.InnerText = txtNotes.Value;
            saveDiaryNode.AppendChild(taskNotesNode);

            dueDateNode = saveDiaryDoc.CreateElement("DueDate");
            //dueDateNode.InnerText = txtDueDate.Value;
            dueDateNode.InnerText = AppHelper.GetRMDate(txtDueDate.Value);  //Aman MITS 31001
            saveDiaryNode.AppendChild(dueDateNode);

            completeTimeNode = saveDiaryDoc.CreateElement("CompleteTime");
            completeTimeNode.InnerText = txtDueTime.Value;
            saveDiaryNode.AppendChild(completeTimeNode);

            estimateTimeNode = saveDiaryDoc.CreateElement("EstimateTime");
            estimateTimeNode.InnerText = txtEstTime.Value;
            saveDiaryNode.AppendChild(estimateTimeNode);

            priorityNode = saveDiaryDoc.CreateElement("Priority");
            priorityNode.InnerText = ddlPriority.SelectedItem.Value;
            saveDiaryNode.AppendChild(priorityNode);
            

            activityStringNode = saveDiaryDoc.CreateElement("ActivityString");
            activityStringNode.InnerText = actstring.Value;
            saveDiaryNode.AppendChild(activityStringNode);

            if (optIssue2.Checked == true)
            {
                issueCompletionNode = saveDiaryDoc.CreateElement("IssueCompletion");
                issueCompletionNode.InnerText = "2";
                saveDiaryNode.AppendChild(issueCompletionNode);

                issueDateTextNode = saveDiaryDoc.CreateElement("IssueDateText");
                issueDateTextNode.InnerText = txtIssuePeriod.Value;
                saveDiaryNode.AppendChild(issueDateTextNode);
            }

            autoConfirmNode = saveDiaryDoc.CreateElement("AutoConfirm");
            notifyFlagNode = saveDiaryDoc.CreateElement("NotifyFlag");
            //07/07/2011 MDHAMIJA : WPA Email Notification
            notifyByMailNode = saveDiaryDoc.CreateElement("EmailNotifyFlag");
            //07/07/2011 MDHAMIJA : WPA Email Notification End
            if (chkAutoConfrim.Checked == true)
            {
                autoConfirmNode.InnerText = "true";
                notifyFlagNode.InnerText = "true";
            }
            else
            {
                autoConfirmNode.InnerText = "";
                notifyFlagNode.InnerText = "";
            }

            saveDiaryNode.AppendChild(autoConfirmNode);
            saveDiaryNode.AppendChild(notifyFlagNode);
            //07/07/2011 MDHAMIJA : WPA Email Notification
            if (chkEmailNotification.Checked)
            {
                notifyByMailNode.InnerText = "true";
            }
            else
            {
                notifyByMailNode.InnerText = "";
            }
            saveDiaryNode.AppendChild(notifyByMailNode);
            //07/07/2011 MDHAMIJA : WPA Email Notification End
            //added by amitosh for QBE Enhancement of OverDueDiary Notification
            sendOverDueDiaryNotification = saveDiaryDoc.CreateElement("OverDueDiaryNotify");
            if (chkDiaryOverDueNotify.Checked)
            {
                sendOverDueDiaryNotification.InnerText = "true";
            }
            else
            {
                sendOverDueDiaryNotification.InnerText = "";
            }
            saveDiaryNode.AppendChild(sendOverDueDiaryNotification);
            //End Amitosh

            //igupta3 Jira-439
            nonRoutable = saveDiaryDoc.CreateElement("NonRoutableFlag");
            if (chkNotRoutable.Checked)
            {
                nonRoutable.InnerText = "true";
            }
            else
            {
                nonRoutable.InnerText = "";
            }
            saveDiaryNode.AppendChild(nonRoutable);
            nonRollable = saveDiaryDoc.CreateElement("NonRollableFlag");
            if (chkNotRollable.Checked)
            {
                nonRollable.InnerText = "true";
            }
            else
            {
                nonRollable.InnerText = "";
            }
            saveDiaryNode.AppendChild(nonRollable);
            //igupta3 ends

            if (txtClaimNumber_cid.Value.Trim() != string.Empty)
            {
                attachRecordIdNode = saveDiaryDoc.CreateElement("AttachRecordId");
                attachRecordIdNode.InnerText = txtClaimNumber_cid.Value;
                saveDiaryNode.AppendChild(attachRecordIdNode);

                attachTableNode = saveDiaryDoc.CreateElement("AttachTable");
                attachTableNode.InnerText = "CLAIM";
                saveDiaryNode.AppendChild(attachTableNode);
            }
            
            actionNode = saveDiaryDoc.CreateElement("Action");
            actionNode.InnerText = "Create";
            saveDiaryNode.AppendChild(actionNode);

            saveDiaryDoc.AppendChild(saveDiaryNode);

            return saveDiaryNode;
        }
        
        private XmlNode GetInputDocForLoadDiary()
        {
            XmlDocument loadDiaryDoc = null;
            XmlNode loadDiaryNode = null;
            
            try
            {
                loadDiaryDoc = new XmlDocument();
                loadDiaryNode = loadDiaryDoc.CreateElement("Diary");
                
                loadDiaryDoc.AppendChild(loadDiaryNode);
                return loadDiaryNode;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            XmlDocument diaryDoc = null;
            string isDiaryCalPeek = string.Empty;
            string isDiaryCalProssOffice = string.Empty;
            string diaryListUrl = string.Empty;
            string sDiaryError = string.Empty;

            try
            {
                diaryDoc = SaveDiary();

                //if diarydoc has success message then redirect to diarylist 
                //else throw new execption with error messge in it
               
                if (diaryDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                {
                    if (Request.QueryString["isdiarycalendar"] != null)
                    {
                        if (Request.QueryString["isdiarycalendar"] == "true")
                        {
                            isDiaryCalPeek = Request.QueryString["isdiarycalendarpeek"];
                            isDiaryCalProssOffice = Request.QueryString["isdiarycalendarprocessingoffice"];

                            diaryListUrl = "DiaryCalendar/DiaryCalendar.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&IsPeek=" + isDiaryCalPeek + "&IsProcessingOffice=" + isDiaryCalProssOffice;
                            Response.Redirect(diaryListUrl, false);
                        }
                    }
                    //nsachdeva2 15 Sep 2011: MITS 25643
                    else if (Request.QueryString["RtnClaimScreen"] != null)
                    {
                        this.Page.ClientScript.RegisterStartupScript(typeof(string), "CreateDiary", " if(window.opener.document.getElementById('containsopendiaries')!=null) window.opener.document.getElementById('containsopendiaries').value = true; if(window.opener.document.forms[0].id='frmDiaryList') {window.opener.document.forms[0].__EVENTTARGET.value = 'creatediary';window.opener.document.forms[0].submit();} self.close();", true);
                    }
                    //End MITS: 25643
                    else
                    {
                        diaryListUrl = "DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString();
                        Server.Transfer(diaryListUrl,false);
                    }
                }
                else
                {
                    //srajindersin MITS 26319 04/20/2012
                    ErrorControl.errorDom = diaryDoc.InnerXml;
                    //throw new ApplicationException(diaryDoc.InnerXml);
                    //srajindersin MITS 26319 04/20/2012

                    //Add by kuladeep ---If error come in that case mentain UserList data MITS:35142 Start
                    string script = "<script>OnPostbackLoadUserData();</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "FillUserData", script);
                    //Add by kuladeep ---If error come in that case mentain UserList data MITS:35142 End

                }

            }

            catch (ApplicationException ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);              
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string isDiaryCalPeek = string.Empty;
            string isDiaryCalProssOffice = string.Empty;
            string diaryListUrl = string.Empty;

            try
            {
                if (Request.QueryString["isdiarycalendar"] != null)
                {
                    if (Request.QueryString["isdiarycalendar"] == "true")
                    {
                        isDiaryCalPeek = Request.QueryString["isdiarycalendarpeek"];
                        isDiaryCalProssOffice = Request.QueryString["isdiarycalendarprocessingoffice"];

                        diaryListUrl = "DiaryCalendar/DiaryCalendar.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&IsPeek=" + isDiaryCalPeek + "&IsProcessingOffice=" + isDiaryCalProssOffice;
                        Response.Redirect(diaryListUrl, false);
                    }
                }
                //nsachdeva2 15 Sep 2011: MITS 25643
                else if (Request.QueryString["RtnClaimScreen"] != null)
                {
                    this.Page.ClientScript.RegisterStartupScript(typeof(string), "CreateDiary", " if(window.opener.document.getElementById('containsopendiaries')!=null) window.opener.document.getElementById('containsopendiaries').value = true; if(window.opener.document.forms[0].id='frmDiaryList') {window.opener.document.forms[0].__EVENTTARGET.value = 'creatediary';window.opener.document.forms[0].submit();} self.close();", true);
                }
                //End MITS: 25643
                else
                {
                    diaryListUrl = "DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString();
                    Server.Transfer(diaryListUrl, false);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

    }
}
