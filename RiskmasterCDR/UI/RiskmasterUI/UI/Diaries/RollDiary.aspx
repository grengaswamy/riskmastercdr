﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RollDiary.aspx.cs" Inherits="Riskmaster.UI.Diaries.RollDiary" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Roll Diary</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    
    <script src="../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
     <script src="../../Scripts/form.js" type="text/javascript"></script>

    
    <script language="javascript" type="text/javascript">


        function ValForm() 
		{            
						if(replace(document.forms[0].txtRollDate.value," ","")=="")
						{
						    //alert("Please enter the roll date.");
						    alert(RollDiaryValidations.EnterRollDate);
							document.forms[0].txtRollDate.focus();
							return false;
			}

						//alert(document.forms[0].createdate.value);
			//objcreatedate = new Date(document.forms[0].createdate.value);
            //Aman MITS 31225
			var target = $("#txtRollDate");
			var inst = $.datepicker._getInst(target[0]);

			var Date1 = new Date(document.forms[0].createdate.value);
			
			fdate = Date1.getDate();
			fmonth = Date1.getMonth();
			fyear = Date1.getFullYear();

			secdate = inst.selectedDay; 
			secmonth = inst.selectedMonth;
			secyear = inst.selectedYear;			
			var objcreatedate = new Date();
			objcreatedate.setFullYear(fyear, fmonth, fdate);

			var objrolldate = new Date();
			objrolldate.setFullYear(secyear, secmonth, secdate);
			
						//alert(objcurrentdate);
			            //objrolldate = new Date(document.forms[0].txtRollDate.value);
			 //Aman MITS 31225
						
						datediff = new Date();
						datediff = objrolldate - objcreatedate;
						if(datediff < 0) 
						{
						    //alert("Invalid Date. Roll Date must be greater than Creation Date of diary which is " + document.forms[0].createdate.value);
						    alert(RollDiaryValidations.InvalidDate  + " " + document.forms[0].culCreateDate.value);
							document.forms[0].txtRollDate.focus();
							return false;
						}
						return true;
						
						
					}
					function replace(sSource, sSearchFor, sReplaceWith)
					{
						var arr = new Array();
						arr=sSource.split(sSearchFor);
						return arr.join(sReplaceWith);
					}
    </script>
</head>
<body>
    <form id="frmData" runat="server">
        <table border="0" align="center">
         <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl" runat="server" />
                <input type="hidden"  runat="server" value="" id="actstring"/><%--Mits 15176:Asif--%>
            </td>
        </tr>
         <tr>
                 <td colspan="2" align="left">
                   <asp:ImageButton ID="btnRoll" ImageUrl="~/Images/tb_diaryroll_active.png" class="bold" ToolTip="<%$ Resources:ttRoll %>"
                        runat="server" OnClientClick="return ValForm();;" OnClick="btnRoll_Click" />
                   <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                        runat="server" OnClick="btnCancel_Click" />
                 </td>
            </tr>
        <tr>
            <td colspan="2" class="ctrlgroup">
             <asp:Label ID="lblRolDiaryResrc" runat="server" Text="<%$ Resources:lblRolDiaryResrc %>"></asp:Label> 
          </td>
        </tr>
        <tr>
        <td class="required">
         <asp:Label ID="lblRollDateResrc" class="required" runat="server" Text="<%$ Resources:lblRollDateResrc %>"></asp:Label> 
     
        </td>
            <td class="required" align="left">
                <input type="text" value="" id="txtRollDate" runat="server"  size="15" onblur="dateLostFocus(this.id);"/>
               
                <script type="text/javascript">
                    $(function () {
                        $("#txtRollDate").datepicker({
                            showOn: "button",
                            buttonImage: "../../Images/calendar.gif",
                            buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        });
                    });
                       </script>
                <input type="hidden" runat="server" id="createdate" value=""/>
                 <input type="hidden" runat="server" id="culCreateDate" value=""/>
		    </td>
        </tr>
        <%--<tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnRoll" runat="server" CssClass="button" Text="Roll" 
                    OnClientClick = "return ValForm();;" onclick="btnRoll_Click" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" 
                    onclick="btnCancel_Click" />
            </td>
        </tr>--%>
        </table>
    </form>
</body>
</html>
