﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using System.Globalization;

namespace Riskmaster.UI.Diaries
{
    public partial class RollDiary : System.Web.UI.Page
    {
        XmlDocument diaryDoc = null;//Asif Mits 15176 
        XmlNode loadDiaryNode = null;//Asif Mits 15176 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("RollDiary.aspx"), "RollDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "RollDiaryValidations", sValidationResources, true);

                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }

                if (!IsPostBack)
                {
                    if (Request.QueryString["entryid"] != null)
                    {
                        ViewState["entryid"] = Request.QueryString["entryid"];
                    }
                    else
                    {
                        ViewState["entryid"] = "";
                    }

                    if (Request.QueryString["assigninguser"] != null)
                    {
                        ViewState["assigninguser"] = Request.QueryString["assigninguser"];
                    }
                    else
                    {
                        ViewState["assigninguser"] = "";
                    }
                    //Aman MITS 31225--Start
                    if (Request.QueryString["creationdate"] != null)
                    {
                        //createdate.Value = Request.QueryString["creationdate"];
                       
                        DateTimeFormatInfo fmt = (new CultureInfo("en-US")).DateTimeFormat;
                        DateTime dt = DateTime.Parse((Request.QueryString["creationdate"]),fmt );
                        createdate.Value = dt.ToString("MM") + "/" + dt.ToString("dd") + "/" + dt.Year.ToString();
                        culCreateDate.Value = AppHelper.GetDate(Request.QueryString["creationdate"]);
                    }
                    //Aman MITS 31225--End
                    if (Request.QueryString["assigneduser"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }

                    //pkandhari Jira 6412 starts
                    if (Request.QueryString["assignedgroup"] != null)
                    {
                        ViewState["assignedgroup"] = Request.QueryString["assignedgroup"];
                    }
                    else
                    {
                        ViewState["assignedgroup"] = "";
                    }
                    //pkandhari Jira 6412 ends

                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";//"EVENT";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";// "1";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }
                    //Mits 15176:Asif start
                    diaryDoc = LoadDiary();
                    loadDiaryNode = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary");

                    if (loadDiaryNode != null)
                    {
                        PopulateFormControls(loadDiaryNode);
                    }
                    //Mits 15176:Asif end
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //mits 15176:  Asif start
        private void PopulateFormControls(XmlNode loadDiaryNode)
        {
            XmlNodeList activitiesList = null;
            activitiesList = loadDiaryNode.SelectNodes("Activities/Activity");

            if (activitiesList.Count > 0)
            {
                actstring.Value = loadDiaryNode.SelectSingleNode("Activities/sActs").InnerText;
            }
        }
        private XmlDocument LoadDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "WPAAdaptor.LoadDiary";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForLoadDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);

                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetInputDocForLoadDiary()
        {
            XmlDocument loadDiaryDoc = null;

            XmlNode loadDiaryNode = null;
            XmlAttribute atbBase = null;
            XmlAttribute atbEntryId = null;
            XmlAttribute atbNotify = null;
            XmlAttribute atbRoot = null;
            XmlAttribute atbAssignedGroup = null; //pkandhari Jira 6412
            XmlAttribute atbAssignedUser = null; //pkandhari Jira 6412

            try
            {
                loadDiaryDoc = new XmlDocument();
                loadDiaryNode = loadDiaryDoc.CreateElement("LoadDiary");

                atbBase = loadDiaryDoc.CreateAttribute("base");
                atbBase.Value = "";

                atbEntryId = loadDiaryDoc.CreateAttribute("entryid");
                atbEntryId.Value = ViewState["entryid"].ToString();

                atbNotify = loadDiaryDoc.CreateAttribute("notify");
                atbNotify.Value = "";

                atbRoot = loadDiaryDoc.CreateAttribute("root");
                atbRoot.Value = "";

                atbAssignedGroup = loadDiaryDoc.CreateAttribute("AssignedGroup");//pkandhari Jira 6412
                atbAssignedGroup.Value = "";//pkandhari Jira 6412

                atbAssignedUser = loadDiaryDoc.CreateAttribute("AssignedUser");//pkandhari Jira 6412
                atbAssignedUser.Value = "";//pkandhari Jira 6412

                loadDiaryNode.Attributes.Append(atbBase);
                loadDiaryNode.Attributes.Append(atbEntryId);
                loadDiaryNode.Attributes.Append(atbNotify);
                loadDiaryNode.Attributes.Append(atbRoot);
                loadDiaryNode.Attributes.Append(atbAssignedGroup); //pkandhari Jira 6412
                loadDiaryNode.Attributes.Append(atbAssignedUser); //pkandhari Jira 6412

                loadDiaryDoc.AppendChild(loadDiaryNode);
                return loadDiaryNode;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        //mits 15176 :Asif end
        private XmlDocument RollCurrentDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "WPAAdaptor.SaveDiary";

                inputDocNode = GetInputDocForRollDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetInputDocForRollDiary()
        {
            XmlDocument rollDairyDoc = null;
            XmlNode rollDiaryNode = null;
            XmlNode entryIdNode = null;
            XmlNode assigningUserNode = null;
            XmlNode assignedgroupNode = null; //pkandhari Jira 6412
            XmlNode assigneduserNode = null; //pkandhari Jira 6412
            XmlNode dueDateUserNode = null;
            XmlNode activityStringNode = null;
            XmlNode actionNode = null;

            rollDairyDoc = new XmlDocument();

            rollDiaryNode = rollDairyDoc.CreateElement("SaveDiary");

            entryIdNode = rollDairyDoc.CreateElement("EntryId");
            entryIdNode.InnerText = ViewState["entryid"].ToString();
            rollDiaryNode.AppendChild(entryIdNode);

            assigningUserNode = rollDairyDoc.CreateElement("AssigningUser");
            assigningUserNode.InnerText = ViewState["assigninguser"].ToString();
            rollDiaryNode.AppendChild(assigningUserNode);

            //pkandhari Jira 6412 starts
            assigneduserNode = rollDairyDoc.CreateElement("AssignedUser");
            assigneduserNode.InnerText = ViewState["assigneduser"].ToString();
            rollDiaryNode.AppendChild(assigneduserNode);

            assignedgroupNode = rollDairyDoc.CreateElement("AssignedGroup");
            assignedgroupNode.InnerText = ViewState["assignedgroup"].ToString();
            rollDiaryNode.AppendChild(assignedgroupNode);
            //pkandhari Jira 6412 ends

            dueDateUserNode = rollDairyDoc.CreateElement("DueDate");
            dueDateUserNode.InnerText = AppHelper.GetRMDate(txtRollDate.Value);//Deb ML changes
            rollDiaryNode.AppendChild(dueDateUserNode);

            activityStringNode = rollDairyDoc.CreateElement("ActivityString");
            activityStringNode.InnerText = actstring.Value;//Asif Mits 15176   
            rollDiaryNode.AppendChild(activityStringNode);

            actionNode = rollDairyDoc.CreateElement("Action");
            actionNode.InnerText = "Roll";
            rollDiaryNode.AppendChild(actionNode);

            rollDairyDoc.AppendChild(rollDiaryNode);

            return rollDiaryNode;
        }

        protected void btnRoll_Click(object sender, EventArgs e)
        {
            XmlDocument responseDoc = null;

            try
            {
                responseDoc = RollCurrentDiary();

                if (responseDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                {
                    Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
                }
                else
                {
                    throw new ApplicationException(responseDoc.InnerXml);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
