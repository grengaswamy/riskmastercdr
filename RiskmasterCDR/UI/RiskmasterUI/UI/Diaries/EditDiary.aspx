﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditDiary.aspx.cs" Inherits="Riskmaster.UI.Diaries.EditDiary" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
    <title>Edit Diary</title>
    <link href="../../Content/dhtml-combo.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    
    <script src="../../Scripts/cul.js" type="text/javascript"></script>
    <script src="../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../Scripts/DcomboBox.js" type="text/javascript"></script>
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    <script src="../../Scripts/m-x.js" type="text/javascript"></script>
     <!--Rakhel ML Changes!-->
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <!--Rakhel ML Changes!-->

    
    <script type="text/javascript">
        function DisableDateTime()
        {            
            var checkbox = document.getElementById('chkNotRollable');
            if (checkbox.checked)
            {
                $('#txtDueDate').prop('disabled', true);
                $('#txtDueDate').datepicker().datepicker('disable');
                $('#txtDueTime').prop('disabled', true);
                
            }
            else {
                $('#txtDueDate').prop('disabled', false);
                $('#txtDueDate').datepicker().datepicker('enable');
                $('#txtDueTime').prop('disabled', false);
            }
            document.getElementById('dueDateValue').value = document.getElementById('txtDueDate').value;
            document.getElementById('dueTimeValue').value = document.getElementById('txtDueTime').value;
        }
        var _isSelect = false;
        function pageLoaded() {
            var i;
            DisableDateTime();
            for (i = 0; i < document.forms[0].length; i++) {
                if ((document.forms[0].item(i).type == "text") || (document.forms[0].item(i).type == "select-one") || (document.forms[0].item(i).type == "textarea")) {
                    document.forms[0].item(i).focus();
                    break;
                }
            }
            if (_isSelect) {
                //            MGaba2: MITS 14739
                _offset = 0;
                ComboInit('txtSubject');
                arrAllComboBoxes.push('txtSubject');
            }
        }

        function replace(sSource, sSearchFor, sReplaceWith) {
            var arr = new Array();
            arr = sSource.split(sSearchFor);
            return arr.join(sReplaceWith);
        }
        function AddActivity() {

            if (m_codeWindow != null)
                m_codeWindow.close();

            document.onCodeClose = onCodeClose;
            document.OnAddActivity = OnAddActivity;
            m_codeWindow = window.open('/RiskmasterUI/UI/Diaries/DiaryActivity.aspx', 'codeWnd',
						'width=400,height=200' + ',top=' + (screen.availHeight - 200) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=yes,scrollbars=yes');
            return false;
        }

        function OnAddActivity(sId, sText) {

            if (m_codeWindow != null)
                m_codeWindow.close();
            m_codeWindow = null;

            var objCtrl = document.forms[0].lstActivities;
            var bAdd = true;
            for (var i = 0; i < objCtrl.length; i++) {
                //MGaba2:MITS 20190:multiple free text were not getting added to work activity
                //In case of free text sId is ""  and objCtrl.options[i].value is also "".Hence previous condition was resulting true
                //in case multiple freetext field are added 
                //if(objCtrl.options[i].value == sId && sId != "0")
                if ((sId == "" && trim(objCtrl.options[i].innerText.toLowerCase() || objCtrl.options[i].textContent.toLowerCase()) == trim(sText).toLowerCase()) || (sId != "" && objCtrl.options[i].value == sId && sId != "0")) {
                    bAdd = false;
                }
            }

            if (bAdd) {
                var objOption = new Option(sText, sId, false, false);
                objCtrl.options[objCtrl.length] = objOption;
                objCtrl = null;
                //Parijat: Mits 9390 an extension to it .
                //Actually implemeting Mits 8623 where it was not completely done, Now adding '|' in place of ','.
                if (document.forms[0].txtActivities.value != "")
                    document.forms[0].txtActivities.value = document.forms[0].txtActivities.value + "|";
                document.forms[0].txtActivities.value = document.forms[0].txtActivities.value + sId + "|" + sText;
                document.forms[0].actstring.value = document.forms[0].txtActivities.value;
            }
        }

        function DelActivity() {
            var objCtrl = document.forms[0].lstActivities;
            if (objCtrl.selectedIndex < 0)
                return false;

            var bRepeat = true;
            while (bRepeat) {
                bRepeat = false;
                for (var i = 0; i < objCtrl.length; i++) {
                    // remove selected elements
                    if (objCtrl.options[i].selected) {
                        objCtrl.options[i] = null;
                        bRepeat = true;
                        break;
                    }
                }
            }
            // Now create ids list
            var sId = "";
            for (var i = 0; i < objCtrl.length; i++) {
                //Parijat: MIts 9390 extension to this .
                //Actually implemeting Mits 8623 where it was not completely done, Now adding '|' in place of ','.
                if (sId != "")
                    sId = sId + "|";
                sId = sId + objCtrl.options[i].value + "|" + replace(objCtrl.options[i].text, "|", "|");
            }
            objCtrl = null;
            document.forms[0].txtActivities.value = sId;
            document.forms[0].actstring.value = document.forms[0].txtActivities.value;
            return true;
        }

        // rrachev RMA 5018
        function IsFormValid() {
            var result = true;
            var objTaskName = $("#txtSubject")
            if (objTaskName.val() == "") {
                result = false;
                alert(EditDiaryValidations.ValidTaskNameCheck);
                objTaskName.focus();
            }
            return result;
        }
    </script>
</head>
    <body onload="pageLoaded();parent.MDIScreenLoaded();">
        <form id="frmData" runat="server">
         
            <uc1:ErrorControl ID="ErrorControl" runat="server" />
            <table border="0"  class="toolbar" cellpadding="0" cellspacing="0">
             <tr>
                 <td colspan="2" align="left">
                     <%--Ritesh:MITS 27323--"OnClientClick" removed--%>
                     <%--rrachev: RMA-5018--"OnClientClick" add--%>
                   <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_savemapping_active.png" class="bold" ToolTip="<%$ Resources:ttSave %>"
                        runat="server"  OnClick="btnSave_Click" OnClientClick="return IsFormValid();" />
                   <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                        runat="server" OnClick="btnCancel_Click" />
                 </td>
            </tr>
            </table>
            <table border="0" align="left" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="7" class="ctrlgroup"><asp:Label ID="lblEditDiary" runat="server" Text="<%$ Resources:lblEditDiary %>"></asp:Label></td>
                </tr>

                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td class="required" align="left" >
                        <label><asp:Label ID="lblTaskName" runat="server" Text="<%$ Resources:lblTaskName %>"></asp:Label></label>
                    </td>
                    <td style="width:400px;">                    
                        <table border="0">
                            <tr>
                                <td>
                                    <input type="text" value="" maxlength="50"  runat="server" id="txtSubject" class="comboTxt" style="width:325px;"/>
                                    <asp:DropDownList  ID="txtSubjectDcboBox" CssClass="comboDropDown" runat="server" onchange="UpdateDCBoxGeneric(event);" Width="340px"></asp:DropDownList>
                                </td>
                           </tr>
                          </table>
                    </td>
                                    
                    <td align="left" ><asp:Label ID="lblDueDate" runat="server" Text="<%$ Resources:lblDueDate %>"></asp:Label>:</td>
                     <td>
                        <input type="text"  runat="server" id="txtDueDate" size="15" onblur="dateLostFocus(this.id);"/>
                        
                        <!-- Rakhel ML Changes -Start !-->
                        <script type="text/javascript">
                            $(function () {
                                $("#txtDueDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../Images/calendar.gif",
                                    buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                });
                            });
                       </script>
                        <!-- Rakhel ML Changes - End!-->

                        <input type="text"  runat="server" value="" id="txtDueTime" size="15" onblur="timeLostFocus(this.id);"/>
                    </td> 
                </tr>

                <tr>
                    <td  align="left" valign="middle"><asp:Label ID="lblAttachRec" runat="server" Text="<%$ Resources:lblAttachRec %>"></asp:Label>:</td>
                    <td>
                        <input type="text" value="" id="txtAttachRecord"  runat="server" disabled="true" size="50" onblur="dateLostFocus(this.id);"/>
                        <asp:Label ID="lblAttachRecord" runat="server"></asp:Label>
                    </td>
               
                     <td align="left" valign="top">
                        <label><asp:Label ID="lblWorkAct" runat="server" Text="<%$ Resources:lblWorkAct %>"></asp:Label>: </label>
                    </td>
                     <td>
                        <asp:ListBox ID="lstActivities" runat="server" Width="169px" SelectionMode="Multiple"></asp:ListBox>
                        <input type="button"  runat="server" class="CodeLookupControl" value="" id="cmdAddActivity" onclick="AddActivity()"/>
                        <input type="button"  runat="server" class="BtnRemove" value="-" id="cmdDelActivity" onclick="DelActivity()"/>
                       <input type="hidden"  runat="server" value="" id="actstring"/>
                       <input type="hidden" id="txtActivities" runat="server"  value=""/>
                         <input type="hidden" id="dueDateValue" runat="server"/>
                         <input type="hidden" id="dueTimeValue" runat="server"/>
                    </td>
                </tr>

                <tr>
                    <td align="left" valign="top">
                        <label><asp:Label ID="lblEstTime" runat="server" Text="<%$ Resources:lblEstTime %>"></asp:Label>:</label>
                    </td>
                    <td>
                        <input type="text" value="" id="txtEstTime" runat="server" size="14" onblur="numLostFocusNonNegative(this);" maxlength="14"/><%--amitosh for mits 28027--%>
                    </td>
                
                 <td align="left" valign="top">
                    <label><asp:Label ID="lblPriority" runat="server" Text="<%$ Resources:lblPriority %>"></asp:Label>: </label>
                </td>
                 <td  align="left">
                    <asp:DropDownList ID="ddlPriority" runat="server">
                          <asp:ListItem Text="<%$ Resources:Optional %>" Value="1"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Important %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Required %>" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                </tr>

                <tr>
                     <td align="left" valign="top">
                        <label><asp:Label ID="lblNotes" runat="server" Text="<%$ Resources:lblNotes %>"></asp:Label>:</label>
                    </td>
                     <td>
                        <textarea cols="50" wrap="soft" rows="4" id="txtNotes" runat="server"></textarea>
                    </td> 
                
                    <td>&nbsp;</td>
                    <td>
                        <input type="checkbox" value="true" runat="server" id="chkAutoConfrim" /><asp:Label ID="lblAutoNotif" runat="server" Text="<%$ Resources:lblAutoNotif %>"></asp:Label>
                    </td> 
                    </tr>
                <!--07/08/2011 MDHAMIJA: WPA Email Notification-->
                <tr>
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkEmailNotification" /><asp:Label ID="lblEmailNotif" runat="server" Text="<%$ Resources:lblEmailNotif %>"></asp:Label>
             </td>
            
            <!--07/08/2011 MDHAMIJA: End-->
                      
                  
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkDiaryOverDueNotify" /><asp:Label ID="lblAutoNotifLate" runat="server" Text="<%$ Resources:lblAutoNotifLate %>"></asp:Label>
             </td>
            </tr>
                <!--igupta3 jira 439-->
                <tr>                    
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkNotRoutable" /><asp:Label ID="lblNotRoutable" runat="server" Text="<%$ Resources:lblNotRoutable %>"></asp:Label>
                </td>                

                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkNotRollable" onclick="DisableDateTime();"/><asp:Label ID="lblNotRollable" runat="server" Text="<%$ Resources:lblNotRollable %>"></asp:Label>
             </td>
            </tr>
                <%--<tr>
                 <td colspan="2" align="center">
                    <asp:Button ID="btnSave" runat="server" CssClass="button"  Text="Save" 
                         onclick="btnSave_Click" Width="38px" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button"  Text="Cancel" 
                         onclick="btnCancel_Click" Width="45px" />
                </td>
                </tr>--%>
            </table>
        </form> 
    </body>
</html>
