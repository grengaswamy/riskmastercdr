﻿<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="true" CodeBehind="AttachDiary.aspx.cs" Inherits="Riskmaster.UI.Diaries.AttachDiary" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Create Diary</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/dhtml-combo.css" rel="stylesheet" type="text/css" />
   
    
    <script src="../../Scripts/cul.js" type="text/javascript"></script>
    <script src="../../Scripts/m-x.js" type="text/javascript"></script>
    <script src="../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    <script src="../../Scripts/DcomboBox.js" type="text/javascript"></script>
   
    <!--Praveen ML-->    
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
    <!--Praveen ML-->
    
    <script type="text/javascript">
         //MITS 27964 hlv 4/10/2012 begin
         if (window.opener != null) {
             window.opener.window.parent.parent.iwintype = document.title;
         }

         window.onunload = function () {
             if (window.opener != null) {
                 window.opener.window.parent.parent.iwintype = "";
             }
         }
         //MITS 27964 hlv 4/10/2012 end

          function ValForm()
          {        
                        if(document.forms[0].lstUsers.options.length<1)
					    {
							//alert("Please select the user to assign diary to.");
                            alert(AttachDiaryValidations.SelectUser); //Rakhel ML Changes
							return false;
 					    }
	
						if(replace(document.forms[0].txtSubject.value," ","")=="")
						{
							//alert("Please enter the Task Name.");
                            alert(AttachDiaryValidations.TaskName); //Rakhel ML Changes
							document.forms[0].txtSubject.focus();
							return false;
						}
	
						if(replace(document.forms[0].txtDueDate.value," ","")=="")
						{
							//ravi commented to match the functionality with rmworld-dont know whether its as it should be --4/22/03
							//alert("Please enter the due date.");
							//document.forms[0].txtDueDate.focus();
							//return false;
						}
	
						if(document.forms[0].optIssue2.checked)
						{
						    if(isNaN(parseInt(document.forms[0].txtIssuePeriod.value)))
						    
						    //if (isNaN(document.forms[0].txtIssuePeriod.value))
							{
								//MITS 22016: Raman Bhatia
							    //alert("Please enter a number into the Issue every n days until the completion date field.");
							    //alert("Please fill in a valid value in the days until completion date field to proceed !");
                                alert(AttachDiaryValidations.ValidValue); //Rakhel ML Changes
								document.forms[0].txtIssuePeriod.focus();
								return false;
							}
							
						}
						//Umesh fix for MITS 7887
						if(document.forms[0].optIssue2.checked)
						{
							if(parseInt(document.forms[0].txtIssuePeriod.value)<=0)
                            {
                                //alert("Please enter positive value.");
                                alert(AttachDiaryValidations.PosValue); //Rakhel ML Changes
                                document.forms[0].txtIssuePeriod.focus();
                                return false;
                            }

                        }
                        //end

          }

          function replace(sSource, sSearchFor, sReplaceWith)
          {
          var arr = new Array();
          arr=sSource.split(sSearchFor);
          return arr.join(sReplaceWith);
          }
          
	    function AddActivity()
	    {
		    if(m_codeWindow!=null)
		    m_codeWindow.close();

		    document.onCodeClose=onCodeClose;
		    document.OnAddActivity=OnAddActivity;
		    m_codeWindow=window.open('/RiskmasterUI/UI/Diaries/DiaryActivity.aspx','codeWnd',
		    'width=400,height=200'+',top='+(screen.availHeight-200)/2+',left='+(screen.availWidth-400)/2+',resizable=yes,scrollbars=yes');
		    return false;
	    }

        function OnAddActivity(sId,sText) {
	        if(m_codeWindow!=null)
	        m_codeWindow.close();
	        m_codeWindow=null;

	        var objCtrl=document.forms[0].lstActivities;
	        var bAdd=true;
	        for(var i=0;i<objCtrl.length;i++)
	        {
	            //MGaba2:MITS 20190:multiple free text were not getting added to work activity
	            //In case of free text sId is ""  and objCtrl.options[i].value is also "".Hence previous condition was resulting true
                //in case multiple freetext field are added 
	            //if(objCtrl.options[i].value == sId && sId != "0")
	            if ((sId == "" && trim(objCtrl.options[i].innerText.toLowerCase() || objCtrl.options[i].textContent.toLowerCase()) == trim(sText).toLowerCase()) || (sId != "" && objCtrl.options[i].value == sId && sId != "0"))
                {
	                bAdd = false;
	            }
            }
            if(bAdd)
            {
                var objOption =	new Option(sText, sId, false, false);
                objCtrl.options[objCtrl.length] = objOption;
                objCtrl=null;
                //Parijat: Mits 9390 an extension to it .
                //Actually implemeting Mits 8623 where it was not completely done, Now adding '|' in place of ','.
                if(document.forms[0].txtActivities.value!="")
                document.forms[0].txtActivities.value=document.forms[0].txtActivities.value+"|";
                document.forms[0].txtActivities.value=document.forms[0].txtActivities.value+sId+"|"+sText;
                document.forms[0].actstring.value = document.forms[0].txtActivities.value;
            }
        }

          function DelActivity()
          {
              var objCtrl=document.forms[0].lstActivities;
              if(objCtrl.selectedIndex<0)
			    return false;

			    var bRepeat=true;
			    while(bRepeat)
			    {
				    bRepeat=false;
				    for(var i=0;i<objCtrl.length;i++)
				    {
				    // remove selected elements
				    if(objCtrl.options[i].selected)
				    {
					    objCtrl.options[i]=null;
					    bRepeat=true;
					    break;
				    }
			    }
					    }	
		        // Now create ids list
		        var sId="";
		        for(var i=0;i<objCtrl.length;i++)
                  {
                  //Parijat: MIts 9390 extension to this .
                  //Actually implemeting Mits 8623 where it was not completely done, Now adding '|' in place of ','.
                  if(sId!="")
                      sId=sId+"|";
                  sId=sId+objCtrl.options[i].value+"|"+ replace(objCtrl.options[i].text, "|", "|");
                  }
                  objCtrl=null;
                  document.forms[0].txtActivities.value=sId;
                  document.forms[0].actstring.value = document.forms[0].txtActivities.value;
                  return true;
          }

          var _isSelect = false;
          function setDefaults()
          {
            document.forms[0].optIssue1.checked = true;
            if( document.forms[0].txtSubjectDcboBox != null)
            {
              _offset = 0;
              ComboInit('txtSubject');
              arrAllComboBoxes.push('txtSubject');
            }
			//Anjaneya : set txtDueDate to currentdate MITS 9621
//			var CompletedOnDate = new Date();
//			var curr_date = CompletedOnDate.getDate();
//			if( curr_date < 10 )
//				curr_date = "0" + curr_date;
//						
//			var curr_month = CompletedOnDate.getMonth();
//			curr_month += 1;
//			if( curr_month < 10 )
//				curr_month = "0" + curr_month;
//			var curr_year = CompletedOnDate.getFullYear();
         
			//document.forms[0].txtDueDate.value = curr_month + "/" + curr_date + "/" + curr_year;
		
//			var hours = CompletedOnDate.getHours()
//			var minutes = CompletedOnDate.getMinutes()
//			if (minutes < 10)
//			minutes = "0" + minutes
//			var time;
//			if(hours > 11)
//			{
//				time = "PM"
//			} 
//			else 
//			{
//				time = "AM"
//			}
            //document.forms[0].txtDueTime.value = hours + ":" + minutes + " " + time ;
          //if ((document.forms[0].DefaultAssignedTo != null) && (document.forms[0].DefaultAssignedTo.value == "True")) {
          //    objCtrl = document.getElementById('lstUsers');
          //    if (document.forms[0].LoginUserName.value != "" && objCtrl.length == 0) { //averma62 - MITS 31546
          //        objOption = new Option(document.forms[0].LoginUserName.value, document.forms[0].LoginID.value, false, false);
          //        objCtrl.options[objCtrl.length] = objOption;
          //        document.forms[0].UserStr.value = document.forms[0].LoginUserName.value;
          //        document.forms[0].UserIdStr.value = document.forms[0].LoginID.value;
          //        objCtrl = null;
          //    }
          //    zmohammad MITs 33655 Diary Initialisation script : Start
          //    else if (document.forms[0].LoginUserName.value != "" && document.forms[0].ScriptInit.value == "1") {
          //        objOption = new Option(document.forms[0].LoginUserName.value, document.forms[0].LoginID.value, false, false);
          //        objCtrl.options[objCtrl.length] = objOption;
          //        if (document.forms[0].UserStr.value != null && document.forms[0].UserStr.value != "") {
          //            document.forms[0].UserStr.value = document.forms[0].UserStr.value + ' ' + document.forms[0].LoginUserName.value;
          //        }
          //        else {
          //            document.forms[0].UserStr.value = document.forms[0].LoginUserName.value;
          //        }
          //        if (document.forms[0].UserIdStr.value != null && document.forms[0].UserIdStr.value != "") {
          //            document.forms[0].UserIdStr.value = document.forms[0].UserIdStr.value + ' ' + document.forms[0].LoginID.value;
          //        }
          //        else {
          //            document.forms[0].UserIdStr.value = document.forms[0].LoginID.value;
          //        }
          //        objCtrl = null;
          //    }
          //    zmohammad MITs 33655 Diary Initialisation script : End
          //  }
          }

          //Start - averma62 - MITS 31546
          function setCheckbox(checkboxname) {
          document.getElementById('hdnIssueChkBx').value = checkboxname;
          }

          function check() {
              if (document.getElementById('hdnIssueChkBx').value == '')
                  document.getElementById('hdnIssueChkBx').value = 'optIssue1';

		    if (document.getElementById('hdnIssueChkBx').value == 'optIssue1')
			{
			    document.forms[0].txtIssuePeriod.disabled = true;
			    document.forms[0].txtIssuePeriod.value = "";
			}
			else if (document.getElementById('hdnIssueChkBx').value == 'optIssue2')
			{
			  document.forms[0].optIssue2.checked = true;
              document.forms[0].txtIssuePeriod.disabled = false;
			}
		  }
		  //End - averma62 - MITS 31546

		  function AddCustomizeUser()
		  {
		      document.getElementById('lstUsers').add(new Option("CSC","csc"));
		      document.getElementById('lstUsers').add(new Option("cul1","cul1"));
		      document.getElementById('lstUsers').add(new Option("cul2","cul2"));
		      document.getElementById('lstUsers').add(new Option("cul3","cul3"));
		      
		      document.getElementById('UserIdStr').value = "cul1 cull3 cul4";
		      
		      return false;
		}
		
        //<!--mbahl3 mit:17614 date:7/10/2011: start -->
        function LostFocus() {
		    var odb = document.getElementById('txtDueDate');
		    odb.focus();
		    return false;
		}

        //<!--mbahl3 mit:17614 date:7/10/2011: end-->
        </script>
    
    <style type="text/css">
        .style1
        {
            FONT-WEIGHT: bold;
        }
    </style>
    
</head>
<body onload="setDefaults();check();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <table border="0" align="left">
            <tr>
                <td colspan="2" class="ctrlgroup">
                  <asp:Label ID="lblCreateDiaryResrc" runat="server" Text="<%$ Resources:lblCreateDiaryResrc %>"></asp:Label> 
               </td>
            </tr>
            <tr>
                 <td class="required">
                  <asp:Label ID="lblAssignToResrc"  runat="server" Text="<%$ Resources:lblAssignToResrc %>"></asp:Label> 
                 </td>
                 <td class="style1">
                    <select multiple="true" id="lstUsers" size="3" runat="server" style="width: 190px; height: 71px">
                    </select><input type="button" class="CodeLookupControl" value="" id="cmdAddCustomizedListUser" style="width:21px" onclick="AddCustomizedListUser('creatediary','lstUsers','UserIdStr','UserStr');"/>
                    <input type="button" class="BtnRemove" value="-" id="cmdDelCustomizedListUser" style="width:21px" onclick="DelCustomizedListUser('creatediary','lstUsers','UserIdStr','UserStr')"/>
                    <input type="hidden" runat="server" name="" value="" id="UserIdStr"/>
                    <input type="hidden" runat="server" value="" id="UserStr"/>
                    <input type="hidden" runat="server" name="" value="" id="LoginUserName"/>
                    <input type="hidden" runat="server" value="" id="LoginID"/>
                    <input type="hidden" runat="server" value="False" id="DefaultAssignedTo"/>
                 </td>
            </tr>
            <tr>
                <td class="required">
                   <asp:Label ID="lblTaskNameResrc"  runat="server" Text="<%$ Resources:lblTaskNameResrc %>"></asp:Label> 
                </td>
                <td>
                    <input type="text" value="" maxlength="50"  runat="server" id="txtSubject" 
                        class="comboTxt" onblur="return LostFocus();" style="width:324px;"/> <!--mbahl3 mit:17614 date:7/10/2011:  -->
                    <asp:DropDownList  ID="txtSubjectDcboBox" CssClass="comboDropDown"  runat="server" onchange="UpdateDCBoxGeneric(event);" Width="340px"></asp:DropDownList>
                </td>
            </tr>
                <tr>
                    <td  align="left">
                     <asp:Label ID="lblDueDateTimeResrc"  runat="server" Text="<%$ Resources:lblDueDateTimeResrc %>"></asp:Label> 
                    </td>
                    <td>
                    <input type="text"  runat="server" id="txtDueDate" style="width:138px" size="15" onblur="dateLostFocus(this.id);"/>
                    <script type="text/javascript">
                        $(function () {
                            $("#txtDueDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                    </script>
                    <input type="text"  runat="server" value="" id="txtDueTime" size="15" onblur="timeLostFocus(this.id);"/>
	            </td>
            </tr>
            <tr>
                <td  valign="top" align="left">
                   <asp:Label ID="lblWorkActResrc"  runat="server" Text="<%$ Resources:lblWorkActResrc %>"></asp:Label> 
                   
                </td>
                <td>
                    <asp:ListBox ID="lstActivities" runat="server" Width="169px" SelectionMode="Multiple"></asp:ListBox>
                        <!--Changed by Saurabh P Arora for MITS 15158: Start-->
                    <input type="button"  runat="server" class="CodeLookupControl" value="" id="cmdAddActivity" onclick="AddActivity()"/>
<%--                     <input type="button"  runat="server" class="EllipsisControl" value="..." id="cmdAddActivity" onclick="AddActivity()"/>--%>
                         <!--Changed by Saurabh P Arora for MITS 15158: End-->
                    <input type="button"  runat="server" class="BtnRemove" value="-" id="cmdDelActivity" onclick="DelActivity()"/>
                    <input type="hidden"  runat="server" value="" id="actstring"/>
                    <input type="hidden" id="txtActivities" runat="server"  value=""/>
                </td>
            </tr>
            <tr>
                <td  align="right">
                  <asp:Label ID="lblEstTimeResrc"  runat="server" Text="<%$ Resources:lblEstTimeResrc %>"></asp:Label> 
                
                </td>
                <td>
                    <input type="text" value="" id="txtEstTime" runat="server" size="15" onblur="numLostFocusNonNegative(this);"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <input type="radio" value="1" onclick="setCheckbox('optIssue1'); check();" style="width:21px" id="optIssue1" runat="server"  
                        name="optIssue" checked="true" align="left"/>
                            <asp:Label ID="lblIssueOnCompDateResrc"  runat="server" Text="<%$ Resources:lblIssueOnCompDateResrc %>"></asp:Label> 
                      
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left" >
                    <input type="radio"  value="2"  runat="server" onclick="setCheckbox('optIssue2'); check();" style="width:21px" id="optIssue2"  name="optIssue" checked="true"/>
                     <asp:Label ID="lblIssueEveryResrc"  runat="server" Text="<%$ Resources:lblIssueEveryResrc %>"></asp:Label> 
                    <input type="text"  value="" runat="server"  style="width:86px" id="txtIssuePeriod"/>
                     <input type="hidden" id="hdnIssueChkBx" runat="server"  value=""/>
                   <asp:Label ID="lblDaysCompDateResrc"  runat="server" Text="<%$ Resources:lblDaysCompDateResrc %>"></asp:Label> 
                </td>
            </tr>
            <tr>
                 <td align="left">
                 <asp:Label ID="lblPriorityResrc"  runat="server" Text="<%$ Resources:lblPriorityResrc %>"></asp:Label> 
                 
                 </td>
             <td align="left">
                <asp:DropDownList ID="ddlPriority" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Optional %>" Value="1"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Important %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Required %>" Value="3"></asp:ListItem>
                </asp:DropDownList>
             </td>
            </tr>
            <tr>
                <td valign="top" align="left">
                   <asp:Label ID="lblNotesResrc"  runat="server" Text="<%$ Resources:lblNotesResrc %>"></asp:Label>  
                </td>
             <td align="left">
                <textarea cols="50" wrap="soft" rows="4" id="txtNotes" runat="server"></textarea>
            </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkAutoConfrim" />
                  <asp:Label ID="lblAutoNotiResrc"  runat="server" Text="<%$ Resources:lblAutoNotiResrc %>"></asp:Label>  
              
             </td>
            </tr>
            <!--07/08/2011 MDHAMIJA: WPA Email Notification-->
            <tr>
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkEmailNotification" />
                  <asp:Label ID="lblNotifyByEmailResrc"  runat="server" Text="<%$ Resources:lblNotifyByEmailResrc %>"></asp:Label>  
                
             </td>
            </tr>
              <tr>
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkDiaryOverDueNotify" />
                 <asp:Label ID="lblAutoNotiLateResrc"  runat="server" Text="<%$ Resources:lblAutoNotiLateResrc %>"></asp:Label>  
                
                
             </td>
            </tr>
            <tr>
                <!--Igupta3 Jira 439-->
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkNotRoutable" /><asp:Label ID="lblNotRoutable" runat="server" Text="<%$ Resources:lblNotRoutable %>"></asp:Label>
                </td>                
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkNotRollable" /><asp:Label ID="lblNotRollable" runat="server" Text="<%$ Resources:lblNotRollable %>"></asp:Label>
             </td>
            </tr>
            <!--07/08/2011 MDHAMIJA: End-->
             <!--Aman MITS 31225 removed the width attribute-->
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnSave" runat="server" CssClass="button"  Text="<%$ Resources:btnSave %>" 
                        OnClientClick = "return ValForm();; " onclick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button"  Text="<%$ Resources:btnCancel %>"  OnClientClick="javascript:window.close();" />
                </td>
            </tr>
        </table>
     </form>
</body>
</html>
