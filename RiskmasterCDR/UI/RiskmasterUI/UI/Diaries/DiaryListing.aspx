﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiaryListing.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryListing" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Dtd/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Diary List</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    
    <script src="../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    <script src="../../Scripts/drift.js" type="text/javascript"></script>
    <script src="../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
     <!--Rakhel ML Changes!-->
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
     <!--Rakhel ML Changes!-->

    
    <script language="JavaScript" type="text/javascript">
					function SetFocus()
					{
						//Set focus to first field
						var i; 
						for (i=0;i<document.forms[0].length;i++)			
						{	 				
							if((document.forms[0].item(i).type=="text") || (document.forms[0].item(i).type=="select-one") || (document.forms[0].item(i).type=="textarea"))
							{
								document.forms[0].item(i).focus();
								break;
							}
						}
					}
		
					function StoreSelection()
					{
					    if(document.forms[0].selDiary5.checked==true)
						{  
							if(document.forms[0].txtFromDate.value=='' || document.forms[0].txtFromDate.value==null)
							{
							    //alert("You must enter a 'From' date.");
                                alert(DiaryListingValidations.EnterFromDate);
								document.forms[0].txtFromDate.focus;
								return false;
							}
							
							if(document.forms[0].txtToDate.value=='' || document.forms[0].txtToDate.value==null)
							{
								//alert("You must enter a 'To' date.");
                                alert(DiaryListingValidations.EnterToDate);
								document.forms[0].txtToDate.focus;
								return false;
							}
						}
						
					    var tmpSortBy, tmpSortOrder;
					    tmpSortBy = "";
					    tmpSortOrder = "";
			
					    if (document.forms[0].selSortBy1.value != 0) {
					        tmpSortBy = document.forms[0].selSortBy1.value;
					        tmpSortOrder = document.forms[0].selSortOrder1.value;
					    }
						if (document.forms[0].selSortBy2.value!=0)
						{
						    if (tmpSortBy.match(document.forms[0].selSortBy2.value)) {
						        //alert("Duplicate sort order is not allowed.");
						        alert(DiaryListingValidations.DupSortOrder);
						        return false;
						    }
						    else {
						        tmpSortBy = tmpSortBy + "," + document.forms[0].selSortBy2.value;
						        tmpSortOrder = tmpSortOrder + "," + document.forms[0].selSortOrder2.value;
						    }
						}
			
						if (document.forms[0].selSortBy3.value!=0)
						{
						    if (tmpSortBy.match(document.forms[0].selSortBy3.value)) {
						        //alert("Duplicate sort order is not allowed.");
						        alert(DiaryListingValidations.DupSortOrder);
						        return false;
						    }
						    else {
						        tmpSortBy = tmpSortBy + "," + document.forms[0].selSortBy3.value;
						        tmpSortOrder = tmpSortOrder + "," + document.forms[0].selSortOrder3.value;
						    }
						}

						if (document.forms[0].selSortBy4.value!=0)
						{
						    if (tmpSortBy.match(document.forms[0].selSortBy4.value)) {
						        //alert("Duplicate sort order is not allowed.");
						        alert(DiaryListingValidations.DupSortOrder);
						        return false;
						    }
						    else {
						        tmpSortBy = tmpSortBy + "," + document.forms[0].selSortBy4.value;
						        tmpSortOrder = tmpSortOrder + "," + document.forms[0].selSortOrder4.value;
						    }
						}
						document.forms[0].sSortBy.value = tmpSortBy;
						document.forms[0].sSortOrder.value = tmpSortOrder;
						
						return true;
					}
		
					function PrintDetails()
					{
                        EnableDisable();
//						if (document.forms[0].PrintClick.value != "")
//						{
//								//window.open('home?pg=riskmaster/Diaries/PrintListing&assigneduser=' + assigneduser,'DetailView');
//								var location = "home?pg=riskmaster/Diaries/PrintListing&assigneduser=" + assigneduser;
//								m_codeWindow=window.open(location,'codeWnd',
//								'width=930,height=630'+',top='+(screen.availHeight-630)/2+',left='+(screen.availWidth-930)/2+',resizable=yes,scrollbars=yes');
//		
//								document.forms[0].selSortBy1.value = document.forms[0].sSortFirst.value;
//								document.forms[0].selSortBy2.value = document.forms[0].sSortSecond.value;
//								document.forms[0].selSortBy3.value = document.forms[0].sSortThird.value;
//								document.forms[0].selSortBy4.value = document.forms[0].sSortLast.value;
//								
//								return true;
//						}
						return false;
                }
                
                function EnableDisable()
                {
                    if(self.document.getElementById('chkUseTaskDesc')!=null)
                    {
                        if(self.document.getElementById('chkUseTaskDesc').checked == true)
                            self.document.getElementById('cboTaskDesc').disabled = false;
                        else
                            self.document.getElementById('cboTaskDesc').disabled = true;
                    }
                    else
                    {
                        self.document.getElementById('TaskDesc').style.display = "none"
                        self.document.getElementById('FTTaskDesc').style.display = "none"
                    }
				}
		</script>
</head>
<body class="10pt" onload="return PrintDetails();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
                 <td colspan="2" align="left">
                   <asp:ImageButton ID="btnPrint" ImageUrl="~/Images/tb_print_active.png" class="bold" ToolTip="<%$ Resources:ttPrint %>"
                        runat="server" OnClientClick="return StoreSelection();" OnClick="btnPrint_Click" />
                   <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                        runat="server" OnClick="btnCancel_Click" />
                 </td>
            </tr>
   
    <tr class="ctrlgroup">
     <td width="100%" colspan="2"> <asp:Label ID="lblDiaryPeekingResrc"  runat="server" Text="<%$ Resources:lblDiarySelResrc %>"></asp:Label> </td>
    </tr>
    <tr>
     <td colspan="2">
        <input type="radio" value="0" name="selectDiaryRadio" id="selDiary1" runat="server"  checked="true">
        <asp:Label ID="lblDiaryDueTdResrc"  runat="server" Text="<%$ Resources:lblDiaryDueTdResrc %>"></asp:Label>
        </td>
    </tr>
    <tr>
     <td colspan="2">
        <input type="radio" value="1" name="selectDiaryRadio" id="selDiary2"  runat="server"  />
          <asp:Label ID="lblDiaryNOdueResrc"  runat="server" Text="<%$ Resources:lblDiaryNOdueResrc %>"></asp:Label>
       </td>
    </tr>
    <tr>
     <td colspan="2"><input type="radio" name="selectDiaryRadio" value="2" runat="server" id="selDiary3"/>
        <asp:Label ID="lblDiaryOdueResrc"  runat="server" Text="<%$ Resources:lblDiaryOdueResrc %>"></asp:Label>
    </td>
    </tr>
    <tr>
     <td colspan="2"><input type="radio" name="selectDiaryRadio" value="3" runat="server" id="selDiary4"/>
        <asp:Label ID="lblAOpDiaryResrc"  runat="server" Text="<%$ Resources:lblAOpDiaryResrc %>"></asp:Label>
     
    </td>
    </tr>
    <tr>
     <td colspan="2"><input type="radio" name="selectDiaryRadio" value="4" runat="server" id="selDiary5"/>
      <asp:Label ID="lblOpDiaryDue"  runat="server" Text="<%$ Resources:lblOpDiaryDue %>"></asp:Label>
     </td>
    </tr>
    <tr>
     <td width="10%" align="right">
       <asp:Label ID="lblFromResrc"  runat="server" Text="<%$ Resources:lblFromResrc %>"></asp:Label>
     </td>
     <td>
        <input type="text" value="" id="txtFromDate" size="30" runat="server" onblur="dateLostFocus(this.id);"/>
        
        <!-- Rakhel ML Changes - start !-->
        <script type="text/javascript">
            $(function () {
                $("#txtFromDate").datepicker({
                    showOn: "button",
                    buttonImage: "../../Images/calendar.gif",
                    buttonImageOnly: true,
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    changeYear: true
                });
            });
                       </script>
        <!-- Rakhel ML Changes - end !-->
	</td>
    </tr>
    <tr>
     <td width="10%" align="right">

    <!-- Richa : Mits 30984 start-->
      <asp:Label ID="lblToResrc"  runat="server" Text="<%$ Resources:lblToResrc %>"></asp:Label>
       <!-- Richa : Mits 30984 start-->
     
     </td>
     <td>
        <input type="text" value="" id="txtToDate"  runat="server" size="30" onblur="dateLostFocus(this.id);"/>
       
        <!-- Rakhel ML Changes - start !-->
        <script type="text/javascript">
            $(function () {
                $("#txtToDate").datepicker({
                    showOn: "button",
                    buttonImage: "../../Images/calendar.gif",
                    buttonImageOnly: true,
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    changeYear: true
                });
            });
                       </script>
        <!-- Rakhel ML Changes - end !-->
    </td>
    </tr>
    <tr id ="FTTaskDesc">
     <td nowrap="1" colspan="2">
        <input type="checkbox"  id="chkUseTaskDesc" runat="server" onclick="EnableDisable();" value="true" />
         <asp:Label ID="lblFilterResrc"  runat="server" Text="<%$ Resources:lblFilterResrc %>"></asp:Label>
      </td>
    </tr>
    <tr id ="TaskDesc">
     <td align="right" nowrap="1" width="10%" >
       <asp:Label ID="lblTaskDesResrc"  runat="server" Text="<%$ Resources:lblTaskDesResrc %>"></asp:Label>
     </td>
     <td>
        <!-- MITS 32210 Raman Bhatia -->
        <!-- This dropdown will be replaced by a text box where user can enter the folter criteria -->
        <%--<asp:DropDownList ID="cboTaskDesc" runat="server" Width="500" ></asp:DropDownList>--%>
        <asp:TextBox ID="cboTaskDesc" runat="server" Width="500"></asp:TextBox>
    </td>
    </tr>
    <tr  class="ctrlgroup">
     <td width="100%" colspan="2">
      <asp:Label ID="lblSortbyResrc"  runat="server" Text="<%$ Resources:lblSortbyResrc %>"></asp:Label>
   </td>
    </tr>
    <tr>
     <td width="10%" align="right">
     <asp:Label ID="lblSortFirstResrc"  runat="server" Text="<%$ Resources:lblSortFirstResrc %>"></asp:Label>
      
      </td>
     <td>
        <asp:DropDownList ID="selSortBy1" runat="server">
            <asp:ListItem Text="" Value="0"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Due Date %>" Value="1"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Priority %>" Value="2"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Attached Record %>" Value="3"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Task Description %>" Value="4"></asp:ListItem>
         </asp:DropDownList>
		<asp:DropDownList ID="selSortOrder1" runat="server">
                        <asp:ListItem Text ="<%$ Resources:Asc %>" Value="ASC"></asp:ListItem>
                        <asp:ListItem Text ="<%$ Resources:Desc %>" Value="DESC"></asp:ListItem>
                     </asp:DropDownList>

     </td>
    </tr>
    <tr>
     <td align="right" width="10%">
      <asp:Label ID="lblSortSecResrc"  runat="server" Text="<%$ Resources:lblSortSecResrc %>"></asp:Label>
     </td>
     <td>
         <asp:DropDownList ID="selSortBy2" runat="server">
            <asp:ListItem Text="" Value="0"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Due Date %>" Value="1"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Priority %>" Value="2"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Attached Record %>" Value="3"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Task Description %>" Value="4"></asp:ListItem>
         </asp:DropDownList>
         <asp:DropDownList ID="selSortOrder2" runat="server">
                        <asp:ListItem Text ="<%$ Resources:Asc %>" Value="ASC"></asp:ListItem>
                        <asp:ListItem Text ="<%$ Resources:Desc %>" Value="DESC"></asp:ListItem>
                     </asp:DropDownList>
    </td>
    </tr>
    <tr>
     <td align="right" width="10%">
       <asp:Label ID="lblSortThirdResrc"  runat="server" Text="<%$ Resources:lblSortThirdResrc %>"></asp:Label>
      </td>
     <td>
         <asp:DropDownList ID="selSortBy3" runat="server">
            <asp:ListItem Text="" Value="0"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Due Date %>" Value="1"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Priority %>" Value="2"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Attached Record %>" Value="3"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Task Description %>" Value="4"></asp:ListItem>
         </asp:DropDownList>
          <asp:DropDownList ID="selSortOrder3" runat="server">
                        <asp:ListItem Text ="<%$ Resources:Asc %>" Value="ASC"></asp:ListItem>
                        <asp:ListItem Text ="<%$ Resources:Desc %>" Value="DESC"></asp:ListItem>
                     </asp:DropDownList>
     </td>
    </tr>
    <tr>
     <td align="right" width="10%">
      <asp:Label ID="lblSortLastResrc"  runat="server" Text="<%$ Resources:lblSortLastResrc %>"></asp:Label>
    </td>
     <td>
        <asp:DropDownList ID="selSortBy4" runat="server">
            <asp:ListItem Text="" Value="0"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Due Date %>" Value="1"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Priority %>" Value="2"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Attached Record %>" Value="3"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:Task Description %>" Value="4"></asp:ListItem>
        </asp:DropDownList>

       <asp:DropDownList ID="selSortOrder4" runat="server">
                        <asp:ListItem Text ="<%$ Resources:Asc %>" Value="ASC"></asp:ListItem>
                        <asp:ListItem Text ="<%$ Resources:Desc %>" Value="DESC"></asp:ListItem>
                     </asp:DropDownList>
        <input type="hidden" value="" runat="server" id="sSortBy"/>
         <input type="hidden"  runat="server" value="" id="sSortOrder"/>
    </td>
    </tr>
    <%--<tr>
     <td colspan="2" align="left">
     <asp:Button  ID="btnPrint" runat="server" CssClass="button" Text="Print" 
             OnClientClick="return StoreSelection();" onclick="btnPrint_Click" />
     <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" 
             onclick="btnCancel_Click" />
    </td>
    </tr>--%>
   </table>
    </form>
</body>
</html>
