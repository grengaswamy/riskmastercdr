﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class EditDiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument diaryDoc = null;
            XmlNode loadDiaryNode = null;
            
            try
            {
                //Rakhel ML Changes - start 
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //Rakhel ML Changes - end 

                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("EditDiary.aspx"), "EditDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "EditDiaryValidations", sValidationResources, true);

                if (!IsPostBack)
                {
                    if (Request.QueryString["entryid"] != null)
                    {
                        ViewState["entryid"] = Request.QueryString["entryid"];
                    }
                    else
                    {
                        ViewState["entryid"] = "";
                    }

                    if (Request.QueryString["assigninguser"] != null)
                    {
                        ViewState["assigninguser"] = Request.QueryString["assigninguser"];
                    }
                    else
                    {
                        ViewState["assigninguser"] = "";
                    }

                    if (Request.QueryString["attachprompt"] != null)
                    {
                        if ((Request.QueryString["attachprompt"]) != string.Empty)
                        {
                            txtAttachRecord.Value = Request.QueryString["attachprompt"];
                            lblAttachRecord.Visible = false;
                        }
                        else
                        {
                            txtAttachRecord.Visible = false;
                            lblAttachRecord.Text = "No";
                        }
                    }
                    else
                    {
                        txtAttachRecord.Visible = false;
                        lblAttachRecord.Text = "No";
                    }

                    if (Request.QueryString["assigneduser"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }
                    
                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";//"EVENT";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";// "1";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }

                    //07/26/2011 SMISHRA54: WPA Email Notification
                    //chkEmailNotification.Checked = true; //nsachdeva2 - MITS 27324 - Commented the hardcoded flag setting. Currently Flag is not saved into database and do not have documentation to fix it properly. 
                    //07/26/2011 SMISHRA54: WPA Email Notification

                    diaryDoc = LoadDiary();
                    loadDiaryNode = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary");
                    if (loadDiaryNode != null)
                    {
                        PopulateFormControls(loadDiaryNode);
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void PopulateFormControls(XmlNode loadDiaryNode)
        {
            XmlNodeList wpaTaskNodeList = null;
            XmlNode activityIdNode = null;
            XmlNode activityTextNode = null;

            string activityId = string.Empty;
            string activityText = string.Empty;

            //igupta3 jira 439
            if ((loadDiaryNode.SelectSingleNode("OVERRIDE_NOTROLL_PERM") != null)
                &&(loadDiaryNode.SelectSingleNode("OVERRIDE_NOTROLL_PERM").InnerText==Boolean.FalseString ))
            {
                chkNotRollable.Disabled=true;
            }

            if ((loadDiaryNode.SelectNodes("OVERRIDE_NOTROUTE_PERM") != null)
                && (loadDiaryNode.SelectSingleNode("OVERRIDE_NOTROUTE_PERM").InnerText == Boolean.FalseString))
            {
                chkNotRoutable.Disabled = true;
            }

            wpaTaskNodeList = loadDiaryNode.SelectNodes("WpaTaskList/WpaTask");

            if (wpaTaskNodeList.Count > 0)
            {
                foreach (XmlNode wpaTaskNode in wpaTaskNodeList)
                {
                    txtSubjectDcboBox.Items.Add(wpaTaskNode.Attributes["code_desc"].Value);
                }

                this.Page.ClientScript.RegisterStartupScript(typeof(string), "WpaTaskIsSelect", "_isSelect = true;", true);
                txtSubject.Value = loadDiaryNode.SelectSingleNode("TaskSubject").InnerText; // MGaba2: MITS 14739            
            }
            else
            {
                txtSubject.Value = loadDiaryNode.SelectSingleNode("TaskSubject").InnerText;
                txtSubjectDcboBox.Visible = false;
            }

            if (loadDiaryNode.SelectSingleNode("CompleteDate") != null)
            {
                txtDueDate.Value = AppHelper.GetDate( loadDiaryNode.SelectSingleNode("CompleteDate").InnerText);
            }
            else
            {
                txtDueDate.Value = "";
            }

            if (loadDiaryNode.SelectSingleNode("CompleteTime") != null)
            {
                txtDueTime.Value = AppHelper.GetTime( loadDiaryNode.SelectSingleNode("CompleteTime").InnerText);
            }
            else
            {
                txtDueTime.Value = "";
            }
            // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
            if (txtAttachRecord.Visible)
            {
                if (loadDiaryNode.SelectSingleNode("AttachRecord") != null)
                {
                    txtAttachRecord.Value = loadDiaryNode.SelectSingleNode("AttachRecord").InnerText;
                }
                else
                {
                    txtAttachRecord.Value = "";
                }
            }
            // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries
            if (loadDiaryNode.SelectSingleNode("EstimateTime") != null)
            {
                txtEstTime.Value = loadDiaryNode.SelectSingleNode("EstimateTime").InnerText;
            }
            else
            {
                txtEstTime.Value = "";
            }

            if (loadDiaryNode.SelectSingleNode("Priority") != null)
            {
                ddlPriority.SelectedValue = loadDiaryNode.SelectSingleNode("Priority").InnerText;
            }

            if (loadDiaryNode.SelectSingleNode("TaskNotes") != null)
            {
                txtNotes.Value = loadDiaryNode.SelectSingleNode("TaskNotes").InnerText;
            }
            else
            {
                txtNotes.Value = "";
            }


            if ((loadDiaryNode.SelectSingleNode("NotifyFlag").InnerText.ToLower() != "false") || loadDiaryNode.SelectSingleNode("AutoConfirm").InnerText.ToLower() != "false")
            {
                chkAutoConfrim.Checked = true;
            }

            //if (loadDiaryNode.SelectSingleNode("MAIL_DISABLED") != null)
            //{
            //    if (loadDiaryNode.SelectSingleNode("MAIL_DISABLED").InnerText.ToLower() != "false")
            //    {
            //        chkAutoConfrim.Disabled = true;
            //    }
            //}
            //dvatsa JIRA -11627(start)
            if (loadDiaryNode.SelectSingleNode("MAIL_ENABLED") != null)
            {
                if (loadDiaryNode.SelectSingleNode("MAIL_ENABLED").InnerText.ToLower() == "true")
                {
                    chkAutoConfrim.Disabled = false;
                    chkEmailNotification.Disabled = false;
                    chkDiaryOverDueNotify.Disabled = false;
                }
                else
                {
                    chkAutoConfrim.Disabled = true;
                    chkEmailNotification.Disabled = true;
                    chkDiaryOverDueNotify.Disabled = true;
                }
            }
            //dvatsa JIRA 11627(end)
            if (loadDiaryNode.SelectSingleNode("OverDueDiaryNotify").InnerText.ToLower() != "false")
            {
                chkDiaryOverDueNotify.Checked = true;
            }
            //igupta3 jira 439
            if (loadDiaryNode.SelectSingleNode("NonRoutableFlag").InnerText.ToLower() == "yes")
            {
                chkNotRoutable.Checked = true;
            }
            if (loadDiaryNode.SelectSingleNode("NonRollableFlag").InnerText.ToLower() == "yes")
            {
                chkNotRollable.Checked = true;
            }

            XmlNodeList activitiesList = null;

            activitiesList = loadDiaryNode.SelectNodes("Activities/Activity");

            if (activitiesList.Count > 0)
            {
                foreach (XmlNode activityNode in activitiesList)
                {
                    activityId = string.Empty;
                    activityText = string.Empty;

                    activityIdNode = activityNode.SelectSingleNode("ActivityId");
                    activityTextNode = activityNode.SelectSingleNode("ActivityName");

                    if (activityIdNode != null)
                    {
                        activityId = activityIdNode.InnerText;
                    }

                    if (activityTextNode != null)
                    {
                        activityText = activityTextNode.InnerText;
                    }

                    lstActivities.Items.Add(new ListItem(activityText, activityId));
                }

                txtActivities.Value = loadDiaryNode.SelectSingleNode("Activities/sActs").InnerText;
                actstring.Value = txtActivities.Value;
            }
        }

        private XmlDocument LoadDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            
            try
            {
                serviceMethodToCall = "WPAAdaptor.LoadDiary";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForLoadDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);

                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetInputDocForLoadDiary()
        {
            XmlDocument loadDiaryDoc = null;

            XmlNode loadDiaryNode = null;
            XmlAttribute atbBase = null;
            XmlAttribute atbEntryId = null;
            XmlAttribute atbNotify = null;
            XmlAttribute atbRoot = null;

            try
            {
                loadDiaryDoc = new XmlDocument();
                loadDiaryNode = loadDiaryDoc.CreateElement("LoadDiary");

                atbBase = loadDiaryDoc.CreateAttribute("base");
                atbBase.Value = "";

                atbEntryId = loadDiaryDoc.CreateAttribute("entryid");
                atbEntryId.Value = ViewState["entryid"].ToString();

                atbNotify = loadDiaryDoc.CreateAttribute("notify");
                atbNotify.Value = "";

                atbRoot = loadDiaryDoc.CreateAttribute("root");
                atbRoot.Value = "";

                loadDiaryNode.Attributes.Append(atbBase);
                loadDiaryNode.Attributes.Append(atbEntryId);
                loadDiaryNode.Attributes.Append(atbNotify);
                loadDiaryNode.Attributes.Append(atbRoot);

                loadDiaryDoc.AppendChild(loadDiaryNode);
                return loadDiaryNode;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlDocument SaveDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            
            try
            {
                serviceMethodToCall = "WPAAdaptor.SaveDiary";

                inputDocNode = GetInputDocForSaveDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetInputDocForSaveDiary()
        {
            XmlDocument saveDiaryDoc = null;
            XmlNode saveDiaryNode = null;

            XmlNode entryIdNode = null;
            XmlNode taskSubjectNode = null;
            XmlNode assigningUserNode = null;
            XmlNode statusOpenNode = null;
            XmlNode dueDateNode = null;
            XmlNode completeTimeNode = null;
            XmlNode taskNotesNode = null;
            XmlNode actionNode = null;
            XmlNode estimateTimeNode = null;
            XmlNode priorityNode = null;
            XmlNode notifyFlagNode = null;
            XmlNode autoConfirmNode = null;
            XmlNode activityStringNode = null;
            //07/07/2011 MDHAMIJA : WPA Email Notification
            XmlNode notifyByMailNode = null;
            //07/07/2011 MDHAMIJA : WPA Email Notification End
            XmlNode sendOverDueDiaryNotification = null;
            //igupta3 jira 439
            XmlNode nonRoutable = null;
            XmlNode nonRollable = null;

            saveDiaryDoc = new XmlDocument();
            saveDiaryNode = saveDiaryDoc.CreateElement("SaveDiary");

            entryIdNode = saveDiaryDoc.CreateElement("EntryId");
            entryIdNode.InnerText = ViewState["entryid"].ToString();
            saveDiaryNode.AppendChild(entryIdNode);

            taskSubjectNode = saveDiaryDoc.CreateElement("TaskSubject");
            taskSubjectNode.InnerText = txtSubject.Value;
            saveDiaryNode.AppendChild(taskSubjectNode);

            assigningUserNode = saveDiaryDoc.CreateElement("AssigningUser");
            assigningUserNode.InnerText = ViewState["entryid"].ToString();
            saveDiaryNode.AppendChild(assigningUserNode);

            statusOpenNode = saveDiaryDoc.CreateElement("StatusOpen");
            statusOpenNode.InnerText = "";
            saveDiaryNode.AppendChild(statusOpenNode);

            dueDateNode = saveDiaryDoc.CreateElement("DueDate");
            if (txtDueDate.Value == "")
                txtDueDate.Value = dueDateValue.Value;
            dueDateNode.InnerText = AppHelper.GetRMDate(txtDueDate.Value);//Deb ML changes
            saveDiaryNode.AppendChild(dueDateNode);

            completeTimeNode = saveDiaryDoc.CreateElement("CompleteTime");
            if (txtDueTime.Value == "")
                txtDueTime.Value = dueTimeValue.Value;
            completeTimeNode.InnerText = txtDueTime.Value;
            saveDiaryNode.AppendChild(completeTimeNode);

            estimateTimeNode = saveDiaryDoc.CreateElement("EstimateTime");
            estimateTimeNode.InnerText = txtEstTime.Value;
            saveDiaryNode.AppendChild(estimateTimeNode);

            priorityNode = saveDiaryDoc.CreateElement("Priority");
            priorityNode.InnerText = ddlPriority.SelectedItem.Value;
            saveDiaryNode.AppendChild(priorityNode);

            notifyFlagNode = saveDiaryDoc.CreateElement("NotifyFlag");
           autoConfirmNode = saveDiaryDoc.CreateElement("AutoConfirm");
           //07/07/2011 MDHAMIJA : WPA Email Notification
           notifyByMailNode = saveDiaryDoc.CreateElement("EmailNotifyFlag");
           //07/07/2011 MDHAMIJA : WPA Email Notification End
            
            if (chkAutoConfrim.Checked == true)
            {
                autoConfirmNode.InnerText = "true";
                notifyFlagNode.InnerText = "true";
            }
            else
            {
                autoConfirmNode.InnerText = "false";
                notifyFlagNode.InnerText = "false";
            }

            saveDiaryNode.AppendChild(autoConfirmNode);
            saveDiaryNode.AppendChild(notifyFlagNode);
            //07/07/2011 MDHAMIJA : WPA Email Notification
            if (chkEmailNotification.Checked)
            {
                notifyByMailNode.InnerText = "true";
            }
            else
            {
                notifyByMailNode.InnerText = "";
            }
            saveDiaryNode.AppendChild(notifyByMailNode);
            //07/07/2011 MDHAMIJA : WPA Email Notification End
            //added by amitosh for QBE Enhancement of OverDueDiary Notification
            sendOverDueDiaryNotification = saveDiaryDoc.CreateElement("OverDueDiaryNotify");
            if (chkDiaryOverDueNotify.Checked)
            {
                sendOverDueDiaryNotification.InnerText = "true";
            }
            else
            {
                sendOverDueDiaryNotification.InnerText = "";
            }
            saveDiaryNode.AppendChild(sendOverDueDiaryNotification);
            //End Amitosh

            //igupta3 jira 439
            nonRoutable = saveDiaryDoc.CreateElement("NonRoutableFlag");
            if (chkNotRoutable.Checked)
            {
                nonRoutable.InnerText = "true";
            }
            else
            {
                nonRoutable.InnerText = "";
            }
            saveDiaryNode.AppendChild(nonRoutable);
            nonRollable = saveDiaryDoc.CreateElement("NonRollableFlag");
            if (chkNotRollable.Checked)
            {
                nonRollable.InnerText = "true";
            }
            else
            {
                nonRollable.InnerText = "";
            }
            saveDiaryNode.AppendChild(nonRollable);
            //igupta3 ends

            taskNotesNode = saveDiaryDoc.CreateElement("TaskNotes");
            taskNotesNode.InnerText = txtNotes.Value;
            saveDiaryNode.AppendChild(taskNotesNode);
            
            activityStringNode = saveDiaryDoc.CreateElement("ActivityString");
            activityStringNode.InnerText = actstring.Value;
            saveDiaryNode.AppendChild(activityStringNode);

            actionNode = saveDiaryDoc.CreateElement("Action");
            actionNode.InnerText = "Edit";
            saveDiaryNode.AppendChild(actionNode);

            saveDiaryDoc.AppendChild(saveDiaryNode);

            return saveDiaryNode;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            XmlDocument diaryDoc = null;
            string isDiaryCalPeek = string.Empty;
            string isDiaryCalProssOffice = string.Empty;
            string diaryListUrl = string.Empty;

            try
            {
                diaryDoc = SaveDiary();

                ////if diarydoc has success message then redirect to diarylist 
                ////else throw new execption with error messge in it

                if (diaryDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                {
                    if (Request.QueryString["isdiarycalendar"] != null)
                    {
                        if (Request.QueryString["isdiarycalendar"] == "true")
                        {
                            isDiaryCalPeek = Request.QueryString["isdiarycalendarpeek"];
                            isDiaryCalProssOffice = Request.QueryString["isdiarycalendarprocessingoffice"];

                            diaryListUrl = "DiaryCalendar/DiaryCalendar.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&IsPeek=" + isDiaryCalPeek + "&IsProcessingOffice=" + isDiaryCalProssOffice;
                            Response.Redirect(diaryListUrl, false);
                        }
                    }
                    else
                    {
                        diaryListUrl = "DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString();
                        Server.Transfer(diaryListUrl, false);
                    }
                }
                else
                {
                    throw new ApplicationException(diaryDoc.InnerXml);
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string isDiaryCalPeek = string.Empty;
            string isDiaryCalProssOffice = string.Empty;
            string diaryListUrl = string.Empty;

            try
            {
                if (Request.QueryString["isdiarycalendar"] != null)
                {
                    if (Request.QueryString["isdiarycalendar"] == "true")
                    {
                        isDiaryCalPeek = Request.QueryString["isdiarycalendarpeek"];
                        isDiaryCalProssOffice = Request.QueryString["isdiarycalendarprocessingoffice"];

                        diaryListUrl = "DiaryCalendar/DiaryCalendar.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&IsPeek=" + isDiaryCalPeek + "&IsProcessingOffice=" + isDiaryCalProssOffice;
                        Response.Redirect(diaryListUrl, false);
                    }
                }
                else
                {
                    diaryListUrl = "DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString();
                    Server.Transfer(diaryListUrl, false);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        
    }
}
