﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompleteDiary.aspx.cs" Inherits="Riskmaster.UI.Diaries.CompleteDiary" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"  TagPrefix="mc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Diary Task Completion</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
       
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    <script src="../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
     <script src="../../Scripts/form.js" type="text/javascript"></script>
    
    <!--Praveen ML-->    
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
    <!--Praveen ML-->
    
    <script type="text/javascript">
					function pagecompleteLoaded()
					{
						var i;
						for (i=0;i<document.forms[0].length;i++)			
						{	
							if((document.forms[0].item(i).type=="text") || (document.forms[0].item(i).type=="select-one")|| (document.forms[0].item(i).type=="textarea"))
	                        {
                                if (document.forms[0].item(i).disabled == false) 
                                {
								    document.forms[0].item(i).focus();
								    break;
							    }
							}
						}
						//set Completed On to currentdate
//						var CompletedOnDate = new Date();
//						var curr_date = CompletedOnDate.getDate();
//						if( curr_date < 10 )
//							curr_date = "0" + curr_date;
//						
//						var curr_month = CompletedOnDate.getMonth();
//						curr_month += 1;
//						if( curr_month < 10 )
//							curr_month = "0" + curr_month;
//						var curr_year = CompletedOnDate.getFullYear();
						//document.forms[0].txtCompletedOn.value = curr_month + "/" + curr_date + "/" + curr_year;

						
					}
					function chkTrackClick()
					{
						var b= !document.forms[0].chkTrack.checked;
						document.forms[0].txtTEStartTime.disabled=b;
						document.forms[0].txtTEEndTime.disabled=b;
						document.forms[0].txtTETotalTime.disabled=b;
						document.forms[0].txtTEExpenses.disabled=b;
					}
					
					function save_validate()
					{
						if(document.forms[0].txtCompletedOn.value=='')
						{
						    //alert("Completed On is required field");
						    alert(CompleteDiaryValidations.CompleteOn); //Rakhel ML Changes
							//document.forms[0].ouraction.value="savefailed";
							//document.forms[0].checkvalidity.value="savefailed";
							//document.forms[0].submit();
							return false;
						}
						return true;
					}
					// MITS 9391
					function checkForDecimal(form)
					{
						if (isNaN(parseFloat(document.getElementById(form).value)))
						{
							 document.getElementById(form).value="";
						}
						else
						{ 
							if(document.getElementById(form).value!=(parseFloat(document.getElementById(form).value))) 
							{	          
								document.getElementById(form).value="";
							} 
						}
					}

                    //Rakhel - ML Changes
					function Disp() {
					    $.datepicker._disabledInputs = [];
					}
					//Rakhel - ML Changes
                </script>
   
      
</head>
    <body onload="pagecompleteLoaded()" onunload="Disp();">
        <form id="frmData" runat="server">
             <table border="0" width="100%">
                <tr>
                  <td colspan="2" >
                    <uc1:ErrorControl ID="ErrorControl" runat="server" />
                  </td>
                </tr>


                <tr>
                 <td colspan="2" align="left">
                   <asp:ImageButton ID="ImageButton1" ImageUrl="~/Images/tb_savemapping_active.png" class="bold" ToolTip="<%$ Resources:ttSave %>"
                        runat="server" OnClientClick="return save_validate();;" OnClick="btnSave_Click" />
                   <asp:ImageButton ID="ImageButton2" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                        runat="server" OnClick="btnCancel_Click" />
                 </td>
            </tr>

                <tr>
                  <td colspan="2" class="ctrlgroup">
                    <asp:Label ID="lblDiaryTaskCompResrc" runat="server" Text="<%$ Resources:lblDiaryTaskCompResrc %>"></asp:Label> 
                  
                  </td>
                </tr>
                <tr>
                    <td>
                     <asp:Label ID="lblCompletedOnResrc" runat="server" class="required" Text="<%$ Resources:lblCompletedOnResrc %>"></asp:Label> 
                  
                       
                    </td>
                    <td align="left">
                      <asp:TextBox ID="txtCompletedOn" runat="server" MaxLength="15" onchange = "dateLostFocus(this.id);"/>
                      <script type="text/javascript">
                          $(function () {
                              $("#txtCompletedOn").datepicker({
                                  showOn: "button",
                                  buttonImage: "../../Images/calendar.gif",
                                  buttonImageOnly: true,
                                  showOtherMonths: true,
                                  selectOtherMonths: true,
                                  changeYear: true
                              });
                          });
                       </script>
                    </td>
                </tr>
                <tr>
                    <td nowrap="1"> <asp:Label ID="lblCompRespResrc"  runat="server" class="required" Text="<%$ Resources:lblCompRespResrc %>"></asp:Label> 
                 </td>
                    <td>
                        <%--<textarea cols="55" wrap="soft" rows="5"></textarea>--%>
                        <asp:TextBox runat="server"  ID="txtAreaCompleteResponse" TextMode="MultiLine"  Columns="55" Rows="5" />
                    </td>
                </tr>
                <tr>
                    <td nowrap="1">
                        <%--<input type="checkbox" value="true" id="chkTrack" runat="server" onclick="chkTrackClick();"/>Track Time/Expenses for Task--%>
                        <asp:CheckBox ID="chkTrack" Text="<%$ Resources:chkTrackTimeExpenses %>" runat="server" onclick ="chkTrackClick();" /> 
                    </td>
                </tr>
                <tr>
                    <td colspan="2" bgcolor="#D5CDA4">
                    <asp:Label ID="lblTimeExpResrc"  runat="server" Font-Bold="true"  Text="<%$ Resources:lblTimeExpResrc %>"></asp:Label> 
                    <b>
                    </b>
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd" >
                       <asp:Label ID="lblStartTimeResrc" class="datatd" runat="server" Font-Italic="true"  Text="<%$ Resources:lblStartTimeResrc %>"></asp:Label> 
                       
                    </td>
                    <td nowrap="" class="datatd">
                        <%--<input type="text" value=""  runat="server" disabled="disabled" id="txtTEStartTime" size="15" onblur="timeLostFocus(this.id);"/>--%>
                        <asp:TextBox Text="" runat="server" Enabled="false" ID="txtTEStartTime" MaxLength="15" onchange="timeLostFocus(this.id);"/>
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblEndimeResrc" class="datatd" runat="server" Font-Italic="true"  Text="<%$ Resources:lblEndimeResrc %>"></asp:Label> 
                        
                    </td>
                    <td nowrap="nowrap" class="datatd">
                        <%--<input type="text" value="" disabled="disabled" id="txtTEEndTime" size="15" onblur="timeLostFocus(this.id);"/>--%>
                        <asp:TextBox Text="" runat="server" Enabled="false" ID="txtTEEndTime" MaxLength="15" onchange="timeLostFocus(this.id);"/>
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                      <asp:Label ID="lblTotTimeSpentResrc" class="datatd" runat="server" Font-Italic="true"  Text="<%$ Resources:lblTotTimeSpentResrc %>"></asp:Label> 
                       
                    </td>
                    <td nowrap="" class="datatd">
                        <%--<input type="text" value="" disabled="disabled" runat="server" id="txtTETotalTime" size="15" onblur="checkForDecimal(this.id);"/>--%>
                        <asp:TextBox Text="" runat="server" Enabled="false" ID="txtTETotalTime" MaxLength="15" onchange="checkForDecimal(this.id);"/>
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                      <asp:Label ID="lblExpensesResrc" class="datatd" runat="server" Font-Italic="true"  Text="<%$ Resources:lblExpensesResrc %>"></asp:Label>    
                    </td>
                    <td nowrap="" class="datatd">
                        <%--<input type="text" value="" runat="server" disabled="disabled" id="txtTEExpenses" size="15" onblur="currencyLostFocus(this);"/>--%>
                        <mc:CurrencyTextbox  Text="" runat="server" Enabled="false" ID="txtTEExpenses" MaxLength="15" onchange="currencyLostFocus(this);"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<input type="checkbox" id="chkNotifyTaskAssigner" runat="server" value="true" disabled="disabled"/>--%>

                        <asp:CheckBox ID="chkNotifyTaskAssigner" runat="server" Text="" />
                          <asp:Label ID="lblNotifyToAssignResrc" class="datatd" runat="server"  Text="<%$ Resources:lblNotifyToAssignResrc %>"></asp:Label> 
                    </td>
                </tr>
                <%--<tr>
                    <td colspan="2" align="center">
                        <asp:Button runat="server"  ID="btnSave" Text="Save" CssClass="button" 
                            OnClientClick = "return save_validate();; " onclick="btnSave_Click" 
                            Width="38px" />
                        <asp:Button runat="server" ID="btnCancel" value="Cancel" Text="Cancel" 
                            CssClass="button" onclick="btnCancel_Click" Width="43px" />
                    </td>
                 </tr>--%>
            </table>
        </form>
    </body>
</html>
