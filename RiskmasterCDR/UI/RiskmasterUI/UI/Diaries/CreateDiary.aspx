﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateDiary.aspx.cs" Inherits="Riskmaster.UI.Diaries.CreateDiary" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Create Diary</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <%-- RMA-13498 : Not required css file doesnot exist now <link href="../../Content/dhtml-combo.css" rel="stylesheet" type="text/css" />--%>
    
    
    <script src="../../Scripts/cul.js" type="text/javascript"></script>
    <script src="../../Scripts/m-x.js" type="text/javascript"></script>
    <script src="../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    <script src="../../Scripts/DcomboBox.js" type="text/javascript"></script>
    

   <!--Praveen ML-->    
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
    <!--Praveen ML-->
    
    <script type="text/javascript">
        function ValForm() {
            if (document.forms[0].lstUsers.options.length < 1) {
                //alert("Please select the user to assign diary to.");
                alert(CreateDiaryValidations.ValidAssignDiaryToCheck);
                return false;
            }

            if (replace(document.forms[0].txtSubject.value, " ", "") == "") {
                //alert("Please enter the Task Name.");
                alert(CreateDiaryValidations.ValidTaskNameCheck);
                document.forms[0].txtSubject.focus();
                return false;
            }

            if (replace(document.forms[0].txtDueDate.value, " ", "") == "") {
                //ravi commented to match the functionality with rmworld-dont know whether its as it should be --4/22/03
                //alert("Please enter the due date.");
                //document.forms[0].txtDueDate.focus();
                //return false;
            }

            if (document.forms[0].optIssue2.checked) {
                if (isNaN(parseInt(document.forms[0].txtIssuePeriod.value))) {
                    //MITS 22016: Raman Bhatia
                    //alert("Please enter a number into the Issue every n days until the completion date field.");
                    //alert("Please fill in a valid value in the days until completion date field to proceed !");
                    alert(CreateDiaryValidations.ValidDaysToComplete);
                    document.forms[0].txtIssuePeriod.focus();
                    return false;
                }

            }
            //Umesh fix for MITS 7887 
            if (document.forms[0].optIssue2.checked) {
                if (parseInt(document.forms[0].txtIssuePeriod.value) <= 0) {
                    //alert("Please enter positive value.");
                    alert(CreateDiaryValidations.ValidPositiveValue);
                    document.forms[0].txtIssuePeriod.focus();
                    return false;
                }

            }
            //end

        }

        //Start MITS 26887: Ritesh
//        function timeLostFocus(sCtrlName) {                              
//            var objFormElem = eval('document.forms[0].' + sCtrlName);
//            var sTime = new String(objFormElem.value);
//            //Start Parag MITS 10615 
//            var swhere = 0;
//            var stemptime = "";
//            //End Parag MITS 10615 
//            if (sTime == "")
//                return true;
//            sTime = sTime.toUpperCase();
//            //Start Parag MITS 10615 
//            if (sTime.length < 3) {
//                objFormElem.value = "";
//                return true;
//            }
//            if (sTime.length == 3)
//                swhere = 0;
//            else
//                swhere = 1;
//            if (sTime.indexOf(":") < 0) {
//                stemptime = "";
//                for (i = 0; i < sTime.length; i++) {
//                    if (i == swhere)
//                        stemptime = stemptime + sTime.charAt(i) + ":";
//                    else
//                        stemptime = stemptime + sTime.charAt(i);
//                }
//                sTime = stemptime;
//            }
//            if (sTime.length == 6) {
//                if (sTime.indexOf("P") == 5) {
//                    sTime = sTime.replace("P", "PM");
//                }
//                if (sTime.indexOf("A") == 5) {
//                    sTime = sTime.replace("A", "AM");
//                }
//            }
//            if (sTime.length == 7) {
//                if (sTime.indexOf(" P") == 5) {
//                    sTime = sTime.replace("P", "PM");
//                }
//                if (sTime.indexOf(" A") == 5) {
//                    sTime = sTime.replace("A", "AM");
//                }
//            }
//            //End Parag MITS 10615 
//            var sArr = sTime.split(":");
//            if (sArr.length != 2) {
//                objFormElem.value = "";
//                return true;
//            }
//            if (sArr[0] == "" || sArr[1] == "" || isNaN(parseInt(sArr[0])) || isNaN(parseInt(sArr[1]))) {
//                objFormElem.value = "";
//                return true;
//            }
//            sArr[0] = new String(parseInt(sArr[0], 10));
//            sArr[1] = new String(parseInt(sArr[1], 10));
//            if (sTime.indexOf("AM") < 0 && sTime.indexOf("PM") < 0) {
//                if (parseInt(sArr[0], 10) < 0 || parseInt(sArr[0], 10) > 23 || parseInt(sArr[1], 10) < 0 || parseInt(sArr[1], 10) > 59) {
//                    objFormElem.value = "";
//                    return true;
//                }
//                if (sArr[0].length == 1)
//                    sArr[0] = "0" + sArr[0];
//                if (sArr[1].length == 1)
//                    sArr[1] = "0" + sArr[1];
//                objFormElem.value = formatRMTime(sArr[0] + sArr[1]);
//                if (parseInt(sArr[0], 10) < 12)
//                    objFormElem.value = objFormElem.value + " AM";
//                else if (parseInt(sArr[0], 10) == 12)
//                    objFormElem.value = objFormElem.value + " PM";
//                else {
//                    if (parseInt(sArr[0], 10) > 12 && parseInt(sArr[0], 10) <= 23) {
//                        temptime = parseInt(sArr[0], 10) - 12;
//                        var strTemp = new String(temptime);
//                        if (strTemp.length == 1)
//                            strTemp = "0" + strTemp;
//                        objFormElem.value = formatRMTime(strTemp + sArr[1]) + " PM"
//                    }
//                }
//            }
//            else {
//                if (parseInt(sArr[0], 10) < 0 || parseInt(sArr[0], 10) > 12 || parseInt(sArr[1], 10) < 0 || parseInt(sArr[1], 10) > 59) {
//                    objFormElem.value = "";
//                    return true;
//                }
//                if (sArr[0].length == 1)
//                    sArr[0] = "0" + sArr[0];
//                if (sArr[1].length == 1)
//                    sArr[1] = "0" + sArr[1];
//                if (sTime.indexOf("AM") >= 0)
//                    objFormElem.value = formatRMTime(sArr[0] + sArr[1]) + " AM";
//                else
//                    objFormElem.value = formatRMTime(sArr[0] + sArr[1]) + " PM";
//                if (parseInt(sArr[0], 10) == 0 && sTime.indexOf("AM") < 0) {
//                    objFormElem.value = "";
//                    return true;
//                }
//            }
//            return true;
//        }
        //End MITS 26887: Ritesh

        function replace(sSource, sSearchFor, sReplaceWith) {
            var arr = new Array();
            arr = sSource.split(sSearchFor);
            return arr.join(sReplaceWith);
        }

        function AddActivity() {

            if (m_codeWindow != null)
                m_codeWindow.close();

            document.onCodeClose = onCodeClose;
            document.OnAddActivity = OnAddActivity;
            m_codeWindow = window.open('DiaryActivity.aspx', 'codeWnd',
						'width=400,height=200' + ',top=' + (screen.availHeight - 200) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=yes,scrollbars=yes');
            return false;
        }

        function OnAddActivity(sId, sText) {

            if (m_codeWindow != null)
                m_codeWindow.close();
            m_codeWindow = null;

            var objCtrl = document.forms[0].lstActivities;
            var bAdd = true;
            for (var i = 0; i < objCtrl.length; i++) {
                //MGaba2:MITS 20190:multiple free text were not getting added to work activity
                //In case of free text sId is ""  and objCtrl.options[i].value is also "".Hence previous condition was resulting true
                //in case multiple freetext field are added 
                //if(objCtrl.options[i].value == sId && sId != "0")	            
                if ((sId == "" && trim(objCtrl.options[i].innerText.toLowerCase() || objCtrl.options[i].textContent.toLowerCase()) == trim(sText).toLowerCase()) || (sId != "" && objCtrl.options[i].value == sId && sId != "0")) {
                    bAdd = false;
                }
            }
            if (bAdd) {
                var objOption = new Option(sText, sId, false, false);
                objCtrl.options[objCtrl.length] = objOption;
                objCtrl = null;
                //Parijat: Mits 9390 an extension to it .
                //Actually implemeting Mits 8623 where it was not completely done, Now adding '|' in place of ','.
                if (document.forms[0].txtActivities.value != "")
                    document.forms[0].txtActivities.value = document.forms[0].txtActivities.value + "|";
                document.forms[0].txtActivities.value = document.forms[0].txtActivities.value + sId + "|" + sText;
                document.forms[0].actstring.value = document.forms[0].txtActivities.value;
            }
        }

        function DelActivity() {
            var objCtrl = document.forms[0].lstActivities;
            if (objCtrl.selectedIndex < 0)
                return false;

            var bRepeat = true;
            while (bRepeat) {
                bRepeat = false;
                for (var i = 0; i < objCtrl.length; i++) {
                    // remove selected elements
                    if (objCtrl.options[i].selected) {
                        objCtrl.options[i] = null;
                        bRepeat = true;
                        break;
                    }
                }
            }
            // Now create ids list
            var sId = "";
            for (var i = 0; i < objCtrl.length; i++) {
                //Parijat: MIts 9390 extension to this .
                //Actually implemeting Mits 8623 where it was not completely done, Now adding '|' in place of ','.
                if (sId != "")
                    sId = sId + "|";
                sId = sId + objCtrl.options[i].value + "|" + replace(objCtrl.options[i].text, "|", "|");
            }
            objCtrl = null;
            document.forms[0].txtActivities.value = sId;
            document.forms[0].actstring.value = document.forms[0].txtActivities.value;
            return true;
        }

        var _isSelect = false;
        function setDefaults() {
            document.forms[0].optIssue1.checked = true;
            if (document.forms[0].txtSubjectDcboBox != null) {
                // MGaba2: MITS 14739
                _offset = 0;
                //_offset = -5.5;
                ComboInit('txtSubject');
                arrAllComboBoxes.push('txtSubject');
            }
            //Anjaneya : set txtDueDate to currentdate MITS 9621
//            var CompletedOnDate = new Date();
//            var curr_date = CompletedOnDate.getDate();
//            if (curr_date < 10)
//                curr_date = "0" + curr_date;

//            var curr_month = CompletedOnDate.getMonth();
//            curr_month += 1;
//            if (curr_month < 10)
//                curr_month = "0" + curr_month;
//            var curr_year = CompletedOnDate.getFullYear();
            //document.forms[0].txtDueDate.value = curr_month + "/" + curr_date + "/" + curr_year;

//            var hours = CompletedOnDate.getHours()
//            var minutes = CompletedOnDate.getMinutes()
//            if (minutes < 10)
//                minutes = "0" + minutes
//            var time;
//            if (hours > 11) {
//                time = "PM"
//            }
//            else {
//                time = "AM"
//            }
            //document.forms[0].txtDueTime.value = hours + ":" + minutes + " " + time;

            //if ((document.forms[0].DefaultAssignedTo != null) && (document.forms[0].DefaultAssignedTo.value == "True")) {
            //    objCtrl = document.getElementById('lstUsers');
            //    if (document.forms[0].LoginUserName.value != "") {
            //        objOption = new Option(document.forms[0].LoginUserName.value, document.forms[0].LoginID.value, false, false);
            //        objCtrl.options[objCtrl.length] = objOption;
            //        objCtrl = null;
            //    }
            //    zmohammad MITs 33655 Diary Initialisation script : Start
            //    if (document.forms[0].UserStr.value != null && document.forms[0].UserStr.value != "") {
            //        document.forms[0].UserStr.value = document.forms[0].UserStr.value + ' ' + document.forms[0].LoginUserName.value;
            //    }
            //    else {
            //        document.forms[0].UserStr.value = document.forms[0].LoginUserName.value;
            //    }
            //    if (document.forms[0].UserIdStr.value != null && document.forms[0].UserIdStr.value != "") {
            //        document.forms[0].UserIdStr.value = document.forms[0].UserIdStr.value + ' ' + document.forms[0].LoginID.value;
            //    }
            //    else {
            //        document.forms[0].UserIdStr.value = document.forms[0].LoginID.value;
            //    }
            //    zmohammad MITs 33655 Diary Initialisation script : End
            //  }
        }
        function check() {
            if (document.forms[0].optIssue1.checked) {
                document.forms[0].txtIssuePeriod.disabled = true;
                document.forms[0].txtIssuePeriod.value = "";
            }
            else if (document.forms[0].optIssue2.checked) {
                document.forms[0].txtIssuePeriod.disabled = false;
            }
        }

        function AddCustomizeUser() {
            document.getElementById('lstUsers').add(new Option("CSC", "csc"));
            document.getElementById('lstUsers').add(new Option("cul1", "cul1"));
            document.getElementById('lstUsers').add(new Option("cul2", "cul2"));
            document.getElementById('lstUsers').add(new Option("cul3", "cul3"));

            document.getElementById('UserIdStr').value = "cul1 cull3 cul4";

            return false;
        }

        

        </script>
        
    
</head>
<body onload="setDefaults();check();try{parent.MDIScreenLoaded();}catch(b){}">
    <form id="frmData" runat="server">
        <input type="hidden" id="wsrp_rewrite_action_1" value=""/>
        <input type="hidden" value="" id="ouraction"/>
        <input type="hidden" value="" id="diaryid"/>
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <table border="0"  class="toolbar" cellpadding="0" cellspacing="0">
             <tr>
                 <td colspan="2" align="center">
                   <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_savemapping_active.png" class="bold" ToolTip="<%$ Resources:ttSave %>"
                        runat="server" OnClientClick="return ValForm();;" OnClick="btnSave_Click" />
                   <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                        runat="server" OnClick="btnCancel_Click" />
                 </td>
            </tr>
         </table>   
         <table border="0" align="left" width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="5" class="ctrlgroup"><asp:Label ID="lblCreateDiary" runat="server" Text="<%$ Resources:lblCreateDiary %>"></asp:Label></td>
            </tr>
            <tr>  
                 <td height="10px"></td>
            </tr>                               
            <tr>  
                           
                 
                 <td class="required" valign="top" align="left"><asp:Label ID="lblAssignDry" runat="server" Text="<%$ Resources:lblAssignDry %>"></asp:Label>:</td>                                                                   
                 <td>                   
                    <select multiple="true" id="lstUsers" size="3" runat="server" style="width: 190px; height: 71px">
                    </select><input type="button" class="CodeLookupControl" value="" id="cmdAddCustomizedListUser" onclick="AddCustomizedListUser('creatediary','lstUsers','UserIdStr','UserStr')"/>
                    <input type="button" class="BtnRemove" value="" id="cmdDelCustomizedListUser" style="width:21px" onclick="DelCustomizedListUser('creatediary','lstUsers','UserIdStr','UserStr')"/>
                    <input type="hidden"  runat="server" name="" value="" id="UserIdStr"/>
                    <input type="hidden"  runat="server" value="" id="UserStr"/>
                    <input type="hidden" runat="server" name="" value="" id="LoginUserName"/>
                    <input type="hidden" runat="server" value="" id="LoginID"/>
                    <input type="hidden" runat="server" value="False" id="DefaultAssignedTo"/>                 
                 </td>

                                      
                <td class="required" align="left" ><asp:Label ID="lblTaskName" runat="server" Text="<%$ Resources:lblTaskName %>"></asp:Label>:</td>               
                <td style="width:400px;">
                <table>
                <tr><td>
                    <input type="text" value="" maxlength="50"  runat="server" id="txtSubject" class="comboTxt" style="width:325px;"/>
                    <asp:DropDownList  ID="txtSubjectDcboBox" CssClass="comboDropDown" runat="server" onchange="UpdateDCBoxGeneric(event);" Width="340px"></asp:DropDownList>
                    </td></tr></table>
                </td>
            </tr>
            
            <tr>
                <td  align="left"><asp:Label ID="lblAttachTo" runat="server" Text="<%$ Resources:lblAttachTo %>"></asp:Label>:</td>
                <td>
                    <input type="text"  runat="server" value="" id="txtClaimNumber" onkeypress="return eatKeystrokes(event);" size="20"/>
                    <input type="button" class="CodeLookupControl" id="testbtn" onclick="return lookupData('txtClaimNumber' ,'claim',1,'txtClaimNumber',6);" value=""/>
                    <input type="hidden"  runat="server" value="" id="txtClaimNumber_cid"/>
                </td>       
                <td  align="left"><asp:Label ID="lblDueDate" runat="server" Text="<%$ Resources:lblDueDate %>"></asp:Label>:</td>
                <td>
                    <input type="text"  runat="server" id="txtDueDate" size="15" onblur="dateLostFocus(this.id);"/>               
                    <input type="text"  runat="server" value="" id="txtDueTime" size="15" onblur="timeLostFocus(this.id);"/>
                    <script type="text/javascript">
                        $(function () {
                            $("#txtDueDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                    </script>
	            </td>
            </tr>

            <tr>
                <td  valign="top" align="left">
                    <label><asp:Label ID="lblWorkAct" runat="server" Text="<%$ Resources:lblWorkAct %>"></asp:Label>:</label>
                </td>
                <td>
                    <asp:ListBox ID="lstActivities" runat="server" Width="169px" SelectionMode="Multiple"></asp:ListBox>
                    <input type="button"  runat="server" class="CodeLookupControl" value="" id="cmdAddActivity" onclick="AddActivity()"/>
                    <input type="button"  runat="server" class="BtnRemove" value="" id="cmdDelActivity" onclick="DelActivity()"/>
                    <input type="hidden"  runat="server" value="" id="actstring"/>
                    <input type="hidden" id="txtActivities" runat="server"  value=""/>
                </td>

           
                <td  align="left">
                    <label><asp:Label ID="lblEstTime" runat="server" Text="<%$ Resources:lblEstTime %>"></asp:Label>:</label>
                </td>
                <td>
                    <input type="text" value="" id="txtEstTime" runat="server" size="15" onblur="numLostFocusNonNegative(this);" maxlength="14"/><%--amitosh for mits 28027--%>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="radio" value="1" onclick="check();" id="optIssue1" runat="server"  name="optIssue" checked="true"/><asp:Label ID="lblIssueOnCom" runat="server" Text="<%$ Resources:lblIssueOnCom %>"></asp:Label>
                </td>
            
                <td>&nbsp;</td>
                <td>
                    <input type="radio"  value="2"  runat="server" onclick="check();" id="optIssue2"  name="optIssue" checked="true"/><asp:Label ID="lblIssueEvry" runat="server" Text="<%$ Resources:lblIssueEvry %>"></asp:Label>
                    <input type="text"  value="" runat="server"  size="5" id="txtIssuePeriod"/>
                    <label><asp:Label ID="lblDaysCom" runat="server" Text="<%$ Resources:lblDaysCom %>"></asp:Label></label>
                </td>
            </tr>

            <tr>
                 <td align="left">
                    <label><asp:Label ID="lblPriority" runat="server" Text="<%$ Resources:lblPriority %>"></asp:Label>:</label>
                 </td>
             <td >
                <asp:DropDownList ID="ddlPriority" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Optional %>" Value="1"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Important %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Required %>" Value="3"></asp:ListItem>
                </asp:DropDownList>
             </td>
            
                <td valign="top" align="left">
                    <label><asp:Label ID="lblNotes" runat="server" Text="<%$ Resources:lblNotes %>"></asp:Label>:</label>
                </td>
             <td>
                <textarea cols="50" wrap="soft" rows="4" id="txtNotes" runat="server"></textarea>
            </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkAutoConfrim" enable="true"/><asp:Label ID="lblAutoNotif" runat="server" Text="<%$ Resources:lblAutoNotif %>"></asp:Label>
             </td>
           
            <!--07/06/2011 SMISHRA54: WPA Email Notification-->
            
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkEmailNotification" /><asp:Label ID="lblEmailNotif" runat="server" Text="<%$ Resources:lblEmailNotif %>"></asp:Label>
             </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkDiaryOverDueNotify" /><asp:Label ID="lblAutoNotifLate" runat="server" Text="<%$ Resources:lblAutoNotifLate %>"></asp:Label>
             </td>            
            <!--07/06/2011 SMISHRA54: End--> <!--igupta3 jira-439 -->             
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkNotRoutable" /><asp:Label ID="lblNotRoutable" runat="server" Text="<%$ Resources:lblNotRoutable %>"></asp:Label>
             </td>
                <td>&nbsp;</td>
                            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>
                <input type="checkbox" value="true" runat="server" id="chkNotRollable" /><asp:Label ID="lblNotRollable" runat="server" Text="<%$ Resources:lblNotRollable %>"></asp:Label>
             </td>
            </tr>
            <%-- Commented by Nitin for R5-R4 merging of Mits 13019 
                <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="checkbox" runat="server" disabled="true" value="true"/>Export To Schedule+
                </td>
            </tr>--%>
            
            <%--<tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnSave" runat="server" CssClass="button"  Text="Save" 
                        OnClientClick = "return ValForm();; " onclick="btnSave_Click" 
                        Width="35px" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button"  Text="Cancel" 
                        onclick="btnCancel_Click" Width="42px" />
                </td>
            </tr>--%>
            </table>
        
     </form>
</body>
</html>
