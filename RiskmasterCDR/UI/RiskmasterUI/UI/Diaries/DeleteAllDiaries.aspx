﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteAllDiaries.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryAllDiaries" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Delete All Diaries...</title>
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css"></link>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css"></link>
    
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    <script src="../../Scripts/form.js" type="text/javascript"></script>
    
    <%--Changed for MITS 14770--%>
      <script type="text/javascript">
          function ResetOpenDiaryFlag() {
              if (window.opener.document.getElementById('containsopendiaries') != null)
                  window.opener.document.getElementById('containsopendiaries').value = false;
          }
	    </script>
	    
 </head>
<body>
    <form id="frmData" runat="server">
    <table border="0" align="center" width="80%">
        <tr>
            <td colspan="2" align="center">
                <uc1:ErrorControl ID="ErrorControl" runat="server" />  	        
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
              <asp:Label ID="lblDeleteConfirmResrc" runat="server" Text="<%$ Resources:lblDeleteConfirmResrc %>"></asp:Label> 
      	       
            </td>
        </tr>
        <tr>
            <td width="50%" align="center">
                <asp:Button ID="btnOk" runat="server" Text="<%$ Resources:btnOk %>" CssClass="button" 
                    onclick="btnOk_Click" OnClientClick="ResetOpenDiaryFlag();" />
            </td>        
            <td width="50%" align="center">
                <asp:Button ID="btnCancel"  runat="server" Text="<%$ Resources:btnCancel %>" OnClientClick="self.close();" CssClass="button" />
            </td>
        </tr>
   </table>
    </form>
</body>
</html>
