﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class CompleteDiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Rakhel ML Change
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CompleteDiary.aspx"), "CompleteDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CompleteDiaryValidations", sValidationResources, true);
            //Rakhel ML Change

            //Deb: ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                 
                    
                }
            //Deb: ML Changes
            
            XmlDocument diaryDoc = null;
            string BaseCurrency = string.Empty;
          
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["entryid"] != null)
                    {
                        ViewState["entryid"] = Request.QueryString["entryid"];
                    }
                    else
                    {
                        ViewState["entryid"] = "";
                    }

                    if (Request.QueryString["assigninguser"] != null)
                    {
                        ViewState["assigninguser"] = Request.QueryString["assigninguser"];
                    }
                    else
                    {
                        ViewState["assigninguser"] = "";
                    }

                    if (Request.QueryString["assigneduser"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }

                    //pkandhari Jira 6412 starts
                    if (Request.QueryString["assignedgroup"] != null)
                    {
                        ViewState["assignedgroup"] = Request.QueryString["assignedgroup"];
                    }
                    else
                    {
                        ViewState["assignedgroup"] = "";
                    }
                    //pkandhari Jira 6412 ends
                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";//"EVENT";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";// "1";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }
					 //Start: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; added ifthenelse in case multiple entry ids are present

                    if (Convert.ToString(ViewState["entryid"]).IndexOf(',') == -1)
                    {

                    diaryDoc = LoadDiary();
                    BaseCurrency = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary/BaseCurrency").InnerText;

                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(BaseCurrency);
                    System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(BaseCurrency);

                    ViewState["duedate"] = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary/CompleteDate").InnerText;
                    ViewState["statusopen"] = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary/StatusOpen").InnerText;
                    ViewState["autoconfirm"] = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary/AutoConfirm").InnerText;
                    ViewState["notifyflag"] = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary/NotifyFlag").InnerText;
                    //ViewState["mail_disabled"] = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary/MAIL_DISABLED").InnerText;
                    //dvatsa-JIRA 11627(start)
                    ViewState["mail_enabled"] = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary/MAIL_ENABLED").InnerText;
                    //dvatsa-JIRA 11627(end)
                    ViewState["activitystring"] = "";


                    //setting enable or disable of notify checkbox 
                    //zmohammad MITs 35145 : Removing redundant check for autoconfirm.. Start
                    if (ViewState["notifyflag"].ToString().ToLower() == "true")
                    {
                        chkNotifyTaskAssigner.Checked = true;
                    }

                    //if ((ViewState["notifyflag"].ToString().ToLower() == "false") ||
                    //     (ViewState["mail_disabled"].ToString().ToLower() == "false") || (ViewState["assigninguser"].ToString() == ""))
                    //{
                    //    chkNotifyTaskAssigner.Enabled = false;
                    //}
                    //dvatsa JIRA 11627(start)
                    if (((ViewState["notifyflag"].ToString().ToLower() == "false") || (ViewState["assigninguser"].ToString() == "")) && (ViewState["mail_enabled"].ToString().ToLower() == "true"))
                    {
                        chkNotifyTaskAssigner.Enabled = true;
                    }
                    else if (((ViewState["notifyflag"].ToString().ToLower() == "false") || (ViewState["assigninguser"].ToString() == "")) && (ViewState["mail_enabled"].ToString().ToLower() == "false"))
                    {
                        chkNotifyTaskAssigner.Enabled = false;
                    }
                    //dvatsa JIRA 11627 (end)
                    //zmohammad MITs 35145 : Removing redundant check for autoconfirm... End
                    //Mits 15176: Start Asif
                    XmlNodeList activitiesList = null;
                    XmlNode loadDiaryNode = null;
                    loadDiaryNode = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary");
                    activitiesList = diaryDoc.SelectNodes("ResultMessage/Document/LoadDiary/Activities/Activity");
                    if (activitiesList != null)
                    {
                        if (activitiesList.Count > 0)
                        {
                            ViewState["activitystring"] = loadDiaryNode.SelectSingleNode("Activities/sActs").InnerText;
                        }  
                    }
                    //Mits 15176: End Asif
				 }

                    else //if multiple diary entries are set to complete, then LoadDiary is called for all before savediary to keep 15176 mits fix unchanged.
                    {
                        chkNotifyTaskAssigner.Checked = true;
                        chkNotifyTaskAssigner.Enabled = false;
                    }
                    //End: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; added ifthenelse in case multiple entry ids are present
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            XmlDocument diaryDoc = null;
            try
            {
                diaryDoc = SaveDiary();

                //if diarydoc has success message then redirect to diarylist 
                //else throw new execption with error messge in it

                if (diaryDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                {
                   Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
                }
                else
                {
                    // Changed by Amitosh For MITS 24229 (03/04/2011)
                  //  throw new ApplicationException(diaryDoc.InnerXml); 
                    throw new ApplicationException(diaryDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText); 
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        private XmlDocument LoadDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            string sFreezeDate = string.Empty;
            try
            {
                txtCompletedOn.Text = AppHelper.GetDate(DateTime.Now.ToShortDateString());
                serviceMethodToCall = "WPAAdaptor.LoadDiary";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForLoadDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput =  dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                //nsachdeva2 MITS:23654 09/19/2011
                sFreezeDate = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary/FreezeCompletionDate").InnerText;
                if (sFreezeDate == "True") 
                {
                    //Rakhel - ML Changes
                    //Page.ClientScript.RegisterStartupScript( typeof(Page),"DisableCalender", "<script> document.getElementById('txtCompletedOnbtn').disabled =" + sFreezeDate.ToLower() + "; </script>");
                    Page.ClientScript.RegisterStartupScript(typeof(Page), "DisableCalender", "<script> var btnCompletedOn = $('#txtCompletedOn'); $.datepicker._disabledInputs = [btnCompletedOn[0]]; </script>");
                    //txtCompletedOn.Text = AppHelper.GetDate(DateTime.Now.ToShortDateString());
                    txtCompletedOn.Enabled = !Convert.ToBoolean(sFreezeDate);
                }
                //End MITS:23654
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlDocument SaveDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            
            //Start: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; 
            string[] arrEntryIds ;
            int iIteratorVar = 0;
            XmlDocument saveDiaryDoc = null;
            XmlNode saveDiaryNode = null;
            string sSaveDiaryInnerXML = string.Empty;
            //End: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; 
            try
            {
                //Start: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; added ifthenelse in case multiple diaries are selected

                if (Convert.ToString(ViewState["entryid"]).IndexOf(',') == -1)
                {
                    serviceMethodToCall = "WPAAdaptor.SaveDiary";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForSaveDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
                }
                else
                {
                    serviceMethodToCall = "WPAAdaptor.SaveDiary";

                    saveDiaryDoc = new XmlDocument();
                    saveDiaryNode = saveDiaryDoc.CreateElement("MultipleDiaries");

                    arrEntryIds = Convert.ToString(ViewState["entryid"]).Split(new char[] { ',' });
                    for (iIteratorVar = 0; iIteratorVar < arrEntryIds.Length; iIteratorVar++)
                    {
                        ViewState["entryid"] = arrEntryIds[iIteratorVar];
                        //send default values as these are filled from LoadDiary and sent as is to Savediary
                        //Thus to avoid webservice hit in a loop LoadDiary is called in applayer for this case
                        ViewState["duedate"] = "";
                        ViewState["autoconfirm"] = "0";
                        ViewState["activitystring"] = "";                        

                        //to be fill up by viewstate
                        inputDocNode = GetInputDocForSaveDiary();
                        sSaveDiaryInnerXML = sSaveDiaryInnerXML + inputDocNode.OuterXml; 
                        
                        //saveDiaryNode.AppendChild(inputDocNode);
                    }
                    saveDiaryNode.InnerXml = sSaveDiaryInnerXML;

                    dHelper = new DiariesHelper();
                    diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(saveDiaryNode, serviceMethodToCall);

                    diaryDoc = new XmlDocument();
                    diaryDoc.LoadXml(diaryServiceOutput);
                    return diaryDoc;
                }
                //End: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; added ifthenelse in case multiple diaries are selected
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        
        private XmlNode GetInputDocForSaveDiary()
        {
            XmlDocument saveDiaryDoc = null;
            XmlNode saveDiaryNode = null;
            XmlNode entryIdNode = null;
            XmlNode assigningUserNode = null;
            XmlNode assignedgroupNode = null; //pkandhari Jira 6412
            XmlNode dueDateNode = null;
            XmlNode responseDateNode = null;
            XmlNode responseNode = null;
            XmlNode teTrackedNode = null;
            XmlNode teTotalHoursNode = null;
            XmlNode teExpensesNode = null;
            XmlNode teEndTimeNode = null;
            XmlNode startTimeNode = null;
            XmlNode statusOpenNode = null;
            XmlNode autoConfirmNode = null;
            XmlNode activityStringNode = null;


            saveDiaryDoc = new XmlDocument();
            saveDiaryNode = saveDiaryDoc.CreateElement("SaveDiary");
            
            entryIdNode = saveDiaryDoc.CreateElement("EntryId");
            entryIdNode.InnerText = ViewState["entryid"].ToString();
            saveDiaryNode.AppendChild(entryIdNode);

            assigningUserNode = saveDiaryDoc.CreateElement("AssigningUser");
            assigningUserNode.InnerText = ViewState["assigninguser"].ToString();
            saveDiaryNode.AppendChild(assigningUserNode);

            //pkandhari Jira 6412 starts
            assignedgroupNode = saveDiaryDoc.CreateElement("AssignedGroup");
            assignedgroupNode.InnerText = ViewState["assignedgroup"].ToString(); ;
            saveDiaryNode.AppendChild(assignedgroupNode);
            //pkandhari Jira 6412 ends

            dueDateNode = saveDiaryDoc.CreateElement("DueDate");
            dueDateNode.InnerText = ViewState["duedate"].ToString();
            saveDiaryNode.AppendChild(dueDateNode);

            responseDateNode = saveDiaryDoc.CreateElement("ResponseDate");
            responseDateNode.InnerText = txtCompletedOn.Text.Trim();
            saveDiaryNode.AppendChild(responseDateNode);

            responseNode = saveDiaryDoc.CreateElement("Response");
            responseNode.InnerText = txtAreaCompleteResponse.Text; 
            saveDiaryNode.AppendChild(responseNode);

            teTrackedNode = saveDiaryDoc.CreateElement("TeTracked");
            if (chkTrack.Checked == true)
            {
                teTrackedNode.InnerText = "True";
            }
            else
            {
                teTrackedNode.InnerText = "";
            }
            saveDiaryNode.AppendChild(teTrackedNode);

            teTotalHoursNode = saveDiaryDoc.CreateElement("TeTotalHours");
            teTotalHoursNode.InnerText = txtTETotalTime.Text.Trim(); //aaggarwal29: fixed for MITS 36840 / JIRA 1699
            saveDiaryNode.AppendChild(teTotalHoursNode);

            teExpensesNode = saveDiaryDoc.CreateElement("TeExpenses");
            //teExpensesNode.InnerText = txtTEExpenses.Text.Trim(); 
            teExpensesNode.InnerText = txtTEExpenses.AmountInString;
            saveDiaryNode.AppendChild(teExpensesNode);

            teEndTimeNode = saveDiaryDoc.CreateElement("TeEndTime");
            teEndTimeNode.InnerText = txtTEEndTime.Text.Trim(); 
            saveDiaryNode.AppendChild(teEndTimeNode);

            startTimeNode = saveDiaryDoc.CreateElement("TeStartTime");
            startTimeNode.InnerText = txtTEStartTime.Text.Trim(); 
            saveDiaryNode.AppendChild(startTimeNode);

            statusOpenNode = saveDiaryDoc.CreateElement("StatusOpen");
            statusOpenNode.InnerText = "False";
            saveDiaryNode.AppendChild(statusOpenNode);

            if (ViewState["autoconfirm"].ToString() != "")
            {
                autoConfirmNode = saveDiaryDoc.CreateElement("AutoConfirm");

                autoConfirmNode.InnerText = ViewState["autoconfirm"].ToString();
                saveDiaryNode.AppendChild(autoConfirmNode);
            }

            activityStringNode = saveDiaryDoc.CreateElement("ActivityString");
            activityStringNode.InnerText = ViewState["activitystring"].ToString();
            saveDiaryNode.AppendChild(activityStringNode);

            saveDiaryDoc.AppendChild(saveDiaryNode);

            return saveDiaryNode;
        }

        private XmlNode GetInputDocForLoadDiary()
        {
            //<LoadDiary base="" entryid="128" notify="false" root="" />

            XmlDocument loadDiaryDoc = null;

            XmlNode loadDiaryNode = null;
            XmlAttribute atbBase = null;
            XmlAttribute atbEntryId = null;
            XmlAttribute atbNotify = null;
            XmlAttribute atbRoot = null;
            XmlAttribute atbAssignedGroup = null; //pkandhari Jira 6412

            try
            {
                loadDiaryDoc = new XmlDocument();
                loadDiaryNode = loadDiaryDoc.CreateElement("LoadDiary");

                atbBase = loadDiaryDoc.CreateAttribute("base");
                atbBase.Value = "";

                atbEntryId = loadDiaryDoc.CreateAttribute("entryid");
                atbEntryId.Value = ViewState["entryid"].ToString();

                atbNotify = loadDiaryDoc.CreateAttribute("notify");
                atbNotify.Value = "";

                atbRoot = loadDiaryDoc.CreateAttribute("root");
                atbRoot.Value = "";

                atbAssignedGroup = loadDiaryDoc.CreateAttribute("AssignedGroup");//pkandhari Jira 6412
                atbAssignedGroup.Value = "";//pkandhari Jira 6412

                loadDiaryNode.Attributes.Append(atbBase);
                loadDiaryNode.Attributes.Append(atbEntryId);
                loadDiaryNode.Attributes.Append(atbNotify);
                loadDiaryNode.Attributes.Append(atbRoot);
                loadDiaryNode.Attributes.Append(atbAssignedGroup); //pkandhari Jira 6412

                loadDiaryDoc.AppendChild(loadDiaryNode);
                return loadDiaryNode;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

    }
}
