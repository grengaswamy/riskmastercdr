﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;
using Infragistics.WebUI.WebSchedule;
using Infragistics.WebUI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries.DiaryCalendar
{
    public partial class DiaryCalendar : System.Web.UI.Page
    {
        /// <summary>
        /// Page for Diary Calendar
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument dairyDoc = null;
            XmlElement temp = null;
            HtmlControl dataFrame = null;
            DiaryCalendarHelper objDairyHelper = null;
            HtmlControl htmlControl = null;
            string code = string.Empty;
            bool isBESEnabled = false;
            string calendarView = "MonthView.aspx";
            string diaryAction = "default";
            string userName = string.Empty, s_PageCode = string.Empty, s_Id = string.Empty, s_parentpage=string.Empty,s_SelectedDate=string.Empty;            

            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DiaryCalendar.aspx"), "DiaryCalValidations", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()).Replace("\\\\", "\\");  // pyadav25 - MITS 31743 - 6/6/2013
                ClientScript.RegisterStartupScript(this.GetType(), "DiaryCalValidations", sValidationResources, true);
                string sValResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DiaryCalendar.aspx"), "DiaryCalendarValidations", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "DiaryCalendarValidations", sValResources, true);
                if (!IsPostBack)
                {
                    dataFrame = (HtmlControl)this.FindControl("DataFrame");
                }//postback
                
                if (Request.QueryString["parentpage"] != null)
                {
                    if (Request.QueryString["parentpage"]== "creatediary")
                    {
                        s_PageCode = "2";
                        btnCreateDairy.Visible = false;
                        btnPrint.Visible = false;
                        btnPeek.Visible = false;
                        btnPeekProcOffice.Visible = false;
                        s_parentpage = "creatediary";
                    }
                }
                if (Request.Params["id"] != null)
                {
                    s_Id = Convert.ToString(Request.Params["id"]);
                }
                if (Request.Params["dt"] != null)
                {
                    s_SelectedDate = Convert.ToString(Request.Params["dt"]);
                }

                if (Request.Params["IsPeek"] != null)
                {//This is Peek comes into flow when the page is loaded from the Integate.cs. The scenarios are namely
                    //creating/editing a diary
                    //rsharma220 MITS 31030
                    if (Riskmaster.Common.Conversion.ConvertStrToBool(Request.Params["IsPeek"]))
                    {
                        btnPeek.Value = GetResourceValue("btnHome", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
                        diaryAction = "peek";
                        btnPeekProcOffice.Disabled = true;
                        lblMessage.Text = GetResourceValue("lblPeekDiary", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                    }
                    else
                    {
                        //rsharma220 MITS 31456
                        btnPeek.Value = GetResourceValue("btnpeek", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
                    }

                }
                if (Request.Params["IsProcessingOffice"] != null)
                {//This is Peek comes into flow when the page is loaded from the Integate.cs. The scenarios are namely
                    //creating/editing a diary
                    //rsharma220 MITS 31030
                    if (Riskmaster.Common.Conversion.ConvertStrToBool(Request.Params["IsProcessingOffice"]))
                    {
                        btnPeekProcOffice.Value = GetResourceValue("btnHome", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
                        diaryAction = "processingoffice";
                        btnPeek.Disabled = true;
                        lblMessage.Text = GetResourceValue("lblViewDiaries", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                    }
                    else
                    {
                        //rsharma220 MITS 31456
                        btnPeekProcOffice.Value = GetResourceValue("btnpeekusergroup", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
                    }
                }
                objDairyHelper = new DiaryCalendarHelper();
                dairyDoc = objDairyHelper.GetOnLoadInformation(ref userName);
                //added by Nitin for Mits 14641
                hdnUserName.Value = userName;

                //When comes from style window
                if (Request.Params["CalendarView"] != null)
                {
                    calendarView = Request.Params["CalendarView"].ToString();
                    htmlControl = (HtmlControl)this.FindControl("hdnSelectedControl");
                    htmlControl.Attributes["value"] = calendarView;

                   // if (Request.Params["isHome"] != null && Request.Params["isHome"].ToString().Trim() != string.Empty)//ksahu5 ML change
                    if (HttpUtility.ParseQueryString(Request.RawUrl).Get("isHome") != null && HttpUtility.ParseQueryString(Request.RawUrl).Get("isHome").ToString().Trim() != string.Empty)//ksahu5 ML Change
                    {
                        //btnPeek.Value = Request.Params["isHome"].ToString();//ksahu5 ML Change
                        btnPeek.Value = HttpUtility.ParseQueryString(Request.RawUrl).Get("isHome");//ksahu5 ML Change
                    }
                    if (Request.Params["action"] != null)
                    {
                        hdnDiaryAction.Value = Request.Params["action"].ToString();
                        if (Request.Params["action"].ToString() == "peek")
                        {
                            btnPeekProcOffice.Disabled = true;
                        }
                        else if (Request.Params["action"].ToString() == "processingoffice")
                        {
                            btnPeek.Disabled = true;
                        }
                        DataFrame.Attributes["src"] = calendarView + "?action=" + Request.Params["action"].ToString() + "&FromStyle=true" + "&id=" + s_Id + "&parentpage=" + s_parentpage + "&dt=" + s_SelectedDate;
                    }

                    temp = (XmlElement)dairyDoc.SelectSingleNode("//BESEnabled");
                    if (temp != null)
                    {
                        //rsharma220 MITS 31030
                        isBESEnabled = Riskmaster.Common.Conversion.ConvertStrToBool(temp.InnerText);
                        htmlControl = (HtmlControl)this.FindControl("btnPeekProcOffice");
                        if (isBESEnabled)
                        {
                            htmlControl.Disabled = false;
                        }
                        else
                        {
                            htmlControl.Disabled = true;
                        }

                        if (isBESEnabled)
                        {
                            htmlControl = (HtmlControl)this.FindControl("hdnIsBESEnabled");
                            htmlControl.Attributes["value"] = "true";

                        }
                        else
                        {
                            htmlControl = (HtmlControl)this.FindControl("hdnIsBESEnabled");
                            htmlControl.Attributes["value"] = "false";
                        }
                        ViewState["IsBESEnabled"] = isBESEnabled.ToString();
                    }
                }
                else
                {
                    temp = (XmlElement)dairyDoc.SelectSingleNode("//CalendarView");
                    if (temp != null)
                    {
                        if (!string.IsNullOrEmpty(s_PageCode) && s_PageCode.Equals("2"))
                            code = s_PageCode;
                        else
                            code = temp.InnerText;
                        if (code == "0")
                        {
                            //priya-start
                            htmlControl = (HtmlControl)this.FindControl("hdnSelectedControl");
                            htmlControl.Attributes["value"] = "MonthView.aspx";
                            //priya-end
                            DataFrame.Attributes["src"] = "MonthView.aspx?action=" + diaryAction;
                        }
                        else if (code == "1")
                        {
                            //priya-start
                            htmlControl = (HtmlControl)this.FindControl("hdnSelectedControl");
                            htmlControl.Attributes["value"] = "WeekView.aspx";
                            //priya-end
                            DataFrame.Attributes["src"] = "WeekView.aspx?action=" + diaryAction;
                        }
                        else if (code == "2")
                        {
                            //priya-start
                            htmlControl = (HtmlControl)this.FindControl("hdnSelectedControl");
                            htmlControl.Attributes["value"] = "DayView.aspx";
                            //priya-end
                            DataFrame.Attributes["src"] = "DayView.aspx?action=" + diaryAction + "&id=" + s_Id + "&parentpage=" + s_parentpage + "&dt=" + s_SelectedDate; 
                        }
                    }
                    temp = (XmlElement)dairyDoc.SelectSingleNode("//BESEnabled");
                    if (temp != null)
                    {
                        //rsharma220 MITS 31030
                        isBESEnabled = Riskmaster.Common.Conversion.ConvertStrToBool(temp.InnerText);
                        htmlControl = (HtmlControl)this.FindControl("btnPeekProcOffice");
                        if (isBESEnabled)
                        {
                            htmlControl.Disabled = false;
                        }
                        else
                        {
                            htmlControl.Disabled = true;
                        }

                        if (isBESEnabled)
                        {
                            htmlControl = (HtmlControl)this.FindControl("hdnIsBESEnabled");
                            htmlControl.Attributes["value"] = "true";

                        }
                        else
                        {
                            htmlControl = (HtmlControl)this.FindControl("hdnIsBESEnabled");
                            htmlControl.Attributes["value"] = "false";
                        }
                        ViewState["IsBESEnabled"] = isBESEnabled.ToString();
                    }
                    //set current date to be used for Print functionality
                    hdnCurrentDate.Value = DateTime.Now.Date.ToShortDateString();
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (dairyDoc != null)
                {
                    dairyDoc = null;
                }
                if (temp != null)
                {
                    temp = null;
                }
                if (objDairyHelper != null)
                {
                    objDairyHelper = null;
                }
                if (htmlControl != null)
                {
                    htmlControl = null;
                }
                if (dataFrame != null)
                {
                    dataFrame = null;
                }
            }
        }
        protected void btnCreateDairy_Click(object sender, EventArgs e)
        {
            string isPeek = string.Empty;
            string isProcessingOffice = string.Empty;

            try
            {
                if (btnPeek.Value == GetResourceValue("btnHome", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()))
                {
                    isPeek = "true";
                }
                else if (btnPeek.Value == GetResourceValue("btnpeek", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()))
                {
                    isPeek = "false";
                }

                if (btnPeekProcOffice.Value == GetResourceValue("btnHome", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()))
                {
                    isProcessingOffice = "true";
                }
                else
                {
                    isProcessingOffice = "false";
                }

                //RMA - 4691
                //Response.Redirect("/RiskmasterUI/UI/Diaries/CreateDiary.aspx?assigneduser=" + hdnUserName.Value + "&isdiarycalendar=true&isdiarycalendarpeek=" + isPeek + "&isdiarycalendarprocessingoffice=" + isProcessingOffice);
                Response.Redirect("/RiskmasterUI/UI/FDM/creatediary.aspx?assignedusername=" + hdnUserName.Value + "&isdiarycalendar=true&isdiarycalendarpeek=" + isPeek + "&isdiarycalendarprocessingoffice=" + isProcessingOffice + "&CalledBy=creatediary");
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string isPeek = string.Empty;
            string isProcessingOffice = string.Empty;

            try
            {
                if (btnPeek.Value == GetResourceValue("btnHome", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()))
                {
                    isPeek = "true";
                }
                else if (btnPeek.Value == GetResourceValue("btnpeek", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()))
                {
                    isPeek = "false";
                }

                if (btnPeekProcOffice.Value == GetResourceValue("btnHome", ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()))
                {
                    isProcessingOffice = "true";
                }
                else
                {
                    isProcessingOffice = "false";
                }

                Response.Redirect("/RiskmasterUI/UI/Diaries/DiaryListCalendar.aspx?assigneduser=" + hdnUserName.Value + "&isdiarycalendar=true&isdiarycalendarpeek=" + isPeek + "&isdiarycalendarprocessingoffice=" + isProcessingOffice);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("DiaryCalendar.aspx"), strResourceType).ToString();
        }
    }
}