﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Infragistics.WebUI.WebSchedule;
using Infragistics.WebUI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using System.Globalization; //MITS 34511 - hlv

namespace Riskmaster.UI.Diaries.DiaryCalendar
{
    public partial class WeekView : System.Web.UI.Page
    {
        private string PageID = RMXResourceProvider.PageId("WeekView.aspx"); //MITS 34511 - hlv
        /// <summary>
        /// Page load for Week View
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            TimeZone timeZone = null;
            TimeZoneInfo timeZoneInfo = null;
            DiaryCalendarHelper objDiaryHelper = null;
            XmlDocument diaryDoc = null;
            CurrentWebSchedule objCurrentControl = null;
            CurrentTimeFrame objCurrentDates = null;
            string fromDate = string.Empty;
            string toDate = string.Empty;
            string uiAction = string.Empty;
            bool bFromSTyle = false;
            string userName = string.Empty;

            //MITS 34511 - hlv begin
            //WebCalendarView1.CultureInfo = new System.Globalization.CultureInfo(AppHelper.GetCulture());
            WeekViewWebScheduleInfo.CultureInfo = new System.Globalization.CultureInfo(AppHelper.GetCulture());
            DiaryWeekView.CultureInfo = new System.Globalization.CultureInfo(AppHelper.GetCulture());
          //  DiaryWeekView.CaptionFormatString = AppHelper.GetDateFormat();
            //WebCalendarView1.FooterFormat = AppHelper.GetResourceValue(this.PageID, "lblToday", "0") + " {0:" + AppHelper.GetDateFormat() + "}";
            //MITS 34511 - hlv end

            try
            {
                timeZone = TimeZone.CurrentTimeZone;
                timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone.StandardName);
                this.DiaryWeekView.NavigationAnimation = Infragistics.WebUI.WebSchedule.AnimationRate.Linear;
                this.DiaryWeekView.AppointmentFormatString = "<SUBJECT>";
                //this.DiaryWeekView.AppointmentTooltipFormatString = "Number of Dairies due:<SUBJECT>";
                this.DiaryWeekView.AppointmentTooltipFormatString = AppHelper.GetResourceValue(this.PageID, "lblNumOfDairies", "0") + "<SUBJECT>"; //MITS 34511 - hlv
                this.DiaryWeekView.AppointmentStyle.Font.Bold = true;
                this.DiaryWeekView.AppointmentStyle.Font.Size = FontUnit.Small;
                //this.DiaryWeekView.AppointmentStyle.ForeColor = System.Drawing.Color.b;
                this.DiaryWeekView.AppointmentStyle.Font.Underline = false;
                this.WeekViewWebScheduleInfo.Activities.Clear();
                this.DiaryWeekView.CaptionHeaderStyle.BackColor = System.Drawing.Color.White;
                //this.DiaryWeekView.CaptionHeaderStyle.ForeColor = System.Drawing.Color;
                DateTime dtSelected = DateTime.Now;
                if (!IsPostBack)
                {
                    if (Request.Params["action"] != null)
                    {
                        uiAction = Request.Params["action"].ToString();
                        ViewState["action"] = uiAction;
                    }
                    //Priya
                    if (Request.Params["FromStyle"] != null)
                    {
                        bFromSTyle = Convert.ToBoolean(Request.Params["FromStyle"]);
                        ViewState["FromStyle"] = bFromSTyle;
                    }
                    //Priya

                    if (Request.QueryString["parentpage"] != null && Convert.ToString(Request.QueryString["parentpage"]) == "creatediary")
                    {
                        btnSetDate.Visible = true;
                    }
                    if (Request.Params["dt"] != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(Request.Params["dt"])))
                        {
                            try
                            {
                                dtSelected = DateTime.Parse(Convert.ToString(Request.Params["dt"]), new CultureInfo(WeekViewWebScheduleInfo.CultureInfo.Name, false));
                            }
                            catch (Exception ex)
                            {
                                dtSelected = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(WeekViewWebScheduleInfo.UtcNow.ToString()), timeZoneInfo);
                            }
                        }
                        else
                            dtSelected = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(WeekViewWebScheduleInfo.UtcNow.ToString()), timeZoneInfo);
                    }
                    else
                    {
                        dtSelected = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(WeekViewWebScheduleInfo.UtcNow.ToString()), timeZoneInfo);
                    }


                    objCurrentDates = new CurrentTimeFrame();
                    fromDate = objCurrentDates.GetFromDate(dtSelected);
                    toDate = objCurrentDates.GetToDate(dtSelected);
                    
                    objCurrentControl = new CurrentWebSchedule();
                    objCurrentControl.CurrentControl = "webweekview";
                    objCurrentControl.FromDate = fromDate;
                    objCurrentControl.ToDate = toDate;
                    objCurrentControl.Action = uiAction;
                    objCurrentControl.FromStyle = bFromSTyle;

                    //objDiaryHelper = new DiaryCalendarHelper();
                    objDiaryHelper = new DiaryCalendarHelper(this.PageID); //MITS 34511 - hlv
                    //getting DiaryList through Webservice 
                    if (Request.Params["id"] != null)
                    {
                        objDiaryHelper.s_User = Convert.ToString(Request.Params["id"]);
                        ViewState["id"] = objDiaryHelper.s_User;
                    }
                    if (btnSetDate.Visible)
                        objDiaryHelper.bCreateHyperlink = false;
                    diaryDoc = objDiaryHelper.GetDiaryList(objCurrentControl);  
                    objDiaryHelper.GetDiaryActivities(ref WeekViewWebScheduleInfo, diaryDoc, objCurrentControl.Action);
                    hdnserverdate.Value = dtSelected.ToShortDateString();
                    txtDate.Text = dtSelected.ToShortDateString();
                    WeekViewWebScheduleInfo.ActiveDayUtc = new SmartDate(dtSelected);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objDiaryHelper != null)
                {
                    objDiaryHelper = null;
                }
                if (objCurrentDates != null)
                {
                    objCurrentDates = null;
                }
                if (objCurrentControl != null)
                {
                    objCurrentControl = null;
                }
            }
        }
        //asharma326 JIRA 11081 starts
        /// <summary>
        /// WeekViewWebScheduleInfo_FrameIntervalChanging
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void WeekViewWebScheduleInfo_FrameIntervalChanging(object sender, FrameIntervalChangingEventArgs e)
        {
            //XmlDocument diaryDoc = null;
            //DiaryCalendarHelper objDiaryHelper = null;
            //CurrentTimeFrame objCurrentFrame = null;
            //CurrentWebSchedule objCurrentControl = null;
            //string fromDate = string.Empty;
            //string toDate = string.Empty;
            //string userName = string.Empty;

            //try
            //{
            //    if (!IsPostBack)
            //    {
            //        return;
            //    }

            //    objCurrentFrame = new CurrentTimeFrame();

            //    fromDate = objCurrentFrame.GetFromDate(Convert.ToDateTime(e.FrameInterval.StartDate.AddDays(1).ToString()));
            //    toDate = objCurrentFrame.GetToDate(Convert.ToDateTime(e.FrameInterval.StartDate.AddDays(1).ToString()));

            //    objCurrentControl = new CurrentWebSchedule();
            //    objCurrentControl.CurrentControl = "webweekview";
            //    objCurrentControl.FromDate = fromDate;
            //    objCurrentControl.ToDate = toDate;
            //    objCurrentControl.Action = ViewState["action"].ToString();

            //    //objDiaryHelper = new DiaryCalendarHelper();
            //    objDiaryHelper = new DiaryCalendarHelper(this.PageID); //MITS 34511 - hlv
            //    objDiaryHelper.s_User = Convert.ToString(ViewState["id"]);
            //    diaryDoc = objDiaryHelper.GetDiaryList(objCurrentControl);
            //    WeekViewWebScheduleInfo.Activities.Clear();
            //    objDiaryHelper.GetDiaryActivities(ref WeekViewWebScheduleInfo, diaryDoc, objCurrentControl.Action);
            //}
            //catch (Exception ee)
            //{
            //    ErrorHelper.logErrors(ee);
            //    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            //    err.Add(ee, BusinessAdaptorErrorType.SystemError);
            //    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            //}
            //finally
            //{
            //    if (objDiaryHelper != null)
            //    {
            //        objDiaryHelper = null;
            //    }
            //    if (objCurrentFrame != null)
            //    {
            //        objCurrentFrame = null;
            //    }
            //    if (objCurrentControl != null)
            //    {
            //        objCurrentControl = null;
            //    }
            //}
        }

        private enum CalledFrom
        {
            Prev,
            Next,
            DateControl
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            UpdateAsynUI(CalledFrom.Prev);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            UpdateAsynUI(CalledFrom.Next);
        }
        protected void btnSelectDate_Click(object sender, EventArgs e)
        {
            UpdateAsynUI(CalledFrom.DateControl);
        }
        
        private void UpdateAsynUI(CalledFrom currentOperation)
        {
            XmlDocument diaryDoc = null;
            DiaryCalendarHelper objDiaryHelper = null;
            CurrentTimeFrame objCurrentFrame = null;
            CurrentWebSchedule objCurrentControl = null;
            string fromDate = string.Empty;
            string toDate = string.Empty;
            string userName = string.Empty;

            try
            {
                if (!IsPostBack)
                {
                    return;
                }

                var dateTime = DateTime.Parse(hdnserverdate.Value, new CultureInfo(WeekViewWebScheduleInfo.CultureInfo.Name, false));

                switch (currentOperation)
                {
                    case CalledFrom.Prev:
                        dateTime = dateTime.AddDays(-7);
                        break;
                    case CalledFrom.Next:
                        dateTime = dateTime.AddDays(7);
                        break;
                }

                objCurrentFrame = new CurrentTimeFrame();

                fromDate = objCurrentFrame.GetFromDate(dateTime).ToString();
                toDate = objCurrentFrame.GetToDate(dateTime).ToString();

                objCurrentControl = new CurrentWebSchedule();
                objCurrentControl.CurrentControl = "webweekview";
                objCurrentControl.FromDate = fromDate;
                objCurrentControl.ToDate = toDate;
                objCurrentControl.Action = ViewState["action"].ToString();

                //objDiaryHelper = new DiaryCalendarHelper();
                objDiaryHelper = new DiaryCalendarHelper(this.PageID); //MITS 34511 - hlv
                objDiaryHelper.s_User = Convert.ToString(ViewState["id"]);
                if (btnSetDate.Visible)
                    objDiaryHelper.bCreateHyperlink = false;
                diaryDoc = objDiaryHelper.GetDiaryList(objCurrentControl);
                WeekViewWebScheduleInfo.Activities.Clear();
                objDiaryHelper.GetDiaryActivities(ref WeekViewWebScheduleInfo, diaryDoc, objCurrentControl.Action);
                WeekViewWebScheduleInfo.ActiveDayUtc = new SmartDate(dateTime);
                hdnserverdate.Value = dateTime.ToShortDateString();
                txtDate.Text = dateTime.ToShortDateString();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objDiaryHelper != null)
                {
                    objDiaryHelper = null;
                }
                if (objCurrentFrame != null)
                {
                    objCurrentFrame = null;
                }
                if (objCurrentControl != null)
                {
                    objCurrentControl = null;
                }
            }
        }
        //asharma326 JIRA 11081 Ends

    }

}