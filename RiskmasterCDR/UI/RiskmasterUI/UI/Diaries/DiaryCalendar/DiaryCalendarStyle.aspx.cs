﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.Diaries.DiaryCalendar
{
    public partial class DiaryCalendarStyle : System.Web.UI.Page
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            DiaryCalendarHelper objDiaryHelper = null;
            XmlDocument diaryDoc = null;
            XmlElement temp = null;
            string code = string.Empty;
            try
            {
                objDiaryHelper = new DiaryCalendarHelper();
                
                diaryDoc = objDiaryHelper.GetCalendarView();
                temp = (XmlElement)diaryDoc.SelectSingleNode("//CalendarView");
                if (temp != null)
                {
                    code = temp.InnerText;
                    if (code == "0")
                    {
                        rdMonthly.Checked = true;
                        hdnSelectedValue.Value = "MonthView.aspx";
                    }
                    else if (code == "1")
                    {
                        rdWeekly.Checked = true;
                        hdnSelectedValue.Value = "WeekView.aspx";
                    }
                    else if (code == "2")
                    {
                        rdDaily.Checked = true;
                        hdnSelectedValue.Value = "DayView.aspx";
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objDiaryHelper != null)
                {
                    objDiaryHelper = null;
                }
                if (diaryDoc != null)
                {
                    diaryDoc = null;
                }
                if (temp != null)
                {
                    temp = null;
                }
            }
        }
    }
}