﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiaryCalendar.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryCalendar.DiaryCalendar" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Diary Calendar</title>
    <script language="javascript"  type="text/javascript">

        function OnPeekBtnClick() 
		{
            var dariyFrame;
            var peekBtn;
            var hdnDiaryAction;
            var hdnSelectedControl;
            var stringMessage;
            peekBtn = document.getElementById('btnPeek');
            hdnDiaryAction = document.getElementById('hdnDiaryAction');
			
			if (peekBtn.value == DiaryCalendarValidations.btnHome) 
			{
               hdnDiaryAction.value = 'default';
                peekBtn.value = DiaryCalendarValidations.btnpeek;
                stringMessage = "";
                if (document.getElementById("hdnIsBESEnabled").value == "true") 
				{
                    document.getElementById('btnPeekProcOffice').disabled = false;
                }
            }
            else 
			{
                hdnDiaryAction.value = 'peek';
                peekBtn.value = DiaryCalendarValidations.btnHome;

                stringMessage = DiaryCalValidations.lblPeekDiary;
                document.getElementById('btnPeekProcOffice').disabled = true;
            }
            var peekProcessingOfficeButton = document.getElementById('btnPeekProcOffice');
            if (peekProcessingOfficeButton.value == DiaryCalendarValidations.btnHome) 
			{
                peekProcessingOfficeButton.value = DiaryCalendarValidations.btnpeekusergroup;
            }
            hdnSelectedControl = document.getElementById('hdnSelectedControl');
            dairyFrame = document.getElementById('DataFrame');
            dairyFrame.src = hdnSelectedControl.value + "?action=" + hdnDiaryAction.value;
            document.getElementById('lblMessage').innerText = stringMessage;
        }


        function OnPeekProcessingBtnClick() 
		{
            var dariyFrame;
            var peekprocessingBtn;
            var hdnDiaryAction;
            var hdnSelectedControl;
            var stringMessage;
            var peekBtn = document.getElementById('btnPeek');

            peekBtn = document.getElementById('btnPeekProcOffice');
            hdnDiaryAction = document.getElementById('hdnDiaryAction');
            if (peekBtn.value == DiaryCalendarValidations.btnHome) 
			{
                hdnDiaryAction.value = 'default';
                peekBtn.value = DiaryCalendarValidations.btnpeekusergroup;
                stringMessage = "";

                document.getElementById('btnPeek').disabled = false;
            }
            else 
			{
                hdnDiaryAction.value = 'processingoffice';
                peekBtn.value = DiaryCalendarValidations.btnHome;
                stringMessage = DiaryCalValidations.lblViewDiaries;

                document.getElementById('btnPeek').disabled = true;
            }
            var peekBtn = document.getElementById('btnPeek');
            if (peekBtn.value == DiaryCalendarValidations.btnHome) 
			{
                peekBtn.value = DiaryCalendarValidations.btnpeek;
            }
            hdnSelectedControl = document.getElementById('hdnSelectedControl');
            dairyFrame = document.getElementById('DataFrame');
            dairyFrame.src = hdnSelectedControl.value + "?action=" + hdnDiaryAction.value;
            document.getElementById('lblMessage').innerText = stringMessage;
        }

        function CalenderStyle() 
		{
            var dialogProps = "dialogWidth:350px;dialogHeight:200px;dialogLeft=500;dialogTop=310;help=no;scroll=no;status=no;center=no;";
            selectedControl = window.open('DiaryCalendarStyle.aspx', 'wndDiaryCalendar',
		        'width=250,height=250' + ',top=' + (screen.availHeight - 250) / 2 + ',left=' + (screen.availWidth - 250) / 2 + ',resizable=yes,scrollbars=no,resizable=no');

            var hdnselectedControl = document.getElementById('hdnSelectedControl');
            hdnselectedControl.value = selectedControl;
            var selectedAction = document.getElementById('hdnDiaryAction');
        }


        function checkReadyState() 
		{
            for (var f = 0; f < document.frames.length; f++) 
			{
                frame = document.frames(f);
                while (frame.document.readyState != "complete")
				 {
                    status += "."; // just wait...

                }
                //document.getElementById('IframerLoader').style.display = 'none';
            }

            document.body.scroll = "yes";
        }

        function NavigateToDiaryPage(diaryPage) 
		{
            var isPeek;
            var isProcessingOffice;
            var peekBtn = document.getElementById('btnPeek');

            if (peekBtn.value == DiaryCalendarValidations.btnHome) 
			{
                isPeek = true;
            }
            else if (peekBtn.value == DiaryCalendarValidations.btnpeek) 
			{
                isPeek = false;
            }

            var peekProcessingOfficeButton = document.getElementById('btnPeekProcOffice');

            if (peekProcessingOfficeButton.value == DiaryCalendarValidations.btnHome) 
			{
                isProcessingOffice = true;
            }
            else 
			{
                isProcessingOffice = false;
            }

            window.location = "../" + diaryPage + "?assigneduser=" + document.getElementById("hdnUserName").value + "&isdiarycalendar=true&isdiarycalendarpeek=" + isPeek
                    + "&isdiarycalendarprocessingoffice=" + isProcessingOffice;
        }

        function OpenPrintDiary() 
		{
            NavigateToDiaryPage('DiaryListCalendar.aspx');
        }
        function CheckMDIScreenLoaded() {
            try {
                parent.MDIScreenLoaded();
            }
            catch (e) { }
        }
        window.onload = function () {
            if(window.frameElement)
                window.frameElement.scrolling = "no";
        }
            
    </script>
    
    </head>
<body onload="CheckMDIScreenLoaded();">
    <form id="form1" runat="server">
     <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
     <div style="height:88vh">  <!-- igupta3 Mits: 33301 -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <input type="hidden" id="hdnCurrentDate" value="default" class="button" runat="server"/>            
            <input type="hidden" id="hdnUserName" value="default" class="button" runat="server"/>      
         <tr>
            <td >
              <table>
                <tr>
                    <td>
                        <input type="button" id="Button1" onclick="CalenderStyle()" runat="server" value="<%$ Resources:btncalendarstyle %>" class="button"/> <%-- csingh7 for MITS 14633
                    </td>
                    <td>
                            <%--<input type="button" id="btnCreateDairy"  onclick="OpenCreatDairy()" value="Create Diary" class="button" />--%>
                            <asp:Button ID="btnCreateDairy" Text="<%$ Resources:btncreatediary %>"  CssClass="button" runat="server" onclick="btnCreateDairy_Click" />
                    </td>
                    <td>
                        <%--<input type="button" id="btnPrint" onclick="OpenPrintDiary()" value="Print" class="button"/>                       --%>
                        <asp:Button ID="btnPrint" Text="<%$ Resources:btnprint %>"  CssClass="button" runat="server"  style="width:157px"
                            onclick="btnPrint_Click" />
                    </td>
                    <td>
                        <input type="button" id="btnPeek"  onclick="OnPeekBtnClick()" value="<%$ Resources:btnpeek %>"  class="button" style="width:157px" runat="server"/>
                        <input type="hidden" runat="server" id="hdnIsBESEnabled"/>
                    </td>
                    <td align="center">
                        <input type="button" id="btnPeekProcOffice"  onclick="OnPeekProcessingBtnClick()" runat="server" value="<%$ Resources:btnpeekusergroup %>"  class="button" />
                        <input type="hidden" id="hdnDiaryAction" value="default" class="button" runat="server" enableviewstate="true"/>
                        <input type="hidden" id="hdnSelectedControl" value="MonthView.aspx" runat="server" class="button" enableviewstate="true"/>
                    </td>
                </tr> 
              </table>
             </td>
         </tr>  
         <tr>            
            <td colspan="2" class="msgheader">
            <asp:Label ID="diarycalendar" runat="server" Text="<%$ Resources:lbldiarycalendar %>" />
            </td>
         </tr></table>
         <asp:Label ID="lblMessage" runat="server"></asp:Label>
       <%-- <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" style="width:100%;height:100%"><!--1091-->
                 <tr>
                    <td>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                 </tr>
                 <tr>
                    <td>--%>
                        <iframe id="DataFrame" runat="server" style="width:99.5%;height:100%;overflow:hidden;"  frameborder="0" src="MonthView.aspx?action=default">
                        </iframe>
                    <%-- </td>
                     
                 </tr>
                </table>
            </td>
         </tr>
         </table> --%>
        </div>    
   </form>
</body>
</html>

