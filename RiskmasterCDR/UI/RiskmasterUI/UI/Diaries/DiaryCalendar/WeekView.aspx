﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WeekView.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryCalendar.WeekView" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Diary Calendar WeekView</title>
    <%--<link href="../../../Content/rmnet.css" rel="stylesheet" type="text/css"></link>--%>
    <script src="../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript" >        { var i; }  </script>
    <script src="../../../Scripts/DairyCalender.js" type="text/javascript"></script>   

    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <script language="javascript" >
        function WebScheduleWeekView_NavigatePrev(oScheduleInfo, oEvent, oDialog, oActivity)
        {
            document.getElementById('divDiaryDisplay').style.display = 'none';
            $('#btnPrev').click();
        }
        function WebScheduleWeekView_NavigateNext(oScheduleInfo, oEvent, oDialog, oActivity)
        {
            document.getElementById('divDiaryDisplay').style.display = 'none';
            $('#btnNext').click();
        }
        function CloseWindow() {
            if ($('#txtDate').val() != "")
                window.parent.opener.document.getElementById('CompleteDate_date').value = $('#txtDate').val();
            parent.close();
            return false;
        }
        function DateSelected() {
            $('#hdnserverdate').val($('#txtDate').val());
            $('#btnSelectDate').click();
        }
    </script>
</head>

<body>
    <form id="form1" runat="server" style="overflow: hidden">
    <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
    <div id="progressIndicatorTemplate"></div>
        <table width="100%" align="left" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <table>
                        <tr style="vertical-align: middle;">
                            <td align="left">
                                <asp:Label ID="lblSelectDate" runat="server" Text="<%$ Resources:lblSelectDate %>"></asp:Label>
                                <asp:Button ID="btnSelectDate" runat="server" OnClick="btnSelectDate_Click" Style="display: none;" />
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtDate" Style="display: none;" onChange="DateSelected()" />
                                <script type="text/javascript">
                                    $(function () {
                                        $("#txtDate").datepicker({
                                            showOn: "button",
                                            buttonImage: "../../../Images/calendar.gif",
                                            buttonImageOnly: true,
                                            showOtherMonths: true,
                                            selectOtherMonths: true,
                                            changeYear: true
                                        });
                                    });
                                </script>
                            </td>
                            <td>
                                <input id="btnSetDate" type="button" value="Set Due Date" onclick="CloseWindow();" class="button" runat="server" visible="false" />

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" height="100%">
                    <table id="WebMonth" width="100%" height="500px">
                        <tr>
                            <td width="100%" height="90%" valign="top">
                                <igsch:WebWeekView ID="DiaryWeekView" runat="server" EnableViewState="true" EnableAutoActivityDialog="false"
                                    WebScheduleInfoID="WeekViewWebScheduleInfo"
                                    Style="top: 0px; left: 0px" Height="100%" Width="90%"
                                    CaptionFormatString="MMMM dd yyyy"
                                    StyleSheetFileName="../../../Content/DiaryCalendar/Aero/ig_webweekview.css">
                                    <ClientEvents Click="WebSchedule_Click" />
                                    <ClientEvents NavigateNext="WebScheduleWeekView_NavigateNext" />
                                    <ClientEvents NavigatePrevious="WebScheduleWeekView_NavigatePrev" />
                                    <ClientEvents Initialize="setupSchedPI" />
                                    <CaptionHeaderStyle BackColor="#333333"
                                        BorderWidth="2px">
                                        <BorderDetails ColorTop="SlateGray" />
                                    </CaptionHeaderStyle>
                                </igsch:WebWeekView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divDiaryDisplay" style="display: none; width: 97%; background-color: #FFFFFF;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <igsch:WebScheduleInfo ID="WeekViewWebScheduleInfo"
                        EnableProgressIndicator="true" EnableSmartCallbacks="true" runat="server"
                        OnFrameIntervalChanging="WeekViewWebScheduleInfo_FrameIntervalChanging">
                    </igsch:WebScheduleInfo>
                </td>
            </tr>
        </table>
        <asp:hiddenfield ID="hdnserverdate" runat="server"></asp:hiddenfield>
        <asp:Button ID="btnPrev"  runat="server" OnClick="btnPrev_Click" style="display: none;"/>
        <asp:Button ID="btnNext" runat="server" OnClick="btnNext_Click" style="display: none;"/>
       
    </form>
</body>
</html>


