﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DayView.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryCalendar.DayView" EnableViewStateMac="false" EnableEventValidation="false" ValidateRequest="false"%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Diary Calendar DayView</title>

    <%--<link href="../../../Content/rmnet.css" rel="stylesheet" type="text/css"></link>--%>
    <script src="../../../Scripts/DairyCalender.js" type="text/javascript"></script>   
    <script src="../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript" >        { var i; }  </script>



    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $('#DiaryDayView_divScroll').css('overflow', 'hidden');
            $('#DiaryDayView_divScroll').hide();
        });
        function WebScheduleDayView_NavigatePrev(oScheduleInfo, oEvent, oDialog, oActivity) {
            document.getElementById('divDiaryDisplay').style.display = 'none';
            $('#divDiaryDisplayPageLoad').hide();
            $('#DiaryDayView_divScroll').css('overflow', 'hidden');
            $('#DiaryDayView_divScroll').hide();
            $('#btnPrev').click();
            return false;
        }
        function WebScheduleDayView_NavigateNext(oScheduleInfo, oEvent, oDialog, oActivity) {
            document.getElementById('DiaryDayView_divScroll').style.display = 'none';
            $('#DiaryDayView_divScroll').css('overflow', 'hidden');
            $('#divDiaryDisplayPageLoad').hide();
            $('#DiaryDayView_divScroll').hide();
            $('#btnNext').click();
            return false;
        }
        function DateSelected() {
            $('#hdnserverdate').val($('#txtDate').val());
            $('#btnSelectDate').click();
        }
        function CheckMDIScreenLoaded() {
            try {
                parent.MDIScreenLoaded();
            }
            catch (e) { }
        }
        function CloseWindow() {
            if ($('#txtDate').val() != "")
                window.parent.opener.document.getElementById('CompleteDate_date').value = $('#txtDate').val();
            parent.close();
            return false;
        }
    </script>
    <style>
        .igdv_NonWorkingTimeSlot
{
	background-color:white!important;
	border-bottom:0px solid white!important;
}
        .igdv_WorkingTimeSlot
{
	background-color:white!important;
	border-bottom:0px solid white!important;
}


    </style>
</head>
<body onload="CheckMDIScreenLoaded()">
    <form id="form1" runat="server">
    <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
    <div id="progressIndicatorTemplate"></div>
        <table>
            <tr style="vertical-align:middle;">
                <td>
                        <asp:Label ID="lblSelectDate" runat="server" Text="<%$ Resources:lblSelectDate %>" Style="font-size: 12px"></asp:Label>
                        <asp:Button ID="btnSelectDate" runat="server" OnClick="btnSelectDate_Click" Style="display: none;" />
                    </td>
                <td>
                        <asp:TextBox runat="server" ID="txtDate" Style="display: none;" onChange="DateSelected()" />
                        <script type="text/javascript">
                            $(function () {
                                $("#txtDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../../Images/calendar.gif",
                                    buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                });
                            });
                        </script>
                </td>
                <td>
                        <input runat="server" id="btnSetDate" type="button" value="Set Due Date" onclick="CloseWindow();" class="button" visible="false" />

                </td>
            </tr>
        </table>
         

                  <igsch:WebDayView ID="DiaryDayView" runat="server" height="75%" Width="98%" EnableAutoActivityDialog=false 
                        WebScheduleInfoID="DayViewWebScheduleInfo" 
                        TimeSlotLabelAreaVisible="False" ScrollPosition="0" 
                        StyleSheetFileName="../../../Content/DiaryCalendar/Aero/ig_webdayview.css">
                        <ClientEvents  Click ="WebSchedule_Click" />
                        <ClientEvents NavigateNext ="WebScheduleDayView_NavigateNext" /> 
                        <ClientEvents NavigatePrevious ="WebScheduleDayView_NavigatePrev" />    
                        <ClientEvents  Initialize="setupSchedPI" />
                   </igsch:WebDayView>                    
              
         <igsch:WebScheduleInfo ID="DayViewWebScheduleInfo"
                         EnableProgressIndicator="true" EnableSmartCallbacks="true" runat="server"  EnableRecurringActivities="false">
                     </igsch:WebScheduleInfo>
                
       
        <div id="divDiaryDisplay" style="display: none;background-color: #FFFFFF; width: 500px;">
        </div>
        <asp:TextBox id="txtCurrentDate" runat="server" style="display: none;"></asp:TextBox>
        <asp:hiddenfield ID="hdnserverdate" runat="server"></asp:hiddenfield>
        <asp:Button ID="btnPrev"  runat="server" OnClick="btnPrev_Click" style="display: none;"/>
        <asp:Button ID="btnNext" runat="server" OnClick="btnNext_Click" style="display: none;"/>
        <div id="divDiaryDisplayPageLoad" style="display: block;background-color: #FFFFFF; width: 98%;">
         <div  class="msgheader" ><asp:label runat="server" id="lblCount"></asp:label></div>
            <hr />

            <%=this.sDivElements %></div>
     </form>
</body>
</html>
