﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Infragistics.WebUI.WebSchedule;
using Infragistics.WebUI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using System.Globalization; //MITS 34492 - hlv


namespace Riskmaster.UI.Diaries.DiaryCalendar
{
    public partial class MonthView : System.Web.UI.Page
    {
        private string PageID = RMXResourceProvider.PageId("MonthView.aspx"); //MITS 34492 - hlv
        /// <summary>
        /// Page load for Month View
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            TimeZone timeZone = null;
            TimeZoneInfo timeZoneInfo = null;
            XmlDocument diaryDoc = null;
            CurrentWebSchedule objCurrentControl = null;
            DiaryCalendarHelper objDiaryHelper = null;
            string navigationMonth = string.Empty;
            string navigationYear = string.Empty;
            string uiAction = string.Empty;
            bool bFromSTyle = false;
            //string userName = string.Empty;
            //DateTime webScheduleCurrentDateForHdnField;

            //MITS 34492 - hlv begin
            //WebCalendarView1.CultureInfo = new System.Globalization.CultureInfo(AppHelper.GetCulture());
            //WebCalendarView1.FooterFormat = AppHelper.GetResourceValue(this.PageID, "lblToday", "0") + " {0:" + AppHelper.GetDateFormat() + "}";
            MonthViewWebScheduleInfo.CultureInfo = new System.Globalization.CultureInfo(AppHelper.GetCulture());
            DiaryMonthView.CultureInfo = new System.Globalization.CultureInfo(AppHelper.GetCulture());
            //MITS 34492 - hlv end

            try
            {
                timeZone = TimeZone.CurrentTimeZone;
                timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone.StandardName);
                this.DiaryMonthView.NavigationAnimation = Infragistics.WebUI.WebSchedule.AnimationRate.Linear;
                this.DiaryMonthView.AppointmentFormatString = "<SUBJECT>";
                //this.DiaryMonthView.AppointmentTooltipFormatString = "Number of Dairies due:<SUBJECT>";
                this.DiaryMonthView.AppointmentTooltipFormatString = AppHelper.GetResourceValue(this.PageID, "lblNumOfDairies", "0") + "<SUBJECT>"; //MITS 34492 - hlv
                this.DiaryMonthView.AppointmentStyle.Font.Bold = true;
                this.DiaryMonthView.AppointmentStyle.Font.Size = FontUnit.XSmall;
                //this.DiaryMonthView.AppointmentStyle.ForeColor = System.Drawing.Color.Violet;
                this.DiaryMonthView.AppointmentStyle.Font.Underline = false;
                this.MonthViewWebScheduleInfo.Activities.Clear();
                DateTime dtSelected = DateTime.Now;
                if (!IsPostBack)
                {
                    if (Request.Params["action"] != null)
                    {
                        uiAction = Request.Params["action"].ToString();
                        ViewState["action"] = uiAction;
                    }
                    
                    if (Request.Params["FromStyle"] != null)
                    {
                        bFromSTyle = Convert.ToBoolean(Request.Params["FromStyle"]);
                        ViewState["FromStyle"] = bFromSTyle.ToString();
                    }
                    if (Request.QueryString["parentpage"] != null && Convert.ToString(Request.QueryString["parentpage"]) == "creatediary")
                    {
                        btnSetDate.Visible = true;
                    }

                    //objDiaryHelper = new DiaryCalendarHelper();
                    objDiaryHelper = new DiaryCalendarHelper(this.PageID); //MITS 34492 hlv
                    if (Request.Params["id"] != null)
                    {
                        objDiaryHelper.s_User = Convert.ToString(Request.Params["id"]);
                        ViewState["id"] = objDiaryHelper.s_User;
                    }
                    if (btnSetDate.Visible)
                        objDiaryHelper.bCreateHyperlink = false;
                    if (Request.Params["dt"] != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(Request.Params["dt"])))
                        {
                            try
                            {
                                dtSelected = DateTime.Parse(Convert.ToString(Request.Params["dt"]), new CultureInfo(MonthViewWebScheduleInfo.CultureInfo.Name, false));
                            }
                            catch (Exception ex)
                            {
                                dtSelected = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(MonthViewWebScheduleInfo.UtcNow.ToString()), timeZoneInfo);
                            }
                        }
                        else
                            dtSelected = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(MonthViewWebScheduleInfo.UtcNow.ToString()), timeZoneInfo);

                    }
                    else
                        dtSelected = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(MonthViewWebScheduleInfo.UtcNow.ToString()), timeZoneInfo);
                    //making current month in desired format to fetch diaries from DataBase
                    if (dtSelected.Month < 10)
                    {
                        navigationMonth = dtSelected.Month.ToString();
                        navigationMonth = "0" + navigationMonth;
                        //webScheduleCurrentDateForHdnField = dtSelected.AddMonths(-1);
        
                    }
                    else
                    {
                        navigationMonth = dtSelected.Month.ToString();
                        //webScheduleCurrentDateForHdnField = dtSelected.AddMonths(-1);
                    }

                    navigationYear = dtSelected.Year.ToString();

                    //populating hdnfield in order to maintain current date information
                    //hdnCurrentMonth.Value = webScheduleCurrentDateForHdnField.Day.ToString() + " " + webScheduleCurrentDateForHdnField.Month.ToString() + " " + webScheduleCurrentDateForHdnField.Year.ToString(); 

                    objCurrentControl = new CurrentWebSchedule();

                    objCurrentControl.CurrentControl = "webmonthview";
                    objCurrentControl.NavigationMonth = navigationMonth;
                    objCurrentControl.NavigationYear = navigationYear;
                    objCurrentControl.Action = uiAction;
                    objCurrentControl.FromStyle = bFromSTyle;
                    hdnserverdate.Value = dtSelected.ToShortDateString();
                    txtDate.Text = dtSelected.ToShortDateString();
                    //getting DiaryList through Webservice 
                    diaryDoc = objDiaryHelper.GetDiaryList(objCurrentControl);
                    
                    //populating the current webscheduleinfo control with diarylsit obtained from Database
                    objDiaryHelper.GetDiaryActivities(ref MonthViewWebScheduleInfo, diaryDoc, objCurrentControl.Action);
                    MonthViewWebScheduleInfo.ActiveDayUtc = new SmartDate(dtSelected);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objDiaryHelper != null)
                {
                    objDiaryHelper = null;
                }
                if (diaryDoc != null)
                {
                    diaryDoc = null;
                }
                if (objCurrentControl != null)
                {
                    objCurrentControl = null;
                }
            }
        }

        //asharma326 JIRA 11081 starts
        protected void MonthViewWebScheduleInfo_ActiveDayChanging(object sender, ActiveDayChangingEventArgs e)
        {
            //string currentMonth = string.Empty;
            //string currentYear = string.Empty;
            //string currentAction = string.Empty;
            //CurrentTimeFrame objCurrentTimeFrame = null;
            //string sessionId = string.Empty;
            //string uiAction = string.Empty;
            //bool uiFormStyle ;
            
            //try
            //{
            //    if (!IsPostBack)
            //    {
            //        return;
            //    }

            //    uiFormStyle = Convert.ToBoolean(ViewState["FromStyle"]);
            //    uiAction = ViewState["action"].ToString();
                     
            //    if (hdnIsWebCalendarNavigated.Value == "true")
            //    {
            //        currentMonth = e.NewDate.Month.ToString();
            //        currentYear = e.NewDate.Year.ToString();
                    
            //        if (currentMonth.Length < 2)
            //        {
            //            currentMonth = "0" + currentMonth;
            //        }
            //        GetDiariesForSupplyMonth(currentMonth,currentYear,uiAction,uiFormStyle);
            //    }
            //    else
            //    {
            //        if (hdnCurrentMonth.Value != "")
            //        {
            //            objCurrentTimeFrame = new CurrentTimeFrame();

            //            objCurrentTimeFrame.CalculateCurrentMonthandYearForWebMonthView(hdnMonthViewNavigationDirection.Value,hdnCurrentMonth.Value,ref currentMonth,ref currentYear);

            //            if (currentMonth.Length < 2)
            //            {
            //                currentMonth = "0" + currentMonth;
            //            }
            //            GetDiariesForSupplyMonth(currentMonth, currentYear, uiAction, uiFormStyle);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }

        protected void btnSelectDate_Click(object sender, EventArgs e)
        {
            UpdateAsynUI(CalledFrom.DateControl);
        }
        
        protected void btnPrev_Click(object sender, EventArgs e)
        {
            UpdateAsynUI(CalledFrom.Prev);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            UpdateAsynUI(CalledFrom.Next);
        }

        private enum CalledFrom
        {
            Prev,
            Next,
            DateControl
        }
        private void UpdateAsynUI(CalledFrom currentOperation)
        {
            XmlDocument diaryDoc = null;
            DiaryCalendarHelper objDiaryHelper = null;
            CurrentWebSchedule objCurrentControl = null;
            string currentMonth = string.Empty;
            try
            {
                if (!IsPostBack)
                {
                    return;
                }

                var dateTime = DateTime.Parse(hdnserverdate.Value, new CultureInfo(MonthViewWebScheduleInfo.CultureInfo.Name, false));

                switch (currentOperation)
                {
                    case CalledFrom.Prev:
                        dateTime = dateTime.AddMonths(-1);
                        break;
                    case CalledFrom.Next:
                        dateTime = dateTime.AddMonths(1);
                        break;
                }
                currentMonth = dateTime.Month.ToString();

                if (dateTime.Month.ToString().Length < 2)
                {
                    currentMonth = "0" + currentMonth;
                }
                objCurrentControl = new CurrentWebSchedule();
                objCurrentControl.CurrentControl = "webmonthview";
                objCurrentControl.NavigationMonth = currentMonth;
                objCurrentControl.NavigationYear = dateTime.Year.ToString();
                objCurrentControl.Action = Convert.ToString(ViewState["action"].ToString());
                objCurrentControl.FromStyle = Convert.ToBoolean(ViewState["FromStyle"]); 
                

                //objDiaryHelper = new DiaryCalendarHelper();
                objDiaryHelper = new DiaryCalendarHelper(this.PageID); //MITS 34492 hlv
                objDiaryHelper.s_User = Convert.ToString(ViewState["id"]);
                //getting DiaryList through Webservice 
                diaryDoc = objDiaryHelper.GetDiaryList(objCurrentControl);
                if (btnSetDate.Visible)
                    objDiaryHelper.bCreateHyperlink = false;
                MonthViewWebScheduleInfo.Activities.Clear();
                //populating the current webscheduleinfo control with diarylsit obtained from Database
                objDiaryHelper.GetDiaryActivities(ref MonthViewWebScheduleInfo, diaryDoc, objCurrentControl.Action);
                MonthViewWebScheduleInfo.ActiveDayUtc = new SmartDate(dateTime);
                hdnserverdate.Value = dateTime.ToShortDateString();
                txtDate.Text = dateTime.ToShortDateString();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objDiaryHelper != null)
                {
                    objDiaryHelper = null;
                }
                if (diaryDoc != null)
                {
                    diaryDoc = null;
                }
                if (objCurrentControl != null)
                {
                    objCurrentControl = null;
                }
            }
        }
        //asharma326 JIRA 11081 ends
    }
}