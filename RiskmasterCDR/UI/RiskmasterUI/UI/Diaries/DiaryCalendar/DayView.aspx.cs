﻿using System;
using System.Web.UI.WebControls;
using System.Xml;
using Infragistics.WebUI.WebSchedule;
using Infragistics.WebUI.Shared;
using System.Web.UI.HtmlControls;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using System.Web.UI;
using System.Globalization; //MITS 34450 - hlv

namespace Riskmaster.UI.Diaries.DiaryCalendar
{
    public partial class DayView : System.Web.UI.Page
    {
        public string sDivElements { get; set; }
        private string PageID = RMXResourceProvider.PageId("DayView.aspx"); //MITS 34450 - hlv
        /// <summary>
        /// Page load for Day View
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            TimeZone timeZone = null;
            TimeZoneInfo timeZoneInfo = null;

            DiaryCalendarHelper objDiaryHelper = null;
            XmlDocument diaryDoc = null;
            CurrentWebSchedule objCurrentControl = null;
            string uiAction = string.Empty;
            bool bFromSTyle = false;
            string userName = string.Empty;

            //MITS 34450 - hlv begin
            //WebCalendarView1.CultureInfo = new System.Globalization.CultureInfo(AppHelper.GetCulture());
            //WebCalendarView1.FooterFormat = AppHelper.GetResourceValue(this.PageID, "lblToday", "0") + " {0:" + AppHelper.GetDateFormat() + "}";
            DiaryDayView.CultureInfo = new System.Globalization.CultureInfo(AppHelper.GetCulture());
            DayViewWebScheduleInfo.CultureInfo = new System.Globalization.CultureInfo(AppHelper.GetCulture());
            //MITS 34450 - hlv end
            DateTime dtSelected = DateTime.Now;
            var isTimeZoneDifferent = false;
            try
            {
                timeZone = TimeZone.CurrentTimeZone;
                timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone.StandardName);
                this.DiaryDayView.NavigationAnimation = Infragistics.WebUI.WebSchedule.AnimationRate.Linear;
                this.DiaryDayView.AppointmentFormatString = "<SUBJECT>";
                //this.DiaryDayView.AppointmentTooltipFormatString = "Number of Dairies due:<SUBJECT>";
                this.DiaryDayView.AppointmentTooltipFormatString = AppHelper.GetResourceValue(this.PageID, "lblNumOfDairies", "0") + "<SUBJECT>"; //MITS 34450 - hlv
                this.DiaryDayView.AppointmentStyle.Font.Bold = true;
                this.DiaryDayView.AppointmentStyle.Font.Size = FontUnit.Larger;
                this.DiaryDayView.AppointmentStyle.ForeColor = System.Drawing.Color.RoyalBlue;
                this.DiaryDayView.AppointmentStyle.Font.Underline = true;
                this.DayViewWebScheduleInfo.Activities.Clear();
                
                if (!IsPostBack)
                {
                    if (Request.Params["action"] != null)
                    {
                        uiAction = Request.Params["action"].ToString();
                        ViewState["action"] = uiAction;
                    }
                    if (Request.Params["FromStyle"] != null)
                    {
                        bFromSTyle = Convert.ToBoolean(Request.Params["FromStyle"]);
                        ViewState["FromStyle"] = bFromSTyle;
                    }
                    if (Request.QueryString["parentpage"] != null && Convert.ToString(Request.QueryString["parentpage"]) == "creatediary")
                    {
                        btnSetDate.Visible = true;
                    }
                    objCurrentControl = new CurrentWebSchedule();
                    objCurrentControl.CurrentControl = "webdayview";
                    if (Request.Params["dt"] != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(Request.Params["dt"])))
                        {
                            try
                            {
                                dtSelected = DateTime.Parse(Convert.ToString(Request.Params["dt"]), new CultureInfo(DayViewWebScheduleInfo.CultureInfo.Name, false));
                            }
                            catch (Exception ex)
                            {
                                dtSelected = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(DayViewWebScheduleInfo.UtcNow.ToString()), timeZoneInfo);
                            }
                        }
                        else
                            dtSelected = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(DayViewWebScheduleInfo.UtcNow.ToString()), timeZoneInfo);

                    }
                    else
                    {
                        dtSelected = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(DayViewWebScheduleInfo.UtcNow.ToString()), timeZoneInfo);
                    }
                    if (TimeSpan.Compare(timeZoneInfo.BaseUtcOffset, new TimeSpan()) > 0)
                    {
                        isTimeZoneDifferent = true;
                    }
                    objCurrentControl.CurrentActiveDate = dtSelected.ToLongDateString();
                    objCurrentControl.Action = uiAction;
                    objCurrentControl.FromStyle = bFromSTyle;

                    this.DiaryDayView.CaptionHeaderText = dtSelected.Year.ToString();

                    //objDiaryHelper = new DiaryCalendarHelper();
                    objDiaryHelper = new DiaryCalendarHelper(this.PageID); //MITS 34492 hlv
                    if (Request.Params["id"] != null)
                    {
                        objDiaryHelper.s_User = Convert.ToString(Request.Params["id"]);
                        ViewState["id"] = objDiaryHelper.s_User;
                    }
                    if (btnSetDate.Visible)
                        objDiaryHelper.bCreateHyperlink = false;
                    objDiaryHelper.bShowCloseImg = false;
                    //getting DiaryList through Webservice 
                    diaryDoc = objDiaryHelper.GetDiaryList(objCurrentControl);
                    objDiaryHelper.GetDiaryActivities(ref DayViewWebScheduleInfo, diaryDoc, objCurrentControl.Action);
                    sDivElements="";
                    foreach (Appointment item in DayViewWebScheduleInfo.Activities)
                    {
                        sDivElements = sDivElements + item.Description;
                    }
                    //DayViewWebScheduleInfo.ActiveDayUtc = new SmartDate(dtSelected);
                    if (!isTimeZoneDifferent)
                        ((Infragistics.WebUI.WebSchedule.WebScheduleViewBase)(this.DiaryDayView)).WebScheduleInfo.ActiveDayUtc = new SmartDate(dtSelected.AddDays(1));
                    else
                        ((Infragistics.WebUI.WebSchedule.WebScheduleViewBase)(this.DiaryDayView)).WebScheduleInfo.ActiveDayUtc = new SmartDate(dtSelected);

                    lblCount.Text = "Count : " + Convert.ToString(diaryDoc.SelectSingleNode("//diaries").Attributes["maxrecord"].Value);
                    hdnserverdate.Value = dtSelected.ToString();
                    txtDate.Text = dtSelected.ToShortDateString();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objDiaryHelper != null)
                {
                    objDiaryHelper = null;
                }
                if (diaryDoc != null)
                {
                    diaryDoc = null;
                }
                if (objCurrentControl != null)
                {
                    objCurrentControl = null;
                }
            }
        }

        /// <summary>
        /// DayViewWebScheduleInfo_FrameIntervalChanging
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void DayViewWebScheduleInfo_FrameIntervalChanging(object sender, FrameIntervalChangingEventArgs e)
        {
            //XmlDocument diaryDoc = null;
            //DiaryCalendarHelper objDiaryHelper = null;
            //CurrentWebSchedule objCurrentControl = null;
            //string userName = string.Empty;
            //TimeZone timeZone = null;
            //TimeZoneInfo timeZoneInfo = null;
            //try
            //{

            //    if (!IsPostBack)
            //    {
            //        return;
            //    }
            //    timeZone = TimeZone.CurrentTimeZone;
            //    timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone.StandardName);

            //    objCurrentControl = new CurrentWebSchedule();
            //    objCurrentControl.CurrentControl = "webdayview";
            //    objCurrentControl.Action = ViewState["action"].ToString();

            //    if (TimeSpan.Compare(timeZoneInfo.BaseUtcOffset, new TimeSpan()) > 0)
            //    {
            //        objCurrentControl.CurrentActiveDate = e.FrameInterval.StartDate.AddDays(1).ToString();
            //        this.DiaryDayView.CaptionHeaderText = e.FrameInterval.StartDate.AddDays(1).Year.ToString();
            //    }
            //    if (TimeSpan.Compare(timeZoneInfo.BaseUtcOffset, new TimeSpan()) <= 0)
            //    {
            //        objCurrentControl.CurrentActiveDate = e.FrameInterval.StartDate.ToString();
            //        this.DiaryDayView.CaptionHeaderText = e.FrameInterval.StartDate.Year.ToString();
            //    }
            //    ViewState["CurrentActiveDate"] = objCurrentControl.CurrentActiveDate;
            //    ViewState["CaptionHeaderText"] = e.FrameInterval.StartDate.Year.ToString();
            //    //objDiaryHelper = new DiaryCalendarHelper();
            //    objDiaryHelper = new DiaryCalendarHelper(this.PageID); //MITS 34492 hlv

            //    diaryDoc = objDiaryHelper.GetDiaryList(objCurrentControl);

            //    DayViewWebScheduleInfo.Activities.Clear();
            //    objDiaryHelper.GetDiaryActivities(ref DayViewWebScheduleInfo, diaryDoc, objCurrentControl.Action);
            //    //txtCurrentDate.Text = "";
            //    //foreach (Appointment item in DayViewWebScheduleInfo.Activities)
            //    //{
            //    //    txtCurrentDate.Text = txtCurrentDate.Text + item.Description;
            //    //}
            //    //Response.Write(txtCurrentDate.Text);
            //    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "divelement", txtCurrentDate.Text, true);
            //    //ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "divelement", txtCurrentDate.Text, true);
            //}
            //catch (Exception ee)
            //{
            //    ErrorHelper.logErrors(ee);
            //    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            //    err.Add(ee, BusinessAdaptorErrorType.SystemError);
            //    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            //}
            //finally
            //{
            //    if (objDiaryHelper != null)
            //    {
            //        objDiaryHelper = null;
            //    }
            //    if (diaryDoc != null)
            //    {
            //        diaryDoc = null;
            //    }
            //    if (objCurrentControl != null)
            //    {
            //        objCurrentControl = null;
            //    }
            //}
        }
        //asharma326 JIRA 11081 starts
        private enum CalledFrom
        {
            Prev,
            Next,
            DateControl
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            sDivElements = "";
            UpdateAsynUI(CalledFrom.Prev);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            sDivElements = "";
            UpdateAsynUI(CalledFrom.Next);
        }

        
        private void UpdateAsynUI(CalledFrom currentOperation)
        {
            XmlDocument diaryDoc = null;
            DiaryCalendarHelper objDiaryHelper = null;
            CurrentWebSchedule objCurrentControl = null;
            string userName = string.Empty;
            TimeZone timeZone = null;
            TimeZoneInfo timeZoneInfo = null;
            bool isTimeZoneDifferent = false;
            try
            {

                if (!IsPostBack)
                {
                    return;
                }
                timeZone = TimeZone.CurrentTimeZone;
                timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone.StandardName);

                objCurrentControl = new CurrentWebSchedule();
                objCurrentControl.CurrentControl = "webdayview";
                objCurrentControl.Action = ViewState["action"].ToString();

                //string[] formats = { "dd/MM/yyyy" };
                var dateTime = DateTime.Parse(hdnserverdate.Value, new CultureInfo(DayViewWebScheduleInfo.CultureInfo.Name, false));

                switch (currentOperation)
                {
                    case CalledFrom.Prev:
                        dateTime = dateTime.AddDays(-1);
                        break;
                    case CalledFrom.Next:
                        dateTime = dateTime.AddDays(1);
                        break;
                }
                    

                if (TimeSpan.Compare(timeZoneInfo.BaseUtcOffset, new TimeSpan()) > 0)
                {
                    isTimeZoneDifferent = true;
                //    objCurrentControl.CurrentActiveDate = dateTime.ToString();//.AddDays(1).ToString();
                //    this.DiaryDayView.CaptionHeaderText = dateTime.Year.ToString();//.AddDays(1).Year.ToString();
                }
                //if (TimeSpan.Compare(timeZoneInfo.BaseUtcOffset, new TimeSpan()) <= 0)
                //{
                    objCurrentControl.CurrentActiveDate = dateTime.ToString();
                    this.DiaryDayView.CaptionHeaderText = dateTime.Year.ToString();
                //}
                //objDiaryHelper = new DiaryCalendarHelper();
                objDiaryHelper = new DiaryCalendarHelper(this.PageID); //MITS 34492 hlv
                objDiaryHelper.s_User = Convert.ToString(ViewState["id"]);
                diaryDoc = objDiaryHelper.GetDiaryList(objCurrentControl);
                if (btnSetDate.Visible)
                    objDiaryHelper.bCreateHyperlink = false;
                objDiaryHelper.bShowCloseImg = false;
                objDiaryHelper.bShowCloseImg = false;
                DayViewWebScheduleInfo.Activities.Clear();
                objDiaryHelper.GetDiaryActivities(ref DayViewWebScheduleInfo, diaryDoc, objCurrentControl.Action);
                //txtCurrentDate.Text = "";
                foreach (Appointment item in DayViewWebScheduleInfo.Activities)
                {
                    sDivElements = sDivElements+ item.Description;
                }
                lblCount.Text = "Count : " + Convert.ToString(diaryDoc.SelectSingleNode("//diaries").Attributes["maxrecord"].Value);
                hdnserverdate.Value = objCurrentControl.CurrentActiveDate.ToString();
                txtDate.Text = dateTime.ToShortDateString();
                if (!isTimeZoneDifferent)
                    ((Infragistics.WebUI.WebSchedule.WebScheduleViewBase)(this.DiaryDayView)).WebScheduleInfo.ActiveDayUtc = new SmartDate(dateTime.AddDays(1));
                else
                    ((Infragistics.WebUI.WebSchedule.WebScheduleViewBase)(this.DiaryDayView)).WebScheduleInfo.ActiveDayUtc = new SmartDate(dateTime);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnSelectDate_Click(object sender, EventArgs e)
        {
            sDivElements = "";
            UpdateAsynUI(CalledFrom.DateControl);
        }
        //asharma326 JIRA 11081 Ends
    }
}