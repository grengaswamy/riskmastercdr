﻿/********************************************************************************
    Class Name: IntegrationHelper.cs 
    Author    : Nitin 
    Created on: 27-Aug-2008
 *  Purpose   :  This class contains methods create html markup strings on the basis 
 *               given conditions 
 ********************************************************************************
*/

using System;
using System.Configuration;

namespace Riskmaster.UI.Diaries.DiaryCalendar
{
    public class IntegrationHelper
    {
        public IntegrationHelper()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Public Methods
        /// <summary>
        /// Created by Nitin to get html markupstring ,of AttachedRecord url
        /// </summary>
        /// <param name="attachTable"></param>
        /// <param name="attachRecordId"></param>
        /// <returns></returns>
        public string GetAttachedRecordUrl(string attachTable, string attachRecordId)
        {
            string serverName = string.Empty;
            string attachedScreenUrl = string.Empty;
            string sAttachTable = string.Empty;
            try
            {
                if (ConfigurationManager.AppSettings["tomcatserver"] == null)
                {
                    throw new ApplicationException("ServerName is not found in Configuration");
                }

                serverName = ConfigurationManager.AppSettings["tomcatserver"];

                switch (attachTable)
                {
                    case "FUNDS":
                        //orbeonUrl = "home?pg=riskmaster/Funds/trans&TransID=" + attachRecordId;
                        //attachedScreenUrl = "/RiskmasterUI/UI/Funds/trans&TransID=" + attachRecordId;

                        break;
                    case "EVENT":
                            attachedScreenUrl = "/RiskmasterUI/UI/FDM/event.aspx?recordID" + attachRecordId + "&SysCmd=0";
                        break;
                    case "CLAIM":
                        //orbeonUrl = "fdm?SysFormName=claim&SysFormId=" + attachRecordId + "&SysCmd=0";

                        break;
                    case "POLICY_ENH":
                        //orbeonUrl = "fdm?SysFormName=policyenh&SysFormId=" + attachRecordId + "&SysCmd=0";
                        break;
                    case "CMXCMGRHIST":
                        sAttachTable = "CmXCmgrHist";
                        //orbeonUrl = "fdm?SysFormName=" + sAttachTable + "&SysFormId=" + attachRecordId + "&SysCmd=0";
                        break;
                    case "CMXVOCREHAB":
                        sAttachTable = "CmXVocrehab";
                        //orbeonUrl = "fdm?SysFormName=" + sAttachTable + "&SysFormId=" + attachRecordId + "&SysCmd=0";
                        break;
                    case "CMXTREATMENTPLN":
                        sAttachTable = "CmXTreatmentPln";
                        //orbeonUrl = "fdm?SysFormName=" + sAttachTable + "&SysFormId=" + attachRecordId + "&SysCmd=0";
                        break;
                    case "CMXMEDMGTSAVINGS":
                        sAttachTable = "CmXMedmgtsavings";
                        //orbeonUrl = "fdm?SysFormName=" + sAttachTable + "&SysFormId=" + attachRecordId + "&SysCmd=0";
                        break;
                    case "CASEMGRNOTES":
                        sAttachTable = "CaseMgrNotes";
                        //orbeonUrl = "fdm?SysFormName=" + sAttachTable + "&SysFormId=" + attachRecordId + "&SysCmd=0";
                        break;
                    default:
                        sAttachTable = attachTable.ToLower();
                        //orbeonUrl = "fdm?SysFormName=" + sAttachTable + "&SysFormId=" + attachRecordId + "&SysCmd=0";
                        break;
                }



                return attachedScreenUrl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        public string GetAttachedTableCategory(string attachTable)
        {
            string attachedTableCategory = string.Empty;
               
                //cases are changed by Nitin for Mits 15044 on 07th April 09
                switch (attachTable)
                {
                    case "LEAVEPLAN":
                        attachedTableCategory = "Leaveplan";
                        break;
                    case "ADJUST_DATED_TEXT":
                        attachedTableCategory = "adjusterdatedtext";
                        break;
                    case "POLICY_ENH":
                        attachedTableCategory = "policyenh";
                        break;
                    // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                    case "POLICY_ENH_AL":
                        attachedTableCategory = "policyenhal";
                        break;
                    case "POLICY_ENH_GL":
                        attachedTableCategory = "policyenhgl";
                        break;
                    case "POLICY_ENH_PC":
                        attachedTableCategory = "policyenhpc";
                        break;
                    case "POLICY_ENH_VA":
                        attachedTableCategory = "policyenhva";
                        break;
                    case "POLICY_ENH_WC":
                        attachedTableCategory = "policyenhwc";
                        break;
                    // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries
                    case "DISABILITY_PLAN":
                        attachedTableCategory = "plan";
                        break;
                    case "MED_STAFF":
                        attachedTableCategory = "staff";
                        break;
                    case "BILL_X_INSTLMNT":
                        attachedTableCategory = "policybilling";
                        break;
                    case "BILL_X_BILL_ITEM":
                        attachedTableCategory = "policybilling";
                        break;
                    default:
                        attachedTableCategory = attachTable.ToLower();
                        break;
                }
                return attachedTableCategory;
            }
        

        /// <summary>
        /// Created by Nitin to get html markupstring ,of edit diaryentry
        /// </summary>
        /// <param name="diaryEntryId"></param>
        /// <returns></returns>
        public string GetOrbeonDiaryEntryUrl(string diaryEntryId)
        {
            string tomcatServerName = string.Empty;
            string diaryDescUrl = string.Empty;
            try
            {
                //RMA - 4691
                //diaryDescUrl = "../EditDiary.aspx?entryid=" + diaryEntryId;
                diaryDescUrl = "/RiskmasterUI/UI/FDM/creatediary.aspx?entryid=" + diaryEntryId + "&recordID=" + diaryEntryId + "&CalledBy=creatediary";

                return diaryDescUrl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        # endregion
    }
}