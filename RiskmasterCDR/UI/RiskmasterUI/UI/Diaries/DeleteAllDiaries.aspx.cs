﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Diaries
{
    public partial class DiaryAllDiaries : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["ClaimId"] != null)
                    {
                        ViewState["claimid"] = Request.QueryString["ClaimId"].ToString();
                    }
                    else
                    {
                        ViewState["claimid"] = "";
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlNode GetInputDocForDeleteDiaries()
        {
            XmlDocument deleteAllDoc = null;
            XmlNode deleteAllNode = null;
            XmlNode claimIdNode = null;
            XmlNode actionNode = null;

            deleteAllDoc = new XmlDocument();
            deleteAllNode = deleteAllDoc.CreateElement("DeleteAll");

            claimIdNode = deleteAllDoc.CreateElement("ClaimId");
            claimIdNode.InnerText = ViewState["claimid"].ToString();
            deleteAllNode.AppendChild(claimIdNode); 

            actionNode = deleteAllDoc.CreateElement("Action");
            actionNode.InnerText = "DeleteAllDiaries";
            deleteAllNode.AppendChild(actionNode);

            deleteAllDoc.AppendChild(deleteAllNode);

            return deleteAllNode; 
        }
        
        private XmlDocument DeleteAllDiaries()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "WPAAdaptor.DeleteAllDiaries";

                inputDocNode = GetInputDocForDeleteDiaries();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            XmlDocument responseDoc = null;

            try
            {
                responseDoc = DeleteAllDiaries();

                if (responseDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                {
                    this.Page.ClientScript.RegisterStartupScript(typeof(string), "DeleteAllDiaries", "self.close();", true); 
                }
                else
                {
                    throw new ApplicationException(responseDoc.InnerXml);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
