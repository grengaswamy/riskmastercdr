﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.Diaries
{
    public partial class DiaryListCalendar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Rakhel ML Changes
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DiaryListCalendar.aspx"), "DiaryListCalendarValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "DiaryListCalendarValidations", sValidationResources, true);
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //Rakhel ML Changes
                if (!IsPostBack)
                {
                    if (Request.QueryString["assigneduser"] != null)
                    {
                        hdnAssignedUser.Value = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        hdnAssignedUser.Value = "";
                    }

                    if (Request.QueryString["attachtable"] != null)
                    {
                        hdnAttachtable.Value  = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        hdnAttachtable.Value = "";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        hdnAttachRecordId.Value  = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        hdnAttachRecordId.Value = "";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        hdnIsPeek.Value  = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        hdnIsPeek.Value = "false";
                    }
                    
                    
                    currentyear.Value = System.DateTime.Now.Year.ToString();
                    hdnCurrentMonth.Value = System.DateTime.Now.Month.ToString();//MGaba2:MITS 14638
                }
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string isDiaryCalPeek = string.Empty;
            string isDiaryCalProssOffice = string.Empty;
            string diaryListUrl = string.Empty;
            string assignedUser = string.Empty;

            try
            {
                if (Request.QueryString["isdiarycalendar"] != null)
                {
                    if (Request.QueryString["isdiarycalendar"] == "true")
                    {
                        isDiaryCalPeek = Request.QueryString["isdiarycalendarpeek"];
                        isDiaryCalProssOffice = Request.QueryString["isdiarycalendarprocessingoffice"];
                        assignedUser = Request.QueryString["assigneduser"];

                        diaryListUrl = "DiaryCalendar/DiaryCalendar.aspx?assigneduser=" + hdnAssignedUser.Value + "&IsPeek=" + isDiaryCalPeek + "&IsProcessingOffice=" + isDiaryCalProssOffice;
                    }
                }
                else
                {
                    diaryListUrl = "DiaryList.aspx?assigneduser=" + hdnAssignedUser.Value + "&ispeekdiary=" + hdnIsPeek.Value + "&attachtable=" + hdnAttachtable.Value + "&attachrecordid=" + hdnAttachRecordId.Value;
                }

                Response.Redirect(diaryListUrl, false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
