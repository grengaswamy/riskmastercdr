﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetEmailOptions.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.SetEmailOptions" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Set email Options</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script type="text/javascript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" src="../../Scripts/security.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">{ var i; }</script>

</head>
<body class="10pt" onload="pageLoaded();">
    <form id="frmData" runat="server" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
   
    
    <asp:TextBox ID="txtformname" runat="server" Style="display: none" RMXRef="/Instance/Document/form[ @name = 
'SetEmailOptions' ]" rmxretainvalue="true"></asp:TextBox>
   <table border="0">
        <tr>
            <td>
                <table border="0">
                    <tr>
                        <td class="ctrlgroup" colspan="2">
                            Set E-mail Options
                        </td>
                    </tr>
                    <tr>
                        <td>
                            SMTP Server
                        </td>
                        <td>
                            <asp:TextBox size="30" ID="smtpserver" onchange="setDataChanged(true);" runat="server"
                                RMXRef="/Instance/Document/form/group/displaycolumn/control[ @name = 
'smtpserver' ]" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Admin E-mail Address
                        </td>
                        <td>
                            <asp:TextBox ID="adminemailaddr" onchange="setDataChanged(true);" runat="server"
                                size="30" RMXRef="/Instance/Document/form/group/displaycolumn/control[ @name = 'adminemailaddr' ]"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Use Secure DSN Login
                        </td>
                        <td>
                            <asp:CheckBox runat="server" value="True" appearance="full" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name =  'chkSecure' ]"
                                ID="chkSecure" />
                        </td>
                    </tr>
                    <!--rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 -->
                    <tr>
                        <td>
                            Sender's Alternate Domain
                        </td>
                        <td>
                            <asp:TextBox ID="senderalternatedomain" onchange="setDataChanged(true);" runat="server"
                                size="30" RMXRef="/Instance/Document/form/group/displaycolumn/control[ @name = 'senderalternatedomain' ]"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Reply-to Alternate Domain
                        </td>
                        <td>
                            <asp:TextBox ID="senderreplytodomain" onchange="setDataChanged(true);" runat="server"
                                size="30" RMXRef="/Instance/Document/form/group/displaycolumn/control[ @name = 'senderreplytodomain' ]"></asp:TextBox>
                        </td>
                    </tr>
                    <!-- end rsushilaggar -->
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:TextBox name="$node^9" runat="server" Style="display: none" ID="hdAction"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:TextBox name="$node^48" Style="display: none" runat="server" ID="hdUserLoginName"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnOK" CssClass="button" Text="OK" OnClientClick="return ClosePopUpOnSuccess('','SetEmailOptions','');"
                                runat="server" />
                            <asp:Button ID="btnCancel" CssClass="button" Text="Cancel" runat="server" OnClientClick="OnCancel();" />
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
            </td>
        </tr>
    </table>
     <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
