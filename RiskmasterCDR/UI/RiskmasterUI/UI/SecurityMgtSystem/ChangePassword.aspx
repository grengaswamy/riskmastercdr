<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.ChangePassword" ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="pleasewait" Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Password</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js"></script>

    <script type="text/javascript">
							function ValidateComplexity()
							{
							    if (document.getElementById('hdispwdpolicyenabled').value == "true") {
							        document.getElementById('btnvalidate').click();
							    }
							}
							function LoadAfterAjaxHandler() {
							    //Handler attached to the request
							    try //asharma326 MITS 27586 for handling exe
							    {
							        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
							        if (window.opener != null && window.opener.document.getElementById('password') != null) {
							            document.getElementById('hdoldpwd').value = window.opener.document.getElementById('password').value;
							        
							        }
							        if (window.opener != null && window.opener.document.getElementById('loginname') != null) {
							            document.getElementById('hdopenerusername').value = window.opener.document.getElementById('loginname').value;
							        
							        }
							    }
							    catch (exe)
							    {}
							}
							function EndRequestHandler(sender, eArgs) {
							    if (document.getElementById('enablesave').value == "true") {
							        document.getElementById('btnOK').disabled = false; //RMA-866  Make btnOK button id same as control id
							    }
							    else if (document.getElementById('enablesave').value == "false") {
							        document.getElementById('btnOK').disabled = true; //RMA-866  Make btnOK button id same as control id
							    }
							}
							function ClosePage() {
							    window.opener.document.forms[0].hdSave.value = 'refresh';
							    window.opener.document.forms[0].submit(); 
							    window.close();
							    return false;
							}
    </script>

</head>
    <!--RMA-866  Make btnOK button id same as control id-->
<body class="10pt" onload="LoadAfterAjaxHandler();if (document.getElementById('enablesave') != null && document.getElementById('enablesave').value == 'false') {
							        if(document.getElementById('hdispwdpolicyenabled').value == 'true'){
							        document.getElementById('btnOK').disabled = true;}
							    }">
    <form id="frmData" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    
    <!-- MITS 17812 : Raman Bhatia -->
            <%if (Request.QueryString["firstname"] != null)
              { %>
               
            <div runat="server" id="DivControls1">
                <div runat="server" id="Div1" align="center" visible="true">
                <b>
                    Please select a new password for '<%=Request.QueryString["firstname"].ToString()%> <%=Request.QueryString["lastname"].ToString()%>' 
               </b>
             </div>   
            </div>
            <%
            }
                %>
                <div id="toolbardrift" class="toolbardrift" style="width: 100%">
                    <div class="image">
                        <asp:ImageButton runat="server" ID="btnOK" name="btnOK" ImageUrl="../../Images/save.gif"
                            OnClientClick="return ChangePassword();" ToolTip="Save Changed Data"
                            Width="28" Height="28" border="0" OnClick="btnOK_Click" onMouseOver="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                            onMouseOut="this.src='../../Images/save.gif';this.style.zoom='100%'" title="Save Changed Data"
                            TabIndex="4"></asp:ImageButton>
                    </div>
                </div>
                </div>
                <div runat="server" id="DivControls2">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
                <div style="height:10%">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
                </div>
                <caption>
                   
                   
                   
                </caption>
                <br />
                <br />
                <br />
                
                <asp:HiddenField ID="enablesave" runat="server" value="false"/>
                 <% if (hdispwdpolicyenabled.Value == "true") {%>
                  <asp:UpdateProgress runat="server" ID="updateprogress1" AssociatedUpdatePanelID="UpdatePanel2">
                  <ProgressTemplate><strong>Validating Complexity...</strong></ProgressTemplate>
                  </asp:UpdateProgress>
                   <%} %>
                            <asp:Label ID="lblstatus" runat="server" CssClass="ChangePwdlblStatus"></asp:Label>
                            
                <table align="left" width="80%">
                   
                    <div runat="server" id="DivOldPassword">
                        <tr>
                            <td>
                                <asp:Label ID="lbloldpwd" runat="server" Text="Old Password"></asp:Label> <%--asharma326 MITS 27586--%>
                            </td>
                            <td>
                                <asp:TextBox ID="oldpasswd" runat="server" EnableViewState="true" MaxLength="50"
                                    name="oldpasswd" onchange="return setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[ @name = 'oldpasswd' ]"
                                    size="30" TextMode="Password" TabIndex="1" autocomplete="off"></asp:TextBox>
                            </td>
                            
                        </tr>
                    </div>
            <tr>
                <td>
                    New Password
                        </td>
                        <td>
                            <asp:TextBox ID="newpasswd" runat="server" EnableViewState="true" MaxLength="50"
                                name="newpasswd" onblur="return ValidateComplexity();" onchange="return setDataChanged(true);"
                                rmxref="/Instance/Document/form/group/displaycolumn/control[ @name = 'newpasswd' ]"
                                size="30" TabIndex="2" TextMode="Password" autocomplete="off"></asp:TextBox>
                        </td>
                       
                        <asp:Button ID="btnvalidate" runat="server" name="btnvalidate" OnClick="btnvalidate_Click"
                            Style="display: none" />
                        
                    </tr>
                    <tr>
                        <td>
                    <asp:Label ID="Label1" runat="server" Text="Confirm New Password"></asp:Label>
                        </td>
                        <td>
                    <asp:TextBox ID="confirmpasswd" runat="server" EnableViewState="true" MaxLength="50"
                        name="confirmpasswd" onchange="return setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[ @name = 'confirmpasswd' ]"
                        size="30" TabIndex="3" TextMode="Password" autocomplete="off"></asp:TextBox>
                </td>
            </tr>
            </table> </div>
            <%--            <div runat="server" id="DivSuccessMsg" align="center" visible="false" class="msgheader">
                <caption>
                    <br>
                    <br></br>
                    <br></br>
                </caption>
                Password associated with your Login has been changed successfully.
            </div>--%>
           
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
     <div runat="server" id="DivSuccessMsg" visible="false">
                <table width="100%">
                    <tr>
                        <td class="msgheader" bgcolor="#D5CDA4">
                            <asp:Label runat="server" ID="lblUpdateStatus"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table style="margin-top: 10%; margin-left: 25%;">
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblmessage" Text="Password associated with your Login has been changed successfully."/><%--asharma326 MITS 27586--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <br />
                            <br />
                <div id="divlogin" runat="server" visible="false">
                    <a href="../Home/Login.aspx" class="LightBold" style="font-size: small; font-weight: bold; font-style: normal; font-variant: normal"><< Back to Login Page</a><%--asharma326 MITS 27586--%>
                </div>
            </td>
                    </tr>
                     <% if (hdInvokedFrom.Text == "pwdexpire")
                        { %>
                     <tr>
                         <td align="center"><a href="../Home/Status.aspx" class="LightBold">Continue to RM application</a></td>
                      </tr>
                      <%} %>
                      <% if (hdopenerusername.Value!= "") { %>
                      <tr>
                      <td align="center"><a href="#" onclick="return ClosePage();" class="LightBold">Close</a></td>
                      </tr>
                      <%} %>
                </table>
            </div>
    <asp:TextBox runat="server" name="hdAction" ID="hdAction" type="hidden" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" name="hdLoginName" ID="hdLoginName" rmxref="/Instance/Document/form/group/displaycolumn/control[ @name = 'hdLoginName' ]"
        type="hidden" Style="display: none"></asp:TextBox>
        <asp:TextBox runat="server" name="hdIsPwdReset" ID="hdIsPwdReset" rmxref="/Instance/Document/form/group/displaycolumn/control[ @name = 'hdIsPwdReset' ]"
        type="hidden" Style="display: none"></asp:TextBox><%--asharma326 MITS 27586--%>
    <asp:HiddenField ID="hdispwdpolicyenabled" runat="server" />
    <asp:HiddenField ID="hdoldpwd" runat="server" />
    <asp:HiddenField ID="hdopenerusername" runat="server" />
    
    <%-- NOTE- hdInvokedFrom TAKES 2 VALUES: "chgpwd" AND "pwdexpire" --%>
    <asp:TextBox runat="server" name="hdInvokedFrom" ID="hdInvokedFrom" type="hidden" Style="display: none"></asp:TextBox>
        <uc:pleasewait CustomMessage="Loading" runat="server" ID="pleasewait" />
    </form>
</body>
</html>
