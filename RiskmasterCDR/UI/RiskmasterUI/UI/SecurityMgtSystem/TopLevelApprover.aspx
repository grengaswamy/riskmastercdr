﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TopLevelApprover.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.TopLevelApprover" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="pleasewait"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Line of Business Top Level Approval</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
   

    <script type="text/javascript" language="javascript">
        function TopLevelApprover_Save() {

            //        Added by Akashyap3 on 14-Apr-09

            if (document.forms[0].GCReserveMax.value == '') {
                document.forms[0].GCReserveMax.value = 0;
            }

            if (document.forms[0].GCPaymentMax.value == '') {
                document.forms[0].GCPaymentMax.value = 0;
            }

            if (document.forms[0].VAReserveMax.value == '') {
                document.forms[0].VAReserveMax.value = 0;
            }

            if (document.forms[0].VAPaymentMax.value == '') {
                document.forms[0].VAPaymentMax.value = 0;
            }

            if (document.forms[0].WCReserveMax.value == '') {
                document.forms[0].WCReserveMax.value = 0;
            }

            if (document.forms[0].WCPaymentMax.value == '') {
                document.forms[0].WCPaymentMax.value = 0;
            }

            if (document.forms[0].STDReserveMax.value == '') {
                document.forms[0].STDReserveMax.value = 0;
            }

            if (document.forms[0].STDPaymentMax.value == '') {
                document.forms[0].STDPaymentMax.value = 0;
            }

            //Anu Tennyson: MITS 18145 Starts

            if (document.forms[0].PCReserveMax.value == '') {
                document.forms[0].PCReserveMax.value = 0;
            }
            if (document.forms[0].PCPaymentMax.value == '') {
                document.forms[0].PCPaymentMax.value = 0;
            }
            //Anu Tennyson: MITS 18145 Ends

            //Jira 6385- Incurred Limit starts
          
            if (document.forms[0].GCClaimIncurredMax.value == '') {
                document.forms[0].GCClaimIncurredMax.value = 0;
            }

            if (document.forms[0].VAClaimIncurredMax.value == '') {
                document.forms[0].VAClaimIncurredMax.value = 0;
            }

            if (document.forms[0].WCClaimIncurredMax.value == '') {
                document.forms[0].WCClaimIncurredMax.value = 0;
            }

            if (document.forms[0].STDClaimIncurredMax.value == '') {
                document.forms[0].STDClaimIncurredMax.value = 0;
            }

            if (document.forms[0].PCClaimIncurredMax.value == '') {
                document.forms[0].PCClaimIncurredMax.value = 0;
            }
            //Jira 6385- Incurred Limit ends

            if (m_DataChanged == false) {
                return false;
            }
            else {
                document.forms[0].SysCmd.value = '5';
                pleaseWait.Show();
                return true;
            }
        }
    </script>

    <script  type="text/javascript" language  ="JavaScript" src="../../Scripts/cul.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/Utilities.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/WaitDialog.js"></script>

</head>
<body class="10pt" onload="pageLoaded();">
    <form id="frmData" runat="server" name="frmData" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox runat="server" Style="display: none" ID="hTabName"></asp:TextBox>
    <div id="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" height="32">
                    <asp:ImageButton runat="server" ID="Save" ImageUrl="../../Images/save.gif" OnClientClick="return TopLevelApprover_Save();"
                        ToolTip="Save" Width="28" Height="28" border="0" onMouseOver="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                        onMouseOut="this.src='../../Images/save.gif';this.style.zoom='100%'" title="Save" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div class="msgheader" id="formtitle">
        Line of Business Top Level Approval</div>
    <div class="errtextheader">
    </div>
    <br />
    <table border="0">
        <tr>
            <td>
                <%--abansal23: MITS 14667 Starts--%>
                <div class="tabGroup" id="TabsDivGroup" runat="server">
                    <div class="Selected" runat="server" nowrap="true" name="TABSGeneralClaim" id="TABSGeneralClaim">
                        <a class="Selected" href="#" runat="server" onclick="tabChange(this.name);" name="GeneralClaim"
                            id="LINKTABSGeneralClaim"><span style="text-decoration: none">General Claim</span></a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSPGeneralClaim">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" runat="server" nowrap="true" name="TABSVehicleAccident"
                        id="TABSVehicleAccident">
                        <a class="NotSelected1" href="#" runat="server" onclick="tabChange(this.name);" name="VehicleAccident"
                            id="LINKTABSVehicleAccident"><span style="text-decoration: none">Vehicle Accident</span></a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSPVehicleAccident">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" runat="server" nowrap="true" name="TABSWorkersCompensation"
                        id="TABSWorkersCompensation">
                        <a class="NotSelected1" href="#" runat="server" onclick="tabChange(this.name);" name="WorkersCompensation"
                            id="LINKTABSWorkersCompensation"><span style="text-decoration: none">Workers Compensation</span></a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSPWorkersCompensation">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" runat="server" nowrap="true" name="TABSNonOccupational"
                        id="TABSNonOccupational">
                        <a class="NotSelected1" href="#" runat="server" onclick="tabChange(this.name);" name="NonOccupational"
                            id="LINKTABSNonOccupational"><span style="text-decoration: none">Non Occupational</span></a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSPNonOccupational">
                        <nbsp />
                        <nbsp />
                    </div>
              <%--Anu Tennyson: MITS 18145 Starts--%>
                    <div class="NotSelected" runat="server" nowrap="true" name="TABSPropertyClaim"
                        id="TABSPropertyClaim">
                        <a class="NotSelected1" href="#" runat="server" onclick="tabChange(this.name);" name="PropertyClaim"
                            id="LINKTABSPropertyClaim"><span style="text-decoration: none">Property Claim</span></a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSPPropertyClaim">
                        <nbsp />
                        <nbsp />
                    </div>
                <%--Anu Tennyson: MITS 18145 Ends--%>    
                  
                    <div class="NotSelected" runat="server" nowrap="true" name="TABSTopLevel" id="TABSTopLevel">
                        <a class="NotSelected1" href="#" runat="server" onclick="tabChange(this.name);" name="TopLevel"
                            id="LINKTABSTopLevel"><span style="text-decoration: none">Top Level</span></a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSPTopLevel">
                        <nbsp />
                        <nbsp />
                    </div>
                    <%--abansal23: MITS 14667 Ends--%>
                </div>
                <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 800px;
                    height: 315px; overflow: auto">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table border="0" cellspacing="0" runat="server" cellpadding="0" name="FORMTABGeneralClaim"
                                    id="FORMTABGeneralClaim">
                                    <tr>
                                        <td>
                                       <%--Added value="241" by Akashyap3 on 14-Apr-09--%>
                                            <asp:TextBox runat="server" name="GCLOB" value="241" Style="display: none" ID="GCLOB"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name='GCLOB']"></asp:TextBox>
                                            User Name:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                        <%--akaur9: MITS 26518 Start--%>
                                            <asp:TextBox type="code" runat="server" name="GCUserName" size="30" ID="GCUserName" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='GCUserName']" onchange="CheckUser('GCUserName','GCUserName_cid');"></asp:TextBox>   <%--akaur9: onchange event added--%>
                                                                                                                                        
                                            
                                            <asp:button runat = "server" CssClass="CodeLookupControl" ID="GCUserNamebtn" OnClientClick="return AddCustomizedListUser('toplevelapproval','GCUserName','GCUserName_cid','UserStr')"/>
                                            
                                            <%--<asp:Button runat="server" class="CodeLookupControl" ID="GCUserNamebtn" OnClientClick="return selectCode('rm_sys_users','GCUserName');">--%>
                                            <%--</asp:Button> --%>
                                        <%--akaur9: MITS 26518 --%>
                                            <asp:TextBox runat="server" Style="display: none" ID="GCUserName_cid" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='GCUserName']/@codeid"
                                                cancelledvalue=""></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Reserve Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="GCReserveMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="GCReserveMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='GCReserveMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Payment Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="GCPaymentMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="GCPaymentMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='GCPaymentMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                   <%-- Jira 6385- Incurred Limit--%>
                                     <tr>
                                        <td>
                                            Claim Incurred Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="GCClaimIncurredMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="GCClaimIncurredMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='GCClaimIncurredMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                   <%--  Jira 6385- Incurred Limit--%>
                                </table>
                                <table border="0" cellspacing="0" runat="server" cellpadding="0" name="FORMTABVehicleAccident"
                                    id="FORMTABVehicleAccident" style="display: none;">
                                    <%--abansal23: MITS 14667 --%>
                                    <tr>
                                        <td>
                                            <asp:TextBox runat="server" name="VALOB" value="242" Style="display: none" ID="VALOB"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name='VALOB']"></asp:TextBox>
                                            User Name:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <%--akaur9: MITS 26518 Start --%>
                                            <asp:TextBox runat="server" name="VAUserName" type="code" size="30" ID="VAUserName" cancelledvalue="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='VAUserName']" onchange="CheckUser('VAUserName','VAUserName_cid');"></asp:TextBox>  <%-- akaur9 onchange event added --%>
                                          
                                            <asp:button runat = "server" CssClass="CodeLookupControl" ID="VAUserNamebtn" OnClientClick = "return AddCustomizedListUser('toplevelapproval','VAUserName','VAUserName_cid','UserStr')"/>
                                            <%--<asp:Button runat="server" class="CodeLookupControl" ID="VAUserNamebtn" OnClientClick="return selectCode('rm_sys_users','VAUserName');"> </asp:Button> --%>
                                            <%--akaur9: MITS 26518 Ends --%>
                                            <asp:TextBox runat="server" value="" Style="display: none" ID="VAUserName_cid" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='VAUserName']/@codeid"
                                                cancelledvalue=""></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Reserve Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="VAReserveMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="VAReserveMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='VAReserveMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Payment Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="VAPaymentMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="VAPaymentMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='VAPaymentMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%-- Jira 6385- Incurred Limit--%>
                                    <tr>
                                        <td>
                                            Claim Incurred Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="VAClaimIncurredMax:" value="" size="30" onblur="numLostFocus(this);"
                                                ID="VAClaimIncurredMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='VAClaimIncurredMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%-- Jira 6385- Incurred Limit--%>
                                </table>
                                <table border="0" runat="server" cellspacing="0" cellpadding="0" name="FORMTABWorkersCompensation"
                                    id="FORMTABWorkersCompensation" style="display: none;">
                                    <%--abansal23: MITS 14667 --%>
                                    <tr>
                                        <td>
                                            <asp:TextBox runat="server" name="WCLOB" value="243" Style="display: none" ID="WCLOB"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name='WCLOB']"></asp:TextBox>
                                            User Name:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                        <%--akaur9: MITS 26518 Start--%>
                                            <asp:TextBox runat="server" name="WCUserName" type="code" size="30" ID="WCUserName" cancelledvalue="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='WCUserName']" onchange="CheckUser('WCUserName','WCUserName_cid');"></asp:TextBox>  <%--akaur9 onchange event added --%>
                                           
                                            <%--<asp:Button runat="server" class="CodeLookupControl" ID="WCUserNamebtn" OnClientClick="return selectCode('rm_sys_users','WCUserName');"> </asp:Button> --%>
                                            <asp:button runat = "server" CssClass = "CodeLookupControl" ID ="WCUserNamebtn" OnClientClick ="return AddCustomizedListUser('toplevelapproval','WCUserName','WCUserName_cid','UserStr')"/>
                                         <%--akaur9: MITS 26518 Ends--%>
                                            <asp:TextBox runat="server" Style="display: none" ID="WCUserName_cid"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name='WCUserName']/@codeid"
                                                cancelledvalue=""></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Reserve Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="WCReserveMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="WCReserveMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='WCReserveMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Payment Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="WCPaymentMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="WCPaymentMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='WCPaymentMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%-- Jira 6385- Incurred Limit--%>
                                     <tr>
                                        <td>
                                            Claim Incurred Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="WCClaimIncurredMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="WCClaimIncurredMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='WCClaimIncurredMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%-- Jira 6385- Incurred Limit--%>
                                </table>
                                <table border="0" runat="server" cellspacing="0" cellpadding="0" name="FORMTABNonOccupational"
                                    id="FORMTABNonOccupational" style="display: none;">
                                    <%--abansal23: MITS 14667 --%>
                                    <tr>
                                        <td>
                                            <asp:TextBox runat="server" name="STDLOB" value="844" Style="display: none" ID="STDLOB"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name='STDLOB']"></asp:TextBox>
                                            User Name:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                        <%--akaur9: MITS 26518 Start--%>
                                            <asp:TextBox runat="server" name="STDUserName" type="code" size="30" ID="STDUserName" cancelledvalue="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='STDUserName']" onchange="CheckUser('STDUserName','STDUserName_cid');"></asp:TextBox>   <%--akaur9 onchange event added --%>                                         
                                            <%--<asp:Button runat="server" class="CodeLookupControl" ID="STDUserNamebtn" OnClientClick="return selectCode('rm_sys_users','STDUserName');">
                                            </asp:Button> --%>
                                             <asp:button runat = "server" CssClass = "CodeLookupControl" ID = "STDUserNamebtn" OnClientClick = "return AddCustomizedListUser('toplevelapproval','STDUserName','STDUserName_cid','UserStr')"/>
                                              <%--akaur9: MITS 26518 Ends--%>
                                             <asp:TextBox runat="server" Style="display: none" ID="STDUserName_cid"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name='STDUserName']/@codeid"
                                                cancelledvalue=""></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Reserve Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="STDReserveMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="STDReserveMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='STDReserveMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Payment Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="STDPaymentMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="STDPaymentMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='STDPaymentMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%-- Jira 6385- Incurred Limit--%>
                                     <tr>
                                        <td>
                                            Claim Incurred Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="STDClaimIncurredMax:" value="" size="30" onblur="numLostFocus(this);"
                                                ID="STDClaimIncurredMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='STDClaimIncurredMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%-- Jira 6385- Incurred Limit--%>
                                </table>
                                
                                <%--Anu Tennyson: MITS 18145 Starts--%>
                                 <table border="0" runat="server" cellspacing="0" cellpadding="0" name="FORMTABPropertyClaim"
                                    id="FORMTABPropertyClaim" style="display: none;">
                                  
                                    <tr>
                                        <td>
                                            <asp:TextBox runat="server" name="PCLOB" value="845" Style="display: none" ID="PCLOB"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name='PCLOB']"></asp:TextBox>
                                            User Name:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                        <%--akaur9: MITS 26518 Start--%>
                                            <asp:TextBox runat="server" name="PCUserName" type="code" size="30" ID="PCUserName" cancelledvalue="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='PCUserName']" onchange="CheckUser('PCUserName','PCUserName_cid');"></asp:TextBox>  <%--akaur9 onchange event added --%>
                                            
                                            <%-- <asp:Button runat="server" class="CodeLookupControl" ID="PCUserNamebtn" OnClientClick="return selectCode('rm_sys_users','PCUserName');">
                                            </asp:Button> --%>
                                            <asp:button runat="server" CssClass="CodeLookupControl" ID="PCUserNamebtn" OnClientClick="return AddCustomizedListUser('toplevelapproval','PCUserName','PCUserName_cid','UserStr')"/>
                                        <%--akaur9: MITS 26518 Ends --%>
                                            <asp:TextBox runat="server" Style="display: none" ID="PCUserName_cid"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name='PCUserName']/@codeid"
                                                cancelledvalue=""></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Reserve Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PCReserveMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="PCReserveMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='PCReserveMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Payment Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PCPaymentMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="PCPaymentMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='PCPaymentMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <%-- Jira 6385- Incurred Limit--%>
                                     <tr>
                                        <td>
                                           Claim Incurred Max:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PCClaimIncurredMax" value="" size="30" onblur="numLostFocus(this);"
                                                ID="PCClaimIncurredMax" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='PCClaimIncurredMax']"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <%-- Jira 6385- Incurred Limit--%>
                                </table>

                                <%--Anu Tennyson: MITS 18145 Ends--%> 
                                
                                <table border="0" runat="server" cellspacing="0" cellpadding="0" name="FORMTABTopLevel"
                                    id="FORMTABTopLevel" style="display: none;">
                                    <%--abansal23: MITS 14667 --%>
                                    <tr>
                                        <td>
                                            <asp:TextBox runat="server" name="TLLOB" value="-1" Style="display: none" ID="TLLOB"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name='TLLOB']"></asp:TextBox>
                                            User Name:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                        <%--akaur9: MITS 26518 Start--%>
                                            <asp:TextBox runat="server" name="TLUserName" type="code" size="30" ID="TLUserName" cancelledvalue="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='TLUserName']"  onchange="CheckUser('TLUserName','TLUserName_cid');"></asp:TextBox>  <%--akaur9 onchange event added --%>
                                            
                                            <%--<asp:Button runat="server" class="CodeLookupControl" ID="TLUserNamebtn" OnClientClick="return selectCode('rm_sys_users','TLUserName');">
                                            </asp:Button> --%>
                                            <asp:button runat="server" CssClass="CodeLookupControl" ID="TLUserNamebtn" OnClientClick="return AddCustomizedListUser('toplevelapproval','TLUserName','TLUserName_cid','UserStr')"/>
                                        <%--akaur9: MITS 26518 Ends--%>
                                            <asp:TextBox runat="server" Style="display: none" ID="TLUserName_cid"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name='TLUserName']/@codeid"
                                                cancelledvalue=""></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
               
                <%--akaur9: MITS 26518 START--%>                                           
                <input type="hidden"  runat="server" value="" id="UserStr"/>
                <%--akaur9: MITS 26518 Ends--%>
                <input type="text" name="" value="" id="SysViewType" style="display: none" />
                <asp:TextBox runat="server" ID="SysCmd" Style="display: none" Text=""></asp:TextBox>
                <input type="text" name="" value="" id="SysCmdConfirmSave" style="display: none" />
                <input type="text" name="" value="" id="SysCmdQueue" style="display: none" />
                <input type="text" name="" value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate" />
                <input type="text" name="" value="" id="SysClassName" style="display: none" rmxforms:value="" />
                <input type="text" name="" value="" id="SysSerializationConfig" style="display: none" />
                <input type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId" />
                <input type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId" />
                <input type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="TopLevelApproval" />
                <input type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value="" />
                <input type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="TopLevelApproval" />
                <input type="text" name="" value="" id="SysRequired" style="display: none" />
                <input type="text" name="" value="" id="SysFocusFields" style="display: none" />
                <%--Mgaba2: R8:SuperVisory Approval--%>
                 <asp:TextBox runat="server" ID="hdnLOBsDeselectedInGenSysParms" Style="display: none" Text="" rmxref="/Instance/Document/form/group/displaycolumn/LOBsDeselectedInGenSysParms"></asp:TextBox>
                
            </td>
        </tr>
    </table>
    <uc:pleasewait CustomMessage="Loading" runat="server" ID="PleaseWaitDialog1" />
    </form>
</body>
</html>
