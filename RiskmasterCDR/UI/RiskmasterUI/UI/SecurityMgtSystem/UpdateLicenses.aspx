﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateLicenses.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.UpdateLicenses" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>

</head>
<body class="10pt" onload="">
    <form id="frmData" runat="server" name="DsnWizard" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
   
    <asp:TextBox ID="txtformname" runat="server" Visible="false" RMXRef="/Instance/Document/form[ @name = 
'frmClear' ]"></asp:TextBox>
    <asp:TextBox ID="txtiserror" runat="server" Visible="false" RMXRef="/Instance/Document/form/license[ @name = 
'lic' ]/@iserror"></asp:TextBox>
    <asp:TextBox ID="txterrmessage" runat="server" Visible="false" RMXRef="/Instance/Document/form/license[ @name = 
'lic' ]/@message"></asp:TextBox>
    <asp:TextBox ID="txtpostback" runat="server" Visible="false" RMXRef="/Instance/Document/form/license[ @name = 
'lic' ]/@postback"></asp:TextBox>
    <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <div id="mainwin" runat="server">
                    <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="ctrlgroup">
                                Change Workstation License Info
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                Enter code below to update licenses:
                            </td>
                            <td>
                                <asp:TextBox runat="server" name="$node^31" size="30" ID="licensecode" RMXref="/Instance/Document/form/license/group/displaycolumn/control[ @name = 'licensecode' ]"
                                    onchange="setDataChanged(true);">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:TextBox runat="server" name="$node^9" value="" Style="display: none" ID="hdAction">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:TextBox runat="server" name="$node^41" value="" RMXref="/Instance/Document/form/license/group/displaycolumn/control[ @name = 'hdDsnId' ]"
                                    Style="display: none" ID="hdDsnId">
                                </asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td align="middle" style="color: red">
                <asp:Literal ID="ltrerror" runat="server" Visible="false"></asp:Literal>
            </td>
        </tr>
        <br>
        <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center">
                    <asp:Button ID="btnUpdate" class="button" runat="server" name="btnUpdate" Text="Update" OnClientClick="return ClosePopUpOnSuccess('','UpdateLicenses','')"
                        Style="" />
                    <asp:Button ID="btnCancel" runat="server" class="button" name="btnCancel" Text="Cancel" OnClientClick="return OnCancel();"
                        Style="" />
                </td>
            </tr>
        </table>
    </table>
     <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
