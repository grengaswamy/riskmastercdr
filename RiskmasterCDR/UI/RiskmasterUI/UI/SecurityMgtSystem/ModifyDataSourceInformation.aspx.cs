﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class ModifyDataSourceInformation : System.Web.UI.Page
    {
        public XElement oResponse = null;
        string sReturn = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (DataSourceSelectedIndex != -1)
                {
                    ViewState["DataSourceSelectedIndex"] = DataSourceSelectedIndex;
                }
                if (inputPassword.Text != "")
                {
                    ViewState["inputpassword"] = inputPassword.Text;
                    if (!String.IsNullOrEmpty(inputPassword.Attributes["value"]))
                    {
                        inputPassword.Attributes["value"] = ViewState["inputpassword"].ToString();
                    }
                    else
                    {
                        inputPassword.Attributes.Add("value", ViewState["inputpassword"].ToString());
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(inputPassword.Attributes["value"]))
                    {
                        inputPassword.Text = inputPassword.Attributes["value"].ToString();
                    }
                }
                if (inputLicensecode.Text != "")
                {
                    ViewState["inputLicensecode"] = inputLicensecode.Text;
                    if (!String.IsNullOrEmpty(inputLicensecode.Attributes["value"]))
                    {
                        inputLicensecode.Attributes["value"] = ViewState["inputLicensecode"].ToString();
                    }
                    else
                    {
                        inputLicensecode.Attributes.Add("value", ViewState["inputLicensecode"].ToString());
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(inputLicensecode.Attributes["value"]))
                    {
                        inputLicensecode.Text = inputLicensecode.Attributes["value"].ToString();
                    }
                }
                if (Wizardstep1_chkDSN.Checked)
                {
                    inputLogin.Focus();
                }
                else
                {
                    inputServerName.Focus();
                }

                if (!Page.IsPostBack)
                {
                    //Preparing XML to send to service
                    XElement oMessageElement = GetDataSourceTemplate();

                    //Modify XML
                    XElement oElement = oMessageElement.XPathSelectElement("./Document/form/groupDB/displaycolumn/control[@name='hdInvokedFrom']");
                    if (oElement != null)
                    {
                        //Pentesting Changes - XSS error fix - srajindersin - MITS 27823
                        //oElement.Value = AppHelper.GetQueryStringValue("InvokedFrom");
                        oElement.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("InvokedFrom"));
                        //END Pentesting Changes - XSS error fix - srajindersin - MITS 27823
                        hdInvokedFrom.Value = oElement.Value;
                    }
                    oElement = oMessageElement.XPathSelectElement("./Document/form/groupDB/displaycolumn/control[@name='hdEditDsnId']");
                    if (oElement != null)
                    {
                        //Pentesting Changes - XSS error fix - srajindersin - MITS 27823
                        //oElement.Value = AppHelper.GetQueryStringValue("dsnid");
                        oElement.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("dsnid"));
                        //END Pentesting Changes - XSS error fix - srajindersin - MITS 27823
                        hdEditDsnId.Value = oElement.Value;

                        //Change Wizard Title
                        if (hdEditDsnId.Value != "")
                        {
                            DataSourceWizard.HeaderText = "Edit the DataSource";
                        }

                    }


                    //Call service to get initial data
                    sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                    ErrorControl1.errorDom = sReturn;

                    //Verify if this is the first step
                    if (DataSourceWizard.ActiveStepIndex == 0)
                    {
                        oResponse = XElement.Parse(sReturn);
                        if (hdEditDsnId.Value != "")
                        {
                            BindControls(oResponse);
                        }
                        WizardStep1_BindGridToXml(sReturn, Wizardstep1_chkDSN.Checked);
                        Wizardstep1_SetHeader();
                    }
                }
                else
                {
                    switch (DataSourceWizard.ActiveStepIndex)
                    {
                        case 0:
                            Wizardstep1_SetHeader();
                            break;
                    }
                }
                if (hdInvokedFrom.Value == "SetDocPath")
                {
                    DataSourceWizard.WizardSteps.Remove(Wizardstep5);
                    DataSourceWizard.WizardSteps.Remove(Wizardstep6);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            

        }
        /// <summary>
        /// CWS request message template for fetching child nodes
        /// </summary>
        /// <returns></returns>
        private XElement GetDataSourceTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>1f5c22e4-6909-49bb-b541-061e650a5d79</Authorization> 
              <Call>
                  <Function>SecurityAdaptor.GetXMLData</Function> 
              </Call>
              <Document>
                  <form class='10pt' name='ODBCDrivers' title='ODBCDrivers'>
                      <groupDB name='dsnwizard' title='Select the ODBC Driver for the Database you want to connect to: For Microsoft Access clients, you need to use predefined ODBC Datasources.'>
                          <displaycolumn>
                              <control checked='' name='optservers' title='' type='radioDB' value='' /> 
                              <control checked='' name='optDSNs' type='radioDB' value='' />
                              <control name='chkDSN' onclick='chkDSNWizard()' title='' type='checkbox' /> 
                              <control name='txtLabel' title='Use Secure DSN Login' type='textlabel'>Use Predefined ODBC Datasource to Connect to Database</control> 
                              <control name='hdAction' type='hidden' value='' /> 
                              <control name='hdView' type='hidden' value='' /> 
                              <control name='hdInvokedFrom' type='hidden' value='' /> 
                              <control name='hdEditDsnId' type='hidden' value='' /> 
                          </displaycolumn>
                      </groupDB>
                      <EditDsnKeys>
                          <control name='inputServerName' title='Server Name' type='text' /> 
                          <control name='inputDatabaseName' title='Database Name' type='text' /> 
                          <control name='inputLogin' title='Login User Name' type='text' /> 
                          <control name='inputPassword' title='Login Password' type='text' /> 
                          <control name='inputDatasourcename' title='Login Password' type='text' /> 
                          <control name='chkAddParams' title='Login Password' type='text' /> 
                          <control name='txtAreaAddParams' title='Login Password' type='text' value='' /> 
                      </EditDsnKeys>
                 </form>
              </Document>
             </Message>
        
            ");
            return oTemplate;
        }
        /// <summary>
        /// Fill Data Source Validation Template
        /// </summary>
        private void FillDataSourceValidationTemplate(XElement oMessageElement)
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputservername");
            if (oElement != null)
            {
                oElement.Value = inputServerName.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputdatabasename");
            if (oElement != null)
            {
                oElement.Value = inputDatabaseName.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputlogin");
            if (oElement != null)
            {
                oElement.Value = inputLogin.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputpassword");
            if (oElement != null)
            {
                oElement.Value = inputPassword.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/IsDsn");
            if (oElement != null)
            {
                if (Wizardstep1_chkDSN.Checked)
                {
                    oElement.Value = "Yes";
                }
                else
                {
                    oElement.Value = "No";
                }
                
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/additionalparamschecked");
            if (oElement != null)
            {
                oElement.Value = chkAddParams.Checked.ToString();
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/additionalparams");
            if (oElement != null)
            {
                oElement.Value = txtAreaAddParams.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn");
            if (oElement != null)
            {
                XElement child = XElement.Parse(ViewState["DSNXml"].ToString());
                oElement.Add(child);
            }
            if (Wizardstep1_chkDSN.Checked)
            {
                oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn/control[@name='optDSNs']");
            }
            else
            {
                oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn/control[@name='optservers']");
            }
            if (oElement != null && oElement.Attribute("value") != null)
            {
                oElement.Attribute("value").Value = (Convert.ToInt32(ViewState["DataSourceSelectedIndex"].ToString()) + 1).ToString();
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn/control[@name='hdInvokedFrom']");
            if (oElement != null)
            {
                // Pentesting Changes - XSS error fix - srajindersin - MITS 27823
                //oElement.Value = hdInvokedFrom.Value;
                oElement.Value = AppHelper.HTMLCustomEncode(hdInvokedFrom.Value);
                //END Pentesting Changes - XSS error fix - srajindersin - MITS 27823
            }
            
        }
        /// <summary>
        /// CWS request message template for fetching child nodes
        /// </summary>
        /// <returns></returns>
        private XElement GetDataSourceValidationTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>45990435-011c-4789-ad28-8a2c95bae261</Authorization> 
                <Call>
                    <Function>SecurityAdaptor.GetXMLData</Function> 
                </Call>
                <Document>
                    <form class='10pt' name='dsnwizardprocesslogin' title='DSN Wizard'>
                    <dsnmessage iserror='' message='' mode=''>
                        <group name='UserSettings' title='Please Enter Necessary Data to Connect to Database'>
                        </group>
                    </dsnmessage>
                    <keys>
                        <DBkey>
                            <displaycolumn>
                                  <control name='hdInvokedFrom' type='hidden' value='' /> 
                                  <control name='hdEditDsnId' type='hidden' value='' /> 
                          </displaycolumn>
                        </DBkey>
                        <loginkey /> 
                        <loginscreenkeys>
                              <inputservername></inputservername> 
                              <inputdatabasename></inputdatabasename> 
                              <inputlogin></inputlogin> 
                              <inputpassword></inputpassword> 
                              <IsDsn></IsDsn> 
                              <inputfileselect /> 
                              <additionalparamschecked></additionalparamschecked> 
                              <additionalparams></additionalparams> 
                        </loginscreenkeys>
                        <resultkeys>
                            <connkey /> 
                        </resultkeys>
                    </keys>
                </form>
            </Document>
        </Message>

        
            ");
            return oTemplate;
        }
        /// <summary>
        /// Fill Data Source Validation Template
        /// </summary>
        private void FillDataSourceLicenseCheckTemplate(XElement oMessageElement)
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputservername");
            if (oElement != null)
            {
                oElement.Value = inputServerName.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputdatabasename");
            if (oElement != null)
            {
                oElement.Value = inputDatabaseName.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputlogin");
            if (oElement != null)
            {
                oElement.Value = inputLogin.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputpassword");
            if (oElement != null)
            {
                oElement.Value = inputPassword.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/IsDsn");
            if (oElement != null)
            {
                if (Wizardstep1_chkDSN.Checked)
                {
                    oElement.Value = "Yes";
                }
                else
                {
                    oElement.Value = "No";
                }

            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/additionalparamschecked");
            if (oElement != null)
            {
                oElement.Value = chkAddParams.Checked.ToString();
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/additionalparams");
            if (oElement != null)
            {
                oElement.Value = txtAreaAddParams.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn");
            if (oElement != null)
            {
                XElement child = XElement.Parse(ViewState["DSNXml"].ToString());
                oElement.Add(child);
            }
            if (Wizardstep1_chkDSN.Checked)
            {
                oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn/control[@name='optDSNs']");
            }
            else
            {
                oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn/control[@name='optservers']");
            }
            if (oElement != null && oElement.Attribute("value") != null)
            {
                oElement.Attribute("value").Value = (Convert.ToInt32(ViewState["DataSourceSelectedIndex"].ToString()) + 1).ToString();
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/licenseKey/displaycolumn/control[@name='inputDatasourcename']");
            if (oElement != null)
            {
                oElement.Value = inputDatasourcename.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/licenseKey/displaycolumn/control[@name='inputLicensecode']");
            if (oElement != null)
            {
                oElement.Value = inputLicensecode.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/connkey");
            if (oElement != null)
            {
                oElement.Value = ViewState["connectionstring"].ToString();
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/EditDsnId");
            if (oElement != null)
            {
                //Pentesting Changes - XSS error fix - srajindersin - MITS 27823
                //oElement.Value = hdEditDsnId.Value;
                oElement.Value = AppHelper.HTMLCustomEncode(hdEditDsnId.Value);
                //END Pentesting Changes - XSS error fix - srajindersin - MITS 27823
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn/control[@name='hdInvokedFrom']");
            if (oElement != null)
            {
                //Pentesting Changes - XSS error fix - srajindersin - MITS 27823
                //oElement.Value = hdInvokedFrom.Value;
                oElement.Value = AppHelper.HTMLCustomEncode(hdInvokedFrom.Value);
                //END Pentesting Changes - XSS error fix - srajindersin - MITS 27823
            }
            

        }
        /// <summary>
        /// CWS request message template for fetching child nodes
        /// </summary>
        /// <returns></returns>
        private XElement GetDataSourceLicenseCheckTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization /> 
                 <Call>
                  <Function>SecurityAdaptor.GetXMLData</Function> 
                 </Call>
                 <Document>
                    <form class='10pt' name='dsnwizardprocesslicense' title='DSN Wizard'>
                    <dsnmessage iserror='No' message='Database Connection Wizard will now try to connect and validate the database you have selected.Please click Next > to start the connection and validation process.' mode='closedsnwizard'>
                        <group name='UserSettings' title='Please Enter Necessary Data to Connect to Database'>
                         <displaycolumn>
                              <control name='hdAction' type='hidden' value='' /> 
                              <control name='hdIsDsn' type='hidden' value=''></control> 
                              <control name='hdNewDBId' type='hidden' value='' /> 
                              <control name='hdEntityType' type='hidden' value='' /> 
                          </displaycolumn>
                        </group>
                    </dsnmessage>
                    <keys>
                        <licenseKey>
                             <displaycolumn>
                                  <control custom='true' maxlength='32' name='txtDatasourcenameDB' title='' type='textlabel'>Please enter unique name for this database connection:</control> 
                                  <control custom='true' maxlength='32' name='inputDatasourcename' onkeyup='CheckDBName(this)' readOnly='false' title='' type='text'></control> 
                                  <control custom='true' maxlength='50' name='inputLicensecode' title='' type='text'></control> 
                                  <control name='hdOrigEditDbName' type='hidden' value='' /> 
                                  <control name='hdUserLoginName' type='hidden' value=''></control> 
                              </displaycolumn>
                        </licenseKey>
                        <DBkey>
                            <displaycolumn>
                              <control name='hdAction' type='hidden' value='' /> 
                              <control name='hdView' type='hidden' value=''>server</control> 
                              <control name='hdInvokedFrom' type='hidden' value='' /> 
                              <control name='hdEditDsnId' type='hidden' value='' /> 
                            </displaycolumn>
                        </DBkey>
                        <loginkey /> 
                        <loginscreenkeys>
                              <inputservername></inputservername> 
                              <inputdatabasename></inputdatabasename> 
                              <inputlogin></inputlogin> 
                              <inputpassword></inputpassword> 
                              <IsDsn></IsDsn> 
                              <EditDsnId /> 
                              <additionalparamschecked /> 
                              <additionalparams /> 
                        </loginscreenkeys>
                        <connkey></connkey> 
                        <resultkeys /> 
                    </keys>
                </form>
        </Document>
  </Message>

        
            ");
            return oTemplate;
        }
        protected void RefreshStep1(object sender, EventArgs e)
        {
            try
            {
                ViewState["DataSourceSelectedIndex"] = "-1";
                Button btn = (Button)DataSourceWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StartNextButton");
                btn.Enabled = false;
                if (Wizardstep1_chkDSN.Checked)
                {
                    Wizardstep1_hdAction.Value = "PredefinedDSN";
                }
                else
                    Wizardstep1_hdAction.Value = "ODBCDrivers";

                //Preparing XML to send to service
                XElement oMessageElement = GetDataSourceTemplate();

                XElement oFormNameElement = oMessageElement.XPathSelectElement("./Document/form");
                if (oFormNameElement != null && oFormNameElement.Attribute("name") != null)
                {
                    oFormNameElement.Attribute("name").Value = Wizardstep1_hdAction.Value;
                }


                //Call service to get initial data
                sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                ErrorControl1.errorDom = sReturn;

                //Verify if this is the first step
                if (DataSourceWizard.ActiveStepIndex == 0)
                {
                    WizardStep1_BindGridToXml(sReturn, Wizardstep1_chkDSN.Checked);

                    oResponse = XElement.Parse(sReturn);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void grdLookUp_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int iDataSourceSelectedIndex = DataSourceSelectedIndex;
                if (iDataSourceSelectedIndex == -1 && ViewState["DataSourceSelectedIndex"] != null)
                {
                    iDataSourceSelectedIndex = Convert.ToInt32(ViewState["DataSourceSelectedIndex"]);
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                    // Grab a reference to the Literal control
                    Literal output = (Literal)e.Row.FindControl("RadioButtonMarkup");
                    // Output the markup except for the "checked" attribute 
                    output.Text = string.Format(@"<input type='radio' onclick='enableNext();' name='DataSourceGroup' " + @"id='RowSelector{0}' value='{0}' ", e.Row.RowIndex);

                    // See if we need to add the "checked" attribute
                    if (iDataSourceSelectedIndex == e.Row.RowIndex)
                    {
                        output.Text += @" checked='checked'";
                        Button btn = (Button)DataSourceWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StartNextButton");
                        btn.Enabled = true;

                    }

                    // Add the closing tag 
                    output.Text += " />";
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                    e.Row.CssClass = "colheader3";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        private void WizardStep1_BindGridToXml(string sReturnValue , bool p_bPreDefinedDSN)
        {
            DataRow drLookUpRow = null;
            DataTable dtLookUp = new DataTable();
            
            XElement objDataSource = XElement.Parse(sReturnValue);

            if (p_bPreDefinedDSN)
            {
                dtLookUp.Columns.Add(new DataColumn("Name"));
                dtLookUp.Columns.Add(new DataColumn("Driver"));
                dtLookUp.Columns.Add("pid");
            }
            else
            {
                dtLookUp.Columns.Add(new DataColumn("Name"));
                dtLookUp.Columns.Add(new DataColumn("Version"));
                dtLookUp.Columns.Add(new DataColumn("Company"));
                dtLookUp.Columns.Add("pid");
            }

            var objRowXml = from rows in objDataSource.XPathSelectElement("./Document/form/groupDB").Descendants("optionDB")

                            select rows;

            string[] arrPid = new string[1];

            string sDescendant = string.Empty;

            if (p_bPreDefinedDSN)
            {
                foreach (XElement row in objRowXml)
                {
                    arrPid[0] = "pid";
                    grdLookUp.DataKeyNames = arrPid;

                    drLookUpRow = dtLookUp.NewRow();
                    drLookUpRow["Name"] = row.Attribute("name").Value;
                    drLookUpRow["Driver"] = row.Attribute("company").Value;
                    drLookUpRow["pid"] = row.Attribute("value").Value;
                    dtLookUp.Rows.Add(drLookUpRow);
                }
                ViewState["DSNXml"] = objDataSource.XPathSelectElement("./Document/form/groupDB/displaycolumn/control[@name='optDSNs']").ToString();
            }
            else
            {
                foreach (XElement row in objRowXml)
                {
                    arrPid[0] = "pid";
                    grdLookUp.DataKeyNames = arrPid;

                    drLookUpRow = dtLookUp.NewRow();
                    drLookUpRow["Name"] = row.Attribute("name").Value;
                    drLookUpRow["Version"] = row.Attribute("version").Value;
                    drLookUpRow["Company"] = row.Attribute("company").Value;
                    drLookUpRow["pid"] = row.Attribute("value").Value;
                    dtLookUp.Rows.Add(drLookUpRow);
                }
                ViewState["DSNXml"] = objDataSource.XPathSelectElement("./Document/form/groupDB/displaycolumn/control[@name='optservers']").ToString();
                
            }

            // Bind DataTable to GridView.
            grdLookUp.Visible = true;
            grdLookUp.DataSource = dtLookUp;
            grdLookUp.DataBind();
            
        }
        private int DataSourceSelectedIndex 
        {
            get 
            {
                if (string.IsNullOrEmpty(Request.Form["DataSourceGroup"])) 
                    return -1; 
                else
                    return Convert.ToInt32(Request.Form["DataSourceGroup"]); 
            }
        }
        private void Wizardstep1_SetHeader()
        {
            
            if (Wizardstep1_chkDSN.Checked)
                Wizardstep1_Header.Text = "Select the ODBC Data Source to Use to Connect to the Database:";
            else
                Wizardstep1_Header.Text = "Select the ODBC Driver for the Database you want to connect to: For Microsoft Access clients, you need to use predefined ODBC Datasources.";
        }
        protected void onNextClick(object sender, EventArgs e)
        {
            try
            {
                switch (DataSourceWizard.ActiveStepIndex)
                {
                    case 0:
                        if (Wizardstep1_chkDSN.Checked)
                        {
                            div_inputServerName.Visible = false;
                            div_inputDatabaseName.Visible = false;
                            div_chkAddParams.Visible = false;
                            inputLogin.Focus();
                        }
                        else
                        {
                            div_inputServerName.Visible = true;
                            div_inputDatabaseName.Visible = true;
                            div_chkAddParams.Visible = true;
                            inputServerName.Focus();
                        }
                        //Changing Image
                        ((HtmlImage)DataSourceWizard.FindControl("SideBarContainer").FindControl("dsnimage")).Src = "../../images/dsn1.gif";
                        break;

                    case 1:
                        //Changing Image
                        ((HtmlImage)DataSourceWizard.FindControl("SideBarContainer").FindControl("dsnimage")).Src = "../../images/dsn1.gif";
                        break;
                    

                    case 2:
                        //Preparing XML to send to service
                        XElement oMessageElement = GetDataSourceValidationTemplate();

                        //Modify XML
                        FillDataSourceValidationTemplate(oMessageElement);

                        //Call service to get initial data
                        sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                          ErrorControl1.errorDom = sReturn;
                        
                        XElement oResponse = XElement.Parse(sReturn);
                        XElement oEle = oResponse.XPathSelectElement("./Document/form/dsnmessage");
                        if (oEle != null && oEle.Attribute("iserror") != null)
                        {
                            wizardstep4_iserror.Value = oEle.Attribute("iserror").Value;
                            wizardstep4_message.Text = ErrorHelper.UpdateErrorMessage(oEle.Attribute("message").Value);
                            if (wizardstep4_iserror.Value.ToLower() == "yes")
                            {
                                //Changing Image
                                ((HtmlImage)DataSourceWizard.FindControl("SideBarContainer").FindControl("dsnimage")).Src = "../../images/dsn1.gif";
                                wizardstep4_message.CssClass = "errtext1";
                            }
                            else
                            {
                                wizardstep4_message.CssClass = "";
                                //Changing Image
                                ((HtmlImage)DataSourceWizard.FindControl("SideBarContainer").FindControl("dsnimage")).Src = "../../images/dsn2.gif";

                                //No error:store connection string in ViewState
                                oEle = oResponse.XPathSelectElement("./Document/form/keys/resultkeys/connkey");
                                if (oEle != null)
                                {
                                    ViewState["connectionstring"] = oEle.Value;
                                    connStr.Value = oEle.Value;
                                }

                            }
                        }

                        break;

                    case 3:
                        Button btn = (Button)DataSourceWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepNextButton");
                        btn.Enabled = true;
                        break;
                    case 4:
                        //Preparing XML to send to service
                        oMessageElement = GetDataSourceLicenseCheckTemplate();

                        //Modify XML
                        FillDataSourceLicenseCheckTemplate(oMessageElement);

                        //Call service to get initial data
                        sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                        ErrorControl1.errorDom = sReturn;

                        oResponse = XElement.Parse(sReturn);
                        oEle = oResponse.XPathSelectElement("./Document/form/dsnmessage");
                        if (oEle != null && oEle.Attribute("iserror") != null)
                        {
                            wizardstep6_iserror.Value = oEle.Attribute("iserror").Value;
                            wizardstep6_message.Text = ErrorHelper.UpdateErrorMessage(oEle.Attribute("message").Value);
                            if (wizardstep6_iserror.Value.ToLower() == "yes")
                            {
                                wizardstep6_message.CssClass = "errtext1";
                                btn = (Button)DataSourceWizard.FindControl("FinishNavigationTemplateContainerID").FindControl("FinishButton");
                                btn.Enabled = false;
                                DataSourceWizard.WizardSteps[5].Title = "Security Code Error";
                            }
                            else
                            {
                                wizardstep6_message.CssClass = "";

                                oEle = oResponse.XPathSelectElement("./Document/form/dsnmessage/group/displaycolumn/control[@name='hdNewDBId']");
                                if (oEle != null)
                                {
                                    hdNewDBId.Value = oEle.Value;
                                }
                                oEle = oResponse.XPathSelectElement("./Document/form/dsnmessage/group/displaycolumn/control[@name='hdEntityType']");
                                if (oEle != null)
                                {
                                    hdEntityType.Value = oEle.Value;
                                }

                            }
                        }


                        break;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void onPreviousClick(object sender, EventArgs e)
        {
            if (DataSourceWizard.ActiveStepIndex <= 3)
            {
                //Changing Image
                ((HtmlImage)DataSourceWizard.FindControl("SideBarContainer").FindControl("dsnimage")).Src = "../../images/dsn1.gif";
            }
        }

        private void BindControls(XElement oResponseElement)
        {
            XElement oElement = oResponseElement.XPathSelectElement("./Document/form/EditDsnKeys/control[@name='inputServerName']");
            if (oElement != null)
            {
                inputServerName.Text = oElement.Value;
            }

            oElement = oResponseElement.XPathSelectElement("./Document/form/EditDsnKeys/control[@name='inputDatabaseName']");
            if (oElement != null)
            {
                inputDatabaseName.Text = oElement.Value;
            }
            oElement = oResponseElement.XPathSelectElement("./Document/form/EditDsnKeys/control[@name='inputLogin']");
            if (oElement != null)
            {
                inputLogin.Text = oElement.Value;
            }

            oElement = oResponseElement.XPathSelectElement("./Document/form/EditDsnKeys/control[@name='inputPassword']");
            if (oElement != null)
            {
                inputPassword.Text = oElement.Value;
                inputPassword.Attributes.Add("value", oElement.Value);
            }
            oElement = oResponseElement.XPathSelectElement("./Document/form/EditDsnKeys/control[@name='inputDatasourcename']");
            if (oElement != null)
            {
                inputDatasourcename.Text = oElement.Value;
            }
            oElement = oResponseElement.XPathSelectElement("./Document/form/EditDsnKeys/control[@name='inputPassword']");
            if (oElement != null)
            {
                inputPassword.Text = oElement.Value;
            }
            oElement = oResponseElement.XPathSelectElement("./Document/form/EditDsnKeys/control[@name='chkAddParams']");
            if (oElement != null)
            {
                chkAddParams.Checked = Conversion.ConvertStrToBool(oElement.Value);
            }
            oElement = oResponseElement.XPathSelectElement("./Document/form/EditDsnKeys/control[@name='txtAreaAddParams']");
            if (oElement != null && oElement.Attribute("value")!=null)
            {
                txtAreaAddParams.Text = oElement.Attribute("value").Value;
            }

            oElement = oResponseElement.XPathSelectElement("./Document/form/groupDB/displaycolumn/control[@name='optservers']");
            if (oElement != null && oElement.Attribute("value")!= null)
            {
                ViewState["DataSourceSelectedIndex"] = Conversion.ConvertStrToInteger(oElement.Attribute("value").Value) - 1;
            }
        }
        
    }
}
