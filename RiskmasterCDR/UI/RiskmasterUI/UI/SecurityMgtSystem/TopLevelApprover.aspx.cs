﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.AppHelpers;//abansal: MITS 14667
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class TopLevelApprover : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sReturn = "";
                if (!Page.IsPostBack)
                {
                    NonFDMCWSPageLoad("TopLevelApprovalAdaptor.Get");
                    HidingDivs();//Mgaba2: R8:SuperVisory Approval                 
                }
                else
                {
                    switch (SysCmd.Text)
                    {
                        case "5": CallCWS("TopLevelApprovalAdaptor.Save", null, out sReturn, true, false);



                            break;

                        default: break;
                    }

                }
                //abansal: MITS 14667 Starts
                DatabindingHelper.UpdateTabFocus(this);
                //abansal: MITS 14667 Ends
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }


        }
        /// <summary>
        /// Mgaba2: R8:SuperVisory Approval
        /// In case Carrier Claim is ON or LOB is deselected in General System paraeters,corresponding LOB should be invisible
        /// </summary>
        private void HidingDivs()
        {            
            try
            {
                String[] sLobs = null;
                
                if (hdnLOBsDeselectedInGenSysParms.Text != "")
                {
                    sLobs = hdnLOBsDeselectedInGenSysParms.Text.Split("*".ToCharArray()); 
                    foreach (string sLob in sLobs)
                    {
                        switch (sLob)
                        {
                            case "GC":
                                TABSGeneralClaim.Attributes["style"] = "display:none;";
                                TBSPGeneralClaim.Attributes["style"] = "display:none;";
                                break;
                            case "VA":
                                TABSVehicleAccident.Attributes["style"] = "display:none;";
                                TBSPVehicleAccident.Attributes["style"] = "display:none;";
                                break;
                            case "WC":
                                TABSWorkersCompensation.Attributes["style"] = "display:none;";
                                TBSPWorkersCompensation.Attributes["style"] = "display:none;";
                                break;
                            case "DI":
                                TABSNonOccupational.Attributes["style"] = "display:none;";
                                TBSPNonOccupational.Attributes["style"] = "display:none;";
                                break;
                            case "PC":
                                TABSPropertyClaim.Attributes["style"] = "display:none;";
                                TBSPPropertyClaim.Attributes["style"] = "display:none;";
                                break;
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
