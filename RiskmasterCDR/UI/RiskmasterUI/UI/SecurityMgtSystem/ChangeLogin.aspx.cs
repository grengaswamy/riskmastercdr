﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class ChangeLogin : NonFDMBasePageCWS
    {
        //abansal23: MITS 15117 on 04/16/2009 Starts
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();
        //abansal23: MITS 15117 on 04/16/2009 Ends
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturn = false;
            XElement oMessageElement = null;
            try
            {
                hdUserLoginName.Text = AppHelper.GetUserLoginName();
                if (!Page.IsPostBack)
                {
                    string sReturn = "";
                    messagewin.Visible = false;
                    //Preparing XML to send to service
                    oMessageElement = GetAdminUserCheckMessageTemplate();

                    XElement oLoginNameElement = oMessageElement.XPathSelectElement("./Document/UI/Session/LoginName");
                    if (oLoginNameElement != null)
                    {
                        oLoginNameElement.Value = hdUserLoginName.Text;
                    }

                    //Call Service
                    sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

                    BindData2ErrorControl(sReturn);

                    if (HttpContext.Current.Items["Error"] != null)
                    {
                        mainwin.Visible = false;
                    }
                    else
                    {
                        mainwin.Visible = true;
                        newlogin.Text = hdUserLoginName.Text;
                    }

                }
                else
                {
                    switch (hdAction.Text)
                    {
                        //abansal23: MITS 15117 on 04/16/2009 Starts
                        case "OK": CallCWSFunction("RMWinSecurityAdaptor.SaveUserLoginData",out sCWSresponse);
                                  XmlDoc.LoadXml(sCWSresponse);
                                  string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                                  if (sMsgStatus == "Success")
                                  {
                                      messagewin.Visible = true;
                                      mainwin.Visible = false;
                                  }
                            break;
                        //abansal23: MITS 15117 on 04/16/2009 Ends
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        /// <summary>
        /// CWS request message template for fetching child nodes
        /// </summary>
        /// <returns></returns>
        private XElement GetAdminUserCheckMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>1f5c22e4-6909-49bb-b541-061e650a5d79</Authorization> 
            <Call>
              <Function>SMSAccessManagerAdaptor.IsSMSAdminUser</Function> 
              </Call>
            <Document>
                <UI>
                    <Session>
                        <LoginName></LoginName> 
                    </Session>                    
                </UI>
             </Document>
             </Message>
        
            ");
            return oTemplate;
        }
    }
}
