﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModifyDataSourceInformation.aspx.cs" Inherits="Riskmaster.UI.SecurityMgtSystem.ModifyDataSourceInformation" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Data Source Wizard</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript">

    function onUpdating(){
        // get the update progress div
        var updateProgressDiv = $get('updateProgressDiv'); 
        // make it visible
        updateProgressDiv.style.display = '';

        //  get the wizard element        
        var wizard = $get('<%# this.DataSourceWizard.ClientID %>');
        
        // get the bounds of both the wizard and the progress div
        var wizardBounds = Sys.UI.DomElement.getBounds(wizard);
        var updateProgressDivBounds = Sys.UI.DomElement.getBounds(updateProgressDiv);
        
        //	do the math to figure out where to position the element (the center of the gridview)
        var x = wizardBounds.x + Math.round(wizardBounds.width / 2) - Math.round(updateProgressDivBounds.width / 2);
        var y = wizardBounds.y + Math.round(wizardBounds.height / 2) - Math.round(updateProgressDivBounds.height / 2);
        
        //	set the progress element to this position
        Sys.UI.DomElement.setLocation (updateProgressDiv, x, y);        
    }

    function onUpdated() {
        // get the update progress div
        var updateProgressDiv = $get('updateProgressDiv'); 
        // make it invisible
        updateProgressDiv.style.display = 'none';
    }

    </script>    
 
</head>
<body>
    <form id="frmData" name="DSNWizard" runat="server" method="post">
    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
    
   
   
    <div>
                <asp:UpdatePanel ID="upd" runat="server">
    <ContentTemplate>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:HiddenField runat="server" ID="hdEditDsnId" />
    <asp:HiddenField runat="server" ID="hdInvokedFrom" />
    <asp:HiddenField runat="server" ID="connStr" />
    <asp:wizard runat="server" id="DataSourceWizard" 
        Font-names="verdana" 
        ForeColor="Navy" HeaderText="Add New Data Source"
        Style="border:outset 1px black" 
        ActiveStepIndex="0" DisplayCancelButton="True" Width="100%" Height="80%" HeaderStyle-Width="400" 

  SideBarStyle-Width="70"
  SideBarStyle-VerticalAlign="Top"

  StepStyle-Width="500"
  StepStyle-Height="300"
  StepStyle-VerticalAlign="Top"

  NavigationStyle-Width="300"
  NavigationStyle-HorizontalAlign="Left"
  NavigationButtonStyle-Width="70"
>
        <stepstyle backcolor="White" borderwidth="1" 
            borderstyle="Outset" /> 
        
       <sidebartemplate>
            <div>
            <img id="dsnimage" runat="server" src="../../images/dsn1.gif" alt=""/>
            <asp:datalist runat="Server" id="SideBarList">
                <ItemTemplate>
                   <asp:linkbutton runat="server" id="SideBarButton" OnClientClick="return false"/>
               </ItemTemplate>
              <SelectedItemTemplate>
                   <asp:LinkButton ID="SideBarButton" runat="server" Font-Bold="true" ForeColor="#465d7e" OnClientClick="return false">LinkButton</asp:LinkButton>
              </SelectedItemTemplate>

            </asp:datalist> 
            </div>
        </sidebartemplate>
    
        <StartNavigationTemplate>
            <asp:Button ID="StartNextButton" runat="server" BackColor="LightGray" 
                BorderStyle="Solid" BorderWidth="1px" CommandName="MoveNext" Text="Next" Enabled="false" OnClick="onNextClick" 
                Width="80px" />
            <asp:Button ID="CancelButton" runat="server" BackColor="LightGray" 
                BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" 
                CommandName="Cancel" Text="Cancel" Width="80px" OnClientClick="return SetWizardAction(this,'ODBCDrivers',event);"/>
        </StartNavigationTemplate>
    
        <wizardsteps>
        <asp:wizardstep ID="Wizardstep1" runat="server" steptype="Start" 
                title="ODBC Driver Selection">
            <div style="height:100%;overflow:auto">
                <asp:HiddenField id="step1" runat="server" />
                <asp:Label CssClass="ctrlgroup" ID="Wizardstep1_Header" runat="server" Width="100%"></asp:Label>
                <asp:HiddenField ID="Wizardstep1_hdAction" runat="server" />
                <asp:GridView ID="grdLookUp" runat="server" OnRowCreated="grdLookUp_RowCreated" CellPadding="4">
                    <Columns>
                        <asp:TemplateField>  
                            <ItemTemplate>
                              <asp:Literal ID="radiobuttonmarkup" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns> 
                </asp:GridView>
                <br />
                <asp:CheckBox runat="server" ID="Wizardstep1_chkDSN" 
                    Text="Use Predefined ODBC Datasource to Connect to Database" Visible="false"
                    OnCheckedChanged="RefreshStep1" onclick="ClearRadioSelection();" AutoPostBack="True" />
                
            </div>
        </asp:wizardstep>
         <asp:wizardstep ID="Wizardstep2" runat="server" steptype="auto" 
                title="Connection Information">
             <div style="height:100%;width:100%">
             <asp:HiddenField id="step2" runat="server" />
            <asp:Label CssClass="ctrlgroup" runat="server" Width="100%" Text="Please Enter Necessary Data to Connect to Database:"></asp:Label>
         
             <table border="0" width="100%" class="singleborder" cellspacing="0" cellpadding="0" align="left">
       <div id="div_inputServerName" runat="server">
       <tr>
        <td>Server Name</td>
        <td><asp:textbox runat="server" size="30" id="inputServerName" tabindex="1" onchange="setDataChanged(true);" onkeyup="OnKeyupDsnWizardFields('ServerLogin')"/></td>
       </tr>
       </div>
       <br />
       <div id="div_inputDatabaseName" runat="server">
       <tr>
        <td>Database Name</td>
        <td><asp:textbox runat="server" size="30" id="inputDatabaseName" tabindex="2" onchange="setDataChanged(true);"/></td>
       </tr>
       </div>
       <br />
       <div id="div_inputLogin" runat="server">
       <tr>
        <td>Login User Name</td>
        <td><asp:textbox runat="server" size="30" id="inputLogin" tabindex="3" onchange="setDataChanged(true);"/></td>
       </tr>
       </div>
       <br />
        <div id="div_inputPassword" runat="server">
       <tr>
        <td>Login Password</td>
        <td><asp:textbox runat="server" TextMode="Password" size="30" tabindex="4" onchange="setDataChanged(true);" id="inputPassword" autocomplete="off"/></td>
       </tr>
       </div>
       <div runat="server" id="div_chkAddParams">
       <tr>
         <td><asp:checkbox runat="server" id="chkAddParams" tabindex="5" onclick="MakeAddParamsVisible(this);" Text="Add Additional Parameters"/></td>
       </tr>
       </div>
       <tr>
         <td><asp:TextBox TextMode="MultiLine" Columns="20" tabindex="6" Rows="4" id="txtAreaAddParams" runat="server" onchange="setDataChanged(true);"/></td>
       </tr> 
       
            <asp:HiddenField ID="Wizardstep2_hdAction" runat="server" />
       
       
      </table>
    
            </div>
        </asp:wizardstep>
         <asp:wizardstep ID="Wizardstep3" runat="server" steptype="auto" 
                title="Preparing for Validation">
           <asp:HiddenField id="step3" runat="server" />
            <div style="height:100%">
               <table width="100%" cellspacing="0" cellpadding="0" align="left">
                <br /><br /><br /><br /><br /><br />
                <asp:Label runat="server" Width="100%" Text="Database Connection Wizard will now try to connect and validate the database you have selected.Please click Next to start the connection and validation process." ID="lblValidate"></asp:Label> 
                <br /><br /><br /><br /><br /><br /><br />
            </table>
            </div>
             
        </asp:wizardstep>
        <asp:wizardstep ID="Wizardstep4" runat="server" steptype="auto" 
                title="Validation Results">
            <div style="height:100%">
               <table width="100%" cellspacing="0" cellpadding="0" align="left">
                <asp:HiddenField id="step4" runat="server" />
               <asp:HiddenField ID="wizardstep4_iserror" runat="server" />
               <br /><br /><br /><br /><br /><br />
               <asp:Label runat="server" Width="100%" ID="wizardstep4_message"></asp:Label> 
               <br /><br /><br /><br />
               </table>
            </div>
        </asp:wizardstep>
        <asp:wizardstep ID="Wizardstep5" runat="server" steptype="auto"
                title="Verifying Security Code">
            <div style="height:100%">
            
               <asp:HiddenField id="step5" runat="server" />
               <asp:HiddenField ID="olddatasourcename" runat="server" />
               <div runat="server" id="mainwin" style="vertical-align:middle">
                <table border="0" width="100%" class="singleborder" cellspacing="0" cellpadding="0">
                   <br /><br /><br /><br />
                   <tr>
                    <td>Please enter unique name for this database connection:<br/><asp:textbox runat="server" size="30" id="inputDatasourcename" onblur="return ValidateDataSource(this);"/></td>
                   </tr>
                   <tr>
                     <td>
                        <br /><br />
                    </td>
                   </tr>
                   
                   <tr>
                    <td>Enter the code provided by the product support to activate licenses:<br/><asp:textbox runat="server" TextMode="Password" size="30" id="inputLicensecode" autocomplete="off"/></td>
                   </tr>
                   
      </table>
      </div>
     
            </div>
        </asp:wizardstep>
         <asp:wizardstep ID="Wizardstep6" runat="server" steptype="finish"
                title="Finalizing">
            <div style="height:100%">
                <asp:HiddenField id="step6" runat="server" />
                 <asp:HiddenField ID="hdNewDBId" runat="server" />
               <asp:HiddenField ID="hdEntityType" runat="server" />
                <table width="100%" cellspacing="0" cellpadding="0" align="left">
                
               <asp:HiddenField ID="wizardstep6_iserror" runat="server" />
               <br /><br /><br /><br /><br /><br />
               <asp:Label runat="server" Width="100%" ID="wizardstep6_message"></asp:Label> 
               <br /><br /><br /><br />
               </table>
                </div>
                </asp:wizardstep>
        </wizardsteps>

        <navigationbuttonstyle borderwidth="1" width="80"
            borderstyle="Solid" backcolor="lightgray" /> 
        <FinishNavigationTemplate>
            <asp:Button ID="FinishPreviousButton" runat="server" BackColor="LightGray" 
                BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" 
                CommandName="MovePrevious" Text="Previous" Width="80px" OnClick="onPreviousClick"/>
            <asp:Button ID="FinishButton" runat="server" BackColor="LightGray" 
                BorderStyle="Solid" BorderWidth="1px" CommandName="MoveComplete" Text="Finish" OnClick="onNextClick" OnClientClick="return VerifyFinish();"
                Width="80px" />
            
        </FinishNavigationTemplate>
        <headerstyle horizontalalign="Center" font-bold="true" 
            font-size="120%" /> 
        <SideBarStyle BackColor="White" Font-Size="0.9em" VerticalAlign="Top" Width="25%" />
                        <SideBarButtonStyle ForeColor="white" />
        
        <StepNavigationTemplate>
            <asp:Button ID="StepPreviousButton" runat="server" BackColor="LightGray" 
                BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" 
                CommandName="MovePrevious" Text="Previous" Width="80px" OnClick="onPreviousClick"/>
            <asp:Button ID="StepNextButton" runat="server" BackColor="LightGray" 
                BorderStyle="Solid" BorderWidth="1px" CommandName="MoveNext" Text="Next" Enabled="false" OnClick="onNextClick" 
                Width="80px" />
            <asp:Button ID="CancelButton" runat="server" BackColor="LightGray" 
                BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" 
                CommandName="Cancel" Text="Cancel" Width="80px" OnClientClick="return SetWizardAction(this,'ODBCDrivers',event);"/>
        </StepNavigationTemplate>
        
    </asp:wizard>
    
    </ContentTemplate>
    </asp:UpdatePanel>
   </div>
     <ajaxToolkit:UpdatePanelAnimationExtender ID="upae" BehaviorID="animation" runat="server" TargetControlID="upd">
                <Animations>
                    <OnUpdating>
                        <Parallel duration="0">
                            <%-- place the update progress div over the wizard control --%>
                            <ScriptAction Script="onUpdating();" />  
                            <%-- fade-out the wizard --%>
                            <FadeOut minimumOpacity=".5" />
                         </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel duration="0">
                            <%-- fade back in the wizard --%>
                            <FadeIn minimumOpacity=".5" />
                            <%--find the update progress div and place it over the wizard control--%>
                            <ScriptAction Script="onUpdated();" /> 
                        </Parallel> 
                    </OnUpdated>
                </Animations>
            </ajaxToolkit:UpdatePanelAnimationExtender>
            <div id="updateProgressDiv" style="display: none; height: 40px; width: 40px">
                <img src="../../Images/loading1.gif" />
            </div>
             <uc:PleaseWaitDialog runat="server" ID="plswait" CustomMessage="Loading" />     
    </form>
</body>
</html>
