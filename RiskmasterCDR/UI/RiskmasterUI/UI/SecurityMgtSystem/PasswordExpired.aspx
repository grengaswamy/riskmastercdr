﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PasswordExpired.aspx.cs"
    Inherits="Riskmaster.UI.UI.SecurityMgtSystem.PasswordExpired" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

		<script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div runat="server">
        <table width="100%">
            <tr>
                <td class="msgheader" bgcolor="#D5CDA4">
                    Password expiration:
                </td>
            </tr>
        </table>
        <br/>
        <br/>
        <br/>
        <table width="100%">
            <tr>
                <td align="center"> 
                    <a class="LightBold" href="../SecurityMgtSystem/ChangePassword.aspx?invokedfrom=pwdexpire" onclick="">Your Password is expired, please change it now.</a>
					<%--changed path of href to point to SecurityMgtSystem: atavaragiri MITS 28446--%>
					

					
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
