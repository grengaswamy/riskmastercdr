﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class Datasource : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                loginname.Text = AppHelper.GetUserLoginName();
                txts3bucketname.Text = AppHelper.ClientId.ToString();
                if (!Page.IsPostBack)
                {
                    chkModuleSec.InputAttributes.Add("onchange", "return ApplyBool(this);");
                    string sDsnid = AppHelper.GetQueryStringValue("dsnid");
                    dsnid.Text = sDsnid;
                    //MITS 22793 : Password visible on viewing source
                    systempassword.Attributes.Add("tempdsnpwd", "");
                    NonFDMCWSPageLoad("SecurityAdaptor.GetXMLData");
                    systempassword.Attributes.Remove("tempdsnpwd");
                    //MITS 22793 : Password visible on viewing source
                    ViewState["dsnpwd"] = systempassword.Text;
                }
                else
                {
                    //Called when toolbar Save is pressed
                    switch (hdSave.Text.ToLower())
                    {
                        case "save":    systempassword.Attributes.Add("tempdsnpwd", ViewState["dsnpwd"].ToString());
                                        CallCWSFunction("DataSourcesAdaptor.Save");
                                        ViewState["dsnpwd"] = systempassword.Text;
                                        systempassword.Text = systempassword.Attributes["value"];
                                        systempassword.Attributes.Remove("tempdsnpwd");
                            break;

                        case "refresh": password.Attributes.Add("tempdsnpwd", ViewState["dsnpwd"].ToString());
                                        ViewState["dsnpwd"] = password.Text;
                                        NonFDMCWSPageLoad("SecurityAdaptor.GetXMLData");
                                        ViewState["dsnpwd"] = password.Text;
                                        password.Text = password.Attributes["value"];
                                        password.Attributes.Remove("tempdsnpwd");
                            break;

                        default: break;
                    }

                }
                if (optservers2.Checked)
                {
                    btdocpath.Enabled = true;
                }
                else
                {
                    btdocpath.Enabled = false;
                }
                if (optS3server.Checked)
                {
                    if (string.IsNullOrEmpty(ipdocpath.Text)) ipdocpath.Text = "Bucket" + AppHelper.ClientId.ToString();
                }
                if (AppHelper.ClientId == 0)//For base functionality --- S3 Bucket will not work,It will work only for cloud.
                {
                    optS3server.Visible = false;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        
    }
}
