﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class DomainAuthenticationModify : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hdUserLoginName.Text = AppHelper.GetUserLoginName();
                if (!IsPostBack)
                {
                    string sDomainName = AppHelper.GetQueryStringValue("selectedid");
                    string sDomainEnable = AppHelper.GetQueryStringValue("enabled");

                    txtdomainName.Text = sDomainName;
                    if (sDomainEnable.ToLower().ToString() == "enabled")
                    {
                        chkDomain.Checked = true;
                    }
                }
                else
                {
                    switch (hdnAction.Text.ToLower())
                    {
                        case "ok": CallCWSFunction("RMWinSecurityAdaptor.AddEditDomain");
                            break;

                        default: break;
                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
    }
}
