﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangeLogin.aspx.cs" Inherits="Riskmaster.UI.SecurityMgtSystem.ChangeLogin" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Change System Login Parameters</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>
</head>
<body onload="pageLoaded();">
  <form id="frmData" name="user" method="post" runat="server">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
   <table border="0" width="100%" cellspacing="0" cellpadding="0">
   <asp:TextBox style="display:none" id="hdAction" runat="server"></asp:TextBox>
    <tbody>
     <tr>
      <td>
        <div id="messagewin" runat="server">
            <table style="margin-top:15%;margin-left:3%" align="left">
               <tr align="left">
                <td align="left">Password associated with your Login has been changed successfully.</td>
               </tr>
               <tr align="center">
                <td align="center"><input type="button" value="close" class="button" onclick="window.close();"></td>
               </tr>
           </table>
        </div>
       <div id="mainwin" runat="server">
       <table border="0">
        <tbody>
         <tr>
          <td class="ctrlgroup" colspan="2">Change System Login Parameters</td>
         </tr>
         <tr>
          <td>Admin Login Name</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='newlogin']" size="30" id="newlogin" onchange="setDataChanged(true);" class="disabled" ReadOnly="true"/></td>
         </tr>
         <tr>
          <td>New Login Password</td>
          <td><asp:TextBox runat="server" type="password" TextMode="Password" autocomplete="off" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='newpasswd']" size="30" onchange="setDataChanged(true);" id="newpasswd" maxlength="50"/></td>
         </tr>
         <tr>
          <td>Confirm New Password</td>
          <td><asp:TextBox runat="server" type="password" TextMode="Password" autocomplete="off" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='confirmpasswd']" size="30" onchange="setDataChanged(true);" id="confirmpasswd" maxlength="50"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdUserLoginName']" Style="display: none" ID="hdUserLoginName"/></td> 
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td><asp:button class="button" id="btnOK" Text="  OK  " OnClientClick="return ClosePopUpOnSuccess('','ChangeLoginInfo','');" style="" runat="server"/><asp:button runat="server" class="button" id="btnCancel" Text="Cancel" OnClientClick="return OnCancel();" style=""/></td>
         </tr>
        </tbody>
       </table>
       </div>
      </td>
      <td valign="top"></td>
     </tr>
    </tbody>
   </table>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
 </body>
</html>
