﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="AddNewModuleGroup.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.AddNewModuleGroup" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add New Module Group</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script type="text/javascript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" src="../../Scripts/security.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>

    <script type="text/javascript" language="javascript">
        function CloseIfCallSuccessful() {
            if (document.getElementById('hdAction').value == "OK") {
                ClosePopUpOnSuccess(document.getElementById('hdNewDBId').value, "AddNewModuleGroupEntity", document.getElementById('selectedentityid').value);
            }

        }
    </script>

</head>
<body onload="javascript:CloseIfCallSuccessful();">
    <form id="frmData" runat="server" name="DsnWizard" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:Label ID="errormessage" CssClass="errtext" runat="server"></asp:Label>
    <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="ctrlgroup">
                            Add New Group
                        </td>
                    </tr>
                </table>
                <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='entitytype']"
                                Style="display: none" ID="entitytype" />
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='selectedentityid']"
                                Style="display: none" ID="selectedentityid" />
                            Please type name for new group
                        </td>
                        <td>
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='groupname']"
                                ID="groupname" size="30" onchange="setDataChanged(true);" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="width: 50px">
                    </tr>
                    <tr>
                        <%-- abansal23: Clone for module security groups Start--%>
                        <td align="center">
                            <asp:CheckBox runat="server" ID="chkClone" onclick="EnableClone(this);"  Text="Clone" Checked="false" />
                        </td>
                        <td>
                            <asp:DropDownList Enabled="false" runat="server" ID="lstGroups" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name='groups']/@value"
                                ItemSetRef="/Instance/Document/form/group/displaycolumn/control[@name='groups']">
                            </asp:DropDownList>
                        </td>
                        <%-- abansal23: Clone for module security groups End--%>
                        <td>
                            <asp:TextBox runat="server" Style="display: none" ID="hdAction" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdDBId']"
                                Style="display: none" ID="hdDBId" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdNewDBId']"
                                Style="display: none" ID="hdNewDBId" />
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdUserLoginName']"
                                Style="display: none" ID="hdUserLoginName" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/@postback" Style="display: none"
                                ID="hdpostback" />
                        </td>
                        <%-- abansal23: Clone for module security groups Start--%>
                        <td>
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdCloneModuleId']"
                                Style="display: none" ID="hdCloneModuleId" />
                        </td>
                        <%-- abansal23: Clone for module security groups End--%>
                        <td>
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/@iserror" Style="display: none"
                                ID="hdiserror" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/@closeonpostbackifnoerr"
                                Style="display: none" ID="hdcloseonpostbackifnoerr" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" rmxref="/Instance/Document/form/@message" Style="display: none"
                                ID="hdmessage" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" class="button" ID="btnOk" Text=" OK " OnClientClick="return SetWizardAction(this,'AddNewModuleGroupEntity',event)"
                                Style="" /><asp:Button runat="server" class="button" ID="btnCancel" Text=" Cancel "
                                    OnClientClick="return OnCancel()" Style="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
