﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class ClearLicense : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    btnBack.Visible = false;
                    btnOK.Visible = true;
                }
                else
                {
                    bool bReturnStatus = false;
                    switch (hdAction.Text)
                    {

                        case "Clear":
                            bReturnStatus = CallCWSFunction("RMWinSecurityAdaptor.ClearLicenseHistory");
                            if (txterrmessage.Text != "")
                            {
                                btnBack.Visible = true;
                                btnOK.Visible = false;
                                mainwin.Visible = false;
                                ltrerror.Visible = true;
                                ltrerror.Text = txterrmessage.Text;
                            }
                            break;

                        case "ClearLicense":
                            btnBack.Visible = false;
                            btnOK.Visible = true;
                            mainwin.Visible = true;
                            ltrerror.Visible = false;
                            ltrerror.Text = "";
                            licensecode.Text = "";
                            //Ashish Ahuja - Mits 24659
                            txtformname.Text = "";
                            txterrmessage.Text="";
                            break;

                        default: break;
                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
