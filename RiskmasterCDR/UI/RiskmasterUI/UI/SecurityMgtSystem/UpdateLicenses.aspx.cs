﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class UpdateLicenses : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                }
                else
                {
                    bool bReturnStatus = false;
                    switch (hdAction.Text)
                    {

                        case "UpdateLicenses":
                            bReturnStatus = CallCWSFunction("RMWinSecurityAdaptor.UpdateLicenseInfo");
                            if (txterrmessage.Text != "")
                            {
                                mainwin.Visible = false;
                                ltrerror.Visible = true;
                                ltrerror.Text = txterrmessage.Text;
                            }
                            else if (bReturnStatus)
                            {
                                ClientScript.RegisterClientScriptBlock(typeof(Page), "", "<script type='text/javascript' language='javascript'>window.opener.document.forms[0].hdSave.value='refresh';window.opener.document.forms[0].submit();window.close();</script>");
                            }
                            break;

                        default: break;
                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
