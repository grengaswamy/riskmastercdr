﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class SetEmailOptions : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hdUserLoginName.Text = AppHelper.GetUserLoginName();

                // psharma206  smtp cloud start
                if (ConfigurationManager.AppSettings["CloudDeployed"].ToLower() == "true")
                {
                    smtpserver.Enabled = false;
                }
                // psharma206  smtp cloud end

                if (!IsPostBack)
                {
                    chkSecure.InputAttributes.Add("onchange", "return setDataChanged(true);");
                    NonFDMCWSPageLoad("SecurityAdaptor.GetXMLData");
                }
                else
                {
                    bool bReturnStatus = false;
                    switch (hdAction.Text)
                    {
                        case "OK": bReturnStatus = CallCWSFunction("RMWinSecurityAdaptor.SaveEmailData");
                            if (bReturnStatus)
                            {
                                ClientScript.RegisterClientScriptBlock(typeof(Page), "", "<script type='text/javascript' language='javascript'>window.close();</script>");
                            }
                            break;

                        default: break;
                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
