﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClearLicense.aspx.cs" Inherits="Riskmaster.UI.SecurityMgtSystem.ClearLicense" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Clear License</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
</head>
<body>
    <form id="frmData" name="DsnWizard" runat="server" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox ID="txtformname" runat="server" Visible="false" RMXRef="/Instance/Document/form[ @name = 
'frmClear' ]"></asp:TextBox>
    <asp:TextBox ID="txtiserror" runat="server" Visible="false" RMXRef="/Instance/Document/form/license[ @name = 
'lic' ]/@iserror"></asp:TextBox>
    <asp:TextBox ID="txterrmessage" runat="server" Visible="false" RMXRef="/Instance/Document/form/license[ @name = 
'lic' ]/@message"></asp:TextBox>
    <asp:TextBox ID="txtpostback" runat="server" Visible="false" RMXRef="/Instance/Document/form/license[ @name = 
'lic' ]/@postback"></asp:TextBox>
<asp:TextBox style="display:none" id="hdAction" runat="server"></asp:TextBox>
    <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
    <tr>
     <td>
      <div id="mainwin" runat="server">
      <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
       <tr>
        <td class="ctrlgroup">Change Workstation License Info</td>
       </tr>
      </table><br/><table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
       <tr>
        <td>Enter code to clear license history</td>
        <td><asp:TextBox ID="licensecode" runat="server" size="30" onchange="setDataChanged(true);" rmxref="/Instance/Document/form/license/group/displaycolumn/control[@name='licensecode']"></asp:TextBox></td>
        <td></td>
        <td></td>
       </tr>
      </table>
      </div>
     </td>
     <td align="middle" style="color:red">
                <asp:Literal ID="ltrerror" runat="server" Visible="false" ></asp:Literal>
            </td>
    </tr><br/><table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
     <tr>
      <td align="center"><asp:button class="button" id="btnBack" Text="Back" OnClientClick="return SetWizardAction(this,'ClearLicenseHistory',event);" runat="server" style="" Visible="false"/><asp:button class="button" id="btnOK" Text="  OK  " OnClientClick="return ClosePopUpOnSuccess('','ClearLicenseHistory','');" runat="server" style=""/><asp:button class="button" id="btnCancel" Text="Cancel" OnClientClick="OnCancel();" style="" runat="server"/></td>
     </tr>
    </table>
   </table>
    <div>
    
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
