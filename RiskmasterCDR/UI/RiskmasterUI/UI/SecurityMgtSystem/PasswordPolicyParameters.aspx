﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PasswordPolicyParameters.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.PasswordPolicyParameters" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="pleasewait"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Password Policy Parameters</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <style type="text/css">
        @import url(../../Content/dhtml-div.css);
    </style>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/rmx-common-ref.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js"></script>

</head>
<body class="10pt" onload="PwdPolicyOnLoad()">
    <form id="frmData" runat="server" name="frmData" method="post">
   
    <input type="hidden" name="hTabName"><div id="toolbardrift" name="toolbardrift" class="toolbardrift">
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" height="32">
                    <asp:ImageButton runat="server" ID="Save" alt="" ImageUrl="../../Images/save.gif"
                        OnClientClick="return PwdPolForm_Save();" ToolTip="Save" Width="28" Height="28"
                        border="0" onMouseOver="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                        onMouseOut="this.src='../../Images/save.gif';this.style.zoom='100%'" 
                        title="Save" TabIndex="10" />
                </td>
            </tr>
        </table>
    </div>
    <br>
    <div class="msgheader" id="formtitle">
        Password Policy Parameters</div>
    <div class="errtextheader">
    </div>
    <br>
    <table border="0">
        <tr>
            <td>

                <script type="text/javascript">
							function PwdPolicyOnLoad()
							{
								pageLoaded();
								if(!document.forms[0].optSub4.checked)
								{
									document.forms[0].chkLitUpperCase.disabled = true;
									document.forms[0].chkLitLowerCase.disabled = true;
									document.forms[0].chkLitNumerals.disabled = true;
									document.forms[0].chkLitSpecialChar.disabled = true;
								}
								else
								{
									document.forms[0].chkLitUpperCase.disabled = false;
									document.forms[0].chkLitLowerCase.disabled = false;
									document.forms[0].chkLitNumerals.disabled = false;
									document.forms[0].chkLitSpecialChar.disabled = false;
								}
							}
							function radioOnclick()
							{
								if(!document.forms[0].optSub4.checked)
								{
									document.forms[0].chkLitUpperCase.disabled = true;
									document.forms[0].chkLitLowerCase.disabled = true;
									document.forms[0].chkLitNumerals.disabled = true;
									document.forms[0].chkLitSpecialChar.disabled = true;
								}
								else
								{
									document.forms[0].chkLitUpperCase.disabled = false;
									document.forms[0].chkLitLowerCase.disabled = false;
									document.forms[0].chkLitNumerals.disabled = false;
									document.forms[0].chkLitSpecialChar.disabled = false;
								}
							}
                </script>

                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="Selected" nowrap="true" name="TABSPwdGenSettings" id="TABSPwdGenSettings">
                            <a class="Selected" href="#" onclick="tabChange(this.name);" name="PwdGenSettings"
                                id="LINKTABSPwdGenSettings"><span style="text-decoration: none">Password General Settings</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSPwdComplexitySettings" id="TABSPwdComplexitySettings">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" name="PwdComplexitySettings"
                                id="LINKTABSPwdComplexitySettings"><span style="text-decoration: none">Password Complexity
                                    Settings</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td valign="top" nowrap="true">
                        </td>
                    </tr>
                </table>
                <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 800px;
                    height: 315px; overflow: auto">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABPwdGenSettings" id="FORMTABPwdGenSettings">
                                    <tr id="">
                                        <td>
                                            <u>Password Minimum Length</u>:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PwdMinLength" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PwdMinLength']"
                                                onblur="return numLostFocus(this);" ID="PwdMinLength" MaxLength="5" TabIndex="1"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <u>Password Maximum Length</u>:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PwdMaxLength" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PwdMaxLength']"
                                                onblur="return numLostFocus(this);" ID="PwdMaxLength" MaxLength="5" TabIndex="2"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            <u>Password Minimum Age in days</u>:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PwdMinAge" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PwdMinAge']"
                                                onblur="return numLostFocus(this);" ID="PwdMinAge" MaxLength="5" TabIndex="3"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <u>Password Maximum Age in days</u>:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PwdExpireDays" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PwdExpireDays']"
                                                onblur="return numLostFocus(this);" ID="PwdExpireDays" MaxLength="5" TabIndex="4"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            <u>Lockout Duration in Hours</u>:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PwdLockDuration" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PwdLockDuration']"
                                                onblur="return numLostFocus(this);" ID="PwdLockDuration" MaxLength="5" TabIndex="5"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <u>Days before Password Expire warning will start</u>:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PwdExpireWarningDays" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PwdExpireWarningDays']"
                                                onblur="return numLostFocus(this);" ID="PwdExpireWarningDays" 
                                                MaxLength="5" TabIndex="6"
                                                onchange=" return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            <u>No. of Previous Password to remember</u>:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PreviousPwds" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PreviousPwds']"
                                                onblur="return numLostFocus(this);" ID="PreviousPwds" MaxLength="5" TabIndex="7"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <u>No. of Password attempts allowed before account is locked</u>:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" name="PwdAttempts" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PwdAttempts']"
                                                onblur="return numLostFocus(this);" ID="PwdAttempts" MaxLength="5" TabIndex="8"
                                                onchange="return setDataChanged(true);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkUseName" name="chkUseName" runat="server" appearance="full"
                                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='chkUseName']"
                                                TabIndex="9" onclick="setDataChanged(true);" />Allow user to set First Name and Last Name
                                            as passwords.
                                            
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABPwdComplexitySettings"
                                    id="FORMTABPwdComplexitySettings" style="display: none;">
                                    <tr id="">
                                        <td colspan="2">
                                            <font color="#99999">Password complexity can be set by choosing from following options:</font>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="2">
                                            <asp:RadioButton runat="server" ID="optMain" name="optMain" GroupName="Group" value="0"
                                                Text="All of the four literal types should be present in the password." rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='optMain']/@value"
                                                onclick="setDataChanged(true);radioOnclick();" />
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="2">
                                            <asp:RadioButton runat="server" ID="optSub1" name="optSub1" GroupName="Group" value="1"
                                                Text="Any of the three literal types should be present in the password." rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='optMain']/@value"
                                                onclick="setDataChanged(true);radioOnclick();" />
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="2">
                                            <asp:RadioButton runat="server" ID="optSub2" name="optSub2" GroupName="Group" value="2"
                                                Text="Any of the two literal types should be present in the password." rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='optMain']/@value"
                                                onclick="setDataChanged(true);radioOnclick();" />
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="2">
                                            <asp:RadioButton runat="server" ID="optSub3" name="optSub3" GroupName="Group" value="3"
                                                Text="Any of the one literal type should be present in the password." rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='optMain']/@value"
                                                onclick="setDataChanged(true);radioOnclick();" />
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="2">
                                            <asp:RadioButton runat="server" ID="optSub4" name="optSub4" GroupName="Group" value="4"
                                                Text="Following literal type should be present in the password." rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='optMain']/@value"
                                                onclick="setDataChanged(true);radioOnclick();" />
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="12">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="10%">
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkLitUpperCase" name="chkLitUpperCase" runat="server" appearance="full"
                                                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='chkLitUpperCase']"
                                                            TabIndex="5" onclick="setDataChanged(true);" />English Uppercase letters (A to Z)
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="10%">
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkLitLowerCase" name="chkLitLowerCase" runat="server" appearance="full"
                                                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='chkLitLowerCase']"
                                                            TabIndex="6" onclick="setDataChanged(true);" />English Lower-case letters (a to z)
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="10%">
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkLitNumerals" name="chkLitLowerCase" runat="server" appearance="full"
                                                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='chkLitNumerals']"
                                                            TabIndex="7" onclick="setDataChanged(true);" />
                                                        Numerals (0 to 9)
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="10%">
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkLitSpecialChar" name="chkLitSpecialChar" runat="server" appearance="full"
                                                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='chkLitSpecialChar']"
                                                            TabIndex="8" 
                                                            onclick="setDataChanged(true);" />Non-alpha-numeric chars (such as !, $,
                                                        #, %)
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
                <input type="text" name="" value="" id="SysViewType" style="display: none" />
                <asp:TextBox runat="server" name="" ID="SysCmd" Style="display: none"></asp:TextBox>
                <input type="text" name="" value="" id="SysCmdConfirmSave" style="display: none" />
                <input type="text" name="" value="" id="SysCmdQueue" style="display: none" />
                <input type="text" name="" value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate" />
                <input type="text" name="" value="" id="SysClassName" style="display: none" rmxforms:value="" />
                <input type="text" name="" value="" id="SysSerializationConfig" style="display: none" />
                <input type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="" />
                <input type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="" />
                <input type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="PwdPolicyParms" />
                <input type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value="" />
                <input type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="PwdPolicyParms" />
                <input type="text" name="" value="PwdMinLength|PwdMaxLength|PwdMinAge|PwdExpireDays|PwdLockDuration|PwdExpireWarningDays|PreviousPwds|PwdAttempts|"
                    id="SysRequired" style="display: none" />
                <input type="text" name="" value="PwdMinLength|" id="SysFocusFields" style="display: none" />
            </td>
        </tr>
    </table>
    <uc:pleasewait CustomMessage="Loading" runat="server" ID="pleasewait" />
    </form>
</body>
</html>
