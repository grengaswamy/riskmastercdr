﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class UserPerm : NonFDMBasePageCWS
    {
        private XmlDocument oFDMPageDom = null;
        private string sReturn = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hdUserLoginName.Text = AppHelper.GetUserLoginName();

                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes
                if (AppHelper.ClientId != 0 && ConfigurationManager.AppSettings["CloudDeployed"].ToLower() == "true")
                {
                    lbFilePath.Visible = false;
                    filepath.Visible = false;
			               }

                if (!Page.IsPostBack)
                {
                    userid.Text = AppHelper.GetQueryStringValue("userid");
                    dsnid.Text = AppHelper.GetQueryStringValue("dsnid");
                    hdPostback.Text = AppHelper.GetQueryStringValue("hdpostback");
                    hdPwdFrmUserScreen.Text = AppHelper.GetQueryStringValue("hdpwdfrmuserscreen");
                    dbid.Text = AppHelper.GetQueryStringValue("dbid");
                    //MITS 22793 : Password visible on viewing source
                    password.Attributes.Add("tempuserpwd", "");
                    NonFDMCWSPageLoad("SecurityAdaptor.GetXMLData");
                    password.Attributes.Remove("tempuserpwd");
                    PassPolicy();
                    //MITS 22793 : Password visible on viewing source
                    ViewState["userpwd"] = password.Text;
                }
                else
                {
                    //Called when toolbar Save is pressed
                    switch (hdSave.Text.ToLower())
                    {
                        case "savenomodulegrpselected":
                                                        password.Attributes.Add("tempuserpwd", ViewState["userpwd"].ToString());
                                                        CallCWS("UserLoginsAdaptor.Save" , null , out sReturn , true , true);
                                                        ViewState["userpwd"] = password.Text;
                                                        password.Text = password.Attributes["value"];
                                                        password.Attributes.Remove("tempuserpwd");
                                                        break;

                        case "savewithmodulegrpselected":
                                                        password.Attributes.Add("tempuserpwd", ViewState["userpwd"].ToString());
                                                        CallCWS("UserLoginsAdaptor.Save", null, out sReturn, true, true);
                                                        ViewState["userpwd"] = password.Text;
                                                        password.Text = password.Attributes["value"];
                                                        password.Attributes.Remove("tempuserpwd");
                                                        ErrorControl1.errorDom = sReturn;
                                                        if (!ErrorControl1.errorFlag)
                                                        {
                                                            hdPostback.Text = "true";
                                                        }
                                                        break;

                        case "refresh":
                                                        password.Attributes.Add("tempuserpwd", ViewState["userpwd"].ToString());
                                                        ViewState["userpwd"] = password.Text;
                                                        NonFDMCWSPageLoad("SecurityAdaptor.GetXMLData");
                                                        ViewState["userpwd"] = password.Text;
                                                        password.Text = password.Attributes["value"];
                                                        PassPolicy();
                                                        password.Attributes.Remove("tempuserpwd");
                                                        break;
                        default: break;
                    }

                }
                if (permexpires.Checked)
                {
                    expdate.Enabled = true;
                    expdate.Attributes.Remove("disabled");
                    //expdatebtn.Enabled = true;
                    //expdatebtn.Attributes.Remove("disabled"); //vkumar258 - 
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        void PassPolicy()
        {
            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            XmlNode xmlNode = oFDMPageDom.SelectSingleNode("//control[@name='IsPasswordPolicyEnable']");
            if (xmlNode != null)
            {
                if (xmlNode.InnerText.ToLower() == "true")
                {
                    divPassPolicy.Visible = true;
                    password.ReadOnly = true;
                    password.CssClass = "disabled";

                }
            }
        }
    }
}

