﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiLangDateFormat.aspx.cs" Inherits="Riskmaster.UI.UI.SecurityMgtSystem.MultiLangDateFormat" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="usc" %>
<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/MultiCodeLookup.ascx" TagName="MultiCode" TagPrefix="ucmc" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Date Format</title>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>
    <script language="JavaScript" type="text/javascript" src="../../Scripts/WaitDialog.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/cul.js"></script>
    <script src="../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
</head>
<body class="" style="overflow-x:inherit;">
    <form id="frmData" runat="server">
        <uc1:errorcontrol id="ErrorControl1" runat="server" />
        <input type="hidden" name="hTabName" />
        <div>
            <table border="0" cellspacing="0" cellpadding="0" name="FORMTABGlobalization" id="FORMTABGlobalization">
                <tr id="Tr1">
                    <td class="ctrlgroup" width="100%">
                        <asp:label id="lblMultiLangSetup" runat="Server" text="<%$ Resources:lblMultiLangSetup %>"></asp:label>

                    </td>
                </tr>
                <tr id="Tr5">
                    <td>
                        <div title="" style="padding: 0px; margin: 0px">
                            <table>
                                <tr>
                                    <td>
                                        <dg:usercontroldatagrid runat="server" id="MultiLanguageSetupGrid" gridname="MultiLanguageSetupGrid"
                                            target="/Document/form/MultiLanguageSetup"
                                            ref="/Instance/Document/form[@name='MultiLanguageSetupGrid']" unique_id="RowId"
                                            showradiobutton="true" width="" height="800" hidenodes="|RowId|" showheader="True"
                                            linkcolumn="" popupwidth="500" popupheight="200" type="GridAndButtons" rowdataparam="listrow"
                                            onclick="KeepRowForEdit('MultiLanguageSetupGrid');" hidebuttons="New|Delete" imgedittooltip="<%$ Resources:ttEdit %>" />
                                        <asp:textbox style="display: none" runat="server" id="MultiLanguageSetupSelectedId"
                                            rmxtype="id" />
                                        <asp:textbox style="display: none" runat="server" id="MultiLanguageSetupGrid_RowDeletedFlag"
                                            rmxtype="id" text="false" />
                                        <asp:textbox style="display: none" runat="server" id="MultiLanguageSetupGrid_Action"
                                            rmxtype="id" />
                                        <asp:textbox style="display: none" runat="server" id="MultiLanguageSetupGrid_RowAddedFlag"
                                            rmxtype="id" text="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:button id="btnCancel" cssclass="button" text="Cancel" runat="server" onclientclick="OnCancel();" tooltip="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <asp:checkbox id="RMXLSSEnable" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='RMXLSSEnable']"
            style="display: none" runat="server" />
        <asp:textbox id="SysWindowId" type="hidden" value="rmx-widget-handle-3" style="display: none"
            runat="server"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="txtGenerateXml" text="true"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="txtPageId" rmxref="/Instance/Document/form/PageId"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="txtLangCode" rmxref="/Instance/Document/form/LangId"></asp:textbox>
        <uc:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="" />

    </form>
</body>
</html>

