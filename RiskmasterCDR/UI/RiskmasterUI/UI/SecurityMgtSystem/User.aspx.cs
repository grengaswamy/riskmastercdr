﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class User : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes

                hdUserLoginName.Text = AppHelper.GetUserLoginName();
                if (!Page.IsPostBack)
                {
                    string sUserId = AppHelper.GetQueryStringValue("userid");
                    userid.Text = sUserId;

                    NonFDMCWSPageLoad("SecurityAdaptor.GetXMLData");


                }
                else
                {
                    //Called when toolbar Save is pressed
                    switch (hdSave.Text.ToLower())
                    {
                        case "savewithnodsn": CallCWSFunction("UsersAdaptor.Save");
                            break;

                        case "savewithdsn": Mon.Checked = true;
                            Tue.Checked = true;
                            Wed.Checked = true;
                            Thu.Checked = true;
                            Fri.Checked = true;
                            Sat.Checked = true;
                            Sun.Checked = true;

                            CallCWSFunction("UsersAdaptor.Save");

                            break;


                        default: break;
                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
