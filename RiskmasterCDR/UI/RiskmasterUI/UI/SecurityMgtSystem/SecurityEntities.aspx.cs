﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class SecurityEntities : NonFDMBasePageCWS
    {
        /// <summary>
        /// Contains web service output
        /// </summary>
        XmlDocument oFDMPageDom = null;
        /// <summary>
        /// This nodeList would maintain list of all eligible nodes in order to set their dynamic population property 
        /// </summary>
        ArrayList arlstDynamicPopulatedNodesList = null;
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                arlstDynamicPopulatedNodesList = new ArrayList();
                if (!Page.IsCallback)
                {
                    //registring onclick event for treeview
                    //this event would handle all kinds of client side manipulations 
                    LTV.Attributes.Add("onclick", "return OnTreeClick(event)");

                    //Calling the service and binding controls accordingly
                    NonFDMCWSPageLoad("SecurityAdaptor.GetXmlStructureForLeftTree");
                    oFDMPageDom = Data;

                    if (NewUserId.Text != "" && UserEntityUserId.Text == "")
                    {
                        UserEntityUserId.Text = NewUserId.Text;
                    }

                    //Update Tree Bindings to populate data
                    if (ErrorControl1.errorFlag == false && hdWarnings.Text == "")
                    {
                        UpdateTreeBindings();

                        //Clear User Name array list and Data Source Name array list
                        usernamestring.Text = "";
                        datasourcestring.Text = "";
                        
                        //Recursively Iterate over all nodes to prepare an arraylist of all nodes which need to dynamically populated
                        foreach (TreeNode objNode in LTV.Nodes)
                        {
                            UpdateDynamicNodeList(objNode);
                        }
                        //Update properties of node for dynamic population from Service
                        UpdateDynamicNodes();
                    }
                    else
                    {
                        if (hdWarnings.Text != "")
                        {
                            lblWarning.Text = hdWarnings.Text;
                            back.Visible = true;
                        }
                    }



                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        /// <summary>
        /// Update All Tree Bindings to display data
        /// </summary>
        private void UpdateTreeBindings()
        {
            //srajindersin MITS 30263 dt 11/27/2012
            //LTV.DataSourceID = "PreBindedXMLSource";
            ////this.OTV.DataBindings.Clear();

            //TreeNodeBinding tnb1 = new TreeNodeBinding();
            //tnb1.DataMember = "entry";
            //tnb1.ValueField = "url";
            //tnb1.TextField = "text";
            //// Raman: We are removing all images in SMS
            ////tnb1.ImageUrlField = "icon";
            //this.LTV.DataBindings.Add(tnb1);

            XmlDocument XmlDocument1 = null;
            string NewXmlText = oFDMPageDom.SelectSingleNode("//entry[@id='0']").OuterXml;

            PreBindedXMLSource.Data = NewXmlText;

            //if (Page.IsPostBack)
            //{
            XmlDocument1 = PreBindedXMLSource.GetXmlDocument();
            XmlDocument1.LoadXml(NewXmlText);
            //}

            ShowTreeData(XmlDocument1, NewXmlText);
            //LTV.DataBind();
            //END srajindersin MITS 30263 dt 11/27/2012
				


        }
        /// <summary>
        /// This function gets called dynamically from server using AJAX to populate child nodes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LTV_GetChildNodes(object sender, TreeNodeEventArgs e)
        {
            XElement oMessageElement = null;
            XmlDocument oResponse = null;
            string sReturn = "";
            XElement oCmdElement = null;
            try
            {
                oMessageElement = GetChildNodePopulateMessageTemplate();
                oResponse = new XmlDocument();
                
                oCmdElement = oMessageElement.XPathSelectElement("./Document/DsnIdForLoadingMG");
                if (oCmdElement != null)
                {
                    oCmdElement.Value = e.Node.Value.Substring(e.Node.Value.LastIndexOf('=') + 1);
                }

                //Call WCF wrapper for cws
                sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                oResponse.LoadXml(sReturn);
                BindData2ErrorControl(sReturn);
                XmlNode oInstanceNode = oResponse.SelectSingleNode("/ResultMessage/Document");
                XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                oResponse.LoadXml(oInstanceNode.OuterXml);
                foreach (XmlNode oModuleGroupNode in oResponse.FirstChild.FirstChild.ChildNodes)
                {
                    TreeNode parentNode = AddChildNode(e.Node, oModuleGroupNode.Attributes["url"].Value, oModuleGroupNode.Attributes["text"].Value, oModuleGroupNode.Attributes["icon"].Value);

                    foreach (XmlNode oChildNode in oModuleGroupNode.ChildNodes)
                    {
                        AddChildNode(parentNode, oChildNode.Attributes["url"].Value, oChildNode.Attributes["text"].Value, oChildNode.Attributes["icon"].Value);
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
           

            
        }
        /// <summary>
        /// This function adds a child node to parent
        /// </summary>
        /// <param name="oParentNode"></param>
        /// <param name="p_Id"></param>
        /// <param name="p_sName"></param>
        /// <param name="p_sCode"></param>
        /// <param name="p_bHasChild"></param>
        private TreeNode AddChildNode(TreeNode oParentNode, string p_Value, string p_sName, string p_sImagePath)
        {
            TreeNode childNode = null;
            try
            {
                childNode = new TreeNode(p_sName, p_Value.ToString());
                childNode.PopulateOnDemand = false;
                childNode.Target = "_self";
                //childNode.ImageUrl = p_sImagePath;
                oParentNode.ChildNodes.Add(childNode);

            }
            catch (Exception e)
            {
            }
            return childNode;
        }
        /// <summary>
        /// This function would prepare an arraylist of all nodes which need to dynamically populated
        /// </summary>
        /// <param name="objNode"></param>
        private void UpdateDynamicNodeList(TreeNode objNode)
        {
            //We need to prepare strings containing all user names for client side validations
            if (objNode.Value.IndexOf("User.aspx") != -1)
            {
                usernamestring.Text = usernamestring.Text + '|' + objNode.Text + "| ";
            }

            //We need to prepare strings containing all data source names for client side validations
            if (objNode.Value.IndexOf("Datasource.aspx") != -1)
            {
                datasourcestring.Text = datasourcestring.Text + '|' + objNode.Text + "| ";
            }
                            
            if (objNode.ChildNodes.Count == 0 && objNode.Text == "Module Security Groups")
            {
                arlstDynamicPopulatedNodesList.Add(objNode);

            }
            else
            {
                switch (TreePostedBy.Text.ToLower())
                {
                    case "addnewuser":
                    case "updateuserentity":
                    case "updateuserpermentity":

                        //User has been assigned to a module group
                        if (UserPermEntityDsnId.Text != "" && UserPermEntityUserId.Text != "" && UserPermModuleGrpSelectedId.Text != "" && TreePostedBy.Text.ToLower() == "updateuserpermentity")
                        {
                            foreach (TreeNode objChildNode in objNode.ChildNodes)
                            {
                                if ((objChildNode.Value.IndexOf("NoData.html") != -1) && HttpUtility.ParseQueryString(objChildNode.Value).Count == 3 && HttpUtility.ParseQueryString(objChildNode.Value)[0].ToString() == UserPermEntityDsnId.Text && HttpUtility.ParseQueryString(objChildNode.Value)[1].ToString() == UserPermModuleGrpSelectedId.Text && HttpUtility.ParseQueryString(objChildNode.Value)[2].ToString() == UserPermEntityUserId.Text)
                                {
                                    if (objNode.Parent.Parent.Parent != null)
                                    {
                                        objNode.Parent.Parent.Parent.Expand();
                                    }
                                    if (objNode.Parent.Parent != null)
                                    {
                                        objNode.Parent.Parent.Expand();
                                    }
                                    objNode.Parent.Expand();
                                    objNode.Expand();
                                    objChildNode.Selected = true;
                                    
                                }
                                UpdateDynamicNodeList(objChildNode);

                            }
                        }
                        else if (UserEntityDsnIdSelected.Text == "" && UserEntityUserId.Text != "") //User information has been updated
                        {
                            foreach (TreeNode objChildNode in objNode.ChildNodes)
                            {
                                if (HttpUtility.ParseQueryString(objChildNode.Value).Count == 1 && (objChildNode.Value.IndexOf("userid")!= -1) && HttpUtility.ParseQueryString(objChildNode.Value)[0].ToString() == UserEntityUserId.Text)
                                {
                                    if (objNode.Parent.Parent != null)
                                    {
                                        objNode.Parent.Parent.Expand();
                                    }
                                    objNode.Parent.Expand();
                                    objNode.Expand();
                                    objChildNode.Selected = true;
                                }

                                UpdateDynamicNodeList(objChildNode);

                            }
                            
                        }  //User Dsn Permission information has been updated
                        else if (objNode.Text == "Permission To Login" && UserEntityDsnIdSelected.Text != "" && UserEntityUserId.Text != "")
                        {
                            foreach (TreeNode objChildNode in objNode.ChildNodes)
                            {
                                if (HttpUtility.ParseQueryString(objChildNode.Value).Count == 2 && HttpUtility.ParseQueryString(objChildNode.Value)[0].ToString() == UserEntityDsnIdSelected.Text && HttpUtility.ParseQueryString(objChildNode.Value)[1].ToString() == UserEntityUserId.Text)
                                {
                                    objNode.Parent.Parent.Expand();
                                    objNode.Parent.Expand();
                                    objNode.Expand();
                                    objChildNode.Selected = true;

                                }
                                UpdateDynamicNodeList(objChildNode);

                            }
                        }
                        else
                        {
                            foreach (TreeNode objChildNode in objNode.ChildNodes)
                            {
                                UpdateDynamicNodeList(objChildNode);

                            }
                        }
                        

                        break;

                    case "addnewmodulegroupentity": 
                    case "renamemodulegroupentity":
                            
                        //New Module Group has been added
                        if (objNode.Text == "Module Security Groups")
                        {
                            string[] sIdDbSplit = idDb.Text.Split(',');
                            foreach (TreeNode objChildNode in objNode.ChildNodes)
                            {
                                if (HttpUtility.ParseQueryString(objChildNode.Value).Count == 2 && (objChildNode.Value.IndexOf("dsnid") != -1) && (objChildNode.Value.IndexOf("groupid") != -1) && HttpUtility.ParseQueryString(objChildNode.Value)[0].ToString() == sIdDbSplit[0] && HttpUtility.ParseQueryString(objChildNode.Value)[1].ToString() == sIdDbSplit[1])
                                {
                                    if (objNode.Parent.Parent != null)
                                    {
                                        objNode.Parent.Parent.Expand();
                                    }
                                    objNode.Parent.Expand();
                                    objNode.Expand();
                                    objChildNode.Selected = true;
                                }

                                UpdateDynamicNodeList(objChildNode);

                            }
                        }
                        else
                        {
                            foreach (TreeNode objChildNode in objNode.ChildNodes)
                            {
                                UpdateDynamicNodeList(objChildNode);

                            }
                        }
                        break;

                    case "delete":
                        
                        switch (SelectedEntityFromLeftTree.Text)
                        {
                            case "UserPerm":
                                string[] sIdDb = EntityIdForDeletion.Text.Split(',');
                                if (objNode.Value.IndexOf("Datasource.aspx") != -1 && HttpUtility.ParseQueryString(objNode.Value)[0].ToString() == sIdDb[0].ToString())
                                {
                                    if (objNode.Parent.Parent != null)
                                    {
                                        objNode.Parent.Parent.Expand();
                                    }
                                    objNode.Parent.Expand();
                                    objNode.Expand();
                                    if (objNode.ChildNodes[0].ChildNodes.Count > 0)
                                    {
                                        objNode.ChildNodes[0].Expand();
                                    }
                                    objNode.Selected = true;
                                }
                                foreach (TreeNode objChildNode in objNode.ChildNodes)
                                {
                                    UpdateDynamicNodeList(objChildNode);

                                }
                                break;

                            case "GrpModuleAccessPermission":
                                sIdDb = EntityIdForDeletion.Text.Split(',');
                                if (objNode.Value.IndexOf("Datasource.aspx") != -1 && HttpUtility.ParseQueryString(objNode.Value)[0].ToString() == sIdDb[0].ToString())
                                {
                                    if (objNode.Parent.Parent != null)
                                    {
                                        objNode.Parent.Parent.Expand();
                                    }
                                    objNode.Parent.Expand();
                                    objNode.Expand();
                                    if (objNode.ChildNodes[1].ChildNodes.Count > 0)
                                    {
                                        objNode.ChildNodes[1].Expand();
                                    }
                                    objNode.ChildNodes[1].Selected = true;
                                }
                                foreach (TreeNode objChildNode in objNode.ChildNodes)
                                {
                                    UpdateDynamicNodeList(objChildNode);

                                }
                                break;

                            case "Module Group User":
                                sIdDb = EntityIdForDeletion.Text.Split(',');
                                if (objNode.Value.IndexOf("Datasource.aspx") != -1 && HttpUtility.ParseQueryString(objNode.Value)[0].ToString() == sIdDb[0].ToString())
                                {
                                    if (objNode.Parent.Parent != null)
                                    {
                                        objNode.Parent.Parent.Expand();
                                    }
                                    objNode.Parent.Expand();
                                    objNode.Expand();
                                    if (objNode.ChildNodes[1].ChildNodes.Count > 0)
                                    {
                                        objNode.ChildNodes[1].Expand();
                                        foreach (TreeNode objChildNode in objNode.ChildNodes[1].ChildNodes)
                                        {
                                            if (HttpUtility.ParseQueryString(objChildNode.Value)[1].ToString() == sIdDb[1].ToString())
                                            {
                                                objChildNode.Selected = true;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        objNode.ChildNodes[1].Selected = true;
                                    }
                                }
                                foreach (TreeNode objChildNode in objNode.ChildNodes)
                                {
                                    UpdateDynamicNodeList(objChildNode);

                                }
                                break;
                            default:
                                foreach (TreeNode objChildNode in objNode.ChildNodes)
                                {
                                    UpdateDynamicNodeList(objChildNode);

                                }
                                break;
                        }
                        break;

                    

                    case "addnewdatasource":
                        if (objNode.Text == "Data Sources" && AddedDBId.Text != "")
                        {
                            foreach (TreeNode objChildNode in objNode.ChildNodes)
                            {
                                if (HttpUtility.ParseQueryString(objChildNode.Value).Count == 1 && HttpUtility.ParseQueryString(objChildNode.Value)[0].ToString() == AddedDBId.Text) 
                                {
                                    objNode.Parent.Expand();
                                    objNode.Expand();
                                    objChildNode.Selected = true;

                                }
                                UpdateDynamicNodeList(objChildNode);
                            }
                        }
                        else
                        {
                            foreach (TreeNode objChildNode in objNode.ChildNodes)
                            {
                                UpdateDynamicNodeList(objChildNode);

                            }
                        }
                        break;
                    default:

                        foreach (TreeNode objChildNode in objNode.ChildNodes)
                        {
                            UpdateDynamicNodeList(objChildNode);
                        }
                        break;
                }
                
            }
        }
        /// <summary>
        /// This function would update properties of node for dynamic population from Service
        /// </summary>
        private void UpdateDynamicNodes()
        {
            foreach (TreeNode objNode in arlstDynamicPopulatedNodesList)
            {
                if (objNode != null)
                {
                    TreeNode currentNodeReplica = new TreeNode(objNode.Text, objNode.Value);
                    currentNodeReplica.PopulateOnDemand = true;
                    TreeNode parentNode = objNode.Parent;
                    parentNode.ChildNodes.Remove(objNode);
                    parentNode.ChildNodes.Add(currentNodeReplica);
                }
            }
        }
        /// <summary>
        /// CWS request message template for fetching child nodes
        /// </summary>
        /// <returns></returns>
        private XElement GetChildNodePopulateMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>1f5c22e4-6909-49bb-b541-061e650a5d79</Authorization> 
            <Call>
              <Function>SecurityAdaptor.GetModuleGroupChildrenInfo</Function> 
              </Call>
            <Document>
            <DsnIdForLoadingMG></DsnIdForLoadingMG> 
              </Document>
              </Message>
          
            ");
            return oTemplate;
        }

        #region Tree Bind Methods
        //srajindersin MITS 30263 dt 11/27/2012
        public void InsertNodes(XmlNode Parent, TreeNode ParentTree)
        {
            for (int i = 0; i < Parent.ChildNodes.Count; i++)
            {
                XmlNode xnod = Parent.ChildNodes[i]; 
                TreeNode tnod = new TreeNode();
                tnod.Text = xnod.Attributes["text"].Value;
                tnod.Value = xnod.Attributes["url"].Value; 
                ParentTree.ChildNodes.Add(tnod);
                if (xnod.HasChildNodes)
                {
                    InsertNodes(xnod, tnod);
                }
            }
        } 
        public void ShowTreeData(XmlNode xnodRoot,string XmlTreeData) 
        { 
            LTV.Nodes.Clear(); 
            TreeNode treeNodeRoot = new TreeNode();
            treeNodeRoot.Text = ((xnodRoot).ChildNodes[0]).Attributes["text"].Value;
           treeNodeRoot.Value = ((xnodRoot).ChildNodes[0]).Attributes["url"].Value; //mbahl3 JIRA[RMA-293]
            LTV.Nodes.Add(treeNodeRoot);
            InsertNodes((xnodRoot).ChildNodes[0], treeNodeRoot);
        }
        #endregion
    }
}
