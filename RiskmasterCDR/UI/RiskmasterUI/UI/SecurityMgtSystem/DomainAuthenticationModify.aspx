﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DomainAuthenticationModify.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.DomainAuthenticationModify" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>

</head>
<body class="10pt" onload="pageLoaded();Closewindow();" >

    <script type="text/javascript" language="javascript">
       
        function Closewindow() {
            if (document.getElementById('hdsavecomplete').value == "true") {
                document.getElementById('hdsavecomplete').value = "";
                window.opener.ReSubmitPage();
                window.close();
            }
        }
        
    </script>

    <form id="frmData" runat="server" name="frmData" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
    <asp:TextBox ID="txtformname" runat="server" Visible="false" RMXRef="/Instance/Document/form[ @name = 
'DomainAuthenticationModify' ]"></asp:TextBox>
    <asp:HiddenField ID="hdsavecomplete" runat="server" />
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'hdUserLoginName' ]"
                                        name="$node^24" Style="display: none" ID="hdUserLoginName"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Add/edit Domain
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Domain Name
                                </td>
                                <td>
                                    <asp:TextBox runat="server" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'txtdomainName' ]"
                                        name="$node^36" size="30" ID="txtdomainName" onchange="setDataChanged(true);"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Enabled
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" name="$node^42" appearance="full" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'chkDomain' ]"
                                        ID="chkDomain" onclick="setDataChanged(true);" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" name="$node^9" Style="display: none" ID="hdnAction"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" RMXref="/Instance/Document/form/oldNameVal[@name = 'txtoldname']"
                                        name="$node^30" Style="display: none" ID="oldName"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" RMXref="/Instance/Document/form/sequenceidVal[@name = 'txtseqid']"
                                        name="$node^29" Style="display: none" ID="sequenceid"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" RMXref="/Instance/Document/form/statusVal[ @name = 'txtstatus' ]"
                                        name="$node^31" Style="display: none" ID="status"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" name="$node^68" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'DomainAuthenticationOption']"
                                        Style="display: none" ID="DomainAuthenticationOption"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Button runat="server" ID="btnOK" class="button" name="btnOK" Text="  Save  "
                                        OnClientClick="return ClosePopUpOnSuccess('','DomainAuthenticationModify','')"
                                        Style="" />
                                    <asp:Button runat="server" ID="btnCancel" class="button" name="btnCancel" Text="Cancel"
                                        OnClientClick="return OnCancel();" Style="" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
