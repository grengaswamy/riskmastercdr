﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class PwdPolUnlockUserAccnt : NonFDMBasePageCWS
    {
        #region Member variables
        private string swresponse = "";
        private bool bGetControlData = false;
        private bool bSetControlData = false;
        private XElement oMessageElement = null;
        private XmlDocument oFDMPageDom = null;
        private DataTable dtGridData = null;
        private bool bReturnStatus = false;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                oMessageElement = this.GetNonFDMCWSMessageTemplate();
                oMessageElement = this.GetMessageElement(oMessageElement);
                if (!Page.IsPostBack)
                {
                    CallCWS("PwdPolicyParmsAdaptor.GetUsersList", oMessageElement, out swresponse, bGetControlData, bSetControlData);
                    BindGridData();
                    SelectedUserIds.Text = "";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            
        }

        /// <summary>
        /// Generates the input xml for webservice
        /// </summary>
        /// <param name="oMessageElement">xmlelement containing the documenmt tag</param>
        /// <returns>Message element for web service</returns>
        private XElement GetMessageElement(XElement oMessageElement)
        {
            XmlDocument xmlDoc = new XmlDocument();

            //Load the specified XML
            xmlDoc.LoadXml(oMessageElement.ToString());

            XmlNode xdDocument = xmlDoc.SelectSingleNode("//Document");

            XmlElement xdForm = xmlDoc.CreateElement("form");
            XmlElement xdLdapExportUsersList = xmlDoc.CreateElement("LdapExportUsersList");
            XmlElement xdUsers = xmlDoc.CreateElement("Users");


            xdLdapExportUsersList.AppendChild(xdUsers);
            xdForm.AppendChild(xdLdapExportUsersList);
            

            XmlElement xdSelectedUserIds = xmlDoc.CreateElement("SelectedUserIds");
            if(SelectedUserIds.Text!="")
            {
                XmlText xdSelectedUserIdsValue = xmlDoc.CreateTextNode(SelectedUserIds.Text);
                xdSelectedUserIds.AppendChild(xdSelectedUserIdsValue);
            }
            xdForm.AppendChild(xdSelectedUserIds);
           
            xdDocument.AppendChild(xdForm);

            return XElement.Parse(xmlDoc.OuterXml);

        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetNonFDMCWSMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
                
        /// <summary>
        /// Binds data to the grid 
        /// </summary>
        internal void BindGridData()
        {
            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            dtGridData = new DataTable();

            dtGridData.Columns.Add("userid");
            dtGridData.Columns.Add("UserFullName");
            dtGridData.Columns.Add("loginname");
            dtGridData.Columns.Add("status");

            DataRow objRow = null;

            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("//User");

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();
                string suserid = objNodes.Attributes["userid"].Value.ToString();
                string sUserFullName = objNodes.Attributes["UserFullName"].Value.ToString();
                string sloginname = objNodes.Attributes["loginname"].Value.ToString();
                string sstatus = objNodes.Attributes["status"].Value.ToString();

                objRow["userid"] = suserid;
                objRow["UserFullName"] = sUserFullName;
                objRow["loginname"] = sloginname;
                objRow["status"] = sstatus;
                dtGridData.Rows.Add(objRow);
            }


            grdusers.DataSource = dtGridData;
            grdusers.DataBind();
            BeforePrerender();

        }

        /// <summary>
        /// Adds javascripts to the checkboxes in the grid
        /// </summary>
        private void BeforePrerender()
        {
            for (int i = 0; i < grdusers.Rows.Count; i++)
            {
                CheckBox cbSelectSingle = (CheckBox)grdusers.Rows[i].FindControl("SingleCheckbox");
                TextBox tbUserId = (TextBox)grdusers.Rows[i].FindControl("userid");
                if (i % 2 == 0)
                {
                    cbSelectSingle.Attributes.Add("onclick", "UserOptionUnderModifications(this,'" + tbUserId.Text + "');");
                }
                else
                {
                    cbSelectSingle.Attributes.Add("onclick", "UserOption(this,'" + tbUserId.Text + "');");
                }

            }
        }

        protected void btnUnlock_Click(object sender, EventArgs e)
        {
            try
            {
                bReturnStatus = CallCWS("PwdPolicyParmsAdaptor.UnlockUserAccount", oMessageElement, out swresponse, bGetControlData, bSetControlData);
                if (bReturnStatus)
                {
                    CallCWS("PwdPolicyParmsAdaptor.GetUsersList", oMessageElement, out swresponse, bGetControlData, bSetControlData);
                    BindGridData();
                }
                SelectedUserIds.Text = "";
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        protected void btnLock_Click(object sender, EventArgs e)
        {
            try
            {
                bReturnStatus = CallCWS("PwdPolicyParmsAdaptor.LockUserAccount", oMessageElement, out swresponse, bGetControlData, bSetControlData);
                if (bReturnStatus)
                {
                    CallCWS("PwdPolicyParmsAdaptor.GetUsersList", oMessageElement, out swresponse, bGetControlData, bSetControlData);
                    BindGridData();
                }
                SelectedUserIds.Text = "";
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
