﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class AddNewModuleGroup : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hdUserLoginName.Text = AppHelper.GetUserLoginName();
                hdDBId.Text = AppHelper.GetQueryStringValue("dsnid");
                //abansal23: Clone for module security groups Starts
                if (!Page.IsPostBack)
                {
                    CallCWSFunction("ModuleGroupsAdaptor.Get");
                }
                else
                {
                    switch (hdAction.Text.ToLower())
                    {
                        case "ok":
                            if (chkClone.Checked)
                            {
                                hdCloneModuleId.Text = lstGroups.SelectedValue.ToString();
                            }
                            CallCWSFunction("ModuleGroupsAdaptor.SaveObject");
                            if (!String.IsNullOrEmpty(((Label)ErrorControl1.FindControl("lblError")).Text))
                            {
                                hdAction.Text = "";
                            }
                            else if (!String.IsNullOrEmpty(hdmessage.Text))
                            {
                                errormessage.Text = hdmessage.Text;
                                hdmessage.Text = "";
                                hdAction.Text = "";
                            }
                            break;

                        default: break;
                    }
                }
                //abansal23: Clone for module security groups Ends
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
