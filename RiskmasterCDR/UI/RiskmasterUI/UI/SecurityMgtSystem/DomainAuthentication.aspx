﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DomainAuthentication.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.DomainAuthentication" %>

<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />

    <script type="text/javascript" language="javascript">
        function ReSubmitPage() {
            window.location.href = window.location;
        }
    </script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>

</head>
<body class="10pt" onload="pageLoaded();">
    <form id="frmData" name="frmData" runat="server" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
    <asp:TextBox ID="txtformname" runat="server" Visible="false" rmxretainvalue="true"
        RMXRef="/Instance/Document/form[ @name = 
'DomainAuthentication' ]"></asp:TextBox>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'hdUserLoginName' ]"
                                        Style="display: none" ID="hdUserLoginName"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Set Domain Authentication Options
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Enable Domain authentication
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" name="$node^43" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'chkDomain' ]"
                                        ID="chkDomain" onclick="setDataChanged(true);" />
                                    
                                </td>
                            </tr>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Domains List
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <b></b>
                                    <table width="100%">
                                        <tr>
                                            <td width="80%">
                                                <div runat="server" style="width: 300px; height: auto">
                                                    <dg:UserControlDataGrid runat="server" ID="DomainsGrid" GridName="DomainTableList"
                                                        Target="/Document/form/group/displaycolumn/DomainTableList" Ref="/Document/form/group/displaycolumn/DomainTableList[ @name ='DomainTableList']"
                                                        ShowRadioButton="true" Width="680px" ShowHeader="True" Unique_Id="" LinkColumn=""
                                                        PopupWidth="500px" PopupHeight="150px" Type="gridandbuttons" RowDataParam="listrow" />
                                                    <asp:TextBox ID="tbDomainTableList" Style="display: none" runat="server" rmxignoreset="true"
                                                        RMXref="/Instance/Document/form/group/displaycolumn/DomainTableList[@name='tbDomainTableList']"></asp:TextBox>
                                                    <asp:TextBox ID="tbDomainName" Style="display: none" runat="server" Text="Domain Name"
                                                        RMXref="/Instance/Document/form/group/displaycolumn/DomainTableList/listhead/DomainName[ @name ='tbDomainName']"></asp:TextBox>
                                                    <asp:TextBox ID="tbDomainStatus" Style="display: none" runat="server" Text="Status"
                                                        RMXref="/Instance/Document/form/group/displaycolumn/DomainTableList/listhead/Enabled[ @name ='tbDomainStatus']"></asp:TextBox>
                                                    <asp:TextBox Style="display: none" runat="server" ID="DomainTableList_Action" RMXType="id" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" name="$node^9" Style="display: none" ID="hdAction"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'sequenceid' ]"
                                        name="$node^74" Style="display: none" ID="sequenceid"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Button ID="btnOK" runat="server" class="button" name="btnOK" Text="  OK  " OnClientClick="return ClosePopUpOnSuccess('','DomainAuthentication','')"
                                        Style="" />
                                    <asp:Button ID="btnCancel" runat="server" class="button" name="btnCancel" Text="Cancel"
                                        OnClientClick="return OnCancel();" Style="" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
