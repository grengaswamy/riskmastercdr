﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class PasswordPolicyParameters : NonFDMBasePageCWS
    {
        #region Pageload

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sReturn = "";
                if (!Page.IsPostBack)
                {
                    NonFDMCWSPageLoad("PwdPolicyParmsAdaptor.LoadParmsData");
                }
                else
                {
                    switch (SysCmd.Text)
                    {
                        case "5": CallCWS("PwdPolicyParmsAdaptor.SaveParmsData", null, out sReturn, true, false);

                            break;

                        default: break;
                    }

                }
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
                #endregion
    }
}
