﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserPrivileges.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.UserPrivileges" ValidateRequest="false" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc1" %>
<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>User Privileges Setup</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>

</head>
<body class="10pt">

    <script type="text/javascript" language="javascript">

        var m_FormSubmitted = false;

        function trim(str) {
            return str.replace(/^\s*|\s*$/g, "");
        }

        function ClearActionText() {
            document.forms[0].actionSelectedOnScreen.value = "";
        }

        function ShowHideUserPrivilegeListBox(radioname, thislist, otherlist) {
            var radio = document.getElementById(radioname);
            var firstlist = document.getElementById(thislist);
            var secondlist = document.getElementById(otherlist);
            if (radio.checked == true) {
                firstlist.style.display = '';
                firstlist.style.width = "90%";
                secondlist.style.display = 'none';
            }
            else {
                firstlist.style.display = 'none';
                secondlist.style.display = '';
                secondlist.style.width = "90%";
            }
        }

        function submitLOB() {

            if (m_FormSubmitted) {
                alert("You already submitted this form. Please wait for server to respond.");
                return false;
            }
            m_FormSubmitted = true;
            pleaseWait.Show();
            document.forms[0].submit();
        }

        function SaveEnableDiableLimits(id, message) {
            
            if (confirm(message)) {
                var tabname = id.toString().substring(0, id.toString().indexOf('_'));
                var tbEnable = eval('document.forms[0].tbEnable' + tabname);
                obj = eval('document.forms[0].' + id);
                tbEnable.value = obj.checked.toString();

                //Asharma326 MITS 30874 for setting parent textbox value from checkbox
                var parentcbx = document.getElementById(tabname.replace('Users','') + '_EnableLimits');
                var parenttb = document.getElementById('tbEnable' + tabname.replace('Users', ''));
                if (parenttb != null && parentcbx != null)
                    parenttb.value = parentcbx.checked.toString();

                //Asharma326 MITS 30874  for child checkbox
                if (obj.checked.toString() == 'false') {
                    var childcbx = document.getElementById('tbEnable' + tabname + 'Users');
                    if (childcbx != null)
                        childcbx.value = obj.checked.toString();
                }
                
                document.forms[0].screenAction.value = "CheckBoxSelected";
                document.forms[0].actionSelectedOnScreen.value = "CheckBoxSelected";
                document.forms[0].submit();
                pleaseWait.Show();
            }
            else {
                obj = eval('document.forms[0].' + id);
                obj.checked = !(obj.checked);
            }
        }

        function openAdditionalSettings() {
            window.open('TopLevelApprover.aspx', 'LOBTopLevelApproval',
          'width=800,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 800) / 2 + ',resizable=yes,scrollbars=yes');

            return false;
        }

        // After user/group list selected item changed, get the limits from server again
        function userGroupSelectChange() {
            radioUser = eval('document.forms[0].rdUser');
            radioGroup = eval('document.forms[0].rdGroup');
            var iSelectedIndex = -1;
            var hdSelectedGroup = eval('document.forms[0].hdSelectedGroup');
            var hdSelectedUser = eval('document.forms[0].hdSelectedUser');
            hdSelectedGroup.value = "0";
            hdSelectedUser.value = "0";

            if (radioUser.checked) {
                lstUser = eval('document.forms[0].lbUserList');

                iSelectedIndex = lstUser.selectedIndex;
                if (iSelectedIndex >= 0) {
                    hdSelectedUser.value = lstUser.options[iSelectedIndex].value;
                }
            }
            else {
                lstGroup = eval('document.forms[0].lbGroupList');

                iSelectedIndex = lstGroup.selectedIndex;
                if (iSelectedIndex >= 0) {
                    hdSelectedGroup.value = lstGroup.options[iSelectedIndex].value;
                }
            }

            document.forms[0].actionSelectedOnScreen.value = "";
            pleaseWait.Show();
            document.forms[0].submit();
            
            return true;

        }
        <%-- Start. Added by Gaurav for MITS 30226. --%> 
        function HasOverRideAuthorityChk() {
            document.getElementById('HasOverRideAuthority').checked = false;
        }

        function PreventPayAboveLmtChk() {
            document.getElementById('PreventPayAboveLmt').checked = false;
        }
        <%--End. Added by Gaurav for MITS 30226. --%> 
    </script>

    <form id="frmData" name="frmData" runat="server" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
    <input type="hidden" name="$node^8" value="" id="screenAction" />
    <asp:TextBox runat="server" Style="display: none" RMXRef="/Instance/Document/Data/actionSelectedOnScreen[ @name = 'ActionSelected']"
        ID="actionSelectedOnScreen"></asp:TextBox>
    <%--abansal: MITS 14677 Starts--%>
    <asp:TextBox runat="server" Style="display: none" RMXRef="/Instance/Document/Data/SelectedTab[ @name = 'TabSelected']"
        ID="hTabName"></asp:TextBox>
    <%--//abansal: MITS 14677 Stops--%>
    <asp:TextBox runat="server" Style="display: none" RMXRef="/Instance/Document/Data/LimitToDelete[ @name = 'LimitToDelete']"
        ID="LimitToDelete"></asp:TextBox>
    <input type="hidden" name="SysFocusFields" id="SysFocusFields" />
    <input type="hidden" name="SysFormName" id="SysFormName"  value="UserPrivileges" />
    <div class="msgheader" id="formtitle">
        User Privileges Setup
    </div>
    <br />
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" class="label" ID="lbl_classname" Text="Line of Business:" />
                <span>
                    <asp:DropDownList runat="server" ID="lstLOB" RMXRef="/Instance/Document/Data/control[ @name = 'LOB']/@value"
                        ItemSetRef="/Instance/Document/Data/control[ @name = 'LOB']" onchange="return submitLOB();" />
                    <asp:TextBox runat="server" ID="txtLOB" RMXRef="/Instance/Document/Data/LineOfBusiness[ @name = 'LineOfBusiness']"
                        Style="display: none"></asp:TextBox>
                </span>
            </td>
            <td align="left">
                <asp:Button runat="server" name="btnAdditional"
                    Text="Additional Settings" ID="btnAdditional" class="button" OnClientClick="return openAdditionalSettings();" />
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <td colspan="2">
                            <div class="tabGroup" id="TabsDivGroup" runat="server">
                                <div class="Selected" runat="server" nowrap="true" name="TABSReserveLimits" id="TABSReserveLimits">
                                    <a class="Selected" runat="server" href="#" onclick="tabChange(this.name);" name="ReserveLimits"
                                        id="LINKTABSReserveLimits"><span style="text-decoration: none">Reserve Limits</span></a>
                                </div>
                                <div class="tabSpace" runat="server" id="TBSPReserveLimits">
                                    <nbsp />
                                    <nbsp />
                                </div>
                                <div class="NotSelected" runat="server" nowrap="true" name="TABSPrintCheckLimits"
                                    id="TABSPrintCheckLimits">
                                    <a class="NotSelected1" runat="server" href="#" onclick="tabChange(this.name);" name="PrintCheckLimits"
                                        id="LINKTABSPrintCheckLimits"><span style="text-decoration: none">Print Check Limits</span></a>
                                </div>
                                <div class="tabSpace" runat="server" id="TBSPPrintCheckLimits">
                                    <nbsp />
                                    <nbsp />
                                </div>
                                <div class="NotSelected" runat="server" nowrap="true" name="TABSPaymentLimits" id="TABSPaymentLimits">
                                    <a class="NotSelected1" runat="server" href="#" onclick="tabChange(this.name);" name="PaymentLimits"
                                        id="LINKTABSPaymentLimits"><span style="text-decoration: none">Payment Limits</span></a>
                                </div>
                                <div class="tabSpace" runat="server" id="TBSPPaymentLimits">
                                    <nbsp />
                                    <nbsp />
                                </div>
                                <div class="NotSelected" runat="server" nowrap="true" name="TABSPayDetailLimits"
                                    id="TABSPayDetailLimits">
                                    <a class="NotSelected1" runat="server" href="#" onclick="tabChange(this.name);" name="PayDetailLimits"
                                        id="LINKTABSPayDetailLimits"><span style="text-decoration: none">Pay Detail Limits</span></a>
                                </div>
                                <div class="tabSpace" runat="server" id="TBSPPayDetailLimits">
                                    <nbsp />
                                    <nbsp />
                                </div>
                                <div class="NotSelected" runat="server" nowrap="true" name="TABSClaimLimits" id="TABSClaimLimits">
                                    <a class="NotSelected1" runat="server" href="#" onclick="tabChange(this.name);" name="ClaimLimits"
                                        id="LINKTABSClaimLimits"><span style="text-decoration: none">Claim Limits</span></a>
                                </div>
                                <div class="tabSpace" runat="server" id="TBSPClaimLimits">
                                    <nbsp />
                                    <nbsp />
                                </div>
                                <div class="NotSelected" runat="server" nowrap="true" name="TABSEventTypeLimits"
                                    id="TABSEventTypeLimits">
                                    <a class="NotSelected1" runat="server" href="#" onclick="tabChange(this.name);" name="EventTypeLimits"
                                        id="LINKTABSEventTypeLimits"><span style="text-decoration: none">Event Type Limits</span></a>
                                </div>
                                <div class="tabSpace" runat="server" id="TBSPEventTypeLimits">
                                    <nbsp />
                                    <nbsp />
                                </div>
                                                    <div class="NotSelected" runat="server" nowrap="true" name="TABSPerClaimPayLimits"
                        id="TABSPerClaimPayLimits">
                        <a class="NotSelected1" runat="server" href="#" onclick="tabChange(this.name);" name="PerClaimPayLimits"
                            id="LINKTABSPerClaimPayLimits"><span style="text-decoration: none"> <%--Mgaba2: R8:SuperVisory Approval--%> <%= hdnPerClaimPayLimitTabname.Value %></span></a>
                    </div>

                                <div class="tabSpace" runat="server" id="TBSPPerClaimPayLimits">
                                    <nbsp />
                                    <nbsp />
                                </div>
                                <%--sharishkumar Jira 6385--%>
                                <div class="NotSelected" runat="server" nowrap="true" name="TABSClaimsTotalIncurredLimits"
                                    id="TABSClaimsTotalIncurredLimits">
                                    <a class="NotSelected1" runat="server" href="#" onclick="tabChange(this.name);" name="ClaimsTotalIncurredLimits"
                                        id="LINKTABSClaimsTotalIncurredLimits"><span style="text-decoration: none">Per Claim Incurred Limits</span></a>
                                </div>
                                <div class="tabSpace" runat="server" id="TBSClaimsTotalIncurredLimits">
                                    <nbsp />
                                    <nbsp />
                                </div>
                                <%--sharishkumar Jira 6385--%>
                            </div>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td style="width:20%; border-width:0">
                            <!--MITS:28212 : Added div to give fixed size to listbox so that the size doen't vary in small window-->
                            <div style="position: relative; left: 0; top: 0; width: 250px; overflow: auto" class="singletopborder">
                            <table width="100%">
                                <tr>
                                    <td width="100%">
                                        <asp:RadioButton GroupName="optReserveusergroup" Text="Users" value="Users" ID="rdUser"
                                            Checked="true" onclick="return ShowHideUserPrivilegeListBox('rdUser','lbUserList','lbGroupList');"
                                            RMXref="/Instance/Document/Data/ReserveLimits/UserGroup[ @name = 'optReserveusergroup' ]/@value"
                                            runat="server" />
                                        <asp:RadioButton onclick="return ShowHideUserPrivilegeListBox('rdGroup','lbGroupList','lbUserList');"
                                            GroupName="optReserveusergroup" Text="Groups" value="Groups" ID="rdGroup"
                                            RMXref="/Instance/Document/Data/ReserveLimits/UserGroup[ @name = 'optReserveusergroup' ]/@value"
                                            runat="server" />                        
                                    </td>
                                </tr>
                               <tr>
                                    <td width="100%">
                                    <%--<div style="width:230px; overflow:auto; position:absolute; ">--%> <%--Comment -To be used if horizontal scrollbar is needed--%>
                                        <asp:ListBox SelectionMode="Single" ID="lbUserList" RMXref="/Instance/Document/Data/form/displaycolumn/Users[ @name = 'UserList' ]"
                                            runat="server" name="lbUserList" Rows="10" size="30" Style="width: 95%" onchange="return userGroupSelectChange();"></asp:ListBox>
                                        <asp:ListBox SelectionMode="Single" ID="lbGroupList" RMXref="/Instance/Document/Data/form/displaycolumn/Groups[ @name = 'GroupList' ]"
                                            runat="server" name="lbGroupList" Rows="10" size="30" Style="width: 95%; display: none;" onchange="return userGroupSelectChange();">
                                        </asp:ListBox>
                                     <%--</div>--%>
                                        <asp:TextBox runat="server" ID="hdSelectedUser" style="display:none" RMXref="/Instance/Document/Data/selecteduser" Text="0"></asp:TextBox>
                                        <asp:TextBox runat="server" ID="hdSelectedGroup" style="display:none" RMXref="/Instance/Document/Data/selectedgroup" Text="0"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>                        
                            </div>
                        </td>
                        <td valign="top" align="left" width="80%" style="border-width:0">
                            <div style="position: relative; left: 0; top: 0; width: 950px; height: 450px; overflow: auto; valign: top" class="singletopborder">
                                <table border="0" cellspacing="0" runat="server" celpadding="0" width="100%" name="FORMTABReserveLimits" id="FORMTABReserveLimits">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <table>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Button runat="server" Text="     Add ->>   " class="button" Style="width: 100"
                                                            ID="Add_ReserveLimits" OnClientClick="return ValidateUserPrivilegesForAdding('ReserveLimits');; " /><br />
                                                        <br />
                                                        <asp:Button runat="server" Text="<<- Remove" class="button" Style="width: 100" ID="Remove_ReserveLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForDeleting('ReserveLimits');; " />
                                                        <asp:TextBox ID="tbReserveLimits_SelectedUserGroup" runat="server" Style="display: none;"
                                                                        RMXRef="/Instance/Document/Data/ReserveLimits/SelectedUserGroup[ @name = 'ReserveLimits_SelectedUserGroup' ]"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;<br />&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b><u>Reserve Type:</u></b>
                                                    </td>
                                                    <td>    
                                                        <asp:DropDownList runat="server" ID="ReserveLimits_ReserveType" RMXRef="/Instance/Document/Data/form/displaycolumn/control[ @name = 'ReserveType' ]/@value" ItemSetRef="/Instance/Document/Data/form/displaycolumn/control[ @name = 'ReserveType' ]">
                                                                </asp:DropDownList>
                                                        <asp:TextBox ID="tbReserveLimits_ReserveType" runat="server" Style="display: none;"
                                                            RMXRef="/Instance/Document/Data/ReserveLimits/ReserveType[ @name = 'ReserveLimitsReserveType' ]"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b><u>Max Amount:</u></b>
                                                    </td>
                                                    <td>    
                                                        <asp:TextBox runat="server" ID="ReserveMax" RMXRef="/Instance/Document/Data/ReserveLimits/MaxAmount[ @name = 'ReserveMax' ]" type="numeric" name="ReserveMax" value="" size="10"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                                                                    <tr>
                                                    <td>
                                                        <asp:Label ID="lbReserveLimits_ClaimType" runat="server" Text="Claim Type:"></asp:Label>   
                                                    </td>
                                                    <%--Added by amitosh for R8 enhancement Supervisory Approval--%>
                                                    <td>
                                                    <uc1:CodeLookUp runat="server" CodeTable="CLAIM_TYPE" ControlName="ReserveLimitsClaimType" ID="ReserveLimitsClaimType"   RMXref="/Instance/Document/Data/ReserveLimits/ClaimType[@name = 'ReserveLimitsClaimType']"/>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                        <td>
                                            <dg:UserControlDataGrid runat="server" ID="ReserveLimitsGrid" GridName="ReserveLimitsGrid"
                                                HideButtons="New|Edit|Delete" Target="/Document/Data/form/group/displaycolumn/ReserveLimitsList"
                                                Ref="/Document/Data/form/group/displaycolumn/ReserveLimitsList[ @name ='ReserveGrid']"
                                                Unique_Id="RowId" ShowRadioButton="true" Width="680px" Height="150px" HideNodes="|RowId|"
                                                ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" Type="Grid"
                                                RowDataParam="listrow" />
                                            <asp:TextBox ID="tbreserveGrid" Style="display: none" runat="server" rmxignoreset="true"
                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/ReserveLimitsList[ @name ='txtReserveGrid']"></asp:TextBox>
                                            <asp:TextBox ID="tbreserveGridGroup" Style="display: none" runat="server" Text="Group/User"
                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/ReserveLimitsList/listhead/Group[ @name ='txtReserveGridGroup']"></asp:TextBox>
                                            <asp:TextBox ID="tbreserveGridResType" Style="display: none" runat="server" Text="Res Type"
                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/ReserveLimitsList/listhead/ResType[ @name ='txtReserveGridResType']"></asp:TextBox>
                                            <asp:TextBox ID="tbreserveGridMax" Style="display: none" runat="server" Text="Max Amount"
                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/ReserveLimitsList/listhead/Max[ @name ='txtReserveGridMax']"></asp:TextBox>
                                            <%--Added by amitosh for R8 enhancement Supervisory Approval--%>
                                            <asp:TextBox ID="tbreserveGridClaimType" Style="display: none" runat="server" 
                                                Text="Claim Type" RMXref="/Instance/Document/Data/form/group/displaycolumn/ReserveLimitsList/listhead/ClaimType[ @name ='txtReserveGridClaimType']"></asp:TextBox>
                                            <asp:TextBox ID="tbreserveGridRowID" Style="display: none" runat="server" Text="RowId"
                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/ReserveLimitsList/listhead/RowId[ @name ='txtReserveGridRowId']"></asp:TextBox>
                                            <asp:TextBox Style="display: none" runat="server" ID="ReserveLimitsGrid_Action" Text="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><hr /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox runat="server" RMXRef="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblResLimits' ]"
                                                ID="ReserveLimits_EnableLimits" Text="Enable Reserve Limits" Onclick="return SaveEnableDiableLimits('ReserveLimits_EnableLimits','Are you sure you wish to change the Enable Reserve Limits Option?')" />
                                            <asp:TextBox ID="tbEnableReserveLimits" runat="server" RMXRef="/Instance/Document/Data/ReserveLimits/EnableLimits[ @name = 'EnableResLimits' ]"
                                                Style="display: none"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%--asharma326 MITS 30874 Starts--%>
                                        <tr>
                                            <td colspan="2">
                                                <asp:CheckBox runat="server" RMXRef="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblResLimitsUsers' ]"
                                                    ID="ReserveLimitsUsers_EnableLimits" Text="Restrict Unspecified Users" Onclick="return SaveEnableDiableLimits('ReserveLimitsUsers_EnableLimits','Are you sure you wish to change the Restrict Unspecified Users Option?')" />
                                                <asp:TextBox ID="tbEnableReserveLimitsUsers" runat="server" RMXRef="/Instance/Document/Data/ReserveLimits/EnableLimitsUsers[ @name = 'EnblResLimitsUsers' ]"
                                                    Style="display: none"></asp:TextBox>
                                            </td>
                                        </tr>
                                    <%--asharma326 MITS 30874 Ends--%>
                                </table>
                                <table border="0" cellspacing="0" runat="server" celpadding="0" width="100%" name="FORMTABPrintCheckLimits"
                                       id="FORMTABPrintCheckLimits" style="display: none;">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" valign="top" border="0">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Button runat="server" name="Add_PrintCheckLimits"
                                                                        Text="     Add ->>   " class="button" Style="width: 100" ID="Add_PrintCheckLimits"
                                                                        OnClientClick="return ValidateUserPrivilegesForAdding('PrintCheckLimits');; " /><br />
                                                        <br />
                                                        <asp:Button runat="server" name="Remove_PrintCheckLimits"
                                                            Text="<<- Remove" class="button" Style="width: 100" ID="Remove_PrintCheckLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForDeleting('PrintCheckLimits');; " />
                                                        <asp:TextBox ID="tbPrintCheckLimits_SelectedUserGroup" runat="server" Style="display: none;"
                                                            RMXRef="/Instance/Document/Data/PrintCheckLimits/SelectedUserGroup[ @name = 'PrintCheckLimits_SelectedUserGroup' ]"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;<br />&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="30%">
                                                      <%-- mbahl3 Mit:-26718--%>  <b><u>Max Amount:</u></b><%--&nbsp;&nbsp;--%>
                                                        </td>
                                                        <td>
                                                        <asp:TextBox ID="PrintCheckMax" runat="server" RMXRef="/Instance/Document/Data/PrintCheckLimits/MaxAmount[ @name = 'PrintCheckMax' ]" type="numeric" name="PrintCheckMax" value="" size="10"></asp:TextBox>
                                                   </td><%-- mbahl3 Mit:-26718--%>

                                                </tr>
                                                                                                    <tr>
                                                      <%--Added by amitosh for R8 enhancement Supervisory Approval--%>
                                                     <td>
                                                     <asp:Label ID="lbPrintCheckLimits_ClaimType" runat="server" Text="Claim Type:"></asp:Label>
                                                     </td>
                                                    <td>
                                                    <uc1:CodeLookUp runat="server" CodeTable="CLAIM_TYPE" ControlName="PrintCheckLimitsClaimType" ID="PrintCheckLimitsClaimType" RMXref="/Instance/Document/Data/PrintCheckLimits/ClaimType[@name = 'PrintCheckLimitsClaimType']"/>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                        <td valign="top">
                                            <div style="width: 100%; height: 250; overflow: auto">
                                                <dg:UserControlDataGrid runat="server" ID="PrintCheckLimitsGrid" GridName="PrintCheckLimitsGrid"
                                                    HideButtons="New|Edit|Delete" Target="/Document/Data/form/group/displaycolumn/PrintCheckLimitsList"
                                                    Ref="/Document/Data/form/group/displaycolumn/PrintCheckLimitsList[ @name ='PrintCheckGrid']"
                                                    Unique_Id="RowId" ShowRadioButton="true" Width="680px" Height="150px" HideNodes="|RowId|"
                                                    ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" Type="Grid"
                                                    RowDataParam="listrow" />
                                                <asp:TextBox ID="tbPrintCheckGrid" Style="display: none" runat="server" rmxignoreset="true"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/PrintCheckLimitsList[ @name ='tbPrintCheckGrid']"></asp:TextBox>
                                                <asp:TextBox ID="tbPrintCheckGridGroup" Style="display: none" runat="server" Text="Group/User"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/PrintCheckLimitsList/listhead/Group[ @name ='tbPrintCheckGridGroup']"></asp:TextBox>
                                                <asp:TextBox ID="tbPrintCheckGridMaxAmount" Style="display: none" runat="server"
                                                    Text="Max Amount" RMXref="/Instance/Document/Data/form/group/displaycolumn/PrintCheckLimitsList/listhead/Max[ @name ='tbPrintCheckGridMaxAmount']"></asp:TextBox>
                                           <%--Added by amitosh for R8 enhancement Supervisory Approval--%>
                                                <asp:TextBox ID="tbPrintCheckGridClaimType" Style="display: none" runat="server"
                                                    Text="Claim Type" RMXref="/Instance/Document/Data/form/group/displaycolumn/PrintCheckLimitsList/listhead/ClaimType[ @name ='tbPrintCheckGridClaimType']"></asp:TextBox>
                                                <asp:TextBox ID="tbPrintCheckGridRowId" Style="display: none" runat="server" Text="RowId"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/PrintCheckLimitsList/listhead/RowId[ @name ='tbPrintCheckGridRowId']"></asp:TextBox>
                                                <asp:TextBox Style="display: none" runat="server" ID="PrintCheckLimitsGrid_Action"
                                                    RMXType="id" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox runat="server" RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblPrntChkLimits' ]"
                                                ID="PrintCheckLimits_EnableLimits" Text="Enable Print Check  Limits" onclick="return SaveEnableDiableLimits('PrintCheckLimits_EnableLimits','Are you sure you wish to change the Enable Print Checks Limit Option?')" />
                                            <asp:TextBox ID="tbEnablePrintCheckLimits" runat="server" RMXRef="/Instance/Document/Data/PrintCheckLimits/EnableLimits[ @name = 'EnablePrinntLimits' ]"
                                                Style="display: none"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <%--asharma326 MITS 30874--%>
                                        <tr>
                                            <td colspan="2">
                                                <asp:CheckBox runat="server" RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblPrntChkLimitsUsers' ]"
                                                    ID="PrintCheckLimitsUsers_EnableLimits" Text="Restrict Unspecified Users" onclick="return SaveEnableDiableLimits('PrintCheckLimitsUsers_EnableLimits','Are you sure you wish to change the Restrict Unspecified Users Option?')" />
                                                <asp:TextBox ID="tbEnablePrintCheckLimitsUsers" runat="server" RMXRef="/Instance/Document/Data/PrintCheckLimits/EnableLimitsUsers[ @name = 'EnablePrinntLimitsUsers' ]"
                                                    Style="display: none"></asp:TextBox>
                                            </td>
                                        </tr>
                                    <%--asharma326 MITS 30874--%>
                                </table>
                                <table border="0" cellspacing="0" runat="server" celpadding="0" width="100%" name="FORMTABPaymentLimits"
                                    id="FORMTABPaymentLimits" style="display: none;">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <table width="100%" border="0" valign="top" cellpadding="2" cellspacing="2">
                                                <tr valign="top">
                                                    <td valign="top">
                                                        <asp:Button runat="server" name="$action^setvalue%26node-ids%268%26content%26Add_PaymentLimits&amp;setvalue%26node-ids%268%26content%26AddLimit"
                                                            Text="     Add ->>   " class="button" Style="width: 100" ID="Add_PaymentLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForAdding('PaymentLimits');; " /><br />
                                                        <br />
                                                        <asp:Button runat="server" name="$action^setvalue%26node-ids%268%26content%26Remove_PaymentLimits&amp;setvalue%26node-ids%268%26content%26DeleteLimit"
                                                            Text="<<- Remove" class="button" Style="width: 100" ID="Remove_PaymentLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForDeleting('PaymentLimits');; " />
                                                        <asp:TextBox ID="tbPaymentLimits_SelectedUserGroup" runat="server" Style="display: none;"
                                                            RMXRef="/Instance/Document/Data/PaymentLimits/SelectedUserGroup[ @name = 'PaymentLimits_SelectedUserGroup' ]"></asp:TextBox>
                                                    </td>
                                                 </tr>
                                                 <tr>
                                                    <td>&nbsp;<br />&nbsp;</td>
                                                 </tr>
                                                    <td valign="top" width="30%">
                                                        Max Amount:
                                                    </td>
                                                    <td valign="top">
                                                        <asp:TextBox ID="PaymentMax" runat="server" RMXRef="/Instance/Document/Data/PaymentLimits/MaxAmount[ @name = 
'PaymentMax' ]" type="numeric" name="$node^28" value="" size="30"></asp:TextBox>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                     <%--Added by amitosh for R8 enhancement Supervisory Approval--%>
                                                    <td>
                                                    <asp:Label ID="lbPaymentLimits_ClaimType" runat="server" Text="Claim Type:"></asp:Label>
                                                    </td>
                                                    <td>
                                                    <uc1:CodeLookUp runat="server" CodeTable="CLAIM_TYPE" ControlName="PaymentLimitsClaimType" ID="PaymentLimitsClaimType"  RMXref="/Instance/Document/Data/PaymentLimits/ClaimType[@name = 'PaymentLimitsClaimType']"/>
                                                    </td>
                                                </tr>
                                            </table>
                                            </div>
                                        </td>
                                        <td width="60%" valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="80%">
                                                        <div style="width: 100%; height: 250;">
                                                            <dg:UserControlDataGrid runat="server" ID="PaymentLimitsGrid" GridName="PaymentLimitsGrid"
                                                                HideButtons="New|Edit|Delete" Target="/Document/Data/form/group/displaycolumn/PaymentLimitsList"
                                                                Ref="/Document/Data/form/group/displaycolumn/PaymentLimitsList[ @name ='PaymentLimitsGrid']"
                                                                Unique_Id="RowId" ShowRadioButton="true" Width="680px" Height="150px" HideNodes="|RowId|"
                                                                ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" Type="Grid"
                                                                RowDataParam="listrow" />
                                                            <asp:TextBox ID="tbPaymentLimitsGrid" Style="display: none" runat="server" rmxignoreset="true"
                                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/PaymentLimitsList[ @name ='tbPaymentLimitsGrid']"></asp:TextBox>
                                                            <asp:TextBox ID="tbPaymentLimitsGridGroup" Style="display: none" runat="server" Text="Group/User"
                                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/PaymentLimitsList/listhead/Group[ @name ='tbPaymentLimitsGridGroup']"></asp:TextBox>
                                                            <asp:TextBox ID="tbPaymentLimitsGridMaxAmount" Style="display: none" runat="server"
                                                                Text="Max Amount" RMXref="/Instance/Document/Data/form/group/displaycolumn/PaymentLimitsList/listhead/Max[ @name ='tbPaymentLimitsGridMaxAmount']"></asp:TextBox>
                                                                <%--Added by amitosh for R8 enhancement Supervisory Approval--%>
                                                                <asp:TextBox ID="tbPaymentLimitsGridClaimType" Style="display: none" runat="server"
                                                                Text="Claim Type" RMXref="/Instance/Document/Data/form/group/displaycolumn/PaymentLimitsList/listhead/ClaimType[ @name ='tbPaymentLimitsGridClaimtype']"></asp:TextBox>
                                                               
                                                            <asp:TextBox ID="tbPaymentLimitsGridRowId" Style="display: none" runat="server" Text="RowId"
                                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/PaymentLimitsList/listhead/RowId[ @name ='tbPaymentLimitsGridRowId']"></asp:TextBox>
                                                            <asp:TextBox Style="display: none" runat="server" ID="PaymentLimitsGrid_Action" RMXType="id" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox runat="server" Text="Enable Payment Limits" name="$node^39" appearance="full"
                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblPaymentLimits' ]"
                                                ID="PaymentLimits_EnableLimits" onclick="return SaveEnableDiableLimits('PaymentLimits_EnableLimits','Are you sure you wish to change the Enable Payment Limits Option?')" />
                                            <asp:TextBox ID="tbEnablePaymentLimits" runat="server" RMXRef="/Instance/Document/Data/PaymentLimits/EnableLimits[ @name = 'EnableResLimits' ]"
                                                Style="display: none"></asp:TextBox>
                                        </td>
                                    </tr>
                     <%--Asharma326 MITS 30874--%>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox runat="server" Text="Restrict Unspecified Users" name="$node^39" appearance="full"
                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblPaymentLimitsUsers' ]"
                                    ID="PaymentLimitsUsers_EnableLimits" onclick="return SaveEnableDiableLimits('PaymentLimitsUsers_EnableLimits','Are you sure you wish to change the Restrict Unspecified Users Option?')" />
                                <asp:TextBox ID="tbEnablePaymentLimitsUsers" runat="server" RMXRef="/Instance/Document/Data/PaymentLimits/EnableLimitsUsers[ @name = 'EnableResLimitsUsers' ]"
                                    Style="display: none"></asp:TextBox>
                            </td>
                        </tr>
                    <%--Asharma326 MITS 30874--%>
                                </table>
                                <table border="0" cellspacing="0" runat="server" celpadding="0" width="100%" name="FORMTABPayDetailLimits"
                                            id="FORMTABPayDetailLimits" style="display: none;">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" valign="top" border="0">
                                                <tr>
                                                    <td valign="top" colspan="2">
                                                        <asp:Button runat="server" name="$action^setvalue%26node-ids%268%26content%26Add_PayDetailLimits&amp;setvalue%26node-ids%268%26content%26AddLimit"
                                                            Text="     Add ->>   " class="button" Style="width: 100" ID="Add_PayDetailLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForAdding('PayDetailLimits');; " /><br />
                                                        <br />
                                                        <asp:Button runat="server" name="$action^setvalue%26node-ids%268%26content%26Remove_PayDetailLimits&amp;setvalue%26node-ids%268%26content%26DeleteLimit"
                                                            Text="<<- Remove" class="button" Style="width: 100" ID="Remove_PayDetailLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForDeleting('PayDetailLimits');; " />
                                                        <asp:TextBox ID="tbPayDetailLimits_SelectedUserGroup" runat="server" Style="display: none;"
                                                                     RMXRef="/Instance/Document/Data/PayDetailLimits/SelectedUserGroup[ @name = 'PayDetailLimits_SelectedUserGroup' ]"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;<br />&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="30%">
                                                        <b><u>Reserve Type:</u></b>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList runat="server" ID="PayDetailLimits_ReserveType" RMXRef="/Instance/Document/Data/form/displaycolumn/control[ @name = 
'ReserveType' ]/@value" ItemSetRef="/Instance/Document/Data/form/displaycolumn/control[ @name = 'ReserveType' ]">
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID="tbPayDetailLimits_ReserveType" runat="server" Style="display: none;"
                                                            RMXRef="/Instance/Document/Data/PayDetailLimits/ReserveType[ @name = 
'PayDetailLimitsReserveType' ]"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="30%">
                                                        Max Amount:
                                                    </td>
                                                    <td valign="top">
                                                        <asp:TextBox runat="server" ID="PayDetailMax" RMXRef="/Instance/Document/Data/PayDetailLimits/MaxAmount[ @name = 
'PayDetailMax' ]" type="numeric" name="$node^28" value="" size="30"></asp:TextBox>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                     <%--Added by amitosh for R8 enhancement Supervisory Approval--%>
                                                    <td>
                                                    <asp:Label ID="lbPayDetailLimits_ClaimType" runat="server" Text="Claim Type:"></asp:Label>
                                                    </td>
                                                    <td>
                                                    <uc1:CodeLookUp runat="server" CodeTable="CLAIM_TYPE" ControlName="PayDetailLimitsClaimType" ID="PayDetailLimitsClaimType"  RMXref="/Instance/Document/Data/PayDetailLimits/ClaimType[@name = 'PayDetailLimitsClaimType']"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="60%">
                                            <div style="width: 100%; height: 250; overflow: auto;">
                                                <dg:UserControlDataGrid runat="server" ID="PayDetailLimitsGrid" GridName="PayDetailLimitsGrid"
                                                    HideButtons="New|Edit|Delete" Target="/Document/Data/form/group/displaycolumn/PayDetailLimitsList"
                                                    Ref="/Document/Data/form/group/displaycolumn/PayDetailLimitsList[ @name ='PayDetailLimitsList']"
                                                    Unique_Id="RowId" ShowRadioButton="true" Width="680px" Height="150px" HideNodes="|RowId|"
                                                    ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" Type="Grid"
                                                    RowDataParam="listrow" />
                                                <asp:TextBox ID="tbPayDetailLimitsGrid" Style="display: none" runat="server" rmxignoreset="true"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/PayDetailLimitsList[ @name ='txtReserveGrid']"></asp:TextBox>
                                                <asp:TextBox ID="tbPayDetailLimitsGridGroup" Style="display: none" runat="server"
                                                    Text="Group/User" RMXref="/Instance/Document/Data/form/group/displaycolumn/PayDetailLimitsList/listhead/Group[ @name ='tbPayDetailLimitsGridGroup']"></asp:TextBox>
                                                <asp:TextBox ID="tbPayDetailLimitsGridResType" Style="display: none" runat="server"
                                                    Text="Res Type" RMXref="/Instance/Document/Data/form/group/displaycolumn/PayDetailLimitsList/listhead/ResType[ @name ='tbPayDetailLimitsGridResType']"></asp:TextBox>
                                                <asp:TextBox ID="tbPayDetailLimitsGridMax" Style="display: none" runat="server" Text="Max Amount"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/PayDetailLimitsList/listhead/Max[ @name ='tbPayDetailLimitsGridMax']"></asp:TextBox>
                                                                 <%--Added by amitosh for R8 enhancement Supervisory Approval--%>
                                                                 <asp:TextBox ID="tbPayDetailLimitsGridClaimType" Style="display: none" runat="server" Text="Claim Type"
                                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/PayDetailLimitsList/listhead/ClaimType[ @name ='tbPayDetailLimitsGridClaimType']"></asp:TextBox>
                                                <asp:TextBox ID="tbPayDetailLimitsRowId" Style="display: none" runat="server" Text="RowId"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/PayDetailLimitsList/listhead/RowId[ @name ='tbPayDetailLimitsGridRowId']"></asp:TextBox>
                                                <asp:TextBox Style="display: none" runat="server" ID="PayDetailLimitsGrid_Action"
                                                    RMXType="id" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox name="$node^45" runat="server" Text="Enable Payment Detail Limits"
                                                appearance="full" RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblPaymentDetailLimits' ]"
                                                ID="PayDetailLimits_EnableLimits" onclick="return SaveEnableDiableLimits('PayDetailLimits_EnableLimits','Are you sure you wish to change the Enable Payment Detail Limits Option?')" />
                                            <asp:TextBox ID="tbEnablePayDetailLimits" runat="server" RMXRef="/Instance/Document/Data/PayDetailLimits/EnableLimits[ @name = 'EnablePayDetailLimits' ]"
                                                Style="display: none"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <%--asharma326 MITS 30874--%>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox name="$node^45" runat="server" Text="Restrict Unspecified Users"
                                    appearance="full" RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblPaymentDetailLimitsUsers' ]"
                                    ID="PayDetailLimitsUsers_EnableLimits" onclick="return SaveEnableDiableLimits('PayDetailLimitsUsers_EnableLimits','Are you sure you wish to change the Restrict Unspecified Users Option?')" />
                                <asp:TextBox ID="tbEnablePayDetailLimitsUsers" runat="server" RMXRef="/Instance/Document/Data/PayDetailLimits/EnableLimitsUsers[ @name = 'EnablePayDetailLimitsUsers' ]"
                                    Style="display: none"></asp:TextBox>
                            </td>
                        </tr>
                                    <%--asharma326 MITS 30874--%>
                                </table>
                                <table border="0" cellspacing="0" runat="server" celpadding="0" width="100%" name="FORMTABClaimLimits"
                                        id="FORMTABClaimLimits" style="display:none;">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" valign="top" border="0">
                                                <tr>
                                                    <td valign="top" colspan="2">                                                                                                       
                                                        <asp:Button runat="server" name="Add_ClaimLimits"
                                                            Text="     Add ->>   " class="button" Style="width: 100" ID="Add_ClaimLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForAdding('ClaimLimits');; " /><br />
                                                        <br />
                                                        <asp:Button runat="server" name="Remove_ClaimLimits"
                                                            Text="<<- Remove" class="button" Style="width: 100" ID="Remove_ClaimLimits" OnClientClick="return ValidateUserPrivilegesForDeleting('ClaimLimits');; " />
                                                        <asp:TextBox ID="tbClaimLimits_SelectedUserGroup" runat="server" Style="display: none;"
                                                                        RMXRef="/Instance/Document/Data/ClaimLimits/SelectedUserGroup[ @name = 'ClaimLimits_SelectedUserGroup' ]"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;<br />&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <b><u>Claim Type:</u></b>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox type="code" runat="server" RMXref="/Instance/Document/Data/ClaimLimits/ClaimType[@name = 'CodeName']"
                                                            name="$node^49" size="30" onblur="return codeLostFocus(this.id);" onchange="return lookupTextChanged(this);"
                                                            ID="ClaimLimits_ClaimType"></asp:TextBox>
                                                            <%--bkumar33:cosmetic changes--%>
                                                            <%--Changed by Amitosh for R8 enhancement--%>
                                                      <%--  <asp:Button class="EllipsisControl" runat="server" ID="ClaimLimits_ClaimTypebtn" OnClientClick="return selectCode('CLAIM_TYPE','ClaimLimits_ClaimType');"
                                                            Text="..." />--%>
                                                            <asp:Button class="CodeLookupControl" runat="server" ID="ClaimLimits_ClaimTypebtn" OnClientClick="return selectCode('CLAIM_TYPE','ClaimLimits_ClaimType');"/>
                                                        <asp:TextBox runat="server" RMXref="/Instance/Document/Data/ClaimLimits/ClaimType[@name = 'CodeName']/@codeid"
                                                            name="$node^50" value="" Style="display: none" ID="ClaimLimits_ClaimType_cid"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="30%">
                                                        <b><u>Claim Status:</u></b>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox type="code" runat="server" RMXref="/Instance/Document/Data/ClaimLimits/ClaimStatus[@name = 'CodeName']"
                                                            name="$node^51" size="30" onblur="return codeLostFocus(this.id);" onchange="return lookupTextChanged(this);"
                                                            ID="ClaimLimits_ClaimStatus"></asp:TextBox>
                                                            <%--bkumar33:cosmetic changes--%>
                                                            <%--Changed by Amitosh for R8 Enhancement--%>
                                                       <%-- <asp:Button runat="server" class="EllipsisControl" ID="ClaimLimits_ClaimStatusbtn" OnClientClick="return selectCode('CLAIM_STATUS','ClaimLimits_ClaimStatus');"
                                                            Text="..." />--%>
                                                             <asp:Button runat="server" class="CodeLookupControl" ID="ClaimLimits_ClaimStatusbtn" OnClientClick="return selectCode('CLAIM_STATUS','ClaimLimits_ClaimStatus');"/>
                                                        <asp:TextBox runat="server" RMXref="/Instance/Document/Data/ClaimLimits/ClaimStatus[@name = 'CodeName']/@codeid"
                                                            name="$node^52" value="" Style="display: none" ID="ClaimLimits_ClaimStatus_cid"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" colspan="2">
                                                        <asp:CheckBox name="ClaimLimits_AllowCreate" value="True" appearance="full" RMXref="/Instance/Document/Data/ClaimLimits/AllowCreate"
                                                            ID="ClaimLimits_AllowCreate" Text="Allow Create" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" colspan="2">
                                                        <asp:CheckBox name="ClaimLimits_AllowAccess" value="True" appearance="full" RMXref="/Instance/Document/Data/ClaimLimits/AllowAccess"
                                                            ID="ClaimLimits_AllowAccess" Text="Allow Access" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top">
                                            <div style="width: 100%; height: 250; overflow: auto;">
                                                <dg:UserControlDataGrid runat="server" ID="ClaimLimitsGrid" GridName="ClaimLimitsGrid"
                                                    HideButtons="New|Edit|Delete" Target="/Document/Data/form/group/displaycolumn/ClaimLimitsList"
                                                    Ref="/Document/Data/form/group/displaycolumn/ClaimLimitsList[ @name ='ClaimLimitsList']"
                                                    Unique_Id="RowId" ShowRadioButton="true" Width="680px" Height="150px" HideNodes="|RowId|"
                                                    ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" Type="Grid"
                                                    RowDataParam="listrow" />
                                                <asp:TextBox ID="tbClaimLimitsGrid" Style="display: none" runat="server" rmxignoreset="true"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimLimitsList[ @name ='tbClaimLimitsGrid']"></asp:TextBox>
                                                <asp:TextBox ID="tbClaimLimitsGridGroup" Style="display: none" runat="server" Text="Group/User"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimLimitsList/listhead/Group[ @name ='tbClaimLimitsGridGroup']"></asp:TextBox>
                                                <asp:TextBox ID="tbClaimLimitsGridClaimType" Style="display: none" runat="server"
                                                    Text="Claim Type" RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimLimitsList/listhead/ClaimType[ @name ='tbClaimLimitsGridClaimType']"></asp:TextBox>
                                                <asp:TextBox ID="tbClaimLimitsGridClaimStatus" Style="display: none" runat="server"
                                                    Text="Claim Status" RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimLimitsList/listhead/ClaimStatus[ @name ='tbClaimLimitsGridClaimStatus']"></asp:TextBox>
                                                <asp:TextBox ID="tbClaimLimitsGridClaimAccess" Style="display: none" runat="server"
                                                    Text="Access" RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimLimitsList/listhead/Access[ @name ='tbClaimLimitsGridClaimAccess']"></asp:TextBox>
                                                <asp:TextBox ID="tbClaimLimitsGridClaimCreate" Style="display: none" runat="server"
                                                    Text="Create" RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimLimitsList/listhead/Create[ @name ='tbClaimLimitsGridClaimCreate']"></asp:TextBox>
                                                <asp:TextBox ID="tbClaimLimitsGridRowId" Style="display: none" runat="server" Text="RowId"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimLimitsList/listhead/RowId[ @name ='tbClaimLimitsGridRowId']"></asp:TextBox>
                                                <asp:TextBox Style="display: none" runat="server" ID="ClaimLimitsGrid_Action" RMXType="id" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox name="$node^55" runat="server" appearance="full" RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblClaimLimits' ]"
                                                ID="ClaimLimits_EnableLimits" Text="Enable Claim Limits" onclick="return SaveEnableDiableLimits('ClaimLimits_EnableLimits','Are you sure you wish to change the Enable Claim Access Limits Option?')" />
                                            <asp:TextBox ID="tbEnableClaimLimits" runat="server" RMXRef="/Instance/Document/Data/ClaimLimits/EnableLimits[ @name = 'EnablePayDetailLimits' ]"
                                                Style="display: none"></asp:TextBox>
                                            <asp:Label ID="lblClaimNote" Text="Note: Enter Claim Type or Claim Status or a combination of the two."
                                                runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                     <%--asharma326 MITS 30874--%>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox name="$node^55" runat="server" appearance="full" RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblClaimLimitsUsers' ]"
                                    ID="ClaimLimitsUsers_EnableLimits" Text="Restrict Unspecified Users" onclick="return SaveEnableDiableLimits('ClaimLimitsUsers_EnableLimits','Are you sure you wish to change the Restrict Unspecified Users Option?')" />
                                <asp:TextBox ID="tbEnableClaimLimitsUsers" runat="server" RMXRef="/Instance/Document/Data/ClaimLimits/EnableLimitsUsers[ @name = 'EnablePayDetailLimitsUsers' ]"
                                    Style="display: none"></asp:TextBox>
                            </td>
                        </tr>
                                    <%--asharma326 MITS 30874--%>
                                </table>
                                <table border="0" cellspacing="0" runat="server" celpadding="0" width="100%" name="FORMTABEventTypeLimits"
                                        id="FORMTABEventTypeLimits" style="display: none;">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" valign="top" border="0">
                                                <tr>
                                                    <td valign="top" colspan="2">
                                                        <asp:Button runat="server" name="$action^setvalue%26node-ids%268%26content%26Add_EventTypeLimits&amp;setvalue%26node-ids%268%26content%26AddLimit"
                                                            Text="     Add ->>   " class="button" Style="width: 100" ID="Add_EventTypeLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForAdding('EventTypeLimits');; " /><br />
                                                        <br />
                                                        <asp:Button runat="server" name="$action^setvalue%26node-ids%268%26content%26Remove_ClaimLimits&amp;setvalue%26node-ids%268%26content%26DeleteLimit"
                                                            Text="<<- Remove" class="button" Style="width: 100" ID="Remove_EventTypeLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForDeleting('EventTypeLimits');; " />
                                                        <asp:TextBox ID="tbEventTypeLimits_SelectedUserGroup" runat="server" Style="display: none;"
                                                             RMXRef="/Instance/Document/Data/EventTypeLimits/SelectedUserGroup[ @name = 'EventTypeLimits_SelectedUserGroup' ]"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;<br />&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <b><u>Event Type:</u></b>&nbsp;&nbsp;
                                                        <asp:TextBox type="code" RMXref="/Instance/Document/Data/EventTypeLimits/EventType[@name = 'CodeName']"
                                                            runat="server" name="$node^59" value="" size="30" onblur="return codeLostFocus(this.id);"
                                                            onchange="return lookupTextChanged(this);" ID="EventTypeLimits_EventType"></asp:TextBox>
                                                            <%--bkumar33:cosmetic changes--%>
                                                            <%--Changed by Amitosh for R8 Enhancement--%>
                                                        <%--<asp:Button class="EllipsisControl" runat="server" ID="EventTypeLimits_EventTypebtn" OnClientClick="return selectCode('EVENT_TYPE','EventTypeLimits_EventType');"
                                                            Text="..." />--%>
                                                             <asp:Button class="CodeLookupControl" runat="server" ID="EventTypeLimits_EventTypebtn" OnClientClick="return selectCode('EVENT_TYPE','EventTypeLimits_EventType');"/>
                                                        <asp:TextBox runat="server" RMXref="/Instance/Document/Data/EventTypeLimits/EventType[@name = 'CodeName']/@codeid"
                                                            name="$node^60" value="" Style="display: none" ID="EventTypeLimits_EventType_cid"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" colspan="2">
                                                        <asp:CheckBox name="EventTypeLimits_AllowCreate" value="True" appearance="full" RMXref="/Instance/Document/Data/EventTypeLimits/AllowCreate"
                                                            ID="EventTypeLimits_AllowCreate" runat="server" Text="Allow Create" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" colspan="2">
                                                        <asp:CheckBox name="EventTypeLimits_AllowAccess" value="True" appearance="full" RMXref="/Instance/Document/Data/EventTypeLimits/AllowAccess"
                                                            ID="EventTypeLimits_AllowAccess" runat="server" Text="Allow Access" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="60%" valign="top">
                                            <div style="&#xa; width: 100%; height: 250; overflow: auto&#34; &#xa;">
                                                <dg:UserControlDataGrid runat="server" ID="EventTypeLimitsGrid" GridName="EventTypeLimitsGrid"
                                                    HideButtons="New|Edit|Delete" Target="/Document/Data/form/group/displaycolumn/EventTypeList"
                                                    Ref="/Document/Data/form/group/displaycolumn/EventTypeList[ @name ='EventTypeList']"
                                                    Unique_Id="RowId" ShowRadioButton="true" Width="680px" Height="150px" HideNodes="|RowId|"
                                                    ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" Type="Grid"
                                                    RowDataParam="listrow" />
                                                <asp:TextBox ID="tbEventTypeGrid" Style="display: none" runat="server" rmxignoreset="true"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/EventTypeList[ @name ='tbEventTypeGrid']"></asp:TextBox>
                                                <asp:TextBox ID="tbEventTypeGridGroup" Style="display: none" runat="server" Text="Group/User"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/EventTypeList/listhead/Group[ @name ='tbEventTypeGridGroup']"></asp:TextBox>
                                                <asp:TextBox ID="tbEventTypeGridEventType" Style="display: none" runat="server" Text="Event Type"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/EventTypeList/listhead/EventType[ @name ='tbEventTypeGridEventType']"></asp:TextBox>
                                                <asp:TextBox ID="tbEventTypeGridAccess" Style="display: none" runat="server" Text="Access"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/EventTypeList/listhead/Access[ @name ='tbEventTypeGridAccess']"></asp:TextBox>
                                                <asp:TextBox ID="tbEventTypeGridCreate" Style="display: none" runat="server" Text="Create"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/EventTypeList/listhead/Create[ @name ='tbEventTypeGridCreate']"></asp:TextBox>
                                                <asp:TextBox ID="tbEventTypeGridRowId" Style="display: none" runat="server" Text="RowId"
                                                    RMXref="/Instance/Document/Data/form/group/displaycolumn/EventTypeList/listhead/RowId[ @name ='tbEventTypeGridRowId']"></asp:TextBox>
                                                <asp:TextBox Style="display: none" runat="server" ID="EventTypeGrid_Action" RMXType="id" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox runat="server" name="$node^63" appearance="full" RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblEventTypeLimits' ]"
                                                ID="EventTypeLimits_EnableLimits" Text="Enable Event Type Limits" onclick="return SaveEnableDiableLimits('EventTypeLimits_EnableLimits','Are you sure you wish to change the Enable Event Type Limits Option?')" />
                                            <asp:TextBox ID="tbEnableEventTypeLimits" runat="server" RMXRef="/Instance/Document/Data/EventTypeLimits/EnableLimits[ @name = 'EnblEventTypeLimits' ]"
                                                Style="display: none"></asp:TextBox>
                                            <asp:Label ID="eventtypenote" Text=" Note: Event Type Limits Are Not Linked to the Line of Business."
                                                runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                     <%--Asharma326 MITS 30874--%>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox runat="server" name="$node^63" appearance="full" RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblEventTypeLimitsUsers' ]"
                                    ID="EventTypeLimitsUsers_EnableLimits" Text="Restrict Unspecified Users" onclick="return SaveEnableDiableLimits('EventTypeLimitsUsers_EnableLimits','Are you sure you wish to change the Restrict Unspecified Users Option?')" />
                                <asp:TextBox ID="tbEnableEventTypeLimitsUsers" runat="server" RMXRef="/Instance/Document/Data/EventTypeLimits/EnableLimitsUsers[ @name = 'EnblEventTypeLimitsUsers' ]"
                                    Style="display: none"></asp:TextBox>
                            </td>
                        </tr>
                                    <%--Asharma326 MITS 30874--%>
                                </table>
                                <table border="0" cellspacing="0" runat="server" celpadding="0" width="100%" name="FORMTABPerClaimPayLimits"
                                        id="FORMTABPerClaimPayLimits" style="display: none;">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" valign="top" border="0">
                                                <tr>
                                                    <td valign="top" colspan="2">
                                                        <asp:Button runat="server" name="$action^setvalue%26node-ids%268%26content%26Add_PerClaimPayLimits&amp;setvalue%26node-ids%268%26content%26AddLimit"
                                                            Text="     Add ->>   " class="button" Style="width: 100" ID="Add_PerClaimPayLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForAdding('PerClaimPayLimits');; " /><br />
                                                        <br />
                                                        <asp:Button runat="server" name="$action^setvalue%26node-ids%268%26content%26Remove_PerClaimPayLimits&amp;setvalue%26node-ids%268%26content%26DeleteLimit"
                                                            Text="<<- Remove" class="button" Style="width: 100" ID="Remove_PerClaimPayLimits"
                                                            OnClientClick="return ValidateUserPrivilegesForDeleting('PerClaimPayLimits');; " />
                                                        <asp:TextBox ID="tbPerClaimPayLimits_SelectedUserGroup" runat="server" Style="display: none;"
                                                                        RMXRef="/Instance/Document/Data/PerClaimPayLimits/SelectedUserGroup[ @name = 'PerClaimPayLimits_SelectedUserGroup' ]"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;<br />&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <b><u>Reserve Type:</u></b>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList runat="server" ID="PerClaimPayLimits_ReserveType" RMXRef="/Instance/Document/Data/form/displaycolumn/control[ @name = 'ReserveType' ]/@value"
                                                            ItemSetRef="/Instance/Document/Data/form/displaycolumn/control[ @name = 'ReserveType' ]">
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID="tbPerClaimPayLimits_ReserveType" runat="server" Style="display: none;"
                                                            RMXRef="/Instance/Document/Data/PerClaimPayLimits/ReserveType[ @name = 'PerClaimPayLimitsReserveType' ]"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <b><u>Max Amount:</u></b>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:TextBox runat="server" ID="PerClaimPayLimitsMaxAmount" RMXRef="/Instance/Document/Data/PerClaimPayLimits/MaxAmount[ @name = 'ReserveMax' ]" type="numeric" name="$node^28" value="" size="10"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <b><u>Claim Type:</u></b>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" name="$node^69" value="" RMXref="/Instance/Document/Data/PerClaimPayLimits/ClaimType[@name = 'CodeName']"
                                                            size="30" onblur="return codeLostFocus(this.id);" onchange="return lookupTextChanged(this);"
                                                            ID="PerClaimPayLimits_ClaimType"></asp:TextBox>
                                                            <%--bkumar33:cosmetic changes--%>
                                                            <%--Changed by Amitosh for R8 Enhancement--%>
                                                    <%--    <asp:Button class="EllipsisControl" runat="server" ID="PerClaimPayLimits_ClaimTypebtn" OnClientClick="return selectCode('CLAIM_TYPE','PerClaimPayLimits_ClaimType');"
                                                            Text="..." />--%>
                                                                <asp:Button class="CodeLookupControl" runat="server" ID="PerClaimPayLimits_ClaimTypebtn" OnClientClick="return selectCode('CLAIM_TYPE','PerClaimPayLimits_ClaimType');"/>
                                                        <asp:TextBox runat="server" name="$node^70" RMXref="/Instance/Document/Data/PerClaimPayLimits/ClaimType[@name = 'CodeName']/@codeid"
                                                            value="" Style="display: none" ID="PerClaimPayLimits_ClaimType_cid"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                <td>
                                                Has OverRide Authority:
                                                </td>
                                                <td>
                                                <asp:CheckBox name="$node^54" value="True" appearance="full" RMXref="/Instance/Document/Data/PerClaimPayLimits/HasOverRideAuthority"
                                                            ID="HasOverRideAuthority" onclick ="javascript:PreventPayAboveLmtChk();" runat="server" /> <%-- Added by Gaurav for MITS 30226. --%> 
                                                </td>
                                    <td>
                                        Prevent Payments Above Limits:
                                    </td>

                                    <td>
                                        <asp:CheckBox name="$node^54" value="True" appearance="full" RMXref="/Instance/Document/Data/PerClaimPayLimits/PreventPayAboveLmt"
                                            ID="PreventPayAboveLmt" onclick ="javascript:HasOverRideAuthorityChk();" runat="server"/>  <%-- Added by Gaurav for MITS 30226. --%> 
                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="60%" valign="top">
                                            <div style="overflow:auto;">
                                                <dg:UserControlDataGrid runat="server" ID="PerClaimPayLimitsGrid" GridName="PerClaimPayLimitsGrid"
                                                    HideButtons="New|Edit|Delete" Target="/Document/Data/form/group/displaycolumn/PerClaimPayLimitsList"
                                                    Ref="/Document/Data/form/group/displaycolumn/PerClaimPayLimitsList[ @name ='PerClaimPayLimitsList']"
                                                    Unique_Id="RowId" ShowRadioButton="true" Width="680px" Height="150px" HideNodes="|RowId|"
                                                    ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" Type="Grid"
                                                    RowDataParam="listrow" />
                                                <asp:TextBox ID="tbPerClaimPayLimitsGrid" Style="display: none" rmxignoreset="true"
                                                    runat="server" RMXref="/Instance/Document/Data/form/group/displaycolumn/PerClaimPayLimitsList[ @name ='tbPerClaimPayLimitsGrid']"></asp:TextBox>
                                                <asp:TextBox ID="tbPerClaimPayLimitsGridGroup" Style="display: none" runat="server"
                                                    Text="Group/User" RMXref="/Instance/Document/Data/form/group/displaycolumn/PerClaimPayLimitsList/listhead/Group[ @name ='tbPerClaimPayLimitsGridGroup']"></asp:TextBox>
                                                <asp:TextBox ID="tbPerClaimPayLimitsGridResType" Style="display: none" runat="server"
                                                    Text="Res Type" RMXref="/Instance/Document/Data/form/group/displaycolumn/PerClaimPayLimitsList/listhead/ResType[ @name ='tbPerClaimPayLimitsGridResType']"></asp:TextBox>
                                                <asp:TextBox ID="tbPerClaimPayLimitsGridMax" Style="display: none" runat="server"
                                                    Text="Max Amount" RMXref="/Instance/Document/Data/form/group/displaycolumn/PerClaimPayLimitsList/listhead/Max[ @name ='tbPerClaimPayLimitsGridMax']"></asp:TextBox>
                                                            <asp:TextBox ID="tbPerClaimPayLimitsGridClaimType" Style="display: none" runat="server"
                                                                Text="Claim Type" RMXref="/Instance/Document/Data/form/group/displaycolumn/PerClaimPayLimitsList/listhead/ClaimType[ @name ='tbPerClaimPayLimitsGridClaimType']"></asp:TextBox>
                                                                <%--Added by Amitosh for R8 enhancement of OverRide Authority--%>
                                                                  <asp:TextBox ID="tbPerClaimPayLimitsGridOverRideAuthority" Style="display: none" runat="server"
                                                                Text="OverRide Authority" RMXref="/Instance/Document/Data/form/group/displaycolumn/PerClaimPayLimitsList/listhead/OverRideAuthority[ @name ='tbPerClaimPayLimitsGridOverRideAuthority']"></asp:TextBox>
                                <%--Added by Gaurav --%>
                                <asp:TextBox ID="tbPerClaimPayLimitsGridPreventPaymentsAboveLimit" Style="display: none"
                                    runat="server" Text="Prevent AboveLmtPayments" RMXref="/Instance/Document/Data/form/group/displaycolumn/PerClaimPayLimitsList/listhead/PreventPaymentAboveLimit[ @name ='tbPerClaimPayLimitsGridPreventPaymentsAboveLimit']"></asp:TextBox>
                                <asp:TextBox ID="tbPerClaimPayLimitsGridRowId" Style="display: none" runat="server"
                                                                Text="RowId" RMXref="/Instance/Document/Data/form/group/displaycolumn/PerClaimPayLimitsList/listhead/RowId[ @name ='tbPerClaimPayLimitsGridRowId']"></asp:TextBox>
                                                            <asp:TextBox Style="display: none" runat="server" ID="PerClaimPayLimitsGrid_Action"
                                                    RMXType="id" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox runat="server" RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblPerClaimPaymentLimits' ]"
                                                ID="PerClaimPayLimits_EnableLimits" Onclick="return SaveEnableDiableLimits('PerClaimPayLimits_EnableLimits','Are you sure you wish to change the Enable Per Claim Payment Limits Option?')"
                                                Text="Enable Per Claim Pay Limits" /><!-- MITS 14677 : abansal 23 -->
                                            <asp:TextBox ID="tbEnablePerClaimPayLimits" runat="server" RMXRef="/Instance/Document/Data/PerClaimPayLimits/EnableLimits[ @name = 'EnblPerClaimPaymentLimits' ]"
                                                Style="display: none"></asp:TextBox><!-- MITS 14677 : abansal 23 -->
                                        </td>
                                    </tr>
                                     <%--Asharma326 MITS 30874--%>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox runat="server" RMXref="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblPerClaimPaymentLimitsUsers' ]"
                                    ID="PerClaimPayLimitsUsers_EnableLimits" Onclick="return SaveEnableDiableLimits('PerClaimPayLimitsUsers_EnableLimits','Are you sure you wish to change the Restrict Unspecified Users Option?')"
                                    Text="Restrict Unspecified Users" /><!-- MITS 14677 : abansal 23 -->
                                <asp:TextBox ID="tbEnablePerClaimPayLimitsUsers" runat="server" RMXRef="/Instance/Document/Data/PerClaimPayLimits/EnableLimitsUsers[ @name = 'EnblPerClaimPaymentLimitsUsers' ]"
                                    Style="display: none"></asp:TextBox><!-- MITS 14677 : abansal 23 -->
                            </td>
                        </tr>
                                    <%--Asharma326 MITS 30874--%>
                                </table>
                                <%--sharishkumar Jira 6385 starts--%>
                                <table border="0" cellspacing="0" runat="server" celpadding="0" width="100%" name="FORMTABClaimsTotalIncurredLimits" id="FORMTABClaimsTotalIncurredLimits">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <table>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Button runat="server" Text="     Add ->>   " class="button" Style="width: 100"
                                                            ID="Add_ClaimsTotalIncurredLimits" OnClientClick="return ValidateUserPrivilegesForAdding('ClaimsTotalIncurredLimits');; " /><br />
                                                        <br />
                                                        <asp:Button runat="server" Text="<<- Remove" class="button" Style="width: 100" ID="Remove_ClaimsTotalIncurredLimits" name="$action^setvalue%26node-ids%268%26content%26Remove_ClaimsTotalIncurredLimits&amp;setvalue%26node-ids%268%26content%26DeleteLimit"
                                                            OnClientClick="return ValidateUserPrivilegesForDeleting('ClaimsTotalIncurredLimits');; " />
                                                        <asp:TextBox ID="tbClaimsTotalIncurredLimits_SelectedUserGroup" runat="server" Style="display: none;"
                                                                        RMXRef="/Instance/Document/Data/ClaimsTotalIncurredLimits/SelectedUserGroup[ @name = 'ClaimsTotalIncurredLimits_SelectedUserGroup' ]"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;<br />&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b><u>Max Amount:</u></b>
                                                    </td>
                                                    <td>    
                                                        <asp:TextBox runat="server" ID="ClaimsTotalIncurredLimitsMaxAmount" RMXRef="/Instance/Document/Data/ClaimsTotalIncurredLimits/MaxAmount[ @name = 'ClaimsTotalIncurredLimitsMaxAmount' ]" type="numeric" name="ClaimsTotalIncurredLimitsMaxAmount" value="" size="10"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ClaimsTotalIncurredLimits_ClaimType" runat="server" Text="Claim Type:"></asp:Label>   
                                                    </td>
                                                    <%--Added by amitosh for R8 enhancement Supervisory Approval--%>
                                                    <td>
                                                    <uc1:CodeLookUp runat="server" CodeTable="CLAIM_TYPE" ControlName="ClaimsTotalIncurredLimitsClaimType" ID="ClaimsTotalIncurredLimitsClaimType"   RMXref="/Instance/Document/Data/ClaimsTotalIncurredLimits/ClaimType[@name = 'ClaimsTotalIncurredLimitsClaimType']"/>
                                                    </td>
                                                </tr>                                               
                                            </table>
                                        </td>
                                        <td>
                                            <dg:UserControlDataGrid runat="server" ID="ClaimsTotalIncurredLimitsGrid" GridName="ClaimsTotalIncurredLimitsGrid"
                                                HideButtons="New|Edit|Delete" Target="/Document/Data/form/group/displaycolumn/ClaimsTotalIncurredLimitsList"
                                                Ref="/Document/Data/form/group/displaycolumn/ClaimsTotalIncurredLimitsList[ @name ='ClaimsTotalIncurredLimitsGrid']"
                                                Unique_Id="RowId" ShowRadioButton="true" Width="680px" Height="150px" HideNodes="|RowId|"
                                                ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" Type="Grid"
                                                RowDataParam="listrow" />
                                            <asp:TextBox ID="tbClaimsTotalIncurredGrid" Style="display: none" runat="server" rmxignoreset="true"
                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimsTotalIncurredLimitsList[ @name ='tbClaimsTotalIncurredGrid']"></asp:TextBox>
                                            <asp:TextBox ID="tbClaimsTotalIncurredGridGroup" Style="display: none" runat="server" Text="Group/User"
                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimsTotalIncurredLimitsList/listhead/Group[ @name ='tbClaimsTotalIncurredGridGroup']"></asp:TextBox>                                            
                                            <asp:TextBox ID="tbClaimsTotalIncurredGridMax" Style="display: none" runat="server" Text="Max Amount"
                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimsTotalIncurredLimitsList/listhead/Max[ @name ='tbClaimsTotalIncurredGridMax']"></asp:TextBox>                                           
                                            <asp:TextBox ID="tbClaimsTotalIncurredGridClaimType" Style="display: none" runat="server" 
                                                Text="Claim Type" RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimsTotalIncurredLimitsList/listhead/ClaimType[ @name ='tbClaimsTotalIncurredGridClaimType']"></asp:TextBox>
                                            <asp:TextBox ID="tbClaimsTotalIncurredGridRowID" Style="display: none" runat="server" Text="RowId"
                                                RMXref="/Instance/Document/Data/form/group/displaycolumn/ClaimsTotalIncurredLimitsList/listhead/RowId[ @name ='tbClaimsTotalIncurredGridRowID']"></asp:TextBox>
                                            <asp:TextBox Style="display: none" runat="server" ID="ClaimsTotalIncurredLimits_Action" RMXType="id" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><hr /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox runat="server" RMXRef="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblClaimsTotalIncurredLimits' ]"
                                                ID="ClaimsTotalIncurredLimits_EnableLimits" Text="Enable Per Claim Incurred Limits" Onclick="return SaveEnableDiableLimits('ClaimsTotalIncurredLimits_EnableLimits','Are you sure you wish to change the Enable Per Claim Incurred Limits Option?')" />
                                            <asp:TextBox ID="tbEnableClaimsTotalIncurredLimits" runat="server" RMXRef="/Instance/Document/Data/ClaimsTotalIncurredLimits/EnableLimits[ @name = 'EnableClaimsTotalIncurredLimits' ]"
                                                Style="display: none"></asp:TextBox>
                                        </td>
                                    </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:CheckBox runat="server" RMXRef="/Instance/Document/Data/form/group/displaycolumn/control[ @name = 'EnblClaimsTotalIncurredLimitsUsers' ]"
                                                    ID="ClaimsTotalIncurredLimitsUsers_EnableLimits" Text="Restrict Unspecified Users" Onclick="return SaveEnableDiableLimits('ClaimsTotalIncurredLimitsUsers_EnableLimits','Are you sure you wish to change the Restrict Unspecified Users Option?')" />
                                                <asp:TextBox ID="tbEnableClaimsTotalIncurredLimitsUsers" runat="server" RMXRef="/Instance/Document/Data/ClaimsTotalIncurredLimits/EnableLimitsUsers[ @name = 'EnblClaimsTotalIncurredLimitsUser' ]"
                                                    Style="display: none"></asp:TextBox>
                                            </td>
                                        </tr>
                                </table>
                                <%--sharishkumar Jira 6385 ends--%>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>   
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    <asp:HiddenField runat="server" ID="TabNameList" Value="TABSReserveLimits|TABSPrintCheckLimits|TABSPaymentLimits|TABSPayDetailLimits|TABSClaimLimits|TABSEventTypeLimits|TABSPerClaimPayLimits|TABSClaimsTotalIncurredLimits" /><%--sharishkumar jira 6385 --%>
     <%--Mgaba2: R8:SuperVisory Approval--%>
     <asp:TextBox type="hidden"  style="display:none" runat ="server" ID ="hdnAdvancedClaims" RMXref="/Instance/Document/Data/control[ @name = 'hdnAdvancedClaims']" />

    <asp:HiddenField runat ="server" ID ="hdnPerClaimPayLimitTabname" value="Per Claim Pay Limit" />
      <%--skhare7: R8:SuperVisory Approval--%>
      <asp:HiddenField runat ="server" ID ="hdnReserveLimitsList" value="" />
       <asp:HiddenField runat ="server" ID ="hdnPrintCheckLimitsList" value="" />
        <asp:HiddenField runat ="server" ID ="hdnPaymentLimitsList" value="" />
         <asp:HiddenField runat ="server" ID ="hdnPayDetailLimitsList" value="" />
          <asp:HiddenField runat ="server" ID ="hdnClaimLimitsList" value="" />
           <asp:HiddenField runat ="server" ID ="hdnEventTypeList" value="" />
           <asp:HiddenField runat ="server" ID ="hdnPerClaimPayLimitsList" value="" />
        <asp:HiddenField runat ="server" ID ="hdnClaimsTotalIncurredLimitsList" value="" /><%--sharishkumar jira 6385 --%>
           <%-- skhare7 R8 supervisory approval--%>
    </form>
</body>
</html>
