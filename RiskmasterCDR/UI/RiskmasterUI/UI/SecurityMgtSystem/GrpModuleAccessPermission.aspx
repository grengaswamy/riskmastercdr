﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeBehind="GrpModuleAccessPermission.aspx.cs" Inherits="Riskmaster.UI.SecurityMgtSystem.GrpModuleAccessPermission" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
     <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/GrpModuleAccessPermission.js"></script>
   
</head>
<body>
    <form id="frmPermissionTree" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:ScriptManager runat="server" ID="hdscriptmanager"></asp:ScriptManager>
    <div id="divScroll">
        
    
        <asp:TreeView
                           ID="RTV"
                           PopulateNodesFromClient="true" 
                           ShowLines="true" 
                           ShowExpandCollapse="true" 
                           runat="server"
                           OnTreeNodePopulate="RTV_GetChildNodes"
                           SelectedNodeStyle-Font-Bold="true"
                           SelectedNodeStyle-BackColor="Cyan"
                           ForeColor="Black"
                           Target="_self"
                           EnableViewState="true"
                           ExpandDepth="1"
                           ShowCheckBoxes="All"
                           >
                           <Nodes>
                             <asp:TreeNode Text="Module Access Permissions" ShowCheckBox="false" Expanded="true" Value="0" PopulateOnDemand="true">
                             </asp:TreeNode>
                           </Nodes>
                           </asp:TreeView>
    </div>
    <asp:TextBox style="display:none" ID="hddsnid" runat="server" rmxref="/Instance/Document/Document/form/DsnId"></asp:TextBox>
    <asp:TextBox style="display:none" ID="hdgroupid" runat="server" rmxref="/Instance/Document/Document/form/GroupId"></asp:TextBox>
    <asp:TextBox style="display:none" ID="hdGrantedFuncIDs" runat="server" rmxref="/Instance/Document/Document/form/grantedFuncIDs"></asp:TextBox>
    <asp:TextBox style="display:none" ID="hdRevokedFuncIDs" runat="server" rmxref="/Instance/Document/Document/form/revokedFuncIDs"></asp:TextBox>
    <asp:TextBox style="display:none" ID="hdModulePermissionDisabledFlag" runat="server" rmxref="/Instance/Document/Document/form/parentFuncId/@ModulePermissionDisabled"></asp:TextBox>
    <asp:TextBox style="display:none" ID="hdCurrentFuncId" runat="server" rmxref="/Instance/Document/Document/form/parentFuncId"></asp:TextBox>
     <asp:TextBox style="display:none" ID="hdGrantedFromFuncId" runat="server" rmxref="/Instance/Document/Document/form/grantedFromFuncId"></asp:TextBox>
    <asp:TextBox style="display:none" ID="hdRevokedFromFuncId" runat="server" rmxref="/Instance/Document/Document/form/revokedFromFuncId"></asp:TextBox>
    <asp:TextBox style="display:none" ID="hdFunctionToCall" runat="server" rmxref="/Instance/Document/Document/FunctionToCall" Text="ModuleGroupsAdaptor.GetChildPermissions"></asp:TextBox>
    <asp:TextBox style="display:none" ID="hdRetVal" runat="server" rmxref="/Instance/Document/Document/form/RetVal"></asp:TextBox>
    <asp:TextBox runat="server" rmxref="/Instance/Document/Document/form/group/displaycolumn/control[@name='hdUserLoginName']"
                        Style="display: none" ID="hdUserLoginName" />
    
    
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    
    </form>
</body>
</html>
