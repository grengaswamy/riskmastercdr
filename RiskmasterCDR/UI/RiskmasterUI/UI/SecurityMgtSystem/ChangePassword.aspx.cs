﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;
//using Riskmaster.UI.RMAuthentication;//asharma326 MITS 27586

namespace Riskmaster.UI.SecurityMgtSystem
{
    /// <summary>
    /// Author: Ashutosh Kashyap(akashyap3)
    /// coded on 27-Jan-2009
    /// </summary>
    public partial class ChangePassword : NonFDMBasePageCWS
    {
        #region PrivateVariables
        //private AuthenticationServiceClient authService = new AuthenticationServiceClient();//asharma326 For loging out user in case password is successful.

        /// <summary>
        /// Returned response string from AppHelper.CallCWSService
        /// </summary>
        string _strResponse = "";

        /// <summary>
        /// XElement representation of _strResponse
        /// </summary>
        XElement oResponse = null;

        /// <summary>
        /// Boolean Value to check if the Old Password textbox needs to be shown.
        /// used incase of change password from security management.
        /// </summary>
        bool _bShowOldPwd = true;
        int _intShowOldPwd;
        //Asharma326 MITS 27586 Starts
        bool _bForgotPassword = false;
        int _intForgotPwd; 
        //Asharma326 MITS 27586 Ends

        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    //Preparing XML to send to service
                    XElement oMessageElement = GetInitialMessageTemplate();

                    _strResponse = AppHelper.CallCWSService(oMessageElement.ToString());

                    //parse CallCWSService response to an XElement to be parsed for errors
                    oResponse = XElement.Parse(_strResponse);
                    XElement oEle = oResponse.XPathSelectElement("./Document/form/PasswordPolicy");
                    if (oEle != null)
                    {
                        hdispwdpolicyenabled.Value = oEle.Value;
                    }
                }

                
                hdInvokedFrom.Text = AppHelper.GetQueryStringValue("invokedfrom");
                hdLoginName.Text = AppHelper.GetUserLoginName();
                RetainPasswordControlValue(oldpasswd);
                RetainPasswordControlValue(newpasswd);
                RetainPasswordControlValue(confirmpasswd);
            _bShowOldPwd = GetShowOldPwd();
            //Asharma326 MITS 27586 Starts
            _bForgotPassword = GetShowForgotPassword();

            if (_bForgotPassword)
            {
                lbloldpwd.Text = "Temporary Password";
                hdIsPwdReset.Text = "0";
                    //HtmlGenericControl js = new HtmlGenericControl("script");
                    //js.Attributes["type"] = "text/javascript";
                    //js.Attributes["src"] = "../../Scripts/securityscript.js";
                    //Page.Header.Controls.Add(js);
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "UNIQUEID", @"<script type='text/javascript' language='JavaScript' src='../..Scripts/securityscript.js'/>",false);
                if (!IsPostBack)
                {
                    string sScript = @"<script language='javascript' type='text/javascript' src='../../Scripts/securityscript.js'></script>";
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "FILENAME", sScript, false);
                }
            }
            
            //Asharma326 MITS 27586 Ends
            if (_bShowOldPwd)
            {
                DivOldPassword.Visible = true;
            }
            else
            {
                DivOldPassword.Visible = false;
            }
            if (hdispwdpolicyenabled.Value == "false")
            {
                btnOK.Enabled = true;
            }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }

        //Asharma326 MITS 27586 Starts
        private bool GetShowForgotPassword()
        {
            _intForgotPwd = Convert.ToInt32(!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("forgotpassword").ToString()));

            if (_intForgotPwd == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        } 
        //Asharma326 MITS 27586 Ends

        #endregion
        #region GetShowOldPwd
        /// <summary>
        /// Enable or Disable the visibility of OldPassword depending upon "showoldpwd" value in query string.
        /// </summary>
        /// <returns></returns>
        private bool GetShowOldPwd()
        {
            try
            {
                _intShowOldPwd = Convert.ToInt32(AppHelper.GetQueryStringValue("showoldpwd").ToString());

                if (_intShowOldPwd == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch(Exception)
            {
                return true;
            }
        }
        #endregion

        #region GetMessageTemplate()
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>6dfdbcb5-8a5c-44dd-944f-74b2d3c96f30</Authorization>
              <Call>
                <Function>SecurityAdaptor.IsPasswordComplex</Function>
              </Call>
              <Document>
                <form>
                  <Password></Password>
                  <FirstName />
                  <LastName />
                  <Userid />
                  <LoginName></LoginName>
                  <RetVal />
                  <SuccessMsg />
                  <PasswordPolicy/>
                </form>
              </Document>
            </Message>
            ");
            return oTemplate;
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetInitialMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>6dfdbcb5-8a5c-44dd-944f-74b2d3c96f30</Authorization>
              <Call>
                <Function>SecurityAdaptor.IsPasswordPolicyEnabled</Function>
              </Call>
              <Document>
                <form>
                  <PasswordPolicy/>
                </form>
              </Document>
            </Message>
            ");
            return oTemplate;
        }

        #endregion

        #region ModifyTemplate(XElement oMessageElement)
        /// <summary>
        /// This function would be used to modify the template as and when necessary
        /// </summary>
        /// <param name="oMessageElement"></param>
        private void ModifyTemplate(XElement oMessageElement)
        {
            string sLastName = string.Empty;
            string sFirstName = string.Empty;
            if (newpasswd.Text != "")
            {
                XElement oNewPasswordElement = oMessageElement.XPathSelectElement("./Document/form/Password");
                if (oNewPasswordElement != null)
                {
                    oNewPasswordElement.Value = newpasswd.Text.ToString();
                }
            }

        XElement oLoginNameElement = oMessageElement.XPathSelectElement("./Document/form/LoginName");

        if (hdopenerusername.Value != "")
        {
            //Pentesting - XSS error fix - srajindersin - MITS 27843
            //hdLoginName.Text = hdopenerusername.Value;
            hdLoginName.Text = AppHelper.HTMLCustomEncode(hdopenerusername.Value);
            //END Pentesting - XSS error fix - srajindersin - MITS 27843
        }
            oLoginNameElement.Value = hdLoginName.Text.ToString();
            //Added for Mits 18690
            if (AppHelper.GetQueryStringValue("lastname") != "")
            {
                sLastName = AppHelper.GetQueryStringValue("lastname");
                XElement oLastName = oMessageElement.XPathSelectElement("./Document/form/LastName");
                if (oLastName != null)
                {
                    oLastName.Value = sLastName;
                }

            }
            if (AppHelper.GetQueryStringValue("firstname") != "")
            {
                sFirstName = AppHelper.GetQueryStringValue("firstname");
                XElement oFirstName = oMessageElement.XPathSelectElement("./Document/form/FirstName");
                if (oFirstName != null)
                {
                    oFirstName.Value = sFirstName;
                }

            }
            //Added for Mits 18690    
        }
        #endregion

        #region btnvalidate_Click
        protected void btnvalidate_Click(object sender, EventArgs e)
        {                     
            _strResponse = "";
           
            try
            {
                if (!Page.IsPostBack || !Page.IsCallback)
                {
                    //Preparing XML to send to service
                    XElement oMessageElement = GetMessageTemplate();

                    //Modify XML if needed
                    ModifyTemplate(oMessageElement);

                    _strResponse = AppHelper.CallCWSService(oMessageElement.ToString());
                   
                    //parse CallCWSService response to an XElement to be parsed for errors
                    oResponse = XElement.Parse(_strResponse);

                    // Extract success message from the CallCWSService response string
                    ExtractResponse(oResponse);

                    
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        #endregion

        #region ExtractResponse(XElement oMessageElement)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <returns></returns>
        private void ExtractResponse(XElement oMessageElement)
        {
            string strExtract = "";

            XElement oStatusCd = oMessageElement.XPathSelectElement("./MsgStatus/MsgStatusCd");

            if (oStatusCd.Value == "Error")
            {
                string sShowErrCode = RMXResourceProvider.IsShowErrorCode();
                //Deb MITS 33443
                lblstatus.Text = UpdateErrorMessage(oMessageElement.XPathSelectElement("./MsgStatus/ExtendedStatus/ExtendedStatusDesc").Value);
                lblstatus.CssClass = "errortext";
                enablesave.Value = "false";
                ((Label)ErrorControl1.FindControl("lblError")).Text = "";
                newpasswd.Focus();
                
            }
            else if (oStatusCd.Value == "Success")
            {
                enablesave.Value = "true";
                XElement oResponseElement = oMessageElement.XPathSelectElement("./Document/form/SuccessMsg");
                if (oResponseElement != null)
                {
                    strExtract = oResponseElement.Value;

                    lblstatus.Text = strExtract;
                    lblstatus.CssClass = "ChangePwdlblStatus";
                    ((Label)ErrorControl1.FindControl("lblError")).Text = "";
                    confirmpasswd.Focus();
                }
            }
        }
        //Deb MITS 33443
        public static string UpdateErrorMessage(string p_sKeyValue)
        {
            string[] sDataSeparator = { "~*~" };
            string[] ArrDataSeparator = { "|^|" };
            string[] ArrErrMsg = p_sKeyValue.Split(sDataSeparator, StringSplitOptions.None);
            string sReturnMsg = string.Empty;
            string sKeyId = string.Empty;
            try
            {
                if (!p_sKeyValue.Contains("|^|"))
                {
                    return p_sKeyValue;
                }
                string sShowErrCode = RMXResourceProvider.IsShowErrorCode();
                string sLangCode = AppHelper.GetLanguageCode();
                if (sLangCode.Length != 4) sLangCode = AppHelper.GetBaseLanguageCodeWithCulture().Split('|')[0].ToString();
                foreach (string s in ArrErrMsg)
                {
                    if (s.Contains(sLangCode))
                    {
                        sReturnMsg = s;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(sReturnMsg))
                {
                    sLangCode = AppHelper.GetBaseLanguageCodeWithCulture().Split('|')[0].ToString();
                    foreach (string s in ArrErrMsg)
                    {
                        if (s.Contains(sLangCode))
                        {
                            sReturnMsg = s;
                            break;
                        }
                    }
                }
                string[] sLangSeparator = { sLangCode };
                if (sShowErrCode.ToUpper() == "TRUE")
                {
                    string[] sArr = sReturnMsg.Split(sLangSeparator, StringSplitOptions.None);
                    string[] arr2 = { "", "" };
                    string codelist = string.Empty;
                    if (sArr.Length > 1)
                    {

                        if (sArr.Length > 2)
                        {
                            for (int j = 1; j < sArr.Length - 1; j++)
                            {
                                arr2[0] = sArr[j].Substring(sArr[j].Length - 7);
                                arr2[1] = sArr[j].Remove(sArr[j].Length - 7);
                                sArr[j] = arr2[0] + arr2[1];
                            }
                            codelist = AppendErrorCode(sArr[0].ToString());

                            for (int i = 1; i < sArr.Length - 1; i++)
                            {
                                codelist = codelist + "rmA-" + sArr[i].Substring(0, 7) + " ";
                                sArr[i] = sArr[i].Substring(8);
                                sKeyId = sKeyId + sArr[i];
                            }
                        }
                        else
                        {
                            codelist = AppendErrorCode(sArr[0].ToString());
                            sKeyId = sArr[1];
                        }
                    }
                    sReturnMsg = (codelist + ": " + sKeyId).Replace("|^|", "").Replace("^|", "");
                }
                else
                {
                    string[] sArr = sReturnMsg.Split(sLangSeparator, StringSplitOptions.None);
                    string Msg = string.Empty;
                    if (sArr.Length > 2)
                    {
                        for (int j = 1; j < sArr.Length - 1; j++)
                        {

                            sArr[j] = sArr[j].Remove(sArr[j].Length - 7);
                            sKeyId = sKeyId + sArr[j];
                        }
                        sReturnMsg = sKeyId.Replace("|^|", "").Replace("^|", "") + sArr[sArr.Length - 1].Replace("|^|", "");
                    }
                    else
                    {
                        sKeyId = sArr[0].Replace("|^|", "");
                        sReturnMsg = sArr[sArr.Length - 1].Replace("|^|", "");
                    }
                }
            }
            catch (Exception ee)
            {

            }
            return (!string.IsNullOrEmpty(sKeyId) ? sReturnMsg : p_sKeyValue);
        }
        private static string AppendErrorCode(string sKeyID)
        {
            if (string.IsNullOrEmpty(sKeyID))
            {
                return string.Empty;
            }
            else
            {
                sKeyID = sKeyID.Replace("|^|", "");
                sKeyID = "rmA-" + sKeyID + " ";
                return sKeyID;
            }
        }
        //Deb
        #endregion

        #region btnOK_Click
        /// <summary>
        /// Update Password
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOK_Click(object sender, EventArgs e)
        {
            //bool bReturnStatus = false;
            string sReturn = "";
            
            try
            {
                 if (hdoldpwd.Value != "" && oldpasswd.Text == "")
                 {
                    oldpasswd.Text = hdoldpwd.Value;
                 }
                 if (hdopenerusername.Value != "")
                 {
                     //Pentesting - XSS error fix - srajindersin - MITS 27843
                     //hdLoginName.Text = hdopenerusername.Value;
                     hdLoginName.Text = AppHelper.HTMLCustomEncode(hdopenerusername.Value);
                     //END Pentesting - XSS error fix - srajindersin - MITS 27843
                 }
                 CallCWS("UserLoginsAdaptor.UpdatePassword", null, out sReturn, true, false);
                 if (HttpContext.Current.Items["Error"] != null)
                 {

                     //Asharma326 MITS 27586 Starts
                     if (_bForgotPassword)
                     {
                         sReturn = sReturn.Replace("Old", "Temporary");
                     } 
                     //Asharma326 MITS 27586 Ends
                       ErrorControl1.errorDom = sReturn;
                       lblstatus.Text = "";
                       btnOK.Enabled = true;
                 }
                 else
                 {
                        DivControls1.Visible = false;
                        DivControls2.Visible = false;
                        lblUpdateStatus.Text = "Password Update Status for Login Name '" + hdLoginName.Text.ToString() + "'";
                        DivSuccessMsg.Visible = true;
                        btnOK.Visible = false;//srajindersin MITS 34484 1/8/2014
                        //Asharma326 MITS 27586 Starts
                        if (_bForgotPassword)
                        {
                            lblmessage.Text = lblmessage.Text + " Please Login to Continue.";
                            divlogin.Visible = true;
                            try
                            {
                                //Open the call to the Service
                                //authService.Open();
                                string strSessionID = AppHelper.ReadCookieValue("SessionId");
                                //Log out the user
                                //authService.LogOutUser(strSessionID);
                                AuthData oAuthData = new AuthData();
                                oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                                oAuthData.Token = strSessionID;
                                AppHelper.GetResponse("RMService/Authenticate/logout", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                                //Remove the user's cookie
                                AppHelper.RemoveCookie();
                                FormsAuthentication.SignOut();
                                oAuthData = null;
                                //authService.Close();
                                ////Clean up
                                //if (authService != null)
                                //{
                                //    authService = null;
                                //}
                            }
                            catch
                            {// dead 
                            }
                        } 
                     //Asharma326 MITS 27586 Ends
                 }
                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        #endregion
        
        #region RetainPasswordControlValue
        private void RetainPasswordControlValue(Control oControl)
        {
            ((TextBox)oControl).Attributes["value"] = ((TextBox)oControl).Text;
            //asharma326 what ever value set it to textbox. 
            //if (((TextBox)oControl).Text != "")
            //{
            //    if (((TextBox)oControl).Attributes["value"] != null)
            //    {
            //        ((TextBox)oControl).Attributes["value"] = ((TextBox)oControl).Text;
            //    }
            //    else
            //    {
            //        ((TextBox)oControl).Attributes.Add("value", ((TextBox)oControl).Text);
            //    }
            //}
            //else
            //{
            //    if (((TextBox)oControl).Attributes["value"] != null)
            //    {
            //        ((TextBox)oControl).Text = ((TextBox)oControl).Attributes["value"].ToString();
            //    }
            //}
        }
        #endregion
        
    }
}
