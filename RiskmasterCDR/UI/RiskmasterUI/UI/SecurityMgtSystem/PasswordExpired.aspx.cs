﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.UI.SecurityMgtSystem
{
    public partial class PasswordExpired : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         // akaushik5 Added for MITS 32094 Starts
            if (!Page.IsPostBack)
            {
                this.ResetAuthenticationCookie();
            }
            // akaushik5 Added for MITS 32094 Ends
        }

        // akaushik5 Added for MITS 32094 Starts
        /// <summary>
        /// Resets the authentication cookie.
        /// </summary>
        private void ResetAuthenticationCookie()
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null)
            {
                return;
        }
            FormsAuthenticationTicket authTicket = null;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch (Exception ex)
            {
                return;
            }
            if ((authTicket == null) || authTicket.Expired)
            {
                return;
            }

            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            authCookie.Value = encryptedTicket;
            authCookie.Expires = authTicket.Expiration;
            authCookie.HttpOnly = true;
            Response.Cookies.Add(authCookie);
        }
        // akaushik5 Added for MITS 32094 Ends
    }
}
