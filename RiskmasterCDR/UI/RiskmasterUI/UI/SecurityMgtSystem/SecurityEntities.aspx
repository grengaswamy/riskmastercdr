﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SecurityEntities.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.SecurityEntities" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SecurityEntities</title>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/SecurityEntities.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>

    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>

</head>
<body onload="scrollNodeIntoView();" style="margin-left: 0px">
    <form id="frmLeftTree" runat="server" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <table>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblWarning" Style="color: red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button runat="server" ID="back" Visible="false" Text="Back" OnClientClick="history.back();return false;" />
            </td>
        </tr>
    </table>
    <div id="divScroll">
        <asp:TreeView ID="LTV" PopulateNodesFromClient="true" ShowLines="true" ShowExpandCollapse="true"
            runat="server" OnTreeNodePopulate="LTV_GetChildNodes" SelectedNodeStyle-Font-Bold="true"
            SelectedNodeStyle-BackColor="LightBlue" ForeColor="Black" Target="_self" EnableViewState="false"
            ExpandDepth="1">
        </asp:TreeView>
        <asp:HiddenField ID="hd_selectednodevalue" runat="server" />
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/Errors/Warning"
            ID="hdWarnings">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/hdIsDomainAuthenticationEnabled"
            ID="hdIsDomainAuthenticationEnabled">
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/hdShowPasswordScreen"
            ID="hdShowPasswordScreen">
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/EntityToBeFocussed"
            ID="EntityToBeFocussed">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/hdIsPwdPolicyEnabled"
            ID="hdIsPwdPolicyEnabled">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/SelectedEntityFromLeftTree"
            ID="SelectedEntityFromLeftTree">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/SerializedState"
            ID="NodeState">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/DsnIdForLoadingMG"
            ID="DsnIdForLoadingMG">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/IsPostBack"
            ID="IsPostBack">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/TreePostedBy"
            ID="TreePostedBy">	
						
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/AddNewUser/NewUserId"
            ID="NewUserId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateUserEntity/UserEntityUserId"
            ID="UserEntityUserId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateDatasourceEntity/DatasourceEntityDsnId"
            ID="DatasourceEntityDsnId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateUserPermEntity/UserPermEntityDsnId"
            ID="UserPermEntityDsnId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateUserPermEntity/UserPermEntityUserId"
            ID="UserPermEntityUserId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateUserPermEntity/UserPermModuleGrpSelectedId"
            ID="UserPermModuleGrpSelectedId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/RenameModuleGroupEntity/RenameModuleGroupForDsnId"
            ID="RenameModuleGroupForDsnId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateUserEntity/UserEntityDsnIdSelected"
            ID="UserEntityDsnIdSelected">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateModuleGroupEntity/ModuleGroupEntityDsnId"
            ID="ModuleGroupEntityDsnId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateModuleGroupEntity/ModuleGroupEntityGroupId"
            ID="ModuleGroupEntityGroupId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/AddNewDatasourceEntity/AddedDBId"
            ID="AddedDBId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateModuleGrpUserEntity/ModuleGroupUserEntityDsnId"
            ID="ModuleGroupUserEntityDsnId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateModuleGrpUserEntity/ModuleGroupUserEntityGrpId"
            ID="ModuleGroupUserEntityGrpId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/UpdateModuleGrpUserEntity/ModuleGroupUserEntityUserId"
            ID="ModuleGroupUserEntityUserId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/SelectedItemTreeId"
            ID="SelectedItemTreeId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/Delete/EntityIdForDeletion"
            ID="EntityIdForDeletion">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" rmxref="/Instance/Document/form/ToBePosted/DsnIdForLoadingMG"
            ID="DsnIdForCurrentlyLoadedMGrps">
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="Keys" name="Keys">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="APP" name="APP">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="CurrentPage" name="CurrentPage">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="AutoHide" name="AutoHide">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="IsHidden" name="IsHidden">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="NavSize" name="NavSize">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="txtDsnId" name="txtDsnId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="CurrentPerspective" name="CurrentPerspective">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="PageToBeShownInMainFrame" name="PageToBeShownInMainFrame"
            rmxref="/Instance/Document/form/PostBackResults/PageToBeShownInMainFrame">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="entityType" name="entityType"
            rmxref="/Instance/Document/form/PostBackResults/entityType">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="idDb" name="idDb" rmxref="/Instance/Document/form/PostBackResults/idDb">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="NodeId" name="NodeId" rmxref="/Instance/Document/form/PostBackResults/NodeId">	
        </asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="usernamestring"></asp:TextBox>
        <asp:TextBox runat="server" Style="display: none" ID="datasourcestring"></asp:TextBox>
    </div>
    <asp:XmlDataSource ID="PreBindedXMLSource" runat="server" EnableViewState="false"
        EnableCaching="false" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
