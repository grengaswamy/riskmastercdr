﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class GrpModuleAccessPermission : NonFDMBasePageCWS
    {
        /// <summary>
        /// Contains web service output
        /// </summary>
        private XmlDocument oFDMPageDom = null;
        private string m_sReturn = "";
        private XmlDocument oResponse = null; 
        private TreeNodeCollection oNodeCollection = new TreeNodeCollection();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hdUserLoginName.Text = AppHelper.GetUserLoginName();
                oResponse = new XmlDocument();
                if (hddsnid.Text == String.Empty)
                {
                    hddsnid.Text = AppHelper.GetQueryStringValue("dsnid");
                }
                if (hdgroupid.Text == String.Empty)
                {
                    hdgroupid.Text = AppHelper.GetQueryStringValue("groupid");
                }
                if (!Page.IsCallback && !Page.IsPostBack)
                {
                    //registring onclick event for treeview
                    //this event would handle all kinds of client side manipulations 
                    RTV.Attributes.Add("onclick", "return OnTreeClick(event)");

                    //Initializing Tree View
                    RTV.FindNode("0").Expand();

                }
                else if (Page.IsPostBack && hdFunctionToCall.Text == "ModuleGroupsAdaptor.UpdateModulePermissions")
                {
                    TreeNode rootNode = RTV.FindNode("0");
                    int iCounter = rootNode.ChildNodes.Count;
                    for (int i = 0; i < iCounter; i++)
                    {
                        rootNode.ChildNodes.RemoveAt(0);
                        if (rootNode.ChildNodes.Count == 0)
                        {
                            break;
                        }
                    }
                    InitializeTree(rootNode);
                    hdFunctionToCall.Text = "ModuleGroupsAdaptor.GetChildPermissions";
                    hdCurrentFuncId.Text = "";//abansal23: MITS 16744 on 06/10/2009
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }

        private void RemoveChildNodes(TreeNode oNode)
        {
            foreach (TreeNode oChildNode in oNode.ChildNodes)
            {
                if (oChildNode.ChildNodes.Count == 0)
                {
                    oNode.ChildNodes.Remove(oChildNode);
                }
                else
                {
                    RemoveChildNodes(oChildNode);
                }
            }
        }
        /// <summary>
        /// This function gets called dynamically from server using AJAX to populate child nodes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RTV_GetChildNodes(object sender, TreeNodeEventArgs e)
        {
            try
            {
                if (hdFunctionToCall.Text == "ModuleGroupsAdaptor.GetChildPermissions")
                {
                    if (e.Node.Value != "0")
                    {
                        hdCurrentFuncId.Text = e.Node.Value;
                    }
                    InitializeTree(e.Node);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            
        }

        private void InitializeTree(TreeNode oParentNode)
        {
            //Call WCF wrapper for cws
            NonFDMCWSPageLoad(hdFunctionToCall.Text);
            oResponse = Data;

            XElement oMessageRespElement = XElement.Parse(oResponse.OuterXml);
            XElement oChildEle = oMessageRespElement.XPathSelectElement("./Document/form/RetVal");
            if (oChildEle != null && (!String.IsNullOrEmpty(oChildEle.Value)))
            {
                oResponse.LoadXml(oChildEle.Value);

                foreach (XmlNode oModuleGroupNode in oResponse.FirstChild.ChildNodes)
                {
                    TreeNode parentNode = AddChildNode(oParentNode, oModuleGroupNode.Attributes["id"].Value, oModuleGroupNode.Attributes["name"].Value, oModuleGroupNode.Attributes["allowed"].Value, oModuleGroupNode.Attributes["childCount"].Value);
                }
            }
            else
            {
                if (hdModulePermissionDisabledFlag.Text == "true")
                {
                    Response.Redirect("ModuleAccessDisabled.html", true);
                }
            }
        }
        /// <summary>
        /// This function adds a child node to parent
        /// </summary>
        /// <param name="oParentNode"></param>
        /// <param name="p_Id"></param>
        /// <param name="p_sName"></param>
        /// <param name="p_sCode"></param>
        /// <param name="p_bHasChild"></param>
        private TreeNode AddChildNode(TreeNode oParentNode, string p_Value, string p_sName, string p_sAllowed, string p_sChildCount)
        {
            TreeNode childNode = null;
            try
            {
                childNode = new TreeNode(p_sName, p_Value.ToString());
                if (p_sChildCount != "0")
                {
                    childNode.PopulateOnDemand = true;
                }
                else
                {
                    childNode.PopulateOnDemand = false;
                }
                childNode.Target = "_self";
                if (p_sAllowed == "1")
                    childNode.Checked = true;
                else
                    childNode.Checked = false;
                oParentNode.ChildNodes.Add(childNode);

            }
            catch (Exception e)
            {
            }
            return childNode;
        }
        /// <summary>
        /// CWS request message template for fetching child nodes
        /// </summary>
        /// <returns></returns>
        private XElement GetChildNodePopulateMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
            <Call>
              <Function>ModuleGroupsAdaptor.GetChildPermissions</Function> 
              </Call>
            <Document>
                <Document>
                    <form title='Module Access Permissions'>
                        <group>
                             <displaycolumn>
                                <control name='hdUserLoginName' type='hidden'></control> 
                             </displaycolumn>
                        </group>
                        <Errors>
                        <Warning /> 
                        <Fatal /> 
                        <Trivial /> 
                        </Errors>
                        <DsnId></DsnId> 
                        <GroupId></GroupId> 
                        <parentFuncId /> 
                        <grantedFromFuncId /> 
                        <revokedFromFuncId /> 
                        <grantedFuncIDs /> 
                        <revokedFuncIDs /> 
                        <RetVal /> 
                        <SuccessMsg /> 
                    </form>
                    <FunctionToCall>ModuleGroupsAdaptor.GetChildPermissions</FunctionToCall> 
                </Document>
             </Document>
          </Message>
          
            ");
            return oTemplate;
        }

    }
}
