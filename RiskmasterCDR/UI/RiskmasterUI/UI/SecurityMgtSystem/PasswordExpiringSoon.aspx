﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PasswordExpiringSoon.aspx.cs" Inherits="Riskmaster.UI.UI.SecurityMgtSystem.PasswordExpiringSoon" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="margin-top: 10%; margin-left: 25%;">
                    <tr>
                        <td colspan="3" align="center">
                            Your password will expire in <%=AppHelper.GetQueryStringValue("Diff")%> days.
                        </td>
                    </tr>
                     
                     <tr>
                         <td><a href="../Home/Status.aspx" class="LightBold">Continue to RM application.</a></td>
                         <td></td>
                         <td>
                             <%--akaushik5 Changed for MITS 37995 Starts--%>
                             <%--<a href="ChangePassword.aspx?invokedfrom=pwdexpire" class="LightBold">Change Password.</a>--%>
                             <a href="../SecurityMgtSystem/ChangePassword.aspx?invokedfrom=pwdexpire" class="LightBold">Change Password.</a>
                             <%--akaushik5 Changed for MITS 37995 Ends--%>
                         </td>
                      </tr>
                      
                </table>
    </div>
    </form>
</body>
</html>
