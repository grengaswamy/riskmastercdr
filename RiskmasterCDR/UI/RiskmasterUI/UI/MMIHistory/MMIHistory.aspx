﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MMIHistory.aspx.cs" Inherits="Riskmaster.UI.MMIHistory.MMIHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MMI History</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css">

    <script language="JavaScript" src="../../Scripts/form.js"></script>

    <script language="JavaScript" src="../../Scripts/MMIHistory.js"></script>
    <%--vkumar258 -RAMA-6037 - Starts--%>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 -RAMA-6037 - End--%>

    <script language="JavaScript">
					function PageLoaded()
					{
						document.forms[0].method="post";
						return false;
		}
		function ValidateDelete() {
		    var bCheckboxSelected = false;
		    for (var i = 0; i < document.forms[0].elements.length; i++) {
		        if (document.forms[0].elements[i].type == 'checkbox') {
		            if (document.forms[0].elements[i].checked == true) {
		                bCheckboxSelected = true;
		            }
		        }

		    }
		    if (!bCheckboxSelected) {
		        alert('Please select the MMI row(s) to be deleted');
		    }
		    else {
		        if (confirm('Are you sure you want to delete selected item(s)')) {
		            return true;
		        }
		    }
		    return false;
		}
    </script>

<%--    <script src="csc-Theme/riskmaster/common/javascript/zapatec/utils/zapatec.js" type="text/javascript"></script>
     <script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/window.js" type="text/javascript"></script>
     <script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
     <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar-setup.js"></script>
    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/calendar-alias.js"></script>
    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid.js"></script>
    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-xml.js"></script>
    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-editable.js"></script>
    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-query.js"></script>
    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/rmx-common-ref.js"></script>--%>
  <script language="JavaScript" src="../../Scripts/cul.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}</script>
</head>
<body>
    <form id="frmData" runat ="server" name="frmData">
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""><p align="center">
        <input type="hidden" id="formname" value="document"><input type="hidden" name="$node^26"
            value="" id="hdnReason" runat ="server"><input type="hidden" name="$node^31" value="" id="hdnMaxDate" runat ="server"><input
                type="hidden" name="$node^34" value="True" id="hdntrack">
                <input type="hidden" name="$node^26" value="" id="hdnClaimId" runat ="server"/>
                <input type="hidden" name="$node^26" value="" id="hdnEmployeeId" runat ="server"/>
                 <input type="hidden" name="$node^26" value="" id="hdnPiRowid" runat ="server"/>
                <table class="singleborder">
                    <tr>
                        <td colspan="6" class="ctrlgroup" id="title" runat="server">
                            <%--MMI History [ WC0001 ]--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd" nowrap="">
                            <b>MMI Date:</b>
                        </td>
                        <td class="datatd">
                            <input type="text" name="$node^19" value="" id="mmidate" size="30" onblur="dateLostFocus(this.id);" runat ="server">
                        <%--vkumar258 -RAMA-6037 - Starts--%>
                        <%-- <input type="button" class="DateLookupControl" id="btnmmidate"><script type="text/javascript">
										Zapatec.Calendar.setup(
										{
											inputField : "mmidate",
											ifFormat : "%m/%d/%Y",
											button : "btnmmidate"
										}
										);
                                </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#mmidate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                            });
                        </script>
                        <%--vkumar258 -RAMA-6037 - End--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd" nowrap="">
                            <b>Date of Exam.:</b>
                        </td>
                        <td class="datatd">
                            <input type="text" name="$node^20" value="" id="doedate" size="30" onblur="dateLostFocus(this.id);" runat ="server">
                        <%--vkumar258 -RAMA-6037 - Starts--%>
                        <%--<input type="button" class="DateLookupControl" id="btndoedate"><script type="text/javascript">
										Zapatec.Calendar.setup(
										{
											inputField : "doedate",
											ifFormat : "%m/%d/%Y",
											button : "btndoedate"
										}
										);
                                </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#doedate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                            });
                        </script>
                        <%--vkumar258 -RAMA-6037 - End--%>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="" class="datatd">
                            <b>Changed By:</b>
                        </td>
                        <td class="datatd" id="ChangedBy" runat="server">
                            <%--csc--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd" nowrap="">
                            <b>Approved By:</b>
                        </td>
                        <td class="datatd">
                            <input type="text" name="$node^22" value="" id="approvedby" size="30" maxlength="8" runat ="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd" nowrap="">
                            <b>Reason:</b>
                        </td>
                        <td class="datatd">
                            <input type="submit" name="$action^setvalue%26node-ids%2626%26content%26" value="..."
                                class="button" id="btnReason" onclick="checkReason('');EditMemo('hdnReason');return false;">
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd" nowrap="">
                            <b>Disability %:</b>
                        </td>
                        <td class="datatd">
                            <input type="text" name="$node^23" value="" id="disability" size="30" maxlength="10"
                                onkeyup="setDataChangedKD();" onblur="setDataChanged();" runat ="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd" nowrap="">
                            <b>Physician:</b>
                        </td>
                        <td class="datatd">
                            <input type="text" name="$node^24" value="" size="30" id="Physician" readonly runat ="server"><input
                                type="submit" name="$action^" value="..." class="button" onclick="lookupData('Physician','physician','4','Physician','2');return false;; "><input
                                    type="hidden" name="$node^25" value="" id="Physician_cid" runat ="server">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        </td>
                        <tr>
                            <td colspan="2">
                               <%-- <table align="center">
                                    <tr>
                                        <td class="headertext">
                                            MMI Date:
                                        </td>
                                        <td class="headertext">
                                            Date of Exam.:
                                        </td>
                                        <td class="headertext">
                                            Disability %:
                                        </td>
                                        <td class="headertext">
                                            Physician:
                                        </td>
                                        <td class="headertext">
                                            Changed By:
                                        </td>
                                        <td class="headertext">
                                            Approved By:
                                        </td>
                                        <td class="headertext">
                                            Reason:
                                        </td>
                                    </tr>
                                </table>--%>
                                 <table align="center" width ="100%">
                                    <tr>
                                    <td colspan ="2">
                                    <asp:GridView ID="GridView1" runat="server" HeaderStyle-CssClass="headertext" AutoGenerateColumns ="false" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="MMIRowId">
           
                                         <Columns>
                                             <asp:TemplateField HeaderText="MMI Date:" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMMIDate" runat="server" ></asp:Label>
                                                </ItemTemplate>
                                             </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Date of Exam.:" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExamDate" runat="server" ></asp:Label>
                                                </ItemTemplate>
                                             </asp:TemplateField>
                                            
                                             <asp:BoundField DataField="Disability" HeaderText="Disability %:"  />
                                             <asp:BoundField DataField="Physician" HeaderText="Physician:"  />
                                             <asp:BoundField DataField="AddedByUser" HeaderText="Changed By:"   />
                                             <%--MGaba2:MITS 16640 <asp:BoundField DataField="AddedByUser" HeaderText="Approved By:"   />--%>
                                             <asp:BoundField DataField="ApprovedByUser" HeaderText="Approved By:"   />
                                             <asp:TemplateField HeaderText="Reason:" >
                                                <ItemTemplate>
                                                    <asp:Button ID="btnReason" runat="server" class="button"></asp:Button>
                                                </ItemTemplate>
                                             </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Select:">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkRows" runat="server"/>
                                                </ItemTemplate>
                                             </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Date of Exam:" Visible = "false" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblReason" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Reason")%>'></asp:Label>
                                                </ItemTemplate>
                                             </asp:TemplateField>
                                           
                                         </Columns>
                                     </asp:GridView>
                                     </td>
                                    </tr>
                                   </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button type="submit" name="Save" text="  OK  "
                                    class="button" OnClientClick="return checkMMIHistory('csc','152','');; " OnClick = "SaveMMIHistory" runat="server"/>&nbsp;<input type="submit"
                                        name="$action^" value="Cancel" class="button" onclick="window.close();; ">
                                        <asp:Button ID="btnMultipleRowDelete" type="submit" class="button" OnClientClick="return ValidateDelete();" OnClick="btnMultipleRowDelete_Click" runat="server" Text="Delete Rows" Visible=false/>
                            </td>
                        </tr>
                    </tr>
                </table>
    </p>
    <input type="hidden" name="$node^7" value="rmx-widget-handle-4" id="SysWindowId"><input
        type="hidden" name="$instance" value="H4sIAAAAAAAAAIVUTW+jMBA9p78C5U7dzd4qFqmbtlq0jbRqethbNWsmwYo/kG0K/Pu1IYHYSVVO&#xA;43nPzzPPY7JCGguSYtIJLs19Z9iPZWVtfU9I27a37fdbpfdkdXf3jfzdvGxphQJSdty0nHZ9vWni&#xA;7pQWn/FXZISXeZIkN8nZlwm0kN8sFospY6hGlCkrc6KZOQgwFjXZbIpfzFileyIES6sxzsjMDnQn&#xA;tcag9ig1NCOnxXWqQWOY8lrkE8YbGvsb+y/g1TU8a5ksVetPz8gcDxAZTYgKZ6EtQK0rjoTJFyYP&#xA;Y0GBg0rblCreiJg/IEqXqIe8sy9MhMQSLKS2rzFWqVzxqLXSJt8BN+h0zlIBd8wNAmOvvq+g0e1o&#xA;eyF3KjIue1S0EShtKPmneFVtUUZFuQF5BIvO3VMUwD7z1IFw+BQGhHUFco/lz9jMh7rW6sMDbuvZ&#xA;IlRnBv4xzqwnnS3CuqveMMpAOs4cX6e802FQwnVAfUUwyksdg4tuH8oSY5OeOveWJPB0OzybZw77&#xA;iPEBvInv+7mRw/C9qTVwHvsO3cn3YxSeKGquesSL+1pzYOIiazXQgy/L6c3xNDzzQIx0cvrP5f8B&#xA;Yt0PDPIEAAA="></form>
</body>
</html>
