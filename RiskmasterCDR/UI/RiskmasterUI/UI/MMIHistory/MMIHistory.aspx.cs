﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
//using Riskmaster.Models;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.MMIHistory
{
    public partial class MMIHistory : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes
            if (!Page.IsPostBack)
            {
                XmlDocument oFDMPageDom  = new XmlDocument(); 
                string sReturn = "";
                //Preparing XML to send to service
                XElement oMessageElement = LoadListMMIHistoryTemplate();
                //Modify XML 
                XElement oEmployeeID = oMessageElement.XPathSelectElement("./Document/LoadListMMIHistory/EmployeeId");
                if (oEmployeeID != null)
                {
                    if (Request.QueryString["employeeid"] != null)
                    {
                        oEmployeeID.Value = Request.QueryString["employeeid"];
                        hdnEmployeeId.Value = Request.QueryString["employeeid"];
                    }
                }
                XElement oClaimId = oMessageElement.XPathSelectElement("./Document/LoadListMMIHistory/ClaimId");
                if (oClaimId != null)
                {
                    if (Request.QueryString["claimid"] != null)
                    {
                        oClaimId.Value = Request.QueryString["claimid"];
                        hdnClaimId.Value = Request.QueryString["claimid"];
                    }

                }
                //neha start added Pi Id to open the pop from injury acreen
                XElement oPiRowId = oMessageElement.XPathSelectElement("./Document/LoadListMMIHistory/PiRowId");
                if (oPiRowId != null)
                {
                    if (Request.QueryString["pirowid"] != null)
                    {
                        oPiRowId.Value = Request.QueryString["pirowid"];
                        hdnPiRowid.Value = Request.QueryString["pirowid"];
                    }

                }//Neha end
                //Calling Service to get all PreBinded Data 
                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                if (oFDMPageDom.GetElementsByTagName("MMIHistory").Count > 0)
                {
                    XmlElement xmlMMIHistoryNode = (XmlElement)oFDMPageDom.GetElementsByTagName("MMIHistory")[0];
                    title.InnerText = "MMI History [ " + xmlMMIHistoryNode.Attributes["claimnumber"].Value +" ]";
                    ChangedBy.InnerText = xmlMMIHistoryNode.Attributes["username"].Value;;
                    hdnMaxDate.Value = xmlMMIHistoryNode.Attributes["maxdate"].Value; ;
                }
                // Creates a DataSet and loads it with an Xml Content
                if (oFDMPageDom.GetElementsByTagName("Document").Count > 0)
                {
                    DataSet aDataSet = new DataSet();
                    aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("Document")[0].InnerXml));

                    //// Bind the DataSet to the grid view
                    //GridView gv = (GridView)sender;
                    GridView1.DataSource = aDataSet.Tables["data"];
                    GridView1.DataBind();
                }

            }
        }
        protected void SaveMMIHistory(object sender, EventArgs e)
        {
            
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            //Preparing XML to send to service
            XElement oMessageElement = SaveMMIHistoryTemplate();
            XElement oClaimID = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/ClaimId");
            if (oClaimID != null)
            {
                if (Request.QueryString["claimid"] != null)
                {
                    oClaimID.Value = Request.QueryString["claimid"];
                }
            }
            XElement oEmployeeId = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/EmployeeId");
            if (oEmployeeId != null)
            {
                if (Request.QueryString["employeeid"] != null)
                    oEmployeeId.Value = Request.QueryString["employeeid"];
            }
            //neha start added Pi Id to open the pop from injury acreen
            XElement oPiRowId = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/PiRowId");
            if (oPiRowId != null)
            {
                if (Request.QueryString["pirowid"] != null)
                {
                    oPiRowId.Value = Request.QueryString["pirowid"];
                }

            }//Neha end
            XElement oMMIDate = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/MMIDate");
            if (oMMIDate != null)
            {
                oMMIDate.Value = mmidate.Value;
            }
            XElement oDateExam = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/DateExam");
            if (oDateExam != null)
            {
                oDateExam.Value = doedate.Value;
            }
            XElement oChangedBy = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/ChangedBy");
            if (oChangedBy != null)
            {
                oChangedBy.Value = ChangedBy.InnerText;
            }
            XElement oApprovedBy = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/ApprovedBy");
            if (oApprovedBy != null)
            {
                oApprovedBy.Value = approvedby.Value;
            }
            XElement oDisability = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/Disability");
            if (oDisability != null)
            {
                oDisability.Value = disability.Value;
            }
            XElement oReason = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/Reason");
            if (oReason != null)
            {
                oReason.Value = hdnReason.Value;
            }
            XElement oPhysician = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/Physician");
            if (oPhysician != null)
            {
                oPhysician.Value = Physician.Value;
            }
            XElement oPhysician_cid = oMessageElement.XPathSelectElement("./Document/MMIHistorySummary/Physician_cid");
            if (oPhysician_cid != null)
            {
                oPhysician_cid.Value = Physician_cid.Value;
            }
            //Calling Service to get all PreBinded Data 
            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
            RegisterClientScriptBlock("", "<Script>window.close()</Script>");
        }
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        private void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }
        /// <summary>
        /// CWS request message template for LoadListMMIHistory
        /// --Added Pirow id to use the screen from person involved.
        /// </summary>
        /// <returns></returns>
        private XElement LoadListMMIHistoryTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>MMIHistorySummaryAdaptor.LoadListMMIHistory</Function> 
                      </Call>
                      <Document>
                          <LoadListMMIHistory>
                              <EmployeeId></EmployeeId> 
                              <ClaimId></ClaimId> 
                              <PiRowId></PiRowId>
                          </LoadListMMIHistory>
                      </Document>
                </Message>


            ");
            return oTemplate;
        }

        /// <summary>
        /// CWS request message template for SaveMMIHistory
        /// </summary>
        /// <returns></returns>
        private XElement SaveMMIHistoryTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>MMIHistorySummaryAdaptor.SaveMMIHistory</Function> 
                      </Call>
                      <Document>
                          <MMIHistorySummary>
                              <ClaimId></ClaimId> 
                              <PiRowId></PiRowId> 
                              <EmployeeId></EmployeeId> 
                              <MMIDate></MMIDate> 
                              <DateExam></DateExam> 
                              <ChangedBy /> 
                              <ApprovedBy></ApprovedBy> 
                              <Disability></Disability> 
                              <Reason></Reason> 
                              <Physician></Physician> 
                              <Physician_cid></Physician_cid> 
                         </MMIHistorySummary>
                      </Document>
                </Message>


            ");
            return oTemplate;
        }
        //sgoel6 04/27/2009 MITS 14887
        private XElement DeleteMMIHistoryTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>MMIHistorySummaryAdaptor.DeleteMMIHistory</Function> 
                      </Call>
                      <Document>
                          <DeleteMMIHistory>      
                            <MMIRowId></MMIRowId>
                          </DeleteMMIHistory>
                      </Document>
                </Message>
            ");
            return oTemplate;
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowIndex != -1)
            {
                DateTime fieldDate;
                if (DataBinder.Eval(e.Row.DataItem, "MMIDate") != null)
                {
                    if (DataBinder.Eval(e.Row.DataItem, "MMIDate").ToString() != "")
                    {
                        fieldDate = System.Xml.XmlConvert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "MMIDate").ToString(), "yyyymmdd");
                        ((Label)e.Row.FindControl("lblMMIDate")).Text = fieldDate.ToString("mm/dd/yyyy");
                    }
                }
                if (DataBinder.Eval(e.Row.DataItem, "ExamDate") != null)
                {
                    if (DataBinder.Eval(e.Row.DataItem, "ExamDate").ToString() != "")
                    {
                        fieldDate = System.Xml.XmlConvert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "ExamDate").ToString(), "yyyymmdd");
                        ((Label)e.Row.FindControl("lblExamDate")).Text = fieldDate.ToString("mm/dd/yyyy");
                    }
                }
                if (DataBinder.Eval(e.Row.DataItem, "Reason") != null)
                {
                    string strReason = DataBinder.Eval(e.Row.DataItem, "Reason").ToString();
                    Button B = ((Button)e.Row.FindControl("btnReason"));
                    B.Text = strReason.Length > 5 ? (strReason.Substring(0, 4) + "...") : strReason + "...";


                    B.Attributes.Add("OnClick", "checkReason('" + strReason + "');EditMemo('hdnReason');return false;");
                }
                //Show button when MMI date record(s) exists;
                btnMultipleRowDelete.Visible = true;
            }            
        }

        //sgoel6 04/27/2009 MITS 
        protected void btnMultipleRowDelete_Click(object sender, EventArgs e)
        {
            string sMMIRowIds = string.Empty;
            string sReturn = string.Empty;
            string sMMIDate = string.Empty;
            int iMMIRowId = 0;
            bool bChecked = false;

            XElement oMMIDate = null;
            XmlDocument oFDMPageDom = new XmlDocument();
            XElement oMessageElement = DeleteMMIHistoryTemplate();
            XElement oMMIRowId = oMessageElement.XPathSelectElement("./Document/DeleteMMIHistory/MMIRowId");

            foreach (GridViewRow row in GridView1.Rows)
            {
                CheckBox checkbox = (CheckBox)row.FindControl("chkRows");
                //Check if the checkbox is checked. 
                if (checkbox.Checked)
                {
                    bChecked = true;
                    // Retreive the Employee ID
                    if (iMMIRowId != 0)
                    {
                        sMMIRowIds = sMMIRowIds + "!,#";
                        iMMIRowId = Convert.ToInt32(GridView1.DataKeys[row.RowIndex].Value);
                        sMMIRowIds = sMMIRowIds + iMMIRowId.ToString();
                    }
                    else
                    {
                        iMMIRowId = Convert.ToInt32(GridView1.DataKeys[row.RowIndex].Value);
                        sMMIRowIds = iMMIRowId.ToString();
                    }
                }
            }

            if (bChecked)
            {//Preparing XML to send to service
                if (oMMIRowId.Value != null)
                    oMMIRowId.Value = sMMIRowIds.Trim();
                //Calling Service to get all PreBinded Data 
                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

                //Get the Latest MMI date from the return XML
                oMessageElement = XElement.Parse(@sReturn);
                oMMIDate = oMessageElement.XPathSelectElement("./Document/MMIDate");
                sMMIDate = oMMIDate.Attribute("value").Value.ToString().Trim();
                
                //Store the value of latest MMI date in the hidden field on form
                if (sMMIDate != "")
                sMMIDate = sMMIDate.Substring(4, 2) + "/" + sMMIDate.Substring(6, 2) + "/" + 
                    sMMIDate.Substring(0, 4);
                
                //Transfer the MMI date to the opener window and close the popup window
                RegisterClientScriptBlock("", "<Script>fillLatestMMIDate('" + sMMIDate
                    + "');window.close();</Script>");
            }
            
        }
    }
}
