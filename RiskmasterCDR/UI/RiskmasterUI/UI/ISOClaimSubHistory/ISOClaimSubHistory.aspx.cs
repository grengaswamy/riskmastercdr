﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Text;


namespace Riskmaster.UI.UI.ISOClaimSubHistory
{
    public partial class ISOClaimSubHistory :NonFDMBasePageCWS 
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        string sreturnValue = "";
        bool bReturnStatus = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Mits id: 34975 - Design change - Govind - Start
                //if (Request.QueryString["ClaimId"] != null)
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("ClaimId")))
                {
                    //ViewState["ClaimId"] = Request.QueryString["ClaimId"];
                    claimid.Value = AppHelper.GetQueryStringValue("ClaimId").ToString();



                }
                else
                {
                    //ViewState["ClaimId"] = "";
                    claimid.Value = "";
                }
                //Mits id: 34975 - Design change - Govind - End
                GetAccordClaimMapping();
            }

        }
        private void GetAccordClaimMapping()
        {
            XmlDocument claimSubHistoryXmlDoc = new XmlDocument();
            XElement inputXml = null;
            try
            {
                inputXml = GetInputDocForISOClaimSubHistory();
                bReturnStatus = CallCWS("ISOClaimSubHistoryAdaptor.GetISOClaimSubHistory", inputXml, out sreturnValue, false, true);
                if (bReturnStatus)
                {
                    claimSubHistoryXmlDoc.LoadXml(sreturnValue);
                    rootElement = XElement.Parse(claimSubHistoryXmlDoc.OuterXml);
                    result = from claimHistory in rootElement.XPathSelectElements("//Row")
                             select claimHistory;
                }
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                //ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetInputDocForISOClaimSubHistory()
        {
            XElement inputXML = null;
            try
            {
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>6d21c66c-403c-4ba2-9b08-ee5c76296cd2</Authorization>");
                sXml = sXml.Append("<Call><Function>ISOClaimSubHistoryAdaptor.GetISOClaimSubHistory</Function></Call><Document><ISOClaimSubHistory>");
                sXml = sXml.Append("<SubmitToISOFlag></SubmitToISOFlag> <ClaimId>");
                sXml = sXml.Append(claimid.Value.ToString()); //Mits id: 34975 - Design change - Govind
                sXml = sXml.Append("</ClaimId></ISOClaimSubHistory></Document></Message>");

                inputXML = XElement.Parse(sXml.ToString());
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            return inputXML;
        }

        protected void SaveISOClaimSubHistory(object sender, EventArgs e)
        {
            XElement inputXML = null;
            try
            {
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>6d21c66c-403c-4ba2-9b08-ee5c76296cd2</Authorization>");
                sXml = sXml.Append("<Call><Function>ISOClaimSubHistoryAdaptor.SaveISOClaimSubSettings</Function></Call><Document><ISOClaimSubHistory>");
                sXml = sXml.Append("<SubmitToISOFlag>");
                sXml = sXml.Append(SubmitToISOFlagStatus.Text);
                sXml = sXml.Append("</SubmitToISOFlag> <ClaimId>");
                sXml = sXml.Append(claimid.Value.ToString()); //Mits id: 34975 - Design change - Govind
                sXml = sXml.Append("</ClaimId><SubType>");
                sXml = sXml.Append(subtype.Text);
                sXml = sXml.Append("</SubType></ISOClaimSubHistory></Document></Message>");
                inputXML = XElement.Parse(sXml.ToString());
                bReturnStatus = CallCWS("ISOClaimSubHistoryAdaptor.SaveISOClaimSubSettings", inputXML, out sreturnValue, false, true);

                //Mits id: 34975 - Design change - Govind - Start
                if (bReturnStatus)
                {
                    if (sreturnValue.Contains("Required Claimant"))
                    {
                        string sMessage = "No claimant record is attached to the claim. Please add a claimant before ISO submission.";    

                        string str = "alert('" + sMessage + "');self.close();";

                        ClientScript.RegisterClientScriptBlock(this.GetType(), "messageKey", str, true);

                    }
                    else
                    {
                        string sMessage = "The record has been updated successfully!";              

                        string str = "alert('" + sMessage + "');self.close();";

                        ClientScript.RegisterClientScriptBlock(this.GetType(), "messageKey", str, true);
                    }
                }
                //Mits id: 34975 - Design change - Govind - End
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

    }
}
