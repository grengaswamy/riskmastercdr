﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;


namespace Riskmaster.UI.ClaimCommnetSummary
{
    public partial class commentsummary : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
           try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";
                XmlDocument commentXDoc = new XmlDocument();
                DataTable objTable = new DataTable();
                XslCompiledTransform commentXSL = new XslCompiledTransform();
                DataRow objRow;
                if (!IsPostBack)
                {
                    formname.Text  = Request.QueryString["screenname"];
                    recordid.Text= Request.QueryString["recordid"];
                    XmlTemplate = GetMessageTemplate(formname.Text , recordid.Text);
                    bReturnStatus = CallCWSFunction("ClaimCommentSummaryAdaptor.GetXmlInfo", out sreturnValue, XmlTemplate);
                    if (bReturnStatus)
                    {
                        commentXDoc.LoadXml(sreturnValue);
                        XmlNodeList clist = commentXDoc.SelectNodes("//Report/Comments/data/Comment");
                        objTable.Columns.Add("comments");

                        for (int i = 0; i < clist.Count;i++ )
                        {
                            objRow = objTable.NewRow();
                            //bkumar33:cosmetic change
                            if (clist[i].Attributes["HeaderText"].Value != "")
                                objRow[0] = clist[i].Attributes["Header"].Value + ":" + clist[i].Attributes["HeaderText"].Value;
                            else
                                objRow[0] = clist[i].Attributes["Header"].Value;
                            //end
                            objTable.Rows.Add(objRow);
                            objRow = objTable.NewRow();
                            objRow[0] = clist[i].InnerText;
                            objTable.Rows.Add(objRow);
                            
                        }
                        objTable.AcceptChanges();
                        GrdViewComment.Visible = true;
                        GrdViewComment.DataSource = objTable;
                        GrdViewComment.DataBind();

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate(string sFormname,string sRecordid)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ClaimCommentSummary><ScreenFlag>");
            sXml = sXml.Append(sFormname);
            sXml = sXml.Append("</ScreenFlag><Id>");
            sXml = sXml.Append(sRecordid);
            sXml = sXml.Append("</Id><LangCode>");
            sXml = sXml.Append(AppHelper.GetLanguageCode());
            sXml = sXml.Append("</LangCode></ClaimCommentSummary></Document></Message>");
     
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
       

    }
}
