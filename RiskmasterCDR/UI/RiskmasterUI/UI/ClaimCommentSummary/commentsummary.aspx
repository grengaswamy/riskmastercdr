<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="commentsummary.aspx.cs" Inherits="Riskmaster.UI.ClaimCommnetSummary.commentsummary" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
    <title>Comment Summary</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript">
        //MITS 27964 hlv 4/10/2012 begin
        if (window.opener != null) {
            window.opener.window.parent.parent.iwintype = document.title;
        }

        window.onunload = function () {
            if (window.opener != null) {
                window.opener.window.parent.parent.iwintype = "";
            }
        }
        //MITS 27964 hlv 4/10/2012 end
    </script>
</head>
<body class="10pt">
    <form id="frmdata" runat="server">
        <asp:TextBox ID ="recordid"  runat ="server" style="display:none" value="1" RMXRef="Instance/Document/ClaimCommentSummary/recordid"/>
        <asp:TextBox ID ="formname" runat ="server" style="display:none" value="" RMXRef="Instance/Document/ClaimCommentSummary/formname" />
                <table>
	      <tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
         </tr> 
        <tr>
            <asp:GridView ID="GrdViewComment" runat="server" AutoGenerateColumns="False" Width="100%"  EnableViewState="true"  >
                <rowstyle CssClass="msgheader"  Font-Bold="true" HorizontalAlign="Left" />
                <alternatingrowstyle CssClass="None"  backcolor="White" Font-Bold="false" HorizontalAlign="Left"/>

            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%# Eval("comments")%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns> 
            </asp:GridView> 
        </tr>
         <tr align="center">
              <td colspan="2">
              <%--Yukti-ML Changes Start,MITS 34070--%> 
                    <%--<asp:Button  class="button" id="close" Text =" Close" runat="server" OnClientClick ="self.close();" />--%>
                    <asp:Button  class="button" id="btnClose" Text ="<%$ Resources:btnClose %>" runat="server" OnClientClick ="self.close();" />
                <%--Yukti-ML Changes End--%>
                    &#160;&#160;
                    <%--Yukti-ML Changes Start,MITS 34070--%>
                   <%-- Mits 16633:Asif	--%>
                    <%--<asp:Button class="button" id="printit" Text ="Print" runat="server" OnClientClick="window.frmData.close.style.display='none';window.frmData.printit.style.display='none';self.print();window.frmData.close.style.display='';window.frmData.printit.style.display='';"/>--%>
                    <asp:Button class="button" id="btnprintit" Text ="<%$ Resources:btnprintit %>" runat="server" OnClientClick="document.forms[0].btnClose.style.display = 'none';document.forms[0].btnprintit.style.display='none';self.print();document.forms[0].btnClose.style.display='';document.forms[0].btnprintit.style.display='';"/>
                    <%--Yukti-ML Changes End--%>
                    &#160;&#160;
              </td>
         </tr>
        </table>
                 
    </form>
    
</body>
</html>
