﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiagSearch.aspx.cs" Inherits="Riskmaster.SecondaryDiagSearch.DiagSearch"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Secondary Diagnosis Search</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="JavaScript">
		function PageLoaded()
		  {
			document.forms[0].method="post";
			return false;
		  }
		function ReturnDiagSearch()
          { 
          
            var sCodeText = document.forms[0].DIAGNOSIS_CODE_codelookup.value;
            var lCodeId = document.forms[0].DIAGNOSIS_CODE_codelookup_cid.value;
            var sControlName = document.forms[0].ControlName.value;
            var sParText = "";
            window.opener.DiagSearchSelected(sCodeText, lCodeId, sControlName);
            window.opener.setDataChanged(true);
		    window.close();
          }
	</script>
	
</head>
<body onload="PageLoaded();">
    <form id="frmData" runat="server">
    <!-- pen testing changes by atavaragiri MITS 27870 : -->
  <%--<input name="SysFormName" type="text" value='<%=AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("FormName"))%>' id="Text1" style="display:none" /><%--Parijat :19159 changes to accomodate filtering of diagnosis code--%> 
     <input name="SysFormName" type="text" value='<%=AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("FormName"))%>' id="SysFormName" style="display:none" /><%--Parijat :19159 changes to accomodate filtering of diagnosis code--%> 
     <%-- END:pen testing --%>
        <div style="text-align:center;">
            <table class="singleborder">
	            <tr>
		            <td colspan="6" class="ctrlgroup">
		            <asp:Label class="label" id="lblSubTitle" runat="server" Text="<%$ Resources:lblSubTitleC %>"></asp:Label>
		            </td>
	            </tr>
	            <tr>
		            <td class="datatd">
			            <b><%--Diagnosis: // rsushilaggar MITS 25692 date 08/31/2011--%><asp:Label runat="server" ID="lblName"></asp:Label></b>
		            </td>
                    <td>
                       <uc:CodeLookUp runat="server" ID="DIAGNOSIS_CODE" CodeTable="DIAGNOSIS_CODE" ControlName="DIAGNOSIS_CODE" rmxref="//DiagnosisCode" RMXType="code" DescSearch="1" ValidationGroup="vgSave" />
                    </td>    
	            </tr> 
	            <tr>
					<td colspan="2" align="center">
					    <asp:Button  class="button" id="btnOK" Text ="<%$ Resources:btnOK %>" runat="server" OnClientClick ="ReturnDiagSearch();" Width ="50px" />
					    &#160;
					    <asp:Button  class="button" id="btnCancel" Text ="<%$ Resources:btnCancel %>" runat="server" OnClientClick ="window.close();" Width ="50px" />
					</td>
				</tr>
            </table> 
            <%--pen testing changes:atavaragiri  MITS 27870--%>
            <%--<input id="Hidden1" type="hidden" value='<%=AppHelper.GetQueryStringValue("ControlName")%>'	/>--%>
            <input id="ControlName" type="hidden" value='<%=AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ControlName"))%>'	/>
            <%--END:pen testing--%>

            <%--pen testing changes :atavaragiri   MITS 27870--%>
            <%--<input id="Hidden1" type="hidden" value='<%=AppHelper.GetQueryStringValue("DeptEid")%>'	/><%--nadim 23191--%>
            <input id="DeptEid" type="hidden" value='<%=AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("DeptEid"))%>'	/><%--nadim 23191--%>
            <%--END:pen testing--%>

    	</div> 					
    </form>
</body>
</html>
