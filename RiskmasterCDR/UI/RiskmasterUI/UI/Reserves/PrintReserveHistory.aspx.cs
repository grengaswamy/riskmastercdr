﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.Data;
using System.Xml.Linq;
using System.Collections;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.UI.UI.Reserves
{
    public partial class PrintReserveHistory : NonFDMBasePageCWS
    {
        private XmlDocument oResponse = null;
        private string sReturn = "";
        ArrayList arrIndex = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            oResponse = new XmlDocument();
            if (!Page.IsPostBack)
            {
                try
                {

                    //Start averma62 - MITS 28261 - policy id and coverage id added in order to print reserve history
                    policyid.Text = AppHelper.GetQueryStringValue("Policyid");
                    polcvgid.Text = AppHelper.GetQueryStringValue("Polcvgid");
                    //End averma62 - MITS 28261 - policy id and coverage id added in order to print reserve history
                    claimid.Text = AppHelper.GetQueryStringValue("ClaimId");
                    unitid.Text = AppHelper.GetQueryStringValue("UnitId");
                    claimanteid.Text = AppHelper.GetQueryStringValue("ClaimantId");
                    claimnumber.Text = AppHelper.GetQueryStringValue("ClaimNumber");
                    lob.Text = AppHelper.GetQueryStringValue("Lob");
                    Reserve.Text = AppHelper.GetQueryStringValue("Reserve");
                    cvglossid.Text = AppHelper.GetQueryStringValue("CvgLossId");
                    // Manish Jain MITS 27276
                    //Caption.Value = AppHelper.GetQueryStringValue("Caption");
                    NonFDMCWSPageLoad("ReserveFundsAdaptor.GetReservesXML");
                    oResponse = Data;
                    sReturn = oResponse.OuterXml;

                    XElement objOuterXml = XElement.Parse(sReturn);

                    BindDataToXml(sReturn);
                    
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
                
            }
        }
        protected void grdReserve_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                    e.Row.CssClass = "colheader3";
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void grdReserve_RowDataBound(object Sender, GridViewRowEventArgs e)
        {
            try
            {
                int iTempIndex = 0;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView objDataRow = (DataRowView)e.Row.DataItem;

                    e.Row.ID = objDataRow["pid"].ToString();

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }

                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    arrIndex = new ArrayList();

                    foreach (TableCell cell in e.Row.Cells)
                    {
                        if ((cell.Text == "pid"))
                        {
                            cell.Visible = false;
                            arrIndex.Add(iTempIndex);
                        }
                        iTempIndex++;
                    }
                    e.Row.CssClass = "colheader3";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        private void BindDataToXml(string sReturnValue)
        {
            DataRow drReserveRow = null;
            DataTable dtReserve = new DataTable();

            XElement objReserveDoc = XElement.Parse(sReturnValue);

            dtReserve.Columns.Add("Reserve Date");
            dtReserve.Columns.Add("Reserve Amount");
            dtReserve.Columns.Add("Status");
            dtReserve.Columns.Add("User");
            dtReserve.Columns.Add("Reason");
            dtReserve.Columns.Add("pid");

            var objRowXml = from rows in objReserveDoc.Descendants("Reserve")

                            select rows;

           
            string[] arrPid = new string[1];
            string sDescendant = string.Empty;
            int iRowCount = 0;
            foreach (XElement row in objRowXml)
            {
                iRowCount = iRowCount + 1;
                arrPid[0] = "pid";
                grdReserve.DataKeyNames = arrPid;
                
                drReserveRow = dtReserve.NewRow();
                drReserveRow[0] = row.Attribute("Date").Value;
                drReserveRow[1] = row.Attribute("Amount").Value;
                drReserveRow[2] = row.Attribute("Status").Value;
                drReserveRow[3] = row.Attribute("User").Value;
                drReserveRow[4] = row.Attribute("Reason").Value;
                dtReserve.Rows.Add(drReserveRow);

            }
            
            // Bind DataTable to GridView.
            grdReserve.Visible = true;
            grdReserve.DataSource = dtReserve;
            grdReserve.DataBind();
        }
        
        
    }
}
