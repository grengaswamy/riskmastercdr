﻿<%@ Page Language="C#" Theme="RMX_Default" AutoEventWireup="true" CodeBehind="ReserveListing.aspx.cs" ValidateRequest="false" Inherits="Riskmaster.UI.UI.Reserves.ReserveListing" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>Web Reserves</title>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-reserves.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-custom.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/org.js"></script> <!--Added for Mits 18939--> 
     <script type="text/javascript" language="javascript" src="../../Scripts/form.js"></script><!--Added for Mits 23601--> 
    <script type="text/javascript" language="javascript">
        //function TransferToModifyReserve(Reserves) {
        function TransferToModifyReserve(reserveID, paidAmount, collectionAmount) { // mkaran2 : MITS 32837       
        //rupal:start
        if (ValidateClaimCurrency()) {
            //document.forms[0].Reserve.value = Reserves;
            // mkaran2 : MITS 32837 : Start           
            if (reserveID != null  && reserveID != undefined)
                document.forms[0].Reserve.value = reserveID;
            if (paidAmount != null && paidAmount != undefined)
                document.forms[0].Paid.value = paidAmount;
            if (collectionAmount != null  && collectionAmount != undefined)
                document.forms[0].Collections.value = collectionAmount;       
            // mkaran2 : MITS 32837 : End           
            //Deb: Update the code for MS Security HP not to submit the page directly     
            //document.forms[0].action = "ModifyReserve.aspx";
            //document.forms[0].method = "post";
            document.forms[0].action = "ReserveListing.aspx?Submit=true";
            document.forms[0].submit(); 
            //Deb: Update the code for MS Security HP not to submit the page directly
            return false;
        }
        else {
            return false;
        }
        //rupal:end
    }
    function ValidateAddPayment() {
        var bRet = RMX.Reserves.CheckForFrozenFlag();
        if (bRet == true) 
        {
            if (ValidateClaimCurrency()) 
            {
                return RMX.Reserves.NavigateToFunds("false");
            }
            else
                return false;
        }
        else 
        {
            return false;
        }
    }
        //Start asharma326 MITS 32848
    function ExportToVSS() {
        //alert(document.getElementById("hdnselectedreserve").value);
//        var varclaimanteid = document.getElementById('claimanteid').value;
//        debugger;
//        if ((varclaimanteid == "") || (varclaimanteid == "0")) {
//            alert("Reserve pushed to VSS only if claimant exist on a claim.");
//            return false;
//        }
//        else {
            if (document.getElementById("hdnselectedreserve").value == '0') {
                if (confirm("You are going to Push all Reserves attached with this Claimant to VSS. Do you want to Continue ?")) {
                    return true;
                }
                else { return false; }
            }
            else {
                return true;
            }
        //}
    }
    function RadioCheck(rb, reserve) {
            var gv = document.getElementById('grdReserve');
            var rbs = gv.getElementsByTagName("input");
            var row = rb.parentNode.parentNode;
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "radio") {
                    if (rbs[i].checked) {
                        document.getElementById("hdnselectedreserve").value = reserve;
                    }
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
    }

                <%-- START : ngupta73 : JIRA: RMA-4608  - RMA Swiss Re Financial Deatil History --%>
        function TransferToFinancialDetailHistory() {

            if (navigator.appName != "Netscape") {
                alert(ReserveListingValidations.UpgradeVersionforCorporate);
                return false;
            }

            var claimID = document.getElementById('claimid').value;
            var claimnumber = document.getElementById('claimnumber').value;
            var claimantID = document.getElementById('claimanteid').value;
            var claimantrowID = document.getElementById('claimantrowid').value;
            var unitid = document.getElementById('unitid').value;
            var subtitle = document.getElementById('subtitle').value;
            var frozenflag = document.getElementById('frozenflag').value;
            var fromFunds = document.getElementById('hdFromFunds').value;
            var CarrierClaim = "No";

            document.forms[0].action = "FinancialDetailHistory.aspx?ClaimID=" + claimID + "&ClaimantID=" + claimantID + "&ClaimNo=" + claimnumber + "&ClaimantRowID=" + claimantrowID + "&UnitID=" + unitid + "&Subtitle=" + subtitle + "&FrozenFlag=" + frozenflag + "&FromFunds=" + fromFunds + "&CarrierClaim=" + CarrierClaim;
            document.forms[0].method = "post";
            document.forms[0].submit();
            return false;
        }
        <%-- END : ngupta73 : JIRA: RMA-4608  - RMA Swiss Re Financial Detail History --%>

        //Ends asharma326 MITS 32848
    </script>
</head>

<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();RMX.Reserves.PageLoaded();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
	  <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
      <%--MITS 28450--%>
       <div class="toolBarButton" runat="server" id="div_enhancednotes" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return EnhancedNotes();" src="../../Images/tb_enhancednotes_active.png" width="28" height="28" border="0" id="enhancednotes" AlternateText="Enhanced Notes" title="Enhanced Notes" onMouseOver="this.src='../../Images/tb_enhancednotes_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_enhancednotes_active.png';this.style.zoom='100%'" />
        </div>
           <%--start - asharma326 --%>
        <div class="toolBarButton" runat="server" id="div_vssexport" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick = "return ExportToVSS();" onclick="VssExport_Click" src="../../Images/tb_exporttoVSS_active.png" width="28" height="28" border="0" id="VssExport" Visible="false" title="<%$ Resources:ttvssexport %>" AlternateText="<%$ Resources:ttvssexport %>" onMouseOver="this.src='../../Images/tb_exporttoVSS_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_exporttoVSS_active.png';this.style.zoom='100%'" />
        </div>
        <%--end - asharma326--%>
      </div>				
  <div id="maindiv" style="height:100%;width:99%;overflow:auto">
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimID" id="claimid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimNumber" ID="claimnumber" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/FrozenFlag" ID="frozenflag" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/SubTitle" ID="subtitle" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimantRowID" ID="claimantrowid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/UnitRowID" ID="unitrowid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimantEID" ID="claimanteid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/UnitID" ID="unitid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/LOB" ID="lob" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/IsTandE" ID="istandeinput" runat="server" Text="1" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ParentSecurityId" ID="ParentSecurityId" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/FormTitle" ID="FormTitle" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    			
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/SecurityId" ID="SecurityId" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" id="subtitleValue" runat="server"></asp:TextBox>
    <asp:TextBox style="display:none" id="caption" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@caption"></asp:TextBox>
    <asp:TextBox style="display:none" id="ShowRSW" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@ShowRSW"></asp:TextBox>
    <asp:TextBox style="display:none" id="istande" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@istande"></asp:TextBox>
    <asp:TextBox style="display:none" id="ScheduleCheckViewPermissionOnly" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@viewonly"></asp:TextBox>
    <asp:TextBox style="display:none" id="ScheduleCheckPermission" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@isschedulecheck"></asp:TextBox>
    <asp:TextBox style="display:none" id="add_payment" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@add_payment"></asp:TextBox>
    <asp:TextBox style="display:none" id="add_collection" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@add_collection"></asp:TextBox>
    <asp:TextBox style="display:none" id="QueuedTotal" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/totals/@QueuedTotal"></asp:TextBox>
    <asp:TextBox style="display:none" id="UseQueuedPayments" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/totals/@UseQueuedPayments"></asp:TextBox>
    <asp:TextBox style="display:none" id="claimstatus" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@claimstatus"></asp:TextBox>
    <asp:TextBox style="display:none" id="hdlob" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@lob"></asp:TextBox>
    <asp:TextBox style="display:none" id="hIsClaimCurrencySet" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@IsClaimCurrencySet"></asp:TextBox>
    <asp:textbox style="display: none" runat="server" id="currencytype" rmxref="/Instance/Claim/CurrencyType" /><%--//MITS:34082--%>
   <%--Mgaba2: MITS 28450 --%>
   <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXType="hidden" Text="ReserveListing" /> 
   <%--Mgaba2: MITS 28450 --%>
    <asp:HiddenField runat="server" ID="hdReadOnly" />
    <asp:HiddenField runat="server" ID="Reserve" />
    <asp:HiddenField runat="server" ID="hdFromFunds" />
         <asp:HiddenField runat="server" ID="hdFDHButton" /><%--pgupta93: RMA-4608--%>

	 <!-- mkaran2 : MITS 32837 : Start-->
      <asp:HiddenField runat="server" ID="Paid" />
      <asp:HiddenField runat="server" ID="Collections" />
      <!-- mkaran2 : MITS 32837 : End-->
	  <asp:HiddenField runat="server" ID="hdnselectedreserve" Value="0"/>
        <asp:Label runat="server" id="lblReserve" Text="<%$ Resources:lblReserve %>" style="display:none" ></asp:Label>
        <asp:Label runat="server" class="msgheader" id="errorhook" Visible="false" ></asp:Label>        
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
            <td>
                <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="100%" ></asp:Label>    
            </td>
            </tr>
            <tr>
                <td>
                        <asp:GridView ID="grdReserve" runat="server" OnRowCreated="grdReserve_RowCreated" OnRowDataBound="grdReserve_RowDataBound" Width="100%" CellPadding="4" HeaderStyle-CssClass="ctrlgroup2" RowStyle-HorizontalAlign="Center">
                        </asp:GridView>
                </td>
            </tr>    
                <tr>
							<td class="ctrlgroup2" colspan="6">&#160;</td>
						</tr>
            
        </table>
        <br />
        <!-- Mihika - RSW IC for MCIC -->
		<asp:Button runat="server" class="button" OnClientClick="return RMX.Reserves.OpenResWS();" Text="<%$ Resources:btnResWS %>" id="btnResWS" />
		<asp:Button runat="server" class="button" OnClientClick="return RMX.Reserves.AssignValue();" Text="<%$ Resources:btnPaymentHistory %>" id="btnPaymentHistory" OnClick="btnPaymentHistory_onclick"/>
          <asp:Button runat="server" class="button" Text="<%$ Resources:btnFDH %>" OnClientClick="return TransferToFinancialDetailHistory();" ID="btn_FDH" />

		<asp:Button runat="server" class="button" Text="<%$ Resources:btnTE %>" id="btnTE" OnClick="btnTE_onclick" OnClientClick="return ValidateClaimCurrency();" />
		<asp:Button runat="server" class="button" Text="<%$ Resources:btnAddPayment %>" OnClientClick='return ValidateAddPayment();' ID="btnAddPayment" />
		<asp:Button runat="server" class="button" Text="<%$ Resources:btnAddCollection %>" OnClientClick='bRet = RMX.Reserves.CheckForFrozenFlag();if(bRet == true) if(ValidateClaimCurrency()) return RMX.Reserves.NavigateToFunds("true"); else return false;else return false;' ID="btnAddCollection" />
		<asp:Button runat="server" class="button" Text="<%$ Resources:btnAutoCheck %>" OnClientClick='bRet = RMX.Reserves.CheckForFrozenFlag();if(bRet == true) if(ValidateClaimCurrency()) return RMX.Reserves.ScheduleChecks(); else return false;else return false;' ID="btnAutoCheck" />
		<asp:Button runat="server" class="button" Text="<%$ Resources:btnBack %>" ID="btnBack" Visible="false" OnClientClick="return LoadClaim();" />
        <asp:Label runat="server" ID="lblCurrencytype" Text="<%$ Resources:lblCurrencytype %>" Font-Bold="True"></asp:Label>
        <asp:DropDownList runat="server"  ID="drdCurrencytype" OnSelectedIndexChanged="drdCurrencytype_onchange" AutoPostBack="true"  rmxref="/Instance/Document/ReserveFunds/claimreserves/CurrencyType" itemsetref="/Instance/Document/ReserveFunds/claimreserves/CurrencyTypeList" rmxtype="combobox"></asp:DropDownList>
		<table width="100%" border="0" cellspacing="0" cellpadding="4">
						<tr></tr>
						<tr>
							
								<td>
                                <asp:Label runat="server" ID="lblQuedPaymentTotal" Text="<%$ Resources:lblQuedPaymentTotal %>" style="display:none"></asp:Label>
								<asp:Label runat="server" ID="lblQP"></asp:Label>
								</td>
							
						</tr>
					</table>	
						
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
