﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 12/16/2013 | 34082   | pgupta93   | Changes req for MultiCurrency
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.Data;
using System.Xml.Linq;
using System.Collections;
using Riskmaster.BusinessAdaptor.Common;
using System.Globalization;
using System.Threading;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Reserves
{
    public partial class ModifyReserve : NonFDMBasePageCWS
    {
        private XmlDocument oResponse = null;
        private string sReturn = "";
        ArrayList arrIndex = null;
        DateTimeFormatInfo fmt = null;
        bool bIsMultiCurrency = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ModifyReserve.aspx"), "ModifyReserveValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "ModifyReserveValidations", sValidationResources, true);
            
            //Praveen: ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
            //Praveen: ML Changes
            fmt = (new CultureInfo(sCulture)).DateTimeFormat;
            oResponse = new XmlDocument();
            if (!Page.IsPostBack)
            {
                try
                {
                    //claimid.Text = AppHelper.GetQueryStringValue("ClaimId");
                    //unitid.Text = AppHelper.GetQueryStringValue("UnitId");
                    //claimanteid.Text = AppHelper.GetQueryStringValue("ClaimantId");
                    //claimnumber.Text = Server.UrlDecode(AppHelper.GetQueryStringValue("ClaimNumber"));
                    //lob.Text = AppHelper.GetQueryStringValue("Lob");
                    //Reserve.Text = AppHelper.GetQueryStringValue("Reserve");
                    //ClaimantRowId.Value = AppHelper.GetQueryStringValue("ClaimantRowId");
                    //unitrowid.Text = AppHelper.GetQueryStringValue("UnitRowId");
                    //Caption.Value = Server.UrlDecode(AppHelper.GetQueryStringValue("caption"));
                    claimid.Text = AppHelper.GetFormValue("claimid");
                    unitid.Text = AppHelper.GetFormValue("unitid");
                    claimanteid.Text = AppHelper.GetFormValue("claimanteid");
                    claimnumber.Text = AppHelper.GetFormValue("claimnumber");
                    lob.Text = AppHelper.GetFormValue("lob");
                    Reserve.Text = AppHelper.GetFormValue("Reserve");
                    //BOB Reserves
                    policyid.Text = AppHelper.GetFormValue("policyid");
                    polcvgid.Text = AppHelper.GetFormValue("polcvgid");
                    ClaimantRowId.Value = AppHelper.GetFormValue("claimantrowid");
                    unitrowid.Text = AppHelper.GetFormValue("unitrowid");
                    Caption.Value = AppHelper.GetFormValue("caption");
                    // mkaran2 : MITS 32837 : Start
                    lblPaidAmount.Text = AppHelper.GetFormValue("Paid");
                    lblCollectionsAmount.Text = AppHelper.GetFormValue("Collections");
                    // mkaran2 : MITS 32837 : End  

                    NonFDMCWSPageLoad("ReserveFundsAdaptor.GetReservesXML");
                    oResponse = Data;
                    sReturn = oResponse.OuterXml;

                    XElement objOuterXml = XElement.Parse(sReturn);

                    BindDataToXml(sReturn);
                    CustomizePage();
                    ProcDate.Text = DateTime.Now.ToString(fmt.ShortDatePattern);
                    //pgupta93: RA-4608 START
                    if (!(string.IsNullOrEmpty(AppHelper.GetQueryStringValue("FormName"))))
                    {
                        hdFDHButton.Value = AppHelper.GetQueryStringValue("FormName");
                    }

                    //pgupta93: RA-4608 END

                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                }

            }
        }
        protected void grdReserve_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                    e.Row.CssClass = "colheader3";
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void grdReserve_RowDataBound(object Sender, GridViewRowEventArgs e)
        {
            try
            {
                int iTempIndex = 0;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView objDataRow = (DataRowView)e.Row.DataItem;

                    e.Row.ID = objDataRow["pid"].ToString();

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }

                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    arrIndex = new ArrayList();

                    foreach (TableCell cell in e.Row.Cells)
                    {
                        if ((cell.Text == "pid"))
                        {
                            cell.Visible = false;
                            arrIndex.Add(iTempIndex);
                        }
                        iTempIndex++;
                    }
                    e.Row.CssClass = "colheader3";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        private void BindDataToXml(string sReturnValue)
        {
            DataRow drReserveRow = null;
            DataTable dtReserve = new DataTable();
            //skhare7 MITs 30916
            XElement objReserveDoc = XElement.Parse(sReturnValue);
            if (objReserveDoc.XPathSelectElement("//Reserves") != null)
            {

                if (objReserveDoc.XPathSelectElement("//Reserves").Attribute("IsMultiCurrencyOn") != null)
                {
                    if (objReserveDoc.XPathSelectElement("//Reserves").Attribute("IsMultiCurrencyOn").Value.CompareTo("0") != 0)

                        bIsMultiCurrency = true;


                }

            }
            //skhare7 MITs 30916
            //dtReserve.Columns.Add("Reserve Date");
            //dtReserve.Columns.Add("Reserve Amount");
            //dtReserve.Columns.Add("Status");
            //dtReserve.Columns.Add("User");
            //dtReserve.Columns.Add("Reason");
            //dtReserve.Columns.Add("pid");

            dtReserve.Columns.Add(GetResourceValue("gvHdrReserveDate", "0"));
            dtReserve.Columns.Add(GetResourceValue("gvHdrReserveAmount", "0"));
            dtReserve.Columns.Add(GetResourceValue("gvHdrStatus", "0"));
            dtReserve.Columns.Add(GetResourceValue("gvHdrUser", "0"));
            dtReserve.Columns.Add(GetResourceValue("gvHdrReason", "0"));
            //Deb : MITS 31213
            dtReserve.Columns.Add(GetResourceValue("gvHdrChangeAmount", "0"));
            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            dtReserve.Columns.Add(GetResourceValue("gvHdrAssignedAdjuster", "0"));
            //Ankit End
            dtReserve.Columns.Add("pid");
            //dtReserve.Columns.Add(GetResourceValue("gvHdrpid", "0"));
            //Deb : MITS 31213
            var objRowXml = from rows in objReserveDoc.Descendants("Reserve")

                            select rows;
            //MGaba2: R8: SuperVisory Approval: Disabling Modify Button

            //if (objRowXml.Count() != 0 && objRowXml.First().Attribute("Status").Value == "H")//RMA-7078 
            if (objRowXml.Count() != 0 && objRowXml.First().Attribute("StatusParentCode").Value == "H")//RMA-7078 
            {
                btnModify.Enabled = false;
                btnApprove.Visible = true;
                btnReject.Visible = true;
                hdnRsvRowId.Text = objRowXml.First().Attribute("RsvRowId").Value;
                
                hdnRsvHistRowId.Text = objRowXml.First().Attribute("RsvHistRowId").Value; //mbahl3 Reserve Approval Enhancement Mits 32471
                //skhare7 R8 SuperVisory approval
                txtAppRejReqCom.Visible = true;
                lblReason.Visible = true;
                ///skhare7 R8 SuperVisory approval End
            }
            else
            {
                btnModify.Enabled = true;
                btnApprove.Visible = false;
                btnReject.Visible = false;
                //skhare7 R8 SuperVisory approval
                txtAppRejReqCom.Visible = false;
                lblReason.Visible = false;
                //skhare7 R8 SuperVisory approval End
            }

            string[] arrPid = new string[1];
            string sDescendant = string.Empty;
            int iRowCount = 0;
            foreach (XElement row in objRowXml)
            {
                iRowCount = iRowCount + 1;
                arrPid[0] = "pid";
                grdReserve.DataKeyNames = arrPid;

                drReserveRow = dtReserve.NewRow();
                drReserveRow[0] = AppHelper.GetDate(row.Attribute("Date").Value);
                drReserveRow[1] = row.Attribute("Amount").Value;
                drReserveRow[2] = row.Attribute("Status").Value;
                drReserveRow[3] = row.Attribute("User").Value;
                drReserveRow[4] = row.Attribute("Reason").Value;
                drReserveRow[5] = row.Attribute("ChangeAmount").Value;           //Ankit Start : Financial Enhancements - Change Amount change
                drReserveRow[6] = row.Attribute("AssignedAdjuster").Value;      //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                dtReserve.Rows.Add(drReserveRow);

            }
            bool bIsEmpty = false;
            if (dtReserve.Rows.Count == 0)
            {
                drReserveRow = dtReserve.NewRow();
                drReserveRow[0] = "";
                drReserveRow[1] = "";
                drReserveRow[2] = "";
                drReserveRow[3] = "";
                drReserveRow[4] = "";
                drReserveRow[5] = "";        //Ankit Start : Financial Enhancements - Change Amount change
                drReserveRow[6] = "";       //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                dtReserve.Rows.Add(drReserveRow);
                bIsEmpty = true;
            }

            // Bind DataTable to GridView.
            grdReserve.Visible = true;
            grdReserve.DataSource = dtReserve;
            grdReserve.DataBind();
            if (bIsEmpty)
            {
                grdReserve.Rows[0].Visible = false;
            }

        }
        ////Deb Multi Currency
        //protected override void InitializeCulture()
        //{
        //    if (Request.Form["txtcurrencytype"] != null)
        //    {
        //        if (!string.IsNullOrEmpty(Request.Form["txtcurrencytype"]))
        //        {
        //            Culture = Request.Form["txtcurrencytype"].Split('|')[1];
        //            UICulture = Request.Form["txtcurrencytype"].Split('|')[1];
        //            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.Form["txtcurrencytype"].Split('|')[1]);
        //            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Request.Form["txtcurrencytype"].Split('|')[1]);
        //        }
        //    }
        //    base.InitializeCulture();
        //}
        private void CustomizePage()
        {//skhare7 changes made for MIS 30916
            //Deb Multi Currency
            if (bIsMultiCurrency)
            {
                //MITS:34082 MultiCurrency START
                //if (!string.IsNullOrEmpty(currencytype.Text))
                if (!string.IsNullOrEmpty(ReservecurrencytypetextModify.Text))
                {
                    //lcurrencytype.Visible = true;
                    lblCurrencyType.Visible = true;
                    //Culture = currencytype.Text.Split('|')[1];
                    //UICulture = currencytype.Text.Split('|')[1];
                    //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(currencytype.Text.Split('|')[1]);
                    //Thread.CurrentThread.CurrentUICulture = new CultureInfo(currencytype.Text.Split('|')[1]);
                    //lcurrencytype.Text = currencytype.Text.Split('_')[0];
                    ReservecurrencytypetextModify.Visible = true;
                    ReservecurrencytypetextModifybtn.Visible = true;
                    ReservecurrencytypetextModify.Text = ReservecurrencytypetextModify.Text.Split('_')[0];
                    //MITS:34082 MultiCurrency END
                }
            }
            else
            {
                //MITS:34082 MultiCurrency START
                //lcurrencytype.Visible = false;
                lblCurrencyType.Visible = false;
                ReservecurrencytypetextModify.Visible = false;
                ReservecurrencytypetextModifybtn.Visible = false;
                //MITS:34082 MultiCurrency END
            
            }
        }
        protected void btnCancel_onclick(object sender, EventArgs e)
        {
            //string sRedirectString = "ReserveListing.aspx?" + "ClaimId=" + claimid.Text + "&ClaimantEId=" + claimanteid.Text + "&ClaimantId=" + ClaimantRowId.Value + "&UnitId=" + unitid.Text + "&UnitRowId=" + unitrowid.Text;
            string sRedirectString = "ReserveListing.aspx?" + "ClaimId=" + claimid.Text + "&ClaimantEId=" + claimanteid.Text + "&ClaimantId=" + ClaimantRowId.Value + "&UnitId=" + unitid.Text + "&UnitRowId=" + unitrowid.Text + "&FormName=" + hdFDHButton.Value;//RMA-4608 pgupta93

            Server.Transfer(sRedirectString);
        }
        protected void btnModify_onclick(object sender, EventArgs e)
        {
            CallCWS("ReserveFundsAdaptor.ModifyReserves", null, out sReturn, true, false);
            ErrorControl1.errorDom = sReturn;
            ////Amandeep 10/02/2011 MITS 26522 ---start
            //XElement objOuterXml = XElement.Parse(sReturn);
            //if (!ErrorControl1.errorFlag)
            //{
            //    string sRedirectString = "ReserveListing.aspx?" + "ClaimId=" + claimid.Text + "&ClaimantEId=" + claimanteid.Text + "&ClaimantId=" + ClaimantRowId.Value + "&UnitId=" + unitid.Text + "&UnitRowId=" + unitrowid.Text;
            //    if (objOuterXml.XPathSelectElement("//claimreserve") != null)
            //    {
            //        if (objOuterXml.XPathSelectElement("//claimreserve").Attribute("reservestatus").Value == "H")
            //        {
            //            ClientScriptManager csm = Page.ClientScript;
            //            string sMessage = RMXResourceProvider.GetSpecificObject("ApprovalStatID", RMXResourceProvider.PageId("ModifyReserve.aspx"), ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            //            string str = String.Format("<script>alert('{0}');window.location.href='{1}'</script>", sMessage, sRedirectString);
            //            csm.RegisterClientScriptBlock(this.GetType(), "dd", str);
            //            return;
            //        }
            //    }
            //    Server.Transfer(sRedirectString);
            //}  //Amandeep ---end         

            // mkaran2 - MITS 34609 - Start
            ClientScriptManager csm = Page.ClientScript;
            string sMessage = string.Empty;
            string str = string.Empty;
            string sRedirectString = string.Empty;
            string sWarningMessage = string.Empty;

            XElement objOuterXml = XElement.Parse(sReturn);
            if (!ErrorControl1.errorFlag)
            {
                //sRedirectString = "ReserveListing.aspx?" + "ClaimId=" + claimid.Text + "&ClaimantEId=" + claimanteid.Text + "&ClaimantId=" + ClaimantRowId.Value + "&UnitId=" + unitid.Text + "&UnitRowId=" + unitrowid.Text;
                sRedirectString = "ReserveListing.aspx?" + "ClaimId=" + claimid.Text + "&ClaimantEId=" + claimanteid.Text + "&ClaimantId=" + ClaimantRowId.Value + "&UnitId=" + unitid.Text + "&UnitRowId=" + unitrowid.Text + "&FormName=" + hdFDHButton.Value;//RMA-4608 pgupta93

                if (objOuterXml.XPathSelectElement("//claimreserve") != null)
                {
                    if ((objOuterXml.XPathSelectElement("//MsgStatusCd").Value == "Success"))
                    {
                        if (objOuterXml.XPathSelectElement("//ExtendedMsgType") != null)
                        {
                            if ((objOuterXml.XPathSelectElement("//ExtendedMsgType").Value == "Warning"))
                            {
                                sWarningMessage = @"Following warning(s) reported: ";
                                IEnumerable<XElement> list = objOuterXml.XPathSelectElements("//ExtendedStatus");
                                foreach (XElement el in list)
                                {
                                    if ((el.XPathSelectElement("ExtendedMsgType").Value == "Warning"))
                                    {
                                        sWarningMessage = String.Format("{0} \\n {1}", sWarningMessage, ErrorHelper.UpdateErrorMessage(el.XPathSelectElement("ExtendedStatusDesc").Value));
                                    }
                                }
                            }
                        }
                    }

                    if (objOuterXml.XPathSelectElement("//claimreserve").Attribute("reservestatus").Value == "H")
                    {
                        if (!string.IsNullOrEmpty(sWarningMessage))
                        {
                            str = String.Format("<script>alert('{0}')</script>", sWarningMessage);
                            csm.RegisterClientScriptBlock(this.GetType(), "Warning", str);
                        }

                        sMessage = RMXResourceProvider.GetSpecificObject("ApprovalStatID", RMXResourceProvider.PageId("ModifyReserve.aspx"), ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());

                        if (objOuterXml.XPathSelectElement("//claimreserve").Attribute("reserveholdreasondesc") != null 
                            && !string.IsNullOrEmpty(objOuterXml.XPathSelectElement("//claimreserve").Attribute("reserveholdreasondesc").Value))
                        {
                            sMessage = sMessage + " as " + objOuterXml.XPathSelectElement("//claimreserve").Attribute("reserveholdreasondesc").Value; 
                        }

                        str = String.Format("<script>alert('{0}');window.location.href='{1}'</script>", sMessage, sRedirectString);
                        csm.RegisterClientScriptBlock(this.GetType(), "dd", str);

                        return;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(sWarningMessage))
                        {
                            str = String.Format("<script>alert('{0}');window.location.href='{1}'</script>", sWarningMessage, sRedirectString);
                            csm.RegisterClientScriptBlock(this.GetType(), "dd", str);
                            return;
                        }

                    }
                }

                Server.Transfer(sRedirectString);
            }  
            // mkaran2 - MITS 34609 - End
        }

        protected void btnApprove_onclick(object sender, EventArgs e)
        {

            hdnAction.Text = "Approve";
            NonFDMCWSPageLoad("ReserveFundsAdaptor.ApproveOrRejectReserve");

            // NonFDMCWSPageLoad("ReserveFundsAdaptor.GetReservesXML");
            oResponse = Data;
            sReturn = oResponse.OuterXml;

            XElement objOuterXml = XElement.Parse(sReturn);

            BindDataToXml(sReturn);
            CustomizePage();

        }
        protected void btnReject_onclick(object sender, EventArgs e)
        {
            hdnAction.Text = "Reject";
            NonFDMCWSPageLoad("ReserveFundsAdaptor.ApproveOrRejectReserve");
            //NonFDMCWSPageLoad("ReserveFundsAdaptor.GetReservesXML");
            oResponse = Data;
            sReturn = oResponse.OuterXml;

            XElement objOuterXml = XElement.Parse(sReturn);

            BindDataToXml(sReturn);
            CustomizePage();
        }

        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("ModifyReserve.aspx"), strResourceType).ToString();
        }
    }
}
