﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReserveSummary.aspx.cs" Inherits="Riskmaster.UI.UI.Reserves.ReserveSummary" EnableViewStateMac="false" %>
<%--/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            
**********************************************************************************************
* 15/03/2014 | 34275  | abhadouria | RMA Swiss Re Financial Summary
**********************************************************************************************/--%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
    <title>Web Reserves</title>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-reserves.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-custom.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/org.js"></script> <!--Added for Mits 18939--> 
    <script type="text/javascript" language="javascript">
        function BackToClaim()
         {
//             document.forms[0].action = "ReserveListingBOB.aspx";
//             document.forms[0].method = "post";
             //  document.forms[0].submit();
             var sRedirectString = "/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=" + document.getElementById('claimid').value;
             window.location.href = sRedirectString;
             //document.forms[0].SysCmd.value = 7;
             //document.forms[0].submit();
             return false;
         }

         // akaushik5 Added for MITS 33577 Starts
         function reserveSummary(claimid, claimantid) {
             var wnd = window.open("/RiskmasterUI/UI/Reserves/ReserveSummary.aspx?claimid=" + claimid + "&claimantid=" + claimantid, null, 'width=1035,height=335,resizable=no,scrollbars=yes');
             return false;
         }
         // akaushik5 Added for MITS 33577 Ends
    </script>
</head>

<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
					
  <div id="maindiv" style="height:100%;width:99%;overflow:auto">
        <%-- MITS 34275- RMA Swiss Re Financial Summary START --%>
        <table>
            <tr>
                <td>
                    <asp:Label runat="server" ID="Level" Text="Level: " Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="drdFS" AutoPostBack="true"  rmxtype="combobox" OnSelectedIndexChanged = "drdFS_onchange" >
                    <asp:ListItem value="1" Text ="POLICY - Financial Summary"  ></asp:ListItem>
                    <asp:ListItem value="2" Text ="UNIT - Financial Summary"  ></asp:ListItem>
                    <asp:ListItem value="3" Text ="COVERAGE - Financial Summary"  ></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
              <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/selectedcurrencytype"
            ID="selectedcurrencytype" runat="server" rmxignoreget="true"></asp:TextBox>
           <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/previouscurrencytype"
            ID="previouscurrencytype" runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/PolicyID"
            ID="PolicyId" runat="server" rmxignoreget="true"></asp:TextBox>
            <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/UnitID"
            ID="UnitId" runat="server" rmxignoreget="true"></asp:TextBox>
            <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/CoverageID"
            ID="CoverageId" runat="server" rmxignoreget="true"></asp:TextBox>
           <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/Selected"
            ID="Selected" runat="server" rmxignoreget="true"></asp:TextBox>
         <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/Formlevel"
            ID="FormLevel" runat="server" rmxignoreget="true"></asp:TextBox>
            <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/Multicurrency"
            ID="Multicurrencyindicator" runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/claimcurrency"
            ID="claimcurrency" runat="server" rmxignoreget="true"></asp:TextBox>
      <%--Ashish Ahuja - MITS 34275-Display header start--%>
      <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/policyname"
            ID="PolicyName" runat="server" rmxignoreget="true"></asp:TextBox>
      <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/unit"
            ID="Unit" runat="server" rmxignoreget="true"></asp:TextBox>
      <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/coverage"
            ID="Coverage" runat="server" rmxignoreget="true"></asp:TextBox>
      <%--//Ashish Ahuja - MITS 34275-Display header stop--%> 
        <%-- MITS 34275- RMA Swiss Re Financial Summary END --%>
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/ClaimID"
            ID="claimid" runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/ClaimNumber"
            ID="claimnumber" runat="server" rmxignoreget="true"></asp:TextBox>
    <%--akaushik5 Changed for MITS 33577 Starts--%>
    <%--<asp:TextBox style="display:none" id="caption" runat="server"  ></asp:TextBox>--%>
	<asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/claimreserves/@caption" id="caption" runat="server"  ></asp:TextBox>
    <%--akaushik5 Changed for MITS 33577 Ends--%>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/SubTitle" ID="subtitle" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimantRowID" ID="claimantrowid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/UnitRowID" ID="unitrowid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimantEID" ID="claimanteid" runat="server" rmxignoreget="true"></asp:TextBox>
   
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/LOB" ID="lob" runat="server" rmxignoreget="true"></asp:TextBox>
   
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ParentSecurityId" ID="ParentSecurityId" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/FormTitle" ID="FormTitle" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    			
  
    <asp:TextBox style="display:none" id="hdlob" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/@lob"></asp:TextBox>
    <asp:HiddenField runat="server" ID="hdReadOnly" />
    <asp:HiddenField runat="server" ID="Reserve" />
       
        <asp:Label runat="server" class="msgheader" id="errorhook" Visible="false"></asp:Label>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
            <td>
                <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="100%"></asp:Label>    
            </td>
             
            </tr>
            <tr>
                <td>
                          <asp:GridView ID="grdReserve" runat="server" OnRowCreated="grdReserve_RowCreated" OnRowDataBound="grdReserve_RowDataBound" Width="100%" CellPadding="4" HeaderStyle-CssClass="ctrlgroup2" RowStyle-HorizontalAlign="Center">
                        </asp:GridView>
                </td>
            </tr>    
                <tr>
							<td class="ctrlgroup2" colspan="6">&#160;</td>
						</tr>
            
        </table>
        <%-- akaushik5 Added for MITS 33577 Starts--%>
        <br />
        <table width="100%" border="0" cellspacing="0" cellpadding="4" id="tblClaimant" runat="server">
            <tr>
                <td>
                    <asp:Label runat="server" class="msgheader" ID="lblClaimants" Text="Claimants" Width="100%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="grdClaimants" runat="server" OnRowCreated="grdClaimants_RowCreated" OnRowDataBound="grdClaimants_RowDataBound" Width="100%" CellPadding="4" HeaderStyle-CssClass="ctrlgroup2" RowStyle-HorizontalAlign="Center">
                    </asp:GridView>
                </td>
            </tr>    
            <tr>
				<td class="ctrlgroup2" colspan="6">&#160;</td>
			</tr>
        </table>
        <%-- akaushik5 Added for MITS 33577 Ends--%>
        <br />
        <!-- Mihika - RSW IC for MCIC -->
		
		<table width="100%" border="0" cellspacing="0" cellpadding="4">
						<tr>
							
								<td>
		<asp:Button runat="server" class="button" Text="Back " ID="btnBack" 
                                        OnClientClick="return BackToClaim();" />
        <%-- akaushik5 Added for MITS 33577 Starts--%>
		<asp:Button runat="server" class="button" Text="Close " ID="btnClose" OnClientClick="return window.close();" Visible ="false" />
        <%-- akaushik5 Added for MITS 33577 Ends--%>
								</td>
						</tr>
					</table>	
         <%--  MITS 34275- RMA Swiss Re Financial Summary START --%>
           <div align="right">
        <table>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblCurrencytype" Text="Currency Type:"
                        Font-Bold="True"></asp:Label>
                </td>
                <td>
                 <%-- <usc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9; " ID="Reservecurrencytypetext"
                                CodeTable="CURRENCY_TYPE" ControlName="Reservecurrencytypetext" RMXRef="/Instance/Document/Reserves/CurrencyTypeForClaim"
                                CodeFilter="" RMXType="code" TabIndex="40" />--%>
                    <asp:DropDownList runat="server" ID="drdCurrencytype" onselectedindexchanged = "drdCurrencytype_onchange"
                        AutoPostBack="true" rmxref="/Instance/Document/ReserveFunds/claimreserves/CurrencyType"  itemsetref="/Instance/Document/ReserveFunds/claimreserves/Multicurrency/CurrencyTypeList"
                        rmxtype="combobox">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
    <%-- MITS 34275- RMA Swiss Re Financial Summary END --%>
						
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
