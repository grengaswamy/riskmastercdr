﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using AjaxControlToolkit;
using System.Text;
using System.Web.Services;
using System.Collections.Specialized;
using System.Threading;
using System.Globalization;
using Riskmaster.UI.Shared.Controls; 
using Riskmaster.RMXResourceManager;
using Riskmaster.Common;
using System.ServiceModel.Channels;
using System.Collections;

namespace Riskmaster.UI.UI.Reserves
{
    public partial class MoveFinancials : NonFDMBasePageCWS
    {

        public DataTable GridDataSource
        {
            get
            {
                return (DataTable)ViewState["GRID_DATA"];
            }
            set
            {
                ViewState["GRID_DATA"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            string sResponse = string.Empty;
           
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MoveFinancials.aspx"), "MoveFinancialsValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MoveFinancialsValidations", sValidationResources, true);
                if (!IsPostBack)
                {
                    
                    txtClaimId.Text = AppHelper.GetQueryStringValue("ClaimID");
                    txtReserveID.Text = AppHelper.GetQueryStringValue("ReserveID");
                    txtCovgID.Text = AppHelper.GetQueryStringValue("CovgID");
                    txtPolicyUnitRowId.Text = AppHelper.GetQueryStringValue("PolicyUnitRowId");
                    txtPolicyId.Text = AppHelper.GetQueryStringValue("PolicyId");
                    txtLOB.Text = AppHelper.GetQueryStringValue("LOB");
                    txtLossCode.Text = AppHelper.GetQueryStringValue("LossCode");
                    txtClaimantEid.Text = AppHelper.GetQueryStringValue("ClaimantEid");
                    lblSubTypeText.Text = AppHelper.GetQueryStringValue("ReserveSubType");
                    lblSourceBalText.Text = AppHelper.GetQueryStringValue("ReserveBalanceAmount");
                    lblSourceIncurredText.Text = AppHelper.GetQueryStringValue("Incurred");
                    lblSourceRsvTypeText.Text = AppHelper.GetQueryStringValue("ReserveType");

                    lblSourceLossTypeText.Text = AppHelper.GetQueryStringValue("Loss");
                    if (txtLOB.Text == "243")
                    {
                      
                        cmbCoverageLossType.Visible = false;
                        lblLossType.Visible = false;
                     //   lblsourceLossType.Visible = false;
                        lblsourceLossType.Visible = false;
                        lblsourceDisabilityType.Visible = true;
                        
                    }
                    else
                    {
                        //lblsourceDisabilityType.Visible = false;
                        cmbCovDisabilityType.Visible = false;
                        lblDisabilityType.Visible = false;
                        lblsourceLossType.Visible = true;
                        lblsourceDisabilityType.Visible = false;
                        
                    }                   
                    sResponse = AppHelper.CallCWSService(GetMessageTemplate());
                    BindData(sResponse);
                    InitializeTree(true, false);
                    SourceTree.Visible = true;
                    TargetTree.Visible = false;
                   
                }
               
                if (txtFunctionToCall.Text == "MarkPolicyInvalid")
                {
                    sResponse = AppHelper.CallCWSService(GetMessageTemplateForInvalidPolicy());
                  XmlDocument  objReturnXml = new XmlDocument();
                    objReturnXml.LoadXml(sResponse);
                    string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                    if (sMsgStatus == "Success")
                    {

                        ClientScript.RegisterStartupScript(this.GetType(), "script", "MoveToReserveListing();", true);
                    }
                    else
                    {
                        ErrorControl1.errorDom = sResponse;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void BindData(string sResponse)
        {
            ListItem lvItem = null;
            XmlDocument objXmlDoc = null;
            DataSet objDataSet = null;
            cmbSourcePolicy.Items.Clear();
            cmbTargetPolicy.Items.Clear();

            objXmlDoc = new XmlDocument();
            objXmlDoc.LoadXml(sResponse);
            objDataSet = new DataSet();
            objDataSet.ReadXml(new XmlNodeReader(objXmlDoc));

            

           

            
            foreach (DataRow drow in objDataSet.Tables["Data"].Rows)
            {
             
                lvItem = new ListItem();
                lvItem.Text = drow[0].ToString();
                lvItem.Value = drow[1].ToString();
                cmbSourcePolicy.Items.Add(lvItem);
              
                
                lvItem = null;
            }

            cmbSourcePolicy.SelectedValue = txtPolicyId.Text;


            lvItem = new ListItem();
            lvItem.Text = RMXResourceProvider.GetSpecificObject("lblTargetPolicy", RMXResourceProvider.PageId("MoveFinancials.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            lvItem.Value = "0";
            cmbTargetPolicy.Items.Add(lvItem);
            lvItem = null;


            foreach (DataRow drow in objDataSet.Tables["Data"].Rows)
            {
                lvItem = new ListItem();
                lvItem.Text = drow[0].ToString();
                lvItem.Value = drow[1].ToString();
               
                cmbTargetPolicy.Items.Add(lvItem);
                lvItem = null;
            }

            cmbTargetPolicy.SelectedValue = "0";
            
            if (txtLOB.Text == "243")
            {
                DropDownList cmbCovDisabilityType = (DropDownList)(Page.FindControl("cmbCovDisabilityType"));

                foreach (DataRow drow in objDataSet.Tables["disability"].Rows)
                {
                    lvItem = new ListItem();
                    lvItem.Text = drow[0].ToString();
                    lvItem.Value = drow[1].ToString();
                    cmbCovDisabilityType.Items.Add(lvItem);

                    lvItem = null;
                }

                cmbCovDisabilityType.SelectedValue = txtLossCode.Text;

                //  lblSourceLossTypeText.Text = cmbCovDisabilityType.SelectedItem.Text;
            }
            else
            {
                DropDownList cmbCoverageLossType = (DropDownList)(Page.FindControl("cmbCoverageLossType"));

                foreach (DataRow drow in objDataSet.Tables["loss"].Rows)
                {
                    lvItem = new ListItem();
                    lvItem.Text = drow[0].ToString();
                    lvItem.Value = drow[1].ToString();
                    cmbCoverageLossType.Items.Add(lvItem);

                    lvItem = null;
                }

                cmbCoverageLossType.SelectedValue = txtLossCode.Text;

                //lblSourceLossTypeText.Text = cmbCoverageLossType.SelectedItem.Text;
            }

            DropDownList cmbReserveType = (DropDownList)(Page.FindControl("cmbReserveType"));
            foreach (DataRow drow in objDataSet.Tables["reserve"].Rows)
            {
                lvItem = new ListItem();
                lvItem.Text = drow[0].ToString();
                lvItem.Value = drow[1].ToString();
                cmbReserveType.Items.Add(lvItem);

                lvItem = null;


            }



            cmbReserveType.SelectedValue = txtReserveID.Text;
            DropDownList cmbClaimant = (DropDownList)(Page.FindControl("cmbClaimant"));
            foreach (DataRow drow in objDataSet.Tables["claimant"].Rows)
            {
                lvItem = new ListItem();
                lvItem.Text = drow[0].ToString() + " " + drow[1].ToString();
                lvItem.Value = drow[2].ToString();
                cmbClaimant.Items.Add(lvItem);

                lvItem = null;
            }

            cmbClaimant.SelectedValue = txtClaimantEid.Text;

            // lblSourceClaimantText.Text = cmbClaimant.SelectedItem.Text;
            lvItem = null;

            objXmlDoc = null;
            objDataSet = null;
            sResponse = string.Empty;

            lblSourceClaimantText.Text = cmbClaimant.SelectedItem.Text;
        }

        private string GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>MoveFinancialsAdaptor.GetFinancialData</Function></Call><Document><GetPolicies>");

            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(txtClaimId.Text);
            sXml = sXml.Append("</ClaimID>");
            sXml = sXml.Append("<CovgID>");
            sXml = sXml.Append(txtCovgID.Text);
            sXml = sXml.Append("</CovgID>");
            sXml = sXml.Append("<PolicyUnitRowId>");
            sXml = sXml.Append(txtPolicyUnitRowId.Text);
            sXml = sXml.Append("</PolicyUnitRowId>");

            sXml = sXml.Append("<PolicyId>");
            sXml = sXml.Append(txtPolicyId.Text);
            sXml = sXml.Append("</PolicyId>");

            sXml = sXml.Append("<LOB>");
            sXml = sXml.Append(txtLOB.Text);
            sXml = sXml.Append("</LOB>");
            sXml = sXml.Append("</GetPolicies></Document></Message>");

            return sXml.ToString();
        }

        private string GetMessageTemplateForInvalidPolicy()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>MoveFinancialsAdaptor.MarkPolicyInvalid</Function></Call><Document><InvalidPolicy>");

            sXml = sXml.Append("<SourcePolicyId>");
            sXml = sXml.Append(cmbSourcePolicy.SelectedValue);
            sXml = sXml.Append("</SourcePolicyId>");

            sXml = sXml.Append("</InvalidPolicy></Document></Message>");

            return sXml.ToString();
        }
        private string GetMessageTemplateForEditReserve()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>MoveFinancialsAdaptor.EditReserveKey</Function></Call><Document><EditReserve>");

            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(txtClaimId.Text);
            sXml = sXml.Append("</ClaimID>");

            sXml = sXml.Append("<RCRowId>");
            sXml = sXml.Append(txtReserveID.Text);
            sXml = sXml.Append("</RCRowId>");
            sXml = sXml.Append("<LOB>");
            sXml = sXml.Append(txtLOB.Text);
            sXml = sXml.Append("</LOB>");
            sXml = sXml.Append("<NewClaimantEid>");
            sXml = sXml.Append(cmbClaimant.Text);
            sXml = sXml.Append("</NewClaimantEid>");

            sXml = sXml.Append("<NewLossType>");
            if (txtLOB.Text == "243")
            {
                sXml = sXml.Append(cmbCovDisabilityType.Text);
            }
            else
            {
                sXml = sXml.Append(cmbCoverageLossType.Text);
            }
            sXml = sXml.Append("</NewLossType>");

            sXml = sXml.Append("<NewDisabilityType>");
            sXml = sXml.Append("0");
            sXml = sXml.Append("</NewDisabilityType>");
            sXml = sXml.Append("<NewReserveType>");
            sXml = sXml.Append(cmbReserveType.Text);
            sXml = sXml.Append("</NewReserveType>");

            sXml = sXml.Append("<TargetPolicyId>");
            sXml = sXml.Append(txtPolicyId.Text);
            sXml = sXml.Append("</TargetPolicyId>");

            sXml = sXml.Append("<NewReserveSubType>");
            sXml = sXml.Append("0");
            sXml = sXml.Append("</NewReserveSubType>");
            sXml = sXml.Append("</EditReserve></Document></Message>");
         
            return sXml.ToString();


        }

        private string GetMessageTemplateForBulkMove(string sMappedIds)
        {


            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>MoveFinancialsAdaptor.BulkFinancialMove</Function></Call><Document><BulkMove>");

            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(txtClaimId.Text);
            sXml = sXml.Append("</ClaimID>");

            sXml = sXml.Append("<MappedIds>");
            sXml = sXml.Append(sMappedIds);
            sXml = sXml.Append("</MappedIds>");

            sXml = sXml.Append("<TargetPolicyId>");
            sXml = sXml.Append(cmbTargetPolicy.SelectedValue);
            sXml = sXml.Append("</TargetPolicyId>");

            sXml = sXml.Append("<SourcePolicyId>");
            sXml = sXml.Append(cmbSourcePolicy.SelectedValue);
            sXml = sXml.Append("</SourcePolicyId>");


            

            sXml = sXml.Append("</BulkMove></Document></Message>");

            return sXml.ToString();


        }
        // private string GetMessageTemplateForTree(string sNodeValue,string sEventSender)

        private string GetMessageTemplateForTree(string sPolicyId)
        {
            

           
           StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>MoveFinancialsAdaptor.GetTreeData</Function></Call><Document><GetTreeData>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(txtClaimId.Text);

            sXml = sXml.Append("</ClaimID>");
            sXml = sXml.Append("<PolicyId>");
            sXml = sXml.Append(sPolicyId);

            sXml = sXml.Append("</PolicyId>");



            //sXml = sXml.Append("<NodeValue>");
            //sXml = sXml.Append(sNodeValue);
            //sXml = sXml.Append("</NodeValue>");

            sXml = sXml.Append("</GetTreeData></Document></Message>");

            return sXml.ToString();
            

        }

        protected void SourceTree_Change(Object sender, EventArgs e)
        {
            if (TargetTree.Nodes.Count > 0)
            {
                TreeNode objTargetPolicyNode = TargetTree.FindNode("P" + cmbTargetPolicy.SelectedValue);
                objTargetPolicyNode.SelectAction = TreeNodeSelectAction.None;
                objTargetPolicyNode.Selected = false;
                if(TargetTree.SelectedNode!=null)
                TargetTree.SelectedNode.Selected = false;

                switch (SourceTree.SelectedNode.Value.Substring(0, 1))
                {
                    case "P":
                        objTargetPolicyNode.Selected = true;
                        objTargetPolicyNode.SelectAction = TreeNodeSelectAction.Select;

                        break;
                    case "C":
                        TargetTree.ExpandAll();
                        break;
                    case "U":
                        TargetTree.FindNode("P" + cmbTargetPolicy.SelectedValue).Expand();

                        break;
                }


                foreach (TreeNode objUnitnode in objTargetPolicyNode.ChildNodes)
                {


                    if ((objUnitnode.ChildNodes.Count > 0) && (SourceTree.SelectedNode.Value.Substring(0, 1) == "U"))
                    {
                        objUnitnode.SelectAction = TreeNodeSelectAction.Select;

                    }
                    else
                    {
                        objUnitnode.SelectAction = TreeNodeSelectAction.None;
                    }
                    foreach (TreeNode objCovNode in objUnitnode.ChildNodes)
                    {
                        if (SourceTree.SelectedNode.Value.Substring(0, 1) == "C")
                        {
                            objCovNode.SelectAction = TreeNodeSelectAction.Select;

                        }
                        else
                        {
                            objCovNode.SelectAction = TreeNodeSelectAction.None;
                        }
                    }
                }

            }
        }


        private void ValidateMapping()
        {
            string value;
            DataTable objReturnTable = null;
            bool bUnitsMapped = false;
            bool bCovsMapped = false;
            bool bReturn = false;
            bool bRemoveCss = false;

            txtMappedIds.Text = string.Empty;
            if (SourceTree.SelectedNode == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "script", "SelectSource();", true);
                return;
            }

            if (TargetTree.SelectedNode == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "script", "SelectTarget();", true);
                return;
            }

            if (SourceTree.SelectedNode.Value == TargetTree.SelectedNode.Value)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "script", "SimiliarRecord();", true);
                return;
            }

            if (SourceTree.SelectedNode.Value.StartsWith("P") && TargetTree.SelectedNode.Value.StartsWith("P"))
            {
             

                txtIsPolicyMove.Text = "true";
                
                objReturnTable = GetPolicyUnits(SourceTree.SelectedNode, TargetTree.SelectedNode, ref bReturn);

            }
            else if (SourceTree.SelectedNode.Value.StartsWith("U") && TargetTree.SelectedNode.Value.StartsWith("U"))
            {
                txtIsPolicyMove.Text = "false";
                objReturnTable = GetUnitCoverages(SourceTree.SelectedNode, TargetTree.SelectedNode, ref bReturn, ref bRemoveCss);
            }
            else if (SourceTree.SelectedNode.Value.StartsWith("C") && TargetTree.SelectedNode.Value.StartsWith("C"))
            {
                txtIsPolicyMove.Text = "false";

                objReturnTable = GetCoverageList();
                bReturn = true;
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "script", "InValidMapping();", true);
                bReturn = false;
            }
            if ((this.GridDataSource != null) && (this.GridDataSource.Rows.Count > 0))
            {
                objReturnTable.Merge(this.GridDataSource);
            }

            if (bReturn)
            {

                SourceTree.SelectedNode.Selected = false;
                TargetTree.SelectedNode.Selected = false;

                MappingGrid.DataSource = objReturnTable;
                this.GridDataSource = objReturnTable;
                MappingGrid.DataBind();
            }
           
            
        }

        private DataTable GetCoverageList()
        {
            DataTable objTable = new DataTable();
            DataRow objRow = null;

            objTable.Columns.Add("MAPPING_ID");
            objTable.Columns.Add("SOURCE");
            objTable.Columns.Add("TARGET");
            objTable.Columns.Add("SOURCE_NODE_PATH");
            
            objRow = objTable.NewRow();

            objRow[0] = SourceTree.SelectedNode.Value + "|" + TargetTree.SelectedNode.Value;
            objRow[1] = SourceTree.SelectedNode.Parent.Text.Replace("<span class='enable'>", "").Replace("</span>", "") + "-" + SourceTree.SelectedNode.Text.Replace("<span class='enable'>", "").Replace("</span>", "");
            objRow[2] = TargetTree.SelectedNode.Parent.Text.Replace("<span class='enable'>", "").Replace("</span>", "") + "-" + TargetTree.SelectedNode.Text.Replace("<span class='enable'>", "").Replace("</span>", "");
            objRow[3] = SourceTree.SelectedNode.ValuePath;
            objTable.Rows.Add(objRow);

            SourceTree.SelectedNode.SelectAction = TreeNodeSelectAction.None;
            SourceTree.SelectedNode.Text =   SourceTree.SelectedNode.Text.Replace("enable", "disable");
            return objTable;
        }
        private void ClearUnmappedCSS(TreeNode objNode)
        {
            if (objNode.Value.StartsWith("P"))
            {
                foreach (TreeNode objChild1 in objNode.ChildNodes)
                {

                    if (objChild1.ChildNodes.Count > 0)
                    {
                        foreach (TreeNode objChild2 in objChild1.ChildNodes)
                        {

                            objChild2.Text = objChild2.Text.Replace("disable", "enable");
                            objChild2.SelectAction = TreeNodeSelectAction.Select;
                            
                        }
                    }

                    objChild1.Text =  objChild1.Text.Replace("disable","enable");
                    objChild1.SelectAction = TreeNodeSelectAction.Select;

                }
            }
            else if (objNode.Value.StartsWith("U"))
            {
                foreach (TreeNode objChild1 in objNode.ChildNodes)
                {

                    objChild1.Text = objChild1.Text.Replace("disable", "enable");
                    objChild1.SelectAction = TreeNodeSelectAction.Select;

                }


            }

            //else if (objNode.Value.StartsWith("C"))
            //{

            objNode.Text =  objNode.Text.Replace("disable", "enable");
                objNode.SelectAction = TreeNodeSelectAction.Select;
            //}

        }

        private DataTable GetPolicyUnits(TreeNode objSourceNode, TreeNode objTargetNode, ref bool bUnitsMapped)
        {
            DataTable objTable = new DataTable();
            DataRow objRow = null;
            bool allunitsAreSame = false;
            bool bIsUnitMapped = false;
            int iSourceChildCount = 0;
            bool bCovMapped = false;
            bool bRemoveCss = true;

            foreach (TreeNode objSourceChildNode in objSourceNode.ChildNodes)
            {
                if (objSourceChildNode.Text.Contains("<span class='disable'>"))
                {
                    bRemoveCss = false;
                    break;
                }
                if (!bRemoveCss)
                {
                    break;
                }

                foreach (TreeNode objTargetChildNode in objTargetNode.ChildNodes)
                {
                    bRemoveCss = false;
                    if (string.Equals(objSourceChildNode.Text.Replace("<span class='enable'>", "").Replace("</span>", ""), objTargetChildNode.Text.Replace("<span class='enable'>", "").Replace("</span>", ""), StringComparison.InvariantCultureIgnoreCase))
                    {

                        bRemoveCss = true;

                        objTable = GetUnitCoverages(objSourceChildNode, objTargetChildNode, ref bCovMapped, ref bRemoveCss);

                        if (!bRemoveCss)
                        {
                            break;
                        }
                        if (bCovMapped)
                        {
                            bIsUnitMapped = true;
                            iSourceChildCount++;
                        }
                        else
                        {
                            bIsUnitMapped = false;
                        }

                        objSourceChildNode.SelectAction = TreeNodeSelectAction.None;
                        
                        objSourceChildNode.Text = objSourceChildNode.Text.Replace("enable", "disable");

                        break;

                    }
                    else
                    {
                        bIsUnitMapped = false;
                    }


                    //SourceTree.Nodes[1].forecolor = color.red;
                }
            }

            if (objSourceNode.ChildNodes.Count != iSourceChildCount)
            {
                bUnitsMapped = false;
                if (bRemoveCss)
                {
                    ClearUnmappedCSS(SourceTree.SelectedNode);
                }
                ClientScript.RegisterStartupScript(this.GetType(), "script", "MappingFailed();", true);
                
                

            }
            else
            {
                bUnitsMapped = true;

                objTable = new DataTable();
                objTable.Columns.Add("MAPPING_ID");
                objTable.Columns.Add("SOURCE");
                objTable.Columns.Add("TARGET");
                objTable.Columns.Add("SOURCE_NODE_PATH");
               
                objRow = objTable.NewRow();
                objRow[0] = txtMappedIds.Text;
                objRow[1] = objSourceNode.Text.Replace("<span class='enable'>", "").Replace("</span>", "");
                objRow[2] = objTargetNode.Text.Replace("<span class='enable'>", "").Replace("</span>", "");
                objRow[3] = objSourceNode.ValuePath;
                
                objSourceNode.SelectAction = TreeNodeSelectAction.None;
                objSourceNode.Text = objSourceNode.Text.Replace("enable", "disable");
                objTable.Rows.Add(objRow);

            }
            


            return objTable;
        }

        private DataTable GetUnitCoverages(TreeNode objSourceNode, TreeNode objTargetNode, ref bool bCovsMapped,ref bool bRemoveCss )
        {
          bool bCoveragesMapped = false;
          DataTable objTable = new DataTable();
            DataRow objRow = null;
          objTable.Columns.Add("MAPPING_ID");
          objTable.Columns.Add("SOURCE");
          objTable.Columns.Add("TARGET");
          objTable.Columns.Add("SOURCE_NODE_PATH");
            int iSourceChildCount = 0;
            foreach (TreeNode objSourceChildNode in objSourceNode.ChildNodes)
            {

                if(objSourceChildNode.Text.Contains("<span class='disable'>"))
                {
                    bRemoveCss = false;
                    break;
                }

                foreach (TreeNode objTargetChildNode in objTargetNode.ChildNodes)
                {
                    bCoveragesMapped = false;
                    bRemoveCss = false;
                    if (string.Equals(objSourceChildNode.Text.Replace("<span class='enable'>", "").Replace("</span>", ""), objTargetChildNode.Text.Replace("<span class='enable'>", "").Replace("</span>", ""), StringComparison.InvariantCultureIgnoreCase))
                    {
                        
                        //objRow = objTable.NewRow();
                        //objRow[0] = objSourceChildNode.Value + "|" + objTargetChildNode.Value;
                        //objRow[1] = objSourceNode.Text.Replace("<span class='enable'>", "").Replace("</span>", "") + "-" + objSourceChildNode.Text.Replace("<span class='enable'>", "").Replace("</span>", "");
                        //objRow[2] = objTargetNode.Text.Replace("<span class='enable'>", "").Replace("</span>", "") + "-" + objTargetChildNode.Text.Replace("<span class='enable'>", "").Replace("</span>", "");
                        //objRow[3] = objSourceChildNode.ValuePath;
                      //  objTable.Rows.Add(objRow);
                        bCoveragesMapped = true;
                        bRemoveCss = true;
                        iSourceChildCount++;
                        if (string.IsNullOrEmpty(txtMappedIds.Text))
                        {
                            txtMappedIds.Text = objSourceChildNode.Value + "|" + objTargetChildNode.Value;
                        }
                        else
                        {
                            txtMappedIds.Text = txtMappedIds.Text + "," + objSourceChildNode.Value + "|" + objTargetChildNode.Value;
                        }

                      
                        objSourceChildNode.SelectAction = TreeNodeSelectAction.None;
                        objSourceChildNode.Text = objSourceChildNode.Text.Replace("enable", "disable");
                        break;

                    }
                    else
                    {
                        bCoveragesMapped = false;
                    }

                   
                }
            }

            if (objSourceNode.ChildNodes.Count != iSourceChildCount)
            {
                bCovsMapped = false;
                if (bRemoveCss)
                {
                    ClearUnmappedCSS(SourceTree.SelectedNode);
                }
                ClientScript.RegisterStartupScript(this.GetType(), "script", "MappingFailed();", true);
            }
            else
            {
                bCovsMapped = true;
                objTable = new DataTable();
                objTable.Columns.Add("MAPPING_ID");
                objTable.Columns.Add("SOURCE");
                objTable.Columns.Add("TARGET");

                objTable.Columns.Add("SOURCE_NODE_PATH");
                objRow = objTable.NewRow();
                objRow[0] = txtMappedIds.Text;
                objRow[1] = objSourceNode.Text.Replace("<span class='enable'>", "").Replace("</span>", "");
                objRow[2] = objTargetNode.Text.Replace("<span class='enable'>", "").Replace("</span>", "");
                objRow[3] = objSourceNode.ValuePath;
                objTable.Rows.Add(objRow);
                objSourceNode.Text = objSourceNode.Text.Replace("enable", "disable");
                objSourceNode.SelectAction = TreeNodeSelectAction.None;
            }

            return objTable;
        }

        protected void btnBulkMove_Click(object sender, EventArgs e)
        {
            string sResponse = string.Empty;
            XmlDocument objReturnXml = null;
            string sSelectedRowIds = string.Empty;
            try
            {
                if (MappingGrid.Rows.Count > 0)
                {
                    foreach (GridViewRow objRow in MappingGrid.Rows)
                    {
                        if (string.IsNullOrEmpty(sSelectedRowIds))
                        {
                            sSelectedRowIds = MappingGrid.DataKeys[objRow.RowIndex].Value.ToString();
                        }
                        else
                        {
                            sSelectedRowIds = sSelectedRowIds + "," + MappingGrid.DataKeys[objRow.RowIndex].Value.ToString();
                        }

                    }

                    sResponse = AppHelper.CallCWSService(GetMessageTemplateForBulkMove(sSelectedRowIds));
                    objReturnXml = new XmlDocument();
                    objReturnXml.LoadXml(sResponse);
                    string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                    if (sMsgStatus == "Success")
                    {
                        //if(txtIsPolicyMove.Text=="false")
                        //{
                        ClientScript.RegisterStartupScript(this.GetType(), "script", "BulkMoveSuccessful();", true);
                        //}
                        //else
                        //{
                        //  ClientScript.RegisterStartupScript(this.GetType(), "script", "MarkPolicyInvalid();", true);
                        //}
                    }
                    else
                    {
                        ErrorControl1.errorDom = sResponse;
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "script", "AddMapping();", true);
                }
            }
            
            catch (Exception ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void btnEditReserve_Click(object sender, EventArgs e)
        {
              string sResponse= string.Empty;
              XmlDocument objReturnXml = null;
            try
            {
                sResponse = AppHelper.CallCWSService(GetMessageTemplateForEditReserve());
                objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sResponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {

                    ClientScript.RegisterStartupScript(this.GetType(), "script", "EditReserveSuccessful();", true);
                }
                else
                {
                    ErrorControl1.errorDom = sResponse; 
                }
            }
           
            catch (Exception ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void btnAddMapping_Click(object sender, EventArgs e)
        {
           ValidateMapping();
           //foreach (TreeNode item in SourceTree.SelectedNode.ChildNodes)
           //{
           //    item.Text = "<span class='disable'>" + item.Text + "</span>";
           //}
        }
        protected void btnRemoveMapping_Click(object sender, EventArgs e)
        {
            
            string sRemoveMapping = string.Empty;
            bool bShowAlert= true;
          
                foreach (GridViewRow objRow in MappingGrid.Rows)
                {
                    CheckBox chkRow = (objRow.Cells[0].FindControl("chkMapping") as CheckBox);
                    if (chkRow.Checked)
                    {
                        bShowAlert=false;
                        if (GridDataSource.Rows.Count > 1)
                        {
                            this.GridDataSource = (from myRow in this.GridDataSource.AsEnumerable()
                                                   where myRow.Field<string>("MAPPING_ID") != MappingGrid.DataKeys[objRow.RowIndex].Value.ToString()
                                                   select myRow).CopyToDataTable();

                            // ClearUnmappedCSS(SourceTree.FindNode(objRow.Cells[3].Text));
                        }
                        else
                        {
                            GridDataSource.Rows.RemoveAt(0);
                        }

                        ClearUnmappedCSS(SourceTree.FindNode((objRow.Cells[3].FindControl("SOURCE_NODE_PATH") as TextBox).Text));
                    }


                }

                MappingGrid.DataSource = this.GridDataSource;
                MappingGrid.DataBind();

                if (bShowAlert)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "script", "NoMapping();", true);
                }
                
     
         }
        private void InitializeTree(bool bBindSourceTree, bool bBindTargetTree)
        {
            string sPolicyId = string.Empty;
            if (bBindSourceTree)
            {
                sPolicyId = cmbSourcePolicy.SelectedValue;
            }
            else if (bBindTargetTree)
            {
                sPolicyId = cmbTargetPolicy.SelectedValue;
            }

            string sResponse = AppHelper.CallCWSService(GetMessageTemplateForTree(sPolicyId));
            XmlDocument oResponse = new XmlDocument();
            DataSet objDataSet = new DataSet();
            oResponse.LoadXml(sResponse);
            string[] sArray = null;
            DataTable objPolicytable = null;
            DataTable objUnittable = null;
            DataTable objCovtable = null;
            objDataSet.ReadXml(new XmlNodeReader(oResponse));
            if (objDataSet.Tables.Count<3)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "script", "NoCoverages();", true);
                return;
            }

            #region bindSourceTree
            if (bBindSourceTree)
            {
                TreeNode objSourcePolicyNode = new TreeNode(cmbSourcePolicy.SelectedItem.Text, "P" + cmbSourcePolicy.SelectedValue);
                SourceTree.Nodes.Add(objSourcePolicyNode);
                objSourcePolicyNode.Selected = true;
                objSourcePolicyNode.Text = "<span class='enable'>" + objSourcePolicyNode.Text+"</span>";
                sArray = new string[] { "PolicyName", "PolicyId" };
                objDataSet.Tables[3].DefaultView.RowFilter = "PolicyId=" + cmbSourcePolicy.SelectedValue;
                objPolicytable = objDataSet.Tables[3].DefaultView.ToTable(true, sArray);

                foreach (DataRow objPolicyRow in objPolicytable.Rows)
                {


                    sArray = new string[] { "PolUnitRowId", "UNITNAME" };
                    objDataSet.Tables[3].DefaultView.RowFilter = "PolicyId=" + Conversion.ConvertObjToStr(objPolicyRow["PolicyId"]);


                    objUnittable = objDataSet.Tables[3].DefaultView.ToTable(true, sArray);

                    foreach (DataRow objUnitRow in objUnittable.Rows)
                    {

                        TreeNode unitNode = AddChildNode(objSourcePolicyNode, "U" + Conversion.ConvertObjToStr(objUnitRow["PolUnitRowId"]), Conversion.ConvertObjToStr(objUnitRow["UNITNAME"]));

                        sArray = new string[] { "PolCvgRowId", "CovText" };
                        objDataSet.Tables[3].DefaultView.RowFilter = "PolUnitRowId=" + Conversion.ConvertObjToStr(objUnitRow["PolUnitRowId"]);

                        objCovtable = objDataSet.Tables[3].DefaultView.ToTable(true, sArray);



                        foreach (DataRow objCovRow in objCovtable.Rows)
                        {
                            TreeNode covNode = AddChildNode(unitNode, "C" + Conversion.ConvertObjToStr(objCovRow["PolCvgRowId"]), Conversion.ConvertObjToStr(objCovRow["CovText"]));

                        }

                    }


                }

                objSourcePolicyNode.Expand();
            }
            #endregion

            #region bindTargetTree
            if (bBindTargetTree)
            {
              
                TreeNode objTargetPolicyNode = new TreeNode(cmbTargetPolicy.SelectedItem.Text, "P" + cmbTargetPolicy.SelectedValue);
                TargetTree.Nodes.Add(objTargetPolicyNode);
                objTargetPolicyNode.Text = "<span class='enable'>" + objTargetPolicyNode.Text + "</span>";
                objTargetPolicyNode.Selected = true;
                sArray = new string[] { "PolicyName", "PolicyId" };
                objDataSet.Tables[3].DefaultView.RowFilter = "PolicyId=" + cmbTargetPolicy.SelectedValue;
                objPolicytable = objDataSet.Tables[3].DefaultView.ToTable(true, sArray);

                foreach (DataRow objPolicyRow in objPolicytable.Rows)
                {


                    sArray = new string[] { "PolUnitRowId", "UNITNAME" };
                    objDataSet.Tables[3].DefaultView.RowFilter = "PolicyId=" + Conversion.ConvertObjToStr(objPolicyRow["PolicyId"]);


                    objUnittable = objDataSet.Tables[3].DefaultView.ToTable(true, sArray);

                    
                    foreach (DataRow objUnitRow in objUnittable.Rows)
                    {

                        TreeNode unitNode = AddChildNode(objTargetPolicyNode, "U" + Conversion.ConvertObjToStr(objUnitRow["PolUnitRowId"]), Conversion.ConvertObjToStr(objUnitRow["UNITNAME"]));
                       
                        sArray = new string[] { "PolCvgRowId", "CovText" };
                        objDataSet.Tables[3].DefaultView.RowFilter = "PolUnitRowId=" + Conversion.ConvertObjToStr(objUnitRow["PolUnitRowId"]);

                        objCovtable = objDataSet.Tables[3].DefaultView.ToTable(true, sArray);
                        unitNode.SelectAction = TreeNodeSelectAction.None;


                        foreach (DataRow objCovRow in objCovtable.Rows)
                        {

                            TreeNode covNode = AddChildNode(unitNode, "C" + Conversion.ConvertObjToStr(objCovRow["PolCvgRowId"]), Conversion.ConvertObjToStr(objCovRow["CovText"]));
                            covNode.SelectAction = TreeNodeSelectAction.None;

                        }

                    }


                }
                objTargetPolicyNode.Expand();

            }
                #endregion
        }
        private TreeNode AddChildNode(TreeNode oParentNode, string p_Value, string p_sName)
        {
            TreeNode childNode = null;
            try
            {
                childNode = new TreeNode(p_sName, p_Value);
                childNode.Text="<span class='enable'>"+childNode.Text+"</span>";
                childNode.PopulateOnDemand = false;
                childNode.Target = "_self";
                oParentNode.ChildNodes.Add(childNode);

            }
            catch (Exception e)
            {
            }
            return childNode;
        }

        protected void cmbSourcePolicy_onchange(object sender, EventArgs e)
        {
                SourceTree.Nodes.Clear();
            txtMappedIds.Text = string.Empty;
            MappingGrid.DataSource = null;
            this.GridDataSource = null;
            MappingGrid.DataBind();
            if (TargetTree.FindNode("P" + cmbTargetPolicy.SelectedValue) != null)
            {
                TargetTree.FindNode("P" + cmbTargetPolicy.SelectedValue).Selected = true;
            }
            InitializeTree(true,false);
        }
        protected void cmbTargetPolicy_onchange(object sender, EventArgs e)
        {
            
            TargetTree.Visible = true;
                TargetTree.Nodes.Clear();
            txtMappedIds.Text = string.Empty;
            MappingGrid.DataSource = null;
            this.GridDataSource = null;

            SourceTree.FindNode("P" + cmbSourcePolicy.SelectedValue).Selected = true;
            ClearUnmappedCSS(SourceTree.FindNode("P" + cmbSourcePolicy.SelectedValue));
            MappingGrid.DataBind();

            if (cmbTargetPolicy.SelectedValue != "0")
            {

                InitializeTree(false, true);
            }
        }
    }
}