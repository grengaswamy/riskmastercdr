﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Threading;
using System.Globalization;

using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Reserves
{
    public partial class ReserveListing : NonFDMBasePageCWS
    {
        private XmlDocument oResponse = null;
        private string sReturn = String.Empty;
        ArrayList arrIndex = null;
        bool m_bReadOnly = false;
        //Asharma326 MITS 32848 Starts
        int grdrowcount = 0;
        //Asharma326 MITS 32848 Ends

        protected void Page_Load(object sender, EventArgs e)
        {
            //Deb: Update the code for MS Security HP not to submit the page directly
            string sSubmitedFrmRsv = AppHelper.GetQueryStringValue("Submit");
            //pgupta93: RA-4608 START
            if (!(string.IsNullOrEmpty(AppHelper.GetQueryStringValue("FormName"))))
            {
                btn_FDH.Visible = false;
                hdFDHButton.Value = AppHelper.GetQueryStringValue("FormName");
            }
            //pgupta93: RA-4608 end

            if (!string.IsNullOrEmpty(sSubmitedFrmRsv))
            {
                if (sSubmitedFrmRsv == "true")
                {
                    //RMA-4608: pgupta93 START
                    //Server.Transfer(ModifyReserve.aspx);
                    string sRedirectString = "ModifyReserve.aspx?FormName=" + hdFDHButton.Value;//RMA-4608: pgupta93 
                    Server.Transfer(sRedirectString);
                    //RMA-4608: pgupta93 END
                }

            }
            //Deb: Update the code for MS Security HP not to submit the page directly 
            oResponse = new XmlDocument();
            XElement xeHideEnhancedNotes = null;
            //Start asharma326
            if (AppHelper.ReadCookieValue("EnableVSS").Equals("-1"))
            {
                VssExport.Visible = true;
            }
            //End asharma326
            // Added for JIRA 4608
            string sValidationResources = RMXResourceManager.JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceManager.RMXResourceProvider.PageId("ReserveListing.aspx"), "ReserveListingValidations", ((int)RMXResourceManager.RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "ReserveListingValidations", sValidationResources, true);
            // Added  for JIRA 4608

            if (!Page.IsPostBack)
            {
                try
                {
                    claimid.Text = AppHelper.GetQueryStringValue("ClaimId");
                    claimantrowid.Text = AppHelper.GetQueryStringValue("ClaimantId");
                    claimanteid.Text = AppHelper.GetQueryStringValue("ClaimantEId");
                    unitid.Text = AppHelper.GetQueryStringValue("UnitId"); 
                    unitrowid.Text = AppHelper.GetQueryStringValue("UnitRowId");
                    NonFDMCWSPageLoad("ReserveFundsAdaptor.GetClaimReserves");
                    oResponse = Data;
                    sReturn = oResponse.OuterXml;

                    XElement objOuterXml = XElement.Parse(sReturn);

                    if (objOuterXml != null)
                    {
                        XElement objClaimReserves = null;
                        objClaimReserves = objOuterXml.XPathSelectElement("./ReserveFunds/claimreserves");

                        if (objClaimReserves != null)
                        {
                            claimid.Text = objClaimReserves.Attribute("claimid").Value;
                            claimanteid.Text = objClaimReserves.Attribute("claimant").Value;
                            //if (unitrowid.Text == "")
                            //{
                            //    unitrowid.Text = objClaimReserves.Attribute("unit").Value;
                            //}
                            if (unitid.Text == "")
                            {
                                unitid.Text = objClaimReserves.Attribute("unit").Value;
                            }
                            claimnumber.Text = objClaimReserves.Attribute("claimnumber").Value;
                            frozenflag.Text = objClaimReserves.Attribute("paymentfrozenflag").Value;
                        }

                        XElement objReserves = null;
                        objReserves = objOuterXml.XPathSelectElement("./ReserveFunds/claimreserves/reserves");

                        if (objReserves != null)
                        {
                            if (claimanteid.Text != "")
                            {
                                claimanteid.Text = objReserves.Attribute("claimant").Value;
                            }
                        }
                        //Mgaba2: MITS 29116
                         xeHideEnhancedNotes =  objOuterXml.XPathSelectElement("//HideEnhancedNotes");
                        if(xeHideEnhancedNotes != null && xeHideEnhancedNotes.Value == "True")
                        {
                            div_enhancednotes.Visible = false;
                        }
                    }
                   
                    BindDataToXml(sReturn);
                    CustomizePage();
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }
        //Deb
        protected void drdCurrencytype_onchange(object sender, EventArgs e)
        {
            try
            {
                NonFDMCWSPageLoad("ReserveFundsAdaptor.GetClaimReserves");
                oResponse = Data;
                sReturn = oResponse.OuterXml;

                XElement objOuterXml = XElement.Parse(sReturn);

                if (objOuterXml != null)
                {
                    XElement objClaimReserves = null;
                    objClaimReserves = objOuterXml.XPathSelectElement("./ReserveFunds/claimreserves");

                    if (objClaimReserves != null)
                    {
                        claimid.Text = objClaimReserves.Attribute("claimid").Value;
                        claimanteid.Text = objClaimReserves.Attribute("claimant").Value;
                        if (unitid.Text == "")
                        {
                            unitid.Text = objClaimReserves.Attribute("unit").Value;
                        }
                        claimnumber.Text = objClaimReserves.Attribute("claimnumber").Value;
                        frozenflag.Text = objClaimReserves.Attribute("paymentfrozenflag").Value;
                    }

                    XElement objReserves = null;
                    objReserves = objOuterXml.XPathSelectElement("./ReserveFunds/claimreserves/reserves");

                    if (objReserves != null)
                    {
                        if (claimanteid.Text != "")
                        {
                            claimanteid.Text = objReserves.Attribute("claimant").Value;
                        }
                    }
                }
                BindDataToXml(sReturn);
                CustomizePage();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //Deb
        protected void grdReserve_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                    if (!AppHelper.ReadCookieValue("EnableVSS").Equals("-1"))
                    {
                        e.Row.Cells[0].Visible = false;
                    }
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                    e.Row.CssClass = "colheader3";
                    if (!AppHelper.ReadCookieValue("EnableVSS").Equals("-1"))
                    {
                        e.Row.Cells[0].Visible = false;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void grdReserve_RowDataBound(object Sender, GridViewRowEventArgs e)
        {
            try
            {
                int iTempIndex = 0;
                int iPos = 0;
                string sTemp = string.Empty;
                // mkaran2 :  MITS 32837  : start               
                string reserveID = string.Empty;
                string paidAmount = string.Empty;
                string collectionAmount = string.Empty;
                // mkaran2 :  MITS 32837  : end

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView objDataRow = (DataRowView)e.Row.DataItem;

                    e.Row.ID = objDataRow["pid"].ToString(); 
                    if (!String.IsNullOrEmpty(e.Row.ID) && !m_bReadOnly)
                    {
                        //string sQueryString = "ClaimId=" + claimid.Text + "&ClaimNumber=" + Server.UrlEncode(claimnumber.Text) + "&ClaimantId=" + claimanteid.Text + "&UnitId=" + unitid.Text + "&Reserve=" + objDataRow["pid"].ToString() + "&Lob=" + lob.Text + "&ClaimantRowId=" + claimantrowid.Text + "&UnitRowId=" + unitrowid.Text;// +"&caption=" + Server.UrlEncode(caption.Text);
                        //sQueryString = Server.UrlEncode(sQueryString);
                        //e.Row.Cells[0].Text = "<a href='ModifyReserve.aspx?" + sQueryString + "'>" + e.Row.Cells[0].Text + "</a>";
                       // string sReserves = objDataRow["pid"].ToString();
                       //e.Row.Cells[0].Text = "<a href='#' onclick='return TransferToModifyReserve(" + sReserves + ")" + "'>" + e.Row.Cells[0].Text + "</a>";// + sQueryString + "'>" + e.Row.Cells[0].Text + "</a>";                       
                        // mkaran2 :  MITS 32837  : start                                                 
                         reserveID = objDataRow["pid"].ToString();
                         //Ashish Ahuja mits 34452 - start
                         //paidAmount = objDataRow["Paid"].ToString();
                         paidAmount = objDataRow[GetResourceValue("gvHdrPaid", "0")].ToString();

                         //collectionAmount = objDataRow["Collections"].ToString();
                         collectionAmount = objDataRow[GetResourceValue("gvHdrCollections", "0")].ToString();
                         //Ashish Ahuja mits 34452 - end
                         e.Row.Cells[1].Text = string.Concat("<a href='#' onclick='return TransferToModifyReserve(",reserveID,",\"",paidAmount,"\",\"",collectionAmount,"\")'>",e.Row.Cells[1].Text,"</a>");                       
                       // mkaran2 :  MITS 32837  : end
                    }
                    e.Row.Cells[1].Style.Add("text-align", "left");
                    e.Row.Cells[2].Style.Add("text-align", "right");
                    e.Row.Cells[3].Style.Add("text-align", "right");
                    e.Row.Cells[4].Style.Add("text-align", "right");
                    e.Row.Cells[5].Style.Add("text-align", "right");
                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                    iPos = 0;
                    sTemp = e.Row.Cells[1].Text;
                    iPos = sTemp.IndexOf("Totals", 0);
                    if (iPos > 0)
                    {
                    
                        e.Row.Cells[1].Font.Bold = true;
                        e.Row.Cells[2].Font.Bold = true;
                        e.Row.Cells[3].Font.Bold = true;
                        e.Row.Cells[4].Font.Bold = true;
                        e.Row.Cells[5].Font.Bold = true;
                        e.Row.Cells[6].Font.Bold = true;
                    }
                    //Asharma326 MITS 32848 Starts
                    if (e.Row.Cells[0].Text == "radio" && (!String.IsNullOrEmpty(e.Row.ID)) && (!m_bReadOnly))
                    {
                        grdrowcount++;
                        string control = @"<input type='radio' id='id_" + grdrowcount + "' onclick = 'RadioCheck(this," + reserveID + ");' name='name_" + grdrowcount + "' value='value_" + grdrowcount + "'/>";
                        e.Row.Cells[0].Text = control.Replace(@"\", " ");
                    }
                    else 
                    {
                        if (e.Row.Cells[0].Text == "radio")
                        {
                            grdrowcount++;
                            string control = @"<input type='radio' id='id_" + grdrowcount + "' onclick = 'RadioCheck(this," + reserveID + ");' name='name_" + grdrowcount + "' value='value_" + grdrowcount + "'/>";
                            e.Row.Cells[0].Text = control.Replace(@"\", " ");
                            e.Row.Cells[0].Enabled = false;
                            VssExport.Enabled = false;
                        }
                    }
                    
                    //Asharma326 MITS 32848 Ends
                }
                
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    arrIndex = new ArrayList();

                    foreach (TableCell cell in e.Row.Cells)
                    {
                        if ((cell.Text == "pid")) 
                        {
                            cell.Visible = false;
                            arrIndex.Add(iTempIndex);
                        }
                        iTempIndex++;
                    }
                    e.Row.Cells[1].Style.Add("text-align", "left");
                    e.Row.Cells[2].Style.Add("text-align", "right");
                    e.Row.Cells[3].Style.Add("text-align", "right");
                    e.Row.Cells[4].Style.Add("text-align", "right");
                    e.Row.Cells[5].Style.Add("text-align", "right");
                    e.Row.Cells[6].Style.Add("text-align", "center");
                    e.Row.CssClass = "colheader3";
                   
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void BindDataToXml(string sReturnValue)
        {
            string sTemp1 = string.Empty;

            DataRow drReserveRow = null;
            DataTable dtReserve = new DataTable();

            XElement objReserveDoc = XElement.Parse(sReturnValue);

            //dtReserve.Columns.Add("Reserve");
            //dtReserve.Columns.Add("Balance");
            //dtReserve.Columns.Add("Paid");
            //dtReserve.Columns.Add("Collections");
            //dtReserve.Columns.Add("Incurred");
            //dtReserve.Columns.Add("Status");
            //dtReserve.Columns.Add("pid");

            //Asharma326 MITS 32848 Starts
            dtReserve.Columns.Add(" "); 
            //Asharma326 MITS 32848 Ends
            dtReserve.Columns.Add(GetResourceValue("gvHdrReserve", "0"));
            dtReserve.Columns.Add(GetResourceValue("gvHdrBalance", "0"));
            dtReserve.Columns.Add(GetResourceValue("gvHdrPaid", "0"));
            dtReserve.Columns.Add(GetResourceValue("gvHdrCollections", "0"));
            dtReserve.Columns.Add(GetResourceValue("gvHdrIncurred", "0"));
            dtReserve.Columns.Add(GetResourceValue("gvHdrStatus", "0"));
            //dtReserve.Columns.Add(GetResourceValue("gvHdrpid", "0")); MITS 31013 
            dtReserve.Columns.Add("pid");
            var objRowXml = from rows in objReserveDoc.Descendants("reserve")
                            select rows;

            if (objRowXml.Count() == 0)
            {
                objRowXml = from rows in objReserveDoc.Descendants("reservereadonly")
                                select rows;

                if (objRowXml.Count() != 0)
                {
                    m_bReadOnly = true;
                }
                hdReadOnly.Value = m_bReadOnly.ToString();
            }
            string[] arrPid = new string[1];
            string sDescendant = string.Empty;
            int iRowCount = 0;

            foreach (XElement row in objRowXml)
            {
                iRowCount = iRowCount + 1;
				arrPid[0] = "pid";
                grdReserve.DataKeyNames = arrPid;
                
                drReserveRow = dtReserve.NewRow();
                DataRow drEmptyRow = dtReserve.NewRow();
                //Asharma326 MITS 32848 Starts
                drReserveRow[0] = "radio"; 
                //Asharma326 MITS 32848 Ends
                drReserveRow[1] = row.Attribute("reservename").Value;
                drReserveRow[2] = row.Attribute("balance").Value;
                drReserveRow[3] = row.Attribute("paid").Value;
                drReserveRow[4] = row.Attribute("collected").Value;
                drReserveRow[5] = row.Attribute("incurred").Value;
                //rupal
                //sTemp1 = row.Attribute("reservestatus").Value;
                sTemp1 = row.Attribute("reservestatusdesc").Value;
                if (sTemp1.Length > 1)
                {
                    sTemp1 = sTemp1.Replace("-", "");
                }
                drReserveRow[6] = sTemp1;
                drReserveRow[7] = row.Attribute("reservetypecode").Value;
                dtReserve.Rows.Add(drEmptyRow);
                dtReserve.Rows.Add(drReserveRow);
            }

            if (iRowCount == 0)
            {
                DataRow drEmptyRow = dtReserve.NewRow();
                dtReserve.Rows.Add(drEmptyRow);
            }

            //Add Totals to Grid
            var objTotalXml = from rows in objReserveDoc.Descendants("totals")
                              select rows;

            foreach (XElement row in objTotalXml)
            {
			 	arrPid[0] = "pid"; 
                grdReserve.DataKeyNames = arrPid;
                DataRow drEmptyRow = dtReserve.NewRow();
                drReserveRow = dtReserve.NewRow();
                //Asharma326 MITS 32848 Starts
                drReserveRow[0] = String.Empty; 
                //Asharma326 MITS 32848 Ends
                drReserveRow[1] = "          Totals -->";
                drReserveRow[2] = row.Attribute("balanceamount").Value;
                drReserveRow[3] = row.Attribute("paidtotal").Value;
                drReserveRow[4] = row.Attribute("collectedtotal").Value;
                drReserveRow[5] = row.Attribute("incurredamount").Value;
                drReserveRow[6] = String.Empty;
                drReserveRow[7] = String.Empty;
                dtReserve.Rows.Add(drEmptyRow);
                dtReserve.Rows.Add(drReserveRow);
            }

            // Bind DataTable to GridView.
            grdReserve.Visible = true;
            grdReserve.DataSource = dtReserve;
            grdReserve.DataBind();
        }

        private void CustomizePage()
        {
            if (!String.IsNullOrEmpty(claimnumber.Text))
            {
                //Praveen ML Changes ----Start

                //errorhook.Text = String.Format("Reserves ({0})", caption.Text);
                //lblcaption.Text = String.Format("Reserves ({0})", caption.Text);
                errorhook.Text = lblReserve.Text + " (" +  caption.Text + ")";
                lblcaption.Text =   lblReserve.Text + " (" + caption.Text + ")";

                //Praveen ML Changes ----End

            }//if

            //Change by kuladeep for mits:25401 08/07/2011 Start
            //btnResWS.Visible = (ShowRSW.Text != "0") ? true : false;
            btnResWS.Visible = (string.Compare(Convert.ToString(ShowRSW.Text), "1") == 0 || string.Compare(Convert.ToString(ShowRSW.Text), "-1") == 0) ? true : false;
            //Change by kuladeep for mits:25401 08/07/2011 End
            btnTE.Visible = (istande.Text != "0") ? true : false;
            //Neha Mits 2306 Added back button.
            if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("wnd")) || !String.IsNullOrEmpty(AppHelper.GetQueryStringValue("FromFunds")))
            {
                // aahuja21 : MITS : 31314 
              //  btnBack.Visible = true;
                hdFromFunds.Value = "1";
            }
            if (add_payment.Text == "1")
            {
                btnAddPayment.Visible = true;
                //rupal:start,multicurrency
                string sOnClientClick = "bReturn = RMX.Reserves.CheckForFrozenFlag();if(bReturn == true)  if (ValidateClaimCurrency()) return RMX.Reserves.NavigateToFunds('false'); else return false; else return false;";

                if (m_bReadOnly)
                {
                    sOnClientClick = "bReturn = RMX.Reserves.CheckForFrozenFlag();if(bReturn == true) bReturn = RMX.Reserves.DetailTrack(); else return false; if(bReturn == true) if (ValidateClaimCurrency()) return RMX.Reserves.NavigateToFunds('false'); else return false; else return false;";
                }
                //rupal:end
                btnAddPayment.OnClientClick = sOnClientClick;
            }
            else
            {
                btnAddPayment.Visible = false;
            }

            if (add_collection.Text == "1")
            {
                btnAddCollection.Visible = true;
                ////rupal:start,multicurrency
                //string sOnClientClick = "bReturn = RMX.Reserves.CheckForFrozenFlag();if(bReturn == true) return RMX.Reserves.NavigateToFunds('true'); else return false;";
                string sOnClientClick = "bReturn = RMX.Reserves.CheckForFrozenFlag();if(bReturn == true) if (ValidateClaimCurrency()) return RMX.Reserves.NavigateToFunds('true'); else return false; else return false;";

                if (m_bReadOnly)
                {
                    //sOnClientClick = "bReturn = RMX.Reserves.CheckForFrozenFlag();if(bReturn == true) bReturn = RMX.Reserves.DetailTrack(); else return false; if(bReturn == true) return RMX.Reserves.NavigateToFunds('true'); else return false;";
                    sOnClientClick = "bReturn = RMX.Reserves.CheckForFrozenFlag();if(bReturn == true) bReturn = RMX.Reserves.DetailTrack(); else return false; if(bReturn == true) if (ValidateClaimCurrency()) return RMX.Reserves.NavigateToFunds('true'); else return false; else return false;";
                }
                //rupal:end
                btnAddCollection.OnClientClick = sOnClientClick;
            }
            else
            {
                btnAddCollection.Visible = false;
            }

            if (UseQueuedPayments.Text == "True")
            {
                lblQP.Text = lblQuedPaymentTotal.Text + "   " + QueuedTotal.Text;
            }

            //merge 13084
            if (ScheduleCheckPermission.Text == "0")
            {
                btnAutoCheck.Visible = false;
            }
            else
            {
                btnAutoCheck.Visible = true;
                ////rupal:start,multicurrency
                //string sOnClientClick = "bReturn = RMX.Reserves.CheckForFrozenFlag();if(bReturn == true) return RMX.Reserves.ScheduleChecks(); else return false;";
                string sOnClientClick = "bReturn = RMX.Reserves.CheckForFrozenFlag();if(bReturn == true) if (ValidateClaimCurrency()) return RMX.Reserves.ScheduleChecks(); else return false; else return false;";

                if (m_bReadOnly)
                {
                    //sOnClientClick = "bReturn = RMX.Reserves.CheckForFrozenFlag();if(bReturn == true) bReturn = RMX.Reserves.DetailTrack(); else return false; if(bReturn == true) return RMX.Reserves.ScheduleChecks(); else return false;";
                    sOnClientClick = "bReturn = RMX.Reserves.CheckForFrozenFlag();if(bReturn == true) bReturn = RMX.Reserves.DetailTrack(); else return false; if(bReturn == true) if (ValidateClaimCurrency()) return RMX.Reserves.ScheduleChecks(); else return false; else return false;";
                }
                //rupal:end
                btnAutoCheck.OnClientClick = sOnClientClick;
            }
            //Deb Multi Currency
            if (drdCurrencytype.Items.Count == 1)
            {
                lblCurrencytype.Visible = false;
                drdCurrencytype.Visible = false;
            }
            //Deb Multi Currency
        }

        protected void btnPaymentHistory_onclick(object sender, EventArgs e)
        {
            //string sRedirectString = String.Format("../Funds/PaymentHistory.aspx?ClaimId={0}&ClaimantEid={1}&ClaimantRowId={2}&UnitId={3}&UnitRowId={4}&SubTitle={5}&FrozenFlag={6}&ClaimNumber={7}&Caption={8}", claimid.Text, claimanteid.Text, claimantrowid.Text, unitid.Text, unitrowid.Text, subtitle.Text, frozenflag.Text, claimnumber.Text, caption.Text);
            //Neha Mits 23601
            //pgupta93: RMA-4608 START
            string sRedirectString = string.Empty;
            //string sRedirectString = String.Format("../Funds/PaymentHistory.aspx?ClaimId={0}&ClaimantEid={1}&ClaimantRowId={2}&UnitId={3}&UnitRowId={4}&SubTitle={5}&FrozenFlag={6}&ClaimNumber={7}&Caption={8}&FromFunds={9}", claimid.Text, claimanteid.Text, claimantrowid.Text, unitid.Text, unitrowid.Text, subtitle.Text, frozenflag.Text, claimnumber.Text, caption.Text, hdFromFunds.Value);
            sRedirectString = String.Format("../Funds/PaymentHistory.aspx?ClaimId={0}&ClaimantEid={1}&ClaimantRowId={2}&UnitId={3}&UnitRowId={4}&SubTitle={5}&FrozenFlag={6}&ClaimNumber={7}&Caption={8}&FromFunds={9}&FormName={10}", claimid.Text, claimanteid.Text, claimantrowid.Text, unitid.Text, unitrowid.Text, subtitle.Text, frozenflag.Text, claimnumber.Text, caption.Text, hdFromFunds.Value, hdFDHButton.Value);
            //pgupta93: RMA-4608 END
            Response.Redirect(sRedirectString);//rma 11145
        }

        protected void btnTE_onclick(object sender, EventArgs e)
        {
            //MITS 16608 - added the ClaimaintEid and UnitId in case Reserve Tracking "Detail Level" is turned on via LOB setup...
            string sRedirectString = String.Format("../TAndE/TimeAndExpense.aspx?ClaimId={0}&ClaimantEid={1}&UnitId={2}", claimid.Text, claimanteid.Text, unitid.Text);
            Server.Transfer(sRedirectString);
        }
        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("ReserveListing.aspx"), strResourceType).ToString();
        }
        //Asharma326 MITS 32848 Starts
        private string GetVSSExportTemplate()
        {
            System.Text.StringBuilder sXml = new System.Text.StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>VSSInterface.VSSExportForCCOff</Function></Call><Document>");

            sXml = sXml.Append("<BOB>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(claimid.Text);
            sXml = sXml.Append("</ClaimID>");

            sXml = sXml.Append("<ClaimantEID>");
            sXml = sXml.Append(claimanteid.Text);
            sXml = sXml.Append("</ClaimantEID>");

            sXml = sXml.Append("<Reserve>");
            sXml = sXml.Append(hdnselectedreserve.Value);
            sXml = sXml.Append("</Reserve>");

            sXml = sXml.Append("</BOB></Document></Message>");

            return sXml.ToString();
        }
        
        protected void VssExport_Click(object sender, ImageClickEventArgs e)
        {
            string sCWSRequest = String.Empty;
            string sCWSOutput = string.Empty;
            sCWSRequest = GetVSSExportTemplate();
            sCWSOutput = AppHelper.CallCWSService(sCWSRequest);
            hdnselectedreserve.Value = "0";
        }
        //Asharma326 MITS 32848 Ends
    }
}
