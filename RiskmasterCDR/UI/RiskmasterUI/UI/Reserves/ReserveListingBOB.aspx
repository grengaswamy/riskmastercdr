﻿<!--**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 03/14/2014 | 34082   | pgupta93   | Multicurrency changes
* 15/03/2014 | 34275  | abhadouria | RMA Swiss Re Financial Summary
* 09/20/2014 | rma-857 | ngupta36  | Incurred Handling - Collection Allowed or Not Allowed
**********************************************************************************************-->
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReserveListingBOB.aspx.cs" Inherits="Riskmaster.UI.UI.Reserves.ReserveListingBOB" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register Src="GridRadioButton.ascx" TagName="GridRbtn" TagPrefix="uc4" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl" TagPrefix="mc" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="usc" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .hiddencol
        {
            display:none;
        }
        .viscol
        {
            display:block;
        } 
        .modalBackground {
	    background-color:Gray;
	    filter:alpha(opacity=70);
	    opacity:0.7;
        }

        .modalPopup {
	    background-color:#ffffdd;
	    border-width:3px;
	    border-style:solid;
	    border-color:Gray;
	    padding:3px;
	    width:350px;
        }
        
    </style>    
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }
    </script>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-reserves.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-custom.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/org.js"></script> <!--Added for Mits 18939--> 
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js"> { var i; } </script> <!-- Mits 37856 -->
    <!-- bsharma33 RMA-3887 Reserve Supplemental starts -->
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.7.1.min.js" ></script>
    <script type="text/javascript" src="../../Scripts/jquery/jquery-ui-1.8.17.custom.min.js"></script>
    <link href="../../Scripts/jquery/themes/cupertino/jquery-ui-1.8.17.custom.css" rel="Stylesheet" type="text/css" />
    <!-- bsharma33 RMA-3887 Reserve Supplemental ends -->
    <script type="text/javascript">
        
        var iPolicyID = 0;
        var iCoverageID = 0;
        var iResevreID = 0;
        var iAmount = 0;
        var bShowModal = true;
        var CovgSeqNum = 0; //rupal
        var sDisabilityCat;
        // akaushik5 Added for MITS 33495 Starts
        var iLoadReserveAmount;
        var iLoadReserveBalance;
        var iPolicyUnitRowId = 0; //MITS 34275- RMA Swiss Re Financial Summary 
        // akaushik5 Added for MITS 33495 Ends
        var TransSeqNum = 0;
        var CoverageKey = "";       //Ankit Start : Worked on MITS - 34297
        var dropDownChangeflag = true;
        //igupta3 Mits# 33301
        function stylinggrid() {
            if (!window.ie) {
                var myTab = $get('TabContainer1').control;
                var iIndex = (myTab.get_activeTabIndex() + 1);
                var count = document.getElementById('hdncount');
                if (count.value > 0) {
                    for (var i = 0; i < count.value; i++) {
                        var changecss = document.getElementById('TabContainer1_TabPanel' + iIndex + '_Grid' + iIndex + '_GridView1_Unit_' + i);
                        if (changecss != null) {
                            changecss.style.width = "99%";
                        }
                    }
                }
            }
        }

        function SetDefaultValues() {
            var lblError = document.getElementById('lblError1');
            if (lblError != null)
                lblError.innerHTML = "";
            if (document.getElementById('cmbPolicy') != null
                && document.getElementById('cmbPolicy').length > 1
                && (!document.getElementById('cmbPolicy').options[1].selected)
                && dropDownChangeflag) {
                document.getElementById('cmbPolicy').options[1].selected = true;
                $find('CascadingDropDown1').set_SelectedValue(document.getElementById('cmbPolicy').options[1].value);
                $find('CascadingDropDown2')._onParentChange(null, true);
            }
            if (dropDownChangeflag) {
                if (document.getElementById('cmbUnit') != null
                    && document.getElementById('cmbUnit').length > 1) {
                    document.getElementById('cmbUnit').options[1].selected = true;
                    //msampathkuma RMA-12831 -start
                    for (i = 0; i < document.getElementById('cmbUnit').length; i++)
                        document.getElementById('cmbUnit').options[i].text = DecodeFckEditor(document.getElementById('cmbUnit').options[i].text);
                    //msampathkuma RMA-12831 -end
                    $find('CascadingDropDown2').set_SelectedValue(document.getElementById('cmbUnit').options[1].value);
                    $find('CascadingDropDown3')._onParentChange(null, true);
                }
            }
            if (dropDownChangeflag) {
                if (document.getElementById('cmbCoverageType') != null
                    && document.getElementById('cmbCoverageType').length ==2) {
                    document.getElementById('cmbCoverageType').options[1].selected = true;
                    $find('CascadingDropDown3').set_SelectedValue(document.getElementById('cmbCoverageType').options[1].value);
                    var ctrlLOB = document.getElementById("lob");
                    $find('CascadingDropDown5')._onParentChange(null, true);
                    if (ctrlLOB != null&& ctrlLOB.value == '243') {
                            $find('CascadingDropDown6')._onParentChange(null, true);
                    } else {
                        $find('CascadingDropDown4')._onParentChange(null, true);
                    }
                }
            }
            //MITS:34082 START
            if (document.forms[0].Reservecurrencytypetext_codelookup_cid != null) {
                if (document.forms[0].Reservecurrencytypetext_codelookup_cid.value == "0") {
                    document.forms[0].Reservecurrencytypetext_codelookup_cid.value = document.getElementById('txtReserveCurrID').value;
                }
            }
            //MITS:34082 END
        }
        function TransferToModifyReserve() {
            // document.forms[0].Reserve.value = Reserves;
            var myTab = $get('TabContainer1').control;
            var iIndex = (myTab.get_activeTabIndex() + 1);

            document.getElementById('lblError1').innerHTML = "";

            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;

            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                }
            }

            if (iPos != -1) {

                //document.forms[0].action = "ModifyReserve.aspx";
               // document.forms[0].method = "post";
                document.forms[0].action = "ReserveListing.aspx?Submit=true";
                document.forms[0].submit();
                return false;
            }
            else {
                //alert("Please select a row in the grid.");
                alert(ReserveListingBOBValidations.SelectRowID);
                //$find('EditReserveModalPopup1').hide();
                //document.getElementById('CancelButton').click();
                return false;
            }
        }

        function TransfertoReserveHistoryDetail() {

            var myTab = $get('TabContainer1').control;
            var iIndex = (myTab.get_activeTabIndex() + 1);

            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;

            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                }
            }

            if (iPos == -1) {

                //alert("Please select a row in the grid.");
                alert(ReserveListingBOBValidations.SelectRowID);
                //$find('EditReserveModalPopup1').hide();
                //document.getElementById('CancelButton').click();

                return false;
            }

        }

        function TransfertoPayment() {

            var myTab = $get('TabContainer1').control;
            var iIndex = (myTab.get_activeTabIndex() + 1);

            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;

            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                }
            }
            //Rupal:start, for first & final payment
            if (iPos == -1) {
                
                //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
                if (document.getElementById('hdnEnableFirstAndFinalPayment').value == "true")
                {
                    if (confirm(ReserveListingBOBValidations.IsFirstnFinal) == false) {

                        document.getElementById('IsFirstFinal').value = "False"; //false                    
                        return false;
                    }
                    else {
                        document.getElementById('IsFirstFinal').value = "True"; //true                    
                    }
                }
                //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
                else
                {
                    alert(ReserveListingBOBValidations.DisableFirstAndFinalPaymentCheck);
                    document.getElementById('IsFirstFinal').value = "False";
                    return false;
                }
                //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
            }
            else {
                document.getElementById('IsFirstFinal').value = "False"; //false              
            }
            //First & final Payment checkbox in split screen will always be readonly when we navigate from this page
            document.getElementById("IsFirstFinalReadOnly").value = "True"; //true
            //Rupal:end, for first & final payment

            //bpaskova 13 Jan 2015: added second condition for fixing JIRA RMA-6270
            if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlag(iIndex, p_sGridName, iPos) && RMX.Reserves.CheckForDeductible(iIndex, p_sGridName, iPos))
                return RMX.Reserves.NavigateToFundsBOB("false");
            else
                return false;

            //rupal:end first & final payment
        }

        function TransfertoCollection() {

            var myTab = $get('TabContainer1').control;
            var iIndex = (myTab.get_activeTabIndex() + 1);

            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;

            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                }
            }

            if (iPos == -1) {
                //;start -  nbhatia6 .JIRA# RMA-11396 - Generate First and Final Recovery
                ////alert("Please select a row in the grid.");
                //alert(ReserveListingBOBValidations.SelectRowID);
                ////$find('EditReserveModalPopup1').hide();
                ////document.getElementById('CancelButton').click();

                //return false;

              
                if (document.getElementById('hdnEnableFirstAndFinalPayment').value == "true") {
                    //  if (confirm(ReserveListingBOBValidations.IsFirstnFinal) == false) {
                    if (confirm(ReserveListingBOBValidations.IsFirstnFinal) == false) {

                        document.getElementById('IsFirstFinal').value = "False"; //false                    
                        return false;
                    }
                    else {
                        //bpaskova 13 Jan 2015: added second condition for fixing JIRA RMA-6270
                        if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlag(iIndex, p_sGridName, iPos)) {
                            document.getElementById('IsFirstFinal').value = "True"; //true   
                            document.getElementById("IsFirstFinalReadOnly").value = "True"; //true
                            return RMX.Reserves.NavigateToFundsBOB("true");
                        }
                        else {
                            document.getElementById('IsFirstFinal').value = "False";
                            return false;
                        }

                                     
                    }
                }
                  
                else {
                    alert(ReserveListingBOBValidations.DisableFirstAndFinalPaymentCheck);
                    document.getElementById('IsFirstFinal').value = "False";
                    return false;
                }
              
            }
            else {
                document.getElementById('IsFirstFinal').value = "False"; //false     
                //bpaskova 23 Feb 2015: added second condition for fixing JIRA RMA-6270
                if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlag(iIndex, p_sGridName, iPos) && RMX.Reserves.CheckForDeductible(iIndex, p_sGridName, iPos))
                //JIRA-857 Starts
                {
                    if ((document.getElementById('PreventCollOnRes').value == -1) || ((document.getElementById('collonlob').value == -1) && (document.getElementById('MasResType').value != "R"))) {
                        alert(ReserveListingBOBValidations.CollectionNotAllowed + " " + document.getElementById('ResType').value);
                        return false;
                    }
                    else
                        //JIRA-857 Ends
                        return RMX.Reserves.NavigateToFundsBOB("true");
                }
                else         
                    return false;
            }
            //End -  nbhatia6 .JIRA# RMA-11396 - Generate First and Final Recovery
          //  else {
             
           // }
        }
        function TransfertoManualDeductible() {
           // debugger;
            var myTab = $get('TabContainer1').control;
            var iIndex = (myTab.get_activeTabIndex() + 1);

            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;

            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                }
            }

            if (iPos == -1) {

                //alert("Please select a row in the grid.");
                alert(ReserveListingBOBValidations.SelectRowID);
                //$find('EditReserveModalPopup1').hide();
                //document.getElementById('CancelButton').click();

                return false;
            }
            else {
                //bpaskova 23 Feb 2015: added second condition for fixing JIRA RMA-6270
                if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlag(iIndex, p_sGridName, iPos) && RMX.Reserves.CheckForDeductible(iIndex, p_sGridName, iPos))
                    //JIRA-857 Starts
                {
                    if ((document.getElementById('PreventCollOnRes').value == -1) || ((document.getElementById('collonlob').value == -1) && (document.getElementById('MasResType').value != "R"))) {
                        alert(ReserveListingBOBValidations.CollectionNotAllowed + " " + document.getElementById('ResType').value);
                        return false;

                    }
                    else {
                        var StatusObj = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_DedTypeCode_" + (iPos));
                        var ctrltxtNotDetVal = document.getElementById("txtFPDedVal");
                        if (StatusObj != null && StatusObj.innerHTML == ctrltxtNotDetVal.value) {
                             //alert("Manual deductible can not be created for first party.");
                            alert(ReserveListingBOBValidations.ManualDedForFirstParty);
                            return false;
                        }
                        var ctrltxtNoneDedVal = document.getElementById("txtNoneDedVal");
                        if (StatusObj != null && StatusObj.innerHTML == ctrltxtNoneDedVal.value) {
                            //alert("The deductible type associated with this coverage is set to None. Deductible processing does not apply for it.");
                            alert(ReserveListingBOBValidations.ManualDedForNone);
                            return false;
                        }
                        var ctrlDedRcRowId = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_DedRcRowId_" + (iPos));
                        if (ctrlDedRcRowId != null && ctrlDedRcRowId.innerHTML == "0") {
                            //alert("There is no Deductible reserve associated with this payment reserve. Manual Deductible cannot be created.");
                            alert(ReserveListingBOBValidations.ManualDedForNoPymtRC);
                            return false;
                        }
                        var ctrlMasterReserveType = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_MasterReserveType_" + (iPos));
                        if (ctrlMasterReserveType != null && ctrlMasterReserveType.innerHTML == "R") {
                            alert(ReserveListingBOBValidations.ManualDedForRecRsv);
                            //alert("Manual Deductibles cannot be created for Recovery reserve.");
                            return false;
                        }

                        document.getElementById('txtIsDeductible').value = "1";
                        //JIRA-857 Ends
                        return RMX.Reserves.NavigateToFundsBOB("true");
                    }
                    }
                    else
                        return false;
                }
            }

        function TransfertoScheduleChecks() {

            var myTab = $get('TabContainer1').control;
            var iIndex = (myTab.get_activeTabIndex() + 1);

            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;

            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                }
            }

            if (iPos == -1) {

                //alert("Please select a row in the grid.");
                alert(ReserveListingBOBValidations.SelectRowID);
                //$find('EditReserveModalPopup1').hide();
                //document.getElementById('CancelButton').click();

                return false;
            }
            else {
                //bpaskova 23 Feb 2015: added second condition for fixing JIRA RMA-6270
                if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlag(iIndex, p_sGridName, iPos) && RMX.Reserves.CheckForDeductible(iIndex, p_sGridName, iPos))
                    return RMX.Reserves.ScheduleChecksBOB();
                else
                    return false;
            }
        }

        //Start averma62
        function ExportToVSS() {
            var myTab = $get('TabContainer1').control;
            var iIndex = (myTab.get_activeTabIndex() + 1);

            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;

            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                }
            }

            if (iPos == -1) {
                //alert("Please select a row in the grid.");
                //alert(ReserveListingBOBValidations.SelectRowID);
                if (confirm("You are going to Push all Reserves attached with this Claimant to VSS. Do you want to Continue ?")) {
                    return true;
                }
                else { return false; }
            }
            else {
                return true;
            }
        }
        //End averma62
        //Added by sharishkumar for Mits 35472
        function ExportToLSS()
        {          
            document.getElementById('hdLSSReserve').value = "";
            var myTab = $get('TabContainer1').control;
            var iIndex = (myTab.get_activeTabIndex() + 1);

            var p_sGridName = "Grid" + (iIndex);

            var gridElementsCheck = document.getElementsByName(p_sGridName + '_CheckClaimantsGroup');
            var iPos = -1;
            var iIndex = p_sGridName.substring(4, 6);
            if (gridElementsCheck != null) {
                for (var i = 0; i < gridElementsCheck.length; i++) {
                    var gridName = gridElementsCheck[i].name;                    
                    if (gridName == p_sGridName + '_CheckClaimantsGroup') {
                        if (gridElementsCheck[i].checked && !(gridElementsCheck[i].disabled))
                        {                            
                            if (document.getElementById('hdLSSReserve') != null && document.getElementById('hdLSSReserve').value == "")
                                document.getElementById('hdLSSReserve').value = gridElementsCheck[i].getAttribute('reserve');
                            else
                                document.getElementById('hdLSSReserve').value = document.getElementById('hdLSSReserve').value + ',' + gridElementsCheck[i].getAttribute('reserve');
                        }
                    }
                }
                //sharishkumar :starts JIRA RMA-1029
                if(document.getElementById('hdLSSReserve').value==null || document.getElementById('hdLSSReserve').value=="")
                {
                    alert("Please select a row in the grid to push reserve to LSS.");
                    return false;
                }
                //Ends JIRA RMA-1029
            }
            return true;
        }
       //End  Mits 35472
        //Called on selection of Radio button
        function RefreshGrid(p_sGridName) {            
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;
            var iIndex = p_sGridName.substring(4,6);    // changed for MITS 33693: to fetch value 10 instead of 1 in case of multiple claimants. 
            if (gridElementsRadio != null) {
                for (var i = 0; i < gridElementsRadio.length; i++) {
                    var gridName = gridElementsRadio[i].name;
                    if (gridName == p_sGridName + '_ClaimantsGroup' && gridElementsRadio[i].checked) {
                        var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                    }
                }
            }


            if (iPos != -1) {

                var sUpdatePanelID = "TabContainer1_" + "TabPanel" + iIndex + "_UpdatePanel" + iIndex;
                var sID = "";
                if (iPos < 8)
                    sID = "0";

                iReserveID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveTypeCode_" + (iPos)).innerHTML;
                iPolicyID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolicyID_" + (iPos)).innerHTML;
                iPolCvgLossID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolCvgLossID_" + (iPos)).innerHTML;
                iPolCvgID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolCvgID_" + (iPos)).innerHTML;
                iPolicyUnitRowId = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolicyUnitRowID_" + (iPos)).innerHTML; //MITS 34275- RMA Swiss Re Financial Summary 
                iRC_RowID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveID_" + (iPos)).innerHTML;                                               
                document.getElementById('PreventCollOnRes').value = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PreventCollOnRes_" + (iPos)).innerHTML; //JIRA-857
                document.getElementById('ResType').value = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveType_" + (iPos)).innerHTML; //JIRA-857
                document.getElementById('MasResType').value = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_MasterReserveType_" + (iPos)).innerHTML; //JIRA-857
                //rupal:start
                iCovgSeqNum = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_CovgSeqNum_" + (iPos)).innerHTML;
                if (iCovgSeqNum != "")
                    document.getElementById('CovgSeqNum').value = iCovgSeqNum; //jira : 7518
                    //document.getElementById('covgseqnum').value = iCovgSeqNum;
                    
                //rupal:end
                iTransSeqNum = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_TransSeqNum_" + (iPos )).innerHTML;
                if (iTransSeqNum != "")
                    document.getElementById('TransSeqNum').value = iTransSeqNum; //jira : 7518
                    //document.getElementById('transseqnum').value = iTransSeqNum;
                //Ankit Start : Worked on MITS - 34297
                iCoverageKey = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_CoverageKey_" + (iPos)).innerHTML;
                if (iCoverageKey != "")
                    document.getElementById('CoverageKey').value = iCoverageKey;
                //Ankit End

                document.getElementById('policyid').value = iPolicyID;
                document.getElementById('polcvgid').value = iPolCvgID;
                document.getElementById('cvglossid').value = iPolCvgLossID;
                document.getElementById('txtUnitID').value = iPolicyUnitRowId; //MITS 34275- RMA Swiss Re Financial Summary  
                document.getElementById('Reserve').value = iReserveID;
                document.getElementById('rc_row_id').value = iRC_RowID;
                iRsvStatusParent = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_RsvStatusParent_" + (iPos)).innerHTML;
                if (iRsvStatusParent != "")
                document.getElementById('ResStatusParent').value = iRsvStatusParent;
                document.getElementById('currencytype').value = document.getElementById('txtClaimCurrency').value;//MITS:34082

                var sIDs = iPolicyID + "|" + iPolCvgID + "|" + iReserveID + "|" + iRC_RowID + "|" + iPolCvgLossID;
                //iClaimantId = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ID_" + iPos).innerHTML
                document.getElementById('TabContainer1_TabPanel' + iIndex + "_SelectedId" + iIndex).value = sIDs;
                document.getElementById('TabContainer').value = iIndex;//MITS:34082
                __doPostBack(sUpdatePanelID, '');
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(stylinggrid);
            }

        }
  <%-- MITS 34275- RMA Swiss Re Financial Summary START --%>

        function TransferToUnitCoverageSummary(level) {


            var policyID = document.getElementById('policyid').value;
            var coverageID = document.getElementById('polcvgid').value;
            var unitrowid = document.getElementById('txtUnitID').value;
            var multicurrencyindicator = document.getElementById('txtUseMulticurrency').value;
            var claimcurrencytype = document.getElementById('txtClaimCurrenyType').value;

            document.forms[0].action = "ReserveSummary.aspx?PolicyID=" + policyID + "&CoverageID=" + coverageID + "&UnitID=" + unitrowid + "&UseMulticurrency=" + multicurrencyindicator + "&ClaimCurrency=" + claimcurrencytype;
            document.forms[0].method = "post";
            var myTab = $get('TabContainer1').control;
            var iIndex = (myTab.get_activeTabIndex() + 1);

            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;

            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                }
            }

            if (iPos == -1) {

                alert(ReserveListingBOBValidations.SelectRowID);
                return false;
            }
            else if (document.getElementById('txtReserveSummaryLevel') != null) {
                //Ashish Ahuja - MITS 34275 - Display header start
                var sPolicy = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Policy_" + (iPos)).innerHTML;
                var sUnit = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Unit_" + (iPos)).innerHTML;
                var sPolCvg = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_CoverageType_" + (iPos)).innerHTML;
                document.forms[0].action += "&Policy=" + sPolicy + "&Unit=" + sUnit + "&Coverage=" + sPolCvg;
                //Ashish Ahuja - MITS 34275 - Display header end
                document.getElementById('txtReserveSummaryLevel').value = level;
            }
            document.forms[0].submit();
            return false;
        }
                       <%-- START : JIRA: RMA-4608  - RMA Swiss Re Financial Deatil History --%>
        function TransferToFinancialDetailHistory(level) {

            var claimID = document.getElementById('claimid').value;
            var claimnumber = document.getElementById('claimnumber').value;
            var claimantID = document.getElementById('claimanteid').value;
            var policyID = document.getElementById('policyid').value;
            var unitrowid = document.getElementById('txtUnitID').value;
            var coverageID = document.getElementById('polcvgid').value;
            var multicurrencyindicator = document.getElementById('txtUseMulticurrency').value;
            var claimcurrencytype = document.getElementById('txtClaimCurrenyType').value;


            var myTab = $get('TabContainer1').control;
            var iIndex = (myTab.get_activeTabIndex() + 1);

            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;

            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                }
            }

            var SBrowser = navigator.appName;

            if (navigator.appName != "Netscape") {
                alert(ReserveListingBOBValidations.UpgradeVersion);
                return false;
            }

            if (iPos > -1) {
                var claimID = document.getElementById('claimid').value;
                var claimnumber = document.getElementById('claimnumber').value;
                var claimantID = document.getElementById('claimanteid').value;
                var sclaimantname = document.getElementById("__tab_TabContainer1_TabPanel" + iIndex).innerHTML;
                var reservetypecode = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveTypeCode_" + (iPos)).innerHTML;
                var reservestatus = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Status_" + (iPos)).innerHTML;
                var reservetypedesc = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveType_" + (iPos)).innerHTML;
                var policyID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolicyID_" + (iPos)).innerHTML;
                var sPolicy = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Policy_" + (iPos)).innerHTML;
                var unitrowid = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolicyUnitRowID_" + (iPos)).innerHTML;
                var sUnit = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Unit_" + (iPos)).innerHTML;
                var sPolCvg = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_CoverageType_" + (iPos)).innerHTML;
                var coverageID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolCvgID_" + (iPos)).innerHTML;
                var polcvglossID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolCvgLossID_" + (iPos)).innerHTML;
                var rcrowID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveID_" + (iPos)).innerHTML;
                document.getElementById('txtReserveSummaryLevel').value = level;
            }
            else {
                var claimID = document.getElementById('claimid').value;
                var claimnumber = document.getElementById('claimnumber').value;
                var claimantID = document.getElementById('claimanteid').value;
                var sclaimantname = document.getElementById("__tab_TabContainer1_TabPanel" + iIndex).innerHTML;
                var reservetypecode = "0";
                var reservestatus = "0";
                var reservetypedesc = "0";
                var policyID = "0";
                var sPolicy = "0";
                var unitrowid = "0";
                var sUnit = "0";
                var sPolCvg = "0";
                var coverageID = "0";
                var polcvglossID = "0";
                var rcrowID = "0";
                document.getElementById('txtReserveSummaryLevel').value = level;
            }

            document.forms[0].action = "FinancialDetailHistory.aspx?ClaimID=" + claimID + "&ClaimantID=" + claimantID + "&PolicyID=" + policyID + "&CoverageID=" + coverageID + "&UnitID=" + unitrowid + "&UseMulticurrency=" + multicurrencyindicator + "&ClaimCurrency=" + claimcurrencytype + "&CvgLossId=" + polcvglossID + "&ReserveType=" + reservetypecode + "&RcRowId=" + rcrowID + "&ReserveStatus=" + reservestatus + "&ReserveTypeDesc=" + reservetypedesc + "&ClaimNo=" + claimnumber + "&ClaimantName=" + sclaimantname;
            document.forms[0].action += "&Policy=" + sPolicy + "&Unit=" + sUnit + "&Coverage=" + sPolCvg;
            document.forms[0].method = "post";
            document.forms[0].submit();
            return false;
        }
        <%-- END :  JIRA: RMA-4608  - RMA Swiss Re Financial Detail History --%>



        <%-- MITS 34275- RMA Swiss Re Financial Summary END --%>
        function TransferToReserveSummary(level) {
            // akaushik5 Changed for MITS 33577 Ends
            //document.forms[0].action = "ReserveSummary.aspx";
            document.forms[0].method = "post";
            // akaushik5 Added for MITS 33577 Starts
            if (document.getElementById('txtReserveSummaryLevel') != null) {
                document.getElementById('txtReserveSummaryLevel').value = level;
            }
            // akaushik5 Added for MITS 33577 Ends
            document.forms[0].action = "ReserveListingBOB.aspx?page=ReserveSummary";
            document.forms[0].submit();
            return false;
        }

        function GetPositionForSelectedGridRowforRadioButton(p_sGridName, p_iSelectedId) {
            var iRowPosition = -1;
            var objForm = document;

            if (objForm == null) {
                return iRowPosition;
            }

            var gridElements = document.getElementsByName(p_sGridName);
            var iRunningNumber = 0;
            if (gridElements != null) {
                for (var i = 0; i < gridElements.length; i++) {
                    var gridName = gridElements[i].name;
                    if (gridName == p_sGridName) {
                        if (gridElements[i].checked && gridElements[i].value == p_iSelectedId) {
                            iRowPosition = iRunningNumber;
                            break;
                        }
                        iRunningNumber++;
                    }
                }
            }
            else {
                //alert("Error. Unable to access form elements on the parent window. Please contact your system administrator.");
                alert(ReserveListingBOBValidations.UnableToAccessID);
            }
            return iRowPosition;
        }

        function ActiveTabChanged(sender, e) {
            var b = sender.get_activeTab().get_headerText();
            var i = sender.get_activeTabIndex();
            var sUpdatePanelID = "TabContainer1_" + "TabPanel" + (i + 1) + "_TabUpdatePanel" + (i + 1);
            var sIsDataLoadedID = "TabContainer1_" + "TabPanel" + (i + 1) + "_IsDataLoaded" + (i + 1);
            var sClaimantEID = "TabContainer1_" + "TabPanel" + (i + 1) + "_ClaimantEID" + (i + 1);

            document.getElementById('claimanteid').value = document.getElementById(sClaimantEID).value;
            document.getElementById('currencytype').value = document.getElementById('txtClaimCurrency').value;//MITS:34082
            //Deb MITS 31370
            //if (document.getElementById(sIsDataLoadedID).value != "1")
            __doPostBack(sUpdatePanelID, sender.get_activeTab().get_headerText());
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(stylinggrid);
        }

        //bpaskova JIRA 9419 start
        function UnloadClaimData() {
            if (parent.UnloadClaimOnNewReserve != null) {
                parent.UnloadClaimOnNewReserve();
            }
        }
        //bpaskova JIRA 9419 end
        //bpaskova JIRA 10347 start
        function UnloadClaimantData(iClaimantId) {
            if (parent.UnloadClaimantOnNewReserve != null) {
                parent.UnloadClaimantOnNewReserve(iClaimantId);
            }
        }
        //bpaskova JIRA 10347 end
     //bsharma33 RMA-3887 Reserve Supplemental starts 

       /* function onOkAdd() {
            //rupal:start, jira 341
            //var ctrlDisabilityCat, ctrlDisabilityType;
            //document.getElementById('lblError1').innerHTML = "";
            //if (document.getElementById('Reservecurrencytypetext_codelookup') != undefined) {
            //    if (document.getElementById('Reservecurrencytypetext_codelookup').value == "") {
            //        document.getElementById('lblError1').innerHTML = "Please select a Currency Type.";
            //        return false;

            //    }
            //}
            //iPolicyID = (document.all('cmbPolicy').options[document.all('cmbPolicy').selectedIndex].value);

            //if (iPolicyID == "") {
            //    document.getElementById('lblError1').innerHTML = ReserveListingBOBValidations.SelectPolicyID;
            //    return false;

            //}
            ////rupal:start
            ////iCoverageID = (document.all('cmbCoverageType').options[document.all('cmbCoverageType').selectedIndex].value);
            //var arrCoverageID = (document.all('cmbCoverageType').options[document.all('cmbCoverageType').selectedIndex].value.split('#'));
            //iCoverageID = arrCoverageID[0];
            ////rupal:end
            //if (iCoverageID == "") {
            //    document.getElementById('lblError1').innerHTML = ReserveListingBOBValidations.SelectCoverageID;
            //    return false;

            //}
            //var ctrlLOB = document.getElementById("lob");
            //if (ctrlLOB != null) {
            //    if (ctrlLOB.value == '243') {
            //        ctrlDisabilityCat = (document.all('cmbdisablitycat').options[document.all('cmbdisablitycat').selectedIndex].value);
            //        ctrlDisabilityType = document.forms[0].disabilitytype_codelookup_cid.value;

            //        if (ctrlDisabilityCat == "") {
            //            document.getElementById('lblError1').innerHTML = "Please select Disability Category.";
            //            return false;
            //        }
            //        if (trim(ctrlDisabilityType) == "" || trim(ctrlDisabilityType) == "0") {
            //            document.getElementById('lblError1').innerHTML = "Please select Disability Loss Type.";
            //            document.forms[0].disabilitytype_codelookup.focus();
            //            return false;
            //        }

            //    }
            //    else {
            //        iCoverageLossID = (document.all('cmbCoverageLossType').options[document.all('cmbCoverageLossType').selectedIndex].value);
            //        if (iCoverageLossID == "") {
            //            document.getElementById('lblError1').innerHTML = "Please select a Loss Type.";
            //            return false;
            //        }
            //    }

            //}
            //iReserveID = (document.all('cmbReserveType').options[document.all('cmbReserveType').selectedIndex].value);
            //if (iReserveID == "") {
            //    document.getElementById('lblError1').innerHTML = ReserveListingBOBValidations.SelectReserveID;
            //    return false;
            //}
            //if (document.getElementById('txtAmount').value == "") {
            //    document.getElementById('lblError1').innerHTML = ReserveListingBOBValidations.EnterAmountID;
            //    return false;

            //}

            //var bIsValidated;
            //bIsValidated = Validate();
            //if (bIsValidated == false)
            //    return false;

            ////igupta3 JIRA RMA-348
            //var sReserveAmount;
            //if (document.forms[0].PrevResModifyzero != null) {
            //    sReserveAmount = (document.forms[0].txtAmount.value).replace(/[,\$]/g, ""); //JIRA RMA-616
            //    sReserveAmount = parseFloat(sReserveAmount);
            //    if (sReserveAmount <= 0 && document.forms[0].PrevResModifyzero.value == "True") {
            //        if (!confirm("The Reserve amount should be greater than $0.00. Do you really want to modify this reserve to $0.00?"))
            //            return false;
            //    }
            //}
            //if (ValidateAddReserve()) {   //bpaskova JIRA 10347
            var myTab = $get('TabContainer1').control;
            var iActiveTabIndex = myTab.get_activeTabIndex();
            var iClaimantId = document.getElementById('TabContainer1_TabPanel' + (iActiveTabIndex + 1) + "_ClaimantRowId" + (iActiveTabIndex + 1)).value;
            if (ValidateAddReserve(iClaimantId)) {  //bpaskova JIRA 10347

                //document.getElementById('txtaction').value = "Add";
                document.getElementById('TabContainer1_TabPanel' + (iActiveTabIndex + 1) + "_txtAction" + (iActiveTabIndex + 1)).value = "Add";


                var sUpdatePanelID = "TabContainer1_" + "TabPanel" + (iActiveTabIndex + 1) + "_TabUpdatePanel" + (iActiveTabIndex + 1);
                __doPostBack(sUpdatePanelID, "");
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(stylinggrid);
                UnloadClaimData();  //bpaskova JIRA 9419
                UnloadClaimantData(iClaimantId); //bpaskova JIRA 10347

                document.getElementById('Ok').visible = "true";
                document.getElementById('Ok').click();
                return false;
            }
            else
                return false;
            //   Clear();
            //rupal:end, jira 341
        }*/

        //rupal:start, jira 341
        function HideAddMore() {
            stylinggrid();
            var AddResModalPopup = $find('behAddMore');
            if (AddResModalPopup)
                AddResModalPopup.hide();

            //$find('behAddMore').show();
        }

        /*function onAddMore() {
            //if (ValidateAddReserve()) { //bpaskova JIRA 10347

            var myTab = $get('TabContainer1').control;
            var iActiveTabIndex = myTab.get_activeTabIndex();
            var iClaimantId = document.getElementById('TabContainer1_TabPanel' + (iActiveTabIndex + 1) + "_ClaimantRowId" + (iActiveTabIndex + 1)).value; //bpaskova JIRA 10347

            if (ValidateAddReserve(iClaimantId)) { //bpaskova JIRA 10347

                ////document.getElementById('txtaction').value = "Add";
                //document.getElementById('TabContainer1_TabPanel' + (iActiveTabIndex + 1) + "_txtAction" + (iActiveTabIndex + 1)).value = "AddMore";


                //var sUpdatePanelID = "TabContainer1_" + "TabPanel" + (iActiveTabIndex + 1) + "_TabUpdatePanel" + (iActiveTabIndex + 1);
                //__doPostBack(sUpdatePanelID, "");
                //document.getElementById('TabContainer1_TabPanel' + (iActiveTabIndex + 1) + "_txtAction" + (iActiveTabIndex + 1)).value = "";


                //document.getElementById("txtActionAddMore").value = "AddMore";
                //document.getElementById('txtaction').value = "AddMore";
                var sUpdatePanelID = "TabContainer1_" + "TabPanel" + (iActiveTabIndex + 1) + "_TabUpdatePanel" + (iActiveTabIndex + 1);
                document.getElementById('TabContainer1_TabPanel' + (iActiveTabIndex + 1) + "_txtAction" + (iActiveTabIndex + 1)).value = "AddMore";
                //__doPostBack(sUpdatePanelID, "AddMore");
                //__doPostBack("upnlAddMore", "AddMore");
                //__doPostBack("upnlAddMore", "");
                UnloadClaimData();  //bpaskova JIRA 9419
                UnloadClaimantData(iClaimantId); //bpaskova JIRA 10347

                return true;
            }
            else
                return false;

        }*/
       /* function ValidateAddReserve() {
            var ctrlDisabilityCat, ctrlDisabilityType;
            //bpaskova JIRA 9419 start
            if (parent.MDIIsClaimAncestorDirty != null && parent.MDIIsClaimAncestorDirty()) {
                alert(parent.CommonValidations.sharedReserveInfoWarning);
                return false;
            }
            //bpaskova JIRA 9419 end
            document.getElementById('lblError1').innerHTML = "";
            if (document.getElementById('Reservecurrencytypetext_codelookup') != undefined) {
                if (document.getElementById('Reservecurrencytypetext_codelookup').value == "") {
                    document.getElementById('lblError1').innerHTML = "Please select a Currency Type.";
                    return false;

                }
            }
            iPolicyID = (document.all('cmbPolicy').options[document.all('cmbPolicy').selectedIndex].value);

            if (iPolicyID == "") {
                document.getElementById('lblError1').innerHTML = ReserveListingBOBValidations.SelectPolicyID;
                return false;

            }
            //rupal:start
            //iCoverageID = (document.all('cmbCoverageType').options[document.all('cmbCoverageType').selectedIndex].value);
            var arrCoverageID = (document.all('cmbCoverageType').options[document.all('cmbCoverageType').selectedIndex].value.split('#'));
            iCoverageID = arrCoverageID[0];
            //rupal:end
            if (iCoverageID == "") {
                document.getElementById('lblError1').innerHTML = ReserveListingBOBValidations.SelectCoverageID;
                return false;

            }
            var ctrlLOB = document.getElementById("lob");
            if (ctrlLOB != null) {
                if (ctrlLOB.value == '243') {
                    ctrlDisabilityCat = (document.all('cmbdisablitycat').options[document.all('cmbdisablitycat').selectedIndex].value);
                    ctrlDisabilityType = document.forms[0].disabilitytype_codelookup_cid.value;

                    if (ctrlDisabilityCat == "") {
                        document.getElementById('lblError1').innerHTML = "Please select Disability Category.";
                        return false;
                    }
                    if (trim(ctrlDisabilityType) == "" || trim(ctrlDisabilityType) == "0") {
                        document.getElementById('lblError1').innerHTML = "Please select Disability Loss Type.";
                        document.forms[0].disabilitytype_codelookup.focus();
                        return false;
                    }

                }
                else {
                    iCoverageLossID = (document.all('cmbCoverageLossType').options[document.all('cmbCoverageLossType').selectedIndex].value);
                    if (iCoverageLossID == "") {
                        document.getElementById('lblError1').innerHTML = "Please select a Loss Type.";
                        return false;
                    }
                }

            }
            iReserveID = (document.all('cmbReserveType').options[document.all('cmbReserveType').selectedIndex].value);
            if (iReserveID == "") {
                document.getElementById('lblError1').innerHTML = ReserveListingBOBValidations.SelectReserveID;
                return false;
            }
            if (document.getElementById('txtAmount').value == "") {
                document.getElementById('lblError1').innerHTML = ReserveListingBOBValidations.EnterAmountID;
                return false;

            }

            var bIsValidated;
            bIsValidated = Validate();
            if (bIsValidated == false)
                return false;

            //igupta3 JIRA RMA-348
            var sReserveAmount;
            if (document.forms[0].PrevResModifyzero != null) {
                sReserveAmount = (document.forms[0].txtAmount.value).replace(/[,\$]/g, ""); //JIRA RMA-616
                sReserveAmount = parseFloat(sReserveAmount);
                if (sReserveAmount <= 0 && document.forms[0].PrevResModifyzero.value == "True") {
                    if (!confirm("The Reserve amount should be greater than $0.00. Do you really want to modify this reserve to $0.00?"))
                        return false;
                }
            }


            return true;
        }*/
        //bsharma33 RMA-3887 Reserve Supplemental ends
        //rupal:end,jira 341
        //MITS:34082 START
        function SetCulture() {
            document.getElementById('currencytype').value = document.getElementById('txtClaimCurrency').value;
            var txtEditAmount = document.getElementById('txtEditAmount');
            txtEditAmount.value = "0.00";
            var txtEditBalanceAmount = document.getElementById('txtEditBalanceAmount');
            txtEditBalanceAmount.value = "0.00";
            var txtAmount = document.getElementById('txtAmount');
            txtAmount.value = "0.00";
            document.getElementById("Reservecurrencytypetext_codelookup").value = document.getElementById('txtClaimCurrenyType').value;
            document.forms[0].submit();
        }

        function SetCultureEdit() {
            document.getElementById('currencytype').value = document.getElementById('txtClaimCurrency').value;
            var txtEditAmount = document.getElementById('txtEditAmount');
            txtEditAmount.value = "0.00";  //rupal:r8 multi currency
            var txtEditBalanceAmount = document.getElementById('txtEditBalanceAmount');
            txtEditBalanceAmount.value = "0.00";  //rupal:r8 multi currency
            var txtAmount = document.getElementById('txtAmount');
            txtAmount.value = "0.00";  //rupal:r8 multi currency
            document.getElementById("ReservecurrencytypetextEdit_codelookup").value = document.getElementById('txtClaimCurrenyType').value;
            document.forms[0].submit();
        }
        //MITS:34082 END
        //bsharma33 RMA-3887 Reserve Supplemental starts
        //function OnEditLoad() {
            
        //    var myTab = $get('TabContainer1').control;
        //    //MITS:34082 START
        //    //var iIndex = (myTab.get_activeTabIndex() + 1);
        //    if (myTab == undefined) {
        //        if (document.getElementById('TabContainer').value != "") {
        //            var iIndex = document.getElementById('TabContainer').value;
        //        }
        //    }
        //    else {
        //        var iIndex = (myTab.get_activeTabIndex() + 1);
        //    }
        //    //var myTab = $get('TabContainer1').control;
        //    //var iIndex = (myTab.get_activeTabIndex() + 1);
        //    document.getElementById('TabContainer').value = iIndex;
        //    //MITS:34082 END
        //    document.getElementById('lblError1').innerHTML = "";

        //    var p_sGridName = "Grid" + (iIndex);
        //    var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
        //    var iPos = -1;
        //    var sAdjusterDetails;
        //    //35453 starts
        //    var sClmCurrBalAmount;
        //    var sClmCurrResAmount;
        //    //35453 ends 



        //    for (var i = 0; i < gridElementsRadio.length; i++) {
        //        if (gridElementsRadio[i].checked) {
        //            var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
        //        }
        //    }


        //    if (iPos != -1) {

        //        //var sUpdatePanelID = "TabContainer1_" + "TabPanel" + iIndex + "_UpdatePanel" + iIndex;
        //        var sID = "";
        //        if (iPos < 8)
        //            sID = "0";
        //        bShowModal = true;
        //        iReserveID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveID_" + (iPos)).innerHTML;
        //        iPolicyID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolicyID_" + (iPos)).innerHTML;
        //        iPolCvgID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolCvgID_" + (iPos)).innerHTML;
        //        iPolCvgLossID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolCvgLossID_" + (iPos)).innerHTML;
        //        iPolicyUnitRowId = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolicyUnitRowID_" + (iPos)).innerHTML;
        //        iReserveTypeCode = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveTypeCode_" + (iPos)).innerHTML;
        //        //rupal:start
        //        iCovgSeqNum = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_CovgSeqNum_" + (iPos)).innerHTML;
        //        //rupal:end
        //        iTransSeqNum = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_TransSeqNum_" + (iPos)).innerHTML;
        //        iCoverageKey = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_CoverageKey_" + (iPos)).innerHTML;       //Ankit Start : Worked on MITS - 34297
        //        //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
        //        sReserveSubType = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveSubType_" + (iPos)).innerHTML;
        //        sReserveBalanceAmount = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Balance_" + (iPos)).innerHTML;
        //        //Ankit End
        //        sReserve = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveType_" + (iPos)).innerHTML;
        //        sPolicy = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Policy_" + (iPos)).innerHTML;
        //        sUnit = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Unit_" + (iPos)).innerHTML;
        //        sPolCvg = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_CoverageType_" + (iPos)).innerHTML;
        //        sCvgLoss = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_LossType_" + (iPos)).innerHTML;
        //        sAmount = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveAmount_" + (iPos)).innerHTML;
        //        //35453 starts
        //        sClmCurrBalAmount = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ClmCurrBalAmount_" + (iPos)).innerHTML;
        //        sClmCurrResAmount = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ClmCurrResAmount_" + (iPos)).innerHTML;
        //        //35453 ends 
        //        sStatus = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Status_" + (iPos)).innerHTML;
        //        sDisabilityCat = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_disabilitycatdesc_" + (iPos)).innerHTML;
        //        sAdjusterDetails = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_AdjusterDetails_" + (iPos)).innerHTML;   //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        //        iRsvStatusParent = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_RsvStatusParent_" + (iPos)).innerHTML;//pgupta93: RMA-7078
        //        //mgaba2:R8:Supervisory Approval
        //        ctlStatusEdit = document.getElementById('lblStatusEdit');
        //        if (ctlStatusEdit != null) {
        //            //MITS 26605:mjain26  Changing status from HOLD to H
        //            //pgupta93: RMA-7078 START
        //            //if (sStatus == 'H') { 
        //            if (iRsvStatusParent == 'H')
        //            //pgupta93: RMA-7078 END
        //            {
        //                ctlStatusEdit.style.display = '';
        //                ctlStatusEdit.innerHTML = sStatus;
        //                document.getElementById('cmbStatusEdit').style.display= 'none';
        //                document.getElementById('EditOkButton').disabled = true;
        //                document.getElementById('btnApprove').style.display= '';
        //                document.getElementById('btnReject').style.display= '';
        //                // skhare7 R8 supervisory approval  
        //                document.getElementById('txtAppRejReqCom').style.display = '';
        //                document.getElementById('lblReason').style.display = '';
        //            }
        //            else {
        //                ctlStatusEdit.style.display = 'none';
        //                document.getElementById('cmbStatusEdit').style.display = '';
        //                document.getElementById('EditOkButton').disabled = false;
        //                document.getElementById('btnApprove').style.display = 'none';
        //                document.getElementById('btnReject').style.display = 'none';
        //                // skhare7 R8 supervisory approval  
        //                document.getElementById('txtAppRejReqCom').style.display = 'none';
        //                document.getElementById('lblReason').style.display = 'none';
        //                //Already a bug exists in BOB code that status field doesnt come up in pop up
        //            }
        //        }
        //        document.getElementById('txtReserveTypeCode').value = iReserveTypeCode;
        //        document.getElementById('lblPolicy').innerHTML = sPolicy;
        //        document.getElementById('txtPolicyID').value = iPolicyID;
        //        document.getElementById('lblUnit').innerHTML = sUnit;
               
        //       //MITS 32517 start: set text and tooltip title to new control for "Edit Reserve" screen. 
        //        document.getElementById('txtUnitText').value = sUnit;
        //        document.getElementById('txtUnitText').title = sUnit;
        //        // MITS 32517 end:
        //        document.getElementById('txtUnitID').value = iPolicyUnitRowId;
        //        document.getElementById('lblCoverageType').innerHTML = sPolCvg;
        //        document.getElementById('txtCoverageType').value = iPolCvgID;
        //        //   document.getElementById('lblCoverageLossType').innerHTML = 'Disability Code';
        //        //document.getElementById('txtCoverageLossType').value = sCvgLoss;
        //        document.getElementById('txtCoverageLossType').innerHTML = sCvgLoss; //aaggarwal29: Added innerHTML as it is a lable type control.
        //        document.getElementById('txtDisabilityLossType').value = sCvgLoss;

        //        document.getElementById('lblReserveType').innerHTML = sReserve;

        //        //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
        //        document.getElementById('lblReserveSubType').innerHTML = sReserveSubType;
        //        // akaushik5 Changed for MITS 33495 Starts
        //        // document.getElementById('lblReserveBalanceAmount').innerHTML = sReserveBalanceAmount;
        //        //MIts 354543 starts
        //        if (document.getElementById('hdUseMultiCurrency').value != "" && document.getElementById('hdUseMultiCurrency').value != "0"){
				
        //            if (document.getElementById("ReservecurrencytypetextEdit_codelookup").value == document.getElementById('currencytype').value.split('_')[0])//MITS:34082  //mbahl3 JIRA[RMA-460]
        //        document.getElementById('txtEditBalanceAmount').value = sClmCurrBalAmount;
        //        }//MITS:34082
        //        else
        //        //Mits 35453 ends 
        //        document.getElementById('txtEditBalanceAmount').value = sReserveBalanceAmount;
        //        MultiCurrencyToDecimal_Form(document.getElementById('txtEditBalanceAmount'));
        //        iLoadReserveBalance = document.getElementById('txtEditBalanceAmount').getAttribute("Amount");
        //        iLoadReserveAmount = parseFloat(sAmount);
        //        // akaushik5 Changed for MITS 33495 Ends
        //        //Ankit End
        //        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        //        sAdjusterDetails = sAdjusterDetails.split('|@');
        //        if (sAdjusterDetails[0] != null) {
        //            document.getElementById('claimadjusterlookup_cid').value = sAdjusterDetails[0];
        //            document.getElementById('editclaimadjusterlookup_cid').value = sAdjusterDetails[0];
        //        }
        //        if (sAdjusterDetails[1] != null) {
        //            document.getElementById('claimadjusterlookup').value = sAdjusterDetails[1];
        //            document.getElementById('editclaimadjusterlookup').value = sAdjusterDetails[1];
        //        }
        //        //Ankit End
        //        document.getElementById('txtReserveID').value = iReserveID;
        //        //MITS:34082 START
        //        if (document.getElementById('hdUseMultiCurrency').value != "0") {
        //            if (document.getElementById("ReservecurrencytypetextEdit_codelookup").value == document.getElementById('currencytype').value.split('_')[0])  //mbahl3 JIRA[RMA-460]
        //                document.getElementById('txtEditAmount').value = sAmount;
        //        }
        //        else
        //        document.getElementById('txtEditAmount').value = sAmount;
        //        //MITS:34082 END
        //        // MITS 35453 starts
        //        if (document.getElementById('hdUseMultiCurrency').value != "" && document.getElementById('hdUseMultiCurrency').value != "0"){
        //            if (document.getElementById("ReservecurrencytypetextEdit_codelookup").value == document.getElementById('currencytype').value.split('_')[0])//MITS:34082  //mbahl3 JIRA[RMA-460]
        //            document.getElementById('txtEditAmount').value = sClmCurrResAmount;
        //        }//MITS:34082
        //        else
        //        //Mits 35453 ends 
        //        document.getElementById('txtEditAmount').value = sAmount;
        //        // akaushik5 Added for MITS 33495 Starts
        //        MultiCurrencyToDecimal_Form(document.getElementById('txtEditAmount'));
        //        iLoadReserveAmount = document.getElementById('txtEditAmount').getAttribute("Amount");
        //        // akaushik5 Changed for MITS 33495 Ends

        //        //rupal
        //        document.getElementById('txtCovgSeqNum').value = iCovgSeqNum;
        //        document.getElementById('txtTransSeqNum').value = iTransSeqNum;
        //        document.getElementById('txtCoverageKey').value = iCoverageKey;     //Ankit Start : Worked on MITS - 34297
        //        var ctrlLOB = document.getElementById("lob");
        //        if (ctrlLOB != null) 
        //        {
        //            if (ctrlLOB.value == '243') 
        //            {
        //                document.getElementById('txtDisabilityCat').innerHTML = sDisabilityCat;
        //                document.getElementById('txtCoverageLossType').style.display = 'none';
        //                document.getElementById('lblCoverageLossType').style.display= 'none';

        //            }
        //            else {
        //                document.getElementById('lblDisabilityCat').style.display = 'none';
        //                document.getElementById('txtDisabilityCat').style.display= 'none';
        //                document.getElementById('lblDisabilityLossType').style.display = 'none';
        //                document.getElementById('txtDisabilityLossType').style.display = 'none';

        //            }

        //        }




        //        //Ankit Start : Financial Enhancement - Status Code Changes
        //        var ddlStatusCode = document.getElementById('cmbStatusEdit');
        //        var sPaidAmount = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Paid_" + (iPos)).innerHTML;
        //        sPaidAmount = parseFloat(sPaidAmount.replace(/[^0-9\.\-]+/g, ""));
        //        //pgupta93: RMA-7078 START
        //        //if (sStatus.toUpperCase() == 'O') { 
        //        if (iRsvStatusParent.toUpperCase() == 'O')
        //        //pgupta93: RMA-7078 END
        //        {
        //            if (sPaidAmount == 0)
        //                AddRemoveComboItem(ddlStatusCode, 'C');
        //            else
        //                AddRemoveComboItem(ddlStatusCode, 'CN');
        //        }
        //        else
        //            AddRemoveComboItem(ddlStatusCode, 'CN');
        //        //Ankit End

        //        //prashbiharis MITS: 30949
        //        var statusDetail = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_StatusDetail_" + (iPos)).innerHTML;
        //        //set Value of status
        //        for (var i = 0; i < ddlStatusCode.options.length; i++) {
        //            if (ddlStatusCode.options[i].text == statusDetail) {
        //                ddlStatusCode.options[i].selected = true;
        //            }
        //        }

        //        //set value of Reason
        //        var reasondetail = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Reason_" + (iPos)).innerHTML;
        //        document.getElementById('txtReasonEdit').value = reasondetail;
        //        var ddlReason = document.getElementById('cmbReasonEdit');
        //        for (var i = 0; i < ddlReason.options.length; i++) {
        //            if (ddlReason.options[i].text == reasondetail) {
        //                ddlReason.options[i].selected = true;
        //            }
        //        }
        //        //prashbiharis MITS: 30949
                // akaushik5 Added for MITS 38130 Starts
                //var ctlReserveAmount = document.getElementById('lblReserveAmount');
                //var ctlReserveBalanceAmount = document.getElementById('lblReserveBalanceAmount');
                //ctlReserveAmount.style.display = 'none';
                //ctlReserveBalanceAmount.style.display = 'none';
                //if (iRsvStatusParent == 'H') {
                //    if (ctlReserveAmount != null && document.getElementById('txtEditAmount') != null) {
                //        ctlReserveAmount.style.display = '';
                //        ctlReserveAmount.innerHTML = document.getElementById('txtEditAmount').value;
                //        document.getElementById('txtEditAmount').style.display = 'none';
                //    }

                //    if (ctlReserveBalanceAmount != null && document.getElementById('txtEditBalanceAmount') != null) {
                //        ctlReserveBalanceAmount.style.display = '';
                //        ctlReserveBalanceAmount.innerHTML = document.getElementById('txtEditBalanceAmount').value;
                //        document.getElementById('txtEditBalanceAmount').style.display = 'none';
                //    }
                //}
                //else {
                //    if (ctlReserveAmount != null && document.getElementById('txtEditAmount') != null) {
                //        ctlReserveAmount.style.display = 'none';
                //        document.getElementById('txtEditAmount').style.display = '';
                //    }

                //    if (ctlReserveBalanceAmount != null && document.getElementById('txtEditBalanceAmount') != null) {
                //        ctlReserveBalanceAmount.style.display = 'none';
                //        document.getElementById('txtEditBalanceAmount').style.display = '';
                //    }
                //}
                // akaushik5 Added for MITS 38130 Ends
        //    }
        //    else {
        //        //alert("Please select a row to edit");
        //        alert(ReserveListingBOBValidations.SelectRowEditID);
        //        bShowModal = false;

        //        return false;
        //    }
        //}
        //bsharma33 RMA-3887 Reserve Supplemental ends
        //Ankit Start : Financial Enhancement - Status Code Changes
        function AddRemoveComboItem(ddl, removeshortcodeval) {
            var hdDeletedVal = document.getElementById('hdDeletedVal');

            //Addition Logic
            if (hdDeletedVal.value != '') {
                var newOption = document.createElement("option");
                var arrSplitVal = hdDeletedVal.value.split('|');

                newOption.value = arrSplitVal[0];
                newOption.text = arrSplitVal[1];
                newOption.Tag = arrSplitVal[2];
                ddl.options.add(newOption);
                hdDeletedVal.value = '';
            }
            //End Addition

            //Rmoval Logic
            if (ddl != null) {
                for (var i = 1; i < ddl.options.length; i++) {
                    //var ShortCodeValue = ddl.options[i].Tag; // MITS 37707: Replaced Tag with innerText :aaggarwal29              
                    var ShortCodeValue = '';
                    if (ddl.options[i].innerText.split('-') != undefined && ddl.options[i].innerText.split('-').length > 0)           
                        ShortCodeValue = ddl.options[i].innerText.split('-')[0].trim();

                    if (ShortCodeValue.toUpperCase() == removeshortcodeval.toUpperCase()) {
                        hdDeletedVal.value = ddl.options[i].value + '|' + ddl.options[i].innerHTML + '|' + ShortCodeValue.toUpperCase();   
                        ddl.options.remove(i);
                    }
                }
            }
            //End Rmoval
        }
        //Ankit End

        function modalPopupShowingHandler(sender, ev) {

            //if (!confirm('Sure')) {
            if (bShowModal == false) {
                ev.set_cancel(true);
            }
        }
        function addCascadingDropDownPopulatedHandler(behaviorId, populatedEventHandler) {

            var behavior = $find(behaviorId);

            if (behavior != null) {

                behavior.add_populated(populatedEventHandler);

            }

        }
        function pageLoad(sender, args) {
           
            //bsharma33 RMA-3887 starts
            // $find("EditReserveModalPopup").add_showing(modalPopupShowingHandler);
            //bsharma33 RMA-3887 ends
            addCascadingDropDownPopulatedHandler('CascadingDropDown1', function () { addAllOption('cmbPolicy'); });
            addCascadingDropDownPopulatedHandler('CascadingDropDown2', function () { addAllOption('cmbUnit'); });
            addCascadingDropDownPopulatedHandler('CascadingDropDown3', function () { addAllOption('cmbCoverageType'); });

            addCascadingDropDownPopulatedHandler('CascadingDropDown4', function () { addAllOption('cmbCoverageLossType'); });
            addCascadingDropDownPopulatedHandler('CascadingDropDown6', function () { addAllOption('cmbReserveType'); });
            addCascadingDropDownPopulatedHandler('CascadingDropDown5', function () { addAllOption('cmbdisablitycat'); });
            //addCascadingDropDownPopulatedHandler('CascadingDropDown7', function () { addAllOption('cmbReserveCategoryAdd'); }); //mkaran2 - [JIRA] (RMA-4110) MITS 37158  


            
        }
        function addAllOption(dropdownListId) {
           

    var dropdownList = document.getElementById(dropdownListId);

    for (var i = 0; i < dropdownList.length; i++)

        dropdownList.options[i].title = dropdownList.options[i].text;
    if (((dropdownListId == "cmbUnit") && dropdownList.length > 1) || dropdownListId == "cmbCoverageType" && dropdownList.length == 2) {
        SetDefaultValues();
    }

}
        //bsharma33 RMA-3887 starts
        //function onOkEdit() {
        //    document.getElementById('lblError2').innerHTML = "";
        //    //MITS:34082 START
        //    if (document.getElementById('ReservecurrencytypetextEdit_codelookup') != undefined) {
        //        if (document.getElementById('ReservecurrencytypetextEdit_codelookup').value == "") {
        //            document.getElementById('lblError1').innerHTML = "Please select a Currency Type.";
        //            return false;
        //        }
        //    }
        //     //MITS:34082 END
        //    if (document.getElementById('txtEditAmount').value == "") {
        //        document.getElementById('lblError2').innerHTML = ReserveListingBOBValidations.EnterAmountID; ;
        //        return false;
        //    }

        //    //igupta3 JIRA RMA-348            
        //    var sReserveAmount;
        //    if (document.forms[0].PrevResModifyzero != null) {
        //        sReserveAmount = (document.forms[0].txtEditAmount.value).replace(/[,\$]/g, "");
        //        sReserveAmount = parseFloat(sReserveAmount);
        //        if (sReserveAmount <= 0 && document.forms[0].PrevResModifyzero.value == "True") {
        //            if (!confirm("The Reserve amount should be greater than $0.00. Do you really want to modify this reserve to $0.00?"))
        //                return false;
        //        }
        //    }
        //    //  pleaseWait.Show();

        //    var myTab = $get('TabContainer1').control;
        //    var iActiveTabIndex = myTab.get_activeTabIndex();
        //    //document.getElementById('txtaction').value = "Edit";
        //    document.getElementById('TabContainer1_TabPanel' + (iActiveTabIndex + 1) + "_txtAction" + (iActiveTabIndex + 1)).value = "Edit";
        //    var sUpdatePanelID = "TabContainer1_" + "TabPanel" + (iActiveTabIndex + 1) + "_TabUpdatePanel" + (iActiveTabIndex + 1);
        //    __doPostBack(sUpdatePanelID, "");
        //    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(stylinggrid);

        //    document.getElementById('OkButton').visible = "true";
        //    document.getElementById('OkButton').click();

        //    return false;

        //}
        //bsharma33 RMA-3887 ends
        //Added by aahuja21 for MITS 30977
        function ClearData(i) {

            if (i == 'disabilitytype_codelookup') {
                document.getElementById('disabilitytype_codelookup').value = "";
                document.forms[0].disabilitytype_codelookup_cid.value = ""
            }
            //mkaran2 - [JIRA] (RMA-4110) MITS 37158                          
            if (i == 'ctReserveSubType_codelookup') {
                document.getElementById('ctReserveSubType_codelookup').value = "";
                document.forms[0].ctReserveSubType_codelookup_cid.value = ""
            }
        }
        function ClearErrMsg() {
            document.getElementById('lblError1').innerHTML = "";
            document.getElementById('lblError2').innerHTML = "";
        }


        function Clear() {

            document.all('cmbPolicy').selectedIndex = 0;
            document.all('cmbCoverageType').selectedIndex = 0;
            document.all('cmbCoverageLossType').selectedIndex = 0;
            document.all('cmbReserveType').selectedIndex = 0;
            document.all('cmbUnit').selectedIndex = 0; //rupal:bob

            //document.all('cmbReserveCategoryAdd').selectedIndex = 0; //mkaran2 - [JIRA] (RMA-4110) MITS 37158 
            document.all('cmbdisablitycat').selectedIndex = 0;
            document.all('cmbCoverageLossType').selectedIndex = 0;
            if (document.forms[0].disabilitytype_codelookup_cid != null)
                document.forms[0].disabilitytype_codelookup_cid.value = "";

            if (document.forms[0].disabilitytype_codelookup != null)
                document.forms[0].disabilitytype_codelookup.value = "";
            //mkaran2 - [JIRA] (RMA-4110) MITS 37158 -start
            if (document.forms[0].ctReserveSubType_codelookup_cid != null)
                document.forms[0].ctReserveSubType_codelookup_cid.value = "";

            if (document.forms[0].ctReserveSubType_codelookup != null)
                document.forms[0].ctReserveSubType_codelookup.value = "";
            //mkaran2 - [JIRA] (RMA-4110) MITS 37158 -end

            //rupal:r8 multicurrency
            var txtAmount = document.getElementById('txtAmount');
            txtAmount.value = "0.00";  //rupal: r8 multi currency
            var functionname = txtAmount.getAttributeNode("onblur").value;
            functionname = functionname.split('(')[0];
            window[functionname](txtAmount);
            var txtEditAmount = document.getElementById('txtEditAmount');
            txtEditAmount.value = "0.00";  //rupal:r8 multi currency
            window[functionname](txtEditAmount);
            //rupal:end

            document.all('cmbReasonAdd').selectedIndex = 0;
            document.all('cmbStatusAdd').selectedIndex = 0;
            document.all('txtReasonAdd').value = "";
            document.all('cmbReasonEdit').selectedIndex = 0;
            document.all('cmbStatusEdit').selectedIndex = 0;
            document.all('txtReasonEdit').value = "";
            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement 
            document.all('claimadjusterlookup').value = "";
            document.all('claimadjusterlookup_cid').value = 0;
            document.all('editclaimadjusterlookup').value = "";
            document.all('editclaimadjusterlookup_cid').value = 0;
            //Ankit End
            //MITS:34082 MultiCurrency START
            document.all('ReservecurrencytypetextEdit_codelookup_cid').value = 0;
            document.all('Reservecurrencytypetext_codelookup_cid').value = 0;
            document.getElementById('TabContainer').value = "";
            //MITS:34082 MultiCurrency END
            //rupal:jira 341
            var myTab = $get('TabContainer1').control;
            var iActiveTabIndex = myTab.get_activeTabIndex();
            var ErrorControl = document.getElementById('TabContainer1_TabPanel' + (iActiveTabIndex + 1) + "_ErrorCtl" + (iActiveTabIndex + 1) + "_lblError");
            if (ErrorControl != null)
                ErrorControl.innerHTML = "";
            //rupal:end, jira 341
        }

        function Validate() {
            var myTab = $get('TabContainer1').control;
            var iIndex = myTab.get_activeTabIndex() + 1;
            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
            var iPos = -1;
            var iPolCvgLossCode;
            iSelPolicyID = document.all('cmbPolicy').value;
            //rupal:start
            //iSelPolCvgID = document.all('cmbCoverageType').value;
            var arrCovgId = document.all('cmbCoverageType').value.split('#');
            iSelPolCvgID = arrCovgId[0];
            //rupal:end
            iSelReserveID = document.all('cmbReserveType').value;



            var LOB = document.getElementById("lob");
            if (LOB != null) {
                if (LOB.value == '243') {
                    iSelCvgLossID = trim(document.forms[0].disabilitytype_codelookup_cid.value);
                }
                else {
                    iSelCvgLossID = document.all('cmbCoverageLossType').value;
                }
            }


            for (var i = 0; i < gridElementsRadio.length; i++) {
                //var gridName = gridElementsRadio[i].name;            
                //var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                var iPos = i;
                var sID = "";
                if (iPos < 8)
                    sID = "0";


            iReserveID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveTypeCode_" + (iPos)).innerHTML;
            iPolicyID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolicyID_" + (iPos)).innerHTML;
            iPolCvgID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolCvgID_" + (iPos)).innerHTML;
            iPolCvgLossID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolCvgLossID_" + (iPos)).innerHTML;
            iPolCvgLossCode = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_LossTypeCode_" + (iPos)).innerHTML;
                //alert(iPolicyID + ' ' + iReserveID + ' ' + iPolCvgID);
                if (iSelPolicyID == iPolicyID && iSelCvgLossID == iPolCvgLossCode && iSelReserveID == iReserveID && iSelPolCvgID == iPolCvgID) {
                    //alert("This reserve already exists.");
                    alert(ReserveListingBOBValidations.ReserveExistsID);
                    return false;
                }

            }
            return true;
        }

        //bsharma33 RMA-3887 Reserve Supp starts
        //function SetReasonAdd() {
        //    document.getElementById('txtReasonAdd').value = (document.all('cmbReasonAdd').options[document.all('cmbReasonAdd').selectedIndex].innerHTML);
        //}


        //function SetReasonEdit() {
        //    document.getElementById('txtReasonEdit').value = (document.all('cmbReasonEdit').options[document.all('cmbReasonEdit').selectedIndex].innerHTML);
        //}

        // akaushik5 Added for MITS 33495 Starts
        //function RecalculateIncurred() {
        //    var iChangeInBalance = 0.0;
        //    if (document.getElementById('txtEditBalanceAmount') != null) {
        //        MultiCurrencyToDecimal_Form(document.getElementById('txtEditBalanceAmount'));
        //        //iChangeInBalance = parseFloat(document.getElementById('txtEditBalanceAmount').Amount) - parseFloat(iLoadReserveBalance);
        //        iChangeInBalance = parseFloat(document.getElementById('txtEditBalanceAmount').getAttribute('Amount')) - parseFloat(iLoadReserveBalance); //Jira id: 7636
        //        document.getElementById('txtEditAmount').value = parseFloat(iLoadReserveAmount) + parseFloat(iChangeInBalance);
        //        MultiCurrencyOnBlur_Form(document.getElementById('txtEditAmount'));
        //    }
        //}

        //function RecalculateBalance() {
        //    var iChangeInIncurred = 0.0;
        //    if (document.getElementById('txtEditAmount') != null) {
        //        MultiCurrencyToDecimal_Form(document.getElementById('txtEditAmount'));
        //        //iChangeInIncurred = parseFloat(document.getElementById('txtEditAmount').Amount) - parseFloat(iLoadReserveAmount);
        //        iChangeInIncurred = parseFloat(document.getElementById('txtEditAmount').getAttribute('Amount')) - parseFloat(iLoadReserveAmount); //Jira id: 7636
        //        document.getElementById('txtEditBalanceAmount').value = parseFloat(iLoadReserveBalance) + parseFloat(iChangeInIncurred);
        //        MultiCurrencyOnBlur_Form(document.getElementById('txtEditBalanceAmount'));
        //    }
        //}
        // akaushik5 Added for MITS 33495 Ends
        //bsharma33 RMA-3887 Reserve Supp ends
    </script>
        <script type="text/javascript">
           //bsharma33 RMA-3887 Reserve supplementals starts
            function CheckReserveSelection() {
                var myTab = $get('TabContainer1').control;
                //MITS:34082 START
                //var iIndex = (myTab.get_activeTabIndex() + 1);
                if (myTab == undefined) {
                    if (document.getElementById('TabContainer').value != "") {
                        var iIndex = document.getElementById('TabContainer').value;
                    }
                }
                else {
                    var iIndex = (myTab.get_activeTabIndex() + 1);
                }
                //var myTab = $get('TabContainer1').control;
                //var iIndex = (myTab.get_activeTabIndex() + 1);
                document.getElementById('TabContainer').value = iIndex;
                //MITS:34082 END
                document.getElementById('lblError1').innerHTML = "";

                var p_sGridName = "Grid" + (iIndex);
                var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
                var iPos = -1;
                var sAdjusterDetails;
                //35453 starts
                var sClmCurrBalAmount;
                var sClmCurrResAmount;
                var iReserveID = 0;
                //35453 ends 



                for (var i = 0; i < gridElementsRadio.length; i++) {
                    if (gridElementsRadio[i].checked) {
                        var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                    }
                }

                if (iPos != -1) {

                    iReserveID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveID_" + (iPos)).innerHTML;
                }

                return iReserveID;
            }


        function ShowDialog(mode)
        {
                var page = "";
                var popuptitle;
                var iClaimId = $('#claimid').attr('value');
                var iClaimantEID = document.getElementById('claimanteid').value;
                var bMultiCurrency = $('#hdUseMultiCurrency').attr('value');
                var claimcurrencytype = document.getElementById('currencytype').value;
                var claimcurrencyid = document.getElementById('txtReserveCurrID').value;

                if (bMultiCurrency == '') {
                    bMultiCurrency = "0";
                }
                var myTab = $get('TabContainer1').control;
                if (myTab == undefined) {
                    if (document.getElementById('TabContainer').value != "") {
                        var iIndex = document.getElementById('TabContainer').value;
                    }
                }
                else {
                    var iIndex = (myTab.get_activeTabIndex() + 1);
                }

                document.getElementById('TabContainer').value = iIndex;
                document.getElementById('lblError1').innerHTML = "";

                var p_sGridName = "Grid" + (iIndex);
                var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');
                var iPos = -1;
                var sAdjusterDetails;
                var sClmCurrBalAmount = 0;
                var sClmCurrResAmount = 0;
                var sStatus = "0", iRsvStatusParent = "", sDisabilityCat = "", sCvgLossType, iPolicyID = "0", sUnit = "";

                for (var i = 0; i < gridElementsRadio.length; i++) {
                    if (gridElementsRadio[i].checked) {
                        var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                    }
                }

                if (iPos != -1) {

                    var sID = "";
                    if (iPos < 8)
                        sID = "0";
                    if (mode == "1") {
                        sStatus = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Status_" + (iPos)).innerHTML
                        sDisabilityCat = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_disabilitycatdesc_" + (iPos)).innerHTML;
                        sCvgLossType = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_LossType_" + (iPos)).innerHTML;
                        iPolicyID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolicyID_" + (iPos)).innerHTML;
                        sUnit = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Unit_" + (iPos)).innerHTML;
                        iRsvStatusParent = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_RsvStatusParent_" + (iPos)).innerHTML;
                        if (sUnit != "")
                        {
                            sUnit = sUnit.replace("&amp;", " ^*^*^ ");
                        }
                }
            }
                var lob = $('#lob').attr('value');

                if (!ValidateClaimCurrency()) {
                    return false;
                }
                var iReserveID = CheckReserveSelection();
                if (iReserveID == 0 || mode == "0") {
                    iReserveID = 0;
                    if (mode == "1") {
                        alert(ReserveListingBOBValidations.SelectRowEditID);
                        return false;
                        }
                        popuptitle = "Add New Reserve";
                    }
                else {

                    popuptitle = "Edit Existing Reserve";
                }
                // var q1 = "?inp1=" + $("#Input1").val();
                //var q2 = "&inp2=" + $("#Input2").val();

                // url = url + q1 + q2; //url in the form 'DialogContentPage.aspx?inp1=xx&inp2=yy #MainContentDiv'
                var SysExternalParam = "";
                SysExternalParam = "<SysExternalParam><ClaimId>" + iClaimId + "</ClaimId><LOBQueryString>" + lob + "</LOBQueryString><ClaimantEID>" + iClaimantEID + "</ClaimantEID><PolicyID>" + iPolicyID + "</PolicyID><UnitName>" + sUnit + "</UnitName><ClaimCurrencyCode>"+ claimcurrencyid +"</ClaimCurrencyCode></SysExternalParam>";
                page = "/RiskmasterUI/UI/FDM/reservecurrent.aspx?SysExternalParam=" + SysExternalParam + "&claimId=" + iClaimId.toString() + "&recordID=" + iReserveID.toString() + "&multicurrencyonoff=" + bMultiCurrency.toString() + "&lob=" + lob.toString() + "&claimanteid=" + iClaimantEID.toString() + "&claimcurrencytype=" + claimcurrencytype + "&ClaimCurrencyCode=" + claimcurrencyid + "&status=" + iRsvStatusParent + "&discat=" + sDisabilityCat + "&cvglosstyp=" + sCvgLossType + "&policyid=" + iPolicyID + "&UnitName=" + sUnit;


                var $dialog = $('<div id="res"></div>')
                       .html('<iframe style="border: 0px;width:100%;height:100%;background:white;" src="' + page + '"></iframe>')
                       .dialog({
                           create: function () { $(".ui-dialog-titlebar-close").attr("title", "close") },
                           autoOpen: false,
                           modal: true,
                           height: 500,
                           width: 700,
                           title: popuptitle,
                           resizable: false,
                           draggable: false
                       });
                $dialog.dialog('open');
                return false;
            }

            function SetActiveTab(action) {
                var ctrl = $get('TabContainer1').control;
                // var i = ctrl.get_activeTabIndex();
                // ctrl.set_activeTab(ctrl.get_tabs()[tabNumber]);
                //var sUpdatePanelID = "TabContainer1_" + "TabPanel" + (i + 1) + "_TabUpdatePanel" + (i + 1);
                //__doPostBack(sUpdatePanelID, "");
                //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(stylinggrid);

                var b = ctrl.get_activeTab().get_headerText();
                var i = ctrl.get_activeTabIndex();
                var sUpdatePanelID = "TabContainer1_" + "TabPanel" + (i + 1) + "_TabUpdatePanel" + (i + 1);
                var sIsDataLoadedID = "TabContainer1_" + "TabPanel" + (i + 1) + "_IsDataLoaded" + (i + 1);
                var sClaimantEID = "TabContainer1_" + "TabPanel" + (i + 1) + "_ClaimantEID" + (i + 1);

                document.getElementById('TabContainer1_TabPanel' + (i + 1) + "_txtAction" + (i + 1)).value = action;
                document.getElementById('claimanteid').value = document.getElementById(sClaimantEID).value;
                document.getElementById('currencytype').value = document.getElementById('txtClaimCurrency').value;//MITS:34082
                //Deb MITS 31370
                //if (document.getElementById(sIsDataLoadedID).value != "1")
                document.forms[0].__EVENTTARGET.value = sUpdatePanelID;
                document.forms[0].__EVENTARGUMENT.value = ctrl.get_activeTab().get_headerText();
                document.forms[0].submit();

                // __doPostBack(sUpdatePanelID, ctrl.get_activeTab().get_headerText());
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(stylinggrid);
            }
          //bsharma33 RMA-3887 Reserve supplementals ends 
        function MoveFinancials() {

            var iUnitRowId;
            var lob = document.getElementById("lob").value;
            
            var myTab = $get('TabContainer1').control;
            var iIndex = myTab.get_activeTabIndex() + 1;
            var p_sGridName = "Grid" + (iIndex);
            var gridElementsRadio = document.getElementsByName(p_sGridName + '_ClaimantsGroup');

            var iPos = -1;

            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    var iPos = GetPositionForSelectedGridRowforRadioButton(p_sGridName + "_ClaimantsGroup", gridElementsRadio[i].value);
                }
            }

            if (iPos == -1) {

                alert(ReserveListingBOBValidations.SelectRowID);
                return false;
            }

            

            iReserveID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveID_" + (iPos)).innerHTML;
            iPolCvgID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolCvgID_" + (iPos)).innerHTML;
            iUnitRowId = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolicyUnitRowID_" + (iPos)).innerHTML;
            iPolicyID = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_PolicyID_" + (iPos)).innerHTML;
            iPolCvgLossCode = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_LossTypeCode_" + (iPos)).innerHTML;
            iReserveBalanceAmount = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Balance_" + (iPos)).innerHTML.replace(/[^A-Za-z 0-9 \.,\?""!@#\%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
            sCvgLoss = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_LossType_" + (iPos)).innerHTML;
            iReserveSubType = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveSubType_" + (iPos)).innerHTML;
            iIncurred = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_Incurred_" + (iPos)).innerHTML.replace(/[^A-Za-z 0-9 \.,\?""!@#\%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
            sReserve = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_ReserveType_" + (iPos)).innerHTML;
            //sDisabilityCat = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + p_sGridName + "_GridView1_disabilitycatdesc_" + (iPos)).innerHTML;
            window.location.href = "MoveFinancials.aspx?ClaimID=" + document.getElementById('claimid').value + "&ReserveID=" + iReserveID + "&CovgID=" + iPolCvgID + "&PolicyUnitRowId=" + iUnitRowId + "&PolicyId=" + iPolicyID + "&LOB=" + lob + "&LossCode=" + iPolCvgLossCode + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&ReserveBalanceAmount=" + iReserveBalanceAmount + "&ReserveSubType=" + iReserveSubType + "&Incurred=" + iIncurred + "&ReserveType=" + sReserve + "&Loss=" + sCvgLoss;
          
            return false;
        }
    </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();stylinggrid(); " >
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ToolkitScriptManager>

  <asp:TextBox style="display:none" runat="server" id="EditLoad" RMXType="hidden" Text="" /> <%--  //preeti, jira 341--%>

    <table width="99.5%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td>
                <asp:UpdatePanel id="captionupdatepanel" runat="server">
                    <ContentTemplate>
                <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="100%" Text="<%$ Resources:lblcaptionID %> "></asp:Label>    
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_addreserve" xmlns="" xmlns:cul="remove">
            <%-- bsharma33 RMA-3887 Reserve supplementals starts --%>
            <asp:imagebutton runat="server" src="../../Images/tb_addreserve_active.png" OnClientClick="return ShowDialog('0');"
                        width="28" height="28" border="0" id="AddReserve" alternatetext="<%$ Resources:ttAddReserve %>"  onmouseover="this.src='../../Images/tb_addreserve_mo.png';;this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_addreserve_active.png';;this.style.zoom='100%'" title="<%$ Resources:ttAddReserve %>" />
            <%-- bsharma33 RMA-3887 Reserve supplementals ends --%>
        </div>
        <div class="toolBarButton" runat="server" id="div_editreserve" xmlns="" xmlns:cul="remove">
            <%-- bsharma33 RMA-3887 Reserve supplementals starts --%>
<asp:imagebutton runat="server" OnClientClick = "return ShowDialog('1');"
                        src="../../Images/tb_editreserve_active.png" width="28" height="28" border="0" id="EditReserve" alternatetext="<%$ Resources:ttEditReserve %>"
                        onmouseover="this.src='../../Images/tb_editreserve_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_editreserve_active.png';this.style.zoom='100%'" title="<%$ Resources:ttEditReserve %>" />
            <%-- bsharma33 RMA-3887 Reserve supplementals ends --%>
        </div>
        <div class="toolBarButton" runat="server" id="div_makepayment" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server"  onclientclick='if(ValidateClaimCurrency()) return TransfertoPayment(); else return false; ' src="../../Images/tb_payment_active.png"
                        width="28" height="28" border="0" id="MakePayment" alternatetext="<%$ Resources:ttPayment %>" onmouseover="this.src='../../Images/tb_payment_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_payment_active.png';this.style.zoom='100%'" title="<%$ Resources:ttPayment %>"/></div>
                <div class="toolBarButton" runat="server" id="div_addcollection" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick='if(ValidateClaimCurrency()) return TransfertoCollection(); else return false;'  src="../../Images/tb_collection_active.png"
                        width="28" height="28" border="0" id="AddCollection" alternatetext="<%$ Resources:ttAddColl %>"
                        onmouseover="this.src='../../Images/tb_collection_mo.png';this.style.zoom='110%'" onmouseout="this.src='../../Images/tb_collection_active.png';this.style.zoom='100%'" title="<%$ Resources:ttAddColl %>"/></div>
         <div class="toolBarButton" runat="server" id="div_ManualDeductible" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick='if(ValidateClaimCurrency()) return TransfertoManualDeductible(); else return false;'  src="../../Images/tb_ManualDeductible.png"
                        width="28" height="28" border="0" id="AddManualDeductible" alternatetext="<%$ Resources:ttAddManDed %>"
                        onmouseover="this.src='../../Images/tb_ManualDeductible_mo.png';this.style.zoom='110%'" onmouseout="this.src='../../Images/tb_ManualDeductible.png';this.style.zoom='100%'" title="<%$ Resources:ttAddManDed %>"/></div>
                <div class="toolBarButton" runat="server" id="div_paymenthistory" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick= "return RMX.Reserves.AssignValue();" src="../../Images/tb_paymenthistory_active.png"
                        width="28" height="28" border="0" id="PaymentHistory" alternatetext="<%$ Resources:ttPayHist %>" onmouseover="this.src='../../Images/tb_paymenthistory_mo.png';;this.style.zoom='110%'"
                        onclick="PaymentHistory_Click"
                        onmouseout="this.src='../../Images/tb_paymenthistory_active.png';this.style.zoom='100%'" title="<%$ Resources:ttPayHist %>"/></div>
                <div class="toolBarButton" runat="server" id="div_schedulecheck" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick='if(ValidateClaimCurrency()) return TransfertoScheduleChecks(); else return false;'  src="../../Images/tb_schedulecheck_active.png"
                        width="28" height="28" border="0" id="ScheduleCheck" alternatetext="<%$ Resources:ttSchCheck %>" onmouseover="this.src='../../Images/tb_schedulecheck_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_schedulecheck_active.png';this.style.zoom='100%'" title="<%$ Resources:ttSchCheck %>"/></div>
                <div class="toolBarButton" runat="server" id="div_summary" xmlns="" xmlns:cul="remove">
                    <%--akaushik5 Changed for MITS 33577 Starts--%>
                    <%--<asp:imagebutton runat="server" onclientclick="return TransferToReserveSummary();"  src="../../Images/tb_finsummary_active.png"
                        width="28" height="28" border="0" id="Summary" alternatetext="<%$ Resources:ttSummary %>" onmouseover="this.src='../../Images/tb_finsummary_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_finsummary_active.png';this.style.zoom='100%'" />--%>
                    <asp:imagebutton runat="server" onclientclick="return TransferToReserveSummary('claimant');"  src="../../Images/tb_finsummary_active.png"
                        width="28" height="28" border="0" id="Summary" alternatetext="<%$ Resources:ttSummary %>" onmouseover="this.src='../../Images/tb_finsummary_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_finsummary_active.png';this.style.zoom='100%'" title="<%$ Resources:ttSummary %>" />
                        </div>
                    <%--akaushik5 Changed for MITS 33577 Ends--%>
                           <div class="toolBarButton" runat="server" id="div1" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick="return TransfertoReserveHistoryDetail();"  src="../../Images/tb_ofac_active.png"
                        width="28" height="28" border="0" id="ReserveHistory" alternatetext="<%$ Resources:ttResHist %>" onmouseover="this.src='../../Images/tb_ofac_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_ofac_active.png';this.style.zoom='100%'" 
                                   onclick="ReserveHistory_Click" title="<%$ Resources:ttResHist %>"/></div>
                    <%-- tanwar2 - mits 30910 - 01302013 - start --%>
                    <div class="toolBarButton" runat="server" id="div_deductible" xmlns="" xmlns:cul="remove">
                        <asp:imagebutton runat="server" src="../../Images/tb_deductiblesummary_active.png"
                            width="28" height="28" border="0" id="Deductible" alternatetext="<%$ Resources:ttDeductibleSummary %>" onmouseover="this.src='../../Images/tb_deductiblesummary_mo.png';this.style.zoom='110%'"
                            onmouseout="this.src='../../Images/tb_deductiblesummary_active.png';this.style.zoom='100%'" onclick="Deductible_Click" title="<%$ Resources:ttDeductibleSummary %>"/>
                    </div>
                     <div class="toolBarButton" runat="server" id="div_deductibleEvent" xmlns="" xmlns:cul="remove"> 
                        <asp:imagebutton runat="server" src="../../Images/tb_deductibleeventsummary_active.png"
                            width="28" height="28" border="0" id="DeductibleEvent" 
                             alternatetext="<%$ Resources:ttDeductibleEventSummary %>" onmouseover="this.src='../../Images/tb_deductibleeventsummary_mo.png';this.style.zoom='110%'"                            
                             onmouseout="this.src='../../Images/tb_deductibleeventsummary_active.png';this.style.zoom='100%'" 
                             onclick="DeductibleEvent_Click" title="<%$ Resources:ttDeductibleEventSummary %>"/>
                    </div>
                    <%-- tanwar2 - mits 30910 - 01302013 - end --%>
                                   <%--MITS 28450--%>
       <div class="toolBarButton" runat="server" id="div_enhancednotes" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick="return EnhancedNotes();" src="../../Images/tb_enhancednotes_active.png" width="28" height="28" border="0" id="enhancednotes" AlternateText="<%$ Resources:ttEnhNotes %>" onMouseOver="this.src='../../Images/tb_enhancednotes_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_enhancednotes_active.png';this.style.zoom='100%'" title="<%$ Resources:ttEnhNotes %>" />
        </div>
        <%--start - averma62--%>
        <div class="toolBarButton" runat="server" id="div_vssexport" xmlns="" xmlns:mc="remove">
          <asp:ImageButton runat="server" OnClientClick = "return ExportToVSS();" onclick="VssExport_Click" src="../../Images/tb_exporttoVSS_active.png" width="28" height="28" border="0" id="VssExport" AlternateText="<%$ Resources:ttvssexport %>" onMouseOver="this.src='../../Images/tb_exporttoVSS_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_exporttoVSS_active.png';this.style.zoom='100%'" title="<%$ Resources:ttvssexport %>"/>
        </div>
        <%--end - averma62--%>
         <%--Added by sharishkumar for Mits 35472--%>           
            <div class="toolBarButton" runat="server" id="div_lssexport" xmlns="" xmlns:mc="remove">
                <asp:imagebutton runat="server" onclientclick="return ExportToLSS();" onclick="LssExport_Click" src="../../Images/tb_LSS_active.png" width="28" height="28" border="0" id="LSSExport" visible="false" alternatetext="<%$ Resources:ttlssexport %>" onmouseover="this.src='../../Images/tb_LSS_mo.png';this.style.zoom='110%'" onmouseout="this.src='../../Images/tb_LSS_active.png';this.style.zoom='100%'" title="<%$ Resources:ttlssexport %>"/>
            </div>
            <%--End Mits 35472--%>
                <%--akaushik5 Added for MITS 33577 Starts--%>
                <div class="toolBarButton" runat="server" id="div_claimsummary" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick="return TransferToReserveSummary('claim');"  src="../../Images/tb_claimreservesummary_active.png" width="28" height="28" border="0" id="ClaimSummary" alternatetext="<%$ Resources:ttClmReserveSummary %>" onmouseover="this.src='../../Images/tb_claimreservesummary_mo.png';this.style.zoom='110%'" onmouseout="this.src='../../Images/tb_claimreservesummary_active.png';this.style.zoom='100%'"  title="<%$ Resources:ttClmReserveSummary %>"/>
                </div>
                <%--akaushik5 Added for MITS 33577 Ends--%>
            <%-- MITS 34275- RMA Swiss Re Financial Summary START --%>
            <div class="toolBarButton" runat="server" id="div_unitcoveragesummary" xmlns="" xmlns:cul="remove">
                <asp:ImageButton runat="server" OnClientClick="return TransferToUnitCoverageSummary('Policy');"
                    src="../../Images/tb_unitcoveragesummary_active.png" Width="28" Height="28" border="0"
                    ID="UnitCoveragefinancialSummary" AlternateText="<%$ Resources:ttUnitCoveragefinancialSummary %>" onmouseover="this.src='../../Images/tb_unitcoveragesummary_mo.png';this.style.zoom='110%'"
                    onmouseout="this.src='../../Images/tb_unitcoveragesummary_active.png';this.style.zoom='100%'" title="<%$ Resources:ttUnitCoveragefinancialSummary %>"/>
            </div>
            <%-- MITS 34275- RMA Swiss Re Financial Summary END --%>
                    <div class="toolBarButton" runat="server" id="div2" xmlns="" xmlns:cul="remove">
                <asp:ImageButton runat="server" OnClientClick="return TransferToFinancialDetailHistory('Claim');"
                    src="../../Images/tb_financialdetailhistory_active.png" Width="28" Height="28" border="0"
                    ID="FinancialDetailHistory" AlternateText="<%$ Resources:ttFDHforCarrier %>" onmouseover="this.src='../../Images/tb_financialdetailhistory_mo.png';this.style.zoom='110%'"
                    onmouseout="this.src='../../Images/tb_financialdetailhistory_active.png';this.style.zoom='100%'" title="<%$ Resources:ttFDHforCarrier %>"/>
            </div>
        <div class="toolBarButton" runat="server" id="div_MoveFinancials" xmlns="" xmlns:cul="remove">
                <asp:ImageButton runat="server" OnClientClick="return MoveFinancials();"
                    src="../../Images/tb_MovetoFinancial_active.png" Width="28" Height="28" border="0"
                    ID="MoveFinancials" AlternateText="<%$ Resources:ttMoveFinancials %>" onmouseover="this.src='../../Images/tb_MovetoFinancial_mo.png';this.style.zoom='110%'"
                    onmouseout="this.src='../../Images/tb_MovetoFinancial_active.png';this.style.zoom='100%'" title="<%$ Resources:ttMoveFinancials %>"/>
            </div>

    </div>
         <div style="height:2px"></div>
    <asp:TabContainer ID="TabContainer1" OnClientActiveTabChanged="ActiveTabChanged" runat="server" >
                <asp:TabPanel ID="TabPanel1"  runat="server">
                <HeaderTemplate>Tab 2</HeaderTemplate>
                <ContentTemplate>Here is some</ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>
   
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimID" id="claimid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/Reserves/@claimnumber" ID="claimnumber" runat="server" rmxignoreset="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimantRowID" ID="claimantrowid" runat="server" rmxignoreget="true"></asp:TextBox>    
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimantEID" ID="claimanteid" runat="server" rmxignoreget="true"></asp:TextBox>    
    
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/SubTitle" ID="subtitle" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" ID="policyid" runat="server" ></asp:TextBox>    
    <asp:TextBox style="display:none" ID="polcvgid" runat="server" ></asp:TextBox>    
    <%--  MITS 34275- RMA Swiss Re Financial Summary--%>
    <asp:TextBox Style="display: none" ID="GridUnitId" runat="server"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="txtUseMulticurrency" runat="server"></asp:TextBox>
    <%--  MITS 34275- RMA Swiss Re Financial Summary--%>
        <asp:TextBox style="display:none" ID="cvglossid" runat="server" ></asp:TextBox>  
    <asp:TextBox style="display:none" ID="Reserve" runat="server" ></asp:TextBox>       
    <asp:TextBox style="display:none" ID="rc_row_id" runat="server" ></asp:TextBox>       
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/LOB" ID="lob" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" id="caption" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@caption"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/Reserves/@paymentfrozenflag" ID="frozenflag" runat="server" rmxignoreset="true"></asp:TextBox>  
    <asp:TextBox style="display:none" rmxref="/Instance/Document/Reserves/@preventcollonlob" ID="collonlob" runat="server" rmxignoreset="true"></asp:TextBox> <%--rma-857--%>
    <asp:TextBox style="display:none" ID="PreventCollOnRes" runat="server"></asp:TextBox> <%--rma-857--%>
        <asp:TextBox style="display:none" ID="ResType" runat="server"></asp:TextBox> <%--rma-857--%>
        <asp:TextBox style="display:none" ID="MasResType" runat="server"></asp:TextBox> <%--rma-857--%>
        <asp:TextBox style="display:none"  id="PrevResModifyzero" runat="server"></asp:TextBox> <%--igupta3 JIRA RMA-348 --%>
    <asp:TextBox style="display:none" id="claimstatus" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@claimstatus"></asp:TextBox>
    <asp:TextBox style="display:none" id="ScheduleCheckViewPermissionOnly" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@viewonly"></asp:TextBox>
    <asp:TextBox style="display:none" id="hIsClaimCurrencySet" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@IsClaimCurrencySet"></asp:TextBox>
     <%--Mgaba2: MITS 28450 --%>
   <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXType="hidden" Text="ReserveListingBOB" />  
   <%--Mgaba2: MITS 28450 --%>
        <asp:TextBox runat="server" ID="currencytype" Text="" Style="display: none" rmxref="/Instance/Claim/CurrencyType" rmxignoreset="true" /><%--MITS:34082--%>
        <asp:TextBox runat="server" ID="txtClaimCurrency" Text="" Style="display: none" rmxref="/Instance/Claim/CurrencyType" rmxignoreset="true" /><%--MITS:34082--%>
        <asp:TextBox runat="server" ID="TabContainer" Text="" Style="display: none" /><%--MITS:34082--%>
        <asp:TextBox runat="server" ID="txtAddEditPopup" Text="" Style="display: none" /><%--MITS:34082--%>
        <asp:TextBox runat="server" ID ="txtReserveCurrID" Text="" style="display:none" rmxref="/Instance/Document/Reserves/CurrencyTypeID" rmxignoreset="true" /><%--MITS:34082--%>
        <asp:TextBox Style="display: none" ID="txtClaimCurrenyType" runat="server"></asp:TextBox><%--MITS:34082--%>
    <asp:TextBox style="display:none" ID="txtClaimants" runat="server"></asp:TextBox>   
    <asp:TextBox style="display:none" ID="txtClaimantEIDs" runat="server"></asp:TextBox>
    <asp:TextBox style="display:none" ID="txtClaimantRowIds" runat="server"></asp:TextBox><%--JIRA 10347--%>
        <asp:TextBox style="display:none" ID="txtDisabilityCode" runat="server"></asp:TextBox>
        <%--akaushik5 Added for MITS 33577 Starts--%>
        <asp:TextBox style="display:none" ID="txtReserveSummaryLevel" runat="server"></asp:TextBox>
        <%--akaushik5 Added for MITS 33577 Ends--%>
         <%--  mkaran2 - [JIRA] (RMA-4110) MITS 37158 Starts --%>
        <asp:TextBox style="display:none" ID="txtReserveSubTypeCode" runat="server"></asp:TextBox>
        <%--  mkaran2 - [JIRA] (RMA-4110) MITS 37158 Ends --%>
     
    <%--WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298 - Start--%>
	<asp:HiddenField ID="hdnEnableFirstAndFinalPayment" runat="server" Value="false" />
	<%--WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298 - End--%> 

    <%-- tanwar2 - mits 30910 - 01302013 - start --%>
    <asp:TextBox style="display:none" id="txtApplyDedToPayments" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@ApplyDedToPayments"></asp:TextBox>
    <asp:TextBox style="display:none" id="txtDedRecoveryReserveCode" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@DedRecoveryReserveCode"></asp:TextBox>
    <%-- tanwar2 - mits 30910 - 01302013 - end --%>
        <asp:TextBox style="display:none" runat="server"  ID="txtNotDetDedVal" rmxref="/Instance/Document/Reserves/@NotDetDedVal"></asp:TextBox>      
<asp:TextBox  runat="server" ID="txtIsDeductible" style="display:none"></asp:TextBox>
<asp:TextBox style="display:none" runat="server"  ID="txtFPDedVal" rmxref="/Instance/Document/Reserves/@FPDedVal"></asp:TextBox>
        <asp:TextBox style="display:none" runat="server"  ID="txtNoneDedVal" rmxref="/Instance/Document/Reserves/@NoneDedVal"></asp:TextBox>
    <div align="right">
     <table>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblCurrencytype" Text="<%$ Resources:lblCurrencytype %>" Font-Bold="True"></asp:Label>
            </td>
            <td>
                <%--MITS 35453 --%>
                 <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <ContentTemplate>
            <asp:DropDownList runat="server"  ID="drdCurrencytype" OnSelectedIndexChanged="drdCurrencytype_onchange" AutoPostBack="true"  rmxref="/Instance/Document/ReserveFunds/CurrencyType" itemsetref="/Instance/Document/Reserves/CurrencyTypeList" rmxtype="combobox"  ></asp:DropDownList>
                        </ContentTemplate></asp:UpdatePanel>
            </td>
        </tr>
    </table>    
    </div>  
    <!--rupal:end-->
    <!--Rupal:Start For First & Final Payment-->    
    <asp:TextBox style="display:none"  ID="IsFirstFinal" runat="server" />
   <asp:TextBox style="display:none" ID="IsFirstFinalReadOnly" runat="server" />
   <asp:TextBox style="display:none" ID="CovgSeqNum" runat="server" />
   <asp:TextBox style="display:none" ID="ResStatusParent" runat="server" />
      <asp:TextBox style="display:none" ID="TransSeqNum" runat="server" />
    <asp:TextBox style="display:none" ID="CoverageKey" runat="server" />        <!--Ankit Start : Worked on MITS - 34297-->
    <!--Rupal:End For First & Final Payment-->
    <%--    <asp:TextBox style="display:none" ID="txtaction" Text="" runat="server"></asp:TextBox>--%>
    <asp:TextBox style="display:none" ID="txtCancelledShortCode" runat="server" />        <!--aaggarwal29 : RMA-9856-->
        <asp:TextBox style="display:none" ID="txtClosedShortCode" runat="server" />       <!--aaggarwal29 : RMA-9856-->

    <asp:Panel ID="Panel1" runat="server" Style="display: none" CssClass="modalPopup" BackColor="White" >            
   <asp:UpdatePanel ID="upnlAddMore" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
                    <ContentTemplate>  
            <asp:Panel ID="Panel3" runat="server" Style="cursor: move;" >

                <div style="text-align:center" class="colheader3">
                    <p ><b>
                        <asp:Label ID="lblAddNewReserveID" runat="server" Text="<%$ Resources:lblAddNewReserveID %>"></asp:Label></b></p>
                </div>
            </asp:Panel>
                <div>
                <table width="99%">
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" Text="" BackColor="White" ForeColor="Red" Visible="true" ID="lblError1"></asp:Label></td>

                    </tr>
                    <%--//pgupta93: MITS:34082 START--%>
                    <tr runat="server" id="trReserveCurrType">
                        <td>
                            <%--    <asp:Label Text="<%$ Resources:lblCurrencytype %>" ID="Label1" runat="server"></asp:Label>
                                           </td>
                                            <td>
                                            <asp:Label ID="lcurrencytype" runat="server" rmxignoreset="true" ></asp:Label>
                                            <asp:TextBox runat="server" ID ="currencytype" Text="" style="display:none" rmxref="/Instance/Document/Reserves/CurrencyTypeForClaim" rmxignoreset="true" />
                                            <!--rupal:end-->--%>
                            <asp:Label runat="server" class="label" ID="lbl_currencytypetext" Text="Currency Type" />
                        </td>
                        <td>
                            <span class="formw">
                                <usc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9; " ID="Reservecurrencytypetext"
                                    CodeTable="CURRENCY_TYPE"  ControlName="Reservecurrencytypetext" RMXRef="/Instance/Document/Reserves/CurrencyTypeForClaim"
                                    CodeFilter="" RMXType="code"  />
                            </span>

                        </td>
                    </tr>
                    <%-- //pgupta93: MITS:34082 END--%>
                            <tr>
                            <td>
                                <asp:Label ID="lblPolicyNameID" runat="server" Text="<%$ Resources:lblPolicyNameID %>"></asp:Label></td>
                            <td>
                            <asp:DropDownList runat="server" ID="cmbPolicy" onchange="ClearErrMsg();dropDownChangeflag = false;">
                            </asp:DropDownList>
                            </td>
                            </tr>
                            <tr>
                            <td>
                                <asp:Label ID="lblUnitID" runat="server" Text="<%$ Resources:lblUnitID %>"></asp:Label></td>
                            <td>
                        <asp:DropDownList runat="server" ID="cmbUnit" onchange="ClearErrMsg();dropDownChangeflag = false;" Width="200 px" >
                            </asp:DropDownList>
                            </td>
                            </tr>
                            <tr>
							<td><asp:Label ID="lblCoverageTypeID" runat="server" Text="<%$ Resources:lblCoverageTypeID %>"></asp:Label></td>
							
                            <td>
                            <asp:DropDownList runat="server" ID="cmbCoverageType" onchange="ClearErrMsg();dropDownChangeflag = false;" Width="250 px">
                            </asp:DropDownList>
                            </td>
							</tr>
							<tr>
                            <td> <asp:Label Text="<%$ Resources:lblDisabilityCat %>" ID="lbldisabltycat" runat="server"></asp:Label></td>
                            <td>
                            <asp:DropDownList runat="server" ID="cmbdisablitycat" onchange="ClearErrMsg();ClearData('disabilitytype_codelookup');">
                            </asp:DropDownList>
                            </td>
                            </tr>
							
							
                            <tr>
							
							
                           <td> <asp:Label Text="<%$ Resources:lblDisabilityLossType %>" ID="lbldistype" runat="server"></asp:Label></td>
                          <%--  <td>
                            <asp:DropDownList runat="server" ID="cmbdisabilityCode" onchange="ClearErrMsg();">
                            </asp:DropDownList>
                            </td>--%>
                             
                             <td>
                              <usc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="disabilitytype" CodeTable="DISABILITY_TYPE" ControlName="disabilitytype" RMXRef="" Filter="CODES.RELATED_CODE_ID IN(DisabilityCat,0)" RMXType="code" /> <%--// dbisht6 mits 35554--%>
                              </td>
                           
                            </tr>
                            <tr>
                            <td> <asp:Label Text="<%$ Resources:lblCoverageLossType %>" ID="lblLossType" runat="server"></asp:Label></td>
                            <td>
                            <asp:DropDownList runat="server" ID="cmbCoverageLossType" onchange="ClearErrMsg();">
                            </asp:DropDownList>
                            </td>
                            </tr>
                             <tr>
                             <td> <asp:Label ID="lblReserveTypeID" runat="server" Text="<%$ Resources:lblReserveTypeID %>"></asp:Label></td>
							<td>
                           <%-- <asp:DropDownList runat="server" ID="cmbReserveType"  onchange="ClearErrMsg();">
                            </asp:DropDownList>--%>
                                <%-- mkaran2 - [JIRA] (RMA-4110) MITS 37158--%>
                            <asp:DropDownList runat="server" ID="cmbReserveType"  onchange="ClearErrMsg();ClearData('ctReserveSubType_codelookup');">
                            </asp:DropDownList>
                            </td>
                            </tr>                            
                            <%-- Ankit Start : Financial Enhancement - Reserve Sub Type Changes--%>
                            <tr>
                                <td>
                                    <asp:Label ID="lblReserveSubTypeID" runat="server" Text="<%$ Resources:lblReserveSubTypeID %>"></asp:Label></td>
                               <%-- <td>
                                    <asp:DropDownList ID="cmbReserveCategoryAdd" runat="server" 
                                        onchange="ClearErrMsg();">
                                    </asp:DropDownList>
                                </td>--%>

                                <%-- mkaran2 - [JIRA] (RMA-4110) MITS 37158--%>
                              <td>
                                    <usc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="ctReserveSubType" CodeTable="RESERVE_SUB_TYPE" ControlName="ctReserveSubType" RMXRef="" Filter="CODES.RELATED_CODE_ID IN(ReserveSubType,0)" RMXType="code" />
                                </td>
                            </tr>
                            <%-- Ankit End--%>
                            <%-- Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement--%>
                            <tr>
                                <td>
                                    <asp:Label ID="lbl_claimadjusterlookup" runat="server" Text="<%$ Resources:lblAssignedAdjusterID %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="claimadjusterlookup" />
                                    <asp:button runat="server" class="EllipsisControl" id="claimadjusterlookupbtn" onclientclick="return lookupData('claimadjusterlookup','ADJUSTERS',7,'claimadjusterlookup',2);" />
                                    <asp:TextBox style="display:none" runat="server" id="claimadjusterlookup_cid" />
                                </td>
                            </tr>
                            <%-- Ankit End--%>
                    <tr>
                        <td>
                            <asp:Label ID="lblAmountID" runat="server" Text="<%$ Resources:lblAmountID %>"></asp:Label></td>
                        <td>
                            <!--rupal, r8 multicurrency-->
                            <%--//pgupta93: MITS:34082 START--%>
                            <%--  <mc:CurrencyTextbox runat="server" ID="txtAmount" />--%>
                            <mc:CurrencyTextbox runat="server" ID="txtAmount" />
                            <%--//pgupta93: MITS:34082 END--%>
                            </td>
                            </tr>
                    <%--//pgupta93: MITS:34082 START--%>
                    <%-- <tr>
                        <td>
                            <asp:Label Text="<%$ Resources:lblCurrencytype %>" ID="Label1" runat="server"></asp:Label>
                            </td>
                            <td>
                            <asp:Label ID="lcurrencytype" runat="server" rmxignoreset="true" ></asp:Label>
                            <asp:TextBox runat="server" ID ="currencytype" Text="" style="display:none" rmxref="/Instance/Document/Reserves/CurrencyTypeForClaim" rmxignoreset="true" />
                            <!--rupal:end-->
                        </td>
                    </tr>--%>
                    <%--//pgupta93: MITS:34082 END--%>
                            <tr>
                            <td>
                                <asp:Label ID="lblStatusID" runat="server" Text="<%$ Resources:lblStatusID %>"></asp:Label></td>
                            <td>
                            <asp:DropDownList runat="server" ID="cmbStatusAdd"></asp:DropDownList>                            
                            </td>
                            </tr>
                            <tr>
                            <td>
                                <asp:Label ID="lblReasonID" runat="server" Text="<%$ Resources:lblReasonID %>"></asp:Label></td>
                            <td>
                            <asp:TextBox runat="server" ID="txtReasonAdd"></asp:TextBox>
                            </td>
                            </tr>
                            <tr>
                            <td></td>
                            <td>
                            <asp:DropDownList runat="server" ID="cmbReasonAdd" onchange="SetReasonAdd();"></asp:DropDownList>                            
                            </td>
                            </tr>
                            </table>
                            </div>
                  
            <asp:CascadingDropDown ID="CascadingDropDown1" runat="server" TargetControlID="cmbPolicy"
            Category="policy"  PromptText="<%$ Resources:PrompttextSelectPolicy %>" LoadingText="<%$ Resources:LoadingtextSelectPolicy %>"
            ServiceMethod="GetDropDownContentsPageMethod"  BehaviorID="CascadingDropDown1"/>
            <asp:CascadingDropDown ID="CascadingDropDown2" runat="server" TargetControlID="cmbUnit"
            Category="unit"  PromptText="<%$ Resources:PrompttextSelectUnit %>"  LoadingText="<%$ Resources:LoadingtextSelectUnit %>"
            ServiceMethod="GetDropDownContentsPageMethod" ParentControlID="cmbPolicy" BehaviorID="CascadingDropDown2"/>
            <asp:CascadingDropDown ID="CascadingDropDown3" runat="server" TargetControlID="cmbCoverageType"
            Category="coverage" PromptText="<%$ Resources:PrompttextSelectCoverage %>" LoadingText="<%$ Resources:LoadingtextSelectCoverage %>"
            ServiceMethod="GetDropDownContentsPageMethod" ParentControlID="cmbUnit" BehaviorID="CascadingDropDown3" />
             <asp:CascadingDropDown ID="CascadingDropDown4" runat="server" TargetControlID="cmbCoverageLossType"
            Category="loss" PromptText="<%$ Resources:PrompttextSelectLossType %>" LoadingText="<%$ Resources:LoadingtextSelectLossType %>"
            ServiceMethod="GetDropDownContentsPageMethod" ParentControlID="cmbCoverageType" BehaviorID="CascadingDropDown4"/>
            <asp:CascadingDropDown ID="CascadingDropDown6" runat="server" TargetControlID="cmbReserveType"
            Category="reserve" PromptText="<%$ Resources:PrompttextSelectReserve %>" LoadingText="<%$ Resources:LoadingtextSelectReserve %>"
            ServiceMethod="GetDropDownContentsPageMethod"
            ParentControlID="cmbCoverageType" BehaviorID="CascadingDropDown5"/>
             <asp:CascadingDropDown ID="CascadingDropDown5" runat="server" TargetControlID="cmbdisablitycat"
            Category="disabilitycat" PromptText="<%$ Resources:PrompttextSelectDisabilityCategory %>" LoadingText="<%$ Resources:LoadingtextSelectDisabilityCategory %>"
            ServiceMethod="GetDropDownContentsPageMethod"
            ParentControlID="cmbCoverageType" BehaviorID="CascadingDropDown6"/>
            <%--mkaran2 - [JIRA] (RMA-4110) MITS 37158  --%>
            <%--<asp:CascadingDropDown ID="CascadingDropDown7" runat="server" TargetControlID="cmbReserveCategoryAdd"
            Category="reservesubtype" PromptText="<%$ Resources:PrompttextSelectReserveSubType %>" LoadingText="<%$ Resources:LoadingtextSelectReserveSubType %>"
            ServiceMethod="GetDropDownContentsPageMethod"
            ParentControlID="cmbReserveType" BehaviorID="CascadingDropDown7" />--%> 
         <p style="text-align: center;">
             <%-- bsharma33 RMA-3887 Reserve supplementals starts --%>
                       <%-- <asp:Button ID="AddOk" runat="server" Text="<%$ Resources:btnAddOk %>"  OnClientClick="return onOkAdd();"/>
                        <asp:Button ID="Ok" runat="server" style="display:none" Text="<%$ Resources:btnOk %>"/>
                        <asp:Button ID="Cancel" runat="server" Text="<%$ Resources:btnCancel %>" />
                        <asp:TextBox style="display:none" ID="txtActionAddMore" Text="" runat="server"></asp:TextBox>              
                        <asp:Button ID="AddMore" runat="server" Text="Add More" onClientClick="return onAddMore();" onclick="AddMore_Click"/>   --%>
             <%-- bsharma33 RMA-3887 Reserve supplementals ends --%>
                        </ContentTemplate>
        <Triggers> 
      <%-- bsharma33 RMA-3887 Reserve supplementals starts --%>
      <%--<asp:PostBackTrigger ControlID="AddOk" /> 
            <asp:PostBackTrigger ControlID="Cancel" /> --%>
      <%-- bsharma33 RMA-3887 Reserve supplementals ends --%>
   </Triggers>
					</asp:UpdatePanel>        
                    </p>    
        </asp:Panel>                               
                

        <asp:Panel ID="Panel2" runat="server" Style="display: none" CssClass="modalPopup"  BackColor="White">
        <asp:Panel ID="Panel4" runat="server" Style="cursor: move;">
            <%--<asp:Panel ID="Panel4" runat="server" Style="cursor: move;background-color:Olive;border:solid 1px Gray;color:Black">--%>
    
                    <div style="text-align:center" class="colheader3">
                    <p ><b>
                        <asp:Label ID="lblEditExistingReserveID" runat="server" Text="<%$ Resources:lblEditExistingReserveID %>"></asp:Label></b></p>
                    </div>                    
    
            </asp:Panel>
                <div>
                    <table width="99%">                        
                            <tr>
                            <td colspan="2"><asp:Label runat="server" Text="" BackColor="White" ForeColor="Red" Visible="true" ID="lblError2"></asp:Label></td>                                                        
                            </tr>
                            <tr>
                            <td>
                                <asp:Label ID="lblPolNameID" runat="server" Text="<%$ Resources:lblPolicyNameID %>"></asp:Label>:</td>
                            <td>
                            <asp:Label runat="server" ID="lblPolicy" ForeColor="Black" BackColor="White" Visible="true" >
                            </asp:Label>                            
                            <asp:TextBox style="display:none" ID="txtPolicyID" Text="" runat="server"></asp:TextBox>
                            
                            </td>
                            </tr>
                            <tr>
                            <td>
                                <asp:Label ID="lblUntID" runat="server" Text="<%$ Resources:lblUnitID %>"></asp:Label>:</td>
                            <td>
                            <%--MITS 32517 start: Using textbox instead of label to show Unit information in Edit mode . Label has been set to disply:none. Label was not able to wrap long text such as 300 char into single line --%>
                            <asp:Label runat="server" ID="lblUnit" ForeColor="Black" BackColor="White" style="display:none" >
                            </asp:Label>   
                            <asp:TextBox runat="server" ID="txtUnitText" ReadOnly="true" ForeColor="Black" BackColor="White" Width="250px" BorderWidth="0"></asp:TextBox>                
                            <%--MITS 32517 end:  --%>
                            <asp:TextBox style="display:none" ID="txtUnitID" Text="" runat="server"></asp:TextBox>
                            </td>
                            </tr>
                            <tr>
                            <td>
                                <asp:Label ID="lblCovrgeTypeID" runat="server" Text="<%$ Resources:lblCoverageTypeID %>"></asp:Label>:</td>
                            <td>
                            <asp:Label runat="server" ID="lblCoverageType" ForeColor="Black" BackColor="White">
                            </asp:Label>                            
                            <asp:TextBox style="display:none" ID="txtCoverageType" Text="" runat="server"></asp:TextBox>   
                            <asp:TextBox style="display:none" ID="txtCovgSeqNum" Text="" runat="server"></asp:TextBox>                                                        
                            <asp:TextBox style="display:none" ID="txtTransSeqNum" Text="" runat="server"></asp:TextBox>                                                        
                            <asp:TextBox style="display:none" ID="txtCoverageKey" Text="" runat="server"></asp:TextBox>     <!--Ankit Start : Worked on MITS - 34297-->
                            </td>

                          </tr>
                            
                                  <tr>
                            <td> <asp:Label runat="server" Text="<%$ Resources:lblDisabilityCat %>" ID="lblDisabilityCat" ForeColor="Black" BackColor="White">
                            </asp:Label>     </td>
                           <td>
                                <asp:Label runat="server"  ID="txtDisabilityCat" ForeColor="Black" BackColor="White" >
                            </asp:Label>                      
                               </td>                 
                            
                            </tr>
                            <tr>
                            <td> <asp:Label runat="server" Text="<%$ Resources:lblCoverageLossType %>" ID="lblCoverageLossType" ForeColor="Black" BackColor="White">
                            </asp:Label>     </td>
                           <td>
                                <asp:Label runat="server"  ID="txtCoverageLossType" ForeColor="Black" BackColor="White">
                            </asp:Label>                      
                               </td>                 
                            
                            </tr>
                            <tr>
                            <td> <asp:Label runat="server" Text="<%$ Resources:lblDisabilityLossType %>" ID="lblDisabilityLossType" ForeColor="Black" BackColor="White">
                            </asp:Label>     </td>
                           <td>
                                <asp:Label runat="server"  ID="txtDisabilityLossType" ForeColor="Black" BackColor="White">
                            </asp:Label>                      
                               </td>                 
                            
                            </tr>
                            <tr>
							    <td>
                                    <asp:Label ID="lblRsrveTypeID" runat="server" Text="<%$ Resources:lblReserveTypeID %>"></asp:Label>:
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblReserveType"></asp:Label>                            
                                    <asp:TextBox style="display:none" ID="txtReserveID" Text="" runat="server"></asp:TextBox>
                                    <asp:TextBox style="display:none" ID="txtReserveTypeCode" Text="" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <%-- Ankit Start : Financial Enhancement - Reserve Sub Type Changes--%>
                            <tr>
							    <td>
                                    <asp:Label ID="lblRsrvSubTypeID" runat="server" Text="<%$ Resources:lblReserveSubTypeID %>"></asp:Label>:
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblReserveSubType"></asp:Label>
                        </td>
                    </tr>
                    <%-- //pgupta93: MITS:34082 START--%>
                    <tr runat="server" id="trReservecurrencytypeEdit">
                        <td>

                            <asp:Label Text="<%$ Resources:lblCurrencytype %>" ID="lblCurrencyTypeEdit" runat="server"
                                rmxignoreset="true"></asp:Label>
                        </td>
                        <td>
                            <span class="formw">
                                <usc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  "
                                    ID="ReservecurrencytypetextEdit" CodeTable="CURRENCY_TYPE" ControlName="ReservecurrencytypetextEdit"
                                    RMXRef="/Instance/Document/Reserves/CurrencyTypeForClaim" CodeFilter="" RMXType="code" />
                            </span>
                        </td>
                        <%-- <td>
                            <asp:Label Text="<%$ Resources:lblCurrencytype %>" ID="Label2" runat="server"></asp:Label>
                            </td>
                            <td>
                            <asp:Label ID="lcurrencytypeedit" runat="server" rmxignoreset="true" ></asp:Label>                            
                            <asp:Label ID="lcurrencytype" runat="server" rmxignoreset="true" ></asp:Label>
                            <asp:TextBox runat="server" ID ="currencytype" Text="" style="display:none" rmxref="/Instance/Document/Reserves/CurrencyTypeForClaim" rmxignoreset="true" />
                            <!--rupal:end-->
                    </td>--%>
                    </tr>
                    <%-- //pgupta93: MITS:34082 END--%>
                            <%-- Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement--%>
                            <tr>
                                <td>
                                    <asp:Label ID="lbl_editclaimadjusterlookup" runat="server" Text="<%$ Resources:lblAssignedAdjusterID %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="editclaimadjusterlookup" />
                                    <asp:button runat="server" class="EllipsisControl" id="editclaimadjusterlookupbtn" onclientclick="return lookupData('editclaimadjusterlookup','ADJUSTERS',7,'editclaimadjusterlookup',2);" />
                                    <asp:TextBox style="display:none" runat="server" id="editclaimadjusterlookup_cid" />
                                </td>
                            </tr>
                            <%-- Ankit End--%>
                            <tr>
							    <td>
                                    <asp:Label ID="lblReserveBalance" runat="server" Text="<%$ Resources:lblReserveBalance %>"></asp:Label>:
                                </td>
                                <td>
                                    <%--akaushik5 Changed for MITS 33495 Starts--%>
                                    <%--<asp:Label runat="server" ID="lblReserveBalanceAmount"></asp:Label>--%>
                                    <mc:CurrencyTextbox runat="server" id="txtEditBalanceAmount" OnBlur="RecalculateIncurred()"/>
                                    <%--akaushik5 Changed for MITS 33495 Ends--%>
                                    <input id="hdUseMultiCurrency" name="" runat="server" type="hidden" />
                                    <%--akaushik5 added for MITS 38130 Starts--%>
                                    <asp:Label runat="server" ID="lblReserveBalanceAmount"></asp:Label>
                                    <%--akaushik5 added for MITS 38130 Ends--%>
                                </td>
                            </tr>
                            <%-- Ankit End--%>
                            <tr>
                                <td>
                                    <td>
                                        <%--mgaba2:R8:Supervisory Approval--%>
                                        <asp:Label ID="lblreasonedit" runat="server" BackColor="White" 
                                            ForeColor="Black" style="display:none"> </asp:Label>
                                    </td>
                                </td>
                            </tr>

                             <tr>
							<td>
							<asp:Label ID="lblAmntID" runat="server" Text="<%$ Resources:lblAmountID %>"></asp:Label></td>
                                <td>
                                    <!--rupal, r8 multicurrency-->                            
                                    <%--akaushik5 Changed for MITS 33495 Starts--%>
                                    <%--<mc:CurrencyTextbox runat="server" id="txtEditAmount"/>--%>
                                    <mc:CurrencyTextbox runat="server" id="txtEditAmount" OnBlur="RecalculateBalance()"/>
                                    <%--akaushik5 Changed for MITS 33495 Ends--%>
                                    <%--akaushik5 added for MITS 38130 Starts--%>
                                    <asp:Label runat="server" ID="lblReserveAmount"></asp:Label>
                                    <%--akaushik5 added for MITS 38130 Ends--%>
                                </td>
                            </tr>
                    <%-- MITS:34082 START--%>
                    <%-- <tr>
                        <td>
                            <asp:Label Text="<%$ Resources:lblCurrencytype %>" ID="Label2" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lcurrencytypeedit" runat="server" rmxignoreset="true"></asp:Label>
                            <!--rupal:end-->
                        </td>
                    </tr>--%>
                    <%-- MITS:34082 END--%>
                            <tr>
                            <td>
                                <asp:Label ID="lblStatID" runat="server" Text="<%$ Resources:lblStatusID %>"></asp:Label></td>
                            <td>
                            <asp:DropDownList runat="server" ID="cmbStatusEdit"></asp:DropDownList>
                            <!--Ankit Start : Financial Enhancement - Status Code Changes-->
                            <input id="hdDeletedVal" name="" type="hidden" />
                            <!--Ankit End-->
                                <input id="hdncount" name="" runat="server" type="hidden" /><input id="hdLSSReserve" name="" runat="server" type="hidden" /><%--added by sharishkumar for Mits 35472--%>                               
                              <%--mgaba2:R8:Supervisory Approval--%>
                            <asp:Label runat="server" ID="lblStatusEdit" ForeColor="Black" BackColor="White" style="display:none" > </asp:Label>                            
                            </td>
                            </tr>
                            <tr>
                            <td>
                                <asp:Label ID="lblRsnID" runat="server" Text="<%$ Resources:lblReasonID %>"></asp:Label></td>
                            <td>
                            <asp:TextBox runat="server" ID="txtReasonEdit"></asp:TextBox>
                            </td>
                            </tr>
                            <tr>
                            <td></td>
                            <td>
                            <asp:DropDownList runat="server" ID="cmbReasonEdit" onchange="SetReasonEdit();"></asp:DropDownList>                            
                            </td>
                            </tr>
                            <%--skhare7--%>
                             <tr>
                            <td >  <asp:Label runat="server" ID="lblReason" Text="<%$ Resources:lblReason %>" style="display:none"  /></td>
                            <td>
                          
              <asp:TextBox runat="server" ID="txtAppRejReqCom" style="display:none" Columns="30" Rows="4" TextMode="MultiLine" />
              </td>
              </tr>
                            </table>
                    <p style="text-align: center;">
                       <%-- bsharma33 RMA-3887 Reserve supplementals starts --%>
                        <%--<asp:Button ID="EditOkButton" runat="server" Text="<%$ Resources:btnAddOk %>" OnClientClick="return onOkEdit();"/>
                        <asp:Button ID="OkButton" runat="server" Text="<%$ Resources:btnOk %>" style="display:none"/>
                        <asp:Button ID="CancelButton" runat="server" Text="<%$ Resources:btnCancel %>"/>
                        <!--mgaba2:R8:Supervisory Approval-->
                        <asp:Button ID="btnApprove" runat="server" Text="<%$ Resources:btnApprove %>" style="display:none" OnClick="btnApprove_onclick"  />
                        <asp:Button ID="btnReject" runat="server" Text="<%$ Resources:btnReject %>" style="display:none" OnClick="btnReject_onclick" /> --%>
                        <%-- bsharma33 RMA-3887 Reserve supplementals ends --%>
                    </p>
                </div>
        </asp:Panel>
        
        <%--MITS:34082 START --%>
        <%-- bsharma33 RMA-3887 Reserve supplementals starts --%>
        <%--<asp:ModalPopupExtender ID="AddReserveModalPopup" runat="server"
            PopupControlID="Panel1"
            BackgroundCssClass="modalBackground"
            RepositionMode="RepositionOnWindowResize"
            OkControlID="Ok"
            OnOkScript="return false;"
            CancelControlID="Cancel"
            OnCancelScript="Clear()"
            DropShadow="true" PopupDragHandleControlID="Panel3"
            TargetControlID="hIsClaimCurrencySet" />

        <asp:ModalPopupExtender ID="EditReserveModalPopup" runat="server"
            TargetControlID="hIsClaimCurrencySet"
            PopupControlID="Panel2"
            BackgroundCssClass="modalBackground"
            RepositionMode="RepositionOnWindowResize"
            OkControlID="OkButton"
            OnOkScript="return false;"
            CancelControlID="CancelButton"
            OnCancelScript="Clear()"
            DropShadow="true" PopupDragHandleControlID="Panel4" />

        <asp:ModalPopupExtender ID="AddReserveModalPopup" runat="server"
            PopupControlID="Panel1"
            BackgroundCssClass="modalBackground"
            RepositionMode="RepositionOnWindowResize"
            OkControlID="Ok"
            OnOkScript="return false;"
            CancelControlID="Cancel"
            OnCancelScript="Clear();SetCulture();"
            DropShadow="true" PopupDragHandleControlID="Panel3"
            TargetControlID="hIsClaimCurrencySet" 
            BehaviorID="behAddMore"
             />

        <asp:ModalPopupExtender ID="EditReserveModalPopup" runat="server"
            TargetControlID="hIsClaimCurrencySet"
            PopupControlID="Panel2"
            BackgroundCssClass="modalBackground"
            RepositionMode="RepositionOnWindowResize"
            OkControlID="OkButton"
            OnOkScript="return false;"
            CancelControlID="CancelButton"
            OnCancelScript="Clear(); SetCultureEdit();"
            DropShadow="true" PopupDragHandleControlID="Panel4" />--%>
        <%-- bsharma33 RMA-3887 Reserve supplementals ends --%>
        <%--MITS:34082 END --%>

    </form>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
</body>
</html>
