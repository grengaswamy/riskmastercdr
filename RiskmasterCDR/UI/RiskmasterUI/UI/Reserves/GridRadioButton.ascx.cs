﻿/**********************************************************************************************
 *   Date     |  Jira   | Programmer | Description                                            *
 **********************************************************************************************
 * 09/20/2014 | rma-857 | ngupta36  | Incurred Handling - Collection Allowed or Not Allowed
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.UI.Reserves
{
    public partial class GridRadioButton : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        

        protected void grdFixtures_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Grab a reference to the Literal control
                Literal output = (Literal)e.Row.FindControl("RadioButtonMarkup");

                // Output the markup except for the "checked" attribute
                //output.Text = string.Format(@"<input type=""radio"" onclick=""RefreshGrid()"" name=""ClaimantsGroup_" + GridView1.ID.ToString() + @"""" + @" id=""RowSelector{0}"" value=""{0}""", e.Row.DataItemIndex);
                output.Text = string.Format(@"<input type=""radio"" onclick=""RefreshGrid('" + this.ID + @"')"" name="""+ this.ID + @"_ClaimantsGroup""  id=""RowSelector{0}"" value=""{0}""", e.Row.DataItemIndex);

                // OPTIONAL: If you want to have the first radio button selected on the first
                // page load, use the following if statement instead of the one below
                //if (SuppliersSelectedIndex == e.Row.RowIndex || (!Page.IsPostBack && e.Row.RowIndex == 0))

                
                // See if we need to add the "checked" attribute
                if (ClaimantsSelectedIndex == e.Row.RowIndex)
                    output.Text += @" checked=""checked""";

                // Add the closing tag
                output.Text += " />";
                //Added by sharishkumar for Mits 35472
                Literal outputLSS = (Literal)e.Row.FindControl("CheckBoxMarkup");               
                outputLSS.Text = string.Format(@"<input type=""checkbox"" name=""" + this.ID + @"_CheckClaimantsGroup""  id=""RowSelector{0}"" value=""{0}""", e.Row.DataItemIndex);

                outputLSS.Text += " />";
                //End Mits 35472
            }

            e.Row.Cells[1].CssClass = "hiddencol";
            e.Row.Cells[2].CssClass = "hiddencol";
            e.Row.Cells[3].CssClass = "hiddencol";
            e.Row.Cells[4].CssClass = "hiddencol";
            e.Row.Cells[5].CssClass = "hiddencol";
            e.Row.Cells[6].CssClass = "hiddencol";
            e.Row.Cells[12].CssClass = "hiddencol";

            e.Row.Cells[18].CssClass = "hiddencol";

            e.Row.Cells[19].CssClass = "hiddencol";
            e.Row.Cells[20].CssClass = "hiddencol";
            e.Row.Cells[21].CssClass = "hiddencol";
            e.Row.Cells[22].CssClass = "hiddencol";
            e.Row.Cells[23].CssClass = "hiddencol";
            e.Row.Cells[25].CssClass = "hiddencol";
            e.Row.Cells[26].CssClass = "hiddencol";
            e.Row.Cells[27].CssClass = "hiddencol";     //Ankit Start : Worked on MITS - 34297
            //Mits 35453 starts
            e.Row.Cells[28].CssClass = "hiddencol";
            e.Row.Cells[29].CssClass = "hiddencol";
            //Mits 35453 ends 
            //Added by sharishkumar for Mits 35472
            e.Row.Cells[31].CssClass = "hiddencol";
            e.Row.Cells[32].CssClass = "hiddencol";
            //End Mits 35472
            e.Row.Cells[33].CssClass = "hiddencol"; //JIRA-857
            e.Row.Cells[34].CssClass = "hiddencol";
            e.Row.Cells[35].CssClass = "hiddencol";
        }

        //Added by sharishkumar for Mits 35472
        Int32 iCheckClaimantsSelectedIndex = -1;

        private Int32 CheckClaimantsSelectedIndex
        {
            get
            {
                if (string.IsNullOrEmpty(Request.Form[this.ID + "_CheckClaimantsGroup"]))
                    return -1;
                else
                    return Convert.ToInt32(Request.Form[this.ID + "_CheckClaimantsGroup"]);
            }
        }
        //End Mits 35472
        int iClaimantsSelectedIndex = -1;

        
        private int ClaimantsSelectedIndex
        {
            get
            {
                if (string.IsNullOrEmpty(Request.Form[this.ID + "_ClaimantsGroup"]))
                    return -1;
                else
                    return Convert.ToInt32(Request.Form[this.ID + "_ClaimantsGroup"]);
            }
        }

        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblAdjusterDetails = (Label)e.Row.FindControl("AdjusterDetails");
                Label lblAdjusterName = (Label)e.Row.FindControl("AdjusterName");
                if (lblAdjusterDetails != null && !string.IsNullOrEmpty(lblAdjusterDetails.Text.Trim()))
                {
                    int iLastIndex = lblAdjusterDetails.Text.IndexOf("|@");
                    if (lblAdjusterName != null)
                        lblAdjusterName.Text = lblAdjusterDetails.Text.Remove(0, iLastIndex + 2);

                    lblAdjusterDetails.Attributes.Add("style", "display:none");
                }
                //Added by sharishkumar for Mits 35472
                Label ReserveID = (Label)e.Row.FindControl("ReserveID");

                if (ReserveID != null && !string.IsNullOrEmpty(ReserveID.Text.Trim()))
                {
                    Literal outputExpLSS = (Literal)e.Row.FindControl("CheckBoxMarkup");
                    Label lblLSS = (Label)e.Row.FindControl("LSSResExpFlag");
                    if (lblLSS != null && !string.IsNullOrEmpty(lblLSS.Text.Trim()))
                    {
                        if (lblLSS.Text == "-1")
                            outputExpLSS.Text = string.Format(@"<input type=""checkbox"" reserve= ""{1}"" checked=""checked"" disabled=""true"" name=""" + this.ID + @"_CheckClaimantsGroup""  id=""RowSelector{0}"" value=""{0}""", e.Row.DataItemIndex, ReserveID.Text);
                        else
                            outputExpLSS.Text = string.Format(@"<input type=""checkbox"" reserve= ""{1}""  name=""" + this.ID + @"_CheckClaimantsGroup""  id=""RowSelector{0}"" value=""{0}""", e.Row.DataItemIndex, ReserveID.Text);
                    }
                    Label lblReserve = (Label)e.Row.FindControl("MasterReserveType");
                    if (lblReserve != null && !string.IsNullOrEmpty(lblReserve.Text.Trim()))
                    {
                        if (lblReserve.Text != "E")
                            outputExpLSS.Text = string.Format(@"<input type=""checkbox"" style=""display:none;"" />");
                    }
                }
                //End Mits 35472
            }
        }
        //Ankit End

    }
}