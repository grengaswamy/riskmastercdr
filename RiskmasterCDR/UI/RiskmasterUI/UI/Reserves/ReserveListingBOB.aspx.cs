﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 12/06/2013 | 34082   | pgupta93   | Changes req for MultiCurrency
 * 12/03/2014 | 34275  | abhadouria |  RMA Swiss Re Financial Summary 
 * 09/20/2014 | rma-857 | ngupta36  | Incurred Handling - Collection Allowed or Not Allowed
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Collections;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using AjaxControlToolkit;
using System.Text;
using System.Web.Services;
using System.Collections.Specialized;
using System.Threading;
using System.Globalization;
using Riskmaster.UI.Shared.Controls; //rupal:mits 27373
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Reserves
{
    public partial class ReserveListingBOB : NonFDMBasePageCWS
    {
        private XmlDocument oResponse = null;
        private string sReturn = String.Empty;
        public string sCWSOutput = String.Empty; //rupal;mits 27373
        XmlDocument objXmlDoc = null; //objXmlDoc used for binding data only (DataBind())

        // Member variables
     //   private static XmlDocument _document;
        //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
        private static int iReserveID = 0;
        //Ankit End
        //Added by sharishkumar for Mits 35472
        string enableLSS = string.Empty;
        string exportToLss = string.Empty;
        //End Mits 35472
        //private static object _lock = new object();
        private string[] sDataSeparator = { "~~~" };  //Deb MITS 28251
        // we make these public statics just so we can call them from externally for the
        // page method call
        // Added by Amitosh for JIRA 9659. Hiding Check total when apply deductibles is false. since check total is not calculated when check is printed from print check module
        //code needs to be reverted.
        private bool bApplyDeductibles = false;
        public static XmlDocument GetDocument()
        {

            XmlDocument document = new XmlDocument();
            XmlDocument Response = null;
            string sCWSRequest = String.Empty;
            string sCWSresponse = String.Empty;

            string[] arrQuery = HttpContext.Current.Request.UrlReferrer.Query.Split('&');
            arrQuery = arrQuery[0].Split('=');

            int iClaimID = int.Parse(arrQuery[1]);

            sCWSRequest = GetMessageTemplate2(iClaimID);
            sCWSresponse = AppHelper.CallCWSService(sCWSRequest);
            sCWSresponse = System.Web.HttpUtility.HtmlDecode(sCWSresponse);  //mkaran2 - [JIRA] (RMA-4110) MITS 37158 
            document.LoadXml(sCWSresponse);
            document.LoadXml(document.SelectSingleNode("//BOB").OuterXml);
            return document;

        }


        private static string GetMessageTemplate2(int iClaimID)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>ReserveFundsAdaptor.GetLookupXml</Function></Call><Document>");

            sXml = sXml.Append("<BOB>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(iClaimID.ToString());
            sXml = sXml.Append("</ClaimID>");
            //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
            sXml = sXml.Append("<ReserveType>");
            sXml = sXml.Append(iReserveID.ToString());
            sXml = sXml.Append("</ReserveType>");
            //Ankit End
            sXml = sXml.Append("</BOB></Document></Message>");

            //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
            iReserveID = 0;
            //Ankit End
            return sXml.ToString();

            //XElement oTemplate = XElement.Parse(sXml.ToString());
            //return oTemplate;
        }

        public static string[] Hierarchy
        {
            get { return new string[] { "policy", "unit", "coverage", "loss" }; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string[] arrClaimantNames = null;
            string[] arrClaimantEIDs = null;
            string[] arrClaimantRowIds = null;   //bpaskova JIRA 10347
            int iLength = 0;
            //rupal:multicurrency
            string sUseMulticurrency = string.Empty;
            string sLobCode = string.Empty;
            string sPrevtResModZero = string.Empty;

            try
            {
                string sSubmitedFrmRsv = AppHelper.GetQueryStringValue("page");
                if (!string.IsNullOrEmpty(sSubmitedFrmRsv))
                {
                    if (sSubmitedFrmRsv == "ReserveSummary")
                    {
                        Server.Transfer("ReserveSummary.aspx");
                    }
                }
                if (TabPanel1 != null)
                {
                    TabContainer1.Tabs.Remove(TabPanel1);
                }

                if (!IsPostBack)
                {
                    claimid.Text = AppHelper.GetQueryStringValue("ClaimId");
                    oResponse = new XmlDocument();
                    NonFDMCWSPageLoad("ReserveFundsAdaptor.GetClaimants");
                    oResponse = Data;
                    sReturn = oResponse.OuterXml;

                    XmlDocument objOuterXml = new XmlDocument();
                    objOuterXml.LoadXml(sReturn);

                    foreach (XmlNode objNode in objOuterXml.SelectNodes("//Claimants/Claimant"))
                    {
                        if (txtClaimants.Text == "")
                            txtClaimants.Text = objNode.SelectSingleNode("ClaimantName").InnerText;
                        else
                            txtClaimants.Text += "|" + objNode.SelectSingleNode("ClaimantName").InnerText;

                        if (txtClaimantEIDs.Text == "")
                            txtClaimantEIDs.Text = objNode.SelectSingleNode("ClaimantEID").InnerText;
                        else
                            txtClaimantEIDs.Text += "|" + objNode.SelectSingleNode("ClaimantEID").InnerText;

                        //bpaskova JIRA 10347 start
                        if (txtClaimantRowIds.Text == "")
                            txtClaimantRowIds.Text = objNode.SelectSingleNode("ClaimantRowId").InnerText;
                        else
                            txtClaimantRowIds.Text += "|" + objNode.SelectSingleNode("ClaimantRowId").InnerText;
                        //bpaskova JIRA 10347 end
                    }
                    //rupal:start,multicurrency
                    if (objOuterXml.SelectSingleNode("//Claimants/UseMulticurrency") != null)
                    {
                        sUseMulticurrency = objOuterXml.SelectSingleNode("//Claimants/UseMulticurrency").InnerText;
                        txtUseMulticurrency.Text = sUseMulticurrency; //MITS 34275- RMA Swiss Re Financial Summary 
                    }
                    if (sUseMulticurrency != "0")
                    {
                        //multicurrency is ON                        
                        hdUseMultiCurrency.Value = sUseMulticurrency.ToString(); //35453
                    }
                    else
                    {
                        lblCurrencytype.Attributes.Add("style", "display:none");
                        drdCurrencytype.Attributes.Add("style", "display:none");
                        //MITS:34082 MultiCurrency START
                        lbl_currencytypetext.Attributes.Add("style", "display:none");
                        Reservecurrencytypetext.Attributes.Add("style", "display:none");
                        lblCurrencyTypeEdit.Attributes.Add("style", "display:none");
                        ReservecurrencytypetextEdit.Attributes.Add("style", "display:none");
                        trReserveCurrType.Attributes.Add("style", "display:none");
                        trReservecurrencytypeEdit.Attributes.Add("style", "display:none");
                        //Label1.Attributes.Add("style", "display:none");
                        //lcurrencytype.Attributes.Add("style", "display:none");
                        //lcurrencytypeedit.Attributes.Add("style", "display:none");                        
                        //Label2.Attributes.Add("style", "display:none");
                        //MITS:34082 MultiCurrency END
                    }
                    //rupal:end

                    //igupta3 JIRA RMA-616
                    if (objOuterXml.SelectSingleNode("//Claimants/PrevtResModZero") != null)
                    {
                        sPrevtResModZero = objOuterXml.SelectSingleNode("//Claimants/PrevtResModZero").InnerText;
                        PrevResModifyzero.Text = sPrevtResModZero;
                    }
                    //JiRA end
                    if (objOuterXml.SelectSingleNode("//Claimants/lobcode") != null)
                    {
                        sLobCode = objOuterXml.SelectSingleNode("//Claimants/lobcode").InnerText;
                        lob.Text = sLobCode;
                    }
                    if (sLobCode != "243")
                    {
                        lbldisabltycat.Attributes.Add("style", "display:none");
                        cmbdisablitycat.Attributes.Add("style", "display:none");
                        //lblDisCat.Attributes.Add("style", "display:none");
                        //txtDisabilityCat.Attributes.Add("style", "display:none");
                      //  lblLossType.Text = "Loss Type";
                        //lblCoverageLossType.Text = "Loss Type";
                     //   CascadingDropDown5.ParentControlID = "cmbCoverageLossType";
                        disabilitytype.Visible = false;
                        lbldistype.Attributes.Add("style", "display:none");
                        lbldisabltycat.Attributes.Add("style", "display:none");
                        txtDisabilityCat.Attributes.Add("style", "display:none");
                        lblDisabilityLossType.Attributes.Add("style", "display:none");
                        txtDisabilityLossType.Attributes.Add("style", "display:none");
                      //  txtDisTypeCode.Attributes.Add("style", "display:none");
                    //    lblDisTypeCode.Attributes.Add("style", "display:none");



                    }
                    else
                    {
                        lblLossType.Attributes.Add("style", "display:none");
                        cmbCoverageLossType.Attributes.Add("style", "display:none");
                       // txtCoverageLossType.Attributes.Add("style", "display:none");
                       // lblCoverageLossType.Attributes.Add("style", "display:none");
                      
                    //  cmbReserveType.Enabled = true;
                      
                       // cmbdisablitycat.Attributes.Add("style", "display:none");
                     //   lblDisCat.Attributes.Add("style", "display:none");
                        //txtDisabilityCat.Attributes.Add("style", "display:none");
                     //   lblLossType.Text = "Disability Code";
                        //lblCoverageLossType.Text = "Disability Code";
                        lblCoverageLossType.Attributes.Add("style", "display:none");
                        txtCoverageLossType.Attributes.Add("style", "display:none");
                      //  CascadingDropDown4.ParentControlID = "";
                    
                    }
                    //Added by sharishkumar for Mits 35472
                    if (objOuterXml.SelectSingleNode("//Claimants/rmxlssenable") != null)
                    {
                        enableLSS = objOuterXml.SelectSingleNode("//Claimants/rmxlssenable").InnerText;                        
                    }
                    //End Mits 35472
                }

                if (!IsPostBack)
                {
                    arrClaimantEIDs = txtClaimantEIDs.Text.Split('|');
                    arrClaimantRowIds = txtClaimantRowIds.Text.Split('|');  //bpaskova JIRA 10347
                    arrClaimantNames = txtClaimants.Text.Split('|');
                    if (txtClaimants.Text != "" && arrClaimantNames != null)
                        iLength = arrClaimantNames.Length;
                    else
                        iLength = 0;
                }
                else
                {
                    if (Request.Form["txtClaimants"] != null)
                        arrClaimantNames = Request.Form["txtClaimants"].Split('|');
                    if (Request.Form["txtClaimantEIDs"] != null)
                        arrClaimantEIDs = Request.Form["txtClaimantEIDs"].Split('|');
                    //bpaskova JIRA 10347
                    if (Request.Form["txtClaimantRowIds"] != null)
                        arrClaimantRowIds = Request.Form["txtClaimantRowIds"].Split('|');

                    if (arrClaimantNames != null && arrClaimantNames[0] != "")
                    {
                        iLength = arrClaimantNames.Length;
                    }
                    else
                    {
                        iLength = 0;
                    }
                }

                for (int i = 0; i < iLength; i++)
                {
                    TabPanel objTabPanel = new TabPanel();

                    GridRadioButton objGrid = (GridRadioButton)LoadControl("GridRadioButton.ascx");

                    objTabPanel.ID = "TabPanel" + (i + 1);
                    objTabPanel.HeaderText = arrClaimantNames[i];
                    //objTabPanel.HeaderText = "Claimant" + (i + 1).ToString();

                    UpdatePanel objUpdatePnl = new UpdatePanel();
                    objUpdatePnl.ID = "TabUpdatePanel" + (i + 1).ToString();

                    //rupal:start, MITS 27373
                    ErrorControl objErrorCtl = (ErrorControl)this.Page.LoadControl("..\\Shared\\Controls\\ErrorControl.ascx");
                    objErrorCtl.ID = "ErrorCtl" + (i + 1).ToString();
                    objUpdatePnl.ContentTemplateContainer.Controls.Add(objErrorCtl);
                    //rupal:end

                    TextBox sIsDataLoaded = new TextBox();
                    sIsDataLoaded.ID = "IsDataLoaded" + (i + 1).ToString();
                    sIsDataLoaded.Text = "0";
                    sIsDataLoaded.Attributes.Add("style", "display:none");
                    objUpdatePnl.ContentTemplateContainer.Controls.Add(sIsDataLoaded);

                    TextBox txtAction = new TextBox();
                    txtAction.ID = "txtAction" + (i + 1).ToString();
                    txtAction.Text = "";
                    txtAction.Attributes.Add("style", "display:none");
                    objUpdatePnl.ContentTemplateContainer.Controls.Add(txtAction);


                    TextBox txtClaimantEID = new TextBox();
                    txtClaimantEID.ID = "ClaimantEID" + (i + 1);
                    txtClaimantEID.Text = arrClaimantEIDs[i];
                    txtClaimantEID.Attributes.Add("style", "display:none");
                    //rupal: r8 multicurrency
                    TextBox txtClaimantDisplayCurrType = new TextBox();
                    txtClaimantDisplayCurrType.ID = "txtClaimantDisplayCurrType";
                    //txtClaimantDisplayCurrType.Text=
                    objUpdatePnl.ContentTemplateContainer.Controls.Add(txtClaimantEID);

                    //bpaskova JIRA 10347 start
                    TextBox txtClaimantRowId = new TextBox();
                    txtClaimantRowId.ID = "ClaimantRowId" + (i + 1);
                    txtClaimantRowId.Text = arrClaimantRowIds[i];
                    txtClaimantRowId.Attributes.Add("style", "display:none");
                    objUpdatePnl.ContentTemplateContainer.Controls.Add(txtClaimantRowId);
                    //bpaskova JIRA 10347 end

                    Label lblMessage = new Label();
                    lblMessage.ID = "Label" + (i + 1);
                    lblMessage.Text = "";
                    objUpdatePnl.ContentTemplateContainer.Controls.Add(lblMessage);


                    objGrid.ID = "Grid" + (i + 1);
                    objUpdatePnl.ContentTemplateContainer.Controls.Add(objGrid);


                    objTabPanel.Controls.Add(objUpdatePnl);

                    TextBox objTextBox = new TextBox();
                    objTextBox.ID = "SelectedId" + (i + 1).ToString();
                    objTextBox.Text = "-1";
                    objTextBox.Attributes.Add("style", "display:none");
                    objTabPanel.Controls.Add(objTextBox);

                    UpdatePanel objUpdatePanel = new UpdatePanel();
                    objUpdatePanel.ID = "UpdatePanel" + (i + 1);
                    objUpdatePanel.Load += new EventHandler(UpdatePanel_Load);

                    Label lblPayments = new Label();
                    lblPayments.ID = "LabelPayments" + (i + 1);
                    lblPayments.Text = "";
                    objUpdatePnl.ContentTemplateContainer.Controls.Add(lblPayments);


                    GridView objPayeeGrid = new GridView();
                    objPayeeGrid.ID = "PayeeGrid" + (i + 1);
                    objPayeeGrid.RowDataBound += new GridViewRowEventHandler(grdPayee_RowDataBound);
                    objPayeeGrid.HeaderStyle.CssClass = "colheader3";
                    objPayeeGrid.AlternatingRowStyle.CssClass = "rowlight2";
                    objPayeeGrid.RowStyle.CssClass = "rowlight1";
                    objPayeeGrid.Width = Unit.Percentage(100);
                    objUpdatePanel.ContentTemplateContainer.Controls.Add(objPayeeGrid);

                    objTabPanel.Controls.Add(objUpdatePanel);

                    TabContainer1.Tabs.Add(objTabPanel);
                }

                if (iLength == 0)
                {
                    Label lblMessage = new Label();
                    this.Controls.Add(lblMessage);
                    lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;                    " + RMXResourceProvider.GetSpecificObject("lblNoClaimaints", RMXResourceProvider.PageId("ReserveListingBOB.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) + "</h3></p>";
                    toolbardrift.Attributes.Add("style", "display:none");
                    //rupal:start, multicurrency
                    lblCurrencytype.Attributes.Add("style", "display:none");
                    drdCurrencytype.Attributes.Add("style", "display:none");
                    //rupal:end
                }

                //Added by sharishkumar for Mits 35472
                if (enableLSS == "True")               
                LSSExport.Visible = true;
                else               
                  LSSExport.Visible = false;
                //End Mits 35472
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                arrClaimantNames = null;
                arrClaimantEIDs = null;
                arrClaimantRowIds = null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            TextBox objIsDataLoaded = null;
            UpdatePanel objUpdatePanel = null;
            TextBox txtaction = null;
            TextBox objClaimantEID = null;
               TextBox txtactiontest = null;
            
            string sCWSRequest = String.Empty;
            //string sCWSResponse = String.Empty; //rupal:mits 27373
            oResponse = new XmlDocument();
            XmlDocument objXmlDoc = null;  //akaur9 MITS 27907 --Start
            XmlNode objNode = null;
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ReserveListingBOB.aspx"), "ReserveListingBOBValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "ReserveListingBOBValidations", sValidationResources, true);
                objXmlDoc = new XmlDocument();
                if (!Page.IsPostBack)
                {
                    claimid.Text = AppHelper.GetQueryStringValue("ClaimId");
                }
                int i = TabContainer1.ActiveTabIndex;

                if (i != -1)
                {
                    objUpdatePanel = (UpdatePanel)(this.TabContainer1.Tabs[i].FindControl("TabUpdatePanel" + (i + 1).ToString()));
                    objIsDataLoaded = (TextBox)((objUpdatePanel.FindControl("IsDataLoaded" + (i + 1).ToString())));
                    txtaction = (TextBox)((objUpdatePanel.FindControl("txtAction" + (i + 1).ToString())));
                    //claimanteid = (TextBox)((objUpdatePanel.FindControl("ClaimantEID" + (i + 1).ToString())));
                    objClaimantEID = (TextBox)((objUpdatePanel.FindControl("ClaimantEID" + (i + 1).ToString())));
                    claimanteid.Text = objClaimantEID.Text;
                    //if (Request.Form["txtClaimantEIDs"] != null)
                    //  arrClaimantEIDs = Request.Form["txtClaimantEIDs"].Split('|');

                    //rupal:start, MITS 27373
                    ErrorControl ErrorControl1 = (ErrorControl)((objUpdatePanel.FindControl("ErrorCtl" + (i + 1).ToString())));
                    //preeti:start,jira 341
                    if (ViewState["Error"] != null)
                    {

                        string error = ViewState["Error"].ToString();
                        objXmlDoc.LoadXml(error);
                        if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                        {
                            HttpContext.Current.Items["Error"] = error;
                            ErrorControl1.errorDom = HttpContext.Current.Items["Error"].ToString();
                        }
                        ViewState["Error"] = null;
                    }
                    //preeti:end,jira 341
                    if (ErrorControl1 != null)
                    {
                        if (ErrorControl1.errorhandle == string.Empty)
                            ErrorControl1.errorDom = string.Empty;
                    }
                    //rupal:end
                    //MITS:34082 START
                    //bsharma33 RMA-3887 Reserve supp starts
                    //if (txtAddEditPopup.Text == "AddPopup")
                    //    AddReserveModalPopup.Show();
                    //if (txtAddEditPopup.Text == "EditPopup")
                    //{
                    //    ClientScript.RegisterStartupScript(this.GetType(), "script", "OnEditLoad();", true);
                    //    EditReserveModalPopup.Show();
                    //}
                    //bsharma33 RMA-3887 Reserve supp ends
                    txtAddEditPopup.Text = "";
                    //MITS:34082 END
                    string s = "";
                    if (Request.Form["__EVENTARGUEMENT"] != null)
                        s = Request.Form["__EVENTARGUEMENT"].ToString();
                    if (txtaction.Text == "Add" || txtaction.Text == "AddMore" || txtaction.Text == "Approve" || txtaction.Text == "Reject")//rupal:jira 341 //bsharma33 RMA-3887 Reserve supp starts
                    {
                        //bsharma33 RMA-3887 Reserve supp starts
                        //sCWSRequest = GetMessageTemplate3();
                        //sCWSOutput = AppHelper.CallCWSService(sCWSRequest);
                        //bsharma33 RMA-3887 Reserve supp ends
                        //preeti:start,jira 341
                        if (EditLoad.Text == "true")
                            if (sCWSOutput != "")
                            {
                                objXmlDoc.LoadXml(sCWSOutput);
                                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                                {
                                    ViewState["Error"] = sCWSOutput;
                                }
                            }
                        //preeti:end,jira 341
                        objIsDataLoaded.Text = "0";
                        currencytype.Text = txtClaimCurrency.Text;//MITS:34082
                        //bsharma33 RMA-3887 Reserve supp starts
                        //if (!txtaction.Text.Equals("AddMore"))//rupal:jira 341
                        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "SetCulture();", true);//MITS:34082
                        if (txtaction.Text == "AddMore")
                        {
                            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "ShowDialog();", true);//MITS:34082
                            ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "ShowDialog('0');", true);
                        }
                        //bsharma33 RMA-3887 Reserve supp ends
                        txtaction.Text = "";
                    }
                    else if (txtaction.Text == "Edit")
                    {
                        txtaction.Text = "";
                        //bsharma33 RMA-3887 Reserve supp starts
                        //sCWSRequest = GetMessageTemplate4();
                        //sCWSOutput = AppHelper.CallCWSService(sCWSRequest);
                        //bsharma33 RMA-3887 Reserve supp ends

                        objIsDataLoaded.Text = "0";
                        //MITS:34082 START
                        currencytype.Text = txtClaimCurrency.Text;
                        currencytype.Text = txtClaimCurrency.Text;
                        //rupal:start,jira 341
                        if (sCWSOutput != "")
                        {
                            objXmlDoc.LoadXml(sCWSOutput);
                            //preeti:start,jira 341
                            if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                            {
                                ViewState["Error"] = sCWSOutput;
                            }
                        }
                        //preeti:end,jira 341
                        //bsharma33 RMA-3887 Reserve supp starts
                        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "SetCultureEdit();", true);
                        //bsharma33 RMA-3887 Reserve supp ends
                        //MITS:34082 END
                    }

                    //If data in tab hasn't been loaded in a previous post back , load it here.
                    if (objIsDataLoaded.Text != "1")
                    {
                        //akaur9 MITS 27907 --Start
                        if (sCWSOutput != "")
                        {
                            objXmlDoc.LoadXml(sCWSOutput);
                            objNode = objXmlDoc.SelectSingleNode("//ReserveStatus");
                            if (objNode != null && objNode.Attributes["Status"].Value == "H")
                            {
                                //ScriptManager.RegisterStartupScript(this, typeof(Page), "UniqueID", "alert('This reserve has been sent for the Supervisor approval and is in the Hold status.')", true);
                                string sMessage = RMXResourceProvider.GetSpecificObject("ApprovalStatusID", RMXResourceProvider.PageId("ReserveListingBOB.aspx"), ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                                string str = String.Format("<script>alert('{0}');</script>", sMessage);
                                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "UniqueID", str, true);
                            }
                        }
                        //akaur9 MITS 27907 --End
                        DataBind();
                    }
                    //MITS:34082 START
                    if (!Page.IsPostBack)
                    {
                        string sCurrCulture = Reservecurrencytypetext.CodeText;
                        if (!string.IsNullOrEmpty(sCurrCulture))
                        {
                            Culture = sCurrCulture.Split('|')[1];
                            UICulture = sCurrCulture.Split('|')[1];
                            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(sCurrCulture.Split('|')[1]);
                            Thread.CurrentThread.CurrentUICulture = new CultureInfo(sCurrCulture.Split('|')[1]);
                        }
                        base.InitializeCulture();
                        //MITS:34082 END
                        if (Reservecurrencytypetext.CodeText != "")
                        {                       
                            txtClaimCurrenyType.Text = Reservecurrencytypetext.CodeText.Split('_')[0];
                            txtClaimCurrency.Text = Reservecurrencytypetext.CodeText;
                            currencytype.Text = Reservecurrencytypetext.CodeText;
                        }
                        if (ReservecurrencytypetextEdit.CodeText != "")
                        {
                            txtClaimCurrenyType.Text = ReservecurrencytypetextEdit.CodeText.Split('_')[0];
                            txtClaimCurrency.Text = ReservecurrencytypetextEdit.CodeText;
                            currencytype.Text = ReservecurrencytypetextEdit.CodeText;
                        }
                    }
                    //MITS:34082 START
                    //Deb MITS 31370
                    caption.Text = caption.Text.Replace(caption.Text.Substring(caption.Text.LastIndexOf("*")),"* " + this.TabContainer1.ActiveTab.HeaderText + "]");
                    //Deb MITS 28251
                    lblcaption.Text = String.Format("{0} ({1})", RMXResourceProvider.GetSpecificObject("lblcaptionID", RMXResourceProvider.PageId("ReserveListingBOB.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), caption.Text);
                    //lblcaption.Text += "(" + caption.Text + ")";
                    //Deb MITS 28251
                    //rupal: r8 multicurrency
                    CustomizePage();

                }

                //Start averma62
                if (AppHelper.ReadCookieValue("EnableVSS").Equals("-1"))
                    div_vssexport.Visible = true; //JIRA RMA-442
                else
                    div_vssexport.Visible = false;
                //End averma62
                //rupal:start, multicurrency
                //validation to check if claim currency is set
                //if (hIsClaimCurrencySet.Text == "0")
                //{
                //    AddReserveModalPopup.TargetControlID = "hIsClaimCurrencySet"; //dummy targetcontrolid as we dont want to show popup if claim currency is not set              
                //    EditReserveModalPopup.TargetControlID = "hIsClaimCurrencySet"; //dummy targetcontrolid as we dont want to show popup if claim currency is not set              
                //}
                //else
                //{
                //    AddReserveModalPopup.TargetControlID = "AddReserve";
                //    EditReserveModalPopup.TargetControlID = "EditReserve";
                //}
                //rupal:end
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        //rupal:start, r8 multi currency changes

        protected void drdCurrencytype_onchange(object sender, EventArgs e)
        {
            try
            {
                //Deb MITS 28251
                DropDownList drpCurr=(DropDownList)sender;
                string sSelectedText = drpCurr.SelectedItem.Text; 
                for (int j = 0; j < drpCurr.Items.Count; j++)
                {
                    if (drpCurr.Items[j].Value.Contains("~~~"))
                    {
                        drpCurr.Items[j].Value = drpCurr.Items[j].Value.Split(sDataSeparator, StringSplitOptions.None)[0];
                    }
                }
                //Deb MITS 28251
                int iActiveIndex = TabContainer1.ActiveTabIndex;
                for (int i = 0; i < TabContainer1.Tabs.Count; i++)
                {
                    TabContainer1.ActiveTabIndex = i;
                    claimid.Text = AppHelper.GetQueryStringValue("ClaimId");
                    DataBind();
                }
                // MITS 35453 Prashant 
                for (int j = 0; j < drpCurr.Items.Count; j++)
                {
                    if (!drpCurr.Items[j].Value.Contains("~~~"))
                    {
                        drpCurr.Items[j].Value += "~~~" + drpCurr.Items[j].Text;
                    }
                }

                    // MITS 35453 
                TabContainer1.ActiveTabIndex = iActiveIndex;
                //Deb MITS 28251
                ListItem objItem = drpCurr.Items.FindByText(sSelectedText);
                if (objItem != null)
                {
                    drpCurr.Items.FindByText(sSelectedText).Selected = true;
                }
                //Deb MITS 28251
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void CustomizePage()
        {
            //MITS:34082 MultiCurrency START

            string ReservecurrencyType = Reservecurrencytypetext.CodeTextValue;
            Reservecurrencytypetext.CodeId = txtReserveCurrID.Text;
            if (!string.IsNullOrEmpty(ReservecurrencyType))
            {
                Reservecurrencytypetext.CodeText = ReservecurrencyType.Split('_')[0];
            }

            string ReservecurrencyTypeEdit = ReservecurrencytypetextEdit.CodeTextValue;
            ReservecurrencytypetextEdit.CodeId = txtReserveCurrID.Text;
            if (!string.IsNullOrEmpty(ReservecurrencyTypeEdit))
            {
                ReservecurrencytypetextEdit.CodeText = ReservecurrencyTypeEdit.Split('_')[0];
            }


            ////Deb Multi Currency
            //if (!string.IsNullOrEmpty(currencytype.Text))
            //{
            //    lcurrencytype.Text = currencytype.Text.Split('_')[0];
            //    lcurrencytypeedit.Text = currencytype.Text.Split('_')[0];
            //}

            //MITS:34082 MultiCurrency END
        }
        //rupal:end
        /* bsharma33 RMA-3887 Res supp, commented following code starts */
        //private string GetXmlInForApproveOrReject(string p_sAction)
        //{
        //    StringBuilder sXml = new StringBuilder("<Message>");
        //    sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
        //    sXml = sXml.Append("<Call><Function>ReserveFundsAdaptor.ApproveOrRejectReserve</Function></Call><Document>");

        //    sXml = sXml.Append("<BOB>");
        //    sXml = sXml.Append("<ClaimID>");
        //    sXml = sXml.Append(claimid.Text);
        //    sXml = sXml.Append("</ClaimID>");

        //    sXml = sXml.Append("<ClaimantEID>");
        //    sXml = sXml.Append(claimanteid.Text);
        //    sXml = sXml.Append("</ClaimantEID>");

        //    sXml = sXml.Append("<CoverageID>");
        //    sXml = sXml.Append(txtCoverageType.Text);
        //    sXml = sXml.Append("</CoverageID>");

        //    sXml = sXml.Append("<ReserveTypeCode>");
        //    sXml = sXml.Append(txtReserveTypeCode.Text);
        //    sXml = sXml.Append("</ReserveTypeCode>");

        //    sXml = sXml.Append("<RCRowId>");
        //    sXml = sXml.Append(txtReserveID.Text);
        //    sXml = sXml.Append("</RCRowId>");

        //    sXml = sXml.Append("<Amount>");
        //    sXml = sXml.Append(txtEditAmount.Text);
        //    sXml = sXml.Append("</Amount>");
        //    sXml = sXml.Append("<ApproveorRejectRsv>");
        //    sXml = sXml.Append(p_sAction);
        //    sXml = sXml.Append("</ApproveorRejectRsv>");
        //    //skhare7 R8 Supervisory Approval 
        //    sXml = sXml.Append("<ApproveReasonCom>");
        //    sXml = sXml.Append(txtAppRejReqCom.Text);
        //    sXml = sXml.Append("</ApproveReasonCom>");
        //    sXml = sXml.Append("<CvgLossId>");
        //    sXml = sXml.Append(cvglossid.Text);
        //    sXml = sXml.Append("</CvgLossId>");
        //    //skhare7 R8 Supervisory Approval End
        //    sXml = sXml.Append("</BOB></Document></Message>");

        //    return sXml.ToString();


        //}

        //protected void btnApprove_onclick(object sender, EventArgs e)
        //{

        //    try
        //    {
        //        string sTemplate = GetXmlInForApproveOrReject("Approve");
        //        //Mgaba2:R8:Supervisory Approval
        //        sReturn = AppHelper.CallCWSService(sTemplate);
        //        XDocument xd = XDocument.Parse(sReturn);
        //        if (xd.XPathSelectElement("//MsgStatus/MsgStatusCd") != null)
        //        {
        //            if (xd.XPathSelectElement("//MsgStatus/MsgStatusCd").Value.ToLower() == "error")
        //            {

        //                if (xd.XPathSelectElement("//ExtendedStatus/ExtendedStatusDesc") != null)
        //                {
        //                    ErrorControl1.errorDom = sReturn;


        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ee)
        //    {
        //        ErrorHelper.logErrors(ee);
        //        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        //        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        //        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
        //    }
        //}

        //protected void btnReject_onclick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string sTemplate = GetXmlInForApproveOrReject("Reject");
        //        // MANISH JAIN MITS 26605 
        //        sReturn = AppHelper.CallCWSService(sTemplate);
        //        XDocument xd = XDocument.Parse(sReturn);
        //        if (xd.XPathSelectElement("//MsgStatus/MsgStatusCd") != null)
        //        {
        //            if (xd.XPathSelectElement("//MsgStatus/MsgStatusCd").Value.ToLower() == "error")
        //            {

        //                if (xd.XPathSelectElement("//ExtendedStatus/ExtendedStatusDesc") != null)
        //                {
        //                    ErrorControl1.errorDom = sReturn;

        //                }
        //            }
        //        }
        //    }

        //    catch (Exception ee)
        //    {
        //        ErrorHelper.logErrors(ee);
        //        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        //        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        //        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
        //    }
        //}

        //private string GetMessageTemplate3()
        //{
        //    StringBuilder sXml = new StringBuilder("<Message>");
        //    sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
        //    sXml = sXml.Append("<Call><Function>ReserveFundsAdaptor.AddReserve</Function></Call><Document>");

        //    sXml = sXml.Append("<BOB>");
        //    sXml = sXml.Append("<ClaimID>");
        //    sXml = sXml.Append(claimid.Text);
        //    sXml = sXml.Append("</ClaimID>");

        //    sXml = sXml.Append("<ClaimantEID>");
        //    sXml = sXml.Append(claimanteid.Text);
        //    sXml = sXml.Append("</ClaimantEID>");

        //    sXml = sXml.Append("<PolicyID>");
        //    sXml = sXml.Append(cmbPolicy.SelectedValue);
        //    sXml = sXml.Append("</PolicyID>");
        //    sXml = sXml.Append("<CoverageID>");
        //    //rupal:start
        //    string[] arrCovgType = cmbCoverageType.SelectedValue.Split('#');
        //    //sXml = sXml.Append(cmbCoverageType.SelectedValue);
        //    sXml = sXml.Append(arrCovgType[0]);
        //    //rupal:end            
        //    sXml = sXml.Append("</CoverageID>");
        //    if (lob.Text.CompareTo("243") == 0)
        //    {
        //        sXml = sXml.Append("<DisablityCat>");
        //        sXml = sXml.Append(cmbdisablitycat.SelectedValue);
        //        sXml = sXml.Append("</DisablityCat>");
        //        sXml = sXml.Append("<LossTypeCode>");
        //        sXml = sXml.Append(txtDisabilityCode.Text);
        //        sXml = sXml.Append("</LossTypeCode>");

        //    }
        //    else
        //    {
        //        sXml = sXml.Append("<LossTypeCode>");
        //        sXml = sXml.Append(cmbCoverageLossType.SelectedValue);
        //        sXml = sXml.Append("</LossTypeCode>");
            
        //    }

          
        //    sXml = sXml.Append("<CoverageLossID>");
        //   // sXml = sXml.Append(cmbCoverageLossType.SelectedValue);
        //    sXml = sXml.Append("</CoverageLossID>");
        //    sXml = sXml.Append("<ReserveTypeCode>");
        //    sXml = sXml.Append(cmbReserveType.SelectedValue);
        //    sXml = sXml.Append("</ReserveTypeCode>");
        //    sXml = sXml.Append("<ReserveCat>");
        //    //sXml = sXml.Append(cmbReserveCategoryAdd.SelectedValue); 
        //    sXml = sXml.Append(txtReserveSubTypeCode.Text); //mkaran2 - [JIRA] (RMA-4110) MITS 37158
        //    sXml = sXml.Append("</ReserveCat>");
        //    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        //    sXml = sXml.Append("<AssignAdjusterEid>");
        //    sXml = sXml.Append(claimadjusterlookup_cid.Text);
        //    sXml = sXml.Append("</AssignAdjusterEid>");
        //    //Ankit End
        //    sXml = sXml.Append("<Amount>");
        //    sXml = sXml.Append(txtAmount.AmountInString);
        //    sXml = sXml.Append("</Amount>");
        //    sXml = sXml.Append("<Reason>");
        //    sXml = sXml.Append(txtReasonAdd.Text);
        //    sXml = sXml.Append("</Reason>");
        //    sXml = sXml.Append("<Status>");
        //    sXml = sXml.Append(cmbStatusAdd.SelectedValue);
        //    sXml = sXml.Append("</Status>");
        //    //MITS:34082 MultiCurrency START

        //    sXml = sXml.Append("<CurrencyType>");
        //    sXml = sXml.Append(Reservecurrencytypetext.CodeText);
        //    sXml = sXml.Append("</CurrencyType>");

        //    //MITS:34082 MultiCurrency END
        //    sXml = sXml.Append("</BOB></Document></Message>");

        //    return sXml.ToString();


        //}

        /// <summary>
        /// GetMessageTemplate4
        /// </summary>
        /// <returns></returns>
   //     private string GetMessageTemplate4()
   //     {
   //         StringBuilder sXml = new StringBuilder("<Message>");
   //         sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
   //         sXml = sXml.Append("<Call><Function>ReserveFundsAdaptor.EditReserve</Function></Call><Document>");

   //         sXml = sXml.Append("<BOB>");
   //         sXml = sXml.Append("<ClaimID>");
   //         sXml = sXml.Append(claimid.Text);
   //         sXml = sXml.Append("</ClaimID>");

   //         sXml = sXml.Append("<ClaimantEID>");
   //         sXml = sXml.Append(claimanteid.Text);
   //         sXml = sXml.Append("</ClaimantEID>");

   //         sXml = sXml.Append("<PolicyID>");
   //         sXml = sXml.Append(txtPolicyID.Text);
   //         sXml = sXml.Append("</PolicyID>");
   //         sXml = sXml.Append("<CoverageID>");
   //         sXml = sXml.Append(txtCoverageType.Text);
   //         sXml = sXml.Append("</CoverageID>");
   //         //rupal:start
   //sXml = sXml.Append("<CoverageLossID>");
   //         sXml = sXml.Append(cvglossid.Text);
   //         sXml = sXml.Append("</CoverageLossID>");
   //         sXml = sXml.Append("<CovgSeqNum>");
   //         sXml = sXml.Append(txtCovgSeqNum.Text);
   //         sXml = sXml.Append("</CovgSeqNum>");
   //         //rupal:end
   //         sXml = sXml.Append("<ReserveTypeCode>");
   //         sXml = sXml.Append(txtReserveTypeCode.Text);
   //         sXml = sXml.Append("</ReserveTypeCode>");
   //         sXml = sXml.Append("<ReserveID>");
   //         sXml = sXml.Append(txtReserveID.Text);
   //         sXml = sXml.Append("</ReserveID>");
   //         //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
   //         sXml = sXml.Append("<AssignAdjusterEid>");
   //         sXml = sXml.Append(editclaimadjusterlookup_cid.Text);
   //         sXml = sXml.Append("</AssignAdjusterEid>");
   //         //Ankit End
   //         sXml = sXml.Append("<Amount>");
   //         sXml = sXml.Append(txtEditAmount.AmountInString);
   //         sXml = sXml.Append("</Amount>");
   //         sXml = sXml.Append("<Reason>");
   //         sXml = sXml.Append(txtReasonEdit.Text);
   //         sXml = sXml.Append("</Reason>");
   //         sXml = sXml.Append("<Status>");
   //         sXml = sXml.Append(cmbStatusEdit.SelectedValue);
   //         sXml = sXml.Append("</Status>");
   //         sXml = sXml.Append("<TransSeqNum>");
   //         sXml = sXml.Append(txtTransSeqNum.Text);
   //         sXml = sXml.Append("</TransSeqNum>");
   //         //MITS:34082 MultiCurrency START

   //         sXml = sXml.Append("<CurrencyType>");
   //         sXml = sXml.Append(ReservecurrencytypetextEdit.CodeText);
   //         sXml = sXml.Append("</CurrencyType>");

   //         //MITS:34082 MultiCurrency END
   //         sXml = sXml.Append("</BOB></Document></Message>");

   //         return sXml.ToString();
   //     }
   /* bsharma33 RMA-3887 Res supp, commented following code ends */
        //Start - averma62
        /// <summary>
        /// GetVSSExportTemplate
        /// </summary>
        /// <returns></returns>
        private string GetVSSExportTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>VSSInterface.VSSExport</Function></Call><Document>");

            sXml = sXml.Append("<BOB>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(claimid.Text);
            sXml = sXml.Append("</ClaimID>");

            sXml = sXml.Append("<ClaimantEID>");
            sXml = sXml.Append(claimanteid.Text);
            sXml = sXml.Append("</ClaimantEID>");
      
            sXml = sXml.Append("<ReserveID>");
            sXml = sXml.Append(rc_row_id.Text);
            sXml = sXml.Append("</ReserveID>");

            sXml = sXml.Append("</BOB></Document></Message>");

            return sXml.ToString();
        }
        protected void VssExport_Click(object sender, ImageClickEventArgs e)
        {
            string sCWSRequest = String.Empty;
            sCWSRequest = GetVSSExportTemplate();
            sCWSOutput = AppHelper.CallCWSService(sCWSRequest);
        }

        //End - averma62

        //Added by sharishkumar for Mits 35472
        private string GetLSSExportTemplate()
        {
            
            exportToLss = hdLSSReserve.Value.ToString();            

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>ReserveFundsAdaptor.ReserveExport</Function></Call><Document>");

            sXml = sXml.Append("<BOB>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(claimid.Text);
            sXml = sXml.Append("</ClaimID>");

            sXml = sXml.Append("<ClaimantEID>");
            sXml = sXml.Append(claimanteid.Text);
            sXml = sXml.Append("</ClaimantEID>");
          
             sXml = sXml.Append("<ReserveID>");
             sXml = sXml.Append(exportToLss);
             sXml = sXml.Append("</ReserveID>");          

            sXml = sXml.Append("</BOB></Document></Message>");

            return sXml.ToString();
        }

        protected void LssExport_Click(object sender, ImageClickEventArgs e)
        {
            string sCWSRequest = String.Empty;
            exportToLss = hdLSSReserve.Value.ToString();           
            sCWSRequest = GetLSSExportTemplate();
            if (!string.IsNullOrEmpty(exportToLss))
            {
                sCWSOutput = AppHelper.CallCWSService(sCWSRequest);
            }               
                DataBind();            
        }
        /// <summary>
        /// DataBind
        /// </summary>
        private void DataBind()
        {
            GridView objGrid;
            GridRadioButton objGridRbtn;
            UpdatePanel objUpdatePanel;
            TextBox objIsDataLoaded;
            TextBox txtClaimantEID;
            XmlDocument objXmlDoc = null;
            DataRow objRow = null;
            TabPanel objTabPanel = null;
            Label lblMessage = null;
            //rupal
            ErrorControl objErrorControl = null;
            XmlNode xeHideEnhancedNotes = null;
            XmlNode xeCancelledCode = null, xeClosedCode = null; //aaggarwal29: RMA-9856
            
            bool bIsViewPermission = true;//Add by kuladeep for SMS Permission

            //tanwar2 - mits 30910 - 03112013 - start
            string sDedRecReserveCode = string.Empty;
            //tanwar2 - mits 30910 - 03112013 - end
            try
            {
                int i = TabContainer1.ActiveTabIndex;
                objXmlDoc = new XmlDocument();

                txtClaimantEID = (TextBox)(this.TabContainer1.Tabs[i].FindControl("ClaimantEID" + (i + 1).ToString()));
                claimanteid.Text = txtClaimantEID.Text;
                string slob = lob.Text;
                NonFDMCWSPageLoad("ReserveFundsAdaptor.GetClaimReservesBOB");
                oResponse = Data;
                sReturn = oResponse.OuterXml;
                objXmlDoc.LoadXml(sReturn);
                //Mgaba2: MITS 28450
                xeHideEnhancedNotes = objXmlDoc.SelectSingleNode("//HideEnhancedNotes");
                if (xeHideEnhancedNotes != null && xeHideEnhancedNotes.InnerText == "True")
                {
                    div_enhancednotes.Visible = false;
                }

                //aaggarwal29 : RMA-9856 start
                xeCancelledCode = objXmlDoc.SelectSingleNode("//CancelledShortCode");
                if(xeCancelledCode!=null)
                {
                    txtCancelledShortCode.Text = xeCancelledCode.InnerText;
                }

                xeClosedCode = objXmlDoc.SelectSingleNode("//ClosedShortCode");
                if (xeClosedCode != null)
                {
                    txtClosedShortCode.Text = xeClosedCode.InnerText;
                }
                //aaggarwal29 : RMA-9856 end

                //tanwar2 - mits 30910 - 01302013 - start
                if (string.Compare(txtApplyDedToPayments.Text,"-1") != 0)
                {
                    div_deductible.Visible = false;
                    div_deductibleEvent.Visible = false;
                }
                if (!string.IsNullOrEmpty(txtDedRecoveryReserveCode.Text))
                {
                    sDedRecReserveCode = txtDedRecoveryReserveCode.Text;
                }
                //tanwar2 - mits 30910 - 01302013 - end

                objUpdatePanel = (UpdatePanel)(this.TabContainer1.Tabs[i].FindControl("TabUpdatePanel" + (i + 1).ToString()));
                objIsDataLoaded = (TextBox)((objUpdatePanel.FindControl("IsDataLoaded" + (i + 1).ToString())));
                objIsDataLoaded.Text = "1";
                objTabPanel = this.TabContainer1.Tabs[i];

                //rupal:start, mits 27373
                objErrorControl = (ErrorControl)((objUpdatePanel.FindControl("ErrorCtl" + (i + 1).ToString())));
                objErrorControl.errorDom = sCWSOutput;
                //rupal:end

                //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
                if (objXmlDoc.SelectSingleNode("//Reserves/@EnableFirstAndFinalPayment") != null)
                {
                    hdnEnableFirstAndFinalPayment.Value = objXmlDoc.SelectSingleNode("//Reserves/@EnableFirstAndFinalPayment").InnerText;
                }
                //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298

                if (objXmlDoc.SelectSingleNode("//Reserves/@AllowManualdeductible") != null && objXmlDoc.SelectSingleNode("//Reserves/@AllowManualdeductible").InnerText.ToLower()=="false")
                {
                    AddManualDeductible.Visible = false;
                    div_ManualDeductible.Visible = false;
                }
                //Apply SMS Permission by kuladeep MITS:34720  Start
                if (objXmlDoc.SelectSingleNode("//Reserves/@IsReservePermission") != null)
                {
                    if (objXmlDoc.SelectSingleNode("//Reserves/@IsReservePermission").InnerText == "0")
                    {
                        AddReserve.Enabled = false;
                        EditReserve.Enabled = false;
                        PaymentHistory.Enabled = false;
                        bIsViewPermission = false;
                    }
                    else
                    {
                        if (objXmlDoc.SelectSingleNode("//Reserves/@IsAddPermission") != null)
                        {
                            if (objXmlDoc.SelectSingleNode("//Reserves/@IsAddPermission").InnerText == "1")
                            {
                                AddReserve.Enabled = false;
                            }
                            else
                                AddReserve.Enabled = true;
                        }

                        if (objXmlDoc.SelectSingleNode("//Reserves/@IsUpdatePermission") != null)
                        {
                            if (objXmlDoc.SelectSingleNode("//Reserves/@IsUpdatePermission").InnerText == "1")
                            {
                                EditReserve.Enabled = false;
                            }
                            else
                                EditReserve.Enabled = true;
                        }

                        if (objXmlDoc.SelectSingleNode("//Reserves/@IsViewPermission") != null)
                        {
                            if (objXmlDoc.SelectSingleNode("//Reserves/@IsViewPermission").InnerText == "1")
                            {
                                bIsViewPermission = false;
                            }
                            else
                                bIsViewPermission = true;
                        }

                        if (objXmlDoc.SelectSingleNode("//Reserves/@IsPaymentHistPermission") != null)
                        {
                            if (objXmlDoc.SelectSingleNode("//Reserves/@IsPaymentHistPermission").InnerText == "1")
                            {
                                PaymentHistory.Enabled = false;
                            }
                            else
                                PaymentHistory.Enabled = true;
                        }
                        if (objXmlDoc.SelectSingleNode("//Reserves/@IsMoveToPermission") != null)
                        {
                            if (objXmlDoc.SelectSingleNode("//Reserves/@IsMoveToPermission").InnerText == "1")
                            {
                                MoveFinancials.Visible = false;
                            }
                            else
                                MoveFinancials.Visible = true;
                        }
                    }
                }
                //Apply SMS Permission by kuladeep MITS:34720 End

                objGridRbtn = ((GridRadioButton)(objUpdatePanel.FindControl("Grid" + (i + 1).ToString())));
                objGrid = ((GridView)(objGridRbtn.FindControl("GridView1")));

                DataTable objTable = new DataTable("Reserves");

                objTable.Columns.Add("PolicyID");
                objTable.Columns.Add("PolicyUnitRowID");
                objTable.Columns.Add("CoverageID");
                objTable.Columns.Add("CoverageLossID");
                objTable.Columns.Add("ReserveID");
                objTable.Columns.Add("ReserveTypeCode");
                objTable.Columns.Add("Policy");
                objTable.Columns.Add("Unit");
                objTable.Columns.Add("CoverageType");
                objTable.Columns.Add("LossType");
                objTable.Columns.Add("ReserveType");
                objTable.Columns.Add("ReserveAmount");
                objTable.Columns.Add("Balance");
                objTable.Columns.Add("Incurred");
                objTable.Columns.Add("Paid");
                objTable.Columns.Add("Collection");                
                objTable.Columns.Add("PreventCollOnRes"); //JIRA-857
                objTable.Columns.Add("Status");
                //rupal
                objTable.Columns.Add("CovgSeqNum");
                objTable.Columns.Add("LossTypeCode");
                //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
                objTable.Columns.Add("ReserveSubType");
                objTable.Columns.Add("disabilitycatdesc");
                //prashbiharis MITS : 30949
                objTable.Columns.Add("Reason");
                objTable.Columns.Add("StatusDetail");
                objTable.Columns.Add("RsvStatusParent");
                //prashbiharis MITS : 30949
                //Ankit End
                objTable.Columns.Add("AdjusterDetails");    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                //MITS 35453 starts
                objTable.Columns.Add("ClmCurrBalAmount");
                objTable.Columns.Add("ClmCurrResAmount");
                //MITS 35453 ends
                objTable.Columns.Add("TransSeqNum");
                objTable.Columns.Add("CoverageKey");            //Ankit Start : Worked on MITS - 34297
                //Added by sharishkumar for Mits 35472
                objTable.Columns.Add("LSSResExpFlag");
                objTable.Columns.Add("MasterReserveType");
                //End Mits 35472
                objTable.Columns.Add("DedTypeCode");
                objTable.Columns.Add("DedRcRowId");

                foreach (XmlNode objNode in objXmlDoc.SelectNodes("//Reserves/Reserve"))
                {
                    objRow = objTable.NewRow();
                    objRow["PolicyID"] = objNode.Attributes["policyid"].Value;
                    objRow["PolicyUnitRowID"] = objNode.Attributes["policyunitrowid"].Value;
                    objRow["CoverageID"] = objNode.Attributes["polcvgrowid"].Value;
                    objRow["CoverageLossID"] = objNode.Attributes["polcvglossrowid"].Value;
                    objRow["LossType"] = objNode.Attributes["losstypename"].Value;
                    objRow["ReserveID"] = objNode.Attributes["reserveid"].Value;
                    objRow["ReserveTypeCode"] = objNode.Attributes["reservetypecode"].Value;
                    objRow["Policy"] = objNode.Attributes["policyname"].Value;
                    objRow["Unit"] = objNode.Attributes["unit"].Value;
                    objRow["CoverageType"] = objNode.Attributes["coveragename"].Value;
                    objRow["ReserveType"] = objNode.Attributes["reservename"].Value;
                    //rupal
                    //pgupta93: RMA-7078 START
                    //objRow["Status"] = objNode.Attributes["reservestatus"].Value;                            //akaur9 10/21/2011 MITS 26081                  
                    objRow["Status"] = objNode.Attributes["reservestatusdesc"].Value;                          //akaur9 10/21/2011 MITS 26081
                    //pgupta93: RMA-7078 END
                    objRow["ReserveAmount"] = objNode.Attributes["reserveamount"].Value;
                    objRow["Balance"] = objNode.Attributes["balance"].Value;
                    objRow["Paid"] = objNode.Attributes["paid"].Value;
                    objRow["Incurred"] = objNode.Attributes["incurred"].Value;
                    objRow["Collection"] = objNode.Attributes["collected"].Value;                    
                    //JIRA-857 Start
                    objRow["PreventCollOnRes"] = objNode.Attributes["preventcollonres"].Value;
                    //JIRA-857 Ends
                    //rupal
                    objRow["CovgSeqNum"] = objNode.Attributes["covgseqnum"].Value;
                    objRow["LossTypeCode"] = objNode.Attributes["losstypecode"].Value;
                    //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
                    objRow["ReserveSubType"] = objNode.Attributes["reservecatdesc"].Value;
                    //Ankit End
                    objRow["disabilitycatdesc"] = objNode.Attributes["disabilitycatdesc"].Value;
                    //prashbiharis MITS : 30949
                    objRow["Reason"] = objNode.Attributes["reason"].Value;
                    objRow["StatusDetail"] = objNode.Attributes["reservestatusdetails"].Value;
                    objRow["RsvStatusParent"] = objNode.Attributes["RsvStatusParent"].Value;
                    //MITS 35453 starts
                    objRow["ClmCurrBalAmount"] = objNode.Attributes["clmcurrbalamount"].Value;
                    objRow["ClmCurrResAmount"] = objNode.Attributes["clmcurrresamount"].Value;
                    //MITS 35453 ends
                    objRow["AdjusterDetails"] = objNode.Attributes["adjusterdetails"].Value;   //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                    
                    //prashbiharis MITS : 30949
                    objRow["TransSeqNum"] = objNode.Attributes["transseqnum"].Value;
                    objRow["CoverageKey"] = objNode.Attributes["CoverageKey"].Value;        //Ankit Start : Worked on MITS - 34297
                    //Added by sharishkumar for Mits 35472
                    objRow["LSSResExpFlag"] = objNode.Attributes["lssresexpflag"].Value;                   
                    objRow["MasterReserveType"] = objNode.Attributes["reservetype"].Value;
                    //End Mits 35472

                    objRow["DedTypeCode"] = objNode.Attributes["DedTypeCode"].Value;
                    objRow["DedRcRowId"] = objNode.Attributes["DedRcRowId"].Value;
                    //tanwar2 - mits 30910 - 03112013 - start
                    //objTable.Rows.Add(objRow);
                    if (String.Compare(objNode.Attributes["reservetypecode"].Value, sDedRecReserveCode, false)!=0)
                    {
                        objTable.Rows.Add(objRow);
                    }
                    //tanwar2 - mits 30910 - 03112013 - end

                }

                ListItem objItem = null;
                objItem = new ListItem();
                objItem.Text = "";
                objItem.Value = "";
                //Deb MITS 28251
                cmbStatusAdd.Items.Clear();
                cmbStatusEdit.Items.Clear();
                cmbReasonAdd.Items.Clear();
                cmbReasonEdit.Items.Clear();
                //Deb MITS 28251
                cmbReasonAdd.Items.Add(objItem);
                cmbReasonEdit.Items.Add(objItem);
                //Populating Reason combo boxes
                foreach (XmlNode objNode in objXmlDoc.SelectNodes("//Reserves/Reason/ReasonCode"))
                {
                    objItem = new ListItem();
                    objItem.Value = objNode.Attributes["value"].Value;
                    objItem.Text = objNode.InnerText;
                    cmbReasonAdd.Items.Add(objItem);
                    cmbReasonEdit.Items.Add(objItem);
                }
                objItem = new ListItem();
                objItem.Text = "";
                objItem.Value = "";
                //Ankit Start : Financial Enhancement - Status Code Changes
                objItem.Attributes.Add("Tag", "");
                //Ankit End
                cmbStatusAdd.Items.Add(objItem);
                cmbStatusEdit.Items.Add(objItem);
                //Populating Status combo boxes
                foreach (XmlNode objNode in objXmlDoc.SelectNodes("//Reserves/Status/StatusCode"))
                {
                    objItem = new ListItem();
                    objItem.Value = objNode.Attributes["value"].Value;
                    objItem.Text = objNode.InnerText;
                    //Ankit Start : Financial Enhancement - Status Code Changes
                    objItem.Attributes.Add("Tag", objNode.Attributes["shortcode"].Value.ToUpper());
                    //Ankit End
                    
                    //rupal:first & final payment enhancement
                    //first and final status should not be added in the reserve_status dropdown
                    //rupal:start, mits 33043
                    if (objNode.Attributes["shortcode"] != null)
                    {
                        //add all reserve status excep first & final & Cancel
                        if (!objNode.Attributes["shortcode"].Value.ToUpper().Equals("F") && !objNode.Attributes["shortcode"].Value.ToUpper().Equals("CN"))
                            cmbStatusAdd.Items.Add(objItem);
                        if  (!objNode.Attributes["shortcode"].Value.ToUpper().Equals("F") )
                            cmbStatusEdit.Items.Add(objItem);                   
                    }
                    else
                    {
                        cmbStatusAdd.Items.Add(objItem);
                        cmbStatusEdit.Items.Add(objItem);                   
                    }
                    //rupal:end                    
                    //cmbStatusEdit.Items.Add(objItem);                   
                    //rupal:end
                }

                if (objTable.Rows.Count == 0 || bIsViewPermission == false)//Add OR condition for View Permission as per SMS Setting by kuladeep for MITS:34720
                {
                    if (!bIsViewPermission)//Add IF condition for View Permission as per SMS Setting by kuladeep for MITS:34720
                    {
                        lblMessage = ((Label)(objUpdatePanel.FindControl("Label" + (i + 1).ToString()))); //.Controls.Add(lblMessage); 
                        lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;                    " + RMXResourceProvider.GetSpecificObject("lblNoResPermission", RMXResourceProvider.PageId("ReserveListingBOB.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) + "</h3></p>";

                    }
                    else
                    {
                        lblMessage = ((Label)(objUpdatePanel.FindControl("Label" + (i + 1).ToString()))); //.Controls.Add(lblMessage); 
                        lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;                    " + RMXResourceProvider.GetSpecificObject("lblNoResForClaimaint", RMXResourceProvider.PageId("ReserveListingBOB.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) + "</h3></p>";
                    }
                }
                else
                {
                    hdncount.Value = Convert.ToString( objTable.Rows.Count); //igupta3 Mits# 33301
                    objGrid.DataSource = objTable;
                    objGrid.DataBind();
                    lblMessage = ((Label)(objUpdatePanel.FindControl("Label" + (i + 1).ToString()))); //.Controls.Add(lblMessage); 
                    lblMessage.Text = "";
                }
                // MITS 35453 -commented code 
                //Deb MITS 28251
                //if (this.FindControl("drdCurrencytype") != null)
                //{
                //    DropDownList drpCurr = (DropDownList)this.FindControl("drdCurrencytype");
                //    for (int j = 0; j < drpCurr.Items.Count; j++)
                //    {
                //        if (!drpCurr.Items[j].Value.Contains("~~~"))
                //        {
                //            drpCurr.Items[j].Value += "~~~" + drpCurr.Items[j].Text;
                //        }
                //    }
                //}
                //Deb MITS 28251
                // MITS 35453 -commented code 
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                objXmlDoc = null;
                objRow = null;

            }
        }

        protected void UpdatePanel_Load(object sender, EventArgs e)
        {
            int iCount = 0;

            try
            {
                int iActiveTabIndex = TabContainer1.ActiveTabIndex;

                //UpdatePanel_Load method will be called for all UpdatePanels
                //so restricting it to update panel for the currently selected (active) tab
                string id = ((UpdatePanel)(sender)).ID;

                id = id.Substring(11); //Igupta3 Mits: 32220

                if ((iActiveTabIndex + 1).ToString() != id)
                    return;

                TextBox txtSelectedRowID = (TextBox)(((UpdatePanel)(sender)).FindControl("SelectedId" + (id).ToString()));
                string sSelectedID = txtSelectedRowID.Text;
                if (sSelectedID == "-1")
                {
                    EditLoad.Text = "false"; //preeti:jira 341
                    return;
                }
                else EditLoad.Text = "true"; //preeti:jira 341

                TextBox txtClaimantEID = (TextBox)(((UpdatePanel)(sender)).FindControl("ClaimantEID" + (id).ToString()));
                claimanteid.Text = txtClaimantEID.Text;

                GridView objPayeeGrid = ((GridView)(((UpdatePanel)(sender)).FindControl("PayeeGrid" + (id).ToString())));
                if (objPayeeGrid != null)
                {
                    iCount = PaymentsDataBind(objPayeeGrid, iActiveTabIndex, sSelectedID);
                    Label objLabel = ((Label)(((UpdatePanel)(sender)).FindControl("LabelPayments" + (id).ToString())));
                    //rupal
                    objLabel.Font.Bold = true;
                    if (iCount == 0)
                    {
                        objLabel.Text = "";
                    }
                    else
                    {
                        //pgupta93: RMA-7113 START
                        //objLabel.Text = "Payments of the selected Reserve";
                        objLabel.Text = RMXResourceProvider.GetSpecificObject("lblTransactionDetailonReserve", RMXResourceProvider.PageId("ReserveListingBOB.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                        //pgupta93: RMA-7113 END
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
            }

        }

        /// <summary>
        /// PaymentsDataBind
        /// </summary>
        /// <param name="objPayeeGrid"></param>
        /// <param name="p_iActiveTabIndex"></param>
        /// <param name="p_sSelectedID"></param>
        private int PaymentsDataBind(GridView objPayeeGrid, int p_iActiveTabIndex, string p_sSelectedID)
        {
            DataRow objRow;
            DataTable objTable = null;
            int iPolicyID = 0;
            int iPolicyCvgRowID = 0;
            int iReserveTypeCode = 0;
            int iRC_RowID = 0;
            int iCvgLossId = 0;
            int iCollAlertonRes = 0; //JIRA-857
        
            string[] arrIDs = null;
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            XmlNodeList objNodeList = null;
            XmlNode objXmlNode = null;
            XmlDocument objDocument = null;
            string sCWSresponse = "";
            int iCount = 0;

            try
            {
                arrIDs = p_sSelectedID.Split('|');
                if (arrIDs != null)
                {
                    iPolicyID = int.Parse(arrIDs[0]);
                    iPolicyCvgRowID = int.Parse(arrIDs[1]);
                    iReserveTypeCode = int.Parse(arrIDs[2]);
                    iRC_RowID = int.Parse(arrIDs[3]);
                    iCvgLossId = int.Parse(arrIDs[4]);
                }
                else
                {
                    return 0;
                }

                XmlTemplate = GetMessageTemplate(iPolicyID, iPolicyCvgRowID, iReserveTypeCode, iRC_RowID, iCvgLossId);

                bReturnStatus = CallCWS("ReserveFundsAdaptor.GetPaymentsBOB", XmlTemplate, out sCWSresponse, false, false);

                objDocument = new XmlDocument();
                objDocument.LoadXml(sCWSresponse);

                objNodeList = objDocument.SelectNodes("//Payments/Payment");

                objTable = new DataTable("Payments");
                objPayeeGrid.ShowHeader = true;
                objTable.Columns.Add("PAYEE_NAME");
                objTable.Columns.Add("CHECK_NUMBER");
                objTable.Columns.Add("DATE_OF_CHECK");
                //pgupta93: RMA-7113 START
                objTable.Columns.Add("Type");
                //pgupta93: RMA-7113 END
                objTable.Columns.Add("TRANSACTION_TYPE");
                objTable.Columns.Add("AMOUNT");
                objTable.Columns.Add("STATUS_CODE");
                //Ankit Start : Worked on MITS - 32120
                objTable.Columns.Add("TOTAL_PAYEES");
                //Ankit End
                //RUPAL Start : MITS - 33152
                objTable.Columns.Add("VOID_FLAG");
                //RUPAL End
                //Added by Swati agarwal for MITS # 33431
                objTable.Columns.Add("STOP_PAY_FLAG");
                //change end here by swati
                objTable.Columns.Add("CLEARED_FLAG");//MITS 33239 asharma326
                //START -  added by Nikhil on 08/04/14. Check Total field added.
                // Added by Amitosh for JIRA 9659. Hiding Check total when apply deductibles is false. since check total is not calculated when check is printed from print check module
                //code needs to be reverted.
                bApplyDeductibles = objDocument.SelectSingleNode("//Payments").Attributes["ApplyDeductibles"].Value.Equals("True", StringComparison.InvariantCultureIgnoreCase);
                if (bApplyDeductibles)
                {
                    objTable.Columns.Add("CHECK_TOTAL");
                }
                //end amitosh
                //End -  added by Nikhil on 08/04/14. Check Total field added.
                objTable.Columns.Add("CONTROL_NUMBER");

                foreach (XmlNode objNode in objNodeList)
                {
                    objRow = objTable.NewRow();
                    objRow["PAYEE_NAME"] = objNode.Attributes["payeename"].Value;
                    objRow["CHECK_NUMBER"] = objNode.Attributes["checknumber"].Value;
                    objRow["DATE_OF_CHECK"] = objNode.Attributes["dateofcheck"].Value;
                    //pgupta93: RMA-7113 START
                    objRow["Type"] = objNode.Attributes["type"].Value;
                    //pgupta93: RMA-7113 END
                    objRow["TRANSACTION_TYPE"] = objNode.Attributes["transactiontype"].Value;
                    objRow["AMOUNT"] = objNode.Attributes["amount"].Value;
                    objRow["STATUS_CODE"] = objNode.Attributes["statuscode"].Value;
                    //Ankit Start : Worked on MITS - 32120
                    objRow["TOTAL_PAYEES"] = objNode.Attributes["totalpayees"].Value;
                    //Ankit End
                    //rupal Start : Worked on MITS - 33152
                    objRow["VOID_FLAG"] = objNode.Attributes["voidflag"].Value;
                    //rupal End
                    //Added by Swati Aggarwal for MITS # 33431
                    objRow["STOP_PAY_FLAG"] = objNode.Attributes["stoppayflag"].Value;
                    //end by swati
                    objRow["CLEARED_FLAG"] = objNode.Attributes["clearedflag"].Value;//MITS 33239 asharma326
                    //START -  added by Nikhil on 08/04/14. Check Total field added.
                    // Added by Amitosh for JIRA 9659. Hiding Check total when apply deductibles is false. since check total is not calculated when check is printed from print check module
                    //code needs to be reverted.
                    if (bApplyDeductibles)
                    {
                        objRow["CHECK_TOTAL"] = objNode.Attributes["checktotal"].Value;
                    }
                    //End -  added by Nikhil on 08/04/14. Check Total field added.
                    objRow["CONTROL_NUMBER"] = objNode.Attributes["ctrlnumber"].Value;
                    objTable.Rows.Add(objRow);
                }


                objPayeeGrid.DataSource = objTable;
                objPayeeGrid.DataBind();
               
                   
                iCount = objTable.Rows.Count;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                objRow = null;
                objTable = null;
                arrIDs = null;
                XmlTemplate = null;
                objNodeList = null;
                objXmlNode = null;
                objDocument = null;
            }

            return iCount;
        }

        protected void grdPayee_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Payee Name";
                e.Row.Cells[1].Text = "Check Number";
                e.Row.Cells[2].Text = "Date of Check";
                e.Row.Cells[3].Text = "Type";//pgupta93: RMA-7113
                e.Row.Cells[4].Text = "Transaction Type";
                e.Row.Cells[5].Text = "Amount";
                e.Row.Cells[6].Text = "Status";
                e.Row.Cells[8].Text = "Void";//rupal:mits 33152
                e.Row.Cells[9].Text = "Stop Pay";//swati:mits 33431
                e.Row.Cells[10].Text = "Cleared";//asharma326 MITS 33239
                //START -  added by Nikhil on 08/04/14. Check Total field added.
                // Added by Amitosh for JIRA 9659. Hiding Check total when apply deductibles is false. since check total is not calculated when check is printed from print check module
                //code needs to be reverted.
                if (bApplyDeductibles)
                {
                    e.Row.Cells[11].Text = "Check Total";//asharma326 MITS 33239
                    e.Row.Cells[12].Text = "Control Number";
                }
                else
                {
                    e.Row.Cells[11].Text = "Control Number";
                }
                //End -  added by Nikhil on 08/04/14. Check Total field added.

                // e.Row.CssClass = "";
            }
            //Ankit Start : Worked on MITS - 32120
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TableCell tableCell = e.Row.Cells[0];
                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                lbl.Text = tableCell.Text;
                tableCell.Controls.Add(lbl);

                System.Web.UI.WebControls.Image img = new System.Web.UI.WebControls.Image();
                string imageUrl = "~/Images/secUsersNode.gif";
                img.ImageUrl = imageUrl;
                img.AlternateText = Server.HtmlDecode(e.Row.Cells[7].Text);
                img.ToolTip = Server.HtmlDecode(e.Row.Cells[7].Text);
                img.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px");
                tableCell.Wrap = false;
                tableCell.Controls.Add(img);
            }
            e.Row.Cells[7].Visible = false;
            //Ankit End
        }

        private XElement GetMessageTemplate(int p_iPolicyID, int p_iPolCvgID, int p_iReserveTypeCode,int p_iRC_RowID,int p_iCvgLossId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");

            sXml = sXml.Append("<Payments>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(claimid.Text);
            sXml = sXml.Append("</ClaimID>");
            sXml = sXml.Append("<ClaimantEID>");
            sXml = sXml.Append(claimanteid.Text);
            sXml = sXml.Append("</ClaimantEID>");
            sXml = sXml.Append("<PolicyID>");
            sXml = sXml.Append(p_iPolicyID.ToString());
            sXml = sXml.Append("</PolicyID>");
            sXml = sXml.Append("<PolCvgID>");
            sXml = sXml.Append(p_iPolCvgID.ToString());
            sXml = sXml.Append("</PolCvgID>");
            sXml = sXml.Append("<ReserveTypeCode>");
            sXml = sXml.Append(p_iReserveTypeCode.ToString());
            sXml = sXml.Append("</ReserveTypeCode>");
            sXml = sXml.Append("<RCRowID>");
            sXml = sXml.Append(p_iRC_RowID.ToString());
            sXml = sXml.Append("</RCRowID>");
            sXml = sXml.Append("<CvgLossId>");
            sXml = sXml.Append(p_iCvgLossId.ToString());
            sXml = sXml.Append("</CvgLossId>");
            //rupal:start, r8 multicurrency
            sXml = sXml.Append("<CurrencyType>");
            if (drdCurrencytype.Items.Count > 0)
                sXml = sXml.Append(drdCurrencytype.SelectedItem.Value.Split(sDataSeparator,StringSplitOptions.None)[0]);//Deb MITS 28251
            else
                sXml = sXml.Append("0");
            sXml = sXml.Append("</CurrencyType>");
            //rupal:end
            sXml = sXml.Append("</Payments></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public static CascadingDropDownNameValue[] GetDropDownContentsPageMethod(string knownCategoryValues, string category)
        {
            //return new CarsService().GetDropDownContents(knownCategoryValues, category);
            // Get a dictionary of known category/value pairs
            StringDictionary knownCategoryValuesDictionary = AjaxControlToolkit.CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

            //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
            if (string.Equals(category, "reservesubtype", StringComparison.OrdinalIgnoreCase) && knownCategoryValuesDictionary["reserve"] != null)
                iReserveID = Convert.ToInt32(knownCategoryValuesDictionary["reserve"]);
            //Ankit End

            // Perform a simple query against the data document
            //return AjaxControlToolkit.CascadingDropDown.QuerySimpleCascadingDropDownDocument(Document, Hierarchy, knownCategoryValuesDictionary, category, InputValidationRegex);
            // Perform a simple query against the data document
            return AjaxControlToolkit.CascadingDropDown.QuerySimpleCascadingDropDownDocument(GetDocument(), Hierarchy, knownCategoryValuesDictionary, category);
        }

        protected void PaymentHistory_Click(object sender, ImageClickEventArgs e)
        {

            string sRedirectString = String.Format("../Funds/PaymentHistory.aspx?ClaimId={0}&ClaimantEid={1}&ClaimantRowId={2}&UnitId={3}&UnitRowId={4}&SubTitle={5}&FrozenFlag={6}&ClaimNumber={7}&Caption={8}&CheckMode=1", claimid.Text, claimanteid.Text, claimantrowid.Text, "", "", "", "", claimnumber.Text, "");
            //Server.Transfer(sRedirectString);
            //Neha changed to Response. Redirect as telrik gives an issue on filter.
            Response.Redirect(sRedirectString);

        }
        protected void ReserveHistory_Click(object sender, ImageClickEventArgs e)
        {
            string sRedirectString = String.Format("../Reserves/ViewReserveDetail.aspx");
            Server.Transfer(sRedirectString);
        }
        /* bsharma33 RMA-3887 Res supp, commented following code starts */
        //protected void AddMore_Click(object sender, EventArgs e)
        //{
        //    int i = TabContainer1.ActiveTabIndex;
        //   txtActionAddMore.Text = string.Empty;
        //    txtAmount.Amount = 0;
        //    txtReasonAdd.Text = "";
        //    if (lob.Text.CompareTo("243") == 0)
        //    {
        //        CascadingDropDown5.SelectedValue = string.Empty;
        //        if (disabilitytype != null)
        //        {
        //            ((TextBox)this.FindControl("disabilitytype$codelookup")).Text = "";
        //            ((TextBox)this.FindControl("disabilitytype$codelookup_cid")).Text = "";
        //        }
        //    }
        //    else
        //    {
        //        CascadingDropDown4.SelectedValue = string.Empty;
        //    }
        //    CascadingDropDown6.SelectedValue = string.Empty;
        //    if (this.FindControl("claimadjusterlookup") != null)
        //    {
        //        ((TextBox)this.FindControl("claimadjusterlookup")).Text = "";
        //        ((TextBox)this.FindControl("claimadjusterlookup_cid")).Text = "";
        //    }

        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "SetDefaultValues(); dropDownChangeflag = true;", true);
        //    if (i != -1)
        //    {
        //        UpdatePanel objUpdatePanel = (UpdatePanel)(this.TabContainer1.Tabs[i].FindControl("TabUpdatePanel" + (i + 1).ToString()));
        //        TextBox txtaction = (TextBox)((objUpdatePanel.FindControl("txtAction" + (i + 1).ToString())));
        //        if (txtaction != null)
        //            txtaction.Text = "";
        //        //if (objUpdatePanel != null)
        //        //    objUpdatePanel.Update();                
        //        ErrorControl ErrorControl1 = (ErrorControl)((objUpdatePanel.FindControl("ErrorCtl" + (i + 1).ToString())));
        //        if (ErrorControl1 != null)
        //        {
        //            if (ErrorControl1.errorhandle != string.Empty)
        //            {
        //                //AddReserveModalPopup.Hide();
        //                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "SetCulture();", true);//MITS:34082
        //            }
        //        }

        //    }

        //}
        /* bsharma33 RMA-3887 Res supp, commented following code ends */
		 //tanwar2 - NIS
        protected void Deductible_Click(object sender, ImageClickEventArgs e)
        {
            string sRedirectString = String.Format("../Deductible/DeductibleGrid.aspx");
            Server.Transfer(sRedirectString);
        }

        protected void DeductibleEvent_Click(object sender, ImageClickEventArgs e)
        {
            string sRedirectString = String.Format("../Deductible/DeductibleEventSummary.aspx");
            Server.Transfer(sRedirectString);
        }
    }
}