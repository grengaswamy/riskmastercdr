﻿/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            
**********************************************************************************************
* 15/03/2014 | 34275  | abhadouria | RMA Swiss Re Financial Summary
**********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;

namespace Riskmaster.UI.UI.Reserves
{
    public partial class ReserveSummary : NonFDMBasePageCWS
    {
        private XmlDocument oResponse = null;
        private string sReturn = String.Empty;
        ArrayList arrIndex = null;
        bool m_bReadOnly = false;
        // akaushik5 Added for MITS 33577 Starts
        public string sReserveSummaryLevel = string.Empty;
        // akaushik5 Added for MITS 33577 Ends
        public int selvalue = 0; //MITS: 34275RMA Swiss Re Financial Summary
       

        protected void Page_Load(object sender, EventArgs e)
        {
            oResponse = new XmlDocument();
            Selected.Text = drdFS.SelectedValue;    // MITS 34275- RMA Swiss Re Financial Summary

            if (!Page.IsPostBack)
            {
                try
                {
                    sReserveSummaryLevel = AppHelper.GetFormValue("txtReserveSummaryLevel");
                    FormLevel.Text = sReserveSummaryLevel;

                    // MITS 34275- RMA Swiss Re Financial Summary START
                    claimid.Text = AppHelper.GetFormValue("ClaimId");
                    PolicyId.Text = AppHelper.GetQueryStringValue("PolicyID");
                    CoverageId.Text = AppHelper.GetQueryStringValue("CoverageID");
                    UnitId.Text = AppHelper.GetQueryStringValue("UnitID");
                    Multicurrencyindicator.Text = AppHelper.GetQueryStringValue("UseMulticurrency");
                    claimcurrency.Text = AppHelper.GetQueryStringValue("ClaimCurrency");
                    //Ashish Ahuja - MITS 34275 - Display header and display label currency type start
                    PolicyName.Text = AppHelper.GetQueryStringValue("Policy");
                    Unit.Text = AppHelper.GetQueryStringValue("Unit");
                    Coverage.Text = AppHelper.GetQueryStringValue("Coverage");
                 
                    Selected.Text = drdFS.SelectedValue;
                    
                    if (Multicurrencyindicator.Text == "-1")
                    {
                        drdCurrencytype.Visible = true;
                        lblCurrencytype.Visible = true;
                    }
                    else
                    {
                        drdCurrencytype.Visible = false;
                        lblCurrencytype.Visible = false;
                    }
                    //Ashish Ahuja - MITS 34275 - Display header and display label currency type stop
                    if (PolicyId.Text != "")
                    {
                        getPolicyUnitCoveragedata();
                        //Ashish Ahuja - MITS 34275 - Display header
                        if(caption.Text != "")
                        lblcaption.Text = caption.Text.Substring(0,caption.Text.Length-1)+" * "+ PolicyName.Text+" ]";
                
                    }


                    else
                    {
                        Level.Visible = false;
                        drdFS.Visible = false;

                        // MITS 34275- RMA Swiss Re Financial Summary END
                    // akaushik5 Commented for MITS 33577 Starts
                    //caption.Text = AppHelper.GetFormValue("caption");
                    // akaushik5 Commented for MITS 33577 Ends
                    claimid.Text = AppHelper.GetFormValue("claimid");
                 //   claimantrowid.Text = AppHelper.GetQueryStringValue("ClaimantId");
                  //  claimanteid.Text = AppHelper.GetQueryStringValue("ClaimantEId");
                    // akaushik5 Changed for MITS 33577 Starts
                    //NonFDMCWSPageLoad("ReserveFundsAdaptor.GetBOBReserves");
                    
                    // When screen displayed in pop-up window claimid and claimantid 
                    // should be populated from query string.
                    // Back button won't be available in pop-up window.
                    if (string.IsNullOrEmpty(claimid.Text))
                    {
                        claimid.Text = AppHelper.GetQueryStringValue("claimid");
                        claimanteid.Text = AppHelper.GetQueryStringValue("claimantid");
                        btnBack.Visible = false;
                        btnClose.Visible = true;
                    }

                    if (!string.IsNullOrEmpty(sReserveSummaryLevel) && sReserveSummaryLevel.Equals("claim"))
                    {
                        NonFDMCWSPageLoad("ReserveFundsAdaptor.GetClaimReserveSummaryBOB");
                    }
                    else
                    {
                        tblClaimant.Visible = false;
                        NonFDMCWSPageLoad("ReserveFundsAdaptor.GetBOBReserves");
                    }
                    // akaushik5 Changed for MITS 33577 Ends
                    
                    oResponse = Data;
                    sReturn = oResponse.OuterXml;

                    XElement objOuterXml = XElement.Parse(sReturn);

                    if (objOuterXml != null)
                    {
                        XElement objClaimReserves = null;
                        objClaimReserves = objOuterXml.XPathSelectElement("./ReserveFunds/claimreserves");

                        if (objClaimReserves != null)
                        {
                            claimid.Text = objClaimReserves.Attribute("claimid").Value;
                           
                            claimnumber.Text = objClaimReserves.Attribute("claimnumber").Value;
                          //  frozenflag.Text = objClaimReserves.Attribute("paymentfrozenflag").Value;
                        }

                        XElement objReserves = null;
                        objReserves = objOuterXml.XPathSelectElement("./ReserveFunds/claimreserves/reserves");

                        if (objReserves != null)
                        {
                            if (claimanteid.Text != "")
                            {
                                claimanteid.Text = objReserves.Attribute("claimant").Value;
                            }
                        }
                    }
                   BindDataToXml(sReturn);
                    // akaushik5 Added for MITS 33577 Starts
                   if (!string.IsNullOrEmpty(sReserveSummaryLevel) && sReserveSummaryLevel.Equals("claim"))
                   {
                       BindClaimantFromXML(sReturn);
                   }
                   // akaushik5 Added for MITS 33577 Ends

                   lblcaption.Text = caption.Text;
                   // CustomizePage();
                    }
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }
        // MITS 34275- RMA Swiss Re Financial Summary START

        protected void getPolicyUnitCoveragedata()
        {
            try
            {

                tblClaimant.Visible = false;
                lblClaimants.Visible = false;
                Level.Visible = true;
                drdFS.Visible = true;
                //previouscurrencytype.Text = selectedcurrencytype.Text;
                
                if (FormLevel.Text=="Policy")
                {

                    NonFDMCWSPageLoad("ReserveFundsAdaptor.GetPolicyUnitCoverageSummary");

                }


                else
                {
                    tblClaimant.Visible = false;
                    NonFDMCWSPageLoad("ReserveFundsAdaptor.GetBOBReserves");
                }
                oResponse = Data;
                sReturn = oResponse.OuterXml;

                XElement objOuterXml = XElement.Parse(sReturn);

                if (objOuterXml != null)
                {
                    XElement objClaimReserves = null;
                    objClaimReserves = objOuterXml.XPathSelectElement("./ReserveFunds/claimreserves");

                    if (objClaimReserves != null)
                    {
                        claimid.Text = objClaimReserves.Attribute("claimid").Value;
                        claimnumber.Text = objClaimReserves.Attribute("claimnumber").Value;
                        //  frozenflag.Text = objClaimReserves.Attribute("paymentfrozenflag").Value;
                    }

                    XElement objReserves = null;
                    objReserves = objOuterXml.XPathSelectElement("./ReserveFunds/claimreserves/reserves");

                    if (objReserves != null)
                    {
                        if (claimanteid.Text != "")
                        {
                            claimanteid.Text = objReserves.Attribute("claimant").Value;
                        }
                    }
                }
                BindDataToXml(sReturn);
          previouscurrencytype.Text = selectedcurrencytype.Text;
            }
            catch (Exception ee)    
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void drdFS_onchange(object sender, EventArgs e)
        {
            try
            {

                tblClaimant.Visible = false;
                lblClaimants.Visible = false;
                Level.Visible = true;
                drdFS.Visible = true;
                //Ashish Ahuja - Gap 17 changes
                selvalue = drdCurrencytype.SelectedIndex;
                getPolicyUnitCoveragedata();
                drdCurrencytype.SelectedIndex = selvalue;
                
                //Ashish Ahuja - MITS 34275 - Display header start
                if (caption.Text != "")
                {
                    if (drdFS.SelectedIndex == 0)
                    {
                        lblcaption.Text = caption.Text.Substring(0, caption.Text.Length - 1) + " * " + PolicyName.Text + " ]";
                    }
                    else if (drdFS.SelectedIndex == 1)
                    {
                        lblcaption.Text = caption.Text.Substring(0, caption.Text.Length - 1) + " * " + PolicyName.Text + " * " + Unit.Text + " ]";
                    }
                    else if (drdFS.SelectedIndex == 2)
                    {
                        lblcaption.Text = caption.Text.Substring(0, caption.Text.Length - 1) + " * " + PolicyName.Text + " * " + Unit.Text + " * " + Coverage.Text + " ]";
                    }
                }
                //Ashish Ahuja - MITS 34275 - Display header stop
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void drdCurrencytype_onchange(object sender, EventArgs e)
        {
            try
            { 
                if (previouscurrencytype.Text == "")
                {
                    previouscurrencytype.Text = claimcurrency.Text;
                }
                selvalue= drdCurrencytype.SelectedIndex;
                selectedcurrencytype.Text = drdCurrencytype.SelectedValue;
                getPolicyUnitCoveragedata();
                drdCurrencytype.SelectedIndex =selvalue;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        // MITS 34275- RMA Swiss Re Financial Summary END
        protected void grdReserve_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                    e.Row.CssClass = "colheader3";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void grdReserve_RowDataBound(object Sender, GridViewRowEventArgs e)
        {
            try
            {
                int iTempIndex = 0;
                int iPos = 0;
                string sTemp = string.Empty;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView objDataRow = (DataRowView)e.Row.DataItem;

                    e.Row.ID = objDataRow["pid"].ToString();

                    e.Row.Cells[0].Style.Add("text-align", "left");
                    e.Row.Cells[1].Style.Add("text-align", "right");
                    e.Row.Cells[2].Style.Add("text-align", "right");
                    e.Row.Cells[3].Style.Add("text-align", "right");
                    e.Row.Cells[4].Style.Add("text-align", "right");
                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                    iPos = 0;
                    sTemp = e.Row.Cells[0].Text;
                    iPos = sTemp.IndexOf("Totals", 0);
                    if (iPos > 0)
                    {
                        e.Row.Cells[0].Font.Bold = true;
                        e.Row.Cells[1].Font.Bold = true;
                        e.Row.Cells[2].Font.Bold = true;
                        e.Row.Cells[3].Font.Bold = true;
                        e.Row.Cells[4].Font.Bold = true;
                        e.Row.Cells[5].Font.Bold = true;
                    }
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    arrIndex = new ArrayList();

                    foreach (TableCell cell in e.Row.Cells)
                    {
                        if ((cell.Text == "pid"))
                        {
                            cell.Visible = false;
                            arrIndex.Add(iTempIndex);
                        }
                        iTempIndex++;
                    }
                    e.Row.Cells[0].Style.Add("text-align", "left");
                    e.Row.Cells[1].Style.Add("text-align", "right");
                    e.Row.Cells[2].Style.Add("text-align", "right");
                    e.Row.Cells[3].Style.Add("text-align", "right");
                    e.Row.Cells[4].Style.Add("text-align", "right");
                    e.Row.Cells[5].Style.Add("text-align", "center");
                    e.Row.CssClass = "colheader3";
                }
                
                // akaushik5 Added for MITS 33577 Starts
                // Hide the status when displaying Claim Summary.
                if (!string.IsNullOrEmpty(sReserveSummaryLevel) && sReserveSummaryLevel.Equals("claim"))
                {
                    e.Row.Cells[5].Visible = false;
                }
                // akaushik5 Added for MITS 33577 Ends
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void BindDataToXml(string sReturnValue)
        {
            string sTemp1 = string.Empty;

            DataRow drReserveRow = null;
            DataTable dtReserve = new DataTable();

            XElement objReserveDoc = XElement.Parse(sReturnValue);

            dtReserve.Columns.Add("Reserve");
            dtReserve.Columns.Add("Balance");
            dtReserve.Columns.Add("Paid");
            dtReserve.Columns.Add("Collections");
            dtReserve.Columns.Add("Incurred");
            //dtReserve.Columns.Add("Status");//pgupta93: RMA-7539
            dtReserve.Columns.Add("pid");

            var objRowXml = from rows in objReserveDoc.Descendants("reserve")
                            select rows;

            if (objRowXml.Count() == 0)
            {
                objRowXml = from rows in objReserveDoc.Descendants("reservereadonly")
                            select rows;

                if (objRowXml.Count() != 0)
                {
                    m_bReadOnly = true;
                }
                hdReadOnly.Value = m_bReadOnly.ToString();
            }
            string[] arrPid = new string[1];
            string sDescendant = string.Empty;
            int iRowCount = 0;

            foreach (XElement row in objRowXml)
            {
                iRowCount = iRowCount + 1;
                arrPid[0] = "pid";
                grdReserve.DataKeyNames = arrPid;

                drReserveRow = dtReserve.NewRow();
                DataRow drEmptyRow = dtReserve.NewRow();
                drReserveRow[0] = row.Attribute("reservename").Value;
                drReserveRow[1] = row.Attribute("balance").Value;
                drReserveRow[2] = row.Attribute("paid").Value;
                drReserveRow[3] = row.Attribute("collected").Value;
                drReserveRow[4] = row.Attribute("incurred").Value;
                //pgupta93: RMA-7539 start
                //sTemp1 = row.Attribute("reservestatus").Value;
                //if (sTemp1.Length > 1)
                //{
                //    sTemp1 = sTemp1.Replace("-", "");
                //}
                //drReserveRow[5] = sTemp1;
                //pgupta93: RMA-7539 end
                drReserveRow[5] = row.Attribute("reservetypecode").Value;
                dtReserve.Rows.Add(drEmptyRow);
                dtReserve.Rows.Add(drReserveRow);
            }

            if (iRowCount == 0)
            {
                DataRow drEmptyRow = dtReserve.NewRow();
                dtReserve.Rows.Add(drEmptyRow);
            }

            //Add Totals to Grid
            var objTotalXml = from rows in objReserveDoc.Descendants("totals")
                              select rows;

            foreach (XElement row in objTotalXml)
            {
                arrPid[0] = "pid";
                grdReserve.DataKeyNames = arrPid;
                DataRow drEmptyRow = dtReserve.NewRow();
                drReserveRow = dtReserve.NewRow();
                drReserveRow[0] = "          Totals -->";
                drReserveRow[1] = row.Attribute("balanceamount").Value;
                drReserveRow[2] = row.Attribute("paidtotal").Value;
                drReserveRow[3] = row.Attribute("collectedtotal").Value;
                drReserveRow[4] = row.Attribute("incurredamount").Value;
                drReserveRow[5] = String.Empty;
               // drReserveRow[6] = String.Empty;//pgupta93: RMA-7539
                dtReserve.Rows.Add(drEmptyRow);
                dtReserve.Rows.Add(drReserveRow);
            }

            // Bind DataTable to GridView.
            grdReserve.Visible = true;
            grdReserve.DataSource = dtReserve;
            grdReserve.DataBind();
        }

        // akaushik5 Added for MITS 33577 Starts
        /// <summary>
        /// Handles the RowCreated event of the grdClaimants control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void grdClaimants_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                }
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                    e.Row.CssClass = "colheader3";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Handles the RowDataBound event of the grdClaimants control.
        /// </summary>
        /// <param name="Sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void grdClaimants_RowDataBound(object Sender, GridViewRowEventArgs e)
        {
            try
            {
                int iTempIndex = 0;
                int iPos = 0;
                string sTemp = string.Empty;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView objDataRow = (DataRowView)e.Row.DataItem;

                    e.Row.ID = objDataRow["pid"].ToString();
                    var firstCell = e.Row.Cells[0];
                    firstCell.Controls.Clear();
                    string clickFunction = string.Format("reserveSummary('{0}', '{1}');", claimid.Text, e.Row.Cells[5].Text);
                    firstCell.Controls.Add(new HyperLink { Text = firstCell.Text, ID = "lnkClaimant", NavigateUrl = "#" });
                    (firstCell.FindControl("lnkClaimant") as HyperLink).Attributes.Add("onclick", clickFunction);

                    e.Row.Cells[0].Style.Add("text-align", "left");
                    e.Row.Cells[1].Style.Add("text-align", "right");
                    e.Row.Cells[2].Style.Add("text-align", "right");
                    e.Row.Cells[3].Style.Add("text-align", "right");
                    e.Row.Cells[4].Style.Add("text-align", "right");
                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    arrIndex = new ArrayList();

                    foreach (TableCell cell in e.Row.Cells)
                    {
                        if ((cell.Text == "pid"))
                        {
                            cell.Visible = false;
                            arrIndex.Add(iTempIndex);
                        }
                        iTempIndex++;
                    }
                    e.Row.Cells[0].Style.Add("text-align", "left");
                    e.Row.Cells[1].Style.Add("text-align", "right");
                    e.Row.Cells[2].Style.Add("text-align", "right");
                    e.Row.Cells[3].Style.Add("text-align", "right");
                    e.Row.Cells[4].Style.Add("text-align", "right");
                    e.Row.CssClass = "colheader3";
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Binds the claimant from XML.
        /// </summary>
        /// <param name="sReturnValue">The s return value.</param>
        private void BindClaimantFromXML(string sReturnValue)
        {
            DataRow claimantDataRow = null;
            DataTable claimantDatatable = new DataTable();
            XElement objReserveDoc = XElement.Parse(sReturnValue);

            claimantDatatable.Columns.Add("Claimant");
            claimantDatatable.Columns.Add("Balance");
            claimantDatatable.Columns.Add("Paid");
            claimantDatatable.Columns.Add("Collections");
            claimantDatatable.Columns.Add("Incurred");
            claimantDatatable.Columns.Add("pid");

            var objRowXml = from rows in objReserveDoc.Descendants("claimant")
                            select rows;

            string[] arrPid = new string[1];
            string sDescendant = string.Empty;
            int iRowCount = 0;

            foreach (XElement row in objRowXml)
            {
                iRowCount = iRowCount + 1;
                arrPid[0] = "pid";
                grdClaimants.DataKeyNames = arrPid;

                claimantDataRow = claimantDatatable.NewRow();
                DataRow drEmptyRow = claimantDatatable.NewRow();
                claimantDataRow["Claimant"] = row.Attribute("claimantname").Value;
                claimantDataRow["Balance"] = row.Attribute("balance").Value;
                claimantDataRow["Paid"] = row.Attribute("paid").Value;
                claimantDataRow["Collections"] = row.Attribute("collected").Value;
                claimantDataRow["Incurred"] = row.Attribute("incurred").Value;
                claimantDataRow["pid"] = row.Attribute("claimantid").Value;
                claimantDatatable.Rows.Add(drEmptyRow);
                claimantDatatable.Rows.Add(claimantDataRow);
            }

            if (iRowCount == 0)
            {
                DataRow drEmptyRow = claimantDatatable.NewRow();
                claimantDatatable.Rows.Add(drEmptyRow);
            }

            // Bind DataTable to GridView.
            grdClaimants.Visible = true;
            grdClaimants.DataSource = claimantDatatable;
            grdClaimants.DataBind();
        }
        // akaushik5 Added for MITS 33577 Ends
    }
}
