﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrgSearchCriteria.ascx.cs"
    Inherits="Riskmaster.UI.Shared.Controls.OrgSearchCriteria" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<table cellpadding="2" width="100%">
    <tr class="ctrlgroup">
        <td width="100%" colspan="2">
            Search
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:CheckBox ID="chkTreeNode" runat="server" onclick="return onChanged('chkTreeNode');" />
            <asp:Label ID="lblSelectedBranch" runat="server" Text="Search Within Selected Branch"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblSelectedLevel" runat="server" Text="Search Specific Level "></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="cmb_slevel" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblStringSearch" runat="server" Text="String to Search  "></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtsorg" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtcity" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label runat="server" ID="lblstate" Text="State" />
        </td>
        <td>
            <uc:CodeLookUp runat="server" ID="txtstate" CodeTable="states" ControlName="OrgSearch_txtstate"
                type="code" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblzip" runat="server" Text="Zip Code"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtzip" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:HiddenField ID="txtentity" runat="server" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><!--pankaj-->
            <asp:Button ID="Button1" runat="server" Text="Advanced Search" OnClick="btntview_Click"
                OnClientClick="return LoadAdvSearch();" />                        
            <asp:Button ID="btntview" runat="server" Text="Tree View" OnClick="btntview_Click"
                OnClientClick="return UpdateSearchCriteria();" />
            <asp:Button ID="btnlview" runat="server" Text="List View" OnClick="btnlview_Click"
                OnClientClick="return LoadListPage();" />
            <asp:Button ID="btnreset" runat="server" Text="Reset" OnClick="btnreset_Click" OnClientClick="pleaseWait.Show();" />
        </td>
    </tr>
</table>
