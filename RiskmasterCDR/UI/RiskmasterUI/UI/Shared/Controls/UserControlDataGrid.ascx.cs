﻿//**********************************************************************************************
//*   Date     |  MITS   | Programmer | Description                                            *
//**********************************************************************************************
//* 05/27/2014 | 34276   | achouhan3  | SR Gap 18 - Entity ID & Entity ID Number
//**********************************************************************************************
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Text;
using System.Collections.Generic;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.Shared.Controls
{
    /// <summary>
    /// Author  :   Naresh Chandra Padhy
    /// Dated   :   17th,Oct 2008
    /// Purpose :   The User Control for Implementing the Grid in RMX
    /// </summary>
    public partial class UserControlDataGrid : System.Web.UI.UserControl
    {
        //Ashish Ahuja : Mits 30264 -- To assign tab index to controls available in usercontrol
        private short tabIndex;
        public short TabIndex
        {
            get
            {
                return tabIndex;
            }
            set
            {
                tabIndex = value;

                New.TabIndex = (short)(value++);
                Edit.TabIndex = (short)(value++);
                Clone.TabIndex = (short)(value++);
                Delete.TabIndex = (short)(value);

            }
        }

		//rkulavil - ML Changes - MITS 34107  -start
        private string NewToolTip;
        public string ImgNewToolTip
        {
            get
            {
                return New.ToolTip ;
            }
            set
            {
                New.ToolTip = value;
            }
        }

        private string EditToolTip;
        public string ImgEditToolTip
        {
            get
            {
                return Edit.ToolTip;
            }
            set
            {
                Edit.ToolTip = value;
            }
        }

        private string CloneToolTip;
        public string ImgCloneToolTip
        {
            get
            {
                return Clone.ToolTip;
            }
            set
            {
                Clone.ToolTip = value;
            }
        }

        private string DeleteToolTip;
        public string ImgDeleteToolTip
        {
            get
            {
                return Delete.ToolTip;
            }
            set
            {
                Delete.ToolTip = value;
            }
        }
		//rkulavil - ML Changes - MITS 34107  -end
		protected void Page_PreRender(object sender, EventArgs e)
        {
            //In the following scenario, we have to do the following
            //1. Go to a new fund transaction
            //2. Click "Lookup" toolbar button and select a transaction with multiple splits, i.e. S1 and S2
            //3. Add a new split S3, it displays blank split, S2 and S3. The problem is that the hidden
            //field in step1 (for the blank split) overwrites S1 before rendering in step 2.
            //This could be an issue for other pages too.
            //if (gvData.ClientID == "FundsSplitsGrid_gvData" || gvData.ClientID == "FundsBRSSplitsGrid_gvData" || gvData.ClientID == "CommonPortletsGrid_gvData")
            //{
                int index = 2;
                string sIndex = string.Empty;
                string sHiddenFieldId = string.Empty;
                while (true)
                {
                    if( index < 10 )
                    {
                        sIndex = "0" + index.ToString();
                    }
                    else
                    {
                        sIndex = index.ToString();
                    }
                    //sHiddenFieldId = string.Format("{0}_ctl{1}_|{2}|Data", gvData.ClientID, sIndex, sIndex);
                    sHiddenFieldId = string.Format("{0}_|{1}|Data_{2}", gvData.ClientID, sIndex, (index-2));
                    HiddenField hfData = (HiddenField)AppHelpers.DatabindingHelper.FindControlRecursive(gvData, sHiddenFieldId);
                    if (hfData != null)
                    {
                        TableCell tbCell = (TableCell)(hfData.Parent);
                        if (tbCell != null)
                        {
                            //hfData.Value = tbCell.Text.Replace("&#39;", "&quot;");// Reverting the MITS : 35872 bkuzhanthaim
							hfData.Value = tbCell.Text;
                        }
                    }
                    else
                    {
                        break;
                    }
                    index++;
                }
            //}
        }

        #region Properties
        /// <summary>
        /// The Property Exposed for Binding the Grid with a Datasource from the Page invoking the User Control
        /// This property is exposed for Non FDM Screens where any generic method might not be available to bind the grid.
        /// </summary>
        public object GridDataSource
        {
            get
            {
                return gvData.DataSource;
            }
            set
            {
                gvData.DataSource = value;
                gvData.DataBind();
            }
        }

        /// <summary>
        /// The String to Store the Grid Name. The Grid Name is equal to the User Control Id in the Parent Page.
        /// It is used as a parameter to Javascript when LinkColumn is clicked
        /// </summary>
        private string m_sGridName;

        /// <summary>
        /// The Property to expose the Grid Name to the Parent Page. 
        /// </summary>
        public string GridName
        {
            get
            {
                return m_sGridName;
            }
            set
            {
                m_sGridName = value;
            }
        }

        /// <summary>
        /// Displays the Grid Title. The Grid Title is set at the Parent Page and is displayed in a Label.
        /// </summary>
        public string GridTitle
        {
            set
            {
                if (value != "" && value != null)
                    lblGridTitle.Text = value;
                else
                    lblGridTitle.Visible = false;
            }
        }
        //Deb Multi Currency Changes
        /// <summary>
        /// Displays Currency Type Over Grid
        /// </summary>
        public string CurrencyTypeTitle
        {
            set
            {
                if (value != "" && value != null)
                    lblCurrType.Text = "Currency Type: " + value;
            }
        }
        //Deb Multi Currency Changes
        /// <summary>
        /// Sets the Width of the Grid. The Grid Width needs to be passed from the Parent Page.
        /// </summary>
        public string Width
        {
            set
            {
                if (string.IsNullOrEmpty(value)) value="100%";
                GridDiv.Style.Add(HtmlTextWriterStyle.Width, value);
            }
        }

        /// <summary>
        /// Sets the Width of the Grid. The Grid Width needs to be passed from the Parent Page.
        /// </summary
        public string Height
        {
            set
            {
                if (string.IsNullOrEmpty(value)) value = "180px";
                GridDiv.Style.Add(HtmlTextWriterStyle.Height, value);
            }
        }

        /// <summary>
        /// Adds the Scrollbar to grid Div if the property is true. If the Property is false then Grid will not have any scrollbar and 
        /// would be displayed without any scrollbar
        /// </summary>
        public bool StyleOverflowAuto
        {
            set
            {
                //if (value)
                //    GridDiv.Style.Add(HtmlTextWriterStyle.Overflow, "auto");
                //else
                    //GridDiv.Style.Add(HtmlTextWriterStyle.OverflowY, "scroll");
            }
        }

       
        /// <summary>
        /// The String to Store the Hidden Column names of Grid. The Column Names are pipe delimited.
        /// </summary>
        private string m_sHideNodes;

        /// <summary>
        /// The Property to expose the Hidden Columns to the Parent Page.
        /// </summary>
        public string HideNodes
        {
            get
            {
                return m_sHideNodes;
            }
            set
            {
                m_sHideNodes = value;
            }
        }

        //rupal:start, mits 33600
        private string m_sShowColumns;

        /// <summary>
        /// The Property to show particular grid columns. multiple columns should be pipe separated
        /// </summary>
        public string ShowColumns
        {
            get
            {
                return m_sShowColumns;
            }
            set
            {
                m_sShowColumns = value;
            }
        }

        private string m_sHideColumns;

        /// <summary>
        /// The Property to hide particular grid columns. multiple columns should be pipe separated
        /// </summary>
        public string HideColumns
        {
            get
            {
                return m_sHideColumns;
            }
            set
            {
                m_sHideColumns = value;
            }
        }
        //rupal:end

        //MITS @34276 Starts - Validate Buttons property for Alert on New/Edit/Delete
        /// <summary>
        /// Validate Permissions with alert on New/Edit/Delete
        /// </summary>
        private string m_sValidateButtons;

        public string ValidateButtons
        {
            get
            {
                return m_sValidateButtons;
            }
            set
            {
                m_sValidateButtons = value;
            }
        }
        //MITS @34276 Ends - Validate Buttons property for Alert on New/Edit/Delete
        /// <summary>
        /// For FDM pages this string refers to the XML node to which the grid is bound.
        /// </summary>
        private string m_sTarget;

        /// <summary>
        ///  The Property to expose the Target to the Parent Page.
        /// </summary>
        public string Target
        {
            get
            {
                return m_sTarget;
            }
            set
            {
                m_sTarget = value;
            }
        }

        /// <summary>
        /// For Non FDM pages this string refers to the XML node to which the grid is bound.
        /// </summary>
        private string m_sRef;

        /// <summary>
        ///  The Property to expose the Ref to the Parent Page.
        /// </summary>
        public string Ref
        {
            get
            {
                return m_sRef;
            }
            set
            {
                m_sRef = value;
            }
        }

        /// <summary>
        /// If this property is set,row data is picked from it.
        /// </summary>
        private string m_sDataParam;

        /// <summary>
        ///  The Property to expose the RowDataParam to the Parent Page.
        /// </summary>
        public string RowDataParam
        {
            get
            {
                return m_sDataParam;
            }
            set
            {
                m_sDataParam = value;
            }
        }

        /// <summary>
        /// If this Property is true then the Clone image button will be displayed in the Grid.
        /// </summary>
        private bool m_bShowCloneButton;

        /// <summary>
        /// The Property to expose the ShowCloneButton Property to the Parent Page.
        /// </summary>
        public bool ShowCloneButton
        {
            get
            {
                return m_bShowCloneButton;
            }
            set
            {
                m_bShowCloneButton = value;
                Clone.Visible = value;
            }
        }

        /// <summary>
        /// If this Property is true then a RadioButton Column is displayed in the Grid.
        /// </summary>
        private bool m_bShowRadioButton;

        /// <summary>
        /// The Property to expose the ShowRadioButton Property to the Parent Page.
        /// </summary>
        public bool ShowRadioButton
        {
            get
            {
                return m_bShowRadioButton;
            }
            set
            {
                m_bShowRadioButton = value;
            }
        }
        /// <summary>
        /// If this property is set,blank row is not added when RowDataParam is not null
        /// </summary>
        private bool m_sOverrideBlankRowInsert;

        /// <summary>
        ///  The Property to expose the RowDataParam to the Parent Page.
        /// </summary>
        public bool OverrideBlankRowInsert
        {
            get
            {
                return m_sOverrideBlankRowInsert;
            }
            set
            {
                m_sOverrideBlankRowInsert = value;
            }
        }

        /// <summary>
        /// If this Property is true then only Edit button will be enabled. The user can not add/delete/clone records.
        /// </summary>
        private bool m_bReadonly;

        /// <summary>
        /// The Property to control whether user can add/delete/clone records.
        /// </summary>
        public bool Readonly
        {
            get
            {
                return m_bReadonly;
            }
            set
            {
                m_bReadonly = value;
                New.Visible = (!value);
                Delete.Visible = (!value);
                Clone.Visible = (m_bShowCloneButton && !value);
            }
        }


        /// <summary>
        /// If this Property is true then the Radio Button Column has a Header Caption. 
        /// The Caption if specified through the Property RadioButtonHeader is Set.
        /// If RadioButtonHeader is not specified then By Default "Select One" is the 
        /// Default Caption of the Column
        /// </summary>
        private bool m_bShowRadioButtonHeader;

        /// <summary>
        /// The Property to expose the ShowRadioButtonHeader to the Parent Page
        /// </summary>
        public bool ShowRadioButtonHeader
        {
            get
            {
                return m_bShowRadioButtonHeader;
            }
            set
            {
                m_bShowRadioButtonHeader = value;
            }
        }

        /// <summary>
        /// If this Property is specified and ShowRadioButton and ShowRadioButtonHeader Property is true,
        /// then the Value specified in this field is set as the Column Header Caption.
        /// </summary>
        private string m_sRadioButtonHeader;

        /// <summary>
        /// The Property to expose the RadioButtonHeader to the Parent Page
        /// </summary>
        public string RadioButtonHeader
        {
            get
            {
                return m_sRadioButtonHeader;
            }
            set
            {
                m_sRadioButtonHeader = value;
            }
        }

        /// <summary>
        /// If this Property is true then a CheckBox Column is displayed in the Grid.
        /// </summary>
        private bool m_bShowCheckBox;

        /// <summary>
        /// The Property to expose the ShowCheckBox Property to the Parent Page.
        /// </summary>
        public bool ShowCheckBox
        {
            get
            {
                return m_bShowCheckBox;
            }
            set
            {
                m_bShowCheckBox = value;
            }
        }

        /// <summary>
        /// If this Property is true then the Checkbox Column has a Header Caption. 
        /// The Caption if specified through the Property CheckBoxHeader is Set.
        /// If RadioButtonHeader is not specified then By Default "Select Many" is the 
        /// Default Caption of the Column
        /// </summary>
        private bool m_bShowCheckBoxHeader;

        /// <summary>
        /// The Property to expose the ShowCheckBoxHeader to the Parent Page
        /// </summary>
        public bool ShowCheckBoxHeader
        {
            get
            {
                return m_bShowCheckBoxHeader;
            }
            set
            {
                m_bShowCheckBoxHeader = value;
            }
        }

        /// <summary>
        /// If this Property is specified and ShowCheckBox and ShowCheckBoxHeader Property is true,
        /// then the Value specified in this field is set as the Column Header Caption.
        /// </summary>
        private string m_sCheckBoxHeader;

        /// <summary>
        /// The Property to expose the CheckBoxHeader to the Parent Page
        /// </summary>
        public string CheckBoxHeader
        {
            get
            {
                return m_sCheckBoxHeader;
            }
            set
            {
                m_sCheckBoxHeader = value;
            }
        }

        /// <summary>
        /// If this attribute is specified, then is associated with the Click of radiobutton or checkbox
        /// </summary>
        private string m_sOnClick;

        /// <summary>
        /// The Property to expose the OnClick to the Parent Page
        /// </summary>
        public string OnClick
        {
            get
            {
                return m_sOnClick;
            }
            set
            {
                m_sOnClick = value;
            }
        }

        /// <summary>
        /// Unique Id for the Grid. A column name is specified in this property. Another column name with name "Unique_Hidden" is created which is
        /// bound with the Checkbox or the Radiobutton
        /// </summary>
        private string m_sUniqueId;

        /// <summary>
        /// The Property to expose the UniqueId to the Parent Page
        /// Note - The name of this property is deliberately made Unique_Id and not Uniqueid as in RMX R4 as the ASP .NET page has a property 
        /// UniqueId and creating a property with same name was creating problem.
        /// </summary>
        public string Unique_Id
        {
            get
            {
                return m_sUniqueId;
            }
            set
            {
                m_sUniqueId = value;
            }
        }

        /// <summary>
        /// Specifies the Column width
        /// </summary>
        private double m_dColumnWidth;

        /// <summary>
        /// The Property to expose the Column Width to the Parent Page
        /// </summary>
        public double ColumnWidth
        {
            get
            {
                return m_dColumnWidth;
            }
            set
            {
                m_dColumnWidth = value;
            }
        }
        
        /// <summary>
        /// Specifies the Column Height
        /// </summary>
        private double m_dColumnHeight;

        /// <summary>
        /// The Property to expose the Column Height to the Parent Page
        /// </summary>
        public double ColumnHeight
        {
            get
            {
                return m_dColumnHeight;
            }
            set
            {
                m_dColumnHeight = value;
            }
        }

        /// <summary>
        /// When the New/Edit button is clicked, the Pop up opens. This attribute specifies the width of the Pop up.
        /// </summary>
        private string m_sPopupWidth;

        /// <summary>
        /// The Property to expose the Pop up width to the Parent Page
        /// </summary>
        public string PopupWidth
        {
            get
            {
                return m_sPopupWidth;
            }
            set
            {
                m_sPopupWidth = value;
            }
        }

        /// <summary>
        /// When the New/Edit button is clicked, the Pop up opens. This attribute specifies the height of the Pop up.
        /// </summary>
        private string m_sPopupHeight;

        /// <summary>
        /// The Property to expose the Pop up Height to the Parent Page
        /// </summary>
        public string PopupHeight
        {
            get
            {
                return m_sPopupHeight;
            }
            set
            {
                m_sPopupHeight = value;
            }
        }
        
        /// <summary>
        /// If the Grid Header needs to be shown, this property needs to be set to true otherwise false
        /// </summary>
        private bool m_bShowHeader;

        /// <summary>
        /// The Property to expose the Show Header to the Parent Page
        /// </summary>
        public bool ShowHeader
        {
            get
            {
                return m_bShowHeader;
            }
            set
            {
                m_bShowHeader = value;
            }
        }

        /// <summary>
        /// If the Grid Footer needs to be shown, this property needs to be set to true otherwise false
        /// </summary>
        private bool m_bShowFooter;

        /// <summary>
        /// The Property to expose the Show Footer to the Parent Page
        /// </summary>
        public bool ShowFooter
        {
            get
            {
                return m_bShowFooter;
            }
            set
            {
                m_bShowFooter = value;
                gvData.ShowFooter = value;
            }
        }

        /// <summary>
        /// The name of buttons present in this string will not be displayed. 
        /// For Control like Grid, this string will contain a string like "New|Edit|Delete" so all the buttons are hidden.
        /// For screens like Policy Mangement, In case of policy only Edit button is shown and New and delete button are not.
        /// so in that case, we would pass "New|Delete" in this variable.
        /// </summary>
        private string m_sHideButtons;

        /// <summary>
        /// The Property to expose the Hide Buttons to the Parent Page
        /// </summary>
        public string HideButtons
        {
            get
            {
                return m_sHideButtons;
            }
            set
            {
                m_sHideButtons = value;
            }
        }

        /// <summary>
        /// Multiple Column Names will be present in this string which will be converted into a link column. 
        /// A hyperlink will be provided for all of the columns and clicking this link will invoke a javascript function
        /// If there are two columns A and B to be displayed as hyperlink, then LinkColumn will have value A|B
        /// </summary>
        private string m_sLinkColumn;

        /// <summary>
        /// The Property to expose the Link Column to the Parent Page
        /// </summary>
        public string LinkColumn
        {
            get
            {
                return m_sLinkColumn;
            }
            set
            {
                m_sLinkColumn = value;
            }
        }

        /// <summary>
        /// The Arraylist contains all the Column Names which needs to be converted into the Link Column.
        /// </summary>
        private ArrayList m_alLinkColumns;

        /// <summary>
        /// The Property to expose the Link Columns to the Parent Page
        /// </summary>
        private ArrayList LinkColumns
        {
            
            get
            {
                String[] sLinkColumns;
                if (LinkColumn != null && LinkColumn != "")
                {
                    sLinkColumns = LinkColumn.Split(new char[] { '|' });
                    if (sLinkColumns.Length != 0)
                    {
                        m_alLinkColumns = new ArrayList(sLinkColumns);
                    }
                    else
                    {
                        m_alLinkColumns = new ArrayList();
                        m_alLinkColumns.Add(LinkColumn);
                    }
                }
                return m_alLinkColumns;
            }
        }

        /// <summary>
        /// The Link Type to be rendered. If we need to render all the columns as Hyperlink then we do not need this attribute.
        /// But if we need to change the default behavior then, we need to specify the Link Types for all the Columns.
        /// For ex if A and B are two column, with A as link and B as Image then we need to specify Link|Image in this Attribute
        /// </summary>
        private string m_sLinkType;

        /// <summary>
        /// The Property to expose the Link Type to the Parent Page
        /// </summary>
        public string LinkType
        {
            get
            {
                return m_sLinkType;
            }
            set
            {
                m_sLinkType = value;
            }
        }

        /// <summary>
        /// The Arraylist contains all the Link Types.
        /// </summary>
        private ArrayList m_alLinkTypes;

        /// <summary>
        /// The Property to expose the Link Types to the Parent Page
        /// </summary>
        private ArrayList LinkTypes
        {
            get
            {
                String[] sLinkTypes;
                if (LinkType != null && LinkType != "")
                {
                    sLinkTypes = LinkType.Split(new char[] { '|' });
                    if (sLinkTypes.Length != 0)
                    {
                        m_alLinkTypes = new ArrayList(sLinkTypes);
                    }
                    else
                    {
                        m_alLinkTypes = new ArrayList();
                        m_alLinkTypes.Add(LinkType);
                    }
                }
                return m_alLinkTypes;
            }
        }

        /// <summary>
        /// If The links has to be rendered as Image, then the Image Url needs to be specified here. 
        /// If A, B and C are Link Columns and B is image then this Property needs to be specified as |B_imageUrl|
        /// </summary>
        private string m_sImageUrl;

        /// <summary>
        /// The Property to expose the Image Url to the Parent Page
        /// </summary>
        public string ImageUrl
        {
            get
            {
                return m_sImageUrl;
            }
            set
            {
                m_sImageUrl = value;
            }
        }

        /// <summary>
        /// The Arraylist contains all the Image Urls.
        /// </summary>
        private ArrayList m_alImageUrls;

        /// <summary>
        /// The Property to expose the Image Urls to the Parent Page
        /// </summary>
        private ArrayList ImageUrls
        {
            get
            {
                String[] sImageUrls;
                if (ImageUrl != null && ImageUrl != "")
                {
                    sImageUrls = ImageUrl.Split(new char[] { '|' });
                    if (sImageUrls.Length != 0)
                    {
                        for (int i = 0; i < sImageUrls.Length; i++ )
                        {
                            if (!string.IsNullOrEmpty(sImageUrls[i]) && !sImageUrls[i].Contains("/"))
                            {
                                sImageUrls[i] = "/RiskmasterUI/Images/" + sImageUrls[i];
                            }
                        }
                        m_alImageUrls = new ArrayList(sImageUrls);
                    }
                    else
                    {
                        m_alImageUrls = new ArrayList();
                        m_alImageUrls.Add(ImageUrl);
                    }
                }
                return m_alImageUrls;
            }
        }

        private string m_sTextColumn;

        public string TextColumn
        {
            get
            {
                return m_sTextColumn;
            }
            set
            {
                m_sTextColumn = value;
            }
        }


        /// <summary>
        /// This distinguishes between the Grid and GridAndButtons. While rendering and saving we do not show/use the last row for GridAndButtons
        /// This Variable will help in distinguishing between Grid and GridandButtons
        /// </summary>
        private string m_sType;

        /// <summary>
        /// The Property to expose the Type (Grid or GridandButtons) to the Parent Page
        /// </summary>
        public string Type
        {
            get
            {
                return m_sType;
            }
            set
            {
                m_sType = value;
            }
        }

        /// <summary>
        /// Exposes the grid of the usercontrol to the Parent Page.
        /// This can be used for making some page specific customizations from the code behind of that page.
        /// </summary>
        public object GridView
        {
            get
            {
                return gvData;
            }
        }

        private bool m_bAlign;
        public bool Align
        {
            get
            {
                return m_bAlign;
            }
            set
            {
                m_bAlign = value;
            }
        }
        private string m_sCenterAlignColumns;
        public string CenterAlignColumns
        {
            get
            {
                return m_sCenterAlignColumns;
            }
            set
            {
                m_sCenterAlignColumns = value;
            }
        }

        private string m_sRightAlignColumns;
        public string RightAlignColumns
        {
            get
            {
                return m_sRightAlignColumns;
            }
            set
            {
                m_sRightAlignColumns = value;
            }
        }

        private bool m_RefImplementation;

        public bool RefImplementation
        {
            get
            {
                return m_RefImplementation;
            }
            set
            {
                m_RefImplementation = value;
            }
        }

        private bool m_IncludeLastRecord;

        public bool IncludeLastRecord
        {
            get
            {
                return m_IncludeLastRecord;
            }
            set
            {
                m_IncludeLastRecord = value;
            }
        }
        //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
        /// <summary>
        /// Allow Sorting
        /// </summary>
        private bool m_bAllowSorting = false;
        /// <summary>
        /// The Property to Enable/Disable Sorting in the Parent Page
        /// </summary>
        public bool AllowSorting
        {
            get
            {
                return m_bAllowSorting;
            }
            set
            {
                m_bAllowSorting = value;
                gvData.AllowSorting = m_bAllowSorting;
                gvData.Sorting += new GridViewSortEventHandler(gvData_Sorting);
                gvData.HeaderStyle.ForeColor = System.Drawing.Color.White;
            }
        }
        /// <summary>
        /// Sort Property
        /// </summary>
        public string sortOrder
        {
            get
            {
                if (Convert.ToString(ViewState["sortOrder_" + gvData.ClientID]) == "desc")
                {
                    ViewState["sortOrder_" + gvData.ClientID] = "asc";
                }
                else
                {
                    ViewState["sortOrder_" + gvData.ClientID] = "desc";
                }

                return Convert.ToString(ViewState["sortOrder_" + gvData.ClientID]);
            }
            set
            {
                ViewState["sortOrder_" + gvData.ClientID] = value;
            }
        } 
        //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter

        // npadhy Start Paging implementation in Grid

        /// <summary>
        /// Allow Paging in the GridView. If the value is true then, Paging will be allowed in Grid, otherwise no Paging will be allowed in the Grid
        /// </summary>
        private bool m_bAllowPaging = false;

        /// <summary>
        /// The Property to Enable/Disable Paging in the Parent Page
        /// </summary>
        public bool AllowPaging
        {
            get
            {
                return m_bAllowPaging;
            }
            set
            {
                m_bAllowPaging = value;
                gvData.AllowPaging = m_bAllowPaging;


                DataPaging dp = (DataPaging)this.FindControl("dpPaging");
                //dp.PageSize = PageSize;
                dp.Target = Target;
                //dp.Target = Target;

               // hdnPageSize.Text = PageSize.ToString();

               // hdnCurrentPage.Attributes.Add("RMXRef", Target + "/CurrentPage");

                // If the PageSize property is not set, then By Default we will assign this value to it.
                // If the PageSize property is set, then this value will be overridden in that Property.
                if (PageSize == 0)
                {
                    PageSize = 2;
                }
            }
        }

        /// <summary>
        /// Number of records to be displayed at a time. If the Value is not set then By Default 10 Records would be displayed.
        /// </summary>
        private int m_iPageSize;

        /// <summary>
        /// The Property to Expose Pagesize to the Parent Page
        /// </summary>
        public int PageSize
        {
            get
            {
                return m_iPageSize;
            }
            set
            {
                m_iPageSize = value;
                
                // Increase the Pagesize by 1 if the Mode is GridAndButtons
                //if (Type == "GridAndButtons")
                //    m_iPageSize = m_iPageSize + 1;

                
                gvData.PagerSettings.Visible = false;
                gvData.PageSize = m_iPageSize + 1;

                DataPaging dp = (DataPaging)this.FindControl("dpPaging");
                dp.PageSize = PageSize;
                
                //hdnPageSize.Text = PageSize.ToString();

                //hdnPageSize.Attributes.Add("RMXRef", Target + "/hdnPageSize");
            }
        }


       

        // npadhy End Paging implementation in Grid

        /// <summary>
        /// This Variable will Store the GridData for the User Control</summary>
        private XmlDocument m_GridData;

        /// <summary>
        /// The Property to Expose the Grid Data.
        /// As of now, it is private and will keep it this way till required
        /// ,Made public by Deb Jena
        /// </summary>
        //Deb Mutli Currency
        //We need this to be public to be used for counting splits in Auto Checks
        //private XmlDocument GridData
        //Deb Mutli Currency
        public XmlDocument GridData
        {
            get
            {
                return m_GridData;
            }
            set
            {
                m_GridData = value;
            }
        }
        #region Commented Code for GridWithinGrid Code
        //Added Rakhi for R7:Add Emp Data Elements
        //private bool m_PopupGrid;

        //public bool PopupGrid
        //{
        //    get
        //    {
        //        return m_PopupGrid;
        //    }
        //    set
        //    {
        //        m_PopupGrid = value;
        //    }
        //}

        //private bool m_SessionGrid;

        //public bool SessionGrid
        //{
        //    get
        //    {
        //        return m_SessionGrid;
        //    }
        //    set
        //    {
        //        m_SessionGrid = value;
        //    }
        //}

        //Added Rakhi for R7:Add Emp Data Elements
#endregion
        #endregion Properties

        # region Control Specific events

        /// <summary>
        /// The Page Load of the User Control
        /// Associates the CSS class with grid Header and grid rows
        /// Associates Grid Buttons with attributes like mouseover, mouseout and onclick
        /// Hides the buttons specified in HideButtons string
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {

            //GridDiv.Style.Add(HtmlTextWriterStyle.Overflow, "auto");
            //GridDiv.Style.Add(HtmlTextWriterStyle.OverflowY, "scroll");

            // Specify the CSS class with Grid Header, and Grid Rows
            gvData.HeaderStyle.CssClass = "msgheader";
            gvData.RowStyle.CssClass  = "datatd1";
            gvData.AlternatingRowStyle.CssClass = "datatd";

            // Associate the Grid Buttons with the Attributes.
            // We need to do this at run time as onmouseover and onmouseout attributes are not exposed for Imagebutton.
            New.Attributes.Add("onmouseover", "this.src='/RiskmasterUI/Images/tb_new_active.png'");
            New.Attributes.Add("onmouseout", "this.src='/RiskmasterUI/Images/tb_new_mo.png'");
            
            // After the javascriopt function is called, we are returning false from here.
            // as we do not want a postback here.

            New.Attributes.Add("onclick", "openGridAddEditWindow('" + GridName + "','add','" +
                                            PopupWidth + "','" + PopupHeight + "', document.getElementById('" + GridName + "_" + OtherParams.ID + "').value);return false;");

            Edit.Attributes.Add("onmouseover", "this.src='/RiskmasterUI/Images/tb_edit_active.png'");
            Edit.Attributes.Add("onmouseout", "this.src='/RiskmasterUI/Images/tb_edit_mo.png'");
            Edit.Attributes.Add("onclick", "openGridAddEditWindow('" + GridName + "','edit','" +
                                            PopupWidth + "','" + PopupHeight + "', document.getElementById('" + GridName + "_" + OtherParams.ID + "').value);return false;");

            Clone.Attributes.Add("onmouseover", "this.src='/RiskmasterUI/Images/tb_clone_active.png'");
            Clone.Attributes.Add("onmouseout", "this.src='/RiskmasterUI/Images/tb_clone_mo.png'");
            Clone.Attributes.Add("onclick", "openGridAddEditWindow('" + GridName + "','clone','" +
                                            PopupWidth + "','" + PopupHeight + "', document.getElementById('" + GridName + "_" + OtherParams.ID + "').value);return false;");

            Delete.Attributes.Add("onmouseover", "this.src='/RiskmasterUI/Images/tb_delete_active.png'");
            Delete.Attributes.Add("onmouseout", "this.src='/RiskmasterUI/Images/tb_delete_mo.png'");
            Delete.Attributes.Add("onclick", "return validateGridForDeletion('" + GridName + "');");
            //MGaba2:Commenting the following code as HideButtons was not initialized the time when 
            //BindData function is called.Moving it to BindData function.
            //// The UserControl is called one more time then required.
            //// Could not figure out the reason. This hack hides the button when it is not required
            //if (GridName == null)
            //{
            //    HideButtons = "New|Edit|Delete|Clone";
            //}

            //// Check if the Grid Type is "Grid" and not "GridandButtons" then Hide the Buttons
            //if (Type == "Grid")
            //    HideButtons = "New|Edit|Delete|Clone";

            // Call the function HideButton to hide the buttons not required.
            // For grid all the three buttons are made invisible.
            //HideButton(HideButtons);    ommented by csingh7
           
        }

        /// <summary>
        /// Invokes when the row of the grid is bound with data.
        /// Hides the grid header if not required
        /// Converts the column to link column if the link column attribute is passed.
        /// </summary>
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // Check if the Row is header, and if the ShowHeader is false, then hide it.
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (!ShowHeader)
                    e.Row.Visible = false;
            }
           
            // If the LinkColumn attribute is specified, then that column of grid is made hyperlink and on its click 
            // a javascript function is called.
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
                TableCell tcGridCell;
                LinkButton lbLinkColumn;
                Image imLinkColumn;
                WebControl wbLinkColumn;
                int iUniqueIdColumnIndex;
                int iCellPosition = 0;
                    
                //Add a hidden field for Data (XML string)
                if (drvGridRow.DataView.Table.Columns["Data"] != null)
                {
                    int iDataColumnIndex = drvGridRow.DataView.Table.Columns["Data"].Ordinal;
                    iDataColumnIndex = GetCellPosition(iDataColumnIndex,false);

                    if (iDataColumnIndex <= e.Row.Cells.Count)
                    {
                        HiddenField hf = new HiddenField();
                        tcGridCell = e.Row.Cells[iDataColumnIndex];

                        int iIndex = e.Row.RowIndex + 2;
                        if (iIndex.ToString().Length == 1)
                            hf.ID = "|0" + iIndex.ToString() + "|" + "Data";
                        else
                            hf.ID = "|" + iIndex.ToString() + "|" + "Data";
                        hf.Value = tcGridCell.Text;
                        e.Row.Cells[iDataColumnIndex].Controls.Add(hf);

                        //MITS #34276  Starts -   Hidden field for Vendor Persmission Ends
                        Boolean bPermission = false;
                        string strPermission = String.Empty;
                        if (GridName.ToLower() == "entityxentityidtypegrid")
                        {
                            XmlDocument xmlRow = new XmlDocument();
                            xmlRow.LoadXml(tcGridCell.Text.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'"));                           
                            XmlNodeList xmlRowNodeList = xmlRow.SelectNodes("option");
                            foreach (XmlNode objRowNodesTemp in xmlRowNodeList)
                            {
                                //if (objRowNodesTemp.Attributes["IsVendorNew"] != null && !Convert.ToBoolean(objRowNodesTemp.Attributes["IsVendorNew"].Value))
                                //{
                                //    ValidateButtons += ",ValidateNewVendor";
                                //}
                                if (objRowNodesTemp.Attributes["IsVendorUpdate"] != null && !Convert.ToBoolean(objRowNodesTemp.Attributes["IsVendorUpdate"].Value))
                                {
                                    bPermission = true;
                                    strPermission += "ValidateUpdateVendor,";
                                }
                                if (objRowNodesTemp.Attributes["IsVendorDelete"] != null && !Convert.ToBoolean(objRowNodesTemp.Attributes["IsVendorDelete"].Value))
                                {
                                    bPermission = true;
                                    strPermission += "ValidateDeleteVendor";
                                }
                                strPermission = strPermission.TrimEnd(',');
                            }
                            if (bPermission && !String.IsNullOrEmpty(strPermission))
                            {
                                HiddenField hfVendor = new HiddenField();
                                tcGridCell = e.Row.Cells[iDataColumnIndex];
                                hfVendor.ClientIDMode = ClientIDMode.Static;
                                int iIndexVendor = e.Row.RowIndex + 2;
                                if (iIndex.ToString().Length == 1)
                                    hfVendor.ID = "|0" + iIndex.ToString() + "|" + "VendorPermission";
                                else
                                    hfVendor.ID = "|" + iIndex.ToString() + "|" + "VendorPermission";
                                hfVendor.Value = strPermission;
                                e.Row.Cells[iDataColumnIndex].Controls.Add(hfVendor);
                            }
                        }
                        //MITS #34276  Ends -   Hidden field for Vendor Persmission 
                    }
                }

                int iNumLinkColumn = 0;
                Control obj = new Control();
                for (int iColumnIndex = drvGridRow.DataView.Table.Columns.Count - 1; iColumnIndex >= 0; iColumnIndex--)
                {
                    
                    if (!string.IsNullOrEmpty(LinkColumn))
                    {
                        // Check if grid Column Name is same as specified in LinkColumn. 
                        // The And condition is specified to reduce the number of loops
                        if (LinkColumns.Contains(drvGridRow.DataView.Table.Columns[iColumnIndex].ColumnName) &&
                            drvGridRow.DataView.Table.Columns[iColumnIndex].Caption != "Hidden")
                        {
                            string sLinkText;
                            iCellPosition = GetCellPosition(iColumnIndex, false); ;
                            tcGridCell = e.Row.Cells[iCellPosition];
                            wbLinkColumn = (WebControl)tcGridCell.Controls[0];

                           
                            // href is assigned the value # to stop the postback on click.
                            wbLinkColumn.Attributes.Add("href", "#");
                            int iIndex = e.Row.RowIndex + 2;

                            //PJS, MITS 16370: Commented as another id attribute was generating from some place
                            //wbLinkColumn.Attributes.Add("id", iIndex.ToString() + "_lblLinkColumn" + iNumLinkColumn.ToString());
                             
                            // Get the Collumn index of the UniqueId_Hidden column.
                            iUniqueIdColumnIndex = GetIndexFromColumnName("UniqueId", true);
                            
                            // Get the Link Text
                            // if the Link is Link Button then take the Text from Text Attribute, If it is Image take it from Alternate Text
                            // If the Link Type is not there, all are links
                            if (!string.IsNullOrEmpty(LinkType))
                            {
                                if (LinkTypes[iNumLinkColumn].ToString() == "Link")
                                {
                                    lbLinkColumn = (LinkButton)wbLinkColumn;
                                    
                                    sLinkText = lbLinkColumn.Text;
                                }
                                else if (LinkTypes[iNumLinkColumn].ToString() == "Image")
                                {
                                    imLinkColumn = (Image)wbLinkColumn;
                                    imLinkColumn.Attributes.Add("id", "lblLinkColumn" + iNumLinkColumn.ToString());
                                    sLinkText = imLinkColumn.AlternateText;
                                }
                                else
                                {
                                    sLinkText = string.Empty;
                                }
                            }
                            else
                            {
                                lbLinkColumn = (LinkButton)wbLinkColumn;
                                sLinkText = lbLinkColumn.Text;
                            }
                            
                            //Added bY Tushar for handle "'" 
                            sLinkText = sLinkText.Replace("'", "\\'"); 

                            // Associate Javascript function, Javascript function takes grid name, value of Unique Id column and 
                            // Text of Link Column
                            wbLinkColumn.Attributes.Add("onclick", "gridlinkclicked('" + GridName + "','" +
                                        e.Row.Cells[iUniqueIdColumnIndex].Text + "','" + sLinkText + "','" + iNumLinkColumn + "'  );return false;");
                            
                            iNumLinkColumn++;

                            
                        }
                    }
                }
            }
        }

        
        /// <summary>
        /// Invokes when all the data is bound with the Grid.
        /// Hides the column which need not be shown to the User.
        /// It is done in this event and not in RowDatabound so that column are made invisble after data is bound.
        /// If the columns are made invisble during data is bound, the data in hidden column are not accessible.
        /// </summary>
        protected void gvData_DataBound(object sender, EventArgs e)
        {
            HideColumnAndRow(false);

            if (Align)
            {
                // By Default all the Columns are Left Alligned

                // Get Center Align Columns
                if (CenterAlignColumns != null)
                {
                    string[] caColumns = CenterAlignColumns.Split(new char[] { '|' });
                    foreach (string str in caColumns)
                    {
                        int iIndex = GetIndexFromColumnName(str, false);
                        gvData.Columns[iIndex].HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                        gvData.Columns[iIndex].ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    }
                }

                // Get Right Align Columns
                if (RightAlignColumns != null)
                {
                    string[] raColumns = RightAlignColumns.Split(new char[] { '|' });
                    foreach (string str in raColumns)
                    {
                        int iIndex = GetIndexFromColumnName(str, false);
                        gvData.Columns[iIndex].HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                        gvData.Columns[iIndex].ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    }
                }
            }
            // rsushilaggar - Added pagination on the Policy setup grid MITS 22371
            if (AllowPaging && gvData.Rows.Count == 1)
            {
                divPaging.Attributes.Add("style", "display:none");
            }
            else
            {
                divPaging.Attributes.Add("style", "display:block");
            }
        }
        # endregion Control Specific events
                
        #region Public Functions
        // This region defines the public functions exposed to parent page


        /// <summary>
        /// This function creates the Grid Columns and the data to be bound with the Grid
        /// </summary>
        /// <param name="p_sobjXml"> The Xml file from which the grid columns and Data has to be created.</param>
        public void BindData(XmlDocument p_GridData)
        {
            string sTargetPath = Target;

            GridData = p_GridData;

            int iLinkColumns = 0;
            //MITS 34276  Start -  Added validatebutton for alert because usercontrol load falls later
            if (GridName.ToLower() == "entityxentityidtypegrid")
                ValidateButton(ValidateButtons);
            //MITS 34276  Ends -  Added validatebutton for alert because usercontrol load falls later
            #region
            //Added Rakhi for R7:Add Emp Data Elements
            //TextBox txtPageLoad = (TextBox)this.Parent.FindControl("PageLoad");
            //bool bPageLoad = false;
            //if (txtPageLoad != null)
            //{
            //    bPageLoad = Convert.ToBoolean(txtPageLoad.Text);
            //}
            //Added Rakhi for R7:Add Emp Data Elements
            #endregion
            // If the Target is not specified, then Grid can not bind with XML Document.
            if (sTargetPath != null)
            {
                if (sTargetPath != "" && !(sTargetPath.Contains("/")))
                    sTargetPath = Ref;

                // The Target and the XML Document are not in sync. Change the Target to match with the Xml Document structure.
                if (sTargetPath != "" && sTargetPath.StartsWith("/Instance/UI"))
                    sTargetPath = sTargetPath.Replace("/Instance/UI", "/Document/ParamList/Param[@name='SysFormVariables']");

                // When the page postbacks the grid columns which need not be shown are made hidden.
                // So, the hidden column can not be bound to the data. Changing the Column visibility back to true enables to bind the 
                // Grid hidden column with data.
                if (Page.IsPostBack)
                {
                    HideColumnAndRow(true);
                }

                DataSet dsGridDatasource = new DataSet();
                DataTable dtGridData = new DataTable();
                DataColumn dcGridColumn;

                // Get the Headers Collection. 
                XmlNode objGridHeaders = p_GridData.SelectSingleNode(sTargetPath + "/listhead");

                // Get the Footers Collection. 
                XmlNode objGridFooters = p_GridData.SelectSingleNode(sTargetPath + "/listfoot");

                //If listhead is not in the input XML, it could be some error happens in the 
                //backend and not able to create the required Xml. If it's a postback, we can
                //try to call GetXml to get the Xml from the hidden fields.
                if (objGridHeaders == null)
                {
                    //If it's not a postback and can not find listhead not, don't try to get xml from
                    //the hidden field and just skip data binding
                    if (!Page.IsPostBack)
                        return;

                    //If can not get xml from the hidden fields, just return
                    string sTargetXml = GetXml();
                    if (string.IsNullOrEmpty(sTargetXml))
                        return;

                    //If the target node is not found, try to create the input document
                    XmlNode oTargetNode = p_GridData.SelectSingleNode(sTargetPath);
                    if (oTargetNode == null)
                    {
                        p_GridData.LoadXml(sTargetXml);
                        sTargetPath = "/";
                        oTargetNode = p_GridData.DocumentElement;
                    }
                    else
                    {
                        XmlDocument oTargetDoc = new XmlDocument();
                        oTargetDoc.LoadXml(sTargetXml);
                        oTargetNode.InnerXml = oTargetDoc.DocumentElement.InnerXml;
                    }

                    objGridHeaders = p_GridData.SelectSingleNode(sTargetPath + "/listhead");
                    if (objGridHeaders == null)
                        return;
                }

                //Put the listhead in the hidden field
                string sListHead = objGridHeaders.OuterXml;
                sListHead = sListHead.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                hdListHead.Value = sListHead;
                //srajindersin Page Validation Failure Fix
                //hdListHead.Value = sListHead.Replace("<", "&lt;").Replace(">", "&gt;");


                // Get the Column names for which hidden columns need to be created
                String[] sHiddenColumns;
                ArrayList alHiddenColumns;
                if (HideNodes != null && HideNodes != "")
                {
                    sHiddenColumns = HideNodes.Split(new char[] { '|' });
                    alHiddenColumns = new ArrayList(sHiddenColumns);
                }
                else
                {
                    alHiddenColumns = new ArrayList();
                }

                //If the hidenodes does not contain Unique_Id, add to the hidenodes list
                if (!alHiddenColumns.Contains(Unique_Id))
                    alHiddenColumns.Add(Unique_Id);

                //A Dictionary to store all field name and the number of columns for that field in the Grid. It could handle the case 
                //where listhead and hidenodes are not the same order of XML child nodes.
                Dictionary<string, int> dicGridFieldList = new Dictionary<string, int>();
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    string sNodeName = objHeaderDetails.Name;
                    if (string.Compare(sNodeName, "rowhead", true) == 0)
                    {
                        if (objHeaderDetails.Attributes["colname"] != null)
                            sNodeName = objHeaderDetails.Attributes["colname"].Value;
                    }

                    if (alHiddenColumns.Contains(sNodeName))
                        continue;

                    if (!dicGridFieldList.ContainsKey(sNodeName))
                        dicGridFieldList.Add(sNodeName, 2);
                    else
                        continue;
                }
                foreach (string sHiddenNodeName in alHiddenColumns)
                {
                    if(string.IsNullOrEmpty(sHiddenNodeName))
                        continue;

                    if (!dicGridFieldList.ContainsKey(sHiddenNodeName))
                        dicGridFieldList.Add(sHiddenNodeName, 1);
                    else
                        continue;
                }

                // Create a Header for Ref. This column will be used while creating the xml while saving the Grid Data.
                alHiddenColumns.Add("Ref");

                // Create a Header for the Unique Id. This column is created to bind the radio button present in the grid. 
                // This column will contain the same data as the column specified in Unique Id Property,
                if (Unique_Id != "" && alHiddenColumns.Contains(Unique_Id))
                {
                    alHiddenColumns.Add("UniqueId");
                }

                // Create a Header for Data. This column will be used to store the Xml for New and Edit purposes
                alHiddenColumns.Add("Data");

                // Instance of the Grid Column
                BoundField bfGridColumn;

                // Instance of the grid column for link type of column
                ButtonField butGridLinkColumn;

                #region RadioButton
                // Handle Radio Button Column. If the RadioButtton column needs to be displayed, 
                // then perform various processing. Else hide the column.
                if (ShowRadioButton)
                {
                    dcGridColumn = new DataColumn();

                    // If the Radio button column Header needs to be displayed and header name is not specified,
                    // then default the header name to "Select One"
                    if (ShowRadioButtonHeader && RadioButtonHeader == null)
                    {
                        dcGridColumn.ColumnName = "Select One";
                        gvData.Columns[0].HeaderText = dcGridColumn.ColumnName;

                    }
                    // If the Radio button column Header needs to be displayed and header name is specified,
                    // then display the header name.
                    else if (ShowRadioButtonHeader && RadioButtonHeader != "")
                    {
                        dcGridColumn.ColumnName = RadioButtonHeader;
                        gvData.Columns[0].HeaderText = dcGridColumn.ColumnName;
                    }
                    // If the Radio Button Column Header need not be displayed, then Blank it.
                    else
                    {
                        dcGridColumn.ColumnName = "";
                        gvData.Columns[0].HeaderText = "";
                    }

                    // The width of this column would be 5% of the grid.
                    gvData.Columns[0].ItemStyle.Width = Unit.Percentage(5);

                    // Add column to the Datasource with which grid will be bound
                    dtGridData.Columns.Add(dcGridColumn);
                }
                else
                {
                    gvData.Columns[0].Visible = false;
                }
                #endregion RadioButton

                #region CheckBox
                // Handle Check Box Column. If the Check Box column needs to be displayed, 
                // then perform various processing. Else hide the column.
                if (ShowCheckBox)
                {
                    dcGridColumn = new DataColumn();

                    // If the Checkbox column Header needs to be displayed and header name is not specified,
                    // then default the header name to "Select Many"
                    if (ShowCheckBoxHeader && CheckBoxHeader == null)
                    {
                        dcGridColumn.ColumnName = "Select Many";
                        gvData.Columns[1].HeaderText = dcGridColumn.ColumnName;
                    }
                    // If the Checkbox column Header needs to be displayed and header name is specified,
                    // then display the header name.
                    else if (ShowCheckBoxHeader && CheckBoxHeader != "")
                    {
                        dcGridColumn.ColumnName = CheckBoxHeader;
                        gvData.Columns[1].HeaderText = dcGridColumn.ColumnName;
                    }
                    // If the Checkbox Column Header need not be displayed, then Blank it.
                    else
                    {
                        dcGridColumn.ColumnName = "";
                        gvData.Columns[1].HeaderText = "";
                    }

                    // The width of this column would be 5% of the grid.
                    gvData.Columns[1].ItemStyle.Width = Unit.Percentage(5);

                    // Add column to the Datasource with which grid will be bound
                    dtGridData.Columns.Add(dcGridColumn);
                }
                else
                {
                    gvData.Columns[1].Visible = false;
                }
                #endregion CheckBox


                if (TextColumn != null && TextColumn != "")
                {
                    dcGridColumn = new DataColumn();

                    // Add column to the Datasource with which grid will be bound
                    dtGridData.Columns.Add(dcGridColumn);
                }
                else
                {
                    gvData.Columns[2].Visible = false;
                }

                #region Headers and Footers

                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    if (alHiddenColumns.Contains(objHeaderDetails.InnerText) || alHiddenColumns.Contains(objHeaderDetails.Name))
                        continue;

                    // Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();

                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName =  objHeaderDetails.InnerText; 

                    // Add Column to Data Table
                    dtGridData.Columns.Add(dcGridColumn);

                    // Logic below adds the columns to Grid. 
                    // We do not need to do this after every postback as columns are already added to Grid
                    //if (!Page.IsPostBack || (PopupGrid && !SessionGrid && bPageLoad)) //Added PopupGrid && bPageLoad for R7:Add Emp Data Elements
                    if (!Page.IsPostBack)
                    {
                        // If the Column is not Link Column, Create normal bound columns
                        if (String.IsNullOrEmpty(LinkColumn) || (LinkColumns!= null && !LinkColumns.Contains(dcGridColumn.ColumnName)))
                        {
                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;
                            //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
                            if (AllowSorting)
                            {
                                bfGridColumn.SortExpression = dcGridColumn.ColumnName;
                            }
                            //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Get the FooterText if the Show Footer Property is true
                            if (ShowFooter)
                            {
                                if (objGridFooters != null)
                                    bfGridColumn.FooterText = objGridFooters[objHeaderDetails.Name].InnerText;
                            }

                            // Width of the Column. If the Column width is specified, then assign it.
                            // otherwise calculate the Column width as per the number of Columns required to be displayed
                            if (ColumnWidth != 0)
                            {
                                bfGridColumn.ItemStyle.Width = Unit.Percentage(ColumnWidth);
                            }
                            else
                            {
                                int iNumberOfColumns = objGridHeaders.ChildNodes.Count;
                                int iTotalWidth = 100;

                                if (ShowRadioButton)
                                    iTotalWidth = Convert.ToInt32(iTotalWidth - gvData.Columns[0].ItemStyle.Width.Value);

                                if (ShowCheckBox)
                                    iTotalWidth = Convert.ToInt32(iTotalWidth - gvData.Columns[0].ItemStyle.Width.Value);

                                int iWidthofColumn = iTotalWidth / iNumberOfColumns;

                                bfGridColumn.ItemStyle.Width = Unit.Percentage(iWidthofColumn);

                            }

                            // Add the newly created bound field to the GridView. 
                            gvData.Columns.Add(bfGridColumn);
                        }
                        else
                        {
                            // The Column is linkcolumn, so associate a hyperlink with it.

                            // Link Column to be associated with the grid.
                            butGridLinkColumn = new ButtonField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            butGridLinkColumn.DataTextField = dcGridColumn.ColumnName;
                            //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
                            if (AllowSorting)
                            {
                                butGridLinkColumn.SortExpression = dcGridColumn.ColumnName;
                            }
                            //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
                            // Create the hyperlink for the column
                            if (String.IsNullOrEmpty(LinkType))
                            {
                                butGridLinkColumn.ButtonType = ButtonType.Link;
                            }
                            else
                            {
                                if(LinkTypes[iLinkColumns].ToString() == "Image")
                                {
                                    butGridLinkColumn.ButtonType = ButtonType.Image;
                                    butGridLinkColumn.ImageUrl = ImageUrls[iLinkColumns].ToString() ;
                                }
                                else if(LinkTypes[iLinkColumns].ToString() == "Link")
                                {
                                    butGridLinkColumn.ButtonType = ButtonType.Link;
                                }
                                iLinkColumns++;
                            }

                            // Assign the HeaderText
                            butGridLinkColumn.HeaderText = dcGridColumn.ColumnName;

                            // Width of the Column. If the Column width is specified, then assign it.
                            // otherwise calculate the Column width as per the number of Columns required to be displayed
                            if (ColumnWidth != 0)
                            {
                                butGridLinkColumn.ItemStyle.Width = Unit.Percentage(ColumnWidth);
                            }
                            else
                            {
                                int iNumberOfColumns = objGridHeaders.ChildNodes.Count;
                                int iTotalWidth = 100;

                                if (ShowRadioButton)
                                    iTotalWidth = Convert.ToInt32(iTotalWidth - gvData.Columns[0].ItemStyle.Width.Value);

                                if (ShowCheckBox)
                                    iTotalWidth = Convert.ToInt32(iTotalWidth - gvData.Columns[0].ItemStyle.Width.Value);

                                int iWidthofColumn = iTotalWidth / iNumberOfColumns;

                                butGridLinkColumn.ItemStyle.Width = Unit.Percentage(iWidthofColumn);

                            }

                            // Add the newly created bound field to the GridView. 
                            gvData.Columns.Add(butGridLinkColumn);
                        }

                    }

                    // Column for Value Purposes. These columns store the value corresponding to the columns displayed in grid.
                    // Like code if for code fields etc.
                    dcGridColumn = new DataColumn();

                    // Get the Xml Node for Header in data section. 
                    XmlNode objNode = null;
                    if (RowDataParam == null)
                        objNode = p_GridData.SelectSingleNode(sTargetPath + "/option/" + objHeaderDetails.Name);
                    else
                        objNode = p_GridData.SelectSingleNode(sTargetPath + "/" + RowDataParam + "/" + objHeaderDetails.Name);
                   
                    // npadhy We do not need to check whether the Attribute is present or not
                        dcGridColumn.ColumnName = objHeaderDetails.InnerText + "_Value";

                    // Keep the Header Node Name in the Caption, this will be used while generating the XML for saving the records
                    dcGridColumn.Caption = objHeaderDetails.Name;

                    // Add Column to Datasource that will be bound with grid.
                    dtGridData.Columns.Add(dcGridColumn);

                    // Logic below adds the value columns to Grid. 
                    // We do not need to do this after every postback as columns are already added to Grid
                    //if (!Page.IsPostBack || (PopupGrid && !SessionGrid && bPageLoad)) //Added PopupGrid && bPageLoad for R7:Add Emp Data Elements
                    if (!Page.IsPostBack)
                    {
                        // Column to be associated with the Grid
                        bfGridColumn = new BoundField();

                        // Associate the Grid Bound Column with the Column of the Datasource
                        bfGridColumn.DataField = dcGridColumn.ColumnName;

                        // Assign the HeaderText
                        bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                        // Keep the Header Node Name in the Grid Footer, this will be used while generating the XML for saving the records
                        bfGridColumn.FooterText = dcGridColumn.Caption;
                        //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
                        if (AllowSorting)
                        {
                            bfGridColumn.SortExpression = dcGridColumn.ColumnName;
                        }
                        //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
                        // Add the newly created bound field to the GridView. 
                        gvData.Columns.Add(bfGridColumn);

                    }
                }

                #endregion Headers

                #region Hidden Columns
                foreach (string sHiddenNodeName in alHiddenColumns)
                {
                    if (sHiddenNodeName != "")
                    {
                        dcGridColumn = new DataColumn();

                        bfGridColumn = new BoundField();

                        dcGridColumn.ColumnName = sHiddenNodeName;

                        dcGridColumn.Caption = "Hidden";

                        dtGridData.Columns.Add(dcGridColumn);

                        //dt.Columns[dt.Columns.Count - 1].ColumnMapping = MappingType.Hidden;
                        //if (!Page.IsPostBack || (PopupGrid && !SessionGrid && bPageLoad)) //Added PopupGrid && bPageLoad for R7:Add Emp Data Elements
                        if (!Page.IsPostBack)
                        {
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            //  Initialize the HeaderText field value. 
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName + "_Hidden";
                            //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
                            if (AllowSorting)
                            {
                                bfGridColumn.SortExpression = dcGridColumn.ColumnName;
                            }
                            //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
                            // Add the newly created bound field to the GridView. 
                            gvData.Columns.Add(bfGridColumn);
                        }
                    }
                }

                #endregion Hidden Columns
                #region Data
                Control cPostBackControl = DatabindingHelper.GetPostBackControl(this.Page);
                TextBox txtMode = (TextBox)this.Parent.FindControl(GridName + "_Action");
                string sMode = string.Empty;
                if( txtMode != null )
                {
                    sMode = txtMode.Text.ToLower();
                }

                // MITS #34276  Start - Removed ID Type Vendor Node if view permission is not granted for ID Type Vendor
                if (GridName.ToLower() == "entityxentityidtypegrid")
                {
                    XmlNodeList objRowNodesTempList = null;
                    objRowNodesTempList = p_GridData.SelectNodes(sTargetPath + "/option");
                    int iRowPostion = 0;
                    foreach (XmlNode objRowTempNodes in objRowNodesTempList)
                    {
                        if (objRowTempNodes.Attributes["IsVendorView"] != null && !Convert.ToBoolean(objRowTempNodes.Attributes["IsVendorView"].Value))
                            p_GridData.SelectNodes(sTargetPath + "/option").Item(iRowPostion).ParentNode.RemoveChild(objRowTempNodes);

                    }
                }
                //MITS #34276 Ends - Removed ID Type Vendor Node if view permission is not granted for ID Type Vendor

                // If the Last Record is Included, then the XML which is generated to send to CWS includes all the changed record, so we can use the 
                // Same xml to bind the grid.
                if ((!IsPostBack || cPostBackControl != null || string.IsNullOrEmpty(sMode) || IncludeLastRecord)&&
                    ((sMode.ToLower() != "delete") || string.IsNullOrEmpty(sMode)))
                {
                    XmlNodeList objRowNodesList=null;
                    if(RowDataParam==null)
                        objRowNodesList = p_GridData.SelectNodes(sTargetPath + "/option");
                    else
                        objRowNodesList = p_GridData.SelectNodes(sTargetPath + "/" + RowDataParam);
                    
                    DataRow dr = null;

                    int iRowCount = 0;
                    foreach (XmlNode objRowNodes in objRowNodesList)
                    {
                        string sRefAttribute = "" ;
                        if (objRowNodes.Attributes["ref"] != null)
                        {
                            sRefAttribute = objRowNodes.Attributes["ref"].Value;
                        }
                        dr = dtGridData.NewRow();

                        // npadhy This logic does not hold good if there are more than one Template Column
                        //int iColumnIndex = gvData.Columns.Count - dtGridData.Columns.Count -1;
                        int iColumnIndex = 0;

                        iColumnIndex = GetCellPosition(iColumnIndex,true);

                        //The sequence should be the same as the dicGridField2Column.
                        //foreach (XmlElement objElem in objNodes)
                        foreach(string sNodeName in dicGridFieldList.Keys)
                        {
                            XmlElement objElem = objRowNodes[sNodeName];
                            if (objElem != null)
                            {
                                //Deb Multi Currency
                                if (objElem.Attributes["CurrencyValue"] != null)
                                {
                                 if(!string.IsNullOrEmpty(objElem.Attributes["CurrencyValue"].Value))
                                    dr[iColumnIndex++] = objElem.Attributes["CurrencyValue"].Value;
                                 else
                                    dr[iColumnIndex++] = objElem.InnerText;
                                }
                                //Deb Multi Currency
                                else
                                    dr[iColumnIndex++] = objElem.InnerText;
                                if (dicGridFieldList[sNodeName] == 2)
                                    if (objElem.Attributes["codeid"] != null)
                                        dr[iColumnIndex++] = objElem.Attributes["codeid"].Value;
                                    else
                                        dr[iColumnIndex++] = objElem.InnerText;
                            }
                            else
                            {
                                //can not find the node in the XML. It means the specified node name is wrong or XML is wrong
                                dr[iColumnIndex++] = string.Empty;
                                if (dicGridFieldList[sNodeName] == 2)
                                        dr[iColumnIndex++] = string.Empty;
                            }
                        }

                        dr[iColumnIndex++] = sRefAttribute;
                        dtGridData.Rows.Add(dr);

                        // Store the Unique_Id if Unique Id is not null
                        if (Unique_Id != "")
                            dr[iColumnIndex++] = dtGridData.Rows[iRowCount][Unique_Id].ToString();

                        dr[iColumnIndex++] = objRowNodes.OuterXml;
                        iRowCount++;
                    }
                    
                }
                if ((cPostBackControl == null && IsPostBack && !string.IsNullOrEmpty(sMode) && !IncludeLastRecord)
                    || (sMode == "delete"))
                {
                    int row = 2;
                    int iNewRecords = 0;
                    string sDataFormat = "";
                    bool bRowSkip = false;
                    int iCounter = 0;
                    string sFormat=string.Empty;
                    foreach (GridViewRow gvr in gvData.Rows)
                    {
                        if (gvr.RowType == DataControlRowType.DataRow)
                        {
                            //if (gvData.Rows.Count == row - 1 && (txtMode.Text.ToLower() == "edit" || txtMode.Text.ToLower() == "delete"))
                            if (gvData.Rows.Count == row - 1 && (sMode != "add" && sMode != "clone"))
                                continue;

                            DataRow dr;
                            string index;
                            if (row.ToString().Length == 1)
                                index = "0" + row.ToString();
                            else
                                index = row.ToString();
                            string sXml = Request.Form[gvr.UniqueID + "$|" + index.ToString() + "|Data"];
                            sXml = sXml.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                            XmlDocument objXml = new XmlDocument();
                            objXml.LoadXml(sXml);
                            XmlNodeList objRowNodesList;
                            if (RowDataParam == null)
                             objRowNodesList = objXml.SelectNodes("/option");
                            else
                                objRowNodesList = objXml.SelectNodes("/" + RowDataParam);
                            XmlNode objRowNodes = objRowNodesList[0];

                            // Get the Selected Row Id if Mode is Deletion. This will be called only in Specific Case
                            // When the Page is not FDM and in case of NON FDM the New records are not directly saved to DB
                            
                            if (txtMode.Text.ToLower() == "delete")
                            {
                                TextBox txtDeletedValues = (TextBox)this.Parent.FindControl("hdnDeletedValues");
                               // if (objNodes.SelectSingleNode("//" + Unique_Id).InnerText == txtDeletedValues.Text)
                                    //if (objNodes.SelectSingleNode("//" + Unique_Id).InnerText.Contains(txtDeletedValues.Text))
                                if (txtDeletedValues.Text.Contains(objRowNodes.SelectSingleNode("//" + Unique_Id).InnerText))
                                {
                                    sFormat = objRowNodes.OuterXml;
                                    row++;
                                    bRowSkip = true;
                                    iCounter++;
                                    continue;
                                }
                            }

                            string sRefAttribute = "";
                            if (objRowNodes.Attributes["ref"] != null)
                            {
                               sRefAttribute = objRowNodes.Attributes["ref"].Value;
                            }

                            dr = dtGridData.NewRow();

                            // npadhy This logic is not valid if there are more than one Template Columns
                            //int iColumnIndex = gvData.Columns.Count - dtGridData.Columns.Count -1;
                            int iColumnIndex = 0;
                            iColumnIndex = GetCellPosition(iColumnIndex,true);

                            //The sequence should be the same as the dicGridField2Column.
                            //foreach (XmlElement objElem in objNodes)
                            foreach (string sNodeName in dicGridFieldList.Keys)
                            {
                                XmlElement objElem = objRowNodes[sNodeName];
                                if (objElem != null)
                                {
                                    dr[iColumnIndex++] = objElem.InnerText;
                                    if (dicGridFieldList[sNodeName]==2)
                                        if (objElem.Attributes["codeid"] != null)
                                            dr[iColumnIndex++] = objElem.Attributes["codeid"].Value;
                                        else
                                            dr[iColumnIndex++] = objElem.InnerText;
                                }
                                else
                                {
                                    //can not find the node in the XML. It means the specified node name is wrong or XML is wrong
                                    dr[iColumnIndex++] = string.Empty;
                                    if (dicGridFieldList[sNodeName]==2)
                                        dr[iColumnIndex++] = string.Empty;
                                }
                            }

                            if (objRowNodes.Attributes["type"] == null)
                            {
                                dr[iColumnIndex++] = sRefAttribute;
                                dtGridData.Rows.Add(dr);
                                if ((ShowCheckBox || ShowRadioButton) && Unique_Id != "")
                                {
                                    if(bRowSkip)
                                        dr[iColumnIndex++] = dtGridData.Rows[row - 2-iCounter][Unique_Id].ToString();
                                    else
                                    dr[iColumnIndex++] = dtGridData.Rows[row - 2][Unique_Id].ToString();
                                }
                                dr[iColumnIndex++] = objRowNodes.OuterXml;
                            }
                            else if (objRowNodes.Attributes["type"].Value == "new")
                            {
                                dr[iColumnIndex++] = sRefAttribute;
                                dtGridData.Rows.Add(dr);
                                if ((ShowCheckBox || ShowRadioButton) && Unique_Id != "")
                                {
                                    iNewRecords = GetMinimumUniqueId(dtGridData.Rows.Count);
                                    dr[iColumnIndex++] = iNewRecords;
                                }

                                // Update the objNodes with the Unique Id
                                XmlNode objPrimaryTemp = objRowNodes.SelectSingleNode("//" + Unique_Id);
                                objPrimaryTemp.InnerText = iNewRecords.ToString();

                                dr[iColumnIndex++] = objRowNodes.OuterXml;
                            }
                            sDataFormat = objRowNodes.OuterXml;
                        }
                        row++;
                    }

                    DataRow drNewRecord = dtGridData.NewRow();
                    string sRef = string.Empty ;//Asif
                    //Asif Start
                    if (txtMode != null && txtMode.Text.ToLower() == "delete")
                    {
                        sRef = "";
                  
                    }
                    else
                    {
                        sRef = dtGridData.Rows[dtGridData.Rows.Count - 1]["Ref"].ToString();
                    }
                    //Asif End
                    if (sRef != "")
                    {
                    int iIndex1 = sRef.IndexOf('[');
                    int iIndex2 = sRef.IndexOf(']');
                    if (iIndex1 != -1 && iIndex2 != -1)
                    {
                    string sRefNo = sRef.Substring(iIndex1 + 1, iIndex2 - iIndex1 - 1);
                    sRef = sRef.Substring(0, sRef.IndexOf('[') + 1) + (Int32.Parse(sRefNo) + 1).ToString() + "]";
                    }
                    }
                    drNewRecord["Ref"] = sRef;

                    XmlDocument objXml1 = new XmlDocument();
                    if (txtMode != null && txtMode.Text.ToLower() == "delete")
                    {
                        objXml1.LoadXml(sFormat);
                    }
                    else
                    {
                        objXml1.LoadXml(sDataFormat);
                    }

                    XmlNodeList objNodeList1;

                    if (RowDataParam == null)
                        objNodeList1 = objXml1.SelectNodes("/option");
                    else
                        objNodeList1 = objXml1.SelectNodes("/" + RowDataParam);

                    XmlNode objNodes1 = objNodeList1[0];
                    if (sRef != "")
                    {
                        objNodes1.Attributes["ref"].Value = sRef;
                    }

                    // The type of the newly inserted record should be new as this will help in correcly determine the unique Id while
                    // inserting the next record
                    if (objNodes1.Attributes["type"] != null)
                        objNodes1.Attributes["type"].Value = "new";
                    else
                        ((XmlElement)objNodes1).SetAttribute("type", "new");

                    #region
                    ////Changed for R7:Add Emp Data Element
                    ////drNewRecord["Data"] = objNodes1.OuterXml;
                    //XmlNode objEmptyNode = CreateNodeTemplate(objNodes1);
                    //drNewRecord["Data"] = objEmptyNode.OuterXml;
                    ////Changed for R7:Add Emp Data Elements
                    #endregion
                    drNewRecord["Data"] = objNodes1.OuterXml;
                    dtGridData.Rows.Add(drNewRecord);

                    // Add Data For Data fieldss

                }


                if (Type.ToLower() == "grid")
                {
                    // Adding a Dummy record for grid, so that if there is no data, then also the Headers are displayed.
                    DataRow drNewRecord = dtGridData.NewRow();
                    dtGridData.Rows.Add(drNewRecord);
                }

                if (Type.ToLower() == "gridandbuttons" && RowDataParam != null && !OverrideBlankRowInsert)
                {
                    // Adding a Dummy record for grid, so that if there is no data, then also the Headers are displayed.
                    DataRow drNewRecord = dtGridData.NewRow();
                    dtGridData.Rows.Add(drNewRecord);
                }


                dsGridDatasource.Tables.Add(dtGridData);

                //if (gvData.AllowPaging)//MITS 22371 rsushilaggar Modified the pagesize for Hidden row
                //    gvData.PageSize = gvData.PageSize + 1;
               //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
                if (AllowSorting)
                {
                    //zmohammad : User needs to set sortOrder from UI first for the grid as a default value. If i set it here, it would be a problem in pages with multiple grids.
                    ViewState["dsGridDatasource_" + gvData.ClientID] = dsGridDatasource.Tables[0];
                }
                gvData.DataSource = dsGridDatasource.Tables[0];
                //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
                gvData.DataBind();

                //Resetting Text Mode Asif
                if (txtMode != null)
                {
                    txtMode.Text = string.Empty;
                }
                #endregion Data

                //MGaba2:Moved from page_load as HideButtons was coming null
                // The UserControl is called one more time then required.
                // Could not figure out the reason. This hack hides the button when it is not required
                if (GridName == null)
                {
                    HideButtons = "New|Edit|Delete|Clone";
                }

                // Check if the Grid Type is "Grid" and not "GridandButtons" then Hide the Buttons
                if (Type == "Grid")
                    HideButtons = "New|Edit|Delete|Clone";

                //mbahl3 Mits 31043 removed the below code as as HideButtons was coming null
                //MITS 27094 hlv 9/25/12 begin; refixd 1/30/2013
                if (HideButtons == null && GridName.ToLower().Equals("fundssplitsgrid") && Type.ToLower().Equals("gridandbuttons"))
                {
                    XmlNode nodeTag = p_GridData.SelectSingleNode(sTargetPath);
                    //rsharma220 JIRA 3810
                    if (nodeTag != null && nodeTag.Attributes != null)
                    {
                        if (nodeTag.Attributes["GridHideButtons"] != null)
                        {
                            HideButtons = nodeTag.Attributes["GridHideButtons"].Value;
                        }
                    }
                }
                //MITS 27094 hlv 9/25/12 end; refixd 1/30/2013
                //mbahl3 mits 31043  removed the below code as as HideButtons was coming null

                HideButton(HideButtons);  // Added by csingh7

                //rupal:start, mits 33600
                if ((ShowColumns != null && ShowColumns != string.Empty) || (HideColumns != null && HideColumns != string.Empty))
                {
                    HideShowGridColumns();
                }
                //rupal:end
                Page.ClientScript.RegisterStartupScript(this.GetType(), GridName + "SelectRow", "<script>SelectAfterPostback('" + GridName + "');</script>");
            }
        }

        public XmlDocument GenerateXml()
        {
            XmlDocument objXmlDocument = null;
            XmlElement objElement = null;
            XmlElement objRootElement = null;
            int iRunningNumber = 0;
            int iCurrPrimaryKey = 0;
            XmlNode objPrimaryColumn = null;
            XmlNode objRootNode = null;
            objXmlDocument = new XmlDocument();
            string sRootName = GridName.Replace("Grid", "");
            StartDocument(ref objXmlDocument, ref objRootElement, sRootName);
            CreateElement(objRootElement, "listhead", ref objElement);

            string sListHead = hdListHead.Value;

            XmlDocument objListHeadDoc = null;

            if (!string.IsNullOrEmpty(sListHead))
            {
                sListHead = sListHead.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                objListHeadDoc = new XmlDocument();
                objListHeadDoc.LoadXml(sListHead);
                objElement.InnerXml = objListHeadDoc.DocumentElement.InnerXml;
            }

            // Create Data from the Xml Stored in the Hidden Column
            int iGridRowCount = gvData.Rows.Count;

            // npadhy Get the Postback Action. This will be used only when the IncludeLastRecord is True, If the Post back Action is 
            // Delete then we done want to include the last record in XMLin
            TextBox txtSysPostBackAction = (TextBox)this.Parent.FindControl("SysPostBackAction");
            string sSysPostBackAction = string.Empty;
            if(txtSysPostBackAction != null)
                sSysPostBackAction  = txtSysPostBackAction.Text;
            // npadhy Get the Mode. This will be used only when the IncludeLastRecord is True, If the Moden is 
            // Edit then we done want to include the last record in XMLin
            TextBox txtMode = (TextBox)this.Parent.FindControl(GridName + "_Action");
            string sMode = string.Empty;
            if (txtMode != null)
            {
                sMode = txtMode.Text.ToLower();
            }
            //if ((!IncludeLastRecord || (IncludeLastRecord && (sSysPostBackAction == "Delete" || sMode == "edit")))&& !PopupGrid) //Added PopupGrid condition for R7:Add Employee Data Elements
            if (!IncludeLastRecord || (IncludeLastRecord && (sSysPostBackAction == "Delete" || sMode == "edit")))
            {
                // If Delete was Clicked, then we need to reset it as It will give us problem while adding next record
                if (IncludeLastRecord && sSysPostBackAction == "Delete")
                {
                    txtSysPostBackAction.Text = "";
                }
                iGridRowCount--;
            }
            for (int i = 0; i < iGridRowCount ; i++)
            {
                int index;
                if (RowDataParam == null)
                {
                    CreateElement(objRootElement, "option", ref objElement);
                    index = GetIndexFromColumnName("Ref", true);
                    if(sRootName != "CommonPortlets")
                    {
                        objElement.SetAttribute("ref", gvData.Rows[i].Cells[index].Text);
                    } // if
                }
                else
                {
                    CreateElement(objRootElement, RowDataParam, ref objElement);
                }
                string sXmlData;

                // If the Last Record need not be included, then we can get the records from Grid
                // As all the entries is bound to grid
                if (!IncludeLastRecord)
                {
                    index = GetIndexFromColumnName("Data", true);
                    sXmlData = gvData.Rows[i].Cells[index].Text;
                    sXmlData = sXmlData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'").Replace("&amp;amp;", "&amp;"); //Parijat : Mits 19022 Removing &amp; from transaction detail for invoice number
                }
                else
                {
                    // If the last record needs to be included, then we can not get the last record from Grid, as it is still not bound to grid.
                    // So we need to fetch the record from the control where the Javascript has put it.
                    // So using Request.Form in this Case.
                    GridViewRow gvr = gvData.Rows[i];
                    string sSelectedRowIndex = string.Empty;
                    // Grid Rows are rendered from index 2
                    if ( (i + 2).ToString().Length == 1)
                        sSelectedRowIndex = "0" + (i + 2).ToString();
                    else
                        sSelectedRowIndex = (i + 2).ToString();
                    sXmlData = Request.Form[gvr.UniqueID + "$|" + sSelectedRowIndex + "|Data"];
                    sXmlData = sXmlData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'").Replace("&amp;amp;", "&amp;"); //Parijat : Mits 19022 -- Removing &amp; from transaction detail for invoice number
                }
                XmlDocument objTemp = new XmlDocument();
                if(sXmlData != null && sXmlData != "" && sXmlData != "&nbsp;")
                    objTemp.LoadXml(sXmlData);
                if(IncludeLastRecord && objTemp != null)
                {
                    // Set the Primary Record for new Records

                    // Get the Primary Field Column in Xml
                    objPrimaryColumn = objTemp.SelectSingleNode("//" + Unique_Id);

                    if (Int32.TryParse(objPrimaryColumn.InnerText, out iCurrPrimaryKey))
                    {
                        if (RowDataParam == null)
                            objRootNode = objTemp.SelectSingleNode("/option");
                        else
                            objRootNode = objTemp.SelectSingleNode("/" + RowDataParam);

                        // npadhy Start MITS 20747 Not able to add more than 2 coverages in policy
                        if (iCurrPrimaryKey < 0)
                        {
                            iRunningNumber--;
                            objPrimaryColumn.InnerText = iRunningNumber.ToString();
                        }
                        // npadhy End MITS 20747 Not able to add more than 2 coverages in policy
                    }
                    else
                    {
                        iRunningNumber--;
                        objPrimaryColumn.InnerText = iRunningNumber.ToString();
                    }
                }
                //Added Rakhi to kill Grid Nodes
                TextBox txtGridKillNodes = (TextBox)this.Parent.FindControl(GridName+"_ComboRelations");
                if (txtGridKillNodes != null)
                {
                    if (txtGridKillNodes.Text != "")
                    {
                        string[] sValueTextPair = new string[50];
                        string sRemoveNodes = "";
                        sValueTextPair = txtGridKillNodes.Text.Split(";".ToCharArray()[0]);//Each ValueTextPair is separated from other with a ;
                        foreach (string sValueText in sValueTextPair)
                        {
                            string[] sValueTextSplit = new string[2];
                            sValueTextSplit = sValueText.Split("|".ToCharArray()[0]);//Each ValueText is | separated.
                            if (sValueTextSplit.Length == 2)
                            {
                                if (sRemoveNodes == "")
                                {
                                    sRemoveNodes = sValueTextSplit[1];
                                }
                                else
                                {
                                    sRemoveNodes += "|" + sValueTextSplit[1];
                                }
                            }

                        }
                        KillGridNodes(ref objTemp, sRemoveNodes);
                    }
                }
                //Added Rakhi to kill Grid Nodes
                if (objTemp != null && objTemp.ChildNodes[0] != null)
                    objElement.InnerXml = objTemp.ChildNodes[0].InnerXml;
            }
            return objXmlDocument;
        }

        /// <summary>
        /// Get xml for headlist and all the rows, includes the blank row
        /// </summary>
        /// <returns></returns>
        public string GetXml()
        {
            if (gvData.Rows.Count == 0)
                return string.Empty;

            StringBuilder sbXML = new StringBuilder();
            
            string sRootName = GridName.Replace("Grid", "");
            sbXML.Append(string.Format("<{0}>", sRootName));

            string sListHead = hdListHead.Value;
            sListHead = sListHead.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
            sbXML.Append(sListHead);

            int index = 2;
            string sIndex = string.Empty;
            string sHiddenFieldId = string.Empty;
            string sXmlData = string.Empty;
            bool bItemFound = false;
            for (int i = 0; i < gvData.Rows.Count; i++)
            {
                if( index < 10 )
                {
                    sIndex = "0" + index.ToString();
                }
                else
                {
                    sIndex = index.ToString();
                }
                sHiddenFieldId = string.Format("{0}$gvData$ctl{1}$|{2}|Data", GridName, sIndex, sIndex);
                sXmlData = Request.Form[sHiddenFieldId];
                if (!string.IsNullOrEmpty(sXmlData))
                {
                    sXmlData = sXmlData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'").Replace("&amp;amp;", "&amp;"); //Parijat : Mits 19022 Removing &amp; from transaction detail for invoice number
                    sbXML.Append(sXmlData);
                    bItemFound = true;
                }
                index++;
            }

            //For mode "add", add the blank row to the xml; for mode "delete", remove the deleted row 
            string sMode = string.Empty;
            TextBox txtMode = (TextBox)this.Parent.FindControl(GridName + "_Action");
            if (txtMode != null)
            {
                sMode = txtMode.Text;
            }
            if (sMode == "add" || sMode == "clone" || !bItemFound)
            {
                    index = GetIndexFromColumnName("Data", true);
                    sXmlData = gvData.Rows[gvData.Rows.Count-1].Cells[index].Text;
                    sXmlData = sXmlData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'").Replace("&amp;amp;", "&amp;"); //Parijat : Mits 19022 Removing &amp; from transaction detail for invoice number
                    sbXML.Append(sXmlData);
            }
            sbXML.Append(string.Format("</{0}>", sRootName));
            string sXML = sbXML.ToString();

            if (sMode == "delete")
            {
                string sControlName = GridName.Substring(0, GridName.Length - 4) + "SelectedId";
                TextBox txtSelectedId = (TextBox)this.Parent.FindControl(sControlName);
                string sSelectedId = string.Empty;
                if (txtSelectedId != null)
                {
                    sSelectedId = txtSelectedId.Text;
                }
                if (!string.IsNullOrEmpty(sSelectedId))
                {
                    XElement oGridXml = XElement.Parse(sXML);
                    string sPath = string.Format("//option[{0}='{1}']", Unique_Id, sSelectedId);
                    XElement oDeletedElement = oGridXml.XPathSelectElement(sPath);
                    if (oDeletedElement != null)
                    {
                        oDeletedElement.Remove();
                    }
                    sXML = oGridXml.ToString();
                }
            }

            return sXML.Replace("&nbsp;", ""); //sagarwal54 MITS 35386
        }
        #endregion Public Functions

        #region Private Functions
        #region Common XML functions
        /// <summary>
        /// Initialize the XML document
        /// </summary>
        /// <param name="objXmlDocument">XML document</param>
        /// <param name="p_objRootNode">Root Node</param>
        /// <param name="p_objMessagesNode">Messages Node</param>
        /// <param name="p_sRootNodeName">Root Node Name</param>
        internal void StartDocument(ref XmlDocument objXmlDocument, ref XmlElement p_objRootNode, string p_sRootNodeName)
        {
            try
            {
                objXmlDocument = new XmlDocument();
                p_objRootNode = objXmlDocument.CreateElement(p_sRootNodeName);
                objXmlDocument.AppendChild(p_objRootNode);
            }

            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Node Name</param>		
        internal void CreateElement(XmlElement p_objParentNode, string p_sNodeName)
        {
            XmlElement objChildNode = null;
            try
            {
                objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                objChildNode = null;
            }

        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Child Node Name</param>
        /// <param name="p_objChildNode">Child Node</param>
        internal void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }

        }

        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>		
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            try
            {
                XmlElement objChildNode = null;

                if (p_sText == "&nbsp;")
                    p_sText = "";

                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>
        /// <param name="p_objChildNode">Child Node</param>
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                if (p_sText == "&nbsp;")
                    p_sText = "";
                p_objChildNode.InnerText = p_sText;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
        }

        public void ResetSysExData(XmlDocument SysEx, string sNode, string sValue)
        {
            try { SysEx.DocumentElement.RemoveChild(SysEx.SelectSingleNode("/SysExData/" + sNode)); }
            catch { };
            CreateSysExData(SysEx, sNode, sValue);
        }

        public void CreateSysExData(XmlDocument SysEx, string sNode, string sValue)
        {
            XmlNode objNew;
            //Determine if the specific Node already exists in the SysExData of the Instance Document
            if (SysEx.SelectSingleNode("/SysExData/" + sNode) == null)
            {
                //Create a new XML Element for the specified Xml Node
                objNew = SysEx.CreateElement(sNode);

                //Set the element inner text to an empty string
                objNew.AppendChild(SysEx.CreateCDataSection(sValue));
                //objNew.InnerText = FormDataAdaptor.CData();

                //Add the new XML Element to the existing Instance Data
                SysEx.DocumentElement.AppendChild(objNew);
            }
        }

        #endregion

        private int GetIndexFromColumnName(string p_sColumnName, bool bHidden)
        {
            try
            {
                string sColumnName = p_sColumnName + (bHidden ? "_Hidden" : "");
                for (int i = gvData.Columns.Count - 1; i > 0; i--)
                {
                    if (gvData.Columns[i].HeaderText == sColumnName)
                        return i;
                }
                return 0;
            }
            catch
            {
                return 0;
            }


        }

        private void HideButton(string p_sHideNodes)
        {
            if (p_sHideNodes != null)
            {
                if (p_sHideNodes.Contains("New"))
                    New.Visible = false;
                else
                    New.Visible = true;
                if (p_sHideNodes.Contains("Edit"))
                    Edit.Visible = false;
                else
                    Edit.Visible = true;
                if (p_sHideNodes.Contains("Delete"))
                    Delete.Visible = false;
                else
                    Delete.Visible = true;
                if (p_sHideNodes.Contains("Clone") || (m_bShowCloneButton==false) )
                    Clone.Visible = false;
                else
                    Clone.Visible = true;
            }
        }

        private void HideColumnAndRow(bool p_bShow)
        {
            string sCssClass = string.Empty;

            if (p_bShow)
                sCssClass = "visiblerowcol";
            else
                sCssClass = "hiderowcol";
            for (int i = 0; i < gvData.Columns.Count; i++)
            {
                if (gvData.Columns[i].HeaderText.Contains("Value") || gvData.Columns[i].HeaderText.Contains("Hidden"))
                {
                    gvData.Columns[i].HeaderStyle.CssClass = sCssClass;
                    gvData.Columns[i].ItemStyle.CssClass = sCssClass;

                    if (ShowFooter)
                        gvData.Columns[i].FooterStyle.CssClass = sCssClass;
                }
            }

            if (TextColumn != null && TextColumn != "")
            {
                gvData.Columns[2].HeaderStyle.CssClass = sCssClass;
                gvData.Columns[2].ItemStyle.CssClass = sCssClass;
            }
            
            // abhateja 12.09.2008
            // Need to distinguish between type="Grid" and type="GridAndButtons".
            // For now using "ShowRadioButton" property as the distinguishing factor.
            // Later on we can expose another property as well, if required.
            //if ((ShowRadioButton && p_bShow) || (ShowRadioButton && !p_bShow) || (!ShowRadioButton && p_bShow))
            //if (Type.ToLower() != "grid")
            if (gvData.Rows.Count > 0)
            {
                gvData.Rows[gvData.Rows.Count - 1].CssClass = sCssClass;
            }

        }

        //rupal:start.mits 33600
        private void HideShowGridColumns()
        {
            string sShowCssClass = "visiblerowcol";
            string sHideCssClass = "hiderowcol";
            List<string> lstShowCol = new List<string>();
            List<string> lstHideCol = new List<string>();

            if ((!string.IsNullOrEmpty(ShowColumns)) || (!string.IsNullOrEmpty(HideColumns)))
            {
                if (!string.IsNullOrEmpty(ShowColumns))
                {
                    lstShowCol = ShowColumns.Split('|').ToList();
                }
                if (!string.IsNullOrEmpty(HideColumns))
                {
                    lstHideCol = HideColumns.Split('|').ToList();
                }
                for (int i = 0; i < gvData.Columns.Count; i++)
                {
                    if (lstShowCol.Count() > 0 && lstShowCol.IndexOf(gvData.Columns[i].HeaderText) >= 0)
                    {
                        gvData.Columns[i].HeaderStyle.CssClass = sShowCssClass;
                        gvData.Columns[i].ItemStyle.CssClass = sShowCssClass;
                    }
                    else if (lstHideCol.Count() > 0 && lstHideCol.IndexOf(gvData.Columns[i].HeaderText) >= 0)
                    {
                        gvData.Columns[i].HeaderStyle.CssClass = sHideCssClass;
                        gvData.Columns[i].ItemStyle.CssClass = sHideCssClass;
                    }
                }
            }
            lstShowCol = null;
            lstHideCol = null;
        }
        //rupal:end
        private void KillGridNodes(ref XmlDocument objTemp, string sRemoveNodes)
        {
            string[] sKillNodes = new String[50];
            XmlNode objKillNode = null;
            try
            {
                sKillNodes = sRemoveNodes.Split('|');
                foreach (string sCurrentNode in sKillNodes)
                {

                    objKillNode = objTemp.ChildNodes[0].SelectSingleNode(sCurrentNode);
                    if (objKillNode != null)
                    {
                        objTemp.ChildNodes[0].RemoveChild(objKillNode);
                    }

                }

            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
        }

        /// <summary>
        /// It Returns the required Cell position in the Grid. If Template Columns are hidden, though the Column count of grid does not 
        /// consider Template Columns, but GridViewRowEventArgs consider them also. 
        /// </summary>
        /// <param name="iColumnIndex"> The Column Index where a Control has to be added</param>
        /// <returns>Absolute Position for a Cell</returns>
        private int GetCellPosition(int iColumnIndex,bool bMode)
        {
            int iCellPosition = iColumnIndex;

            if (bMode)
            {
                if (ShowRadioButton)
                    iCellPosition++;

                if (ShowCheckBox)
                    iCellPosition++;

                if (TextColumn != null && TextColumn != "")
                    iCellPosition++;
            }
            else
            {
                if (!ShowRadioButton)
                    iCellPosition++;

                if (!ShowCheckBox)
                    iCellPosition++;

                if (TextColumn == null || TextColumn == "")
                    iCellPosition++;
            }

            return iCellPosition;
        }

        private int GetMinimumUniqueId(int iProcessingRow)
        {
            int iMinUniqueId = 0;
            int iCurrentValue;
            // Get the Collumn index of the UniqueId_Hidden column.
            int iUniqueIdColumnIndex = GetIndexFromColumnName("UniqueId", true);
            for (int i = 0; i < gvData.Rows.Count; i++)
            {
                if (Int32.TryParse(gvData.Rows[i].Cells[iUniqueIdColumnIndex].Text, out iCurrentValue))
                {
                    if (iCurrentValue < 0)
                    {
                        
                        iMinUniqueId = iCurrentValue;
                        // The Index of the loop runs from 0 but the Grid Row Count will start from 1
                        if (i == (iProcessingRow - 1))
                            return iMinUniqueId;
                    }
                }
                else
                {
                        iMinUniqueId--;
                }
            }
            return iMinUniqueId;
        }
        //Zakir : MITS 34188 Merging Deb's Sorting from Underwriter
        /// <summary>
        /// Sorting 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = SortGrid(e.SortExpression);
            //ViewState["dsGridDatasource_" + gvData.ClientID] = dt;
            gvData.DataSource = dt;
            gvData.DataBind();
            if (sortOrder == "desc")
            {
                sortOrder = "asc";
            }
            else
            {
                sortOrder = "desc";
            }
        }
        /// <summary>
        /// Sort Grid
        /// </summary>
        /// <param name="sortColumn"></param>
        /// <returns></returns>
        private DataTable SortGrid(string sortColumn)
        {
            DataView myDataView = ((DataTable)ViewState["dsGridDatasource_" + gvData.ClientID]).DefaultView;
            if (sortColumn != string.Empty)
            {
                myDataView.Sort = string.Format("{0} {1}", sortColumn, sortOrder);
            }
            return CreateTableFromDataView(myDataView);
        }
        /// <summary>
        /// Create Table from Dataview
        /// </summary>
        /// <param name="obDataView"></param>
        /// <returns></returns>
        private DataTable CreateTableFromDataView(DataView obDataView)
        {
            if (null == obDataView)
            {
                throw new ArgumentNullException("DataView", "Invalid DataView object specified");
            }
            DataRow dBlankrow = null;
            DataTable obNewDt = obDataView.Table.Clone();
            int idx = 0;
            string[] strColNames = new string[obNewDt.Columns.Count];
            foreach (DataColumn col in obNewDt.Columns)
            {
                strColNames[idx++] = col.ColumnName;
            }
            IEnumerator viewEnumerator = obDataView.GetEnumerator();
            while (viewEnumerator.MoveNext())
            {
                DataRowView drv = (DataRowView)viewEnumerator.Current;
                DataRow dr = obNewDt.NewRow();
                try
                {
                    foreach (string strName in strColNames)
                    {
                        dr[strName] = drv[strName];
                    }
                }
                catch (Exception ex){}
                if (string.IsNullOrEmpty(Convert.ToString(dr[Unique_Id])))
                {
                    dBlankrow = dr;
                }
                else
                {
                    obNewDt.Rows.Add(dr);
                }
            }
            if (dBlankrow != null)
            {
                obNewDt.Rows.Add(dBlankrow);
            }
            return obNewDt;
        }
        //Deb : MITS 32214
        protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //gvData.PageIndex = e.NewPageIndex;
            //gvData.AllowPaging = true;
            //gvData.PageSize = m_iPageSize;
            //gvData.PagerSettings.Mode = PagerButtons.Numeric;
            //    gvData.PagerSettings.FirstPageImageUrl = "";
            //    gvData.PagerSettings.FirstPageText = "";
            //    gvData.PagerSettings.LastPageImageUrl = "";
            //    gvData.PagerSettings.LastPageText = "";
            //    gvData.PagerSettings.Position = PagerPosition.TopAndBottom;
            //    gvData.PagerSettings.NextPageImageUrl = "";
            //    gvData.PagerSettings.NextPageText = "";
            //    gvData.PagerSettings.PreviousPageImageUrl = "";
            //    gvData.PagerSettings.PreviousPageText = "";
            ////hdnCurrentPage.Text = e.NewPageIndex.ToString();
            //    if (GridData != null)
            //        BindData(GridData);
        }

        #region CreateNodeTemplate
        //private XmlNode CreateNodeTemplate(XmlNode objNodes1)
        //{
        //    if (objNodes1 != null)
        //    {
        //        foreach (XmlElement objElem in objNodes1)
        //        {
        //            if (objElem.HasChildNodes && objElem.ChildNodes[0].ChildNodes.Count>0) //For the Inner Grid Data
        //            {

        //                XmlNodeList objRowNodesList;
        //                if (RowDataParam == null)
        //                    objRowNodesList = objElem.SelectNodes("option");
        //                else
        //                    objRowNodesList = objElem.SelectNodes(RowDataParam);

        //                XmlNode objChildNode = objRowNodesList[0];//Taking 1st option tag only
        //                if(objChildNode!=null)
        //                {
        //                    foreach (XmlElement objChildElem in objChildNode)
        //                    {
        //                        RemoveNodeContent(objChildElem);
        //                    }
        //                }
        //                if (objRowNodesList.Count > 1) //Removing remaining children as we need only 1 for the template
        //                {
        //                    for(int i=1;i<objRowNodesList.Count;i++)
        //                    {
        //                        objElem.RemoveChild(objRowNodesList[i]);
        //                    }
        //                }
                         
        //            }
        //            else
        //            {

        //                RemoveNodeContent(objElem);
        //            }
        //        }
        //    }
        //    return objNodes1;
        //}
        //private void RemoveNodeContent(XmlElement objElem)
        //{
        //    if (objElem != null)
        //    {
        //        if (objElem.Attributes.Count == 0)
        //            objElem.InnerText = "";
        //        else
        //        {
        //            if (objElem.Attributes["codeid"] != null)
        //            {
        //                objElem.Attributes["codeid"].InnerText = "-1";
        //                objElem.InnerText = "";

        //            }
        //        }
        //    }
        //}
        #endregion 


        // MITS #34276 Starts    Entity ID Type Permission starts
        /// <summary>
        /// function to validate Entity ID Type Permission 
        /// </summary>
        /// <param name="obDataView"></param>
        /// <returns></returns>
        private void ValidateButton(string p_sValidateNodes)
        {
            if (!String.IsNullOrEmpty(p_sValidateNodes))
            {
                HiddenField hfEntityIDType = new HiddenField();
                hfEntityIDType.ID = "hfEntityIDTypePermission";
                hfEntityIDType.ClientIDMode = ClientIDMode.Static;
                hfEntityIDType.Value = p_sValidateNodes;
                GridDiv.Controls.Add(hfEntityIDType);
                if (p_sValidateNodes.Contains("ValidateNewVendor"))
                {
                    HiddenField hfVendor = new HiddenField();
                    hfVendor.ID = "hfNewVendorPermission";
                    hfVendor.Value = "ValidateNewVendor";
                    GridDiv.Controls.Add(hfVendor);
                }
            }
        }
        //  //MITS #34276  Ends  Entity ID Type Permission ends
        #endregion Private Functions
    }

}
