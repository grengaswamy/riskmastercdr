﻿<%@ control language="C#" autoeventwireup="true" codefile="MultiSelectListbox.ascx.cs"
    inherits="MultiSelectListBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td colspan="4">
            <asp:updatepanel id="UpdatePanel1" runat="server">
                <contenttemplate>
                    <table width="100%" align="center">
                        <tr>
                            <td width="40%" align="center">
                                <asp:listbox id="lbUnselected" runat="server" width="250px" height="140" selectionmode="Multiple"
                                    borderstyle="Outset"></asp:listbox>
                            </td>
                            <td align="center" width="10%">
                                <asp:button id="btnaddall" runat="server" causesvalidation="false" cssclass="button"
                                    text=">>" width="25px" onclick="btnaddall_Click"></asp:button><br>
                                <asp:button id="btnadditem" runat="server" causesvalidation="false" cssclass="button"
                                    text=">" width="25px" onclick="btnadditem_Click"></asp:button><br>
                                <asp:button id="btnremoveitem" runat="server" causesvalidation="false" cssclass="button"
                                    text="<" width="25px" onclick="btnremoveitem_Click"></asp:button><br>
                                <asp:button id="btnremoveall" runat="server" causesvalidation="false" cssclass="button"
                                    text="<<" width="25px" onclick="btnremoveall_Click"></asp:button>
                            </td>
                            <td width="40%" align="center">
                                <asp:listbox id="lbSelected" runat="server" width="250px" height="140" selectionmode="Multiple">
                                </asp:listbox>
                            </td>
                        </tr>
                    </table>
                </contenttemplate>
            </asp:updatepanel>
        </td>
    </tr>
</table>
