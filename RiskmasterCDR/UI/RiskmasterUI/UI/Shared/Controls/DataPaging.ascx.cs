﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Shared.Controls
{
    public partial class DataPaging : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hdCurrentPage.Attributes.Add("RMXRef", Target + "/CurrentPage");
            hdPageSize.Attributes.Add("RMXRef", Target + "/hdnPageSize");
            hdTotalPages.Attributes.Add("RMXRef", Target + "/hdTotalPages");

            if (hdCurrentPage.Text=="1")
            {
                lnkFirst.Enabled = false;
                lnkPrev.Enabled = false;
                if (hdCurrentPage.Text == hdTotalPages.Text || hdTotalPages.Text == "0")
                {
                    lnkNext.Enabled = false;
                    lnkLast.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                    lnkLast.Enabled = true;
                }

                lblCurrentPage.Text = "1";
                lblTotalPage.Text = hdTotalPages.Text;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            hdCurrentPage.Attributes.Add("RMXRef", Target + "/CurrentPage");
            hdPageSize.Attributes.Add("RMXRef", Target + "/hdnPageSize");
            hdTotalPages.Attributes.Add("RMXRef", Target + "/hdTotalPages");
        }

        protected void LinkCommand_Click(object sender, CommandEventArgs e)
        {
            int iCurrentPage = 0;
            int iPreviousPage = 0;
            int iPageSize = Conversion.ConvertObjToInt(hdPageSize.Text); ;
            int iTotalRows = Conversion.ConvertObjToInt(hdTotalRows.Value);

            lnkFirst.Enabled = true;
            lnkLast.Enabled = true;
            lnkPrev.Enabled = true;
            lnkNext.Enabled = true;

            bool blnSuccess;
            
            switch (e.CommandName.ToUpper())
            {
                case "FIRST":
                    {
                        hdCurrentPage.Text = "1";
                        iCurrentPage = Conversion.ConvertStrToInteger(hdCurrentPage.Text);
                        lnkFirst.Enabled = false;
                        lnkPrev.Enabled = false;

                        lblCurrentPage.Text = iCurrentPage.ToString();
                        lblTotalPage.Text = hdTotalPages.Text;
                        break;
                    }
                case "LAST":
                    {
                        iCurrentPage = Conversion.ConvertStrToInteger(hdTotalPages.Text) ;

                        lnkLast.Enabled = false;
                        lnkNext.Enabled = false;

                        lblCurrentPage.Text = iCurrentPage.ToString();
                        lblTotalPage.Text = hdTotalPages.Text;

                        break;
                    }
                case "PREV":
                    {
                       iCurrentPage = Conversion.ConvertStrToInteger(hdCurrentPage.Text);
                        
                        if (iCurrentPage == 1)
                        {
                            lnkFirst.Enabled = false;
                            lnkPrev.Enabled = false;
                        }
                        lblCurrentPage.Text = iCurrentPage.ToString();
                        lblTotalPage.Text = hdTotalPages.Text;

                        break;
                    }
                case "NEXT":
                    {
                        
                        iCurrentPage = Conversion.ConvertStrToInteger(hdCurrentPage.Text);

                        if (iCurrentPage  == Conversion.ConvertObjToInt(hdTotalPages.Text))
                        {
                            lnkLast.Enabled = false;
                            lnkNext.Enabled = false;
                        }

                        lblCurrentPage.Text = iCurrentPage.ToString();
                        lblTotalPage.Text = hdTotalPages.Text;
                        break;
                    }
                
            }
        }

        /// <summary>
        /// For FDM pages this string refers to the XML node to which the grid is bound.
        /// </summary>
        private string m_sTarget;

        /// <summary>
        ///  The Property to expose the Target to the Parent Page.
        /// </summary>
        public string Target
        {
            get
            {
                return m_sTarget;
            }
            set
            {
                m_sTarget = value;
            }
        }

        /// <summary>
        /// For FDM pages this string refers to the XML node to which the grid is bound.
        /// </summary>
        private int m_iPageSize;
        public int PageSize
        {
            get
            {
                return m_iPageSize;
            }
            set
            {
                m_iPageSize = value;
                hdPageSize.Text = m_iPageSize.ToString();
            }
        }
    }
}