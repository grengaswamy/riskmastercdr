﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadFile.ascx.cs" Inherits="Riskmaster.UI.UI.Shared.Controls.UploadFile" %>
<script type="text/javascript" >
    var esckeycount = 0;
    function getFilesToUpload(obj) {
        var cntrlId = obj.id.toString();
        cntrlId = cntrlId.substr(0, cntrlId.lastIndexOf("_") + 1);
        var txtFileUpload = cntrlId + "txtFileUpload";
        var cntrl = document.getElementById(txtFileUpload);
        var dialog = new ActiveXObject('MSComDlg.CommonDialog');
        dialog.Filter = 'All Files(*.*)|*.*';
        dialog.MaxFileSize = 1;
        dialog.MaxFileSize = 32767;
        dialog.DialogTitle = 'Choose Files to Upload';
        dialog.Flags = 0x200 | 0x80000 | 0x800 | 0x4 | 0x200000
        dialog.ShowOpen();
        cntrl.value = dialog.fileName;
    }
    function restrictInput(obj) {
        if ((event.keyCode >= 35 && event.keyCode <= 40) || (event.keyCode == 9)) { return true; } // ASCII values for arrow keys, home and end keys, tab key.
        if (event.keyCode == 8) { // ASCII values for backspace
            esckeycount = 0;
            obj.value = "";
        }
        if (event.keyCode == 27) { //ASCII values for Escape
            esckeycount = esckeycount + 1;
            if (esckeycount > 1) {
                obj.value = "";
            }
        }
        return false;
    }
    </script>
<asp:TextBox ID="txtFileUpload" runat="server" Width="180" style="background:LightGray" onkeydown="return restrictInput(this)" ondblclick="getFilesToUpload(this)"></asp:TextBox>
<asp:Button ID="btnUpload" runat="server" 
    OnClientClick="getFilesToUpload(this);" onclick="btnUpload_Click" />
