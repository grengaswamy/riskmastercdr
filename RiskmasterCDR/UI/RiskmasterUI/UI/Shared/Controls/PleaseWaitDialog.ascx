﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PleaseWaitDialog.ascx.cs" Inherits="Riskmaster.UI.Shared.Controls.PleaseWaitDialog" %>

<div id="pleaseWaitFrame" runat="server" style="display:none" class="CustompleaseWaitFrame" >

        <img onclick="pleaseWait.pleaseWait('stop');" src="/RiskmasterUI/Images/Loading1.gif" />
</div>
<script  type="text/javascript" language="javascript">
	<%if (CustomMessage.Trim()!="") {%>
	    var pleaseWait = new PleaseWaitDialog("PleaseWaitIFrame", "pleaseWait", 4, "pleasewait_", ".gif", 125, 308, 172,'<%=CustomMessage %>','','<%=pleaseWaitFrame.ClientID%>'); 
	<%}%>
	<%else{%>
	    var pleaseWait = new PleaseWaitDialog("PleaseWaitIFrame", "pleaseWait", 4, "pleasewait_", ".gif", 125, 147, 207,'','','<%=pleaseWaitFrame.ClientID%>');
	 <%}%>
</script>