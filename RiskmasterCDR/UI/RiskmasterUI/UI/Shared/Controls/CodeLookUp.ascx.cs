﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.CodeDom;
using System.Web.Compilation;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.Shared.Controls
{
    [ValidationProperty("CodeId")]
    public partial class CodeLookUp : System.Web.UI.UserControl
    {
        private string m_sCodeTable;
        private string m_RMXRef;
        private string m_CodeText;
        private string m_CodeId;
        private string m_sFilter; //Added By Tushar for VACo  //Mona:Adding code filter like showing codes whith parent "Open":R6

        public string CodeTable
        {
            get
            {
                return m_sCodeTable;
            }
            set
            {
                m_sCodeTable = value;
            }
        }
        //Added By Tushar for VACo //Mona:Adding codefilter like showing codes whith parent "Open":R6
        public string Filter
        {
            get
            {
                return m_sFilter ;
            }
            set
            {
                m_sFilter = value;
            }
        }

        public string RMXRef
        {
            set
            {
                m_RMXRef = value;
            }
        }
        private string m_PowerViewReadOnly;
        public string powerviewreadonly
        {
            set
            {
                m_PowerViewReadOnly = value;
            }
        }
        private string m_sFieldTitle;
        public string FieldTitle
        {
            get
            {
                return m_sFieldTitle;
            }
            set
            {
                m_sFieldTitle = value;
            }
        }
        private string m_sDescSearch;
        public string DescSearch
        {
            get
            {
                return m_sDescSearch;
            }
            set
            {
                m_sDescSearch = value;
            }
        }
        private string m_sControlName;
        public string ControlName
        {
            get
            {
                return m_sControlName;
            }
            set
            {
                m_sControlName = value;
            }
        }
        private string m_sParentNode;
        public string ParentNode
        {
            get
            {
                return m_sParentNode;
            }
            set
            {
                m_sParentNode = value;
            }
        }

        public string CodeId
        {
            get
            {
                m_CodeId = codelookup_cid.Text;
                return m_CodeId;
            }
            set
            {
               m_CodeId = value;
               codelookup_cid.Text = m_CodeId;
            }
        }
        public string CodeIdValue
        {
            get
            {
                return codelookup_cid.Text;
            }
        }

        public string CodeText
        {
            get
            {
                m_CodeText = codelookup.Text;
                return m_CodeText;
            }
            set
            {
                m_CodeText = value;
                codelookup.Text = value;
            }
        }
        public string CodeTextValue
        {
            get
            {
                return codelookup.Text;
            }
        }

        private bool m_bRequired = false;
        public bool Required
        {
            get
            {
                return m_bRequired;
            }
            set
            {
                m_bRequired = value;
            }
        }
        private string m_sTabIndex;
        public string TabIndex
        {
            get
            {
                return m_sTabIndex;
            }
            set
            {
                m_sTabIndex = value;
            }
        }
        private string m_sValidationGroup = string.Empty;
        public string ValidationGroup
        {
            get
            {
                return m_sValidationGroup;
            }
            set
            {
                m_sValidationGroup = value;
            }
        }
        private void SetInitialProperties()
        {
            if (!string.IsNullOrEmpty(m_PowerViewReadOnly))
            {
                codelookup.Attributes.Add("powerviewreadonly", m_PowerViewReadOnly);
            }
            if (!string.IsNullOrEmpty(m_RMXRef))
            {
                codelookup.Attributes.Add("RMXRef", m_RMXRef);
                codelookup_cid.Attributes.Add("RMXRef", m_RMXRef + "/@codeid");
            }
            if (!string.IsNullOrEmpty(m_CodeId))
            {
                codelookup_cid.Text = m_CodeId;
            }
            if (!string.IsNullOrEmpty(m_CodeText))
            {
                codelookup.Text = m_CodeText;
            }
            //Added By Tushar for VACo //Mona:Adding codefilter like showing codes whith parent "Open":R6
            if (!string.IsNullOrEmpty(m_sFilter ))
            {
                codelookup.Attributes.Add("Filter", m_sFilter);
            }
        }
        // Public Property exposed to disable/enable the UserControl
        public bool Enabled
        {
            set
            {
                if (value == true)
                {
                    codelookup.ReadOnly = false;
                    //codelookup.Style.Add(HtmlTextWriterStyle.BackgroundColor, "White");//deb:Comment gets style automatically :MITS 20003
                    codelookup.Style.Add(HtmlTextWriterStyle.BackgroundColor, "");
                }
                else
                {
                    codelookup.ReadOnly = true;
                    codelookup.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                }

                codelookupbtn.Visible = value;
            }
        }
        /// <summary>
        /// Property For only disabling the Buttons but not disable them. asharma326 JIRA 6411
        /// </summary>
        public bool ReadOnly
        {
            set
            {
                if (value == true)
                {
                    codelookup.Enabled = false;
                    codelookup.Style.Add(HtmlTextWriterStyle.BackgroundColor, "");
                    codelookupbtn.Enabled = false;
                }
                else
                {
                    codelookup.Enabled = true;
                    codelookup.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                    codelookupbtn.Enabled = true;
                }
            }
        }
        //Deb MITS 26077
        public bool ShowHide
        {
            set
            {
                if (value == true)
                {
                    codelookup.Style.Add(HtmlTextWriterStyle.Display, "");
                }
                else
                {
                    codelookup.Style.Add(HtmlTextWriterStyle.Display, "none");
                }
                codelookupbtn.Visible = value;
            }
        }
        //Deb MITS 26077
        public string OnChange
        {
            set
            {
                if (codelookup != null)
                {
                    if (value != "")
                    {
                        codelookup.Attributes["onchange"] = value + "; lookupTextChanged(this);";
                    }
                    else
                    {
                        codelookup.Attributes["onchange"] = "lookupTextChanged(this);";
                    }
                }
            }
        }

        public string GroupAssocClientId
        {
            get
            {
                return codelookup_cid.ClientID;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //March 25:Raman Bhatia: Incase of Juris postback we have new controls and hence we need to run this event again
            //Sept 23: Nitin Sharma: In case of Reserve worksheet postback we have new controls and hence we need to run this event again
            if (!IsPostBack || (m_RMXRef != null && m_RMXRef.Contains("Jurisdictionals")) || (m_RMXRef != null && m_RMXRef.Contains("ReserveWorksheetReason")))
            {
                SetInitialProperties();

                //Tushar done for VACo //Mona:Adding codefilter like showing codes whith parent "Open":R6
                string sOnclick = string.Format("return selectCode('{0}','{1}_codelookup', '{2}', '{3}','{4}')", CodeTable, ControlName, ParentNode, m_sDescSearch, Filter);
                codelookupbtn.Attributes["onclick"] = sOnclick;
                //irajagopal 2/24/2014 Commenting the below line of code for fixing 35458
                //codelookup.Attributes.Add("onchange", "setDataChanged(true);");   //MITS 35274 Prashant added  
                if (!string.IsNullOrEmpty(TabIndex))
                {
                    //TabIndex is crashing whenever supplementals assign large integers..modified by Raman Bhatia
                    //06/08/2009 Aim to set tabindex as html

                    codelookup.Attributes.Add("tabindex", (Convert.ToInt32(TabIndex)).ToString());
                    codelookupbtn.Attributes.Add("tabindex", (Convert.ToInt32(TabIndex) + 1).ToString());
                    
                    //codelookup.TabIndex = Int16.Parse(TabIndex);
                    //codelookupbtn.TabIndex = Convert.ToInt16(Convert.ToInt32(TabIndex) + 1);

                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
 
        }
        
    }

   



}
