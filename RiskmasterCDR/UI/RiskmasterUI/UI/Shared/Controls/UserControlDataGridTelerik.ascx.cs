﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Data;
using System.Data.OleDb;
using System.Text;
using System.Collections;
using Riskmaster.AppHelpers;
using Telerik.Web.UI;
namespace Riskmaster.UI.Shared.Controls
{
    public partial class UserControlDataGridTelerik : System.Web.UI.UserControl
    {
        bool bNoSort = false; 
        #region Properties
        /// <summary>
        /// The Property Exposed for Binding the Grid with a Datasource from the Page invoking the User Control
        /// This property is exposed for Non FDM Screens where any generic method might not be available to bind the grid.
        /// </summary>
        public object GridDataSource
        {
            get
            {
                return gvData.DataSource;
            }
            set
            {
                gvData.DataSource = value;
                gvData.DataBind();
            }
        }

        /// <summary>
        /// The String to Store the Grid Name. The Grid Name is equal to the User Control Id in the Parent Page.
        /// It is used as a parameter to Javascript when LinkColumn is clicked
        /// </summary>
        private string m_sGridName;

        /// <summary>
        /// The Property to expose the Grid Name to the Parent Page. 
        /// </summary>
        public string GridName
        {
            get
            {
                return m_sGridName;
            }
            set
            {
                m_sGridName = value;
            }
        }

        /// <summary>
        /// Displays the Grid Title. The Grid Title is set at the Parent Page and is displayed in a Label.
        /// </summary>
        public string GridTitle
        {
            set
            {
                if (value != "" && value != null)
                    lblGridTitle.Text = value;
                else
                    lblGridTitle.Visible = false;
            }
        }

        /// <summary>
        /// Sets the Width of the Grid. The Grid Width needs to be passed from the Parent Page.
        /// </summary>
        public string Width
        {
            set
            {
                if (string.IsNullOrEmpty(value)) value = "100%";
                GridDiv.Style.Add(HtmlTextWriterStyle.Width, value);
            }
        }

        /// <summary>
        /// Sets the Width of the Grid. The Grid Width needs to be passed from the Parent Page.
        /// </summary
        public string Height
        {
            set
            {
                if (string.IsNullOrEmpty(value)) value = "180px";
                GridDiv.Style.Add(HtmlTextWriterStyle.Height, value);
            }
        }

        /// <summary>
        /// Adds the Scrollbar to grid Div if the property is true. If the Property is false then Grid will not have any scrollbar and 
        /// would be displayed without any scrollbar
        /// </summary>
        public bool StyleOverflowAuto
        {
            set
            {
                //if (value)
                //    GridDiv.Style.Add(HtmlTextWriterStyle.Overflow, "auto");
                //else
                //GridDiv.Style.Add(HtmlTextWriterStyle.OverflowY, "scroll");
            }
        }


        /// <summary>
        /// The String to Store the Hidden Column names of Grid. The Column Names are pipe delimited.
        /// </summary>
        private string m_sHideNodes;

        /// <summary>
        /// The Property to expose the Hidden Columns to the Parent Page.
        /// </summary>
        public string HideNodes
        {
            get
            {
                return m_sHideNodes;
            }
            set
            {
                m_sHideNodes = value;
            }
        }

        /// <summary>
        /// For FDM pages this string refers to the XML node to which the grid is bound.
        /// </summary>
        private string m_sTarget;

        /// <summary>
        ///  The Property to expose the Target to the Parent Page.
        /// </summary>
        public string Target
        {
            get
            {
                return m_sTarget;
            }
            set
            {
                m_sTarget = value;
            }
        }

        /// <summary>
        /// For Non FDM pages this string refers to the XML node to which the grid is bound.
        /// </summary>
        private string m_sRef;

        /// <summary>
        ///  The Property to expose the Ref to the Parent Page.
        /// </summary>
        public string Ref
        {
            get
            {
                return m_sRef;
            }
            set
            {
                m_sRef = value;
            }
        }

        /// <summary>
        /// If this property is set,row data is picked from it.
        /// </summary>
        private string m_sDataParam;

        /// <summary>
        ///  The Property to expose the RowDataParam to the Parent Page.
        /// </summary>
        public string RowDataParam
        {
            get
            {
                return m_sDataParam;
            }
            set
            {
                m_sDataParam = value;
            }
        }

        /// <summary>
        /// If this Property is true then the Clone image button will be displayed in the Grid.
        /// </summary>
        private bool m_bShowCloneButton;

        /// <summary>
        /// The Property to expose the ShowCloneButton Property to the Parent Page.
        /// </summary>
        public bool ShowCloneButton
        {
            get
            {
                return m_bShowCloneButton;
            }
            set
            {
                m_bShowCloneButton = value;
                Clone.Visible = value;
            }
        }

        /// <summary>
        /// If this Property is true then a RadioButton Column is displayed in the Grid.
        /// </summary>
        private bool m_bShowRadioButton;

        /// <summary>
        /// The Property to expose the ShowRadioButton Property to the Parent Page.
        /// </summary>
        public bool ShowRadioButton
        {
            get
            {
                return m_bShowRadioButton;
            }
            set
            {
                m_bShowRadioButton = value;
            }
        }
        /// <summary>
        /// If this property is set,blank row is not added when RowDataParam is not null
        /// </summary>
        private bool m_sOverrideBlankRowInsert;

        /// <summary>
        ///  The Property to expose the RowDataParam to the Parent Page.
        /// </summary>
        public bool OverrideBlankRowInsert
        {
            get
            {
                return m_sOverrideBlankRowInsert;
            }
            set
            {
                m_sOverrideBlankRowInsert = value;
            }
        }

        /// <summary>
        /// If this Property is true then only Edit button will be enabled. The user can not add/delete/clone records.
        /// </summary>
        private bool m_bReadonly;

        /// <summary>
        /// The Property to control whether user can add/delete/clone records.
        /// </summary>
        public bool Readonly
        {
            get
            {
                return m_bReadonly;
            }
            set
            {
                m_bReadonly = value;
                New.Visible = (!value);
                Delete.Visible = (!value);
                Clone.Visible = (m_bShowCloneButton && !value);
            }
        }


        /// <summary>
        /// If this Property is true then the Radio Button Column has a Header Caption. 
        /// The Caption if specified through the Property RadioButtonHeader is Set.
        /// If RadioButtonHeader is not specified then By Default "Select One" is the 
        /// Default Caption of the Column
        /// </summary>
        private bool m_bShowRadioButtonHeader;

        /// <summary>
        /// The Property to expose the ShowRadioButtonHeader to the Parent Page
        /// </summary>
        public bool ShowRadioButtonHeader
        {
            get
            {
                return m_bShowRadioButtonHeader;
            }
            set
            {
                m_bShowRadioButtonHeader = value;
            }
        }

        /// <summary>
        /// If this Property is specified and ShowRadioButton and ShowRadioButtonHeader Property is true,
        /// then the Value specified in this field is set as the Column Header Caption.
        /// </summary>
        private string m_sRadioButtonHeader;

        /// <summary>
        /// The Property to expose the RadioButtonHeader to the Parent Page
        /// </summary>
        public string RadioButtonHeader
        {
            get
            {
                return m_sRadioButtonHeader;
            }
            set
            {
                m_sRadioButtonHeader = value;
            }
        }

        /// <summary>
        /// If this Property is true then a CheckBox Column is displayed in the Grid.
        /// </summary>
        private bool m_bShowCheckBox;

        /// <summary>
        /// The Property to expose the ShowCheckBox Property to the Parent Page.
        /// </summary>
        public bool ShowCheckBox
        {
            get
            {
                return m_bShowCheckBox;
            }
            set
            {
                m_bShowCheckBox = value;
            }
        }

        /// <summary>
        /// If this Property is true then the Checkbox Column has a Header Caption. 
        /// The Caption if specified through the Property CheckBoxHeader is Set.
        /// If RadioButtonHeader is not specified then By Default "Select Many" is the 
        /// Default Caption of the Column
        /// </summary>
        private bool m_bShowCheckBoxHeader;

        /// <summary>
        /// The Property to expose the ShowCheckBoxHeader to the Parent Page
        /// </summary>
        public bool ShowCheckBoxHeader
        {
            get
            {
                return m_bShowCheckBoxHeader;
            }
            set
            {
                m_bShowCheckBoxHeader = value;
            }
        }

        /// <summary>
        /// If this Property is specified and ShowCheckBox and ShowCheckBoxHeader Property is true,
        /// then the Value specified in this field is set as the Column Header Caption.
        /// </summary>
        private string m_sCheckBoxHeader;

        /// <summary>
        /// The Property to expose the CheckBoxHeader to the Parent Page
        /// </summary>
        public string CheckBoxHeader
        {
            get
            {
                return m_sCheckBoxHeader;
            }
            set
            {
                m_sCheckBoxHeader = value;
            }
        }

        /// <summary>
        /// If this attribute is specified, then is associated with the Click of radiobutton or checkbox
        /// </summary>
        private string m_sOnClick;

        /// <summary>
        /// The Property to expose the OnClick to the Parent Page
        /// </summary>
        public string OnClick
        {
            get
            {
                return m_sOnClick;
            }
            set
            {
                m_sOnClick = value;
            }
        }

        /// <summary>
        /// Unique Id for the Grid. A column name is specified in this property. Another column name with name "Unique_Hidden" is created which is
        /// bound with the Checkbox or the Radiobutton
        /// </summary>
        private string m_sUniqueId;

        /// <summary>
        /// The Property to expose the UniqueId to the Parent Page
        /// Note - The name of this property is deliberately made Unique_Id and not Uniqueid as in RMX R4 as the ASP .NET page has a property 
        /// UniqueId and creating a property with same name was creating problem.
        /// </summary>
        public string Unique_Id
        {
            get
            {
                return m_sUniqueId;
            }
            set
            {
                m_sUniqueId = value;
            }
        }

        /// <summary>
        /// Specifies the Column width
        /// </summary>
        private double m_dColumnWidth;

        /// <summary>
        /// The Property to expose the Column Width to the Parent Page
        /// </summary>
        public double ColumnWidth
        {
            get
            {
                return m_dColumnWidth;
            }
            set
            {
                m_dColumnWidth = value;
            }
        }

        /// <summary>
        /// Specifies the Column Height
        /// </summary>
        private double m_dColumnHeight;

        /// <summary>
        /// The Property to expose the Column Height to the Parent Page
        /// </summary>
        public double ColumnHeight
        {
            get
            {
                return m_dColumnHeight;
            }
            set
            {
                m_dColumnHeight = value;
            }
        }

        /// <summary>
        /// When the New/Edit button is clicked, the Pop up opens. This attribute specifies the width of the Pop up.
        /// </summary>
        private string m_sPopupWidth;

        /// <summary>
        /// The Property to expose the Pop up width to the Parent Page
        /// </summary>
        public string PopupWidth
        {
            get
            {
                return m_sPopupWidth;
            }
            set
            {
                m_sPopupWidth = value;
            }
        }

        /// <summary>
        /// When the New/Edit button is clicked, the Pop up opens. This attribute specifies the height of the Pop up.
        /// </summary>
        private string m_sPopupHeight;

        /// <summary>
        /// The Property to expose the Pop up Height to the Parent Page
        /// </summary>
        public string PopupHeight
        {
            get
            {
                return m_sPopupHeight;
            }
            set
            {
                m_sPopupHeight = value;
            }
        }

        /// <summary>
        /// If the Grid Header needs to be shown, this property needs to be set to true otherwise false
        /// </summary>
        private bool m_bShowHeader;

        /// <summary>
        /// The Property to expose the Show Header to the Parent Page
        /// </summary>
        public bool ShowHeader
        {
            get
            {
                return m_bShowHeader;
            }
            set
            {
                m_bShowHeader = value;
            }
        }

        /// <summary>
        /// If the Grid Footer needs to be shown, this property needs to be set to true otherwise false
        /// </summary>
        private bool m_bShowFooter;

        /// <summary>
        /// The Property to expose the Show Footer to the Parent Page
        /// </summary>
        public bool ShowFooter
        {
            get
            {
                return m_bShowFooter;
            }
            set
            {
                m_bShowFooter = value;
                gvData.ShowFooter = value;
            }
        }

        /// <summary>
        /// The name of buttons present in this string will not be displayed. 
        /// For Control like Grid, this string will contain a string like "New|Edit|Delete" so all the buttons are hidden.
        /// For screens like Policy Mangement, In case of policy only Edit button is shown and New and delete button are not.
        /// so in that case, we would pass "New|Delete" in this variable.
        /// </summary>
        private string m_sHideButtons;

        /// <summary>
        /// The Property to expose the Hide Buttons to the Parent Page
        /// </summary>
        public string HideButtons
        {
            get
            {
                return m_sHideButtons;
            }
            set
            {
                m_sHideButtons = value;
            }
        }

        /// <summary>
        /// Multiple Column Names will be present in this string which will be converted into a link column. 
        /// A hyperlink will be provided for all of the columns and clicking this link will invoke a javascript function
        /// If there are two columns A and B to be displayed as hyperlink, then LinkColumn will have value A|B
        /// </summary>
        private string m_sLinkColumn;

        /// <summary>
        /// The Property to expose the Link Column to the Parent Page
        /// </summary>
        public string LinkColumn
        {
            get
            {
                return m_sLinkColumn;
            }
            set
            {
                m_sLinkColumn = value;
            }
        }

        /// <summary>
        /// The Arraylist contains all the Column Names which needs to be converted into the Link Column.
        /// </summary>
        private ArrayList m_alLinkColumns;

        /// <summary>
        /// The Property to expose the Link Columns to the Parent Page
        /// </summary>
        private ArrayList LinkColumns
        {

            get
            {
                String[] sLinkColumns;
                if (LinkColumn != null && LinkColumn != "")
                {
                    sLinkColumns = LinkColumn.Split(new char[] { '|' });
                    if (sLinkColumns.Length != 0)
                    {
                        m_alLinkColumns = new ArrayList(sLinkColumns);
                    }
                    else
                    {
                        m_alLinkColumns = new ArrayList();
                        m_alLinkColumns.Add(LinkColumn);
                    }
                }
                return m_alLinkColumns;
            }
        }

        /// <summary>
        /// The Link Type to be rendered. If we need to render all the columns as Hyperlink then we do not need this attribute.
        /// But if we need to change the default behavior then, we need to specify the Link Types for all the Columns.
        /// For ex if A and B are two column, with A as link and B as Image then we need to specify Link|Image in this Attribute
        /// </summary>
        private string m_sLinkType;

        /// <summary>
        /// The Property to expose the Link Type to the Parent Page
        /// </summary>
        public string LinkType
        {
            get
            {
                return m_sLinkType;
            }
            set
            {
                m_sLinkType = value;
            }
        }

        /// <summary>
        /// The Arraylist contains all the Link Types.
        /// </summary>
        private ArrayList m_alLinkTypes;

        /// <summary>
        /// The Property to expose the Link Types to the Parent Page
        /// </summary>
        private ArrayList LinkTypes
        {
            get
            {
                String[] sLinkTypes;
                if (LinkType != null && LinkType != "")
                {
                    sLinkTypes = LinkType.Split(new char[] { '|' });
                    if (sLinkTypes.Length != 0)
                    {
                        m_alLinkTypes = new ArrayList(sLinkTypes);
                    }
                    else
                    {
                        m_alLinkTypes = new ArrayList();
                        m_alLinkTypes.Add(LinkType);
                    }
                }
                return m_alLinkTypes;
            }
        }

        /// <summary>
        /// If The links has to be rendered as Image, then the Image Url needs to be specified here. 
        /// If A, B and C are Link Columns and B is image then this Property needs to be specified as |B_imageUrl|
        /// </summary>
        private string m_sImageUrl;

        /// <summary>
        /// The Property to expose the Image Url to the Parent Page
        /// </summary>
        public string ImageUrl
        {
            get
            {
                return m_sImageUrl;
            }
            set
            {
                m_sImageUrl = value;
            }
        }

        /// <summary>
        /// The Arraylist contains all the Image Urls.
        /// </summary>
        private ArrayList m_alImageUrls;

        /// <summary>
        /// The Property to expose the Image Urls to the Parent Page
        /// </summary>
        private ArrayList ImageUrls
        {
            get
            {
                String[] sImageUrls;
                if (ImageUrl != null && ImageUrl != "")
                {
                    sImageUrls = ImageUrl.Split(new char[] { '|' });
                    if (sImageUrls.Length != 0)
                    {
                        for (int i = 0; i < sImageUrls.Length; i++)
                        {
                            if (!string.IsNullOrEmpty(sImageUrls[i]) && !sImageUrls[i].Contains("/"))
                            {
                                sImageUrls[i] = "/RiskmasterUI/Images/" + sImageUrls[i];
                            }
                        }
                        m_alImageUrls = new ArrayList(sImageUrls);
                    }
                    else
                    {
                        m_alImageUrls = new ArrayList();
                        m_alImageUrls.Add(ImageUrl);
                    }
                }
                return m_alImageUrls;
            }
        }

        private string m_sTextColumn;

        public string TextColumn
        {
            get
            {
                return m_sTextColumn;
            }
            set
            {
                m_sTextColumn = value;
            }
        }


        /// <summary>
        /// This distinguishes between the Grid and GridAndButtons. While rendering and saving we do not show/use the last row for GridAndButtons
        /// This Variable will help in distinguishing between Grid and GridandButtons
        /// </summary>
        private string m_sType;

        /// <summary>
        /// The Property to expose the Type (Grid or GridandButtons) to the Parent Page
        /// </summary>
        public string Type
        {
            get
            {
                return m_sType;
            }
            set
            {
                m_sType = value;
            }
        }

        /// <summary>
        /// Exposes the grid of the usercontrol to the Parent Page.
        /// This can be used for making some page specific customizations from the code behind of that page.
        /// </summary>
        public object GridView
        {
            get
            {
                return gvData;
            }
        }

        private bool m_bAlign;
        public bool Align
        {
            get
            {
                return m_bAlign;
            }
            set
            {
                m_bAlign = value;
            }
        }
        private string m_sCenterAlignColumns;
        public string CenterAlignColumns
        {
            get
            {
                return m_sCenterAlignColumns;
            }
            set
            {
                m_sCenterAlignColumns = value;
            }
        }

        private string m_sRightAlignColumns;
        public string RightAlignColumns
        {
            get
            {
                return m_sRightAlignColumns;
            }
            set
            {
                m_sRightAlignColumns = value;
            }
        }

        private bool m_RefImplementation;

        public bool RefImplementation
        {
            get
            {
                return m_RefImplementation;
            }
            set
            {
                m_RefImplementation = value;
            }
        }

        private bool m_IncludeLastRecord;

        public bool IncludeLastRecord
        {
            get
            {
                return m_IncludeLastRecord;
            }
            set
            {
                m_IncludeLastRecord = value;
            }
        }
        // Pradyumna 11/17/2014 MITS 36930 - start
        /// <summary>
        /// Property to allow grid column resizing
        /// </summary>
        public bool AllowColumnResize { get; set; }

        /// <summary>
        /// Provide with of each column seperated by pipe (|) character.
        /// Value will be treated as percentage.
        /// To provide percentage only of nth column use n-1 pipe character. eg. If 3rd column is 50% provide value as: "||50"
        /// </summary>
        public string ColumnWidhtPercent { get; set; }
        // Pradyumna 11/17/2014 MITS 36930 - End
       #endregion Properties
        

        protected void Page_Load(object sender, EventArgs e)
        {
            // Pradyumna 11/17/2014 MITS 36930
            gvData.ClientSettings.Resizing.AllowColumnResize = AllowColumnResize;
            gvData.HeaderStyle.CssClass = "msgheader";
            gvData.ItemStyle.CssClass = "datatd1";
            gvData.AlternatingItemStyle.CssClass = "datatd";

            // Associate the Grid Buttons with the Attributes.
            // We need to do this at run time as onmouseover and onmouseout attributes are not exposed for Imagebutton.
            New.Attributes.Add("onmouseover", "this.src='/RiskmasterUI/Images/tb_new_active.png'");
            New.Attributes.Add("onmouseout", "this.src='/RiskmasterUI/Images/tb_new_mo.png'");

            // After the javascriopt function is called, we are returning false from here.
            // as we do not want a postback here.

            New.Attributes.Add("onclick", "openGridAddEditWindow('" + GridName + "','add','" +
                                            PopupWidth + "','" + PopupHeight + "', document.getElementById('" + GridName + "_" + OtherParams.ID + "').value);return false;");

            Edit.Attributes.Add("onmouseover", "this.src='/RiskmasterUI/Images/tb_edit_active.png'");
            Edit.Attributes.Add("onmouseout", "this.src='/RiskmasterUI/Images/tb_edit_mo.png'");
            Edit.Attributes.Add("onclick", "openGridAddEditWindow('" + GridName + "','edit','" +
                                            PopupWidth + "','" + PopupHeight + "', document.getElementById('" + GridName + "_" + OtherParams.ID + "').value);return false;");

            Clone.Attributes.Add("onmouseover", "this.src='/RiskmasterUI/Images/tb_clone_active.png'");
            Clone.Attributes.Add("onmouseout", "this.src='/RiskmasterUI/Images/tb_clone_mo.png'");
            Clone.Attributes.Add("onclick", "openGridAddEditWindow('" + GridName + "','clone','" +
                                            PopupWidth + "','" + PopupHeight + "', document.getElementById('" + GridName + "_" + OtherParams.ID + "').value);return false;");

            Delete.Attributes.Add("onmouseover", "this.src='/RiskmasterUI/Images/tb_delete_active.png'");
            Delete.Attributes.Add("onmouseout", "this.src='/RiskmasterUI/Images/tb_delete_mo.png'");
            Delete.Attributes.Add("onclick", "return validateGridForDeletion('" + GridName + "');");
                  
        }

        protected void gvData_ItemDataBound(object sender, GridItemEventArgs e)
        {// Check if the Row is header, and if the ShowHeader is false, then hide it.
            if (e.Item is GridHeaderItem)
            {
                if (!ShowHeader)
                    e.Item.Visible = false;
            }

            // If the LinkColumn attribute is specified, then that column of grid is made hyperlink and on its click 
            // a javascript function is called.
            if (e.Item is GridDataItem)
            {
                GridDataItem  grdItem = (GridDataItem)e.Item;
                TableCell tcGridCell;
                LinkButton lbLinkColumn;
                Image imLinkColumn;
                WebControl wbLinkColumn;
                int iUniqueIdColumnIndex;
                int iCellPosition = 0;
                
                //Add a hidden field for Data (XML string)
                if (grdItem["Data"] != null)
                {


                    HiddenField hf = new HiddenField();
                    tcGridCell = grdItem["Data"];
                    int iIndex = e.Item.RowIndex + 2;
                    if (iIndex.ToString().Length == 1)
                        hf.ID = "|0" + iIndex.ToString() + "|" + "Data";
                    else
                        hf.ID = "|" + iIndex.ToString() + "|" + "Data";
                    hf.Value = tcGridCell.Text;
                    grdItem["Data"].Controls.Add(hf);

                    
                }
                //if (grdItem["UniqueId"].Text == "0")
                ////{
                //    (grdItem["Select"].Controls[0] as CheckBox).Visible = false;
                //    //smishra54: MITS 26222- displaying blank values on all cells for blank rows (Auto assign adjuster screen)
                //    for (int cellIndex = 0; cellIndex < grdItem.Cells.Count;cellIndex++ )
                //    {
                //        if (!string.IsNullOrEmpty(Server.HtmlDecode(grdItem.Cells[cellIndex].Text).Trim()))
                //            grdItem.Cells[cellIndex].Text = string.Empty;
                //    }
                //    //smishra54:end
                //}
    // Ijha: MITS 26535: Adjuster's Divided in groups with a broad Line
                 
                DataRowView oDRView = (DataRowView)e.Item.DataItem;
                if (oDRView.Row.Table.Columns.Contains("GroupIdentifier") && oDRView["GroupIdentifier"] != null && string.Equals(oDRView["GroupIdentifier"], "1") && bNoSort == false)
                {

                    for (int cellIndex = 0; cellIndex < grdItem.Cells.Count; cellIndex++)
                    {
                        grdItem.Cells[cellIndex].Style.Add("BORDER-BOTTOM", "5px outset #CCC");
                    }
                }

            }
        }

        
        protected void gvData_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            GridSortExpression sGridSort = new GridSortExpression();
            
            switch(e.OldSortOrder)
            {
                case GridSortOrder.None:
                    sGridSort.FieldName = e.SortExpression;
                    sGridSort.SortOrder = GridSortOrder.Descending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sGridSort);
                    bNoSort = true;
                    break;

                case GridSortOrder.Ascending:                   
                    sGridSort.FieldName = e.SortExpression;
                    sGridSort.SortOrder = GridSortOrder.None;
                    e.Item.OwnerTableView.SortExpressions.Clear();                  
                    break;

                case GridSortOrder.Descending:                    
                    sGridSort.FieldName = e.SortExpression;
                    sGridSort.SortOrder = GridSortOrder.Ascending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sGridSort);
                    bNoSort = true;
                    break;
            }

            e.Canceled = true;
            gvData.Rebind();
        }
    // Ijha End
        protected void gvData_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {


            if (ViewState["UserControlDataSet"] != null)
            {
                try
                {
                    DataSet dsData = (DataSet)ViewState["UserControlDataSet"];

                    gvData.DataSource = dsData.Tables[0];
                }
                catch
                {
                    gvData.Visible = false;
                   
                }
            }

        }


        
        protected void gvData_DataBound(object sender, EventArgs e)
        {
            HideColumnAndRow(false);//Neha

            if (Align)
            {
                // By Default all the Columns are Left Alligned

                // Get Center Align Columns
                if (CenterAlignColumns != null)
                {
                    string[] caColumns = CenterAlignColumns.Split(new char[] { '|' });
                    foreach (string str in caColumns)
                    {
                        int iIndex = GetIndexFromColumnName(str, false);
                        gvData.Columns[iIndex].HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                        gvData.Columns[iIndex].ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    }
                }

                // Get Right Align Columns
                if (RightAlignColumns != null)
                {
                    string[] raColumns = RightAlignColumns.Split(new char[] { '|' });
                    foreach (string str in raColumns)
                    {
                        int iIndex = GetIndexFromColumnName(str, false);
                        gvData.Columns[iIndex].HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                        gvData.Columns[iIndex].ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    }
                }
            }
        }

        /// Added by Pradyumna 11/17/2014 MITS 36930
        protected void gvData_PreRender(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(ColumnWidhtPercent))
            {
                double width = 0d;
                string[] colWidths = ColumnWidhtPercent.Split(new char[] { '|' });
                int iCounter = 0;
                int iGridColumnCount = gvData.Columns.Count;
                foreach (string colWidht in colWidths)
                {
                    if (!String.IsNullOrWhiteSpace(colWidht))
                    {
                        Double.TryParse(colWidht, out width);
                        if (width > 0 && iCounter < iGridColumnCount)
                        {
                            gvData.MasterTableView.Columns[iCounter].HeaderStyle.Width = Unit.Percentage(width);
                        }
                    }
                    iCounter++;
                }
            }
        }
       
        public XmlDocument GenerateXml()
        {
            XmlDocument objXmlDocument = null;
            XmlElement objElement = null;
            XmlElement objRootElement = null;
            int iRunningNumber = 0;
            int iCurrPrimaryKey = 0;
            XmlNode objPrimaryColumn = null;
            XmlNode objRootNode = null;
            objXmlDocument = new XmlDocument();
            string sRootName = GridName.Replace("Grid", "");
            StartDocument(ref objXmlDocument, ref objRootElement, sRootName);
            CreateElement(objRootElement, "listhead", ref objElement);

            string sListHead = hdListHead.Value;

            XmlDocument objListHeadDoc = null;

            if (!string.IsNullOrEmpty(sListHead))
            {
                sListHead = sListHead.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                objListHeadDoc = new XmlDocument();
                objListHeadDoc.LoadXml(sListHead);
                objElement.InnerXml = objListHeadDoc.DocumentElement.InnerXml;
            }

            // Create Data from the Xml Stored in the Hidden Column
            int iGridRowCount = gvData.Items.Count;

            // npadhy Get the Postback Action. This will be used only when the IncludeLastRecord is True, If the Post back Action is 
            // Delete then we done want to include the last record in XMLin
            TextBox txtSysPostBackAction = (TextBox)this.Parent.FindControl("SysPostBackAction");
            string sSysPostBackAction = string.Empty;
            if (txtSysPostBackAction != null)
                sSysPostBackAction = txtSysPostBackAction.Text;
            // npadhy Get the Mode. This will be used only when the IncludeLastRecord is True, If the Moden is 
            // Edit then we done want to include the last record in XMLin
            TextBox txtMode = (TextBox)this.Parent.FindControl(GridName + "_Action");
            string sMode = string.Empty;
            if (txtMode != null)
            {
                sMode = txtMode.Text.ToLower();
            }
            //if ((!IncludeLastRecord || (IncludeLastRecord && (sSysPostBackAction == "Delete" || sMode == "edit")))&& !PopupGrid) //Added PopupGrid condition for R7:Add Employee Data Elements
            if (!IncludeLastRecord || (IncludeLastRecord && (sSysPostBackAction == "Delete" || sMode == "edit")))
            {
                // If Delete was Clicked, then we need to reset it as It will give us problem while adding next record
                if (IncludeLastRecord && sSysPostBackAction == "Delete")
                {
                    txtSysPostBackAction.Text = "";
                }
                iGridRowCount--;
            }
            for (int i = 0; i < iGridRowCount; i++)
            {
                int index;
                if (RowDataParam == null)
                {
                    CreateElement(objRootElement, "option", ref objElement);
                    index = GetIndexFromColumnName("Ref", true);
                    if (sRootName != "CommonPortlets")
                    {
                        objElement.SetAttribute("ref", gvData.Items[i].Cells[index].Text);
                    } // if
                }
                else
                {
                    CreateElement(objRootElement, RowDataParam, ref objElement);
                }
                string sXmlData;

                // If the Last Record need not be included, then we can get the records from Grid
                // As all the entries is bound to grid
                if (!IncludeLastRecord)
                {
                    index = GetIndexFromColumnName("Data", true);
                    sXmlData = gvData.Items[i].Cells[index].Text;
                    sXmlData = sXmlData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'").Replace("&amp;amp;", "&amp;"); //Parijat : Mits 19022 Removing &amp; from transaction detail for invoice number
                }
                else
                {
                    // If the last record needs to be included, then we can not get the last record from Grid, as it is still not bound to grid.
                    // So we need to fetch the record from the control where the Javascript has put it.
                    // So using Request.Form in this Case.
                   
                    string sSelectedRowIndex = string.Empty;
                    // Grid Rows are rendered from index 2
                    if ((i + 2).ToString().Length == 1)
                        sSelectedRowIndex = "0" + (i + 2).ToString();
                    else
                        sSelectedRowIndex = (i + 2).ToString();
                    sXmlData = Request.Form[gvData.Items[i].UniqueID + "$|" + sSelectedRowIndex + "|Data"];
                    sXmlData = sXmlData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'").Replace("&amp;amp;", "&amp;"); //Parijat : Mits 19022 -- Removing &amp; from transaction detail for invoice number
                }
                XmlDocument objTemp = new XmlDocument();
                if (sXmlData != null && sXmlData != "" && sXmlData != "&nbsp;")
                    objTemp.LoadXml(sXmlData);
                if (IncludeLastRecord && objTemp != null)
                {
                    // Set the Primary Record for new Records

                    // Get the Primary Field Column in Xml
                    objPrimaryColumn = objTemp.SelectSingleNode("//" + Unique_Id);

                    if (Int32.TryParse(objPrimaryColumn.InnerText, out iCurrPrimaryKey))
                    {
                        if (RowDataParam == null)
                            objRootNode = objTemp.SelectSingleNode("/option");
                        else
                            objRootNode = objTemp.SelectSingleNode("/" + RowDataParam);

                        // npadhy Start MITS 20747 Not able to add more than 2 coverages in policy
                        if (iCurrPrimaryKey < 0)
                        {
                            iRunningNumber--;
                            objPrimaryColumn.InnerText = iRunningNumber.ToString();
                        }
                        // npadhy End MITS 20747 Not able to add more than 2 coverages in policy
                    }
                    else
                    {
                        iRunningNumber--;
                        objPrimaryColumn.InnerText = iRunningNumber.ToString();
                    }
                }
                //Added Rakhi to kill Grid Nodes
                TextBox txtGridKillNodes = (TextBox)this.Parent.FindControl(GridName + "_ComboRelations");
                if (txtGridKillNodes != null)
                {
                    if (txtGridKillNodes.Text != "")
                    {
                        string[] sValueTextPair = new string[50];
                        string sRemoveNodes = "";
                        sValueTextPair = txtGridKillNodes.Text.Split(";".ToCharArray()[0]);//Each ValueTextPair is separated from other with a ;
                        foreach (string sValueText in sValueTextPair)
                        {
                            string[] sValueTextSplit = new string[2];
                            sValueTextSplit = sValueText.Split("|".ToCharArray()[0]);//Each ValueText is | separated.
                            if (sValueTextSplit.Length == 2)
                            {
                                if (sRemoveNodes == "")
                                {
                                    sRemoveNodes = sValueTextSplit[1];
                                }
                                else
                                {
                                    sRemoveNodes += "|" + sValueTextSplit[1];
                                }
                            }

                        }
                        KillGridNodes(ref objTemp, sRemoveNodes);
                    }
                }
                //Added Rakhi to kill Grid Nodes
                if (objTemp != null && objTemp.ChildNodes[0] != null)
                    objElement.InnerXml = objTemp.ChildNodes[0].InnerXml;
            }
            return objXmlDocument;
        }
        /// <summary>
        /// This function creates the Grid Columns and the data to be bound with the Grid
        /// </summary>
        /// <param name="p_sobjXml"> The Xml file from which the grid columns and Data has to be created.</param>
        public void BindData(XmlDocument p_GridData)
        {
            string sTargetPath = Target;
            bool blnSuccess = false;
            int iLinkColumns = 0;
             // If the Target is not specified, then Grid can not bind with XML Document.
            if (sTargetPath != null)
            {
                if (sTargetPath != "" && !(sTargetPath.Contains("/")))
                    sTargetPath = Ref;

                // The Target and the XML Document are not in sync. Change the Target to match with the Xml Document structure.
                if (sTargetPath != "" && sTargetPath.StartsWith("/Instance/UI"))
                    sTargetPath = sTargetPath.Replace("/Instance/UI", "/Document/ParamList/Param[@name='SysFormVariables']");

                // When the page postbacks the grid columns which need not be shown are made hidden.
                // So, the hidden column can not be bound to the data. Changing the Column visibility back to true enables to bind the 
                // Grid hidden column with data.
                if (Page.IsPostBack)
                {
                    HideColumnAndRow(true);
                }

                DataSet dsGridDatasource = new DataSet();
                DataTable dtGridData = new DataTable();
                DataColumn dcGridColumn;

                // Get the Headers Collection. 
                XmlNode objGridHeaders = p_GridData.SelectSingleNode(sTargetPath + "/listhead");

                // Get the Footers Collection. 
                XmlNode objGridFooters = p_GridData.SelectSingleNode(sTargetPath + "/listfoot");

                //If listhead is not in the input XML, it could be some error happens in the 
                //backend and not able to create the required Xml. If it's a postback, we can
                //try to call GetXml to get the Xml from the hidden fields.
                if (objGridHeaders == null)
                {
                    //If it's not a postback and can not find listhead not, don't try to get xml from
                    //the hidden field and just skip data binding
                    if (!Page.IsPostBack)
                       return;

                    //If can not get xml from the hidden fields, just return
                    string sTargetXml = GetXml();
                    if (string.IsNullOrEmpty(sTargetXml))
                        return;

                    //If the target node is not found, try to create the input document
                    XmlNode oTargetNode = p_GridData.SelectSingleNode(sTargetPath);
                    if (oTargetNode == null)
                    {
                        p_GridData.LoadXml(sTargetXml);
                        sTargetPath = "/";
                        oTargetNode = p_GridData.DocumentElement;
                    }
                    else
                    {
                        XmlDocument oTargetDoc = new XmlDocument();
                        oTargetDoc.LoadXml(sTargetXml);
                        oTargetNode.InnerXml = oTargetDoc.DocumentElement.InnerXml;
                    }

                    objGridHeaders = p_GridData.SelectSingleNode(sTargetPath + "/listhead");
                    if (objGridHeaders == null)
                        return;
                }

                //Put the listhead in the hidden field
                string sListHead = objGridHeaders.OuterXml;
                sListHead = sListHead.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                hdListHead.Value = sListHead;

                // Get the Column names for which hidden columns need to be created
                String[] sHiddenColumns;
                ArrayList alHiddenColumns;
                if (HideNodes != null && HideNodes != "")
                {
                    sHiddenColumns = HideNodes.Split(new char[] { '|' });
                    alHiddenColumns = new ArrayList(sHiddenColumns);
                }
                else
                {
                    alHiddenColumns = new ArrayList();
                }

                //If the hidenodes does not contain Unique_Id, add to the hidenodes list
                if (!alHiddenColumns.Contains(Unique_Id))
                    alHiddenColumns.Add(Unique_Id);

                //A Dictionary to store all field name and the number of columns for that field in the Grid. It could handle the case 
                //where listhead and hidenodes are not the same order of XML child nodes.
                Dictionary<string, int> dicGridFieldList = new Dictionary<string, int>();
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    string sNodeName = objHeaderDetails.Name;
                    if (string.Compare(sNodeName, "rowhead", true) == 0)
                    {
                        if (objHeaderDetails.Attributes["colname"] != null)
                            sNodeName = objHeaderDetails.Attributes["colname"].Value;
                    }

                    if (alHiddenColumns.Contains(sNodeName))
                        continue;

                    if (!dicGridFieldList.ContainsKey(sNodeName))
                        dicGridFieldList.Add(sNodeName, 2);
                    else
                        continue;
                }
                foreach (string sHiddenNodeName in alHiddenColumns)
                {
                    if (string.IsNullOrEmpty(sHiddenNodeName))
                        continue;

                    if (!dicGridFieldList.ContainsKey(sHiddenNodeName))
                        dicGridFieldList.Add(sHiddenNodeName, 1);
                    else
                        continue;
                }

                // Create a Header for Ref. This column will be used while creating the xml while saving the Grid Data.
                alHiddenColumns.Add("Ref");

                // Create a Header for the Unique Id. This column is created to bind the radio button present in the grid. 
                // This column will contain the same data as the column specified in Unique Id Property,
                if (Unique_Id != "" && alHiddenColumns.Contains(Unique_Id))
                {
                    alHiddenColumns.Add("UniqueId");
                }

                // Create a Header for Data. This column will be used to store the Xml for New and Edit purposes
                alHiddenColumns.Add("Data");

                // Instance of the Grid Column
                GridBoundColumn bfGridColumn;

                // Instance of the grid column for link type of column
                ButtonField butGridLinkColumn;

                #region RadioButton
                // Handle Radio Button Column. If the RadioButtton column needs to be displayed, 
                // then perform various processing. Else hide the column.
                if (ShowRadioButton)
                {
                    dcGridColumn = new DataColumn();

                    // If the Radio button column Header needs to be displayed and header name is not specified,
                    // then default the header name to "Select One"
                    if (ShowRadioButtonHeader && RadioButtonHeader == null)
                    {
                        dcGridColumn.ColumnName = "Select One";
                        gvData.Columns[0].HeaderText = dcGridColumn.ColumnName;

                    }
                    // If the Radio button column Header needs to be displayed and header name is specified,
                    // then display the header name.
                    else if (ShowRadioButtonHeader && RadioButtonHeader != "")
                    {
                        dcGridColumn.ColumnName = RadioButtonHeader;
                        gvData.Columns[0].HeaderText = dcGridColumn.ColumnName;
                    }
                    // If the Radio Button Column Header need not be displayed, then Blank it.
                    else
                    {
                        dcGridColumn.ColumnName = "";
                        gvData.Columns[0].HeaderText = "";
                    }

                    // The width of this column would be 5% of the grid.
                    gvData.Columns[0].ItemStyle.Width = Unit.Percentage(5);

                    // Add column to the Datasource with which grid will be bound
                    dtGridData.Columns.Add(dcGridColumn);
                }
                else
                {
                    gvData.Columns[0].Visible = false;
                }
                #endregion RadioButton

                #region CheckBox
                // Handle Check Box Column. If the Check Box column needs to be displayed, 
                // then perform various processing. Else hide the column.
                if (ShowCheckBox)
                {
                    dcGridColumn = new DataColumn();

                    // If the Checkbox column Header needs to be displayed and header name is not specified,
                    // then default the header name to "Select Many"
                    if (ShowCheckBoxHeader && CheckBoxHeader == null)
                    {
                        dcGridColumn.ColumnName = "Select Many";
                        gvData.Columns[1].HeaderText = dcGridColumn.ColumnName;
                    }
                    // If the Checkbox column Header needs to be displayed and header name is specified,
                    // then display the header name.
                    else if (ShowCheckBoxHeader && CheckBoxHeader != "")
                    {
                        dcGridColumn.ColumnName = CheckBoxHeader;
                        gvData.Columns[1].HeaderText = dcGridColumn.ColumnName;
                    }
                    // If the Checkbox Column Header need not be displayed, then Blank it.
                    else
                    {
                        dcGridColumn.ColumnName = "";
                        gvData.Columns[1].HeaderText = "";
                    }

                    // The width of this column would be 5% of the grid.
                    gvData.Columns[1].ItemStyle.Width = Unit.Percentage(5);

                    // Add column to the Datasource with which grid will be bound
                    dtGridData.Columns.Add(dcGridColumn);
                }
                else
                {
                    gvData.Columns[1].Visible = false;
                }
                #endregion CheckBox


                if (TextColumn != null && TextColumn != "")
                {
                    dcGridColumn = new DataColumn();

                    // Add column to the Datasource with which grid will be bound
                    dtGridData.Columns.Add(dcGridColumn);
                }
                //else//neha
                //{
                //    gvData.Columns[2].Visible = false;
                //}

                #region Headers and Footers

                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    if (alHiddenColumns.Contains(objHeaderDetails.InnerText) || alHiddenColumns.Contains(objHeaderDetails.Name))
                        continue;

                    // Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();

                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.InnerText;
                    //smishra54: MITS 26222- Number sorting implementation on grid
                    if(objHeaderDetails.Attributes["type"] != null && string.Equals(objHeaderDetails.Attributes["type"].Value,"integer",StringComparison.InvariantCultureIgnoreCase))
                        dcGridColumn.DataType = System.Type.GetType("System.Int32");
                    //smishra54:end

                    // Add Column to Data Table
                    dtGridData.Columns.Add(dcGridColumn);
                    
                    // Logic below adds the columns to Grid. 
                    // We do not need to do this after every postback as columns are already added to Grid
                    //if (!Page.IsPostBack || (PopupGrid && !SessionGrid && bPageLoad)) //Added PopupGrid && bPageLoad for R7:Add Emp Data Elements
                    // If the Column is not Link Column, Create normal bound columns
                    if (!Page.IsPostBack)
                    {
                        if (String.IsNullOrEmpty(LinkColumn) || (LinkColumns != null && !LinkColumns.Contains(dcGridColumn.ColumnName)))
                        {
                            // Column to be associated with the Grid
                            bfGridColumn = new GridBoundColumn();
                            // Add the newly created bound field to the GridView. 


                            gvData.MasterTableView.Columns.Add(bfGridColumn);
                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;
                            bfGridColumn.ItemStyle.Wrap = true;
                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                            
                            // Get the FooterText if the Show Footer Property is true
                            if (ShowFooter)
                            {
                                if (objGridFooters != null)
                                    bfGridColumn.FooterText = objGridFooters[objHeaderDetails.Name].InnerText;
                            }

                            // Width of the Column. If the Column width is specified, then assign it.
                            // otherwise calculate the Column width as per the number of Columns required to be displayed
                            if (ColumnWidth != 0)
                            {
                                bfGridColumn.ItemStyle.Width = Unit.Percentage(ColumnWidth);
                            }
                            else
                            {
                                int iNumberOfColumns = objGridHeaders.ChildNodes.Count;
                                int iTotalWidth = 100;

                                if (ShowRadioButton)
                                    iTotalWidth = Convert.ToInt32(iTotalWidth - gvData.Columns[0].ItemStyle.Width.Value);

                                if (ShowCheckBox)
                                    iTotalWidth = Convert.ToInt32(iTotalWidth - gvData.Columns[0].ItemStyle.Width.Value);

                                int iWidthofColumn = iTotalWidth / iNumberOfColumns;

                                bfGridColumn.ItemStyle.Width = Unit.Percentage(iWidthofColumn);

                            }

                        }
                    }
                        // Column for Value Purposes. These columns store the value corresponding to the columns displayed in grid.
                        // Like code if for code fields etc.
                        dcGridColumn = new DataColumn();

                        // Get the Xml Node for Header in data section. 
                        XmlNode objNode = null;
                        if (RowDataParam == null)
                            objNode = p_GridData.SelectSingleNode(sTargetPath + "/option/" + objHeaderDetails.Name);
                        else
                            objNode = p_GridData.SelectSingleNode(sTargetPath + "/" + RowDataParam + "/" + objHeaderDetails.Name);

                        // npadhy We do not need to check whether the Attribute is present or not
                        dcGridColumn.ColumnName = objHeaderDetails.InnerText + "_Value";

                        // Keep the Header Node Name in the Caption, this will be used while generating the XML for saving the records
                        dcGridColumn.Caption = objHeaderDetails.Name;

                        // Add Column to Datasource that will be bound with grid.
                        dtGridData.Columns.Add(dcGridColumn);
                        // Logic below adds the value columns to Grid. 
                        // We do not need to do this after every postback as columns are already added to Grid
                        //if (!Page.IsPostBack || (PopupGrid && !SessionGrid && bPageLoad)) //Added PopupGrid && bPageLoad for R7:Add Emp Data Elements
                        if (!Page.IsPostBack)
                        {
                            // Column to be associated with the Grid
                            bfGridColumn = new GridBoundColumn();
                            // Add the newly created bound field to the GridView. 
                            gvData.MasterTableView.Columns.Add(bfGridColumn);
                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;
                            bfGridColumn.ItemStyle.Wrap = true;
                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Keep the Header Node Name in the Grid Footer, this will be used while generating the XML for saving the records
                            bfGridColumn.FooterText = dcGridColumn.Caption;

                        }

                    }
                

                #endregion Headers

                #region Hidden Columns
                foreach (string sHiddenNodeName in alHiddenColumns)
                {
                    if (sHiddenNodeName != "")
                    {
                        dcGridColumn = new DataColumn();

                        bfGridColumn = new GridBoundColumn();
                        // Add the newly created bound field to the GridView. 
                        gvData.MasterTableView.Columns.Add(bfGridColumn);
                        dcGridColumn.ColumnName = sHiddenNodeName;

                        dcGridColumn.Caption = "Hidden";
                        //if (!Page.IsPostBack)
                        //{
                            dtGridData.Columns.Add(dcGridColumn);

                            //dt.Columns[dt.Columns.Count - 1].ColumnMapping = MappingType.Hidden;
                            //if (!Page.IsPostBack || (PopupGrid && !SessionGrid && bPageLoad)) //Added PopupGrid && bPageLoad for R7:Add Emp Data Elements

                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            //  Initialize the HeaderText field value. 
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName + "_Hidden";

                          
                        
                    }
                }

                #endregion Hidden Columns
                #region Data
                Control cPostBackControl = DatabindingHelper.GetPostBackControl(this.Page);
                TextBox txtMode = (TextBox)this.Parent.FindControl(GridName + "_Action");
                string sMode = string.Empty;
                if (txtMode != null)
                {
                    sMode = txtMode.Text.ToLower();
                }

                // If the Last Record is Included, then the XML which is generated to send to CWS includes all the changed record, so we can use the 
                // Same xml to bind the grid.
                if ((!IsPostBack || cPostBackControl != null || string.IsNullOrEmpty(sMode) || IncludeLastRecord) &&
                    ((sMode.ToLower() != "delete") || string.IsNullOrEmpty(sMode)))
                {
                    XmlNodeList objRowNodesList = null;
                    if (RowDataParam == null)
                        objRowNodesList = p_GridData.SelectNodes(sTargetPath + "/option");
                    else
                        objRowNodesList = p_GridData.SelectNodes(sTargetPath + "/" + RowDataParam);

                    DataRow dr = null;

                    int iRowCount = 0;
                    foreach (XmlNode objRowNodes in objRowNodesList)
                    {
                        string sRefAttribute = "";
                        if (objRowNodes.Attributes["ref"] != null)
                        {
                            sRefAttribute = objRowNodes.Attributes["ref"].Value;
                        }
                        dr = dtGridData.NewRow();

                        // npadhy This logic does not hold good if there are more than one Template Column
                        //int iColumnIndex = gvData.Columns.Count - dtGridData.Columns.Count -1;
                        int iColumnIndex = 0;

                        iColumnIndex = GetCellPosition(iColumnIndex, true);

                        //The sequence should be the same as the dicGridField2Column.
                        //foreach (XmlElement objElem in objNodes)
                        foreach (string sNodeName in dicGridFieldList.Keys)
                        {
                            XmlElement objElem = objRowNodes[sNodeName];
                            if (objElem != null )
                            {
                                //smishra54 : Implementation of numeric sorting on grid
                                if (string.Equals(dr.Table.Columns[iColumnIndex].DataType.Name, System.Type.GetType("System.Int32").Name))
                                    dr[iColumnIndex++] = Common.Conversion.CastToType<Int32>( objElem.InnerText, out blnSuccess);
                                else
                                    dr[iColumnIndex++] = objElem.InnerText;
                                //smishra54:end
                                //dr[iColumnIndex++] = objElem.InnerText;
                                if (dicGridFieldList[sNodeName] == 2)
                                    if (objElem.Attributes["codeid"] != null )
                                        dr[iColumnIndex++] = objElem.Attributes["codeid"].Value;
                                    else
                                        dr[iColumnIndex++] = objElem.InnerText;

                            }
                            else
                            {
                                //can not find the node in the XML. It means the specified node name is wrong or XML is wrong
                                dr[iColumnIndex++] = string.Empty;
                                if (dicGridFieldList[sNodeName] == 2)
                                    dr[iColumnIndex++] = string.Empty;
                            }
                        }

                        dr[iColumnIndex++] = sRefAttribute;
                        dtGridData.Rows.Add(dr);

                        // Store the Unique_Id if Unique Id is not null
                        if (Unique_Id != "")
                            dr[iColumnIndex++] = dtGridData.Rows[iRowCount][Unique_Id].ToString();

                        dr[iColumnIndex++] = objRowNodes.OuterXml;
                        iRowCount++;
                    }

                }
                if ((cPostBackControl == null && IsPostBack && !string.IsNullOrEmpty(sMode) && !IncludeLastRecord)
                    || (sMode == "delete"))
                {
                    int row = 2;
                    int iNewRecords = 0;
                    string sDataFormat = "";
                    bool bRowSkip = false;
                    int iCounter = 0;
                    string sFormat = string.Empty;
                    foreach (DataListItem gvr in gvData.Items)
                    {
                        
                            //if (gvData.Rows.Count == row - 1 && (txtMode.Text.ToLower() == "edit" || txtMode.Text.ToLower() == "delete"))
                            if (gvData.Items.Count == row - 1 && (sMode != "add" && sMode != "clone"))
                                continue;

                            DataRow dr;
                            string index;
                            if (row.ToString().Length == 1)
                                index = "0" + row.ToString();
                            else
                                index = row.ToString();
                            string sXml = Request.Form[gvr.UniqueID + "$|" + index.ToString() + "|Data"];
                            sXml = sXml.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                            XmlDocument objXml = new XmlDocument();
                            objXml.LoadXml(sXml);
                            XmlNodeList objRowNodesList;
                            if (RowDataParam == null)
                                objRowNodesList = objXml.SelectNodes("/option");
                            else
                                objRowNodesList = objXml.SelectNodes("/" + RowDataParam);
                            XmlNode objRowNodes = objRowNodesList[0];

                            // Get the Selected Row Id if Mode is Deletion. This will be called only in Specific Case
                            // When the Page is not FDM and in case of NON FDM the New records are not directly saved to DB

                            if (txtMode.Text.ToLower() == "delete")
                            {
                                TextBox txtDeletedValues = (TextBox)this.Parent.FindControl("hdnDeletedValues");
                                // if (objNodes.SelectSingleNode("//" + Unique_Id).InnerText == txtDeletedValues.Text)
                                //if (objNodes.SelectSingleNode("//" + Unique_Id).InnerText.Contains(txtDeletedValues.Text))
                                if (txtDeletedValues.Text.Contains(objRowNodes.SelectSingleNode("//" + Unique_Id).InnerText))
                                {
                                    sFormat = objRowNodes.OuterXml;
                                    row++;
                                    bRowSkip = true;
                                    iCounter++;
                                    continue;
                                }
                            }

                            string sRefAttribute = "";
                            if (objRowNodes.Attributes["ref"] != null)
                            {
                                sRefAttribute = objRowNodes.Attributes["ref"].Value;
                            }

                            dr = dtGridData.NewRow();

                            // npadhy This logic is not valid if there are more than one Template Columns
                            //int iColumnIndex = gvData.Columns.Count - dtGridData.Columns.Count -1;
                            int iColumnIndex = 0;
                            iColumnIndex = GetCellPosition(iColumnIndex, true);

                            //The sequence should be the same as the dicGridField2Column.
                            //foreach (XmlElement objElem in objNodes)
                            foreach (string sNodeName in dicGridFieldList.Keys)
                            {
                                XmlElement objElem = objRowNodes[sNodeName];
                                if (objElem != null)
                                {
                                    //smishra54 : Implementation of numeric sorting on grid
                                    if (string.Equals(dr.Table.Columns[iColumnIndex].DataType.Name, System.Type.GetType("System.Int32").Name))
                                        dr[iColumnIndex++] = Common.Conversion.CastToType<Int32>(objElem.InnerText, out blnSuccess);
                                    else
                                        dr[iColumnIndex++] = objElem.InnerText;
                                    //smishra54:end
                                    //dr[iColumnIndex++] = objElem.InnerText;
                                    if (dicGridFieldList[sNodeName] == 2)
                                        if (objElem.Attributes["codeid"] != null)
                                            dr[iColumnIndex++] = objElem.Attributes["codeid"].Value;
                                        else
                                            dr[iColumnIndex++] = objElem.InnerText;
                                }
                                else
                                {
                                    //can not find the node in the XML. It means the specified node name is wrong or XML is wrong
                                    dr[iColumnIndex++] = string.Empty;
                                    if (dicGridFieldList[sNodeName] == 2)
                                        dr[iColumnIndex++] = string.Empty;
                                }
                            }

                            if (objRowNodes.Attributes["type"] == null)
                            {
                                dr[iColumnIndex++] = sRefAttribute;
                                dtGridData.Rows.Add(dr);
                                if ((ShowCheckBox || ShowRadioButton) && Unique_Id != "")
                                {
                                    if (bRowSkip)
                                        dr[iColumnIndex++] = dtGridData.Rows[row - 2 - iCounter][Unique_Id].ToString();
                                    else
                                        dr[iColumnIndex++] = dtGridData.Rows[row - 2][Unique_Id].ToString();
                                }
                                dr[iColumnIndex++] = objRowNodes.OuterXml;
                            }
                            else if (objRowNodes.Attributes["type"].Value == "new")
                            {
                                dr[iColumnIndex++] = sRefAttribute;
                                dtGridData.Rows.Add(dr);
                                if ((ShowCheckBox || ShowRadioButton) && Unique_Id != "")
                                {
                                    iNewRecords = GetMinimumUniqueId(dtGridData.Rows.Count);
                                    dr[iColumnIndex++] = iNewRecords;
                                }

                                // Update the objNodes with the Unique Id
                                XmlNode objPrimaryTemp = objRowNodes.SelectSingleNode("//" + Unique_Id);
                                objPrimaryTemp.InnerText = iNewRecords.ToString();

                                dr[iColumnIndex++] = objRowNodes.OuterXml;
                            }
                            sDataFormat = objRowNodes.OuterXml;
                       
                        row++;
                    }

                    DataRow drNewRecord = dtGridData.NewRow();
                    string sRef = string.Empty;//Asif
                    //Asif Start
                    if (txtMode != null && txtMode.Text.ToLower() == "delete")
                    {
                        sRef = "";

                    }
                    else
                    {
                        sRef = dtGridData.Rows[dtGridData.Rows.Count - 1]["Ref"].ToString();
                    }
                    //Asif End
                    if (sRef != "")
                    {
                        int iIndex1 = sRef.IndexOf('[');
                        int iIndex2 = sRef.IndexOf(']');
                        if (iIndex1 != -1 && iIndex2 != -1)
                        {
                            string sRefNo = sRef.Substring(iIndex1 + 1, iIndex2 - iIndex1 - 1);
                            sRef = sRef.Substring(0, sRef.IndexOf('[') + 1) + (Int32.Parse(sRefNo) + 1).ToString() + "]";
                        }
                    }
                    drNewRecord["Ref"] = sRef;

                    XmlDocument objXml1 = new XmlDocument();
                    if (txtMode != null && txtMode.Text.ToLower() == "delete")
                    {
                        objXml1.LoadXml(sFormat);
                    }
                    else
                    {
                        objXml1.LoadXml(sDataFormat);
                    }

                    XmlNodeList objNodeList1;

                    if (RowDataParam == null)
                        objNodeList1 = objXml1.SelectNodes("/option");
                    else
                        objNodeList1 = objXml1.SelectNodes("/" + RowDataParam);

                    XmlNode objNodes1 = objNodeList1[0];
                    if (sRef != "")
                    {
                        objNodes1.Attributes["ref"].Value = sRef;
                    }

                    // The type of the newly inserted record should be new as this will help in correcly determine the unique Id while
                    // inserting the next record
                    if (objNodes1.Attributes["type"] != null)
                        objNodes1.Attributes["type"].Value = "new";
                    else
                        ((XmlElement)objNodes1).SetAttribute("type", "new");

                    #region
                    ////Changed for R7:Add Emp Data Element
                    ////drNewRecord["Data"] = objNodes1.OuterXml;
                    //XmlNode objEmptyNode = CreateNodeTemplate(objNodes1);
                    //drNewRecord["Data"] = objEmptyNode.OuterXml;
                    ////Changed for R7:Add Emp Data Elements
                    #endregion
                    drNewRecord["Data"] = objNodes1.OuterXml;
                    dtGridData.Rows.Add(drNewRecord);

                    // Add Data For Data fieldss

                }


                //if (Type.ToLower() == "grid")
                //{
                //    // Adding a Dummy record for grid, so that if there is no data, then also the Headers are displayed.
                //    DataRow drNewRecord = dtGridData.NewRow();
                //    dtGridData.Rows.Add(drNewRecord);
                //}

                //if (Type.ToLower() == "gridandbuttons" && RowDataParam != null && !OverrideBlankRowInsert)
                //{
                //    // Adding a Dummy record for grid, so that if there is no data, then also the Headers are displayed.
                //    DataRow drNewRecord = dtGridData.NewRow();
                //    dtGridData.Rows.Add(drNewRecord);
                //}
                // Ijha: MITS 26535: Adjuster's Divided in groups with a broad Line
                for (int i = 0; i < dtGridData.Rows.Count; i++)
                {
                    DataRow oRow = dtGridData.Rows[i];
                    if (string.Equals(oRow["RowId"],"0"))
                    {
                        if (!dtGridData.Columns.Contains("GroupIdentifier"))
                        {
                            dtGridData.Columns.Add("GroupIdentifier");
                        }
                        dtGridData.Rows[i - 1]["GroupIdentifier"] = 1;
                        dtGridData.Rows.RemoveAt(i);
                    }
                }
                // Ijha: End
                dsGridDatasource.Tables.Add(dtGridData);

                ViewState["UserControlDataSet"] = dsGridDatasource;
                gvData.DataSource   = dsGridDatasource.Tables[0];
                gvData.DataBind();
                HideColumnAndRow(true);
                gvData.Rebind();
                //Resetting Text Mode Asif
                if (txtMode != null)
                {
                    txtMode.Text = string.Empty;
                }
                #endregion Data
                //spahariya:Added to hide particular button based on grid xml GridHideButtons attribute - start 10/12/12 MITS 28867
                if (HideButtons != null && HideButtons.Length > 0)
                {
                //MGaba2:Moved from page_load as HideButtons was coming null
                // The UserControl is called one more time then required.
                // Could not figure out the reason. This hack hides the button when it is not required
                if (GridName == null)
                {
                    HideButtons = "New|Edit|Delete|Clone";
                }

                // Check if the Grid Type is "Grid" and not "GridandButtons" then Hide the Buttons
                if (Type == "Grid")
                    HideButtons = "New|Edit|Delete|Clone";
                }
                else
                {
                    if (p_GridData.SelectSingleNode(sTargetPath) != null && p_GridData.SelectSingleNode(sTargetPath).Attributes != null)
                    {
                        XmlAttribute attrHidBtn = p_GridData.SelectSingleNode(sTargetPath).Attributes["GridHideButtons"];

                        if (attrHidBtn != null)
                            HideButtons = attrHidBtn.Value;
                    }
                }
                //spahariya - end
                HideButton(HideButtons);  // Added by csingh7
                //HideColumnAndRow(false);
                Page.ClientScript.RegisterStartupScript(this.GetType(), GridName + "SelectRow", "<script>SelectAfterPostback('" + GridName + "');</script>");
            }
        }
        // <summary>
        /// Get xml for headlist and all the rows, includes the blank row
        /// </summary>
        /// <returns></returns>
        public string GetXml()
        {
            if (gvData.Items.Count == 0)//neha
                return string.Empty;

            StringBuilder sbXML = new StringBuilder();

            string sRootName = GridName.Replace("Grid", "");
            sbXML.Append(string.Format("<{0}>", sRootName));

            string sListHead = hdListHead.Value;
            sListHead = sListHead.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
            sbXML.Append(sListHead);

            int index = 2;
            string sIndex = string.Empty;
            string sHiddenFieldId = string.Empty;
            string sXmlData = string.Empty;
            bool bItemFound = false;
            for (int i = 0; i < gvData.Items.Count; i++)
            {
                if (index < 10)
                {
                    sIndex = "0" + index.ToString();
                }
                else
                {
                    sIndex = index.ToString();
                }
                sHiddenFieldId = string.Format("{0}$gvData$ctl{1}$|{2}|Data", GridName, sIndex, sIndex);
                sXmlData = Request.Form[sHiddenFieldId];
                if (!string.IsNullOrEmpty(sXmlData))
                {
                    sXmlData = sXmlData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'").Replace("&amp;amp;", "&amp;"); //Parijat : Mits 19022 Removing &amp; from transaction detail for invoice number
                    sbXML.Append(sXmlData);
                    bItemFound = true;
                }
                index++;
            }

            //For mode "add", add the blank row to the xml; for mode "delete", remove the deleted row 
            string sMode = string.Empty;
            TextBox txtMode = (TextBox)this.Parent.FindControl(GridName + "_Action");
            if (txtMode != null)
            {
                sMode = txtMode.Text;
            }
            //if (sMode == "add" || sMode == "clone" || !bItemFound)
            //{
            //    index = GetIndexFromColumnName("Data", true);
            //    sXmlData = gvData.Rows[gvData.Rows.Count - 1].Cells[index].Text;
            //    sXmlData = sXmlData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'").Replace("&amp;amp;", "&amp;"); //Parijat : Mits 19022 Removing &amp; from transaction detail for invoice number
            //    sbXML.Append(sXmlData);
            //}
            sbXML.Append(string.Format("</{0}>", sRootName));
            string sXML = sbXML.ToString();

            if (sMode == "delete")
            {
                string sControlName = GridName.Substring(0, GridName.Length - 4) + "SelectedId";
                TextBox txtSelectedId = (TextBox)this.Parent.FindControl(sControlName);
                string sSelectedId = string.Empty;
                if (txtSelectedId != null)
                {
                    sSelectedId = txtSelectedId.Text;
                }
                if (!string.IsNullOrEmpty(sSelectedId))
                {
                    XElement oGridXml = XElement.Parse(sXML);
                    string sPath = string.Format("//option[{0}='{1}']", Unique_Id, sSelectedId);
                    XElement oDeletedElement = oGridXml.XPathSelectElement(sPath);
                    if (oDeletedElement != null)
                    {
                        oDeletedElement.Remove();
                    }
                    sXML = oGridXml.ToString();
                }
            }

            return sXML;
        }
        /// <summary>
        /// It Returns the required Cell position in the Grid. If Template Columns are hidden, though the Column count of grid does not 
        /// consider Template Columns, but GridViewRowEventArgs consider them also. 
        /// </summary>
        /// <param name="iColumnIndex"> The Column Index where a Control has to be added</param>
        /// <returns>Absolute Position for a Cell</returns>
        private int GetCellPosition(int iColumnIndex, bool bMode)
        {
            int iCellPosition = iColumnIndex;

            if (bMode)
            {
                if (ShowRadioButton)
                    iCellPosition++;

                if (ShowCheckBox)
                    iCellPosition++;

                if (TextColumn != null && TextColumn != "")
                    iCellPosition++;
            }
            else
            {
                if (!ShowRadioButton)
                    iCellPosition++;

                if (!ShowCheckBox)
                    iCellPosition++;

                if (TextColumn == null || TextColumn == "")
                    iCellPosition++;
            }

            return iCellPosition;
        }
        private int GetIndexFromColumnName(string p_sColumnName, bool bHidden)
        {
            try
            {
                string sColumnName = p_sColumnName + (bHidden ? "_Hidden" : "");
                for (int i = gvData.Columns.Count - 1; i > 0; i--)
                {
                    if (gvData.Columns[i].HeaderText == sColumnName)
                        return i;
                }
                return 0;
            }
            catch
            {
                return 0;
            }


        }

        private void HideButton(string p_sHideNodes)
        {
            if (p_sHideNodes != null)
            {
                if (p_sHideNodes.Contains("New"))
                    New.Visible = false;
                else
                    New.Visible = true;
                if (p_sHideNodes.Contains("Edit"))
                    Edit.Visible = false;
                else
                    Edit.Visible = true;
                if (p_sHideNodes.Contains("Delete"))
                    Delete.Visible = false;
                else
                    Delete.Visible = true;
                if (p_sHideNodes.Contains("Clone") || (m_bShowCloneButton == false))
                    Clone.Visible = false;
                else
                    Clone.Visible = true;
            }
        }

        private int GetMinimumUniqueId(int iProcessingRow)
        {
            int iMinUniqueId = 0;
            int iCurrentValue;
            // Get the Collumn index of the UniqueId_Hidden column.
            int iUniqueIdColumnIndex = GetIndexFromColumnName("UniqueId", true);
            for (int i = 0; i < gvData.Items.Count; i++)
            {
                if (Int32.TryParse(gvData.Items[i].Cells[iUniqueIdColumnIndex].Text, out iCurrentValue))
                {
                    if (iCurrentValue < 0)
                    {

                        iMinUniqueId = iCurrentValue;
                        // The Index of the loop runs from 0 but the Grid Row Count will start from 1
                        if (i == (iProcessingRow - 1))
                            return iMinUniqueId;
                    }
                }
                else
                {
                    iMinUniqueId--;
                }
            }
            return iMinUniqueId;
        }


        #region Private Functions
        #region Common XML functions
        /// <summary>
        /// Initialize the XML document
        /// </summary>
        /// <param name="objXmlDocument">XML document</param>
        /// <param name="p_objRootNode">Root Node</param>
        /// <param name="p_objMessagesNode">Messages Node</param>
        /// <param name="p_sRootNodeName">Root Node Name</param>
        internal void StartDocument(ref XmlDocument objXmlDocument, ref XmlElement p_objRootNode, string p_sRootNodeName)
        {
            try
            {
                objXmlDocument = new XmlDocument();
                p_objRootNode = objXmlDocument.CreateElement(p_sRootNodeName);
                objXmlDocument.AppendChild(p_objRootNode);
            }

            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Node Name</param>		
        internal void CreateElement(XmlElement p_objParentNode, string p_sNodeName)
        {
            XmlElement objChildNode = null;
            try
            {
                objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                objChildNode = null;
            }

        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Child Node Name</param>
        /// <param name="p_objChildNode">Child Node</param>
        internal void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }

        }

        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>		
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            try
            {
                XmlElement objChildNode = null;

                if (p_sText == "&nbsp;")
                    p_sText = "";

                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>
        /// <param name="p_objChildNode">Child Node</param>
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                if (p_sText == "&nbsp;")
                    p_sText = "";
                p_objChildNode.InnerText = p_sText;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
        }

        public void ResetSysExData(XmlDocument SysEx, string sNode, string sValue)
        {
            try { SysEx.DocumentElement.RemoveChild(SysEx.SelectSingleNode("/SysExData/" + sNode)); }
            catch { };
            CreateSysExData(SysEx, sNode, sValue);
        }

        public void CreateSysExData(XmlDocument SysEx, string sNode, string sValue)
        {
            XmlNode objNew;
            //Determine if the specific Node already exists in the SysExData of the Instance Document
            if (SysEx.SelectSingleNode("/SysExData/" + sNode) == null)
            {
                //Create a new XML Element for the specified Xml Node
                objNew = SysEx.CreateElement(sNode);

                //Set the element inner text to an empty string
                objNew.AppendChild(SysEx.CreateCDataSection(sValue));
                //objNew.InnerText = FormDataAdaptor.CData();

                //Add the new XML Element to the existing Instance Data
                SysEx.DocumentElement.AppendChild(objNew);
            }
        }

        #endregion

      

      
        private void KillGridNodes(ref XmlDocument objTemp, string sRemoveNodes)
        {
            string[] sKillNodes = new String[50];
            XmlNode objKillNode = null;
            try
            {
                sKillNodes = sRemoveNodes.Split('|');
                foreach (string sCurrentNode in sKillNodes)
                {

                    objKillNode = objTemp.ChildNodes[0].SelectSingleNode(sCurrentNode);
                    if (objKillNode != null)
                    {
                        objTemp.ChildNodes[0].RemoveChild(objKillNode);
                    }

                }

            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
        }


        private void HideColumnAndRow(bool p_bShow)
        {
            string sCssClass = string.Empty;
            bool blnDisplay = true;
            if (p_bShow)
            {
                sCssClass = "visiblerowcol";

            }
            else
            {
                sCssClass = "hiderowcol";
                blnDisplay = false;
            }
            for (int i = 0; i < gvData.Columns.Count; i++)
            {
                if (gvData.Columns[i].HeaderText.Contains("Value") || gvData.Columns[i].HeaderText.Contains("Hidden"))
                {
                    gvData.Columns[i].HeaderStyle.CssClass = sCssClass;
                    gvData.Columns[i].ItemStyle.CssClass = sCssClass;
                    gvData.Columns[i].Display = blnDisplay;
                    if (ShowFooter)
                        gvData.Columns[i].FooterStyle.CssClass = sCssClass;
                }
            }

            if (TextColumn != null && TextColumn != "")
            {
                gvData.Columns[2].HeaderStyle.CssClass = sCssClass;
                gvData.Columns[2].ItemStyle.CssClass = sCssClass;
                gvData.Columns[2].Display = blnDisplay;
            }
        }
              
        #endregion Private Functions
       
    }
}