﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserControlDataGrid.ascx.cs" Inherits="Riskmaster.UI.Shared.Controls.UserControlDataGrid" %>
<%@ Register Src="~/UI/Shared/Controls/DataPaging.ascx" TagName="DataPaging" TagPrefix="dp" %>


<table id="UserControl" width="100%">
<tr>
    <td>
        <asp:Label ID="lblGridTitle" runat="server" Font-Bold="True" 
            Font-Names="Tahoma" Font-Size="Small"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblCurrType" runat="server" Font-Bold="True" 
            Font-Names="Tahoma" Font-Size="Small"></asp:Label>
    </td>
</tr>
<tr valign="top">
    <td width="93%">
        <div id="GridDiv" runat="server" style="overflow:auto" >
            <asp:GridView Width="98%" ID="gvData" runat="server" 
                AutoGenerateColumns="False" onrowdatabound="gvData_RowDataBound" 
                Font-Names="Tahoma" Font-Size="Smaller" ondatabound="gvData_DataBound" 
                GridLines="Both" CssClass="singleborder" 
                onpageindexchanging="gvData_PageIndexChanging">
                <Columns>
                <asp:TemplateField>
                <ItemTemplate>
                    <input  id="MyRadioButton" name="<%#GridName %>" type="radio" onclick="<%#OnClick%>"
                        value='<%# Unique_Id==""? "" : Eval("UniqueId") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                <ItemTemplate>
                    <input  id="MyCheckBox" name="<%#GridName %>" type="checkbox"  onclick="<%#OnClick%>"
                        value='<%# Unique_Id==""? "" : Eval("UniqueId") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TextColumn">
                <ItemTemplate>
                    <asp:HiddenField  id="hfGrid" runat="server" Value='<%# TextColumn != null && TextColumn != "" ? Eval(TextColumn):"" %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                </Columns>
                <HeaderStyle HorizontalAlign="Left" />
                
                    
            </asp:GridView>
            <asp:HiddenField ID="hdListHead" runat="server" />
        </div>
    </td>
    <td width="2%" >
        <asp:TextBox id="OtherParams" style="display:none" runat ="server" Text=""/>
        <asp:ImageButton ID="New" runat="server" ToolTip="New" 
            ImageUrl="~/Images/tb_new_mo.png" ImageAlign="Middle"/>
        <asp:ImageButton ID="Edit" runat="server" ToolTip="Edit" 
            ImageUrl="~/Images/tb_edit_mo.png" ImageAlign="Middle"/>
        <asp:ImageButton ID="Clone" runat="server" ToolTip="Clone" 
            ImageUrl="~/Images/tb_clone_mo.png" ImageAlign="Middle" Visible="false"/>
        <asp:ImageButton ID="Delete" runat="server" ToolTip="Delete" 
            ImageUrl="~/Images/tb_delete_mo.png" ImageAlign="Middle"/>
        </td>
</tr>
<!-- rsushilaggar - Added pagination on the Policy setup grid MITS 22371-->
<tr>
    <td>
    <%if (AllowPaging)
     {%>
        <div id="divPaging" runat="server">
           <dp:DataPaging id="dpPaging" runat="server"></dp:DataPaging>
        </div>
    <%}%>
    </td>
</tr>
<!-- End rsushilaggar-->
<tr><td></td></tr>
</table>


