﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Collections.Generic;


public partial class MultiSelectListBox : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
    }//event: Page_Load



    #region Public Properties
    /// <summary>
    /// Gets and sets the Selected Items from the List Box
    /// </summary>
    public ListItemCollection Selected
    {
        get
        {
            return lbSelected.Items;
        }

        set
        {
            lbSelected.DataSource = value;
            lbSelected.DataTextField = "text";
            lbSelected.DataValueField = "value";
            lbSelected.DataBind();
        }
    }//property: Selected


    /// <summary>
    /// Gets and sets the Unselected Items from the List Box
    /// </summary>
    public ListItemCollection Unselected
    {
        get
        {
            return lbUnselected.Items;
        }

        set
        {
            lbUnselected.DataSource = value;
            lbUnselected.DataTextField = "text";
            lbUnselected.DataValueField = "value";
            lbUnselected.DataBind();
        }
    } 
    #endregion


    #region Public Methods
    /// <summary>
    /// Sorts both list boxes, can be modified later to sort in a certain 
    /// order or just one of the list boxes
    /// </summary>
    public void Sort()
    {
        this.Unselected = SortLIC(this.lbUnselected.Items);
        this.Selected = SortLIC(this.lbSelected.Items);
    }//method: Sort() 
    #endregion

    #region Private Methods
    /// <summary>
    /// Sorts the listbox items
    /// </summary>
    /// <param name="Items"></param>
    /// <returns></returns>
    private ListItemCollection SortLIC(ListItemCollection Items)
    {
        ArrayList Values = new ArrayList();

        foreach (ListItem obj in Items)
        {
            Values.Add(obj.Value);
        }

        Values.Sort();
        ListItemCollection RetItems = new ListItemCollection();

        foreach (string contact in Values)
        {
            RetItems.Add(new ListItem(Items.FindByValue(contact).ToString(), contact));
        }

        return RetItems;
    } 
    #endregion

    #region Manages actions on the List Box buttons
    private void MoveUp(ListBox lstBox)
    {
        int iIndex, iCount, iOffset, iInsertAt, iIndexSelectedMarker = -1;
        string lItemData, lItemval;
        try
        {

            // Get the count of items in the list control
            iCount = lstBox.Items.Count;

            // Set the base loop index and the increment/decrement value based on the direction the item are being moved (up or down).
            iIndex = 0;
            iOffset = -1;

            // Loop through all of the items in the list.
            while (iIndex < iCount)
            {
                // Check if this item is selected.
                if (lstBox.SelectedIndex > 0)
                {
                    // Get the item data for this item
                    lItemval = lstBox.SelectedItem.Value.ToString();
                    lItemData = lstBox.SelectedItem.Text.ToString();
                    iIndexSelectedMarker = lstBox.SelectedIndex;

                    // Don't move selected items past other selected items
                    if (-1 != iIndexSelectedMarker)
                    {
                        for (int iIndex2 = 0; iIndex2 < iCount; ++iIndex2)
                        {
                            // Find the index of this item in enabled list
                            if (lItemval == lstBox.Items[iIndex2].Value.ToString())
                            {
                                // Remove the item from its current position
                                lstBox.Items.RemoveAt(iIndex2);

                                // Reinsert the item in the array one space higher than its previous position
                                iInsertAt = (iIndex2 + iOffset) < 0 ? 0 : iIndex2 + iOffset;
                                ListItem li = new ListItem(lItemData, lItemval);
                                lstBox.Items.Insert(iInsertAt, li);
                                break;
                            }
                        }
                    }
                }
                // If this item wasn't selected save the index so we can check it later so we don't move past the any selected items.
                else if (-1 == iIndexSelectedMarker)
                {
                    iIndexSelectedMarker = iIndex;
                    break;

                }

                iIndex = iIndex + 1;
            }

            if (iIndexSelectedMarker == 0)
                lstBox.SelectedIndex = iIndexSelectedMarker;
            else
                lstBox.SelectedIndex = iIndexSelectedMarker - 1;
        }
        catch (Exception expException)
        {
            Response.Write(expException.Message);
        }

    }

    private void MoveDown(ListBox lstBox)
    {
        try
        {
            int iIndex, iCount, iOffset, iInsertAt, iIndexSelectedMarker = -1;
            string lItemData;
            string lItemval;


            // Get the count of items in the list control
            iCount = lstBox.Items.Count;

            // Set the base loop index and the increment/decrement value based on the direction the item are being moved (up or down).
            iIndex = iCount - 1;
            iOffset = 1;

            // Loop through all of the items in the list.
            while (iIndex >= 0)
            {
                // Check if this item is selected.
                if (lstBox.SelectedIndex >= 0)
                {
                    // Get the item data for this item
                    lItemData = lstBox.SelectedItem.Text.ToString();
                    lItemval = lstBox.SelectedItem.Value.ToString();
                    iIndexSelectedMarker = lstBox.SelectedIndex;

                    // Don't move selected items past other selected items
                    if (-1 != iIndexSelectedMarker)
                    {
                        for (int iIndex2 = 0; iIndex2 < iCount - 1; ++iIndex2)
                        {
                            // Find the index of this item in enabled list
                            if (lItemval == lstBox.Items[iIndex2].Value.ToString())
                            {
                                // Remove the item from its current position

                                lstBox.Items.RemoveAt(iIndex2);

                                // Reinsert the item in the array one space higher than its previous position
                                iInsertAt = (iIndex2 + iOffset) < 0 ? 0 : iIndex2 + iOffset;
                                ListItem li = new ListItem(lItemData, lItemval);
                                lstBox.Items.Insert(iInsertAt, li);
                                break;

                            }
                        }
                    }
                }
                iIndex = iIndex - 1;
            }
            if (iIndexSelectedMarker == lstBox.Items.Count - 1)
                lstBox.SelectedIndex = iIndexSelectedMarker;
            else
                lstBox.SelectedIndex = iIndexSelectedMarker + 1;
        }
        catch (Exception expException)
        {
            Response.Write(expException.Message);
        }
    }

    private void AddRemoveAll(ListBox aSource, ListBox aTarget)
    {
        try
        {

            foreach (ListItem item in aSource.Items)
            {
                aTarget.Items.Add(item);
            }
            aSource.Items.Clear();

        }
        catch (Exception expException)
        {
            Response.Write(expException.Message);
        }

    }

    protected void AddRemoveItem(ListBox aSource, ListBox aTarget)
    {

        ListItemCollection licCollection;

        try
        {

            licCollection = new ListItemCollection();
            for (int intCount = 0; intCount < aSource.Items.Count; intCount++)
            {
                if (aSource.Items[intCount].Selected == true)
                    licCollection.Add(aSource.Items[intCount]);
            }

            for (int intCount = 0; intCount < licCollection.Count; intCount++)
            {
                aSource.Items.Remove(licCollection[intCount]);
                aTarget.Items.Add(licCollection[intCount]);
            }

        }
        catch (Exception expException)
        {
            Response.Write(expException.Message);

        }
        finally
        {
            licCollection = null;
        }

    }

    protected void btnaddall_Click(object sender, System.EventArgs e)
    {
        AddRemoveAll(lbUnselected, lbSelected);

    }

    protected void btnadditem_Click(object sender, System.EventArgs e)
    {
        AddRemoveItem(lbUnselected, lbSelected);
    }

    protected void btnremoveitem_Click(object sender, System.EventArgs e)
    {
        AddRemoveItem(lbSelected, lbUnselected);
    }

    protected void btnremoveall_Click(object sender, System.EventArgs e)
    {
        AddRemoveAll(lbSelected, lbUnselected);
    }

    protected void btnColmoveup_Click(object sender, System.EventArgs e)
    {
        MoveUp(lbSelected);
    }

    protected void btnColmovedwn_Click(object sender, System.EventArgs e)
    {
        MoveDown(lbSelected);
    } 
    #endregion
}//class: MultiSelectListBox
