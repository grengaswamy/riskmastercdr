﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrgTree.ascx.cs" Inherits="Riskmaster.Views.Shared.Controls.OrgTree" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
      
       <uc1:ErrorControl ID="ErrorControl" runat="server" />
     
             
       <div id="orgcaption" style="color:#005172;FONT-FAMILY:arial" runat="server">No Node Selected</div>
          <table width="100%">
            <tr>
              <td align="center" class="errortext">
                <asp:Label id="lblErrorInfo" runat="server"></asp:Label>
              </td>
            </tr>
            <tr>
              <td>
                <table align="left" width="100%">
                  
                    <tr>
                      <td>
                        <div style="height:400px;overflow:auto;" class="divScroll" >
                        <asp:Label runat="server" ID="lblMiscInfo"></asp:Label>    
                        <asp:TreeView
                           ID="OTV"
                           PopulateNodesFromClient="true" 
                           ShowLines="true" 
                           ShowExpandCollapse="true" 
                           runat="server"
                           OnTreeNodePopulate="OTV_GetChildNodes"
                           SelectedNodeStyle-Font-Bold="true"
                           SelectedNodeStyle-BackColor="AliceBlue"
                           ForeColor="Black"
                           Target="_self"
                           EnableViewState="false"
                           >
                           
                           <LevelStyles>
                                <asp:TreeNodeStyle />
                                <asp:TreeNodeStyle />
                                <asp:TreeNodeStyle />
                                <asp:TreeNodeStyle />
                                <asp:TreeNodeStyle />
                                <asp:TreeNodeStyle />
                                <asp:TreeNodeStyle />
                                <asp:TreeNodeStyle />
                                <asp:TreeNodeStyle />
                           </LevelStyles>
                           
                        </asp:TreeView>  
                            </div> 
                            <div style="padding-top:5px;width:35%;float:left">
                                <asp:CheckBox runat="server" ID="chkFilter" AutoPostBack="true" Text="Filter by Effective Date"/>
                                
                            </div>
                            <div style="padding-top:5px;width:65%;float:left">
                                <asp:Label ID="lblExpList" runat="server">Default Expansion Level for List </asp:Label> 
                                <asp:DropDownList  onChange="pleaseWait.Show();" ID="cmb_dlevel" runat="server" AutoPostBack="true"></asp:DropDownList>
                            </div>                 
                      </td>
                      <td>
                      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="_" />
                      </td>
                    </tr>
                  
                </table>
              </td>
            </tr>
            </table>
            <table cellpadding="2">
            <tr>
                <td>
                    <asp:CheckBox ID="chkTreeNode" runat="server" style="display:none"/>
                </td>
                <td>
            <asp:HiddenField ID="hd_slevel" runat="server" /></td><td>
            <asp:HiddenField ID="hd_txtsorg" runat="server" /></td><td>
            <asp:HiddenField ID="hd_txtcity" runat="server" /></td><td>
            <asp:HiddenField ID="hd_txtstate" runat="server" /></td><td>
            <asp:HiddenField ID="hd_txtstatetext" runat="server" /></td><td>
            <asp:HiddenField ID="hd_txtzip" runat="server" /></td><td>            
            <asp:HiddenField ID="hd_entityid" runat="server" /></td><td>
            <asp:HiddenField ID="hd_tablename" runat="server" /></td><td>
            <asp:HiddenField ID="hd_rowid" runat="server" /></td><td>
            <asp:HiddenField ID="hd_eventdate" runat="server" /></td><td>
            <asp:HiddenField ID="hd_claimdate" runat="server" /></td><td>
            <asp:HiddenField ID="hd_policydate" runat="server" /> </td><td>            
            <asp:HiddenField ID="hd_selectednodevalue" runat="server" EnableViewState="true" /></td><td>
            <asp:HiddenField ID="hd_selectednodevaluelevel" runat="server" EnableViewState="true"/></td><td>
                
            <asp:HiddenField ID="hd_lob" runat="server" /></td><td>
            <asp:HiddenField ID="hd_lastviewedentityid" runat="server" /> </td><td>
          
<asp:XmlDataSource ID="PreBindedXMLSource" runat="server" EnableViewState="false" EnableCaching="false"/>
                </td>
            
               <%-- <td> 
                    <asp:CheckBox ID="chkAddr" runat="server" Enabled="false"/>
                </td>
                <td>
	                <asp:Label ID="lblAddressCheck" runat="server" Text="Search For Address"></asp:Label>
	            </td>--%>
            </tr>
        </table>
          



