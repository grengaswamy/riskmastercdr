﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.Shared.Controls
{
    public partial class OrgSearchCriteria : System.Web.UI.UserControl
    {
        /// <summary>
        ///  Delegate declaration
        /// </summary>
        /// <param name="strValue"></param>

        public delegate void OnTreeButtonClick(string strValue);

        // Event declaration
        public event OnTreeButtonClick btnTreeHandler;

        /// <summary>
        ///  Delegate declaration
        /// </summary>
        /// <param name="strValue"></param>
        public delegate void OnListButtonClick(string strValue);

        // Event declaration
        public event OnListButtonClick btnListHandler;

        /// <summary>
        ///  Delegate declaration
        /// </summary>
        /// <param name="strValue"></param>
        public delegate void OnResetButtonClick(string strValue);

        // Event declaration
        public event OnResetButtonClick btnResetHandler;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            chkTreeNode.Checked = false;
        }

        protected void btntview_Click(object sender, EventArgs e)
        {
            // Check if event is null
            if (btnTreeHandler != null)
                btnTreeHandler(string.Empty);
        }

        protected void btnlview_Click(object sender, EventArgs e)
        {
            // Check if event is null
            if (btnListHandler != null)
                btnListHandler(string.Empty);
        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            // Check if event is null
            if (btnResetHandler != null)
                btnResetHandler(string.Empty);
            
            txtsorg.Text = "";
            txtstate.CodeText = "";
            txtcity.Text = "";
            txtzip.Text = "";
        }
    }
}