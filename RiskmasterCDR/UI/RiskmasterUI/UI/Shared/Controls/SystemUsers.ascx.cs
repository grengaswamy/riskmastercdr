﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.Shared.Controls
{
    [ValidationProperty("CodeId")]
    public partial class SystemUsers : System.Web.UI.UserControl
    {
        private string m_sCodeTable;
        public string CodeTable
        {
            get
            {
                return m_sCodeTable;
            }
            set
            {
                m_sCodeTable = value;
            }
        }
        
        private string m_sRMXRef;
        public string RMXRef
        {
            set
            {
                rmsyslookup.Attributes.Add("RMXRef", value);
                rmsyslookup_cid.Attributes.Add("RMXRef", value + "/@codeid");
            }
        }

        private string m_sControlName;
        public string ControlName
        {
            get
            {
                return m_sControlName;
            }
            set
            {
                m_sControlName = value;
            }
        }

        private string m_sCodeId;
        public string CodeId
        {
            get
            {
                return rmsyslookup_cid.Text;
            }
            set
            {
                rmsyslookup_cid.Text = value;
            }
        }
        private string m_sCodeText;
        public string CodeText
        {
            get
            {
                return rmsyslookup.Text;
            }
            set
            {
                rmsyslookup.Text = value;
            }
        }

        private string m_sPageName;
        public string PageName
        {
            get
            {
                return m_sPageName;
            }
            set
            {
                m_sPageName = value;
            }
        }

        private string m_sTabIndex;
        public string TabIndex
        {
            get
            {
                return m_sTabIndex;
            }
            set
            {
                m_sTabIndex = value;
            }
        }
        //deb :MITS 20003
        // Public Property exposed to disable/enable the UserControl
        public bool Enabled
        {
            set
            {
                if (value == true)
                {
                    rmsyslookup.ReadOnly = false;
                    rmsyslookup.Style.Add(HtmlTextWriterStyle.BackgroundColor, "");
                }
                else
                {
                    rmsyslookup.ReadOnly = true;
                    rmsyslookup.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                }

                rmsyslookupbtn.Visible = value;
            }
        }
        //deb :MITS 20003
        protected void Page_Load(object sender, EventArgs e)
        {
            rmsyslookupbtn.Attributes["onclick"] = "return OpenCustomizedUserList('" + ControlName + "_rmsyslookup" + "','" + CodeTable + "')";

          if (!string.IsNullOrEmpty(TabIndex))
          {
              rmsyslookup.TabIndex = Int16.Parse(TabIndex);
              rmsyslookupbtn.TabIndex = Convert.ToInt16(Convert.ToInt32(TabIndex) + 1);

          }

        }
    }
}