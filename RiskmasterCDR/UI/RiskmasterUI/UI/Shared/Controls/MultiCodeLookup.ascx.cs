﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.CodeDom;
using System.Web.Compilation;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.Shared.Controls
{
    [ValidationProperty("CodeId")]
    public partial class MultiCodeLookUp : System.Web.UI.UserControl
    {
        private string m_sCodeTable;
        private string m_sMultiCodeMaxSize;
        private string m_CodeText;
        private string m_CodeId;
        public string CodeTable
        {
            get
            {
                return m_sCodeTable;
            }
            set
            {
                m_sCodeTable = value;
            }
        }
        public string MaxSize
        {
            set
            {
                m_sMultiCodeMaxSize = value;
            }
        }

        private string m_RMXType;
        public string RMXType
        {
            set
            {
                m_RMXType = value;
            }

        }

        private string m_sRMXRef;
        public string RMXRef
        {
            set
            {
                m_sRMXRef = value;
            }

        }
        private string m_PowerViewReadOnly;
        public string powerviewreadonly
        {
            set
            {
                m_PowerViewReadOnly = value;
            }
        }
        private string m_sFieldTitle;
        public string FieldTitle
        {
            get
            {
                return m_sFieldTitle;
            }
            set
            {
                m_sFieldTitle = value;
            }
        }
        private string m_sDesSearch;
        public string DesSearch
        {
            get
            {
                return m_sDesSearch;
            }
            set
            {
                m_sDesSearch = value;
            }
        }
        private string m_sControlName;
        public string ControlName
        {
            get
            {
                return m_sControlName;
            }
            set
            {
                m_sControlName = value;
            }
        }
        ////Mridul 07/07/09 MITS 16745 - Enhanced control to have tool tip (Chubb ACK)
        private string m_sToolTip;
        public string ToolTip
        {
            get
            {
                return m_sToolTip;
            }
            set
            {
                m_sToolTip = value;
            }
        }

        private string m_sFilter;
        public string Filter
        {
            get
            {
                return m_sFilter;
            }
            set
            {
                m_sFilter = value;
            }
        }

        private string m_sRemoveToolTip;
        public string RemoveToolTip
        {
            get
            {
                return multicodebtndel.ToolTip;
            }
            set
            {
                multicodebtndel.ToolTip = value;
            }
        }

        private void SetInitialProperties()
        {
            if (!string.IsNullOrEmpty(m_PowerViewReadOnly))
            {
                multicode.Attributes.Add("powerviewreadonly", m_PowerViewReadOnly);
            }
            if (!string.IsNullOrEmpty(m_sRMXRef))
            {
                multicode.Attributes.Add("RMXRef", m_sRMXRef);
                multicode_lst.Attributes.Add("RMXRef", m_sRMXRef + "/@codeid");
            }
            if (!string.IsNullOrEmpty(m_CodeId))
            {
                multicode_lst.Text = m_CodeId;
            }
            if (!string.IsNullOrEmpty(m_CodeText))
            {
                multicode.Text = m_CodeText;
            }
            if (!string.IsNullOrEmpty(m_RMXType))
            {
                multicode.Attributes.Add("RMXType", m_RMXType);
                multicode_lst.Attributes.Add("RMXType", m_RMXType);
            }
            if (!string.IsNullOrEmpty(m_sMultiCodeMaxSize))
            {
                multicode_maxsize.Text = m_sMultiCodeMaxSize;
            }
            else
            {
                multicode_maxsize.Text = string.Empty;
            }
            //Mridul 07/07/09 MITS 16745 - Enhanced control to have tool tip (Chubb ACK)
            if (!string.IsNullOrEmpty(m_sToolTip))
                multicode.ToolTip = m_sToolTip;
//Mona:Adding codefilter like showing codes whith parent "Open":R6
            if (!string.IsNullOrEmpty(m_sFilter))
            {
                multicode.Attributes.Add("Filter", m_sFilter);
            }
            // Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 
            if (!string.IsNullOrEmpty(ShowCheckBox))
            {
                multicode.Attributes.Add("ShowCheckBox", ShowCheckBox);
            }
        }
        
        private string m_sParentNode;
        public string ParentNode
        {
            get
            {
                return m_sParentNode;
            }
            set
            {
                m_sParentNode = value;
            }
        }
        private string m_sTabIndex;
        public string TabIndex
        {
            get
            {
                return m_sTabIndex;
            }
            set
            {
                m_sTabIndex = value;
            }
        }
        private bool m_bRequired = false;
        public bool Required
        {
            get
            {
                return m_bRequired;
            }
            set
            {
                m_bRequired = value;
            }
        }
        private string m_sValidationGroup = string.Empty;
        public string ValidationGroup
        {
            get
            {
                return m_sValidationGroup;
            }
            set
            {
                m_sValidationGroup = value;
            }
        }
        public string CodeId
        {
            get
            {
                m_CodeId = multicode_lst.Text;
                return m_CodeId;
            }
            set
            {
               m_CodeId = value;
               multicode.Text = m_CodeId;
            }
        }
        public string CodeText
        {
            get
            {
                m_CodeText = multicode.Text;
                return m_CodeText;
            }
            set
            {
                m_CodeText = value;
                multicode_lst.Text = value;
            }
        }

        public string Height
        {
            
            set
            {
                multicode.Style["height"] = value;
            }
        }
        public string width
        {

            set
            {
                multicode.Style["width"] = value;
            }
        }
        public string ListCodes
        {
            get
            {
                return multicode_lst.Text;
            }
       
        }

        public string AddListCodes
        {
            set
            {
                multicode_lst.Text = multicode_lst.Text + " " + value;
            }
        }


        public ListItem AddItems
        {
            set
            {
                multicode.Items.Add(value);
            }
        }

        public string GroupAssocClientId
        {
            get
            {
                return multicode_lst.ClientID;
            }
        }

        // Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 
        public string ShowCheckBox { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            //March 25:Raman Bhatia: Incase of Juris postback we have new controls and hence we need to run this event again
            if (!IsPostBack || (m_sRMXRef != null && m_sRMXRef.Contains("Jurisdictionals")))
            {
                SetInitialProperties();
            //    if (!string.IsNullOrEmpty(ParentNode))
            //        multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + ControlName + "_multicode','" + ParentNode + "')";
            //    else
            //        multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + multicode.ClientID + "');";

            //    multicodebtndel.Attributes["onclick"] = "return deleteSelCode('" + multicode.ClientID + "');";
            //    if (!string.IsNullOrEmpty(TabIndex))
            //    {
            //        multicode.TabIndex = Int16.Parse(TabIndex);
            //        multicodebtn.TabIndex = Convert.ToInt16(Convert.ToInt32(TabIndex) + 1);
            //        multicodebtndel.TabIndex = Convert.ToInt16(Convert.ToInt32(TabIndex) + 2);
            //    }
            }
        }
        // Public Property exposed to disable/enable the UserControl
        public bool Enabled
        {
            //aaggarwal29 : MITS 37164, JIRA 4113, the buttons in this user control are not rendered as disabled, so making them visible as false.
            set
            {
                if (value == true)
                {
                    multicode.Style.Add(HtmlTextWriterStyle.BackgroundColor, "");
                }
                else
                {
                    multicode.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#f2f2f2");
                }
                multicode.Enabled = value;
                multicodebtn.Enabled = value;
                multicodebtndel.Enabled = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            //SetInitialProperties();
            if (!string.IsNullOrEmpty(ParentNode))
            {
                //Mona:Adding codefilter like showing codes whith parent "Open":R6
                //multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + ControlName + "_multicode','" + ParentNode + "')";
                // Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 
                if (!string.IsNullOrEmpty(Filter))
                {
                    if (!string.IsNullOrEmpty(ShowCheckBox))
                    {
                        multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + ControlName + "_multicode','" + ParentNode + "',null,'" + Filter + "','" + ShowCheckBox + "')";
                    }
                    else
                    {
                        multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + ControlName + "_multicode','" + ParentNode + "',null,'" + Filter + "')";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ShowCheckBox))
                    {
                        multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + ControlName + "_multicode','" + ParentNode + "',null,null,'" + ShowCheckBox + "')";
                    }
                    else
                    {
                        multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + ControlName + "_multicode','" + ParentNode + "')";
                    }
                }

            }
            else
            { //multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + multicode.ClientID + "');";
                if (!string.IsNullOrEmpty(Filter))
                {
                    if (!string.IsNullOrEmpty(ShowCheckBox))
                    {
                        multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + multicode.ClientID + "',null,null,'" + Filter + "','" + ShowCheckBox + "');";
                    }
                    else
                    {
                        multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + multicode.ClientID + "',null,null,'" + Filter + "');";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ShowCheckBox))
                    {
                        multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + multicode.ClientID + "',null,null,null,'" + ShowCheckBox + "');";
                    }
                    else
                    {
                        multicodebtn.Attributes["onclick"] = "return selectCode('" + CodeTable + "','" + multicode.ClientID + "');";
                    }
                }
            }
            // End: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 
            multicodebtndel.Attributes["onclick"] = "return deleteSelCode('" + multicode.ClientID + "');";
            if (!string.IsNullOrEmpty(TabIndex))
            {
                multicode.TabIndex = Int16.Parse(TabIndex);
                multicodebtn.TabIndex = Convert.ToInt16(Convert.ToInt32(TabIndex) + 1);
                multicodebtndel.TabIndex = Convert.ToInt16(Convert.ToInt32(TabIndex) + 2);
            }


        }


    }
}
