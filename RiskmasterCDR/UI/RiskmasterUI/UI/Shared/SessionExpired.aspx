﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SessionExpired.aspx.cs" Inherits="Riskmaster.UI.Shared.SessionExpired" %>
<html>
 <head id="Head1" runat="server">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
  <title>Session Expired</title>
      <link href='/RiskmasterUI/Content/rmnet.css' rel="stylesheet" type="text/css" />
     
 </head>
 <body class="10pt">
  <div align="center">
   <div align="center" class="errtextheader">Your application session has expired.</div><br>Due to a prolonged period of inactivity, the server has closed this application session.  Please re-authenticate in order
   to continue.
   				<br><input type="button" class="button" value="  Close  " onClick="window.close();"></div>
 </body>
</html>
