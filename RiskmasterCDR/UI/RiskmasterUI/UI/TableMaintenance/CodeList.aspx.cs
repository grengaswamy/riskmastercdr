﻿using System;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections;
using System.Collections.Generic;
using System.Xml.XPath;
using Riskmaster.Common;

namespace Riskmaster.UI.Utilities
{
    public partial class CodeList : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = String.Empty;            
            string typecode = String.Empty;
            string table = String.Empty;
            DataSet codeRecordsSet = null;
            DataTable codeDT = null;
            XmlDocument codeXDoc = new XmlDocument();
            XElement XmlTemplate = null;
            Response.Expires = -1441;
            //aanandpraka2:start changes
            int iCurrentPage = 0;
            int iPageSize = 0;
            int iTotalRows = 0;
            //aanandpraka2:End changes
            if (!IsPostBack)
            {
                try
                {

                    //Use values from get unless there are not any then use default values
                    typecode = Request.QueryString["TypeCode"];
                    if (!String.IsNullOrEmpty(typecode))
                    {
                        this.TypeCode.Text = typecode;
                    } // if

                    table = Request.QueryString["Table"];
                    if (!String.IsNullOrEmpty(table))
                    {
                        this.Table.Text = table;
                    } // if

                    alpha.Text = AppHelper.GetQueryStringValue("Alpha");
                    XmlTemplate = GetMessageTemplate();

                    CallCWSFunction("CodeUtilityAdaptor.GetCodesXML", out sreturnValue, XmlTemplate);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);

                    //aanandpraka2:Start changes for MITS 22728
                    BindGridView(bReturnStatus,sreturnValue);

                    iPageSize = Conversion.ConvertStrToInteger(hdPageSize.Value);
                    iTotalRows = Conversion.ConvertStrToInteger(hdTotalRows.Value);

                    lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Value;
                    lblPageRangeDown.Text = "Page 1" + " of " + hdTotalPages.Value;
                    
                    if (((iCurrentPage * iPageSize) + iPageSize) > iTotalRows) // Last Page
                    {
                        if (iTotalRows != 0)
                        {
                            lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                            lblPagerDown.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                        }
                        else
                        {
                            lblPagerTop.Text = iTotalRows + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                            lblPagerDown.Text = iTotalRows + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                        }
                    }
                    else
                    {
                        if (iTotalRows != 0)
                        {
                            lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " of " + iTotalRows + " matches ";
                            lblPagerDown.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " of " + iTotalRows + " matches ";
                        }
                        else
                        {
                            lblPagerTop.Text = iTotalRows + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                            lblPagerDown.Text = iTotalRows + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                        }
                    }
                    
                    if ((hdTotalPages.Value == "1") || (hdTotalPages.Value == string.Empty))
                    {
                        lnkFirstDown.Enabled = false;
                        lnkFirstTop.Enabled = false;
                        lnkPrevDown.Enabled = false;
                        lnkPrevTop.Enabled = false;
                        lnkNextDown.Enabled = false;
                        lnkNextTop.Enabled = false;
                        lnkLastDown.Enabled = false;
                        lnkLastTop.Enabled = false;
                    }
                    else
                    {
                        lnkFirstTop.Enabled = false;
                        lnkPrevTop.Enabled = false;
                        lnkFirstDown.Enabled = false;
                        lnkPrevDown.Enabled = false;
                    }
                    //aanandpraka2:End changes for MITS 22728
                }
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch
            }
        }

        private XElement GetMessageTemplate() //XML template for CWS call
        {
            #region xml template
                        XElement oTemplate = XElement.Parse(@"
                        <Message>
                            <Authorization></Authorization>
                            <Call>
                                <Function>CodeUtilityAdaptor.GetCodesXML</Function>
                            </Call>
                            <Document>
            					<CodeUtility>
            						<Table/>
            						<TypeCode/>
            						<Alpha/>
                                    <XML>
            							<group name='ucTable' title='User Code Tables'>
            								<control SelectSortOrder='ascending' codelistpage='home?pg=riskmaster/TableMaintenance/CodeListNonBase&amp;TypeCode=3&amp;TableId=&amp;Alpha=' col1='Code' col2='Description' col3='language' isfunc='no' islink='yes' linkpage='home?pg=riskmaster/TableMaintenance/AddEditCode&amp;mode=new&amp;TypeCode=3&amp;TableId=' name='codelist' tiltle='Code Texts' type='listview' pagenumber = '1'/>
            							</group>                                        
            						</XML>
            					</CodeUtility>
                            </Document>
                        </Message>");            
            #endregion xml template

            return oTemplate;
        }

        /// <summary>
        ///  Event added for MITS 22728:Custom paging
        /// </summary>
        protected void LinkCommand_Click(object sender, CommandEventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = String.Empty;
            XElement XmlTemplate = null;
            XElement objTempElement = null;
            XElement objXmlCriteria = null;
            XElement objResultXml = null;
            string sSortColumn = string.Empty;
            string sSortOrder = string.Empty;            
            int iPreviousPage = 0;
            try
            {
                int iPageSize = Conversion.ConvertObjToInt(hdPageSize.Value);
                int iTotalRows = Conversion.ConvertObjToInt(hdTotalRows.Value); ;
                int iCurrentPage = Conversion.ConvertObjToInt(hdCurrentPage.Value);
                XAttribute objtop;

                lnkFirstTop.Enabled = true;
                lnkLastTop.Enabled = true;
                lnkPrevTop.Enabled = true;
                lnkNextTop.Enabled = true;
                lnkFirstDown.Enabled = true;
                lnkLastDown.Enabled = true;
                lnkPrevDown.Enabled = true;
                lnkNextDown.Enabled = true;

                objXmlCriteria = GetMessageTemplate();

                switch (e.CommandName.ToUpper())
                {
                    case "FIRST":
                        {
                            iCurrentPage = 0;

                            lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Value;
                            lblPageRangeDown.Text = "Page 1" + " of " + hdTotalPages.Value;

                            if (((iCurrentPage * iPageSize) + iPageSize) > iTotalRows) // Last Page
                            {
                                lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                                lblPagerDown.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                            }
                            else
                            {
                                lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " of " + iTotalRows + " matches ";
                                lblPagerDown.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " of " + iTotalRows + " matches ";
                            }

                            lnkFirstTop.Enabled = false;
                            lnkPrevTop.Enabled = false;
                            lnkFirstDown.Enabled = false;
                            lnkPrevDown.Enabled = false;

                            if (hdTotalPages.Value == "1")
                            {
                                lnkFirstDown.Enabled = false;
                                lnkFirstTop.Enabled = false;
                                lnkPrevDown.Enabled = false;
                                lnkPrevTop.Enabled = false;
                                lnkNextDown.Enabled = false;
                                lnkNextTop.Enabled = false;
                                lnkFirstTop.Enabled = false;
                                lnkFirstDown.Enabled = false;
                            }
                            else
                            {
                                lnkFirstTop.Enabled = false;
                                lnkPrevTop.Enabled = false;
                                lnkFirstDown.Enabled = false;
                                lnkPrevDown.Enabled = false;
                            }
                            objTempElement = objXmlCriteria.XPathSelectElement("//group[@name='ucTable']/control");
                            objTempElement.SetAttributeValue("pagenumber", "1");
                            break;
                        }
                    case "LAST":
                        {
                            iCurrentPage = Conversion.ConvertStrToInteger(hdTotalPages.Value) - 1;

                            lblPageRangeTop.Text = "Page " + hdTotalPages.Value + " of " + hdTotalPages.Value;
                            lblPageRangeDown.Text = "Page " + hdTotalPages.Value + " of " + hdTotalPages.Value;

                            lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                            lblPagerDown.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " matches ";

                            lnkLastTop.Enabled = false;
                            lnkNextTop.Enabled = false;
                            lnkLastDown.Enabled = false;
                            lnkNextDown.Enabled = false;

                            objTempElement = objXmlCriteria.XPathSelectElement("//group[@name='ucTable']/control");
                            objTempElement.SetAttributeValue("pagenumber", hdTotalPages.Value);
                            break;
                        }
                    case "PREV":
                        {
                            iCurrentPage = Conversion.ConvertStrToInteger(hdCurrentPage.Value);
                            iPreviousPage = iCurrentPage - 1;

                            if (iPreviousPage == 1)
                            {
                                lnkFirstTop.Enabled = false;
                                lnkPrevTop.Enabled = false;
                                lnkFirstDown.Enabled = false;
                                lnkPrevDown.Enabled = false;
                            }


                            lblPageRangeTop.Text = "Page " + iPreviousPage + " of " + hdTotalPages.Value;
                            lblPageRangeDown.Text = "Page " + iPreviousPage + " of " + hdTotalPages.Value;

                            lblPagerTop.Text = (((iPreviousPage - 1) * iPageSize) + 1) + " - " + ((((iPreviousPage - 1) * iPageSize) + iPageSize)) + " of " + iTotalRows + " matches ";
                            lblPagerDown.Text = (((iPreviousPage - 1) * iPageSize) + 1) + " - " + ((((iPreviousPage - 1) * iPageSize) + iPageSize)) + " of " + iTotalRows + " matches ";

                            objTempElement = objXmlCriteria.XPathSelectElement("//group[@name='ucTable']/control");
                            objTempElement.SetAttributeValue("pagenumber", Conversion.ConvertObjToStr(iCurrentPage - 1));
                            break;
                        }
                    case "NEXT":
                        {
                            iCurrentPage = Conversion.ConvertStrToInteger(hdCurrentPage.Value);

                            if ((iCurrentPage + 1) == Conversion.ConvertObjToInt(hdTotalPages.Value))
                            {
                                lnkLastTop.Enabled = false;
                                lnkNextTop.Enabled = false;
                                lnkLastDown.Enabled = false;
                                lnkNextDown.Enabled = false;
                            }

                            lblPageRangeTop.Text = "Page " + (iCurrentPage + 1) + " of " + hdTotalPages.Value;
                            lblPageRangeDown.Text = "Page " + (iCurrentPage + 1) + " of " + hdTotalPages.Value;

                            if (((iCurrentPage * iPageSize) + iPageSize) > iTotalRows) // Last Page
                            {
                                lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                                lblPagerDown.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " matches ";
                            }
                            else
                            {
                                lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " of " + iTotalRows + " matches ";
                                lblPagerDown.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " of " + iTotalRows + " matches ";
                            }

                            objTempElement = objXmlCriteria.XPathSelectElement("//group[@name='ucTable']/control");
                            objTempElement.SetAttributeValue("pagenumber", Conversion.ConvertObjToStr(iCurrentPage + 1));
                            break;
                        }
                }

                CallCWSFunction("CodeUtilityAdaptor.GetCodesXML", out sreturnValue, objXmlCriteria);
                bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);
                BindGridView(bReturnStatus, sreturnValue);
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }        
        }
        
        //aanandpraka2:Start changes for MITS 22728
        //Part of the functionality from Page_load moved to this function for reusability.
        private void BindGridView(Boolean bReturnStatus,string sreturnValue)
        {            
            string typecode = String.Empty;
            string table = String.Empty;
            DataSet codeRecordsSet = null;
            DataTable codeDT = null;
            XmlDocument codeXDoc = new XmlDocument();
            XElement XmlTemplate = null;

            if (bReturnStatus)
            {
                codeXDoc.LoadXml(sreturnValue);
                XmlNode XmlNodeValue = codeXDoc.SelectSingleNode("//ResultMessage/Document/group[@name='ucTable']/TotalRecordCount");
                if (XmlNodeValue != null)
                    hdTotalRows.Value = XmlNodeValue.InnerText.ToString();
                else
                    hdTotalRows.Value = "0";

                XmlNodeValue = codeXDoc.SelectSingleNode("//ResultMessage/Document/group[@name='ucTable']/TotalPages");
                if (XmlNodeValue != null)
                {
                    if (XmlNodeValue.InnerText.ToString() == string.Empty)
                        hdTotalPages.Value = "1";
                    else
                        hdTotalPages.Value = XmlNodeValue.InnerText.ToString();
                }
                else
                    hdTotalPages.Value = "1";

                XmlNodeValue = codeXDoc.SelectSingleNode("//ResultMessage/Document/group[@name='ucTable']/CurrentPageNumber");
                if (XmlNodeValue != null)
                    hdCurrentPage.Value = XmlNodeValue.InnerText.ToString();
                else
                    hdCurrentPage.Value = "1";

                XmlNodeValue = codeXDoc.SelectSingleNode("//ResultMessage/Document/group[@name='ucTable']/PageSize");
                if (XmlNodeValue != null)
                    hdPageSize.Value = XmlNodeValue.InnerText.ToString();
                //JIRA 7810 Start snehal
                XmlNodeValue = codeXDoc.SelectSingleNode("//ResultMessage/Document/group[@name='ucTable']/TableName");
                if (XmlNodeValue != null)
                    if (XmlNodeValue.InnerText =="HOLD_REASON_CODE")
                    btnImportCodes.Enabled = false;
                //JIRA 7810 end snehal
                
                //start - MITS 14772 - mpalinski
                XmlNode xnGetTableValue = codeXDoc.SelectSingleNode("//group[@name='ucTable']/Table");
                if (xnGetTableValue != null)
                {
                    Table.Text = xnGetTableValue.InnerText;
                    hdnTableId.Text = xnGetTableValue.InnerText;

                    btnAddCode.Attributes.Add("onClick", "javascript:return formHandler1('CodeDetail.aspx?Mode=new&TypeCode=" + TypeCode.Text + "&Table=','" + Table.Text + "','','normal','');");
                    btnPrint.Attributes.Add("onClick", "javascript:return formHandler1('PrintCode.aspx?TableId=" + Table.Text + "','','new','swindow','');");
                    btnImportCodes.Attributes.Add("onClick", "javascript:return formHandler1('ImportCode.aspx?1=1','','','swindow','');");
                    //end - MITS 14772 - mpalinski

                    //check if results are in a list or tree
                    if (codeXDoc.DocumentElement.ChildNodes[1].ChildNodes[0].ChildNodes[0].Attributes["type"].Value == "listview")
                    {
                        codeRecordsSet = ConvertXmlDocToDataSet(codeXDoc);
                        //Aman ML Change
                        if (codeRecordsSet.Tables["group"] != null)
                        { 
                        
                        }
                        //Aman ML Change
                        if (codeRecordsSet.Tables["option"] != null)
                        {
                            foreach (DataRow row in codeRecordsSet.Tables["option"].Rows) //make links out of the descriptions
                            {
                                row["Description"] = row["Description"].ToString().Replace("'", "&#39;"); //strip out ' from descriptions so they are valid links  
                                //row["Description"] = "<a title='" + row["Description"] + "' href='CodeDetail.aspx?ID=" + row["id"] + "&Mode=Edit&Table=" + Table.Text + "' target='frm_Code_Detail' >" + row["Description"] + "</a>"; //Aman Commented this for ML
                            }

                            codeDT = codeRecordsSet.Tables["option"];
                            if (codeDT.Columns["Parent"] == null)
                            {
                                codeDT.Columns.Add("Parent");
                            }
                            if (codeDT.Columns["Start-Date"] == null)
                            {
                                codeDT.Columns.Add("Start-Date");
                            }
                            if (codeDT.Columns["End-Date"] == null)
                            {
                                codeDT.Columns.Add("End-Date");
                            }
                            //pmittal5 Mits 19292 01/05/10 - Codes should not get sorted on the basis of Description
                            //codeDT.DefaultView.Sort = String.Format("Description {0}", getSortDirection("Description"));
                            Session["TaskTable"] = codeDT;
                            grdCodeMaint.DataSource = Session["TaskTable"];
                            grdCodeMaint.DataBind();
                        }
                    }
                    else if (codeXDoc.DocumentElement.ChildNodes[1].ChildNodes[0].ChildNodes[0].Attributes["type"].Value == "CodetreeView") //tree view
                    {
                        for (int i = 0; i < codeXDoc.GetElementsByTagName("li").Count; i++)
                        {
                            if (codeXDoc.GetElementsByTagName("li")[i].Attributes["level"].Value == "0")
                            {
                                codeXDoc.GetElementsByTagName("li")[i].ChildNodes[0].Value = codeXDoc.GetElementsByTagName("li")[i].ChildNodes[0].Value.ToString().Replace("'", "&#39;"); //strip out ' from descriptions so they are valid link
                                codeXDoc.GetElementsByTagName("li")[i].ChildNodes[0].Value = "<a title='" + codeXDoc.GetElementsByTagName("li")[i].ChildNodes[0].Value + "' href='CodeDetail.aspx?ID=" + codeXDoc.GetElementsByTagName("li")[i].Attributes["id"].Value + "&Mode=Edit&Table=" + Table.Text + "' target='frm_Code_Detail' >" + codeXDoc.GetElementsByTagName("li")[i].ChildNodes[0].Value + "</a>";
                            }
                        }

                        StringWriter swTree = new StringWriter();
                        XmlTextWriter xtwTree = new XmlTextWriter(swTree);

                        if (codeXDoc != null)
                        {
                            if (codeXDoc.GetElementsByTagName("ul") != null)
                            {
                                if (codeXDoc.GetElementsByTagName("ul")[0] != null)
                                {
                                    codeXDoc.GetElementsByTagName("ul")[0].WriteContentTo(xtwTree);
                                }
                            }
                        }
                        treeview.InnerHtml = swTree.ToString();
                        treeview.InnerHtml = Server.HtmlDecode(treeview.InnerHtml);
                    }
                } // if
                else
                {
                    //Button1.Attributes.Add("onClick", "javascript:return formHandler1('CodeDetail.aspx?Mode=new&TypeCode=" + TypeCode.Text + "&Table=','" + Table.Text + "','','normal','');");
                    //Button2.Attributes.Add("onClick", "javascript:return formHandler1('PrintCode.aspx?TableId=" + Table.Text + "','','new','swindow','');");
                    btnAddCode.Visible = false;
                    btnPrint.Visible = false;
                    btnImportCodes.Attributes.Add("onClick", "javascript:return formHandler1('ImportCode.aspx?1=1','','','swindow','');");
                } // else
            }
        }

        protected void grdCodeMaint_Sorting(object sender, GridViewSortEventArgs e) //sort description
        {
            DataTable dt = Session["TaskTable"] as DataTable;
            if (dt != null)
            {
                dt.DefaultView.Sort = e.SortExpression + " " + getSortDirection(e.SortExpression);
                grdCodeMaint.DataSource = Session["TaskTable"];
                grdCodeMaint.DataBind();
            }
        }

        private string getSortDirection(string column)
        {
            string sortDirection = "ASC";
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }
        //Aman ML Change
        protected void grdCodeMaint_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectCode = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectCode = "setValuesForCode('" + DataBinder.Eval(e.Row.DataItem, "Id").ToString() + "')";
                e.Row.Attributes.Add("onclick", javascriptSelectCode);
            }
        }
        protected void EditCode(object sender, EventArgs e)
        {
            if(hdnCodeId.Text !="0")
                Response.Redirect("CodeDetail.aspx?ID=" + hdnCodeId.Text + "&Mode=Edit&Table=" + Table.Text);
            //href='CodeDetail.aspx?ID=" + row["id"] + "&Mode=Edit&Table=" + Table.Text + "' target='frm_Code_Detail' >" + row["Description"] + "</a>"; 
        }

        protected void btnAdditionalLang_Click(object sender, EventArgs e)
        {
            Response.Redirect("CodeListNonBase.aspx?ID=" + hdnCodeId.Text + "&Mode=Edit&Table=" + Table.Text + "&TypeCode=" + TypeCode.Text + "&Alpha="+alpha.Text);
        }
        
        //Aman ML Change    
    }
}
