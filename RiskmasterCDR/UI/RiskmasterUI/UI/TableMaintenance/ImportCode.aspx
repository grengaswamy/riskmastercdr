﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ImportCode.aspx.cs" Inherits="Riskmaster.UI.Utilities.ImportCode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
    <title>Upload</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/TableMaint.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
    <style type="text/css">
    .center
    {
        text-align: center
    }
    </style>    
</head>
<body onload="RefreshCodeTable()">
    <form id="frmData" runat="server">
        <div class="headertext3"><span lang="en-us">Upload</span></div>
        <div class="center" colspan="2">
        <asp:Button ID="Button1" runat="server" name="Import" Text="Import" OnClick="ImportButton" class="button" style="width:100"/>  
        <asp:Button ID="Button2" runat="server" name="Cancel" Text="Cancel"  class="button" style="width:100"/>
        </div> 
        <table width="100%" cellspacing="0" cellpadding="4" border="0">
            <tr>
                <td colspan="2">
                    <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
                </td>
            </tr>
        </table>         
        <div runat="server" class="row" id="div_fileContents">
            <span id="span1" runat="server" class="label">File:</span><span class="formw">
                <asp:FileUpload ID="fileContents" runat="server" />
            </span></div>
        <div runat="server" class="row" id="div_codeType" xmlns="">
            <span class="label">Code Type:</span><span class="formw">
                <asp:DropDownList id="codeType" runat="server">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                    <asp:ListItem Text="System Code Tables" Value="2"></asp:ListItem>
                    <asp:ListItem Text="User Code Tables" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Industry Standard Codes" Value="10"></asp:ListItem>
                </asp:DropDownList>
            </span></div>
        <div runat="server" class="row" id="div_editDesc" xmlns="">
            <span class="label">Edit Code Description:</span><span class="formw"><asp:checkbox runat="server" 
                id="editDesc" />
            </span></div>
            <asp:textbox style="display: none" runat="server" id="finalMessage" rmxref="Instance/Document/CodeUtility/FileName" Text="" rmxtype="hidden" />
        
        <asp:textbox style="display: none" runat="server" id="FileName" rmxref="Instance/Document/CodeUtility/FileName" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId" rmxref="Instance/Document/CodeUtility/TableId" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TypeCode" rmxref="Instance/Document/CodeUtility/TypeCode" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="Edit" rmxref="Instance/Document/CodeUtility/Edit" Text="" rmxtype="hidden" />
    </form>
</body>
</html>