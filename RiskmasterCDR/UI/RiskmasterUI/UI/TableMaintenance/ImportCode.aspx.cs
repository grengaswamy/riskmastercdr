﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.Utilities
{
    public partial class ImportCode : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void ImportButton(object sender, EventArgs e) //import codes when button is pressed
        {
            try
            {
                bool bReturnStatus = false;
                string sreturnValue = "";
                XmlDocument codeXDoc = new XmlDocument();
                XElement XmlTemplate = null;

                XmlTemplate = SendMessageTemplate();

                HttpPostedFile importFile = fileContents.PostedFile;
                int fileLength = importFile.ContentLength;
                byte[] baFileData = new byte[fileLength];
                importFile.InputStream.Read(baFileData, 0, fileLength);
                FileName.Text = Convert.ToBase64String(baFileData); //encode the file for CWS in base 64

                TypeCode.Text = codeType.SelectedValue.ToString();
                Edit.Text = editDesc.Checked.ToString();

                bReturnStatus = CallCWSFunction("CodeUtilityAdaptor.ImportDataFromTextFile", out sreturnValue, XmlTemplate);
                if (ErrorHelper.IsCWSCallSuccess(sreturnValue))
                {
                        finalMessage.Text = "code uploaded";
                }         
                
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        }
        private XElement SendMessageTemplate() //template XML for CWS call to import codes
        {
            XElement oTemplate = XElement.Parse(@"
				<Message>
					<Authorization>a040d3a6-650f-413d-b206-22a0407d6075</Authorization>
					<Call>
						<Function>CodeUtilityAdaptor.ImportDataFromTextFile</Function>
					</Call>
					<Document>
						<CodeUtility>
							<Xml>
								<import>
									<msg desc='' filename='' id='1' isedit='0' level='0'>
										<info id='1' name='system table' value=''/>
										<info id='2' name='user table' value=''/>
										<info id='3' name='row' value=''/>
										<info id='4' name='codedesc' value=''/>
										<info id='5' name='tableid' value=''/>
										<info id='6' name='codetype' value=''/>
									</msg>
								</import>
							</Xml>
							<FileName></FileName>
							<TableId></TableId>
							<TypeCode></TypeCode>
							<Action>3</Action>
							<Edit/>
						</CodeUtility>
					</Document>
				</Message>
            ");

            return oTemplate;
        }
    }
}