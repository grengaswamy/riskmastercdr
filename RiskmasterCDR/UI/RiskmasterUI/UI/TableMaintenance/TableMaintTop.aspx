﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TableMaintTop.aspx.cs" Inherits="Riskmaster.UI.Utilities.TableMaintTop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Table Maintenance</title>
  <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/system.css" type="text/css"/>
  <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">
  </script>
  <script type="text/javascript" language="JavaScript" src="../../Scripts/TableMaint.js">
  </script>
    <style type="text/css">
        .style1
        {
            width: 850px;
        }
        .center
        {
            text-align: center;
        }
        .style5
        {
            width: 8px;
        }
        </style>
</head>

<body>
    <form id="frmData" runat="server">
    <div>
        <table border="0" style="width: 100%">
            <tr>
                <td class="headertext3" colspan="1" width="100%">Table Maintenance</td>
            </tr>
            <table width="100%" cellspacing="0" cellpadding="4" border="0">
                <tr>
                    <td colspan="2">
                        <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
                    </td>
                </tr>
            </table>  
            <tr>
                <td class="style1">
                    <asp:DropDownList ID="DropDown_Table_Types" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        
        <table border="0">
            <tr>
                <%--Bijender Start Mits 14894--%>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','A');return false;">A</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','B');return false;">B</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','C');return false;">C</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','D');return false;">D</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','E');return false;">E</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','F');return false;">F</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','G');return false;">G</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','H');return false;">H</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','I');return false;">I</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','J');return false;">J</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','K');return false;">K</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','L');return false;">L</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','M');return false;">M</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','N');return false;">N</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','O');return false;">O</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','P');return false;">P</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','Q');return false;">Q</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','R');return false;">R</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','S');return false;">S</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','T');return false;">T</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','U');return false;">U</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','V');return false;">V</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','W');return false;">W</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','X');return false;">X</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','Y');return false;">Y</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','Z');return false;">Z</a></td>
                <!--sthiyagaraj7:32111:25-04-2013 -->
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','0');return false;">0</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','1');return false;">1</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','2');return false;">2</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','3');return false;">3</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','4');return false;">4</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','5');return false;">5</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','6');return false;">6</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','7');return false;">7</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','8');return false;">8</a></td>
                <td><a href="#" onclick="javascript:refresh_tables('TableList.aspx','9');return false;">9</a></td>
                <!--End sthiyagaraj7:32111:25-04-2013 -->

                <%--Bijender End Mits 14894--%>
            </tr>
        </table>    
    </div>
<asp:textbox style="display: none" runat="server" id="glossary_types" rmxref="Instance/Document/CodeUtility/TypeCode" Text="" rmxtype="hidden" />
<asp:textbox style="display: none" runat="server" id="id" rmxref="Instance/Document/CodeUtility/ID" Text="0" rmxtype="hidden" />
<asp:textbox style="display: none" runat="server" id="alpha" rmxref="Instance/Document/CodeUtility/Alpha" Text="" rmxtype="hidden" />
<asp:textbox style="display: none" runat="server" id="form_name" rmxref="Instance/Document/CodeUtility/FormName" Text="topmenu" rmxtype="hidden" />
</form>
</body>
</html>
