﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Riskmaster.Models;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.Xml.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Xml;
using System.Globalization;

namespace Riskmaster.UI.Utilities
{
    public partial class CodeLookUp : NonFDMBasePageCWS
    {
        //protected CodeListType objList = null;
        //protected List<CodeType> codes = null;
        protected string sCodeTable = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            DataSet codeRecordsSet = null;
            DataTable codeDT = null;
            XmlDocument codeXDoc = new XmlDocument();
            XElement XmlTemplate = null;
            string typecode, tableID;
            if (!IsPostBack)
            {
                try
                {
                    typecode = Request.QueryString["TypeCode"];
                    tableID = Request.QueryString["ID"];
                    //Use values from get unless there are not any then use default values
                    if (typecode != "" && typecode != null)
                        this.TypeCode.Text = typecode;
                    if (tableID != "" && tableID != null)
                        this.ID.Text = tableID;
                    if (typecode == "Currency_Type")
                    {
                        XmlDocument xCu = new XmlDocument();
                        XElement otemplate = XElement.Parse(@"<Document>
	                                                          </Document>");
                        XmlElement xmlEl = null;
                        xCu.LoadXml(otemplate.ToString());
                        int lcounter = 1;
                        foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
                        {
                            RegionInfo ri=new RegionInfo(ci.Name);
                            xmlEl = xCu.CreateElement("option");
                            xmlEl.SetAttribute("id", lcounter.ToString());
                            xmlEl.SetAttribute("code", ri.ISOCurrencySymbol + " " + ri.CurrencySymbol);
                            xmlEl.SetAttribute("Description", ri.EnglishName + "|" + ci.Name);
                            xmlEl.SetAttribute("Organization","");
                            xCu.DocumentElement.AppendChild(xmlEl);
                            lcounter++;
                        }
                        bReturnStatus = true;
                        sreturnValue = xCu.OuterXml;
                        this.TypeCode.Text = "Code";
                    }
                    else
                    {
                        XmlTemplate = GetMessageTemplate();
                        bReturnStatus = CallCWSFunction("CodeUtilityAdaptor.GetXMLData", out sreturnValue, XmlTemplate);
                    }
                    if (bReturnStatus)
                    {
                        codeXDoc.LoadXml(sreturnValue);
                        codeRecordsSet = ConvertXmlDocToDataSet(codeXDoc);
                        if (codeRecordsSet.Tables["option"] != null) //Moved Null Check For MITS:16782
                        {
                            foreach (DataRow row in codeRecordsSet.Tables["option"].Rows) //make links out of the descriptions
                            {
                                row["Description"] = row["Description"].ToString().Replace("'", "&#39;"); //strip out ' from descriptions so they are valid links
                                // if (typecode == "Currency_Type")
                                //    row["Description"] = "<a title='" + row["Description"] + "' href='javascript:selvalue(&quot;" + row["code"] + "&quot;,&quot;&quot;,&quot;" + row["id"] + "&quot;,&quot;" + this.TypeCode.Text + "&quot;);'>" + row["Description"] + "</a>";
                                //else
                                    row["Description"] = "<a title='" + row["Description"] + "' href='javascript:selvalue(&quot;" + row["code"] + "&quot;,&quot;" + row["Description"] + "&quot;,&quot;" + row["id"] + "&quot;,&quot;" + this.TypeCode.Text + "&quot;);'>" + row["Description"] + "</a>";
                            }

                            //if (codeRecordsSet.Tables["option"] != null) //Commented Null Check For MITS:16782
                            //{
                            codeDT = codeRecordsSet.Tables["option"];

                            if (codeDT.Columns["Parent"] == null)
                            {
                                codeDT.Columns.Add("Parent");
                            } // if
                            if (codeDT.Columns["Start-Date"] == null)
                            {
                                codeDT.Columns.Add("Start-Date");
                            } // if
                            if (codeDT.Columns["End-Date"] == null)
                            {
                                codeDT.Columns.Add("End-Date");
                            } // if

                            codeDT.DefaultView.Sort = "Description" + " " + getSortDirection("Description");
                            Session["TaskTable"] = codeDT;
                            dgCodes.DataSource = Session["TaskTable"];
                            dgCodes.DataBind();
                        }
                        else //Show Grid Headers. //Added to show Grid Header For MITS:16782
                        {
                            codeDT = new DataTable();
                            if (codeDT.Columns["ID"] == null)
                            {
                                codeDT.Columns.Add("ID");
                            }
                            if (codeDT.Columns["Code"] == null)
                            {
                                codeDT.Columns.Add("Code");
                            }
                            if (codeDT.Columns["Description"] == null)
                            {
                                codeDT.Columns.Add("Description");
                            }
                            if (codeDT.Columns["Parent"] == null)
                            {
                                codeDT.Columns.Add("Parent");
                            } // if
                            if (codeDT.Columns["Start-Date"] == null)
                            {
                                codeDT.Columns.Add("Start-Date");
                            } // if
                            if (codeDT.Columns["End-Date"] == null)
                            {
                                codeDT.Columns.Add("End-Date");
                            }
                            if (codeDT.Columns["Organization"] == null)
                            {
                                codeDT.Columns.Add("Organization");
                            }
                            DataRow row = codeDT.NewRow();
                            codeDT.Rows.Add(row);
                            Session["TaskTable"] = codeDT;
                            dgCodes.DataSource = codeDT;
                            dgCodes.DataBind();
                        }

                    }
                }
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch
            }
        }

        private XElement GetMessageTemplate() //XML template for CWS call
        {
            string mode = "";
            mode = Request.QueryString["Mode"];
            XElement oTemplate = XElement.Parse(@"
            <Message>
	            <Authorization>a040d3a6-650f-413d-b206-22a0407d6075</Authorization>
	            <Call>
		            <Function>CodeUtilityAdaptor.GetXMLData</Function>
	            </Call>
	            <Document>
		            <CodeListPopUp>
			            <FormName></FormName>
			            <Mode></Mode>
			            <TypeCode></TypeCode>
			            <ID></ID>
			            <SortOrder/>
                        <Alpha/>
			            <XML>
						    <form name='codetext' sid='150' supp='CLAIM_SUPP' title='Code Texts' topbuttons='1'>
							    <group name='ucTable' title='User Code Tables'>
								    <control SelectSortOrder='ascending' codelistpage='home?pg=riskmaster/TableMaintenance/CodeListPopUp&amp;List=pCode&amp;TableId=1095' col1='Code' col2='Description' col3='parent' col4='Start Date' col5='End Date' col6='Organization' ctname='' isfunc='yes' islink='' linkpage='' name='codelist' tiltle='Code Texts' type='listview'/>
							    </group>
						        </form>
                        </XML>
		                </CodeListPopUp>
	                </Document>
                </Message>
            ");

            return oTemplate;
        }
        protected void dgCodes_Sorting(object sender, GridViewSortEventArgs e) //sort description
        {
            DataTable dt = Session["TaskTable"] as DataTable;
            if (dt != null)
            {
                dt.DefaultView.Sort = e.SortExpression + " " + getSortDirection(e.SortExpression);
                dgCodes.DataSource = Session["TaskTable"];
                dgCodes.DataBind();
            }
        }
        private string getSortDirection(string column)
        {
            string sortDirection = "ASC";
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }
    }
}