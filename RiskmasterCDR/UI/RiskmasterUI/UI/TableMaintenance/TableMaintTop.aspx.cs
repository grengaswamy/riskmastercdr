﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.Utilities
{
    public partial class TableMaintTop : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            XmlDocument codeXDoc = new XmlDocument();
            XElement XmlTemplate=null;
            if (!IsPostBack)
            {
                try
                {
                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWSFunction("CodeUtilityAdaptor.GetXMLData", out sreturnValue, XmlTemplate);
                    if (bReturnStatus)
                    {
                        codeXDoc.LoadXml(sreturnValue);
                        DataSet dSet = new DataSet();
                        dSet.ReadXml(new XmlNodeReader(codeXDoc.SelectSingleNode("//control[@name='glossary_types']")));
                        if (dSet != null)
                        {
                            dSet.Tables["TypeCode"].Columns.Add(new DataColumn("Denormalized", System.Type.GetType("System.String"), "[code]+ '-' + [Description]"));
                            DropDown_Table_Types.DataSource = dSet.Tables["TypeCode"];
                            DropDown_Table_Types.DataValueField = "code";
                            DropDown_Table_Types.DataTextField = "Denormalized";
                            DropDown_Table_Types.DataBind();
                            DropDown_Table_Types.SelectedValue = "3";
                            DropDown_Table_Types.Attributes.Add("onChange", "javascript: setchanges('DropDown_Table_Types');");
                        }
                    }
                }
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch
            }
        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization>
                <Call>
                    <Function>CodeUtilityAdaptor.GetXMLData</Function>
                </Call>
                <Document>
					<CodeUtility>
						<FormName></FormName>
						<ID></ID>
						<TypeCode/>
						<Alpha/>
						<XML>
							<control name='glossary_types' optionname='TypeCode' title='' type='combobox'/>
						</XML>
					</CodeUtility>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
    }
}