<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CodeLookUp.aspx.cs" Inherits="Riskmaster.UI.Utilities.CodeLookUp"
    EnableViewStateMac="false" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Code Selection</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/TableMaint.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>

</head>
<body>
    <form id="frmData" runat="server">
    <table width="100%" cellspacing="0" cellpadding="4" border="0">
        <tr>
            <td colspan="2">
                <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
            </td>
        </tr>
    </table>
               <table width="100%" class="singleborder" cellspacing="0" cellpadding="0">
                
                 <tr>
                 <td style="width:100%;">
                 <asp:GridView ID="dgCodes" runat="server" AllowPaging="false" AllowSorting="true" 
                              AutoGenerateColumns="false" OnSorting="dgCodes_Sorting"
                           Width="100%" GridLines="None"  EnableViewState="true">
                            
                            <PagerSettings FirstPageText="&quot;First&quot;" LastPageText="Last" Mode="NextPreviousFirstLast" 
                                Position="Top" NextPageText="Next" />
                             <HeaderStyle CssClass="msgheader" />
                           <AlternatingRowStyle CssClass="data2" /> 
                            <Columns>
                                <asp:TemplateField HeaderText="Code" ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                    
                                      <span id="spanCode" class ="data"><%# DataBinder.Eval(Container, "DataItem.code")%></span>
                                       <span style="display: none" runat="server" id="fileName"><%# DataBinder.Eval(Container, "DataItem.id")%></span>
                                    </ItemTemplate> 
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" SortExpression="Description" ItemStyle-CssClass="data" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                    
                                      <span id="spanDesc" class ="data"><%# DataBinder.Eval(Container, "DataItem.Description")%></span>
                                       <span style="display: none" runat="server" id="fileName"><%# DataBinder.Eval(Container, "DataItem.id")%></span>
                                    </ItemTemplate> 
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="Parent" ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                    
                                      <span id="spanParent" class ="data"><%# DataBinder.Eval(Container, "DataItem.Parent")%></span>
                                       <span style="display: none" runat="server" id="fileName"><%# DataBinder.Eval(Container, "DataItem.id")%></span>
                                    </ItemTemplate> 
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Start Date" ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                    
                                      <span id="spanDesc" class ="data"><%# DataBinder.Eval(Container, "DataItem.Start-Date")%></span>
                                       <span style="display: none" runat="server" id="fileName"><%# DataBinder.Eval(Container, "DataItem.id")%></span>
                                    </ItemTemplate> 
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="End Date" ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                    
                                      <span id="spanDesc" class ="data"><%# DataBinder.Eval(Container, "DataItem.End-Date")%></span>
                                       <span style="display: none" runat="server" id="fileName"><%# DataBinder.Eval(Container, "DataItem.id")%></span>
                                    </ItemTemplate> 
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Organization" ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                    
                                      <span id="spanDesc" class ="data"><%# DataBinder.Eval(Container, "DataItem.Organization")%></span>
                                       <span style="display: none" runat="server" id="fileName"><%# DataBinder.Eval(Container, "DataItem.id")%></span>
                                    </ItemTemplate> 
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        </td>
                 </table>
              </div>
             </td>
            </tr>
           </table>
    <asp:textbox style="display: none" runat="server" id="FormName" rmxref="Instance/Document/CodeListPopUp/FormName" Text="codelist" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="Mode" rmxref="Instance/Document/CodeListPopUp/Mode" Text="" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="TypeCode" rmxref="Instance/Document/CodeListPopUp/TypeCode" Text="pCode" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="ID" rmxref="Instance/Document/CodeListPopUp/ID" Text="" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="Alpha" rmxref="Instance/Document/CodeListPopUp/Alpha" Text="" rmxtype="hidden" />
    </form>
</body>
</html>