﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities
{
    public partial class PrintCode : NonFDMBasePageCWS
    {  
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus;
            string sReturnValue = String.Empty;
            string typecode = String.Empty;
            string tableid = String.Empty;
            XElement XmlTemplate = null;
            DataSet tableRecordsSet = null;
            XmlDocument codeXDoc = new XmlDocument();

            try
            {
                if (!IsPostBack)
                {
                    //Use values from get unless there are not any then use default values
                    typecode = Request.QueryString["TypeCode"];
                    if (!String.IsNullOrEmpty(typecode))
                    {
                        this.TypeCode.Text = typecode;
                    } // if

                    tableid = Request.QueryString["TableId"];
                    if (!String.IsNullOrEmpty(tableid))
                    {
                        this.TableId.Text = tableid;
                    } // if

                    XmlTemplate = GetMessageTemplate();
                    CallCWSFunction("CodeUtilityAdaptor.PrintCodesXML", out sReturnValue, XmlTemplate);

                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sReturnValue);
                    if (bReturnStatus)
                    {
                        codeXDoc.LoadXml(sReturnValue);
                        tableRecordsSet = ConvertXmlDocToDataSet(codeXDoc);

                        if (tableRecordsSet.Tables["PrintCodeList"].Columns["File"].Table.Rows[0].ItemArray[0] != null)
                        {
                            File.Text = tableRecordsSet.Tables["PrintCodeList"].Columns["File"].Table.Rows[0].ItemArray[0].ToString();
                            TextBox txtPdfFile = (TextBox)this.Form.FindControl("File");

                            byte[] pdfbytes = Convert.FromBase64String(txtPdfFile.Text); //decode the base64 encoded pdf

                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Charset = "";
                            Response.AppendHeader("Content-Encoding", "none;");
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=Codes_{0}.PDF", TableId.Text));
                            Response.AddHeader("Accept-Ranges", "bytes");
                            Response.BinaryWrite(pdfbytes);
                            Response.Flush();
                            Response.Close();
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        }

        /// <summary>
        /// get pdf of codes from cws call
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            #region xml template
            XElement oTemplate = XElement.Parse(@"
				<Message>
					<Authorization>3ac889ae-6fe3-42a2-acad-3a9ab0557c63</Authorization>
					<Call>
						<Function>CodeUtilityAdaptor.PrintCodesXML</Function>
					</Call>
					<Document>
						<PrintCodes>
							<TableId></TableId>
							<TypeCode></TypeCode>
							<Alpha/>
                            <control SelectSortOrder='ascending' codelistpage='home?pg=riskmaster/TableMaintenance/CodeList&amp;TypeCode=3&amp;TableId=&amp;Alpha=' col1='Code' col2='Description' col3='parent' col4='Start Date' col5='End Date' col6='Organization' isfunc='no' islink='yes' linkpage='home?pg=riskmaster/TableMaintenance/AddEditCode&amp;mode=new&amp;TypeCode=3&amp;TableId=' name='codelist' tiltle='Code Texts' type='CodetreeView'/>
						</PrintCodes>
					</Document>
				</Message>");
            #endregion

            return oTemplate;
        }
    }
}
