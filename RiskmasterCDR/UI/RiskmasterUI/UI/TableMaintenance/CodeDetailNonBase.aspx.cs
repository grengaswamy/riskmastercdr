﻿using System;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections;
using System.Collections.Generic;
using System.Xml.XPath;
using Riskmaster.Common;

namespace Riskmaster.UI.Utilities
{
    public partial class CodeDetailNonBase : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            DataSet tableRecordsSet = null;
            XmlDocument codeXDoc = new XmlDocument();
            XElement XmlTemplate = null;
            string typecode, codeID, tableID, mode;
            string iLangCode = string.Empty;
            string sLanguage = string.Empty;          
            if (AppHelper.GetQueryStringValue("Mode") == "newNonBase")
                DeleteButton.Visible = false;
            else
                DeleteButton.Visible = true;
            //End            
           
            if (!IsPostBack)
            {
                try
                {
                    typecode = AppHelper.GetQueryStringValue("TypeCode");
                    codeID = AppHelper.GetQueryStringValue("CodeId");
                    tableID = AppHelper.GetQueryStringValue("Table");
                    mode = AppHelper.GetQueryStringValue("Mode");
                    sLanguage = AppHelper.GetQueryStringValue("Language");
                    //Use values from get unless there are not any then use default values
                    if (typecode != "" && typecode != null)
                        this.TypeCode.Text = typecode;
                    if (codeID != "" && codeID != null)
                        this.ID.Text = codeID;
                    if (tableID != "" && tableID != null)
                        this.TID.Text = tableID;
                    if (sLanguage != "" && sLanguage != null)  
                        this.LangCode.Text = sLanguage;
                    alpha.Text = AppHelper.GetQueryStringValue("Alpha");
                    XmlTemplate = GetMessageTemplate();
                    if (XmlTemplate.XPathSelectElement("//CodeID") != null)
                    {
                        XmlTemplate.XPathSelectElement("//CodeID").Value = codeID;                       
                    }
                    CancelButton.Attributes.Add("onClick", "javascript:return formHandler1('CodeListNonBase.aspx?TypeCode=" + TypeCode.Text + "&ID=" + ID.Text + "&Alpha="+alpha.Text + "&Table=" + TID.Text + "','','','normal','');");
                    DeleteButton.Attributes.Add("onClick", "javascript:return Delete();");

                    if (mode == "EditNonBase") //For Editing a code load data from DB
                        bReturnStatus = CallCWSFunction("CodeUtilityAdaptor.GetXMLData", out sreturnValue, XmlTemplate);
                    else //Enable the onBlur events for the buttons, but don't load data
                    {
                        //ID.Text = TID.Text;
                        bReturnStatus = CallCWSFunction("CodeUtilityAdaptor.GetXMLData", out sreturnValue, XmlTemplate);                        
                    }
                    if (bReturnStatus)
                    {
                        codeXDoc.LoadXml(sreturnValue);
                        tableRecordsSet = ConvertXmlDocToDataSet(codeXDoc);
                        //Added by Amitosh for payee phrase
                        if ((codeXDoc.SelectSingleNode("//RemoveDelButton") != null) && string.Equals(codeXDoc.SelectSingleNode("//RemoveDelButton").InnerText, "true", StringComparison.InvariantCultureIgnoreCase))
                        {
                            DeleteButton.Visible = false;
                        }
                        //end delete
                        if (tableRecordsSet.Tables["control"] != null)
                        {
                            foreach (DataRow row in tableRecordsSet.Tables["control"].Rows)
                            {
                                if (row["name"].ToString() == "TableId")
                                {
                                    //TID.Text = row["value"].ToString(); //use TID for saving
                                   // CancelButton.Attributes.Add("onClick", "javascript:return formHandler1('CodeListNonBase.aspx?TypeCode=" + TypeCode.Text + "&ID=" + ID.Text + "&Table=" + TID.Text + "','','','normal','');");
                                    CancelButton.Attributes.Add("onClick", "javascript:return formHandler1('CodeListNonBase.aspx?TypeCode=" + TypeCode.Text + "&ID=" + ID.Text + "&Alpha=" + alpha.Text + "&Table=" + TID.Text + "','','','normal','');");
                                }
                                if (row["name"].ToString() == "req_fields")//Parijat: MITS 20198
                                {
                                    req_fields.Text = row["value"].ToString();
                                }
                                if (row["name"].ToString() == "LanguageCode")
                                {
                                    iLangCode = row["value"].ToString();
                                }
                                foreach (Control oCtrl in this.Form.Controls)
                                {
                                    foreach (Control oCtrl2 in oCtrl.Controls)
                                    {
                                        if (oCtrl2.ID == row["name"].ToString())
                                        {
                                            switch (oCtrl2.ID)  //set events for the fields and the values for the fields
                                            {
                                                case "Code":
                                                    {
                                                        Code.Text = row["value"].ToString();

                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            Code.Enabled = true;
                                                        else
                                                            Code.Enabled = false;
                                                        break;
                                                    }
                                                case "Code_Desc":
                                                    {
                                                        Code_Desc.Text = row["value"].ToString();
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            Code_Desc.Enabled = true;
                                                        else
                                                            Code_Desc.Enabled = false;
                                                        break;
                                                    }                                                
                                                                                              
                                                case "language":
                                                    {
                                                        if (tableRecordsSet.Tables["option"] != null)
                                                        {
                                                            DataView langDataView = new DataView(tableRecordsSet.Tables["option"]);
                                                            language.DataSource = langDataView;
                                                            language.DataTextField = "option_Text";
                                                            language.DataValueField = "value";
                                                            if (!string.IsNullOrEmpty(iLangCode))
                                                                language.SelectedValue = iLangCode;
                                                            else
                                                                language.SelectedValue = AppHelper.GetBaseLanguageCodeWithCulture().ToString().Split('|')[0];  //replace with the base language                                                            
                                                            if (AppHelper.GetQueryStringValue("Mode") == "EditNonBase")
                                                            {                                                                
                                                                language.Enabled = false;
                                                            }
                                                            language.DataBind();
                                                        }
                                                    }
                                                    break;                                              
                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //if (codeXDoc.SelectSingleNode("//VisibleCurrencyLink") != null)
                        //{
                        //    linkCurrencyCode.Visible = Convert.ToBoolean(codeXDoc.SelectSingleNode("//VisibleCurrencyLink").InnerText);
                        //    linkCurrencyCode.Attributes.Add("onClick", "window.open('CodeLookUp.aspx?TypeCode=Currency_Type&ID=" + Convert.ToString(tableID) + "','','resizable=yes,scrollbars=yes')");
                        //}
                    }
                }
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch

            }

        }
        private XElement GetMessageTemplate() //XML template for CWS call
        {
            string mode = "";
            mode = Request.QueryString["Mode"];
            //Use values from get unless there are not any then use default values
            if (mode != "" && mode != null)
                this.Mode.Text = mode;
            if (mode == "newNonBase")
            {
                this.FormName.Text = "frmaddnonbasecodedesc";
            }
            else if (mode == "EditNonBase")
            {
                this.FormName.Text = "frmeditnonbasecodedesc";
            }           
            XElement oTemplate = XElement.Parse(@"
            <Message>
	            <Authorization>a040d3a6-650f-413d-b206-22a0407d6075</Authorization>
	            <Call>
		            <Function>CodeUtilityAdaptor.GetXMLData</Function>
	            </Call>
	            <Document>
		            <CodeUtility>
			            <FormName></FormName>
			            <Mode></Mode>
			            <TypeCode></TypeCode>
			            <ID></ID>
                        <CodeID/>
			            <Alpha/>
                        <LangCode/>
                        <XML>
						    <form actionname='' bcolor='white' name='frmData' sid='150' title='Enter Code Detail' topbuttons='1'>
							    <body1 func='pageloaded();' req_func='yes'/>
                                <RemoveDelButton/>
							    <group name='code_detail' title='Code Detail' >
								    <error desc='' value='0'/>
								    <control isrequ='yes' name='Code' title='Code' type='text' value='' lock='false'/>
								    <control isrequ='yes' name='Code_Desc' title='Description' type='text' value=''/>
								    <control name='action' type='id' value=''/>
								    <control name='TableId' type='id' value=''/>
								    <control name='req_fields' type='id' value='Code|Code_Desc'/>
                                    <control name='LanguageCode' type='' value=''/>
                                    <control name='language' type='' value=''/>
							    </group>
							    <button linkto='' name='btnsave' param='' title='Save' type='save' windowClose='false'/>
							    <button linkto='home?pg=riskmaster/TableMaintenance/CodeList&amp;TypeCode=3&amp;TableId=' name='btncancel' param='' title='Cancel' type='normal'/>
							    <button linkto='' name='btndelete' param='' title='Delete' type='delete' windowClose='false'/>
						    </form>
			            </XML>
		            </CodeUtility>
	            </Document>
            </Message>
            ");

            return oTemplate;
        }
        protected void SaveCode(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                string sreturnValue = "";
                XElement XmlTemplate = null;
                if (FlagForRefresh.Text != "0")
                {
                    XmlTemplate = SaveMessageTemplate();
                    if (XmlTemplate.XPathSelectElement("//LanguageCode") != null)
                    {
                        XmlTemplate.XPathSelectElement("//LanguageCode").Value = AppHelper.GetBaseLanguageCodeWithCulture().ToString().Split('|')[0];
                    }
                    if (Code_cid.Text != "")
                        CodeDesc.Text = CodeDesc.Text + "|" + Code_cid.Text.Split('|')[1];
                    bReturnStatus = CallCWSFunction("CodeUtilityAdaptor.SaveCode", out sreturnValue, XmlTemplate);
                    //MITS 15488
                    if (ErrorHelper.IsCWSCallSuccess(sreturnValue))
                    {
                        Response.Write("<script>location.href = 'CodeListNonBase.aspx?TypeCode=" + TypeCode.Text + "&Alpha=" +alpha.Text + "&ID=" + ID.Text + "&Table=" + TID.Text + "';</script>");
                    }


                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        }
        private XElement SaveMessageTemplate() //SML template for save CWS call
        {
            if (Mode.Text == "newNonBase")
                TableId.Text = TID.Text;
            else
                TableId.Text = ID.Text;   //R4 stored code ID in table ID field when editing a code....                           
            ShortCode.Text = Code.Text;
            CodeDesc.Text = Code_Desc.Text;
           
            XElement oTemplate = XElement.Parse(@"
				<Message>
					<Authorization>3ac889ae-6fe3-42a2-acad-3a9ab0557c63</Authorization>
					<Call>
						<Function>CodeUtilityAdaptor.SaveCode</Function>
					</Call>
					<Document>
						<CodeUtility>
							<Mode></Mode>
							<InputData>
								<Code>
                                    <ID></ID>
									<TableId></TableId>									
									<ShortCode></ShortCode>
									<CodeDesc></CodeDesc>									
									<LanguageCode>1033</LanguageCode>
								</Code>
							</InputData>
						</CodeUtility>
					</Document>
				</Message>
            ");

            return oTemplate;
        }
        protected void DeleteCode(object sender, EventArgs e)
        {
            try
            {
                Mode.Text = "deleteNonBase";
                SaveCode(sender, e);                
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        }
    }
}