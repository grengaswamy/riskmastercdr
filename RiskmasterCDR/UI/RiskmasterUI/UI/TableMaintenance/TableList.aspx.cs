﻿using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

using Riskmaster.BusinessAdaptor.Common;
using System.Text.RegularExpressions;
using System.Web;


namespace Riskmaster.UI.Utilities
{
    public partial class TableList : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string typecode = String.Empty;
            string letter = String.Empty;
            string sreturnValue = String.Empty;
            DataSet tableRecordsSet = null;
            XmlDocument tableXDoc = new XmlDocument();
            XElement XmlTemplate=null;

            // MITS 15831 MAC
            //if (!IsPostBack)
            //{

                try
                {
                    //Use values from get unless there are not any then use default values
                    //srajindersin - Pentesting - 27683
                    //typecode = Request.QueryString["TypeCode"];
                    if (!string.IsNullOrEmpty(Request.QueryString["TypeCode"]))
                        typecode = AppHelper.HTMLCustomEncode(Request.QueryString["TypeCode"].Replace("<", "").Replace(">", ""));
                        //typecode = Microsoft.Security.Application.Encoder.HtmlEncode(Request.QueryString["TypeCode"].Replace("<", "").Replace(">", ""));
                    //END srajindersin - Pentesting - 27683
                    
                    if (String.IsNullOrEmpty(typecode)) 
                    {
                        typecode = hdnTableId.Value.ToString();
                    } // if

                    // MITS 15831 MAC - Will reload if typecode is empty so no point running the block inside
                    if (!String.IsNullOrEmpty(typecode))
                    {
                        this.TypeCode.Text = typecode;

                        letter = Request.QueryString["Alpha"];
                        if (!String.IsNullOrEmpty(letter))
                        {
                            this.alpha.Text = letter;
                        } // if

                        //vsharma65 start MITS 27728
                        if (String.IsNullOrEmpty(letter))
                        {
                            this.alpha.Text = "A";
                        }
                        //vsharma65 End MITS 27728

                        XmlTemplate = GetMessageTemplate();
                        CallCWSFunction("CodeUtilityAdaptor.GetXMLData", out sreturnValue, XmlTemplate);
                        bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);

                        if (bReturnStatus)
                        {
                            tableXDoc.LoadXml(sreturnValue);
                            tableRecordsSet = ConvertXmlDocToDataSet(tableXDoc);
                            if (tableRecordsSet.Tables["option"] != null)
                            {
                                if (TypeCode.Text == "3" || TypeCode.Text == "10") //make table names into links to load codes
                                {
                                    String sTableName = String.Empty;
                                    String sTableARef = String.Empty;
                                    foreach (DataRow row in tableRecordsSet.Tables["option"].Rows)
                                    {
                                        // MITS 15644 MAC - Replacing HTML characters
                                        // -- START --
                                        sTableName = row[1].ToString();
                                        //sTableName = Regex.Replace(sTableName, "&amp;", "&");
                                        //sTableName = Regex.Replace(sTableName, "&apos;", "'");
                                        sTableARef = "<a href='CodeList.aspx?Table=" + row[0] + "' target='frm_Code_Detail' >" + sTableName + "</a>";
                                        row[1] = sTableARef;
                                        // -- END --
                                    }
                                }
                                //bkumarr33: cosmetic Mits 47 ref. Mits 16442
                                //tableList1.DataSource = tableRecordsSet.Tables["option"];
                                //tableList1.DataBind();
                                //end
                                editTableList.DataSource = tableRecordsSet.Tables["option"];
                                editTableList.DataBind();
                            }
                        }
                    }
                }
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch
            //}
        }

        /// <summary>
        /// button to edit table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void editTableList_ItemCommand(object sender, CommandEventArgs e)
        {
            // MITS 15412 MAC - Modified javascript call to use Page.ClientScript.RegisterStartupScript
            // and added javascript function OpenTableDetails() to the js.
            // -- START --
            //start - MITS 15504 - mpalinski
            StringBuilder sbOpenwindow = new StringBuilder();

            sbOpenwindow.Append("<script language='javascript'>");
            sbOpenwindow.AppendFormat(@"OpenTableDetails(" + e.CommandArgument + "," + TypeCode.Text + ");");
            sbOpenwindow.Append("</script>");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenTableDetails", sbOpenwindow.ToString());

            //end - MITS 15504 - mpalinski
            // -- END --
        }

        /// <summary>
        /// XML Template for CWS to get data from DB
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            #region xml template
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization>
                <Call>
                    <Function>CodeUtilityAdaptor.GetXMLData</Function>
                </Call>
                <Document>
					<CodeUtility>
						<FormName></FormName>
						<ID></ID>
						<TypeCode/>
						<Alpha/>
						<XML>
							<control TypeCode='3' detailpage='home?pg=riskmaster/TableMaintenance/CodeList&amp;TypeCode=3' display='swindow' isdetail='' linkpage='home?pg=riskmaster/TableMaintenance/AddEditTable&amp;mode=edit&amp;id=' name='ctnList' tiltle='Code Tables Name' type='treeView'/>
						</XML>
					</CodeUtility>
                </Document>
            </Message>");
            #endregion

            return oTemplate;
        }
    }
}
