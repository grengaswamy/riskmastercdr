﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="CodeDetail.aspx.cs" Inherits="Riskmaster.UI.Utilities.CodeDetail" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Enter Code Detail</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="Stylesheet" href="../../Scripts/zapatec/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/TableMaint.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
    <%--<script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    
        <script type="text/javascript">
            //Start vsharma65 MITS 23567
//            function removeMultiple() {
//                debugger;
//                var str, strlist, count = 0;
//                var x = document.getElementById("e_Org");

//                for (var indx = 0; indx < x.options.length; indx++) {
//                    if (x.options[indx].selected == true) {
//                        count++;
//                    }
//                }
//                if (x.options.length > 0 && count > 0) {
//                    for (var indx = 0; indx < x.options.length; indx++) {
//                        if (x.options[indx].selected == true) {
//                            str = x.options[indx].value.toString();
//                            strlist = document.getElementById("e_Org_lst").value;
//                            document.getElementById("e_Org_lst").value = strlist.replace(str, "");
//                            x.options[indx] = null;
//                            x.removeChild(x.options[indx]);
//                        }
//                    }
//                }
//                else {
//                    alert("Please select Effective for item.");
//                }
//            }
            //End vsharma65 MITS 23567
            //Aman ML Change
            function selectLanguage() {                              
                var x = document.getElementById("language");
                var objCtrl = document.getElementById("LanguageCode");
                var objMode = document.getElementById("Mode");
                if (objMode != null && objMode.value == "new")
                {
                    if ((x != null && x.options[x.selectedIndex].value == "") || (x != null && x.options[x.selectedIndex].value != "1033")) {
                        alert('Please select the base language for the new codes.');
                        return false;
                    }
                }
                else if(objMode != null && objMode.value == "Edit")
                {
                    if (x != null && x.options[x.selectedIndex].value == "") {
                        alert('Please select one of the languages.');
                        return false;
                    }
                }               
                if (objCtrl != null)
                {
                    objCtrl.value = x.options[x.selectedIndex].value;                       
                }
                return true;
            }
            //Aman ML Change
    </script>

    <style>
        .center
        {
            text-align: center;
            float:left;
            width: 100%;
        }
		/* aravi5 Jira Id: 7251 starts */
		#div_lang {
            padding-top: 2px;
        }
        #div_TriggerDates
        {
            padding-top: 2px;
        } 
		/* aravi5 Jira Id: 7251 ends */
    </style>
</head>
<body>
    <form id="frmData" name="frmData" runat="server">
    <div class="headertext3" colSpan="2" width="100%">Enter Code Detail</div>
    <div class="center" colspan="2">
        <asp:Button ID="SaveButton" onclick = "SaveCode" OnClientClick = "if (checkfields('req_fields','','nullcheck','false')) document.forms[0].FlagForRefresh.value='1'; selectLanguage(); return compare_Date(); " runat="server" Text="Save"  class="button" style="width:100"/>  <%--tmalhotra3:MITS 31698 --%>
        <asp:Button ID="CancelButton" runat="server" Text="Cancel"  class="button" style="width:100"/>
        <asp:Button ID="DeleteButton" OnClientClick = "document.forms[0].FlagForRefresh.value='1';" runat="server" onClick="DeleteCode" Text="Delete"  class="button" style="width:100"/>
    </div>
    <table width="100%" cellspacing="0" cellpadding="4" border="0">
        <tr>
            <td colspan="2">
                <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
            </td>
        </tr>
    </table>
    <div runat="server" class="full" id="div_Code">
        <span id="span1" runat="server" class="required">Code</span><span class="formw"><asp:TextBox
            runat="server" size="30" MaxLength="25" ID="Code" onchange="javascript:return datachanged('true');"/>
            <asp:TextBox Style="display: none" runat="server" ID="Code_cid" rmxtype="hidden" />
            <br />
            <asp:LinkButton ID="linkCurrencyCode" Visible="false" runat="server">Select Currency Code</asp:LinkButton>
        </span>
    </div>
    <div runat="server" class="full" id="div_Code_Desc" xmlns="">
        <span class="required">Description</span><span class="formw"><asp:TextBox runat="server"
            size="30" ID="Code_Desc" MaxLength="100" onchange="javascript:return datachanged('true');"/>
        </span>
    </div>
    <div runat="server" class="full" id="div_pCode" xmlns="">
        <span class="label">Parent Code</span><span class="formw">
            <asp:TextBox runat="server" size="30" ID="pCode"  onchange="javascript:return datachanged('true');"/>
            <asp:TextBox Style="display: none" runat="server" ID="pCode_cid" rmxtype="hidden" />
            <asp:Button runat="server" ID="pCodeButton" Text="..." class="EllipsisControl" />
        </span>
    </div>
    <div runat="server" class="full" id="div_Ind_Standard" xmlns="">
        <span class="label">Industry Standard</span><span class="formw"><asp:TextBox runat="server"
            size="30" ID="Ind_Standard" onchange="javascript:return datachanged('true');"/>
            <asp:TextBox Style="display: none" runat="server" ID="Ind_Standard_cid" rmxtype="hidden" />
            <asp:Button runat="server" ID="Ind_StandardButton" Text="..." class="EllipsisControl"/>
        </span>
    </div>
    <div runat="server" class="full" id="div_Line_of_Bus" xmlns="">
        <span class="label">Line Of Business</span><span class="formw">
            <asp:TextBox runat="server" size="30" ID="Line_of_Bus" onchange="javascript:return datachanged('true');"/>
            <asp:TextBox Style="display: none" runat="server" ID="Line_of_Bus_cid" rmxtype="hidden" />
            <asp:Button runat="server" ID="Line_of_BusButton" Text="..." class="EllipsisControl"/>
        </span>
    </div>
    <div runat="server" class="full" id="div_TriggerDates" xmlns="">
        <span class="label">Effective Date Trigger</span><span class="formw">
            <asp:DropDownList ID="trigger_date" runat="server" onchange="javascript:return setchanges('trigger_date');">
            </asp:DropDownList>
        </span>
    </div>
    <div runat="server" class="full" id="div_e_StartDate" xmlns="">
        <span class="label">Effective Start Date</span><span class="formw"><asp:TextBox runat="server"
            size="30" ID="e_StartDate" onchange="javascript:return datachanged('true');" onblur="dateLostFocus('e_StartDate')" />
            <asp:TextBox Style="display: none" runat="server" ID="e_StartDate_cid" rmxtype="hidden" />
            <%--<asp:Button class="DateLookupControl" runat="server" ID="e_StartDateButton" Text="..." />
            <script type="text/javascript">
                Zapatec.Calendar.setup(
                    {
                        inputField: "e_StartDate",
                        ifFormat: "%m/%d/%Y",
                        button: "e_StartDateButton"
                    }
                );
            </script>--%>
            <%if (e_StartDate.Enabled)
              { %>
            <script type="text/javascript">
                $(function () {
                    $("#e_StartDate").datepicker({
                        showOn: "button",
                        buttonImage: "../../Images/calendar.gif",
                       // buttonImageOnly: true,
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        changeYear: true
                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                });
            </script>
            <%} %>
        </span>
    </div>
    <div runat="server" class="full" id="div_e_EndDate" xmlns="">
        <span class="label">Effective End Date</span><span class="formw"><asp:TextBox runat="server"
            size="30" ID="e_EndDate" onchange="javascript:return datachanged('true');" onblur="dateLostFocus('e_EndDate')" />
            <asp:TextBox Style="display: none" runat="server" ID="e_EndDate_cid" rmxtype="hidden" />
            <%--<asp:Button class="DateLookupControl" runat="server" ID="e_EndDateButton" Text="..." />
            <script type="text/javascript">
                Zapatec.Calendar.setup(
                    {
                        inputField: "e_EndDate",
                        ifFormat: "%m/%d/%Y",
                        button: "e_EndDateButton"
                    }
                );
            </script>  --%>          
            <%if (e_EndDate.Enabled)
              { %>      
            <script type="text/javascript">
                $(function () {
                    $("#e_EndDate").datepicker({
                        showOn: "button",
                        buttonImage: "../../Images/calendar.gif",
                        //buttonImageOnly: true,
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        changeYear: true
                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                });
            </script>            
            <%} %>          
        </span>
    </div>
        <div runat="server" class="full" id="div_e_Org" xmlns="">
        <span class="label">Effective For</span><span class="formw">
           <%-- <asp:TextBox runat="server" size="30" ID="e_Org" onchange="javascript:return datachanged('true');"/>--%>
          <%-- <span class="formw">
              <uc:MultiCode runat="server" ID="e_Org" CodeTable="CODES_TEXT" ControlName="e_Org"  RMXType="codelist" Height="50" width="160" />
            </span>--%>
           <%--<asp:ListBox  ID="e_Org"  runat="server"  Width="160px" Height="100px" > </asp:ListBox> 
           <input type="image" name="StatesGrid$Delete" id="StatesGrid_Delete" title="Delete" onclick="removeMultiple()"  src="../../Images/tb_delete_mo.png" style="border-width:0px; width:20px; height:20px" />                     
           <asp:Button runat="server" ID="e_Orgbtn" Text="..." class="EllipsisControl"/>
           <asp:TextBox Style="display: none" runat="server" ID="e_Org_lst" rmxtype="hidden" />
           <asp:TextBox Style="display: none" runat="server" ID="e_Org_cid"  rmxtype="hidden" />--%> 
            
            <asp:TextBox runat="server" size="30" ID="e_Org" onchange="javascript:return datachanged('true');"/>
            <asp:TextBox Style="display: none" runat="server" ID="e_Org_cid" rmxtype="hidden" />
            <asp:Button runat="server" ID="e_Orgbtn" Text="..." class="EllipsisControl"/>
        </span>
    </div>
    <div runat="server" class="full" id="div_lang" xmlns="">
    <span class= "label">Language</span>
    <asp:DropDownList runat="server" rmxref="/Instance/Document/CodeUtility/control[@name='language']/@value" id="language" onchange="setDataChanged(true);" itemsetref="/Instance/Document/CodeUtility/control[ @name = 'language' ]"/>   
    </div>
    <asp:TextBox Style="display: none" runat="server" ID="FormName" rmxref="Instance/Document/CodeUtility/FormName"
        rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="Mode" rmxref="Instance/Document/CodeUtility/Mode"
        rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="TypeCode" rmxref="Instance/Document/CodeUtility/TypeCode"
        Text="3" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="ID" rmxref="Instance/Document/CodeUtility/ID"
        rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="TID" rmxref="" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="Alpha" rmxref="Instance/Document/CodeUtility/Alpha"
        rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="FlagForRefresh" Text="0" rmxtype="hidden" />
       <%-- <asp:textbox style="display: none" runat="server" id="req_fields" Text="Code|Code_Desc" rmxtype="hidden" />--%>
       <asp:textbox style="display: none" text ="" runat="server" id="req_fields"  />
        <asp:textbox style="display: none" runat="server" id="TableId" rmxref="Instance/Document/CodeUtility/InputData/Code/TableId" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="IndStdCode" rmxref="Instance/Document/CodeUtility/InputData/Code/IndStdCode" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="LineOfBusCode" rmxref="Instance/Document/CodeUtility/InputData/Code/LineOfBusCode" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="ShortCode" rmxref="Instance/Document/CodeUtility/InputData/Code/ShortCode" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="CodeDesc" rmxref="Instance/Document/CodeUtility/InputData/Code/CodeDesc" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="RelatedCode" rmxref="Instance/Document/CodeUtility/InputData/Code/RelatedCode" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TriggerDt" rmxref="Instance/Document/CodeUtility/InputData/Code/TriggerDt" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="StartDt" rmxref="Instance/Document/CodeUtility/InputData/Code/StartDt" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="EndDt" rmxref="Instance/Document/CodeUtility/InputData/Code/EndDt" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="OrgId" rmxref="Instance/Document/CodeUtility/InputData/Code/OrgId" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="LanguageCode" rmxref="Instance/Document/CodeUtility/InputData/Code/LanguageCode" Text="" rmxtype="hidden" />   <%-- Aman ML Changes--%>
    </form>
</body>
</html>
