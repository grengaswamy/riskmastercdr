﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Xml;
using System.Xml.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities
{
    public partial class TableDetail : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = String.Empty;
            string typecode = String.Empty;
            string tableID = String.Empty;
            DataSet tableRecordsSet = null;
            XmlDocument codeXDoc = new XmlDocument();
            XElement XmlTemplate = null;

            if (!IsPostBack)
            {
                try
                {
                    //Use values from get unless there are not any then use default values
                    //srajindersin - Pentesting -27683
                    //typecode = Request.QueryString["TypeCode"];
                    if (!string.IsNullOrEmpty(Request.QueryString["TypeCode"]))
                        typecode = AppHelper.HTMLCustomEncode(Request.QueryString["TypeCode"].Replace("<", "").Replace(">", ""));
                        //typecode = Microsoft.Security.Application.Encoder.HtmlEncode(Request.QueryString["TypeCode"].Replace("<", "").Replace(">", ""));
                    //END srajindersin - Pentesting -27683

                    if (!String.IsNullOrEmpty(typecode))
                    {
                        this.TypeCode.Text = typecode;
                    } // if

                    tableID = Request.QueryString["ID"];
                    if (!String.IsNullOrEmpty(tableID))
                    {
                        this.ID.Text = tableID;
                    } // if

                    //make sure required fields are filled in
                    btSaveTable.Attributes.Add("onClick", "checkfields('req_fields','','nullcheck','true');");

                    //close the table detail form
                    btCancel.Attributes.Add("onClick", "javascript:formHandler1('', '', '', 'close', '');");

                    XmlTemplate = GetMessageTemplate();
                    CallCWSFunction("CodeUtilityAdaptor.GetXMLData", out sreturnValue, XmlTemplate);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);

                    if (bReturnStatus)
                    {
                        codeXDoc.LoadXml(sreturnValue);
                        tableRecordsSet = ConvertXmlDocToDataSet(codeXDoc);

                        if (tableRecordsSet.Tables["control"] != null)
                        {
                            foreach (DataRow row in tableRecordsSet.Tables["control"].Rows)
                            {
                                foreach (Control oCtrl in this.Form.Controls)
                                {
                                    foreach (Control oCtrl2 in oCtrl.Controls)
                                    {
                                        if (oCtrl2.ID == row["name"].ToString())
                                        {
                                            switch (oCtrl2.ID)
                                            {
                                                case "stable": //System Table Name
                                                    {
                                                        stable.Text = Server.HtmlDecode(row["value"].ToString());
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            stable.Enabled = true;
                                                        else
                                                            stable.Enabled = false;
                                                        break;
                                                    }
                                                case "utable": //User Table Name
                                                    {
                                                        utable.Text = Server.HtmlDecode(row["value"].ToString());
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            utable.Enabled = true;
                                                        else
                                                            utable.Enabled = false;
                                                        break;
                                                    }
                                                case "pTable": //Parent Name
                                                    {
                                                        //pTable.DataSource = tableRecordsSet.Tables["pTable"];
                                                        //pTable.DataValueField = "tableid";
                                                        //pTable.DataTextField = "name";
                                                        //pTable.DataBind();
                                                        pTable.Items.Insert(0, String.Empty);

                                                        //start - MITS 15504 - mpalinski
                                                        if (tableRecordsSet.Tables["pTable"] != null)
                                                        {
                                                            ListItem item = null;
                                                            foreach (DataRow row2 in tableRecordsSet.Tables["pTable"].Rows)
                                                            {
                                                                item = new ListItem();
                                                                item.Text = Server.HtmlDecode(row2["name"].ToString());
                                                                item.Value = row2["tableid"].ToString();
                                                                pTable.Items.Add(item);
                                                                if (tableRecordsSet.Tables["pTable"].Columns["selected"] != null)
                                                                {
                                                                    if (row2["selected"].ToString() != "0")
                                                                    {
                                                                        pTable.SelectedValue = row2["tableid"].ToString();
                                                                    }
                                                                }
                                                            }

                                                        } // if
                                                        //end - MITS 15504 - mpalinski

                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            pTable.Enabled = true;
                                                        else
                                                            pTable.Enabled = false;
                                                        break;
                                                    }
                                                case "pTable_Required": //Parent Required flag
                                                    {
                                                        if (row["value"].ToString() == "0" || row["value"].ToString() == "")
                                                            pTable_Required.Checked = false;
                                                        else
                                                            pTable_Required.Checked = true;
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            pTable_Required.Enabled = true;
                                                        else
                                                            pTable_Required.Enabled = false;
                                                        break;
                                                    }
                                                case "ind_stand_table": //Industry Standard Table
                                                    {
                                                        ind_stand_table.DataSource = tableRecordsSet.Tables["Ind_Tables"];
                                                        ind_stand_table.DataValueField = "tableid";
                                                        ind_stand_table.DataTextField = "name";
                                                        ind_stand_table.DataBind();
                                                        ind_stand_table.Items.Insert(0, String.Empty);

                                                        //start - MITS 15504 - mpalinski
                                                        if (tableRecordsSet.Tables["Ind_Tables"] != null)
                                                        {
                                                            if (tableRecordsSet.Tables["Ind_Tables"].Columns["selected"] != null)
                                                            {
                                                                foreach (DataRow row2 in tableRecordsSet.Tables["Ind_Tables"].Rows)
                                                                {
                                                                    if (row2["selected"].ToString() != "0")
                                                                    {
                                                                        ind_stand_table.SelectedValue = row2["tableid"].ToString();
                                                                    }
                                                                }
                                                            }
                                                        } // if
                                                        //end - MITS 15504 - mpalinski

                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            ind_stand_table.Enabled = true;
                                                        else
                                                            ind_stand_table.Enabled = false;
                                                        break;
                                                    }
                                                case "Ind_Tables_Required": //Industry Standard Required flag
                                                    {
                                                        if (row["value"].ToString() == "0" || row["value"].ToString() == "")
                                                            Ind_Tables_Required.Checked = false;
                                                        else
                                                            Ind_Tables_Required.Checked = true;
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            Ind_Tables_Required.Enabled = true;
                                                        else
                                                            Ind_Tables_Required.Enabled = false;
                                                        break;
                                                    }
                                                case "allow_attach": //Allow Attachment flag
                                                    {
                                                        if (row["value"].ToString() == "0" || row["value"].ToString() == "")
                                                            allow_attach.Checked = false;
                                                        else
                                                            allow_attach.Checked = true;
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            allow_attach.Enabled = true;
                                                        else
                                                            allow_attach.Enabled = false;
                                                        break;
                                                    }
                                                case "tree_disp": //Tree Display flag
                                                    {
                                                        if (row["value"].ToString() == "0" || row["value"].ToString() == "")
                                                            tree_disp.Checked = false;
                                                        else
                                                            tree_disp.Checked = true;
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            tree_disp.Enabled = true;
                                                        else
                                                            tree_disp.Enabled = false;
                                                        break;
                                                    }
                                                case "line_of_bus": //Line of Business Required flag
                                                    {
                                                        if (row["value"].ToString() == "0" || row["value"].ToString() == "")
                                                            line_of_bus.Checked = false;
                                                        else
                                                            line_of_bus.Checked = true;
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            line_of_bus.Enabled = true;
                                                        else
                                                            line_of_bus.Enabled = false;
                                                        break;
                                                    }
                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch
            }
        }

        /// <summary>
        /// XML template for CWS call to get data from DB
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            string mode = String.Empty;

            //Use values from get unless there are not any then use default values
            mode = Request.QueryString["Mode"];
            if (!String.IsNullOrEmpty(mode))
            {
                this.Mode.Text = mode;
            } // if

            if (mode == "new")
            {
                this.FormName.Text = "frmaddtable";
            }
            else if (mode == "edit")
            {
                this.FormName.Text = "frmedittable";
            }

            #region xml template
            XElement oTemplate = XElement.Parse(@"
            <Message>
	            <Authorization>a040d3a6-650f-413d-b206-22a0407d6075</Authorization>
	            <Call>
		            <Function>CodeUtilityAdaptor.GetXMLData</Function>
	            </Call>
	            <Document>
		            <CodeUtility>
			            <FormName></FormName>
			            <Mode></Mode>
			            <TypeCode></TypeCode>
			            <ID></ID>
			            <Alpha/>
			            <XML>
				            <form actionname='riskmaster/TableMaintenance/AddEditTable&amp;mode=new' bcolor='white' name='tabledetail' sid='150' title='Enter Table Detail' topbuttons='1'>
					            <body1 func='return checkFlag();' req_func='yes'/>
					            <group name='table_detail' title='Table Detail'>
						            <error desc='' value='0'/>
						            <control isrequ='yes' lock='false' name='stable' title='System Table Name' type='text' value=''/>
						            <control isrequ='yes' lock='false' name='utable' title='User Table Name' type='text' value=''/>
						            <control lock='' name='pTable' optionname='pTable' title='Parent Name' type='combobox'/>
						            <control lock='false' name='pTable_Required' title='Parent Table Required' type='checkbox' value='0'/>
						            <control name='ind_stand_table' optionname='Ind_Tables' title='Industry Standard Table' type='combobox'/>
						            <control lock='false' name='Ind_Tables_Required' title='Industry Standard Table Required' type='checkbox' value='0'/>
						            <control codetable='organization' lock='false' name='allow_attach' title='Allow Attachment' type='checkbox' value='0'/>
						            <control lock='false' name='tree_disp' title='Tree Display' type='checkbox' value='0'/>
						            <control lock='false' name='line_of_bus' title='Line Of Busines Required' type='checkbox' value='0'/>
						            <control name='TypeCode' type='id' value=''/>
						            <control name='req_fields' type='id' value='stable|utable'/>
						            <control name='FlagForRefresh' type='id' value=''/>
					            </group>
					            <button disable='' linkto='' name='btnsave' param='' title='Save' type='save' windowClose='true'/>
					            <button disable='' name='btncancel' title='Cancel' type='close'/>
				            </form>
			            </XML>
		            </CodeUtility>
	            </Document>
            </Message>");
            #endregion

            return oTemplate;
        }

        /// <summary>
        /// XML template for CWS call to save data to DB
        /// </summary>
        /// <returns></returns>
        private XElement SaveMessageTemplate()
        {
            StringBuilder sbXmlTemplate = new StringBuilder();
            XElement oTemplate = null;

            TableId.Text = ID.Text;
            TypeCode2.Text = TypeCode.Text;
            SysTableName.Text = stable.Text;
            TableName.Text = utable.Text;

            // MITS 15644 - MAC : Added code to escape the apostrophy
            // -- START --

            //SysTableName.Text = SecurityElement.Escape(stable.Text);
            //TableName.Text = SecurityElement.Escape(utable.Text);
            // -- END --

            RelatedId.Text = pTable.Text;

            if (pTable_Required.Checked)
                RelTableFlag.Text = "1";
            else
            {
                RelTableFlag.Text = String.Empty;
            }
            IndTableId.Text = ind_stand_table.Text;
            if (Ind_Tables_Required.Checked)
                ReqIndTable.Text = "1";
            else
            {
                ReqIndTable.Text = String.Empty;
            }
            if (allow_attach.Checked)
                AttchFlag.Text = "1";
            else
            {
                AttchFlag.Text = String.Empty;
            }
            if (tree_disp.Checked)
                TreeFlag.Text = "1";
            else
            {
                TreeFlag.Text = String.Empty;
            }
            if (line_of_bus.Checked)
                LineOfBusFlag.Text = "1";
            else
            {
                LineOfBusFlag.Text = String.Empty;
            }

            #region xml template
            sbXmlTemplate.Append(@"
			<Message>
				<Authorization>33e9d119-b761-4795-a405-41f28fe12bbb</Authorization>
				<Call>
					<Function>CodeUtilityAdaptor.SaveTable</Function>
				</Call>
				<Document>
					<CodeUtility>
						<Mode>edit</Mode>
						<InputData>
							<Table>
								<TableId></TableId>
								<UserId></UserId>");
            sbXmlTemplate.AppendFormat(@"<TypeCode>{0}</TypeCode>", TypeCode.Text);//MITS 15504
            sbXmlTemplate.Append(@"<SysTableName>ACCIDENT_TYPE</SysTableName>
								<TableName>Accident Type3</TableName>
								<RelatedId>1003</RelatedId>
								<RelTableFlag/>
								<IndTableId/>
								<AttchFlag/>
								<TreeFlag/>
								<LineOfBusFlag/>
								<ReqIndTable/>
								<DeletedFlag>0</DeletedFlag>
							</Table>
						</InputData>
						<XmlDoc>
							<form actionname='edittable.asp?mode=new' bcolor='white' name='tabledetail' sid='150' title='Enter Table Detail' topbuttons='1'>
								<body1 func='' req_func='yes'/>
								<group name='table_detail' title='Table Detail'>
									<error desc='' value='0'/>
									<control isrequ='yes' lock='false' name='stable' title='System Table Name' type='text' value=''/>
									<control isrequ='yes' lock='false' name='utable' title='User Table Name' type='text' value=''/>
									<control lock='' name='ptable' optionname='p_tables' title='Parent Name' type='combobox'/>
									<control lock='false' name='ptable_required' title='Parent Table Required' type='checkbox' value='0'/>
									<control name='ind_stand_table' optionname='ind_tables' title='Industry Standard Table' type='combobox'/>
									<control name='action' type='id' value=''/>
									<control name='table' type='id' value=''/>
									<control name='TypeCode' type='id' value=''/>
									<control name='req_fields' type='id' value='stable|utable'/>
									<control codetable='organization' lock='false' name='allow_attach' title='Allow Attachment' type='checkbox' value='0'/>
									<control lock='false' name='tree_disp' title='Tree Display' type='checkbox' value='0'/>
									<control lock='false' name='line_of_bus' title='Line Of Busines Required' type='checkbox' value='0'/>
								</group>
								<button disable='' linkto='home?pg=riskmaster/TableMaintenance/AddEditTable&amp;mode=edit' name='btnsave' param='' title='Save' type='save'/>
								<button disable='' name='btncancel' title='Cancel' type='close'/>
							</form>
						</XmlDoc>
					</CodeUtility>
				</Document>
			</Message>");
            #endregion

            oTemplate = XElement.Parse(sbXmlTemplate.ToString());

            return oTemplate;
        }

        public void SaveTable(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                string sreturnValue = String.Empty;
                XElement XmlTemplate = null;

                if (FlagForRefresh.Text != "0")
                {
                    XmlTemplate = SaveMessageTemplate();
                    CallCWSFunction("CodeUtilityAdaptor.SaveTable", out sreturnValue, XmlTemplate);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);

                    if (bReturnStatus)
                    {
                        if (FlagForRefresh.Text != "0")
                        {
                            //reload the table list in the left frame
                            //srajindersin - Pentesting - 27683
                            //ClientScript.RegisterStartupScript(typeof(Page), "tableMaint", "<script>formHandler1('TableList.aspx', '?TypeCode=" + TypeCode.Text + "', '', 'parentTable', '');</script>");//MITS 15504
                            //ClientScript.RegisterStartupScript(typeof(Page), "tableMaint", "<script>formHandler1('TableList.aspx', '?TypeCode=" + Microsoft.Security.Application.Encoder.HtmlEncode(TypeCode.Text.Replace("<", "").Replace(">", "")) + "', '', 'parentTable', '');</script>");//MITS 15504
                            ClientScript.RegisterStartupScript(typeof(Page), "tableMaint", "<script>formHandler1('TableList.aspx', '?TypeCode=" + AppHelper.HTMLCustomEncode(TypeCode.Text.Replace("<", "").Replace(">", "")) + "', '', 'parentTable', '');</script>");//MITS 15504
                            //END srajindersin - Pentesting - 27683

                            //close table detail page
                            ClientScript.RegisterStartupScript(typeof(Page), "tableMaint", "<script>formHandler1('', '', '', 'close', '');</script>");
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        }

        protected void Cancel(object sender, EventArgs e)
        {
        }
    }
}