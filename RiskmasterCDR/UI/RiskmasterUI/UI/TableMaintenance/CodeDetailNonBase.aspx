﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CodeDetailNonBase.aspx.cs" Inherits="Riskmaster.UI.Utilities.CodeDetailNonBase" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Enter Code Detail</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="Stylesheet" href="../../Scripts/zapatec/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/TableMaint.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>   
    <script type="text/javascript">        
        function setLanguageCode() {         
            var x = document.getElementById("language");
            var objCtrl = document.getElementById("LanguageCode");
            var objMode = document.getElementById("Mode");
            if (objMode != null && objMode.value == "newNonBase") {
                if ((x != null && x.options[x.selectedIndex].value == "")) {
                    alert('Please select one of the languages.');
                    return false;
                }
            }
            if (objCtrl != null) {
                objCtrl.value = x.options[x.selectedIndex].value;
            }
            return true;
        }        
    </script>
    <style>
        .center
        {
            text-align: center;
            float:left;
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="frmData" name="frmData" runat="server">
    <div class="headertext3" colSpan="2" width="100%">Enter Code Detail</div>
    <div class="center" colspan="2">
        <asp:Button ID="SaveButton" onclick = "SaveCode" OnClientClick = "if (checkfields('req_fields','','nullcheck','false')) document.forms[0].FlagForRefresh.value='1'; return setLanguageCode();" runat="server" Text="Save"  class="button" style="width:100"/>  
        <asp:Button ID="CancelButton" runat="server" Text="Cancel"  class="button" style="width:100"/>
        <asp:Button ID="DeleteButton" OnClientClick = "document.forms[0].FlagForRefresh.value='1';" runat="server" onClick="DeleteCode" Text="Delete"  class="button" style="width:100"/>
    </div>
    <table width="100%" cellspacing="0" cellpadding="4" border="0">
        <tr>
            <td colspan="2">
                <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
            </td>
        </tr>
    </table>
    <div runat="server" class="full" id="div_Code">
        <span id="span1" runat="server" class="required">Code</span><span class="formw"><asp:TextBox
            runat="server" size="30" ID="Code" />
            <asp:TextBox Style="display: none" runat="server" ID="Code_cid" rmxtype="hidden" />            
        </span>
    </div>
    <div runat="server" class="full" id="div_Code_Desc" xmlns="">
        <span class="required">Description</span><span class="formw"><asp:TextBox runat="server"
            size="30" ID="Code_Desc" onchange="javascript:return datachanged('true');"/>
        </span>
    </div>
     
    <div runat="server" class="full" id="div_lang" xmlns="">
      <span class= "label">Language</span>
         <asp:DropDownList runat="server" rmxref="/Instance/Document/CodeUtility/control[@name='language']/@value" id="language" onchange="setDataChanged(true);" itemsetref="/Instance/Document/CodeUtility/control[ @name = 'language' ]"/>   
    </div>
    <asp:TextBox Style="display: none" runat="server" ID="FormName" rmxref="Instance/Document/CodeUtility/FormName"
        rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="Mode" rmxref="Instance/Document/CodeUtility/Mode"
        rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="TypeCode" rmxref="Instance/Document/CodeUtility/TypeCode"
        Text="3" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="ID" rmxref="Instance/Document/CodeUtility/InputData/Code/ID"
        rmxtype="hidden" />
        <asp:TextBox Style="display: none" runat="server" ID="alpha" rmxref="Instance/Document/CodeUtility/Alpha"
        rmxtype="hidden" />
        <asp:TextBox Style="display: none" runat="server" ID="TID" rmxref="" rmxtype="hidden" />  
        <asp:textbox style="display: none" runat="server" id="FlagForRefresh" Text="0" rmxtype="hidden" />     
        <asp:textbox style="display: none" text ="" runat="server" id="req_fields"  />
        <asp:textbox style="display: none" runat="server" id="TableId" rmxref="Instance/Document/CodeUtility/InputData/Code/TableId" Text="" rmxtype="hidden" />       
        <asp:textbox style="display: none" runat="server" id="ShortCode" rmxref="Instance/Document/CodeUtility/InputData/Code/ShortCode" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="CodeDesc" rmxref="Instance/Document/CodeUtility/InputData/Code/CodeDesc" Text="" rmxtype="hidden" />              
        <asp:textbox style="display: none" runat="server" id="LanguageCode" rmxref="Instance/Document/CodeUtility/InputData/Code/LanguageCode" Text="" rmxtype="hidden" />   <%-- Aman ML Changes--%>
        <asp:TextBox Style="display: none" runat="server" ID="LangCode" rmxref="Instance/Document/CodeUtility/LangCode"
        rmxtype="hidden" />
    </form>
</body>
</html>
