﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace Riskmaster.UI.NonFDMCWS
{
    public partial class ProgressNotesEditPage : NonFDMBasePageCWS
    {
        /// <summary>
        /// Returns the PDF for the specified EventID or ClaimID or AddTracking
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <author>Rahul Solanki</author>        
        protected void Page_Load(object sender, EventArgs e)
        {
            string sAdaptorName;
            TextBox txtRecordID;
            
            

            try
            {
                sAdaptorName = "ProgressNotesAdaptor.SelectClaim";
                txtRecordID = (TextBox)this.Form.FindControl("NoteMemo");
                WebEditorComments.Text = txtRecordID.Text;
                
                
                //WebEditorComments.Text = "asd";
                
                //string sRecordID = Request.QueryString["recid"].ToString();
                
                //if (sRecordID.Length==0)
                //{
                //    throw new Exception("record id missing from querystring.");
                //}
                //// ? error to be displayed in red here or propogated above ???
                           
                //string sReportType = Request.QueryString["type"].ToString().ToUpper();
                //if (sReportType=="EVENT")
                //{
                //    sAdaptorName = "ExecutiveSummaryAdaptor.EventExecSumm";
                //    txtRecordID = (TextBox)this.Form.FindControl("eventid");
                //}
                //else if (sReportType == "CLAIM")
                //{
                //    sAdaptorName = "ExecutiveSummaryAdaptor.ClaimExecSumm";
                //    txtRecordID = (TextBox)this.Form.FindControl("claimid");
                //}
                //else if (sReportType == "ADMTRACKING")
                //{
                //    sAdaptorName = "ExecutiveSummaryAdaptor.AdmTrackingExecSumm";
                //    txtRecordID = (TextBox)this.Form.FindControl("recordid");
                //}
                //else
                //{
                //    throw new Exception("report type missing from querystring.");
                //}

                //txtRecordID.Text = sRecordID;

                if (!IsPostBack)
                {
                    NonFDMCWSPageLoad(sAdaptorName);
                }
                //TextBox txtPdfFile = (TextBox)this.Form.FindControl("File");
                                
                //byte[] pdfbytes = Convert.FromBase64String(txtPdfFile.Text);

                //Response.Buffer = true;
                //Response.Clear();
                //Response.ClearContent();
                //Response.ClearHeaders();
                //Response.Charset = "";
                //Response.AppendHeader("Content-Encoding", "none;");
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", string.Format("attachment; filename=ExecutiveSummary_{0}_{1}.PDF", sReportType, sRecordID));
                //Response.AddHeader("Accept-Ranges", "bytes");
                //Response.BinaryWrite(pdfbytes);
                //Response.End();
            }
            catch (Exception ex)
            {
                //error.Text = "ERROR: " + ex.Message.ToString();
                throw new Exception("EnhancedNotes Add/edit Error", ex.GetBaseException());
            }


        }
      


    }
}
