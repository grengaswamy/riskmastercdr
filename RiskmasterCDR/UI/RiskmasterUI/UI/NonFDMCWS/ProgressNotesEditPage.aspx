﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProgressNotesEditPage.aspx"
    Inherits="Riskmaster.UI.NonFDMCWS.ProgressNotesEditPage" EnableViewState="false" %>

<%@ Register Assembly="Infragistics2.WebUI.WebHtmlEditor.v8.1, Version=8.1.20081.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebHtmlEditor" TagPrefix="ighedit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Create/Edit Enhanced Notes</title>
</head>
<body>

    <script language="javascript">
//function WebEditorComments_ToolbarClick()
//{
//    alert ('hi' );
//}
    </script>

    <form id="form1" runat="server">
    <div>
        <%--color to be made red here--%>
        <%--<asp:textbox style="display: none;" runat="server" id="error" rmxref="Instance/Document/ExecutiveSummaryReport/EventId" Text="15" rmxtype="hidden" />--%>
    </div>
    <asp:TextBox Style="display: none;" runat="server" ID="eventid" rmxref="Instance/Document/ClaimProgressNotes/EventID"
        Text="15" rmxtype="hidden" />
    <asp:TextBox Style="display: none;" runat="server" ID="claimid" rmxref="Instance/Document/ClaimProgressNotes/ClaimID"
        Text="15" rmxtype="hidden" />
    <!--	<asp:textbox style="display: none;" runat="server" id="recordid" rmxref="Instance/Document/ClaimProgressNotes/RecordId" Text="15" rmxtype="hidden" />-->
    <!--	<asp:textbox style="display: none;" runat="server" id="File" rmxref="Instance/Document/ClaimProgressNotes/File" Text=""
 rmxtype="hidden" />	-->
    <asp:TextBox Style="display: none;" runat="server" ID="DateEntered" rmxref="Instance/Document/ClaimProgressNotes/DateEntered"
        Text="15" rmxtype="hidden" />
    <asp:TextBox Style="display: none;" runat="server" ID="NoteMemo" rmxref="Instance/Document/ClaimProgressNotes/NoteMemo"
        Text="15" rmxtype="hidden" />
    <asp:TextBox Style="display: none;" runat="server" ID="notememocaretech" rmxref="Instance/Document/ClaimProgressNotes/notememocaretech"
        Text="15" rmxtype="hidden" />
    <asp:TextBox Style="display: none;" runat="server" ID="notetypecode" rmxref="Instance/Document/ClaimProgressNotes/notetypecode"
        Text="15" rmxtype="hidden" />
    <asp:TextBox Style="display: none;" runat="server" ID="claimprogressnoteid" rmxref="Instance/Document/ClaimProgressNotes/claimprogressnoteid"
        Text="15" rmxtype="hidden" />
 <%--   <claimid>0</claimid>
    <eventid>74</eventid>
    <dateentered />
    <newrecord>false</newrecord>
    <notememo>10/16/2008 2:12:28 PM (csc)dadsasd<br /> <br /> 10/16/2008 2:19:04 PM (csc)gfhfgfh</notememo>
    <notememocaretech>10/16/2008 2:12:28 PM (csc)dadsasd 10/16/2008 2:19:04 PM (csc)gfhfgfh</notememocaretech>
    <notetypecode>5095</notetypecode>
    <claimprogressnoteid>34</claimprogressnoteid>--%>
    <asp:TextBox Style="display: none;" runat="server" ID="NewRecord" rmxref="Instance/Document/ClaimProgressNotes/NewRecord"
        Text="false" rmxtype="hidden" />
    <table>
        <tr>
            <td colspan="2">
                <ighedit:WebHtmlEditor ID="WebEditorComments" runat="server" Height="306px" Width="734px">
                    <%--ontoolbarclick="WebEditorComments_ToolbarClick"--%>
                    <%-- <RightClickMenu>
                        <ighedit:HtmlBoxMenuItem runat="server" Act="Cut">
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem runat="server" Act="Copy">
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem runat="server" Act="Paste">
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem runat="server" Act="PasteHtml">
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem runat="server" Act="CellProperties">
                            <Dialog InternalDialogType="CellProperties"></Dialog>
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem runat="server" Act="TableProperties">
                            <Dialog InternalDialogType="ModifyTable"></Dialog>
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem runat="server" Act="InsertImage">
                        </ighedit:HtmlBoxMenuItem>
                    </RightClickMenu>
                    <Toolbar>
                        <ighedit:ToolbarImage runat="server" Type="DoubleSeparator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton runat="server" Type="Bold"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="Italic"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="Underline"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="Strikethrough"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton runat="server" Type="Subscript"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="Superscript"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton runat="server" Type="Cut"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="Copy"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="Paste"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton runat="server" Type="Undo"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="Redo"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton runat="server" Type="JustifyLeft"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="JustifyCenter"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="JustifyRight"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="JustifyFull"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton runat="server" Type="Indent"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="Outdent"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton runat="server" Type="UnorderedList"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="OrderedList"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarDialogButton runat="server" Type="InsertRule">
                            <Dialog InternalDialogType="InsertRule"></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarImage runat="server" Type="RowSeparator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarImage runat="server" Type="DoubleSeparator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarDialogButton runat="server" Type="FontColor">
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarDialogButton runat="server" Type="FontHighlight">
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarDialogButton runat="server" Type="SpecialCharacter">
                            <Dialog InternalDialogType="SpecialCharacterPicker" Type="InternalWindow"></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarMenuButton runat="server" Type="InsertTable">
                            <Menu>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="TableProperties">
                                    <Dialog InternalDialogType="InsertTable"></Dialog>
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="InsertColumnRight">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="InsertColumnLeft">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="InsertRowAbove">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="InsertRowBelow">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="DeleteRow">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="DeleteColumn">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="IncreaseColspan">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="DecreaseColspan">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="IncreaseRowspan">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="DecreaseRowspan">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="CellProperties">
                                    <Dialog InternalDialogType="CellProperties"></Dialog>
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="TableProperties">
                                    <Dialog InternalDialogType="ModifyTable"></Dialog>
                                </ighedit:HtmlBoxMenuItem>
                            </Menu>
                        </ighedit:ToolbarMenuButton>
                        <ighedit:ToolbarButton runat="server" Type="ToggleBorders"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton runat="server" Type="InsertLink"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="RemoveLink"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton runat="server" Type="Save" RaisePostback="True"></ighedit:ToolbarButton>
                        <ighedit:ToolbarUploadButton runat="server" Type="Open">
                            <Upload Mode="File" Filter="*.htm,*.html,*.asp,*.aspx" Height="350px" Width="500px">
                            </Upload>
                        </ighedit:ToolbarUploadButton>
                        <ighedit:ToolbarButton runat="server" Type="Preview"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarDialogButton runat="server" Type="FindReplace">
                            <Dialog InternalDialogType="FindReplace"></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarDialogButton runat="server" Type="InsertBookmark">
                            <Dialog InternalDialogType="InsertBookmark"></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarUploadButton runat="server" Type="InsertImage">
                            <Upload Height="420px" Width="500px"></Upload>
                        </ighedit:ToolbarUploadButton>
                        <ighedit:ToolbarUploadButton runat="server" Type="InsertFlash">
                            <Upload Mode="Flash" Filter="*.swf" Height="440px" Width="500px"></Upload>
                        </ighedit:ToolbarUploadButton>
                        <ighedit:ToolbarUploadButton runat="server" Type="InsertWindowsMedia">
                            <Upload Mode="WindowsMedia" Filter="*.asf,*.wma,*.wmv,*.wm,*.avi,*.mpg,*.mpeg,*.m1v,*.mp2,*.mp3,*.mpa,*.mpe,*.mpv2,*.m3u,*.mid,*.midi,*.rmi,*.aif,*.aifc,*.aiff,*.au,*.snd,*.wav,*.cda,*.ivf"
                                Height="400px" Width="500px"></Upload>
                        </ighedit:ToolbarUploadButton>
                        <ighedit:ToolbarDialogButton runat="server" Type="Help">
                            <Dialog InternalDialogType="Text"></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarButton runat="server" Type="CleanWord"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="WordCount"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="PasteHtml"></ighedit:ToolbarButton>
                        <ighedit:ToolbarMenuButton runat="server" Type="Zoom">
                            <Menu>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="Zoom25">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="Zoom50">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="Zoom75">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="Zoom100">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="Zoom200">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="Zoom300">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="Zoom400">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="Zoom500">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem runat="server" Act="Zoom600">
                                </ighedit:HtmlBoxMenuItem>
                            </Menu>
                        </ighedit:ToolbarMenuButton>
                        <ighedit:ToolbarButton runat="server" Type="TogglePositioning"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="BringForward"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton runat="server" Type="SendBackward"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage runat="server" Type="RowSeparator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarImage runat="server" Type="DoubleSeparator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarDropDown runat="server" Type="FontName">
                        </ighedit:ToolbarDropDown>
                        <ighedit:ToolbarDropDown runat="server" Type="FontSize">
                        </ighedit:ToolbarDropDown>
                        <ighedit:ToolbarDropDown runat="server" Type="FontFormatting">
                        </ighedit:ToolbarDropDown>
                        <ighedit:ToolbarDropDown runat="server" Type="FontStyle">
                        </ighedit:ToolbarDropDown>
                        <ighedit:ToolbarDropDown runat="server" Type="Insert">
                            <Items>
                                <ighedit:ToolbarDropDownItem runat="server" Act="Greeting"></ighedit:ToolbarDropDownItem>
                                <ighedit:ToolbarDropDownItem runat="server" Act="Signature"></ighedit:ToolbarDropDownItem>
                            </Items>
                        </ighedit:ToolbarDropDown>
                    </Toolbar>--%>
                </ighedit:WebHtmlEditor>
            </td>
        </tr>
        <tr>
            <td>
                Note Type:<input value="code type" />
            </td>
            <td>
                Activity Date:<input type="text"  />
            </td>
        </tr>
        <tr>
            <td>
                Note attached to:<input type="text" />
            </td>
        </tr>
        <tr>
            <td colspan="2"><center>
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" /></center>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    </form>
</body>
</html>
