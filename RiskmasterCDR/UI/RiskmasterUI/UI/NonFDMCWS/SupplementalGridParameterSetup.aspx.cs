﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.UI.Shared;

namespace Riskmaster.UI.Utilities
{
    public partial class SupplementalGridParameterSetup : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                NonFDMCWSPageLoad("SupplementalGridParameterSetupAdaptor.Get");
            }
        }
        protected void Save(object sender, EventArgs e)
        {
           bool bReturnStatus = false;
           bReturnStatus = CallCWSFunction("SupplementalGridParameterSetupAdaptor.Save");
            if(bReturnStatus)
                bReturnStatus = CallCWSFunction("SupplementalGridParameterSetupAdaptor.Get");

        }
    }
}
