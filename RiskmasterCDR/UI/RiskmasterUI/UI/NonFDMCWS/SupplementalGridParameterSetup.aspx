﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SupplementalGridParameterSetup.aspx.cs" Inherits="Riskmaster.UI.Utilities.SupplementalGridParameterSetup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Grid Properties</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/system.css" type="text/css"/>
  <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script><script type="text/javascript" language="JavaScript">
          var m_DataChanged=false;
          function Increment(m_FormID)
          {
          m_DataChanged = true;
          var objElem = eval('document.frmData.'+ m_FormID);
          if(objElem != null)
          if(objElem.value<99)
          objElem.value++;

          }
          function Decrement(m_FormID)
          {
          m_DataChanged = true;
          var objElem = eval('document.frmData.'+ m_FormID);
          if(objElem != null)
          if(objElem.value>1)
          objElem.value--;
          }
          function ClickNotAllowed(m_FormID)
          {
          alert("You cannot type in the textbox. To edit the value click on moveup/down arrows");
          var objElem = eval('document.frmData.'+ m_FormID);
          if(objElem != null)
          objElem.blur();
          return;
          }

          function Save()
          {

          if(!m_DataChanged)
          return false;
          if( Number(document.frmData.minRows.value) > Number(document.frmData.maxRows.value))
          {
          alert("Minimum rows allowed cannot exceed Maximum rows allowed!");
          return false;
          }
          if( Number(document.frmData.minCols.value) > Number(document.frmData.maxCols.value))
          {
          alert("Minimum columns allowed cannot exceed Maximum columns allowed!");
          return false;
          }
//          document.frmData.hdnaction.value = 'Save';
//          document.frmData.submit();
          return true;
          }
          function PageLoad()
          {
          }
        </script>
</head>
<body>
    <form id="frmData" runat="server">
   <table border="0">
   <br><br><br><br>
    <tr>
     <td height="32"><asp:imagebutton id="btnSave" runat="server" class="bold" onclientclick="return Save();" onclick="Save" ImageUrl = "../../Images/save.gif" ToolTip="Save"/></td>
    </tr>
    <tr>
     <td class="msgheader" colspan="5">Grid Parameters</td>
    </tr>
    <tr>
     <td>
      Minimum Rows Allowed
      
     </td>
     <td><asp:textbox runat="server" style="background-color: silver" id="minRows" onfocus="javascript:ClickNotAllowed('minRows')" rmxref="/Instance/Document/GridParameters/MinRows"/></td>
     <td><a class="bold" href="javascript:Increment('minRows')"><img src="../../Images/up.gif" width="20" height="20" border="0" alt="Up" title="Move Up"/></a><a class="bold" href="javascript:Decrement('minRows')"><img src="../../Images/down.gif" width="20" height="20" border="0" alt="Down" title="Move Down"/></a></td>
     <td>
      Maximum Rows Allowed
      
     </td>
     <td><asp:textbox runat="server" id="maxRows" style="background-color: silver" onfocus="javascript:ClickNotAllowed('maxRows')" rmxref="/Instance/Document/GridParameters/MaxRows"/></td>
     <td><a class="bold" href="javascript:Increment('maxRows')"><img src="../../Images/up.gif" width="20" height="20" border="0" alt="Up" title="Move Up"/></a><a class="bold" href="javascript:Decrement('maxRows')"><img src="../../Images/down.gif" width="20" height="20" border="0" alt="Down" title="Move Down"/></a></td>
    </tr>
    <tr>
     <td>
      Minimum Columns Allowed
      
     </td>
     <td><asp:textbox runat="server" id="minCols" style="background-color: silver" onfocus="javascript:ClickNotAllowed('minCols')" rmxref="/Instance/Document/GridParameters/MinCols"/></td>
     <td><a class="bold" href="javascript:Increment('minCols')"><img src="../../Images/up.gif" width="20" height="20" border="0" alt="Up" title="Move Up"/></a><a class="bold" href="javascript:Decrement('minCols')"><img src="../../Images/down.gif" width="20" height="20" border="0" alt="Down" title="Move Down"/></a></td>
     <td>
      Maximum Columns Allowed
      
     </td>
     <td><asp:textbox runat="server" id="maxCols" style="background-color: silver" onfocus="javascript:ClickNotAllowed('maxCols')" rmxref="/Instance/Document/GridParameters/MaxCols"/></td>
     <td><a class="bold" href="javascript:Increment('maxCols')"><img src="../../Images/up.gif" width="20" height="20" border="0" alt="Up" title="Move Up"/></a><a class="bold" href="javascript:Decrement('maxCols')"><img src="../../Images/down.gif" width="20" height="20" border="0" alt="Down" title="Move Down"/></a></td>
    </tr>
    <tr>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
     <td><input type="hidden" runat="server" id="hdnaction"/></td>
     <td></td>
    </tr>
   </table>
    </form>
</body>
</html>
