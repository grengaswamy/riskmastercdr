﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectBatch.aspx.cs" Inherits="Riskmaster.UI.AutoChecks.SelectBatch" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Results For Automatic Check Batches</title>
	<link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
	<script language="javascript" src="../../Scripts/form.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      



    <asp:GridView ID="grdAutoChecksBatch" runat="server" 
        CellPadding="4 " onrowdatabound="grdAutoChecksBatch_RowDataBound"
         >
        <HeaderStyle CssClass="colheader6"/>


    </asp:GridView>


	<label id="lblNoRecord" runat="server"></label>
							




    

    
    </ContentTemplate>
    </asp:UpdatePanel>

    
    
    </div>
    </form>
</body>
</html>
