﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DupClaim.aspx.cs" Inherits="Riskmaster.UI.SupportScreens.DupClaim" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Possible Duplicate Claim...</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css">
    
<script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript">
					function SaveClaim()
					{
					//MGaba2:MITS 14932:Screen popping up again and again if we are ignoring the warninig					    
					//Changed the syscmd value
					    //window.opener.m_DataChanged = true;
					    //window.opener.Navigate(9);
					    //RMA-8965 : aravi5 : Changes for ShowModalDialog issue in Chrome
					    var IEbrowser = false || !!document.documentMode; // At least IE6
					    if (!IEbrowser) { 
					        window.opener.document.forms[0].dupeoverride.value = "IsSaveDup";      // csingh7 : MITS 19840
					        window.opener.window.Navigate(9);
					    }
					    window.returnValue = "OK";  // modified by csingh7 : :hanges done for Duplicate claim window issuen 
					    window.close();
					    return false;
					}
					
					function setFields()
					{
						// Claim Type
					    if (document.all("claimtype") != null)
					        document.all("claimtype").innerHTML = document.all("claimtype").innerHTML
							    + getQval('claimtypecode');
					    //+ window.opener.document.forms[0].claimtypecode_codelookup.value; commented by csingh7 :hanges done for Duplicate claim window issuen 
								
						
                        
                        if((getQval('formname')=='claimwc') || (getQval('formname')=='claimdi'))
                        {
                        // Jurisdiction
                            //Mits 15942:Asif Start
                            if (getQval('formname') == 'claimwc') {
                                if (document.all("jurisdiction") != null)
                                    document.all("jurisdiction").innerHTML = document.all("jurisdiction").innerHTML
							        + getQval('filingstateid');
                                //+ window.opener.document.forms[0].filingstateid_codelookup.value; commented by csingh7 :hanges done for Duplicate claim window issue 
                            }
                            else if (getQval('formname') == 'claimdi')
                             {
                                 if (document.all("jurisdiction") != null)
                                     document.all("jurisdiction").innerHTML = document.all("jurisdiction").innerHTML
                                     + getQval('jurisdiction');
                                 //+ window.opener.document.forms[0].jurisdiction_codelookup.value; commented by csingh7 :hanges done for Duplicate claim window issue
                            }

                            //Mits 15942:Asif End	
								// Employee Name
                            if (document.all("empName") != null)
                                document.all("empName").innerHTML = document.all("empName").innerHTML
							    + getQval('empemployeenumber');
                            //+ window.opener.document.forms[0].empemployeenumber.value; commented by csingh7 :hanges done for Duplicate claim window issue

						// Employee SSN
                            if (document.all("empSSN") != null)
                                document.all("empSSN").innerHTML = document.all("empSSN").innerHTML
							    + getQval('emptaxid');
                            //+ window.opener.document.forms[0].emptaxid.value; commented by csingh7 :hanges done for Duplicate claim window issue
                            //Deb MITS 27423
                            if (getQval('filingstateid') != "")
                                document.getElementById('jurisdiction').style.display = '';
                            else
                                document.getElementById('jurisdiction').style.display = 'none';
                            if (getQval('empemployeenumber') != "")
                                document.getElementById('idTrEmp').style.display = '';
                            else
                                document.getElementById('idTrEmp').style.display = 'none';
                            document.getElementById('claimdate').style.display = 'none';
                            if (getQval('claimtypecode') != "")
                                document.getElementById('claimtype').style.display = '';
                            else
                                document.getElementById('claimtype').style.display = 'none';
                            //Deb MITS 27423
                        }
                        else
                        {
                        // Claim date
                            if (document.all("claimdate") != null)
                                document.all("claimdate").innerHTML = document.all("claimdate").innerHTML
							    + getQval('dateofclaim');
                            //+ window.opener.document.forms[0].dateofclaim.value; commented by csingh7 :hanges done for Duplicate claim window issue
                        
                        document.getElementById('claimdate').style.display ='';
                        document.getElementById('jurisdiction').style.display ='none';
                            document.getElementById('idTrEmp').style.display ='none';
                        }
						return false;
					}
    </script>
    <%-- aravi5 : RMA-8634 - Unable to create claim over chrome Starts --%>
    <script type="text/javascript">
        function HideOverlaydiv() {
            var IEbrowser = false || !!document.documentMode; // At least IE6
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.opener.parent.parent.document.getElementById("overlaydiv").remove();
                }
                return false;
            }
        }
    </script>
    <%-- aravi5 : RMA-8634 - Unable to create claim over chrome Ends --%>
    <%--	<script src="csc-Theme/riskmaster/common/javascript/zapatec/utils/zapatec.js" type="text/javascript">
				</script><script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/window.js" type="text/javascript"></script>
				<script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
				<script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar.js"></script>
				<script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/lang/calendar-en.js"></script>
				<script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar-setup.js"></script>
				<script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/calendar-alias.js"></script>
				<script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid.js"></script>
				<script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-xml.js"></script>
				<script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-editable.js"></script>
				<script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-query.js"></script>
				<script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/rmx-common-ref.js"></script>--%>

    <script language="JavaScript" src="../../Scripts/cul.js"></script>

    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}</script>

</head>
<body class="10pt" onpagehide="HideOverlaydiv(); return false;" onload="setFields();this.focus();"> <%--	csingh7 MITS 18365--%> <%-- aravi5 : RMA-8634 --%>
    <form id="frmData" runat="server" name="frmData" method="post" action="/oxf/home">
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""><table border="0"
        width="100%">
        <tr>
            <td colspan="5" class="errtextheader">
                 <%--MITS 34260 start: Claim was not saved, possible duplicate claim... --%>
                Claim was not saved, possible duplicate claim<%=Request.QueryString["polnum"]%>...
                <%--MITS 34260 end --%>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <br>
            </td>
        </tr>
        <tr class="colheader">
            <td colspan="2">
                <u>Event Date</u>:&nbsp;<%=Request.QueryString["eventdt"]%>
            </td>
            <td colspan="2">
                <u>Department</u>:&nbsp;<%=Request.QueryString["dept"]%>
            </td>
            <td>
            </td>
        </tr>
        <tr class="colheader">
            <td colspan="2" id="claimdate"  style ="display:none"  >
                <u>Date of Claim</u>:&nbsp;
            </td>
            <td colspan="2" id="jurisdiction" style ="display:none"  >
                <u>Jurisdiction</u>:&nbsp;
            </td>
            <td colspan="2" id="claimtype">
                <u>Claim Type</u>:&nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr class="colheader" style ="display:none" id ="idTrEmp">
            <td colspan="2" id="empName">
                <u>Employee Name</u>:&nbsp;
            </td>
            <td colspan="2" id="empSSN">
                <u>Employee SSN</u>:&nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <br>
            </td>
        </tr>
      <%--  <tr class="ctrlgroup">
            <td class="colHeader">
                Claim Number
            </td>
            <td class="colHeader">
                Claim Status
            </td>
            <td class="colHeader">
                Claimant
            </td>
            <td class="colHeader">
                Event Number
            </td>
            <td class="colHeader">
                Event Description
            </td>
        </tr>
        <tr>
            <td>
                WC1RRNSB
            </td>
            <td>
                Open
            </td>
            <td>
                Aditi Kaushik
            </td>
            <td>
                CRV2008003280
            </td>
            <td>
            </td>
        </tr>--%>
        <tr>
            <table align="center" width="100%">
                <tr>
                    <td class="colHeader">
                        <asp:GridView ID="GridView1" runat="server" HeaderStyle-CssClass="ctrlgroup" AutoGenerateColumns="false" Width ="100%" OnRowDataBound="GridView1_RowDataBound">
                            <%--OnRowDataBound="GridView1_RowDataBound"--%>
                            <Columns>
                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblClaimId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "claimid")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ClaimNumber" HeaderText="Claim Number" />
                                <asp:BoundField DataField="ClaimStatus" HeaderText="Claim Status" />
                                <asp:BoundField DataField="Claimant" HeaderText="Claimant" />
                                <asp:BoundField DataField="EventNumber" HeaderText="Event Number" />
                                <asp:BoundField DataField="EventDesc" HeaderText="Event Description" />
                                <asp:TemplateField HeaderText="Claim Date" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblClmDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClaimDate")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                   </td> 
                </tr>
            </table>
        </tr>
        <tr>
            <td colspan="5">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="center">
                <input type="submit" name="$action^" value="Save (Ignore Duplication)" class="button"
                    onclick="return SaveClaim();">
                &nbsp;
                <input type="submit" name="$action^" value="Cancel Save" class="button" onclick="window.close();return false;">
            </td>
        </tr>
    </table>
    <input type="hidden" name="$node^7" value="rmx-widget-handle-4" id="SysWindowId"><input
        type="hidden" name="$instance" value="H4sIAAAAAAAAAHVTUU/CMBB+xl+x8OKLcIgmElKbGKcJ0ZgY/ANNd0DD2o5e5+Df2xXdYMM99b7v&#xA;6919dx1bGPLCSEz2Ojc036+s0/Q43HhfzAGqqhpXd2Pr1jCdTKZwpIf8ajAYJOFjGr2IUQwjRNIh&#xA;mpHKODhFWy3Io4NlWRTW+WUkCdKyeM6F0gxa+XmaktDVqCTJ4C/oVEIiZeu70GG+kPwbHv6Bp9AY&#xA;iHClTGarOj+D9hwpaAwem1KdjEL60EG3zrsy2wvlKQxgJG1e6t6NSFmXhUldYjLhxcgfCuyxm9As&#xA;Omcd8ZXICcNAT6Bz8RFszYe5qjZYHqe5MCt7IkmtLDUafzaxGi8L7OSPeNzqIiN+ez+bMGjCvvI1&#xA;PKUPoZHLWrMOW26QvvjlO/SQCo/8AWb1W5wxaLG+PsXC88+na0p82PlNstvtGESw4wNaI7/xiWEG&#xA;f78H/wEdrjTNKQMAAA=="></form>
</body>
</html>
