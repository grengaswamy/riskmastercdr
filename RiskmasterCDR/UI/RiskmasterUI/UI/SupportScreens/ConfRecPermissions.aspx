﻿<!--Created by pmittal5 - Confidential Record-->

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfRecPermissions.aspx.cs" Inherits="Riskmaster.UI.UI.SupportScreens.ConfRecPermissions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Permissions to use Confidential Record</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css">

    <script language="JavaScript" src="../../Scripts/form.js">
    </script>

    <script language="JavaScript" src="../../Scripts/ConfRecPermissions.js">
    </script>
</head>

<script type="text/javascript" language="javascript">
    function ConfRec() {
        document.getElementById("cancel").value = "1";
        if (window.opener.document.getElementById("hidden_DataChanged") != null)
            window.opener.document.getElementById("hidden_DataChanged").value = "true";

        if (Validate_ConfRec())
            self.close();
        return false;
    }
    function bCancel() {
        if (document.getElementById("cancel").value != "1")
            window.opener.document.getElementById("confrecflag").checked = false;
        self.close();
        return false;
    }
</script>

<body onunload="bCancel();">
    <form id="frmData" runat="server">
    <div class="msgheader" id="formtitle">Permissions to use Confidential Record</div>
    <div>
        <table width="100%">
            <tr>
                <td width="40%" valign="top">Available Users
                    <br>
                    <asp:ListBox id="availperms" ondblclick="AddSingle_ConfRec(this.options[this.selectedIndex]);"
                     SelectionMode="Multiple" style="width: 162px" Rows="12" runat = "server">
                    </asp:ListBox><br>
                </td>
                <td width="20%" valign="middle">
                    <br>
                    <br>
                    <input type="button" class="button" id="btnAdd" value=" &gt;&gt; " runat="server" onclick="AddSelected_ConfRec()" />
                    <br>
                    <input type="button" class="button" value=" &lt;&lt; " id="btnRemove" onclick="RemoveSelected_ConfRec()" runat="server" />
                 </td>
                 <td width="40%" valign="top">Selected Users
                     <br>
                     <asp:ListBox id="selectedperms" ondblclick="RemoveSingle_ConfRec(this.options[this.selectedIndex])"
                      SelectionMode="Multiple"  style="width: 162px" Rows="12" runat= "server">
                     </asp:ListBox>
                 </td>
            </tr>
            <tr>
               <td>
               <br>
                <asp:Button ID="btnOk" Text="OK" class="button" runat="server" OnClientClick="ConfRec()" />
                <asp:Button Text="Cancel" class="button" ID="Button1" OnClientClick="bCancel()" runat="server"  />
               </td>
            </tr>
        </table>
    </div>
     <asp:TextBox style="display:none" runat="server" id="hdnselectedperms" RMXType="hidden"/>       
     <asp:TextBox style="display:none" runat="server" id="hdnselectedpermsIds" RMXType="hidden"/>    
     <asp:TextBox style= "display:none" runat="server" id="CurrentUserID" RMXType="hidden"/>
     <asp:TextBox style= "display:none" runat="server" id="cancel" value="0" RMXType="hidden"/>
    </form>
</body>
</html>
