﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DupAddress.aspx.cs" Inherits="Riskmaster.UI.UI.SupportScreens.DupAddress" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Possible Duplicate Address...</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css">

    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript">
        function SaveAddress() {
            //Changed the syscmd value
            //window.opener.m_DataChanged = true;
            window.opener.document.forms[0].dupeoverride.value = "IsSaveDup";
            window.opener.Navigate(9);
            window.returnValue = "OK";  
            window.close();
            return false;
        }

        // RMA-8753 for Dup popup(RMA-14262) nshah28(added this for other main. screen)
        //In case of cancel from Address maint, we will just close the window. but from "EntityMaint,Employee etc" we will insert/update dup addressid.
        function CancelAddress() {
            if (getQval('formname') != null) {
                if (getQval('formname').toLowerCase() != 'address') {
                    window.opener.document.forms[0].dupeoverride.value = "Cancel";
                    window.opener.Navigate(9);
                    window.returnValue = "OK";
                }
                else {
                    window.opener.m_DataChanged = true; //In case of Address maint. screen, this value need to set true
                }
            }
            window.close();
            return false;
        }

        function setFields() {

          //  if (document.all("claimtype") != null)

                if ((getQval('formname') == 'address')) {


                    if (getQval('addr1') != "") {
                        document.all("addr1").innerHTML = document.all("addr1").innerHTML
                        + getQval('addr1');
                        document.getElementById('addr1').style.display = ''
                    }


                    
                    if (getQval('addr2') != "") {
                        document.all("addr2").innerHTML = document.all("addr2").innerHTML
                        + getQval('addr2');
                        document.getElementById('addr2').style.display = ''
                    }

                    
                    if (getQval('addr3') != "") {
                        document.all("addr3").innerHTML = document.all("addr3").innerHTML
                        + getQval('addr3');
                        document.getElementById('addr3').style.display = ''
                    }

                    if (getQval('addr4') != "") {
                        document.all("addr4").innerHTML = document.all("addr4").innerHTML
                        + getQval('addr4');
                        document.getElementById('addr4').style.display = ''
                    }
                    if (getQval('city') != "") {
                        document.all("city").innerHTML = document.all("city").innerHTML
                        + getQval('city');
                        document.getElementById('city').style.display = ''
                    }
                    
                    
                    if (getQval('state') != "") {
                        document.all("state").innerHTML = document.all("state").innerHTML
                       + getQval('state');
                        document.getElementById('state').style.display = '';
                    }
                    else
                        document.getElementById('state').style.display = 'none';

                    if (getQval('country') != "") {
                        document.all("country").innerHTML = document.all("country").innerHTML
                      + getQval('country');
                        document.getElementById('country').style.display = '';
                    }
                    else
                        document.getElementById('country').style.display = 'none';
                    
                    if (getQval('county') != "") {
                        document.all("county").innerHTML = document.all("county").innerHTML
                      + getQval('county');
                        document.getElementById('county').style.display = '';
                    }
                    else
                        document.getElementById('county').style.display = 'none';
                    
                }
                if (getQval('zipcode') != "") {
                    document.all("zipcode").innerHTML = document.all("zipcode").innerHTML
                  + getQval('zipcode');
                    document.getElementById('zipcode').style.display = '';
                }
                else
                    document.getElementById('zipcode').style.display = 'none';
            return false;
        }
    </script>
    <script language="JavaScript" src="../../Scripts/cul.js"></script>

    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}</script>
</head>
<body class="10pt" onload="setFields();this.focus();">
    
    <form id="frmData" runat="server" name="frmData" method="post" action="/oxf/home">
        <input type="hidden" id="wsrp_rewrite_action_1" name="" value="">
        <table border="0" width="100%">
            <tr>
                <td colspan="5" class="errtextheader">
                    <%--RMA-8753 nshah28 start: Address was not saved, possible duplicate address... --%>
                    <asp:label runat="server" id="lblDupAddressMessage" Text="<%$ Resources:lblDupAddressMessage %>"></asp:label>
                
                <%--RMA-8753 end --%>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <br>
                </td>
            </tr>

            <tr class="colheader">
               <%-- <td colspan="2">
                    <u>Address1</u>:&nbsp;<%=Request.QueryString["addr1"]%>
                </td>
                <td colspan="2">
                    <u>Address2</u>:&nbsp;<%=Request.QueryString["addr2"]%>
                </td>--%>
                <td></td>
            </tr>
            <tr class="colheader">
                <td colspan="2" id="addr1" style="display: none">
                    <u>Address1</u>:&nbsp;
                </td>
                <td colspan="2" id="addr2" style="display: none">
                    <u>Address2</u>:&nbsp;
                </td>
                <td></td>
            </tr>
            <tr class="colheader">
                <td colspan="2" id="addr3" style="display: none">
                    <u>Address3</u>:&nbsp;
                </td>
                <td colspan="2" id="addr4" style="display: none">
                    <u>Address4</u>:&nbsp;
                </td>
                <td></td>
            </tr>
            <tr class="colheader">
                <td colspan="2" id="city" style="display: none">
                    <u>City</u>:&nbsp;
                </td>
                <td colspan="2" id="county" style="display: none">
                    <u>County</u>:&nbsp;
                </td>

            </tr>

            <tr class="colheader">
                <td colspan="2" id="country" style="display: none">
                    <u>Country</u>:&nbsp;
                </td>
                <td colspan="2" id="state" style="display: none">
                    <u>State</u>:&nbsp;
                </td>

            </tr>
            <tr class="colheader">
                <td colspan="2" id="zipcode">
                    <u>ZipCode</u>:&nbsp;
                </td>
            </tr>

            <tr>
                <td colspan="5">
                    <br>
                </td>
            </tr>
            
            <tr>
                 <table align="center" width="100%">
                <tr>
                    <td class="colHeader">

                        <asp:Gridview id="GridView1" runat="server" headerstyle-cssclass="ctrlgroup" autogeneratecolumns="False" width="100%" onrowdatabound="grdDupAddress_RowDataBound" onselectedindexchanged="grdDupAddress_SelectedIndexChanged">
                            <Columns>
                               
                                <asp:BoundField HeaderText="Address1" DataField="Addr1"></asp:BoundField>
                                <asp:BoundField HeaderText="Address2" DataField="Addr2"></asp:BoundField>
                                <asp:BoundField HeaderText="Address3" DataField="Addr3"></asp:BoundField>
                                <asp:BoundField HeaderText="Address4" DataField="Addr4"></asp:BoundField>
                                <asp:BoundField HeaderText="City" DataField="City"></asp:BoundField>
                                <asp:BoundField HeaderText="Country" DataField="Country"></asp:BoundField>
                                <asp:BoundField HeaderText="State" DataField="State"></asp:BoundField>
                                <asp:BoundField HeaderText="County" DataField="County"></asp:BoundField>
                                <asp:BoundField HeaderText="ZipCode" DataField="Zipcode"></asp:BoundField>
                            </Columns>


                        </asp:Gridview>

                        </td>
                    </tr>
                     </table>
            </tr>
            <tr>
            <td colspan="5">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="center">
                <input type="submit" name="$action^" value="Save (Ignore Duplication)" class="button"
                    onclick="return SaveAddress();">
                &nbsp;
                <input type="submit" name="$action^" value="Cancel Save" class="button" onclick="return CancelAddress();">
            </td>
        </tr>
        </table>
    </form>
</body>
</html>
