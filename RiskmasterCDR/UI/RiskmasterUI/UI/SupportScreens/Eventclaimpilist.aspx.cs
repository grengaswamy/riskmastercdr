﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Collections;

namespace Riskmaster.UI.UI.SupportScreens
{
    public partial class Eventclaimpilist : System.Web.UI.Page
    {
        /// <summary>
        /// Contains web service output
        /// </summary>
        XmlDocument oFDMPageDom = null;
        public IEnumerable result = null;
        public XElement rootElement = null;
        string sReturn = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            oFDMPageDom = new XmlDocument();
            if (!Page.IsPostBack)
            {
                //Preparing XML to send to service
                XElement oMessageElement = GetMessageTemplate();

                XElement oElement = oMessageElement.XPathSelectElement("./Document/GetPIList/EventId");
                if (oElement != null)
                {
                    oElement.Value = AppHelper.GetQueryStringValue("eventid");
                    eventid.Value = oElement.Value;
                }

                //Calling Service to get all PreBinded Data 
                sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                oFDMPageDom.LoadXml(sReturn);
                XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                oFDMPageDom.LoadXml(oInstanceNode.OuterXml);

                rootElement = XElement.Parse(oFDMPageDom.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Instance/PersonInvolvedList/PersonInvolved")
                         select c;
                picount.Value = rootElement.XPathSelectElements("//Instance/PersonInvolvedList/PersonInvolved").Count().ToString();
                hdcaller.Value = AppHelper.GetQueryStringValue("caller");
            
            }
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>b9fcbb13-9b1d-4a29-b216-42b069b87a52</Authorization> 
                     <Call>
                        <Function>ListPersonInvolvedAdaptor.GetPIList</Function> 
                     </Call>
                     <Document>
                        <GetPIList>
                            <EventId></EventId> 
                        </GetPIList>
                    </Document>
            </Message>

            ");

            return oTemplate;
        }
    }
}

   
