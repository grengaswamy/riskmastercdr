﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Data;
using System.IO;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.SupportScreens
{
    //RMA-8753 nshah28 start
    public partial class DupAddress : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
                string sReturn = "";
            
            //Preparing XML to send to service
            XElement oMessageElement = GetDupAddressListTemplate();

            //Modify XML
            XElement oElement = oMessageElement.XPathSelectElement("./Document/AddressList/Dupes/AddressId");

            if (oElement != null)
            {
                if (Request.QueryString["AddressId"] != null)
                {
                    oElement.Value = Request.QueryString["AddressId"];
                }
            }
            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
            if (oFDMPageDom.GetElementsByTagName("Dupes").Count > 0)
            {
                DataSet oDataset = new DataSet();
                for (int i = 0; i < oFDMPageDom.GetElementsByTagName("Dupes").Count; i++)
                {
                    oDataset.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("Dupes")[i].OuterXml));
                }
                GridView1.DataSource = oDataset.Tables["Dupes"];
                GridView1.DataBind();
            }
        }

        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        private void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }
        /// <summary>
        /// CWS request message template for GetDupAddressListTemplate
        /// </summary>
        /// <returns></returns>
        private XElement GetDupAddressListTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                    <Call>
                        <Function>DupAddressListAdaptor.GetDupAddressList</Function>
                     </Call>
                      <Document>
                          <AddressList>
                            <Dupes>
                                <AddressId></AddressId>
                                <Addr1></Addr1>
                                <Addr2></Addr2>
                                <Addr3></Addr3>
                                <Addr4></Addr4>
                                <City></City>
                                <Country></Country>
                                <State></State>
                                <County></County>
                                <Zipcode></Zipcode>
                            </Dupes>
                        </AddressList>
                      </Document>
                </Message>


            ");
            return oTemplate;
        }

        protected void grdDupAddress_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grdDupAddress_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}