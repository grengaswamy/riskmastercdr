﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;

namespace Riskmaster.UI.UI.SupportScreens
{
    public partial class BISRedirect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Preparing XML to send to service
            XElement oMessageElement = GetMessageTemplate();

            //Calling Service to get all PreBinded Data 
            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            ErrorControl.errorDom = sReturn;

            if(!ErrorControl.errorFlag)
            {
                XElement objOuterXml = XElement.Parse(sReturn);
                string sLoginStr = "";
                string sBISUrl = "";
                XElement oEle = objOuterXml.XPathSelectElement("./Document/ParamList/Param[@name='LoginStr']");
                if (oEle != null)
                {
                    sLoginStr = oEle.Value;
                }
                oEle = objOuterXml.XPathSelectElement("./Document/ParamList/Param[@name='BISURL']");
                if (oEle != null)
                {
                    sBISUrl = oEle.Value;
                }
                bisurl.Value = sBISUrl + "?connectstr=" + sLoginStr;
            }

        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                    <Authorization>be79b44c-79d1-4022-ab33-4f22f3e76176</Authorization> 
                    <Call>
                        <Function>LoginAdaptor.GetBISSSOString</Function> 
                    </Call>
                    <Document>
                        <ParamList>
                              <Param name='AuthPassword'>BISRMAUTHKEY0306</Param> 
                              <Param name='LoginStr' /> 
                              <Param name='BISURL' /> 
                        </ParamList>
                    </Document>
                  </Message>


            ");
            return oTemplate;
        }
    }
}
