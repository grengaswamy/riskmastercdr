﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.RMXResourceManager; //MITS 34079 - hlv

namespace Riskmaster.UI.SupportScreens
{
    public partial class PrintAdjusterDatedText : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //MITS 34079 - hlv begin
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }

            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("PrintAdjusterDatedText.aspx"), "PringAdjDTValidations",
                                          ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "PringAdjDTValidations", sValidationResources, true);
            //MITS 34079 - hlv end


            hdnadjrowid.Text = Request.QueryString["adjrowid"];
            hdnClaimNumber.Text = Request.QueryString["claimnumber"];


            printall.Attributes.Add("OnClick", "EnableDisableDate();");
            printbtwdates.Attributes.Add("OnClick", "EnableDisableDate();");
            filtertype.Attributes.Add("OnClick", "EnDisFilter();");  
        }
    }
}
