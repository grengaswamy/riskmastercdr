﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
//using Riskmaster.Models;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SupportScreens
{
    public partial class DupClaim : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                XmlDocument oFDMPageDom = new XmlDocument();
                string sReturn = "";
                //Preparing XML to send to service
                XElement oMessageElement = GetDupClaimListTemplate();
                //Modify XML 
                XElement oClaimIDs = oMessageElement.XPathSelectElement("./Document/AdjusterDatedText/Dupes/ClaimIds");
                if (oClaimIDs != null)
                {
                    if (Request.QueryString["claimids"] != null)
                    {
                        oClaimIDs.Value = Request.QueryString["claimids"];
                        //hdnEmployeeId.Value = Request.QueryString["employeeid"];
                    }
                }
                XElement oFormName = oMessageElement.XPathSelectElement("./Document/AdjusterDatedText/Dupes/FormName");
                if (oFormName != null)
                {
                    if (Request.QueryString["formname"] != null)
                    {
                        oFormName.Value = Request.QueryString["formname"];
                        //hdnClaimId.Value = Request.QueryString["claimid"];
                    }

                }
                XElement oEventDate = oMessageElement.XPathSelectElement("./Document/AdjusterDatedText/Dupes/EventDate");
                if (oEventDate != null)
                {
                    if (Request.QueryString["eventdt"] != null)
                    {
                        oEventDate.Value = Request.QueryString["eventdt"];
                        //hdnClaimId.Value = Request.QueryString["claimid"];
                    }

                }
                XElement oDept = oMessageElement.XPathSelectElement("./Document/AdjusterDatedText/Dupes/Dept");
                if (oDept != null)
                {
                    if (Request.QueryString["dept"] != null)
                    {
                        oDept.Value = Request.QueryString["dept"];
                        //hdnClaimId.Value = Request.QueryString["claimid"];
                    }

                }
                //Calling Service to get all PreBinded Data 
                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                //if (oFDMPageDom.GetElementsByTagName("MMIHistory").Count > 0)
                //{
                //    XmlElement xmlMMIHistoryNode = (XmlElement)oFDMPageDom.GetElementsByTagName("MMIHistory")[0];
                //    title.InnerText = "MMI History [ " + xmlMMIHistoryNode.Attributes["claimnumber"].Value + " ]";
                //    ChangedBy.InnerText = xmlMMIHistoryNode.Attributes["username"].Value; ;
                //    hdnMaxDate.Value = xmlMMIHistoryNode.Attributes["maxdate"].Value; ;
                //}
                if (oFDMPageDom.GetElementsByTagName("Dupes").Count > 0)
                {
                    DataSet aDataSet = new DataSet();
                    aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("Dupes")[0].OuterXml));

                    //// Bind the DataSet to the grid view
                    GridView1.DataSource = aDataSet.Tables["claim"];
                    GridView1.DataBind();
                }
            }
        }
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        private void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }
        /// <summary>
        /// CWS request message template for GetDupClaimListTemplate
        /// </summary>
        /// <returns></returns>
        private XElement GetDupClaimListTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                    <Call>
                        <Function>DupClaimListAdaptor.GetDupClaimList</Function>
                     </Call>
                      <Document>
                          <AdjusterDatedText>
                            <Dupes>
                                <ClaimIds></ClaimIds>
                                <FormName></FormName>
                                <EventDate></EventDate>
                                <Dept></Dept>
                            </Dupes>
                        </AdjusterDatedText>
                      </Document>
                </Message>


            ");
            return oTemplate;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowIndex ==0)
            {
                if (Request.QueryString["formname"] == "claimwc" || Request.QueryString["formname"] == "claimdi")
                {
                    GridView1.Columns[6].Visible = true;
                }
                else
                {
                    GridView1.Columns[6].Visible = false;
                }
            }
        }
    }
}
