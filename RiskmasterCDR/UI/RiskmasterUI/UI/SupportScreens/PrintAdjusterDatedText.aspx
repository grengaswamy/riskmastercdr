﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintAdjusterDatedText.aspx.cs" Inherits="Riskmaster.UI.SupportScreens.PrintAdjusterDatedText" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print Adjuster Dated Text</title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <!--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
        <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
        <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>-->

    <!--MITS 34079  : hlv ML-->    
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <!--MITS 34079  : hlv ML-->

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" language="javascript" >
        function SetFromToDate() {
            var today = new Date();
            //MITS 34079 begin 
            $("#txtBeginDate").datepicker('setDate', today);
            $("#txtEndDate").datepicker('setDate', today);
            EnableDisableDate();
            //document.getElementById('txtBeginDate').value = today.getMonth() + 1 + "/" + today.getDate() + "/" + (today.getYear());
            //document.getElementById('txtEndDate').value = today.getMonth() + 1 + "/" + today.getDate() + "/" + (today.getYear());
            //MITS 34079 end
            document.getElementById('lstFilterType_multicode').style.backgroundColor = "#f2f2f2";
            document.getElementById('lstFilterType_multicode').disabled = true;
            document.getElementById('lstFilterType_multicodebtn').disabled = true;
            document.getElementById('lstFilterType_multicodebtndel').disabled = true;
            return false;
        }

        function EnableDisableDate() {
            var datebtn1 = $("#txtBeginDate");
            var datebtn2 = $("#txtEndDate");

            if (document.getElementById('printbtwdates').checked) {
                document.getElementById('txtBeginDate').disabled = false;
                //document.getElementById('btnBeginDate').disabled = false;
                document.getElementById('txtEndDate').disabled = false;
                //document.getElementById('btnEndDate').disabled = false;
                $.datepicker._disabledInputs = [];
            }
            else if (document.getElementById('printall').checked) {
                document.getElementById('txtBeginDate').disabled = true;
                //document.getElementById('btnBeginDate').disabled = true;
                document.getElementById('txtEndDate').disabled = true;
                //document.getElementById('btnEndDate').disabled = true;
                $.datepicker._disabledInputs = [datebtn1[0], datebtn2[0]];
            }
            return false;
        }

        function EnDisFilter() {
            if (document.getElementById('filtertype').checked) {
                document.getElementById('lstFilterType_multicode').disabled = false;
                document.getElementById('lstFilterType_multicodebtn').disabled = false;
                document.getElementById('lstFilterType_multicodebtndel').disabled = false;
                document.getElementById('lstFilterType_multicode').style.backgroundColor = "";
            }
            else {
                document.getElementById('lstFilterType_multicode').disabled = true;
                document.getElementById('lstFilterType_multicodebtn').disabled = true;
                document.getElementById('lstFilterType_multicodebtndel').disabled = true;
                document.getElementById('lstFilterType_multicode').style.backgroundColor = "#f2f2f2";
            }
            return false;
        }
         
        function ValForm() {
            var sParams;   
            if (document.getElementById('printbtwdates').checked) {
                if (document.getElementById('txtBeginDate').value == '') {
                    //alert('Please enter a Begin Date');
                    alert(PringAdjDTValidations.valBeginDate);
                    document.getElementById('txtBeginDate').focus();
                    return false;
                }
                else if (document.getElementById('txtEndDate').value == '') {
                    //alert('Please enter an End Date');
                    alert(PringAdjDTValidations.valEndDate);
                    document.getElementById('txtEndDate').focus();
                    return false;
                }
            }
            if (document.getElementById('filtertype').checked) {
                if (document.getElementById('lstFilterType_multicode_lst').value == '') {
                    //alert('Please select dated text types to be printed.');
                    alert(PringAdjDTValidations.valFilterType);
                    document.getElementById('lstFilterType_multicode').focus();
                    return false;
                }
            }
            //document.getElementById('hdnAction').value = 'continue';
            sParams = qs();
            window.setTimeout("urlNavigate('../SupportScreens/AdjDatedTextList.aspx?" + sParams + "')", 10);
            return true;
        }
        function qs() {
            var sLinkParams="";	
            sLinkParams = "adjrowid=" + document.getElementById('hdnadjrowid').value +"&";
            sLinkParams = sLinkParams + "ClaimNumber=" + document.getElementById('hdnClaimNumber').value + "&";
            if (document.getElementById('printbtwdates').checked) {
                sLinkParams = sLinkParams + "FilterMethod=" + document.getElementById('printbtwdates').value + "&";
                sLinkParams = sLinkParams + "BeginDate=" + document.getElementById('txtBeginDate').value + "&";
                sLinkParams = sLinkParams + "EndDate=" + document.getElementById('txtEndDate').value + "&";
            }
            else {
                sLinkParams = sLinkParams + "FilterMethod=" + document.getElementById('printall').value + "&";
                sLinkParams = sLinkParams + "BeginDate=&";
                sLinkParams = sLinkParams + "EndDate=&";
            }
            if (document.getElementById('filtertype').checked) {
                sLinkParams = sLinkParams + "FilterType=1&";
                sLinkParams = sLinkParams + "FilterTypeList=" + document.getElementById('lstFilterType_multicode_lst').value;
            }
            else {
                sLinkParams = sLinkParams + "FilterType=&FilterTypeList=";
            }
            return sLinkParams;
        }
    </script>
</head>
<body class="10pt" onload="SetFromToDate()">
    <form id="frmData" runat="server">
        <table border="0" width="100%">
            <asp:TextBox ID="hdnAction" runat="server" style="display:none" rmxref="/Instance/ui/action"></asp:TextBox>
            <asp:TextBox ID="hdnadjrowid" runat="server" style="display:none" rmxref="/Instance/Document/adjrowid"></asp:TextBox>
            <asp:TextBox ID="hdnClaimNumber" runat="server"  style="display:none" />
            <tr>
				<td class="formtitle" colspan="2"><asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label></td>
			</tr>
			<tr><td colspan="2"><br/></td></tr>
			<tr>
				<td class="ctrlgroup" colspan="2"><asp:Label ID="lblTopText" runat="server" Text="<%$ Resources:lblTopText %>"></asp:Label></td>
			</tr>
			<tr><td colspan="2"><br/></td></tr>
			<tr>
				<td>
				     <asp:RadioButton  GroupName="FilterMethod"  id="printall" runat="server" rmxref="/Instance/Document/FilterMethod" value="1" Checked="true"/>
			    </td>
				<td><asp:Label ID="lblPrintAll" runat="server" Text="<%$ Resources:lblPrintAll %>"></asp:Label></td>
			</tr>
            <tr>
				<td>
				    <asp:RadioButton GroupName="FilterMethod" id="printbtwdates" runat="server" rmxref="/Instance/Document/FilterMethod" value="2" />
				</td>
				<td><asp:Label ID="lblPrintBTWDates" runat="server" Text="<%$ Resources:lblPrintBTWDates %>"></asp:Label></td>
			</tr>
			<tr>
				<td></td>
				<td><asp:label runat="server" class="label" id="lbl_begindate" text="<%$ Resources:lblBeginDate %>" /></td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" id="txtBeginDate" RMXRef="/Instance/Document/BeginDate" RMXType="date" disabled="true" />

                    <!--<input type="button" class="DateLookupControl" name="btnBeginDate" disabled="true"/>-->
                    <script type="text/javascript">
                        /*
                        Zapatec.Calendar.setup(
				            {
				                inputField: "txtBeginDate",
				                ifFormat: "%m/%d/%Y",
				                button: "btnBeginDate"
				            }
				            );
                        */

                        //MITS 34079  : hlv ML
                        $(function () {
                            $("#txtBeginDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                               // buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                        });

                        //MITS 34079  : hlv ML end
                     </script>
		        </td> 
            </tr>
			<tr>
				<td></td>
				<td><asp:label runat="server" class="label" id="lbl_enddate" text="<%$ Resources:lblEndDate %>" /></td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" id="txtEndDate" RMXRef="/Instance/Document/EndDate" RMXType="date" disabled="true" onblur="dateLostFocus(this.id);"/>
                    <!--<input type="button" class="DateLookupControl" name="btnEndDate" />-->
                    <script type="text/javascript">
                        /*
                        Zapatec.Calendar.setup(
				            {
				                inputField: "txtEndDate",
				                ifFormat: "%m/%d/%Y",
				                button: "btnEndDate"
				            }
				            );
                         */

                        //MITS 34079  : hlv ML begin
                        $(function () {
                            $("#txtEndDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                        });

                        //MITS 34079  : hlv ML end
		            </script>
		        </td> 
            </tr>
			<tr>
			    <td>
			        <asp:CheckBox id="filtertype" rmxref="/Instance/Document/FilterType" runat="server" />
			    </td>	
		        <td><asp:Label ID="lblPrintFilType" runat="server" Text="<%$ Resources:lblPrintFilType %>"></asp:Label></td>
			</tr>
			<tr>
				<td></td>
				<td>
				    <uc:MultiCode ID="lstFilterType" runat ="server" RMXRef="/Instance/Document/FilterTypeList" CodeTable="ADJ_TEXT_TYPE" Enabled="true" ControlName="lstFilterType"  RemoveToolTip="<%$ Resources:ttRemove %>" />
				</td>
			</tr>
			<tr><td colspan="2"><br/><hr/></td></tr>
			<tr>
				<td colspan="2">
				    <asp:Button  class="button" id="btnCont" Text ="<%$ Resources:btnCont %>" runat="server" OnClientClick ="return ValForm();return false;" />
					&#160;
					<asp:Button  class="button" id="btnCancel" Text ="<%$ Resources:btnCancel %>" runat="server" OnClientClick ="window.close();" Width ="50px" />
				</td>
			</tr>
		</table>
    </form>
</body>
</html>
