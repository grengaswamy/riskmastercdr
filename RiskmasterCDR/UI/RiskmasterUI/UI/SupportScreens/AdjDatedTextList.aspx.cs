﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.SupportScreens
{
    public partial class AdjDatedTextList : NonFDMBasePageCWS
    {
        XmlDocument oFDMPageDom = null;
        protected IEnumerable result = null;
        protected XElement rootElement = null;  
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = string.Empty;
                if (!IsPostBack)
                {
                    adjRowID.Text = Request.QueryString["adjrowid"];
                    hdnClaimNumber.Text = Request.QueryString["ClaimNumber"];
                    FilterMethod.Text = Request.QueryString["FilterMethod"];
                    BeginDate.Text = Request.QueryString["BeginDate"];
                    EndDate.Text = Request.QueryString["EndDate"];
                    FilterType.Text = Request.QueryString["FilterType"];
                    FilterTypeList.Text = Request.QueryString["FilterTypeList"];

                    XmlTemplate = GetMessageTemplate(adjRowID.Text, hdnClaimNumber.Text, FilterMethod.Text, BeginDate.Text, EndDate.Text, FilterType.Text, FilterTypeList.Text);
                    bReturnStatus = CallCWS("AdjDtdTxtAdaptor.GetAdjDtdTxt", XmlTemplate, out sreturnValue, false, false);
                    if (bReturnStatus)
                    {
                        XmlDocument XmlDoc = new XmlDocument();
                        XmlDoc.LoadXml(sreturnValue);
                        //nadim for 13515                      
                            if (XmlDoc.SelectSingleNode("//AdjusterDatedText/ClaimNumber") != null)
                            hdnClaimNumber.Text = XmlDoc.SelectSingleNode("//AdjusterDatedText/ClaimNumber").InnerText;
                        else
                                hdnClaimNumber.Text = string.Empty;
                        //nadim for 13515

                        //MITS:13515 Start By abansal23
                        if (XmlDoc.SelectSingleNode("//AdjusterDatedText/PrimaryClaimantName") != null)
                            hdnPrimaryClaimantName.Text = XmlDoc.SelectSingleNode("//AdjusterDatedText/PrimaryClaimantName").InnerText;
                        else
                            hdnPrimaryClaimantName.Text = string.Empty;
                        //MITS:13515 Stop By abansal23


                        if (XmlDoc.SelectSingleNode("//AdjusterDatedText/ProprietaryInfo") != null)
                            ProprietaryInfo.Text = XmlDoc.SelectSingleNode("//AdjusterDatedText/ProprietaryInfo").InnerText;
                        else
                            ProprietaryInfo.Text = string.Empty;
                        if (XmlDoc.SelectSingleNode("//AdjusterDatedText/RMName") != null)
                            RMName.Text = XmlDoc.SelectSingleNode("//AdjusterDatedText/RMName").InnerText;
                        else
                            ProprietaryInfo.Text = string.Empty;
                        if (XmlDoc.SelectSingleNode("//AdjusterDatedText/Copyright") != null)
                            Copyright.Text = XmlDoc.SelectSingleNode("//AdjusterDatedText/Copyright").InnerText;
                        else
                            ProprietaryInfo.Text = string.Empty;
                        if (XmlDoc.SelectSingleNode("//AdjusterDatedText/RightsReserved") != null)
                            RightsReserved.Text = XmlDoc.SelectSingleNode("//AdjusterDatedText/RightsReserved").InnerText;
                        else
                            ProprietaryInfo.Text = string.Empty;
                        if (XmlDoc.SelectSingleNode("//AdjusterDatedText/ReportDate") != null)
                            ReportDate.Text = XmlDoc.SelectSingleNode("//AdjusterDatedText/ReportDate").InnerText;
                        else
                            ProprietaryInfo.Text = string.Empty;

                        rootElement = XElement.Parse(XmlDoc.OuterXml);
                        result = from c in rootElement.XPathSelectElements("//AdjusterDatedText/Row")
                                 select c;

                        
                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate( string sAdjRowID, string sClaimNumber, string sFilterMethod, string sBeginDate,string sEndDate, string sFilterType, string sFilterTypeList)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><AdjusterDatedText><adjrowid>");
            sXml = sXml.Append(sAdjRowID);
            sXml = sXml.Append("</adjrowid><ClaimNumber>");
            sXml = sXml.Append(sClaimNumber);
            sXml = sXml.Append("</ClaimNumber><FilterMethod>");
            sXml = sXml.Append(sFilterMethod);
            sXml = sXml.Append("</FilterMethod><BeginDate>");
            sXml = sXml.Append(sBeginDate);
            sXml = sXml.Append("</BeginDate><EndDate>");
            sXml = sXml.Append(sEndDate);
            sXml = sXml.Append("</EndDate><FilterType>");
            sXml = sXml.Append(sFilterType);
            sXml = sXml.Append("</FilterType><FilterTypeList>");
            sXml = sXml.Append(sFilterTypeList);
            sXml = sXml.Append("</FilterTypeList></AdjusterDatedText></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }

}
