﻿
//Created by pmittal5 - Confidential Record
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;

namespace Riskmaster.UI.UI.SupportScreens
{
    public partial class ConfRecPermissions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            XElement oMessageElement = null;
            XmlNodeList oPermsList = null;
            ListItem lPermItem = null;
            if (!Page.IsPostBack)
            {
                oMessageElement = GetAvailablePermListTemplate();
                XElement oEventId = oMessageElement.XPathSelectElement("//EventID");
                if (oEventId != null)
                {
                    if (Request.QueryString["EventID"] != null)
                        oEventId.Value = Request.QueryString["EventID"];
                }
                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                if (oFDMPageDom != null)
                {
                    oPermsList = oFDMPageDom.SelectNodes("//PermItem");
                    if (oPermsList != null)
                    {
                        foreach (XmlNode oNode in oPermsList)
                        {
                            lPermItem = new ListItem();
                            lPermItem.Text = oNode.Attributes["DisplayName"].Value;
                            if (oNode.Attributes["GroupId"].Value == "0")
                                lPermItem.Value = "user_" + oNode.Attributes["UserId"].Value;
                            if (oNode.Attributes["Selected"] != null && oNode.Attributes["Selected"].Value == "1")
                            {
                                selectedperms.Items.Add(lPermItem);
                            }
                            else
                            {
                                availperms.Items.Add(lPermItem);
                            }
                            lPermItem = null;
                        }
                    }
                    if (oFDMPageDom.SelectSingleNode("//CurrentUserID") != null)
                        CurrentUserID.Text = oFDMPageDom.SelectSingleNode("//CurrentUserID").Attributes["userID"].Value;
                }
            }
        }

        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        private void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);
        }

        /// <summary>
        /// CWS request message template for GetAvailablePermListTemplate
        /// </summary>
        /// <returns></returns>
        private XElement GetAvailablePermListTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                    <Call>
                        <Function>ConfRecPermsAdaptor.GetAvailablePermList</Function>
                     </Call>
                      <Document>
                        <Template>
                            <EventID></EventID>
                        </Template>
                      </Document>
                </Message>
            ");
            return oTemplate;
        }

        //protected void AddSelected_ConfRec(object sender, EventArgs e)
        //{
        //    foreach (ListItem item in availperms.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            if (selectedperms.Items.IndexOf(item) == -1)
        //            {
        //                selectedperms.Items.Add(item);
        //                selectedperms.SelectedIndex = selectedperms.Items.IndexOf(item);
        //            }
        //        }
        //    }
        //}

        //protected void RemoveSelected_ConfRec(object sender, EventArgs e)
        //{
        //    while (selectedperms.SelectedItem != null)
        //    {
        //        if(selectedperms.SelectedItem.Value == "user_"+CurrentUserID.Text)
        //           // "RMX Current loggin user can not be removed"
        //        else
        //            selectedperms.Items.Remove(selectedperms.SelectedItem);
        //    }
        //}
    }
}
