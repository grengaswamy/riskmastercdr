﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BISRedirect.aspx.cs" Inherits="Riskmaster.UI.UI.SupportScreens.BISRedirect" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function Onload() {
            if (parent.MDIScreenLoaded != null) {
                parent.MDIScreenLoaded();
            }
            window.location.href = document.getElementById('bisurl').value;
        }
    </script>
</head>
<body onload="Onload()">
    <form id="form1" runat="server">
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div>
        <asp:HiddenField ID="bisurl" runat="server" />
    </div>
    </form>
</body>
</html>
