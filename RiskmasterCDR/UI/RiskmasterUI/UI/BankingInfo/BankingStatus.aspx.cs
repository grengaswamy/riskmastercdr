﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.BankingInfo
{
    public partial class BankingStatus : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";
                string sCurstatus = "";
                string sCurdate = "";
                string sCurapprovedby = "";
                string sCurreason = "";
                XmlDocument XmlDoc = new XmlDocument();
                DataTable objTable = new DataTable();
                DataTable ddTable = new DataTable();
                XmlNode xNode;
                DataRow objRow;

                //Start - Yukti-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }

                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("BankingStatus.aspx"), "BankingStatusValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "BankingStatusValidations", sValidationResources, true);
                //End - Yukti-ML Changes
                if (!IsPostBack)
                {

                    bankingrowid.Text = Request.QueryString["bankingrowid"];
                  //  triggerdate.Text = Request.QueryString["triggerdate"];
                    sCurstatus = Request.QueryString["curstatus"];
                    sCurdate = Request.QueryString["curdate"];
                    sCurapprovedby = Request.QueryString["curapprovedby"];
                    sCurreason = Request.QueryString["curreason"];

                    XmlTemplate = GetMessageTemplate(bankingrowid.Text, triggerdate.Text, sCurstatus);
                    bReturnStatus = CallCWSFunction("BankingInfoAdaptor.GetBankingStatusHistory", out sreturnValue, XmlTemplate);
                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);
                        //Added by Shivendu for MITS 16148
                        //nadim-17282-start
                        //if (XmlDoc.SelectSingleNode("//BankingStatusHistory/ClaimantName").InnerText == null || XmlDoc.SelectSingleNode("//BankingStatusHistory/ClaimantName").InnerText == "")
                        //{
                        //    lblBankingStatusHistTitle.Text = "Banking Status History [ " + XmlDoc.SelectSingleNode("//BankingStatusHistory/ClaimNumber").InnerText + " ]";
                        //}
                        //else
                        //{

                        //    lblBankingStatusHistTitle.Text = "Banking Status History [ " + XmlDoc.SelectSingleNode("//BankingStatusHistory/ClaimNumber").InnerText
                        //                                   + "*" + XmlDoc.SelectSingleNode("//BankingStatusHistory/ClaimantName").InnerText + "* ]";
                        //}
                        //nadim-17282-end
                        changedby.Text = XmlDoc.SelectSingleNode("//BankingStatusHistory/ChangedBy").InnerText;
                        XmlNodeList statustypelist = XmlDoc.SelectNodes("//BankingStatusHistory/StatusType/level");
                        foreach (XmlNode xStatusCode in statustypelist)
                        {
                            searchtype.Items.Add(new ListItem(xStatusCode.SelectSingleNode("@name").Value, xStatusCode.SelectSingleNode("@id").Value));
                        }

                        XmlNodeList statuslist = XmlDoc.SelectNodes("//BankingStatusHistory/Row");
                        objTable.Columns.Add("Status");
                        objTable.Columns.Add("Date");
                        objTable.Columns.Add("ChangedBy");
                        objTable.Columns.Add("Reason");
                        for (int i = 0; i < statuslist.Count; i++)
                        {
                            xNode = statuslist[i];
                            objRow = objTable.NewRow();
                            objRow["Status"] = xNode.ChildNodes[0].InnerText;
                            objRow["Date"] = xNode.ChildNodes[1].InnerText;
                            objRow["ChangedBy"] = xNode.ChildNodes[2].InnerText;
                            objRow["Reason"] = xNode.ChildNodes[3].InnerText;
                            objTable.Rows.Add(objRow);

                        }
                        objTable.AcceptChanges();
                        GridViewBankStatus.Visible = true;
                        GridViewBankStatus.DataSource = objTable;
                        GridViewBankStatus.DataBind();

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate(string sbankingrowid, string sTriggerDate, string sCodeId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><BankingStatusHistory><BankingRowId>");
            sXml = sXml.Append(sbankingrowid);
            sXml = sXml.Append("</BankingRowId><CodeId>");
            sXml = sXml.Append(sCodeId);
            sXml = sXml.Append("</CodeId>");
             sXml = sXml.Append("<ChangedBy /><ApprovedBy /><Reason /><StatusType /><ChangeDate />");
                        sXml = sXml.Append("</BankingStatusHistory></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
