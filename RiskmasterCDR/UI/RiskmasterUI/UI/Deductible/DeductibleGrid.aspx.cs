﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
//jira# RMA-6135.Multicurrency
using System.Threading;
using System.Globalization;
//jira# RMA-6135.Multicurrency
namespace Riskmaster.UI.UI.Deductible
{
    public partial class DeductibleGrid : NonFDMBasePageCWS
    {
        string sCWSresponse = string.Empty;
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DeductibleGrid.aspx"), "DeductibleGridValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            //ClientScript.RegisterStartupScript(this.GetType(), "DeductibleGridValidations", sValidationResources, true);

            lstDeductables.NeedDataSource += new GridNeedDataSourceEventHandler(lstDeductables_NeedDataSource);
            lstDeductables.ItemDataBound += new GridItemEventHandler(lstDeductables_ItemDataBound);

            if (Request.QueryString["ClaimId"] !=null)
            {
                ClaimId.Text = Request.QueryString["ClaimId"];
            }
        }
        /// <summary>
        /// lstDeductables_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void lstDeductables_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                GridPagerItem pager = (GridPagerItem)e.Item;
                Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                lbl.Visible = false;

                RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                combo.Visible = false;
            }
            else if(e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (item["CoverageGroup"].Controls.Count > 0)
                {
                    HyperLink link = (HyperLink)item["CoverageGroup"].Controls[0];
                    if (string.Compare(link.Text, "NA", true) == 0)
                    {
                        link.Attributes.Add("class", "noLink");
                        //link.ForeColor = System.Drawing.Color.Black;
                    }
                }
                
                
            }
        }
        /// <summary>
        /// lstDeductables_NeedDataSource
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void lstDeductables_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            BindData();
        }
        /// <summary>
        /// BindData
        /// </summary>
        /// <param name=""></param>
        
        private void BindData()
        {
            XElement messageTemplate = GetMessageTemplate();
            XElement xClaimId = messageTemplate.XPathSelectElement("//ClaimId");
            if (xClaimId!=null)
            {
                xClaimId.SetValue(ClaimId.Text);
            }

            CallCWS("DeductibleManagementAdaptor.LoadDeductiblesForClaim", messageTemplate, out sCWSresponse, false, false);
            //jira# RMA-6135.Multicurrency
            if ((BaseCurrency != null) && (!string.IsNullOrEmpty(BaseCurrency.Text)))
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(BaseCurrency.Text);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(BaseCurrency.Text);
            }
            //jira# RMA-6135.Multicurrency
            XmlDocument xDedDetails = null;
            XmlNodeList xDedRows = null;
            DataRow dedDataRow;
            DataSet dedDataSet = new DataSet();
            DataTable dedDataTable = dedDataSet.Tables.Add("Deductibles");

            dedDataTable.Columns.Add("Policy_Deductible_Id", typeof(string));
            dedDataTable.Columns.Add("Policy", typeof(string));
            dedDataTable.Columns.Add("PolicyId", typeof(string));
            dedDataTable.Columns.Add("Unit", typeof(string));
            dedDataTable.Columns.Add("Coverage Type", typeof(string));
            dedDataTable.Columns.Add("SIR/Deductible", typeof(string));
            dedDataTable.Columns.Add("Amount", typeof(decimal));
          
            dedDataTable.Columns.Add("Aggregate", typeof(string));
            dedDataTable.Columns.Add("Coverage_Row_Id", typeof(string));
            dedDataTable.Columns.Add("Claim_Id", typeof(string));
           dedDataTable.Columns.Add("Remaining_Amt", typeof(double));
      
            //aggregate part - start
            dedDataTable.Columns.Add("CovGroupId", typeof(string));
            dedDataTable.Columns.Add("CoverageGroup", typeof(string));
            dedDataTable.Columns.Add("InsuranceLine", typeof(string));
           
            //aggregate part - end
            dedDataTable.Columns.Add("ExcludeExpensePmt", typeof(string));
            dedDataTable.Columns.Add("Diminishing_Type", typeof(string));
            dedDataTable.Columns.Add("ReducedAmount", typeof(string));
            dedDataTable.Columns.Add("EventId", typeof(string));
            dedDataTable.Columns.Add("UnitStateRowId", typeof(string));
            xDedDetails = new XmlDocument();
            xDedDetails.LoadXml(sCWSresponse);
            xDedRows = xDedDetails.SelectNodes("//DEDUCTIBLE");

            foreach (XmlNode xDedRow in xDedRows)
            {
                dedDataRow = dedDataTable.NewRow();
                if (xDedRow.SelectSingleNode("CLM_X_POL_DED_ID") != null)
                {
                    dedDataRow["Policy_Deductible_Id"] = xDedRow.SelectSingleNode("CLM_X_POL_DED_ID").InnerText;
                }
                if (xDedRow.SelectSingleNode("POLICY") != null)
                {
                    dedDataRow["Policy"] = xDedRow.SelectSingleNode("POLICY").InnerText;
                }
                if (xDedRow.SelectSingleNode("UNIT") != null)
                {
                    dedDataRow["Unit"] = xDedRow.SelectSingleNode("UNIT").InnerText;
                }
                if (xDedRow.SelectSingleNode("COVERAGE_TYPE") != null)
                {
                    dedDataRow["Coverage Type"] = xDedRow.SelectSingleNode("COVERAGE_TYPE").InnerText;
                }
                if (xDedRow.SelectSingleNode("SIR_DEDUCTIBLE") != null)
                {
                    dedDataRow["SIR/Deductible"] = xDedRow.SelectSingleNode("SIR_DEDUCTIBLE").InnerText;
                }
                if (xDedRow.SelectSingleNode("AMOUNT") != null)
                {
                    dedDataRow["Amount"] = xDedRow.SelectSingleNode("AMOUNT").InnerText;
                }
                if (xDedRow.SelectSingleNode("REDUCED_AMOUNT") != null)
                {
                    dedDataRow["ReducedAmount"] = xDedRow.SelectSingleNode("REDUCED_AMOUNT").InnerText;
                }
                if (xDedRow.SelectSingleNode("POLCVG_ROW_ID") != null)
                {
                    dedDataRow["Coverage_Row_Id"] = xDedRow.SelectSingleNode("POLCVG_ROW_ID").InnerText;
                }
                if (!string.IsNullOrEmpty(ClaimId.Text))
                {
                    dedDataRow["Claim_Id"] = ClaimId.Text;
                }
                if (xDedRow.SelectSingleNode("REMAINING_AMT") != null)
                {
                    dedDataRow["Remaining_Amt"] = xDedRow.SelectSingleNode("REMAINING_AMT").InnerText;
                }
                if (xDedRow.SelectSingleNode("EXCLUDE_EXPENSE_PAYMENT") != null)
                {
                    dedDataRow["ExcludeExpensePmt"] = xDedRow.SelectSingleNode("EXCLUDE_EXPENSE_PAYMENT").InnerText;
                }
                if (xDedRow.SelectSingleNode("DIMINISHING_TYPE") != null)
                {
                    dedDataRow["Diminishing_Type"] = xDedRow.SelectSingleNode("DIMINISHING_TYPE").InnerText;
                }
                if (xDedRow.SelectSingleNode("EVENT_ID") != null)
                {
                    dedDataRow["EventId"] = xDedRow.SelectSingleNode("EVENT_ID").InnerText;
                }
                //aggregate part - start
                if (xDedRow.SelectSingleNode("COV_GROUP_CODE") != null)
                {
                    dedDataRow["CovGroupId"] = xDedRow.SelectSingleNode("COV_GROUP_CODE").InnerText;
                }
                if (xDedRow.SelectSingleNode("COV_GROUP") != null)
                {
                    dedDataRow["CoverageGroup"] = xDedRow.SelectSingleNode("COV_GROUP").InnerText;
                }
                if (xDedRow.SelectSingleNode("INSURANCE_LINE") != null)
                {
                    dedDataRow["InsuranceLine"] = xDedRow.SelectSingleNode("INSURANCE_LINE").InnerText;
                }
                if (xDedRow.SelectSingleNode("POLICY_ID") != null)
                {
                    dedDataRow["PolicyId"] = xDedRow.SelectSingleNode("POLICY_ID").InnerText;
                }
                if (xDedRow.SelectSingleNode("UNIT_STATE_ROW_ID") != null)
                {
                    dedDataRow["UnitStateRowId"] = xDedRow.SelectSingleNode("UNIT_STATE_ROW_ID").InnerText;
                }
                //aggregate part - end
                dedDataTable.Rows.Add(dedDataRow);
            }

            lstDeductables.DataSource = dedDataSet.Tables["Deductibles"];
            //lstDeductables.DataBind();
            if (xDedDetails.SelectSingleNode("//SHARED_AGG_DED_FLAG") != null)
            {
                if (xDedDetails.SelectSingleNode("//SHARED_AGG_DED_FLAG").InnerText != "-1")
                {
                    lstDeductables.MasterTableView.GetColumn("CoverageGroup").Visible = false;
                }
                else
                {
                    lstDeductables.MasterTableView.GetColumn("CoverageGroup").Visible = true;
                }
            }
            //Start: Added the following code by Sumit Agarwal for NI: 10/14/2014

            //End: Added the following code by Sumit Agarwal for NI: 10/14/2014


            //ddhiman 10/20/2014
            if (xDedDetails.SelectSingleNode("//DIMNISHING_FLAG") != null)
            {
                if (xDedDetails.SelectSingleNode("//DIMNISHING_FLAG").InnerText != "-1")
                {
                    lstDeductables.MasterTableView.GetColumn("Diminishing_Type").Visible = false;
                    lstDeductables.MasterTableView.GetColumn("ReducedAmount").Visible = false;
                }
                else
                {
                    lstDeductables.MasterTableView.GetColumn("Diminishing_Type").Visible = true;
                    lstDeductables.MasterTableView.GetColumn("ReducedAmount").Visible = true;
                }
            }
            //End ddhiman

            dedDataTable.Dispose();
            dedDataSet.Dispose();
        }
        /// <summary>
        /// GetMessageTemplate
        /// </summary>
        /// <param name=""></param>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <ClaimId></ClaimId>
                    <CLM_X_POL_DED_ID></CLM_X_POL_DED_ID>
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            //string sRedirectString = String.Format("../Reserves/ReserveListingBOB.aspx");//?ClaimId=" + ClaimId.Value
            //Server.Transfer(sRedirectString);
            
        }
    }
}