﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="DeductibleHistory.aspx.cs" Inherits="Riskmaster.UI.UI.Deductible.DeductbleHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmDeductibleHistory" runat="server">
    <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            
            <!-- Deepak Dhiman added, 10/14/2014 -->
            <tr>
                <td colspan="2" align="left">
                    <asp:ImageButton ID="btnBack" ImageUrl="~/Images/tb_backtosummary_active.png" class="bold" ToolTip="<%$ Resources:btnBack %>"
                        runat="server" onclick="btnBack_Click"/>
                </td>
            </tr>
            <!-- End Deepak Dhiman added, 10/14/2014 -->

            <tr>
            <td colspan="10" class="msgheader" bgcolor="#D5CDA4">
                    <asp:Label ID="lblDeductibleHistoryHeader" runat="server" Text="<%$ Resources:lblDeductibleHistoryHeader %>"></asp:Label>
            </td></tr>

<tr><td>
    <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
        <script type="text/javascript">
            function GridCreated(sender, eventArgs) {
                //<!-- If Height of all rows is less than the height of grid - then decrease the height of the grid -->
                var scrollArea = sender.GridDataDiv;
                var availableArea = sender.get_masterTableView().get_element();
                if (scrollArea != null && availableArea != null) {
                    if (availableArea.clientHeight <= parseInt(sender.ClientSettings.Scrolling.ScrollHeight)) {
                        //Added 1 to remove scroll bars
                        scrollArea.style.height = availableArea.clientHeight + 1 + "px";

                        if (scrollArea.clientHeight<300) {
                            scrollArea.style.height = "300px"
                        }
                    }
                }
            }
        </script>
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        
    </telerik:RadAjaxManager>

    <telerik:RadGrid runat="server" ID="grdDedHistory"
        AllowPaging="true" PageSize="10" AllowFilteringByColumn="false" AllowSorting="false"
        AutoGenerateColumns="false">
        <ClientSettings>
                <ClientEvents OnGridCreated="GridCreated" />                    
                <Scrolling SaveScrollPosition="true" AllowScroll="true" ScrollHeight="500px"/>
        </ClientSettings>

        <MasterTableView Width="100%" AllowCustomSorting="false" ClientDataKeyNames="DedType">
             <PagerStyle AlwaysVisible="true" Position="Top" mode="NextPrevAndNumeric"/>
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="DedType" HeaderText="<%$ Resources:lblDeductibleType %>" UniqueName="DedType">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Amount" HeaderText="<%$ Resources:lblAmount %>" UniqueName="Amount">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ReducedAmount" HeaderText="<%$ Resources:lblReducedAmt %>" UniqueName="ReducedAmount">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DiminishingType" HeaderText="<%$ Resources:lblDiminishingType %>" UniqueName="DiminishingType" >
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ExcludeExpensePmt" HeaderText="<%$ Resources:lblExclExpensePmt %>" UniqueName="ExcludeExpensePmt">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DimPercent" HeaderText="<%$ Resources:lblDiminishingPercentage %>" UniqueName="DimPercent">
                </telerik:GridBoundColumn>
                <%--Commented by Nikhil on 09/18/14--%>
              <%--  <telerik:GridBoundColumn DataField="Claimant" HeaderText="<%$ Resources:lblClaimant %>" UniqueName="Claimant">
                </telerik:GridBoundColumn>--%>
                <telerik:GridBoundColumn DataField="User" HeaderText="<%$ Resources:lblUser %>" UniqueName="User">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DateChanged" HeaderText="<%$ Resources:lblDateChanged %>" UniqueName="DateChanged">
                </telerik:GridBoundColumn>             
            </Columns>
        </MasterTableView>
            
    </telerik:RadGrid>

    </td></tr>
     <tr>
    <td><%-- ddhiman commented 10/14/2014
        <asp:Button ID="btnBack" Text="<%$ Resources:btnBack %>" runat="server" 
            class="button" onclick="btnBack_Click" />--%></td>
    <td></td>
    </tr>
        </table>
    </div>
    </form>
</body>
</html>
