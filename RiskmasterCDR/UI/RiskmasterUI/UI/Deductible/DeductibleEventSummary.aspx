﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="DeductibleEventSummary.aspx.cs" Inherits="Riskmaster.UI.UI.Deductible.DeductibleEventSummary" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function GridCreated(sender, eventArgs) {
            //<!-- If Height of all rows is less than the height of grid - then decrease the height of the grid -->
            var scrollArea = sender.GridDataDiv;
            var availableArea = sender.get_masterTableView().get_element();
            if (scrollArea != null && availableArea != null) {
                if (availableArea.clientHeight <= parseInt(sender.ClientSettings.Scrolling.ScrollHeight)) {
                    //Added 1 to remove scroll bars
                    scrollArea.style.height = availableArea.clientHeight + 1 + "px";
                }
                if (scrollArea.clientHeight < 100) {
                    scrollArea.style.height = "100px"
                }
                if (scrollArea.clientHeight > 200) {
                    scrollArea.style.height = "200px"
                }
            }
        }
        function SubGridCreated(sender, eventArgs) {
            //<!-- If Height of all rows is less than the height of grid - then decrease the height of the grid -->
            var scrollArea = sender.GridDataDiv;
            var availableArea = sender.get_masterTableView().get_element();
            if (scrollArea != null && availableArea != null) {
                if (availableArea.clientHeight <= parseInt(sender.ClientSettings.Scrolling.ScrollHeight)) {
                    //Added 1 to remove scroll bars
                    scrollArea.style.height = availableArea.clientHeight + 1 + "px";
                }
                
            }
        }

        ///Code from:
        ///http ://www.telerik.com/community/code-library/aspnet-ajax/grid/single-radiobutton-check-at-a-time-with-row-selection.aspx
        function selectSingleRadio(objRadioButton, grdName) {
            var i, obj, pageElements;

            if (navigator.userAgent.indexOf("MSIE") != -1) {
                //IE browser  
                pageElements = document.all;
            }
            else if (navigator.userAgent.indexOf("Mozilla") != -1 || navigator.userAgent.indexOf("Opera") != -1) {
                //FireFox/Opera browser  
                pageElements = document.documentElement.getElementsByTagName("input");
            }
            for (i = 0; i < pageElements.length; i++) {
                obj = pageElements[i];

                if (obj.type == "radio") {
                    if (objRadioButton.id.substr(0, grdName.length) == grdName) {
                        if (objRadioButton.id == obj.id) {
                            obj.checked = true;
                        }
                        else {
                            obj.checked = false;
                        }
                    }
                }
            }
        }

        function BackToFinancials() {
            var txtClaimId = document.getElementById('txtClaimId');
            if (txtClaimId != null) {
                window.location.href = '/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=' + txtClaimId.value;
            }
            return false;
        }
               
    </script> 
</head>
<body onload="parent.MDIScreenLoaded();">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <form id="frmDeductibleEventSummary" runat="server">
    <div>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">

        <!-- Deepak Dhiman added, 10/14/2014 -->
            <tr>
                <td colspan="2" align="left">
                    <asp:ImageButton ID="btnBack" ImageUrl="~/Images/tb_backtofinancials_active.png" class="bold" ToolTip="<%$ Resources:btnBack %>"
                        runat="server" onClientClick="return BackToFinancials()"/>
                </td>
            </tr>
            <!-- End Deepak Dhiman added, 10/14/2014 -->

        <tr>
            <td colspan="10" class="msgheader" bgcolor="#D5CDA4">
                <asp:Label ID="lblDeductibleGridEventHeader" runat="server" Text="<%$ Resources:lblDeductibleGridEventHeader %>"></asp:Label>
            </td>
        </tr>

        <tr><td>
        <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="lstDed">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="lstDed" />
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadGrid runat="server" ID="gvEventDeductables"
        AllowPaging="true" PageSize="8" AllowFilteringByColumn="false" AllowSorting="true"
        AutoGenerateColumns="false" ItemStyle-CssClass="rowlight1" AlternatingItemStyle-CssClass="rowlight2" >
            <SelectedItemStyle CssClass="SelectedItem"  />
            <ClientSettings AllowKeyboardNavigation="true">
                     <ClientEvents  OnGridCreated="GridCreated"/>
                      <Scrolling SaveScrollPosition="true" AllowScroll="true" ScrollHeight="500px" />
             </ClientSettings>
             <MasterTableView Width="100%" ClientDataKeyNames="ClaimNumber">
             <PagerStyle AlwaysVisible="true" Position="Top" mode="NextPrevAndNumeric"/>
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <NoRecordsTemplate>
                  <div>
                    There are no records to display.
                  </div>
                </NoRecordsTemplate>
                <Columns>
                    <telerik:GridTemplateColumn UniqueName="grdRadio">
                        <ItemTemplate>
                            <asp:RadioButton ID="gdRadio" runat="server" AutoPostBack="true" OnCheckedChanged="grdRadio_CheckedChanged"/>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn  HeaderText="<%$ Resources:lblClaimId %>" DataField="ClaimId" UniqueName="ClaimId"
                        SortExpression="ClaimId" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn  HeaderText="<%$ Resources:lblCvgGroup %>" DataField="CoverageGroup" UniqueName="CoverageGroup"
                        SortExpression="CoverageGroup" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn  HeaderText="<%$ Resources:lblClaimNumber %>" DataField="ClaimNumber" UniqueName="ClaimNumber"
                        SortExpression="ClaimNumber" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn  HeaderText="<%$ Resources:lblCvgGroupCode %>" DataField="CoverageGroupCode" UniqueName="CoverageGroupCode"
                        SortExpression="CoverageGroupCode" Visible="false">
                    </telerik:GridBoundColumn>

                    <telerik:GridNumericColumn  HeaderText="<%$ Resources:lblTotalPaid %>" DataField="TotalPaid" UniqueName="TotalPaid" DataType="System.Decimal" NumericType="Currency"
                        SortExpression="TotalPaid" >
                    </telerik:GridNumericColumn>

                    
                    <telerik:GridNumericColumn  HeaderText="<%$ Resources:lblTotalRecovery %>" DataField="TotalRecovery" UniqueName="TotalRecovery" DataType="System.Decimal" NumericType="Currency"
                        SortExpression="TotalRecovery" >
                    </telerik:GridNumericColumn>

                     

                    <telerik:GridNumericColumn  HeaderText="<%$ Resources:lblDedPerEvent %>" DataField="DeductiblePerEvent" UniqueName="DeductiblePerEvent" DataType="System.Decimal" NumericType="Currency"
                        SortExpression="DeductiblePerEvent" >
                    </telerik:GridNumericColumn>


                    <telerik:GridNumericColumn  HeaderText="<%$ Resources:lblDeductibleApplied %>" DataField="DeductibleApplied" UniqueName="DeductibleApplied" DataType="System.Decimal" NumericType="Currency"
                        SortExpression="DeductibleApplied" >
                    </telerik:GridNumericColumn>

                </Columns>
             </MasterTableView>
    </telerik:RadGrid>
    </td></tr>
    <tr>
            <td colspan="10" class="msgheader" bgcolor="#D5CDA4">
                <asp:Label ID="lblDeductibleGridClaimHeader" runat="server" Text="<%$ Resources:lblDeductibleGridClaimHeader %>" Visible="false"></asp:Label>
            </td>
    </tr>
    <tr>
        <td>
            <telerik:RadGrid runat="server" ID="gvDedutible"
        AllowPaging="true" PageSize="12" AllowFilteringByColumn="false" AllowSorting="true"
        AutoGenerateColumns="false" ItemStyle-CssClass="rowlight1" AlternatingItemStyle-CssClass="rowlight2" >
            <SelectedItemStyle CssClass="SelectedItem"  />
            <ClientSettings AllowKeyboardNavigation="true">
                     <ClientEvents  OnGridCreated="SubGridCreated"/>
                      <Scrolling SaveScrollPosition="true" AllowScroll="true" ScrollHeight="500px" />
             </ClientSettings>
             <MasterTableView Width="100%" ClientDataKeyNames="Policy">
             <PagerStyle AlwaysVisible="true" Position="Top" mode="NextPrevAndNumeric"/>
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <NoRecordsTemplate>
                  <div>
                    There are no records to display.
                  </div>
                </NoRecordsTemplate>
                <Columns>
                     <telerik:GridBoundColumn  HeaderText="<%$ Resources:lblPolicy %>" UniqueName="Policy" DataField="Policy"
                        SortExpression="Policy" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn  HeaderText="<%$ Resources:lblInsLine %>" UniqueName="InsLine" DataField="InsLine"
                        SortExpression="InsLine" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn  HeaderText="<%$ Resources:lblUnit %>" UniqueName="Unit" DataField="Unit"
                        SortExpression="Unit" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn  HeaderText="<%$ Resources:lblCoverage %>" UniqueName="Coverage" DataField="Coverage"
                        SortExpression="Coverage" >
                    </telerik:GridBoundColumn>
                   
                    <telerik:GridNumericColumn  HeaderText="<%$ Resources:lblTotalPaid %>" DataField="TotalPaid" UniqueName="TotalPaid" DataType="System.Decimal" NumericType="Currency"
                        SortExpression="TotalPaid" >
                    </telerik:GridNumericColumn>
                    

                    <telerik:GridNumericColumn  HeaderText="<%$ Resources:lblTotalRecovery %>" DataField="TotalRecovery" UniqueName="TotalRecovery" DataType="System.Decimal" NumericType="Currency"
                        SortExpression="TotalRecovery" >
                    </telerik:GridNumericColumn>

                    <telerik:GridNumericColumn  HeaderText="<%$ Resources:lblDeductibleApplied %>" DataField="DeductibleApplied" UniqueName="DeductibleApplied" DataType="System.Decimal" NumericType="Currency"
                        SortExpression="DeductibleApplied" >
                    </telerik:GridNumericColumn>                  
               
                </Columns>
             </MasterTableView>
    </telerik:RadGrid>
        </td>
    </tr>

    <tr><td>
  
    </td></tr>
        <tr><td>
        
    </td></tr>
    </table>
    <asp:TextBox ID="txtClaimId" runat="server" style="display:none" rmxref="/Instance/Document/Deductible/ClaimId" />
          <%--jira# RMA-6135.Multicurrency--%>
        <asp:TextBox runat="server" id="BaseCurrency" Style="display:none" rmxref="/Instance/Deductible/BaseCurrencyType"></asp:TextBox>
    </div>
    <div id="PageValues" style="display:none">
        <input type="text" runat="server" id="bBindSuppGrid" />
        <input type="text" runat="server" id="SelectedClaimId" />
        <input type="text" runat="server" id="SelectedCovGroup" />
    </div>
    </form>
</body>
</html>
