﻿using System;
using System.Web.UI;
using System.Xml;
using System.Xml.Linq;
using System.Threading;
using System.Globalization;
namespace Riskmaster.UI.UI.Deductible
{
    public partial class DeductibleGroupDetails : NonFDMBasePageCWS
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bSuccess = false;
            if ((BaseCurrency != null) && (string.IsNullOrEmpty(BaseCurrency.Text)))
            {
                string sCWSResponse = string.Empty;
                CallCWS("RMUtilitiesAdaptor.GetBaseCurrency", GetMessageTemplate(), out sCWSResponse, false, false);
                if ((BaseCurrency != null) && (!string.IsNullOrEmpty(BaseCurrency.Text)))
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(BaseCurrency.Text);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(BaseCurrency.Text);
                }
            }

            if (!IsPostBack)
            {
                if (Request.QueryString["CovGroupId"] != null)
                {
                    CoverageGroupId.Text = Request.QueryString["CovGroupId"];
                    CovGroupId.CodeId = CoverageGroupId.Text;
                }
                if (Request.QueryString["ClaimId"] != null)
                {
                    txtClaimId.Text = Request.QueryString["ClaimId"];
                }
                if (Request.QueryString["ClaimId"] != null)
                {
                    claimid.Text = Request.QueryString["ClaimId"];
                }
                if (Request.QueryString["PolicyId"] != null)
                {
                    txtPolicyId.Text = Request.QueryString["PolicyId"];
                }
                if (Request.QueryString["EventId"] != null)
                {
                    txtEventId.Text = Request.QueryString["EventId"];
                }
                if (Request.QueryString["ClmXPolDedId"] != null)
                {
                    txtDeductibleId.Text = Request.QueryString["ClmXPolDedId"];
                }
                if (Request.QueryString["UnitStateRowId"] != null)
                {
                //string sUnitStateRowId=string.Empty;
                //sUnitStateRowId = Request.QueryString["UnitStateRowId"];
                //CovGroupId.Filter=CovGroupId.Filter.Replace("UnitStateRowId", sUnitStateRowId);
                    txtCovGroupUnitStateRowId.Text = Request.QueryString["UnitStateRowId"];
                }

               
                            
            }
            else
            {
                //CoverageGroupId.Text = CovGroupId.CodeId;
            }

            if (!IsPostBack || !string.IsNullOrEmpty(hdn_FunctionToCall.Text))
            {
                LoadInitialData();
            }
        }
        /// <summary>
        /// LoadInitialData
        /// </summary>
         private void LoadInitialData()
        {
            XmlDocument xResponse = null;
            XmlNode xPolGrpId = null;
            string sResponse = string.Empty;
            CallCWS("DeductibleManagementAdaptor.GetAggregateDetails", null, out sResponse, true, true);

            xResponse = new XmlDocument();
            xResponse.LoadXml(sResponse);
            xPolGrpId = xResponse.SelectSingleNode("//Pol_Grp_Id");
            if (xPolGrpId != null)
            {
                //Start:added by Nitin goel, For PCRs Changes.
                if (xPolGrpId.Attributes["IsDedPerEventEnabled"] != null)
                {
                    if (string.Compare(xPolGrpId.Attributes["IsDedPerEventEnabled"].Value, "0") == 0)
                    {
                        txtDedPerEvent.ReadOnly = true;
                    }
                    else
                    {
                        txtDedPerEvent.ReadOnly = false;
                    }
                }
                //End: added by Nitin goel, For PCRs changes.
                if (xPolGrpId.Attributes["IsEditAllowed"] != null)
                {
                    if (string.Compare(xPolGrpId.Attributes["IsEditAllowed"].Value, "0") == 0)
                    {
                        CovGroupId.Enabled = false;
                        txtAggLimit.ReadOnly = true;
                        txtDedPerEvent.ReadOnly = true;
                        txtAggBalance.ReadOnly = true;
                        cbSetAtlevel.Enabled = false;
                        chkExcludeExpense.Enabled = false;
                    }
                }
               
            }
        }
        /// <summary>
         /// btnSave_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            if (CallCWSFunction("DeductibleManagementAdaptor.SaveAggregateDetails"))
            {
                CoverageGroupId.Text = CovGroupId.CodeId;
            }
        }
        /// <summary>
        /// btnBack_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            string sRedirectString = String.Format("../Deductible/DeductibleGrid.aspx");
            Server.Transfer(sRedirectString);
        }
        /// <summary>
        /// btnHistory_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnHistory_Click(object sender, EventArgs e)
        {
            string sRedirectString = String.Format(string.Format("./DeductbleAggregateHistory.aspx?ClaimId={0}&CovGroupId={1}", txtClaimId.Text, CoverageGroupId.Text));
            Server.Transfer(sRedirectString);
        }
        /// <summary>
        /// GetMessageTemplate
        /// </summary>
        /// <param name="xElement"></param>
        public override void ModifyXml(ref XElement xElement)
        {
            
            base.ModifyXml(ref xElement);
        }
        private XElement GetMessageTemplate()
        {
            string sMesage = @"<Message><Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization><Call><Function /></Call><Document><BaseCurrencyType></BaseCurrencyType></Document></Message>";
            return XElement.Parse(sMesage);
        }
    }
}