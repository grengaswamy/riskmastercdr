﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Xml.Linq;
using System.Xml;

namespace Riskmaster.UI.UI.Deductible
{
    public partial class DeductbleHistory : NonFDMBasePageCWS
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            grdDedHistory.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(grdDedHistory_NeedDataSource);
            grdDedHistory.ItemDataBound += new Telerik.Web.UI.GridItemEventHandler(grdDedHistory_ItemDataBound);

            
        }
        /// <summary>
        /// grdDedHistory_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void grdDedHistory_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                GridPagerItem pager = (GridPagerItem)e.Item;
                Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                lbl.Visible = false;

                RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                combo.Visible = false;
            }
        }
        /// <summary>
        /// grdDedHistory_NeedDataSource
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void grdDedHistory_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            BindData();
        }
        /// <summary>
        /// BindData
        /// </summary>
        private void BindData()
        {
            string sCWSResponse = string.Empty;
            string sClaimXPolDedId = string.Empty;
            XmlDocument xDoc = null;
            DataRow dedDataRow;
            XmlNodeList xRecords = null;

            if (Request.QueryString["DedId"] != null)
            {
                sClaimXPolDedId = Request.QueryString["DedId"];
            }

            if (!string.IsNullOrEmpty(sClaimXPolDedId))
            {
                CallCWS("DeductibleManagementAdaptor.GetDeductibleHistory", GetMessageTemplate(sClaimXPolDedId), out sCWSResponse, false, false);
            }
            xDoc = new XmlDocument();
            xDoc.LoadXml(sCWSResponse);
            
            DataSet dedDataSet = new DataSet();
            DataTable dedDataTable = dedDataSet.Tables.Add("DeductibleHistory");

            dedDataTable.Columns.Add("DateChanged", typeof(string));
            dedDataTable.Columns.Add("DedType", typeof(string));
            dedDataTable.Columns.Add("User", typeof(string));
            dedDataTable.Columns.Add("Amount", typeof(string));
            dedDataTable.Columns.Add("ExcludeExpensePmt", typeof(string));
            dedDataTable.Columns.Add("DiminishingType", typeof(string));
            dedDataTable.Columns.Add("DimPercent", typeof(string));
            //Commented by Nikhil on 09/18/14
            //dedDataTable.Columns.Add("Claimant", typeof(string));
            dedDataTable.Columns.Add("ReducedAmount", typeof(string));

            xRecords = xDoc.SelectNodes("//RECORD");
            foreach (XmlNode xRecord in xRecords)
            {
                dedDataRow = dedDataTable.NewRow();
                if (xRecord.SelectSingleNode("DATE_DATA_CHANGED") != null)
                {
                    dedDataRow["DateChanged"] = xRecord.SelectSingleNode("DATE_DATA_CHANGED").InnerText;
                }
                if (xRecord.SelectSingleNode("DEDUCTIBLE_TYPE") != null)
                {
                    dedDataRow["DedType"] = xRecord.SelectSingleNode("DEDUCTIBLE_TYPE").InnerText;
                }
                if (xRecord.SelectSingleNode("USER") != null)
                {
                    dedDataRow["User"] = xRecord.SelectSingleNode("USER").InnerText;
                }
                if (xRecord.SelectSingleNode("AMOUNT") != null)
                {
                    dedDataRow["Amount"] = xRecord.SelectSingleNode("AMOUNT").InnerText;
                }
                if (xRecord.SelectSingleNode("REDUCED_AMOUNT") != null)
                {
                    dedDataRow["ReducedAmount"] = xRecord.SelectSingleNode("REDUCED_AMOUNT").InnerText;
                }
                if (xRecord.SelectSingleNode("EXCLUDE_EXPENSE_FLAG") != null)
                {
                    if (string.Compare(xRecord.SelectSingleNode("EXCLUDE_EXPENSE_FLAG").InnerText,"-1")==0)
                    {
                        dedDataRow["ExcludeExpensePmt"] = "Yes";
                    }
                    else
                    {
                        dedDataRow["ExcludeExpensePmt"] = "No";
                    }
                }
                if (xRecord.SelectSingleNode("DIMINISHING_TYPE") != null)
                {
                    dedDataRow["DiminishingType"] = xRecord.SelectSingleNode("DIMINISHING_TYPE").InnerText;
                }
                if (xRecord.SelectSingleNode("DIMINISHING_PERCENT") != null)
                {
                    dedDataRow["DimPercent"] = string.Format("{0:f2}", xRecord.SelectSingleNode("DIMINISHING_PERCENT").InnerText);
                }
                //start - Commented by Nikhil on 09/18/14
                //if (xRecord.SelectSingleNode("CLAIMANT") != null)
                //{
                //    dedDataRow["Claimant"] = xRecord.SelectSingleNode("CLAIMANT").InnerText;
                //}
                //end - Commented by Nikhil on 09/18/14
                dedDataTable.Rows.Add(dedDataRow);
            }

            grdDedHistory.DataSource = dedDataSet.Tables["DeductibleHistory"];


            //ddhiman 10/20/2014
            if (xDoc.SelectSingleNode("//DIMNISHING_FLAG") != null)
            {
                if (xDoc.SelectSingleNode("//DIMNISHING_FLAG").InnerText != "-1")
                {
                    dedDataTable.Columns.Remove("DiminishingType");
                    dedDataTable.Columns.Remove("DimPercent");
                    dedDataTable.Columns.Remove("ReducedAmount");
                }
            }
            //End ddhiman

            dedDataTable.Dispose();
            dedDataSet.Dispose();
        }
        /// <summary>
        /// btnBack_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            string sRedirectString = String.Format("../Deductible/DeductibleGrid.aspx");
            Server.Transfer(sRedirectString);
        }
        /// <summary>
        /// GetMessageTemplate
        /// </summary>
        /// <param name="sClaimXPolDedId"></param>
        private XElement GetMessageTemplate(string sClaimXPolDedId)
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <CLM_X_POL_DED_ID>"
                + sClaimXPolDedId
                +@"</CLM_X_POL_DED_ID>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
    }
}