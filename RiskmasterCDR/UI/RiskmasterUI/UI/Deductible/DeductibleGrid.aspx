﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="DeductibleGrid.aspx.cs" Inherits="Riskmaster.UI.UI.Deductible.DeductibleGrid" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../../Scripts/jquery-1.4.2.js"></script>
    <script type="text/javascript">
        function GridCreated(sender, eventArgs) {
            //<!-- If Height of all rows is less than the height of grid - then decrease the height of the grid -->
            var scrollArea = sender.GridDataDiv;
            var availableArea = sender.get_masterTableView().get_element();
            if (scrollArea != null && availableArea != null) {
                if (availableArea.clientHeight <= parseInt(sender.ClientSettings.Scrolling.ScrollHeight)) {
                    //Added 1 to remove scroll bars
                    scrollArea.style.height = availableArea.clientHeight + 1 + "px";
                }
            }
        }
        $(document).ready(function () {
            $('a.noLink').removeAttr('href');

            $('#btnBack').click(function () {
                window.location.href = '/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=' + $('#ClaimId')[0].value;
                return false;
            });
        });
        
    </script>
     <style type="text/css">
        .GridViewFixedHeader
        {
            /* So the overflow scrolls */
            overflow: auto;
        }
        .GridViewFixedHeader table th
        {
            /* Keep the header cells positioned as we scroll */ /*position:relative; */
            position: relative;
            top: expression(document.getElementById("GridViewFixedHeader").scrollTop-2);
            z-index: 10;
        }
        .GridViewFixedHeader table tbody
        {
            /* Keep the header cells positioned as we scroll */ /*position:relative; */
            overflow-x: hidden;
        }
        .SelectedItem
        {
            background: none repeat scroll 0 0 #6699FF !important;
        }
        a.noLink
        {
            text-decoration: none;
        }
        </style>
</head>
<body onload="parent.MDIScreenLoaded();">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <form id="frmDeductibleGrid" runat="server">
    <div>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        
        <!-- Deepak Dhiman added, 10/14/2014 -->
            <tr>
                <td colspan="2" align="left">
                    <asp:ImageButton ID="btnBack" ImageUrl="~/Images/tb_backtofinancials_active.png" class="bold" ToolTip="<%$ Resources:btnBack %>"
                        runat="server"/>
                </td>
            </tr>
            <!-- End Deepak Dhiman added, 10/14/2014 -->
        
        <tr>
    <td colspan="10" class="msgheader" bgcolor="#D5CDA4">
                    <asp:Label ID="lblDeductibleGridHeader" runat="server" Text="<%$ Resources:lblDeductibleGridHeader %>"></asp:Label>
                </td></tr>
    <tr><td>
    
    <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="lstDed">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="lstDed" />
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
    </telerik:RadAjaxManager>

    <telerik:RadGrid runat="server" ID="lstDeductables"
        AllowPaging="true" PageSize="8" Skin="Office2007" AllowFilteringByColumn="true" AllowSorting="true"
        AutoGenerateColumns="false">
            <SelectedItemStyle CssClass="SelectedItem"  />
            <ClientSettings AllowKeyboardNavigation="true">
                     <ClientEvents OnGridCreated="GridCreated" />
                      <Scrolling SaveScrollPosition="true" AllowScroll="true" ScrollHeight="500px" />
             </ClientSettings>
             <MasterTableView Width="100%" ClientDataKeyNames="Policy">
             <PagerStyle AlwaysVisible="true" Position="Top" mode="NextPrevAndNumeric"/>
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridHyperLinkColumn DataTextField="Policy" HeaderText="<%$ Resources:lblPolicy %>" UniqueName="Policy" 
                        SortExpression="Policy" DataNavigateUrlFields="Policy_Deductible_Id,Coverage_Row_Id,Claim_Id" DataNavigateUrlFormatString="~/UI/FDM/deductibledetail.aspx?recordID={0}&parentID={1}">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn DataField="PolicyId" HeaderText="Policy_Id" UniqueName="PolicyId"
                        SortExpression="PolicyId" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Policy_Deductible_Id" HeaderText="Policy_Deductible_Id" UniqueName="Policy_Deductible_Id"
                        SortExpression="Policy_Deductible_Id" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Unit" HeaderText="<%$ Resources:lblUnit %>" UniqueName="Unit"
                        SortExpression="Unit" ItemStyle-Wrap="true"  >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Coverage Type" HeaderText="<%$ Resources:lblCvgType %>" UniqueName="Coverage Type"
                        SortExpression="Coverage Type" FilterControlWidth="40px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SIR/Deductible" HeaderText="<%$ Resources:lblDeductible %>" UniqueName="SIR/Deductible"
                        SortExpression="SIR/Deductible" FilterControlWidth="30px">
                    </telerik:GridBoundColumn>
                    <telerik:GridNumericColumn DataField="Amount" DataType="System.Decimal" NumericType="Currency"  
                          HeaderText="<%$ Resources:lblAmount %>" SortExpression="Amount" UniqueName="Amount" FilterControlWidth="30px"> 
                    </telerik:GridNumericColumn> 
                
                    <telerik:GridBoundColumn DataField="ReducedAmount" HeaderText="<%$ Resources:lblReducedAmt %>" UniqueName="ReducedAmount"
                        SortExpression="ReducedAmount" FilterControlWidth="30px">
                    </telerik:GridBoundColumn>
               
                    <telerik:GridBoundColumn DataField="Coverage_Row_Id" HeaderText="Coverage_Row_Id" UniqueName="Coverage_Row_Id"
                        SortExpression="Coverage_Row_Id" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Claim_Id" HeaderText="Claim_Id" UniqueName="Claim_Id"
                        SortExpression="Claim_Id" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EventId" HeaderText="EventId" UniqueName="EventId"
                        SortExpression="EventId" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridNumericColumn DataField="Remaining_Amt" DataType="System.Decimal" NumericType="Currency"  
                          HeaderText="<%$ Resources:lblRemainingAmt %>" SortExpression="Remaining_Amt" UniqueName="Remaining_Amt" FilterControlWidth="30px"> 
                    </telerik:GridNumericColumn>
                    
                    <telerik:GridBoundColumn DataField="Diminishing_Type" HeaderText="<%$ Resources:lblDiminishingType %>" UniqueName="Diminishing_Type"
                        SortExpression="Diminishing_Type" FilterControlWidth="30px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="InsuranceLine" HeaderText="<%$ Resources:lblInsuranceLine %>" UniqueName="InsuranceLine"
                        SortExpression="InsuranceLine" FilterControlWidth="30px">
                    </telerik:GridBoundColumn>
                    <telerik:GridHyperLinkColumn DataTextField="CoverageGroup" HeaderText="<%$ Resources:lblGroupId %>" UniqueName="CoverageGroup" FilterControlWidth="30px"
                        SortExpression="CoverageGroup" DataNavigateUrlFields="CovGroupId,Claim_Id,Policy_Deductible_Id,EventId,PolicyId,UnitStateRowId" DataNavigateUrlFormatString="~/UI/Deductible/DeductibleGroupDetails.aspx?CovGroupId={0}&ClaimId={1}&ClmXPolDedId={2}&EventId={3}&PolicyId={4} &UnitStateRowId={5}">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn DataField="CovGroupId" HeaderText="" UniqueName="CovGroupId" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="UnitStateRowId" HeaderText="" UniqueName="UnitStateRowId" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ExcludeExpensePmt" HeaderText="<%$ Resources:lblExcludeExpensePmt %>" UniqueName="ExcludeExpensePmt"
                       SortExpression="ExcludeExpensePmt" FilterControlWidth="30px">
                    </telerik:GridBoundColumn>
                    <telerik:GridHyperLinkColumn HeaderText="<%$ Resources:lblHistory %>" ImageUrl="~/UI/../Images/button_deductibleHist_active.png"
                        DataNavigateUrlFields="Policy_Deductible_Id,Coverage_Row_Id,Claim_Id" Text="<%$ Resources:lblHistoryToolTip %>"
                        DataNavigateUrlFormatString="~/UI/Deductible/DeductibleHistory.aspx?DedId={0}&CvgId={1}&ClaimId={2}" AllowFiltering="false"/>
                </Columns>
             </MasterTableView>
    </telerik:RadGrid>
    </td></tr>
    <tr>
    <td colspan="6" class="ctrlgroup2" bgcolor="">
                    <asp:Label ID="Label1" runat="server" ForeColor="#D5CDA4" Text=""></asp:Label>
                </td>
    </tr>
    <tr>
    <td></td>
    <td></td>
    </tr>
    <tr>
        <td><asp:TextBox Style="display: none" rmxref="/Instance/Document/PaymentHistory/ClaimId"
        ID="ClaimId" runat="server" rmxignoreget="true"></asp:TextBox>
            <%--jira# RMA-6135.Multicurrency--%>
            <asp:TextBox runat="server" id="BaseCurrency" Style="display:none" rmxref="/Instance/Document/BaseCurrencyType"></asp:TextBox>
        </td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
