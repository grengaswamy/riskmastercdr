﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Xml;
using System.Data;
using Riskmaster.Common;
using System.Xml.Linq;
//jira# RMA-6135.Multicurrency
using System.Threading;
using System.Globalization;
//jira# RMA-6135.Multicurrency
namespace Riskmaster.UI.UI.Deductible
{
    public partial class DeductibleEventSummary : NonFDMBasePageCWS
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            gvEventDeductables.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(gvEventDeductables_NeedDataSource);
            gvEventDeductables.ItemDataBound += new Telerik.Web.UI.GridItemEventHandler(gvEventDeductables_ItemDataBound);

            gvDedutible.NeedDataSource += new GridNeedDataSourceEventHandler(gvDedutible_NeedDataSource);
            gvDedutible.ItemDataBound += new GridItemEventHandler(gvDedutible_ItemDataBound);

            if (!IsPostBack)
            {
                bBindSuppGrid.Value = "false";
                SelectedClaimId.Value = "0";
                SelectedCovGroup.Value = "0";
                txtClaimId.Text = AppHelper.GetQueryStringValue("ClaimId");
            }
        }
        /// <summary>
        /// gvDedutible_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void gvDedutible_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                GridPagerItem pager = (GridPagerItem)e.Item;
                Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                lbl.Visible = false;

                RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                combo.Visible = false;
            }
        }
        /// <summary>
        /// gvEventDeductables_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void gvEventDeductables_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                GridPagerItem pager = (GridPagerItem)e.Item;
                Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                lbl.Visible = false;

                RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                combo.Visible = false;
            }
            else if (e.Item is GridDataItem)
            {
                RadioButton grdRadio = null;
                GridDataItem item = e.Item as GridDataItem;
                Control radioControl = item["grdRadio"];
                if (radioControl != null)
                {
                    grdRadio = radioControl.FindControl("gdRadio") as RadioButton;
                    if (grdRadio != null)
                    {
                        grdRadio.Attributes.Add("OnClick", "selectSingleRadio(" + grdRadio.ClientID + ", " + "'gvEventDeductables'" + ") ");
                    }
                }
            }

        }
        /// <summary>
        /// gvEventDeductables_NeedDataSource
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void gvEventDeductables_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            BindData();
        }
        /// <summary>
        /// BindData
        /// </summary>
        /// <param name=""></param>
     private void BindData()
        {
            string sCWSResponse = string.Empty;
            XmlDocument xDoc = null;
            DataRow eventDedRow;
            XmlNodeList xRecords = null;
            bool bSuccess = false;

            CallCWSFunction("DeductibleManagementAdaptor.GetDeductibleDetailsForEvent", out sCWSResponse);
            xDoc = new XmlDocument();
            xDoc.LoadXml(sCWSResponse);
            //jira# RMA-6135.Multicurrency
            if ((BaseCurrency != null) && (!string.IsNullOrEmpty(BaseCurrency.Text)))
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(BaseCurrency.Text);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(BaseCurrency.Text);
            }
            //jira# RMA-6135.Multicurrency
            DataSet eventDedDataSet = new DataSet();
            DataTable eventDedTable = eventDedDataSet.Tables.Add("EventDeductible");

            eventDedTable.Columns.Add("ClaimId", typeof(string));
            eventDedTable.Columns.Add("ClaimNumber", typeof(string));
            eventDedTable.Columns.Add("TotalPaid", typeof(double));
            eventDedTable.Columns.Add("TotalRecovery", typeof(double));
            eventDedTable.Columns.Add("DeductiblePerEvent", typeof(double));
            eventDedTable.Columns.Add("DeductibleApplied", typeof(double));
            
            eventDedTable.Columns.Add("CoverageGroup", typeof(string));
            eventDedTable.Columns.Add("CoverageGroupCode", typeof(string));

            xRecords = xDoc.SelectNodes("//DEDUCTIBLE");
            foreach (XmlNode xRecord in xRecords)
            {
                eventDedRow = eventDedTable.NewRow();
                if (xRecord.SelectSingleNode("CLAIM_ID") != null)
                {
                    eventDedRow["ClaimId"] = xRecord.SelectSingleNode("CLAIM_ID").InnerText;
                }
                if (xRecord.SelectSingleNode("CLAIM_NUMBER") != null)
                {
                    eventDedRow["ClaimNumber"] = xRecord.SelectSingleNode("CLAIM_NUMBER").InnerText;
                }
                if (xRecord.SelectSingleNode("TOTAL_PAID") != null)
                {
                   eventDedRow["TotalPaid"] = Conversion.CastToType<double>(xRecord.SelectSingleNode("TOTAL_PAID").InnerText, out bSuccess);
                 
                }
                if (xRecord.SelectSingleNode("TOTAL_RECOVERY") != null)
                {
                    eventDedRow["TotalRecovery"] = Conversion.CastToType<double>(xRecord.SelectSingleNode("TOTAL_RECOVERY").InnerText, out bSuccess);
                }
                if (xRecord.SelectSingleNode("DED_PER_EVENT") != null)
                {
                    eventDedRow["DeductiblePerEvent"] = Conversion.CastToType<double>(xRecord.SelectSingleNode("DED_PER_EVENT").InnerText, out bSuccess);
                }
                if (xRecord.SelectSingleNode("DEDUCTIBLE_APPLIED") != null)
                {
                    eventDedRow["DeductibleApplied"] = Conversion.CastToType<double>(xRecord.SelectSingleNode("DEDUCTIBLE_APPLIED").InnerText, out bSuccess);
                }
                if (xRecord.SelectSingleNode("COVERAGE_GROUP") != null)
                {
                    eventDedRow["CoverageGroup"] = xRecord.SelectSingleNode("COVERAGE_GROUP").InnerText;
                }
                if (xRecord.SelectSingleNode("COVERAGE_GROUP_CODE") != null)
                {
                    eventDedRow["CoverageGroupCode"] = xRecord.SelectSingleNode("COVERAGE_GROUP_CODE").InnerText;
                }

                eventDedTable.Rows.Add(eventDedRow);
            }

            gvEventDeductables.DataSource = eventDedDataSet.Tables["EventDeductible"];

            eventDedTable.Dispose();
            eventDedDataSet.Dispose();
        }
        /// <summary>
        /// grdRadio_CheckedChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdRadio_CheckedChanged(object sender, EventArgs e)
        {
            
            
            GridDataItem item = (sender as RadioButton).Parent.Parent as GridDataItem;
            if (item!=null)
            {
                SelectedClaimId.Value = item["ClaimId"].Text;
                SelectedCovGroup.Value = item["CoverageGroupCode"].Text;
            }
            bBindSuppGrid.Value = "true";
            lblDeductibleGridClaimHeader.Visible = true;
            gvDedutible.Rebind();
        }
        /// <summary>
        /// gvDedutible_NeedDataSource
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void gvDedutible_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            
            if (string.Compare(bBindSuppGrid.Value,"true",true)==0)
            {
                BindSubGrid();
            }
        }
        /// <summary>
        /// BindSubGrid
        /// </summary>
        /// <param name=""></param>
        void BindSubGrid()
        {
            string sCWSResponse = string.Empty;
            XmlDocument xDoc = null;
            DataSet dedDataSet = null;
            DataTable dedTable = null;
            DataRow dedRow = null;
            XmlNodeList xRecords = null;
            bool bSuccess = false;

            CallCWS("DeductibleManagementAdaptor.GetCoverageDeductibleForClaim", GetMessageTemplate(), out sCWSResponse, false, false);
            xDoc = new XmlDocument();
            xDoc.LoadXml(sCWSResponse);

            dedDataSet = new DataSet();
            dedTable = dedDataSet.Tables.Add("Deductible");
            

            dedTable.Columns.Add("Policy", typeof(string));
            dedTable.Columns.Add("InsLine", typeof(string));
            dedTable.Columns.Add("Unit", typeof(string));
            dedTable.Columns.Add("Coverage", typeof(string));
            dedTable.Columns.Add("Totalpaid", typeof(double));
            dedTable.Columns.Add("TotalRecovery", typeof(double));
            dedTable.Columns.Add("DeductibleApplied", typeof(double));
     


            xRecords = xDoc.SelectNodes("//CVG_DEDUCTIBLE");
            foreach (XmlNode xRecord in xRecords)
            {
                dedRow = dedTable.NewRow();
                if (xRecord.SelectSingleNode("POLICY") != null)
                {
                    dedRow["Policy"] = xRecord.SelectSingleNode("POLICY").InnerText;
                }
                if (xRecord.SelectSingleNode("INS_LINE") != null)
                {
                    dedRow["InsLine"] = xRecord.SelectSingleNode("INS_LINE").InnerText;
                }
                if (xRecord.SelectSingleNode("UNIT") != null)
                {
                    dedRow["Unit"] = xRecord.SelectSingleNode("UNIT").InnerText;
                }
                if (xRecord.SelectSingleNode("COVERAGE") != null)
                {
                    dedRow["Coverage"] = xRecord.SelectSingleNode("COVERAGE").InnerText;
                }
                if (xRecord.SelectSingleNode("TOTAL_PAID") != null)
                {
                   dedRow["Totalpaid"] = Conversion.CastToType<double>(xRecord.SelectSingleNode("TOTAL_PAID").InnerText, out bSuccess);
                   
                }
                if (xRecord.SelectSingleNode("TOTAL_RECOVERY") != null)
                {
                    dedRow["TotalRecovery"] = Conversion.CastToType<double>(xRecord.SelectSingleNode("TOTAL_RECOVERY").InnerText, out bSuccess);
                }
                if (xRecord.SelectSingleNode("DEDCTIBLE_APPLIED") != null)
                {
                    dedRow["DeductibleApplied"] = xRecord.SelectSingleNode("DEDCTIBLE_APPLIED").InnerText;
                }

                dedTable.Rows.Add(dedRow);
            }

            
            
            

            gvDedutible.DataSource = dedTable;

            dedTable.Dispose();
            dedDataSet.Dispose();
        }
        /// <summary>
        /// GetMessageTemplate
        /// </summary>
        /// <param name=""></param>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                   <Instance>
                    <ClaimId>" + SelectedClaimId.Value + @"</ClaimId>
                    <CvgGroupId>" + SelectedCovGroup.Value + @"</CvgGroupId>
                   </Instance>
                </Document>
            </Message>
            ");

            return oTemplate;
        }

    }
}