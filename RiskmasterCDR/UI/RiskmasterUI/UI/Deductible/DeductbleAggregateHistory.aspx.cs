﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Xml;
using System.Xml.Linq;

namespace Riskmaster.UI.UI.Deductible
{
    public partial class DeductbleAggregateHistory : NonFDMBasePageCWS
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            grdAggDedHistory.NeedDataSource += new Telerik.Web.UI.GridNeedDataSourceEventHandler(grdAggDedHistory_NeedDataSource);
            grdAggDedHistory.ItemDataBound += new Telerik.Web.UI.GridItemEventHandler(grdAggDedHistory_ItemDataBound);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void grdAggDedHistory_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                GridPagerItem pager = (GridPagerItem)e.Item;
                Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                lbl.Visible = false;

                RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                combo.Visible = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void grdAggDedHistory_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            BindData();
        }
        /// <summary>
        /// BindData
        /// Bind Grid data containing deductible aggregate changes
        /// </summary>
        private void BindData()
        {
            string sCWSResponse = string.Empty;
            string sCovGroupId = string.Empty;
            XmlDocument xDoc = null;
            DataRow aggDataRow;
            XmlNodeList xRecords = null;

            DataSet aggDataSet = new DataSet();
            DataTable aggDataTable = aggDataSet.Tables.Add("AggregateHistory");
            try
            {
            if (Request.QueryString["CovGroupId"] != null)
            {
                sCovGroupId = Request.QueryString["CovGroupId"];
            }

            if (!string.IsNullOrEmpty(sCovGroupId))
            {
                CallCWS("DeductibleManagementAdaptor.GetAggregateHistory", GetMessageTemplate(sCovGroupId), out sCWSResponse, false, false);
            }
            xDoc = new XmlDocument();
            xDoc.LoadXml(sCWSResponse);

            aggDataTable.Columns.Add("CovGroupId", typeof(string));
            aggDataTable.Columns.Add("AggregateLimit", typeof(string));
            aggDataTable.Columns.Add("User", typeof(string));
            aggDataTable.Columns.Add("DateChanged", typeof(string));
            aggDataTable.Columns.Add("DedPerEvent", typeof(string));
            //Start - ExpenseFlagAddition - nbhatia6 - 07/03/14
            aggDataTable.Columns.Add("ExcludeExpensePmt", typeof(string));
            //End - ExpenseFlagAddition - nbhatia6 - 07/03/14
            xRecords = xDoc.SelectNodes("//AGGREGATE");
            foreach (XmlNode xRecord in xRecords)
            {
                aggDataRow = aggDataTable.NewRow();
                if (xRecord.SelectSingleNode("COV_GROUP_CODE") != null)
                {
                    aggDataRow["CovGroupId"] = xRecord.SelectSingleNode("COV_GROUP_CODE").InnerText;
                }
                if (xRecord.SelectSingleNode("AGGREGATE_LIMIT") != null)
                {
                    aggDataRow["AggregateLimit"] = xRecord.SelectSingleNode("AGGREGATE_LIMIT").InnerText;
                }
                if (xRecord.SelectSingleNode("CHANGED_BY") != null)
                {
                    aggDataRow["User"] = xRecord.SelectSingleNode("CHANGED_BY").InnerText;
                }
                if (xRecord.SelectSingleNode("DATE_DATA_CHANGED") != null)
                {
                    aggDataRow["DateChanged"] = xRecord.SelectSingleNode("DATE_DATA_CHANGED").InnerText;
                }
                if (xRecord.SelectSingleNode("DED_PER_EVENT") != null)
                {
                    aggDataRow["DedPerEvent"] = xRecord.SelectSingleNode("DED_PER_EVENT").InnerText;
                }
                //Start - ExpenseFlagAddition - nbhatia6 - 07/03/14
                if (xRecord.SelectSingleNode("EXCLUDE_EXPENSE_FLAG") != null)
                {
                    if (string.Compare(xRecord.SelectSingleNode("EXCLUDE_EXPENSE_FLAG").InnerText, "-1") == 0)
                    {
                        aggDataRow["ExcludeExpensePmt"] = "Yes";
                    }
                    else
                    {
                        aggDataRow["ExcludeExpensePmt"] = "No";
                    }
                }
                //End - ExpenseFlagAddition - nbhatia6 - 07/03/14
                aggDataTable.Rows.Add(aggDataRow);
            }

                grdAggDedHistory.DataSource = aggDataSet.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                aggDataTable.Dispose();
                aggDataSet.Dispose();
            }
        }
        /// <summary>
        /// Generate xml template
        /// </summary>
        /// <param name="sCovGroupId"></param>
        private XElement GetMessageTemplate(string sCovGroupId)
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <POL_GRP_ID>"
                + sCovGroupId
                + @"</POL_GRP_ID>
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            string sRedirectString = String.Format("./DeductibleGrid.aspx");
            Server.Transfer(sRedirectString);
        }
    }
}