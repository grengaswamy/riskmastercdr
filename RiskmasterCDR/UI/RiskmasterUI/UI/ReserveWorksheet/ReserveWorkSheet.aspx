﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReserveWorkSheet.aspx.cs" ValidateRequest="false" Inherits="Riskmaster.UI.UI.ReserveWorksheet.ReserveWorkSheet" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="CodeLookUp" Src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"TagPrefix="uc" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script src="../../Scripts/form.js" type="text/javascript"></script>
   
    <script src="../../Scripts/WaitDialog.js" type="text/javascript"></script>
    <script>

        function OpenCriteria() {

            var ClaimId = document.getElementById("hdnLOB").value;
            window.open('/RiskmasterUI/UI/Utilities/RSWSheetCustomization/RSWCustomization.aspx?sClaimId=' + ClaimId , 'ReserveWorkSheetCustomization',
		    'width=700,height=600,top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 700) / 2 + ',resizable=yes,scrollbars=yes');
            return false;
        }
//        function RefreshPageAfterCustomization(updatedXMLData)
//         {//700,600
//             __doPostBack('hdnCustomTransXML', updatedXMLData);
//       
//        }	
    
    </script>
    <style type="text/css">
        .ReserveTypeReason
        {
            float:left;
            width: 59.9%;
        }     
        .ReserveTypeComments
        {
            float:left;
            width: 20%;
        }
        .ReserveSummaryComments
        {
	
        }
        
    </style>
</head>
<body>
    <form id="frmData" method=post action='' runat="server">
    <asp:HiddenField ID="hdnXMLValue" runat="server" Value=""  />
    <asp:HiddenField ID="hdnPendingRWSId" value="" runat="server" />
    <asp:HiddenField ID="hdnAproveFromClientSide" Value="" runat="server" />
    <asp:HiddenField ID="hdnPenapprovalRWSId" value="" runat="server" />
    <asp:HiddenField ID="hdnRejectRWSId" value="" runat="server" />
    <asp:HiddenField ID="hdnApprovedRWSId" value="" runat="server" />
    <asp:HiddenField ID="hdnCloneFromApproved" Value="" runat="server" />
    <asp:HiddenField ID="hdnApprovedBtnClicked" Value="false" runat="server" />
    <asp:HiddenField ID="SysFormName" Value="" runat="server" />
    <asp:HiddenField ID="SysFormIdName" Value="" runat="server" />
    <asp:HiddenField ID="rswid" Value="" runat="server" />
    <asp:HiddenField ID="RswTypeName" Value="" runat="server" />
     <asp:HiddenField ID="hdnLOB" Value="" runat="server" />
    <asp:HiddenField ID="SysIsServiceError" Value="" runat="server" />
     <asp:HiddenField ID="hdnCustomTransXML" runat="server" Value=""  />
    <asp:HiddenField ID="SysClassName" Value="" runat="server" />
    <asp:HiddenField ID="SysFormPIdName" Value="" runat="server" />
    <asp:HiddenField ID="SysCmd" Value="" runat="server" />
    <asp:HiddenField ID="currencytype" Value="" runat="server" />
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                    <asp:imagebutton runat="server" src="../../Images/save.gif" width="28" OnClientClick = "return SaveData();" onclick="btnSave_Click" height="28" border="0" 
                    id="btnSave" alternatetext="Save" ToolTip="Save" onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                    <asp:imagebutton runat="server" OnClientClick = "return SubmitSheet();" onclick="btnSubmit_Click" src="../../Images/tb_submit_active.png" width="28"
                        height="28" border="0" id="btnSubmit" alternatetext="Submit" ToolTip="Submit" onmouseover="this.src='../../Images/tb_submit_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_submit_mo.png';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                    <asp:imagebutton runat="server" src="../../Images/tb_accept_active.png" width="28" height="28" border="0" id="btnApprove" alternatetext="Approve" ToolTip="Approve"
                    OnClientClick="return Approve();" onclick="btnApprove_Click"  onmouseover="this.src='../../Images/tb_accept_mo.png';this.style.zoom='110%'" onmouseout="this.src='../../Images/tb_accept_active.png';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                    <asp:imagebutton runat="server" src="../../Images/tb_reject_active.png" width="28" OnClientClick="return Reject();" onclick="btnReject_Click"
                        height="28" border="0" id="btnReject" alternatetext="Reject" ToolTip="Reject" onmouseover="this.src='../../Images/tb_reject_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_reject_active.png';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                    <asp:imagebutton runat="server" OnClientClick ="return DeleteWorksheet();" onclick="btnDelete_Click" src="../../Images/delete.gif"
                        width="28" height="28" border="0" id="btnDelete" alternatetext="Delete Record" ToolTip="Delete Record" onmouseover="this.src='../../Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/delete.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                    <asp:imagebutton runat="server"  src="../../Images/tb_clear_active.png" OnClientClick ="return ClearData();"
                        width="28" height="28" border="0" id="btnClear" alternatetext="Clear Data" ToolTip="Clear Data" onmouseover="this.src='../../Images/tb_clear_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_clear_active.png';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                    <asp:imagebutton runat="server" src="../../Images/tb_print_active.png" width="28"
                        height="28" border="0" id="btnPrint" alternatetext="Print" ToolTip="Print" onmouseover="this.src='../../Images/tb_print_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_print_active.png';this.style.zoom='100%'" OnClientClick ="return PrintWorksheet();" />
                </td>
                <td    align="center" valign="middle" height="32" width="28" xmlns="">
         
                    <asp:imagebutton runat="server" width="28" ImageUrl="~/Images/new2.gif"   
                        class="bold" ToolTip="Customize ReservesTab/Transactions" onMouseOver="this.src='../../Images/new2.gif';this.style.zoom='110%'"
                            onMouseOut="this.src='../../Images/new2.gif';this.style.zoom='100%'" 
                        id="btn_Show" alternatetext="Open" 
                       OnClientClick ="return OpenCriteria();" Visible="False" />
               
                </td>
            </tr>
        </table>
    </div>
    <%--<asp:Button ID="btnNew" runat="server" Text="New" OnClientClick = "return LoadNew();" onclick="btnNew_Click" />--%>
    <%--<asp:Button ID="btnSubmit" runat="server" Text="Submit"  OnClientClick = "return SubmitSheet();" onclick="btnSubmit_Click" />--%>
    <%--<asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick = "return SaveData();" onclick="btnSave_Click" />--%>
    <%--MGaba2:R6:Approve and Reject Button Button --%>
    <%--<asp:Button ID="btnApprove" runat="server" Text="Approve" OnClientClick="return Approve();" onclick="btnApprove_Click" />
    <asp:Button ID="btnReject" runat="server" Text="Reject" OnClientClick="return Reject();" onclick="btnReject_Click"/>--%>
    <%--<asp:Button ID="btnDelete" runat="server" Text="Delete" OnClientClick ="return DeleteWorksheet();" onclick="btnDelete_Click" />--%>
    <%--<asp:Button ID="btnClear" runat="server" Text="Clear" OnClientClick ="return ClearData();" />--%>
    <%--<asp:Button ID="btnEdit" runat="server" Text="Edit" onclick="btnEdit_Click"/>--%>
    
    <div id="divContainer" runat="server"></div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
