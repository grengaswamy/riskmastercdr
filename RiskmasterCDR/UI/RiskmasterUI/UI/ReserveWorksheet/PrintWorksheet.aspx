﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintWorksheet.aspx.cs" Inherits="Riskmaster.UI.UI.ReserveWorksheet.PrintWorksheet" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmData" runat="server">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
     <asp:HiddenField id="hdnRSWId" runat="server"/>
    </form>
</body>
</html>
