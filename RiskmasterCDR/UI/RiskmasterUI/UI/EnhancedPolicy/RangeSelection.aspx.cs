﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Web.UI.HtmlControls;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.UI.EnhancedPolicy
{
    public partial class RangeSelection : NonFDMBasePageCWS 
    {
        XElement XmlTemplate = null;
        bool bReturnStatus = false;               
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    hdnDiscountRowId.Text = AppHelper.GetQueryStringValue("DiscountRowId");
                    hdnDiscountId.Text =AppHelper.GetQueryStringValue("DiscountId");
                }
                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.RangeSelection", XmlTemplate);
                    if (UseVolumeDiscount.Text == "-1")
                    {
                        GridView GridRangeSelection = (GridView)RangeSelectionGrid.GridView;
                        GridRangeSelection.Columns[0].HeaderStyle.CssClass = "hiderowcol";
                        GridRangeSelection.Columns[0].ItemStyle.CssClass = "hiderowcol";                            
                        btnOK.Enabled = false;
                    }                                                       
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.RangeSelection</Function></Call><Document><Document><FunctionToCall/><RangeSelection><DiscountRowId>");
            sXml = sXml.Append(hdnDiscountRowId.Text);          
            sXml = sXml.Append("</DiscountRowId></RangeSelection></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
