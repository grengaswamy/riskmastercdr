﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillingRule.aspx.cs" Inherits="Riskmaster.UI.EnhancedPolicy.BillingRule" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Please Select a Billing Rule...</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
            // Function called when the Page is unloaded
						function handleUnload()
						{
							if(window.opener!=null)
                            {
                            // Calls the Close Method of Form.js
								window.opener.onCodeClose();
                            }
						}
            // Function called when a Billing Rule is Selected
						function selectBillingRule(sBillingRowId, sBillingShortCode, sBillingDesc)
						{
							if(window.opener!=null)
                            {
                            // Calls the setBillingRule Method of Form.js
                                window.opener.setBillingRule(sBillingRowId, sBillingShortCode, sBillingDesc);
                            }
						}
		   // Function called when the Page is loaded	
 						function handleOnload()
 						{
                         // Calls the setBillingRuleFieldName Method of Form.js
 							window.opener.setBillingRuleFieldName();
 						}	
	</script>
</head>
<body onload="handleOnload();" onunload="handleUnload();">
    <form id="frmData" runat="server" >
        <table>
            <tr>
                <td>
                    <asp:gridview ID="gvBillingRuleList" runat="server" 
                        onrowdatabound="gvBillingRuleList_RowDataBound" AutoGenerateColumns="False" 
                        ondatabound="gvBillingRuleList_DataBound" CssClass="singleborder" 
                        Width="560px" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:gridview>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center">
                <td>
                    <asp:button ID="btnCancel" runat="server" text="Cancel" CssClass="button" OnClientClick="window.close();return false;" />
<%--                    <asp:button ID="btnNoPayPlan" runat="server" text="No PayPlan" CssClass="button" OnClientClick="NoPolicy();return false;" />--%>
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
