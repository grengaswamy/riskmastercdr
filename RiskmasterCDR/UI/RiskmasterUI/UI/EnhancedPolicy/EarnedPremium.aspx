﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EarnedPremium.aspx.cs" Inherits="Riskmaster.UI.UI.EnhancedPolicy.EarnedPremium" %>

<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc3" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"  TagPrefix="mc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Earned Premium</title>
    <base target="_self" />
    <%--vkumar258 -RMA-6037 - starts--%>
    <%--<script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>

    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 -RMA-6037 - end--%>
        <script type="text/javascript" src="../../Scripts/form.js">        { var i; } </script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/EnhPolicy.js"></script>
    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />
    <uc1:commontasks id="CommonTasks1" runat="server" />
    <%-- aravi5: RMA-9942 Enhanced Policy - Earned Premium page does not open on chrome. Starts --%>
    <script type="text/javascript">
        function HideOverlaydiv() {
            var IEbrowser = false || !!document.documentMode; // At least IE6
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.opener.parent.parent.document.getElementById("overlaydiv").remove();
                }
                else if (window.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.parent.parent.document.getElementById("overlaydiv").remove();
                }
                return false;
            }
        }
    </script>
    <%-- aravi5: RMA-9942 Enhanced Policy - Earned Premium page does not open on chrome. Ends --%>
</head>
<body class="10pt" onpagehide="HideOverlaydiv(); return false;" onload="EarnedPremiumOnLoad();"> <%-- aravi5: RMA-9942 --%>
    <form id="frmData" runat="server" method="post">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <uc2:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSEarnedPremium" id="TABSEarnedPremium">
            <a class="Selected" href="#" onclick="tabChange(this.name);" name="EarnedPremium" id="LINKTABSEarnedPremium">Earned Premium</a>
        </div>
        <div class="tabSpace" runat="server" id="TABEarnedPremiumSpace">
            <nbsp />
            <nbsp />
        </div>
    </div>
    <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 425px; height: 160px; overflow: auto">
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABEarnedPremium" id="FORMTABEarnedPremium">
           <tr>
                <td>
                    <asp:Label runat="server" class="label" ID="lbl_FromDate" Text="From Date:" />&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="FromDate" RMXRef="/Instance/Document/EarnedPremium/EffectiveDate/EffectiveDate"
                        RMXType="date" TabIndex="1" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                        <%--vkumar258 -RMA-6037 - starts--%>
                        <%--<asp:Button class="DateLookupControl" runat="server" ID="FromDatebtn" TabIndex="2" />
                    <script type="text/javascript">
                        Zapatec.Calendar.setup(
				            {
				                inputField: "FromDate",
				                ifFormat: "%m/%d/%Y",
				                button: "FromDatebtn"
				            }
				            );
                    </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#FromDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");
                            });
                        </script>
                        <%--vkumar258 -RMA-6037 - end--%>

                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="label" ID="lbl_ToDate" Text="To Date:" />&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="ToDate" RMXRef="/Instance/Document/EarnedPremium/TransDate"
                        RMXType="date" TabIndex="3" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                        <%--vkumar258 -RMA-6037 - starts--%>
                        <%--  <asp:Button class="DateLookupControl" runat="server" ID="ToDatebtn" TabIndex="4" />
                  <script type="text/javascript">
                        Zapatec.Calendar.setup(
				            {
				                inputField: "ToDate",
				                ifFormat: "%m/%d/%Y",
				                button: "ToDatebtn"
				            }
				            );
                    </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#ToDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../Images/calendar.gif",
                                   // buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "4");
                            });
                        </script>
                        <%--vkumar258 -RMA-6037 - end--%>

                </td>
            </tr>
             <tr>
                <td>
                    <asp:Label runat="server" class="label" ID="lbl_CalPremium" Text="Earned Premium:" />&nbsp;&nbsp;
                </td>
                <td>
                  <mc:CurrencyTextbox ID="CalPremium" rmxtype="currency" runat="server" rmxref="/Instance/Document/EarnedPremium/Premium"  ReadOnly="true" onchange="setDataChanged(true);" rmxforms:as="currency"/> 
                </td> 
            </tr>
        </table>
    </div>
    <table>
        <tr>
            <td>
                <asp:Button class="button" ID="btnCalculate" Text=" Calculate " runat="server" OnClientClick="return EarnedPremiumCal();"
                    Width="68px" onclick="btnCalculate_Click" TabIndex="5" />
            </td>
            <td>
                <asp:Button class="button" ID="btnCancel" Text=" Cancel " runat="server" OnClientClick="EarnedPremiumCancel();"
                    Width="50px" TabIndex="6" />
            </td>
        </tr>
    </table>
     <asp:TextBox Style="display: none" runat="server" name="PolicyId" ID="PolicyId" />
    </form>
</body>
</html>
