﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddScheduleInfo.aspx.cs" Inherits="Riskmaster.UI.EnhancedPolicy.AddScheduleInfo" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="frmHead" runat="server">
    <title>Schedule Details</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script  type="text/javascript" language="javascript" src='../../Scripts/form.js'>   {var i;}</script>
    <script  type="text/javascript" language="javascript" src='../../Scripts/EnhPolicy.js'>   {var i;}</script>
    <style type="text/css">
        .style1
        {
            border-style: none;
            border-color: inherit;
            border-width: medium;
            FONT-WEIGHT: bold;
            PADDING-BOTTOM: 3px;
            COLOR: #ffffff;
            PADDING-TOP: 3px;
            BACKGROUND-COLOR: #95B3D7;
            float: left;
            width: 418px;
        }
    </style>
</head>
<body>
    <form id="frmData" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Add/Modify Schedule" /></div>
        <table><tr><td class="style1" colSpan="2">Schedule Details</td></tr></table>
        <br />

    <table>
        <tr>
         <td class="required">Name </td>
            <td>
            <asp:TextBox runat ="server" size="30" maxlength="50" onblur="" onchange="setDataChanged(true);" name="Name" id="Name" RMXRef = "Instance/Document/Schedule/Name" />
            </td>
         </tr>
         <tr>
         <td class="required">Amount</td>
            <td>
            <asp:TextBox runat ="server" size="30" onblur="" formatas="currency" RMXType="currency" onchange="setDataChanged(true);currencyLostFocus(this);" name="Amount" id="Amount" RMXRef = "Instance/Document/Schedule/Amount" />
            </td>
         </tr>
         <tr>
         <td>
             <br />
             <br />
           <asp:button class="button" runat="server" id="btnScheduleOk" RMXRef="" Text="OK" width="75px" onClientClick="return fnScheduleOk();" onclick="btnScheduleOk_Click" />
           &nbsp;
           <asp:button class="button" runat="server" id="btnScheduleCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return fnScheduleCancel();" onclick="btnScheduleOk_Click" />
         </td>
         </tr> 
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="txtData"></asp:TextBox>
    <asp:TextBox style="display: none" runat="server" ID="formname" value="scheduledetails" />
    <asp:TextBox Style="display: none" runat="server" ID="FunctionToCall"></asp:TextBox>
    <asp:TextBox style="display:none" runat="server" id="SessionId" RMXRef="" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
    <asp:TextBox style="display:none" runat="server" id="UarId" RMXRef="" RMXType="hidden" Text="" rmxignorevalue="true" xmlns:cul="remove" />
    <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" />
    <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" />
    <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" /> 
    <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" />
    </form>
</body>
</html>
