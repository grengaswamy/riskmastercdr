﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Deductible.aspx.cs" Inherits="Riskmaster.UI.UI.EnhancedPolicy.Deductible" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Please select the suitable Deductible</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
            // Function called when the Page is unloaded
						function handleUnload()
						{
							if(window.opener!=null)
                            {
                            // Calls the Close Method of Form.js
								window.opener.onCodeClose();
                            }
						}
			 // Function called when a Deductible is Selected			
						function selectDeductible(sModifier, sDeductible, sType, sAmount, sExpRateRowId)
						{
							if(window.opener!=null)
                            {
                                setIsDeductibleField(sType);
                            //Start: Neha Suresh Jain, 05/11/2010,MITS 20772 to calculate deductible depending on flat or percent selection
                                var sFlatPer = "";
                                if (document.forms[0].flatper != null) {
                                    sFlatPer = document.forms[0].flatper.value;
                                    }
                                    // Calls the setDeductible Method of Form.js
                                    window.opener.setDeductible(sModifier, sDeductible, sType, sAmount, sExpRateRowId, sFlatPer);                               
                             //End: Neha Suresh Jain
                            }  
						}
						// Function called when the Page is loaded	
 						function handleOnload()
 						{
                        // Calls the setDeductibleFieldName Method of Form.js
 							window.opener.setDeductibleFieldName();
 							setFlatPercentField();
 						}	
	</script>					
</head>
<body onload="handleOnload();" onunload="handleOnload();">
    <form id="frmData" runat="server">
    <div>
    <tr>
       <td>
         <asp:gridview ID="gvDeductibleList" runat="server" 
                        onrowdatabound="gvDeductibleList_RowDataBound" AutoGenerateColumns="False" 
                        ondatabound="gvDeductibleList_DataBound" CssClass="singleborder" 
                        Width="560px" AllowPaging="True" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:gridview>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
         </td>
     </tr>
            <tr align="center">
                <td>
                    <asp:button ID="btnCancel" runat="server" text="Cancel" CssClass="button" OnClientClick="window.close();return false;" />

                </td>
            </tr>
    </div>
    <asp:TextBox style="display: none" runat="server" ID="flatorpercent"  /> 
     <%--Start: Neha Suresh Jain,05/11/2010,MITS 20772,to get short codes for flat/percent--%> 
    <asp:TextBox style="display: none" runat="server" ID="flatper"  />    
     <%--End: Neha Suresh Jain --%>
    <asp:TextBox style="display: none" runat="server" ID="isdeductible"  />
    </form>
    
</body>
</html>
