<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayPlan.aspx.cs" Inherits="Riskmaster.UI.EnhancedPolicy.PayPlan" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Please Select a Pay Plan...</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
            // Function called when the Page is unloaded
						function handleUnload()
						{
							if(window.opener!=null)
                            {
                            // Calls the Close Method of Form.js
								window.opener.onCodeClose();
                            }
						}
            // Function called when a Payplan is Selected
						function selectPayPlan(sPayPlanRowId, sPayPlanShortCode,sPayPlanDesc,sBillingRowId, sBillingShortCode, sBillingDesc)
						{
							if(window.opener!=null)
                            {
                            // Calls the setPayPlan Method of Form.js
								window.opener.setPayPlan(sPayPlanRowId, sPayPlanShortCode,sPayPlanDesc,sBillingRowId, sBillingShortCode, sBillingDesc);
                            }  
						}
		   // Function called when the Page is loaded	
 						function handleOnload()
 						{
                        // Calls the setPayPlanPlanFieldName Method of Form.js
 							window.opener.setPayPlanPlanFieldName();
 						}	
	</script>
</head>
<body onload="handleOnload();" onunload="handleOnload();">
    <form id="frmData" runat="server" >
        <table>
            <tr>
                <td>
                    <asp:gridview ID="gvPayPlanList" runat="server" 
                        onrowdatabound="gvPayPlanList_RowDataBound" AutoGenerateColumns="False" 
                        ondatabound="gvPayPlanList_DataBound" CssClass="singleborder" 
                        Width="560px" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:gridview>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center">
                <td>
                    <asp:button ID="btnCancel" runat="server" text="Cancel" CssClass="button" OnClientClick="window.close();return false;" />
<%--                    <asp:button ID="btnNoPayPlan" runat="server" text="No PayPlan" CssClass="button" OnClientClick="NoPolicy();return false;" />--%>
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
