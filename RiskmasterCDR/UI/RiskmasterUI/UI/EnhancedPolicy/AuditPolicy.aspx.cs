﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
namespace Riskmaster.UI.UI.EnhancedPolicy
{
    public partial class AuditPolicy : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PolicyId.Text = AppHelper.GetQueryStringValue("PolicyID");
                    XElement XmlTemplate = GetMessageTemplate();
                    bool bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.AuditPolicy", XmlTemplate);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.AuditPolicy</Function></Call><Document><Document><FunctionToCall/><AuditPolicy><PolicyID>");
            sXml = sXml.Append(PolicyId.Text);
            sXml = sXml.Append("</PolicyID></AuditPolicy></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

     
    }
}
