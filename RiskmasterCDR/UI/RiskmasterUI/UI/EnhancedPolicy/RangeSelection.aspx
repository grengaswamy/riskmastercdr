﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RangeSelection.aspx.cs"
    Inherits="Riskmaster.UI.UI.EnhancedPolicy.RangeSelection" %>

<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Please select a range/discount...</title>
    <base target="_self"/>
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
    <uc2:CommonTasks ID="CommonTasks1" runat="server" />

    <script type="text/javascript" src="/RiskmasterUI/Scripts/EnhPolicy.js"></script>

</head>
<body onload="RangeSelectionOnLoad();">
    <form id="frmData" runat="server" method="post">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSRangeSelection" id="TABSRangeSelection">
            <a class="Selected" href="#" runat="server" rmxref="" name="RangeSelection" id="LINKTABSRangeSelection">
                Range Selection</a>
        </div>
        <div class="tabSpace" runat="server" id="TABRangeSelectionSpace">
            <nbsp />
            <nbsp />
        </div>
    </div>
    <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 550px;
        height: 160px; overflow: auto">
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABRangeSelection" id="FORMTABRangeSelection">
            <tr>
                <td>
                    <dg:UserControlDataGrid runat="server" ID="RangeSelectionGrid" GridName="RangeSelectionGrid"
                        GridTitle="" Target="/Document/DiscountRange/TermList" Ref="/Instance/Document/RangeSelection/SelectedTermId"
                        Unique_Id="DISC_RANGE_ROWID" ShowRadioButton="True" Width="530px" Height="150px"
                        HideNodes="|AMOUNT_HIDDEN|DISC_RANGE_ROWID|" ShowHeader="True" LinkColumn=""
                        PopupWidth="500" PopupHeight="340" Type="Grid" HideButtons="Edit|Delete|New" class="completerow" TextColumn="AMOUNT_HIDDEN"/>
                    <asp:TextBox Style="display: none" runat="server" ID="RangeSelectionSelectedId" RMXType="id" />
                    <asp:TextBox Style="display: none" runat="server" ID="RangeSelectionGrid_RowDeletedFlag"
                        RMXType="id" Text="false" />
                    <asp:TextBox Style="display: none" runat="server" ID="RangeSelectionGrid_Action"
                        RMXType="id" />
                    <asp:TextBox Style="display: none" runat="server" ID="RangeSelectionGrid_RowAddedFlag"
                        RMXType="id" Text="false" />
                </td>
            </tr>
        </table>
    </div>
    <table>
        <tr>
            <td>
                <asp:Button class="button" ID="btnOK" Text=" OK " runat="server" OnClientClick="return RangeSelectionOK();"
                    Width="50px" />
            </td>
            <td>
                <asp:Button class="button" ID="btnCancel" Text=" Cancel " runat="server" OnClientClick="return RangeSelectionCancel();"
                    Width="50px" />
            </td>
        </tr>
    </table>
    <asp:TextBox runat="server" Style="display: none" ID="UseVolumeDiscount" rmxref="/Instance/Document/DiscountRange/UseVolumeDiscount" />
    <asp:TextBox runat="server" Style="display: none" ID="hdnDiscountRowId" />
    <asp:TextBox runat="server" Style="display: none" ID="hdnDiscountId" />    
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="_" />
    </form>
</body>
</html>
