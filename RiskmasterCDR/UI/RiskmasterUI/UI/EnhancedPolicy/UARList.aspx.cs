﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.Common;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.Text;

//Author    :   Anu Tennyson
//Date      :   4/23/2010
//MITS      :   MITS:18229
//Comments  :   Created the UI to display the UAR Details Added in UAR Popup
namespace Riskmaster.UI.UI.EnhancedPolicy
{
    public class UARCodes
    {
        public string UARName
        {
            get;
            set;
        }
        public string Id
        {
            get;
            set;
        }
    }
    public partial class UARList : NonFDMBasePageCWS
    {
        private const int m_MAX_RECORD_PER_PAGE = 10;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sUARList = string.Empty;
            string sUARName = string.Empty;
            string sUARID = string.Empty;
            int iPageCount = 0;
            int iCount = 0;
            int iExposureStart = 0;
            int iPageValue = 0;
            bool bIsSuccess = false;
            bool bReturnStatus = false;
            string sSessionId = string.Empty;
            string sFormName = string.Empty;
            string sreturnValue = string.Empty;
            XElement xmlTemplate = null;
            UARCodes objUARCodes = null;
            DataTable dtGridData = new DataTable();
            List<UARCodes> codes=new List<UARCodes>();
            DataRow objRow;
            if (AppHelper.GetQueryStringValue("uaraddedsessionid") != "")
                sSessionId = AppHelper.GetQueryStringValue("uaraddedsessionid");
            
            HiddenField txtPageNumber = (HiddenField)this.FindControl("pagenumber");
            if (txtPageNumber.Value == "")
            {
                pagenumber.Value = "1";
                iExposureStart = 0;
            }
            else
            {
                pagenumber.Value = txtPageNumber.Value;
                iPageValue = Conversion.CastToType<int>(txtPageNumber.Value, out bIsSuccess);
                iExposureStart = (((iPageValue - 1) * 10) + 1);
            }

            xmlTemplate = GetMessageTemplate(iExposureStart, sSessionId);
            bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.GetUARDetailsAdded", out sreturnValue, xmlTemplate);
            XmlDocument objUARList = new XmlDocument();
            objUARList.LoadXml(sreturnValue);
            if (bReturnStatus)
            {
                if (objUARList.SelectSingleNode("//UARAdded/UARs/UARList") != null)
                {
                    sUARList = objUARList.SelectSingleNode("//UARAdded/UARs/UARList").InnerText;
                }
                if (objUARList.SelectSingleNode("//UARAdded/UARs/PageCount") != null)
                {
                    iPageCount = Conversion.CastToType<int>(objUARList.SelectSingleNode("//UARAdded/UARs/PageCount").InnerText, out bIsSuccess);
                }
                pagecount.Value = iPageCount.ToString();
                if (!string.IsNullOrEmpty(sUARList))
                {
                    string[] sExposureList = sUARList.Split(new char[] { '~' });
                    for (int iExposureCount = 0; iExposureCount < sExposureList.Length; iExposureCount++)
                    {
                        if (!(string.IsNullOrEmpty(sExposureList[iExposureCount])))
                        {
                            objUARCodes = new UARCodes();
                            displayname.Value = "UAR Added";
                            string[] sExposureListBreak = sExposureList[iExposureCount].Split(new char[] { '|' });
                            if (sExposureListBreak[0] != null)
                            {
                                sUARName = sExposureListBreak[0];
                            }
                            if (sExposureListBreak[1] != null)
                            {
                                sUARID = sExposureListBreak[1];
                            }
                            objUARCodes.UARName = sUARName;
                            objUARCodes.Id = sUARID;
                            codes.Add(objUARCodes);
                        }
                    }
                }
                else
                {
                    displayname.Value = "";
                    pagenumber.Value = "0";
                    pagecount.Value = "0";
                }
            }
            dgUARCodes.DataSource = codes;
            dgUARCodes.DataBind();
        }
        /// <summary>
        /// The Function Binds each row to the Data Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgUARCodes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink rowHyperLink = null;
                rowHyperLink = new HyperLink();
                rowHyperLink.NavigateUrl = "#";
                String sUARName = string.Empty;
                String sDesc = string.Empty;
                String sId = string.Empty;
                String sParentCode = string.Empty;
                String sParentDesc = string.Empty;
                if (DataBinder.Eval(e.Row.DataItem, "UARName") == null)
                {
                    sUARName = string.Empty;
                }
                else
                {
                    sUARName = DataBinder.Eval(e.Row.DataItem, "UARName").ToString();
                }
                
                if (DataBinder.Eval(e.Row.DataItem, "Id") == null)
                {
                    sId = string.Empty;
                }
                else
                {
                    sId = DataBinder.Eval(e.Row.DataItem, "Id").ToString();
                }

                rowHyperLink.Attributes.Add("onclick", "javascript:selUARCode('" + AppHelper.escapeStrings(sUARName) + "','" + sId + "')");
                rowHyperLink.Text = sUARName;
                rowHyperLink.CssClass = "HeaderNavy";
                e.Row.Cells[0].Controls.Add(rowHyperLink);
                e.Row.Cells[0].CssClass = "Bold2";
            }
        }
        
        private XElement GetMessageTemplate(int iExposureStart, string sSessionId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.GetDeductible</Function></Call><Document><Document><SessionId>");
            sXml = sXml.Append(sSessionId);
            sXml = sXml.Append("</SessionId><ExposureStart>");
            sXml = sXml.Append(iExposureStart);
            sXml = sXml.Append("</ExposureStart>");
            sXml = sXml.Append("</Document></Document></Message>");

            XElement oTemplate = XElement.Parse(Convert.ToString(sXml));
            return oTemplate;
        }
        
    }
}
