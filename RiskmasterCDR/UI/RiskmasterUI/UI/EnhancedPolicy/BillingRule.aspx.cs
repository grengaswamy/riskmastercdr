﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;

namespace Riskmaster.UI.EnhancedPolicy
{
    public partial class BillingRule : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = "";
            if (!IsPostBack)
            {
                gvBillingRuleList.HeaderStyle.CssClass = "msgheader";
                gvBillingRuleList.RowStyle.CssClass = "datatd1";
                gvBillingRuleList.AlternatingRowStyle.CssClass = "datatd";

                XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.GetBillingCode", out sreturnValue, XmlTemplate);

                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);

                        XmlNodeList payplanRecords = XmlDoc.SelectNodes("/ResultMessage/Document/BillingRuleCodes/BillingRuleCode");
                        if (payplanRecords.Count != 0)
                        {
                            DataTable dtGridData = new DataTable();
                            DataColumn dcGridColumn;
                            BoundField bfGridColumn;
                            ButtonField butGridLinkColumn;
                            XmlNode xNode;
                            DataRow objRow;
                            string sCssClass = string.Empty;



                            // BillingRule Row Id

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            // Assign the Column Name from XmlNode
                            dcGridColumn.ColumnName = "BillingRule RowId";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvBillingRuleList.Columns.Add(bfGridColumn);


                            // BillingRule Code

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            // Assign the Column Name from XmlNode
                            dcGridColumn.ColumnName = "Code";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvBillingRuleList.Columns.Add(bfGridColumn);

                            // BillingRule Description

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            // Assign the Column Name from XmlNode
                            dcGridColumn.ColumnName = "Description";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            butGridLinkColumn = new ButtonField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            butGridLinkColumn.DataTextField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            butGridLinkColumn.HeaderText = dcGridColumn.ColumnName;

                            // Create the hyperlink for the column
                            butGridLinkColumn.ButtonType = ButtonType.Link;

                            gvBillingRuleList.Columns.Add(butGridLinkColumn);
                            

                            // Hiding Colunm
                            sCssClass = "hiderowcol";
                            gvBillingRuleList.Columns[0].HeaderStyle.CssClass = sCssClass;


                            for (int i = 0; i < payplanRecords.Count; i++)
                            {
                                xNode = payplanRecords[i];
                                objRow = dtGridData.NewRow();

                                objRow["BillingRule RowId"] = xNode.Attributes["Billing_Rule_RowId"].Value;
                                objRow["Code"] = xNode.Attributes["Billing_Rule_ShortCode"].Value;
                                objRow["Description"] = xNode.Attributes["Billing_Rule_CodeDesc"].Value;

                                dtGridData.Rows.Add(objRow);
                            }
                            gvBillingRuleList.DataSource = dtGridData;
                            gvBillingRuleList.DataBind();
                        }
                        else
                        {
                            gvBillingRuleList.Visible = false;
                            btnCancel.Visible = false;
                            lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;No Billing Rules found!</h3></p>";
                        }

                    }
            }

        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.GetBillingCode</Function></Call><Document><Temp/>");
            sXml = sXml.Append("</Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void gvBillingRuleList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
                TableCell tcGridCell;
                LinkButton lbLinkColumn;

                tcGridCell = e.Row.Cells[2];
                lbLinkColumn = (LinkButton)tcGridCell.Controls[0];

                // href is assigned the value # to stop the postback on click.
                lbLinkColumn.Attributes.Add("href", "#");

                lbLinkColumn.Attributes.Add("onclick", "selectBillingRule(" + e.Row.Cells[0].Text + ",'" + e.Row.Cells[1].Text + "','" + lbLinkColumn.Text + "');");

            }
        }

        protected void gvBillingRuleList_DataBound(object sender, EventArgs e)
        {
            gvBillingRuleList.Columns[0].HeaderStyle.CssClass = "hiderowcol";
            gvBillingRuleList.Columns[0].ItemStyle.CssClass = "hiderowcol";
        }

    }
}
