﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
//Author    :   Sumit Kumar
//Date      :   04/27/2010
//MITS      :   MITS# 20483
//Comments  :   New Page to enter the schedule information.
namespace Riskmaster.UI.EnhancedPolicy
{
    public partial class AddScheduleInfo : NonFDMBasePageCWS
    {
        private const string m_FUNCTION_CALL_GET_SCHEDULE = "EnhancePolicyAdaptor.GetSchedule";
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            string sreturnValue = string.Empty;

            if (!IsPostBack)
            {
                int iScheduleId = 0;
                if (AppHelper.GetQueryStringValue("selectedid") != "")
                    iScheduleId = Int32.Parse(AppHelper.GetQueryStringValue("selectedid"));

                string sSessionId = string.Empty;
                if (AppHelper.GetQueryStringValue("sessionid") != "")
                    sSessionId = AppHelper.GetQueryStringValue("sessionid");

                string sMode = string.Empty;
                if (AppHelper.GetQueryStringValue("mode") != "")
                    sMode = AppHelper.GetQueryStringValue("mode");


                TextBox txtGridName = (TextBox)this.FindControl("gridname");
                if (txtGridName != null)
                {
                    txtGridName.Text = AppHelper.GetQueryStringValue("gridname");
                }

                TextBox txtmode = (TextBox)this.FindControl("mode");
                if (txtmode != null)
                {
                    txtmode.Text = sMode;
                }

                TextBox txtselectedrowposition = (TextBox)this.FindControl("selectedrowposition");
                if (txtselectedrowposition != null)
                {
                    txtselectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                }

                TextBox txtScheduleId = (TextBox)this.FindControl("selectedid");
                if (txtScheduleId != null)
                {
                    txtScheduleId.Text = iScheduleId.ToString();
                }

                if (sMode == "edit")
                {
                    XmlTemplate = GetLoadMessageTemplate(iScheduleId, sSessionId);
                    bReturnStatus = CallCWS(m_FUNCTION_CALL_GET_SCHEDULE, XmlTemplate, out sreturnValue, false, true);
                }
            }
        }

        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 04/28/2010
        /// MITS : 20483
        /// Description : This function will set the Session ID and refresh the parent page.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event Argument</param>
        protected void btnScheduleOk_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = "";
            TextBox txtSessionId;
            bReturnStatus = CallCWS("EnhancePolicyAdaptor.SetSchedule", XmlTemplate, out sreturnValue, true, false);
            if (bReturnStatus)
            {
                // Set the Session Id
                XmlDoc.LoadXml(sreturnValue);
                txtSessionId = (TextBox)this.FindControl("SessionId");
                txtSessionId.Text = XmlDoc.SelectSingleNode("//Document/SetSchedule/SessionId").InnerText;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ScheduleOkClick", "<script>fnRefreshParentScheduleOk();</script>");
            }
        }

        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 05/03/2010
        /// MITS : 20483
        /// </summary>
        /// <param name="iPropertyId">Property Unit ID</param>
        /// <param name="sSessionId">Session ID</param>
        /// <returns>XML Element</returns>
        private XElement GetLoadMessageTemplate(int iScheduleId, string sSessionId)
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.GetSchedule</Function></Call><Document><Schedule><ScheduleID>");
            sXml = sXml.Append(iScheduleId);
            sXml = sXml.Append("</ScheduleID><SessionId>");
            sXml = sXml.Append(sSessionId);
            sXml = sXml.Append("</SessionId></Schedule></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
