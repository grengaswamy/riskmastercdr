﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.UI.EnhancedPolicy
{
    public partial class EarnedPremium : NonFDMBasePageCWS 
    {
        XElement XmlTemplate = null;
        bool bReturnStatus = false;
        string sreturnValue = "";
        XmlDocument objReturnXml = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes

                if (!IsPostBack)
                {
                    PolicyId.Text = AppHelper.GetQueryStringValue("PolicyID");
                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.EarnedPremium", out sreturnValue, XmlTemplate);                    
                    objReturnXml = new XmlDocument();
                    objReturnXml.LoadXml(sreturnValue);
                    string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                    if (sMsgStatus == "Success")
                    {
                        FromDate.Text = objReturnXml.SelectSingleNode("//EffectiveDate").InnerText;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.CalculateEarnedPremium</Function></Call><Document><Document><FunctionToCall/><EarnedPremium><PolicyID>");
            sXml = sXml.Append(PolicyId.Text);
            sXml = sXml.Append("</PolicyID><TransDate>");
            sXml = sXml.Append(ToDate.Text);
            sXml = sXml.Append("</TransDate><EffectiveDate>");
            sXml = sXml.Append(FromDate.Text);
            sXml = sXml.Append("</EffectiveDate></EarnedPremium></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }


        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.CalculateEarnedPremium", out sreturnValue, XmlTemplate);
                objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sreturnValue);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    string sEarnPrem=objReturnXml.SelectSingleNode("//CalculatedPremium").InnerText;
                    if(sEarnPrem != "")
                    {
                    //CalPremium.Text = String.Format("{0:C}", Double.Parse(sEarnPrem)); 
                        CalPremium.AmountInString = sEarnPrem; //Manish Jain Multicurrency
                    }
                    
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
