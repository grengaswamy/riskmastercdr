﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintPolicy.aspx.cs" Inherits="Riskmaster.UI.EnhancedPolicy.PrintPolicy" %>

<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Selected Forms</title>

    <script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/EnhPolicy.js"></script>

    <uc1:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body>
    <form id="frmData" runat="server" method="post">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">
                <uc2:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr class="msgheader">
            <td colspan="2">
                Selected Forms
            </td>
        </tr>
        <%--<tr class="ctrlgroup">
            <td colspan="3">
            </td>
        </tr>--%>
    </table>
    <div style="position: relative; width: 530px; height: 250px; overflow: auto" class="divScroll">
        <asp:GridView ID="GridDisplayForms" runat="server" AutoGenerateColumns="false" CssClass="singleborder"
            GridLines="None" Width="100%">
            <HeaderStyle CssClass="ctrlgroup" />
            <RowStyle CssClass="datatd" HorizontalAlign="Left" Font-Bold="false" />
            <AlternatingRowStyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />
            <Columns>
                <asp:TemplateField HeaderText="Select All" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                    <HeaderTemplate>
                        <input type="checkbox" id="chkForms" runat="server" title="SelectAll" onclick="FormsClick('GridDisplayForms');" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <input type="checkbox" runat="server" id="chk_forms" value='<%# Eval("FormId")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="lblTitle" runat="server" Text="Form Title" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblFrmTitle" Text='<%# Eval("FormTitle")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                <ItemTemplate>
                <asp:HiddenField ID="hdnPolicyId" runat="server" Value='<%# Eval("HdnPolicyId")%>'/>
                </ItemTemplate>               
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <table width="17%" border="0">
        <tr>
            <td width="5%">
                <asp:Button class="button" ID="btnAdd" Text=" Add " runat="server" OnClientClick="AddForms();" />
            </td>
            <td width="5%">
                <asp:Button class="button" ID="btnDelete" Text=" Delete " runat="server" OnClientClick="AddSelectedForms('GridDisplayForms','Delete');return  getSelectedForms('GridDisplayForms');"
                    OnClick="btnDelete_Click" />
            </td>
            <td width="5%">
                <asp:Button class="button" ID="btnCancel" Text=" Cancel " runat="server" OnClientClick="window.close();" />
            </td>
            <td width="5%">
                <asp:Button class="button" ID="btnSave" Text=" Save " runat="server" OnClientClick="AddSelectedForms('GridDisplayForms','Save');return getSelectedForms('GridDisplayForms');"
                    OnClick="btnSave_Click" />
            </td>
            <td width="5%">
                <asp:Button class="button" ID="btnPrintPolicy" Text=" Print Policy " runat="server"
                    OnClientClick="return DisplayPrintForms('GridDisplayForms');" />
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" runat="server" name="selectedformnames" ID="selectedformnames" />
    <asp:TextBox Style="display: none" runat="server" name="selectedformids" ID="selectedformids" />
    <asp:TextBox Style="display: none" runat="server" name="policyid" ID="policyid" />
    <asp:TextBox Style="display: none" runat="server" name="DisplayFormIds" ID="DisplayFormIds" />
    <asp:TextBox Style="display: none" runat="server" name="hdnAction" ID="hdnAction" />
    </form>
</body>
</html>
