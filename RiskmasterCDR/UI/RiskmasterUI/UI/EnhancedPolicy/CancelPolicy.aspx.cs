﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.EnhancedPolicy
{
    public partial class CancelPolicy : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes

                if (!IsPostBack)
                {
                    PolicyId.Text = AppHelper.GetQueryStringValue("PolicyID");
                    EffDate.Text = AppHelper.GetQueryStringValue("EffDate");
                    XElement XmlTemplate = GetMessageTemplate("EnhancePolicyAdaptor.GetCancelPolicyCodeData");
                    bool bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.GetCancelPolicyCodeData", XmlTemplate);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate(string sFunctionToCall)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append(sFunctionToCall);
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><CancelPolicy><Codes></Codes><CancelDate>");
            sXml = sXml.Append(CancelDate.Text);
            sXml = sXml.Append("</CancelDate><CancelType codeid=''>");            
            sXml = sXml.Append(CancelType.Text);
            sXml = sXml.Append("</CancelType><CancelReason codeid=''>");            
            sXml = sXml.Append(CancelReason.Text);
            sXml = sXml.Append("</CancelReason>");
            sXml = sXml.Append("<PolicyID>");
            sXml = sXml.Append(PolicyId.Text);
            sXml = sXml.Append("</PolicyID><EffDate>");
            sXml = sXml.Append(EffDate.Text);
            sXml = sXml.Append("</EffDate> </CancelPolicy></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                XElement XmlTemplate = GetMessageTemplate("EnhancePolicyAdaptor.ValidateCancelPolicy");
                string sreturnValue = "";
                bool bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.ValidateCancelPolicy", out sreturnValue, XmlTemplate);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sreturnValue);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    validate.Text = "true";
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
