﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Editor
{
    public partial class EditorPage : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XmlDocument tableXDoc = new XmlDocument();
                XElement XmlTemplate = null;
                if (!IsPostBack)
                {
                    hdFieldName.Text  = Request.QueryString["fieldname"];
                    hdCommentsFlag.Text = Request.QueryString["CommentsFlag"];
                    //Sumit-MITS#18251 -10/12/2009 -Fetch value for ReadOnly Parameter
                    hdnReadOnly.Text=Request.QueryString["ReadOnly"];
                    //Sumit-MITS#18251 -10/12/2009 -Transaction Id from Link
                    hdnTransIdFromLink.Text=Request.QueryString["TransactionId"];
                    //Sumit-MITS#18251 -10/12/2009 -Call changed to fetch comments from DB
                    if (hdnReadOnly.Text=="true" && hdnTransIdFromLink.Text!="")
                    {
                        XmlTemplate = GetCommentsTemplate();
                        XmlTemplate.XPathSelectElement("/Document/TransactionID").SetValue(hdnTransIdFromLink.Text);
                        bReturnStatus = CallCWSFunction("EditorAdaptor.GetComments", XmlTemplate);
                    }
                    else
                    {
                        XmlTemplate = GetMessageTemplate();
                        bReturnStatus = CallCWSFunction("EditorAdaptor.GetMemoAttributes", XmlTemplate);
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                <Call>
                    <Function>EditorAdaptor.GetMemoAttributes</Function>
                </Call>
                <Document>
					<Editor><IsSuppField>" + AppHelper.GetQueryStringValue("SupplementalField") + "</IsSuppField></Editor>" +
                "</Document></Message>");
            return oTemplate;
        }

        //Sumit-MITS#18251 -10/12/2009 -Get  comments from DB
        private XElement GetCommentsTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                <Call>
                    <Function>EditorAdaptor.GetComments</Function>
                </Call>
                <Document>
                    <TransactionID />
					<Editor />
                </Document>
            </Message>
            ");
            return oTemplate;
        }
    }
}
