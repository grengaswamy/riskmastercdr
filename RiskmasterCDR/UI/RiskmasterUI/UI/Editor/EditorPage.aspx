<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="EditorPage.aspx.cs" Inherits="Riskmaster.UI.Editor.EditorPage"   EnableViewStateMac="false" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml"  xmlns:cc1="remove">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Riskmaster Editor</title>
    <script type="text/javascript" language="javascript" >
    var m_bCommentsFlag = 'false';
    var m_bFreezeExisting = 'false';
    var m_bDateStamp = 'false';
    var m_bFreezeDateStamp = 'false';
    var m_FieldName = "";
    var m_qsParm = new Array();
    m_qsParm['fieldname'] = null;

    function getTextFlags() {        
        var oCtrl = document.getElementById("hdCommentsFlag");
        if( oCtrl != null )
            m_bCommentsFlag = oCtrl.value;

        oCtrl = document.getElementById("hdFieldName");
        if( oCtrl != null  )
            m_FieldName = oCtrl.value;
            
            //Bijender has changed for Mits 15919
           //if( (m_FieldName == "locationareadesc") || (m_FieldName == "ev_locationareadesc"))
           //{
           // oCtrl = document.getElementById("hdDateTimeStampEvtLocDesc");
            oCtrl = document.getElementById("hdDateStampFlag");        
            if( oCtrl != null )
                m_bDateStamp = oCtrl.value;

            //oCtrl = document.getElementById("hdFreezeEventLocDesc");

            //jramkumar for MITS 32095
            if (m_FieldName == "paytotheorder")
                oCtrl = document.getElementById("hdFreezePayOrder");
            else
                oCtrl = document.getElementById("hdFreezeTextFlag");
            
            if( oCtrl != null )
                m_bFreezeExisting = oCtrl.value;
        }
//        else
//        {
//            m_bFreezeExisting = 'false';

//	        //Check for Free Text date stamp setting if it's other memo type field
//	        oCtrl = document.getElementById("hdDateStampFlag");
//	        if( oCtrl != null )
//		        m_bDateStamp = oCtrl.value;
//  	    }
        //}
    //End Mits 15919

    // MITS resolution 11678
    function RMXOnClosePromptForPopUp()
    {
	    if (document.title=='Riskmaster Editor')
	    {
		    if (document.forms[0]!=null)
		    if (document.forms[0].idaction!=null)
		    {
			    if (document.forms[0].idaction.value=="Save")
			    return;
			    else if(document.forms[0].idaction.value == "MemoSave")
			    return;
		    }
	    }
	    var sMsg = "Please confirm that you have completed work within this pop-up.";

	    if ((window.event.clientX < 0) || (window.event.clientY < 0))
	    {
		    if(window.event!=null)
		    window.event.returnValue=sMsg;
	    }

    }

    function pageLoadedMemo()
    {
        qs();
        m_FieldName = unescape(m_qsParm['fieldname']);
        document.getElementById('hdFieldName').value = m_FieldName;
        var oCtrl = window.opener.document.getElementById( m_FieldName );
        
        //Sumit-MITS#18251 -10/13/2009 -Remove Control Id if TransactionHistoryGrid is selected
        if( oCtrl != null && m_FieldName!="TransactionHistoryGrid")
        document.getElementById('txtextComments').value = oCtrl.value;
        document.getElementById('hdComments').value = document.getElementById('txtextComments').value;
        //Sumit-MITS#18251 -11/05/2009 -Show label if opened from policy screen.
        if( m_FieldName=="TransactionHistoryGrid")
            //document.forms[0].all.lblNote.style.display="inline";
            document.getElementById('lblNote').style.display = "inline"; //igupta3 Mits# 33301

        getTextFlags();

        if(m_bCommentsFlag == 'false')
        {
            self.opener.document.OnEditMemoLoaded();
        }

        //try to load existing comments into upper frame
        //if the existing text should be freezed. Or load the existing
        //text into memo textbox
        var oMemo = document.getElementById("txtMemo");
        if( m_bFreezeExisting == 'false' )
        {
            oMemo.value = document.getElementById('hdComments').value;
            document.getElementById('hdComments').value = "";
        }
        
        //Sumit-MITS#18251 -10/13/2009 -Hide Existing comments window if ReadOnly is set to true
        if((document.getElementById('hdComments').value == "") || (m_bFreezeExisting == 'false') || document.getElementById('hdnReadOnly').value == 'true')
        {
            if( document.getElementById("tr_extComments") != null )
            {
                //igupta3 Mits# 33301
                //document.forms[0].all.tr_extComments.style.visibility = 'hidden';
                //document.forms[0].all.tr_extComments.style.display="none";
                document.getElementById('tr_extComments').style.visibility = 'hidden';
                document.getElementById('tr_extComments').style.display="none";
            }
        }

        var Today = new Date();
        var CurrentMonth = parseInt(Today.getMonth())+1
        var strDate = CurrentMonth +"/"+ Today.getDate() + "/" + Today.getFullYear();
       // var strTime = Today.getHours() + ":" + Today.getMinutes() + ":" + Today.getSeconds();
       
        var strTime = formatTime(Today, 'hh:mm:ss a');
        var strUid = document.getElementById('hdUid').value;
        var sDatetimeStamp = strDate + " " + strTime + " (" + strUid + ")";

        if( (m_bDateStamp == 'false') || (m_bFreezeExisting == 'false') )
        {
            if( document.getElementById("tr_DateStamp") != null )
            {
                //igupta3 Mits# 33301
                //document.forms[0].all.tr_DateStamp.style.visibility = 'hidden';
                //document.forms[0].all.tr_DateStamp.style.display = 'none';
                document.getElementById('tr_DateStamp').style.visibility = 'hidden';
                document.getElementById('tr_DateStamp').style.display = 'none';
            }
        }

        if (m_bDateStamp == 'true' && m_FieldName != "paytotheorder")//jramkumar for MITS 32095
        {
            if( m_bFreezeExisting == 'true' )
            oMemo.value = sDatetimeStamp;
            else
            {
                if( oMemo.value != "" )
                //MGaba2 :MITS 12780 :07/04/2008 : One line space between comments in free text type fields -start
                //oMemo.value += "\n";
                    oMemo.value += "\n\n";
                //MGaba2 :MITS 12780 :End
                oMemo.value += sDatetimeStamp;
            }
        }
        
        //Sumit-MITS#18251 -10/14/2009 -Copy memo content to Hidden field
        document.getElementById('hdnCommentsOnLoad').value = oMemo.value;
        //Sumit-MITS#18251 -10/12/2009 -Make Controls Read Only
        if(document.getElementById('hdnReadOnly').value == 'true')
            DisableControls();
        
        setcursor(oMemo);
        adjusterSize();
    }

    function formatTime(date, format) {
        format = format + "";
        var result = "";
        var i_format = 0;
        var c = "";
        var token = "";

        var H = date.getHours();
        var m = date.getMinutes();
        var s = date.getSeconds();
        var yyyy, yy, MMM, MM, dd, hh, h, mm, ss, ampm, HH, H, KK, K, kk, k;
        // Convert real date parts into formatted versions
        var value = new Object();
        value["H"] = H;

        if (("0" + H).length == 3)
            value["HH"] = ("0" + H).substr(1, 2);
        else
            value["HH"] = "0" + H;

        if (H == 0) { value["h"] = 12; }
        //else if (H<12){value["h"]=H-12;} 
        else if (H > 12) { value["h"] = H - 12; } // akashyap3 26-Feb-2009 MITS:14295
        else { value["h"] = H; }

        if (("0" + value["h"]).length == 3)
            value["hh"] = ("0" + value["h"]).substr(1, 2);
        else
            value["hh"] = "0" + value["h"];
        if (H > 11) { value["K"] = H - 12; } else { value["K"] = H; }
        value["k"] = H + 1;
        if (("0" + value["K"]).length == 3)
            value["KK"] = ("0" + value["K"]).substr(1, 2);
        else
            value["KK"] = "0" + value["K"];

        if (("0" + value["k"]).length == 3)
            value["kk"] = ("0" + value["k"]).substr(1, 2);
        else
            value["kk"] = "0" + value["k"]
        if (H > 11) { value["a"] = "PM"; }
        else { value["a"] = "AM"; }
        value["m"] = m;

        if (("0" + m).length == 3)
            value["mm"] = ("0" + m).substr(1, 2);
        else
            value["mm"] = "0" + m;
        value["s"] = s;

        if (("0" + s).length == 3)
            value["ss"] = ("0" + s).substr(1, 2);
        else
            value["ss"] = "0" + s;

        while (i_format < format.length) {
            c = format.charAt(i_format);
            token = "";
            while ((format.charAt(i_format) == c) && (i_format < format.length)) {
                token += format.charAt(i_format++);
            }
            if (value[token] != null) { result = result + value[token]; }
            else { result = result + token; }
        }
        return result;
    }
    //Sumit-MITS#18251 -10/12/2009 -Hide Controls
    function DisableControls()
    {
        document.forms[0].btnOK.disabled = "true";
        document.forms[0].btnCancel.disabled = "true";
        document.forms[0].btnFind.disabled = "true";
        document.forms[0].btnLoadSpellCheck.disabled = "true";
        document.forms[0].btnPrint.disabled = "true";
        document.forms[0].btnPreview.disabled = "true";
        document.forms[0].txtMemo.readOnly="true";
        //document.forms[0].all.lblNote.style.display="none";
        document.getElementById('lblNote').style.display = "none"; //igupta3 Mits# 33301
    }
    
    function qs()
    {
        var query = window.location.search.substring(1);
        query=query.replace(/\&amp;/g,'&');// igupta3 Mits # 33301
        var parms = query.split("&");
        for (var i = 0; i < parms.length; i++)
	    {
		    var pos = parms[i].indexOf('=');
		    if (pos > 0) 
		    {
		        var key = parms[i].substring(0, pos);
			    var val = parms[i].substring(pos + 1);
			    m_qsParm[key] = val;
		    }
	    }
    }

    function CopyDataToParent()
    {
	    document.getElementById('hdPlainText').value = document.getElementById('txtMemo').value;
	    self.opener.document.OnEditMemoSave();
	    //Sumit-MITS#18251 -10/14/2009 -Do not close pop up if opened from Policy Management screen
	    if(self.opener.document.forms[0].SysFormName!=null)
	    {
	        var sFormName=self.opener.document.forms[0].SysFormName.value;
	        if (!(sFormName=="policyenhal" ||sFormName=="policyenhgl"||sFormName=="policyenhpc"||sFormName=="policyenhwc"))
    	        window.close();
	    }
    }

    function adjusterSize()
    {
	    var iHeight = document.body.clientHeight;
	    var iWidth = document.body.clientWidth;
	    var trExtComments = document.getElementById("tr_extComments");
	    var oExtComments = document.getElementById("txtextComments");
	    var oNewComments = document.getElementById("txtMemo");
	    if( trExtComments.style.visibility == 'hidden')
	    {
		    oNewComments.style.height = iHeight * 0.85;
		    oNewComments.style.width = iWidth *0.86;
	    }
	    else
	    {
		    oExtComments.style.height = iHeight * 0.44;
		    oExtComments.style.width = iWidth * 0.86;
		    oNewComments.style.height = iHeight * 0.40;
		    oNewComments.style.width = iWidth * 0.86;
	    }
    }

	    //launch the find word pop-up window
	    function launchFind()
	     {
	         //aravi5 RMA-10493 Show modal issue on "Find" click in all editor pages starts
	         if (get_browserName() == "IE") {
	             window.showModalDialog("../../UI/Editor/find.html", self, "dialogWidth:400px;dialogHeight:150px;help:no;scroll:no;status:no");
	         }
	         else {
	             // window.showModalDialog('../../UI/Editor/preview.html', self, 'scroll:yes;resizable:yes;dialogHeight:' + iHeight + 'px;dialogWidth:' + iWidth + 'px');
	             previewPopupWindow = window.open("../../UI/Editor/find.html", self, "width=400,height=150,help=no,scroll=no,status=no");
	             previewPopupWindow.focus();
	             document.onmousedown = focusPreviewPopup;
	             document.onkeypress = focusPreviewPopup;
	             document.onmousemove = focusPreviewPopup;
	         }
	         //aravi5 RMA-10493 Show modal issue on "Find" click in all editor pages ends
	    }

	    //launch the preview window
	    function launchPreview() {
	        var iWidth = screen.availWidth - 200;
	        var iHeight = screen.availHeight - 100;

	        var left = (screen.availWidth / 2) - (iWidth / 2);
	        var top = (screen.availHeight / 2) - (iHeight / 2);
	        if (get_browserName() == "Chrome") {
	            //[JIRA](RMA - 6357)
	            // window.showModalDialog('../../UI/Editor/preview.html', self, 'scroll:yes;resizable:yes;dialogHeight:' + iHeight + 'px;dialogWidth:' + iWidth + 'px');
	            previewPopupWindow = window.open('../../UI/Editor/preview.html', " ", 'height=' + iHeight + 'px,width=' + iWidth + 'px,scrollbars=yes,resizable=yes, top=' + top + ', left=' + left);
	            previewPopupWindow.focus();
	            document.onmousedown = focusPreviewPopup;
	            document.onkeypress = focusPreviewPopup;
	            document.onmousemove = focusPreviewPopup;
	        }
	        else {
	            window.showModalDialog('../../UI/Editor/preview.html', self, 'scroll:yes;resizable:yes;dialogHeight:' + iHeight + 'px;dialogWidth:' + iWidth + 'px');
	        }

	    }
	    function focusPreviewPopup() {
	        if (previewPopupWindow && !previewPopupWindow.closed) { previewPopupWindow.focus(); }
	    }

    //pop-up a new windows and try to print the content
    function printField() {
	    var sComments = "";
	    var tr_extComments = document.getElementById("tr_extComments");
	    if(tr_extComments.style.visibility!="hidden")
		    sComments = document.getElementById('txtextComments').value;
	    else
		    sComments = document.getElementById('txtMemo').value;

	    sComments = sComments.replace(/\r\n/gi, "<br/>");
	    sComments = sComments.replace(/[\r\n]/gi, "<br/>");
	    pWin = window.open("","pWin","location=yes, menubar=yes, toolbar=yes");
	    pWin.document.open();
	    pWin.document.write("<html><head&></head><body>");
	    pWin.document.write(sComments);
	    pWin.document.write("</body></html>");
	    pWin.print();
	    pWin.document.close();
	    pWin.close();
	     
    }

    function setcursor(el)
     {
        if (el.setSelectionRange) // Mozilla firefox
        {
            el.focus();
            //  el.setSelectionRange(st,end); 
        }
        else 
        {
            if (el.createTextRange)   //IE
            {
                range = el.createTextRange();
                range.collapse(false);
                range.select();
            }
        }
     }    


    window.onresize = adjusterSize;
    </script>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body onload ="pageLoadedMemo();">
 <form id="frmWizardData" name="frmWizardData"  runat="server">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" >
   <tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
         </tr>
     <tr id="tr_extComments">
        <td class="caption" width="10%">Existing Content:</td>
        <td>
             <asp:TextBox RMXRef="Instance/Document/Editor/ExistingComments/Comments" runat="server" id="txtextComments"   TextMode="MultiLine" readonly="true"/>
        </td>
    </tr>
     <!--Sumit-MITS#18251 -11/05/2009 -Label for displaying note while entering comments from Transaction History tab on Policy management window-->
	<tr id="tr_note" >
	<td colspan="2" class="transcommentslabel">
			<asp:Label ID="lblNote" style="display:none" Text="Note: Comments once entered cannot be edited later. " runat="server"></asp:Label>
    </td>
	</tr>
	    
    <tr id="tr_newComments">
        <td class="caption">Content:</td>
        <td>
            <asp:TextBox RMXRef="Instance/Document/Editor/ExistingComments/Comments" runat="server" textmode="Multiline" id="txtMemo"/>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <asp:Button class="button" id="btnOK"   Text ="  OK  " runat="server" OnClientClick ="CopyDataToParent();" />
	        &#160;&#160;
	        <asp:Button class="button" id="btnCancel" Text =" Cancel" runat="server" OnClientClick ="window.close();"/>
	        &#160;&#160;	
	        <asp:Button class="button" id="btnFind" Text =" Find " runat="server" OnClientClick ="launchFind();return false;"/>
	        &#160;&#160;
	        <asp:Button class="button" id="btnLoadSpellCheck" Text ="Spell Check" runat="server" OnClientClick ="return false;"/>
            &#160;&#160;
            <asp:Button class="button" id="btnPrint" Text ="Print" runat="server" OnClientClick ="printField();return false;"/>
            &#160;&#160;
            <asp:Button class="button" id="btnPreview" Text ="Preview" runat="server" OnClientClick ="launchPreview();return false;"/>
        </td>
    </tr>
    </table>
 <ig_spell:WebSpellChecker ID="WebSpellChecker1" runat="server" ButtonId ="btnLoadSpellCheck" TextComponentId="txtMemo">
 </ig_spell:WebSpellChecker>

    <asp:TextBox runat="server" id="hdComments"  style="display:none" Text="0" TextMode="MultiLine" RMXRef="Instance/Document/Editor/ExistingComments/Comments" />
    <asp:TextBox runat="server" id="hdHTMLComments"  style="display:none" Text="0" />  
    <asp:TextBox runat="server" id="hdPlainText"  style="display:none" Text="0" TextMode="MultiLine"  RMXRef="Instance/Document/Editor/PlainText" />
    <asp:TextBox runat="server" id="hdTextHtml"  style="display:none" Text="0" RMXRef="Instance/Document/Editor/EditorTextHtml" />
    <asp:TextBox runat="server" id="hdTextEncodedHtml"  style="display:none" Text="0" RMXRef="Instance/Document/Editor/EditorTextEncodedHtml" />
    <!-- Hidden fields required for Memo -->
    <asp:TextBox runat="server" id="hdCommentsFlag"  style="display:none"/>
    <asp:TextBox runat="server" id="hdFieldName"  style="display:none"/>
    <asp:TextBox runat="server" id="hdCallback"  style="display:none" Text="0" />
    <asp:TextBox runat="server" id="hdUid"  style="display:none" Text="0"  RMXRef="Instance/Document/Editor/RecordDetails/@UserName" />
    <asp:TextBox runat="server" id="idaction"  style="display:none" Text="0" />

    <asp:TextBox runat="server" id="hdFreezeTextFlag" style="display:none"  RMXRef="/Instance/Document/Editor/Comments/@FreezeTextFlag">
    </asp:TextBox> 
    <asp:TextBox runat="server" id="hdDateStampFlag" style="display:none" RMXRef="Instance/Document/Editor/Comments/@DateStampFlag">
    </asp:TextBox> 
    <asp:TextBox runat="server" id="hdFreezeDateFlag" style="display:none"  RMXRef="Instance/Document/Editor/Comments/@FreezeDateFlag">
    </asp:TextBox>
    <asp:TextBox runat="server" id="hdDateTimeStampEvtLocDesc" style="display:none" RMXRef="Instance/Document/Editor/Comments/@DateTimeStampEvtLocDesc">
    </asp:TextBox>
    <asp:TextBox runat="server" id="hdFreezeEventDesc" style="display:none" RMXRef="Instance/Document/Editor/Comments/@FreezeEventDesc">
    </asp:TextBox>
    <asp:TextBox runat="server" id="hdFreezeEventLocDesc" style="display:none" RMXRef="Instance/Document/Editor/Comments/@FreezeEventLocDesc">
    </asp:TextBox>
    <!--jramkumar for MITS 32095-->
    <asp:TextBox runat="server" id="hdFreezePayOrder" style="display:none" RMXRef="Instance/Document/Editor/Comments/@FreezePayOrder">
    </asp:TextBox>
    <!--Sumit-MITS#18251 -10/12/2009 -Set hidden field for Editor-->
    <asp:TextBox runat="server" id="hdnReadOnly"  style="display:none"/>
    <!--Sumit-MITS#18251 -10/12/2009 -Hidden field for Transaction ID from Link-->
    <asp:TextBox runat="server" id="hdnTransIdFromLink"  style="display:none"/>
    <!--Sumit-MITS#18251 -10/12/2009 -Hidden field for Existing comments-->
    <asp:TextBox runat="server" id="hdnCommentsOnLoad"  style="display:none"/>
  </form> 
 </body>
</html>
