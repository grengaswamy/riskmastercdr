﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MailMergeIssue.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MailMergeIssue"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>




<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Riskmaster</title>
     <uc4:CommonTasks ID="CommonTasks1" runat="server" />
         <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/merge.js")%>'></script>
        <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/OpenRTF.js")%>'></script>
</head>
<body>
    <form id="form1" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
   	<p>
						<h3><asp:Label ID="lblHeading" runat="server" Text="<%$ Resources:lblHeadingResrc %>"></asp:Label></h3>
                        <asp:Label ID="lblMailMergeIssue" runat="server" Text="<%$ Resources:lblMailMergeIssueResrc %>"></asp:Label>						
						<br /> <asp:Label ID="lblMergeMessage" runat="server" Text="<%$ Resources:lblMergeMessageResrc %>"></asp:Label>	                        
						<br /><br /><hr/>
                        <asp:Label ID="lblNoRecordMessage" runat="server" Text="<%$ Resources:lblNoRecordMessageResrc %>"></asp:Label>	  
						<br />
					</p>
					<hr />
					
					<center>
						<asp:Button ID="Button1" onclick="btnBack_Click" runat="server" Text=" <%$ Resources:btnBackRetry %>  "  CssClass=button UseSubmitBehavior="true" />

						       <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancel %>" CssClass=button 
             EnableTheming="False"  UseSubmitBehavior="true" OnClientClick="return Cancel();" />
					</center>
					 <asp:HiddenField ID="TableName" runat="server" />
        <asp:HiddenField ID="FormName" runat="server" />
        <asp:HiddenField ID="RecordId" runat="server" />
        <asp:HiddenField ID="txtState" runat="server" />
        <asp:HiddenField ID="txtState_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumentformat" runat="server" />
         <asp:HiddenField ID="txtmergedocumentformat_cid" runat="server" />
    <asp:HiddenField ID="allstatesselected" runat="server" />
      <asp:HiddenField ID="psid" runat="server" />
    </form>
</body>
</html>
