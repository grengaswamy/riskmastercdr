﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.MailMerge
{
    public partial class FetchData : System.Web.UI.Page
    {
        public XmlDocument Model = null;
        string MergeMessageTemplate = "<Message><Authorization>9e17a7dd-bc85-4461-81e5-1156b7a5f6b9</Authorization><Call>"+
            "<Function>MailMergeAdaptor.FetchData</Function></Call><Document><Template><TemplateId></TemplateId>"+
            "<RecordId>1</RecordId></Template></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sSubmitedFrmRsv = AppHelper.GetQueryStringValue("page");
                if (!string.IsNullOrEmpty(sSubmitedFrmRsv))
                {
                    if (sSubmitedFrmRsv == "MergeEditResult")
                    {
                        Server.Transfer("MergeEditResult.aspx?EmailCheck=" + this.EmailCheck.Value);
                    }
                }
                SaveValuesFromPreviousScreen();
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("FetchData.aspx"), "FetchDataValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "FetchDataValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end
                if (!Page.IsPostBack)
                {
                    MergeMessageTemplate = PrepareServiceMessage(MergeMessageTemplate);
                    string sReturn = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    //bkuzhanthaim : RMA-8407
                        System.Web.HttpBrowserCapabilities browser = Request.Browser;
                        string sBrowser = browser.Browser;
                        if (sBrowser != "IE" && sBrowser != "InternetExplorer")
                            hdnSilver.Value = "true";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            DataBind();
        }
        private string PrepareServiceMessage(string MergeMessageTemplate)
        {
            string TemplateId = lettername.Value;
            string[] sep={"***"};
            string[] arr = TemplateId.Split(sep,StringSplitOptions.None);
            TemplateId=arr[1];
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//TemplateId", TemplateId);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//RecordId", RecordId.Value);
            return MergeMessageTemplate;
        }
        private void SaveValuesFromPreviousScreen()
        {
            if (!Page.IsPostBack)
            {
                Notes.Value = AppHelper.GetFormValue("Notes");
                Comments.Value = AppHelper.GetFormValue("Comments");
                Keywords.Value = AppHelper.GetFormValue("Keywords");
                Subject.Value = AppHelper.GetFormValue("Subject");
                DocTitle.Value = AppHelper.GetFormValue("DocTitle");
                Category.Value = AppHelper.GetFormValue("Category");
                Class.Value = AppHelper.GetFormValue("Class");
                Type.Value = AppHelper.GetFormValue("Type");
                FormName.Value = AppHelper.GetValue("FormName");
                TableName.Value = AppHelper.GetValue("TableName");
                RecordId.Value = AppHelper.GetValue("RecordId");
                txtState.Value = AppHelper.GetFormValue("txtState");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
                allstatesselected.Value = AppHelper.GetFormValue("allstatesselected");
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
                lettername.Value = AppHelper.GetFormValue("lettername");
                recordselect.Value = AppHelper.GetFormValue("recordselect");
                attach.Value = AppHelper.GetFormValue("attach");
                psid.Value = AppHelper.GetValue("psid");
                //spahariya MITS 28867
                if (string.IsNullOrEmpty(AppHelper.GetValue("EmailCheck")))
                    EmailCheck.Value = AppHelper.GetQueryStringValue("EmailCheck");
                else
                    EmailCheck.Value = AppHelper.GetValue("EmailCheck");
               
                if (string.IsNullOrEmpty(AppHelper.GetValue("lstMailRecipient")))
                    lstMailRecipient.Value = AppHelper.GetValue("txtSelectedRecipient");
                else
                    lstMailRecipient.Value = AppHelper.GetValue("lstMailRecipient");

                txtSelectedRecipient.Value = AppHelper.GetValue("txtSelectedRecipient");
                string TemplateId = lettername.Value;
                string[] sep = { "***" };
                string[] arr = TemplateId.Split(sep, StringSplitOptions.None);
                if (arr[0].Length >1)
                     title.Text = arr[0];
            }

        }
        protected void btnNext_Click(object sender, EventArgs e)
        {

            Server.Transfer("MergeTemplate1.aspx");
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (recordselect.Value == "Checked" && attach.Value == "Checked")
            {
                Server.Transfer("MergeTemplate2.aspx?opentype=samewindow");
            }
            else
            {
                Server.Transfer("MergeTemplate1.aspx");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}
