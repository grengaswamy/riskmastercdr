﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeTemplate1.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeTemplate1" %>
<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>





<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
        <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/merge.js")%>'></script>
           <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/OpenRTF.js")%>'></script>
</head>
<body onload="setdefaults();displayemailoption();">
    <form id="frmData" runat="server">
    <p class="gen"></p>
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
   <table width="100%" border="0">
    <tr>
     <td colspan="3" class="formtitle">
      								<asp:Label ID="lblUseMergeLetter" runat="server" Text="<%$ Resources:lblUseMergeLetterResrc %>"></asp:Label>
      							
     </td>
    </tr>
    <tr>
     <td>
      								&nbsp;
      							
     </td>
    </tr>
    <tr>
     <td>
      								<asp:Label ID="lblFormSelect" runat="server" Text="<%$ Resources:lblFormSelectResrc %>"></asp:Label>
      							
     </td>
    </tr>
    <tr>
     <td>
      								&nbsp;
      							
     </td>
    </tr>
    <tr>
     <td colspan="3" class="ctrlgroup">
      								<asp:Label ID="lblBasicOpt" runat="server" Text="<%$ Resources:lblBasicOptResrc %>"></asp:Label>
      							
     </td>
    </tr>
    <tr>
     <td>
      								&nbsp;
      							
     </td>
    </tr>
    <tr>
     <td nowrap="1" colspan="3"><input type="hidden" runat=server value="" id="previousaction"><input type="hidden" name="" value=""  runat=server id="filename">							
      		
      								<asp:Label ID="lblLetterName" runat="server" Text="<%$ Resources:lblLetterNameResrc %>"></asp:Label>  								
                                    <asp:DropDownList ID="lettername" runat="server" onchange="javascript:lettername_IndexChanged();" >	</asp:DropDownList>						
      								<%--<select runat="server" id="lettername" >

      </select>--%></td>
    </tr>
    <tr>
     <td>
      								&nbsp;
      							
     </td>
    </tr>
    <tr>
     <td colspan="3"><input type="checkbox" runat="server" id="recordselect" value='Checked'>
      										<asp:Label ID="lblAvailableRcds" runat="server" Text="<%$ Resources:lblAvailableRcdsResrc %>"></asp:Label>
      										<em>
       											<asp:Label ID="lblDefaultsFrstRcds" runat="server" Text="<%$ Resources:lblDefaultsFrstRcdsResrc %>"></asp:Label>
       										</em></td>
    </tr>
    <tr>
     <td colspan="3">
      <div><input type="checkbox"  value="Checked" runat="server" id="attach">
       										<asp:Label ID="lblAttachResultingDoc" runat="server" Text="<%$ Resources:lblAttachResultingDocResrc %>"></asp:Label>
       								
      </div>
     </td>
    </tr>
    <tr>
    <table>
    <tr>
            <td runat="server" id="lblEmailCheck">
                <asp:Label ID="SendCopyEmail" runat="server" Text="<%$ Resources:SendCopyEmail %>"></asp:Label>
                
            </td>            
            <td>
                <!--RMA-1248 starts. Set check box default as disabled-->
                <%--<input type="checkbox" runat="server" value="true" id="EmailCheck">--%>
                <input type="checkbox" runat="server" value="true" id="EmailCheck" disabled="disabled"/>
                <!--RMA-1248 ends-->
            </td>
            <td runat="server" id ="lblMailRecipient">
               <asp:Label ID="DesignatedRecipient" runat="server" Text="<%$ Resources:DesignatedRecipient %>"></asp:Label>
            </td>            
            <td>
                <!--RMA-1248 starts. Set check box default as disabled-->
                <%--<asp:DropDownList ID ="lstMailRecipient" runat="server" ></asp:DropDownList>--%>
                <%--<asp:DropDownList ID ="lstMailRecipient" runat="server" disabled="disabled" ></asp:DropDownList>--%>
                <!--RMA-1248 ends-->
                <%--changed by swati for RMA - 36930--%>
                <asp:ListBox ID="lstMailRecipient" width="100px" runat="server" SelectionMode="Multiple" disabled="disabled"></asp:ListBox>                
            </td>
            </tr>
            </table>
    </tr>
    <tr>
     <td colspan="2"><br><p class="gen"><em>
        										<asp:Label ID="lblProduceDocument" runat="server" Text="<%$ Resources:lblProduceDocumentResrc %>"></asp:Label>
        									</em></p>
        									  <asp:Button ID="Button1" onclick="btnBack_Click" runat="server" Text="<%$ Resources:btnBackResrc %>"  CssClass=button UseSubmitBehavior="true" />
         <asp:Button ID="Button2" runat="server" Text="<%$ Resources:btnNextResrc %>"     CssClass=button 
             onclick="btnNext_Click"/>
         <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass=button 
             EnableTheming="False" onclick="btnCancel_Click" UseSubmitBehavior="true" OnClientClick="return Cancel();" />
        									</td>
    </tr><input type="hidden" runat='server' value="" id="hdnDirectDisplay"></table>
    <input type="hidden" runat="server" value="" id="hdnTemplateId"><input type="hidden" runat="server"  id="hdnCatId"><input type="hidden" runat="server" value="" id="hdnLetterName">
    
  <asp:HiddenField ID="TableName" runat="server" />
        <asp:HiddenField ID="FormName" runat="server" />
        <asp:HiddenField ID="RecordId" runat="server" />
        <asp:HiddenField ID="txtState" runat="server" />
        <asp:HiddenField ID="txtState_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumentformat" runat="server" />
         <asp:HiddenField ID="txtmergedocumentformat_cid" runat="server" />
    <asp:HiddenField ID="allstatesselected" runat="server" />
         <asp:HiddenField ID="savemysettingsselected" runat="server" /> <%--//Mits:36030 JIRA RMA-337 nshah28 --%>
    
    <asp:HiddenField ID="psid" runat="server" />
      <asp:HiddenField ID="hdnrowids" runat="server" />
 
            <asp:HiddenField ID="DocTitle" runat="server" />
        <asp:HiddenField ID="Subject" runat="server" />   
        <asp:HiddenField ID="Keywords" runat="server" />   
        <asp:HiddenField ID="Notes" runat="server" />   
        <asp:HiddenField ID="CatId" runat="server" />   
          <asp:HiddenField ID="Class" runat="server" />   
        <asp:HiddenField ID="Category" runat="server" />   
        <asp:HiddenField ID="Type" runat="server" />   
        <asp:HiddenField ID="Comments" runat="server" />   
         <asp:HiddenField ID="DirectDisplay" runat="server" />
         <!-- Mona:PaperVisionMerge : Animesh Inserted -->
         <asp:HiddenField ID="PaperVisionActive" runat="server" />
         <asp:HiddenField ID="PaperVisionValidation" runat="server" />
         <!-- Animesh Insertion Ends -->
         <asp:HiddenField ID="txtSelectedRecipient" runat="server" />
         <asp:HiddenField ID="hdnSilver" runat="server" />
         
   
    </form>
</body>
</html>
