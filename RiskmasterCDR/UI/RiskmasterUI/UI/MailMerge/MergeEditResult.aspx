﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MergeEditResult.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeEditResult" ValidateRequest="false" EnableViewStateMac="false"%>

<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>





<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
         <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/merge.js")%>'></script>
        <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/OpenRTF.js")%>'></script>
        <script type="text/javascript" language="javascript" src="../../Scripts/Silverlight.js"></script>
</head>
<body onload="Javascript:return InitPageSettingsOnBodyLoad();">
    <form id="frmData" runat="server">
<rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
    <table width="100%" border="0"><B><asp:Label ID="lblMrgTmpltDocAttach" runat="server" Text="<%$ Resources:lblMrgTmpltDocAttachResrc %>"></asp:Label><asp:Label ID="FileName" runat=server></asp:Label></B><br><br><tr>
     <td colspan="2" class="ctrlgroup"><asp:Label ID="lblCmpltWrdMrg" runat="server" Text="<%$ Resources:lblCmpltWrdMrgResrc %>"></asp:Label></td>
    </tr>
    <tr>
    <td valign="top" colspan="2">
										<asp:Label ID="lblNowCmpltWrdMrg" runat="server" Text="<%$ Resources:lblNowCmpltWrdMrgResrc %>"></asp:Label><br /><br />
											<ol>
                        <li><asp:Label ID="lblMrgEditLstOpt1" runat="server" Text="<%$ Resources:lblMrgEditLstOpt1Resrc %>"></asp:Label></li>
                        <li><asp:Label ID="lblMrgEditLstOpt2" runat="server" Text="<%$ Resources:lblMrgEditLstOpt2Resrc %>"></asp:Label></li>
                        <li><asp:Label ID="lblMrgEditLstOpt3" runat="server" Text="<%$ Resources:lblMrgEditLstOpt3Resrc %>"></asp:Label></li>
                        <li><asp:Label ID="lblMrgEditLstOpt4" runat="server" Text="<%$ Resources:lblMrgEditLstOpt4Resrc %>"></asp:Label></li>
                        <li><b><asp:Label ID="lblMrgEditLstOpt5" runat="server" Text="<%$ Resources:lblMrgEditLstOpt5Resrc %>"></asp:Label></b> <asp:Label ID="lblMrgEditLstOpt6" runat="server" Text="<%$ Resources:lblMrgEditLstOpt6Resrc %>"></asp:Label></li>
										    <li><asp:Label ID="lblMrgEditLstOpt7Resrc" runat="server" Text="<%$ Resources:lblMrgEditLstOpt7Resrc %> "></asp:Label><b>&nbsp;<asp:Label ID="lblMrgEditLstOpt8" runat="server" Text="<%$ Resources:lblMrgEditLstOpt8Resrc %>" ></asp:Label></b>&nbsp;<asp:Label ID="lblMrgEditLstOpt9" runat="server" Text="  <%$ Resources:lblMrgEditLstOpt9Resrc %>"></asp:Label></li>
										</ol>		
      <center>
      <input type="button" name="Launch" value="<%$ Resources:btnLaunchMergeResrc %>" runat="server" class="button" id="btnLaunchMerge" disabled="true" onclick="Javascript:return StartEditor('',true); " />
      
    

    </center><br><br></td>
    
    </tr>
    <tr>
     <td colspan="2" class="ctrlgroup"></td>
    </tr>
    <tr>
     <td colspan="2">
       <asp:Button ID="btnFinish" runat="server" Text="<%$ Resources:btnFinishResrc %>" CssClass=button 
             EnableTheming="False" onclick="btnFinish_Click" UseSubmitBehavior="true" OnClientClick="return Finish();" />
       
       <input type="button" name="Close" value="<%$ Resources:btnCloseResrc %>" id="Close" runat="server" class="button" onclick="Javascript:return CleanAndClose();; " />
       
       
   </td>
    </tr>
    <tr>
    <td style="margin-left: 80px">
        <asp:HiddenField ID="hdnRTFContent" runat="server" />
        <asp:HiddenField ID="hdnDataSource" runat="server" />
        <asp:HiddenField ID="hdnHeaderContent" runat="server" />
        <asp:HiddenField ID="hdnTempFileName" runat="server" />
        <asp:HiddenField ID="hdnNumRecords" runat="server" />
        <asp:HiddenField ID="hdnNewFileName" runat="server" />
          <asp:HiddenField ID="FormName" runat="server" />
        <asp:HiddenField ID="hdnError" runat="server" />
        <asp:HiddenField ID="firstTime" runat="server" />
        <asp:HiddenField ID="NewFileName" runat="server" />
              <asp:HiddenField ID="attach" runat="server" />
              <asp:HiddenField ID="hdnrowids" runat="server" />
           <asp:HiddenField ID="recordselect" runat="server" />
           <asp:HiddenField ID="lettername" runat="server" />
            <asp:HiddenField ID="DocTitle" runat="server" />
        <asp:HiddenField ID="Subject" runat="server" />   
        <asp:HiddenField ID="Keywords" runat="server" />   
        <asp:HiddenField ID="Notes" runat="server" />   
         <asp:HiddenField ID="RecordId" runat="server" />
          <asp:HiddenField ID="Class" runat="server" />   
        <asp:HiddenField ID="Category" runat="server" />   
        <asp:HiddenField ID="Type" runat="server" />   
        <asp:HiddenField ID="Comments" runat="server" />   
         <asp:HiddenField ID="DirectDisplay" runat="server" />
          <asp:HiddenField ID="EmailCheck" runat="server" />
        <asp:HiddenField ID="lstMailRecipient" runat="server" />
        <asp:HiddenField ID="TableName" runat="server" />
        <asp:HiddenField ID="RetMessage" runat="server" />
       <asp:HiddenField ID="hdnFlagMessage" runat="server" />
         <asp:HiddenField ID="txtSelectedRecipient" runat="server" />
		 <asp:HiddenField ID="hdnSilver" runat="server" />
    </td>
        <asp:HiddenField ID="psid" runat="server" />
    
    </tr>
    
   </table>
         <div id="silverlightControlHost">
        <object data="data:application/x-silverlight-2," id="Silverlight" type="application/x-silverlight-2" width="100%" height="100%">
		  <param name="source" value="ClientBin/RiskmasterSL.xap"/>
		  <param name="onError" value="onSilverlightError" />
		  <param name="background" value="white" />
		  <param name="minRuntimeVersion" value="5.0.61118.0" />
		  <param name="autoUpgrade" value="true" />
           <param name="onload" value="silverlight_onload" />
		  <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=5.0.61118.0" style="text-decoration:none">
 			  <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style:none"/>
		  </a>
	    </object><iframe id="_sl_historyFrame" style="visibility:hidden;height:0px;width:0px;border:0px"></iframe></div>
    </form>
</body>
</html>
