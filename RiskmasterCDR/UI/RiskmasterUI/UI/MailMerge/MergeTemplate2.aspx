﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MergeTemplate2.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeTemplate2" ValidateRequest="false" EnableViewStateMac="false"%>

<%@ Import Namespace="System.Xml" %>


<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>





<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
        <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/merge.js")%>'></script>
           <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/OpenRTF.js")%>'></script>
             <script language='javascript'>
        function submitthisPage()
        {
            var open='<%#AppHelper.GetValue("opentype") %>'
            if (open!='samewindow')
            {
                if (document.forms[0].firstTime.value=="")
                {
                       document.forms[0].firstTime.value=1;
                       document.forms[0].attach.value=window.opener.document.forms[0].attach.value;
                       try
                       {
                           if (window.opener.document.forms[0].recordselect.checked)
                           {
                              document.forms[0].recordselect.value=window.opener.document.forms[0].recordselect.checked;
                           }
                       }
                       catch(e){document.forms[0].recordselect.value=window.opener.document.forms[0].recordselect.value;}
                       
                       document.forms[0].lettername.value=window.opener.document.forms[0].lettername.value;
                       document.forms[0].DocTitle.value=window.opener.document.forms[0].DocTitle.value;
                       document.forms[0].Subject.value=window.opener.document.forms[0].Subject.value;
                       document.forms[0].Keywords.value=window.opener.document.forms[0].Keywords.value;
                       document.forms[0].Notes.value=window.opener.document.forms[0].Notes.value;
                       document.forms[0].Class.value=window.opener.document.forms[0].Class.value;
                       document.forms[0].Category.value=window.opener.document.forms[0].Category.value;
                       document.forms[0].Type.value=window.opener.document.forms[0].Type.value;
                       document.forms[0].Comments.value=window.opener.document.forms[0].Comments.value;
                       document.forms[0].DirectDisplay.value=window.opener.document.forms[0].DirectDisplay.value;
                       document.forms[0].hdnrowids.value=window.opener.document.forms[0].hdnrowids.value;
                       //alert(document.forms[0].hdnrowids.value);
                       document.forms[0].RecordId.value=window.opener.document.forms[0].RecordId.value;
                       document.forms[0].lettername.value=window.opener.document.forms[0].lettername.value;
                         document.forms[0].TableName.value=window.opener.document.forms[0].TableName.value;
                       document.forms[0].CatId.value=window.opener.document.forms[0].CatId.value;
                        document.forms[0].psid.value=window.opener.document.forms[0].psid.value;
                        document.forms[0].FormName.value=window.opener.document.forms[0].FormName.value;
                        document.forms[0].EmailCheck.value = window.opener.document.forms[0].EmailCheck.value;
                        document.forms[0].lstMailRecipient.value = window.opener.document.forms[0].lstMailRecipient.value;
						//swati - changes for Gap 8 MITS # 36930 - start
						document.forms[0].txtSelectedRecipient.value = window.opener.document.forms[0].txtSelectedRecipient.value;
                        //swati - changes for Gap 8 MITS # 36930 - end
                       document.forms[0].submit();
                }
            }
            else
            document.forms[0].firstTime.value=1;
        }   
                   
        </script>
</head>
<!--skarandhama2-30242-26/04/2013 window.focus(); -->
<body onload="submitthisPage();window.focus();">
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
 <table width="100%" border="0">
    <tr>
     <td colspan="3" class="formtitle">						
      							<asp:Label ID="lblUseMergeLetter" runat="server" Text="<%$ Resources:lblUseMergeLetterResrc %>"></asp:Label>
                                [		
      								<asp:Label runat=server ID="LetterTitle"></asp:Label>
      							]
      						
     </td>
    </tr>
    <tr>
     <td>
      							&nbsp;
      						
     </td>
    </tr>
    <tr>
     <td>
      							&nbsp;
      						
     </td>
    </tr>
    <tr>
     <td>
      							&nbsp;
      						
     </td>
    </tr>
    <tr>
     <td colspan="3">
      							<asp:Label ID="lblInfoResults" runat="server" Text="<%$ Resources:lblInfoResultsResrc %>"></asp:Label>
      						
     </td>
    </tr>
    <tr>
     <td>
      							&nbsp;
      						
     </td>
    </tr>
    <tr>
     <td></td>
    </tr>
    <tr>
     <td colspan="3" class="ctrlgroup">
      							<asp:Label ID="lblBasicOpt" runat="server" Text="<%$ Resources:lblBasicOptResrc %>"></asp:Label>
      						
     </td>
    </tr>
    <tr>
     <td></td>
    </tr>
    <tr>
     <td></td>
    </tr>
    <tr>
     <td width="10%"></td>
     <td width="10%">
      							<asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitleResrc %>"></asp:Label>
      						
     </td>
     <td width="80%"><input type="text" runat='server' value="" id="DocTitle" size="32" maxlength="32"></td>
    </tr>
    <tr>
     <td width="10%"></td>
     <td width="10%">
      								<asp:Label ID="lblSubject" runat="server" Text="<%$ Resources:lblSubjectResrc %>"></asp:Label>
      							
     </td>
     <td width="80%"><input type="text" runat='server' value="" id="Subject" size="50" maxlength="50"></td>
    </tr>
    <tr>
     <td><br></td>
    </tr>
    <tr>
     <td width="10%"></td>
     <td width="10%">
      							<asp:Label ID="lblKeywords" runat="server" Text="<%$ Resources:lblKeywordsResrc %>"></asp:Label>
      						
     </td>
     <td width="80%"><input type="text" runat='server' value="" id="Keywords" size="50" maxlength="200"></td>
    </tr>
    <tr>
     <td><br></td>
    </tr>
    <tr>
     <td width="10%"></td>
     <td width="10%">
      							<asp:Label ID="lblNotes" runat="server" Text="<%$ Resources:lblNotesResrc %>"></asp:Label>
      						
     </td>
     <td width="80%"><textarea cols="38" wrap="soft" runat='server' id="Notes" rows="6"></textarea></td>
    </tr>
    <tr>
     <td><br></td>
    </tr>
    <tr>
     <td width="10%"></td>
     <td width="10%">
      							<asp:Label ID="lblType" runat="server" Text="<%$ Resources:lblTypeResrc %>"></asp:Label>
      						
     </td>
     <td width="80%"><select runat='server' id="Type">
      
  </select></td>
    </tr>
    <tr>
     <td width="10%"></td>
     <td width="10%">
      							<asp:Label ID="lblClass" runat="server" Text="<%$ Resources:lblClassResrc %>"></asp:Label>
      						
     </td>
     <td width="80%"><select runat='server' id="Class">
     
      </select></td>
    </tr>
    <tr>
     <td width="10%"></td>
     <td width="10%">
      							<asp:Label ID="lblCategory" runat="server" Text="<%$ Resources:lblCategoryResrc %>"></asp:Label>
      						
     </td>
     <td width="80%"><select runat='server' id="Category">
      
     </select></td>
    </tr>
    <tr>
     <td width="10%"></td>
     <td width="90%" align="left" colspan="2">

     <asp:Label ID="lblCommentsAttach" runat="server" Text="<%$ Resources:lblCommentsAttachResrc %>"></asp:Label>      							
      						
     </td>
    </tr>
    <tr>
     <td width="10%"></td>
     <td width="90%" align="left" colspan="2"><textarea cols="70" wrap="soft" runat=server id="Comments" rows="3"></textarea></td>
    </tr>
    <tr>
     <td colspan="3"><br><p class="gen"><em><asp:Label ID="lblProduceDocument" runat="server" Text="<%$ Resources:lblProduceDocumentResrc %>"></asp:Label> 
        								</em></p><input type="hidden" runat=server value="" id="docresultflag">
        								  <asp:Button ID="Button1" onclick="btnBack_Click" runat="server" Text="<%$ Resources:btnBackResrc %>"  CssClass=button UseSubmitBehavior="true" />
         <asp:Button ID="Button2" runat="server" Text="<%$ Resources:btnNextResrc %>"   UseSubmitBehavior="false"  
                     CssClass=button OnClientClick="return ValidateMergeLetter2();" 
                     />
         <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass=button 
             EnableTheming="False" onclick="btnCancel_Click" UseSubmitBehavior="true" OnClientClick="return Cancel();" /></tr>
   </table>
   <asp:HiddenField ID="TableName" runat="server" />
        <asp:HiddenField ID="FormName" runat="server" />
        <asp:HiddenField ID="RecordId" runat="server" />
        <asp:HiddenField ID="txtState" runat="server" />
        <asp:HiddenField ID="txtState_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumentformat" runat="server" />
         <asp:HiddenField ID="txtmergedocumentformat_cid" runat="server" />
    <asp:HiddenField ID="allstatesselected" runat="server" />
    <asp:HiddenField ID="CatId" runat="server" />   
       <asp:HiddenField ID="attach" runat="server" />
      <asp:HiddenField ID="firstTime" runat="server" />
           <asp:HiddenField ID="hdnrowids" runat="server" />
           <asp:HiddenField ID="recordselect" runat="server" />
           <asp:HiddenField ID="lettername" runat="server" />
    <asp:HiddenField ID="psid" runat="server" />
         <asp:HiddenField ID="EmailCheck" runat="server" />
        <asp:HiddenField ID="lstMailRecipient" runat="server" />
   <asp:HiddenField ID="txtSelectedRecipient" runat="server" />
         <asp:HiddenField ID="DirectDisplay" runat="server" />
    </form>
</body>
</html>
