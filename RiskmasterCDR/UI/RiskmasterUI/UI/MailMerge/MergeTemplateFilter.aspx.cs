﻿/**********************************************************************************************
*   Date     |  MITS/JIRA      | Programmer | Description                                            *
**********************************************************************************************
* 12/08/2014 | 36030/RMA-337   | nshah28    | Changes for save mail merge setting 
**********************************************************************************************/

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Xml; //Mits:36030 JIRA RMA-337 nshah28
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.MailMerge
{
    public partial class MergeTemplateFilter : System.Web.UI.Page
    {

        //Mits:36030 JIRA RMA-337 nshah28 start
        string MergeOptionsTemplate = " <Message> " +
          "<Authorization>e33d3987-4f23-4744-9b51-2c778db8120f</Authorization><Call><Function>MailMergeAdaptor.GetMailMergeOptions</Function>" +
          "</Call><Document><Template><TableName>USER_PREF_XML</TableName><Attach>0</Attach><RecordSelect>0</RecordSelect><AllStates>1</AllStates><MergeDocumentType></MergeDocumentType><MergeDocumentFormat></MergeDocumentFormat>" +
          "<Jurisdiction></Jurisdiction><SaveMySettings>0</SaveMySettings><Lettername>0</Lettername></Template></Document></Message>";
        //Mits:36030 JIRA RMA-337 nshah28 end

        private void SaveValuesFromPreviousScreen()
        {
            FormName.Value = AppHelper.GetValue("FormName");
            TableName.Value = AppHelper.GetValue("TableName");
            RecordId.Value = AppHelper.GetValue("RecordId");
            psid.Value = AppHelper.GetValue("psid");
            Notes.Value = AppHelper.GetFormValue("Notes");
            Comments.Value = AppHelper.GetFormValue("Comments");
            Keywords.Value = AppHelper.GetFormValue("Keywords");
            Subject.Value = AppHelper.GetFormValue("Subject");
            DocTitle.Value = AppHelper.GetFormValue("DocTitle");
            Category.Value = AppHelper.GetFormValue("Category");
            Class.Value = AppHelper.GetFormValue("Class");
            Type.Value = AppHelper.GetFormValue("Type");
            lettername.Value = AppHelper.GetValue("lettername");
            CatId.Value = AppHelper.GetValue("CatId");
            RecordId.Value = AppHelper.GetValue("RecordId");
            recordselect.Value = AppHelper.GetFormValue("recordselect");
            attach.Value = AppHelper.GetFormValue("attach");
            txtState.Value = AppHelper.GetFormValue("txtState");
            txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
            if (AppHelper.GetFormValue("txtState_cid") != "")
            {
                allstatesselected.Checked = false;
                allstatesselected.Value = "";
            }
            else
            {
                allstatesselected.Value = "true";
                allstatesselected.Checked = true;
            }

            //Mits:36030 JIRA RMA-337 nshah28 start
            if (AppHelper.GetFormValue("savemysettings") == "1")
                savemysettings.Checked = true;
            else
                savemysettings.Checked = false;
            //Mits:36030 JIRA RMA-337 nshah28 end

            txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
            txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
            txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
            txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
            //spahariya MITS 28867
            EmailCheck.Value = AppHelper.GetValue("EmailCheck");
            lstMailRecipient.Value = AppHelper.GetValue("txtSelectedRecipient");
            txtSelectedRecipient.Value = AppHelper.GetValue("txtSelectedRecipient");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SaveValuesFromPreviousScreen();
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeTemplateFilter.aspx"), "MergeTemplateFilterValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeTemplateFilterValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end

                //Mits:36030 JIRA RMA-337 nshah28 start
                if (!IsPostBack)
                {
                    //Get the data from user pref XML
                    string sReturn1 = AppHelper.CallCWSService(MergeOptionsTemplate.ToString());
                    XmlDocument ModelOptions = new XmlDocument();
                    ModelOptions.LoadXml(sReturn1);
                    PopulateFormValues(ModelOptions);
                }
                //Mits:36030 JIRA RMA-337 nshah28 end
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            DataBind();
        }

        //Mits:36030 JIRA RMA-337 nshah28 start
        /// <summary>
        /// Show mail merge preferences to user
        /// </summary>
        /// <param name="doc"></param>
        private void PopulateFormValues(XmlDocument doc)
        {
            XmlNode xmlNode = null;
            string pStateWithId = "";
            string pMergedocumenttypeWithId = "";
            string pMergedocumentformatWithId = "";

            xmlNode = doc.SelectSingleNode("//AllStates/@selected");
            if (xmlNode.InnerText == "0")
                allstatesselected.Checked = false;
            else
                allstatesselected.Checked = true;

            xmlNode = doc.SelectSingleNode("//SaveMySettings/@selected");
            if (xmlNode.InnerText == "1")
                savemysettings.Checked = true;
            else
                savemysettings.Checked = false;

            xmlNode = doc.SelectSingleNode("//Jurisdiction/@value");
            pStateWithId = xmlNode.InnerText;
            if (pStateWithId != "")
            {
                int firstPositionOfSpace = pStateWithId.IndexOf('#');

                if (firstPositionOfSpace > 0)
                {
                    txtState_cid.Value = pStateWithId.Substring(0, firstPositionOfSpace);
                    txtState.Value = pStateWithId.Substring(firstPositionOfSpace + 1);
                }
            }

            xmlNode = doc.SelectSingleNode("//MergeDocumentType/@value");
            pMergedocumenttypeWithId = xmlNode.InnerText;
            if (pMergedocumenttypeWithId != "")
            {
                int firstPositionOfSpace = pMergedocumenttypeWithId.IndexOf('#');
                if (firstPositionOfSpace > 0)
                {
                    txtmergedocumenttype_cid.Value = pMergedocumenttypeWithId.Substring(0, firstPositionOfSpace);
                    txtmergedocumenttype.Value = pMergedocumenttypeWithId.Substring(firstPositionOfSpace + 1);

                }
            }

            xmlNode = doc.SelectSingleNode("//MergeDocumentFormat/@value");
            pMergedocumentformatWithId = xmlNode.InnerText;
            if (pMergedocumentformatWithId != "")
            {
                int firstPositionOfSpace = pMergedocumentformatWithId.IndexOf('#');
                if (firstPositionOfSpace > 0)
                {
                    txtmergedocumentformat_cid.Value = pMergedocumentformatWithId.Substring(0, firstPositionOfSpace);
                    txtmergedocumentformat.Value = pMergedocumentformatWithId.Substring(firstPositionOfSpace + 1);
                }
            }


        }

        //Mits:36030 JIRA RMA-337 nshah28 end

        protected void btnNext_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplate1.aspx");
        }
    }
}
