﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.MailMerge
{
    public partial class MergeTemplate2 : System.Web.UI.Page
    {
        string MergeMessageTemplate = "<Message><Authorization>f297eea3-b30a-483f-a768-31351c7b93b4</Authorization><Call>"+
            "<Function>MailMergeAdaptor.GetDocumentTypesClassesCategories</Function></Call><Document><Template><TableName />"+
            "<TemplateId></TemplateId><CatId></CatId></Template></Document></Message>";
        private void PopulateInformation(XmlDocument doc)
        {
            ListItem item = new ListItem();
            //item.Text = "------------------------Please select a Type--------------------------"; //Amandeep ML Enh
            //item.Text = JavaScriptResourceHandler.ProcessSpecificKeyRequest(this.Context, RMXResourceProvider.PageId("MergeTemplate2.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString(), "SelectTypeResrc");
            item.Text = RMXResourceProvider.GetSpecificObject("SelectTypeResrc", RMXResourceProvider.PageId("MergeTemplate2.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            item.Value = "0";
            Type.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//Type"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["description"].Value;
                item.Value = ele.Attributes["codeid"].Value;
                Type.Items.Add(item);
            }
          
            item = new ListItem();
            //item.Text = "------------------------Please select a Class--------------------------"; //Amandeep ML Enh
            //item.Text = JavaScriptResourceHandler.ProcessSpecificKeyRequest(this.Context, RMXResourceProvider.PageId("MergeTemplate2.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString(), "SelectClassResrc");
            item.Text = RMXResourceProvider.GetSpecificObject("SelectClassResrc", RMXResourceProvider.PageId("MergeTemplate2.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            item.Value = "0";
            Class.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//Class"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["description"].Value;
                item.Value = ele.Attributes["codeid"].Value;
                Class.Items.Add(item);
            }

            item = new ListItem();
            //item.Text = "------------------------Please select a Category--------------------------"; //Amandeep ML Enh
            //item.Text = JavaScriptResourceHandler.ProcessSpecificKeyRequest(this.Context, RMXResourceProvider.PageId("MergeTemplate2.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString(), "SelectCategoryResrc");
            item.Text = RMXResourceProvider.GetSpecificObject("SelectCategoryResrc", RMXResourceProvider.PageId("MergeTemplate2.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            item.Value = "0";
            Category.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//Category"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["description"].Value;
                item.Value = ele.Attributes["codeid"].Value;
                Category.Items.Add(item);
            }
            if (!Page.IsPostBack)
            {
                Category.Value = AppHelper.GetFormValue("Category");
                Class.Value = AppHelper.GetFormValue("Class");
                Type.Value = AppHelper.GetFormValue("Type");
            }
        }
        private void SaveValuesFromPreviousScreen()
        {
          
                FormName.Value = AppHelper.GetValue("FormName");
                TableName.Value = AppHelper.GetValue("TableName");
                lettername.Value = AppHelper.GetValue("lettername");
                CatId.Value = AppHelper.GetValue("CatId");
                RecordId.Value = AppHelper.GetValue("RecordId");
                txtState.Value = AppHelper.GetFormValue("txtState");
                recordselect.Value = AppHelper.GetFormValue("recordselect");
                attach.Value = AppHelper.GetFormValue("attach");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
                allstatesselected.Value = AppHelper.GetFormValue("allstatesselected");
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
                Notes.Value = AppHelper.GetFormValue("Notes");
                Comments.Value = AppHelper.GetFormValue("Comments");
                Keywords.Value = AppHelper.GetFormValue("Keywords");
                Subject.Value = AppHelper.GetFormValue("Subject");
                DocTitle.Value = AppHelper.GetFormValue("DocTitle");
                psid.Value = AppHelper.GetValue("psid");
                //spahariya MITS 28867
                if (string.IsNullOrEmpty(EmailCheck.Value))
                    EmailCheck.Value = AppHelper.GetQueryStringValue("EmailCheck");
                else
                    EmailCheck.Value = AppHelper.GetValue("EmailCheck");
                if (string.IsNullOrEmpty(lstMailRecipient.Value))
                    lstMailRecipient.Value = AppHelper.GetValue("txtSelectedRecipient");
                else
                    lstMailRecipient.Value = AppHelper.GetValue("lstMailRecipient");
                txtSelectedRecipient.Value = AppHelper.GetValue("txtSelectedRecipient");
           

        }
        private string PrepareServiceMessage(string MergeMessageTemplate)
        {
            string TemplateId = lettername.Value;
            string[] sep = { "***" };
            string[] arr = TemplateId.Split(sep, StringSplitOptions.None);
            if (arr[0].Length >= 2)
            {
                CatId.Value = arr[2];
                TemplateId = arr[1];
                if (arr[0].Length > 1)
                    LetterTitle.Text = arr[0];
            }
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//TableName", TableName.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//TemplateId", TemplateId);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//CatId", CatId.Value);
            return MergeMessageTemplate;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sSubmitedFrmRsv = AppHelper.GetQueryStringValue("page");
                if (!string.IsNullOrEmpty(sSubmitedFrmRsv))
                {
                    if (sSubmitedFrmRsv == "MergeEditResult")
                    {
                        Server.Transfer("MergeEditResult.aspx?EmailCheck=" + AppHelper.GetQueryStringValue("EmailCheck"));
                    }
                    else if (sSubmitedFrmRsv == "FetchData")
                    {
                        Server.Transfer("FetchData.aspx?EmailCheck=" + AppHelper.GetQueryStringValue("EmailCheck"));
                    }
                }
                //Amandeep MultiLingual Changesc--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeTemplate2.aspx"), "MergeTemplate2Validations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeTemplate2ValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changesc--end

                if (AppHelper.GetValue("opentype") == "samewindow" || (Page.PreviousPage != null && (Page.PreviousPage.ToString() == "ASP.ui_mailmerge_fetchdata_aspx" || Page.PreviousPage.ToString() == "ASP.ui_mailmerge_template1_aspx")))
                {
                    SaveValuesFromPreviousScreen();
                    GetData();
                }
                else if (AppHelper.GetValue("opentype") == "" && (Page.IsPostBack))
                {
                    SaveValuesFromPreviousScreen();
                    GetData();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            DataBind();

        }
        private void GetData()
        {
         
                MergeMessageTemplate = PrepareServiceMessage(MergeMessageTemplate);
                string sReturn = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                XmlDocument Model = new XmlDocument();
                Model.LoadXml(sReturn);
                PopulateInformation(Model);
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
         
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplate1.aspx");            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        

    }
}
