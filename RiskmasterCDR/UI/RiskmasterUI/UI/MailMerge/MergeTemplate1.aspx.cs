﻿/****************************************************************************************************
*   Date     |  MITS/JIRA      | Programmer | Description                                            *
****************************************************************************************************
* 11/08/2014 | 36030/RMA-337   | nshah28    | Ability to save mail merge settings in user preference
****************************************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.MailMerge
{
    public partial class MergeTemplate1 : System.Web.UI.Page
    {
        string MergeMessageTemplate = "<Message> " +
           "<Authorization>e33d3987-4f23-4744-9b51-2c778db8120f</Authorization><Call><Function>MailMergeAdaptor.GetAvailTemplates</Function>" +
           "</Call><Document><Template><TableName>claim</TableName><AllStatesSelected>true</AllStatesSelected><MergeDocumentType /><MergeDocumentFormat />" +
           "<State /><FormName>claimgc</FormName><RecordId>1</RecordId></Template></Document></Message>";

        //Mits 23683 05/13/2011 Neha saved mail merge option in user preferences.
        /* string MergeOptionsTemplate = " <Message> " +
            "<Authorization>e33d3987-4f23-4744-9b51-2c778db8120f</Authorization><Call><Function>MailMergeAdaptor.GetMailMergeOptions</Function>" +
            "</Call><Document><Template><TableName>USER_PREF_XML</TableName><Attach>0</Attach><RecordSelect>0</RecordSelect></Template></Document></Message>";*/

        //Mits:36030 nshah28 JIRA RMA-337 start
        string MergeOptionsTemplate = " <Message> " +
           "<Authorization>e33d3987-4f23-4744-9b51-2c778db8120f</Authorization><Call><Function>MailMergeAdaptor.GetMailMergeOptions</Function>" +
           "</Call><Document><Template><TableName>USER_PREF_XML</TableName><Attach>0</Attach><RecordSelect>0</RecordSelect><AllStates>1</AllStates><MergeDocumentType></MergeDocumentType><MergeDocumentFormat></MergeDocumentFormat>" +
           "<Jurisdiction></Jurisdiction><SaveMySettings>0</SaveMySettings><Lettername>0</Lettername></Template></Document></Message>";

        //Mits:36030 nshah28 JIRA RMA-337 end


        private void SaveValuesFromPreviousScreen()
        {
            //Amandeep MultiLingual Changesc--start
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeTemplate1.aspx"), "MergeTemplate1Validations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "MergeTemplate1ValidationsScripts", sValidationResources, true);
            //Amandeep MultiLingual Changesc--end
            if (!Page.IsPostBack)
            {
                Notes.Value = AppHelper.GetFormValue("Notes");
                Comments.Value = AppHelper.GetFormValue("Comments");
                Keywords.Value = AppHelper.GetFormValue("Keywords");
                Subject.Value = AppHelper.GetFormValue("Subject");
                DocTitle.Value = AppHelper.GetFormValue("DocTitle");
                Category.Value = AppHelper.GetFormValue("Category");
                Class.Value = AppHelper.GetFormValue("Class");
                Type.Value = AppHelper.GetFormValue("Type");
                FormName.Value = AppHelper.GetValue("FormName");
                TableName.Value = AppHelper.GetValue("TableName");
                RecordId.Value = AppHelper.GetValue("RecordId");
                txtState.Value = AppHelper.GetFormValue("txtState");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
                allstatesselected.Value = AppHelper.GetFormValue("allstatesselected");
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");

                //Mits:36030 nshah28 JIRA RMA-337 start
                if (allstatesselected.Value == "")
                {
                    allstatesselected.Value = "0";
                }
                else
                {
                    allstatesselected.Value = "1";
                }

                savemysettingsselected.Value = AppHelper.GetFormValue("savemysettings");
                if (savemysettingsselected.Value == "Checked")
                {
                    savemysettingsselected.Value = "1";
                }
                else
                {
                    savemysettingsselected.Value = "0";
                }
                //Mits:36030 nshah28 JIRA RMA-337 end

                psid.Value = AppHelper.GetValue("ipsid");
                //spahariya 28867                
                if (AppHelper.GetFormValue("EmailCheck") == "true")
                {
                    EmailCheck.Checked = true;
                }
                else
                {
                    EmailCheck.Checked = false;
                }
                txtSelectedRecipient.Value = AppHelper.GetValue("txtSelectedRecipient");
                //spahariya - end
                //added by swati MITS # 36930  
                if (!string.IsNullOrEmpty(txtSelectedRecipient.Value))
                {
                    ListItem item = new ListItem();
                    String[] sValue = txtSelectedRecipient.Value.ToString().Split('|');
                    foreach (string s in sValue)
                    {
                        string[] sText = s.Split(',');
                        item = new ListItem();
                        item.Text = sText[0];
                        item.Value = sText[1];
                        lstMailRecipient.Items.Add(item);
                    }
                }
                //change end here by swati               
                if (AppHelper.GetFormValue("attach") == "Checked")
                    attach.Checked = true;
                if (AppHelper.GetFormValue("recordselect") == "Checked")
                    recordselect.Checked = true;
            }


        }
        private string PrepareServiceMessage(string MergeMessageTemplate)
        {
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//TableName", TableName.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//FormName", FormName.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//RecordId", RecordId.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//AllStatesSelected", allstatesselected.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//MergeDocumentType", txtmergedocumenttype_cid.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//MergeDocumentFormat", txtmergedocumentformat_cid.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//State", txtState_cid.Value);
            return MergeMessageTemplate;
        }

        /// <summary>
        /// //Mits 23683 05/13/2011 Neha saved mail merge option in user preferences.
        /// This function edits the mail merge options template.
        /// </summary>
        /// <param name="MergeOptionsTemplate"></param>
        /// <returns></returns>
        private string PrepareOptionsForSave(string MergeOptionsTemplate)
        {
            string sRecordSelect = "0";
            string sAttach = "0";
            if (recordselect.Checked)
                sRecordSelect = "1";
            if (attach.Checked)
                sAttach = "1";
            MergeOptionsTemplate = AppHelper.ChangeMessageValue(MergeOptionsTemplate, "//TableName", TableName.Value);
            MergeOptionsTemplate = AppHelper.ChangeMessageValue(MergeOptionsTemplate, "//Function", "MailMergeAdaptor.SaveMailMergeOptions");


            //Mits:36030 nshah28 JIRA RMA-337 start
            if (savemysettingsselected.Value == "1")
            {
                //Using this 2 below lines in if condition, as we need to save it only when "savemysettingsselected" is checked
                MergeOptionsTemplate = AppHelper.ChangeMessageValue(MergeOptionsTemplate, "//RecordSelect", sRecordSelect);
                MergeOptionsTemplate = AppHelper.ChangeMessageValue(MergeOptionsTemplate, "//Attach", sAttach);

                MergeOptionsTemplate = AppHelper.ChangeMessageValue(MergeOptionsTemplate, "//AllStates", allstatesselected.Value);
                MergeOptionsTemplate = AppHelper.ChangeMessageValue(MergeOptionsTemplate, "//MergeDocumentType", txtmergedocumenttype_cid.Value + "#" + txtmergedocumenttype.Value);
                MergeOptionsTemplate = AppHelper.ChangeMessageValue(MergeOptionsTemplate, "//MergeDocumentFormat", txtmergedocumentformat_cid.Value + "#" + txtmergedocumentformat.Value);
                MergeOptionsTemplate = AppHelper.ChangeMessageValue(MergeOptionsTemplate, "//Jurisdiction", txtState_cid.Value + "#" + txtState.Value);
                MergeOptionsTemplate = AppHelper.ChangeMessageValue(MergeOptionsTemplate, "//SaveMySettings", savemysettingsselected.Value);
                MergeOptionsTemplate = AppHelper.ChangeMessageValue(MergeOptionsTemplate, "//Lettername", Convert.ToString(lettername.SelectedValue));
            }
            //Mits:36030 nshah28 JIRA RMA-337 end

            return MergeOptionsTemplate;
        }
        private void PopulateLetterName(XmlDocument doc)
        {
            ListItem item = new ListItem();
            //item.Text = "--------------Please select a Form--------------"; //Amandeep ML Enh
            //item.Text = JavaScriptResourceHandler.ProcessSpecificKeyRequest(this.Context, RMXResourceProvider.PageId("MergeTemplate1.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString(), "SelectFormResrc");
            item.Text = RMXResourceProvider.GetSpecificObject("SelectFormResrc", RMXResourceProvider.PageId("MergeTemplate1.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            item.Value = "0";
            lettername.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//Template"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["TemplateDesc"].Value;
                //spahariya MITS 28867
                item.Value = ele.Attributes["TemplateDesc"].Value + "***" + ele.Attributes["TemplateId"].Value + "***" + ele.Attributes["Category"].Value + "***" + ele.Attributes["SendMailFlag"].Value + "***" + ele.Attributes["MailRecipient"].Value;
                lettername.Items.Add(item);
                if (!Page.IsPostBack)
                {
                    if (AppHelper.GetFormValue("lettername") != "")
                    {
                        if (item.Value == AppHelper.GetFormValue("lettername"))
                        {
                            item.Selected = true;
                        }
                    }
                }
            }

        }
        //spahariya : MITS 28867 on 06/26/2012 Start
        private void PopulateMailRecipient(XmlDocument doc)
        {
            ListItem item = new ListItem();
            item.Text = "";
            item.Value = "";
            lstMailRecipient.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//GetMailRecipient/Recipients/Recipient"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["codedesc"].Value;
                item.Value = ele.Attributes["codeid"].Value;
                lstMailRecipient.Items.Add(item);
                if (!Page.IsPostBack)
                {
                    if (AppHelper.GetFormValue("lstMailRecipient") != "")
                    {
                        if (item.Value == AppHelper.GetFormValue("lstMailRecipient"))
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// //Mits 23683 05/13/2011 Neha: saved mail merge option in user preferences.
        /// </summary>
        /// <param name="doc"></param>
        private void PopulateFormValues(XmlDocument doc)
        {
            XmlNode xmlNode = null;
            xmlNode = doc.SelectSingleNode("//recordselect/@selected");
            if (xmlNode.InnerText == "1")
                recordselect.Checked = true;
            else
                recordselect.Checked = false;
            xmlNode = doc.SelectSingleNode("//attach/@selected");
            if (xmlNode.InnerText == "1")
                attach.Checked = true;
            else
                attach.Checked = false;

            //Mits:36030 nshah28 JIRA RMA-337 start
            xmlNode = doc.SelectSingleNode("//Lettername/@value");
            //Check whether item exist in lettername dropdown, then load user preference
            if (lettername.Items.FindByValue(xmlNode.InnerText) != null)
            {
                lettername.SelectedValue = xmlNode.InnerText;
            }

            //JIRA RMA-1248 start
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Change1", "<script>lettername_IndexChanged();</script>", false);
            //JIRA RMA-1248 end

            //Mits:36030 nshah28 JIRA RMA-337 end
            
        }


        private void RedirectonNoTemplates(XmlDocument Model)
        {
            if (Model.SelectSingleNode("//@TemplateDesc") == null || Model.SelectSingleNode("//@TemplateDesc").InnerText == "")
            {
                Server.Transfer("MailMergeIssue.aspx");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sSubmitedFrmRsv = AppHelper.GetQueryStringValue("page");
                if (!string.IsNullOrEmpty(sSubmitedFrmRsv))
                {
                    if (sSubmitedFrmRsv == "MergeTemplate2samewindow")
                    {
                        Server.Transfer("MergeTemplate2.aspx?opentype=samewindow&EmailCheck=" + AppHelper.GetQueryStringValue("EmailCheck"));
                    }
                    else if (sSubmitedFrmRsv == "FetchData")
                    {
                        Server.Transfer("FetchData.aspx?EmailCheck=" + AppHelper.GetQueryStringValue("EmailCheck"));
                    }
                }
                SaveValuesFromPreviousScreen();
                if (!Page.IsPostBack)
                {
                    MergeMessageTemplate = PrepareServiceMessage(MergeMessageTemplate);
                    string sReturn = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                    XmlDocument Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    RedirectonNoTemplates(Model);
                    PopulateLetterName(Model);
                    //commneted by swati MITS # 36930
                    //PopulateMailRecipient(Model);
                    //Mona:PaperVisionMerge : Animesh Inserted For MITS 16697
                    if (Model.SelectSingleNode("//PaperVisionActive") != null)
                    {
                        PaperVisionActive.Value = Model.SelectSingleNode("//PaperVisionActive").InnerText.Trim();
                        PaperVisionValidation.Value = Model.SelectSingleNode("//PaperVisionValidation").InnerText.Trim();
                    }
                    //Animesh Insertion Ends
                    ////Mits 23683 05/13/2011 Neha: save mail merge option in user preferences.
                    string sReturn1 = AppHelper.CallCWSService(MergeOptionsTemplate.ToString());
                    XmlDocument ModelOptions = new XmlDocument();
                    ModelOptions.LoadXml(sReturn1);
                    PopulateFormValues(ModelOptions);
                    //Neha end
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            DataBind();
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {//Mits 23683 Neha 
            //commented by swati MITS # 36930
            //lstMailRecipient.SelectedValue = txtSelectedRecipient.Value;
            if (lstMailRecipient.Rows == 0)
            {
                ListItem item = new ListItem();
                String[] sValue = txtSelectedRecipient.Value.ToString().Split('|');
                foreach (string s in sValue)
                {
                    string[] sText = s.Split(',');
                    item = new ListItem();
                    item.Text = sText[0];
                    item.Value = sText[1];
                    lstMailRecipient.Items.Add(item);
                }
                lstMailRecipient.Enabled = false;
            }
            //change end here by swati
            MergeOptionsTemplate = PrepareOptionsForSave(MergeOptionsTemplate);
            string sReturn1 = AppHelper.CallCWSService(MergeOptionsTemplate.ToString());
            //Mits 23683 removed javascript from button click added from the server side;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "k", "<script>ValidateMergeLetter1();</script>", false);

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplateFilter.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }


    }
}
