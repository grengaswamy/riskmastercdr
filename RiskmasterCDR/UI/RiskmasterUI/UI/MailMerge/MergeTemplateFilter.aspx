﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeTemplateFilter.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeTemplateFilter"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>


<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Filter Merge Letter Page</title>
       <uc4:CommonTasks ID="CommonTasks1" runat="server" />
        <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/merge.js")%>'></script>
        <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/OpenRTF.js")%>'></script>
        <script type="text/javascript">
            //MITS 27964 hlv 4/10/2012 begin
            if (window.opener != null) {
                window.opener.window.parent.parent.iwintype = document.title;
            }

            window.onunload = function () {
                if (window.opener != null) {
                    window.opener.window.parent.parent.iwintype = "";
                }
            }
            //MITS 27964 hlv 4/10/2012 end
        </script>
</head>
<body onload="mailmergedefaults();">
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
    <table width="100%">
    <tr>
     <td colspan="3" class="formtitle">
      								<asp:Label ID="lblFilterMergeLetter" runat="server" Text="<%$ Resources:lblFilterMergeLetterResrc %>"></asp:Label>
      							
     </td>
    </tr>
    <tr>
     <td>
      								&nbsp;
      							
     </td>
    </tr>
    <tr>
     <td>
      								<asp:Label ID="lblCreateDoc" runat="server" Text="<%$ Resources:lblCreateDocResrc %>"></asp:Label>
      							
     </td>
    </tr>
    <tr>
     <td>
      								&nbsp;
      							
     </td>
    </tr>
   </table>
   <table colspan="3" width="100%" border="0">

         <%--//Mits:36030 JIRA RMA-337 nshah28 start--%>
        <tr>
     <td align="left"><asp:Label ID="lblSaveMySettings" runat="server" Text="<%$ Resources:lblSaveMySettings %>"></asp:Label><input type="checkbox"  runat="server" align="center" value="Checked" id="savemysettings" onclick=""></td>
    </tr>
       <%--//Mits:36030 JIRA RMA-337 nshah28 End--%>

    <tr>
     <td>
      								<asp:Label ID="lblJurisOfForm" runat="server" Text="<%$ Resources:lblJurisOfFormResrc %>"></asp:Label>
      							
     </td>
     <td align="left"><input type="checkbox"  runat="server" align="center" value="Checked" id="allstatesselected" onclick="alterstateselection();" checked><asp:Label ID="lblAllStates" runat="server" Text="<%$ Resources:lblAllStatesResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td></td>
     <td><input type="text" disabled size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);" name="txtState" id="txtState" cancelledvalue=""></input>
<input type="button" name="txtStatebtn"  disabled class="CodeLookupControl"  id="txtStatebtn" onclick="return CodeSelect('states','txtState')" />

<input type="hidden" name="txtState_cid"  runat="server"  id="txtState_cid"  /></td>
    </tr>
    <tr>
     <td><asp:Label ID="lblTypeMergeDoc" runat="server" Text="<%$ Resources:lblTypeMergeDocResrc %>"></asp:Label></td>
     <td><input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);" name="txtmergedocumenttype" id="txtmergedocumenttype" cancelledvalue=""></input>
<input type="button" name="txtmergedocumenttypebtn"  class="CodeLookupControl"  id="txtmergedocumenttypebtn" onclick="return CodeSelect('MERGE_DOC_TYPE','txtmergedocumenttype')" />

<input type="hidden" name="txtmergedocumenttype_cid"  runat="server"  id="txtmergedocumenttype_cid"  /></td>
    </tr>
    <tr>
     <td><asp:Label ID="lblFormatMergeDoc" runat="server" Text="<%$ Resources:lblFormatMergeDocResrc %>"></asp:Label></td>
     <td><input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);" name="txtmergedocumentformat" id="txtmergedocumentformat" cancelledvalue=""></input>
<input type="button" name="txtmergedocumentformatbtn"  class="CodeLookupControl"  id="txtmergedocumentformatbtn" onclick="return CodeSelect('MERGE_FORMAT_TYPE','txtmergedocumentformat')" />

<input type="hidden" name="txtmergedocumentformat_cid"  runat="server"  id="txtmergedocumentformat_cid"  /></td>
    </tr>
    <tr class="ctrlgroup">
     <td colspan="8">&nbsp;</td>
    </tr>
    <tr>
     <td colspan="2">  <asp:Button ID="Button2" onclick="btnNext_Click" UseSubmitBehavior="true"   runat="server" Text="<%$ Resources:btnNextResrc %>"   CssClass=button OnClientClick="return validate_filter();"/> 
   <input value="<%$ Resources:btnCancelResrc %>" runat="server" type="button" id="btnCancel"  class="button" onClick="window.close();" />
   
   
   
   </td>
   </tr>
    <tr>
    <td>
    
    </td>
        <asp:HiddenField ID="TableName" runat="server" />
        <asp:HiddenField ID="FormName" runat="server" />
        <asp:HiddenField ID="RecordId" runat="server" />
        <asp:HiddenField ID="psid" runat="server" />
 
 
            <asp:HiddenField ID="DocTitle" runat="server" />
        <asp:HiddenField ID="Subject" runat="server" />   
        <asp:HiddenField ID="Keywords" runat="server" />   
        <asp:HiddenField ID="Notes" runat="server" />   
        <asp:HiddenField ID="CatId" runat="server" />   
          <asp:HiddenField ID="Class" runat="server" />   
        <asp:HiddenField ID="Category" runat="server" />   
        <asp:HiddenField ID="Type" runat="server" />   
        <asp:HiddenField ID="Comments" runat="server" />  
           <asp:HiddenField ID="attach" runat="server" />
      
        
           <asp:HiddenField ID="recordselect" runat="server" />
           <asp:HiddenField ID="lettername" runat="server" /> 
           <asp:HiddenField ID="EmailCheck" runat="server" />
        <asp:HiddenField ID="lstMailRecipient" runat="server" />
        <asp:HiddenField ID="txtSelectedRecipient" runat="server" />
    </tr>
   </table>
    </form>
</body>
</html>
