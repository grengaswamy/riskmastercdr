﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OpenMainMergeSaved.aspx.cs" Inherits="Riskmaster.UI.MailMerge.OpenMainMergeSaved" %>
<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Riskmaster</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
        <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/merge.js")%>'></script>
           <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/OpenRTF.js")%>'></script>
</head>
<body>
   <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
    <br />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
     <br />
     <br />
    <center><input type="button" runat="server" value="<%$ Resources:lblDoneDesc %>" class="button" id="btnDone" onclick="window.opener.close();window.close();"></center>

    <asp:HiddenField ID="TableName" runat="server" />
        <asp:HiddenField ID="RetMessage" runat="server" />
    </form>
</body>
</html>
