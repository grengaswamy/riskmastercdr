﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.MailMerge
{
    public partial class OpenMainMergeSaved : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("OpenMainMergeSaved.aspx"), "OpenMainMergeSavedValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "OpenMainMergeSavedValidationsScripts", sValidationResources, true);

            TableName.Value = AppHelper.GetValue("TableName");
            string sMessage = string.Empty;
            if (TableName.Value == "claim" || TableName.Value == "event")
            {
                sMessage = AppHelper.GetValue("RetMessage");
                if (sMessage.Contains('|'))
                {
                    sMessage = sMessage.Substring(sMessage.LastIndexOf('|') + 1);
                }
                lblMessage.Text = sMessage;
            }
            DataBind();
        }
    }
}