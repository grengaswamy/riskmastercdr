﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Collections;
using Riskmaster.RMXResourceManager; //MITS 31027


namespace Riskmaster.UI.VoidChecks
{
    public partial class VoidClearChecks : System.Web.UI.Page
    {
        string MergeMessageTemplate = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.LoadChecks4Void</Function></Call><Document><FundsCheck><FromDate>oneweekbefore</FromDate><ToDate>today</ToDate><UseCollections>false</UseCollections><NonPrintedOnly>false</NonPrintedOnly><CompanyEID>0</CompanyEID><NotAttached>0</NotAttached><AccountID>-1</AccountID></FundsCheck></Document></Message>";
        string MergeMessageTemplateForVoidORClear = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.LoadChecks4Void</Function></Call><Document><FundsCheck><TransIds/><VoidReason/><VoidReason_HTMLComments/></FundsCheck></Document></Message>";
        int iEnableVoidReason = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument Model = null;
            try
            {
                //Praveen ML Changes :start
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);

                    

                }
                //Praveen ML Changes :End

                // MITS 31027 - Rakhel MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("VoidClearChecks.aspx"), "NoChecksValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "NoChecksValidations", sValidationResources, true);
                //MITS 31027 - Rakhel MultiLingual Changes--end

                if (!IsPostBack)
                {
                    SetVoidORClear();
                    ResetClearedCheck();//To check for cleared-check ---> To printed Status
                    Model = new XmlDocument();
                    MergeMessageTemplate = DecideInitialServiceToCall();
                    string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                    Model.LoadXml(sCWSresponse);
                    BindInitialData(Model);
                    BindpageControls(sCWSresponse);
                   
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private string DecideInitialServiceToCall()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeMessageTemplate);
            if (hdnResetCheck.Text == "true")
            {
                doc.SelectSingleNode("//Function").InnerText = "FundManagementAdaptor.LoadChecks4MarkUnCleared";
            }
            else
            {
                if (hdnMarkChecksCleared.Text == "true")
                {
                    doc.SelectSingleNode("//Function").InnerText = "FundManagementAdaptor.LoadChecks4MarkCleared";
                }
                else
                {
                    doc.SelectSingleNode("//Function").InnerText = "FundManagementAdaptor.LoadChecks4Void";
                }
            }
      
           
            return doc.OuterXml;
        }
        private string DecideProcessServiceToCall()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeMessageTemplateForVoidORClear);

            if (hdnResetCheck.Text == "true")
            {
                doc.SelectSingleNode("//Function").InnerText = "FundManagementAdaptor.MarkChecksUnCleared";
            }
            else
            {
                if (hdnMarkChecksCleared.Text == "true")
                {
                    doc.SelectSingleNode("//Function").InnerText = "FundManagementAdaptor.MarkChecksCleared";
                }
                else
                {
                    doc.SelectSingleNode("//Function").InnerText = "FundManagementAdaptor.VoidChecks";
                }
            }


            return doc.OuterXml;
        }
        private void SetVoidORClear()
        {
            hdnMarkChecksCleared.Text = AppHelper.GetQueryStringValue("MarkChecksCleared");
        }
        private void BindInitialData(XmlDocument doc)
        {
            //Praveen ML Changes --Start
                lblScreen1.Attributes.Add("style", "display: none");
                lblScreen2.Attributes.Add("style", "display: none");
                lblScreen3.Attributes.Add("style", "display: none");
                btnProcess1.Attributes.Add("style", "display: none");
                btnProcess2.Attributes.Add("style", "display: none");
                btnProcess3.Attributes.Add("style", "display: none");
            //Praveen ML Changes ---End
            bool IsSubBankAccountEnabled = false;
            foreach (XmlNode node in doc.SelectNodes("//Company"))
            {
                ListItem item = new ListItem(node.InnerText, node.Attributes["CompanyEid"].Value);
                selCompany.Items.Add(item);
            }

            //Ankit Start : Financial Enhancements - Void Code Reason change
            foreach (XmlNode node in doc.SelectNodes("//VoidReasonCode"))
            {
                ListItem item = new ListItem(node.InnerText, node.Attributes["CodeID"].Value);
                cboVoidReasonCode.Items.Add(item);
            }
            //Ankit End

            //ML changes
          //  txtFromDate.Value = doc.SelectSingleNode("//FundsCheck").Attributes["FromDate"].Value;
          //  txtToDate.Value = doc.SelectSingleNode("//FundsCheck").Attributes["ToDate"].Value;
            txtFromDate.Value = AppHelper.GetDate(doc.SelectSingleNode("//FundsCheck").Attributes["FromDate"].Value);
            txtToDate.Value = AppHelper.GetDate(doc.SelectSingleNode("//FundsCheck").Attributes["ToDate"].Value);
            //ML changes

            if (hdnResetCheck.Text == "true")
            {                
                //Praveen ML Changes --Start
                    //lblScreen.Text = "Select Checks to Mark as Un-cleared";
                    //btnProcess.Text = "Mark Un-cleared";
                    lblScreen1.Attributes.Add("style", "display: inline");
                    btnProcess1.Attributes.Add("style", "display: inline");
                //Praveen ML Changes ---End

            }
            else
            {


                if (hdnMarkChecksCleared.Text == "true")
                {
                    //Praveen ML Changes --Start
                        //lblScreen.Text = "Select Checks to Mark as Cleared";
                        //btnProcess.Text = "Mark Cleared";
                        lblScreen2.Attributes.Add("style", "display: inline");
                    //MITS 30914 Raman: fixing the typo  
                    btnProcess2.Attributes.Add("style", "display: inline");
                        //btnProcess3.Attributes.Add("style", "display: inline");
                    //Praveen ML Changes --End
                }
                else
                {
                    //Praveen ML Changes --Start
                        //lblScreen.Text = "Select Checks to Void";
                        //btnProcess.Text = "Void Checks";
                        lblScreen3.Attributes.Add("style", "display: inline");
                        btnProcess3.Attributes.Add("style", "display: inline");
                    //Praveen ML Changes --End
                }
            }
            
        

            //smishra25:Start for Bank account filter
            //foreach (XmlNode node in doc.SelectNodes("//SubBankAccount"))
            //{
            //    ListItem item = new ListItem(node.InnerText, node.Attributes["SubBankAccountId"].Value);
            //    selBankAccounts.Items.Add(item);
            //    IsSubBankAccountEnabled = true;
            //}
            foreach (XmlNode node in doc.SelectNodes("//BankAccount"))
            {
                ListItem item = new ListItem(node.InnerText, node.Attributes["BankAccountId"].Value);
                selBankAccounts.Items.Add(item);
                IsSubBankAccountEnabled = false;
            }
            //if (IsSubBankAccountEnabled)
            //{
            //    lblBankAccount.Text = "Filter By Sub Bank Account:";
            //}
            //else
            //{
            //lblBankAccount.Text = "Filter By Bank Account:";  //prashbibiharis : Commented for ML changes
            //}
            //smishra25:End for Bank account filter

            //MITS 21974: Need to check null condition because this node is available only for Void checks page.
            if (doc.SelectSingleNode("//EnableVoidReason") != null)
            {
                //start rsushilaggar MITS 19970 05/21/2010
                iEnableVoidReason = Common.Conversion.ConvertStrToInteger(doc.SelectSingleNode("//EnableVoidReason").InnerText);
                //Ankit Start : Financial Enhancements - Void Code Reason change
                if (iEnableVoidReason == -1)
                {
                    voidcheckreason.Visible = true;
                    //voidreason.Visible = true;
                    //voidreasonbtnMemo.Visible = true;
                    //lbl_txtVoidReason.Visible = true;
                }
                else
                {
                    voidcheckreason.Visible = false;
                    //voidreason.Visible = false;
                    //voidreasonbtnMemo.Visible = false;
                    //lbl_txtVoidReason.Visible = false;
                }
                //Ankit End
                //start rsushilaggar MITS 19970 05/21/2010
            }
        }
        private string BindBackData()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeMessageTemplate);
            doc.SelectSingleNode("//ToDate").InnerText = txtToDate.Value;
            doc.SelectSingleNode("//FromDate").InnerText = txtFromDate.Value;
            doc.SelectSingleNode("//UseCollections").InnerText = txtCollections.Value;
            doc.SelectSingleNode("//NotAttached").InnerText = txtNotAttached.Value;
            doc.SelectSingleNode("//CompanyEID").InnerText = selCompany.Value;
            doc.SelectSingleNode("//AccountID").InnerText = selBankAccounts.Value;
            return doc.OuterXml;
        }
        private string BindBackDataForProcess()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeMessageTemplateForVoidORClear);
            doc.SelectSingleNode("//TransIds").InnerText = selectedtransids.Text;
            voidreason.ReadOnly = false;
            //start rsushilaggar MITS 19970 05/21/2010
            doc.SelectSingleNode("//VoidReason").InnerText = HttpContext.Current.Request.Form["voidreason"];
            //start rsushilaggar MITS 19970 05/21/2010
            doc.SelectSingleNode("//VoidReason_HTMLComments").InnerText = voidreason_HTML.Text;
            voidreason.ReadOnly = true;
            return doc.OuterXml;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="diaryDoc"></param>
        /// <returns></returns>
        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
              
                    dSet = new DataSet();
                    dSet.ReadXml(new XmlNodeReader(diaryDoc.SelectSingleNode("//Checks")));


                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
       

        private void BindpageControls(string sreturnValue)
        {
            try
            {
                DataSet usersRecordsSet = null;
                XmlDocument usersXDoc = new XmlDocument();
                usersXDoc.LoadXml(sreturnValue);
                usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);

                GridView gvSelectChecksFrame = (GridView)this.Form.FindControl("gvSelectChecksFrame");

                if (usersRecordsSet.Tables["check"] != null)
                {
                    foreach (DataRow dr in usersRecordsSet.Tables["check"].Rows)
                    {

                        double dblAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(dr["Amount"]));
                        string sAmount = string.Format("{0:C}", dblAmount);
                        dr["Amount"] = sAmount;
                        if (dr["PaymentFlag"] != null)
                        {
                            if (Common.Conversion.ConvertObjToStr(dr["PaymentFlag"]).Trim() == "0")
                            {
                                dr["TransNumber"] = "Collection";
                            }
                        }

                    } // foreach

                    gvSelectChecksFrame.DataSource = usersRecordsSet.Tables["check"].DefaultView;
                    gvSelectChecksFrame.DataBind();
                }


                else
                {
                    gvSelectChecksFrame.DataSource = new ArrayList();
                    gvSelectChecksFrame.DataBind();
                } // else
            }

            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorControl1.errorDom = "";
                MergeMessageTemplate = BindBackData();
                MergeMessageTemplate = DecideInitialServiceToCall();
                string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                BindpageControls(sCWSresponse);
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                MergeMessageTemplateForVoidORClear = DecideProcessServiceToCall();
                MergeMessageTemplateForVoidORClear = BindBackDataForProcess();
                string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplateForVoidORClear.ToString());
                MergeMessageTemplate = BindBackData();
                if (ErrorHelper.IsCWSCallSuccess(sCWSresponse))
                {
                    ErrorControl1.errorDom = "";
                    MergeMessageTemplate = DecideInitialServiceToCall();
                    sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());                    
                    BindpageControls(sCWSresponse);
                }
                else
                {
                    ErrorControl1.errorDom = sCWSresponse;
                }
            
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }
        private void ResetClearedCheck()
        {
            hdnResetCheck.Text = AppHelper.GetQueryStringValue("ResetCheck");
            //Ankit Start : Financial Enhancements - Void Code Reason change
            voidcheckreason.Visible = false;
            //voidreason.Visible = false;
            //voidreasonbtnMemo.Visible = false;
            //lbl_txtVoidReason.Visible = false;
            //Ankit End
        }
    }
}
