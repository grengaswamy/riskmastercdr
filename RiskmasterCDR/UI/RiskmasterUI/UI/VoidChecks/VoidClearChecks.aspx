﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VoidClearChecks.aspx.cs" Inherits="Riskmaster.UI.VoidChecks.VoidClearChecks" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
	TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
	TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title></title>
	
    <script type="text/javascript" language="JavaScript" src="../../Scripts/VoidChecks.js"></script>
	<script type="text/javascript" language="JavaScript" src="../../Scripts/smqueue.js"></script>
	<script type="text/javascript" language="JavaScript" src="../../Scripts/trans.js"></script>
   <script src="../../Scripts/form.js" type="text/javascript"></script>

     <!--Praveen ML-->    
     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
     <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
     <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">  { var i; }  </script>
	 <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js"> { var i; } </script>
     <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js"> { var i; } </script>
     <!--Praveen ML-->

	<uc4:CommonTasks ID="CommonTasks1" runat="server" />
	<script language="javascript" type="text/javascript">
	    //Ankit Start : Financial Enhancements - Void Code Reason change
	    function VoidCodeReasonChanged(cboVoidCodeReasonID) {
	        var objCboSelectedIndex = cboVoidCodeReasonID.options[cboVoidCodeReasonID.selectedIndex];
	        var objTaValue = document.forms[0].voidreason;
	        var objHiddenHtml = document.getElementById("voidreason_HTML");

	        if (objCboSelectedIndex == null || objTaValue == null || objHiddenHtml == null)
	            return false;

	        if (objCboSelectedIndex.value != 0) {
	            var strSpace = '';
	            if (objTaValue.value != "")
                    strSpace = ' ';

	            objTaValue.value += strSpace + objCboSelectedIndex.text;
	            objHiddenHtml.value += strSpace + objCboSelectedIndex.text;
	        }
	    }
	    //Ankit End

		function LoadCheckValues() {
			parent.MDIScreenLoaded();
			if (document.forms[0].txtCollections != null) {
				if (document.forms[0].txtCollections.value == "true")
					document.forms[0].chkCollections.checked = true;
				else
					document.forms[0].chkCollections.checked = false;
			}
			if (document.forms[0].txtNotAttached.value == "0")
				document.forms[0].chkNotAttached.checked = false;
			else
				document.forms[0].chkNotAttached.checked = true;
		}
		function OnCollCheckChange() {
			if (document.forms[0].chkCollections.checked == true)
				document.forms[0].txtCollections.value = 'true';
			else
				document.forms[0].txtCollections.value = 'false';
		}
		function OnAttCheckChange() {
			if (document.forms[0].chkNotAttached.checked == true)
				document.forms[0].txtNotAttached.value = '1';
			else
				document.forms[0].txtNotAttached.value = '0';
		}		
	</script>
   

</head>
<body onload="onPageLoaded5();LoadCheckValues();">
	<form id="frmData" runat="server">
		<div id="maindiv" style="height:100%;width:99%;overflow:auto">
	<table width="90%" border="0">
	<tr>
	 <td class="ctrlgroup2"><asp:Label ID="lblChkSearchCriteria" runat="server" Text="<%$ Resources:lblChkSearchCriteria %>"></asp:Label></td>
	</tr>
	 <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
	<tr>
	 <td width="50%">
	  <table cellspacing="3">
          
	   <tr>
		<td ><asp:Label ID="lblCheckDateFrom" runat="server" Text="<%$ Resources:lblCheckDateFrom %>"></asp:Label></td>
			<td><input type="text" runat="server"  id="txtFromDate" size="10" onblur="dateLostFocus(this.id);"/>&nbsp;
				<script >
				    $(function () {
				        $("#txtFromDate").datepicker({
				            showOn: "button",
				            buttonImage: "../../Images/calendar.gif",
				            buttonImageOnly: true,
				            showOtherMonths: true,
				            selectOtherMonths: true,
				            changeYear: true
				        });

				    });
                </script>

               	&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:Label ID="lblCheckDateTo" runat="server" Text="<%$ Resources:lblCheckDateTo %>"></asp:Label>
				<input type="text" runat="server"  id="txtToDate" size="10" onblur="dateLostFocus(this.id);"/>&nbsp;
				 <script >
				     $(function () {
				         $("#txtToDate").datepicker({
				             showOn: "button",
				             buttonImage: "../../Images/calendar.gif",
				             buttonImageOnly: true,
				             showOtherMonths: true,
				             selectOtherMonths: true,
				             changeYear: true
				         });
				         
				     });
                </script>
                
</td>
	   </tr>
	   <tr>
		<td><asp:Label ID="lblIncludeCollections" runat="server" Text="<%$ Resources:lblIncludeCollections %>"></asp:Label></td>
		<td align="left"><input type="checkbox" runat="server" onclick="OnCollCheckChange();" id="chkCollections" /><input type="text" runat="server" value="false" style="display: none" id="txtCollections" />
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<asp:Label ID="lblCheckNotAttachedToClaims" runat="server" Text="<%$ Resources:lblCheckNotAttachedToClaims %>"></asp:Label>
												<input type="checkbox" runat="server" onclick="OnAttCheckChange();" id="chkNotAttached"><input type="text" runat=server value="0" style="display: none" id="txtNotAttached"></td>
	   </tr>
	   <tr>
		<td><asp:Label ID="lblFilterByCompany" runat="server" Text="<%$ Resources:lblFilterByCompany %>"></asp:Label>:</td>
		<td><select runat="server" id="selCompany">
		  <option value=""></option>
		 </select>
	   </tr>
       <tr>
        <td><asp:Label ID="lblBankAccount" runat="server" Text="<%$ Resources:lblBankAccount %>"></asp:Label></td>
        <td><select runat="server" id="selBankAccounts">
          <option value=""></option>
         </select>
         </td>
       </tr>
	  </table>
	 
								
									<table bgcolor="white" border="0" width="100%">
	   <tr>
		<td class="ctrlgroup2" colspan="6"><asp:Label ID="lblScreen1" runat= "server" Text="<%$ Resources:lblScreen1 %>"></asp:Label><asp:Label ID="lblScreen2" runat= server Text="<%$ Resources:lblScreen2 %>" ></asp:Label><asp:Label ID="lblScreen3" runat= server Text="<%$ Resources:lblScreen3 %>" ></asp:Label></td>
	   </tr>
	   </table>
	   
		 
		<asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Height="350" Width="100%">

          <asp:GridView ID="gvSelectChecksFrame" runat="server" AutoGenerateColumns="false" 
			AllowPaging="false" AllowSorting="true" Width=100%    ShowHeader="true"  
			
			EmptyDataText="<%$ Resources:lblEmptyDataText %>" CellPadding="8">
		  
		   
			<RowStyle CssClass="" />
			<AlternatingRowStyle CssClass="rowdark2" />
			 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
			<Columns>              
						<asp:TemplateField  HeaderStyle-HorizontalAlign ="Left" HeaderStyle-CssClass="headertext2" ControlStyle-Width="5%">
						<HeaderTemplate>
						 <input type="checkbox" id='chkTransactions' onclick="TransClick();" /><asp:Label
                             ID="lblTransactionID" runat="server" Text="<%$ Resources:lblTransactionID %>"></asp:Label>
			   
			</HeaderTemplate>

				<ItemTemplate>
					<input type="checkbox"  id='chk_<%# DataBinder.Eval(Container.DataItem,"TransId")%>' name='chk_<%# DataBinder.Eval(Container.DataItem,"TransId")%>' value='' />
					<%# DataBinder.Eval(Container.DataItem, "TransNumber")%>
					
			   </ItemTemplate >


			 </asp:TemplateField> 
			 
			<asp:BoundField  HeaderText="<%$ Resources:gvHdrCheckDate %>"  DataField="CheckDate" 
					ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>
				<asp:BoundField   HeaderText="<%$ Resources:gvHdrControl %>"  DataField="CtlNumber"  ControlStyle-Width="20%"
					 ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>
				
				
				
				 <asp:TemplateField HeaderStyle-HorizontalAlign ="Left" HeaderStyle-CssClass="headertext2" HeaderText="<%$ Resources:gvHdrPayeeName %>" ControlStyle-Width="20%"  >
					  
				<ItemTemplate>
					<%# DataBinder.Eval(Container.DataItem,"LastName")%> , <%# DataBinder.Eval(Container.DataItem,"FirstName")%>
			   </ItemTemplate>


			 </asp:TemplateField> 
				
				
			  
				<asp:BoundField   HeaderText="<%$ Resources:gvHdrClaimNumber %>"  DataField="ClaimNumber"  ControlStyle-Width="10%" 
					 ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>
				<asp:BoundField  HeaderText="<%$ Resources:gvHdrAmount %>"  DataField="Amount" ControlStyle-Width="10%" 
					 ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>
			  
				<asp:BoundField   HeaderText="<%$ Resources:gvHdrAccountName %>"  DataField="AccountName" ControlStyle-Width="15%" 
					ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>
				</Columns>
			</asp:GridView>
		</asp:Panel>
		<%--Rahul Aggarwal MITS 19970 05/21/2010-start--%>
        <%-- rsushilaggar MITS 22045 Date 01-Sep-2010 --%>
		<table runat="server" border="0" id="voidcheckreason">
        <%--Ankit Start : Financial Enhancements - Void Code Reason change--%>
		<tr>
			<td>
                <u>                    
                    <asp:Label runat="server" id="lbl_txtVoidReasonCode" text="<%$ Resources:lblVoidReasonCode %>"></asp:Label> 
                </u>&nbsp;&nbsp;
            </td>
			<td>
                <select runat="server" id="cboVoidReasonCode" onchange="return VoidCodeReasonChanged(this);" >
                    <option value=""></option>
                </select>                
			</td>
		</tr>
        <%--Ankit End--%>
		<tr>
			<td><u><asp:Label runat="server" id="lbl_txtVoidReason" text="<%$ Resources:lbltxtVoidReason %>"></asp:Label> </u>&nbsp;&nbsp;</td>
			<td>
				<span class="formw">
					  <asp:TextBox runat="Server" id="voidreason" RMXRef="Instance/Funds/VoidReason" RMXType="memo" ReadOnly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="29" textmode="MultiLine" columns="30" rows="3" />
					  <asp:button runat="server" class="MemoButton" name="voidreasonbtnMemo" id="voidreasonbtnMemo" onclientclick="return EditHTMLMemo('voidreason','yes');" />
					  <asp:TextBox style="display:none" runat="server" RMXRef="Instance/Funds/VoidReason_HTMLComments" id="voidreason_HTML" />
				</span>    
			</td>
		</tr>
		</table>
		<%--Rahul - end--%>
	  <table border="0"  >
	   <tr>
	<%--  mbahl3 mits 30913--%>
		<td><input type="button" class="button" runat="server" name="btnAll"  value="<%$ Resources:btnSelectAll %>" onclick="selectAll();"></td>
		<td><input type="button" class="button" runat="server" name="btnDeselectAll" value="<%$ Resources:btnDeselectAll %>" onclick="deselectAll();"></td>
	<%--	mbahl3 mits 30913--%>
		 <td>
         <%--rsharma220 MITS 31131 start--%>
				 <asp:Button ID="btnRefresh" runat="server" Text="<%$ Resources:btnRefresh %>"  CssClass="button" onclick="btnRefresh_Click"/>
		<%--	rsharma220 MITS 31131 start--%>
             </td>
			 <td>
		 <asp:Button ID="btnProcess1" onclick="btnProcess_Click" UseSubmitBehavior="true"   runat="server" Text="<%$ Resources:btnProcess1 %>"  CssClass="button" OnClientClick="return processChecks();"/>
         <asp:Button ID="btnProcess2" onclick="btnProcess_Click" UseSubmitBehavior="true"   runat="server" Text="<%$ Resources:btnProcess2 %>"  CssClass="button" OnClientClick="return processChecks();"/>
         <asp:Button ID="btnProcess3" onclick="btnProcess_Click" UseSubmitBehavior="true"   runat="server" Text="<%$ Resources:btnProcess3 %>"  CssClass="button" OnClientClick="return processChecks();"/>
		 </td> 
		
	   </tr>
	  </table>
	 </td>
	</tr>
   </table>
   <asp:TextBox runat="server" style="display: none" id="orderby"  />
	  <asp:TextBox runat="server" style="display: none" id="selectedtransids"  />
	  <asp:TextBox runat="server" style="display: none" id="hdnMarkChecksCleared"  />
		<asp:TextBox runat="server" style="display: none" id="orderbydirection"/>
         <asp:TextBox runat="server" style="display: none" id="hdnResetCheck"  />
		<p>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</p>
		</div> 
	</form>
</body>
</html>
