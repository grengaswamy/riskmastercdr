﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Collections;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.ReSetChecks
{
    public partial class ReSetChecks : System.Web.UI.Page
    {
        //Ankit Start : Worked on MITS - 33489 (Data is not coming while Funds => Reset Printed Check is initially loaded)
        //string MergeMessageTemplate = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.LoadChecks4Release</Function></Call><Document><FundsCheck><FromDate>oneweekbefore</FromDate><ToDate>today</ToDate><UseCollections>false</UseCollections><NonPrintedOnly>false</NonPrintedOnly><CompanyEID>0</CompanyEID><NotAttached>0</NotAttached><AccountID>-1</AccountID><BatchNumber>0</BatchNumber></FundsCheck></Document></Message>";
        string MergeMessageTemplate = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.LoadChecks4Release</Function></Call><Document><FundsCheck><FromDate>oneweekbefore</FromDate><ToDate>today</ToDate><UseCollections>false</UseCollections><NonPrintedOnly>false</NonPrintedOnly><CompanyEID>0</CompanyEID><NotAttached>0</NotAttached><AccountID></AccountID><BatchNumber>0</BatchNumber></FundsCheck></Document></Message>";
        //Ankit End
        string MergeMessageTemplateForRelease = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.LoadChecks4Release</Function></Call><Document><FundsCheck><TransIds/></FundsCheck></Document></Message>";

        int iEnableVoidReason = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScript(sCulture, this);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);

            }
            
            XmlDocument Model = null;
            try
            {
                //MITS 31027 - Rakhel MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ReSetChecks.aspx"), "NoChksValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "NoChksValidations", sValidationResources, true);
                //MITS 31027 - Rakhel MultiLingual Changes--end

                if (!IsPostBack)
                {
                   
                    Model = new XmlDocument();
                    MergeMessageTemplate = DecideInitialServiceToCall();
                    string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                    Model.LoadXml(sCWSresponse);
                    BindInitialData(Model);
                    BindpageControls(sCWSresponse);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
      
        private string DecideInitialServiceToCall()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeMessageTemplate);
            doc.SelectSingleNode("//Function").InnerText = "FundManagementAdaptor.LoadChecks4Release";
            return doc.OuterXml;
        }
        private string DecideProcessServiceToCall()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeMessageTemplateForRelease);
            doc.SelectSingleNode("//Function").InnerText = "FundManagementAdaptor.ReleaseChecks";
            return doc.OuterXml;
        }
       
        private void BindInitialData(XmlDocument doc)
        {
            bool IsSubBankAccountEnabled = false;

            txtFromDate.Value = AppHelper.GetDate(doc.SelectSingleNode("//FundsCheck").Attributes["FromDate"].Value);
            txtToDate.Value = AppHelper.GetDate(doc.SelectSingleNode("//FundsCheck").Attributes["ToDate"].Value);

            lblScreen.Attributes.Add("style", "display: inline");
            btnProcess.Attributes.Add("style", "display: inline");
           
            foreach (XmlNode node in doc.SelectNodes("//BankAccount"))
            {
                ListItem item = new ListItem(node.InnerText, node.Attributes["BankAccountId"].Value);
                selBankAccounts.Items.Add(item);
                IsSubBankAccountEnabled = false;
            }

            lblBankAccount.Attributes.Add("style", "display: inline");
        
        }
        private string BindBackData()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeMessageTemplate);
            //doc.SelectSingleNode("//ToDate").InnerText = txtToDate.Value;
            //doc.SelectSingleNode("//FromDate").InnerText = txtFromDate.Value;
            doc.SelectSingleNode("//ToDate").InnerText = AppHelper.GetRMDate(txtToDate.Value).ToString();
            doc.SelectSingleNode("//FromDate").InnerText = AppHelper.GetRMDate(txtFromDate.Value).ToString();
            //doc.SelectSingleNode("//UseCollections").InnerText = txtCollections.Value;
           // doc.SelectSingleNode("//NotAttached").InnerText = txtNotAttached.Value;
           // doc.SelectSingleNode("//CompanyEID").InnerText = selCompany.Value;
            doc.SelectSingleNode("//AccountID").InnerText = selBankAccounts.Value;
            doc.SelectSingleNode("//BatchNumber").InnerText = checkbatchReprint.Text;//added by ashutosh
            return doc.OuterXml;
        }
        private string BindBackDataForProcess()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeMessageTemplateForRelease);
            doc.SelectSingleNode("//TransIds").InnerText = selectedtransids.Text;
         
            return doc.OuterXml;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="diaryDoc"></param>
        /// <returns></returns>
        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(diaryDoc.SelectSingleNode("//Checks")));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }


        private void BindpageControls(string sreturnValue)
        {
            try
            {
                DataSet usersRecordsSet = null;
                XmlDocument usersXDoc = new XmlDocument();
                usersXDoc.LoadXml(sreturnValue);
                usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);

                GridView gvSelectChecksFrame = (GridView)this.Form.FindControl("gvSelectChecksFrame");

                //Ashutosh
                //To Make all checked box as checked if this is a Cleraed check


                if (usersRecordsSet.Tables["check"] != null)
                {
                    foreach (DataRow dr in usersRecordsSet.Tables["check"].Rows)
                    {

                        double dblAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(dr["Amount"]));
                        string sAmount = string.Format("{0:C}", dblAmount);
                        dr["Amount"] = sAmount;
                        if (dr["PaymentFlag"] != null)
                        {
                            if (Common.Conversion.ConvertObjToStr(dr["PaymentFlag"]).Trim() == "0")
                            {
                                dr["TransNumber"] = "Collection";
                            }
                        }

                    } // foreach

                    gvSelectChecksFrame.DataSource = usersRecordsSet.Tables["check"].DefaultView;
                    gvSelectChecksFrame.DataBind();
                } // if
                else
                {
                    gvSelectChecksFrame.DataSource = new ArrayList();
                    gvSelectChecksFrame.DataBind();
                } // else



            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                //Start Rferesh grid
                    DataTable dt = new DataTable();
                    gvSelectChecksFrame.DataSource = dt;
                    gvSelectChecksFrame.DataBind();
                //End
                ErrorControl1.errorDom = "";
                MergeMessageTemplate = BindBackData();
                MergeMessageTemplate = DecideInitialServiceToCall();
                string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                BindpageControls(sCWSresponse);

            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                MergeMessageTemplateForRelease = DecideProcessServiceToCall();
                MergeMessageTemplateForRelease = BindBackDataForProcess();
                string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplateForRelease.ToString());
                MergeMessageTemplate = BindBackData();
                if (ErrorHelper.IsCWSCallSuccess(sCWSresponse))
                {
                    ErrorControl1.errorDom = "";
                    MergeMessageTemplate = DecideInitialServiceToCall();
                    sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                    BindpageControls(sCWSresponse);
                }
                else
                {
                    ErrorControl1.errorDom = sCWSresponse;
                }

            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }

        protected void gvSelectChecksFrame_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Text = AppHelper.GetDate(e.Row.Cells[1].Text);

            }
        }

    }

}