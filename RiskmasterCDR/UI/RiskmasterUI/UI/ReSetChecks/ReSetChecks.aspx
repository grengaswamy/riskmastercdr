﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReSetChecks.aspx.cs" Inherits="Riskmaster.UI.UI.ReSetChecks.ReSetChecks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
	TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
	TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
	   <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">	       { var i; }
	</script>
	 <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">	     { var i; }
	</script>

	<script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">	    { var i; }
	</script>--%>


    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>


					<script language="JavaScript" src="../../Scripts/ResetCheck.js"></script>
				<script language="JavaScript" src="../../Scripts/smqueue.js"></script>
				<script language="JavaScript" src="../../Scripts/trans.js"></script>
	<uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script language=javascript>
        function LoadCheckValues() 
        {
            parent.MDIScreenLoaded();
        }
     
      
	</script>
</head>
<body onload="LoadCheckValues();">
    <form id="frmData" runat="server">
		<div id="maindiv" style="height:100%;width:99%;overflow:auto">
	<table width="90%" border="0">
	<tr>
	 <td class="ctrlgroup2">
     <asp:Label ID="checksearch" runat="server" Text="<%$ Resources:lblchecksearchcriteria %>" />
    </td>
	</tr>
	 <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
	<tr>
	 <td width="50%">
	  <table cellspacing="3">
	   <tr>
		<td >
        <asp:Label ID="checkdatefrom" runat="server" Text="<%$ Resources:lblcheckdatefrom %>" />
        </td>
			<td><input type="text" runat=server  id="txtFromDate" size="10" onblur="dateLostFocus(this.id);"/>&nbsp;
				<%--<input type="button" class="DateLookupControl" id="btnFromDate" value="..."/>--%>
				 <script type="text/javascript">
				     $(function () {
				         $("#txtFromDate").datepicker({
				             showOn: "button",
				             buttonImage: "../../Images/calendar.gif",
				             buttonImageOnly: true,
				             showOtherMonths: true,
				             selectOtherMonths: true,
				             changeYear: true
				         });
				     });
                    </script>
				&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="to" runat="server" Text="<%$ Resources:lblto %>" />
				
				<input type="text" runat=server  id="txtToDate" size="10" onblur="dateLostFocus(this.id);"/>&nbsp;
				<%--<input type="button" class="DateLookupControl" id="btnToDate" value="..."/>  --%>  
				 <script type="text/javascript">
				     $(function () {
				         $("#txtToDate").datepicker({
				             showOn: "button",
				             buttonImage: "../../Images/calendar.gif",
				             buttonImageOnly: true,
				             showOtherMonths: true,
				             selectOtherMonths: true,
				             changeYear: true
				         });
				     });
                    </script>
</td>
	   </tr>
	  <%-- <tr>
		<td>Include Collections:</td>
		<td align="left"><input type="checkbox" runat=server onclick="OnCollCheckChange();" id="chkCollections"><input type="text" runat=server value="false" style="display: none" id="txtCollections">
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												Checks Not Attached to Claims:
												<input type="checkbox" runat=server onclick="OnAttCheckChange();" id="chkNotAttached"><input type="text" runat=server value="0" style="display: none" id="txtNotAttached"></td>
	   </tr>--%>
	 <%--  <tr>
		<td>Filter by Company:</td>
		<td><select runat=server id="selCompany">
		  <option value=""></option>
		 </select>
	   </tr>--%>
          <tr>
		<td><asp:Label ID="lblBankAccount" runat="server" Text="<%$ Resources:lblbankaccount %>"></asp:Label>
                
        </td>
		<td><select runat=server id="selBankAccounts">
		  <option value=""></option>
		 </select>
		 </td>
	   </tr>

	<tr id="">
<td>
<asp:Label ID="checkbatch" runat="server" Text="<%$ Resources:lblcheckbatch %>" />
&nbsp;&nbsp;</td>
<td>
<div title="" style="padding: 0px; margin: 0px">
<asp:TextBox runat="server" size="30" RMXRef="/Instance/Document//CheckBatchChanged/CheckBatchNumber"  ID="checkbatchReprint" onchange="setDataChanged(true);"  />					
</div>
</td>
</tr>

   


	  </table>
	 
								
									<table bgcolor="white" border="0" width="100%">
	   <tr>
		<td class="ctrlgroup2" colspan="6"><asp:Label ID="lblScreen" runat= server Text="<%$ Resources:lblscreen %>"></asp:Label>
        </td>
	   </tr>
       <%--tanwar2 - mits 30910 - start--%>
     <%--  <tr>
           <td>
                <asp:Label ID="lblDeductibleMessage" runat="server" Text="<%$ Resources:lblDeductibleMessage %>"></asp:Label>
           </td>
       </tr>--%>
       <%--tanwar2 - mits 30901 - end--%>
	   </table>
	   
		 
		<asp:Panel ID="Panel1" runat="server" ScrollBars=Vertical Height="350" Width="100%">


						   <asp:GridView ID="gvSelectChecksFrame" runat="server" AutoGenerateColumns="false" 
			AllowPaging="false" AllowSorting="true" Width=100%    ShowHeader="true"  
			
			EmptyDataText="<%$ Resources:gvhdrselectcheckframe %>"  
                               CellPadding="8" onrowdatabound="gvSelectChecksFrame_RowDataBound">
		  
		   
			<RowStyle CssClass="" />
			<AlternatingRowStyle CssClass="rowdark2" />
			 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
			<Columns>              
						<asp:TemplateField  HeaderStyle-HorizontalAlign ="Left" HeaderStyle-CssClass="headertext2" ControlStyle-Width="5%">
						<HeaderTemplate>
						 <input type="checkbox" id='chkTransactions' onclick="TransClick();" />
                            <asp:Label ID="lblTransactionID" runat="server" Text="<%$ Resources:lblTransactionID %>"></asp:Label> <!--MITS 31027 !-->
			   
			</HeaderTemplate>

				<ItemTemplate>
					<input type="checkbox"  id='chk_<%# DataBinder.Eval(Container.DataItem,"TransId")%>' name='chk_<%# DataBinder.Eval(Container.DataItem,"TransId")%>' value='' />
					<%# DataBinder.Eval(Container.DataItem, "TransNumber")%>
						<input type="hidden"  id='RollUp_<%# DataBinder.Eval(Container.DataItem,"TransId")%>'  value='<%# DataBinder.Eval(Container.DataItem, "RollUp_id")%>'  />
			   </ItemTemplate >


			 </asp:TemplateField> 

			 
			<asp:BoundField  HeaderText="<%$ Resources:gvhdrcheckdate %>"   DataField="CheckDate" 
					ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>
				<asp:BoundField   HeaderText="<%$ Resources:gvhdrcontrolnum %>"   DataField="CtlNumber"  ControlStyle-Width="20%"
					 ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>
				
				
				
				 <asp:TemplateField HeaderStyle-HorizontalAlign ="Left" HeaderStyle-CssClass="headertext2" HeaderText="<%$ Resources:gvhdrpayeename %>"  ControlStyle-Width="20%"  >
					  
				<ItemTemplate>
					<%# DataBinder.Eval(Container.DataItem,"LastName")%> , <%# DataBinder.Eval(Container.DataItem,"FirstName")%>
			   </ItemTemplate>


			 </asp:TemplateField> 
				
				
			  
				<asp:BoundField   HeaderText="<%$ Resources:gvhdrclaimnum %>"   DataField="ClaimNumber"  ControlStyle-Width="10%" 
					 ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>
				<asp:BoundField  HeaderText="<%$ Resources:gvhdramount %>"   DataField="Amount" ControlStyle-Width="10%" 
					 ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>
			  
				<asp:BoundField   HeaderText="<%$ Resources:gvhdraccountname %>"   DataField="AccountName" ControlStyle-Width="15%" 
					ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>

                	<asp:BoundField   HeaderText="<%$ Resources:gvhdrbatchnum %>"   DataField="BATCH_NUMBER" ControlStyle-Width="15%" 
					ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

				</asp:BoundField>

                	

				</Columns>
			</asp:GridView>
		</asp:Panel>
		
	  <table border="0"  >
	   <tr>


		<td><input type="button" class="button" runat="server" name="btnAll" value="<%$ Resources:btnselectall %>" onclick="selectAll();"></td>
		<td><input type="button" class="button" runat="server" name="btnDeselectAll"  value="<%$ Resources:btndeselectall %>" onclick="deselectAll();"></td>
		 <td><asp:Button ID="btnRefresh" runat="server" Text="<%$ Resources:btnrefresh %>" CssClass=button  onclick="btnRefresh_Click"/>
			 </td>
			 <td>
		 <asp:Button ID="btnProcess" onclick="btnProcess_Click" UseSubmitBehavior="true"   runat="server" Text="<%$ Resources:btnprocess %>"   CssClass=button OnClientClick="return processChecks();"/>
		 </td> 
		
	   </tr>
	  </table>
	 </td>
	</tr>
   </table>
   <asp:TextBox runat="server" style="display: none" id="orderby"  />
	  <asp:TextBox runat="server" style="display: none" id="selectedtransids"  />
	 
		<asp:TextBox runat="server" style="display: none" id="orderbydirection"/>
		
		<p>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</p>
		</div> 
	</form>
</body>
</html>
