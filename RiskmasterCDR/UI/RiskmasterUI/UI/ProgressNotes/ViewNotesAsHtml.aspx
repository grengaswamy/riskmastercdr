﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewNotesAsHtml.aspx.cs" ValidateRequest="false" 

    Inherits="Riskmaster.UI.ProgressNotes.ViewNotesAsHtml" %>   <!-- validate request set to false :pen testing:mits 27832 -->
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" tagname="ErrorControl" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View All Notes</title><%--14132--%>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <!-- akaushik5 Added for MITS 32148 Starts-->
    <link rel="stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <!-- akaushik5 Added for MITS 32148 Ends-->
  <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
  <!-- Print Stylesheet added to hide the elements not required on print -->
  <link rel="stylesheet" href="../../Content/printStyle.css" type="text/css" media="print"/>
<script language="JavaScript" src="../../Scripts/ProgressNotes.js">

</script>
</head>
<body onload="printPage()"><!--onload="return OnLoadViewAllNotes();"-->
<script language="javascript" type="text/javascript">
    // atavaragiri MITS 28264 //
    function ChangeNotesPage(event) {
        if (event.keyCode == 13) {
            __doPostBack('btnHiddenView', 'Click');
        }
    } // END MITS 28264
</script>
    <form id="wsrp_rewrite_form_1" name="frmData" runat="server" method="post" >
    <input type="hidden" name="$node^19" value="14" id="claimid" runat="server"/>
    <input type="hidden" name="$node^18" value="15" id="eventid" runat="server"/>
    <!-- williams--neha goel:MITS 21704--start-->
    <input type="hidden" name="$node^20" value="16" id="policyid" runat="server"/>
    <input type="hidden" name="$node^21" value="17" id="policyname" runat="server"/>
    <input type="hidden" name="$node^22" value="18" id="sysformname" runat="server"/>
     <input type="hidden" name="$node^23" value="19" id="policynumber" runat="server"/>
     <input type="hidden" name="$node^24" value="20" id="notesparentcodeid" runat="server"/>
    <!-- williams--neha goel:MITS 21704--end-->
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="">
    <!-- Start by Shivendu for MITS 18098 -->
    <input type="hidden"  value="" id="hdTotalNumberOfPages" runat="server" />
    <input type="hidden"  value="" id="hdPageNumber" runat="server" />
    <!-- End by Shivendu for MITS 18098 -->
    <%--//added by swati for MITS # 35530 Gap 15 WWIG--%>
    <input type="hidden"  value="" id="claimantid" runat="server" />
    <%--change end here by swati--%>
    <asp:TextBox Visible="false" id="freezetext" value="" runat="server" /><!-- Added by Shivendu for MITS 18485 -->
    <asp:TextBox Visible="false" id="showDateStamp" value="" runat="server" /><!-- Added by Shivendu for MITS 18485 -->
    <asp:TextBox Visible="false" id="username" value="" runat="server" /><!-- Added by Shivendu for MITS 18485 -->
    <input type="hidden"  value="" id="hdSortColumn" runat="server" /><%--Parijat: MITS 20194 direction and sort expression added--%>
    <input type="hidden"  value="" id="hdDirection" runat="server" /><%--Parijat: MITS 20194 direction and sort expression added--%>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
          <td width = "100%">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                 </td>
         </tr>
        <tr>
            <td >
                <br>
                <table width="100%">
                   <tr>
                      <td style="text-align: center;">
                      <!--williams-neha goel:MITS 21704--start:added div-->
                        <b><asp:Label ID="lblNoNotesToShow" Font-Bold="true" runat="server" /></b> <!-- Added by Manu for MITS 27038 -->
                       <div id="eventclaim" runat ="server">                     
                            <b><asp:Label ID="lblEventNo" Font-Bold="true" runat="server" ></asp:Label> <asp:Label ID="lblEventNumber" Font-Bold="true" runat="server" ></asp:Label><asp:Label ID="lblClaimNo" Font-Bold="true" runat="server" ></asp:Label><asp:Label ID="lblClaimNumber" Font-Bold="true" runat="server" ></asp:Label><asp:Label ID="lblClName" Font-Bold="true" runat="server" ></asp:Label><asp:Label ID="lblClaimantName" Font-Bold="true" runat="server" ></asp:Label>      </b>
							 <%--mbahl3 Mit:25495 added lable for adding claimant name--%>
                        </div>
                       </td>                        
                    </tr>
                    <!--williams-neha goel:MITS 21704--start-->
                    <tr>                    
                        <td style="text-align: center;">
                       <div id="piname" runat="server">
                            <b><asp:Label ID="lblPersonInvolved" runat="server" Text="<%$ Resources:lblPersonInvolvedResrc %>"></asp:Label><asp:Label ID="lblPIName" Font-Bold="true" runat="server" ></asp:Label></b>
                         </div>
                        </td>                        
                    </tr>
                    <tr>                    
                        <td style="text-align: center;">
                        <div id="policy" runat="server">
                            <b><asp:Label ID="lblPolicyNames" runat="server" Text="<%$ Resources:lblPolicyNameResrc %>"></asp:Label><asp:Label ID="lblPolicyName" Font-Bold="true" runat="server" ></asp:Label>*<asp:Label ID="lblPolicyNumber" Font-Bold="true" runat="server" ></asp:Label></b>
                         </div>
                        </td>                      
                    </tr>
                     <!--williams-neha goel:MITS 21704--end-->
                </table>
                   <br>
                <br>
                </td>
                </tr>
                <!-- Start by Shivendu for MITS 18098 -->
          <tr >
          <td align="left" colspan="2">
          <asp:LinkButton ID="lnkFirst" Text = "<%$ Resources:lnkFirstResrc %>" runat="server" OnClick  = "lnkFirst_Click"></asp:LinkButton>
          <asp:LinkButton ID="lnkPrev" Text = "<%$ Resources:lnkPrevResrc %>" runat="server" OnClick = "lnkPrev_Click"></asp:LinkButton>
          <asp:LinkButton ID="lnkNext" Text = "<%$ Resources:lnkNextResrc %>" runat="server" OnClick = "lnkNext_Click"></asp:LinkButton>
          <asp:LinkButton ID="lnkLast" Text = "<%$ Resources:lnkLastResrc %>" runat="server" OnClick = "lnkLast_Click"></asp:LinkButton>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <!--Amandeep MultiLingual Enh-->
           <asp:Label ID="lblPageDetails" runat="server" Text = "<%$ Resources:lblPageResrc %>"></asp:Label>
           <asp:Label ID="lblPageNumber" runat="server" Text = ""></asp:Label>
           <asp:Label ID="lblPageOf" runat="server" Text = "<%$ Resources:lblPageOfResrc %>"></asp:Label>
           <asp:Label ID="lblNumberOfPages" runat="server" Text = ""></asp:Label>
           <!-- atavaragiri MITS 28264 -->
           <asp:TextBox ID="txtPageNumber" runat="server"  Width="5%" onkeypress="javascript:ChangeNotesPage(event);" ></asp:TextBox>
           <asp:Button  ID="btnHiddenView" runat="server" Text="View" onclick="btnHiddenView_Click" style="display:none" />
          <!-- end MITS 28264 -->
          
          </td>
          </tr>
          <!-- End by Shivendu for MITS 18098 -->
                <tr>
                 <td >
               <%-- <b>Activity Date : </b>11/10/2008<br>
                <b>Date Created : </b>11/10/2008<br>
                <b>Time Created : </b>4:05:46 PM<br>
                <b>Note Type : </b>CF Confidential<br>
                <a onclick="&#xA;                     window.opener.document.getElementById('ProgressNotes114').checked = true;&#xA;                     window.opener.ProgressNoteSelectionChanged('11/10/2008','11/10/2008','4:05:46 PM','csc','CF Confidential','Admin','CLAIM: WCAPD001373',&#34;1&#34;,'5036','114','1373');                   &#xA;                   "
                    href="home?pg=riskmaster/ProgressNotes/MainPage&amp;EventID=3163&amp;ClaimID=1373&amp;SysFormName=claimgc&amp;EventNumber=CRV2008003163&amp;NoteType=CF Confidential&amp;NoteTypecodeid=5036&amp;ClaimProgressNoteId=114&amp;ActivityDate=11/10/2008&amp;CommentsFlag=true&amp;NewRecord=false&amp;ShowBack=true&amp;FreezeText=true&amp;UserName=csc">
                    <b>Note Text : </b></a>
                <br>
                <div id="NoteText1">
                </div>
                <br>
                <br>
                <input type="hidden" id="1" value="11/10/2008 4:05:43 PM (csc)gfhjtgjfghfghdfhdfh"><b>Activity
                    Date : </b>11/10/2008<br>
                <b>Date Created : </b>11/10/2008<br>
                <b>Time Created : </b>4:06:04 PM<br>
                <b>Note Type : </b>CG Coverage<br>
                <a onclick="&#xA;                     window.opener.document.getElementById('ProgressNotes115').checked = true;&#xA;                     window.opener.ProgressNoteSelectionChanged('11/10/2008','11/10/2008','4:06:04 PM','csc','CG Coverage','Admin','CLAIM: WCAPD001373',&#34;2&#34;,'5040','115','1373');                   &#xA;                   "
                    href="home?pg=riskmaster/ProgressNotes/MainPage&amp;EventID=3163&amp;ClaimID=1373&amp;SysFormName=claimgc&amp;EventNumber=CRV2008003163&amp;NoteType=CG Coverage&amp;NoteTypecodeid=5040&amp;ClaimProgressNoteId=115&amp;ActivityDate=11/10/2008&amp;CommentsFlag=true&amp;NewRecord=false&amp;ShowBack=true&amp;FreezeText=true&amp;UserName=csc">
                    <b>Note Text : </b></a>
                <br>
                <div id="NoteText2">
                </div>
                <br>
                <br
                <input type="hidden" id="2" value="11/10/2008 4:05:59 PM (csc)gjgfmghmkghj">--%>
                <asp:Repeater id="rptNotes" runat="server" OnItemCommand="Repeater_OnRowCommand" 
                         onitemdatabound="rptNotes_ItemDataBound">
                    <HeaderTemplate></HeaderTemplate>
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <table>
                                    <tr><td><b><asp:Label ID="lblActivityDate" runat="server" Text="<%$ Resources:lblActivityDateResrc %>"></asp:Label></b></td><td><%# DataBinder.Eval(Container.DataItem, "DateEntered")%></td></tr>
                                    <tr><td><b><asp:Label ID="lblDateCreated" runat="server" Text="<%$ Resources:lblDateCreatedResrc %>"></asp:Label></b></td><td><%# DataBinder.Eval(Container.DataItem, "DateCreated")%></td></tr>
                                    <tr><td><b><asp:Label ID="lblTimeCreated" runat="server" Text="<%$ Resources:lblTimeCreatedResrc %>"></asp:Label></b></td><td><%# DataBinder.Eval(Container.DataItem, "TimeCreated")%></td></tr>
                                    <tr><td><b><asp:Label ID="lblEnteredBy" runat="server" Text="<%$ Resources:lblEnteredByResrc %>"></asp:Label></b></td><td><%# DataBinder.Eval(Container.DataItem, "EnteredByName")%></td></tr><%--Parijat: 19591 Including 'Entered By' User there--%>
                                    <tr><td><b><asp:Label ID="lblNoteType" runat="server" Text="<%$ Resources:lblNoteTypeResrc %>"></asp:Label></b></td><td><%# DataBinder.Eval(Container.DataItem, "NoteTypeCode")%></td></tr>
                                     <!--Ashish Ahuja Mits 33124 Start-->
                                    <tr><td><b><asp:Label ID="lblSubject" runat="server" Text="<%$ Resources:lblSubjectResrc %>"></asp:Label></b></td><td><%# DataBinder.Eval(Container.DataItem, "Subject")%></td></tr>
                                    <!--Ashish Ahuja Mits 33124 End-->
                                    <tr><td> 
                                    <%--Comment by kuladeep mits:25424 07/22/2011--%>
                                    <%--<asp:LinkButton ID="lblClaimNumber" runat="server" CommandArgument = "notetext" >
                                    <b>Note Text : </b></asp:LinkButton>--%> <%--Parijat :15010 : link button as well as the hidden fields and even the onitemcomman event--%>
                                    <asp:HiddenField ID="hdnNoteTypecode" Value ='<%# DataBinder.Eval(Container.DataItem, "NoteTypeCode")%>' runat="server"/>
                                    <asp:HiddenField ID="hdnNoteTypeCodeId" Value = '<%# DataBinder.Eval(Container.DataItem, "NoteTypeCodeId")%>' runat="server"/>
                                    <asp:HiddenField ID="hdnClaimProgressNoteId" Value = '<%# DataBinder.Eval(Container.DataItem, "ClaimProgressNoteId")%>' runat="server"/>
                                    <asp:HiddenField ID="hdnDateEntered" Value = '<%# DataBinder.Eval(Container.DataItem, "DateEntered")%>' runat="server"/>
                                    <asp:HiddenField ID="hdnEnteredBy" Value = '<%# DataBinder.Eval(Container.DataItem, "EnteredBy")%>' runat="server"/><%--Parijat: 19591 Including 'Entered By' User there--%>
                                    <asp:HiddenField ID="hdnEditMode" Value = '<%# DataBinder.Eval(Container.DataItem, "IsNoteEditable")%>' runat="server"/>
                                    </td><td></td></tr>
                                    </table>
                               
                                </tr>
                                <tr>
                                    <%-- akaushik5 Changed for MITS 32148 Starts --%>
                                    <%--<td><div id="NoteText2"><%# DataBinder.Eval(Container.DataItem, "NoteMemo")%></div></td>--%>
                                    <%--akaushik5 Changed for MITS 30063 Removed pclass from div level--%>
                                    <td><div id="NoteText2"><%# DataBinder.Eval(Container.DataItem, "NoteMemo")%></div></td>
                                    <%-- akaushik5 Changed for MITS 32148 Ends --%>
                                    
                                </tr>
                               <tr><td><br ><br ></td></tr>
                            
                            </table>
                        </ItemTemplate>
                    <FooterTemplate></FooterTemplate>
                </asp:Repeater>
            </td>
            
        </tr>
        </table>
       <table width="100%">
        <tr>
            <td align="center" >
            <%--Start: tanwar2:MITS --%>
            <%--MGaba2:MITS 22127 --%>
                <%--<button id="btnPrint" style="visibility: visible;" onclick="document.getElementById('btnPrint').style.visibility = 'hidden';window.print(); document.getElementById('btnPrint').style.visibility = 'visible';"> Print
                </button>--%>
                <%-- <asp:button id="btnPrint" OnClientClick="self.print()" Text ="Print" runat ="server" />--%>
                <%-- <asp:button id="btnPrint" runat="server" OnClientClick="document.getElementById('btnPrint').style.visibility = 'hidden';window.print(); document.getElementById('btnPrint').style.visibility = 'visible';" Text="Print" />--%>
                <asp:Button id="btnPrint" runat="server" OnClientClick="window.print();" Text="Print" class="noPrint"/>
            <%--End: tanwar2:MITS --%>       
            </td>
        </tr>
    </table>
    

    <input type="hidden" name="$node^7" value="" id="SysWindowId"><input type="hidden"
        name="$instance" value="H4sIAAAAAAAAAH1TTWvDMAw9t78i9N6qbWCD4gXGOljZGION3V1HaU1jO9jOkv77KcnSfLTMp7yn&#xA;J+lJdthOO8+1wKBUqXabMn+YHb3PNgBFUSyIjPcLYw9Q5lnMPc7+dKZMBkJj92j0QhgFFILMGoHO&#xA;GevahOxfucwwlfpSvUyMVW6QUYS1jfVyuYYmPIumk8kkoMMUel6jGtaUExZRz2UcgZXupLjzaOHD&#xA;moMlY+/Go4NvicWje/EqZdDph3Vyh7ZihRMMWjBqRQWlqXJhFPlC51/x3CS34KZkDZdparqQOjZF&#xA;1YtB912H4DJtY1COKnLhyc3YypvUJ+ozpumK/FyYNFdXGXXI2JjWditCr4HP/TnDq+iRzKK1dPlR&#xA;wlOHtNweNRQ3ZDc87Vh24LPZ7E4npifZGpEr1H5U6vmHuN02Cld3IYMWBUPRU8qlInoV3pOoRZVk&#xA;GowOax5Jz1q/MYP2z4l+Aa+aTihEAwAA"></form>
</body>
</html>
