﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProgressNotesEditPage.aspx.cs"
    Inherits="Riskmaster.UI.NonFDMCWS.ProgressNotesEditPage" ValidateRequest="false" %>

<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>

    
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> </title>
    <link href="../../App_Themes/RMX_Default/rmnet.css" rel="stylesheet" type="text/css" />
        <%--<link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />--%>
    <script language="javascript" type="text/javascript" src="../../Scripts/form.js"></script>
    <script language="javascript" type="text/javascript" src="../../Scripts/ProgressNotes.js"></script>
      
    <!--Praveen ML-->    
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
    <!--Praveen ML-->
    <!--JIRA RMA-693: ajohari2-->
    <style type="text/css">
        #WebHtmlEditor_tw
        {
              overflow-x:hidden !important;
              overflow-y:auto !important;
        }
   </style>
    <!--JIRA RMA-693: ajohari2-->
    <script type="text/javascript" id="igClientScript">
        //Ashish Ahuja-Mits 33124 start
        function Count(text, long) {
            var maxlength = new Number(long); 
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);
                alert("Only " + long + " characters are allowed for this field.");
            }
        }
        //Ashish Ahuja - Mits 33124 end
        var m_DataChanged = true; //as default; just to be safe!
        var m_LastValue = null;
        var m_LastValue_cid = null;
        //window.onbeforeunload = closeIt; //12688
        window.onbeforeunload = function (e) {
            var sMsg = ProgressNotesEditPageValidations.ValidDiscardChanges;
            if (m_DataChanged && !m_IsSaved && !window.m_IsSetTemplateName) { // rrachev JIRA RMA-4789 m_IsSetTemplateName - declared in ProgressNotes.js
                if (window.event || e != null) {
                    if (e != null) {
                        e.returnValue = sMsg;
                    }
                    else {
                        window.event.returnValue = sMsg;
                    }
                }
            }
        }
        var m_IsSaved = false;

        function closeIt() {
            //var sMsg = "You are about to DISCARD UNSAVED CHANGES!"; //Aman ML Changes
            var sMsg = ProgressNotesEditPageValidations.ValidDiscardChanges;
		if (m_DataChanged && !m_IsSaved) {
                	if (window.event != null) {
                	    	window.event.returnValue = sMsg;
                	}
		}
        }
        //MITS 27964 hlv 6/28/2012 end

function WebHtmlEditor1_BeforeAction(oEditor, actID, oEvent, p4, p5, p6, p7, p8, act)
{
	if(actID == 'Save')
	{
	    if(document.forms[0].lstNoteTypes_codelookup.value=='')
	    {
	        //alert("Please enter the Note Type Code.");
	        alert(ProgressNotesEditPageValidations.ValidNoteTypeCodeToEnter);
		   oEvent.needPostBack = false;
	    }
	    else
	    {
                m_IsSaved = true;
	        oEvent.needPostBack = true;
	    }
	    
	}

}
function NoteChangeTrack()//12587:changes regarding restriction of save in case where lookup has to fill--here we are updating the last valuewhcih were selected.
{
    if (document.forms[0].lstNoteTypes_codelookup_cid != null)
        document.forms[0].hdnLastValue_cid.value = document.forms[0].lstNoteTypes_codelookup_cid.value; //m_LastValue_cid
    if (document.forms[0].lstNoteTypes_codelookup != null)
        document.forms[0].hdnLastValue.value = document.forms[0].lstNoteTypes_codelookup.value; //m_LastValue
}
// -->
function onload() //12587:changes in order to avoid postback since 'oEvent.needPostBack = false' was not solving the purpose have overriden the base source code
{
    var obj = document.getElementById("m_BlankNoteError")
    if (obj != null) {
        obj.value = ProgressNotesEditPageValidations.ValidBlankNoteError;
    }
    
        var objDataChanged = document.getElementById('SysPageDataChanged');
	if (objDataChanged != null)
	{
		if (objDataChanged.value == "false")
		{
			m_DataChanged = false;
		}
	}

document.forms[0].WebHtmlEditor_tb_ctl39_image.onclick = WebHtmlEditor1_onclick;

    //changed by gagan for mits 14626 : start
    var strShowDateStamp = document.getElementById('hdShowDateStamp').value;
    //changed by gagan for mits 14626 : end

    //changed for mits  15879 : start
    var oEditor = iged_getById("WebHtmlEditor");
    //Deb ML Changes
    var strDate = getCurrentDate();
    var strTime = getCurrentTime();
    //var Today = new Date();
    //var CurrentMonth = parseInt(Today.getMonth()) + 1;
    //var strDate = CurrentMonth + "/" + Today.getDate() + "/" + Today.getFullYear();
    // commented By Nitesh //Nitesh 15 Jan 2006 Bug corrected Tr no.1330
    //var strTime = Today.getHours() + ":" + Today.getMinutes() + ":" + Today.getSeconds();
    //var strTime = formatTime(Today, 'hh:mm:ss a'); //Nitesh 15 Jan 2006 Bug corrected Tr no.1330
    var strUid = document.getElementById('hdUserName').value;
    var txtDtmStamp = strDate + " " + strTime + " (" + strUid + ") "; //MITS 11894 - abansal
    //Deb ML Changes
    var strEditorText = oEditor.getText();
    //changed by gagan for mits 14626 : start
    if (document.getElementById('hdIsNotPostback').value == "True")//Parijat : Mits 17775-- Date time stamp should appear only once when the page is opened not once the save is done
    {
        if (strShowDateStamp == "True") {

            //if (document.getElementById('hdTemplateDetails').value.toLowerCase() == "false") //R6 Notepad Screens-to avoid date to reappear when a template is associated with the note.
            //{
                if (strEditorText == "") {
                    //MGaba2:MITS 19731
                        //oEditor.setText(txtDtmStamp); //18829: Parijat ---cursor at the end of line on clicking
                    strEditorText = txtDtmStamp; //18829: Parijat ---cursor at the end of line on clicking
                }
                else 
                {  //Parijat :changes for 18829 as well as 20260
                    //                     var Pindex =oEditor.getText().lastIndexOf('</P>');
                    //                     var subStr = oEditor.getText().substring(0, Pindex) + oEditor.getText().substring(Pindex + 4, oEditor.getText().length);
                    //                     oEditor.setText(subStr + '<BR>' + '<DIV>' +  txtDtmStamp+"</DIV><DIV>"  ); //18829: Parijat---cursor at the end of line on clicking

                    // Mgaba2: MITS 20935:Cursor should be 1 space after the dttm/Adjuster stamp 
		//Also leaving one line blank between two enhanced notes
                    // oEditor.setText(oEditor.getText() + '<BR>' + '<DIV>' + txtDtmStamp + "</DIV><DIV>"); //18829: Parijat---cursor at the end of line on clicking
                    if (document.getElementById('hdClaimProgressNoteId').value == "0") {
                        strEditorText = txtDtmStamp + " " + strEditorText; //Text populated by Initialization script
                    }
                    else {
                        strEditorText = oEditor.getText() + '<BR><BR>' + txtDtmStamp;
                    }
                }
                document.getElementById('hdShowDateStamp').value = "False";
            //}
        }
    }    
    //changed by gagan for mits 14626 : end

        //MGaba2:MITS 19731:Users need not to scroll down to find the most recent note:Start
        //In case of Saving,temporarily placing a div tag with id scrollBarPosition and scrolling down to reach that control
    oEditor.setText(strEditorText + "<DIV id=scrollBarPosition></DIV>");
    oEditor.focus();
    var scrollBarPosition = document.getElementById("scrollBarPosition");
    if (scrollBarPosition != null)
        scrollBarPosition.scrollIntoView(false);
    //Again resetting the text which doesnt contain that tag with id scrollBarPosition
    oEditor.setText(strEditorText);

   //No need to set it again
   // oEditor.setText(oEditor.getText());   
    //MGaba2:MITS 19731:End

   document.getElementById('hdTemplateDetails').value = "false";     

}


//Nitesh 15 Jan 2006 Bug corrected Tr no.1330
function formatTime(date, format) {
    format = format + "";
    var result = "";
    var i_format = 0;
    var c = "";
    var token = "";

    var H = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    var yyyy, yy, MMM, MM, dd, hh, h, mm, ss, ampm, HH, H, KK, K, kk, k;
    // Convert real date parts into formatted versions
    var value = new Object();
    value["H"] = H;

    if (("0" + H).length == 3)
        value["HH"] = ("0" + H).substr(1, 2);
    else
        value["HH"] = "0" + H;

    if (H == 0) { value["h"] = 12; }
    //else if (H<12){value["h"]=H-12;} 
    else if (H > 12) { value["h"] = H - 12; } // akashyap3 26-Feb-2009 MITS:14295
    else { value["h"] = H; }

    if (("0" + value["h"]).length == 3)
        value["hh"] = ("0" + value["h"]).substr(1, 2);
    else
        value["hh"] = "0" + value["h"];
    if (H > 11) { value["K"] = H - 12; } else { value["K"] = H; }
    value["k"] = H + 1;
    if (("0" + value["K"]).length == 3)
        value["KK"] = ("0" + value["K"]).substr(1, 2);
    else
        value["KK"] = "0" + value["K"];

    if (("0" + value["k"]).length == 3)
        value["kk"] = ("0" + value["k"]).substr(1, 2);
    else
        value["kk"] = "0" + value["k"]
    if (H > 11) { value["a"] = "PM"; }
    else { value["a"] = "AM"; }
    value["m"] = m;

    if (("0" + m).length == 3)
        value["mm"] = ("0" + m).substr(1, 2);
    else
        value["mm"] = "0" + m;
    value["s"] = s;

    if (("0" + s).length == 3)
        value["ss"] = ("0" + s).substr(1, 2);
    else
        value["ss"] = "0" + s;

    while (i_format < format.length) {
        c = format.charAt(i_format);
        token = "";
        while ((format.charAt(i_format) == c) && (i_format < format.length)) {
            token += format.charAt(i_format++);
        }
        if (value[token] != null) { result = result + value[token]; }
        else { result = result + token; }
    }
    return result;
}
//Nitesh 15 Jan 2006 Bug corrected Tr no.1330




//changed for mits  15879 : end







function WebHtmlEditor1_onclick() {
    //MITS 24745 Raman
    var oEditor = iged_getById("WebHtmlEditor");
    var sTextHtml = oEditor.getText();
    if (sTextHtml == '') {
        //alert("Please enter the Note Text.");
        alert(ProgressNotesEditPageValidations.ValidNoteTextToEnter);
    }
    else if (document.forms[0].lstNoteTypes_codelookup.value == '') {
        //alert("Please enter the Note Type Code.");
        alert(ProgressNotesEditPageValidations.ValidNoteTypeCodeToEnter);
        
    }
    else {
		//Save to hidden field first in case the value not passed back to code-behind page MITS 24745
        document.forms[0].hdTextHtml.value = sTextHtml;
		
		//changes for mits 12587 :restrict save
		//Mgaba2:MITS 19287:Activity date was not getting formatted on direct click of Save button
        dateLostFocus("activitydate");
        if (document.forms[0].hdnLastValue.value != document.forms[0].lstNoteTypes_codelookup.value)//&& document.forms[0].hdnLastValue_cid.value == document.forms[0].lstNoteTypes_codelookup_cid.value)
        {

            lookupTextChanged('lstNoteTypes_codelookup');
            selectCode('NOTE_TYPE_CODE', 'lstNoteTypes_codelookup');
            //alert('The Note Type has been Fetched. Please Click on Save Again to Save the Note,If Note Type is not fetched please enter note type again');
            alert(ProgressNotesEditPageValidations.ValidNoteTypeFetch);
            document.forms[0].hdnRestrictSave.value = "false";
        }
        else {
        //Parijat :19880 --Strip HTML tags and creating Plain text
            var oEditor = iged_getById("WebHtmlEditor");
            oEditor.setText(getNoteMemoStripped(oEditor.getText())); //Parijat 20130 
            document.getElementById('hdPlainText').value = StripHTMLTags(oEditor.getText());
       //End
            document.forms[0].hdnRestrictSave.value = "true";
            //added by Nitin for Mits 15993 on 29-Apr-2009
            document.forms[0].WebHtmlEditor_tb_ctl39_image.disabled = true;
            m_IsSaved = true;
            iged_act('Save:_0'); //12587 - using the source code function so that client validation can be placed in places where needed where we don't need postbacks
        }
    }
}
        function onBlur(sCtrlName) {
            var objCtrl = eval('document.forms[0].' + sCtrlName);
            objCtrl.value = "";
            var objFormElemButton = eval('document.forms[0].' + sCtrlName + "btn");
            objFormElemButton.click()
            return false;
        }
        function getNoteMemoStripped(InString)//Parijat 20130
        {
             var index =-1;
             var len =0;
             while ((InString.indexOf("<form")>0) || (InString.indexOf("<body")>0) || (InString.indexOf("<head")>0)) 
             {
                if (InString.indexOf("<form") > 0) 
                {
                    index = InString.indexOf("<form");
                    len = InString.indexOf(">", index) - index;
                    //Deb: Publix Changes
                    InString = InString.replace(InString.substr(index, len + 1), "");
                    InString = InString.replace("</form>", "");
                    //Deb: Publix Changes
                }
                if (InString.indexOf("<body") > 0) 
                {
                    index = InString.indexOf("<body");
                    len = InString.indexOf(">", index) - index;
                    //Deb: Publix Changes
                    InString = InString.replace(InString.substr(index, len + 1), "");
                    InString = InString.replace("</body>", "");
                    //Deb: Publix Changes
                }
                if (InString.indexOf("<head") > 0) 
                {
                    index = InString.indexOf("<head");
                    len = InString.indexOf(">", index) - index;
                    //Deb: Publix Changes
                    InString = InString.replace(InString.substr(index, len + 1), "");
                    InString = InString.replace("</head>", "");
                    //Deb: Publix Changes
                }
            }
            //Deb: Publix Changes
            while ((InString.indexOf("/form") > 0) || (InString.indexOf("/body") > 0) || (InString.indexOf("/head") > 0)) {
                if (InString.indexOf("/form") > 0) {
                    InString = InString.replace("/form", "");
                }
                if (InString.indexOf("/body") > 0) {
                    InString = InString.replace("/body", "");
                }
                if (InString.indexOf("/head") > 0) {
                    InString = InString.replace("/head", "");
                }
            }
            //Deb: Publix Changes
            return InString;
        }
        function StripHTMLTags(InString) {//Parijat :19880 --Strip HTML tags and creating Plain text                        
        //alert(InString);
            InString = InString.replace(/<BR>/g, " \n"); //Parijat :20425--Inserting the line change tag for executive summary & print note
           // InString = "<div>" + InString + "</div>"; //zmohammad MITs 33445 : No tags were being passed by grid for single line. Enclosed all in a div.
            // mkaran2 : MITS 32741 : Code commented below and used json for better performance            
            //        var cleanedbuffer = ""
//        var PatternString = "\\<[^<>]+\\>";
//        var re = new RegExp(PatternString, "gim");
            //        cleanedbuffer = InString.replace(re, '');
            // mkaran2 : MITS 32741 : start
            // cleanedbuffer = $(InString).text();
            cleanedbuffer = InString.replace(/(<\?[a-z]*(\s[^>]*)?\?(>|$)|<!\[[a-z]*\[|\]\]>|<!DOCTYPE[^>]*?(>|$)|<!--[\s\S]*?(-->|$)|<[a-z?!\/]([a-z0-9_:.])*(\s[^>]*)?(>|$))/gi, ''); //mkaran2: MITS 33851
        // mkaran2 : MITS 32741 : end
        //replace special characters
        cleanedbuffer = cleanedbuffer.replace(/(&nbsp;)|(&quot;)|(&ldquo;)|(&rdquo;)|(&lsquo;)|(&rsquo;)|(&bull;)/gim, ' '); //(&amp;)|
        cleanedbuffer = cleanedbuffer.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">"); //Parijat: replacing &amp; with &--19297
        //alert(cleanedbuffer);
        return cleanedbuffer;
    }

	function setTitle()
	{
	    document.title = document.getElementById("hdHeader").value.replace(/\[ampersand\]/gi, "&");  //MITS 28588 hlv 8/3/12;
	}

        <%--gbindra : MITS#34104 START--%>
        $(document).ready(function () {
            $('#attachedto').find('option').each(function () {
                $(this).attr('title', $(this).text());
            });
        });
        <%--gbindra : MITS#34104 END--%>
</script>
</head>
<body  onload ="onload();tryMDIScreenLoaded();setTitle();" style="margin:0px;height:90%;">

   
    <form id="frmData" name="frmData" method="post" runat="server">
    
    <asp:HiddenField ID="SysFormName" runat="server" value="progressnotes"/>
    <asp:HiddenField ID="eventid" runat="server"/>
    <asp:HiddenField ID="hdclaimid" runat="server"/>
    <asp:HiddenField ID="hdclaimantid" runat="server"/><!--Added by gbindra MITS#34104 02132014 WWIG GAP15-->
    <asp:HiddenField ID="hdeventnumber" runat="server"/>
     <%--Williams-neha goel:MITS 21704--%>
    <asp:HiddenField ID="hdpolicyid" runat="server"/>
    <asp:HiddenField ID="hdpolicyname" runat="server"/>
    <asp:HiddenField ID="hdformname" runat="server"/>
    <asp:HiddenField ID="hdpolicynumber" runat="server"/>
    <asp:HiddenField ID="NoteParentType" runat="server" />
    <%--zmohammad MITS 30218--%>
    <asp:HiddenField ID="existingsubject" runat="server" />   
    <%--Williams-neha goel:MITS 21704:end--%>
    <asp:HiddenField ID="hdPlainText" runat="server"/>
    <asp:HiddenField ID="hdTextHtml" runat="server"/>
    <asp:HiddenField ID="hdClaimProgressNotesFlag" runat="server" />
    <asp:HiddenField ID="hdTextEncodedHtml" runat="server"/>
    <asp:HiddenField ID="hdCommentsFlag" runat="server" />
    <asp:HiddenField ID="existingComments" runat="server" />
    <asp:HiddenField ID="hdNewRecord" runat="server"/>
    <asp:HiddenField ID="hdLob" runat="server" />   <%--csingh7 :19333 for filtering Note Type(s) code on the basis of LOB--%>
    <asp:HiddenField ID="hdLOBCode" runat="server" /> <!-- pmittal5 Mits 21514: for checking Enhaanced notes permission on the basis of LOB Short Code-->
    <asp:HiddenField ID="hdClaimProgressNoteId" runat="server" />
    <asp:HiddenField ID="hdnClaimant" runat="server" /><%--Parijat :12821  for showing claimant--%>
    <asp:HiddenField ID="hdnLastValue" runat="server" /><%--changes for 12587:restrict save --%>
    <asp:HiddenField ID="hdnLastValue_cid" runat="server" /><%--changes for 12587:restrict save --%>
    <asp:HiddenField ID="hdnRestrictSave" runat="server" value="true"/>  <%--changes for 12587:restrict save --%>  
    <asp:HiddenField ID="hdFreezeText" runat="server" /><%--changes regarding 12334 ---freeze text area--%>
    <asp:HiddenField ID="hdUserName" runat="server" /><%--changes regarding 12334 ---freeze text area--%>
    <asp:HiddenField ID="hdTemplateDetails" runat="server" value="false"/>
    <asp:HiddenField ID="SysPageDataChanged" runat="server" value="false"/>
    <asp:textbox runat="server" style="display:none" id="hdHeader" />
    <%--Changed by Gagan for mits 14626 : start --%>
    <asp:HiddenField ID="hdShowDateStamp" runat="server" />
    <%--Changed by Gagan for mits 14626 : end --%>
    <asp:HiddenField ID="hdIsNotPostback" runat="server" /><%--Parijat : Mits 17775--%>
    <asp:ScriptManager ID="SMgr" runat="server" />
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    
    <%if (hdNewRecord.Value.ToUpper() == "FALSE" && hdFreezeText.Value.ToUpper() == "TRUE")
      { %>
          <asp:Panel ID="pnlOne" runat="server" Height="20%" ScrollBars="Auto" Style="width:99.5%;" BorderColor="LightGray"  BorderStyle="Ridge"  BorderWidth ="2px">
          <%-- akaushik5 Changed for MITS 32148 Starts--%>
          <%-- <div id="txtExistingComments" style='FONT-SIZE:9pt;FONT-FAMILY:Arial;TEXT-DECORATION:none;background-color:white;border:[grey];' runat="server"/>--%>
          <%--akaushik5 Changed for MITS 30063 Removed pclass from div level--%>
          <div id="txtExistingComments" style='FONT-SIZE:9pt;FONT-FAMILY:Arial;TEXT-DECORATION:none;background-color:white;border:[grey];' runat="server"/>
          <%-- akaushik5 Changed for MITS 32148 Ends--%>
          </asp:Panel>
      <%--</div>--%>
    <%} %>
    <%--changes regarding 12334 ---freeze text area--%>
    <ig_spell:WebSpellChecker ID="WebSpellChecker1" runat="server">
    <%--aanandpraka2-Changes start for MITS 23104-Spell check ignoring upper case words--%>
    <SpellOptions>
    <PerformanceOptions AllowCapitalizedWords="false"/>
    </SpellOptions>
    <%--aanandpraka2-Changes end for MITS 23104-Spell check ignoring upper case words--%>
    </ig_spell:WebSpellChecker>
  <%--12587--BeforeAction="WebHtmlEditor1_BeforeAction" has been removed since the postbacks were not restricted even in case of clienmt validations therefore have overriden the base source code with  own onclick finction --%>
     <%-- UseLineBreak Changed to false by Manika : MITS 24920 --%> 
    <%-- akaushik5 Changed for MITS 32148 Starts--%>
    <%-- ighedit:WebHtmlEditor ID="WebHtmlEditor" width="100%" Height="60%" runat="server" SpellCheckerID="WebSpellChecker1" UploadedFilesDirectory="/RiskmasterUI/UploaderTemp" UseLineBreak="false"--%>
    <ighedit:WebHtmlEditor ID="WebHtmlEditor" width="100%" Height="60%" runat="server" SpellCheckerID="WebSpellChecker1" UploadedFilesDirectory="/RiskmasterUI/UploaderTemp" UseLineBreak="false" CssClass="pclass">
    <%-- akaushik5 Changed for MITS 32148 Ends--%>
    <%-- Changed for mits 16030 : on lost focus show that data has been changed--%>   
    <%-- Rakhi-Changed Event to KeyPress else setDataChanged is always set to true--%> 
     <%-- setDataChanged Changed to false by Manika : MITS 24920 --%> 
     <%-- afteraction = setDataChanged Changed to true by Ishan : MITS 27104 --%> 
     <ClientSideEvents KeyPress="m_DataChanged = true;" AfterAction="m_DataChanged = true;" KeyDown="m_DataChanged = true;"/>
    </ighedit:WebHtmlEditor>
    <div class="half"><span class="required"><asp:Label ID="lblNoteType" runat="server" Text="<%$ Resources:lblNoteTypeResrc %>"></asp:Label></span>
     <uc:CodeLookUp runat="server" ID="lstNoteTypes" CodeTable="NOTE_TYPE_CODE" ControlName="lstNoteTypes" Filter="CODES.RELATED_CODE_ID IN(NoteParentType,0)"/></div>
     <div class="half"><span class="required"><asp:Label ID="lblActivityDate" runat="server" Text="<%$ Resources:lblActivityDateResrc %>"></asp:Label></span>
    <%--MGaba2: Mits 19287: Date entered manually wasnt getting formatted.Replacing the control and adding daterlostfunction--%>     
    <asp:TextBox runat="server" FormatAs="date" ID="activitydate" RMXType="date" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
    <script type="text/javascript">
        $(function () {
            $("#activitydate").datepicker({
                showOn: "button",
                buttonImage: "../../Images/calendar.gif",
                buttonImageOnly: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                changeYear: true
            });
        });
    </script>
    
     </div>      
     <%--<asp:TextBox runat="server" FormatAs="date" id="activitydate" />
     <ajaxtoolkit:calendarextender runat="server" id="activitydate_ajax" TargetControlID="activitydate" OnClientDateSelectionChanged="dateLostFocus(this.id),setDataChangedTrue" /></div> <!--Added OnClientDateSelectionChanged Event for MITS 17774-->--%>
     <div class="half"><span class="label"><asp:Label ID="lblNoteAttachedTo" runat="server" Text="<%$ Resources:lblNoteAttachedToResrc %>"></asp:Label></span><asp:DropDownList ID="attachedto" runat="server" onchange="setDataChanged(true);" Width="210"  OnSelectedIndexChanged="attachedto_change"></asp:DropDownList></div> <!--Added onchange event for MITS 17774--><!--OnSelectedIndexChanged added by gbindra MITS#34104 WWIG GAP15-->
    <div class="half">
        <span class="label"><asp:Label ID="lblTemplates" runat="server" Text="<%$ Resources:lblTemplatesResrc %>"></asp:Label></span>
        <asp:TextBox runat="server" ID="TemplateName" type="text" onblur="return onBlur(this.id);"
            OnTextChanged="GetTemplateDetails" />
        <asp:Button runat="server" ID="TemplateNamebtn" Text="..." class="button" OnClientClick="return lookupTemplateName();" />
        <asp:TextBox runat="server" ID="TemplateName_cid" type="text" Style="display: none" />
        <asp:TextBox runat="server" ID="TempName" type="text" Style="display: none" />
        <br />
       
    </div>
    <!--zmohammad MITS 30218, MITS 31323-->
    <div class="half">
        <span class="label">
        <asp:Label ID="lblSubject" runat="server" Text="<%$ Resources:lblSubject %>">
    </asp:Label>
        </span>
         <!--Ashish Ahuja Mits 33124 Start-->
         <%--<asp:TextBox ID="subject" runat="server"></asp:TextBox> --%>
        <asp:TextBox ID="subject" TextMode="MultiLine" Width="300" runat="server" onKeyUp="Count(this,250)" onChange="Count(this,250)" ></asp:TextBox>
         <!--Ashish Ahuja Mits 33124 Start-->
    </div>
    <div class="full"></div>
    <!--smishra54: MITS 25641 -->
     <div>
        <span class="label">
    <asp:Button CssClass="button" ID="btnCreateAnotherNote" Text="<%$ Resources:btnCreateAnotherNoteResrc %>" runat="server" onclick="btnCreateAnotherNote_Click" />
    <!--smishra54:End-->
     <%--<input type="button" class="button" id="CancelNotes" onclick="return ConfirmCancel();" value="Cancel"/>--%>
    <%--<input type="button" class="button" id="BackToNotes" onclick="BacktoNotes()" value="Back To Notes" runat="server"/>--%>
   <asp:Button CssClass="button" ID="btnBackToNotes" Text="<%$ Resources:btnBackToNotesResrc %>" runat="server" onclick="btnBackToNotes_Click" />
    <%--added by Nitin for Mits 15988 on 29-Apr-2009--%>
    <asp:Button class="button" id="btnCancelNotes"  Text="<%$ Resources:btnCancelNotesResrc %>" onclientclick="window.close();return false;" runat="server"/>
    <asp:Button class="button" id="btnBackToAllRecrods"  Text="<%$ Resources:btnBackToAllRecrodsResrc %>" runat="server" onclick="btnBackToAllRecrods_Click"/>
    <%--ended by Nitin for Mits 15988 on 29-Apr-2009--%>
    <asp:hiddenfield runat="server" ID="m_BlankNoteError" Value="Please enter note text before save." ></asp:hiddenfield>
    </span>
    </div>
   </form>
    <script type="text/javascript">
        <!--
        function getCurrentDate()
        {
            var sDate='<%=AppHelper.GetDate(System.DateTime.Today.ToString())%>';
            if(sDate == "")
            {
                var Today = new Date();
                var CurrentMonth = parseInt(Today.getMonth()) + 1;
                sDate = CurrentMonth + "/" + Today.getDate() + "/" + Today.getFullYear();
            }
            return sDate;
        }
        function getCurrentTime()
        {
            var sTime='<%=AppHelper.GetTime(System.DateTime.Now.TimeOfDay.ToString())%>';
            if(sTime == "")
            {
                sTime = Today.getHours() + ":" + Today.getMinutes() + ":" + Today.getSeconds();
                sTime = formatTime(Today, 'hh:mm:ss a');
            }
            return sTime;
        }
        //-->
    </script>
</body>

</html>
