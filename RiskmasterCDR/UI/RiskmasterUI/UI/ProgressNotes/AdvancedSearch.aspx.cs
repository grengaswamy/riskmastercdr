﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.Shared;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.ProgressNotes
{
    public partial class AdvancedSearch : NonFdmFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Praveen: ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
            //Praveen: ML Changes

            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("AdvancedSearch.aspx"), "AdvancedSearchValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "AdvancedSearchValidationScripts", sValidationResources, true);

           
            
            if (AppHelper.GetValue("formsubtitle") != String.Empty)
            {  
                // pentesting changes atavaragiri :mits 27832 
              //this.hdHeader.Text = AppHelper.GetValue("formsubtitle");
                if (!String.IsNullOrEmpty(AppHelper.GetValue("formsubtitle")))
                    this.hdHeader.Text = AppHelper.HTMLCustomEncode(AppHelper.GetValue("formsubtitle"));
                //END: pentesting changes atavaragiri :mits 27832 
            }
            base.Page_Load(sender, e);
            //EnhancedMDI
            //Pen testing :atavaragiri MITS 27832 (null check and html custom encode)

            //claimant.Value = Request.QueryString["ClaimantInfo"];
            //SysFormName.Value = Request.QueryString["FormName"];
            //eventid.Value = Request.QueryString["EventID"];
            //claimid.Value = Request.QueryString["ClaimID"];
            //eventnumber.Value = Request.QueryString["EventNumber"];
            //LOB.Value = Request.QueryString["LOB"];   //pmittal5 Mits 21514
             //policyid.Value = Request.QueryString["PolicyId"];
            //policyname.Value = Request.QueryString["PolicyName"];
            //policynumber.Value = Request.QueryString["PolicyNumber"];
             //NoteParentType.Value = Request.QueryString["CodeId"];
            //williams-neha goel:MITS 21704---start:added for policy enhanced notes
            if (!String.IsNullOrEmpty(Request.QueryString["ClaimantInfo"]))
                claimant.Value = AppHelper.HTMLCustomEncode(Request.QueryString["ClaimantInfo"]);
            if (!String.IsNullOrEmpty(Request.QueryString["FormName"]))
                SysFormName.Value = AppHelper.HTMLCustomEncode(Request.QueryString["FormName"]);
            if (!String.IsNullOrEmpty(Request.QueryString["EventID"]))
                eventid.Value = AppHelper.HTMLCustomEncode(Request.QueryString["EventID"]);
            if (!String.IsNullOrEmpty(Request.QueryString["ClaimID"]))
                claimid.Value = AppHelper.HTMLCustomEncode(Request.QueryString["ClaimID"]);
            /*Added by gbindra 7/2/14 JIRA issue RMA-359 START*/
            if (!String.IsNullOrEmpty(Request.QueryString["ClaimantId"]))
            {
                hdClaimantId.Value = AppHelper.HTMLCustomEncode(Request.QueryString["ClaimantId"]);
            }
            /*Added by gbindra 7/2/14 JIRA issue RMA-359 STOP*/
            if (!String.IsNullOrEmpty(Request.QueryString["EventNumber"]))
                eventnumber.Value = AppHelper.HTMLCustomEncode(Request.QueryString["EventNumber"]);
            if (!String.IsNullOrEmpty(Request.QueryString["LOB"]))
                if (LOB.Value == null || string.Compare(LOB.Value, "0", true) == 0 || string.Compare(LOB.Value, "<%=Request.QueryString[\"LOB\"]%>", true) == 0)//If condition added by gbindra MITS#34104 WWIG GAP15
                {
                LOB.Value = AppHelper.HTMLCustomEncode(Request.QueryString["LOB"]);   //pmittal5 Mits 21514
                }
            if (!String.IsNullOrEmpty(Request.QueryString["PolicyId"]))
                policyid.Value = AppHelper.HTMLCustomEncode(Request.QueryString["PolicyId"]);
            else
                policyid.Value = "";
            if (!String.IsNullOrEmpty(Request.QueryString["PolicyName"]))
                policyname.Value = AppHelper.HTMLCustomEncode(Request.QueryString["PolicyName"]);
            else
                policyname.Value = "";

            if (!String.IsNullOrEmpty(Request.QueryString["PolicyNumber"]))
                policynumber.Value = AppHelper.HTMLCustomEncode(Request.QueryString["PolicyNumber"]);
            else
                policynumber.Value = "";
            if (!String.IsNullOrEmpty(Request.QueryString["CodeId"]))

                NoteParentType.Value = AppHelper.HTMLCustomEncode(Request.QueryString["CodeId"]);
            
            
            // END:Pen testing changes:mits 27832
            // mkaran2 - MITS 27038 - start 
            if (!String.IsNullOrEmpty(Request.QueryString["SortColumn"]))
                hdSortColumn.Value = AppHelper.HTMLCustomEncode(Request.QueryString["SortColumn"]);
            if (!String.IsNullOrEmpty(Request.QueryString["Direction"]))
                hdDirection.Value = AppHelper.HTMLCustomEncode(Request.QueryString["Direction"]);
            if (!String.IsNullOrEmpty(Request.QueryString["claimprogressnoteid"]))
                claimprogressnoteid.Value = AppHelper.HTMLCustomEncode(Request.QueryString["claimprogressnoteid"]);
            if (!String.IsNullOrEmpty(Request.QueryString["btnViewNotes"]))
            {
                tdbtnNotes.Visible = !(bool.Parse(AppHelper.HTMLCustomEncode(Request.QueryString["btnViewNotes"])));
                tdRadioNotes.Visible = !(bool.Parse(AppHelper.HTMLCustomEncode(Request.QueryString["btnViewNotes"])));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["freezetext"]))
                freezetext.Value = AppHelper.HTMLCustomEncode(Request.QueryString["freezetext"]);
            if (!String.IsNullOrEmpty(Request.QueryString["showDateStamp"]))
                showDateStamp.Value = AppHelper.HTMLCustomEncode(Request.QueryString["showDateStamp"]);
            if (!String.IsNullOrEmpty(Request.QueryString["username"]))
                username.Value = AppHelper.HTMLCustomEncode(Request.QueryString["username"]);
            // mkaran2 - MITS 27038 - end 

            if (SysFormName.Value == "policy")
            {
                HtmlInputButton oButtonControl = (HtmlInputButton)this.FindControl("lstClaimsbtn");
                if (oButtonControl != null)
                {
                    oButtonControl.Disabled = true;
                }
                HtmlInputButton oDeleteControl = (HtmlInputButton)this.FindControl("lstClaimsbtndel");
                if (oDeleteControl != null)
                {
                    oDeleteControl.Disabled = true;
                }
                HtmlInputControl oListControl = (HtmlInputControl)this.FindControl("lstClaims_lst");
                if (oListControl != null)
                {
                    oListControl.Disabled = true;
                }
            }
            //williams-neha goel:MITS 21704---end
            //Added by Amitosh for mits 23473 (05/11/2011)
                string sort = Request.QueryString["sortPref"];
                if (!string.IsNullOrEmpty(sort))
                {
                    string[] sortpref = new string[4];

                    sortpref = sort.Split('|');
                    for (int i = 0; i < sortpref.Length; i++)
                    {

                        if (sortpref[i] == "CLAIM_PRG_NOTE.ENTERED_BY_NAME")
                        {
                            sortpref[i] = "1";
                        }
                        else if (sortpref[i] == "NOTE_TYPE_SHORT_CODE")
                        {
                            sortpref[i] = "2";
                        }
                        else if (sortpref[i] == "DATE_ENTERED")
                        {
                            sortpref[i] = "3";
                        }
                        else if (sortpref[i] == "USER_TYPE_CODE")
                        {
                            sortpref[i] = "4";
                        }
                        else if (sortpref[i] == "ADJUSTER_LASTNAME, ADJUSTER_FIRSTNAME")
                        {
                            sortpref[i] = "5";
                        }
                    }
                        if(1 <= sortpref.Length && !string.IsNullOrEmpty(sortpref[0]))
                        selSortBy1.SelectedValue = sortpref[0];
                        if (2 <= sortpref.Length && !string.IsNullOrEmpty(sortpref[1]))                       
                        selSortBy2.SelectedValue = sortpref[1];
                        if (3 <= sortpref.Length && !string.IsNullOrEmpty(sortpref[2]))
                        selSortBy3.SelectedValue = sortpref[2];
                        if (4 <= sortpref.Length && !string.IsNullOrEmpty(sortpref[3]))
                        selSortBy4.SelectedValue = sortpref[3];

                        // Added by akaushik5 for MITS 30789 Starts
                        string order = Request.QueryString["orderPref"];
                        if (!string.IsNullOrEmpty(order))
                        {
                            string[] orderPref = order.Split('|');
                            if (orderPref.Length >= 1 && !string.IsNullOrEmpty(orderPref[0]))
                            {
                                selOrderBy1.SelectedValue = orderPref[0];
                            }

                            if (orderPref.Length >= 2 && !string.IsNullOrEmpty(orderPref[1]))
                            {
                                selOrderBy2.SelectedValue = orderPref[1];
                            }

                            if (orderPref.Length >= 3 && !string.IsNullOrEmpty(orderPref[2]))
                            {
                                selOrderBy3.SelectedValue = orderPref[2];
                            }

                            if (orderPref.Length >= 4 && !string.IsNullOrEmpty(orderPref[3]))
                            {
                                selOrderBy4.SelectedValue = orderPref[3];
                            }
                        }
                    // Added by akaushik5 for MITS 30789 Ends
                    
                }
            //end Amitosh

        }
       
        //EnhancedMDI
        protected void Return_click(object sender, EventArgs e)
        {
            //williams::MITS 21704-neha goel:added policyid,policyname and policynumber
            //williams::MITS 21704-retrofit:neha goel:  pmittal5 21514 added: "&LOB=" + LOB.Value
            Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotes.aspx?EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + SysFormName.Value + "&EventNumber=" + eventnumber.Value
                          + "&formsubtitle=" + this.hdHeader.Text.Replace("&", "^@").Replace("#", "*@") + "&PolicyId=" + policyid.Value + "&PolicyName=" + policyname.Value + "&PolicyNumber=" + policynumber.Value + "&LOB=" + LOB.Value + "&ClaimantId=" + hdClaimantId.Value);
            //hdHeader hlv MITS 28588 11/12/12 asharma326 MITS 34294
        }
    }
}
