﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Riskmaster.UI.ProgressNoteService;
using Riskmaster.BusinessHelpers;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;

namespace Riskmaster.UI.ProgressNotes
{
    public partial class ProgressNotesTemplates : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ProgressNoteBusinessHelper pn = null;
            ProgressNoteTemplates objTemplates = new ProgressNoteTemplates();
            try
            {
                if (AppHelper.GetValue("formsubtitle") != String.Empty)
                {
                    this.hdHeader.Text = AppHelper.GetValue("formsubtitle");
                    //this.hdHeader.Text = AppHelper.HTMLCustomEncode(AppHelper.GetValue("formsubtitle")); //hlv MITS 28588 11/12/12
                    //msampathkuma jira-11851
                    if (this.hdHeader.Text.Contains("^@"))
                    {
                        this.hdHeader.Text = AppHelper.HTMLCustomEncode(AppHelper.GetValue("formsubtitle"));
                    }
                    else
                    {
                        this.hdHeader.Text = AppHelper.HTMLCustomDecode(AppHelper.GetValue("formsubtitle"));//msampathkuma jira-11170
                    }
                }

                //Amandeep MultiLingual Changes
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ProgressNotesTemplates.aspx"), "ProgressNotesTemplatesValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "ProgressNotesTemplatesValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes

                if(!Page.IsPostBack)
                {
                    // pen testing changes atavaragiri :mits 27832
                    
                    //claimant.Value = AppHelper.GetQueryStringValue("ClaimantInfo");
                    //SysFormName.Value =AppHelper.GetQueryStringValue("FormName");
                    //eventid.Value = AppHelper.GetQueryStringValue("EventID");
                    //claimid.Value = AppHelper.GetQueryStringValue("ClaimID");
                    //eventnumber.Value = AppHelper.GetQueryStringValue("EventNumber");
                    //ViewState["LookUp"] = AppHelper.GetQueryStringValue("LookUp");
                    ////williams:neha goel:MITS 21704:start
                    //policyid.Value = AppHelper.GetQueryStringValue("PolicyId");
                    //policyname.Value = AppHelper.GetQueryStringValue("PolicyName");
                    //policynumber.Value = AppHelper.GetQueryStringValue("PolicyNumber");
                    ////williams:neha goel:MITS 21704:end
                    //LOB.Value =AppHelper.GetQueryStringValue("LOB"); //pmittal5 Mits 21514


                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("ClaimantInfo")))
                    claimant.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimantInfo"));
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("FormName")))
                    SysFormName.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("FormName"));
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("EventID")))
                    eventid.Value =  AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("EventID"));
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("ClaimID")))
                    claimid.Value =  AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimID"));
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("EventNumber")))
                    eventnumber.Value =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("EventNumber"));
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("LookUp")))
                    ViewState["LookUp"] =  AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("LookUp"));
                    //williams:neha goel:MITS 21704:start
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicyId")))
                    policyid.Value =  AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolicyId"));
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicyName")))
                    policyname.Value =  AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolicyName"));
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicyNumber")))
                    policynumber.Value =  AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolicyNumber"));
                    //williams:neha goel:MITS 21704:end
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("LOB")))
                    LOB.Value =  AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("LOB")); //pmittal5 Mits 21514

                    // END: pen testing changes

                    pn = new ProgressNoteBusinessHelper();
                    //pmittal5 Mits 21514 - Pass EventID, ClaimID and LOB to check Enhanced notes permissions
                    int iOutResult=0;
                    bool bResult = false;
                    bResult = Int32.TryParse(eventid.Value, out iOutResult);
                    objTemplates.EventID = iOutResult;
                    bResult = Int32.TryParse(claimid.Value, out iOutResult);
                    objTemplates.ClaimID = iOutResult;
                    objTemplates.LOB = LOB.Value;
                    //End - pmittal5

                    objTemplates = pn.LoadTemplates(objTemplates);
                    if (objTemplates.objProgressNoteTemplateList!= null && objTemplates.objProgressNoteTemplateList.ToList().Count != 0)
                    {
                        DataViewBind(objTemplates.objProgressNoteTemplateList.ToList());
                        tdNoTemplates.Visible = false;
                    }
                    else
                    {
                        tdNoTemplates.Visible = true;
                    }
                    //Start rsushilaggar MITS 21119 06/18/2010
                    ViewState["bCreatePermission"] = objTemplates.bCreatePermission.ToString();
                    ViewState["bDeletePermission"] = objTemplates.bDeletePermission.ToString();
                    ViewState["bEditPermission"] = objTemplates.bEditPermission.ToString();
                    //End rsushilaggar MITS 21119 06/18/2010
                }
                if (Convert.ToBoolean(ViewState["LookUp"]))
                {
                    TemplateMemoDiv.Visible = false;
                    btnCancel.Visible = false;
                    btnCreateTemplate.Visible = false;
                    btnDeleteTemplate.Visible = false;
                    btnEditTemplate.Visible = false;
                    btnClose.Visible = true;
                }
                //Start rsushilaggar MITS 21119 06/18/2010
                ApplySecurityPermissions();

            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl.errorDom = ee.Detail.Errors;
                return;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
        }

        protected void btnCreateTemplate_Click(object sender, EventArgs e)
        {
            //pmittal5 Mits 21514 
            //Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotesEditTemplates.aspx?NewRecord=true&EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + SysFormName.Value + "&EventNumber=" + eventnumber.Value + "&ClaimantInfo=" + claimant.Value + "&formsubtitle=" + this.hdHeader.Text);
            //williams-neha goel:MITS 21704--aded policy parameters
            Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotesEditTemplates.aspx?NewRecord=true&EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + SysFormName.Value + "&EventNumber=" + eventnumber.Value + "&ClaimantInfo=" + claimant.Value
                          + "&formsubtitle=" + AppHelper.EncodeSpecialSigns(this.hdHeader.Text) + "&LOB=" + LOB.Value + "&PolicyId=" + policyid.Value + "&PolicyName=" + policyname.Value + "&PolicyNumber=" + policynumber.Value);
            //hlv MITS 28588 11/12/12 asharma326 MITS 34294
        }

        protected void btnEditTemplate_Click(object sender, EventArgs e)
        {
            //pmittal5 Mits 21514 
            //Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotesEditTemplates.aspx?NewRecord=false&TemplateName=" + (Server.UrlEncode(TemplateName.Value)).Replace("'", "~~*~~") + "&TemplateId=" + TemplateId.Value + "&EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + SysFormName.Value + "&EventNumber=" + eventnumber.Value + "&ClaimantInfo=" + claimant.Value + "&formsubtitle=" + this.hdHeader.Text); //Changed for Mits 18897
            //williams:neha goel:MITS 21704---added policy parameters
            Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotesEditTemplates.aspx?NewRecord=false&TemplateName=" + (Server.UrlEncode(TemplateName.Value)).Replace("'", "~~*~~") + "&TemplateId=" + TemplateId.Value + "&EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + SysFormName.Value + "&EventNumber=" + eventnumber.Value + "&ClaimantInfo=" + claimant.Value
                          + "&formsubtitle=" + AppHelper.EncodeSpecialSigns(this.hdHeader.Text) + "&LOB=" + LOB.Value + "&PolicyId=" + policyid.Value + "&PolicyName=" + policyname.Value + "&PolicyNumber=" + policynumber.Value); //Changed for Mits 18897
            //hlv MITS 28588 11/12/12asharma326 MITS 34294
        } 

        protected void btnDeleteTemplate_Click(object sender, EventArgs e)
        {
            ProgressNoteBusinessHelper pn = null;
            ProgressNoteTemplates objTemplates = new ProgressNoteTemplates();
            bool bResult = false;
            int iOutResult = 0;
            try
            {
                pn = new ProgressNoteBusinessHelper();
                if (TemplateId.Value != "")
                {
                    objTemplates.TemplateId = TemplateId.Value;
                    //Added for Mits 22167:“No Templates Available”-Error Message appeared after a template is deleted.
                    bResult = Int32.TryParse(eventid.Value, out iOutResult);
                    objTemplates.EventID = iOutResult;
                    bResult = Int32.TryParse(claimid.Value, out iOutResult);
                    objTemplates.LOB = LOB.Value;
                    //Added for Mits 22167:“No Templates Available”-Error Message appeared after a template is deleted.
                    objTemplates = pn.DeleteTemplate(objTemplates);
                    DataViewBind(objTemplates.objProgressNoteTemplateList.ToList());

                    if (objTemplates.objProgressNoteTemplateList.ToList().Count != 0)
                    {

                        tdNoTemplates.Visible = false;
                    }
                    else
                    {
                        tdNoTemplates.Visible = true;
                        TemplateId.Value = "";
                    }
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl.errorDom = ee.Detail.Errors;
                return;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
        }
        public void SetGrdRadiosOnClick()
        {
            int i;
            RadioButton B;

            for (i = 0; i < grdTemplates.Rows.Count; i++)
            {
                B = (RadioButton)grdTemplates.Rows[i].FindControl("RowSelector");
                B.Attributes.Add("OnClick", "SelectOnlyOneRadio(" + B.ClientID + ", " + "'grdTemplates'" + ")");
                
            }
        }
        public void DataViewBind(System.Collections.Generic.List<ProgressNoteTemplates> objPrgressNoteTemplatesList)
        {
                if (objPrgressNoteTemplatesList.Count > 0)
                {
                    ViewState["Datagrid_Arraylist"] = objPrgressNoteTemplatesList;
                    tdNoTemplates.Visible = false;
                }
                else
                {
                    ViewState["Datagrid_Arraylist"] = "";
                    tdNoTemplates.Visible = false;
                }
              
                grdTemplates.DataSource = objPrgressNoteTemplatesList;
                grdTemplates.DataBind();
                if (!Convert.ToBoolean(ViewState["LookUp"]))
                {
                    SetGrdRadiosOnClick();
                    //first radio click on load and also the event 
                    if (grdTemplates.Rows.Count > 0)
                    {
                        ((RadioButton)grdTemplates.Rows[0].FindControl("RowSelector")).Checked = true;
                        ChangeOnRadioClick(grdTemplates.Rows[0]);

                    }
                    else
                    {
                        TemplateMemoDiv.InnerHtml = "";
                    }
                }
                else
                {
                    grdTemplates.Columns[1].Visible = false;
                    grdTemplates.Columns[2].Visible = false;
                    grdTemplates.Columns[3].Visible = true;
                    grdTemplates.Columns[4].Visible = true;
                }
           
        }
         protected void Grid_radioOncheckedChanged(object sender, EventArgs e)
        {
            
            RadioButton rdButton = (RadioButton)sender;
            GridViewRow row = (GridViewRow)rdButton.NamingContainer;
            row.BackColor = System.Drawing.Color.FromName("#DDDDDD");
            ChangeOnRadioClick(row);
            
        }
        public void ChangeOnRadioClick(GridViewRow row)
        {
            TemplateId.Value=((Label)row.FindControl("lblTemplateId")).Text;
            TemplateName.Value = ((Label)row.FindControl("lblTemplateName")).Text;
            TemplateMemoDiv.InnerHtml = ((Label)row.FindControl("lblTemplateMemo")).Text;
        }
        protected void OnSort(object sender, GridViewSortEventArgs e)
        {
            if (String.Empty != m_strSortExp)
            {
                if (String.Compare(e.SortExpression, m_strSortExp, true) == 0)
                {
                    m_SortDirection =
                        (m_SortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
                }
            }
         
            m_strSortExp = e.SortExpression;
            
            if (m_SortDirection == SortDirection.Ascending)
                SortGridView(m_strSortExp.ToString(), " ASC");
            else
                SortGridView(m_strSortExp.ToString(), " DESC");
            
        }
        private void SortGridView(string sortExpression, string direction)
        {
            System.Collections.Generic.List<ProgressNoteTemplates> arrProgressList = null;
            if (ViewState["Datagrid_Arraylist"].ToString() != String.Empty)
            {
                arrProgressList = (System.Collections.Generic.List<ProgressNoteTemplates>)ViewState["Datagrid_Arraylist"];
            }
            else
            {
                arrProgressList = new System.Collections.Generic.List<ProgressNoteTemplates>();
            }
            
            DataTable dt = new DataTable();
            dt.Columns.Add("TemplateId");
            dt.Columns["TemplateId"].DataType = System.Type.GetType("System.Int32");
            dt.Columns.Add("TemplateName");
            dt.Columns.Add("TemplateMemo");
            dt.Columns.Add("AddedBy");
            dt.Columns.Add("UpdatedBy");
            dt.Columns.Add("DateAdded");
            dt.Columns.Add("DateUpdated");
            foreach (ProgressNoteTemplates prgNote in arrProgressList)
            {
                DataRow dr = dt.NewRow();
                dr["TemplateId"] = prgNote.TemplateId;
                dr["TemplateName"] = prgNote.TemplateName;
                dr["TemplateMemo"] = prgNote.TemplateMemo;
                dr["AddedBy"] = prgNote.AddedBy;
                dr["UpdatedBy"] = prgNote.UpdatedBy;
                dr["DateAdded"] = prgNote.DateAdded;
                dr["DateUpdated"] = prgNote.DateUpdated;
                dt.Rows.Add(dr);

            }
            
            dt.DefaultView.Sort = sortExpression + direction;

            grdTemplates.DataSource = dt;
            grdTemplates.DataBind();
            SetGrdRadiosOnClick();
            //first radio click on load and also the event 
            if (grdTemplates.Rows.Count > 0)
            {
                ((RadioButton)grdTemplates.Rows[0].FindControl("RowSelector")).Checked = true;
                ChangeOnRadioClick(grdTemplates.Rows[0]);

            }
        }

        protected void OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (String.Empty != m_strSortExp)
                {
                    AddSortImage(e.Row);
                }
            }
        }
        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(Convert.ToBoolean(ViewState["LookUp"]))
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    TableCell tcGridCell;
                    LinkButton lbLinkColumn;
                    RadioButton rbTemplateId;

                    tcGridCell = e.Row.Cells[3]; //LookUpTemplateName
                    lbLinkColumn = (LinkButton)tcGridCell.Controls[1];

                    tcGridCell = e.Row.Cells[0]; //TemplateId
                    rbTemplateId = (RadioButton)tcGridCell.Controls[1];

                    // href is assigned the value # to stop the postback on click.
                    lbLinkColumn.Attributes.Add("href", "#");

                    lbLinkColumn.Attributes.Add("onclick", "setTemplateName('" + rbTemplateId.Attributes["Value"].ToString() + "','" + escape(lbLinkColumn.Text) + "');"); //Added escape() for Mits 18897
                }
            }
        }
        protected void OnDataBound(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(ViewState["LookUp"]))
            {
                grdTemplates.Columns[0].HeaderStyle.CssClass = "hiderowcol";
                grdTemplates.Columns[0].ItemStyle.CssClass = "hiderowcol";
            }
        }

        void AddSortImage(GridViewRow headerRow)
        {
            Int32 iCol = GetSortColumnIndex(m_strSortExp);
            if (-1 == iCol)
            {
                return;
            }
            // Create the sorting image based on the sort direction.
            Image sortImage = new Image();
            if (SortDirection.Ascending == m_SortDirection)
            {
                sortImage.ImageUrl = "../../Images/arrow_up_white.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/arrow_down_white.gif";
                sortImage.AlternateText = "Descending Order";
            }

            // Add the image to the appropriate header cell.
            headerRow.Cells[iCol].Controls.Add(sortImage);
            headerRow.Cells[iCol].ForeColor = System.Drawing.Color.FromName("#cccc33");
        }

        int GetSortColumnIndex(string str)
        {
            int i = -1;
            switch (str)
            {
                case "TemplateName" :
                    i = 1;
                    break;
                case "UpdatedBy":
                    i = 2;
                    break;
            }

            return i;
        }
        public SortDirection m_SortDirection
        {

            get
            {

                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];

            }

            set { ViewState["sortDirection"] = value; }

        }
        public string m_strSortExp
        {

            get
            {
               if (ViewState["sortExpression"] == null)
                    return "";
                else
                    return ViewState["sortExpression"].ToString();

            }

            set { ViewState["sortExpression"] = value; }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //pmittal5 Mits 21514
            //Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotes.aspx?EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + SysFormName.Value + "&EventNumber=" + eventnumber.Value + "&ClaimantInfo=" + claimant.Value + "&formsubtitle=" + this.hdHeader.Text);
            //williams:neha goel:MITS 21704---added policy parameters
            Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotes.aspx?EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + SysFormName.Value + "&EventNumber=" + eventnumber.Value + "&ClaimantInfo=" + claimant.Value
                          + "&formsubtitle=" + AppHelper.EncodeSpecialSigns(this.hdHeader.Text) + "&LOB=" + LOB.Value + "&PolicyId=" + policyid.Value + "&PolicyName=" + policyname.Value + "&PolicyNumber=" + policynumber.Value);
            //hlv MITS 28588 11/12/12 asharma326 MITS 34294
        }
        protected string escape(string sValue)
        {

            sValue = sValue.Replace("'", "\\'");
            return sValue;

        }
       
        /// <summary>
        /// Author: rsushilaggar
        /// Date : 06/18/2010
        /// MITS : 21119
        /// Comments: Enabled/Disabled Controls based upon settings in 
        /// </summary>
        void ApplySecurityPermissions()//Parijat :19934-security Permissions
        {
            if (Convert.ToBoolean(ViewState["bCreatePermission"]))//Parijat :19934-security Permissions
                btnCreateTemplate.Enabled = true;
            else
                btnCreateTemplate.Enabled = false;

            if (Convert.ToBoolean(ViewState["bDeletePermission"]))//Parijat :19934-security Permissions
                btnDeleteTemplate.Enabled = true;
            else
                btnDeleteTemplate.Enabled = false;


            if (Convert.ToBoolean(ViewState["bEditPermission"]) )//Parijat :19934-security Permissions
                btnEditTemplate.Enabled = true;
            else
                btnEditTemplate.Enabled = false;


        }
    }
}
