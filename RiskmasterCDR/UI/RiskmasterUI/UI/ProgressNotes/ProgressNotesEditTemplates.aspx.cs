﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.Common;
using System.Xml;
using System.Xml.XPath;
using Infragistics.WebUI.WebHtmlEditor;
//using Riskmaster.UI.ProgressNoteService;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;

namespace Riskmaster.UI.ProgressNotes
{
    public partial class ProgressNotesEditTemplates : NonFDMBasePageCWS
    {
        XmlDocument oFDMPageDom = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (AppHelper.GetValue("formsubtitle") != String.Empty)
            {
                this.hdHeader.Text = AppHelper.GetValue("formsubtitle");
                //this.hdHeader.Text = AppHelper.HTMLCustomEncode(AppHelper.GetValue("formsubtitle")); //hlv MITS 28588 11/12/12
                //msampathkuma jira-11851
                if (this.hdHeader.Text.Contains("^@"))
                {
                    this.hdHeader.Text = AppHelper.HTMLCustomEncode(AppHelper.GetValue("formsubtitle"));
                }
                else
                {
                    this.hdHeader.Text = AppHelper.HTMLCustomDecode(AppHelper.GetValue("formsubtitle"));//msampathkuma jira-11170
                }
            }
            oFDMPageDom = new XmlDocument();
            string sReturn = "";
            this.WebSpellChecker1.UserDictionaryFile = Server.MapPath("../../App_Data/C1SP_AE.dct");
            this.WebSpellChecker1.SpellOptions.IncludeUserDictionaryInSuggestions = true;
            this.WebHtmlEditor.ToolbarClick += new Infragistics.WebUI.WebHtmlEditor.ToolbarClickEventHandler(WebHtmlEditor_ToolbarClick);
            //Support for adding images in notes..TO DO: discuss deployment and make changes accordingly
            //this.WebHtmlEditor.UploadedFilesDirectory = "../../Images/ProgressNotesImages";
            this.WebSpellChecker1.DialogOptions.ShowFinishedMessage = true;
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.SpellCheck))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.SpellCheck);
            }
            this.WebHtmlEditor.AddToolbarItem(ToolbarItemType.SpellCheck);

            //Changed for MITS 22272 by Gagan : start
            //Hiding Unnecessary buttons in Enhanced Notes Editor
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.InsertImage))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.InsertImage);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.InsertFlash))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.InsertFlash);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.InsertWindowsMedia))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.InsertWindowsMedia);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.InsertBookmark))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.InsertBookmark);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.Open))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.Open);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.Help))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.Help);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.CleanWord))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.CleanWord);
            }
            //Changed for MITS 19670 by Gagan : end

            //Amandeep MultiLingual Changes
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ProgressNotesEditTemplates.aspx"), "ProgressNotesEditTemplatesValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "ProgressNotesEditTemplatesValidationsScripts", sValidationResources, true);
            //Amandeep MultiLingual Changes

            if (!Page.IsPostBack)
            {
                //pen testing changes :atavaragiri mits 27832

                // bool bNewRecord = (ViewState["NewRecord"] != null) ? (Convert.ToBoolean(ViewState["NewRecord"].ToString())) : (Convert.ToBoolean(AppHelper.GetQueryStringValue("NewRecord")));
                bool bNewRecord = (ViewState["NewRecord"] != null) ? Convert.ToBoolean((AppHelper.HTMLCustomEncode(ViewState["NewRecord"].ToString()))) : (Convert.ToBoolean(AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("NewRecord"))));
                
                
                //TemplateName.Text = AppHelper.GetQueryStringValue("TemplateName").Replace("~~*~~", "'"); //Changed for Mits 18897
                //TemplateId.Value = AppHelper.GetQueryStringValue("TemplateId");
                //claimant.Value = AppHelper.GetQueryStringValue("ClaimantInfo");
                //SysFormName.Value = AppHelper.GetQueryStringValue("FormName");
                //eventid.Value = AppHelper.GetQueryStringValue("EventID");
                //claimid.Value = AppHelper.GetQueryStringValue("ClaimID");
                //eventnumber.Value = AppHelper.GetQueryStringValue("EventNumber");
                ////williams:neha goel:MITS 21704:start
                //policyid.Value = AppHelper.GetQueryStringValue("PolicyId");s
                //policyname.Value =AppHelper.GetQueryStringValue("PolicyName");
                //policynumber.Value =AppHelper.GetQueryStringValue("PolicyNumber");
                
                 
                if(!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("TemplateName")))
                TemplateName.Text =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("TemplateName").Replace("~~*~~", "'")); //Changed for Mits 18897
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("TemplateId")))
                 TemplateId.Value =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("TemplateId"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("ClaimantInfo")))
                claimant.Value =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimantInfo"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("FormName")))
                SysFormName.Value =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("FormName"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("EventID")))
                eventid.Value =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("EventID"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("ClaimID")))
                claimid.Value =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimID"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("EventNumber")))
                eventnumber.Value =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("EventNumber"));
                //williams:neha goel:MITS 21704:start
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicyId")))
                policyid.Value =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolicyId"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicyName")))
                policyname.Value =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolicyName"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicyNumber")))
                policynumber.Value =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolicyNumber"));
                //END :pen testing changes :atavaragiri mits 27832
                //williams:neha goel:MITS 21704:end
                LOB.Value = AppHelper.GetQueryStringValue("LOB"); //pmittal5 Mits 21514
              
                hdNewRecord.Value = bNewRecord.ToString();
                if (!bNewRecord)
                {
                    ProgressNoteBusinessHelper pn = null;
                    ProgressNoteTemplates objTemplates = new ProgressNoteTemplates();
                    try
                    {
                        pn = new ProgressNoteBusinessHelper();
                        objTemplates.TemplateId = TemplateId.Value;
                        //pmittal5 Mits 21514 - Update ClaimId, EventId, LOB to check different Enhanced Notes permissions
                        bool bResult = false;
                        int iOutResult = 0;
                        bResult = Int32.TryParse(eventid.Value,out iOutResult);
                        objTemplates.EventID = iOutResult;
                        bResult = Int32.TryParse(claimid.Value, out iOutResult);
                        objTemplates.ClaimID = iOutResult;
                        objTemplates.LOB = LOB.Value;
                        //End - pmittal5
                        objTemplates = pn.GetTemplateDetails(objTemplates);
                        if (objTemplates.TemplateMemo != null)
                        {
                             WebHtmlEditor.Text = objTemplates.TemplateMemo;
                            
                        }

                    }
                    catch (FaultException<RMException> ee)
                    {
                        ErrorHelper.logErrors(ee);
                        ErrorControl.errorDom = ee.Detail.Errors;

                    }
                    catch (Exception ee)
                    {
                        ErrorHelper.logErrors(ee);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ee, BusinessAdaptorErrorType.SystemError);
                        ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                    }

                }
            }

        }
        /// <summary>
        /// Handles the click events of toolbar button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void WebHtmlEditor_ToolbarClick(object sender, ToolbarEventArgs e)
        {
            string sReturn = "";
            ProgressNoteBusinessHelper pn = null;
            ProgressNoteTemplates objTemplates = new ProgressNoteTemplates();
            try
            {

                if (Convert.ToBoolean(hdNewRecord.Value))
                {
                    objTemplates.NewRecord = hdNewRecord.Value;
                    objTemplates.TemplateId = "0";
                    
                }
                else
                {
                    objTemplates.NewRecord = hdNewRecord.Value;
                    objTemplates.TemplateId = TemplateId.Value;
                    
                }
                objTemplates.TemplateName = TemplateName.Text;
                objTemplates.TemplateMemo = WebHtmlEditor.Text;

                if (e.Item.ToString() == ToolbarItemType.Save.ToString())
                {
                    ErrorControl.Text = "";
                    pn = new ProgressNoteBusinessHelper();
                    objTemplates = pn.SaveTemplates(objTemplates);
                    TemplateId.Value = objTemplates.TemplateId;
                    ViewState["NewRecord"] = "false";
                    hdNewRecord.Value = "false";

                    //Amandeep Multi Lingual Changes
                    //string str = "<script>alert('Your template has been saved');</script>"; //Changed Alert Message for Mits 18838

                    string sMessage = RMXResourceProvider.GetSpecificObject("ValidTemplateSaved",RMXResourceProvider.PageId("ProgressNotesEditTemplates.aspx"), ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                    //string str = "<script>alert('" + sMessage + "');</script>";
                    string str = String.Format("<script>alert('{0}');</script>", sMessage);
                    //Amandeep Multi Lingual Changes

                    RegisterClientScriptBlock("", str);
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        protected void btnCancelTemplates_Click(object sender, EventArgs e)
        {
            //pmittal5 Mits 21514
            //Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotesTemplates.aspx?LookUp=false&EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + SysFormName.Value + "&EventNumber=" + eventnumber.Value + "&ClaimantInfo=" + claimant.Value + "&formsubtitle=" + this.hdHeader.Text);
            //williams:neha goel:MITS 21704--added policy parameters
            Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotesTemplates.aspx?LookUp=false&EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + SysFormName.Value + "&EventNumber=" + eventnumber.Value + "&ClaimantInfo=" + claimant.Value
                          + "&formsubtitle=" + AppHelper.EncodeSpecialSigns(this.hdHeader.Text) + "&LOB=" + LOB.Value + "&PolicyId=" + policyid.Value + "&PolicyName=" + policyname.Value + "&PolicyNumber=" + policynumber.Value);
        }

    }

}




