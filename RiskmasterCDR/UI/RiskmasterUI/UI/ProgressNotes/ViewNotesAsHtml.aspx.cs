﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.ProgressNoteService;
using Riskmaster.ServiceHelpers;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using Riskmaster.RMXResourceManager;
using Riskmaster.Common;
using Riskmaster.Models;

namespace Riskmaster.UI.ProgressNotes
{
    public partial class ViewNotesAsHtml : NonFdmFormBase
    {
        //Start by SHivendu for MITS 18098
        private const string sPageLoad = "PageLoad";
        private const string sFirst = "First";
        private const string sNext = "Next";
        private const string sPrev = "Prev";
        private const string sLast = "Last";
        private const string sNoSort = "NoSort";
        //williams:-neha goel::MITS 21704
        private StringBuilder g_sbTemp = new StringBuilder(); 
        //End by SHivendu for MITS 18098
        private bool bEditPermission = true;//MGaba2:MITS 22115

        protected void Page_Load(object sender, EventArgs e)
        {
            //srajindersin MITS 28264 05/01/2012
            ClientScript.GetPostBackEventReference(this, string.Empty);
            base.Page_Load(sender, e);

            //Amandeep MultiLingual Changes --start
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ViewNotesAsHtml.aspx"), "ViewNotesAsHtmlValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "ViewNotesAsHtmlValidationsScripts", sValidationResources, true);
            //Amandeep MultiLingual Changes --end

            if (!IsPostBack)
            {
                OnPageLoad(sPageLoad);//Added by Shivendu fo MITS 18098
                
            }
            
        }
//15010
        /// <summary>
        /// Function will be called on Page_Load and postbacks on pagination
        /// </summary>
        public void OnPageLoad(string sCallee)
        {
            string sPrintOption;
            int iClaimprogressnoteid;
            int iClaimNoteTypeId;

            iClaimprogressnoteid = 0;
            iClaimNoteTypeId = 0;
            sPrintOption = string.Empty;

            StringBuilder sPageDetails = new StringBuilder();//Added by Shivendu for MITS 18098
            ProgressNotesType objProgressNote = new ProgressNotesType();
            //14972 :no constructor generated after removing model references and genrating the same thru reference.svcmap
            objProgressNote.objFilter = new Filter();

            if (Request.QueryString["EventID"] != null)
            {     // pen testing changes:atavaragiri mits 27832
                //objProgressNote.EventID = Convert.ToInt32(Request.QueryString["EventID"].ToString());
                objProgressNote.EventID = Convert.ToInt32(AppHelper.HTMLCustomEncode(Request.QueryString["EventID"].ToString()));
                // END:pen  testing
            }
            if (Request.QueryString["ClaimID"] != null)
            {     // pen testing changes:atavaragiri mits 27832
                //objProgressNote.ClaimID = Convert.ToInt32(Request.QueryString["ClaimID"].ToString());
                objProgressNote.ClaimID = Convert.ToInt32(AppHelper.HTMLCustomEncode(Request.QueryString["ClaimID"].ToString()));
                //END: pen testing
            }
			 if (Request.QueryString["Claimant"] != null)
            {
                objProgressNote.Claimant = Request.QueryString["ClaimantName"].ToString();
            }

            //added by swati for MITS # 35530 Gap 15 WWIG
            if (Request.QueryString["ClaimantId"] != null)
            {
                objProgressNote.ClaimantId = Convert.ToInt32(Request.QueryString["ClaimantId"].ToString());
            }
            //change end here by swati
            //mbahl3 Mit 25457 adding claimant email

            //rsolanki2 : start mits 21294 : adding multiple pages support to the "View all notes screen"

            //rsolanki2: mits 25153 start : view all notes not shoing notes froma dvanced search 
            bool bFilter =  false;
            Boolean.TryParse(Request.QueryString["activatefilter"], out bFilter);
            if (bFilter)
            {
                objProgressNote.ActivateFilter = true;
                //objProgressNote.objFilter.ClaimIDList = Request.QueryString["claimidlist"].ToString();
                //Aman MITS 27290--Start
                //objProgressNote.objFilter.NoteTypeList = Request.QueryString["notetypelist"].ToString().Trim();  
                //objProgressNote.objFilter.EnteredByList = Request.QueryString["enteredbylist"].ToString().Trim(); 
                //objProgressNote.objFilter.ActivityFromDate = Request.QueryString["activityfromdate"].ToString().Trim();
                //objProgressNote.objFilter.ActivityToDate = Request.QueryString["activitytodate"].ToString().Trim();
                //objProgressNote.objFilter.UserTypeList = Request.QueryString["usertypelist"].ToString().Trim();
                //objProgressNote.objFilter.SortBy = Request.QueryString["sortby"].ToString().Trim();
                //objProgressNote.objFilter.NotesTextContains = Request.QueryString["notestextcontains"].ToString().Trim();
                objProgressNote.objFilter.ClaimIDList = AppHelper.GetQueryStringValue("claimidlist");
                objProgressNote.objFilter.NoteTypeList = AppHelper.GetQueryStringValue("notetypelist");
                objProgressNote.objFilter.EnteredByList = AppHelper.GetQueryStringValue("enteredbylist");
                objProgressNote.objFilter.ActivityFromDate = AppHelper.GetQueryStringValue("activityfromdate");
                objProgressNote.objFilter.ActivityToDate = AppHelper.GetQueryStringValue("activitytodate");
                objProgressNote.objFilter.UserTypeList = AppHelper.GetQueryStringValue("usertypelist");
                objProgressNote.objFilter.SortBy = AppHelper.GetQueryStringValue("sortby");
                // Added by akaushik5 for MITS 30789 Starts
                objProgressNote.objFilter.OrderBy = AppHelper.GetQueryStringValue("orderby");
                // Added by akaushik5 for MITS 30789 Ends
                objProgressNote.objFilter.NotesTextContains = AppHelper.GetQueryStringValue("notestextcontains");

                //Aman MITS 27290--End
            }
            //rsolanki2: mits 25153 END : view all notes not shoing notes froma dvanced search 

            objProgressNote.PageNumber = 1;
            if (Request.QueryString["printallpages"] != null)
            {
                // pen testing changes:atavaragiri mits 27832
                //objProgressNote.PrintMultiplePages = Convert.ToBoolean(Request.QueryString["printallpages"].ToString());
                objProgressNote.PrintMultiplePages = Convert.ToBoolean(AppHelper.HTMLCustomEncode(Request.QueryString["printallpages"].ToString()));
                // END:pen testing
              
                //if (objProgressNote.PrintMultiplePages)
                //{
                //    lblPageDetails.Visible = false;
                //    //Amandeep ML Enh
                //    lblPageNumber.Visible = false;
                //    lblPageOf.Visible = false;
                //    lblNumberOfPages.Visible = false;
                //    //Amandeep ML Enh
                //    lnkFirst.Visible = false;
                //    lnkNext.Visible = false;
                //    lnkPrev.Visible = false;
                //    lnkLast.Visible = false;
                //     // atavaragiri mits 28264 //
                //    txtPageNumber.Visible = false;
                //    // atavaragiri mits 28264 //
                //}
                //else
                if (!objProgressNote.PrintMultiplePages)
                {
                    lblPageDetails.Visible = true;
                    //Amandeep ML Enh
                    lblPageNumber.Visible = true;
                    lblPageOf.Visible = true; 
                    lblNumberOfPages.Visible = true;
                    //Amandeep ML Enh
                    lnkFirst.Visible = true;
                    lnkNext.Visible = true;
                    lnkPrev.Visible = true;
                    lnkLast.Visible = true;
                    // atavaragiri mits 28264 //
                    txtPageNumber.Visible = true;
                    // atavaragiri mits 28264 //

                    if (Request.QueryString["pagenum"] != null)
                    {
                        // pen testing changes:atavaragiri mits 27832
                        // objProgressNote.PageNumber = Convert.ToInt32(Request.QueryString["pagenum"].ToString());
                        objProgressNote.PageNumber = Convert.ToInt32(AppHelper.HTMLCustomEncode(Request.QueryString["pagenum"].ToString()));
                        // atavaragiri mits 28264 //
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri mits 28264 //
                        //END: pen testing
                    }
                }
                //objProgressNote.
                //tanwar2 - Print Html notes - end
            }
            
            //rsolanki2 : End mits 21294 : adding multiple pages support to the "View all notes screen"

            //pmittal5 Mits 21514
            if (Request.QueryString["LOB"] != null)
            {//  pen testing changes:atavaragiri mits 27832
                //objProgressNote.LOB = Request.QueryString["LOB"].ToString();
                objProgressNote.LOB = AppHelper.HTMLCustomEncode(Request.QueryString["LOB"].ToString());
                // end:pen testing

            }
            //End - pmittal5
            //williams-neha goel:MITS 21704--start:added for policy enhanced notes
            if (!string.IsNullOrEmpty(Request.QueryString["PolicyID"]))
            { 
                      // pen testing changes atavaragiri :mits 27832
                //objProgressNote.PolicyID = Convert.ToInt32(Request.QueryString["PolicyID"].ToString());
                objProgressNote.PolicyID = Convert.ToInt32(AppHelper.HTMLCustomEncode(Request.QueryString["PolicyID"].ToString()));
                // end :pen testing changes
              
            }
            
                
            if (Request.QueryString["PolicyName"] != null)
            {
                objProgressNote.PolicyName = Request.QueryString["PolicyName"].ToString();
            }
            if (Request.QueryString["PolicyNumber"] != null)
            {
                objProgressNote.PolicyNumber = Request.QueryString["PolicyNumber"].ToString();
            }
            if (Request.QueryString["CodeId"] != null)
            {   // pen testing changes:atavaragiri mits 27832
               // objProgressNote.CodeId = Request.QueryString["CodeId"].ToString();
                objProgressNote.CodeId = AppHelper.HTMLCustomEncode(Request.QueryString["CodeId"].ToString());
                // ENd:pen testing
            }
            //williams-neha goel:MITS 21704--end

            //Start by Shivendu for MITS 18485
            if (Request.QueryString["FreezeText"] != null)
            {
                if (Page.FindControl("freezetext") != null)
                {

                    ((TextBox)Page.FindControl("freezetext")).Text = Request.QueryString["FreezeText"].ToString();

                }

            }
            if (Request.QueryString["ShowDateStamp"] != null)
            {
                if (Page.FindControl("showDateStamp") != null)
                {

                    ((TextBox)Page.FindControl("showDateStamp")).Text = Request.QueryString["ShowDateStamp"].ToString();

                }

            }
            if (Request.QueryString["UserName"] != null)
            {
                if (Page.FindControl("username") != null)
                {

                    ((TextBox)Page.FindControl("username")).Text = Request.QueryString["UserName"].ToString();

                }

            }
            if (Request.QueryString["hdSortColumn"] != null) //Parijat: MITs 20194
            {
                hdSortColumn.Value = Request.QueryString["hdSortColumn"].ToString();
            }
            if (Request.QueryString["hdDirection"] != null) //Parijat: MITs 20194
            {
                hdDirection.Value = Request.QueryString["hdDirection"].ToString();
                //if (hdDirection.Value != "")
                //{
                //    hdDirection.Value = (hdDirection.Value.ToUpper().Trim() == "DESC") ? " ASC" : " DESC";
                //}
                
            }
            //End by Shivendu for MITS 18485
            //if (Request.QueryString["FormName"] != null)
            //{
            //    objProgressNote.FormName = Request.QueryString["FormName"].ToString();
            //}
            //tmalhotra2 - start
            if (Request.QueryString["printOption"] != null)
            {
                sPrintOption = Request.QueryString["printOption"].ToString();
            }

            if (Request.QueryString["claimprogressnoteid"] != null)
            {
                iClaimprogressnoteid = Conversion.ConvertObjToInt(Request.QueryString["claimprogressnoteid"]);
            }

            if (Request.QueryString["claimNoteTypeid"] != null)
            {
                iClaimNoteTypeId = Conversion.ConvertObjToInt(Request.QueryString["claimNoteTypeid"]);
            }

            //tmalhotra2 - end
            // mkaran2 - MITS 27038 - start 
            if (!string.IsNullOrEmpty(Request.QueryString["viewNotesOption"]))
            {
                objProgressNote.ViewNotesOption = Request.QueryString["viewNotesOption"].ToString();                
            }
            if (!string.IsNullOrEmpty(Request.QueryString["notetypelist"]))
            {
                objProgressNote.ViewNotesTypeList = Request.QueryString["notetypelist"].ToString();
            }            
            // mkaran2 - MITS 27038 - end

            ProgressNoteBusinessHelper pn = null;

            try
            {
                pn = new ProgressNoteBusinessHelper();
                //Parijat: MITs 20194
                if (!string.IsNullOrEmpty(hdSortColumn.Value) && !string.IsNullOrEmpty(hdDirection.Value))
                {
                    objProgressNote.SortColumn = hdSortColumn.Value;
                    objProgressNote.Direction = hdDirection.Value;
                }
                else
                {
                    objProgressNote.SortColumn = sNoSort;
                }
                //Parijat: End MITS 20194
                //Start by Shivendu for MITS 18098
                switch (sCallee)
                {
                    case sPageLoad:
                        //rsolanki2: we may have the pageload passed in as querystring
                        //objProgressNote.PageNumber = 1;
                        //objProgressNote.SortColumn = sNoSort; //Parijat: MITs 20194
                        break;
                    case sFirst:
                        objProgressNote.PageNumber = 1;
                        // atavaragiri mits 28264 //
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri mits 28264 //
                        //objProgressNote.SortColumn = sNoSort; //Parijat: MITs 20194
                        break;
                    case sNext:
                        if (Convert.ToInt32(hdPageNumber.Value) < Convert.ToInt32(hdTotalNumberOfPages.Value))
                        {
                            hdPageNumber.Value = Convert.ToString(Convert.ToInt32(hdPageNumber.Value) + 1);
                        }
                        objProgressNote.PageNumber = Convert.ToInt32(hdPageNumber.Value);
                        // atavaragiri mits 28264 //
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri mits 28264 //
                        //objProgressNote.SortColumn = sNoSort; //Parijat: MITs 20194
                        break;
                    case sPrev:
                        if (Convert.ToInt32(hdPageNumber.Value) > 1)
                        {
                            hdPageNumber.Value = Convert.ToString(Convert.ToInt32(hdPageNumber.Value) - 1);
                        }
                        objProgressNote.PageNumber = Convert.ToInt32(hdPageNumber.Value);
                        // atavaragiri mits 28264 //
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri mits 28264 //
                        //objProgressNote.SortColumn = sNoSort; //Parijat: MITs 20194
                        break;
                    case sLast:
                        hdPageNumber.Value = hdTotalNumberOfPages.Value;
                        objProgressNote.PageNumber = Convert.ToInt32(hdPageNumber.Value);
                        // atavaragiri mits 28264 //
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri mits 28264 //
                        //objProgressNote.SortColumn = sNoSort; //Parijat: MITs 20194
                        break;
                    default:
                        break;

                }
                switch (sPrintOption)
                {
                    case "1":
                        objProgressNote.ClaimProgressNoteId = iClaimprogressnoteid.ToString();
                        objProgressNote.NoteTypeCodeId = "";
                        break;
                    case "2":
                        objProgressNote.NoteTypeCodeId = iClaimNoteTypeId.ToString();
                        objProgressNote.ClaimProgressNoteId = "";
                        break;
                    default:
                        objProgressNote.ClaimProgressNoteId = "";
                        objProgressNote.NoteTypeCodeId = "";
                        break;
                }
                //End by Shivendu for MITS 18098
                objProgressNote = pn.Load(objProgressNote);
                if (objProgressNote != null)
                {
                    claimid.Value = objProgressNote.ClaimID.ToString();
                    eventid.Value = objProgressNote.EventID.ToString();
                    //williams-neha goel:MITS 21704--start:added for policy enhanced notes
                    policyid.Value = objProgressNote.PolicyID.ToString();
                    policynumber.Value = objProgressNote.PolicyNumber;
                    notesparentcodeid.Value = objProgressNote.CodeId;
                    sysformname.Value = Request.QueryString["FormName"].ToString();
					//added by swati for MITS # 35530 Gap 15 WWIG
                    claimantid.Value = objProgressNote.ClaimantId.ToString();
                    //end here by swati
                    //tanwar2 - Print HTML Notes - start
                    //code moved from top
                    if (objProgressNote.PrintMultiplePages)
                    {
                        //srajindersin - MITS 34718 - 12/30/2013
                        lblPageDetails.Visible = false;
                        lblPageNumber.Visible = false;
                        lblPageOf.Visible = false;
                        lblNumberOfPages.Visible = false;
                        txtPageNumber.Visible = false;
                        
                        lnkFirst.Visible = false;
                        lnkNext.Visible = false;
                        lnkPrev.Visible = false;
                        lnkLast.Visible = false;
                    }
                    else
                    {
                        //srajindersin - MITS 34718 - 12/30/2013
                        lblPageDetails.Visible = true;
                        lblPageNumber.Visible = true;
                        lblPageOf.Visible = true;
                        lblNumberOfPages.Visible = true;
                        txtPageNumber.Visible = true;

                        lnkFirst.Visible = true;
                        lnkNext.Visible = true;
                        lnkPrev.Visible = true;
                        lnkLast.Visible = true;
                    }
                    //tanwar2 - Print HTML Notes - end
                }
                //williams:MITS 21704-neha goel--end
                if (objProgressNote.objProgressNoteList.Count > 0)
                {
                     //williams:MITS 21704--neha goel
                    //policyname.Value = objProgressNote.objProgressNoteList[0].PolicyName;
                    //notesparentcodeid.Value = objProgressNote.objProgressNoteList[0].CodeId;
                    if (Convert.ToInt32(policyid.Value) != 0)
                    {
                        policyname.Value = objProgressNote.objProgressNoteList[0].PolicyName;
                        lblPolicyName.Text = objProgressNote.objProgressNoteList[0].PolicyName;
                        if (!string.IsNullOrEmpty(objProgressNote.PolicyNumber))
                        {
                            lblPolicyNumber.Text = objProgressNote.PolicyNumber;
                        }
                        HtmlContainerControl oDivControl = (HtmlContainerControl)this.FindControl("eventclaim");
                        if (oDivControl != null)
                        {
                            oDivControl.Visible = false;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < objProgressNote.objProgressNoteList.Count; i++)
                        {
                            if ((!string.IsNullOrEmpty(objProgressNote.objProgressNoteList[i].ClaimNumber)) || (!string.IsNullOrEmpty(objProgressNote.objProgressNoteList[i].EventNumber)))
                            {
                                lblClaimNumber.Text = objProgressNote.objProgressNoteList[i].ClaimNumber;                       
                                lblEventNumber.Text = objProgressNote.objProgressNoteList[i].EventNumber;                                
							    lblClaimantName.Text = objProgressNote.Claimant;  //mbahl3 Mit:25495   Added claimant field
                                // ijha: MITS 27999
                                //Amandeep MulitLingual --start
                                //lblEventNo.Text = "Event Number: ";
                                lblEventNo.Text = RMXResourceProvider.GetSpecificObject("lblEventNumberResrc", RMXResourceProvider.PageId("ViewNotesAsHtml.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                                if (lblClaimNumber.Text == "")
                                {
                                    lblClaimNo.Visible = false;
                                }
                                else
                                {
                                    //lblClaimNo.Text = "**** Claim Number: ";
                                    lblClaimNo.Text = RMXResourceProvider.GetSpecificObject("lblClaimNumberResrc", RMXResourceProvider.PageId("ViewNotesAsHtml.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                                }
                                if (lblClaimantName.Text == "")
                                {
                                    lblClName.Visible = false;
                                }
                                else
                                {
                                    //lblClName.Text = "****Claimant: ";
                                    lblClName.Text =  RMXResourceProvider.GetSpecificObject("lblClaimantResrc", RMXResourceProvider.PageId("ViewNotesAsHtml.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                                }
                                //ijha :end
                                break;
                            }
                        }
                        HtmlContainerControl oDivControl = (HtmlContainerControl)this.FindControl("policy");
                        if (oDivControl != null)
                        {
                            oDivControl.Visible = false;
                        }
                        if ((objProgressNote.EventID) != 0 && (objProgressNote.ClaimID) == 0)
                        {
                            lblPIName.Text = objProgressNote.PIName;//skhare7 27285

                        }
                        else
                        {
                            HtmlContainerControl oDivControlPI = (HtmlContainerControl)this.FindControl("piname");
                            if (oDivControlPI != null)
                            {
                                oDivControlPI.Visible = false;
                            }
                        }
                    }
                }
                else if (objProgressNote.objProgressNoteList.Count == 0)   // mkaran2 - MITS 27038 - start 
                {                   
                    HtmlContainerControl oDivEventControl = (HtmlContainerControl)this.FindControl("eventclaim");
                    if (oDivEventControl != null)
                    {
                        oDivEventControl.Visible = false;
                    }
                   
                    HtmlContainerControl oDivControlPI = (HtmlContainerControl)this.FindControl("piname");
                    if (oDivControlPI != null)
                    {
                        oDivControlPI.Visible = false;
                    }

                    HtmlContainerControl oDivPolicyControl = (HtmlContainerControl)this.FindControl("policy");
                    if (oDivPolicyControl != null)
                    {
                        oDivPolicyControl.Visible = false;
                    }
                    
                    lblNoNotesToShow.Text = RMXResourceProvider.GetSpecificObject("lblNoNotesToShow", RMXResourceProvider.PageId("ViewNotesAsHtml.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                }
                                                            // mkaran2 - MITS 27038 - end
                //lblEventNumber.Text = objProgressNote.EventNumber;
                bEditPermission = objProgressNote.bEditPermission;  //MGaba2:MITS 22115

                rptNotes.DataSource = objProgressNote.objProgressNoteList;
                rptNotes.DataBind();
                //Start by Shivendu for MITS 18098
                //srajindersin - MITS 34718 - 12/30/2013
                hdTotalNumberOfPages.Value = objProgressNote.TotalNumberOfPages.ToString() == "0" ? "1" : objProgressNote.TotalNumberOfPages.ToString();
                hdPageNumber.Value = objProgressNote.PageNumber.ToString();
                // atavaragiri mits 28264 //
                txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                // atavaragiri mits 28264 //
                sPageDetails.Append("Page");
                sPageDetails.Append(hdPageNumber.Value);
                sPageDetails.Append(" Of ");
                sPageDetails.Append(hdTotalNumberOfPages.Value);

                //Amandeep Multi Lingual --start
                //lblPageDetails.Text = sPageDetails.ToString();
                lblPageDetails.Text = RMXResourceProvider.GetSpecificObject("lblPageResrc", RMXResourceProvider.PageId("ViewNotesAsHtml.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                lblPageNumber.Text = hdPageNumber.Value;
                lblPageOf.Text = RMXResourceProvider.GetSpecificObject("lblPageOfResrc", RMXResourceProvider.PageId("ViewNotesAsHtml.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                lblNumberOfPages.Text = hdTotalNumberOfPages.Value;
                //Amandeep Multi Lingual --end

                lnkFirst.Enabled = true;
                lnkNext.Enabled = true;
                lnkPrev.Enabled = true;
                lnkLast.Enabled = true;


                if (string.Compare(hdTotalNumberOfPages.Value, "1") == 0)
                {
                    lnkFirst.Enabled = false;
                    lnkNext.Enabled = false;
                    lnkPrev.Enabled = false;
                    lnkLast.Enabled = false;

                }
                if (string.Compare(hdPageNumber.Value, "1") == 0)
                {
                    lnkFirst.Enabled = false;
                    lnkPrev.Enabled = false;
                }
                if (string.Compare(hdPageNumber.Value, hdTotalNumberOfPages.Value) == 0)
                {
                    lnkLast.Enabled = false;
                    lnkNext.Enabled = false;
                }

                //End by Shivendu for MITS 18098

                //MGaba2:MITS 22127:The user is able to print the Enhanced note without the Print permission in Module access Security                
                if (objProgressNote.bPrintPermission || objProgressNote.bPolicyTrackingPrintPermission)  //Added by Amitosh MITS 27561  
                    btnPrint.Enabled = true;
                else
                    btnPrint.Enabled = false;
                
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        protected void lnkFirst_Click(object sender, EventArgs e)
        {
            OnPageLoad(sFirst);//Added by Shivendu fo MITS 18098
        }
        protected void lnkNext_Click(object sender, EventArgs e)
        {
            OnPageLoad(sNext);//Added by Shivendu fo MITS 18098
        }
        protected void lnkPrev_Click(object sender, EventArgs e)
        {
            OnPageLoad(sPrev);//Added by Shivendu fo MITS 18098
        }
        protected void lnkLast_Click(object sender, EventArgs e)
        {
            OnPageLoad(sLast);//Added by Shivendu fo MITS 18098
        }
        protected void Repeater_OnRowCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument == "notetext")
            {
                LinkButton lnkButton = (LinkButton)e.CommandSource;
                RepeaterItem item = (RepeaterItem)lnkButton.NamingContainer;
                //if condition added by Shivendu to pass the freezetext value:MITS 18485
                if (((TextBox)Page.FindControl("freezetext")) != null && ((TextBox)Page.FindControl("freezetext")).Text != "" && ((TextBox)Page.FindControl("showDateStamp")) != null && ((TextBox)Page.FindControl("showDateStamp")).Text != "" && ((TextBox)Page.FindControl("username")) != null && ((TextBox)Page.FindControl("username")).Text != "")
                {
                    //williams-neha goel:MITS 21704--start: added policy
                    //Response.Redirect("~/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=" + eventid.Value + "&EventNumber=" + lblEventNumber.Text + "&ClaimID=" + claimid.Value + "&SysFormName=" + "" + "&NoteType=" + ((HiddenField)e.Item.FindControl("hdnNoteTypeCode")).Value + "&NoteTypecodeid=" + ((HiddenField)e.Item.FindControl("hdnNoteTypeCodeId")).Value + "&ClaimProgressNoteId=" + ((HiddenField)e.Item.FindControl("hdnClaimProgressNoteId")).Value + "&ActivityDate=" + ((HiddenField)e.Item.FindControl("hdnDateEntered")).Value + "&FreezeText=" + ((TextBox)Page.FindControl("FreezeText")).Text + "&ShowDateStamp=" + ((TextBox)Page.FindControl("showDateStamp")).Text + "&UserName=" + ((TextBox)Page.FindControl("username")).Text + "&CommentsFlag=true&NewRecord=false&ShowBack=true");
                    g_sbTemp.Length = 0;
                    g_sbTemp.Append("~/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=");
                    g_sbTemp.Append(eventid.Value);
                    g_sbTemp.Append("&EventNumber=");
                    g_sbTemp.Append(lblEventNumber.Text);
                    g_sbTemp.Append("&ClaimID=");
                    g_sbTemp.Append(claimid.Value);
                    g_sbTemp.Append("&SysFormName=");
                    g_sbTemp.Append(sysformname.Value);
                    g_sbTemp.Append("&NoteType=");
                    g_sbTemp.Append(((HiddenField)e.Item.FindControl("hdnNoteTypeCode")).Value);
                    g_sbTemp.Append("&NoteTypecodeid=");
                    g_sbTemp.Append(((HiddenField)e.Item.FindControl("hdnNoteTypeCodeId")).Value);
                    g_sbTemp.Append("&ClaimProgressNoteId=");
                    g_sbTemp.Append(((HiddenField)e.Item.FindControl("hdnClaimProgressNoteId")).Value);
                    g_sbTemp.Append("&ActivityDate=");
                    g_sbTemp.Append(((HiddenField)e.Item.FindControl("hdnDateEntered")).Value);
                    g_sbTemp.Append("&FreezeText=");
                    g_sbTemp.Append(((TextBox)Page.FindControl("FreezeText")).Text);
                    g_sbTemp.Append("&ShowDateStamp=");
                    g_sbTemp.Append(((TextBox)Page.FindControl("showDateStamp")).Text);
                    g_sbTemp.Append("&UserName=");
                    g_sbTemp.Append(((TextBox)Page.FindControl("username")).Text);
                    g_sbTemp.Append("&CommentsFlag=true&NewRecord=false&ShowBack=true");
                    g_sbTemp.Append("&PolicyId=");
                    g_sbTemp.Append(policyid.Value);
                    g_sbTemp.Append("&PolicyName=");
                    g_sbTemp.Append(policyname.Value);
                    g_sbTemp.Append("&PolicyNumber=");
                    g_sbTemp.Append(policynumber.Value);
                    g_sbTemp.Append("&CodeId=");
                    g_sbTemp.Append(notesparentcodeid.Value);
                    g_sbTemp.Append("&LOB=");
                    g_sbTemp.Append(Request.QueryString["LOB"].ToString());
                    Response.Redirect(g_sbTemp.ToString(), true);
                    //Response.Redirect("~/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=" + eventid.Value + "&EventNumber=" + lblEventNumber.Text + "&ClaimID=" + claimid.Value + "&SysFormName=" + sysformname.Value + "&NoteType=" + ((HiddenField)e.Item.FindControl("hdnNoteTypeCode")).Value + "&NoteTypecodeid=" + ((HiddenField)e.Item.FindControl("hdnNoteTypeCodeId")).Value + "&ClaimProgressNoteId=" + ((HiddenField)e.Item.FindControl("hdnClaimProgressNoteId")).Value + "&ActivityDate=" + ((HiddenField)e.Item.FindControl("hdnDateEntered")).Value + "&FreezeText=" + ((TextBox)Page.FindControl("FreezeText")).Text + "&ShowDateStamp=" + ((TextBox)Page.FindControl("showDateStamp")).Text + "&UserName=" + ((TextBox)Page.FindControl("username")).Text + "&CommentsFlag=true&NewRecord=false&ShowBack=true" + "&PolicyId=" + policyid.Value + "&PolicyName=" + policyname.Value + "&PolicyNumber=" + policynumber.Value + "&CodeId=" + notesparentcodeid.Value);
                    //williams-neha goel:MITS 21704--end: added policy
                }
                else
                {
                    //williams-neha goel:MITS 21704--start: added policy
                    //Response.Redirect("~/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=" + eventid.Value + "&EventNumber=" + lblEventNumber.Text + "&ClaimID=" + claimid.Value + "&SysFormName=" + "" + "&NoteType=" + ((HiddenField)e.Item.FindControl("hdnNoteTypeCode")).Value + "&NoteTypecodeid=" + ((HiddenField)e.Item.FindControl("hdnNoteTypeCodeId")).Value + "&ClaimProgressNoteId=" + ((HiddenField)e.Item.FindControl("hdnClaimProgressNoteId")).Value + "&ActivityDate=" + ((HiddenField)e.Item.FindControl("hdnDateEntered")).Value + "&CommentsFlag=true&NewRecord=false&ShowBack=true");
                    Response.Redirect("~/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=" + eventid.Value + "&EventNumber=" + lblEventNumber.Text + "&ClaimID=" + claimid.Value + "&SysFormName=" + "" + "&NoteType=" + ((HiddenField)e.Item.FindControl("hdnNoteTypeCode")).Value + "&NoteTypecodeid=" + ((HiddenField)e.Item.FindControl("hdnNoteTypeCodeId")).Value + "&ClaimProgressNoteId=" + ((HiddenField)e.Item.FindControl("hdnClaimProgressNoteId")).Value + "&ActivityDate=" + ((HiddenField)e.Item.FindControl("hdnDateEntered")).Value + "&CommentsFlag=true&NewRecord=false&ShowBack=true" + "&PolicyId=" + policyid.Value + "&PolicyName=" + policyname.Value + "&PolicyNumber=" + policynumber.Value + "&CodeId=" + notesparentcodeid.Value + "&LOB=" + Request.QueryString["LOB"].ToString());
                    //williams-neha goel:MITS 21704--end: added policy
                }
            }
        }

        protected void rptNotes_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lb = (LinkButton)e.Item.FindControl("lblClaimNumber");
            HiddenField hd = (HiddenField)e.Item.FindControl("hdnEditMode");
            //MGaba2:MITS 22115 :Edit Permission Setting was not working on View All Note Screen
            //if (lb != null && hd != null)
            //{
            //    if (hd.Value == "false")
            if (lb != null)
            {
                if ( (hd != null && hd.Value == "false") || !bEditPermission)
                    lb.Enabled = false;
            }
        }

        // atavaragiri :mits 28264 //
        public void OnView()
        {
                try
                {

                    ProgressNotesType objProgressNote = new ProgressNotesType();
                    objProgressNote.objFilter = new Filter();
                    objProgressNote.PageNumber = Int32.Parse(txtPageNumber.Text);
                
                    if (!string.IsNullOrEmpty(hdSortColumn.Value) && !string.IsNullOrEmpty(hdDirection.Value))
                    {
                        objProgressNote.SortColumn = hdSortColumn.Value;
                        objProgressNote.Direction = hdDirection.Value;
                    }
                    else
                    {
                        objProgressNote.SortColumn = sNoSort;
                    }

                    string sPageNumber = txtPageNumber.Text;
                    int iPageNumber = 1, iMaxPages = 1;

                    if (!string.IsNullOrEmpty(sPageNumber))
                    {
                        int.TryParse(sPageNumber, out iPageNumber);
                        objProgressNote.PageNumber = iPageNumber;
                        hdPageNumber.Value = Convert.ToString(objProgressNote.PageNumber);
                    }

                    if (Convert.ToInt32(hdPageNumber.Value) <= Convert.ToInt32(hdTotalNumberOfPages.Value))
                    {
                        if (string.Compare(hdPageNumber.Value, "1") == 0)
                        {
                            OnPageLoad(sFirst);
                        }

                        if (string.Compare(hdPageNumber.Value, hdTotalNumberOfPages.Value) == 0)
                        {
                            OnPageLoad(sLast);
                        }

                        if (Convert.ToInt32(hdPageNumber.Value) > 1 && hdPageNumber.Value != hdTotalNumberOfPages.Value)
                        {
                            hdPageNumber.Value = Convert.ToString(Convert.ToInt32(hdPageNumber.Value) - 1);
                            OnPageLoad(sNext);
                        }

                    }   // end of if block

                    
                    else
                    {
                        txtPageNumber.Text = hdTotalNumberOfPages.Value;
                        OnPageLoad(sLast);
                    }

                }    // end of try block
                catch (Exception ee)
                {
                   // tdNoNotes.Visible = true;
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
        }     // end of OnLoad() function

        protected void btnHiddenView_Click(object sender, EventArgs e)
        {
            OnView();
        }

        // atavaragiri :mits 28264 //
    }
}
