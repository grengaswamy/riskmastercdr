﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdvancedSearch.aspx.cs" ValidateRequest="false"
    Inherits="Riskmaster.UI.ProgressNotes.AdvancedSearch" %>   <!-- validate request set to false:pen testing mits 27832 -->
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc1" %>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" tagname="ErrorControl" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> </title>
   <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
  <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
<script language="JavaScript" src="../../Scripts/ProgressNotes.js" type="text/javascript">
</script>
<script language="JavaScript" src="../../Scripts/form.js" type="text/javascript">
    </script>
   <script language="JavaScript" src="../../Scripts/cul.js" type="text/javascript"></script>

    <%--<script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
    </script>--%>

    <!--Praveen ML-->    
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
    <!--Praveen ML-->

    <script language="JavaScript" type="text/javascript">
        //Ashish Ahuja-Mits 34383 start
        function Count(text, long) {
            var maxlength = new Number(long);
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);
                alert("Only " + long + " characters are allowed for this field.");
            }
        }
        //Ashish Ahuja-Mits 34383 end
        //<%--Parijat :12821  for showing claimant--%>
        var objClaimantinfo = document.getElementById('claimant');   //EnhancedMDI
        if(objClaimantinfo!=null)
        {
            var sClaimantInfo=objClaimantinfo.value;
            if(sClaimantInfo!="")
            {
                var newtitle = "Advanced Search[Claimant:" + sClaimantInfo + "]";
                document.title = newtitle;
            }
            else
            {
                var sNoClaimant = "Advanced Search";
                document.title=sNoClaimant;
            }
        }

        function setTitle()
        {
            document.title = document.getElementById("hdHeader").value;  //MITS 28588 hlv 8/3/12 .replace(/\[ampersand\]/gi, "&")
        } 
    </script>
</head>
<body onload ="setTitle();">
    <form id="frmData" runat="server" name="frmData" method="post">
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""/>
    <input type="hidden" name="$node^9" value="" id="ouraction" runat ="server"/>
    <input type="hidden" name="$node^22" value='<%=Request.QueryString["FormName"]%>' id="SysFormName" runat="server"/>
    <input type="hidden" name="$node^18" value='<%=Request.QueryString["EventID"]%>' id="eventid" runat="server"/>
    <input type="hidden" name="$node^34" value="" id="sSortBy" runat ="server"/>
    <input type="hidden" name="$node^35" value="" id="sSortFirst"/>
    <input type="hidden" name="$node^36" value="" id="sSortSecond"/>
    <input type="hidden" name="$node^37" value="" id="sSortThird"/>
    <input type="hidden" name="$node^38" value="" id="sSortLast"/>
    <input type="hidden" name="$node^34" value="" id="claimant" runat="server"/>
    <%--parijat:EnhanceMDI--%>
    <input type="hidden" name="$node^29" value="true" id="applysort" runat="server"/>
    <input type="hidden" name="$node^26" value="ProgressNotesAdaptor.OnLoad" id="functiontocall"/>
    <input type="hidden" name="$node^28" value="" id="activatefilter" runat = "server"/>
    <input type="hidden" name="$node^31" value="" id="claimidlist" runat = "server"/>
    <input type="hidden" name="$node^32" value="" id="usertypelist" runat = "server"/>
    <input type="hidden" name="$node^33" value="" id="notetypelist" runat = "server"/>
    <input type="hidden" name="$node^34" value="" id="enteredbylist" runat = "server"/>
    <input type="hidden" name="$node^19" value='<%=Request.QueryString["ClaimID"]%>' id="claimid" runat="server"/>
    <%--williams--retrofit-neha goel:MITS 21704---start:added for policy enhanced notes--%>
    <input type="hidden" name="$node^20" value='<%=Request.QueryString["LOB"]%>' id="LOB" runat="server"/> <!-- pmittal5 Mits 21514 -->
    <%--williams--retrofit-neha goel:MITS 21704---end:added for policy enhanced notes--%>
    <input type="hidden" id="txtSubmit" value="" runat ="server"/>
    <input type="hidden" name="$node^25" value='<%=Request.QueryString["EventNumber"]%>' id="eventnumber" runat="server"/>
    <%--williams--neha goel:MITS 21704---start:added for policy enhanced notes--%>
     <input type="hidden" name="$node^39" value='<%=Request.QueryString["PolicyId"]%>' id="policyid" runat="server"/>
     <input type="hidden" name="$node^40" value='<%=Request.QueryString["PolicyName"]%>' id="policyname" runat="server"/>
     <input type="hidden" name="effectivedate" value="" id="effectivedate" />
     <input type="hidden" name="$node^41" value='<%=Request.QueryString["PolicyNumber"]%>' id="policynumber" runat="server"/>
     <input type="hidden" name="$node^42" value="" id="NoteParentType" runat="server"/>
      <%--williams--neha goel:MITS 21704---end--%>
         <%--Added by akaushik5 for MITS 30789 Starts--%>
      <input type="hidden" name="sOrderBy" value="" id="sOrderBy" runat ="server"/>
    <%--Added by akaushik5 for MITS 30789 Ends--%>
    <asp:textbox runat="server" style="display:none" id="hdHeader" />    
        <%-- mkaran2 - MITS 27038 - start --%>
     <input type="hidden" name="$node^42" value="" id="hdSortColumn" runat="server"/>
     <input type="hidden" name="$node^42" value="" id="hdDirection" runat="server"/>
     <input type="hidden" name="$node^42" value="" id="claimprogressnoteid" runat="server"/>
     <input type="hidden" name="$node^42" value="" id="NoteType_codelookup_cid" runat="server"/> 
     <input type="hidden" id="freezetext" value="" runat="server" />        
     <input type="hidden" id="showDateStamp" value="" runat="server" />    
     <input type="hidden" id="username" value="" runat="server" />       
        <%-- mkaran2 - MITS 27038 - end --%>
    
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td colspan="4" class="msgheader" bgcolor="#D5CDA4">
                <asp:Label ID="lblAdvancedSearch" runat="server" Text="<%$ Resources:lblAdvancedSearchResrc %>" />
            </td>
        </tr>
    </table>
    <table colspan="4" border="0" cellspacing="0" celpadding="0" width="80%">
        <tr>
            <% if (Request.QueryString["EventNumber"] != "")
            {  %>
                <td>
                    <asp:Label ID="lblEventNo" runat="server" Text="<%$ Resources:lblEventNoResrc %>" />
                </td>
                <td>
                    <b><%=Request.QueryString["EventNumber"]%></b>
                </td>
            <%} %>
            <% if (Request.QueryString["PolicyName"] != "")
            {  %>
                <td> <%--williams--neha goel:MITS 21704---start:added for policy enhanced notes--%>
                    <asp:Label ID="lblPolicyName" runat="server" Text="<%$ Resources:lblPolicyNameResrc %>" />           
                </td>
                <td>
                    <b><%=Request.QueryString["PolicyName"]%></b>
                </td><%--williams--neha goel:MITS 21704---start:added for policy enhanced notes--%>
            <%} %>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblClaims" runat="server" Text="<%$ Resources:lblClaimsResrc %>" />
            </td>
            <td width="25%">
                <select name="$node^21" multiple="multiple" id="lstClaims" />
                <input type="button" class="button" id="lstClaimsbtn" onclick="selectClaim('lstClaims')" value="..." runat="server" />
                <input type="button" class="button" id="lstClaimsbtndel" onclick="deleteSelClaim('lstClaims')" value="-" runat="server" />
                <input type="text" value="" style="display: none" id="lstClaims_lst" runat="server" />
            </td>
            <td>
                <asp:Label ID="lblUserTypes" runat="server" Text="<%$ Resources:lblUserTypesResrc %>" />
            </td>
            <td width="25%">
                <select name="$node^23" multiple="multiple" id="lstUserTypes" />
                <input type="button" class="button" id="lstUserTypesbtn" onclick="selectCode('USER_GROUPS','lstUserTypes')" value="..." />
                <input type="button" class="button" id="lstUserTypesbtndel" onclick="deleteSelCode('lstUserTypes')" value="-" />
                <input type="hidden" name="$node^24" value="" id="lstUserTypes_lst" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblEnteredBy" runat="server" Text="<%$ Resources:lblEnteredByResrc %>" />
            </td>
            <td width="25%">
                <select name="$node^29" multiple="multiple" id="lstEnteredBy" />
                <input type="button" class="button" id="lstEnteredBybtn" value="..." onclick="AddCustomizedListUser('enhancednotes','lstEnteredBy','lstEnteredBy_lst','lstEnteredByUser_lst')" />
                <input type="button" class="button" id="lstEnteredBybtndel" value="-" onclick="DelCustomizedListUser('enhancednotes','lstEnteredBy','lstEnteredBy_lst','lstEnteredByUser_lst')" />
                <input type="hidden" name="$node^28" value="&#xA;        " id="lstEnteredByUser_lst" />
                <input type="hidden" name="$node^27" value="" id="lstEnteredBy_lst" />
            </td>
            <td>
                <asp:Label ID="lblNoteTypes" runat="server" Text="<%$ Resources:lblNoteTypesResrc %>" />
            </td>
            <td width="25%"><%--williams--neha goel:MITS 21704---start:added for policy enhanced notes ;codeid modifications--%>
                <select name="$node^25" multiple="multiple" id="lstNoteTypes" />
                <input type="button" class="button" id="lstNoteTypesbtn" onclick="selectCode('NOTE_TYPE_CODE','lstNoteTypes','', '','CODES.RELATED_CODE_ID IN(NoteParentType,0)')" value="..." />
                <input type="button" class="button" id="lstNoteTypesbtndel" onclick="deleteSelCode('lstNoteTypes')" value="-" />
                <input type="hidden" name="$node^26" value="" id="lstNoteTypes_lst" />
            </td>
        </tr>
        <!--ADDED BY GBINDRA MITS#34104 02062014 WWIG GAP15 START -->
        <tr>
            <td>
                <asp:Label ID="lblClaimant" runat="server" Text="<%$ Resources:lblClaimantResrc %>" />
            </td>
            <td width="25%">
                <select name="$node^43" multiple="Multiple" id="lstClaimant"/>
                <input type="button" class="button" id="lstClaimantbtn" onclick="selectClaimant('lblClaimant')" value="..." runat="server" />
                <input type="button" class="button" id="lstClaimantbtndel" onclick="deleteSelClaimant('lblClaimant')" value="-" runat="server" />
                <input type="hidden" id="hdClaimantId" runat="server" value="" />
                <input type="text" value="" style="display: none" id="lstClaimant_lst" runat="server" />
            </td>
            <td>
                &nbsp;
            </td>
            <td width="25%">
                &nbsp;
            </td>
        </tr>
        <!--ADDED BY GBINDRA MITS#34104 02062014 WWIG GAP15 START -->
        <tr>
            <td>
                <asp:Label ID="lblActivityFrom" runat="server" Text="<%$ Resources:lblActivityFromResrc %>" />
            </td>
            <td nowrap="1">
                <input type="text" name="$node^30" value="" id="activityfromdate" size="20" rmxforms:as="date" onblur="dateLostFocus(this.id);" runat ="server" />
               <script type="text/javascript">
                   $(function () {
                       $("#activityfromdate").datepicker({
                           showOn: "button",
                           buttonImage: "../../Images/calendar.gif",
                           buttonImageOnly: true,
                           showOtherMonths: true,
                           selectOtherMonths: true,
                           changeYear: true
                       });
                   });
                </script>
              
            </td>
            <td>
                <span class="label">
                    <asp:Label ID="lblSubject" runat="server" Text="<%$ Resources:lblSubject %>" />
                </span>
            </td>
            <td>
                <%--Ashish Ahuja-Mits 34383 --%>
               <%--<input type="text" name="$node^35" value="" id="subjectlist" size="20" runat ="server"  />--%>
               <textarea name="$node^35" id="subjectlist" runat="Server" rows ="2" cols ="50" onkeyup="Count(this,250)" onchange="Count(this,250)" ></textarea>

            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblActivityTo" runat="server" Text="<%$ Resources:lblActivityToResrc %>" />
            </td>
            <td nowrap="1">
                <input type="text" name="$node^31" value="" id="activitytodate" size="20" rmxforms:as="date" onblur="dateLostFocus(this.id);" runat ="server" />
                
                <script type="text/javascript">
                    $(function () {
                        $("#activitytodate").datepicker({
                            showOn: "button",
                            buttonImage: "../../Images/calendar.gif",
                            buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        });
                    });
                </script>
               
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNoteText" runat="server" Text="<%$ Resources:lblNoteTextResrc %>" />
            </td>
            <td>
                <input type="text" name="$node^32" value="" id="txtnotestextcontains" size="50" runat="server"/>
            </td>
        </tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr width="100%" class="ctrlgroup">
            <td width="100%" colspan="2" class="msgheader" bgcolor="#D5CDA4">
                <asp:Label ID="lblSortBy" runat="server" Text="<%$ Resources:lblSortByResrc %>" />
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
            <td>
            <%--Changed by Amitosh for mits 23473 (05/11/2011)--%>
                <%--<select name="$node^33" id="selSortBy1">
                    <option value="0"></option>
                    <option value="1">Entered By</option>
                    <option value="2">Note Type</option>
                    <option value="3">Activity Date</option>
                    <option value="4">User Type</option>
                    <option value="5">Adjuster</option>
                </select>
                and
                <select name="$node^33" id="selSortBy2">
                    <option value="0"></option>
                    <option value="1">Entered By</option>
                    <option value="2">Note Type</option>
                    <option value="3">Activity Date</option>
                    <option value="4">User Type</option>
                    <option value="5">Adjuster</option>
                </select>
                and
                <select name="$node^33" id="selSortBy3">
                    <option value="0"></option>
                    <option value="1">Entered By</option>
                    <option value="2">Note Type</option>
                    <option value="3">Activity Date</option>
                    <option value="4">User Type</option>
                    <option value="5">Adjuster</option>
                </select>
                and
                <select name="$node^33" id="selSortBy4">
                    <option value="0"></option>
                    <option value="1">Entered By</option>
                    <option value="2">Note Type</option>
                    <option value="3">Activity Date</option>
                    <option value="4">User Type</option>
                    <option value="5">Adjuster</option>
                </select>--%>
                <%--Added by akaushik5 for mits 30789 Starts--%>        
                <table border = "0">
                <tr>
                <td>
                <%--Added by akaushik5 for mits 30789 Ends--%>
                <asp:DropDownList ID="selSortBy1" runat="server">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="1" Text="<%$ Resources:lbEnteredByResrc %>"></asp:ListItem>
                    <asp:ListItem Value="2" Text="<%$ Resources:lbNoteTypeResrc %>"></asp:ListItem>
                    <asp:ListItem Value="3" Text="<%$ Resources:lbActivityDateResrc %>"></asp:ListItem>
                    <asp:ListItem Value="4" Text="<%$ Resources:lbUserTypeResrc %>"></asp:ListItem>
                    <asp:ListItem Value="5" Text="<%$ Resources:lbAdjusterResrc %>"></asp:ListItem>
                </asp:DropDownList>
                    <%--Added by akaushik5 for mits 30789 Starts--%>        
                <br/>
                <asp:DropDownList ID="selOrderBy1" runat="server">
                    <asp:ListItem Value="DESC" Text="Descending"></asp:ListItem>
                    <asp:ListItem Value="ASC" Text="Ascending"></asp:ListItem>
                </asp:DropDownList>
                </td>
                <td>
                <%--Added by akaushik5 for mits 30789 Ends--%>
				<asp:Label ID="Label3" runat="server" Text="<%$ Resources:lblAnd %>" />
                    <%--Added by akaushik5 for mits 30789 Starts--%>
                </td>
                <td>
                <%--Added by akaushik5 for mits 30789 Ends--%>
                <asp:DropDownList ID="selSortBy2" runat="server">
                  <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="1" Text="<%$ Resources:lbEnteredByResrc %>"></asp:ListItem>
                    <asp:ListItem Value="2" Text="<%$ Resources:lbNoteTypeResrc %>"></asp:ListItem>
                    <asp:ListItem Value="3" Text="<%$ Resources:lbActivityDateResrc %>"></asp:ListItem>
                    <asp:ListItem Value="4" Text="<%$ Resources:lbUserTypeResrc %>"></asp:ListItem>
                    <asp:ListItem Value="5" Text="<%$ Resources:lbAdjusterResrc %>"></asp:ListItem>
                </asp:DropDownList>
                    <%--Added by akaushik5 for mits 30789 Starts--%>
                <br/>
                 <asp:DropDownList ID="selOrderBy2" runat="server">
                    <asp:ListItem Value="DESC" Text="Descending"></asp:ListItem>
                    <asp:ListItem Value="ASC" Text="Ascending"></asp:ListItem>
                </asp:DropDownList>
                </td>
                <td>
                <%--Added by akaushik5 for mits 30789 Ends--%>
                 <asp:Label ID="Label1" runat="server" Text="<%$ Resources:lblAnd %>" />
                    <%--Added by akaushik5 for mits 30789 Starts--%>
                </td>
                <td>
                <%--Added by akaushik5 for mits 30789 Ends--%>
                <asp:DropDownList ID="selSortBy3" runat="server">
                <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="1" Text="<%$ Resources:lbEnteredByResrc %>"></asp:ListItem>
                    <asp:ListItem Value="2" Text="<%$ Resources:lbNoteTypeResrc %>"></asp:ListItem>
                    <asp:ListItem Value="3" Text="<%$ Resources:lbActivityDateResrc %>"></asp:ListItem>
                    <asp:ListItem Value="4" Text="<%$ Resources:lbUserTypeResrc %>"></asp:ListItem>
                    <asp:ListItem Value="5" Text="<%$ Resources:lbAdjusterResrc %>"></asp:ListItem>
                </asp:DropDownList>
                    <%--Added by akaushik5 for mits 30789 Starts--%>
                <br/>
                 <asp:DropDownList ID="selOrderBy3" runat="server">
                    <asp:ListItem Value="DESC" Text="Descending"></asp:ListItem>
                    <asp:ListItem Value="ASC" Text="Ascending"></asp:ListItem>
                </asp:DropDownList>
                </td>
                <td>
                <%--Added by akaushik5 for mits 30789 Ends--%>
                 <asp:Label ID="Label2" runat="server" Text="<%$ Resources:lblAnd %>" />
                <%--Added by akaushik5 for mits 30789 Starts--%>
                </td>
                <td>
                <%--Added by akaushik5 for mits 30789 Ends--%>
				<asp:DropDownList ID="selSortBy4" runat="server">
                  <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="1" Text="<%$ Resources:lbEnteredByResrc %>"></asp:ListItem>
                    <asp:ListItem Value="2" Text="<%$ Resources:lbNoteTypeResrc %>"></asp:ListItem>
                    <asp:ListItem Value="3" Text="<%$ Resources:lbActivityDateResrc %>"></asp:ListItem>
                    <asp:ListItem Value="4" Text="<%$ Resources:lbUserTypeResrc %>"></asp:ListItem>
                    <asp:ListItem Value="5" Text="<%$ Resources:lbAdjusterResrc %>"></asp:ListItem>
                </asp:DropDownList>
                    <%--Added by akaushik5 for mits 30789 Starts--%>
                <br/>
                 <asp:DropDownList ID="selOrderBy4" runat="server">
                    <asp:ListItem Value="DESC" Text="Descending"></asp:ListItem>
                    <asp:ListItem Value="ASC" Text="Ascending"></asp:ListItem>
                </asp:DropDownList>
                </td>
                </tr>
                </table>
                <%--Added by akaushik5 for mits 30789 Ends--%>
              </td>
        </tr>
    </table>
    <table>
        <tr>
            <td colspan="2" align="left">
                <input type="submit" name="$action^" value="<%$ Resources:btnSearchResrc%>" class="button" runat="server" onclick="return ValidateInfoCreateFilterString(); "  onserverclick="Return_click" /><%-- //EnhancedMDI--%>
            </td>
            <td>
                <input type="submit" name="$action^" value="<%$ Resources:btnCancelResrc %>" class="button" onserverclick="Return_click" runat="server" /><%--onclick="window.close();; "--%><%-- //EnhancedMDI--%>
            </td>
        </tr>
         <!-- mkaran2 - MITS 27038 - start -->
       <tr>
          <td visible="false" id="tdbtnNotes" runat="server">
                <input id="Submit1" type="submit" name="$action^" value="<%$ Resources:btnViewNotesResrc %>" class="button"   onclick="return funcViewAsHtml(); " runat="server" />
          </td> 
          <td colspan="3" align="left" visible="false" id="tdRadioNotes" runat="server">
                <asp:RadioButtonList ID="rdbtnViewNotes" runat="server" RepeatDirection="Horizontal">
                	<asp:ListItem Text="<%$ Resources:rdbtnViewNotes1Resrc %>" Selected="True" Value="0"></asp:ListItem>
                	<asp:ListItem Text="<%$ Resources:rdbtnViewNotes2Resrc %>" Value="1"></asp:ListItem>
                	<asp:ListItem Text="<%$ Resources:rdbtnViewNotes3Resrc %>" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
          </td>                                     
      </tr>
        <!-- mkaran2 - MITS 27038 - end -->            
    </table>
    <input type="hidden" name="$node^7" value="" id="SysWindowId" /><input type="hidden"
        name="$instance" value="H4sIAAAAAAAAAHVUTW/iMBA901+Beqdm6WVVZSN1S5HQInYlsr17naFYxXblMQT+/U4ck8ROmtPM&#xA;e28+/GQnW2t0XAuYXtRR49Nlb6zCH/cH5z6fGKuq6qF6fDD2nS3m8wVr6Pv8bjKZTOnLFDjuM596&#xA;CIUF0DNZ5sxK/FAcHVj2x5p3C4hb4wDZc3muh5Y74FYcMtbVxL1OCLZGBYqM3ZJkHDWVpq5lCVMA&#xA;ul9w/QJesPYUHq6kLk1V989YF3uKtadslpJJRy4cbUB1IYjZjdQfI2ugsW4mzPGk9ChlbEm2jTEl&#xA;d3zmrp8wYA+0NFhrLOZ7fkQgY3tQLG7AzgTyV3bJrnF1rfemJ1kacVKgXdLq9UzYekkG3KLpiGB7&#xA;Uv/A5q9vdJO+z+n79hgKAhPXvBy5VBuJLj2lJ5CGhSBmV3RBt1wB8W2YbPOX7lFB9pGkDccV9fie&#xA;yqexsr7OoVcbjitCryhNXNL0UKD8eQ3SOL+bJl+sr3dMazz2xYy+NtE80y0+S3ddWaOW3NVnG0Dj&#xA;FYVJ9AEYOoIFXNyL0Y5LjcGWGItrdnTx/dIhGGN/1y+mlTTZULeS1rvbxUPNDoTRZRCFZKgqDtLe&#xA;RE081Gx4O8yH3Wvrv6WM3X7D+X8HJgVIkQUAAA==" />
    </form>
</body>
</html>
