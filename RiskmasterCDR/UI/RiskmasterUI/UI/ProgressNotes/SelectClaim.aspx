﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectClaim.aspx.cs" Inherits="Riskmaster.UI.ProgressNotes.SelectClaim" %>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" tagname="ErrorControl" tagprefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Claim Selection</title>
   <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css">
  <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css">
<script language="JavaScript" src="../../Scripts/ProgressNotes.js">
</script>
 <script language="JavaScript" src="../../Scripts/form.js">
    </script>
</head>
<body>
    <form id="form1" runat="server" name="frmData" method="post" >
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""/>
    <input type="hidden" name="$node^9" value="" id="ouraction"/>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                      <td colspan="2">
                          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                     </td>
             </tr>
            <tr>
                <td colspan="4" class="msgheader" bgcolor="#D5CDA4">
                    <asp:Label ID="lblSelectClaim" runat="server" Text="<%$ Resources:lblSelectClaimResrc %>"></asp:Label>
                </td>
            </tr>
        </table>
    <%--<table colspan="4" border="0" cellspacing="0" celpadding="0" width="100%">
        <tr class="ctrlgroup">
            <td class="PadLeft4" nowrap="" align="center">
                Claim Number
            </td>
            <td class="PadLeft4" nowrap="" align="center" width="32">
                &nbsp;
            </td>
            <td class="PadLeft4" nowrap="" align="center">
                Date of Claim
            </td>
            <td class="PadLeft4" nowrap="" align="center" width="32">
                &nbsp;
            </td>
            <td class="PadLeft4" nowrap="" align="center">
                Claim Status
            </td>
            <td class="PadLeft4" nowrap="" align="center" width="32">
                &nbsp;
            </td>
            <td class="PadLeft4" nowrap="" align="center">
                Claimant
            </td>
            <td class="PadLeft4" nowrap="" align="center" width="32">
                &nbsp;
            </td>
            <td class="PadLeft4" nowrap="" align="center">
                Claim Type
            </td>
        </tr>
    </table>
    <div id="divForms" class="divScroll">
        <table width="100%" cellspacing="0" cellpadding="1" border="0">
            <tr>
                <td class="data">
                    <a href="/oxf/home" onclick="; document.getElementById('wsrp_rewrite_action_1').name += '$action^';         event.returnValue=false;         if (CallParentPage('10','111'))          document.forms['wsrp_rewrite_form_1'].submit();         "
                        class="LightBlue" style="text-decoration: underline;">111</a>
                </td>
                <td class="data">

                    <script>
					document.writeln(formatRMDate(20080919));
                    </script>

                </td>
                <td class="data">
                    Open
                </td>
                <td class="data">
                </td>
                <td class="data">
                    1051
                </td>
            </tr>
        </table>
    </div>--%>
     <table colspan="4" border="0" cellspacing="0" celpadding="0" width="100%">
         <tr class="ctrlgroup">
           <asp:GridView ID="GridView1" runat="server" HeaderStyle-CssClass="ctrlgroup" AutoGenerateColumns ="false" 
           AllowSorting ="true" OnRowCommand = "GridView1_OnRowCommand" OnRowDataBound="GridView1_RowDataBound" Width = "100%"
           ><%--OnSelectedIndexChanged = "GridView1_OnSelectedIndexChanged" AutoGenerateSelectButton="true">--%>
         <Columns>
         
            <asp:TemplateField HeaderText="ClaimID" visible="false" >
                <ItemTemplate>
                    <asp:Label ID="lblClaimID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClaimID")%>'></asp:Label>
                </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="<%$ Resources:gvHdrClaimNumberResrc %>"  >
                <ItemTemplate>
                    <asp:LinkButton ID="lblClaimNumber" runat="server" CommandArgument = "claimNumber"  Text='<%# DataBinder.Eval(Container.DataItem, "ClaimNumber")%>'></asp:LinkButton>
                </ItemTemplate>
             </asp:TemplateField>
              <asp:TemplateField HeaderText="<%$ Resources:gvHdrClaimDateResrc %>"  >
                <ItemTemplate>
                    <asp:Label ID="lblClaimDate" runat="server" ></asp:Label>
                </ItemTemplate>
             </asp:TemplateField>
              <asp:TemplateField HeaderText="<%$ Resources:gvHdrClaimStatusResrc %>"  >
                <ItemTemplate>
                    <asp:Label ID="lblClaimStatusDesc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClaimStatusDesc")%>'></asp:Label>
                </ItemTemplate>
             </asp:TemplateField>
              <asp:TemplateField HeaderText="<%$ Resources:gvHdrClaimantResrc %>"  >
                <ItemTemplate>
                    <asp:Label ID="lblClaimantName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClaimantName")%>'></asp:Label>
                </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="<%$ Resources:gvHdrClaimTypeResrc %>"  >
                <ItemTemplate>
                    <asp:Label ID="lblClaimTypeCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClaimTypeCode")%>'></asp:Label>
                </ItemTemplate>
             </asp:TemplateField>
              </Columns>
           </asp:GridView>
           </tr>
       </table>
    <input type="hidden" name="$node^7" value="" id="SysWindowId"><input type="hidden"
        name="$instance" value="H4sIAAAAAAAAAHVSTW/CMAw9d78CcQczuKGsF9gBbZomsT8QpQYimgTZZoV/v7Rd6ce6nOz37Gfn&#xA;JWrnWbQ3OLm53PP6dgjk+GV6ErmsAYqimBereaAjLBeLJdT0NH1KkmQSj3IousqqtILYEKKf2SwF&#xA;snx2mgUJPikcCZk/giDDHnM0ssm1dQrahr7QlZFK1LBR0CSDWVHRhrIXBswXsrzh/R94CY8rVHBh&#xA;fRaKUl9BG1cUPK5YL2UHitpI3CD2/QZ99t3688gaHEhmJuRX50epQFn0bIzJtOiZ3C/4hz3FpZEo&#xA;EKcHnTNGYztQv7gGWxOiv7ZN9rWrO38InZJtMFeHXgZSr98R223T55WCJp70S+rXxqx67+7QrqSC&#xA;5iumP26dLbmVAgAA"></form>
</body>
</html>
