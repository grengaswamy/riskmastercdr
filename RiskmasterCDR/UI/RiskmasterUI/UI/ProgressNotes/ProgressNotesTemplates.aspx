﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProgressNotesTemplates.aspx.cs" Inherits="Riskmaster.UI.ProgressNotes.ProgressNotesTemplates" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title> </title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/ProgressNotes.js" type="text/javascript">
    </script>
    <script language="JavaScript" type="text/javascript">
	function setTitle()
	{
	    document.title = document.getElementById("hdHeader").value;  //MITS 28588 hlv 8/3/12 .replace(/\[ampersand\]/gi, "&")
	}
	//tmalhotra3: MITS 31938
	function window.onerror(strError) 
	{
	    if (strError.toLowerCase().indexOf('unspecified error') >= 0) 
	    {
	        window.event.returnValue = true; //catching the value for Cancel 
	        window.focus();
	    }
	    else 
	    {
	        window.event.returnValue = false;
	    }
	}
    </script>
</head>
<body onload="setTitle();">
    <form id="frmData" runat="server">
     <uc1:ErrorControl ID="ErrorControl" runat="server" />
     <asp:HiddenField ID="TemplateId" runat="server" />
     <asp:HiddenField ID="TemplateName" runat="server" />
      <asp:HiddenField ID="claimant" runat="server" />
    <asp:HiddenField ID="SysFormName" runat="server" />
    <asp:HiddenField ID="eventid" runat="server" />
    <asp:HiddenField ID="claimid" runat="server" />
    <asp:HiddenField ID="policyid" runat="server" /> <!--williams:neha goel:MITS 21704---->
    <asp:HiddenField ID="policynumber" runat="server" /> <!--williams:neha goel:MITS 21704---->
    <asp:HiddenField ID="policyname" runat="server" /> <!--williams:neha goel:MITS 21704---->
    <asp:HiddenField ID="LOB" runat="server" /> <!-- pmittal5 Mits 21514-->
    <asp:HiddenField ID="eventnumber" runat="server" />
    <asp:textbox runat="server" style="display:none" id="hdHeader" />
    <div>
     <div style="position: relative; width: 99.3%; height: 40%; overflow: auto" class="divScroll">
      <table width="97%" cellspacing="0" cellpadding="0" border="0">
                        <tr class="ctrlgroup">
     <asp:GridView Width="100%" ID="grdTemplates" runat="server" AllowSorting="true" OnSorting="OnSort" HeaderStyle-CssClass="GridViewHeader" AutoGenerateColumns="false" OnRowCreated="OnRowCreated" OnRowDataBound="OnRowDataBound" OnDataBound="OnDataBound">
     <Columns>
     <asp:TemplateField>
     <ItemTemplate>
     <asp:RadioButton id="RowSelector" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TemplateId")%>' OnCheckedChanged="Grid_radioOncheckedChanged" AutoPostBack="true"/>
     </ItemTemplate>
     </asp:TemplateField>
	
     <asp:TemplateField HeaderText="<%$ Resources:lbltemplatename %>" SortExpression="TemplateName" HeaderStyle-Width="49%"> 
     <ItemTemplate>
     <asp:Label ID="lblTemplateName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"TemplateName") %>'></asp:Label>
     </ItemTemplate>
     </asp:TemplateField>
     <asp:TemplateField HeaderText="<%$ Resources:lblenteredby %>" SortExpression="UpdatedBy" HeaderStyle-Width="49%">
     <ItemTemplate>
     <asp:Label ID="lblEnteredBy" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"UpdatedBy") %>'/>
     </ItemTemplate>
     </asp:TemplateField>
     <asp:TemplateField HeaderText="<%$ Resources:lbltemplatename %>" HeaderStyle-Width="50%" Visible="false"> 
     <ItemTemplate>
     <asp:LinkButton ID="LookUpTemplateName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"TemplateName") %>'></asp:LinkButton>
     </ItemTemplate>
     </asp:TemplateField>
     <asp:TemplateField HeaderText="<%$ Resources:lblenteredby %>"  HeaderStyle-Width="50%" Visible="false">
     <ItemTemplate>
     <asp:Label ID="LookUpEnteredBy" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"UpdatedBy") %>'/>
     </ItemTemplate>
     </asp:TemplateField>
     <asp:TemplateField HeaderText="<%$ Resources:lbltemplatmemo %>" visible="false">
     <ItemTemplate>
     <asp:Label ID="lblTemplateMemo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"TemplateMemo") %>'/>
     </ItemTemplate>
     </asp:TemplateField>
     <asp:TemplateField HeaderText="<%$ Resources:lbladdedby %>" visible="false">
     <ItemTemplate>
     <asp:Label ID="lblAddedBy" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"AddedBy") %>'/>
     </ItemTemplate>
     </asp:TemplateField>
      <asp:TemplateField HeaderText="<%$ Resources:lbldateadded %>" visible="false">
     <ItemTemplate>
     <asp:Label ID="lblDateAdded" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DateAdded") %>'/>
     </ItemTemplate>
     </asp:TemplateField>
     <asp:TemplateField HeaderText="<%$ Resources:lbldateupdated %>" visible="false">
     <ItemTemplate>
     <asp:Label ID="lblDateUpdated" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DateUpdated") %>'/>
     </ItemTemplate>
     </asp:TemplateField>
    <asp:TemplateField HeaderText="<%$ Resources:lbltemplateid %>" visible="false">
     <ItemTemplate>
     <asp:Label ID="lblTemplateId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"TemplateId") %>'/>
     </ItemTemplate>
     </asp:TemplateField>
	 
     </Columns> 
     </asp:GridView>
     </tr>
     <tr>
     <td align="center" class="PadLeft4" visible="false" id="tdNoTemplates" runat="server">
            <br />
            <br />
            <asp:Label ID="lblNoTemplates" runat="server" Text="<%$ Resources:lblNoTemplatesResrc %>"></asp:Label><br />
            <br />
            <br />
            &nbsp;

            <script>
                UpdateRecordFlag('false');
            </script>

        </td>
        </tr>
     </table>
     </div>
     <div style="position: relative; width: 99.3%; height: 48%; overflow: auto" class="divScroll" id="TemplateMemoDiv" runat="server">
     </div>
     <div>
    <asp:Button class="button" ID="btnCreateTemplate" runat="server" Text="<%$ Resources:btnCreateTemplateResrc %>" 
             onclick="btnCreateTemplate_Click" />
    <asp:Button class="button" ID="btnEditTemplate" runat="server" Text="<%$ Resources:btnEditTemplateResrc %>" OnClientClick="return CheckTemplates();"
             onclick="btnEditTemplate_Click" />
    <asp:Button class="button" ID="btnDeleteTemplate" runat="server" Text="<%$ Resources:btnDeleteTemplateResrc %>" OnClientClick="return DeleteTemplates();"
             onclick="btnDeleteTemplate_Click" />
    <asp:Button class="button" ID="btnCancel" runat="server" Text="<%$ Resources:btnBackToNotesResrc %>" 
             onclick="btnCancel_Click"/> 
     </div>
     <table>
     <tr>
     <td width="250px">
     </td>
     <td align="center"> 
     <asp:Button class="button" runat="server" ID="btnClose" Text="<%$ Resources:btnCancelResrc %>" OnClientClick="return CancelTemplates();" Visible="false" />
     </td>
     </tr>
    </table>
    </div>
    </form>
</body>
</html>
