﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.ProgressNoteService;
using Riskmaster.ServiceHelpers;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;

namespace Riskmaster.UI.ProgressNotes
{
    public partial class SelectClaim : NonFdmFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            //Amandeep MultiLingual Changes --start
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("SelectClaim.aspx"), "SelectClaimValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "SelectClaimValidationsScripts", sValidationResources, true);
            //Amandeep MultiLingual Changes --end

            if(!IsPostBack)
            {
                ProgressNoteBusinessHelper pn = null;
                SelectClaimObject selectClmObj = new SelectClaimObject();
                selectClmObj.EventID = AppHelper.GetQueryStringValue("EventID");
                //pmittal5 Mits 21514
                selectClmObj.ClaimID = AppHelper.GetQueryStringValue("ClaimID");
                selectClmObj.LOB = AppHelper.GetQueryStringValue("LOB");
                //selectClmObj.NewRecord = "false"; 
                selectClmObj.NewRecord = "AdvanceSearch";  //To show claims whose Enhanced Notes View permission is enabled
                //End - pmittal5
                try
                {
                    pn = new ProgressNoteBusinessHelper();
                    selectClmObj = pn.SelectClaim(selectClmObj);
                    GridView1.DataSource = selectClmObj.objClaimList;
                    GridView1.DataBind();
                   
                }
                catch (FaultException<RMException> ee)
                {
                    //ErrorHelper.logErrors(ee);
                    //ErrorControl1.errorDom = ee.Detail.Errors;

                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    //err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    //ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
        }
        protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandArgument == "claimNumber")
            {
                LinkButton lnkButton = (LinkButton)e.CommandSource;
                GridViewRow row = (GridViewRow)lnkButton.NamingContainer;
                string strClaimID = ((Label)row.FindControl("lblClaimID")).Text;
                string strClaimNumber = ((LinkButton)row.FindControl("lblClaimNumber")).Text;
                
                //Modified to no longer use obsolete RegisterClientScriptBlock method
                PageHelper.GetClientScriptManager(this.Page).RegisterClientScriptBlock(this.GetType(), "selectClaimScript", 
                    String.Format("<Script>CallParentPage(\"{0}\",\"{1}\")</Script>", strClaimID, strClaimNumber));
                Response.Write("GridView1_OnRowCommand");
            }
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowIndex != -1)
            {
                DateTime fieldDate = System.Xml.XmlConvert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "ClaimDate").ToString(), "yyyymmdd");
                ((Label)e.Row.FindControl("lblClaimDate")).Text = fieldDate.ToString("mm/dd/yyyy");
                //GridView1.Rows[e.Row.RowIndex].Cells[0].Text
                //((HtmlInputRadioButton)e.Row.FindControl("RowSelector")).Attributes.Add(
                //       "onclick","fnCheckSel('" + e.Row.RowIndex + "')"); 

                //Page.RegisterClientScriptBlock("ee", "<Script>NoteSelectionChanged()</Script>");
            }
        }
    }
}
