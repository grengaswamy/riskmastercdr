﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProgressNotesEditTemplates.aspx.cs"
    Inherits="Riskmaster.UI.ProgressNotes.ProgressNotesEditTemplates" ValidateRequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> </title>
    <link href="../../App_Themes/RMX_Default/rmnet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript" src="../../Scripts/form.js"></script>

    <script language="javascript" type="text/javascript" src="../../Scripts/ProgressNotes.js"></script>

    <script type="text/javascript" id="igClientScript">

        // MITS 27828: Ijha: 12/3/2012: The error message was not present in the code.
        window.onbeforeunload = closeIt;
        var m_IsSaved = false;

        function closeIt() {
            //var sMsg = "You are about to DISCARD UNSAVED CHANGES!";
            var sMsg = ProgressNotesEditTemplatesValidations.ValidDiscardChanges;
            if (m_DataChanged && !m_IsSaved) {
                if (window.event != null) {
                    window.event.returnValue = sMsg;
                }
            }
        }
        // Ijha End
        function WebHtmlEditor1_BeforeAction(oEditor, actID, oEvent, p4, p5, p6, p7, p8, act) {
            if (actID == 'Save') {
                if (document.forms[0].TemplateName.value == '') {
                    //alert("Please enter the Template Name.");
                    alert(ProgressNotesEditTemplatesValidations.ValidTemplateNameToEnter); //Aman ML Changes
                    document.forms[0].TemplateName.focus();
                    oEvent.needPostBack = false;
                }
                else {
                    m_IsSaved = true;
                    oEvent.needPostBack = true;
                }

            }

        }
        function WebHtmlEditor1_onclick() {
            if (document.forms[0].TemplateName.value == '') {
                //alert("Please enter the Template Name.");
                alert(ProgressNotesEditTemplatesValidations.ValidTemplateNameToEnter); //Aman ML Changes
                document.forms[0].TemplateName.focus();
            }
            else {
                document.forms[0].WebHtmlEditor_tb_ctl39_image.disabled = true;
                m_IsSaved = true;
                iged_act('Save:_0');

            }
        }
        function onload() {
            document.forms[0].WebHtmlEditor_tb_ctl39_image.onclick = WebHtmlEditor1_onclick;
            var oEditor = iged_getById("WebHtmlEditor");
            oEditor.setText(oEditor.getText());
        }

	function setTitle()
	{
	    document.title = document.getElementById("hdHeader").value;  //MITS 28588 hlv 8/3/12 .replace(/\[ampersand\]/gi, "&")
	} 
    </script>

</head>
<body onload="onload();tryMDIScreenLoaded();setTitle();">
    <form id="frmData" name="frmData" method="post" runat="server">
    <asp:HiddenField ID="SysFormName" runat="server" Value="progressnotes" />
    <asp:HiddenField ID="hdeventid" runat="server" />
    <asp:HiddenField ID="hdclaimid" runat="server" />
    <asp:HiddenField ID="hdeventnumber" runat="server" />
    <asp:HiddenField ID="hdNewRecord" runat="server" />
    <asp:HiddenField ID="hdnClaimant" runat="server" />
    <asp:HiddenField ID="TemplateId" runat="server" />
    <asp:HiddenField ID="claimant" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="eventid" runat="server" />
    <asp:HiddenField ID="claimid" runat="server" />
     <asp:HiddenField ID="policyid" runat="server" /> <!--williams:neha goel:MITS 21704---->
    <asp:HiddenField ID="policynumber" runat="server" /> <!--williams:neha goel:MITS 21704---->
    <asp:HiddenField ID="policyname" runat="server" /> <!--williams:neha goel:MITS 21704---->    
    <asp:HiddenField ID="LOB" runat="server" /> <!-- pmittal5 Mits 21514-->
    <asp:textbox runat="server" style="display:none" id="hdHeader" />  
    <asp:HiddenField ID="eventnumber" runat="server" />
    <asp:ScriptManager ID="SMgr" runat="server" />
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <ig_spell:WebSpellChecker ID="WebSpellChecker1" runat="server">
    </ig_spell:WebSpellChecker>
    <ighedit:WebHtmlEditor ID="WebHtmlEditor" Width="100%" Height="60%" runat="server"
        SpellCheckerID="WebSpellChecker1" UploadedFilesDirectory="/RiskmasterUI/UploaderTemp" UseLineBreak="True">
        <%--Added afteraction and keydown functions for MITS 27828 :Ishan--%>
        <ClientSideEvents KeyPress="m_DataChanged = true;" AfterAction="m_DataChanged = true;" KeyDown="m_DataChanged = true;" />
    </ighedit:WebHtmlEditor><!--Added UseLineBreak for Mits 22198:Default line spacing is double Enhanced Note Template.--> 
    <div class="full"><!--Changed class to full by Milli for Mits 22200-->
        <span class="required"><asp:Label ID="lblTemplateName" runat="server" Text="<%$ Resources:lblTemplateNameResrc %>"></asp:Label></span>
         <asp:TextBox runat="server" ID="TemplateName" onchange="setDataChanged(true);" MaxLength="30" /> <!--Added onchange event for Mits 18838--> 
        <asp:Label runat="server" ID="lblMaxLength" Text="(Max 30 characters)" Font-Size="Smaller"></asp:Label> <!-- rsharma220: MITS 37172 -->
       <asp:Button class="button" ID="btnCancelNotes" Text="<%$ Resources:btnBackToTemplatesResrc %>" 
            runat="server" OnClick="btnCancelTemplates_Click" /><!--Changed for Mits 18838--> <%--OnClientClick="return BackToNotes();" Parijat : Mits 20504 error on Cancel--%><!--Changed text by Milli for Mits 22200 -->
    </div>
    <div id="TemplateMemoDiv" runat="server" style="display: none" />
    </form>
</body>
</html>
