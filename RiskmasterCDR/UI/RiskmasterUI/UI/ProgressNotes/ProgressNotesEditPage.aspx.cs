﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.Common;
using System.Xml;
using System.Xml.XPath;
using Infragistics.WebUI.WebHtmlEditor;
//using Riskmaster.UI.ProgressNoteService;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.IO;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Text.RegularExpressions;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;

namespace Riskmaster.UI.NonFDMCWS
{
    public partial class ProgressNotesEditPage : NonFDMBasePageCWS
    {
        XmlDocument oFDMPageDom = null;
        //Praveen ML Changes
        //private string m_BlankNoteError = "Please enter note text before save.";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <author>Raman Bhatia</author>        
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //Praveen: ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture,this);
                }
            //Praveen: ML Changes
            
            if (AppHelper.GetValue("formsubtitle") != String.Empty)
            {
                this.hdHeader.Text = AppHelper.GetValue("formsubtitle");
                //this.hdHeader.Text = AppHelper.HTMLCustomEncode(AppHelper.GetValue("formsubtitle")); //hlv MITS 28588 11/12/12
                //msampathkuma jira-11851
                if (this.hdHeader.Text.Contains("^@"))
                {
                    this.hdHeader.Text = AppHelper.HTMLCustomEncode(AppHelper.GetValue("formsubtitle"));
                }
                else
                {
                    this.hdHeader.Text = AppHelper.HTMLCustomDecode(AppHelper.GetValue("formsubtitle"));//msampathkuma jira-11170
                }
            }
            oFDMPageDom = new XmlDocument();
            string sReturn = "";
            this.WebSpellChecker1.UserDictionaryFile = Server.MapPath("../../App_Data/C1SP_AE.dct");
            this.WebSpellChecker1.SpellOptions.IncludeUserDictionaryInSuggestions = true;
            this.WebHtmlEditor.ToolbarClick += new Infragistics.WebUI.WebHtmlEditor.ToolbarClickEventHandler(WebHtmlEditor_ToolbarClick);
            //Support for adding images in notes..TO DO: discuss deployment and make changes accordingly
            //this.WebHtmlEditor.UploadedFilesDirectory = "../../Images/ProgressNotesImages";
            this.WebSpellChecker1.DialogOptions.ShowFinishedMessage = true;
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.SpellCheck))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.SpellCheck);
            }
            this.WebHtmlEditor.AddToolbarItem(ToolbarItemType.SpellCheck);

            //Changed for MITS 19670 by Gagan : start
            //Hiding Unnecessary buttons in Enhanced Notes Editor
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.InsertImage))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.InsertImage);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.InsertFlash))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.InsertFlash);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.InsertWindowsMedia))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.InsertWindowsMedia);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.InsertBookmark))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.InsertBookmark);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.Open))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.Open);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.Help))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.Help);
            }
            if (this.WebHtmlEditor.Toolbar.Items.Contains(ToolbarItemType.CleanWord))
            {
                this.WebHtmlEditor.Toolbar.Items.Remove(ToolbarItemType.CleanWord);
            }
            //Changed for MITS 19670 by Gagan : end

            string sClaimProgressNoteId = "";
            hdIsNotPostback.Value = "False";//Parijat: Mits 17775
            //smishra54, 31Aug 2011: MITS 25641
            //nsachdeva2: 09/22/2011 - commented existing change.
            //btnCreateAnotherNote.Visible = false;
            //smishra54: End

            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ProgressNotesEditPage.aspx"), "ProgressNotesEditPageValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "ProgressNotesEditPageValidationsScripts", sValidationResources, true);

            if (!Page.IsPostBack)
            {
                

                hdIsNotPostback.Value = "True";//Parijat: Mits 17775
                //Parse the querystring and fill the form accordingly
                bool bNewRecord = (ViewState["NewRecord"]!=null)?(Conversion.ConvertStrToBool(ViewState["NewRecord"].ToString())): (Conversion.ConvertStrToBool(AppHelper.GetQueryStringValue("NewRecord")));
                // pen testing changes :atavaragiri mits 27832

                //activitydate.Text = AppHelper.GetQueryStringValue("ActivityDate");
                //eventid.Value = AppHelper.GetQueryStringValue("EventID");
                //hdeventnumber.Value = AppHelper.GetQueryStringValue("EventNumber");
                ////wiliams-neha goel
                //hdpolicyid.Value = AppHelper.GetQueryStringValue("PolicyId");
                //hdpolicyname.Value = AppHelper.GetQueryStringValue("PolicyName");
                //hdformname.Value = AppHelper.GetQueryStringValue("FormName");
                //hdpolicynumber.Value = AppHelper.GetQueryStringValue("PolicyNumber");
                //NoteParentType.Value = AppHelper.GetQueryStringValue("CodeId");
                ////wiliams-neha goel
                //hdNewRecord.Value = bNewRecord.ToString();
                //hdclaimid.Value = AppHelper.GetQueryStringValue("ClaimID");
                ////changes done for 12334
                //hdFreezeText.Value = AppHelper.GetQueryStringValue("FreezeText");
                //hdUserName.Value = AppHelper.GetQueryStringValue("UserName");
                //hdLob.Value = AppHelper.GetQueryStringValue("SysFormName");  //csingh7 :MITS 19333
                ////Changed by Gagan for mits 14626 : start
                //hdShowDateStamp.Value = AppHelper.GetQueryStringValue("ShowDateStamp");
                
                //Changed by Gagan for mits 14626 : end
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("ActivityDate")))
                    activitydate.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ActivityDate"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("EventID")))
                    eventid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("EventID"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("EventNumber")))
                    hdeventnumber.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("EventNumber"));
                //wiliams-neha goel
                 hdpolicyid.Value = AppHelper.GetQueryStringValue("PolicyId");
                //if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicyId")))
                //    hdpolicyid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolicyId"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicyName")))
                    hdpolicyname.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolicyName"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("FormName")))
                    hdformname.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("FormName"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicyNumber")))
                    hdpolicynumber.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolicyNumber"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("CodeId")))
                    NoteParentType.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("CodeId"));
                //wiliams-neha goel
                hdNewRecord.Value = bNewRecord.ToString();
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("ClaimID")))
                    hdclaimid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimID"));
                //changes done for 12334
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("FreezeText")))
                    hdFreezeText.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("FreezeText"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("UserName")))
                    hdUserName.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("UserName"));
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("SysFormName")))
                    hdLob.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("SysFormName"));  //csingh7 :MITS 19333
                //Changed by Gagan for mits 14626 : start
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("ShowDateStamp")))
                    hdShowDateStamp.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ShowDateStamp"));
                //Changed by Gagan for mits 14626 : end

                //END : pen testing changes :atavaragiri mits 27832
                //added by Nitin for Mits 15988 on 29-Apr-2009
                //setting style of buttons if this page is opens from view all records page
                if (AppHelper.GetQueryStringValue("ShowBack") != string.Empty)
                {
                    if (AppHelper.GetQueryStringValue("ShowBack") == "true")
                    {
                        btnBackToAllRecrods.Visible = true;
                        btnBackToNotes.Visible = false;
                        btnCancelNotes.Visible = true;
                    }
                    else
                    {
                        btnBackToAllRecrods.Visible = false;
                        btnBackToNotes.Visible = true;
                        btnCancelNotes.Visible = false;
                    }
                }
                else
                {
                    btnBackToAllRecrods.Visible = false;
                    btnBackToNotes.Visible = true;
                    btnCancelNotes.Visible = false;
                }
                //AMAN MITS 27310--Start 
                if (AppHelper.GetQueryStringValue("CreateNotePermission") != string.Empty && AppHelper.GetQueryStringValue("PolicyTrackingCreatePermission") != string.Empty)
                {
                    if (Convert.ToBoolean(AppHelper.GetQueryStringValue("CreateNotePermission")) || Convert.ToBoolean(AppHelper.GetQueryStringValue("PolicyTrackingCreatePermission")))
                    {
                        btnCreateAnotherNote.Enabled = true;                       
                    }
                    else
                    {
                        btnCreateAnotherNote.Enabled = false;
                    }
                }
                //AMAN MITS 27310--End
                /*Added by gbindra MITS#34104 WWIG GAP 15 START*/
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("ClaimantId")))
                    hdclaimantid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimantId"));
                /*Added by gbindra MITS#34104 WWIG GAP 15 END*/
                //end by Nitin for Mits 15988 on 29-Apr-2009
                //SysFormName.Value = AppHelper.GetQueryStringValue("SysFormName");//Parijat: EnhancedMDI
                //changes done for 12334
                #region CWS
                
                ////Preparing XML to send to service
                //XElement oMessageElement = GetSelectClaimMessageTemplate();

                ////Modify XML 
                //XElement oEventID = oMessageElement.XPathSelectElement("./Document/ClaimProgressNotes/EventID");
                //if (oEventID != null)
                //{
                //    oEventID.Value = hdeventid.Value;
                //}

                //XElement oNewRecord = oMessageElement.XPathSelectElement("./Document/ClaimProgressNotes/NewRecord");
                //if (oNewRecord != null)
                //{
                //    oNewRecord.Value = hdNewRecord.Value;
                //}

                ////Calling Service to get all PreBinded Data 
                //CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

                ////Binding Error Control
                //ErrorControl.errorDom = sReturn;
                #endregion
                #region WCF---Parijat
                ProgressNoteBusinessHelper pn = null;
                SelectClaimObject selectClmObj = new SelectClaimObject();
                SelectClaimantObject selectClmntObj = new SelectClaimantObject();//Added by GBINDRA MITS#34104 02062014 GAP15
                selectClmObj.EventID = eventid.Value;
                selectClmObj.NewRecord = hdNewRecord.Value;
                //pmittal5 Mits 21514 - Pass ClaimID and LOB to SelectclaimObject to check Enhanced notes permissions
                selectClmObj.ClaimID = hdclaimid.Value;
                hdLOBCode.Value = AppHelper.GetQueryStringValue("LOB");
                selectClmObj.LOB = hdLOBCode.Value;
                selectClmObj.SelectEvent = true;
                selectClmObj.PolicyID = hdpolicyid.Value;
                //End - pmittal5
                /*Added by GBINDRA MITS#34104 02062014 GAP15 START*/
                selectClmntObj.EventID = eventid.Value;
                selectClmntObj.NewRecord = hdNewRecord.Value;
                selectClmntObj.ClaimID = hdclaimid.Value;
                hdLOBCode.Value = AppHelper.GetQueryStringValue("LOB");
                selectClmntObj.LOB = hdLOBCode.Value;
                selectClmntObj.SelectEvent = true;
                selectClmntObj.PolicyID = hdpolicyid.Value;

                /*Added by GBINDRA MITS#34104 02062014 GAP15 END*/
                try
                {
                    pn = new ProgressNoteBusinessHelper();
                    selectClmObj = pn.SelectClaim(selectClmObj);
                    selectClmntObj = pn.SelectClaimant(selectClmntObj);//Added by GBINDRA MITS#34104 02062014 GAP15 
                    //Start rsushilaggar MITS 21119 06/18/2010
                    if (!selectClmObj.bViewTemplates)
                    {
                        TemplateName.Enabled = false;
                        TemplateNamebtn.Enabled = false;
                    }
                    else
                    {
                        TemplateName.Enabled = true;
                        TemplateNamebtn.Enabled = true;
                    }
                    //End rsushilaggar MITS 21119 06/18/2010
                    
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorHelper.logErrors(ee);
                    ErrorControl.errorDom = ee.Detail.Errors;

                    //Changed by Gagan for MITS 15503
                    return;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);


                    //Changed by Gagan for MITS 15503
                    return;
                }
                #endregion
                //Populating Attached To DropDownList
               // PopulateAttachedToDropDownList(selectClmObj, eventid.Value, hdeventnumber.Value, hdclaimid.Value);
                //williams-neha goel:MITS 21704
                PopulateAttachedToDropDownList(selectClmObj, eventid.Value, hdeventnumber.Value, hdclaimid.Value, hdpolicyid.Value, hdpolicyname.Value, hdLob.Value, hdpolicynumber.Value, selectClmntObj); //selectClmntObj Added by gbindra MITS#34104 WWIG GAP 15
                //williams:MITS 21704-neha goel

                if (bNewRecord)
                {
                    activitydate.Text = AppHelper.GetDate(System.DateTime.Now.ToShortDateString());
                    //changed for mits  15879 : start
                    //changes done for 12334
                    //WebHtmlEditor.Text = string.Format("{0:G}", DateTime.Now) + " (" + hdUserName.Value + ")";
                    //changes done for 12334
                    //changed for mits  15879 : end

                    GetNoteDetailsObject objNoteDetails = new GetNoteDetailsObject();
                    sClaimProgressNoteId = "0";
                    objNoteDetails.ClaimProgressNoteId = sClaimProgressNoteId;
                    hdClaimProgressNoteId.Value = sClaimProgressNoteId;                   
                    try
                    {
                        pn = new ProgressNoteBusinessHelper();
                        objNoteDetails = pn.GetNoteDetails(objNoteDetails);

                    }
                    catch (FaultException<RMException> ee)
                    {
                        ErrorHelper.logErrors(ee);
                        ErrorControl.errorDom = ee.Detail.Errors;

                    }
                    catch (Exception ee)
                    {
                        ErrorHelper.logErrors(ee);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ee, BusinessAdaptorErrorType.SystemError);
                        ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                    }

                    if (objNoteDetails.DataChangedByInitScript)
                    {
                        SysPageDataChanged.Value = "true";
                        WebHtmlEditor.Text = objNoteDetails.HTML;

                        ((TextBox)lstNoteTypes.FindControl("codelookup")).Text = objNoteDetails.NoteTypeDesc;
                        ((TextBox)lstNoteTypes.FindControl("codelookup_cid")).Text = objNoteDetails.NoteTypeCode;

                        hdnLastValue.Value = objNoteDetails.NoteTypeDesc;
                        hdnLastValue_cid.Value = objNoteDetails.NoteTypeCode;

                        if (objNoteDetails.ActivityDate != string.Empty)
                        {
                            activitydate.Text = AppHelper.GetDate(objNoteDetails.ActivityDate);
                        }
                       
                        TemplateName.Text = objNoteDetails.TemplateName;
                        TemplateName_cid.Text = objNoteDetails.TemplateId;
                        TempName.Text = TemplateName.Text;

                        if (objNoteDetails.TemplateId != string.Empty && objNoteDetails.TemplateId != "0")
                        {
                            GetTemplateDetails(sender, e);
                        }
                    }
                }
                else
                {
                    //MGaba2:MITS 17027: Apostrophe was creating javascript errors
                    //((TextBox)lstNoteTypes.FindControl("codelookup")).Text = AppHelper.GetQueryStringValue("NoteType");
                    ((TextBox)lstNoteTypes.FindControl("codelookup")).Text = (AppHelper.GetQueryStringValue("NoteType")).Replace("~~*~~", "'");
                    ((TextBox)lstNoteTypes.FindControl("codelookup_cid")).Text = AppHelper.GetQueryStringValue("NoteTypecodeid");
                    //Parijat:11587--restricting save in case save is clicked just after entering notetype
                    hdnLastValue.Value = ((TextBox)lstNoteTypes.FindControl("codelookup")).Text;
                    hdnLastValue_cid.Value = ((TextBox)lstNoteTypes.FindControl("codelookup_cid")).Text;
                    //Parijat:11587--restricting save in case save is clicked just after entering notetype
                    attachedto.Enabled = false;
                    activitydate.Enabled = false;
                    subject.Text = HttpUtility.UrlDecode(AppHelper.GetQueryStringValue("Subject")); //zmohammad MITS 30218,33446
                    //MGaba2:MITS 19287
                    //activitydatebtn.Enabled = false;
                    
                    //Fetch HTML and Text from Database
                    //Parijat-- Changes inorder to incorporate the save functionality of new record
                    sClaimProgressNoteId = (ViewState["ClaimProgressNoteId"]!=null)?(ViewState["ClaimProgressNoteId"].ToString()): (AppHelper.GetQueryStringValue("ClaimProgressNoteId"));

                    hdClaimProgressNoteId.Value = sClaimProgressNoteId;
                    //Added Rakhi for R6-Progress Notes Templates
                    TemplateName.Text = AppHelper.GetQueryStringValue("TemplateName").Replace("~~*~~", "'"); //Added for Mits 18897
                    // pen testing changes :atavaragiri mits 27832
                     TemplateName_cid.Text = AppHelper.GetQueryStringValue("TemplateId"); 
                    //if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("TemplateId")))
                    //   TemplateName_cid.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("TemplateId"));
                    // END :pen testing changes :atavaragiri mits 27832
                    //Added Rakhi for R6-Progress Notes Templates
                    #region CWS
                    ////Preparing XML to send to service
                    //oMessageElement = GetNoteDetailsMessageTemplate();

                    ////Modify XML 
                    //XElement oClaimProgressNoteId = oMessageElement.XPathSelectElement("./Document/ClaimProgressNotes/ClaimProgressNoteId");
                    //if (oClaimProgressNoteId != null)
                    //{
                    //    oClaimProgressNoteId.Value = sClaimProgressNoteId;
                    //}

                    ////Calling Service to get all PreBinded Data 
                    //CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

                    ////Binding Error Control
                    //ErrorControl.errorDom = sReturn;
                    #endregion
                    #region WCF---Parijat
                    
                    GetNoteDetailsObject objNoteDetails = new GetNoteDetailsObject();
                    objNoteDetails.ClaimProgressNoteId = sClaimProgressNoteId;
                    
                    try
                    {
                        pn = new ProgressNoteBusinessHelper();
                        objNoteDetails = pn.GetNoteDetails(objNoteDetails);
                        
                    }
                    catch (FaultException<RMException> ee)
                    {
                        ErrorHelper.logErrors(ee);
                        ErrorControl.errorDom = ee.Detail.Errors;

                    }
                    catch (Exception ee)
                    {
                        ErrorHelper.logErrors(ee);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ee, BusinessAdaptorErrorType.SystemError);
                        ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                    }
                    #endregion

                    //changes done for 12334
                    if (hdFreezeText.Value.ToUpper() != "TRUE")
                    {
                        if (objNoteDetails.HTML != null)
                        {
                            //changed for mits  15879 : start
                            WebHtmlEditor.Text = objNoteDetails.HTML;// +"<br/><br/>" + string.Format("{0:G}", DateTime.Now) + " (" + hdUserName.Value + ")";
                            //changed for mits  15879 : end
                        }
                    }
                    else
                    {
                        WebHtmlEditor.Height = Unit.Percentage(40);
                        //changed for mits  15879 : start
                        //WebHtmlEditor.Text = string.Format("{0:G}", DateTime.Now) + " (" + hdUserName.Value + ")";
                        //changed for mits  15879 : end
                       txtExistingComments.InnerHtml = objNoteDetails.HTML;//+ "<br/><br/>" + string.Format("{0:G}",DateTime.Now) + " (" + hdUserName.Value + ")"; 
			        }

                    //changes done for 12334
                }

            }
           
        }
        /// <summary>
        /// Handles the click events of toolbar button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void WebHtmlEditor_ToolbarClick(object sender, ToolbarEventArgs e)
        {
            string sReturn = "";
            bool bSuccess = false;// Added by gbindra MITS#34104 WWIG GAP 15
            if (e.Item.ToString() == ToolbarItemType.Save.ToString())
            {
                if (hdnRestrictSave.Value != "false")// to restrict save in case of validation: 12587
                {
                //SAVE THE DATA
                #region CWS
                ////Preparing XML to send to service
                //XElement oMessageElement = SaveNotesMessageTemplate();

                ////Modify XML 
                //XElement oEle = oMessageElement.XPathSelectElement("./Document/ProgressNotes/ClaimProgressNoteId");
                //if (oEle != null)
                //{
                //    oEle.Value = hdClaimProgressNoteId.Value;
                //}

                //oEle = oMessageElement.XPathSelectElement("./Document/ProgressNotes/EventID");
                //if (oEle != null)
                //{
                //    oEle.Value = hdeventid.Value;
                //}

                //oEle = oMessageElement.XPathSelectElement("./Document/ProgressNotes/ClaimID");
                //if (oEle != null)
                //{
                //    //Check whether its a new record or old
                //    if (Conversion.ConvertStrToBool(hdNewRecord.Value))
                //    {

                //        //ClaimID would be decided based on attached-to dropdownlist
                //        if (attachedto.SelectedValue.StartsWith("event"))
                //        {
                //            oEle.Value = "0";
                //        }
                //        else
                //        {
                //            oEle.Value = attachedto.SelectedValue.Substring(5);
                //        }
                //    }
                //    else
                //    {
                //        oEle.Value = hdclaimid.Value;
                //    }
                
                //}


                //oEle = oMessageElement.XPathSelectElement("./Document/ProgressNotes/DateEntered");
                //if (oEle != null)
                //{
                //    oEle.Value = activitydate.Text;
                //}

                //oEle = oMessageElement.XPathSelectElement("./Document/ProgressNotes/NewRecord");
                //if (oEle != null)
                //{
                //    oEle.Value = hdNewRecord.Value;
                //}

                //oEle = oMessageElement.XPathSelectElement("./Document/ProgressNotes/NoteMemo");
                //if (oEle != null)
                //{
                //    oEle.Value = WebHtmlEditor.Text;
                //}

                //oEle = oMessageElement.XPathSelectElement("./Document/ProgressNotes/NoteMemoCareTech");
                //if (oEle != null)
                //{
                //    oEle.Value = WebHtmlEditor.TextPlain;
                //}

                //oEle = oMessageElement.XPathSelectElement("./Document/ProgressNotes/NoteTypeCode");
                //if (oEle != null)
                //{
                //    oEle.Value =((TextBox)lstNoteTypes.FindControl("codelookup_cid")).Text;
                //}


                ////Calling Service to get all PreBinded Data 
                //CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

                ////Binding Error Control
                //ErrorControl.errorDom = sReturn;
                #endregion
                #region WCF---Parijat
                ProgressNoteBusinessHelper pn;
                ProgressNote objNote = new ProgressNote();
                if(hdClaimProgressNoteId.Value != "")
                    objNote.ClaimProgressNoteId =Convert.ToInt32(hdClaimProgressNoteId.Value);
               
                objNote.EventID =eventid.Value;
                //williams:MITS 21704-neha goel--start
                objNote.PolicyID = hdpolicyid.Value;

                //added by Nitin for Mits 15985 on 30-Apr-2009
                if (Conversion.ConvertStrToBool(hdNewRecord.Value))
                {
                    //ClaimID would be decided based on attached-to dropdownlist
                    //williams:MITS 21704-neha goel-added or condition of policy
                    //if (attachedto.SelectedValue.StartsWith("event") )
                    if (attachedto.SelectedValue.StartsWith("event") || attachedto.SelectedValue.StartsWith("policy"))
                    {
                        objNote.ClaimID = "0";
                        /*added by gbindra MITS#34104 02112014 WWIG GAP15 START*/
                        if (attachedto.SelectedValue.StartsWith("event"))
                        {
                            objNote.AttachedRecordId = Conversion.CastToType<int>(eventid.Value, out bSuccess);
                            objNote.AttachedTable = "EVENT";
                        }
                        else
                        {
                            objNote.AttachedRecordId = Conversion.CastToType<int>(hdpolicyid.Value, out bSuccess);
                            objNote.AttachedTable = "POLICY";
                        }
                    }
                    else
                    {
                        /*added by gbindra MITS#34104 02112014 WWIG GAP15 START*/
                        if (attachedto.SelectedValue.StartsWith("claimant", StringComparison.InvariantCultureIgnoreCase))
                        {
                            objNote.EventID = eventid.Value;
                            objNote.ClaimID = "0";
                            objNote.AttachedTable = "CLAIMANT";
                            objNote.AttachedRecordId = Conversion.CastToType<int>(hdclaimantid.Value, out bSuccess);
                        }
                        else 
                        {
                        objNote.ClaimID = attachedto.SelectedValue.Substring(5);
                            objNote.AttachedRecordId = Conversion.CastToType<int>(hdclaimid.Value, out bSuccess);
                            objNote.AttachedTable = "CLAIM";
                        }
                        /*added by gbindra MITS#34104 02112014 WWIG GAP15 END*/
                    }
                }
                else
                {
                    objNote.ClaimID = hdclaimid.Value;
                    /*Added by gbindra MITS#34104 WWIG GAP15 START*/
                    //rsharma220 MITS 37837 
                    if ((Conversion.CastToType<int>(hdclaimantid.Value, out bSuccess) > 0) && attachedto.SelectedValue.StartsWith("claimant", StringComparison.InvariantCultureIgnoreCase))
                    {
                        objNote.AttachedRecordId = Conversion.CastToType<int>(hdclaimantid.Value, out bSuccess);
                        objNote.AttachedTable = "CLAIMANT";
                    }
                    else if ((Conversion.CastToType<int>(hdclaimid.Value, out bSuccess) > 0) && attachedto.SelectedValue.StartsWith("claim", StringComparison.InvariantCultureIgnoreCase))
                    {
                        objNote.AttachedRecordId = Conversion.CastToType<int>(hdclaimid.Value, out bSuccess);
                        objNote.AttachedTable = "CLAIM";
                    }
                    else if ((Conversion.CastToType<int>(eventid.Value, out bSuccess) > 0) && attachedto.SelectedValue.StartsWith("event", StringComparison.InvariantCultureIgnoreCase))
                    {
                        objNote.AttachedRecordId = Conversion.CastToType<int>(eventid.Value, out bSuccess);
                        objNote.AttachedTable = "EVENT";
                    }
                }
                
                //MGaba2: MITS 19287:In case User make date field blank,it should be replaced by system date
                if (activitydate.Text == "")
                {
                    activitydate.Text = AppHelper.GetDate(System.DateTime.Now.ToShortDateString());
                }
                objNote.DateEntered = AppHelper.GetDateInenUS(activitydate.Text);//Deb ML Changes
                objNote.NewRecord = hdNewRecord.Value;
                //akaushik5 Changed for MITS 30063 Starts
                if (!string.IsNullOrEmpty(WebHtmlEditor.Text.Trim()))
                {
                    if (WebHtmlEditor.Text.Trim().Contains("<P>"))
                    {
                        WebHtmlEditor.Text = WebHtmlEditor.Text.Trim().Replace("<P>", "<P class='pclass'>");
                    }

                    if (WebHtmlEditor.Text.Trim().Contains("<p>"))
                    {
                        WebHtmlEditor.Text = WebHtmlEditor.Text.Trim().Replace("<p>", "<p class='pclass'>");
                    }
                }
                //akaushik5 Changed for MITS 30063 Ends
                objNote.NoteMemo = WebHtmlEditor.Text.Trim();
                objNote.Subject = subject.Text; //zmohammad MITS 30218
				
				//If text is empty, try to get from the hidden field and log it. MITS 24745
                if (string.IsNullOrEmpty(objNote.NoteMemo))
                {
                    string sClaimUserInfo = GetClaimClaimInfo();
                    string sHtmlText = hdTextHtml.Value;
                    if (string.IsNullOrEmpty(sHtmlText))
                    {
                        ErrorHelper.Write(sClaimUserInfo + " " + m_BlankNoteError.Value);
                        ErrorControl.DisplayError(m_BlankNoteError.Value);
                        return;
                    }

                    WebHtmlEditor.Text = sHtmlText;
                    objNote.NoteMemo = sHtmlText;
                    ErrorHelper.Write(sClaimUserInfo + " WebHtmlEditor failed to pass value back to server.");
                    ErrorHelper.Write(sClaimUserInfo + " Note Text: " +sHtmlText);
                }
                //objNote.NoteMemoCareTech = hdPlainText.Value;//StripHTMLTags(WebHtmlEditor.TextPlain);//WebHtmlEditor.TextPlain.Replace("&nbsp;", " ");//Parijat : Mits 19481 & 19041
				 objNote.NoteMemoCareTech = hdPlainText.Value;//Deb : MITS 25510
			   	//If type space, it show up as "&nbsp;" in the plain text. If all the content only contain space, it
                //should be rejected MITS 24745
                string sTextPlain = objNote.NoteMemoCareTech.Trim();
                sTextPlain = sTextPlain.Replace("&nbsp;", "");

                //Add check if Note is empty or not MITS 24745
                if (string.IsNullOrEmpty(objNote.NoteMemo) || string.IsNullOrEmpty(objNote.NoteMemoCareTech)||string.IsNullOrEmpty(sTextPlain))
                {
                    ErrorHelper.Write(m_BlankNoteError.Value);
                    ErrorControl.DisplayError(m_BlankNoteError.Value);
                    return;
                }

                objNote.NoteTypeCode = ((TextBox)lstNoteTypes.FindControl("codelookup_cid")).Text;
                //Added Rakhi for R6-Progress Notes Templates
                if (TemplateName_cid.Text != "")
                    objNote.TemplateID = Convert.ToInt32(TemplateName_cid.Text);
                else
                    objNote.TemplateID = 0;
                //Added Rakhi for R6-Progress Notes Templates
                try
                {
                    pn = new ProgressNoteBusinessHelper();
                    ErrorControl.Text = string.Empty;
                    objNote = pn.SaveNotes(objNote);
                    if (ErrorControl.errorFlag)
                    {
                        SysPageDataChanged.Value = "true";
                    }
                    else
                    {
                        SysPageDataChanged.Value = "false";
                    }
                    //smishra54, 31Aug 2011: MITS 25641
                    //nsachdeva2: 09/22/2011 - commented below changes. Not required as per new requirement. 
                    //if (string.Equals(hdNewRecord.Value,"TRUE",StringComparison.InvariantCultureIgnoreCase))
                    //{
                    //    btnCreateAnotherNote.Visible = true;
                    //}
                    //smishra54: End

                    //Parijat- In order to Incorporate the save of New enhanced note functionality over and over again 
                    ViewState["NewRecord"] = "false";
                    ViewState["ClaimProgressNoteId"] = Convert.ToString(objNote.ClaimProgressNoteId);
                    hdNewRecord.Value = "false";
                    //Request.QueryString["ClaimProgressNoteId"] = Convert.ToString(objNote.ClaimProgressNoteId);
                    hdClaimProgressNoteId.Value =Convert.ToString(objNote.ClaimProgressNoteId);
                   //string str = "<script>window.opener.document.location.reload(true);</script>";
                    //changes for 12334
                    if (hdNewRecord.Value.ToUpper() != "TRUE" && hdFreezeText.Value.ToUpper() == "TRUE")
                    {
                        //smishra25 MITS 18433; Datetime stamp would be added only for Date stamp free text setting in Utilities
                        //not for freeze existing notes setting.  
                        WebHtmlEditor.Text = "";
                        WebHtmlEditor.Height = Unit.Percentage(40);
                        //WebHtmlEditor.Text = string.Format("{0:G}", DateTime.Now) + " (" + hdUserName.Value + ")";
                        txtExistingComments.InnerHtml += "<br /><br />  "+ objNote.NoteMemo ;
                        
                    }
                    //changes for 12334                       //alert for 14132
                    attachedto.Enabled = false;
                   // string str = "<script>alert('Your comments have been saved');</script>"; //window.opener.document.forms[0].txtSubmit.value = \"SUBMIT\";window.opener.document.forms[0].submit();//Parijat: EnhancedMDI
                    string sMessage = RMXResourceProvider.GetSpecificObject("ValidCommentsSaved", RMXResourceProvider.PageId("ProgressNotesEditPage.aspx"), ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                    //string str = "<script>alert('" + sMessage + "');</script>";
                    string str = String.Format("<script>alert('{0}');</script>", sMessage);
                    RegisterClientScriptBlock("", str);
                   
                    ErrorControl.Text = "";
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorHelper.logErrors(ee);
                    ErrorControl.errorDom = ee.Detail.Errors;

                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
                #endregion
                }
                else//12587
                {
                    hdnRestrictSave.Value = "true";
                }
            }
            
        }

       	/// <summary>
        /// Get the login name/claim id/event number MITS 24745
        /// </summary>
        /// <returns></returns>
        private string GetClaimClaimInfo()
        {
            StringBuilder sbText = new StringBuilder();
            sbText.Append(string.Format("Login User: {0}, ", AppHelper.GetUserLoginName()));
            string sClaimID = hdclaimid.Value;
            if (sClaimID != "0")
            {
                sbText.Append(string.Format("Claim ID: {0}, ", sClaimID));
            }

            string sEventNumber = hdeventnumber.Value;
            sbText.Append(string.Format("Event Number: {0}, ", sEventNumber));

            return sbText.ToString();
        }

        protected string StripHTMLTags(string InString) //Parijat :19880 --Strip HTML tags and creating Plain text
        {
        //debugger;
        //alert(InString);
        string cleanedbuffer = "";
        string PatternString = "\\<[^<>]+\\>";
        Regex re = new Regex(PatternString,RegexOptions.CultureInvariant|RegexOptions.IgnoreCase|RegexOptions.Multiline);
        cleanedbuffer = re.Replace(InString,"");//InString.Replace(re, "");

        //replace special characters
        PatternString  = "(&nbsp;)|(&quot;)|(&ldquo;)|(&rdquo;)|(&lsquo;)|(&rsquo;)|(&amp;)|(&gt;)|(&lt;)";
        re = new Regex(PatternString,RegexOptions.CultureInvariant|RegexOptions.IgnoreCase|RegexOptions.Multiline);
        cleanedbuffer = re.Replace(cleanedbuffer, " ");//cleanedbuffer.Replace(//gim, ' ');
        //alert(cleanedbuffer);
        return cleanedbuffer;
       }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetSelectClaimMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>ProgressNotesAdaptor.SelectClaim</Function> 
                      </Call>
                     <Document>
                         <ClaimProgressNotes>
                            <EventID></EventID> 
                            <NewRecord></NewRecord> 
                         </ClaimProgressNotes>
                     </Document>
                </Message>


            ");
            return oTemplate;
        }
        /// <summary>
        /// CWS request message template for GetNoteDetails
        /// </summary>
        /// <returns></returns>
        private XElement GetNoteDetailsMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>ProgressNotesAdaptor.GetNoteDetails</Function> 
                      </Call>
                     <Document>
                         <ClaimProgressNotes>
                            <ClaimProgressNoteId></ClaimProgressNoteId> 
                         </ClaimProgressNotes>
                     </Document>
                </Message>


            ");
            return oTemplate;
        }
        /// <summary>
        /// CWS request message template for SaveNotes
        /// </summary>
        /// <returns></returns>
        private XElement SaveNotesMessageTemplate()
        {
            //williams:MITS 21704-added for policyid node-neha goel---
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                      <Authorization>ab1fd445 97d6 476f b956 c7d61dc9e124</Authorization> 
                      <Call>
                        <Function>ProgressNotesAdaptor.SaveNotes</Function> 
                      </Call>
                      <Document>
                          <ProgressNotes>
                              <ClaimID></ClaimID> 
                              <EventID></EventID> 
                              <PolicyID></PolicyID> 
                              <DateEntered /> 
                              <NewRecord></NewRecord> 
                              <NoteMemo></NoteMemo> 
                              <NoteMemoCareTech></NoteMemoCareTech> 
                              <NoteTypeCode></NoteTypeCode> 
                              <ClaimProgressNoteId></ClaimProgressNoteId> 
                          </ProgressNotes>
                      </Document>
                  </Message>


            ");
            return oTemplate;
        }
        

        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        private void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }
        ////Parijat:EnhancedMDI
        //void BacktoNotes(object sender, EventArgs e)
        //{
        //    Response.Redirect("/RiskmasterUI/UI/ProgressNotes/ProgressNotes.aspx?EventID=" + hdeventid.Value + "&ClaimID=" + hdclaimid.Value + "&FormName=" + AppHelper.GetQueryStringValue("SysFormName") + "&EventNumber=" + hdeventnumber.Value);
        
        //}

        //Added by Nitin for EnhancedMDI 
        protected void btnBackToNotes_Click(object sender, EventArgs e)
        {
//rsolanki2: Enhc notes ajax updates
            //// pen testing changes :atavaragiri mits 27832

           string shdCurrentNode = String.Empty;
           // string shdCurrentNode = AppHelper.GetQueryStringValue("hdCurrentNode");
           if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("hdCurrentNode")))
           {
               shdCurrentNode = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("hdCurrentNode"));
           }
            
           // string shdPage = AppHelper.GetQueryStringValue("PageNumber");
            string shdPage=String.Empty;
            if(!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("PageNumber")))
            {
                shdPage = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PageNumber"));
            }
           //  END :pen testing changes :atavaragiri mits 27832
            
            if (string.IsNullOrEmpty(shdPage)) shdPage = "1";

            string sClaimIdToPass = hdclaimid.Value;
            
            // pen testing changes :atavaragiri mits 27832
           // string sFormName = AppHelper.GetQueryStringValue("SysFormName");
            string sFormName=String.Empty;
            if(!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("SysFormName")))
             sFormName = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("SysFormName"));  
            // // END :pen testing changes
            if (sFormName == "event" && sClaimIdToPass!="0")
            {
                sClaimIdToPass = "0";
            }

            if (!string.IsNullOrEmpty(shdCurrentNode))
            {
                Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotes.aspx?EventID=" + eventid.Value +
                "&ClaimID=" + sClaimIdToPass +
                "&LOB=" + hdLOBCode.Value +  //pmittal5 Mits 21514
                "&FormName=" + sFormName +
                "&hdCurrentNode=" + shdCurrentNode +
                "&PageNumber=" + shdPage +
                "&EventNumber=" + hdeventnumber.Value +
                    //williams-nehagoel:MITS 21704-start
                "&PolicyId=" + hdpolicyid.Value + 
                "&PolicyName=" + hdpolicyname.Value +
                "&PolicyNumber=" + hdpolicynumber.Value +
                    //williams-nehagoel:MITS 21704-end
                "&ClaimantId="+ hdclaimantid.Value +    //Added by gbindra MITS#34104 WWIG GAP15
                //"&formsubtitle=" + this.hdHeader.Text);
                "&formsubtitle=" + AppHelper.EncodeSpecialSigns(this.hdHeader.Text)); //hlv MITS 28588 11/12/12 asharma326 MITS 34294
            }
            else
            {
                Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotes.aspx?EventID=" + eventid.Value +
                "&ClaimID=" + sClaimIdToPass +
                "&LOB=" + hdLOBCode.Value +  //pmittal5 Mits 21514
                "&FormName=" + sFormName +
                "&EventNumber=" + hdeventnumber.Value +
                    //williams-nehagoel:MITS 21704-start
                "&PolicyId=" + hdpolicyid.Value + 
                "&PolicyName=" + hdpolicyname.Value +
                "&ClaimantId=" + hdclaimantid.Value +    //Added by gbindra MITS#34104 WWIG GAP15
                "&PolicyNumber=" + hdpolicynumber.Value);
                //williams-nehagoel:MITS 21704-end
            }
           
        }

        #region CWS
        ///// <summary>
        ///// Populate the Attached Record DropDownList
        ///// </summary>
        ///// <param name="p_oXML"></param>
        ///// <param name="p_sEventID"></param>
        ///// <param name="p_sEventNumber"></param>
        ///// <param name="p_sClaimID"></param>
        //private void PopulateAttachedToDropDownList(XmlDocument p_oXML , string p_sEventID , string p_sEventNumber , string p_sClaimID)
        //{
        //    ListItem oEventItem = new ListItem("Event: " + p_sEventNumber, "event" + p_sEventID);
        //    attachedto.Items.Add(oEventItem);
            
        //    XmlNodeList oClaimList = p_oXML.SelectNodes("//ClaimList/Claim");
        //    foreach (XmlNode oCurrentNode in oClaimList)
        //    {
        //        string sValue = "Claim: " + oCurrentNode.Attributes["ClaimNumber"].Value + " * " + oCurrentNode.Attributes["ClaimantName"].Value;
        //        string sId = "claim" + oCurrentNode.Attributes["ClaimID"].Value;
        //        ListItem oItem = new ListItem(sValue , sId);
        //        attachedto.Items.Add(oItem);

        //        if (oCurrentNode.Attributes["ClaimID"].Value == p_sClaimID)
        //        {
        //            oItem.Selected = true;
        //        }
        //    }
        //}
        #endregion
        #region WCF--Parijat
        //williams-neha goel:MITS 21704
        private void PopulateAttachedToDropDownList(SelectClaimObject objClaim, string p_sEventID, string p_sEventNumber, string p_sClaimID, string p_sPolicyid, string p_sPolicyName, string p_sFormName, string p_sPolicyNumber, SelectClaimantObject objClaimant)//Added by GBINDRA MITS#34101 02062014 GAP15 to change to the following
        {
            //williams-neha goel:MITS 21704--start--added if condition
            if (p_sFormName == "policy" || p_sFormName == "policyenhal" || p_sFormName == "policyenhgl" || p_sFormName == "policyenhpc" || p_sFormName == "policyenhwc")
            {
                if (!string.IsNullOrEmpty(p_sPolicyNumber))
                {
                    ListItem oPolicyItem = new ListItem("Policy: " + p_sPolicyName + "*" + p_sPolicyNumber, "policy" + p_sPolicyid);
                    attachedto.Items.Add(oPolicyItem);
                }
                else
                {
                    ListItem oPolicyItem = new ListItem("Policy: " + p_sPolicyName, "policy" + p_sPolicyid);
                    attachedto.Items.Add(oPolicyItem);
                }
            }
            else
            {  //williams-neha goel:MITS 21704--end
	            //pmittal5 Mits 21514 - Show Event in Drop down list only if its Create permission is enabled in Module permissions
	            if (objClaim.SelectEvent)  
	            {
	                ListItem oEventItem = new ListItem("Event: " + p_sEventNumber, "event" + p_sEventID);
	                attachedto.Items.Add(oEventItem);
	            }

	            if (objClaim.objClaimList.Count > 0)
	            {
	                foreach (ClaimInfo clmObj in objClaim.objClaimList)
	                {
	                    hdnClaimant.Value = clmObj.ClaimantName;//Parijat :12821  for showing claimant
	                    string sValue = "Claim: " + clmObj.ClaimNumber + " * " + clmObj.ClaimantName;
	                    string sId = "claim" + clmObj.ClaimID;
	                    ListItem oItem = new ListItem(sValue, sId);
	                    attachedto.Items.Add(oItem);

	                    if (clmObj.ClaimID == p_sClaimID)
	                    {
	                        oItem.Selected = true;
	                    }
	                }
	            }
                /*GBINDRA MITS#34104 02062014 wwig gap15 START*/
                //if (objClaimant.objClaimantList.Length > 0)//Changed for JIRA-333
                if (!object.ReferenceEquals(objClaimant.objClaimantList, null) && objClaimant.objClaimantList.Count > 0 &&
                    (string.Compare(objClaimant.objClaimantList[0].ClaimID, "-1", true) != 0))
                {
                    foreach (ClaimantInfo clmntObj in objClaimant.objClaimantList)
                    {
                        hdnClaimant.Value = clmntObj.ClaimantFirstName + " " + clmntObj.ClaimantLastName;//Parijat :12821  for showing claimant
                        string sValue = string.Format("Claimant: {0}", clmntObj.ClaimantLastName + " " + clmntObj.ClaimantFirstName + " * " + clmntObj.ClaimNumber);
                        string sId = string.Format("claimant {0}", clmntObj.ClaimantRowId);
                        ListItem oItem = new ListItem(sValue, sId);
                        attachedto.Items.Add(oItem);

                        if (clmntObj.ClaimantRowId.ToString().Equals(hdclaimantid.Value))
                        {
                            foreach (ListItem items in attachedto.Items)
                            {
                                if (items.Selected.Equals(true))
                                    items.Selected = false;
                            }
                            oItem.Selected = true;
                        }
                    }
                }
                /*GBINDRA MITS#34104 02062014 wwig gap15 START*/
	        }
        }
        #endregion

        //added by Nitin for Mits 15988 on 29-Apr-2009
        protected void btnBackToAllRecrods_Click(object sender, EventArgs e)
        {
            //smishra25:Username,freezetext,showdatetimestamp added in query string for MITS 18485
            //pmittal5 Mits 21514
            //williams-neha goel:MITS 21704---start:added for policy enhanced notes
            //Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ViewNotesAsHtml.aspx?EventID=" + eventid.Value + "&ClaimID=" + hdclaimid.Value +  "&FreezeText=" + hdFreezeText.Value + "&ShowDateStamp=" + hdShowDateStamp.Value + "&UserName=" + hdUserName.Value);
            Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ViewNotesAsHtml.aspx?EventID=" + eventid.Value + "&ClaimID=" + hdclaimid.Value + "&FreezeText=" + hdFreezeText.Value + "&ShowDateStamp=" + hdShowDateStamp.Value + "&UserName=" + hdUserName.Value + "&PolicyId=" + hdpolicyid.Value + "&PolicyName=" + hdpolicyname.Value + "&FormName=" + hdLob.Value + "&PolicyNumber=" + hdpolicynumber.Value + "&LOB=" + hdLOBCode.Value);
            //williams-neha goel:MITS 21704---end            
        }
        protected void GetTemplateDetails(object sender, EventArgs e)
        {
             ProgressNoteBusinessHelper pn = null;
             ProgressNoteTemplates objTemplates = new ProgressNoteTemplates();
            try
            {
                    pn = new ProgressNoteBusinessHelper();
                    objTemplates.TemplateId = TemplateName_cid.Text;
                    //pmittal5 Mits 21514 
                    int iOutResult = 0;
                    bool bResult = false;
                    bResult = Int32.TryParse(eventid.Value,out iOutResult);
                    objTemplates.EventID = iOutResult;
                    bResult = Int32.TryParse(hdclaimid.Value, out iOutResult);
                    objTemplates.ClaimID = iOutResult;
                    objTemplates.LOB = hdLOBCode.Value;
                    //End - pmittal5
                    objTemplates = pn.GetTemplateDetails(objTemplates);
                    if (objTemplates.TemplateMemo != null)
                    {
                        WebHtmlEditor.Text += objTemplates.TemplateMemo;

                    }
                    TemplateName.Text = TempName.Text;
                    hdTemplateDetails.Value = "true";
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        //smishra54, 31Aug 2011: MITS 25641
        protected void btnCreateAnotherNote_Click(object sender, EventArgs e)
        {
            StringBuilder sbQueryStringParameters = new StringBuilder();

            sbQueryStringParameters.Append("/RiskmasterUI/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=");
            sbQueryStringParameters.Append(eventid.Value);
            sbQueryStringParameters.Append("&ClaimID=");
            sbQueryStringParameters.Append(hdclaimid.Value);
            sbQueryStringParameters.Append("&LOB="); 
            sbQueryStringParameters.Append(hdLOBCode.Value);
            sbQueryStringParameters.Append("&SysFormName=");
            sbQueryStringParameters.Append(hdLob.Value);
            sbQueryStringParameters.Append("&EventNumber=");
            sbQueryStringParameters.Append(hdeventnumber.Value);
            sbQueryStringParameters.Append("&PolicyId=");
            sbQueryStringParameters.Append(hdpolicyid.Value);
            sbQueryStringParameters.Append("&PolicyName=");
            sbQueryStringParameters.Append(hdpolicyname.Value);
            sbQueryStringParameters.Append("&PolicyNumber=");
            sbQueryStringParameters.Append(hdpolicynumber.Value);
            sbQueryStringParameters.Append("&CodeId=");
            sbQueryStringParameters.Append(NoteParentType.Value);
            sbQueryStringParameters.Append("&CommentsFlag=true&NewRecord=true");
            //nsachdeva2 MITS: 25641  09/22/2011
            //sbQueryStringParameters.Append("&FreezeText=false");
            //sbQueryStringParameters.Append("&ShowDateStamp=false");
            sbQueryStringParameters.Append("&FreezeText=");
            sbQueryStringParameters.Append(AppHelper.GetQueryStringValue("FreezeText"));
            sbQueryStringParameters.Append("&ShowDateStamp=");
            sbQueryStringParameters.Append(AppHelper.GetQueryStringValue("ShowDateStamp"));
            //End MITS: 25641
            sbQueryStringParameters.Append("&UserName=");
            sbQueryStringParameters.Append(hdUserName.Value);
            sbQueryStringParameters.Append("&hdCurrentNode=");
            sbQueryStringParameters.Append(AppHelper.GetQueryStringValue("hdCurrentNode"));
            sbQueryStringParameters.Append("&PageNumber=");
            sbQueryStringParameters.Append(AppHelper.GetQueryStringValue("PageNumber"));
            sbQueryStringParameters.Append("&ClaimantId=");//Added by Gbindra MITS#34104 WWIG GAP 15
            sbQueryStringParameters.Append(hdclaimantid.Value);//Added by Gbindra MITS#34104 WWIG GAP 15
            sbQueryStringParameters.Append("&formsubtitle=");
            //sbQueryStringParameters.Append(this.hdHeader.Text);
            sbQueryStringParameters.Append(AppHelper.EncodeSpecialSigns(this.hdHeader.Text)); //hlv MITS 28588 11/12/12 asharma326 MITS 34294

            Response.Redirect(sbQueryStringParameters.ToString(), true);
        }
        //smishra54: End
        /*Added by gbindra MITS#34104 WWIG GAP15 START*/
        protected void attachedto_change(object sender, EventArgs e)
        {
            if (attachedto.SelectedValue.StartsWith("claimant", StringComparison.InvariantCultureIgnoreCase))
            {
                string claimantValue = attachedto.SelectedValue.Split(' ')[1];
                hdclaimantid.Value = claimantValue;
            }
            else 
            {
                hdclaimantid.Value = "-1";
            }
        }
        /*Added by gbindra MITS#34104 WWIG GAP15 START*/
    }
}
