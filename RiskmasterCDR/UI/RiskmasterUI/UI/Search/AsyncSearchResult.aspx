﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AsyncSearchResult.aspx.cs" Inherits="Riskmaster.UI.UI.Search.AsyncSearchResult" ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%--******************************************************************************************************************************************
*   Date     |  JIRA   | Programmer | Description                                                                                        *
******************************************************************************************************************************************
* 01/06/2015 | RMA4307  | vchouhan6   | Generic Export to Excel for visible columns, independent of size, order of columns and UI filters.
******************************************************************************************************************************************//--%>
<!DOCTYPE html>
<html xmlns:ng="http://angularjs.org">
<head runat="server">
    <meta charset="utf-8">
    <title>Search Results</title>
    <link rel="stylesheet" type="text/css" href="../../Content/ng-grid.css" />
    <%--<link rel="stylesheet" type="text/css" href="http://angular-ui.github.com/ng-grid/css/ng-grid.css" />--%>
    <script src="../../Scripts/jquery/jquery-1.8.0.min.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>--%>
    <script src="../../Scripts/jquery/jquery-ui-1.9.2.min.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>--%>
    <script src="../../Scripts/angularjs/angular.min.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.2/angular.min.js"></script>--%>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid.debug.js"></script>
    <%--<script type="text/javascript" src="http://angular-ui.github.com/ng-grid/lib/ng-grid.debug.js"></script>--%>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid-layout.js"></script>
    <%--<script type="text/javascript" src="http://angular-ui.github.com/ng-grid/lib/ng-grid-layout.js"></script>--%>
    <!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start: Include Export to excel Js in code -->
   <%-- <script src="../../Scripts/angularjs/ng-grid-csv-export.js"></script>--%> 
    <!-- End CUSTOMIZATION-->
      <!-- RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality start-->
        <script type="text/javascript" src="../../Scripts/angularjs/lib/ngGridExportPlugin.js"></script>
    <!--RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality end-->
    <script type="text/javascript" src="../../Scripts/angularjs/SearchResult.js">    </script>
    <script type="text/javascript" src="../../Scripts/form.js"></script>
    <script src="../../Scripts/searchresults.js" type="text/javascript"></script>
    <style type="text/css">
        .gridStyle {
            border: 1px solid rgb(212,212,212);
            width: 99.5%;
            height: 400px;
        }

        .k-grid-filter {
            float: right;
            /*margin: 1.8em -0.1em -0.4em;*/
            padding: 0.3em 0.2em 0.4em;
            position: relative;
            z-index: 1;
        }

        .k-icon {
            position: static;
            vertical-align: middle;
            background-image: url("../../images/Change-Sync.png");
            border-color: transparent;
            opacity: 0.9;
            background-color: transparent;
            background-repeat: no-repeat;
            display: inline-block;
            font-size: 0;
            height: 16px;
            line-height: 0;
            overflow: hidden;
            text-align: center;
            width: 16px;
        }

        .k-filter {
            /* background-position: -32px -80px;*/
        }
        .ngHeaderCell {
            background-color: #B8CCE4 !important;
            color: #fff !important;
        }
        .ngHeaderContainer {
             background-color: #B8CCE4 !important;
        }
        .ng-pristine {
            /*width: auto !important;*/
        }
        .ngFooterPanel {
            background-color: #B8CCE4 !important;
            color: #fff !important;
            height:30px !important;
        }
        .ng-valid {
            /*width: auto !important;*/
            height:auto !important;
        }
        input.ng-pristine.ng-valid {
            height:auto !important;
        }
        .ngRow.even {
          background-color: #dddddd /*#E7F0FB*/ !important; 
        }
        .ngPagerButton {
        height : 22px !important;
        }
        .ngPagerContainer {
            padding : 2px !important;
            margin-top :0px !important;
        }
        .ngViewport {
            height: 390px !important;
        }
    </style>

</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <table width="100%" cellspacing="0" cellpadding="0" border="0" id="tblNoRecord" style="display: none;">
            <tbody>
                <tr>
                    <td xhtml:class="msgheader1" align="center">
                        <asp:Label Text="<%$ Resources:lblNoSearchResults %>" runat="server" ID="lblNoCriteria" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <br />
                        <asp:Label Text="<%$ Resources:lblRefineSelection %>" runat="server" ID="lblRefine" />
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="divReturnSearch" style="display: none;">
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                    <tr>
                        <td xhtml:class="msgheader1" align="center">
                            <asp:Label Text="<%$ Resources:lblNoSearchResults %>" runat="server" ID="Label1" />
                        </td>
                    </tr>
                    <tr>
                        <td align='center'>
                            <br />
                            <asp:Label Text="<%$ Resources:lblReturnSearchCriteria %>" runat="server" ID="lblReturnSearchCriteria" />
                    </tr>
                    <br />
            </table>
            <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr xhtml:class='Arial'>
                    <td align='right' width='50%'>
                        <input type='button' value="<%$ Resources:btnReturnSearch %>" class='button' onClick='self.history.back(); return false;' name='btnBack' id='btnBack' runat="server" clientidmode="Static" />
                    </td>
                    <!-- rsushilaggar JIRA 7767-->
                    <td align='left'>
                       &nbsp; <input type='button' value="Add new" class='button' onclick="btnAddNew_Click();" name='btnAddNew' id='btnAddNew' runat="server" clientidmode="Static" />
                    </td>
                    
                </tr>
            </table>
        </div>
        <div style="clear: both"></div>

        <input type="hidden" runat="server" id="hdCriteriaXml" clientidmode="Static" />
        <input type="hidden" runat="server" id="hdScreenFlag" />
        <asp:HiddenField ID="admtable" runat="server" />
        <input type="hidden" name='searchcat' id="searchcat" />
        <input type="hidden" runat="server" id="hdUserPref" clientidmode="Static" />
        <input type="hidden" runat="server" id="hdnEEFormName" clientidmode="Static" /><!--dnehe added-->  
    </form>
    <div ng-app="rmaApp" id="ng-app" style="display: none;">
        <div ng-controller="searchController">
            <div id="divImage" style="display: none;">
                <div class="image">
                    <input type="image" src="../../Images/tb_save_active.png" style="width: 28px; height: 28px; border: 0px;" id="save" alt="Save" runat="server"
                        title="<%$ Resources:ttSavePref %>" onmouseover="this.src='../../Images/tb_save_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_save_active.png';this.style.zoom='100%'" ng-click="SavePreferences()" tooltip="<%$ Resources:ttSavePref %>" />
                </div>
                <div class="image">
                    <input type="image" src="../../Images/tb_print_active.png" style="width: 28px; height: 28px; border: 0px;" id="btnPrint" alt="Print" runat="server"
                        title="<%$ Resources:ttPrint %>" onmouseover="this.src='../../Images/tb_print_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_print_active.png';this.style.zoom='100%'" onclick="PrintSearch();" tooltip="<%$ Resources:ttPrint %>" />
                </div>
                <!-- Raman : Gatekeeper Action: Export to Excel functionality need to be reworked upon. It should be exporting to excel and not csv as its currently doing.
                    Also formatting need to be retained and export should be readable. Currently it does not add any value to the client.
               <!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start: Add button to redirect click event, better UI  -->
                <div class="image">
                    <input type="image" src="../../Images/tb_exportexcel_active.png" style="width: 28px; height: 28px; border: 0px;" id="btnExport" alt="Export" runat="server"
                        title="<%$ Resources:ttExport %>" onmouseover="this.src='../../Images/tb_exportexcel_mo.png';this.style.zoom='110%'"  
                        onmouseout="this.src='../../Images/tb_exportexcel_active.png';this.style.zoom='100%'" onclick="redirectE2E()" tooltip="<%$ Resources:ttExport %>" />
                </div>
               <!-- End CUSTOMIZATION-->
            </div>
            <div style="clear: both"></div>
            <div class="gridStyle" ng-grid="gridOptions" id="SearchResultGrid"></div><!--dnehe added id--..optional-->
            <input type="hidden" name='hdnSearchCat' id="hdnSearchCat" value="{{ hdSearchCat }}" />
 			<!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start:Added a footer  -->
            <div class="footer">
                <iframe id="csvDownloadFrame" style="display:none" />
            </div>
            <!-- End CUSTOMIZATION-->
        </div>
    </div>
</body>
</html>
