﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Searches.aspx.cs" Inherits="Riskmaster.UI.Utilities.Searches" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>System Customization (Searches)</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/system.css" type="text/css"/>
  <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">
  </script>
</head>
<body>
    <form id="frmData" runat="server">
    <div class="msgheader">
    						System Customization
    					
   </div>
   <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <td colspan="2" class="ctrlgroup">
      								&nbsp;Search Links
      							
     </td>
    </tr>
    <tr>
     <td width="40%"></td>
     <td width="60%"><i>Show</i></td>
    </tr>
    <tr>
     <td class="datatd">Claims</td>
     <td class="datatd"><asp:checkbox runat="server"  rmxref="/Instance/Document/customize_search/RMAdminSettings/Search/Claim" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd1">Events</td>
     <td class="datatd1"><asp:checkbox runat="server" rmxref="/Instance/Document/customize_search/RMAdminSettings/Search/Event" /></td>
    </tr>
    <tr>
     <td class="datatd">Employees</td>
     <td class="datatd"><asp:checkbox runat="server" rmxref="/Instance/Document/customize_search/RMAdminSettings/Search/Employee" /></td>
    </tr>
    <tr>
     <td class="datatd1">Entities</td>
     <td class="datatd1"><asp:checkbox runat="server" rmxref="/Instance/Document/customize_search/RMAdminSettings/Search/Entity" /></td>
    </tr>
    <tr>
     <td class="datatd">Vehicles</td>
     <td class="datatd"><asp:checkbox runat="server" rmxref="/Instance/Document/customize_search/RMAdminSettings/Search/Vehicle" /></td>
    </tr>
    <tr>
     <td class="datatd1">Policies</td>
     <td class="datatd1"><asp:checkbox runat="server" rmxref="/Instance/Document/customize_search/RMAdminSettings/Search/Policy" /></td>
    </tr>
    <tr>
     <td class="datatd">Funds</td>
     <td class="datatd"><asp:checkbox runat="server" rmxref="/Instance/Document/customize_search/RMAdminSettings/Search/Fund" /></td>
    </tr>
    <tr>
     <td class="datatd1">Patients</td>
     <td class="datatd1"><asp:checkbox runat="server" rmxref="/Instance/Document/customize_search/RMAdminSettings/Search/Patient" /></td>
    </tr>
    <tr>
     <td class="datatd">Physicians</td>
     <td class="datatd"><asp:checkbox runat="server" rmxref="/Instance/Document/customize_search/RMAdminSettings/Search/Physician" /></td>
    </tr>
    <tr>
     <td class="datatd1">Leave Plan</td>
     <td class="datatd1"><asp:checkbox runat="server" rmxref="/Instance/Document/customize_search/RMAdminSettings/Search/LeavePlan" /></td>
    </tr>
    <tr>
     <td colspan="2">
      								&nbsp;
      							
     </td>
    </tr>
    <tr>
     <td colspan="2"><asp:Button onclick = "Save" runat="server" name="Save" Text="Save"  class="button" style="width:100"/>  <asp:Button  onclick="Refresh" runat="server" name="Refresh" Text="Refresh"  class="button" style="width:100"/>
     <asp:textbox style="display: none" runat="server" id="fileName" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/FileName"
                 Text="customize_search"   rmxtype="hidden" />
     </td>
    </tr>
   </table>
    </form>
</body>
</html>
