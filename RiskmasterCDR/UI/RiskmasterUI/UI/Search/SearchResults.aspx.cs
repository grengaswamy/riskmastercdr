﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.Text;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager; //mbahl3 mits 30224 for multilingual changes

namespace Riskmaster.UI.Search
{
    public partial class SearchResults : System.Web.UI.Page
    {
        XElement m_objMessageElement = null;
        string sPageId = RMXResourceProvider.PageId("SearchResults.aspx"); //JIRA-RMA-1535
        string showAddNew = string.Empty; 
        bool bUseEntityRole = false;        //avipinsrivas start : Worked for JIRA - 7767
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement xmlIn = null;
            XElement objXmlCriteria = null;
            int iTotalRows = 0;
            int iPageSize = 0;
            showAddNew = Request.QueryString["showaddnew"];
            //mbahl3 for multilingual mits 30224 
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, sPageId, "CreateEntityValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CreateEntityValidationsScripts", sValidationResources, true);
            string sValResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("SearchResults.aspx"), "SearchValidations", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "SearchValidations", sValidationResources, true);
               
            //mbahl3 for multilingual mits 30224 
            try
            {
                if (!IsPostBack)
                {

                    if (Request.Form["hdCriteriaXML"] != null)
                        objXmlCriteria = XElement.Parse(Server.HtmlDecode(Request.Form["hdCriteriaXML"]));

                    if (Request.Form["hdScreenFlag"] != null)
                    {
                        hdScreenFlag.Value = Request.Form["hdScreenFlag"];
                    }

                    PrepareCriteriaXml(objXmlCriteria);

                    objXmlCriteria = UpdateCriteriaXml(objXmlCriteria);

                    // Call CWS and get the result xml.
                    xmlIn = GetSearchResults(objXmlCriteria);

                    iTotalRows = Conversion.ConvertObjToInt(hdTotalRows.Value);
                    iPageSize = Conversion.ConvertObjToInt(hdPageSize.Value);

                    if (iTotalRows == 0)
                    {
                        CallNoResult(hdScreenFlag.Value);
                    }
                    else
                    {
					//JIRA-RMA-1535 start
                        lblPageRangeDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblPage", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), " 1 ", hdTotalPages.Value);
                        lblPageRangeTop.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblOf", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), "1 ", hdTotalPages.Value);
					//JIRA-RMA-1535 end
                        if (iPageSize > iTotalRows) // There is only 1 page.
                        {
						//JIRA-RMA-1535 start
                            lblPagerTop.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), "1", iTotalRows, iTotalRows);
                            lblPagerDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), "1", iTotalRows, iTotalRows);
						//JIRA-RMA-1535 end
                            lnkFirstTop.Enabled = false;
                            lnkLastTop.Enabled = false;
                            lnkPrevTop.Enabled = false;
                            lnkNextTop.Enabled = false;
                            lnkFirstDown.Enabled = false;
                            lnkLastDown.Enabled = false;
                            lnkPrevDown.Enabled = false;
                            lnkNextDown.Enabled = false;
                        }
                        else
                        {
							//JIRA-RMA-1535 start
                           lblPagerTop.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), "1", iPageSize, iTotalRows);
						   lblPagerDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), "1", iPageSize, iTotalRows);
						   //JIRA-RMA-1535 end
                            lnkFirstTop.Enabled = false;
                            lnkPrevTop.Enabled = false;
                            lnkFirstDown.Enabled = false;
                            lnkPrevDown.Enabled = false;
                        }
                    }
                    
                    // Pass the result Xml to GetData() method which will bind it to GridView.
                    BindDataToGrid(xmlIn);

                    hdCriteriaXml.Value = Server.HtmlEncode(objXmlCriteria.ToString());
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void PrepareCriteriaXml(XElement p_objCriteriaXML)
        {
            var objFieldXml = from fields in p_objCriteriaXML.Descendants("field")

                              select fields;

            string sFieldID = string.Empty;
            string sOrderBy = string.Empty;

            foreach (XElement field in objFieldXml)
            {
                XText txtNode = field.Nodes().OfType<XText>().First();
                txtNode.Value = string.Empty;
                switch (field.Attribute("type").Value.ToLower())
                {
                    case "text":
                    case "numeric":
                    
                    case "ssn":
                    case "textml":
                    case "freecode": //MITS 16476
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        // Set the value of the Operation XElement.
                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        // Set the XText of XElement.
                        field.Add(new XText(Request.Form[sFieldID]));
                        break;
                    case "checkbox":
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        // Set the value of the Operation XElement.
                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        // Set the XText of XElement.
                        if (Request.Form[sFieldID] != null)
                            if (Request.Form[sFieldID].ToLower() == "on")
                                field.Add(new XText("-1"));
                        break;

                    case "orgh":
                    case "code":
                    case "entity": // akaushik5 Added for MITS 38161
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        field.Add(new XElement("_cid", Request.Form[sFieldID + "_cid"]));
                        field.Add(new XElement("_tableid", Request.Form[sFieldID + "_tableid"]));

                        // Set the XText of XElement.
                        field.Add(new XText(Request.Form[sFieldID]));
                        break;
                    case "currency":
						//Code added by ybhaskar for MITS 14684
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        field.Add(new XElement("StartCurrency", Request.Form[sFieldID + "start"]));
                        field.Add(new XElement("EndCurrency", Request.Form[sFieldID + "end"]));

                        // Set the XText of XElement.
                        //field.Add(new XText(Request.Form[sFieldID]));
                        
                        break;
                    case "date":
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        field.Add(new XElement("StartDate", Request.Form[sFieldID + "start"]));
                        field.Add(new XElement("EndDate", Request.Form[sFieldID + "end"]));

                        // Set the XText of XElement.
                        //field.Add(new XText(Request.Form[sFieldID]));
                        break;

                    case "time":
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        field.Add(new XElement("StartTime", Request.Form[sFieldID + "start"]));
                        field.Add(new XElement("EndTime", Request.Form[sFieldID + "end"]));

                        // Set the XText of XElement.
                        field.Add(new XText(Request.Form[sFieldID]));
                        break;

                    case "tablelist":
                    case "codelist":
                    case "entitylist":
                        // npadhy JIRA 6415 - Handling the search for User Lookup
                    case "userlookup":
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        field.Add(new XElement("_lst", Request.Form[sFieldID + "_lst"]));

                        // Set the XText of XElement.
                        //field.Add(new XText(Request.Form[sFieldID]));
                        break;
                    case "attachedrecord": //skhare7
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        // Set the value of the Operation XElement.
                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        // Set the XText of XElement.
                        field.Add(new XText(Request.Form[sFieldID]));
                        break;

                    default:
                        break;
                }

            }
        }

        private XElement GetSearchResults(XElement p_objCriteriaXML)
        {
            XmlDocument objXmlCriteriaDom = new XmlDocument();
            objXmlCriteriaDom.LoadXml(p_objCriteriaXML.ToString());

            string sReturn = AppHelper.CallCWSService(objXmlCriteriaDom.InnerXml.ToString());

            //Binding Error Control
            ErrorControl.errorDom = sReturn;

            objXmlCriteriaDom.LoadXml(sReturn);
            XmlNode oInstanceNode = objXmlCriteriaDom.SelectSingleNode("/ResultMessage/Document");

            XElement objResultXml = null;
            if (oInstanceNode.InnerXml != string.Empty)
            {
                objResultXml = XElement.Parse(oInstanceNode.InnerXml.ToString());

                if (objResultXml != null)
                    AssignResultHeaderValues(objResultXml);
            }
            return objResultXml;
        }

        private XElement UpdateCriteriaXml(XElement p_objCriteriaXML)
        {
            try
            {
                // npadhy Start MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
                string sSysFormName = string.Empty;
                // npadhy End MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
                var objFieldXml = from fields in p_objCriteriaXML.Descendants("field")
                                  select fields;


                m_objMessageElement = GetMessageTemplate();
                XElement objTempElement = null;

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                    objTempElement.Value = AppHelper.GetSessionId();
                }//if

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/ViewId");
                if (Request.Form["viewid"] != null)
                    objTempElement.Value = Request.Form["viewid"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/FormName");
                if (Request.Form["hdFormName"] != null)
                    objTempElement.Value = Request.Form["hdFormName"];

                //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/tablename");
                if (Request.Form["hdtablename"] != null)
                    objTempElement.Value = Request.Form["hdtablename"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/rowid");
                if (Request.Form["hdrowid"] != null)
                    objTempElement.Value = Request.Form["hdrowid"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/eventdate");
                if (Request.Form["hdeventdate"] != null)
                    objTempElement.Value = Request.Form["hdeventdate"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/claimdate");
                if (Request.Form["hdclaimdate"] != null)
                    objTempElement.Value = Request.Form["hdclaimdate"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/policydate");
                if (Request.Form["hdpolicydate"] != null)
                    objTempElement.Value = Request.Form["hdpolicydate"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/filter");
                if (Request.Form["hdfilter"] != null)
                    objTempElement.Value = Request.Form["hdfilter"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/TableRestrict");
                if (Request.Form["tablerestrict"] != null)
                {
                    objTempElement.Value = Request.Form["tablerestrict"];

                    // npadhy Start MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
                    if (string.Compare(objTempElement.Value, "policyenh", true) == 0)
                    {
                        sSysFormName = AppHelper.GetFormValue("hdSysFormName");
                        if (sSysFormName.StartsWith("policyenh") && AppHelper.GetFormValue("hdScreenFlag") == "2")
                        {
                            objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/filter");
                            objTempElement.Value = sSysFormName.Substring(9).ToUpper();
                        }
                    }
                    // npadhy End MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
                }

                //Tushar"MITS#18229
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/codefieldrestrict");
                if (Request.Form["codefieldrestrict"] != null)
                    objTempElement.Value = Request.Form["codefieldrestrict"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/codefieldrestrictid");
                if (Request.Form["codefieldrestrictid"] != null)
                    objTempElement.Value = Request.Form["codefieldrestrictid"];

                //End:MITS#18229


                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/DisplayFields/OrderBy1");
                if (Request.Form["orderby1"] != null)
                    objTempElement.Value = Request.Form["orderby1"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/DisplayFields/OrderBy2");
                if (Request.Form["orderby2"] != null)
                    objTempElement.Value = Request.Form["orderby2"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/DisplayFields/OrderBy3");
                if (Request.Form["orderby3"] != null)
                    objTempElement.Value = Request.Form["orderby3"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/SysEx");  // csingh7 R5 Merge : 12545
                if (Request.Form["hdSysFormName"] == "funds")
                      objTempElement.Value = "bFunds";

                //abansal23 MITS 17648 10/09/2009 Starts
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/SelOrgLevels");  // csingh7 R5 Merge : 12545
                if (Request.Form["hdSelectedOrgLevels"] != null)
                    objTempElement.Value = Request.Form["hdSelectedOrgLevels"];
                //abansal23 MITS 17648 10/09/2009 Starts

                // Handle Soundex for Search
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/SoundEx");
                string sSoundexChecked = Request.Form["soundex"];

                if (sSoundexChecked != null)
                {
                    objTempElement.Value = "-1";
                }
                else
                {
                    objTempElement.Value = "";
                }
                //Start - averma62 MITS 25163- Policy Interface Implementation
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/AllowPolicySearch");
                if (Request.Form["hdAllowPolicySearch"] != null)
                    objTempElement.Value = Request.Form["hdAllowPolicySearch"];
                //End  - averma62 MITS 25163- Policy Interface Implementation
				
				//JIRA-RMA-1535 start
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/LangId");
                if (objTempElement != null)
                    objTempElement.Value = Request.Form["hdLangId"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/PageId");
                if (objTempElement != null)
                    objTempElement.Value = Request.Form["hdPageId"];
				//JIRA-RMA-1535 end
				
                var objSearchField = from searchfields in m_objMessageElement.Descendants("SearchFields")

                                     select searchfields;


                XElement objSearchFieldElem = objSearchField.ElementAt(0);

                foreach (XElement field in objFieldXml)
                {
                    objSearchFieldElem.Add(field);
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
            }

            return m_objMessageElement;
        }


        /// <summary>
        /// CWS request message template
        /// <CodeFieldRestrict /> added by Tushar:MITS:18229
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            //Abansal23 : MITS 17648 : SelOrgLevels added as node in template
            //averma62 MITS 25163- Policy Interface Implementation
            //rkulavil : RMA-5977,RMA-5978 :Number of records per page
            XElement objTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>SearchAdaptor.SearchRun</Function>
              </Call>
              <Document>
                <Search>
                  <PageNo />
                  <TotalRecords />
                  <SortColPass />
                  <LookUpId>0</LookUpId> 
                  <PageSize>0</PageSize>
                  <MaxResults></MaxResults>
                  <SortColumn />
                  <Order>ascending</Order>
                  <IfBack/>
                  <SearchMain>
                    <IfLookUp>0</IfLookUp>
                    <InProcess>0</InProcess>
                    <ScreenFlag>1</ScreenFlag>
                    <ViewId>-1</ViewId>
                    <FormName></FormName>
                    <FormNameTemp />
                    <TableRestrict />
                    <codefieldrestrict />
                    <codefieldrestrictid />
                    <tablename/>
                    <rowid/>
                    <eventdate/>
                    <claimdate/>
                    <policydate/>
                    <filter/>
                    <SysEx />
                    <Settings />
                    <TableID />
                    <SoundEx>-1</SoundEx>
                    <DefaultSearch />
                    <DefaultViewId />
                    <Views />
                    <CatId>2</CatId>
                    <DisplayFields>
                      <OrderBy1 />
                      <OrderBy2 />
                      <OrderBy3 />
                    </DisplayFields>
                    <SearchCat />
                    <AdmTable />
                    <EntityTableId />
                    <Type />
                    <LookUpType />
                    <SearchFields>
                    </SearchFields>
                    <SelOrgLevels />
                    <SettoDefault>-1</SettoDefault>
                    <AllowPolicySearch />
                    <LangId />
                    <PageId />
                    </SearchMain>
                </Search>
              </Document>
            </Message>
            ");

            return objTemplate;
        }

        private void BindDataToGrid(XElement p_objResultXml)
        {
            DataRow drSearchRow = null;
            DataTable dtSearch = new DataTable();
            int iPageSize = Conversion.ConvertObjToInt(hdPageSize.Value);
            int iNoSortIndex = 0;
            const string DATE_STRING = "date";
            //achouhan3     MITS#34276      Entity ID Type and ID Type Vendor permission Starts
            Boolean bEntIDTypePermission=true;
            Boolean bIDTypeVendorPermission = true;
            int indxEntIDType=0;
            int indxEntIDNum=0;
            bool isEntityIdnumber = false;
             if (p_objResultXml.Attribute("isentidtypeview") != null)
                bEntIDTypePermission =Convert.ToBoolean( p_objResultXml.Attribute("isentidtypeview").Value);
            if (p_objResultXml.Attribute("isidtypevendorview") != null)
                bIDTypeVendorPermission = Convert.ToBoolean(p_objResultXml.Attribute("isidtypevendorview").Value);
            //achouhan3     MITS#34276      Entity ID Type and ID Type Vendor permission Ends
            //avipinsrivas start : Worked for JIRA - 7767
            if (p_objResultXml.Attribute("useentityrole") != null && string.Equals(p_objResultXml.Attribute("useentityrole").Value.ToUpper(), "TRUE"))
                bUseEntityRole = true;
            //avipinsrivas end
            XDocument objSearchDoc = XDocument.Load(p_objResultXml.CreateReader());           

            var objColXml = from cols in objSearchDoc.Descendants("column")

                            select cols;

            foreach (XElement column in objColXml)
            {
                dtSearch.Columns.Add(GetDataColumn(column.Value,dtSearch));
                if (column.Value.ToLower() == "entity id type")
                    indxEntIDType = iNoSortIndex;
                else if (column.Value.ToLower() == "entity id number")
                {
                    isEntityIdnumber = true;
                    indxEntIDNum = iNoSortIndex;
                }
                if (column.Attribute("nosort") != null)
                {
                    if (column.Attribute("nosort").Value.ToLower() == "on")
                    {
                        if (string.IsNullOrEmpty(hdNoSortIndex.Value))
                        {
                            hdNoSortIndex.Value = iNoSortIndex.ToString();
                        }
                        else
                        {
                            hdNoSortIndex.Value = hdNoSortIndex.Value + "," + iNoSortIndex.ToString();
                        }
                    }
                }
                iNoSortIndex++;
            }

            dtSearch.Columns.Add("pid");
            //Mridul. 11/26/09. MITS:18229
            //if (searchcat.Value.ToLower() == "entity")
            if (string.Compare(searchcat.Value, "entity", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(searchcat.Value, "policyenh", StringComparison.OrdinalIgnoreCase) == 0)
            {
                dtSearch.Columns.Add("entityformname");
            }

            var objRowXml = from rows in objSearchDoc.Descendants("row")

                            select rows;

            string[] arrPid = new string[1];

            foreach (XElement row in objRowXml)
            {
                arrPid[0] = "pid";
                grdSearchResults.DataKeyNames = arrPid;
                
                var objFieldXml = from fields in row.Descendants("field")

                                  select fields;

                int iCount = 0;
                drSearchRow = dtSearch.NewRow();

                // Bijender has Modified the code for MIts 14532
                // there is wrong mapping while adding the row to dtSearch table in case of same column name 
                foreach (XElement column in objColXml)
                {
                    if (column.Attribute("type").Value == DATE_STRING)
                    {
                        if (objFieldXml.ElementAt(iCount).Attribute("formatedtext").Value == "")
                            //drSearchRow[column.Value] = System.DBNull.Value;
                            drSearchRow[dtSearch.Columns[iCount].ColumnName] = System.DBNull.Value;
                        else
                            //drSearchRow[column.Value] = objFieldXml.ElementAt(iCount).Attribute("formatedtext").Value;
                            drSearchRow[dtSearch.Columns[iCount].ColumnName] = objFieldXml.ElementAt(iCount).Attribute("formatedtext").Value;
                    }
                    else
                    {
                        //achouhan3     MITS#34276      Entity ID type and ID type Vendor View Permission
                        if ((dtSearch.Columns[iCount].ColumnName.ToLower() == "entity id number" || dtSearch.Columns[iCount].ColumnName.ToLower() == "entity id type")
                                && !String.IsNullOrEmpty( objFieldXml.ElementAt(iCount).Value))                      
                        {
                            if (!bEntIDTypePermission)
                                drSearchRow[dtSearch.Columns[iCount].ColumnName] = "######";
                            else if (!bIDTypeVendorPermission)
                            {
                                string[] arrVendor = objFieldXml.ElementAt(indxEntIDType).Value.Split(' ');
                                if (arrVendor[0] != null && arrVendor[0].ToString().ToLower() == "vendor")
                                {
                                    objFieldXml.ElementAt(indxEntIDType).Value = "######";
                                    if (isEntityIdnumber)
                                        objFieldXml.ElementAt(indxEntIDNum).Value = "######";
                                }
                                drSearchRow[dtSearch.Columns[iCount].ColumnName] = objFieldXml.ElementAt(iCount).Value;
                            }
                            else
                                drSearchRow[dtSearch.Columns[iCount].ColumnName] = objFieldXml.ElementAt(iCount).Value;
                        }                      
                        else
                        {
                            //drSearchRow[column.Value] = objFieldXml.ElementAt(iCount).Value;
                            drSearchRow[dtSearch.Columns[iCount].ColumnName] = objFieldXml.ElementAt(iCount).Value;
                        }
                    }
                    
                    iCount++;
                }
                //Bijender End Mits 14532

                drSearchRow["pid"] = row.Attribute("pid").Value;
                //Mridul. 11/26/09. MITS:18229
                //if (searchcat.Value.ToLower() == "entity")
                if (string.Compare(searchcat.Value, "entity", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(searchcat.Value, "policyenh", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    drSearchRow["entityformname"] = row.Attribute("entityformname").Value;
                }

                dtSearch.Rows.Add(drSearchRow);
            }

            // Bind DataTable to GridView.
            grdSearchResults.Visible = true;
            grdSearchResults.PageSize = iPageSize;          
            grdSearchResults.DataSource = dtSearch;            
            grdSearchResults.DataBind();           
            hdNoSortIndex.Value = string.Empty;
        }

        private void CallNoResult(string p_sScreenFlag)
        { 
            StringBuilder sNoResult = new StringBuilder();
           
            sNoResult.Append("<head id='Head1'>");
            sNoResult.Append("<title>Search Results</title>");
            sNoResult.Append("<link rel='stylesheet' href='../../App_Themes/RMX_Default/rmnet.css' type='text/css'/>");
            sNoResult.Append("<script language='javascript' src='../../Scripts/searchresults.js' type='text/javascript'></script>");
            sNoResult.Append("</head>");

            sNoResult.Append("<table width='100%' cellspacing='0' cellpadding='0' border='0' >");
            sNoResult.Append("<tr><td xhtml:class='msgheader1' align='center'>" + AppHelper.GetResourceValue(sPageId, "lblNoSearchResults", "0") + "</td></tr>"); //JIRA-RMA-1535 



            // "Return to Search" button is only required for the pop-up searches.
            if (p_sScreenFlag == "2" || p_sScreenFlag == "3")
            {
                sNoResult.Append("<tr><td align='center'><br/>Please hit the Return To Search button and refine your criteria selections.</td></tr><br/>");
                sNoResult.Append("</table>");

                sNoResult.Append("<table width='100%' cellspacing='0' cellpadding='0' border='0'>");
                sNoResult.Append("<tr><td><br/></td></tr>");
                sNoResult.Append("<tr xhtml:class='Arial'>");
                sNoResult.Append("<td align='right' width='50%'>");
                sNoResult.Append("<input type='button' value='Return To Search' class='button' onclick='self.history.back();' name='btnBack' id='btnBack'></input>");
                sNoResult.Append("</td>");
                //rsushilaggar JIRA 7767
                if (!string.IsNullOrEmpty(showAddNew) && showAddNew == "true")
                {
                    sNoResult.Append("<td align='left'>");
                    sNoResult.Append("<input type='button' value='Add New' class='button' onclick='btnAddNew_Click();' name='btnAddNew' id='btnAddNew'></input>");
                    sNoResult.Append("</td>");
                }
                else {
                    sNoResult.Append("<td align='left'>&nbsp;</td>");
                }
                ////end rsushilaggar
                sNoResult.Append("</tr></table>");
            }
            else
            {
                sNoResult.Append("<tr><td align='center'><br/>" + AppHelper.GetResourceValue(sPageId, "lblRefineSelection", "0") + "</td></tr><br/>"); //JIRA-RMA-1535 
                sNoResult.Append("</table>");
            }

            Response.Write(sNoResult);
            Response.End();
        }

        protected void grdSearchResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string sName = String.Empty;
            string sIsActiveEntity = "true";      //avipinsrivas start : Worked for JIRA - 7767
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (string.IsNullOrEmpty(e.Row.Cells[0].Text) || e.Row.Cells[0].Text == "&nbsp;")
                    {
                        e.Row.Cells[0].Text = "<a href='#'>" + "&lt;No Data&gt;" + "</a>";
                    }
                    else
                    {
                        sName = e.Row.Cells[0].Text;
                        e.Row.Cells[0].Text = "<a href='#'>" + e.Row.Cells[0].Text + "</a>";
                    }
                   
                    if (hdScreenFlag.Value == "4")
                    {
                        e.Row.Cells[0].Attributes["onclick"] = String.Format("launchExecSummary({0}); return false;", e.Row.Cells[e.Row.Cells.Count - 1].Text);
                    }
                    else
                    {
                        //Mridul. 11/26/09. MITS:18229
                        //if (searchcat.Value.ToLower() == "entity")
                        if (string.Compare(searchcat.Value, "entity", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(searchcat.Value, "policyenh", StringComparison.OrdinalIgnoreCase) == 0)
                        {
                            //e.Row.Cells[0].Attributes["onclick"] = String.Format("selRecord({0},'{1}'); return false;", e.Row.Cells[e.Row.Cells.Count - 2].Text, e.Row.Cells[e.Row.Cells.Count - 1].Text);
                            //mbahl3 mits 30224
                            //avipinsrivas start : Worked for JIRA - 7767
                            if (e.Row.Cells[e.Row.Cells.Count - 2].Text.IndexOf('|') > 0)
                            {
                                sIsActiveEntity = e.Row.Cells[e.Row.Cells.Count - 2].Text.Split('|')[1];
                                e.Row.Cells[0].Attributes["onclick"] = String.Format("selRecord({0},\"{1}\",'{2}','{3}'); return false;", e.Row.Cells[e.Row.Cells.Count - 2].Text.Split('|')[0], e.Row.Cells[e.Row.Cells.Count - 1].Text, sIsActiveEntity, bUseEntityRole);//MITS 25139
                            }
                            else
                                //mbahl3 mits 30224
                                e.Row.Cells[0].Attributes["onclick"] = String.Format("selRecord({0},\"{1}\",'{2}','{3}'); return false;", e.Row.Cells[e.Row.Cells.Count - 2].Text, e.Row.Cells[e.Row.Cells.Count - 1].Text, sIsActiveEntity, bUseEntityRole);//MITS 25139
                            //avipinsrivas end
                            e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                            e.Row.Cells[e.Row.Cells.Count - 2].Visible = false;
                            
                        }
                            //skhare7 Diary search
                        else  if (string.Compare(searchcat.Value, "diary", StringComparison.OrdinalIgnoreCase) == 0 )
                        {
                            // if (DataBinder.Eval(e.Row.DataItem, "Attach Record") != null)
                            // mkaran2 - MITS 32560 -start
                            DataRowView rowData = e.Row.DataItem as DataRowView;
                            if (rowData.Row.Table.Columns.Contains("Attach Record") && DataBinder.Eval(e.Row.DataItem, "Attach Record") != null)  // mkaran2 - MITS 32560 -end
                            {
                                if (DataBinder.Eval(e.Row.DataItem, "Attach Record").ToString().CompareTo("<No Data>") != 0)
                                    hdnAttachedPrompt.Value = DataBinder.Eval(e.Row.DataItem, "Attach Record").ToString();
                                else
                                    hdnAttachedPrompt.Value = "";
                                e.Row.Cells[0].Attributes["onclick"] = String.Format("selRecord({0},'{1}'); return false;", e.Row.Cells[e.Row.Cells.Count - 1].Text, hdnAttachedPrompt.Value);
                            }
                            else
                            {
                                hdnAttachedPrompt.Value = "";
                                e.Row.Cells[0].Attributes["onclick"] = String.Format("selRecord({0},'{1}'); return false;", e.Row.Cells[e.Row.Cells.Count - 1].Text, hdnAttachedPrompt.Value);
                            
                            }                      
                            e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;                           
                        }
                        else
                        {
                            //e.Row.Cells[0].Attributes["onclick"] = String.Format("selRecord({0},'{1}'); return false;", e.Row.Cells[e.Row.Cells.Count - 1].Text, sName);
                           //mbahl3 mits 30224
                            //avipinsrivas start : Worked for JIRA - 7767
						    if (e.Row.Cells[e.Row.Cells.Count - 1].Text.IndexOf('|') > 0)
                            {
                                sIsActiveEntity = e.Row.Cells[e.Row.Cells.Count - 1].Text.Split('|')[1];
                                e.Row.Cells[0].Attributes["onclick"] = String.Format("selRecord({0},\"{1}\",'{2}'); return false;", e.Row.Cells[e.Row.Cells.Count - 1].Text.Split('|')[0], sName, sIsActiveEntity, bUseEntityRole);//MITS 25139
                            }
                            else
                                e.Row.Cells[0].Attributes["onclick"] = String.Format("selRecord({0},\"{1}\",'{2}'); return false;", e.Row.Cells[e.Row.Cells.Count - 1].Text, sName, sIsActiveEntity, bUseEntityRole);
                         //   e.Row.Cells[0].Attributes["onclick"] = String.Format("selRecord({0},\"{1}\"); return false;", e.Row.Cells[e.Row.Cells.Count - 1].Text, sName);//MITS 25139
						 //mbahl3 mits 30224
                            //avipinsrivas end
                            e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                        }
                    }

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                    else
                    {
                        e.Row.CssClass = "rowlight2";
                    }
                    
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                        e.Row.Cells[i].Wrap = true;//Changed to true by mdhamija : MITS 25829

                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;//sharishkumar Jira RMA-547
                }
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    StringBuilder sHeaderText = new StringBuilder();
                    int iNoSortIndex = 0;

                    //Mridul. 11/26/09. MITS:18229
                    //if (searchcat.Value.ToLower() == "entity")
                    if (string.Compare(searchcat.Value, "entity", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(searchcat.Value, "policyenh", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                        e.Row.Cells[e.Row.Cells.Count - 2].Visible = false;
                    }
                    else
                    {
                        e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                    }

                    foreach (TableCell cell in e.Row.Cells)
                    {
                        sHeaderText.Append(((LinkButton)cell.Controls[0]).Text);
                        sHeaderText.Append("^*^*^");
                        cell.Wrap = false;
                    }
                    hdHeaderText.Value = sHeaderText.ToString();

                    // Do this processing only if we have atleast one column where we dont want sorting.
                    if (!string.IsNullOrEmpty(hdNoSortIndex.Value))
                    {
                        string[] sSeparator = { "," };

                        foreach (TableCell cell in e.Row.Cells)
                        {
                            string[] arrNoSortIndex = hdNoSortIndex.Value.Split(sSeparator,StringSplitOptions.RemoveEmptyEntries);
                            for (int iCount = 0; iCount < arrNoSortIndex.Length; iCount++)
                            {
                                if (arrNoSortIndex[iCount] == iNoSortIndex.ToString())
                                {
                                    ((LinkButton)cell.Controls[0]).Enabled = false;
                                    break;                                 
                                }
                            }
                            iNoSortIndex++;
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void grdSearchResults_Sorting(object sender, GridViewSortEventArgs e)
        {
            XElement xmlIn = null;
            XElement objTempElement = null;
            XElement objXmlCriteria = null;
            string sColumnName = string.Empty;
            string sSortColumn = string.Empty;
            string sSortOrder = string.Empty;

            string sSortExpression = e.SortExpression;
            sColumnName = e.SortExpression;

            string[] sArrSeparator = { "^*^*^" };
            string sHeaderText = hdHeaderText.Value;

            try
            {
                if (Request.Form["hdCriteriaXml"] != null)
                    objXmlCriteria = XElement.Parse(Server.HtmlDecode(Request.Form["hdCriteriaXml"]));

                string[] arrHeader = sHeaderText.Split(sArrSeparator,StringSplitOptions.RemoveEmptyEntries);

                for (int iCount = 0; iCount < arrHeader.Count(); iCount++)
                {
                    if (arrHeader[iCount].ToLower() == sColumnName.ToLower())
                    {
                        sSortColumn = Conversion.ConvertObjToStr(iCount + 1);
                        hdSortColumn.Value = sSortColumn;
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(sSortColumn))
                {
                    objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/SortColumn");
                    objTempElement.Value = sSortColumn;
                }

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;

                    objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/Order");
                    sSortOrder = "descending";
                    hdSortOrder.Value = sSortOrder;
                    objTempElement.Value = sSortOrder;
                    //SortGridView(sSortExpression, " DESC");
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;

                    objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/Order");
                    sSortOrder = "ascending";
                    hdSortOrder.Value = sSortOrder;
                    objTempElement.Value = sSortOrder;
                    //SortGridView(sSortExpression, " ASC");
                }

                objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/PageNo");
                objTempElement.Value = hdCurrentPage.Value;

                objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/TotalRecords");
                objTempElement.Value = hdTotalRows.Value;
                
                // Call CWS and get the result xml.
                xmlIn = GetSearchResults(objXmlCriteria);

                // Pass the result Xml to BindDataToGrid() method which will bind it to GridView.

                //abansal23 09/15/2009: MITS 17771 - Search print not showing the correct results after client sorts by a column
                hdCriteriaXml.Value = Server.HtmlEncode(objXmlCriteria.ToString());
                BindDataToGrid(xmlIn);
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)

                    //ViewState["sortDirection"] = SortDirection.Ascending;  Commented by csingh7 : MITS 17576
                    ViewState["sortDirection"] = SortDirection.Descending;

                return (SortDirection)ViewState["sortDirection"];

            }
            set { ViewState["sortDirection"] = value; }
        }


        protected void LinkCommand_Click(object sender, CommandEventArgs e)
        {
            XElement objTempElement = null;
            XElement objXmlCriteria = null;
            XElement objResultXml = null;
            string sSortColumn = string.Empty;
            string sSortOrder = string.Empty;
            int iCurrentPage = 0;
            int iPreviousPage = 0;
            int iPageSize = Conversion.ConvertObjToInt(hdPageSize.Value); ;
            int iTotalRows = Conversion.ConvertObjToInt(hdTotalRows.Value); ;

            if (Request.Form["hdCriteriaXml"] != null)
                objXmlCriteria = XElement.Parse(Server.HtmlDecode(Request.Form["hdCriteriaXml"]));

            if (Request.Form["hdSortColumn"] != null)
                sSortColumn = Request.Form["hdSortColumn"];

            if (Request.Form["hdSortOrder"] != null)
                sSortOrder = Request.Form["hdSortOrder"];


            lnkFirstTop.Enabled = true;
            lnkLastTop.Enabled = true;
            lnkPrevTop.Enabled = true;
            lnkNextTop.Enabled = true;
            lnkFirstDown.Enabled = true;
            lnkLastDown.Enabled = true;
            lnkPrevDown.Enabled = true;
            lnkNextDown.Enabled = true;

            switch (e.CommandName.ToUpper())
            {
                case "FIRST":
                    {
                        //TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
                        //if (txtpagenumber != null)
                        //    txtpagenumber.Text = "1";
                        //ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;
                        iCurrentPage = 0;
						
						//JIRA-RMA-1535 start
                        lblPageRangeTop.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblPage", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), " 1 ", hdTotalPages.Value);
                        lblPageRangeDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblPage", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), " 1 ", hdTotalPages.Value);
						
						lblPagerTop.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), ((iCurrentPage * iPageSize) + 1), ((iCurrentPage * iPageSize) + iPageSize), iTotalRows);
                        lblPagerDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), ((iCurrentPage * iPageSize) + 1), ((iCurrentPage * iPageSize) + iPageSize), iTotalRows);
						//JIRA-RMA-1535 end
						
                        lnkFirstTop.Enabled = false;
                        lnkPrevTop.Enabled = false;
                        lnkFirstDown.Enabled = false;
                        lnkPrevDown.Enabled = false;

                        objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/PageNo");
                        objTempElement.Value = "1";
                        break;
                    }
                case "LAST":
                    {
                        //TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");
                        //TextBox txtpagecount = (TextBox)this.Form.FindControl("pagecount");
                        //if (txtpagenumber != null && txtpagecount != null && txtpagecount.Text != "")
                        //    txtpagenumber.Text = txtpagecount.Text;
                        //ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;
                        iCurrentPage = Conversion.ConvertStrToInteger(hdTotalPages.Value) - 1;

                        //MITS 35356 srajindersin 2/17/2014
						//JIRA-RMA-1535 start
                        lblPageRangeTop.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblPage", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), hdTotalPages.Value, hdTotalPages.Value);
                        lblPageRangeDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblPage", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), hdTotalPages.Value, hdTotalPages.Value);
                        lblPagerTop.Text= string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), ((iCurrentPage * iPageSize) + 1), iTotalRows, iTotalRows);
						lblPagerDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), ((iCurrentPage * iPageSize) + 1), iTotalRows, iTotalRows);
						//JIRA-RMA-1535 end
                        lnkLastTop.Enabled = false;
                        lnkNextTop.Enabled = false;
                        lnkLastDown.Enabled = false;
                        lnkNextDown.Enabled = false;

                        objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/PageNo");
                        objTempElement.Value = hdTotalPages.Value;
                        break;
                    }
                case "PREV":
                    {
                        //TextBox txtthispage = (TextBox)this.Form.FindControl("thispage");
                        //TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");

                        //if (txtpagenumber != null && txtthispage != null && txtthispage.Text != "")
                        //    txtpagenumber.Text = Convert.ToString(Convert.ToInt32(txtthispage.Text) - 1);
                        //ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;


                        iCurrentPage = Conversion.ConvertStrToInteger(hdCurrentPage.Value);


                        iPreviousPage = iCurrentPage - 1;

                        if (iPreviousPage == 1)
                        {
                            lnkFirstTop.Enabled = false;
                            lnkPrevTop.Enabled = false;
                            lnkFirstDown.Enabled = false;
                            lnkPrevDown.Enabled = false;
                        }

						//JIRA-RMA-1535 start
                        lblPageRangeTop.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblPage", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), iPreviousPage, hdTotalPages.Value);
                        lblPageRangeDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblPage", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), iPreviousPage, hdTotalPages.Value);

                        lblPagerTop.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), (((iPreviousPage - 1) * iPageSize) + 1), ((((iPreviousPage - 1) * iPageSize) + iPageSize)), iTotalRows);
                        lblPagerDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), (((iPreviousPage - 1) * iPageSize) + 1), ((((iPreviousPage - 1) * iPageSize) + iPageSize)), iTotalRows);
						//JIRA-RMA-1535 end
                        objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/PageNo");
                        objTempElement.Value = Conversion.ConvertObjToStr(iCurrentPage - 1);
                        break;
                    }
                case "NEXT":
                    {
                        //TextBox txtthispage = (TextBox)this.Form.FindControl("thispage");
                        //TextBox txtpagenumber = (TextBox)this.Form.FindControl("pagenumber");

                        //if (txtpagenumber != null && txtthispage != null && txtthispage.Text != "")
                        //    txtpagenumber.Text = Convert.ToString(Convert.ToInt32(txtthispage.Text) + 1);
                        //ddlPage.SelectedIndex = Convert.ToInt32(txtpagenumber.Text) - 1;
                        iCurrentPage = Conversion.ConvertStrToInteger(hdCurrentPage.Value);

                        if ((iCurrentPage + 1) == Conversion.ConvertObjToInt(hdTotalPages.Value))
                        {
                            lnkLastTop.Enabled = false;
                            lnkNextTop.Enabled = false;
                            lnkLastDown.Enabled = false;
                            lnkNextDown.Enabled = false;
                        }
						//JIRA-RMA-1535 start
                        lblPageRangeTop.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblPage", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), (iCurrentPage + 1), hdTotalPages.Value);
                        lblPageRangeDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblPage", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), (iCurrentPage + 1), hdTotalPages.Value);
						
                        if (((iCurrentPage * iPageSize) + iPageSize) > iTotalRows) // Last Page
                        {
                            lblPagerTop.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), ((iCurrentPage * iPageSize) + 1), iTotalRows, iTotalRows);
                            lblPagerDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), ((iCurrentPage * iPageSize) + 1), iTotalRows, iTotalRows);
                        }
                        else
                        {
                            lblPagerTop.Text= string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), ((iCurrentPage * iPageSize) + 1), (((iCurrentPage * iPageSize) + iPageSize)), iTotalRows);
                            lblPagerDown.Text = string.Format(AppHelper.GetResourceValue(sPageId,"lblMatches", ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), ((iCurrentPage * iPageSize) + 1), (((iCurrentPage * iPageSize) + iPageSize)), iTotalRows);
                        }
						//JIRA-RMA-1535 end
                        objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/PageNo");
                        objTempElement.Value = Conversion.ConvertObjToStr(iCurrentPage + 1);
                        break;
                    }
            }


            objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/TotalRecords");
            objTempElement.Value = Conversion.ConvertObjToStr(iTotalRows);


            if (!string.IsNullOrEmpty(sSortColumn))
            {
                objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/SortColumn");
                objTempElement.Value = sSortColumn;
            }

            if (!string.IsNullOrEmpty(sSortOrder))
            {
                objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/Order");
                objTempElement.Value = sSortOrder;
            }
            
            // Call CWS and get the result xml.
            objResultXml = GetSearchResults(objXmlCriteria);

            // Pass the result Xml to BindDataToGrid() method which will bind it to GridView.
            BindDataToGrid(objResultXml);
        }

        private void AssignResultHeaderValues(XElement p_objResultXml)
        {
            if (p_objResultXml.Attribute("totalpages") != null)
                hdTotalPages.Value = p_objResultXml.Attribute("totalpages").Value;

            if (p_objResultXml.Attribute("pageno") != null)
                hdCurrentPage.Value = p_objResultXml.Attribute("pageno").Value;

            if (p_objResultXml.Attribute("SortColumn") != null)
                hdSortColumn.Value = p_objResultXml.Attribute("SortColumn").Value;

            if (p_objResultXml.Attribute("Order") != null)
                hdSortOrder.Value = p_objResultXml.Attribute("Order").Value;

            if (p_objResultXml.Attribute("searchcat") != null)
                searchcat.Value = p_objResultXml.Attribute("searchcat").Value;

            if (p_objResultXml.Attribute("admtable") != null)
                admtable.Value = p_objResultXml.Attribute("admtable").Value;


            if (p_objResultXml.Attribute("totalrows") != null) //Added Null Check for Mits:18757
                hdTotalRows.Value = p_objResultXml.Attribute("totalrows").Value;

            if (p_objResultXml.Attribute("pagesize") != null) //Added Null Check for Mits:18757
                hdPageSize.Value = p_objResultXml.Attribute("pagesize").Value;
        }

        // This function makes sure that we do not add 2 columns with the same to the data table. 
        // For now we are just adding numeric identifiers to distinguish between the names but
        // later on this needs to be handled at the "Query Designer" level.This is mits 15062.
        private DataColumn GetDataColumn(string p_sColumnName,DataTable p_objDataTable)
        {
            DataColumn objColumn = new DataColumn();
            int iColumnCount = 1;
            string sColumnName = string.Empty;

            objColumn.Caption = p_sColumnName;

            if (p_objDataTable.Columns.Contains(p_sColumnName))
            {
                sColumnName = p_sColumnName + iColumnCount.ToString();
                while (p_objDataTable.Columns.Contains(sColumnName))
                {
                    iColumnCount++;
                    sColumnName = p_sColumnName + iColumnCount.ToString();
                }
                objColumn.ColumnName = sColumnName;
            }
            else
            {
                objColumn.ColumnName = p_sColumnName;
            }

            return objColumn;
        }

        //private string AppHelper.GetResourceValue(sPageId,string strResourceKey, string strResourceType)
        //{
        //    return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, sPageId, strResourceType,ClientID).ToString();
        //}
        // This function is not being used right now.We might consider using this in future(R6 may be) when doing
        // sorting directly using datatable and not depending on the xml response cws.
        //private void SortGridView(string sortExpression, string direction)
        //{
        //    DataTable m_dtSearch = (DataTable)Cache["m_dtSearch"];
        //    DataView dv = new DataView(m_dtSearch);
        //    dv.Sort = sortExpression + direction;
        //    grdSearchResults.PageSize = m_iPagesize;
        //    grdSearchResults.DataSource = dv;
        //    grdSearchResults.DataBind();
        //}

}
}











