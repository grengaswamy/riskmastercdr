﻿/**********************************************************************************************
 *   Date     |  JIRA   | Programmer | Description                                            *
 **********************************************************************************************
 * 11/02/2014 | RMA-345  | achouhan3 | New Angular Framework Ng-Grid is implemented on search Screen
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.UI.Search
{
    public partial class PrintSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement objXmlCriteria = null;
            XElement objXmlResult = null;
            XElement objTempElement = null;

            try
            {
                if (!String.IsNullOrEmpty(hdCriteriaXml.Value))
                {
                    //RMA-345 Starts    achouhan3   Commented to URLDecode Xml since in case of NG-Grid it we URLEncode Xml to  communicate
                    //objXmlCriteria = XElement.Parse(Server.HtmlDecode(hdCriteriaXml.Value));
                    objXmlCriteria = XElement.Parse(Server.HtmlDecode(Server.UrlDecode(hdCriteriaXml.Value)));
                    //RMA-345 Starts    achouhan3   Commented to URLDecode Xml since in case of NG-Grid we URLEncode Xml to  communicate

                    objTempElement = objXmlCriteria.XPathSelectElement("./Call/Function");
                    objTempElement.Value = "SearchAdaptor.PrintSearch";

                    // Call CWS and get the result xml.
                    objXmlResult = GetSearchResults(objXmlCriteria);

                    string sFileContent = string.Empty;
                    if (objXmlResult != null)
                    {
                        sFileContent = objXmlResult.Descendants("File").First().Value;
                    }

                    byte[] objPdfBytes = Convert.FromBase64String(sFileContent);

                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Charset = "";
                    Response.AppendHeader("Content-Encoding", "none;");
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", string.Format("inline; filename=Search.pdf"));
                    Response.AddHeader("Accept-Ranges", "bytes");
                    Response.BinaryWrite(objPdfBytes);
                    Response.Flush();
                    Response.Close();
                }
            }
            catch (Exception ex)
            {
                //error.Text = "ERROR: " + ex.Message.ToString();
                //throw new Exception("Pdf Error", ex.GetBaseException());
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetSearchResults(XElement p_objCriteriaXML)
        {
            XmlDocument objXmlCriteriaDom = new XmlDocument();
            objXmlCriteriaDom.LoadXml(p_objCriteriaXML.ToString());

            string sReturn = AppHelper.CallCWSService(objXmlCriteriaDom.InnerXml.ToString());

            //Binding Error Control
            ErrorControl.errorDom = sReturn;

            objXmlCriteriaDom.LoadXml(sReturn);
            XmlNode oInstanceNode = objXmlCriteriaDom.SelectSingleNode("/ResultMessage/Document");

            XElement objResultXml = XElement.Parse(oInstanceNode.InnerXml.ToString());

            return objResultXml;
        }
    }
}
