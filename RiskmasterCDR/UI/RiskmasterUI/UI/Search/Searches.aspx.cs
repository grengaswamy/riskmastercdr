﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.Utilities
{
    public partial class Searches : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                NonFDMCWSPageLoad("RMAdminSettingsAdaptor.GetAdminConfig");
            }

        }
        protected void Save(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            bReturnStatus = CallCWSFunction("RMAdminSettingsAdaptor.SaveAdminConfig");
            if (bReturnStatus)
            {
                TextBox txtFileName = (TextBox)this.Form.FindControl("fileName");
                if (txtFileName != null && txtFileName.Text == "")
                    txtFileName.Text = "customize_search";
                bReturnStatus = CallCWSFunction("RMAdminSettingsAdaptor.GetAdminConfig");
            }

        }
        protected void Refresh(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            TextBox txtFileName = (TextBox)this.Form.FindControl("fileName");
            if (txtFileName != null && txtFileName.Text == "")
                txtFileName.Text = "customize_search";
            bReturnStatus = CallCWSFunction("RMAdminSettingsAdaptor.GetAdminConfig");
           

        }
    }
}
