﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections.Generic;

namespace Riskmaster.UI.BankAccount
{
    public partial class DisbursementAccountBalanceInfo : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";

            try
            {
                accountid.Text = AppHelper.GetQueryStringValue("AccountId");
                subaccountid.Text = AppHelper.GetQueryStringValue("SubAccountId");
                monmktacc.Text = AppHelper.GetQueryStringValue("MonMktAcc");
                //recordid.Text = AppHelper.GetQueryStringValue("RecordId ");

                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWSFunctionBind("BankAccountManagementAdaptor.LoadSubBankDisbursementAccount", out sCWSresponse, XmlTemplate);

                btnLoadPriorStatementDate.Attributes.Add("onclick", "LoadPriorStatementDate('" + sdate.Text + "');return false;");

                XmlTemplate = XElement.Parse(sCWSresponse);

                IEnumerable<XElement> elements = XmlTemplate.XPathSelectElements("./Document/DisbursementAccountInfo/Statements/Statement");
                IEnumerable<XAttribute> attList;

                int iCount = 1;
                foreach (XElement el in elements)
                {
                    TextBox objTextBox = new TextBox();
                    objTextBox.ID = "recordid_" + iCount;
                    objTextBox.Text = el.Value;
                    this.Form.Controls.Add(objTextBox);
                    objTextBox.Attributes.Add("style", "display:none");

                    objTextBox = new TextBox();
                    objTextBox.ID = "statementbegindate_" + iCount;
                    attList = el.Attributes("StatementBeginDate");
                    foreach (XAttribute att in attList)
                        objTextBox.Text = att.Value;
                    this.Form.Controls.Add(objTextBox);
                    objTextBox.Attributes.Add("style", "display:none");

                    objTextBox = new TextBox();
                    objTextBox.ID = "statementenddate_" + iCount;
                    attList = el.Attributes("StatementEndDate");
                    foreach (XAttribute att in attList)
                        objTextBox.Text = att.Value;
                    this.Form.Controls.Add(objTextBox);
                    objTextBox.Attributes.Add("style", "display:none");

                    objTextBox = new TextBox();
                    objTextBox.ID = "balanceperbankstatement_" + iCount;
                    attList = el.Attributes("BalancePerBankStatement");
                    foreach (XAttribute att in attList)
                        objTextBox.Text = att.Value;
                    this.Form.Controls.Add(objTextBox);
                    objTextBox.Attributes.Add("style", "display:none");

                    iCount++;


                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }



        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<AccountBalance>");
            sXml = sXml.Append("<AccountId>");
            sXml = sXml.Append(accountid.Text);
            sXml = sXml.Append("</AccountId>");
            sXml = sXml.Append("<SubAccountId>");
            sXml = sXml.Append(subaccountid.Text);
            sXml = sXml.Append("</SubAccountId>");
            sXml = sXml.Append("<MonMktAcc>");
            sXml = sXml.Append(monmktacc.Text);
            sXml = sXml.Append("</MonMktAcc>");
            sXml = sXml.Append("<RecordId>");
            sXml = sXml.Append(recordid.Text);
            sXml = sXml.Append("</RecordId>");
            sXml = sXml.Append("</AccountBalance></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());


            return oTemplate;
        }
    }
}
