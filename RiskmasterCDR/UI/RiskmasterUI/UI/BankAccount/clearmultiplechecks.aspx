﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="clearmultiplechecks.aspx.cs" Inherits="Riskmaster.UI.BankAccount.clearmultiplechecks" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Select Deposits to Clear</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/system.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/deposit.js"/>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />  
   <script type="text/javascript">

       function OnCancel() {
           self.close();
           return true;
       }

       function ClosePageIfCleared() {
           if (document.forms[0].all("ouraction").value == 'OK') {
               //alert('The chosen checks have been successfully marked as cleared.');
               alert(clearmultiplechecksValidations.ChkClearedID);
               document.forms[0].all("ouraction").value = '';
               self.close();
           }
       }						
							
				</script>
    
    
</head>
<body>
    <form id="frmData" runat="server">
    <div>
    <table bgcolor="white" border="0" width="100%">
      <tr>
      <td colspan="2">
        <uc3:ErrorControl ID="ErrorControl1" runat="server" />
       </td>
    </tr> 
    <tr>	
    <td colspan="5" class="msgheader">
        <asp:Label ID="lblCurrentBankAccntID" runat="server" Text="<%$ Resources:lblCurrentBankAccntID %>"></asp:Label>
    <asp:Label runat="server" ID="lblCurrentBank"></asp:Label>
    </td>
						</tr>			
    <tr>
    <td>
    <asp:GridView DataKeyNames="DepositId" ID="Deposits" runat="server" BorderWidth="1" 
            CellSpacing="1" CellPadding="1"  AllowPaging="false" Width="100%"  
            AutoGenerateColumns="false"  GridLines="Both" 
            ondatabound="Deposits_DataBound" onrowdatabound="Deposits_RowDataBound">
         <%-- onrowdatabound="GridView1_RowDataBound" >--%>
		              <HeaderStyle CssClass="colheader3" />
		              <AlternatingRowStyle CssClass="rowlight2" />
		              <RowStyle CssClass="rowlight1" />
		              <Columns>
		                  <asp:TemplateField  ControlStyle-Width="4%" ItemStyle-CssClass="data">
                                    <ItemTemplate>                               
                                        <asp:CheckBox runat="server" id="checkedrow"/>
                                    </ItemTemplate> 
                                    <HeaderTemplate> 
                                    <input type="checkbox" id="chkDeposit" name="chkDeposit" onclick="DepositClick();"/>                               
                                    </HeaderTemplate>
                          </asp:TemplateField> 
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="<%$ Resources:gvHdrDepositId %>" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DepositId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="<%$ Resources:gvHdrControlNumber %>" Visible="true" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblControlNumber" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ControlNo")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="<%$ Resources:gvHdrTransactionDate %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTransactionDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransDate")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="<%$ Resources:gvHdrAmount %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="<%$ Resources:gvHdrDepositType %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblDepositType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DepositType")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
		              </Columns> 
		           </asp:GridView>
		       </td>
		    </tr>   	    
        </table>        
        				<table border="0">
						<tr>
							<td>
								<asp:Button runat="server" id="btnAll" name="btnAll" Text="<%$ Resources:btnAll %>" onClientClick="return selectAll();" />
							</td>
							<td>
								<asp:Button runat="server" id="btnDeselectAll" name="btnDeselectAll" Text="<%$ Resources:btnDeselectAll %>" onClientClick="return deselectAll();" />
							</td>
							<td>
							    <asp:Button runat="server" id="btnOk" text="<%$ Resources:btnOk %>"  
                                    OnClientClick="return ClearDeposits();" />								
							</td>
							<td>
    							<asp:Button ID="Cancel" runat="server" text="<%$ Resources:btnCancel %>" OnClientClick="return OnCancel();" />								
							</td>
						</tr>
					</table>
					
	    <asp:textbox runat="server" id="ouraction" style="display:none"/>   
		<asp:textbox runat="server" style="display:none" id="accountid"/>
		<asp:textbox runat="server" style="display:none" id="subaccountid"/>
		<asp:textbox runat="server" style="display:none" id="selecteddepositids"/>       
        		    
    </div>
    </form>
</body>
</html>
