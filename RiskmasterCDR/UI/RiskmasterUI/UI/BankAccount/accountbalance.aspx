﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="accountbalance.aspx.cs" Inherits="Riskmaster.UI.NonFDMCWS.AccountBalance" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"  TagPrefix="mc" %>
    

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
 <link rel="StyleSheet" href="../../Content/rmnet.css" type="text/css" />  
 <title>Account Balance Information</title>
 <uc4:CommonTasks ID="CommonTasks1" runat="server" /> 
    
</head>
<body>
    <form id="frmData" name="frmData" runat="server">
    <div>
    <table>   
    <tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
         </tr>
    
    <tr>
     <td colspan="8" class="ctrlgroup">
         <asp:Label ID="lblAccntBalInfo" runat="server" Text="<%$ Resources:lblAccntBalInfo %>"></asp:Label></td>
    </tr>
    <!-- hlv MITS 27587 7/25/12 begin -->
    <tr>
        <td align="right"><asp:Label ID="lblBankAccntNameID" runat="server" Text="<%$ Resources:lblBankAccntNameID %>"></asp:Label></td>
        <td class="datatd" colspan="4"><asp:Label ID="labBAName" runat="server" RMXRef="Instance/Document/AccountBalanceInfo/@BankAccountName"></asp:Label></td>
    </tr>
    <!-- hlv MITS 27587 7/25/12 end -->
    <tr>
     <td align="right"><asp:Label ID="lblBalPerBankStatID" runat="server" Text="<%$ Resources:lblBalPerBankStatID %>"></asp:Label></td>
     <td class="datatd"><mc:CurrencyTextbox   runat="server" RMXRef="Instance/Document/AccountBalanceInfo/@BalancePerBankStatement"  Enabled="false"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label ID="lblStatementDatesID" runat="server" Text="<%$ Resources:lblStatementDatesID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox id="txtStatementBeginDate" RMXRef="Instance/Document/AccountBalanceInfo/@StatementBeginDate" runat="server" type="text" text="N/A" Enabled="false"/>--<asp:textbox ID="Textbox1" RMXRef="Instance/Document/AccountBalanceInfo/@StatementEndDate" runat="server" type="text" text="N/A" Enabled="false"  />
	 <asp:button runat="server" class="CodeLookupControl" id="btnLoadPriorStatementDate"/>
     </td>
    </tr>
    <tr>
     <td align="right"><asp:Label ID="lblDepInTransitID" runat="server" Text="<%$ Resources:lblDepInTransitID %>"></asp:Label></td>
     <td class="datatd"><mc:CurrencyTextbox  runat="server" RMXRef="Instance/Document/AccountBalanceInfo/@DepositsInTransit" Enabled="false"  name="$node^27"  disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label ID="lblDepositsID" runat="server" Text="<%$ Resources:lblDepositsID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox2" runat="server" RMXRef="Instance/Document/AccountBalanceInfo/@NumDT" type="text" Enabled="false" name="$node^34" value="0" size="10" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label ID="lblOutstandingChksID" runat="server" Text="<%$ Resources:lblOutstandingChksID %>"></asp:Label></td>
     <td class="datatd"><mc:CurrencyTextbox  runat="server" RMXRef="Instance/Document/AccountBalanceInfo/@OutstandingChecks"  Enabled="false" name="$node^28" disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label ID="lblChecksID" runat="server" Text="<%$ Resources:lblChecksID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox3" runat="server" RMXRef="Instance/Document/AccountBalanceInfo/@NumOC" type="text" Enabled="false" name="$node^35" value="0" size="10" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label ID="lblSubTotalID" runat="server" Text="<%$ Resources:lblSubTotalID %>"></asp:Label></td>
     <td class="datatd"><mc:CurrencyTextbox runat="server" RMXRef="Instance/Document/AccountBalanceInfo/@SubTotal"  Enabled="false" name="$node^29"  disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label ID="lblReconNumChksID" runat="server" Text="<%$ Resources:lblReconNumChksID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox4" RMXRef="Instance/Document/AccountBalanceInfo/@NumDeposits" runat="server" type="text" Enabled="false" name="$node^36" value="0" size="10" disabled="true"/><mc:CurrencyTextbox  RMXRef="/Instance/Document/AccountBalanceInfo/@ClearedDeposits" runat="server"  name="$node^37"  disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label ID="lblBalancePerBooksID" runat="server" Text="<%$ Resources:lblBalancePerBooksID %>"></asp:Label></td>
     <td class="datatd"><mc:CurrencyTextbox RMXRef="Instance/Document/AccountBalanceInfo/@BalancePerBooks" runat="server"  name="$node^30"  disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label ID="lblReconTotChksID" runat="server" Text="<%$ Resources:lblReconTotChksID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox5" RMXRef="Instance/Document/AccountBalanceInfo/@NumChecks" runat="server" type="text" name="$node^38" value="0" size="10" disabled="true"/><mc:CurrencyTextbox RMXref="/Instance/Document/AccountBalanceInfo/@ClearedChecks" runat="server"  name="$node^39" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label ID="lblOutOfBalanceID" runat="server" Text="<%$ Resources:lblOutOfBalanceID %>"></asp:Label></td>
     <td class="datatd"><mc:CurrencyTextbox RMXRef="Instance/Document/AccountBalanceInfo/@OutOfBalance" runat="server"  name="$node^31"  disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label ID="lblReconTotAdjustmentsID" runat="server" Text="<%$ Resources:lblReconTotAdjustmentsID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox6" RMXRef="Instance/Document/AccountBalanceInfo/@NumAdjust" runat="server" type="text" name="$node^40" value="0" size="10" disabled="true"/><mc:CurrencyTextbox RMXRef="Instance/Document/AccountBalanceInfo/@Adjustments" runat="server"  name="$node^41" disabled="true"/></td>
    </tr>
    <tr>
     <td></td>
     <td></td>
     <td></td>
     <td align="center">
     <asp:button  runat="server" text="<%$ Resources:btnClose %>" class="button" id="btnClose" OnClientClick="window.close();"/>     
     </td>
    </tr>
   </table>        
    <asp:textbox ID="hdStatementCount" style="display:none" RMXRef="Instance/Document/AccountBalanceInfo/Statements/@Count" runat="server" />
    <asp:textbox ID="ouraction" style="display:none" runat="server" />
    <asp:textbox ID="accountid" style="display:none" RMXRef="Instance/Document/AccountId" runat="server" />
    <asp:textbox ID="recordid" style="display:none" RMXRef="Instance/Document/RecordId" runat="server" />
    <asp:textbox ID="refreshpage" style="display:none" RMXRef="Instance/Document/RefreshPage" runat="server" />    
    					
    </div>
    
    </form>
</body>
</html>
