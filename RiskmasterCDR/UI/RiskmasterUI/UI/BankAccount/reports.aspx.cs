﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;

namespace Riskmaster.UI.BankAccount
{
    public partial class reports : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {     
         
            try
            {
                accountid.Text = AppHelper.GetQueryStringValue("AccountId");
                //XmlTemplate = GetMessageTemplate();            
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }



        //private XElement GetMessageTemplate()
        //{
        //    StringBuilder sXml = new StringBuilder("<Message>");
        //    sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
        //    sXml = sXml.Append("<Call><Function></Function></Call><Document>");

        //    sXml = sXml.Append("<AccountBalance>");
        //    sXml = sXml.Append("<AccountId>");
        //    sXml = sXml.Append(accountid.Text);
        //    sXml = sXml.Append("</AccountId>");
        //    sXml = sXml.Append("</AccountBalance></Document></Message>");

        //    XElement oTemplate = XElement.Parse(sXml.ToString());
        //    return oTemplate;
        //}

    

    }
}
