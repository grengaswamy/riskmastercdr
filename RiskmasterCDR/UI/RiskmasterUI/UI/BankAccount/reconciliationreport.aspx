﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="reconciliationreport.aspx.cs" Inherits="Riskmaster.UI.BankAccount.reconciliationreport" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reconciliation Report</title>
    <!-- Amit 11/23/2009: Content, file name etc. are being handled using javascript in place of session variables  -->
    <script type="text/javascript">
        function OnPageLoaded() 
        {
            if (document.forms[0].hdFileContent.value =='')
            {
                if (window.opener.document.forms[0].hdFileContent != null)
                    document.forms[0].hdFileContent.value = window.opener.document.forms[0].hdFileContent.value;
                if (window.opener.document.forms[0].filename != null)
                    document.forms[0].filename.value = window.opener.document.forms[0].filename.value;
                if (window.opener.document.forms[0].AccountNoLength != null)
                {
                    if (window.opener.document.forms[0].AccountNoLength.checked)
                        document.forms[0].accountlength.value = "1";
                    else
                        document.forms[0].accountlength.value = "";
                }
                document.forms[0].submit();
            }         
        }
    </script>
</head>
<body onload="OnPageLoaded();"> <!-- Amit 11/23/2009 -->
    <form id="frmData" name="frmData" runat="server">
    <div>      
    <table>       
    <tr>
      <td colspan="2">
        <uc3:ErrorControl ID="ErrorControl1" runat="server" />
       </td>
    </tr> 
     <tr>                
     <asp:textbox ID="accountlength" style="display:none" runat="server" />
     <asp:textbox ID="filename" style="display:none" runat="server" />         
     <asp:textbox ID="hdFileContent" style="display:none" runat="server" />     	
    </tr>          
    </table>     
    </div>
    </form>
</body>
</html>
