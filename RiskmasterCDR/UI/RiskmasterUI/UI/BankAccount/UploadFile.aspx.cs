﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Generic;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.BankAccount
{
    public partial class UploadFile : NonFDMBasePageCWS
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("UploadFile.aspx"), "UploadFileValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "UploadFileValidations", sValidationResources, true);

            try
            {
                if (FileSelected.Text == "True")
                {
                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWS("CheckStockSetupAdaptor.SaveToLocalServer", XmlTemplate, out sCWSresponse, false, true);                    

                    XmlTemplate = XElement.Parse(sCWSresponse);

                    IEnumerable<XElement> elements = XmlTemplate.XPathSelectElements("//TempFileName"); 

                    foreach (XElement el in elements)
                    {
                        TempFileCreated.Text = el.Value;
                    }

                    filename.Text = UploadFile1.PostedFile.FileName;
                }
            }
            
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
        }

        

        /// <summary>
        /// Returns Message Template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            Stream ioStream = UploadFile1.FileContent;
            string base64String;
            byte[] binaryData;

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");

            sXml = sXml.Append("<CheckStock>");
            sXml = sXml.Append("<FileContent>");

            binaryData = new Byte[ioStream.Length];
            long bytesRead = ioStream.Read(binaryData, 0,
                                    (int)ioStream.Length);
            base64String = System.Convert.ToBase64String(binaryData, 0,binaryData.Length);
            ioStream.Flush();
            ioStream.Close();

            sXml = sXml.Append(base64String);

            sXml = sXml.Append("</FileContent>");
            sXml = sXml.Append("</CheckStock>");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Message>");
            
            
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }        
    

    }
}
