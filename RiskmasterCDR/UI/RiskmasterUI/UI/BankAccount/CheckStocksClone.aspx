﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckStocksClone.aspx.cs" Inherits="Riskmaster.UI.BankAccount.CheckStocksClone" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %> 
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Check Stocks Clone</title>
    <script language="JavaScript" src="../../Scripts/CheckStockSetup.js">        { var i; } </script>
</head>
<body onload="Forpopup();">
    <form id="frmData" runat="server" >
    <div>
    <table>
    <tr>
      <td colspan="2">
        <uc3:ErrorControl ID="ErrorControl1" runat="server" />
       </td>
    </tr> 
    </table>  
    <table width="100%" cellspacing="0" cellpadding="2" border="0">
    <tr>
     <td colspan="10" class="msgheader" bgcolor="#D5CDA4">
         <asp:Label ID="lblCheckStocksCloneID" runat="server" Text="<%$ Resources:lblCheckStocksCloneID %>"></asp:Label>
     </td>
    </tr>
   </table>
   <table width="100%" cellspacing="0" cellpadding="2" border="0">
   <asp:textbox ID="clonesaved" style="display:none" runat="server" />
   <asp:textbox ID="AccountId" style="display:none" runat="server" />
   <asp:textbox ID="StockId" style="display:none" runat="server" />
   <tr>
   <asp:RadioButton ref="/Instance/Document/Data/CurBankAccount" text="<%$ Resources:rdbUseCurrentBankAccount %>" runat="server" id="UseCurrentBankAccount" onclick="Form();" GroupName="Clones"/>   
   </tr>
    <tr>
   <asp:RadioButton ref="/Instance/Document/Data/CurBankAccount" text="<%$ Resources:rdbSelectBankAccount %>" onclick="Form();" runat="server" id="SelectBankAccountFromList" GroupName="Clones"/>
    </tr>
   </table>
   <table width="100%" cellspacing="0" cellpadding="2" border="0">
    <tr>    
    <asp:DropDownList runat="server" id="List" tabindex="1" RMXRef="/Instance/Document/Data/AccountId" ItemSetRef="/Instance/Document/CheckStockSetup/Accounts" RMXType="combobox" width="205px">                            
        <%--<asp:ListItem Value="1" Text="Set-Up Account" />
        <asp:ListItem Value="2" Text="6" />
        <asp:ListItem Value="3" Text="8" />
        <asp:ListItem Value="4" Text="10" />
        <asp:ListItem Value="12" Text="12" />
        <asp:ListItem Value="14" Text="14" />
        <asp:ListItem Value="16" Text="16" />
        <asp:ListItem Value="18" Text="18" />
        <asp:ListItem Value="20" Text="20" />
        <asp:ListItem Value="22" Text="22" />
        <asp:ListItem Value="24" Text="24" />--%>
    </asp:DropDownList>   
    
    
    
<%--    <select name="$node^22" id="List">--%>


<%--      <option value="1">Set-Up Account</option>
      <option value="2">XYZ Bank Acct123</option>
      <option value="3">fff</option>
      <option value="4">TestAcc</option>
--%>      <%--</select>--%>
      </tr>
   </table>
   <table width="100%" cellspacing="0" cellpadding="2" border="0">
    <tr>
    
    <asp:button runat="server" id="btnCreate"  text="<%$ Resources:btnCreate %>" OnClick="Create"/>						
		    
	<asp:button runat="server" id="btnCancel" text="<%$ Resources:btnCancel %>" onclientclick="Close();"/>
	
		
	
    
  <%--  <input type="submit" name="$action^setvalue%26node-ids%2618%26content%26RecordSaved&amp;setvalue%26node-ids%267%26content%26Create&amp;setvalue%26node-ids%2616%26content%26Create" value="Create" id="btnCreate" class="button" style="width:100px">
    <input type="submit" name="$action^" value="Cancel" id="btnCancel" class="button" style="width:100px" onclick="Close();; ">--%>
    
    </tr>
    
   </table>
    
    </div>
    </form>
</body>
</html>
