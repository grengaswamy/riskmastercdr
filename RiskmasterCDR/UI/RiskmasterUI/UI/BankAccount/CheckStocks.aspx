<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="checkstocks.aspx.cs" Inherits="Riskmaster.UI.BankAccount.CheckStocks"
    ValidateRequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/MultiCodeLookup.ascx" TagName="MultiCode"
    TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/SystemUsers.ascx" TagName="SystemUsers" TagPrefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:cc1="remove"
xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Check Stocks</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/CheckStockSetup.js">{var i;} </script>
    <script language="JavaScript" src="../../Scripts/form.js">{var i;} </script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="javascript: return OnPageLoading();"">
<form name="frmData" id="frmData" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc3:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <asp:label id="lblError" runat="server" text="" forecolor="Red" />
    <asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" />
    <asp:textbox style="display: none"
        runat="server" name="hTabName" id="hTabName" />
    <asp:scriptmanager id="SMgr" runat="server" />
    <div
        id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_new" xmlns="" xmlns:cul="remove">
            <asp:imagebutton runat="server" onclick="NavigateNew" src="../../Images/tb_new_active.png"
                        width="28" height="28" border="0" id="new" alternatetext="<%$ Resources:ImgBtnNewID %>" title="<%$ Resources:ImgBtnNewID %>" onclientclick="Navigates();" onmouseover="this.src='../../Images/tb_new_mo.png';;this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_new_active.png';;this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick="return Save();" onclick="NavigateSave"
                        src="../../Images/tb_save_active.png" width="28" height="28" border="0" id="save" alternatetext="<%$ Resources:ImgBtnSaveID %>" title="<%$ Resources:ImgBtnSaveID %>"
                        validationgroup="vgSave" onmouseover="this.src='../../Images/tb_save_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_save_active.png';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" onclientclick="return Navigates();" src="../../Images/tb_first_active.png"
                        width="28" height="28" border="0" id="movefirst" title="<%$ Resources:ImgBtnMoveFirstID %>" alternatetext="<%$ Resources:ImgBtnMoveFirstID %>" onmouseover="this.src='../../Images/tb_first_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_first_active.png';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick="return Navigates();"  onclick="NavigatePrev" src="../../Images/tb_previous_active.png"
                        width="28" height="28" border="0" id="moveprevious" title="<%$ Resources:ImgBtnMovePreviousID %>" alternatetext="<%$ Resources:ImgBtnMovePreviousID %>"
                        onmouseover="this.src='../../Images/tb_previous_mo.png';this.style.zoom='110%'" onmouseout="this.src='../../Images/tb_previous_active.png';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick="return Navigates();" onclick="NavigateNext" src="../../Images/tb_next_active.png"
                        width="28" height="28" border="0" id="movenext" title="<%$ Resources:ImgBtnMoveNextID %>" alternatetext="<%$ Resources:ImgBtnMoveNextID %>" onmouseover="this.src='../../Images/tb_next_mo.png';;this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_next_active.png';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick="return Navigates();" onclick="NavigateLast" src="../../Images/tb_last_active.png"
                        width="28" height="28" border="0" id="movelast" title="<%$ Resources:ImgBtnMoveLastID %>" alternatetext="<%$ Resources:ImgBtnMoveLastID %>" onmouseover="this.src='../../Images/tb_last_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_last_active.png';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="" xmlns:cul="remove">
                    <asp:imagebutton runat="server" onclientclick="return ifDelete(); " onclick="NavigateDelete" src="../../Images/delete.gif"
                        width="28" height="28" border="0" id="delete" title="<%$ Resources:ImgBtnDeleteRecordID %>" alternatetext="<%$ Resources:ImgBtnDeleteRecordID %>" onmouseover="this.src='../../Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/delete.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="<%$ Resources:lblformtitleID %>" />
        <asp:label id="formsubtitle"
            runat="server" text="" />
    </div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" />
    </div>

    <%--Bijender Start Mits 14894--%>
    <div class="tabGroup" id="TabsDivGroup" runat="server">

        <div class="Selected" nowrap="true" runat="server" name="TABSBasicOptions" id="TABSBasicOptions">
            <a class="Selected" href="#" onclick="tabChange(this.name);return false;" runat="server" rmxref="" name="BasicOptions" id="LINKTABSBasicOptions">
                <asp:label id="lblBasicOptions" runat="server" text="<%$ Resources:lblBasicOptionsID %>"></asp:label>
            </a>
        </div>
        <div class="tabSpace" runat="server" id="Div2">
            &nbsp;&nbsp;
        </div>

        <div class="NotSelected" nowrap="true" runat="server" name="TABSabc" id="TABSabc">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server" rmxref="" name="abc" id="LINKTABSabc">
                <asp:label id="lblCheckImageID" runat="server" text="<%$ Resources:lblCheckImageID %>"></asp:label>
            </a>
        </div>
        <div class="tabSpace" runat="server" id="Div3">
            &nbsp;&nbsp;
        </div>

        <div class="NotSelected" nowrap="true" runat="server" name="TABSxyz" id="TABSxyz">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server" rmxref="" name="xyz" id="LINKTABSxyz">
                <asp:label id="lblAlignmentOptionsID" runat="server" text="<%$ Resources:lblAlignmentOptionsID %>"></asp:label>
            </a>
        </div>
        <div class="tabSpace" runat="server" id="Div4">
            &nbsp;&nbsp;
        </div>

        <div class="NotSelected" nowrap="true" runat="server" name="TABS123" id="TABS123">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server" rmxref="" name="123" id="LINKTABS123">
                <asp:label id="lblAdvancedOptionsID" runat="server" text="<%$ Resources:lblAdvancedOptionsID %>"></asp:label>
            </a>
        </div>
        <div class="tabSpace" runat="server" id="Div5">
            &nbsp;&nbsp;
        </div>
    </div>
    <%--Bijender, End Mits 14894--%>

    <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 800px; height: 315px; overflow: auto">

        <table name="FORMTABBasicOptions" id="FORMTABBasicOptions" border="0" cellspacing="0" cellpadding="0">


            <tr>
                <td>
                    <asp:textbox style="display: none" runat="server" id="StockId" rmxref="/Instance/Document/CheckStock/StockId"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="AccountId" rmxref="/Instance/Document/CheckStock/AccountId"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="file1"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='0']/FormSource"
                        rmxtype="hidden" />
                    <%--/Instance/Document/Data/CheckStockForms//CheckStockForm[ImageIdx ='0']/FormSource--%>
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="filename1"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='0']/FormSource/@filename"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="file2"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='1']/FormSource"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="filename2"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='1']/FormSource/@filename"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="file3"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='2']/FormSource"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="filename3"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='2']/FormSource/@filename"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="imageidx1"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='0']/ImageIdx"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="imageidx2"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='1']/ImageIdx"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="imageidx3"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='2']/ImageIdx"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="StockId1"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='0']/StockId"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="StockId2"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='1']/StockId"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="StockId3"
                        rmxref="/Instance/Document/CheckStock/CheckStockForms/CheckStockForm[ImageIdx ='2']/StockId"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="image"
                        rmxref="/Instance/Document/image"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="ViewImage"
                        rmxref="/Instance/Document/ViewImage"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="test"
                        rmxref="/Instance/Document/test"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="ouraction"
                        rmxtype="hidden" />
                    <%--rmxref="/Instance/ui/action"--%>
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="sb1"
                        rmxref="/Instance/Document/sb1"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="sb2"
                        rmxref="/Instance/Document/sb2"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="sb3"
                        rmxref="/Instance/Document/sb3"
                        rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="SysCmd"
                        rmxtype="hidden" />


                </td>

            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="required" id="lbl_StockName" text="<%$ Resources:lblStockNameID %>" />
                    &nbsp;&nbsp;
                </td>
                <td>
                    <div title="" style="padding: 0px; margin: 0px">
                        <asp:textbox runat="server" onchange="setDataChanged(true);" id="StockName"
                            rmxref="/Instance/Document/CheckStock/StockName" rmxtype="text" tabindex="1" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="label" id="lbl_FontName" text="<%$ Resources:lblFontNameID %>" />
                    &nbsp;&nbsp;
                    <td width="205px">
                        <asp:dropdownlist runat="server" id="FontName" tabindex="1" rmxref="/Instance/Document/CheckStock/FontName"
                            rmxtype="combobox" onchange="setDataChanged(true);">
                    <asp:listitem value="0" Text="" />
                    <asp:listitem value="Arial" Text="<%$ Resources:liArial %>" />
                    <asp:listitem value="Arial Black" Text="<%$ Resources:liArialBlack %>" />
                    <asp:listitem value="Arial CE" Text="<%$ Resources:liArialCE %>" />
                    <asp:listitem value="Arial Greek" Text="<%$ Resources:liArialGreek %>" />
                    <asp:listitem value="Arial CYR" Text="<%$ Resources:liArialCYR %>"/>
                    <asp:listitem value="Arial Narrow" Text="<%$ Resources:liArialNarrow %>" />
                    <asp:listitem value="Arial TUR" Text="<%$ Resources:liArialTUR %>" />
                    <asp:listitem value="Book Antiqua" Text="<%$ Resources:liBookAntiqua %>" />
                    <asp:listitem value="Bookman Old Style" Text="<%$ Resources:liBookmanOldStyle %>" />
                    <asp:listitem value="Century Gothic" Text="<%$ Resources:liCenturyGothic %>" />
                    <asp:listitem value="Comic Sans MS" Text="<%$ Resources:liComicSansMS %>" />
                    <asp:listitem value="Courier New" Text="<%$ Resources:liCourierNew %>" />
                    <asp:listitem value="Courier New CE" Text="<%$ Resources:liCourierNewCE %>" />
                    <asp:listitem value="Courier New CYR" Text="<%$ Resources:liCourierNewCYR %>" />
                    <asp:listitem value="Courier New Greek" Text="<%$ Resources:liCourierNewGreek %>" />
                    <asp:listitem value="Courier New TUR" Text="<%$ Resources:liCourierNewTUR %>" />
                    <asp:listitem value="Garamond" Text="<%$ Resources:liGaramond %>" />
                    <asp:listitem value="Georgia" Text="<%$ Resources:liGeorgia %>" />
                    <asp:listitem value="Impact" Text="<%$ Resources:liImpact %>" />
                    <asp:listitem value="Lucida Console" Text="<%$ Resources:liLucidaConsole %>" />
                    <asp:listitem value="Lucida Sans Unicode" Text="<%$ Resources:liLucidaSansUnicode %>" />
                    <asp:listitem value="Marlett" Text="<%$ Resources:liMarlett %>" />
                    <asp:listitem value="Microsoft Sans Serif" Text="<%$ Resources:liMicrosoftSansSerif %>" />
                    <asp:listitem value="Microsoft YaHei" Text="<%$ Resources:liMicrosoftYaHei %>" />
                    <asp:listitem value="Modern" Text="<%$ Resources:liModern %>"/>
                    <asp:listitem value="Covix MICR Unicode" Text="<%$ Resources:liCovixMICRUnicode %>" />
                    <asp:listitem value="Monotype Corsiva" Text="<%$ Resources:liMonotypeCorsiva %>" />
                    <asp:listitem value="MS Outlook" Text="<%$ Resources:liMSOutlook %>" />
                    <asp:listitem value="Palatino Linotype" Text="<%$ Resources:liPalatinoLinotype %>" />
                    <asp:listitem value="Roman Script" Text="<%$ Resources:liRomanScript %>" />
                    <asp:listitem value="Symbol" Text="<%$ Resources:liSymbol %>" />
                    <asp:listitem value="Tahoma" Text="<%$ Resources:liTahoma %>" />
                    <asp:listitem value="Times New Roman" Text="<%$ Resources:liTimesNewRoman %>" />
                    <asp:listitem value="Times New Roman CE" Text="<%$ Resources:liTimesNewRomanCE %>" />
                    <asp:listitem value="Times New Roman CYR" Text="<%$ Resources:liTimesNewRomanCYR %>" />
                    <asp:listitem value="Times New Roman Greek" Text="<%$ Resources:liTimesNewRomanGreek %>" />
                    <asp:listitem value="Times New Roman TUR" Text="<%$ Resources:liTimesNewRomanTUR %>" />
                    <asp:listitem value="Trebuchet MS" text="<%$ Resources:liTrebuchetMS %>" />
                    <asp:listitem value="Verdana" Text="<%$ Resources:liVerdana %>" />
                    <asp:listitem value="VisualUI" Text="<%$ Resources:liVisualUI %>" />
                    <asp:listitem value="Webdings" Text="<%$ Resources:liWebdings %>" />
                    <asp:listitem value="Windings" Text="<%$ Resources:liWindings %>" />
                    <asp:listitem value="Windings 2" Text="<%$ Resources:liWindings2 %>" />
                    <asp:listitem value="Windings 3" Text="<%$ Resources:liWindings3 %>" />
                </asp:dropdownlist>
                    </td>
            </tr>

            <tr>
                <td>
                    <asp:label runat="server" class="label" id="lbl_FontSize" text="<%$ Resources:lblFontSizeID %>" />
                </td>
                <td width="205px">
                    <asp:dropdownlist runat="server" id="FontSize" tabindex="1" rmxref="/Instance/Document/CheckStock/FontSize"
                        rmxtype="combobox" width="205px" onchange="setDataChanged(true);"><asp:listitem value="0" Text="" />
                    <asp:listitem value="6" Text="<%$ Resources:liFontSize6 %>" />
                    <asp:listitem value="8" Text="<%$ Resources:liFontSize8 %>" />
                    <asp:listitem value="10" Text="<%$ Resources:liFontSize10 %>" />
                    <asp:listitem value="12" Text="<%$ Resources:liFontSize12 %>" />
                    <asp:listitem value="14" Text="<%$ Resources:liFontSize14 %>" />
                    <asp:listitem value="16" Text="<%$ Resources:liFontSize16 %>" />
                    <asp:listitem value="18" Text="<%$ Resources:liFontSize18 %>" />
                    <asp:listitem value="20" Text="<%$ Resources:liFontSize20 %>" />
                    <asp:listitem value="22" Text="<%$ Resources:liFontSize22 %>" />
                    <asp:listitem value="24" Text="<%$ Resources:liFontSize24 %>" />
                    </asp:dropdownlist>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="label" id="lbl_MICREnabled" text="<%$ Resources:lblMICREnabledID %>" />
                    &nbsp;&nbsp;
                </td>
                <td width="205px">
                    <asp:checkbox runat="server" onchange="setDataChanged(true);" id="MICREnabled"
                        rmxref="/Instance/Document/CheckStock/MICRFlag" rmxtype="checkbox" onclick="MICREnabledsettings();"
                        tabindex="1" />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <%--<asp:textbox style="display: none" runat="server" id="MICREnabled_mirror" rmxref="/Instance/Document/void"
                 rmxtype="hidden" />--%>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:label runat="server" id="MICRSettings" class="required" rmxref="/Instance/Document/Data/value"
                        rmxtype="labelonly" text="<%$ Resources:lblMICRSettingsID %>" />
                </td>
                <td></td>
            </tr>

            <tr>
                <td>
                    <asp:label runat="server" class="label" id="lbl_MICRNumber" text="<%$ Resources:lblMICRNumberID %>" />
                </td>
                <td>
                    <asp:textbox runat="server" onchange="setDataChanged(true);" id="MICRNumber"
                        rmxref="/Instance/Document/CheckStock/MICRSequence" rmxtype="text" tabindex="1" />
                </td>
            </tr>
            <!--Start rsushilaggar MITS 25309 Date 10/07/2011-->
            <%--<tr>
        <td>     
        <asp:label runat="server" id="MICRType" rmxref="/Instance/Document/Data/value" rmxtype="labelonly"
                 text="MICR Type:" />                                                    
        </td>                    
        <td></td>
        </tr>                    

        
        <tr>
        <td colspan="2">
        <Label style='width :10%' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        <asp:radiobutton groupname="micrsettings" runat="server" id="RxLaser" rmxref="/Instance/Document/CheckStock/MICRType"
                 checked="true" rmxtype="radio" onclick="settingsMICRType();" text="RxLaser(hardware cartridge or DIMM)" value="0" />        
        </td>        
        </tr>
                  
        
        <tr>
        <td colspan="2">
              <Label style='width :10%' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       
        <asp:radiobutton groupname="micrsettings" runat="server" id="TrueTypeFont" rmxref="/Instance/Document/CheckStock/MICRType"
                  rmxtype="radio" onclick="settingsMICRType();" text="TrueType Font(software generated)" value="1" />        
        </td>        
        </tr> --%>
            <!-- End rsushilaggar MITS 25309 Date 10/07/2011 -->
            <tr id="">
                <td colspan="12">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="10%"></td>
                            <asp:label id="lblMICRFontSizeID" runat="server" text="<%$ Resources:lblMICRFontSizeID %>"></asp:label>
                            <td colspan="12">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="10%"></td>
                                        <td width="220px">
                                            <asp:label id="lblMICRFontNameID" runat="server" text="<%$ Resources:lblMICRFontNameID %>"></asp:label>
                                            &nbsp;&nbsp;</td>
                                        <td width="205px">
                                            <asp:dropdownlist runat="server" id="MICRFontName" tabindex="1" rmxref="/Instance/Document/CheckStock/MICRFont" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">                                  
                        <asp:ListItem Value="0" Text="" />
                        <asp:listitem value="Arial" Text="<%$ Resources:liArial %>" />
                        <asp:listitem value="Arial Black" Text="<%$ Resources:liArialBlack %>" />
                        <asp:listitem value="Arial CE" Text="<%$ Resources:liArialCE %>" />
                        <asp:listitem value="Arial Greek" Text="<%$ Resources:liArialGreek %>" />
                        <asp:listitem value="Arial CYR" Text="<%$ Resources:liArialCYR %>"/>
                        <asp:listitem value="Arial Narrow" Text="<%$ Resources:liArialNarrow %>" />
                        <asp:listitem value="Arial TUR" Text="<%$ Resources:liArialTUR %>" />                
                        <asp:listitem value="Book Antiqua" Text="<%$ Resources:liBookAntiqua %>" />
                        <asp:listitem value="Bookman Old Style" Text="<%$ Resources:liBookmanOldStyle %>" />
                        <asp:listitem value="Century Gothic" Text="<%$ Resources:liCenturyGothic %>" />
                        <asp:listitem value="Comic Sans MS" Text="<%$ Resources:liComicSansMS %>" />
                        <asp:listitem value="Courier New" Text="<%$ Resources:liCourierNew %>" />
                        <asp:listitem value="Courier New CE" Text="<%$ Resources:liCourierNewCE %>" />
                        <asp:listitem value="Courier New CYR" Text="<%$ Resources:liCourierNewCYR %>" />
                        <asp:listitem value="Courier New Greek" Text="<%$ Resources:liCourierNewGreek %>" />
                        <asp:listitem value="Courier New TUR" Text="<%$ Resources:liCourierNewTUR %>" />
                        <asp:listitem value="Garamond" Text="<%$ Resources:liGaramond %>" />
                        <asp:listitem value="Georgia" Text="<%$ Resources:liGeorgia %>" />
                        <asp:listitem value="Impact" Text="<%$ Resources:liImpact %>" />
                        <asp:listitem value="Lucida Console" Text="<%$ Resources:liLucidaConsole %>" />
                        <asp:listitem value="Lucida Sans Unicode" Text="<%$ Resources:liLucidaSansUnicode %>" />
                        <asp:listitem value="Marlett" Text="<%$ Resources:liMarlett %>" />
                        <asp:listitem value="Microsoft Sans Serif" Text="<%$ Resources:liMicrosoftSansSerif %>" />
                        <asp:listitem value="Modern" Text="<%$ Resources:liModern %>"/>
                        <asp:listitem value="Covix MICR Unicode" Text="<%$ Resources:liCovixMICRUnicode %>" />                                                        
                        <asp:ListItem Value="IDAutomationMICR" Text="<%$ Resources:liIDAutomationMICR %>" />                                                          
                        <asp:ListItem Value="IDAutomationMICR B" Text="<%$ Resources:liIDAutomationMICRB %>" />                                                          
                        <asp:ListItem Value="IDAutomationMICR XB" Text="<%$ Resources:liIDAutomationMICRXB %>" />   
                        <asp:ListItem Value="IDAutomationMICR_E13B" Text="<%$ Resources:IDAutomationMICRE13B %>" />                                                                  
                        <asp:listitem value="Monotype Corsiva"  Text="<%$ Resources:liMonotypeCorsiva %>" />
                        <asp:listitem value="MS Outlook"   Text="<%$ Resources:liMSOutlook %>" />
                        <asp:listitem value="Palatino Linotype" Text="<%$ Resources:liPalatinoLinotype %>" />
                        <asp:listitem value="Roman Script" Text="<%$ Resources:liRomanScript %>" />                                                         
                        <asp:listitem value="Symbol" Text="<%$ Resources:liSymbol %>" />
                        <asp:listitem value="Tahoma" Text="<%$ Resources:liTahoma %>" />
                        <asp:listitem value="Times New Roman" Text="<%$ Resources:liTimesNewRoman %>" />
                        <asp:listitem value="Times New Roman CE" Text="<%$ Resources:liTimesNewRomanCE %>" />
                        <asp:listitem value="Times New Roman CYR" Text="<%$ Resources:liTimesNewRomanCYR %>" />
                        <asp:listitem value="Times New Roman Greek" Text="<%$ Resources:liTimesNewRomanGreek %>" />
                        <asp:listitem value="Times New Roman TUR" Text="<%$ Resources:liTimesNewRomanTUR %>" />                                                          
                        <asp:listitem value="Trebuchet MS" Text="<%$ Resources:liTrebuchetMS %>" />
                        <asp:listitem value="Verdana" Text="<%$ Resources:liVerdana %>" />
                        <asp:listitem value="VisualUI" Text="<%$ Resources:liVisualUI %>" />
                        <asp:listitem value="Webdings" Text="<%$ Resources:liWebdings %>" />
                        <asp:listitem value="Windings" Text="<%$ Resources:liWindings %>" />
                        <asp:listitem value="Windings 2" Text="<%$ Resources:liWindings2 %>" />
                        <asp:listitem value="Windings 3" Text="<%$ Resources:liWindings3 %>" />                                                                                
                        </asp:dropdownlist>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td width="180px">
                                            <asp:label id="lblMICRFontSizeID1" runat="server" text="<%$ Resources:lblMICRFontSizeID %>"></asp:label>
                                            &nbsp;&nbsp;</td>
                                        <td>
                                            <asp:textbox runat="server" rmxref="/Instance/Document/CheckStock/MICRFontSize" size="20"
                                                onblur="numLostFocus(this);" id="MICRFontSize" tabindex="1" onchange="setDataChanged(true);"
                                                rmxtype="text"
                                                value="0" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>


        <table border="0" cellspacing="0" cellpadding="0" style="display: none;"
            name="FORMTABabc" id="FORMTABabc">
            <tr id="Tr1">
                <td colspan="2">

                    <asp:radiobutton rmxtype="radio" groupname="checkimage" runat="server" rmxref="/Instance/Document/radiotwo"
                        value="0" onclick="javascript:toggle_CreateImage3();"
                        tabindex="1" id="NoneorPrePrintedCheckStock" text="<%$ Resources:rdbNoneorPrePrintedCheckStockID %>" />
                </td>
            </tr>
            <tr id="Tr2">

                <td>
                    <br />
                </td>

            </tr>

            <tr id="Tr3">
                <td colspan="2">
                    <asp:radiobutton rmxtype="radio" groupname="checkimage" runat="server" rmxref="/Instance/Document/radiotwo"
                        value="0" onclick="javascript:toggle_CreateImage1();"
                        tabindex="1" id="RxLaserhardwarecartridgeorDIMM" text="<%$ Resources:rdbRxLaserhardwarecartridgeorDIMMID %>" />
                    <label style="width: 30%"></label>
                    <label style="width: 30%"></label>
                </td>
            </tr>

            <tr id="Tr4">
                <td colspan="2">
                    <asp:textbox style="display: none" runat="server" id="hdnRXLaserFlag"
                        rmxref="/Instance/Document/CheckStock/RXLaserFlag" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="hdnSoftForm"
                        rmxref="/Instance/Document/CheckStock/SoftForm" rmxtype="hidden" />
                </td>
            </tr>


            <tr id="Tr5">
                <td colspan="12">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="10%"></td>

                            <td>
                                <asp:label id="lblRegularCheckID" runat="server" text="<%$ Resources:lblRegularCheckID %>"></asp:label>
                                &nbsp;&nbsp;</td>
                            <td>

                                <asp:textbox runat="server" rmxref="/Instance/Document/CheckStock/RXLaserForm" size="5"
                                    onblur="numLostFocus(this);" id="RegularCheck" tabindex="1" onchange="setDataChanged(true);"
                                    rmxtype="text"
                                    value="0" />

                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td width="4%">
                                <label style="width: 4%"></label>
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td>
                                <asp:label id="lblNonnegotiableorVoidID" runat="server" text="<%$ Resources:lblNonnegotiableorVoidID %>"></asp:label>
                                &nbsp;&nbsp;</td>
                            <td>

                                <asp:textbox runat="server" rmxref="/Instance/Document/CheckStock/RXVoidForm" size="5"
                                    onblur="numLostFocus(this);" id="NonnegotiableorVoid" tabindex="2" onchange="setDataChanged(true);"
                                    rmxtype="text"
                                    value="0" />


                            </td>
                        </tr>
                    </table>
                </td>
            </tr>



            <tr id="Tr6">
                <td colspan="12">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="10%"></td>
                            <td></td>
                            <td>
                                <asp:checkbox runat="server" id="UseSpecialChecka"
                                    rmxref="/Instance/Document/CheckStock/SpecialFlag" rmxtype="checkbox" tabindex="1"
                                    onchange="SetSaveFlag();" onclick="toggle_BasicOptions();" appearance="full" />
                                <asp:label id="lblUseSpecialCheckID" runat="server" text="<%$ Resources:lblUseSpecialCheckID %>"></asp:label>
                                <%-- <input type="text" name="$node^75" value="" style="display:none" 
                    cancelledvalue="" id="UseSpecialChecka_mirror">                    
                                --%>                    
                    
                    
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td width="100px">
                                <asp:label id="lblSpecialCheckID" runat="server" text="<%$ Resources:lblSpecialCheckID %>"></asp:label>
                                &nbsp;&nbsp;</td>

                            <td>
                                <asp:textbox runat="server" rmxref="/Instance/Document/CheckStock/RXSpecialForm" size="5"
                                    onblur="numLostFocus(this);" id="SpecialCheck" tabindex="2" onchange="setDataChanged(true);"
                                    value="0" />
                            </td>


                            <td width="180px">
                                <asp:label id="lblActivateWhenAmountID" runat="server" text="<%$ Resources:lblActivateWhenAmountID %>"></asp:label>
                                &nbsp;&nbsp;</td>
                            <td>
                                <asp:textbox runat="server" rmxref="/Instance/Document/CheckStock/SpecialAmount" size="5"
                                    onblur="numLostFocus(this);" id="ActivatewhenAmounta" tabindex="2" onchange="setDataChanged(true);"
                                    value="0" />
                            </td>


                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            </tr>
           <tr id="Tr8">
               <td colspan="2">
                   <asp:radiobutton runat="server" groupname="checkimage" id="WindowsMetafilesoftwaregenerated"
                       rmxref="/Instance/Document/CheckStock/SpecialFlag" rmxtype="checkbox" tabindex="1" value="2"
                       onclick="javascript:toggle_CreateImage2();" appearance="full" />
                   <asp:label id="lblWindowsMetafileID" runat="server" text="<%$ Resources:lblWindowsMetafileID %>"></asp:label>
               </td>
           </tr>

            <tr>
                <td colspan="12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label id="lblVisioID" runat="server" text="<%$ Resources:lblVisioID %>"></asp:label><asp:checkbox runat="server" id="NewVisioVersion"
                    rmxref="/Instance/Document/CheckStock/NewVisioVersion" tabindex="1"
                    onchange="SetSaveFlag();" rmxtype="checkbox" />
                </td>
            </tr>
            <tr id="Tr9">
                <td colspan="12">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="10%"></td>
                            <td>
                                <asp:label id="lblRegChkID" runat="server" text="<%$ Resources:lblRegularCheckID %>"></asp:label>
                                &nbsp;&nbsp;</td>
                            <td></td>
                            <td>&nbsp;&nbsp;</td>
                            <td width="3.5%">
                                <label style="width: 3.5%"></label>
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td colspan="2"><font color="red">
                                <asp:label id="lblNoImageSetID" runat="server" text="<%$ Resources:lblNoImageSetID %>"></asp:label>
                            </font></td>
                            <td width="3%">
                                <label style="width: 3%"></label>
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td colspan="2">
                                <script language="JavaScript" src="">{var i;}</script>

                                <asp:button runat="server" class="button" id="SetImageOne" style="width: 100" onclientclick="return BrowserWindowone();" text="<%$ Resources:btnSetImageID %>" />
                            </td>

                            <td width="1%">
                                <label style="width: 1%"></label>
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td colspan="2">
                                <script language="JavaScript" src="">{var i;}</script>
                                <asp:button runat="server" class="button" id="View_Image1" style="width: 100" onclientclick="return WindowOpenViewImage1();" text="<%$ Resources:btnViewImageID %>" />
                            </td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td>
                                <asp:label id="lblNonnegorVoidID" runat="server" text="<%$ Resources:lblNonnegotiableorVoidID %>"></asp:label>
                                &nbsp;&nbsp;</td>
                            <td></td>
                            <td>&nbsp;&nbsp;</td>
                            <td width="3.5%">
                                <label style="width: 3.5%"></label>
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td colspan="2"><font color="red">
                                <asp:label id="lblNoImgSetID" runat="server" text="<%$ Resources:lblNoImageSetID %>"></asp:label>
                            </font></td>
                            <td width="3%">
                                <label style="width: 3%"></label>
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td colspan="2">
                                <script language="JavaScript" src="">{var i;}</script>
                                <asp:button runat="server" class="button" id="SetImageTwo" style="width: 100" onclientclick="return BrowserWindowtwo();" text="<%$ Resources:btnSetImageID %>" />
                                <td width="1%">
                                    <label style="width: 1%"></label>
                                </td>
                                <td>&nbsp;&nbsp;</td>
                                <td colspan="2">
                                    <script language="JavaScript" src="">{var i;}</script>
                                    <asp:button runat="server" class="button" id="View_Image2" style="width: 100" onclientclick="return WindowOpenViewImage2();" text="<%$ Resources:btnViewImageID %>" />
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Tr10">
                <td colspan="12">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="10%"></td>
                            <td>
                                <asp:checkbox runat="server" id="UseSpecialCheckb"
                                    rmxref="/Instance/Document/CheckStock/SpecialFlag" rmxtype="checkbox" tabindex="1"
                                    onchange="SetSaveFlag();" onclick="toggle_BasicOptions();" appearance="full" />
                                <asp:label id="lblUseSplChkID" runat="server" text="<%$ Resources:lblUseSplChkID %>"></asp:label>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Tr11">
                <td colspan="12">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="10%"></td>
                            <td colspan="12">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="10%"></td>
                                        <td>
                                            <asp:label id="lblUseSplChckID" runat="server" text="<%$ Resources:lblUseSplChkID %>"></asp:label>
                                            &nbsp;&nbsp;</td>
                                        <td></td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td width="4.1%">
                                            <label style="width: 4.1%"></label>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td colspan="2"><font color="red">
                                            <asp:label id="lblNoImgeSetID" runat="server" text="<%$ Resources:lblNoImageSetID %>"></asp:label>
                                        </font></td>
                                        <td width="3%">
                                            <label style="width: 3%"></label>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td colspan="2">
                                            <script language="JavaScript" src="">{var i;}</script>

                                            <asp:button runat="server" id="SetImageThree" class="button" style="width: 100" onclientclick="return BrowserWindowthree();" text="<%$ Resources:btnSetImageID %>" />


                                            <td width="1%">
                                                <label style="width: 1%"></label>
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td colspan="2">
                                                <script language="JavaScript" src="">{var i;}</script>

                                                <asp:button runat="server" id="View_Image3" class="button" style="width: 100" onclientclick="return WindowOpenViewImage3();" text="<%$ Resources:btnViewImageID %>" />
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Tr12">
                <td colspan="12">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="10%"></td>
                            <td width="7%">
                                <label style="width: 7%"></label>
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td>
                                <asp:label id="lblActvWhenAmountID" runat="server" text="<%$ Resources:lblActivateWhenAmountID %>"></asp:label>
                                &nbsp;&nbsp;</td>
                            <td></td>
                            <td>&nbsp;&nbsp;</td>
                            <td width="7%">
                                <label style="width: 7%"></label>
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td></td>

                            <td>
                                <asp:textbox runat="server" rmxref="/Instance/Document/CheckStock/SpecialAmount" size="30" text="0"
                                    onblur="numLostFocus(this);" id="ActivatewhenAmountb" tabindex="3" onchange="setDataChanged(true);"
                                    value="0" />
                            </td>

                        </tr>
                    </table>
                </td>
            </tr>
        </table>


        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABxyz" id="FORMTABxyz" style="display: none">
            <tr>
                <td colspan="7" class="msgheader">
                    <asp:label id="lblformtitlecheckID" runat="server" text="<%$ Resources:lblformtitlecheckID %>" />
                </td>
            </tr>
            <tr id="Tr13">
                <td align="right">
                    <asp:label id="lblDateXID" runat="server" text="<%$ Resources:lblDateXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="DateXt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="1" onchange="toggle_al();" size="20" />
                </td>
                <td>&nbsp;&nbsp;</td>

                <td align="right">
                    <asp:label id="lblPayerXID" runat="server" text="<%$ Resources:lblPayerXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="PayerXt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="1" onchange="toggle_al();" size="20" />
                </td>
                <%--<td align="right">
                    <asp:label id="lblTransDetailXID" runat="server" text="<%$ Resources:lblTransDetailXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="TransDetailXt" size="30"
                        rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>--%>
            </tr>
            <tr id="Tr14">
                <td>
                    <asp:textbox style="display: none" runat="server" id="DateXa" rmxref="/Instance/Document/CheckStock/DateX"
                        rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="DateYa" rmxref="/Instance/Document/CheckStock/DateY"
                        rmxtype="hidden" />
                </td>
            </tr>
            <tr id="Tr15">
                <td align="right">
                    <asp:label id="lblYID1" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>

                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="DateYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" size="20" onchange="toggle_al();" />

                </td>
                <td></td>
                <td align="right">
                    <asp:label id="lblYID4" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="PayerYt" rmxref="/Instance/Document/value"
                        size="20" rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
                <%-- <td>&nbsp;&nbsp;</td>
                <td align="right">
                    <asp:label id="lblYID2" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="TransDetailYt" rmxref="/Instance/Document/value"
                        size="30" rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>--%>
                <td></td>

            </tr>

            <tr id="Tr16">

                <td>
                    <asp:textbox style="display: none" runat="server" id="CheckNumberXa" rmxref="/Instance/Document/CheckStock/CheckNumberX"
                        rmxtype="hidden" />

                    <asp:textbox style="display: none" runat="server" id="CheckNumberYa" rmxref="/Instance/Document/CheckStock/CheckNumberY"
                        rmxtype="hidden" />
                </td>
                <td></td>
            </tr>
            <tr id="Tr17">
                <td align="right">
                    <asp:label id="lblCheckNumbernewXID" runat="server" text="<%$ Resources:lblCheckNumbernewXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="CheckNumberXt" size="20"
                        rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
                <%--<td>&nbsp;&nbsp;</td>--%>


                <td>&nbsp;&nbsp;</td>
                <td align="right">
                    <asp:label id="lblPayeeXID" runat="server" text="<%$ Resources:lblPayeeXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="PayeeXt" rmxref="/Instance/Document/value"
                        size="20" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>

                <%--                <td align="right">
                    <asp:label id="lblPayerXID" runat="server" text="<%$ Resources:lblPayerXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="PayerXt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="1" onchange="toggle_al();" size="30" />
                    </td>--%>
                <td></td>
                <td></td>
            </tr>
            <tr id="Tr18">
                <td>
                    <asp:textbox style="display: none" runat="server" id="AmountXa" rmxref="/Instance/Document/CheckStock/CheckAmountX"
                        size="20" rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="AmountYa" rmxref="/Instance/Document/CheckStock/CheckAmountY"
                        size="20" rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="EventDateXa" rmxref="/Instance/Document/CheckStock/EventDateX"
                        size="20" rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="EventDateYa" rmxref="/Instance/Document/CheckStock/EventDateY"
                        size="20" rmxtype="hidden" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PayeeAddressXa" rmxref="/Instance/Document/CheckStock/PayeeAddressX"
                        size="20" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="AgentAddressXa" rmxref="/Instance/Document/CheckStock/AgentAddressX"
                        size="20" rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PayeeAddressYa" rmxref="/Instance/Document/CheckStock/PayeeAddressY"
                        size="20" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="AgentAddressYa" rmxref="/Instance/Document/CheckStock/AgentAddressY"
                        size="20" rmxtype="hidden" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PayeeAddOnCheckXa" rmxref="/Instance/Document/CheckStock/PayeeAddOnCheckX"
                        size="20" rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PayeeAddOnCheckYa" rmxref="/Instance/Document/CheckStock/PayeeAddOnCheckY"
                        size="20" rmxtype="hidden" />
                </td>
            </tr>

            <tr id="Tr19">
                <td align="right">
                    <asp:label id="lblYID3" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="CheckNumberYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" size="20" />
                </td>
                <td>&nbsp;&nbsp;</td>
                <%--<td></td>
                <td></td>--%>
                <td align="right">
                    <asp:label id="lblYID6" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="PayeeYt" rmxref="/Instance/Document/value"
                        size="20" rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
            </tr>

            <tr id="Tr20">
                <td>
                    <asp:textbox style="display: none" runat="server" id="AmountTextXa" rmxref="/Instance/Document/CheckStock/CheckAmountTextX"
                        size="20" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="AmountTextYa" rmxref="/Instance/Document/CheckStock/CheckAmountTextY"
                        size="20" rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="ClaimDateXa" rmxref="/Instance/Document/CheckStock/ClaimDateX"
                        size="20" rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="ClaimDateYa" rmxref="/Instance/Document/CheckStock/ClaimDateY"
                        size="20" rmxtype="hidden" />
                </td>
            </tr>
            <tr id="Tr21">
                <td align="right">
                    <asp:label id="lblAmountXID" runat="server" text="<%$ Resources:lblAmountXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="AmountXt"
                        size="20" rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
                <%-- <td></td>
                <td></td>--%>
                <td>&nbsp;&nbsp;</td>
                <td align="right">
                    <asp:label id="lblEventDateXID" runat="server" text="<%$ Resources:lblEventDateXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="EventDateXt" rmxref="/Instance/Document/value"
                        size="20" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
            </tr>
            <tr id="Tr22">
                <td align="right">
                    <asp:label id="lblYID5" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="AmountYt" rmxref="/Instance/Document/value"
                        size="20" rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
                <td>&nbsp;&nbsp;</td>
                <%--<td></td>--%>
                <td align="right">
                    <asp:label id="Label13" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="EventDateYt" rmxref="/Instance/Document/value"
                        size="20" rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>

                <%-- <td align="right">
                    <asp:label id="lblYID6" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="PayeeYt" rmxref="/Instance/Document/value"
                        size="30" rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>--%>
            </tr>

            <tr id="Tr23">
                <td align="right">
                    <asp:label id="lblAmountTextXID" runat="server" text="<%$ Resources:lblAmountTextXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="AmountTextXt" size="20"
                        rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />

                </td>
                <td>&nbsp;&nbsp;</td>
                <td align="right">
                    <asp:label id="lblClaimDateXID" runat="server" text="<%$ Resources:lblClaimDateXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="ClaimDateXt"
                        size="20" rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
            </tr>
            <tr id="Tr24">
                <td align="right">
                    <asp:label id="lblYID7" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox size="20"
                        runat="server" onblur="numLostFocus(this);" id="AmountTextYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
                <td>&nbsp;&nbsp;</td>

                <td align="right">
                    <asp:label id="lblYID14" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>

                    <asp:textbox size="20"
                        runat="server" onblur="numLostFocus(this);" id="ClaimDateYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
            </tr>

            <tr id="Tr25">
                <td align="right">
                    <asp:label id="lblClaimNumberXID" runat="server" text="<%$ Resources:lblClaimNumberXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="ClaimNumberXt" size="20"
                        rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
                <td>&nbsp;&nbsp;</td>

                <td align="right">
                    <asp:label id="lblPolicyNumberXID" runat="server" text="<%$ Resources:lblPolicyNumberXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="PolicyNumberXt" size="20"
                        rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
            </tr>
            <tr id="Tr26">
                <td align="right">
                    <asp:label id="lblYID9" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox size="20"
                        runat="server" onblur="numLostFocus(this);" id="ClaimNumberYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
                <td>&nbsp;&nbsp;</td>
                <td align="right">
                    <asp:label id="lblYID15" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <!--//MITS 31003 - Rakhel ML changes !-->
                <td>
                    <asp:textbox size="20"
                        runat="server" onblur="numLostFocus(this);" id="PolicyNumberYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
            </tr>
            <tr id="Tr27">
                <td align="right">
                    <asp:label id="lblMemoTextXID" runat="server" text="<%$ Resources:lblMemoTextXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="MemoTextXt" size="20"
                        rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
                <td>&nbsp;&nbsp;</td>
                <td align="right">
                    <asp:label id="lblInsuredNameXID" runat="server" text="<%$ Resources:lblInsuredNameXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="InsuredNameXt" size="20"
                        rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
            </tr>

            <tr id="Tr28">
                <td align="right">
                    <asp:label id="lblYID10" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox size="20"
                        runat="server" onblur="numLostFocus(this);" id="MemoTextYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
                <td>&nbsp;&nbsp;</td>
                <td align="right">
                    <asp:label id="lblYID16" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="InsuredNameYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" size="20" />
                </td>
            </tr>
            <tr id="Tr7">
        <td align="right">
                    <asp:label id="lblPayeeOnCheckXID" runat="server" text="<%$ Resources:lblPayeeOnCheckXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="PayeeAddOnCheckXt"
                        size="20" rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:label id="Label15" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>

                    <asp:textbox size="20"
                        runat="server" onblur="numLostFocus(this);" id="PayeeAddOnCheckYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
            </tr>
            <tr id="Tr29">
                <td>
                    <asp:textbox style="display: none" runat="server" id="TransDetailXa" rmxref="/Instance/Document/CheckStock/CheckDtlX"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="TransDetailYa" rmxref="/Instance/Document/CheckStock/CheckDtlY"
                        rmxtype="hidden" />
                </td>
            </tr>
            <tr id="">
                <td>
                    <asp:textbox style="display: none" runat="server" id="PayerXa" rmxref="/Instance/Document/CheckStock/CheckPayerX"
                        rmxtype="hidden" />

                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PayerYa" rmxref="/Instance/Document/CheckStock/CheckPayerY"
                        rmxtype="hidden" />

                </td>
            </tr>
            <tr id="">
                <td>
                    <asp:textbox style="display: none" runat="server" id="ClaimNumberXa" rmxref="/Instance/Document/CheckStock/ClaimNumberOffsetX"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PolicyNumberXa" rmxref="/Instance/Document/CheckStock/PolicyNumberX"
                        rmxtype="hidden" />
                </td>
            </tr>
            <tr id="">
                <td>
                    <asp:textbox style="display: none" runat="server" id="PayeeXa" rmxref="/Instance/Document/CheckStock/CheckPayeeX"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PayeeYa" rmxref="/Instance/Document/CheckStock/CheckPayeeY"
                        rmxtype="hidden" />
                </td>
            </tr>
            <tr id="">
                <td>
                    <asp:textbox style="display: none" runat="server" id="ClaimNumberYa" rmxref="/Instance/Document/CheckStock/ClaimNumberOffsetY"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PolicyNumberYa" rmxref="/Instance/Document/CheckStock/PolicyNumberY"
                        rmxtype="hidden" />
                </td>
            </tr>
            <tr id="">
                <td>
                    <asp:textbox style="display: none" runat="server" id="CheckOffsetXa" rmxref="/Instance/Document/CheckStock/CheckOffsetX"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="CheckOffsetYa" rmxref="/Instance/Document/CheckStock/CheckOffsetY"
                        rmxtype="hidden" />
                </td>
            </tr>
            <tr id="">
                <td>
                    <asp:textbox style="display: none" runat="server" id="MemoTextXa" rmxref="/Instance/Document/CheckStock/MemoX"
                        rmxtype="hidden" />
                </td>
                 <td>
                    <asp:textbox style="display: none" runat="server" id="InsuredNameXa" rmxref="/Instance/Document/CheckStock/InsuredNameX"
                        rmxtype="hidden" />
                </td>
            </tr>
            <tr id="">
                <td>
                    <asp:textbox style="display: none" runat="server" id="MICROffsetXa" rmxref="/Instance/Document/CheckStock/CheckMICRX"
                        rmxtype="hidden" />

                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="MICROffsetYa" rmxref="/Instance/Document/CheckStock/CheckMICRY"
                        rmxtype="hidden" />
                </td>
            </tr>
            <tr id="">
                <td>
                    <asp:textbox style="display: none" runat="server" id="MemoTextYa" rmxref="/Instance/Document/CheckStock/MemoY"
                        rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="InsuredNameYa" rmxref="/Instance/Document/CheckStock/InsuredNameY"
                        rmxtype="hidden" />
                </td>
            </tr>
            <tr id="">
                <td>
                    <asp:textbox style="display: none" runat="server" id="tndOffsetXa" rmxref="/Instance/Document/CheckStock/SecondOffsetX"
                        rmxtype="hidden" />

                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="tndOffsetYa" rmxref="/Instance/Document/CheckStock/SecondOffsetY"
                        rmxtype="hidden" />

                </td>
            </tr>
            <tr>
                <td colspan="7" class="msgheader">
                    <asp:label id="lblformtitlecheckstubID" runat="server" text="<%$ Resources:lblformtitlecheckstubID %>" />
                </td>
            </tr>

            <tr id="">
                <%--<td align="right">
                    <asp:label id="lblUnitsID" runat="server" text="<%$ Resources:lblUnitsID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <div title="" style="padding: 0px; margin: 0px">
                        <asp:textbox
                            runat="server" onchange="setDataChanged(true);" id="Units" rmxref="/Instance/Document/CheckStock/Unit"
                            rmxtype="text" tabindex="1" size="30"
                            readonly="true" style="background-color: #F2F2F2;" />
                    </div>
                </td>--%>

                <td align="right" nowrap="true">
                    <asp:label id="lblPayeeAddressXID" runat="server" text="<%$ Resources:lblPayeeAddressXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="PayeeAddressXt"
                        size="20" rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
                <td>&nbsp;&nbsp;</td>
                <td align="right" nowrap="true">
                    <asp:label id="lblAgentAddressXID" runat="server" text="<%$ Resources:lblAgentAddressXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="AgentAddressXt"
                        size="20" rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:label id="Label14" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>

                    <asp:textbox size="20"
                        runat="server" onblur="numLostFocus(this);" id="PayeeAddressYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
                <td>&nbsp;&nbsp;</td>
                <td align="right">
                    <asp:label id="lblAgentAddressYID" runat="server" text="<%$ Resources:lblAgentAddressYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="AgentAddressYt"
                        size="20" rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="7" class="msgheader">
                    <asp:label id="lblformtitlecheckpagesetupID" runat="server" text="<%$ Resources:lblformtitlecheckpagesetupID %>" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:label id="lblCheckOffsetXID" runat="server" text="<%$ Resources:lblCheckOffsetXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="CheckOffsetXt"
                        size="20" rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
                <td></td>

                <td align="right" nowrap="true">
                    <asp:label id="lblTransDetailXID" runat="server" text="<%$ Resources:lblTransDetailXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="TransDetailXt" size="20"
                        rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
                <td align="right" nowrap="true">
                    <asp:label id="lblMICROffsetXID" runat="server" text="<%$ Resources:lblMICROffsetXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="MICROffsetXt" size="20"
                        rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
            </tr>
            <tr>

                <td align="right">
                    <asp:label id="lblYID8" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>

                    <asp:textbox size="20"
                        runat="server" onblur="numLostFocus(this);" id="CheckOffsetYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
                <td></td>
                <td align="right">
                    <asp:label id="Label10" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="TransDetailYt" rmxref="/Instance/Document/value"
                        size="20" rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
                <td align="right">
                    <asp:label id="lblYID12" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox size="20"
                        runat="server" onblur="numLostFocus(this);" id="MICROffsetYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" />
                </td>
            </tr>
            <tr>

                <td align="right">
                    <asp:label id="Label11" runat="server" text="<%$ Resources:lbl2ndOffsetXID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="tndOffsetXt" size="20"
                        rmxref="/Instance/Document/value" rmxtype="numeric" tabindex="1" onchange="toggle_al();" />
                </td>
                <td></td>
                <td align="right">
                    <asp:label id="lblUnitsID" runat="server" text="<%$ Resources:lblUnitsID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <div title="" style="padding: 0px; margin: 0px">
                        <asp:textbox
                            runat="server" onchange="setDataChanged(true);" id="Units" rmxref="/Instance/Document/CheckStock/Unit"
                            rmxtype="text" tabindex="1" size="20"
                            readonly="true" style="background-color: #F2F2F2;" />
                    </div>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:label id="lblYID11" runat="server" text="<%$ Resources:lblYID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:textbox
                        runat="server" onblur="numLostFocus(this);" id="tndOffsetYt" rmxref="/Instance/Document/value"
                        rmxtype="numeric" tabindex="2" onchange="toggle_al();" size="20" />
                </td>

            </tr>
            <td colspan="12">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="10%"></td>
                        <td>
                            <asp:textbox style="display: none" runat="server" id="hdnUnit" rmxref="/Instance/Document/CheckStock/Unit"
                                tabindex="1" rmxtype="hidden" />
                        </td>


                        <td>
                            <asp:button runat="server" text="<%$ Resources:btnRestoreDefaultsID %>" class="button" id="RestoreDefaults" style="width: 100" onclientclick="return RestoreDefault();;" />
                        </td>
                        <td>&nbsp;&nbsp;</td>
                        <td colspan="2">
                            <script language="JavaScript" src="">{var i;}</script>

                            <asp:button runat="server" text="<%$ Resources:btnRestoreID %>" class="button" id="btnRestore" style="width: 100" onclientclick="return CheckInTheMiddle();" />
                            <td colspan="2">
                                <script language="JavaScript" src="">{var i;}</script>

                                <asp:button runat="server" text="<%$ Resources:btnThreePartCheckID %>" class="button" id="btnThreePartCheck" style="width: 100" onclientclick="return CheckAtTheBottom();" />
                    </tr>
                    <tr></tr>
                </table>
            </td>
            </tr>
        </table>


        <table border="0" cellspacing="0" cellpadding="0" name="FORMTAB123" id="FORMTAB123" style="display: none;">
            <%--amar--%>
            <tr>
                <td colspan="5" class="msgheader">
                    <asp:label id="lblheaderfirst" runat="server" text="<%$ Resources:lblformtitlecheckID %>" />
                </td>
            </tr>



            <tr id="Tr31">
                <td>
                    <asp:textbox style="display: none" runat="server" id="SuppressSign_mirror" rmxref="/Instance/Document/void" rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PreNumberedCheckStock_mirror" rmxref="/Instance/Document/void" rmxtype="hidden" />
                </td>
            </tr>
            <%-- <tr>

                <td>
                    <asp:label id="Label6" runat="server" text="<%$ Resources:lblCheckDateFormat %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td width="205px">


                    <asp:dropdownlist runat="server" id="Dropdownlist2" tabindex="1" rmxref="/Instance/Document/CheckStock/CheckDateFormat" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">
                            <asp:ListItem Value="0" Text="" />
                            <asp:ListItem Value="1" Text="<%$ Resources:DMYYYY %>" />
                            <asp:ListItem Value="2" Text="<%$ Resources:DDMMYYYY %>" />
                            <asp:ListItem Value="3" Text="<%$ Resources:YYYYMD %>" />
                            <asp:ListItem Value="4" Text="<%$ Resources:YYYYMMDD %>" />
                          </asp:dropdownlist>
                </td>
                <td></td>
                <td>
                    <asp:label id="lblFormatDateOfCheckID" runat="server" text="<%$ Resources:lblFormatDateOfCheckID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" id="FormatDateOfCheck" rmxref="/Instance/Document/CheckStock/FormatDateOfCheck" rmxtype="checkbox" onclick="toggle_BasicOptions();" tabindex="1" />
                    <%--<input type="text" name="$node^112" value="" style="display:none" cancelledvalue="" id="FormatDateOfCheck_mirror">
                </td>
            </tr>--%>
            <tr id="Tr32">
                <td>
                    <asp:label id="lblPrintPayeeAddressID" runat="server" text="<%$ Resources:lblPrintPayeeAddressID %>"></asp:label>
                    &nbsp;&nbsp;
                </td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintPayeeAddressonCheck" rmxref="/Instance/Document/CheckStock/PrintAddress" rmxtype="checkbox" tabindex="2" />
                </td>
                <td></td>
                <%--<td>
                    <asp:label id="lblPrintClaimNoID" runat="server" text="<%$ Resources:lblPrintClaimNoID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintClaimNoonCheck" rmxref="/Instance/Document/CheckStock/PrintClaimNumberOnCheck" rmxtype="checkbox" tabindex="2" />
                </td>--%>
                <%--<td>
                   <asp:label id="lblPayerLevelID" runat="server" text="<%$ Resources:lblPayerLevelID %>"></asp:label>
                   </td>
                <td width="205px">

                    <asp:dropdownlist runat="server" id="PayerLevelForChecks" tabindex="1" rmxref="/Instance/Document/CheckStock/PayerLevel" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">
                            <asp:ListItem Value="0" Text="" />
                            <asp:ListItem Value="1" Text="<%$ Resources:liSysParmClient %>" />
                            <asp:ListItem Value="1005" Text="<%$ Resources:liClient %>" />
                            <asp:ListItem Value="1006" Text="<%$ Resources:liCompany %>" />
                            <asp:ListItem Value="1007" Text="<%$ Resources:liOperation %>" />
                            <asp:ListItem Value="1008" Text="<%$ Resources:liRegion %>" />
                            <asp:ListItem Value="1009" Text="<%$ Resources:liDivision %>" />
                            <asp:ListItem Value="1010" Text="<%$ Resources:liLocation %>" />
                            <asp:ListItem Value="1011" Text="<%$ Resources:liFacility %>" />
                            <asp:ListItem Value="1012" Text="<%$ Resources:liDepartment %>" />
                          </asp:dropdownlist>
                </td>--%>
                <td>
                    <asp:label id="lblPrintDateofClaimID" runat="server" text="<%$ Resources:lblPrintDateofClaimID %>"></asp:label>
                    &nbsp;&nbsp;
                </td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintDateofClaimonCheck" rmxref="/Instance/Document/CheckStock/PrintDateofClaim" rmxtype="checkbox" tabindex="2" />
                </td>

            </tr>
            <tr>
                <td>
                    <asp:label id="lblPrintFullPolicyNoID" runat="server" text="<%$ Resources:lblPrintFullPolicyNoID %>"></asp:label>
                    &nbsp;&nbsp;
                </td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintFullPolicyNoonCheck" rmxref="/Instance/Document/CheckStock/PrintFullPolicyNumber" rmxtype="checkbox" tabindex="2" />
                </td>
                <td></td>
                <td>
                    <asp:label id="lblPrintDateofEventID" runat="server" text="<%$ Resources:lblPrintDateofEventID %>"></asp:label>
                    &nbsp;&nbsp;
                </td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintDateofEventonCheck" rmxref="/Instance/Document/CheckStock/PrintDateofEvent" rmxtype="checkbox" tabindex="2" />
                </td>

            </tr>
            <tr>
                <td>
                    <asp:label id="lblPrintClaimNonewID" runat="server" text="<%$ Resources:lblPrintClaimNonewID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintClaimNoonCheck" rmxref="/Instance/Document/CheckStock/PrintClaimNumberOnCheck" rmxtype="checkbox" tabindex="2" />
                </td>
                <td></td>

                <td>
                    <asp:label id="lblPrintInsuredNameID" runat="server" text="<%$ Resources:lblPrintInsuredNameID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintInsuredNameonCheck" rmxref="/Instance/Document/CheckStock/PrintInsuredNameOnCheck" rmxtype="checkbox" tabindex="2" />
                </td>
            </tr>
            <tr id="Tr33">
                <td>
                    <asp:textbox style="display: none" runat="server" id="PrintClaiminsurerinsteadofPayer_mirror" rmxref="/Instance/Document/void" rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PrintPayeeAddressonCheck_mirror" rmxref="/Instance/Document/void" rmxtype="hidden" />
                </td>
            </tr>
            <%-- <tr id="Tr34">--%>
            <%-- <td>
                    <asp:label id="lblPrintPayeeTaxID" runat="server" text="<%$ Resources:lblPrintPayeeTaxID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintPayeeTaxIDoncheckStub" rmxref="/Instance/Document/CheckStock/PrintTaxIDOnStub" rmxtype="checkbox" tabindex="1" />

                </td>--%>

            <%-- <td>
                    <asp:label id="lblPrintControlNoID" runat="server" text="<%$ Resources:lblPrintControlNoID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintControlonCheckStub" rmxref="/Instance/Document/CheckStock/PrintCtlNumberOnStub" rmxtype="checkbox" tabindex="1" />
                </td>--%>
            <%-- </tr>--%>
            <tr id="Tr35">
                <td>
                    <asp:textbox style="display: none" runat="server" id="PrintMemoonCheck_mirror" rmxref="/Instance/Document/void" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="PrintClaimNoonCheck_mirror" rmxref="/Instance/Document/void" rmxtype="hidden" />

                </td>
            </tr>
            <%-- <tr>
                <td>
                    <br>
                </td>
            </tr>--%>
            <tr>

                <td>
                    <asp:label id="Label6" runat="server" text="<%$ Resources:lblCheckDateFormat %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td width="205px">


                    <asp:dropdownlist runat="server" id="CheckDateFormat" tabindex="1" rmxref="/Instance/Document/CheckStock/CheckDateFormat" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">
                            <asp:ListItem Value="0" Text="" />
                            <asp:ListItem Value="1" Text="<%$ Resources:DMYYYY %>" />
                            <asp:ListItem Value="2" Text="<%$ Resources:DDMMYYYY %>" />
                            <asp:ListItem Value="3" Text="<%$ Resources:YYYYMD %>" />
                            <asp:ListItem Value="4" Text="<%$ Resources:YYYYMMDD %>" />
                          </asp:dropdownlist>
                </td>
                <td></td>
                <td>
                    <asp:label id="lblFormatDateOfCheckID" runat="server" text="<%$ Resources:lblFormatDateOfCheckID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" id="FormatDateOfCheck" rmxref="/Instance/Document/CheckStock/FormatDateOfCheck" rmxtype="checkbox" onclick="toggle_BasicOptions();" tabindex="1" />
                    <%--<input type="text" name="$node^112" value="" style="display:none" cancelledvalue="" id="FormatDateOfCheck_mirror">--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label id="lblPrintMemoID" runat="server" text="<%$ Resources:lblPrintMemoID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" id="PrintMemoonCheck" rmxref="/Instance/Document/CheckStock/PrintMemo" rmxtype="checkbox" onclick="toggle_BasicOptions();" tabindex="1" />

                </td>
                 <td>&nbsp;&nbsp;</td>
                <td>
                   <asp:label id="lblpayeeaddresscheckfont" runat="server" text="<%$ Resources:lblpayeeaddresscheckfont %>"></asp:label>
                   &nbsp;&nbsp;</td>
               <td width="205px">


                   <asp:dropdownlist runat="server" id="ddpayeeaddresscheckfont" tabindex="1" rmxref="/Instance/Document/CheckStock/PayeeAddressFont" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">                            
                            <asp:ListItem Value="0" Text="" />
							<asp:listitem value="6" Text="<%$ Resources:liFontSize6 %>" />
                            <asp:listitem value="8" Text="<%$ Resources:liFontSize8 %>" />
                            <asp:listitem value="10" Text="<%$ Resources:liFontSize10 %>" />
                            <asp:listitem value="12" Text="<%$ Resources:liFontSize12 %>" />
                            <asp:listitem value="14" Text="<%$ Resources:liFontSize14 %>" />
                            <asp:listitem value="16" Text="<%$ Resources:liFontSize16 %>" />
                            <asp:listitem value="18" Text="<%$ Resources:liFontSize18 %>" />
                            <asp:listitem value="20" Text="<%$ Resources:liFontSize20 %>" />
                            <asp:listitem value="22" Text="<%$ Resources:liFontSize22 %>" />
                            <asp:listitem value="24" Text="<%$ Resources:liFontSize24 %>" />
            </asp:dropdownlist>


               </td>
            </tr>
            <td><u>
                <asp:label id="lblMemoSettingsID" runat="server" text="<%$ Resources:lblMemoSettingsID %>"></asp:label>
            </u>:&nbsp;&nbsp;
            </td>
            <td></td>
            </tr>
           <tr id="Tr43">
               <td>
                   <asp:label id="lblFontNameID" runat="server" text="<%$ Resources:lblFontNameID %>"></asp:label>
                   &nbsp;&nbsp;</td>
               <td width="205px">

                   <asp:dropdownlist runat="server" id="memoFontName" tabindex="1" rmxref="/Instance/Document/CheckStock/MemoFontName" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">
                            <asp:ListItem Value="0" Text="" />
                            <asp:listitem value="1" text="<%$ Resources:liArial %>" />
                            <asp:listitem value="2" text="<%$ Resources:liArialBlack %>" />
                            <asp:listitem value="3" text="<%$ Resources:liArialCE %>" />
                            <asp:listitem value="4" text="<%$ Resources:liArialGreek %>" />
                            <asp:listitem value="5" text="<%$ Resources:liArialCYR %>" />
                            <asp:listitem value="6" text="<%$ Resources:liArialNarrow %>" />
                            <asp:listitem value="7" text="<%$ Resources:liArialTUR %>" />
                            <asp:listitem value="8" text="<%$ Resources:liBookAntiqua %>" />
                            <asp:listitem value="9" text="<%$ Resources:liBookmanOldStyle %>" />
                            <asp:listitem value="10" text="<%$ Resources:liCenturyGothic %>" />
                            <asp:listitem value="11" text="<%$ Resources:liComicSansMS %>" />
                            <asp:listitem value="12" text="<%$ Resources:liCourierNew %>"  />
                            <asp:listitem value="13" text="<%$ Resources:liCourierNewCE %>" />
                            <asp:listitem value="14" text="<%$ Resources:liCourierNewCYR %>" />
                            <asp:listitem value="15" text="<%$ Resources:liCourierNewGreek %>" />
                            <asp:listitem value="16" text="<%$ Resources:liCourierNewTUR %>" />
                            <asp:listitem value="17" text="<%$ Resources:liGaramond %>" />
                            <asp:listitem value="18" text="<%$ Resources:liGeorgia %>" />
                            <asp:listitem value="19" text="<%$ Resources:liImpact %>" />
                            <asp:listitem value="20" text="<%$ Resources:liLucidaConsole %>" />
                            <asp:listitem value="21" text="<%$ Resources:liLucidaSansUnicode %>" />
                            <asp:listitem value="22" text="<%$ Resources:liMarlett %>" />
                            <asp:listitem value="23" text="<%$ Resources:liMicrosoftSansSerif %>" />
                            <asp:listitem value="24" text="<%$ Resources:liModern %>"  />
                            <asp:ListItem Value="25" Text="<%$ Resources:liMICR %>" />
                            <asp:listitem value="26" text="<%$ Resources:liMonotypeCorsiva %>"/>
                            <asp:listitem value="27" text="<%$ Resources:liMSOutlook %>" />
                            <asp:listitem value="28" text="<%$ Resources:liPalatinoLinotype %>" />
                            <asp:listitem value="29" text="<%$ Resources:liRomanScript %>" />
                            <asp:listitem value="30" text="<%$ Resources:liSymbol %>" />
                            <asp:listitem value="31" text="<%$ Resources:liTahoma %>" />
                            <asp:listitem value="32" text="<%$ Resources:liTimesNewRoman %>" />
                            <asp:listitem value="33" text="<%$ Resources:liTimesNewRomanCE %>" />
                            <asp:listitem value="34" text="<%$ Resources:liTimesNewRomanCYR %>" />
                            <asp:listitem value="35" text="<%$ Resources:liTimesNewRomanGreek %>" />
                            <asp:listitem value="36" text="<%$ Resources:liTimesNewRomanTUR %>" />
                            <asp:listitem value="37" text="<%$ Resources:liTrebuchetMS %>" />
                            <asp:listitem value="38" text="<%$ Resources:liVerdana %>" />
                            <asp:listitem value="39" text="<%$ Resources:liVisualUI %>" />
                            <asp:listitem value="40" text="<%$ Resources:liWebdings %>" />
                            <asp:listitem value="41" text="<%$ Resources:liWindings %>" />
                            <asp:listitem value="42" text="<%$ Resources:liWindings2 %>" />
                            <asp:listitem value="43" text="<%$ Resources:liWindings3 %>" />                   
                          </asp:dropdownlist>
               </td>
               <td>&nbsp;&nbsp;</td>

               <td>
                   <asp:label id="lblMICRFntSizeID" runat="server" text="<%$ Resources:lblMICRFontSizeID %>"></asp:label>
                   &nbsp;&nbsp;</td>
               <td width="205px">


                   <asp:dropdownlist runat="server" id="memoFontSize" tabindex="1" rmxref="/Instance/Document/CheckStock/MemoFontSize" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">                            
                            <asp:ListItem Value="0" Text="" />
							<asp:listitem value="6" Text="<%$ Resources:liFontSize6 %>" />
                            <asp:listitem value="8" Text="<%$ Resources:liFontSize8 %>" />
                            <asp:listitem value="10" Text="<%$ Resources:liFontSize10 %>" />
                            <asp:listitem value="12" Text="<%$ Resources:liFontSize12 %>" />
                            <asp:listitem value="14" Text="<%$ Resources:liFontSize14 %>" />
                            <asp:listitem value="16" Text="<%$ Resources:liFontSize16 %>" />
                            <asp:listitem value="18" Text="<%$ Resources:liFontSize18 %>" />
                            <asp:listitem value="20" Text="<%$ Resources:liFontSize20 %>" />
                            <asp:listitem value="22" Text="<%$ Resources:liFontSize22 %>" />
                            <asp:listitem value="24" Text="<%$ Resources:liFontSize24 %>" />
            </asp:dropdownlist>


               </td>
           </tr>

            <tr id="Tr44">
                <td>
                    <asp:label id="lblNoOfRowsID" runat="server" text="<%$ Resources:lblNoOfRowsID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>


                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="NoofRowsForMemo"
                        rmxref="/Instance/Document/CheckStock/MemoRows" rmxtype="numeric" tabindex="1"
                        onchange="setDataChanged(true);" />



                    &nbsp;&nbsp;</td>
                <td></td>
                <td>
                    <asp:label id="lblNoOfCharactersID" runat="server" text="<%$ Resources:lblNoOfCharactersID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>

                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="NoofCharactersperMemoRow"
                        rmxref="/Instance/Document/CheckStock/MemoRowLength" rmxtype="numeric" tabindex="2"
                        onchange="setDataChanged(true);" />

                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>

            </tr>

            <%-- amar--%>
            <tr>
                <td colspan="5" class="msgheader">
                    <asp:label id="Label2" runat="server" text="<%$ Resources:lblformtitlecheckstubID %>" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label id="lblPrintControlNoID" runat="server" text="<%$ Resources:lblPrintControlNoID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintControlonCheckStub" rmxref="/Instance/Document/CheckStock/PrintCtlNumberOnStub" rmxtype="checkbox" tabindex="1" />
                </td>
                <td></td>
                <td>
                    <asp:label id="lblAgentNameAddressID" runat="server" text="<%$ Resources:lblAgentNameAddressID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="AgentNameAddressonStub" rmxref="/Instance/Document/CheckStock/PrintAgentNameAddressOnStub" rmxtype="checkbox" tabindex="1" />
                </td>
                <%--  <td>
                    <asp:label id="lblCheckDateFormat" runat="server" text="<%$ Resources:lblCheckDateFormat %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td width="205px">


                    <asp:dropdownlist runat="server" id="CheckDateFormat" tabindex="1" rmxref="/Instance/Document/CheckStock/CheckDateFormat" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">
                            <asp:ListItem Value="0" Text="" />
                            <asp:ListItem Value="1" Text="<%$ Resources:DMYYYY %>" />
                            <asp:ListItem Value="2" Text="<%$ Resources:DDMMYYYY %>" />
                            <asp:ListItem Value="3" Text="<%$ Resources:YYYYMD %>" />
                            <asp:ListItem Value="4" Text="<%$ Resources:YYYYMMDD %>" />
                          </asp:dropdownlist>
                </td>--%>
            </tr>
            <tr id="Tr36">
                <%--<td>
                    <asp:label id="lblPreNumberedCheckStockID" runat="server" text="<%$ Resources:lblPreNumberedCheckStockID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox tabindex="2" runat="server" onclick="SetSaveFlag();" id="PreNumberedCheckStock" rmxref="/Instance/Document/CheckStock/PreNumStock" rmxtype="checkbox" />

                </td>

                <td>&nbsp;&nbsp;</td>--%>
                <td>
                    <asp:label id="lblPrintAdjusterNameID" runat="server" text="<%$ Resources:lblPrintAdjusterNameID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>

                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintAdjusterNameandPhoneoncheckStub" rmxref="/Instance/Document/CheckStock/PrintAdjNamePhoneOnStub" rmxtype="checkbox" tabindex="2" />

                </td>
                <td></td>
                <td>
                    <asp:label id="lblFullPolicyNumberNoID" runat="server" text="<%$ Resources:lblFullPolicyNumberNoID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>

                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="FullPolicyNumberNoID" rmxref="/Instance/Document/CheckStock/PrintFullPolicyNumberonStub" rmxtype="checkbox" tabindex="2" />

                </td>

            </tr>

            <tr id="Tr37">
                <%--<td>
                    <asp:label id="lblPrintMemoID" runat="server" text="<%$ Resources:lblPrintMemoID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" id="PrintMemoonCheck" rmxref="/Instance/Document/CheckStock/PrintMemo" rmxtype="checkbox" onclick="toggle_BasicOptions();" tabindex="1" />

                </td>--%>

                <%--<td>

                </td>
                <td></td>--%>
                <%--<td>&nbsp;&nbsp;</td>--%>
                <td>
                    <asp:label id="lblPrintDateofCheckID" runat="server" text="<%$ Resources:lblPrintDateofCheckID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" id="PrintDateOfCheck" rmxref="/Instance/Document/CheckStock/PrintCheckDateOnStub" rmxtype="checkbox" onclick="toggle_BasicOptions();" tabindex="1" />
                    <%--<input type="text" name="$node^112" value="" style="display:none" cancelledvalue="" id="PrintDateOfCheck_mirror">--%>
                </td>
                <td></td>

                <td>
                    <asp:label id="lblDateOfClaimNoID" runat="server" text="<%$ Resources:lblDateOfClaimNoID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" id="DateofClaimOnStub" rmxref="/Instance/Document/CheckStock/PrintClaimDateOnStub" rmxtype="checkbox" onclick="toggle_BasicOptions();" tabindex="1" />
                    <%--<input type="text" name="$node^112" value="" style="display:none" cancelledvalue="" id="PrintDateOfCheck_mirror">--%>
                </td>
            </tr>

            <tr id="Tr38">
                <td>
                    <asp:textbox style="display: none" runat="server" id="PrintPayeeTaxIDoncheckStub_mirror" rmxref="/Instance/Document/void" rmxtype="hidden" />
                </td>
                <td></td>
            </tr>
            <tr id="Tr39">

                <td>
                    <asp:label id="lblPrintEventNoID" runat="server" text="<%$ Resources:lblPrintEventNoID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintEventoncheckStub" rmxref="/Instance/Document/CheckStock/PrntEventOnStub" rmxtype="checkbox" tabindex="2" />

                    <asp:textbox style="display: none" runat="server" id="PrintEventoncheckStub_mirror" rmxref="/Instance/Document/void" rmxtype="hidden" />

                </td>
                <td>&nbsp;&nbsp;</td>

                <td>
                    <asp:label id="lblPrintPayeeTaxID" runat="server" text="<%$ Resources:lblPrintPayeeTaxID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintPayeeTaxIDoncheckStub" rmxref="/Instance/Document/CheckStock/PrintTaxIDOnStub" rmxtype="checkbox" tabindex="1" />

                </td>
                <%-- <td>
                    <asp:label id="lblFormatDateOfCheckID" runat="server" text="<%$ Resources:lblFormatDateOfCheckID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" id="FormatDateOfCheck" rmxref="/Instance/Document/CheckStock/FormatDateOfCheck" rmxtype="checkbox" onclick="toggle_BasicOptions();" tabindex="1" />
                    <%--<input type="text" name="$node^112" value="" style="display:none" cancelledvalue="" id="FormatDateOfCheck_mirror">
                </td>--%>
            </tr>

            <tr id="Tr40">
                <td>
                    <asp:textbox style="display: none" runat="server" id="PrintControlonCheckStub_mirror" rmxref="/Instance/Document/void" rmxtype="hidden" />
                </td>
                <td>
                    <asp:textbox style="display: none" runat="server" id="PrintAdjusterNameandPhoneoncheckStub_mirror" rmxref="/Instance/Document/void" rmxtype="hidden" />
                </td>
            </tr>

           <%-- <tr id="Tr41">
                <td></td>
                <%--<td>
                    <asp:label id="lblPayerLevelID" runat="server" text="<%$ Resources:lblPayerLevelID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td width="205px">

                    <asp:dropdownlist runat="server" id="PayerLevelForChecks" tabindex="1" rmxref="/Instance/Document/CheckStock/PayerLevel" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">
                            <asp:ListItem Value="0" Text="" />
                            <asp:ListItem Value="1" Text="<%$ Resources:liSysParmClient %>" />
                            <asp:ListItem Value="1005" Text="<%$ Resources:liClient %>" />
                            <asp:ListItem Value="1006" Text="<%$ Resources:liCompany %>" />
                            <asp:ListItem Value="1007" Text="<%$ Resources:liOperation %>" />
                            <asp:ListItem Value="1008" Text="<%$ Resources:liRegion %>" />
                            <asp:ListItem Value="1009" Text="<%$ Resources:liDivision %>" />
                            <asp:ListItem Value="1010" Text="<%$ Resources:liLocation %>" />
                            <asp:ListItem Value="1011" Text="<%$ Resources:liFacility %>" />
                            <asp:ListItem Value="1012" Text="<%$ Resources:liDepartment %>" />
                          </asp:dropdownlist>
                </td>--%>

                <%--<td>&nbsp;&nbsp;</td>--%>

                <%-- <td>
                    <asp:label id="lblCheckDateFormat" runat="server" text="<%$ Resources:lblCheckDateFormat %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td width="205px">


                    <asp:dropdownlist runat="server" id="CheckDateFormat" tabindex="1" rmxref="/Instance/Document/CheckStock/CheckDateFormat" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">
                            <asp:ListItem Value="0" Text="" />
                            <asp:ListItem Value="1" Text="<%$ Resources:DMYYYY %>" />
                            <asp:ListItem Value="2" Text="<%$ Resources:DDMMYYYY %>" />
                            <asp:ListItem Value="3" Text="<%$ Resources:YYYYMD %>" />
                            <asp:ListItem Value="4" Text="<%$ Resources:YYYYMMDD %>" />
                          </asp:dropdownlist>
                </td>--%>
            <tr id="Tr34">
                
                <td>
                    <asp:label id="lblPrintPayeeAddressOnStub" runat="server" text="<%$ Resources:lblPrintPayeeAddressOnStub %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintPayeeAddressOnStub" rmxref="/Instance/Document/CheckStock/PrintPayeeAddressOnStub" rmxtype="checkbox" tabindex="1" />

                </td>
            </tr>

            <%--<tr>
                <td>
                    <br>
                </td>
            </tr>
            <td><u>
                <asp:label id="lblMemoSettingsID" runat="server" text="<%$ Resources:lblMemoSettingsID %>"></asp:label>
            </u>:&nbsp;&nbsp;
            </td>
            <td></td>
            </tr>
           <tr id="Tr43">
               <td>
                   <asp:label id="lblFontNameID" runat="server" text="<%$ Resources:lblFontNameID %>"></asp:label>
                   &nbsp;&nbsp;</td>
               <td width="205px">

                   <asp:dropdownlist runat="server" id="memoFontName" tabindex="1" rmxref="/Instance/Document/CheckStock/MemoFontName" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">
                            <asp:ListItem Value="0" Text="" />
                            <asp:listitem value="1" text="<%$ Resources:liArial %>" />
                            <asp:listitem value="2" text="<%$ Resources:liArialBlack %>" />
                            <asp:listitem value="3" text="<%$ Resources:liArialCE %>" />
                            <asp:listitem value="4" text="<%$ Resources:liArialGreek %>" />
                            <asp:listitem value="5" text="<%$ Resources:liArialCYR %>" />
                            <asp:listitem value="6" text="<%$ Resources:liArialNarrow %>" />
                            <asp:listitem value="7" text="<%$ Resources:liArialTUR %>" />
                            <asp:listitem value="8" text="<%$ Resources:liBookAntiqua %>" />
                            <asp:listitem value="9" text="<%$ Resources:liBookmanOldStyle %>" />
                            <asp:listitem value="10" text="<%$ Resources:liCenturyGothic %>" />
                            <asp:listitem value="11" text="<%$ Resources:liComicSansMS %>" />
                            <asp:listitem value="12" text="<%$ Resources:liCourierNew %>"  />
                            <asp:listitem value="13" text="<%$ Resources:liCourierNewCE %>" />
                            <asp:listitem value="14" text="<%$ Resources:liCourierNewCYR %>" />
                            <asp:listitem value="15" text="<%$ Resources:liCourierNewGreek %>" />
                            <asp:listitem value="16" text="<%$ Resources:liCourierNewTUR %>" />
                            <asp:listitem value="17" text="<%$ Resources:liGaramond %>" />
                            <asp:listitem value="18" text="<%$ Resources:liGeorgia %>" />
                            <asp:listitem value="19" text="<%$ Resources:liImpact %>" />
                            <asp:listitem value="20" text="<%$ Resources:liLucidaConsole %>" />
                            <asp:listitem value="21" text="<%$ Resources:liLucidaSansUnicode %>" />
                            <asp:listitem value="22" text="<%$ Resources:liMarlett %>" />
                            <asp:listitem value="23" text="<%$ Resources:liMicrosoftSansSerif %>" />
                            <asp:listitem value="24" text="<%$ Resources:liModern %>"  />
                            <asp:ListItem Value="25" Text="<%$ Resources:liMICR %>" />
                            <asp:listitem value="26" text="<%$ Resources:liMonotypeCorsiva %>"/>
                            <asp:listitem value="27" text="<%$ Resources:liMSOutlook %>" />
                            <asp:listitem value="28" text="<%$ Resources:liPalatinoLinotype %>" />
                            <asp:listitem value="29" text="<%$ Resources:liRomanScript %>" />
                            <asp:listitem value="30" text="<%$ Resources:liSymbol %>" />
                            <asp:listitem value="31" text="<%$ Resources:liTahoma %>" />
                            <asp:listitem value="32" text="<%$ Resources:liTimesNewRoman %>" />
                            <asp:listitem value="33" text="<%$ Resources:liTimesNewRomanCE %>" />
                            <asp:listitem value="34" text="<%$ Resources:liTimesNewRomanCYR %>" />
                            <asp:listitem value="35" text="<%$ Resources:liTimesNewRomanGreek %>" />
                            <asp:listitem value="36" text="<%$ Resources:liTimesNewRomanTUR %>" />
                            <asp:listitem value="37" text="<%$ Resources:liTrebuchetMS %>" />
                            <asp:listitem value="38" text="<%$ Resources:liVerdana %>" />
                            <asp:listitem value="39" text="<%$ Resources:liVisualUI %>" />
                            <asp:listitem value="40" text="<%$ Resources:liWebdings %>" />
                            <asp:listitem value="41" text="<%$ Resources:liWindings %>" />
                            <asp:listitem value="42" text="<%$ Resources:liWindings2 %>" />
                            <asp:listitem value="43" text="<%$ Resources:liWindings3 %>" />                   
                          </asp:dropdownlist>
               </td>
               <td>&nbsp;&nbsp;</td>

               <td>
                   <asp:label id="lblMICRFntSizeID" runat="server" text="<%$ Resources:lblMICRFontSizeID %>"></asp:label>
                   &nbsp;&nbsp;</td>
               <td width="205px">


                   <asp:dropdownlist runat="server" id="memoFontSize" tabindex="1" rmxref="/Instance/Document/CheckStock/MemoFontSize" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">                            
                            <asp:ListItem Value="0" Text="" />
							<asp:listitem value="6" Text="<%$ Resources:liFontSize6 %>" />
                            <asp:listitem value="8" Text="<%$ Resources:liFontSize8 %>" />
                            <asp:listitem value="10" Text="<%$ Resources:liFontSize10 %>" />
                            <asp:listitem value="12" Text="<%$ Resources:liFontSize12 %>" />
                            <asp:listitem value="14" Text="<%$ Resources:liFontSize14 %>" />
                            <asp:listitem value="16" Text="<%$ Resources:liFontSize16 %>" />
                            <asp:listitem value="18" Text="<%$ Resources:liFontSize18 %>" />
                            <asp:listitem value="20" Text="<%$ Resources:liFontSize20 %>" />
                            <asp:listitem value="22" Text="<%$ Resources:liFontSize22 %>" />
                            <asp:listitem value="24" Text="<%$ Resources:liFontSize24 %>" />
            </asp:dropdownlist>


               </td>
           </tr>

            <tr id="Tr44">
                <td>
                    <asp:label id="lblNoOfRowsID" runat="server" text="<%$ Resources:lblNoOfRowsID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>


                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="NoofRowsForMemo"
                        rmxref="/Instance/Document/CheckStock/MemoRows" rmxtype="numeric" tabindex="1"
                        onchange="setDataChanged(true);" />



                    <&nbsp;&nbsp;</td>
                <td></td>
                <td>
                    <asp:label id="lblNoOfCharactersID" runat="server" text="<%$ Resources:lblNoOfCharactersID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>

                    <asp:textbox runat="server" onblur="numLostFocus(this);" id="NoofCharactersperMemoRow"
                        rmxref="/Instance/Document/CheckStock/MemoRowLength" rmxtype="numeric" tabindex="2"
                        onchange="setDataChanged(true);" />

                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>

            </tr>--%>
            <%--amar--%>
            <%--         Ashish Ahuja - Pay To The Order Config--%>
            <tr id="Tr50">
            <tr>
             <td><br></td>
            </tr>
            <td><u>
                <asp:Label ID="lblPayToOrderSettingsID" runat="server" Text="<%$ Resources:lblPayToOrderSettingsID %>"></asp:Label></u>:&nbsp;&nbsp;
            </td>
            <td></td>
           </tr>
           
           <tr id="Tr51">
            <td>
                <asp:Label ID="lblSampleText" runat="server" Text="<%$ Resources:lblPayToOrderSampleTextID %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
                <asp:TextBox runat="server" id="txtSampleText" RMXRef="/Instance/Document/CheckStock/SampleText" MaxLength="1000" RMXType="Text" tabindex="1" Text="Sample Text" />            
            </td>
           </tr>
           <tr id="Tr41">
            <td>
                <asp:Label ID="lblPayToOrderFontNameID" runat="server" Text="<%$ Resources:lblPayToOrderFontNameID %>"></asp:Label>&nbsp;&nbsp;</td>
            <td width="205px">
            
             <asp:DropDownList runat="server" id="payToOrderFontName" tabindex="1" RMXRef="/Instance/Document/CheckStock/PayToOrderFontName" RMXType="combobox" width="205px" onchange="setDataChanged(true);">
                            <asp:listitem value="0" Text="" />
                    <asp:listitem value="Arial" Text="<%$ Resources:liArial %>" />
                    <asp:listitem value="Arial Black" Text="<%$ Resources:liArialBlack %>" />
                    <asp:listitem value="Arial CE" Text="<%$ Resources:liArialCE %>" />
                    <asp:listitem value="Arial Greek" Text="<%$ Resources:liArialGreek %>" />
                    <asp:listitem value="Arial CYR" Text="<%$ Resources:liArialCYR %>"/>
                    <asp:listitem value="Arial Narrow" Text="<%$ Resources:liArialNarrow %>" />
                    <asp:listitem value="Arial TUR" Text="<%$ Resources:liArialTUR %>" />
                    <asp:listitem value="Book Antiqua" Text="<%$ Resources:liBookAntiqua %>" />
                    <asp:listitem value="Bookman Old Style" Text="<%$ Resources:liBookmanOldStyle %>" />
                    <asp:listitem value="Century Gothic" Text="<%$ Resources:liCenturyGothic %>" />
                    <asp:listitem value="Comic Sans MS" Text="<%$ Resources:liComicSansMS %>" />
                    <asp:listitem value="Courier New" Text="<%$ Resources:liCourierNew %>" />
                    <asp:listitem value="Courier New CE" Text="<%$ Resources:liCourierNewCE %>" />
                    <asp:listitem value="Courier New CYR" Text="<%$ Resources:liCourierNewCYR %>" />
                    <asp:listitem value="Courier New Greek" Text="<%$ Resources:liCourierNewGreek %>" />
                    <asp:listitem value="Courier New TUR" Text="<%$ Resources:liCourierNewTUR %>" />
                    <asp:listitem value="Garamond" Text="<%$ Resources:liGaramond %>" />
                    <asp:listitem value="Georgia" Text="<%$ Resources:liGeorgia %>" />
                    <asp:listitem value="Impact" Text="<%$ Resources:liImpact %>" />
                    <asp:listitem value="Lucida Console" Text="<%$ Resources:liLucidaConsole %>" />
                    <asp:listitem value="Lucida Sans Unicode" Text="<%$ Resources:liLucidaSansUnicode %>" />
                    <asp:listitem value="Marlett" Text="<%$ Resources:liMarlett %>" />
                    <asp:listitem value="Microsoft Sans Serif" Text="<%$ Resources:liMicrosoftSansSerif %>" />
                    <asp:listitem value="Modern" Text="<%$ Resources:liModern %>"/>
                    <asp:listitem value="Covix MICR Unicode" Text="<%$ Resources:liCovixMICRUnicode %>" />
                    <asp:listitem value="Monotype Corsiva" Text="<%$ Resources:liMonotypeCorsiva %>" />
                    <asp:listitem value="MS Outlook" Text="<%$ Resources:liMSOutlook %>" />
                    <asp:listitem value="Palatino Linotype" Text="<%$ Resources:liPalatinoLinotype %>" />
                    <asp:listitem value="Roman Script" Text="<%$ Resources:liRomanScript %>" />
                    <asp:listitem value="Symbol" Text="<%$ Resources:liSymbol %>" />
                    <asp:listitem value="Tahoma" Text="<%$ Resources:liTahoma %>" />
                    <asp:listitem value="Times New Roman" Text="<%$ Resources:liTimesNewRoman %>" />
                    <asp:listitem value="Times New Roman CE" Text="<%$ Resources:liTimesNewRomanCE %>" />
                    <asp:listitem value="Times New Roman CYR" Text="<%$ Resources:liTimesNewRomanCYR %>" />
                    <asp:listitem value="Times New Roman Greek" Text="<%$ Resources:liTimesNewRomanGreek %>" />
                    <asp:listitem value="Times New Roman TUR" Text="<%$ Resources:liTimesNewRomanTUR %>" />
                    <asp:listitem value="Trebuchet MS" text="<%$ Resources:liTrebuchetMS %>" />
                    <asp:listitem value="Verdana" Text="<%$ Resources:liVerdana %>" />
                    <asp:listitem value="VisualUI" Text="<%$ Resources:liVisualUI %>" />
                    <asp:listitem value="Webdings" Text="<%$ Resources:liWebdings %>" />
                    <asp:listitem value="Windings" Text="<%$ Resources:liWindings %>" />
                    <asp:listitem value="Windings 2" Text="<%$ Resources:liWindings2 %>" />
                    <asp:listitem value="Windings 3" Text="<%$ Resources:liWindings3 %>" />                   
                          </asp:DropDownList>                            
              </td>
            <td>&nbsp;&nbsp;</td>
            
            <td>
                <asp:Label ID="lblPayToOrderMICRFntSizeID" runat="server" Text="<%$ Resources:lblPayToOrderMICRFontSizeID %>"></asp:Label>&nbsp;&nbsp;</td>            
            <td width="205px">            
            
            
            <asp:DropDownList runat="server" id="payToOrderFontSize" tabindex="1" RMXRef="/Instance/Document/CheckStock/PayToOrderFontSize" RMXType="combobox" width="205px" onchange="setDataChanged(true);">                            
                            <asp:ListItem Value="0" Text="" />
							<asp:listitem value="6" Text="<%$ Resources:liFontSize6 %>" />
                            <asp:listitem value="8" Text="<%$ Resources:liFontSize8 %>" />
                            <asp:listitem value="10" Text="<%$ Resources:liFontSize10 %>" />
                            <asp:listitem value="12" Text="<%$ Resources:liFontSize12 %>" />
                            <asp:listitem value="14" Text="<%$ Resources:liFontSize14 %>" />
                            <asp:listitem value="16" Text="<%$ Resources:liFontSize16 %>" />
                            <asp:listitem value="18" Text="<%$ Resources:liFontSize18 %>" />
                            <asp:listitem value="20" Text="<%$ Resources:liFontSize20 %>" />
                            <asp:listitem value="22" Text="<%$ Resources:liFontSize22 %>" />
                            <asp:listitem value="24" Text="<%$ Resources:liFontSize24 %>" />
            </asp:DropDownList>          
                          
                          
              </td>
           </tr>
           
           <tr id="Tr42">
            <td>
                <asp:Label ID="lblPayToOrderNoOfRowsID" runat="server" Text="<%$ Resources:lblPayToOrderNoOfRowsID %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
            
            
            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="NoofRowsForPayToOrder" 
            RMXRef="/Instance/Document/CheckStock/PayToOrderRows" RMXType="int"  tabindex="1" 
            onChange="setDataChanged(true);PayToTheOrderValidate();" MaxLength="9" />            
                                                       
                       
            
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>
                <asp:Label ID="lblPayToOrderNoOfCharactersID" runat="server" Text="<%$ Resources:lblPayToOrderNoOfCharactersID %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
            
            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="NoofCharactersperPayToOrderRow" 
                         RMXRef="/Instance/Document/CheckStock/PayToOrderRowLength" RMXType="int" tabindex="1" 
                        MaxLength="9" onChange="setDataChanged(true);PayToTheOrderValidate();" />           
            
            </td>
           </tr>
   <%--  Ashish Ahuja - Pay To The Order Config --%>
 
            <tr>
                <td colspan="5" class="msgheader">
                    <asp:label id="Label1" runat="server" text="<%$ Resources:lblformtitlecheckpagesetupID %>" />
                </td>
            </tr>
            <tr id="Tr30">
                <td nowrap="true">
                    <asp:label id="lblSuppressID" runat="server" text="<%$ Resources:lblSuppressID %>"></asp:label>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:checkbox runat="server" onclick="SetSaveFlag();" id="SuppressSign" rmxref="/Instance/Document/CheckStock/DollarSign" rmxtype="checkbox" tabindex="1" />
                </td>
                <td>&nbsp;&nbsp;</td>

                <td nowrap="nowrap"><u>
                    <asp:label id="lblCheckPartSettingsID" runat="server" text="<%$ Resources:lblCheckPartSettingsID %>"></asp:label>
                </u>:&nbsp;&nbsp;
            &nbsp;
                </td>
                <td></td>

            </tr>
            <tr>
                <tr id="Tr46">

                    <td>
                        <asp:label id="Label3" runat="server" text="<%$ Resources:lblPreNumberedCheckStockID %>"></asp:label>
                        &nbsp;&nbsp;</td>
                    <td>
                        <asp:checkbox tabindex="2" runat="server" onclick="SetSaveFlag();" id="PreNumberedCheckStock" rmxref="/Instance/Document/CheckStock/PreNumStock" rmxtype="checkbox" />

                    </td>

                    <td>&nbsp;&nbsp;</td>

                    <td colspan="2" rowspan="2">
                        <asp:radiobutton groupname="checkaddress" runat="server" id="Blank" rmxref="/Instance/Document/radiothree" rmxtype="radio" onclick="AddressFlagzero();" text="<%$ Resources:rdbBlankID %>" />
                        <br />
                        <%--<input type="radio" name="$node^52" value="" onclick="AddressFlagzero();" id="Blank" checked>--%>
                        <asp:radiobutton groupname="checkaddress" runat="server" id="PayeeAddress" rmxref="/Instance/Document/radiothree" rmxtype="radio" onclick="AddressFlagone();" text="<%$ Resources:rdbPayeeAddressID %>" />
                        <br />
                        <asp:radiobutton groupname="checkaddress" runat="server" id="PaymentDetail" rmxref="/Instance/Document/radiothree" rmxtype="radio" onclick="AddressFlagtwo();" text="<%$ Resources:rdbPaymentDetailID %>" />
                    </td>


                </tr>
                <tr id="Tr47">
                    <td>
                        <asp:label id="lblPrintClaimInsurernewID" runat="server" text="<%$ Resources:lblPrintClaimInsurernewID %>"></asp:label>
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:checkbox runat="server" onclick="SetSaveFlag();" id="PrintClaiminsurerinsteadofPayer" rmxref="/Instance/Document/CheckStock/PrintInsurer" rmxtype="checkbox" tabindex="1" />


                    </td>
                    <td></td>
                    <td colspan="">

                        <%--<input type="radio" name="$node^52" value="" onclick="AddressFlagone();" id="PayeeAddress" checked>--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label id="lblPayerLevelID" runat="server" text="<%$ Resources:lblPayerLevelID %>"></asp:label>
                    </td>
                    <td width="205px">

                        <asp:dropdownlist runat="server" id="PayerLevelForChecks" tabindex="1" rmxref="/Instance/Document/CheckStock/PayerLevel" rmxtype="combobox" width="205px" onchange="setDataChanged(true);">
                            <asp:ListItem Value="0" Text="" />
                            <asp:ListItem Value="1" Text="<%$ Resources:liSysParmClient %>" />
                            <asp:ListItem Value="1005" Text="<%$ Resources:liClient %>" />
                            <asp:ListItem Value="1006" Text="<%$ Resources:liCompany %>" />
                            <asp:ListItem Value="1007" Text="<%$ Resources:liOperation %>" />
                            <asp:ListItem Value="1008" Text="<%$ Resources:liRegion %>" />
                            <asp:ListItem Value="1009" Text="<%$ Resources:liDivision %>" />
                            <asp:ListItem Value="1010" Text="<%$ Resources:liLocation %>" />
                            <asp:ListItem Value="1011" Text="<%$ Resources:liFacility %>" />
                            <asp:ListItem Value="1012" Text="<%$ Resources:liDepartment %>" />
                          </asp:dropdownlist>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
                <tr id="Tr48">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan=""></td>
                </tr>
                <tr id="Tr49">
                    <td>
                        <asp:textbox style="display: none" runat="server" id="hdnAddressFlag" rmxref="/Instance/Document/CheckStock/AddressFlag" rmxtype="hidden" />
                    </td>
                    <td>
                        <script language="JavaScript">toggle_BasicOptions();</script>
                    </td>
                </tr>
            </tr>


        </table>

    </div>
    <table>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;</td>
        </tr>


        <tr>
            <td>

                <asp:textbox style="display: none" runat="server" name="formname" text="<%$ Resources:txtcheckstockID %>" />
                <asp:textbox
                    style="display: none" runat="server" name="SysRequired" id="SysRequired" text="<%$ Resources:txtSysRequiredID %>" />
                <asp:textbox
                    style="display: none" runat="server" name="SysFocusFields" text="<%$ Resources:txtSysFocusFieldsID %>" />
                <asp:textbox
                    style="display: none" runat="server" id="hdSaveButtonClicked" name="hdSaveButtonClicked" />

                <asp:textbox runat="server" id="txtScreenFlowStack"
                    style="display: none" />

                <asp:button runat="server" text="<%$ Resources:btnPSID %>" class="button" id="btnPS"
                    onclientclick="return WindowOpenPrintSample();;" />
                <asp:button runat="server" text="<%$ Resources:btnCloneID %>" class="button" id="btnClone"
                    onclientclick="return WindowOpen();; " />
                <%-- Changed by Saurabh Arora for MITS	18908:Start --%>
                <%-- <asp:button runat="server" text="Back To Bank Accounts"  class="button" id="btnBankAccounts" onclientclick="return BackToBankAccounts();" />   --%>
                <asp:button runat="server" text="<%$ Resources:btnBankAccountsID %>" class="button" id="btnBankAccounts" onclientclick="return BackToBankAccounts();" onclick="NavigateBackToBankAccounts" />
                <%-- Changed by Saurabh Arora for MITS	18908:End --%>

                <asp:textbox runat="server" id="FromBacktoBankAccounts"
                    style="display: none" />



            </td>
        </tr>
    </table>

    <uc:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="" />
</form>



</body>
</html>
