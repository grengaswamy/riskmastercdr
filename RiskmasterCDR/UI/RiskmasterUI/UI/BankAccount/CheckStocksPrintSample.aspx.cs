﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;

namespace Riskmaster.UI.BankAccount
{
    public partial class CheckStocksPrintSample : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";

            try
            {
                StockId.Text = AppHelper.GetQueryStringValue("StockId");
                //Ashish Ahuja - Pay To The Order Config
                SampleText.Text = AppHelper.GetQueryStringValue("SampleText");
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWS("CheckStockSetupAdaptor.PrintCheckStockSample", XmlTemplate, out sCWSresponse, false, false);

                XmlTemplate = XElement.Parse(sCWSresponse.ToString());

                XElement oEle = XmlTemplate.XPathSelectElement("//file");
                //XElement oEle = XmlTemplate.
                if (oEle != null)
                {
                    string sFileContent = oEle.Value;
                    byte[] byteOrg = Convert.FromBase64String(sFileContent);

                    Response.Clear();
                    Response.Charset = "";
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "inline;filename=CheckStockPrintSample.pdf");
                    Response.BinaryWrite(byteOrg);
                    Response.End();
                }

                ErrorControl1.errorDom = sCWSresponse;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<CheckStockSetup>");
            sXml = sXml.Append("<StockId>");
            sXml = sXml.Append(StockId.Text);
            sXml = sXml.Append("</StockId>");
            //Ashish Ahuja - Pay To The Order Config starts
            sXml = sXml.Append("<SampleText>");
            sXml = sXml.Append(SampleText.Text);
            sXml = sXml.Append("</SampleText>");
            //Ashish Ahuja - Pay To The Order Config ends
            sXml = sXml.Append("</CheckStockSetup></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());


            return oTemplate;
        }

    }
}



