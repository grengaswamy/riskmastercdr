﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoneyMarketAccountBalanceInfo.aspx.cs" Inherits="Riskmaster.UI.BankAccount.MoneyMarketAccountBalanceInfo" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Money Market Account Balance Information</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" /> 
    <link rel="StyleSheet" href="../../Content/rmnet.css" type="text/css" />  
</head>
<body>
    <form id="frmData" runat="server">
    <div>
     <table>   
    <tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
         </tr>
    
    <tr>
     <td colspan="8" class="ctrlgroup">
         <asp:Label ID="lblMonMrktAccntBalInfoID" runat="server" Text="<%$ Resources:lblMonMrktAccntBalInfoID %>"></asp:Label></td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblPriorBalID" runat="server" Text="<%$ Resources:lblPriorBalID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox1" runat="server" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@PriorBalance" text="$0.00" Enabled="false"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label id="lblStatementDatesID" runat="server" Text="<%$ Resources:lblStatementDatesID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox id="sdate" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@StartDate" runat="server" type="text" text="N/A" Enabled="false"/>--<asp:textbox ID="edate" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@EndDate" runat="server" type="text" text="N/A" Enabled="false"  />
	<%-- <asp:button runat="server" class="CodeLookupControl" id="btnLoadPriorStatementDate"/>--%>
     </td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblDepInTransitID" runat="server" Text="<%$ Resources:lblDepInTransitID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox3" runat="server" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@TransitDeposits" Enabled="false" type="text" name="$node^27" value="$0.00" disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label id="lblDepositsID" runat="server" Text="<%$ Resources:lblDepositsID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox4" runat="server" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@NoOfDeposit" type="text" Enabled="false" name="$node^34" value="0" size="10" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblTransNotClearedID" runat="server" Text="<%$ Resources:lblTransNotClearedID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox5" runat="server" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@TransferNotCleared" type="text" Enabled="false" name="$node^28" value="$0.00" disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label id="lblTransfersID" runat="server" Text="<%$ Resources:lblTransfersID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox6" runat="server" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@NoOfTrans" type="text" Enabled="false" name="$node^35" value="0" size="10" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblReconItemsID" runat="server" Text="<%$ Resources:lblReconItemsID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox7" runat="server" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@ReconciledItems" type="text" Enabled="false" name="$node^29" value="$0.00" disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label id="lblReconDepositsID" runat="server" Text="<%$ Resources:lblReconDepositsID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox8" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@RecDepositNum" runat="server" type="text" Enabled="false" name="$node^36" value="0" size="10" disabled="true"/><asp:textbox ID="Textbox9" RMXRef="/Instance/Document/MoneyMarketAccountBalanceInfo/@RecDepositAmt" runat="server" type="text" name="$node^37" value="$0.00" disabled="true"/></td>
    </tr>      
    <tr>
     <td align="right"><asp:Label id="lblSubTotalID" runat="server" Text="<%$ Resources:lblSubTotalID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox16" runat="server" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@SubTotal" type="text" Enabled="false" name="$node^29" value="$0.00" disabled="true"/></td>
     <td class="datatd"></td>     
     <td align="right"><asp:Label id="lblRecTransferNumID" runat="server" Text="<%$ Resources:lblRecTransferNumID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox11" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@RecTransferNum" runat="server" type="text" name="$node^38" value="0" size="10" disabled="true"/><asp:textbox ID="Textbox12" RMXref="/Instance/Document/MoneyMarketAccountBalanceInfo/@RecTransferAmt" runat="server" type="text" name="$node^39" text="$0.00" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblCurrentID" runat="server" Text="<%$ Resources:lblCurrentID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox13" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@Current" runat="server" type="text" name="$node^31" value="$0.00" disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label id="lblRecAdjustmentNumID" runat="server" Text="<%$ Resources:lblRecAdjustmentNumID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox14" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@RecAdjustmentNum" runat="server" type="text" name="$node^40" value="0" size="10" disabled="true"/><asp:textbox ID="Textbox15" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@RecAdjustmentAmt" runat="server" type="text" name="$node^41" value="$0.00" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblOutOfBalanceID" runat="server" Text="<%$ Resources:lblOutOfBalanceID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox10" RMXRef="Instance/Document/MoneyMarketAccountBalanceInfo/@OutOfBalance" runat="server" type="text" name="$node^31" value="$0.00" disabled="true"/></td>     
    </tr>
    <tr>
     <td></td>
     <td></td>
     <td></td>
     <td align="center">     
     <asp:button  runat="server" text="<%$ Resources:btnButton1 %>" class="button" id="Button1" OnClientClick="window.close();"/>          
     </td>
    </tr>
   </table>        
    
    <asp:textbox ID="subaccountid" style="display:none" RMXRef="Instance/Document/SubAccountId" runat="server" />
    <asp:textbox ID="monmktacc" style="display:none" RMXRef="Instance/Document/MoneyMktAccount" runat="server" />     
    <asp:textbox ID="startdate" style="display:none" RMXRef="Instance/Document/StartDate" runat="server" />     
    <asp:textbox ID="enddate" style="display:none" RMXRef="Instance/Document/EndDate" runat="server" />        
 
					
    </div>
    </form>
</body>
</html>
