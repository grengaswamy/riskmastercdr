﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections.Generic;


namespace Riskmaster.UI.BankAccount
{
    public partial class reconciliationreport : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            //Amit 11/23/2009: Content, file name etc. are being handled using javascript in place of session variables Starts
            if (IsPostBack)
            {
                try
                {
                    ////Amit: content sent in session variable in place of query string 11/23/2009
                    //hdFileContent.Text = Session["FileContent"].ToString();
                    //filename.Text = AppHelper.GetQueryStringValue("filename");
                    //accountlength.Text = AppHelper.GetQueryStringValue("AccountLength");

                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWS("BankAccountManagementAdaptor.ProcessBalanceFile", XmlTemplate, out sCWSresponse, true, false);

                    XmlTemplate = XElement.Parse(sCWSresponse.ToString());

                    XElement oEle = XmlTemplate.XPathSelectElement("//File");
                    //XElement oEle = XmlTemplate.
                    if (oEle != null)
                    {
                        string sFileContent = oEle.Value;
                        byte[] byteOrg = Convert.FromBase64String(sFileContent);

                        Response.Clear();
                        Response.Charset = "";
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", "inline;filename=ReconciliationReport.pdf");
                        Response.BinaryWrite(byteOrg);
                        //Response.End();
                        Response.Flush();
                        Response.Close();
                    }

                    ErrorControl1.errorDom = sCWSresponse;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
            //Amit 11/23/2009: Content, file name etc. are being handled using javascript in place of session variables Ends
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {            
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<AccountBalance>");
            sXml = sXml.Append("<AccountNoLength>" + accountlength.Text);            
            sXml = sXml.Append("</AccountNoLength>");            
            //<file filename="C:\Documents and Settings\gbhatnagar\Desktop\balance.txt" xsi:type="xs:base64Binary">OTk4ODg4ODgwMDAwMDAwMTUwMDAwMDAwMTI1MTYxMDIyMDc=</file> 
            //</files>           
            sXml = sXml.Append("<files>");
            sXml = sXml.Append(@"<file filename=""" + filename.Text + @""">");
            sXml = sXml.Append(hdFileContent.Text);
            sXml = sXml.Append("</file>");
            sXml = sXml.Append("</files>");            
            sXml = sXml.Append("</AccountBalance></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
           

            return oTemplate;
        }



    }
}
