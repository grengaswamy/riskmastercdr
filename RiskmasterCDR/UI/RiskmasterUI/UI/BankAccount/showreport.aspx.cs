﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml.XPath;

namespace Riskmaster.UI.BankAccount
{
    public partial class showreport : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";

            try
            {
                accountid.Text = AppHelper.GetQueryStringValue("AccountId");
                reporttype.Text = AppHelper.GetQueryStringValue("ReportSelected");

                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWS("BankAccountManagementAdaptor.ShowReport", XmlTemplate, out sCWSresponse, true, false);

                XmlTemplate = XElement.Parse(sCWSresponse.ToString());

                XElement oEle = XmlTemplate.XPathSelectElement("//File");
                //XElement oEle = XmlTemplate.
                if (oEle != null)
                {
                    string sFileContent = oEle.Value;
                    byte[] byteOrg = Convert.FromBase64String(sFileContent);

                    Response.Clear();
                    Response.Charset = "";
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "inline;filename=ReconciliationReport.pdf");
                    Response.BinaryWrite(byteOrg);
                    Response.End();
                }

                ErrorControl1.errorDom = sCWSresponse;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        private XElement GetMessageTemplate()
        {       
                StringBuilder sXml = new StringBuilder("<Message>");
                

                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document>");  
                sXml = sXml.Append("<Reports>");
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(accountid.Text);
                sXml = sXml.Append("</AccountId>");
                sXml = sXml.Append("<ReportSelected>");
                sXml = sXml.Append(reporttype.Text);
                sXml = sXml.Append("</ReportSelected>");
                sXml = sXml.Append("</Reports>");  
                sXml = sXml.Append("</Document></Message>");

                XElement oTemplate = XElement.Parse(sXml.ToString());
                return oTemplate;
            }
    }
}
