﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.BankAccount
{
    public partial class CheckStocksClone : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";  

            try
            {
                AccountId.Text = AppHelper.GetQueryStringValue("AccountId");
                StockId.Text = AppHelper.GetQueryStringValue("StockId");

                if (!IsPostBack)
                {
                    XmlTemplate = GetMessageTemplate(false);
                    bReturnStatus = CallCWS("CheckStockSetupAdaptor.GetAccountNameAndId", XmlTemplate, out sCWSresponse, false, true);
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }



        /// <summary>
        /// Returns Message Template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate(bool bFlag)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");

            sXml = sXml.Append("<CheckStockSetup>");

            if (bFlag)
            {
                sXml = sXml.Append("<StockId>");
                sXml = sXml.Append(StockId.Text);
                sXml = sXml.Append("</StockId>");
                
                sXml = sXml.Append("<CurBankAccount>");

                if (UseCurrentBankAccount.Checked)
                {
                    sXml = sXml.Append("true");                    
                    sXml = sXml.Append("<AccountId>");
                    sXml = sXml.Append(AppHelper.GetQueryStringValue("AccountId"));                    
                    sXml = sXml.Append("</AccountId>");
                }
                else
                {
                    sXml = sXml.Append("false");
                    sXml = sXml.Append("<AccountId>");
                    sXml = sXml.Append(List.SelectedValue);                   
                    sXml = sXml.Append("</AccountId>");
                }

                sXml = sXml.Append("</CurBankAccount>");
            }

            sXml = sXml.Append("</CheckStockSetup></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }


        
        /// <summary>
        /// Create
        /// </summary>
        public void Create(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";  

            try
            {
                
                XmlTemplate = GetMessageTemplate(true);
                bReturnStatus = CallCWS("CheckStockSetupAdaptor.CloneCheckStockDetails", XmlTemplate, out sCWSresponse, false, true);
                	
                //clonesaved.Text = "RecordSaved"; Rakhel ML Changes
                clonesaved.Text = RMXResourceProvider.GetSpecificObject("txtRecordSaved", RMXResourceManager.RMXResourceProvider.PageId("CheckStocksClone.aspx"), "0"); 
                                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }




    }
}
