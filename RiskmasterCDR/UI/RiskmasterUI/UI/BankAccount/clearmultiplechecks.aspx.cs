﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.BankAccount
{
    public partial class clearmultiplechecks : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            DataRow objRow = null;
            XmlNodeList objNodeList = null;
            XmlNode objXmlNode = null;
            XmlDocument objDocument = null;
            DataTable objDataTable = null;

            try
            {
                accountid.Text = AppHelper.GetQueryStringValue("AccountId");
                subaccountid.Text = AppHelper.GetQueryStringValue("SubAccountId");
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("clearmultiplechecks.aspx"), "clearmultiplechecksValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "clearmultiplechecksValidations", sValidationResources, true);

                if (ouraction.Text == "OK")
                {
                    DeleteSelectedDeposits();
                    XmlTemplate = GetMessageTemplate();                    
                    bReturnStatus = CallCWS("BankAccountManagementAdaptor.ClearDeposits", XmlTemplate, out sCWSresponse, false, false);
                    ErrorControl1.errorDom = sCWSresponse;
                    ouraction.Text = ""; 
                }

                XmlTemplate = GetMessageTemplate();

                bReturnStatus = CallCWS("BankAccountManagementAdaptor.GetDeposits", XmlTemplate, out sCWSresponse, true, false);
                
                objDocument = new XmlDocument();                
                objDocument.LoadXml(sCWSresponse);
                
                objNodeList = objDocument.SelectNodes("//Deposit");

                objDataTable = new DataTable("Deposits");
                objDataTable.Columns.Add("DepositId");             
                objDataTable.Columns.Add("ControlNo");
                objDataTable.Columns.Add("TransDate");
                objDataTable.Columns.Add("Amount");
                objDataTable.Columns.Add("DepositType");

                objXmlNode = objDocument.SelectSingleNode("//Deposits");
                if(objXmlNode!=null)
                {
                    lblCurrentBank.Text = objXmlNode.Attributes["AccountName"].Value ;
                }

                foreach (XmlNode objNode in objNodeList)
                {                    
                    objRow = objDataTable.NewRow();
                    objRow["DepositId"] = objNode.Attributes["DepositId"].Value;
                    objRow["ControlNo"] = objNode.Attributes["ControlNo"].Value;
                    objRow["TransDate"] = objNode.Attributes["TransDate"].Value;
                    objRow["Amount"] = objNode.Attributes["Amount"].Value;
                    objRow["DepositType"] = objNode.Attributes["DepositType"].Value;

                    objDataTable.Rows.Add(objRow);                 
                }

                //Adding a blank row always so that column headers are visible
                //even if no row is there
                objRow = objDataTable.NewRow();
                objRow["DepositId"] = "";
                objRow["ControlNo"] = "";
                objRow["TransDate"] = "";
                objRow["Amount"] = "";
                objRow["DepositType"] = "";

                objDataTable.Rows.Add(objRow);                 
                
                Deposits.DataSource = objDataTable;
                Deposits.DataBind();
                

                ErrorControl1.errorDom = sCWSresponse;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        /// <summary>
        /// Delete Selected Deopsits
        /// </summary>        
        protected void DeleteSelectedDeposits()
        {            
            int despositID;
            selecteddepositids.Text = "";
            
            foreach (GridViewRow row in Deposits.Rows)
            {
                // Access the CheckBox
                CheckBox cb = (CheckBox)row.FindControl("checkedrow");
                if (cb != null && cb.Checked)
                {         
                    // First, get the ProductID for the selected row
                    despositID =
                        Convert.ToInt32(Deposits.DataKeys[row.RowIndex].Value);

                    if (selecteddepositids.Text == "")
                        selecteddepositids.Text += despositID;
                    else
                        selecteddepositids.Text = selecteddepositids.Text + "," + despositID;                    
                }
            }            
        }


        
        private XElement GetMessageTemplate()
        {       
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document>");
                sXml = sXml.Append("<Deposits>");

                if (ouraction.Text == "OK")
                {
                    sXml = sXml.Append("<DepositIds>");
                    sXml = sXml.Append(selecteddepositids.Text);
                    sXml = sXml.Append("</DepositIds>");
                }
                else
                {
                    sXml = sXml.Append("<AccountId>");
                    sXml = sXml.Append(accountid.Text);
                    sXml = sXml.Append("</AccountId>");
                    sXml = sXml.Append("<SubAccountId>");
                    sXml = sXml.Append(subaccountid.Text);
                    sXml = sXml.Append("</SubAccountId>");
                    sXml = sXml.Append("<Action>");
                    sXml = sXml.Append(ouraction.Text);
                    sXml = sXml.Append("</Action>");
                    sXml = sXml.Append("<SelectedDepositIds>");
                    sXml = sXml.Append(accountid.Text);
                    sXml = sXml.Append("</SelectedDepositIds>");
                }
                    sXml = sXml.Append("</Deposits>");
                    sXml = sXml.Append("</Document></Message>");
                

                XElement oTemplate = XElement.Parse(sXml.ToString());
                return oTemplate;
            }


        //Setting visible false for the last row of deposits
        protected void Deposits_DataBound(object sender, EventArgs e)
        {
            Deposits.Rows[Deposits.Rows.Count - 1].Visible = false;
        }

        protected void Deposits_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbltransdate = (Label)e.Row.FindControl("lblTransactionDate");
                if (lbltransdate.Text != String.Empty)
                {
                    lbltransdate.Text = AppHelper.GetDate(lbltransdate.Text);
                }
            }
        }      		
    }
}
