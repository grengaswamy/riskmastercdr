﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EDIHistory.aspx.cs" Inherits="Riskmaster.UI.EDIHistory.EDIHistory" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EDI History</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="80%">
                    <div style="width: 100%; height: 250px; overflow: auto;">
                    <%-- **ksahu5-ML-MITS34121 Start**--%>
                           <%-- <asp:GridView ID="gvEDIHistory" runat="server"  AutoGenerateColumns="False" 
                            Font-Names="Tahoma" Font-Size="Smaller" ondatabound="gvEDIHistory_DataBound"
                            GridLines="Vertical" CssClass="singleborder" AlternatingRowStyle-CssClass="datatd" RowStyle-CssClass="datatd1" HeaderStyle-CssClass="msgheader" CellPadding="-1" CellSpacing="1">
                            <Columns>
                                <asp:BoundField DataField="ClaimNumber" HeaderText="Claim Number"/>
                                <asp:BoundField DataField="FilingDate" HeaderText="Filing Date"/>
                                <asp:BoundField DataField="FilingType" HeaderText="Filing Type"/>
                                <asp:BoundField DataField="FilingMTC" HeaderText="Maintenance Type Code"/>
                                <asp:BoundField DataField="MTCCorrection" HeaderText="MTC Correction Code"/>
                                <asp:BoundField DataField="MTCCorrectDate" HeaderText="MTC Correction Date"/>
                                <asp:BoundField DataField="Ak1Date" HeaderText="Acknowledge Date"/>
                                <asp:BoundField DataField="FilingStatus" HeaderText="Filing Status"/>
                                <asp:BoundField DataField="LateReason" HeaderText="Late Reason"/>
                                <asp:BoundField DataField="AddedByUser" HeaderText="User Id"/>
                                <asp:BoundField DataField="SuspensionEffDate" HeaderText="Suspension Effective Date"/>
                                <asp:BoundField DataField="SuspensionNarr" HeaderText="Suspension Narrative"/>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:GridView>--%>

                         <asp:GridView ID="gvEDIHistory" runat="server"  AutoGenerateColumns="False" 
                            Font-Names="Tahoma" Font-Size="Smaller" ondatabound="gvEDIHistory_DataBound"
                            GridLines="Vertical" CssClass="singleborder" AlternatingRowStyle-CssClass="datatd" RowStyle-CssClass="datatd1" HeaderStyle-CssClass="msgheader" CellPadding="-1" CellSpacing="1">
                            <Columns>
                                <asp:BoundField DataField="ClaimNumber" HeaderText="<%$ Resources:gvHdrClaimNumber%>"/>
                                <asp:BoundField DataField="FilingDate" HeaderText="<%$ Resources:gvHdrFilingDate%>"/>
                                <asp:BoundField DataField="FilingType" HeaderText="<%$ Resources:gvHdrFilingType%>"/>
                                <asp:BoundField DataField="FilingMTC" HeaderText="<%$ Resources:gvHdrFilingMTC%>"/>
                                <asp:BoundField DataField="MTCCorrection" HeaderText="<%$ Resources:gvHdrMTCCorrection%>"/>
                                <asp:BoundField DataField="MTCCorrectDate" HeaderText="<%$ Resources:gvHdrMTCCorrectDate%>"/>
                                <asp:BoundField DataField="Ak1Date" HeaderText="<%$ Resources:gvHdrAk1Date%>"/>
                                <asp:BoundField DataField="FilingStatus" HeaderText="<%$ Resources:gvHdrFilingStatus%>"/>
                                <asp:BoundField DataField="LateReason" HeaderText="<%$ Resources:gvHdrLateReason%>"/>
                                <asp:BoundField DataField="AddedByUser" HeaderText="<%$ Resources:gvHdrAddedByUser%>"/>
                                <asp:BoundField DataField="SuspensionEffDate" HeaderText="<%$ Resources:gvHdrSuspensionEffDate%>"/>
                                <asp:BoundField DataField="SuspensionNarr" HeaderText="<%$ Resources:gvHdrSuspensionNarr%>"/>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:GridView>
                        <%-- **ksahu5-ML-MITS34121 End**--%>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
