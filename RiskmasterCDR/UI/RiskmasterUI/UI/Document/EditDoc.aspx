<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditDoc.aspx.cs" Inherits="Riskmaster.UI.Document.EditDoc" EnableViewStateMac="false" %>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>

<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Riskmaster</title>
    <script  type="text/javascript" language="javascript" src='/RiskmasterUI/Scripts/EditDoc.js'></script>
    <uc4:CommonTasks  ID="CommonTasks1" runat="server" />   
</head> 

<body  onload="InitPageSettingsOnBodyLoad();" onunload="DisposeAll()">
    <form id="frmData"  method="post" runat="server"> 
        <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"  CustomMessage="<%$ Resources:PleaseWaitDialog1Resrc %>"/>
        <p>
        <table class="singleborder" align="center">
            <tr>
                <td colspan="2">
                    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="ctrlgroup">
                    <asp:Label ID="lblEditDocumentProperties" runat="server" Text="<%$ Resources:lblEditDocumentPropertiesResrc %>" />
                </td>
            </tr>
            <tr>
                <td nowrap="" class="datatd">
                    <b><u><asp:Label ID="lblDocumentFolder" runat="server" Text="<%$ Resources:lblDocumentFolderResrc %>" /></u></b>
                </td>
                <td class="datatd">
                    <b><u><asp:Label ID="lblFolderName" runat="server"></asp:Label></u></b>     
                </td>
            </tr>     
            <tr>
                <td class="datatd" nowrap="">
                    <b><u><asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitleResrc %>" /></u></b>
                </td>
                <td class="datatd">
                    <input type="text" onchange="setDataChanged(true);" id="Title" runat="server" size="50" maxlength="100" style="border: 1px solid #104A7B;" />
                </td>
            </tr>
            <tr>
                <td class="datatd" nowrap="">
                    <b><asp:Label ID="lblSubject" runat="server" Text="<%$ Resources:lblSubjectResrc %>" /></b>
                </td>
                <td class="datatd">
                    <input type="text" id="Subject" onchange="setDataChanged(true);" runat="server"  size="50" maxlength="50" style="border: 1px solid #104A7B;" />
                </td>
            </tr>
            <tr>
                <td class="datatd" nowrap="">
                    <b><asp:Label ID="lblType" runat="server" Text="<%$ Resources:lblTypeResrc %>" /></b>
                </td>
                <td class="datatd">
                    <input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);setDataChanged(true);" name="documenttypecode" id="documenttypecode" cancelledvalue="" />
                    <input type="button" name="documenttypecodebtn"  class="CodeLookupControl" onchange="setDataChanged(true);" id="documenttypecodebtn" onclick="return selectCode('DOCUMENT_TYPE','documenttypecode')" />
                    <input type="hidden" name="documenttypecode_cid"  runat="server"  id="documenttypecode_cid"  />                
                </td>
            </tr>
            <tr>
                <td class="datatd" nowrap="">
                    <b><asp:Label ID="lblClass" runat="server" Text="<%$ Resources:lblClassResrc %>" /></b>
                </td>
                <td class="datatd">
                    <input type="text" size="30"  runat="server" onblur="codeLostFocus(this.id);"   onchange="lookupTextChanged(this);setDataChanged(true);"  id="documentclasscode" cancelledvalue="" />
                    <input type="button" name="documentclasscodebtn"   class="CodeLookupControl"  id="documentclasscodebtn" onclick="return selectCode('DOCUMENT_CLASS','documentclasscode')" />
                    <input type="hidden" name="documentclasscode_cid"    runat="server"  id="documentclasscode_cid"  />              
                </td>      
            </tr>
            <tr>
                <td class="datatd" nowrap="">
                    <b><asp:Label ID="lblCategory" runat="server" Text="<%$ Resources:lblCategoryResrc %>" /></b>
                </td>
                <td class="datatd"> 
                    <input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);setDataChanged(true);" name="documentcategorycode" id="documentcategorycode" cancelledvalue="" />
                    <input type="button" name="documentcategorycodebtn"   onchange="setDataChanged(true);" class="CodeLookupControl"  id="documentcategorycodebtn" onclick="return selectCode('DOCUMENT_CATEGORY','documentcategorycode')" />
                    <input type="hidden" runat="server"  name="documentcategorycode_cid"  id="documentcategorycode_cid"  />
                </td>
            </tr>
            <tr>
                <td class="datatd" nowrap="">
                    <b><asp:Label ID="lblKeywords" runat="server" Text="<%$ Resources:lblKeywordsResrc %>" /></b>
                </td>
                <td class="datatd">
                    <input onchange="setDataChanged(true);" type="text" runat="server" id="Keywords" value="" size="50" maxlength="200" style="border: 1px solid #104A7B;" />
                </td>
            </tr>
            <tr>
                <td class="datatd" nowrap="">
                    <b><asp:Label ID="lblNotes" runat="server" Text="<%$ Resources:lblNotesResrc %>" /></b>
                </td>
                <td class="datatd">
                    <textarea onchange="setDataChanged(true);" cols="38" runat="server" wrap="soft" id="Notes" rows="6" size="" style="border: 1px solid #104A7B;"></textarea>
                </td>
            </tr>
            <tr>
                <td class="datatd">
                    <b><asp:Label ID="lblFileName" runat="server" Text="<%$ Resources:lblFileNameResrc %>" /></b>
                </td>
                <%if (IfOfficeDocumentOrTextType())
                { %>
                    <td><a href="#" onclick="return StartEditor('',true);"><%= hdnFileName.Value %></a></td>
                <%} %>
                <%else{ %>
                    <td><input type="text" runat="server" id="FileName" readonly="readonly" size="50" style="border: 0px solid #104A7B;" /></td>
                <%} %>
            </tr>
     
            <tr>
                <td nowrap="" class="datatd">
                    <b><asp:Label ID="lblCreatedOn" runat="server" Text="<%$ Resources:lblCreatedOnResrc %>" /></b>
                </td>
                <td class="datatd">
                    <input type="text" runat="server" readonly="readonly" id="CreateDate"  size="17" style="border: 0px solid #104A7B;" />
                </td>
            </tr>    
            <tr>      
                <td  colspan="2" align="center">
                    <asp:Button ID="btnSave" runat="server"  Text="<%$ Resources:btnSaveResrc %>" CssClass="button" OnClick ="btnSave_Click" OnClientClick="setDataChanged(false);return CheckFinishToCall();"/>&nbsp;
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass="button"  OnClick="btnCancel_Click" OnClientClick="return CheckCloseToCall();"/>
                </td>
            </tr>
            <%if (IfOfficeDocumentOrTextType())
            { %>
            <tr>   
                <td valign="top" colspan="2">
                    <asp:Label ID="lblInsrtuctions" runat="server" Text="<%$ Resources:lblInsrtuctionsResrc %>" />
                </td>
            </tr>
            <%} %>
            <asp:HiddenField ID="Regarding" runat="server" Value=""/>
            <asp:HiddenField ID="hdnFileName" runat="server" Value=""/>
            <asp:HiddenField ID="FolderId" runat="server" />
            <asp:HiddenField ID="hdocId" runat="server" />
            <!--  MITS 23094 skhare7-->
            <asp:HiddenField ID="hdocIdListids" runat="server" />
            <asp:HiddenField ID="Psid" runat="server" />
            <asp:HiddenField ID="flag" runat="server" />
            <input type="hidden" id="hdnTempFileName" runat=server />
            <asp:HiddenField ID="hdnPageNumber" runat="server" />
            <asp:HiddenField ID="hdnSortExpression" runat="server" />
            <asp:HiddenField ID="AttachTable" runat="server" />
            <asp:HiddenField ID="FormName" runat="server" />
            <asp:HiddenField ID="AttachRecordId" runat="server" />
            <asp:HiddenField ID="FolderName" runat="server" />
            <asp:HiddenField ID="AttachTableName" runat="server" />
            <asp:HiddenField ID="officeDocumentType" runat="server" />
            <input type="hidden" id="hdnContent" runat=server />
  
            <input type="hidden" id="hdnRTFContent" runat="server" />
            <input type="hidden" id="IsDocOpening" />
            <asp:HiddenField ID="NonMCMFormName" runat="server" />
        </table>
    <div   id = "divBackground" style=" position:absolute; top:0px; left:0px;background-color:black; z-index:100;opacity: 0.8;filter:alpha(opacity=60); -moz-opacity: 0.8; overflow:hidden; display:none"> 

</div> 

    </p>
    </form>
    
   </body>
   </html> 
