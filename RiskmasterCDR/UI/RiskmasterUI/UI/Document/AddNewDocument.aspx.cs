﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Riskmaster.UI.DocumentService;
using System.Web.UI.WebControls;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;

namespace Riskmaster.UI.DocumentManagement
{
    public partial class AddNewDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Rakhel ML Changes - start
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("AddNewDocument.aspx"), "AddNewDocumentValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "AddNewDocumentValidations", sValidationResources, true);
            //Rakhel ML Changes - end
            ErrorHelper.IsRMXSessionExists();
            if (!Page.IsPostBack)
            {
                SetInitialValues();
                FolderId.Value = AppHelper.GetQueryStringValue("folderid");
                Psid.Value = AppHelper.GetQueryStringValue("psid");
            }
        }
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            Psid.Value = AppHelper.GetValue("Psid");
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
        }
        protected void SaveData_Click(object sender, EventArgs e)
        {
            DocumentBusinessHelper dc = null;
            try
            {
                dc = new DocumentBusinessHelper();
                dc.Add();
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom  = ErrorHelper.formatUIErrorXML(err);

            }
            Server.Transfer("DocumentList.aspx");
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("DocumentList.aspx");
        }
    }
}
