﻿using System;
//using Riskmaster.UI.DocumentService;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;

namespace Riskmaster.UI.Document
{
    public partial class EditDoc : System.Web.UI.Page
    {
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            Psid.Value = AppHelper.GetValue("Psid");
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
            NonMCMFormName.Value = AppHelper.GetValue("NonMCMFormName");
            hdocId.Value = AppHelper.GetValue("hdocId");
        }
        private void BindDataValues(DocumentType ViewData)
        {
            lblFolderName.Text = AppHelper.GetFormValue("FolderName");
            Title.Value = ViewData.Title;
            Subject.Value = ViewData.Subject;
            CreateDate.Value =  AppHelper.GetDate(ViewData.CreateDate);
            documenttypecode.Value = ViewData.Type.Desc;
            documenttypecode_cid.Value = ViewData.Type.Id.ToString();
            documentclasscode.Value = ViewData.Class.Desc;
            documentclasscode_cid.Value = ViewData.Class.Id.ToString();
            documentcategorycode.Value = ViewData.Category.Desc;
            documentcategorycode_cid.Value = ViewData.Category.Id.ToString();
            Keywords.Value = ViewData.Keywords;
            Notes.Value = ViewData.Notes;
            FileName.Value = ViewData.FileName;
            hdnFileName.Value = ViewData.FileName;

        }
        private void SetOfficeDocumentType()
        {
            string sExtension = "";
            if (AppHelper.GetFormValue("FileName") != "")
            {
                string[] arrParseFileName = AppHelper.GetFormValue("FileName").Split(".".ToCharArray()[0]);
                if (arrParseFileName.Length > 1)
                {
                    int iExt=(arrParseFileName.Length-1);
                    sExtension = arrParseFileName[iExt];
                }
            }
            if (sExtension.ToLower().Equals("doc") || sExtension.ToLower().Equals("docx") || sExtension.ToLower().Equals("txt"))
            {
                officeDocumentType.Value = "word";
            }
            if (sExtension.ToLower().Equals("xls") || sExtension.ToLower().Equals("xlsx") || sExtension.ToLower().Equals("csv"))
            {
                officeDocumentType.Value = "xls";
            }
            if (sExtension.ToLower().Equals("ppt") || sExtension.ToLower().Equals("pptx"))
            {
                officeDocumentType.Value = "ppt";
            }
        }
        private void GetDocument()
        {
            DocumentType ViewData = null;
            DocumentBusinessHelper dc = new DocumentBusinessHelper();
            try
            {
                if (hdocId.Value != "")
                {

                    ViewData = dc.Download(Conversion.ConvertStrToInteger(hdocId.Value));
                    hdnContent.Value = Convert.ToBase64String(ViewData.FileContents);
                    hdnTempFileName.Value = hdnFileName.Value;

                }

            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("EditDoc.aspx"), "EditDocValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "EditDocValidationScripts", sValidationResources, true);

            if (!Page.IsPostBack)
            {
                SetInitialValues();
                DocumentBusinessHelper dc = new DocumentBusinessHelper();
                try
                {
                    AttachTable.Value = AppHelper.GetValue("AttachTable");
                    FormName.Value = AppHelper.GetValue("FormName");
                    AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
                    string shdocId = AppHelper.GetFormValue("hdocId");
                        //skhare7:MITS 23094
                    string shdocListId = AppHelper.GetFormValue("hdocIdList");
                    FolderId.Value = AppHelper.GetFormValue("FolderId").ToString();
                    hdocId.Value = shdocId;
                    hdocIdListids.Value = shdocListId;
                         //skhare7:MITS 23094
                    
                    Psid.Value = AppHelper.GetFormValue("Psid").ToString();
                    flag.Value = AppHelper.GetFormValue("flag").ToString();
                    hdnPageNumber.Value = AppHelper.GetFormValue("hdnPageNumber").ToString();
                    hdnSortExpression.Value = AppHelper.GetFormValue("hdnSortExpression").ToString();
                    FolderName.Value = AppHelper.GetFormValue("FolderName");
                    DocumentType ViewData = dc.Edit(Conversion.ConvertStrToInteger(hdocId.Value), flag.Value, Psid.Value, FormName.Value,0);
                    BindDataValues(ViewData);
                    SetOfficeDocumentType();
                    GetDocument();
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
        }
        public bool IfOfficeDocumentOrTextType()
        {
            string sExtension = "";
            bool bIsOfficeDocumentOrText = false;
            if (FileName.Value != "")
            {
                string[] arrParseFileName = FileName.Value.Split(".".ToCharArray()[0]);
                if (arrParseFileName.Length > 1)
                {
                    int iExt = (arrParseFileName.Length - 1);
                    sExtension = arrParseFileName[iExt];
                }
            }
            if (sExtension.ToLower().Equals("doc") || sExtension.ToLower().Equals("docx"))
            {
                bIsOfficeDocumentOrText = true;
            }
            if (sExtension.ToLower().Equals("xls") || sExtension.ToLower().Equals("xlsx") || sExtension.ToLower().Equals("csv"))
            {
                bIsOfficeDocumentOrText = true;
            }
            if (sExtension.ToLower().Equals("ppt") || sExtension.ToLower().Equals("pptx"))
            {
                bIsOfficeDocumentOrText = true;
            }
            if (sExtension.ToLower().Equals("txt"))
            {
                bIsOfficeDocumentOrText = true;
            }
            return bIsOfficeDocumentOrText;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DocumentBusinessHelper dc = new DocumentBusinessHelper();
            bool isError = false;
            try
            {
                dc.EditDocument(Conversion.ConvertStrToInteger(hdocId.Value));
                DocumentType dtype = new DocumentType();
                if (hdnRTFContent.Value.Trim() == "")
                {
                    dtype.FileContents = Convert.FromBase64String(hdnContent.Value);
                }
                else
                {
                    dtype.FileContents = Convert.FromBase64String(hdnRTFContent.Value);
                }
                dtype.DocumentId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("hdocId"));
                dtype.FileName = hdnTempFileName.Value;
                dtype.PsId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("Psid"));
                dtype.ScreenFlag = AppHelper.GetFormValue("flag");
                dc.UpdateDocumentFile(dtype);
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                isError = true;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                isError = true;

            }
            if (!isError)
                Server.Transfer("DisplayDocument.aspx?id="+ hdocId.Value, true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("DisplayDocument.aspx?id=" + hdocId.Value, true);
        }
      
    }
}
