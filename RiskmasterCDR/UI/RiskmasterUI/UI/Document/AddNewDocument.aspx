<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNewDocument.aspx.cs" Inherits="Riskmaster.UI.DocumentManagement.AddNewDocument"  EnableViewStateMac="false"%>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" Tagname="ErrorControl" tagprefix="uc3" %>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Riskmaster</title>
<uc4:CommonTasks ID="CommonTasks1" runat="server" />
      <script type="text/javascript" language="JavaScript">
		function SaveForm()
		{
		    if (ValidateForm())
		    {
			        document.forms[0].action='';
			        document.forms[0].method="post";
			        document.forms[0].submit();
			        //This is all we need to do to Show Please Wait Box.......
			        pleaseWait.Show();			        
			        return true;
	        }	
	        
	        return false;        
		}
		function CancelForm()
		{
          				    
		}
		function ValidateForm()
		{
			if(document.forms[0].FileContents.value=="" || document.forms[0].Title.value=="")
			{
			    //alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
			    alert(AddNewDocumentValidations.ValidRequiredFields);
				return false;
			}
			return true;
		}
      </script>
      </head>
      <body>
        <form  id="frmData" runat="server">
        <p align="center">
            
        <table class="singleborder">
        <tr>
      <td colspan="2">
          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
             </td>
     </tr>
       
     <tr>
      <td colspan="2" class="ctrlgroup">
          <asp:Label ID="lblAddNewDocumentResrc" runat="server" Text="<%$ Resources:lblAddNewDocumentResrc %>"></asp:Label>
         </td>
     </tr>
     <tr>
      <td colspan="2" class="errortext"></td>
     </tr>
     <tr>
      <td class="required" nowrap="">
          <asp:Label ID="lblDocumentFolderResrc" runat="server" Text="<%$ Resources:lblDocumentFolderResrc %>"></asp:Label></td>
      <td class="required"></td>
     </tr>
     <tr>
      <td class="datatd" nowrap=""><b><u>
          <asp:Label ID="lblTitleResrc" runat="server" Text="<%$ Resources:lblTitleResrc %>"></asp:Label></u></b></td>
      <td class="datatd"><input type="text" id="Title" runat="server" size="32" maxlength="32" style="border: 1px solid #104A7B;" ></td>
     </tr>
     <tr>
      <td class="datatd" nowrap=""><b>
          <asp:Label ID="lblSubjectResrc" runat="server" Text="<%$ Resources:lblSubjectResrc %>"></asp:Label></b></td>
      <td class="datatd"><input type="text" id="Subject" runat="server"  size="50" maxlength="50" style="border: 1px solid #104A7B;"></td>
     </tr>
     <tr>
      <td class="datatd" nowrap=""><b>
          <asp:Label ID="lblTypeResrc" runat="server" Text="<%$ Resources:lblTypeResrc %>"></asp:Label></b></td>
        <td class="datatd">
                
            <input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);" name="documenttypecode" id="documenttypecode" cancelledvalue=""></input>
<input type="button" name="documenttypecodebtn"  class="CodeLookupControl"  id="documenttypecodebtn" onclick="return selectCode('DOCUMENT_TYPE','documenttypecode')" />

<input type="hidden" name="documenttypecode_cid"  runat="server"  id="documenttypecode_cid"  />
                
         </td>
      </tr>
     <tr>
      <td class="datatd" nowrap=""><b>
          <asp:Label ID="lblClassResrc" runat="server" Text="<%$ Resources:lblClassResrc %>"></asp:Label></b></td>
      <td class="datatd"><input type="text" size="30"  runat="server" onblur="codeLostFocus(this.id);"   onchange="lookupTextChanged(this);"  id="documentclasscode" cancelledvalue=""></input>
<input type="button" name="documentclasscodebtn"    class="CodeLookupControl"  id="documentclasscodebtn" onclick="return selectCode('DOCUMENT_CLASS','documentclasscode')" />

<input type="hidden" name="documentclasscode_cid"    runat="server"  id="documentclasscode_cid"  />
              
                        </td>
      
     </tr>
     <tr>
      <td class="datatd" nowrap=""><b>
          <asp:Label ID="lblCategoryResrc" runat="server" Text="<%$ Resources:lblCategoryResrc %>"></asp:Label></b></td>
         <td class="datatd"> <input type="text" size="30" runat="server"  onblur="codeLostFocus(this.id);"  onchange="lookupTextChanged(this);" name="documentcategorycode" id="documentcategorycode" cancelledvalue=""></input>
<input type="button" name="documentcategorycodebtn"  class="CodeLookupControl"  id="documentcategorycodebtn" onclick="return selectCode('DOCUMENT_CATEGORY','documentcategorycode')" />

<input type="hidden" runat="server"  name="documentcategorycode_cid"  id="documentcategorycode_cid"  /></td>
     </tr>
      <td class="datatd" nowrap=""><b>
          <asp:Label ID="lblKeyWordsResrc" runat="server" Text="<%$ Resources:lblKeyWordsResrc %>"></asp:Label></b></td>
      
      <td class="datatd"><input type="text" runat="server" id="Keywords" value="" size="50" maxlength="200" style="border: 1px solid #104A7B;"></td>
     </tr>
     <tr>
      <td class="datatd" nowrap=""><b>
          <asp:Label ID="lblNotesResrc" runat="server" Text="<%$ Resources:lblNotesResrc %>"></asp:Label></b></td>
      <td class="datatd"><textarea cols="38" runat="server" wrap="soft" id="Notes" rows="6" size="" style="border: 1px solid #104A7B;"></textarea></td>
     </tr>
     <tr>
      <td class="datatd"><b><u>
          <asp:Label ID="lblFileName" runat="server" Text="<%$ Resources:lblNotesResrc %>"></asp:Label></u></b></td>
      <td><input type="file" id="FileContents"   runat="server"></td>
     </tr>
     <tr>
      <td colspan="2" align="center">
          <asp:Button ID="Button1" runat="server" Text="<%$ Resources:btnButton1Resrc %>" CssClass="button" 
               onclick="SaveData_Click" UseSubmitBehavior="true"  OnClientClick="return SaveForm()" />
          &nbsp;
          <asp:Button ID="Button2" runat="server" Text="<%$ Resources:btnButton2Resrc %>" onclick="Cancel_Click" CssClass="button" />
      </td>
     </tr>
     <tr>
        <td>
            <asp:HiddenField ID="Psid" runat="server" />
            <asp:HiddenField ID="FolderId" runat="server" />
             <asp:HiddenField ID="Regarding" runat="server" Value="Files"/>
     <asp:HiddenField ID="AttachTableName" runat="server" />
     <asp:HiddenField ID="AttachRecordId" runat="server" />
      <asp:HiddenField ID="FormName" runat="server"/>
      <asp:HiddenField ID="flag" runat="server"/>
        
         </td>
     </tr>
  <tr>
  <td>
  
  </td>
  </tr>
           
    </table>
     </p>
     <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"  CustomMessage=""/>
    
      </form>
</body>
</html>
 