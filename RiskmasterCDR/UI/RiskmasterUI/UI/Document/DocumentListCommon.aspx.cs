﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Riskmaster.UI.DocumentService;
using Riskmaster.BusinessHelpers;
using Riskmaster.Models;

namespace Riskmaster.UI.UI.Document
{
    public partial class DocumentListCommon : System.Web.UI.Page
    {
        string sRegarding = string.Empty;
        string sAttachTableName = string.Empty;
        string sAttachRecordId = string.Empty;
        string sFormName = string.Empty;
        string sPsid = string.Empty;
        string sflag = string.Empty;
        string sNonMCMFormName = string.Empty;
        string sformsubtitle = string.Empty;// mmudabbir 04/24/2013 -mits 32260
        DocumentVendorDetails oResponse = null;

        //rbhatia4:R8: Use Media View Setting : July 07 2011
        public enum DOCUMENT_MANAGEMENT_VENDOR_NAME
        {
            RiskmasterX ,
            PaperVision ,
            MediaView ,
            Acrosoft 
        }
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            DocumentBusinessHelper docBusHelper = new DocumentBusinessHelper();
            DOCUMENT_MANAGEMENT_VENDOR_NAME enumDocumentVendorName = DOCUMENT_MANAGEMENT_VENDOR_NAME.RiskmasterX;
            

            if (!Page.IsPostBack)
            {
                SetInitialValues();
                oResponse = new DocumentVendorDetails();
                if (Request.QueryString["OpenMediaView"] != null && Request.QueryString["OpenMediaView"].ToString() == "true")
                {
                    oResponse = docBusHelper.GetDocumentManagementVendorDetails(true);
                }
                else
                {
                    oResponse = docBusHelper.GetDocumentManagementVendorDetails(false);
                }

                //rbhatia4:R8: Use Media View Setting : July 08 2011
                //Restructuring Document Management Code for choosing the selected vendor
                enumDocumentVendorName = (DOCUMENT_MANAGEMENT_VENDOR_NAME)Enum.Parse(typeof(DOCUMENT_MANAGEMENT_VENDOR_NAME), oResponse.DocumentManagementVendorName);   
                
                switch (enumDocumentVendorName)
                {
                    case DOCUMENT_MANAGEMENT_VENDOR_NAME.RiskmasterX :
                        Server.Transfer("/RiskmasterUI/UI/Document/DocumentList.aspx?AttachTableName=" + sAttachTableName
                        + "&AttachRecordId=" + sAttachRecordId + "&FormName=" + sFormName + "&psid=" + sPsid + "&Regarding=" + sRegarding + "&NonMCMFormName=" + sNonMCMFormName +"&formsubtitle=" + sformsubtitle + "&flag=" + sflag, true);// mmudabbir 04/24/2013 -mits 32260 ,passed one more argument to display claimant information
                        break;
                    case DOCUMENT_MANAGEMENT_VENDOR_NAME.PaperVision :
                        Server.Transfer("/RiskmasterUI/UI/PaperVision/PVDocumentList.aspx?AttachTableName=" + sAttachTableName
                        + "&AttachRecordId=" + sAttachRecordId + "&FormName=" + sFormName + "&psid=" + sPsid + "&Regarding=" + sRegarding + "&NonMCMFormName=" + sNonMCMFormName + "&flag=" + sflag, true);
                        break;
                    case DOCUMENT_MANAGEMENT_VENDOR_NAME.Acrosoft:
                        Server.Transfer("/RiskmasterUI/UI/Document/DocumentList.aspx?AttachTableName=" + sAttachTableName
                        + "&AttachRecordId=" + sAttachRecordId + "&FormName=" + sFormName + "&psid=" + sPsid + "&Regarding=" + sRegarding + "&NonMCMFormName=" + sNonMCMFormName + "&flag=" + sflag, true);
                        break;
                    case DOCUMENT_MANAGEMENT_VENDOR_NAME.MediaView:
                        TransferToMediaView();
                        break;
                    default:
                        Server.Transfer("/RiskmasterUI/UI/Document/DocumentList.aspx?AttachTableName=" + sAttachTableName
                        + "&AttachRecordId=" + sAttachRecordId + "&FormName=" + sFormName + "&psid=" + sPsid + "&Regarding=" + sRegarding + "&NonMCMFormName=" + sNonMCMFormName + "&flag=" + sflag, true);
                        break;
                }
                
                /*
                if (sIsPaperVisionOn == "-1")
                {
                    Server.Transfer("/RiskmasterUI/UI/PaperVision/PVDocumentList.aspx?AttachTableName=" + sAttachTableName
                        + "&AttachRecordId=" + sAttachRecordId + "&FormName=" + sFormName + "&psid=" + sPsid + "&Regarding=" + sRegarding + "&NonMCMFormName=" + sNonMCMFormName + "&flag=" + sflag, true);
              
                }
                else
                {
                    Server.Transfer("/RiskmasterUI/UI/Document/DocumentList.aspx?AttachTableName=" + sAttachTableName
                        + "&AttachRecordId=" + sAttachRecordId + "&FormName=" + sFormName + "&psid=" + sPsid + "&Regarding=" + sRegarding + "&NonMCMFormName=" + sNonMCMFormName + "&flag=" + sflag, true);
                }
                */
            }

        }
        private void SetInitialValues()
        {
            sRegarding = AppHelper.GetValue("Regarding");
            sAttachTableName = AppHelper.GetValue("AttachTableName");
            sAttachRecordId = AppHelper.GetValue("AttachRecordId");
            sFormName = AppHelper.GetValue("FormName");
            sformsubtitle = AppHelper.GetValue("formsubtitle");//mmudabbir 04/24/2013 -mits  32260 getting claimant info from httpcontext
            // pen testing changes:atavaragiri MITS 27871
            //sPsid = AppHelper.GetValue("Psid");
            sPsid = AppHelper.HTMLCustomEncode(AppHelper.GetValue("Psid"));
            //END:pen testing changes
            sflag = AppHelper.GetValue("flag");
            sNonMCMFormName = AppHelper.GetValue("NonMCMFormName");

            //MITS 20965 skhare7 MITS 23094
            if (Request.Form["hdocIdList"] != null)
            {
                if (Request.Form["hdocIdDeleteLIst"] != null)
                    hdocIdListids.Value = Request.Form["hdocIdDeleteLIst"].ToString();
                else
                    hdocIdListids.Value = Request.Form["hdocIdList"].ToString();


            }
            if (Request.Form["hdocIdDeleteLIst"] != null)
                hdocIdListids.Value = Request.Form["hdocIdDeleteLIst"].ToString();
            //MITS 20965 skhare7 End 
        }

        private void TransferToMediaView()
        {

            string sUserList = String.Empty;
            string sUrl = String.Empty;
            MMInterfaceNet.Int IntObj = new MMInterfaceNet.Int();
            IntObj.SetSeed("");
            IntObj.AddItem("Source", oResponse.MediaViewDetails.Source);
            IntObj.AddItem("Level", oResponse.MediaViewDetails.Level);
            IntObj.AddItem("Enterprise", oResponse.MediaViewDetails.Enterprise);
            IntObj.AddItem("UserType", oResponse.MediaViewDetails.UserType);
            IntObj.AddItem("UserId", oResponse.MediaViewDetails.CommonUser);
            IntObj.AddItem("MMDocNum", oResponse.AssociatedRecordInfo.AssociatedRecordNumber);
            IntObj.AddItem("MMAgent", oResponse.MediaViewDetails.MMAgent);
            IntObj.AddItem("MMAgency", oResponse.MediaViewDetails.MMAgency);
            IntObj.AddItem("MMGroupId", ConvertToCamelCase(oResponse.AssociatedRecordInfo.AssociatedRecordType));
        
            sUserList = IntObj.GetUserList();
    
            // Modify the below Url as needed.
    
            sUrl = oResponse.MediaViewDetails.MediaViewUrl + "?userlist=" + sUserList;
            //Deb:need to close MDI screen loaded
            //Response.Redirect(sUrl);
            if(!string.IsNullOrEmpty(sUrl))
            {
                strUrl.Value = sUrl;
            }
            //Deb:need to close MDI screen loaded
           // Response.Write("AssociatedRecordType:" + oResponse.AssociatedRecordInfo.AssociatedRecordType + "  AssociatedRecordNumber:" + oResponse.AssociatedRecordInfo.AssociatedRecordNumber);
  
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sInput"></param>
        /// <returns></returns>
        private string ConvertToCamelCase(string sInput)
        {
            if (!String.IsNullOrEmpty(sInput))
            {
                return (sInput.Substring(0, 1).ToUpper() + sInput.Substring(1).ToLower());
            }
            else
            {
                return String.Empty;
            }
        }
    }

    
}