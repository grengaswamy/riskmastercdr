﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisplayDocument.aspx.cs" Inherits="Riskmaster.UI.Document.DisplayDocument"  EnableViewStateMac ="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3"  %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>

<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
   <title>Riskmaster</title>
<uc4:CommonTasks ID="CommonTasks1" runat="server" />
<script language="JavaScript" src="../../Scripts/emaildocuments.js" type="text/javascript"></script>
<script language='javascript' type="text/javascript">

    function OnDocumentSubmit() {
        pleaseWait.Show();
    }

    function EditDocument() {
        var ret = false;
        var fileSize;
        var bContinueEdit = true;
        //changed by Nitin for handling Edit buttong for Large Document
        fileSize = document.getElementById('hdnDocumentSize');

        if (fileSize.value > 15728640) {
            //ret = confirm("Size of Document is Large so proceeding futher can create Error,Download is recommended for this Document instead of Editing .Click 'OK' to still continue futher and 'Cancel' for not proceeding.");
            ret = confirm(DisplayDocumentValidations.ValidFileSizeIssue);
            if (ret)
                bContinueEdit = true;
            else
                bContinueEdit = false;
        }

        if (bContinueEdit == true) 
        {
            OnDocumentSubmit();
            //document.forms[0].action = 'EditDoc.aspx';
            //asingh263 MITS 34137 starts
            var screen = 'DisplayDocument.aspx?redirectdoc=EditDoc.aspx';
            screen = screen + "&FileSize="+fileSize.value;
            //asingh263 MITS 34137 ends
             //rsushilaggar Pen Testing changes
            document.forms[0].action = screen;
            document.forms[0].method = "post";
            document.forms[0].submit();
        }
    }

    function CopyDoc() {
        OnDocumentSubmit();
        //var screen='CopyDocument.aspx';
        var screen = "DisplayDocument.aspx?redirectdoc=CopyDocument.aspx"; //rsushilaggar Pen Testing changes
        document.forms[0].action = screen;
        document.forms[0].method = "post";
        document.forms[0].submit();
    }
    function MoveDoc() {
        OnDocumentSubmit();
        //var screen='MoveDocuments.aspx';
        var screen = "DisplayDocument.aspx?redirectdoc=MoveDocuments.aspx"; //rsushilaggar Pen Testing changes
        document.forms[0].action = screen;
        document.forms[0].method = "post";
        document.forms[0].submit();
    }

    function DeleteDoc() {
        OnDocumentSubmit();
        var screen = "DisplayDocument.aspx?redirectdoc=ConfirmDelete.aspx"; //rsushilaggar Pen Testing changes
        document.forms[0].action = "ConfirmDelete.aspx";
        document.forms[0].method = "post";
        document.forms[0].submit();
    }

    function TransferDoc(type) {
        OnDocumentSubmit();
        var screen = "DisplayDocument.aspx?redirectdoc=TransferDocument.aspx"; //rsushilaggar Pen Testing changes
        //var screen='TransferDocument.aspx';
        document.forms[0].action = screen;
        document.forms[0].method = "post";
        document.forms[0].submit();
    }
    function EmailDoc(type) {
        OnDocumentSubmit();
        var screen = "DisplayDocument.aspx?redirectdoc=EmailDocuments.aspx"; //rsushilaggar Pen Testing changes
        //var screen='EmailDocuments.aspx';
        document.forms[0].action = screen;
        document.forms[0].method = "post";
        document.forms[0].submit();
    }

    function EmailUsingOutlook() {
        if (document.forms[0].hdnFileSize.value != null) {
            var totalfileSize = parseFloat(document.forms[0].hdnFileSize.value);
        }
        else {
            //alert("File size unknown");
            alert(DisplayDocumentValidations.ValidUnknownFile);
            return false;
        }
        var sizeLimitOutlook = parseInt(document.forms[0].hdnSizeLimitOutlook.value);
        //rsharma220 MITS 35261
        if (totalfileSize > (sizeLimitOutlook * 1024 * 1024)) {

            //alert("Total File Size should be less than " + document.forms[0].hdnSizeLimitOutlook.value + " MB");
            alert(DisplayDocumentValidations.ValidSizeRestriction1 + " " + document.forms[0].hdnSizeLimitOutlook.value + " " + DisplayDocumentValidations.ValidSizeRestriction2);

            return false;
        }
        if (confirm(DisplayDocumentValidations.ValidOutlookConfiguration)) {

        }
        else {

            return false;
        }
        //pleaseWait.Message = "Opening Outlook Window.........";
        pleaseWait.Message = DisplayDocumentValidations.ValidPleaseWait;
        pleaseWait.Show();
        document.getElementById('hdnEmailButtonClick').value = "1";
        document.forms[0].submit();
    }
    function CheckAndOpenOutlook() {
        if (document.getElementById('hdnEmailButtonClick').value == "1") {
            SaveFileAndOpenOutlook()
        }
        document.getElementById('hdnEmailButtonClick').value = "0";
    }
    function SaveFileAndOpenOutlook() {

        var fso;
        var strForm;
        var strFile;
        var template = (document.getElementById('hdnformfilecontent').value);
        fso = new ActiveXObject("Scripting.FileSystemObject");
        strFile = document.getElementById('FileName').value;
        strForm = fso.GetSpecialFolder(2).Path + "\\" + strFile;
        StreamAndSave(template, strForm)
        self.close();

    }

    function SendAttach(strAttachPath) {

        var theApp	//Reference to Outlook.Application 
        var theMailItem	//Outlook.mailItem
        //Attach Files to the email
        var attach1 = strAttachPath;

        //Construct the Email including To(address),subject,body

        if (document.forms[0].Regarding != null) {
            var subject = document.forms[0].Regarding.value;
        }

        //Create a object of Outlook.Application
        try {
            theApp = new ActiveXObject("Outlook.Application")

            theMailItem = theApp.CreateItem(0) // value 0 = MailItem
            //Bind the variables with the email

            theMailItem.Subject = (subject);
            theMailItem.Attachments.Add(attach1)

            //Show the mail 
            theMailItem.display()
        }
        catch (err) {
            /*
            alert("The following may have cause this error: \n" +
            "1. The Outlook  2007 is not installed on the machine.\n" +
            "2. The msoutl.olb is not availabe at the location " +
            "C:\\Program Files\\Microsoft Office\\OFFICE11\\msoutl.old on client's machine " +
            "due to bad installation of the office 2007." +
            "Re-Install office 2007 with default settings.\n" +
            "3. The Initialize and Scripts ActiveX controls not marked as safe is not set to enable.")
            */
            alert(DisplayDocumentValidations.ValidCommonError.split('\\n').join('\n'));

        }
    }

    function OpenPage(status) {
        __doPostBack("btnActDownload", status);
    }

</script>
</head>
<body onload="tryMDIScreenLoaded();CheckAndOpenOutlook()">
    <form id="frmData" runat="server">
        <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"  CustomMessage="<%$ Resources:PleaseWaitDialog1Resrc %>"/>
        <asp:scriptmanager id="ScriptManager1" runat="server" AsyncPostBackTimeout="120"/>
        <asp:updatepanel id="UpdatePanel1" runat="server">
            <contenttemplate>
                <asp:Timer ID="Timer1" runat="server" ontick="Timer1_Tick"  Enabled="false" Interval="60" > </asp:Timer>
                <asp:Timer ID="Timer2" runat="server" ontick="Timer2_Tick"  Enabled="false" Interval="60" > </asp:Timer>
            </contenttemplate> 
        </asp:updatepanel> 
        <div>
            <asp:HiddenField ID="FolderId" runat="server" />
            <asp:HiddenField ID="hdocId" runat="server" />
            <!-- Start MITS 20965 skhare7-->
            <asp:HiddenField ID="hdocIdList" runat="server" />   
            <!--End MITS 20965 skhare7-->
            <asp:HiddenField ID="Psid" runat="server" />
            <asp:HiddenField ID="flag" runat="server" />
            <asp:HiddenField ID="hdnPageNumber" runat="server" />
            <asp:HiddenField ID="hdnSortExpression" runat="server" />
            <asp:HiddenField ID="FolderName" runat="server" />
            <asp:HiddenField ID="AttachTableName" runat="server" />
            <asp:HiddenField ID="AttachRecordId" runat="server" />
            <asp:HiddenField ID="FormName" runat="server"/>
            <asp:HiddenField ID="hdnDocumentSize" runat="server" />
            <asp:HiddenField ID="Regarding" runat="server" Value="Files"/>
            <table width="100%" class="singleborder">
                <tr>
                    <td colspan="3">
                        <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                    </td>    
                </tr>
                <tr>
                    <td colspan="3" class="ctrlgroup">
                        <asp:Label ID="lblDocumentProperties" runat="server" Text="<%$ Resources:lblDocumentPropertiesResrc %>" />
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblDocumentFolder" runat="server" Font-Bold="true" Text="<%$ Resources:lblDocumentFolderResrc %>" />
                    </td>
                    <td class="datatd">
                        <b><u><asp:Label ID="lblFolderName" runat="server"></asp:Label></u></b>     
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="<%$ Resources:lblTitleResrc %>" />
                    </td>
                    <td width="100%" class="datatd">
                        <input type="text"  runat="server" id="Title" value="<%#ViewData.Title%>" size="50" style="border: 0px solid #104A7B;" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblSubject" runat="server" Font-Bold="true" Text="<%$ Resources:lblSubjectResrc %>" />
                    </td>
                    <td width="100%" class="datatd">
                        <input type="text" runat="server" id="Subject" value="<%#ViewData.Subject%>" size="50" style="border: 0px solid #104A7B;" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblType" runat="server" Font-Bold="true" Text="<%$ Resources:lblTypeResrc %>" />
                    </td>
                    <td width="100%" class="datatd">
                        <input type="text" runat="server" id="Type" value="<%#ViewData.Type.Desc%>" size="50" style="border: 0px solid #104A7B;" readonly="readonly" />
                        <asp:Image ID="imgProcessing" runat="server" Visible="false"  ImageUrl="../../Images/loading1.gif"/> 
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblClass" runat="server" Font-Bold="true" Text="<%$ Resources:lblClassResrc %>" />
                    </td>
                    <td width="100%" class="datatd">
                        <input type="text" runat="server"  id="Class" value="<%#ViewData.Class.Desc%>" size="50" style="border: 0px solid #104A7B;" readonly="readonly" />
                        <asp:Label ID="lblDownloading" runat="server" Text="<%$ Resources:lblDownloadingResrc %>" Font-Bold="true" Visible="false" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblCategory" runat="server" Font-Bold="true" Text="<%$ Resources:lblCategoryResrc %>" />
                    </td>
                    <td width="100%" class="datatd">
                        <input type="text" runat="server" id="Category" value="<%#ViewData.Category.Desc%>" size="50" style="border: 0px solid #104A7B;" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblNotes" runat="server" Font-Bold="true" Text="<%$ Resources:lblNotesResrc %>" />
                    </td>
                    <td width="100%" class="datatd">
                        <input type="text" runat="server" id="Notes" value="<%#ViewData.Notes%>" size="50" style="border: 0px solid #104A7B;" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblKeywords" runat="server" Font-Bold="true" Text="<%$ Resources:lblKeywordsResrc %>" />
                    </td>
                    <td width="100%" class="datatd">
                        <input type="text" runat="server" id="Keywords" value="<%#ViewData.Keywords%>" size="50" style="border: 0px solid #104A7B;" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblFileName" runat="server" Font-Bold="true" Text="<%$ Resources:lblFileNameResrc %>" />
                    </td>
                    <td width="100%" class="datatd">
                        <input type="text" runat="server" id="FileName" value="<%#ViewData.FileName%>" size="50" style="border: 0px solid #104A7B;" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblCreatedOn" runat="server" Font-Bold="true" Text="<%$ Resources:lblCreatedOnResrc %>" />
                    </td>
                    <td width="100%" class="datatd">
                        <input type="text" runat="server" id="CreateDate" value="<%#ViewData.CreateDate%>" size="17" style="border: 0px solid #104A7B;" readonly="readonly"/>
                    </td>
                </tr>
                <!-- Start by Shivendu to show file size -->
                <tr>
                    <td nowrap="" class="datatd">
                        <asp:Label ID="lblFileSize" runat="server" Font-Bold="true" Text="<%$ Resources:lblFileSizeResrc %>" />
                    </td>
                    <td width="100%" class="datatd">
                        <input type="text" runat="server" id="FileSize" value="<%#ViewData.CreateDate%>" size="17" style="border: 0px solid #104A7B;" readonly="readonly"/>
                    </td>
                </tr>
                <!-- End by Shivendu to show file size -->
                <tr>
                    <td colspan="3" class1="vspacer1">&nbsp;</td>
                </tr>
                <tr>
              <td colspan="3" align="center" nowrap=""><br />
          
          
                <% if (ViewData.Download == 1){%>
             <%if (ViewData.DocInternalType!="3") {%>
              &nbsp;<asp:Button ID="btnDownload" runat="server" Text="<%$ Resources:btnDownloadResrc %>" CssClass="button"
                      onclick="btnDownload_Click"/>
              <%} %>
              <%else if (ViewData.DocInternalType == "3") { %>
              &nbsp;<asp:HyperLink ID="lnkDownloadLink" runat="server" Text="<%$ Resources:btnDownloadResrc %>" Target="_blank" NavigateUrl="<%#ViewData.FilePath %>"/>
              <%} %>
               <%} %>
      
                 <% if (ViewData.Email == 1){%>
             <%if (ViewData.DocInternalType!="3" && !(ViewData.FileName.ToString().Contains(".fdf"))) {%>
              <!--MITS 15265: yatharth: Added condition as we cannot mail fdfs from RMX -->
                &nbsp;<input type="button" runat="server" id="btnEmailDoc" onclick="javascript:EmailDoc('c');" name="E-Mail" value="<%$ Resources:btnEmailDocResrc %>" class="button" />
                 <% if (ViewData.UseOutlookToEmail){%>
                &nbsp;<input id="Button1" type="button" onclick="javascript:EmailUsingOutlook();" name="EmailOutlook" runat="server" value="<%$ Resources:btnEmailUsingOutlookResrc %>" class="button" />
                <%} %>
                <%} %>
               <%} %>
               <% if (ViewData.Edit == 1){%>
             <%if (ViewData.DocInternalType!="3") {%>
              &nbsp;<input type="button" onclick="javascript:EditDocument();" name="Edit"  runat="server" id="btnEdit"  value="<%$ Resources:btnEditResrc %>" class="button"/>
              <%} %>
               <%} %>
      
                <%if (ViewData.ScreenFlag =="Files") {%>
                <% if (ViewData.Move == 1){%>
             <%if (ViewData.DocInternalType!="3") {%>
     
              &nbsp;<input type="button" name="Move" value="<%$ Resources:btnMoveResrc %>" class="button" runat="server" id="btnMove" onclick="javascript:MoveDoc();"/>
                <%} %>
               <%} %>
                <%} %>
                 <% if (ViewData.Delete  == 1){%>
             <%if (ViewData.DocInternalType!="3") {%>
              &nbsp;<input type="button" name="Delete" value="<%$ Resources:btnDeleteResrc %>" class="button" runat="server" id="btnDelete" onclick="javascript:DeleteDoc();"/>
                <%} %>
               <%} %>
                   <% if (ViewData.Transfer  == 1){%>
             <%if (ViewData.DocInternalType!="3") {%>
              <%if (ViewData.ScreenFlag =="Files") {%>
              &nbsp;<input type="button" name="Transfer" value="<%$ Resources:btnTransferResrc %>" class="button" runat="server" id="btnTransfer" onclick="javascript:TransferDoc('t');"/>
                  <%} %>
               <%} %>
               <%} %>
                  <% if (ViewData.Copy  == 1){%>
             <%if (ViewData.DocInternalType!="3") {%>
               <%if (ViewData.ScreenFlag =="Files") {%>
              &nbsp;<input type="button" name="Copy" value="<%$ Resources:btnCopyResrc %>" class="button" runat="server" id="btnCopy" onclick="javascript:CopyDoc();" />&nbsp;
                 <%} %>
               <%} %>
             <%} %>
        &nbsp;
                        <asp:HiddenField ID="NonMCMFormName" runat="server" />
                        <asp:HiddenField ID="hdnformfilecontent" runat="server" />
                        <asp:HiddenField ID="hdnEmailButtonClick" runat="server" />
                        <asp:HiddenField ID="hdnFileSize" runat="server" />
                        <asp:HiddenField ID="hdnSizeLimitOutlook" runat="server"/>
                        <asp:Button ID="btnBack" runat="server" Text="<%$ Resources:btnBackResrc %>" CssClass="button" onclick="btnBack_Click"/>
                    </td> 
                    <td>
                        <asp:Button ID="btnActDownload" runat="server"  Text="<%$ Resources:btnActDownloadResrc %>" onclick="btnActDownload_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </form>   
    </body>
</html>
