﻿using System;
//using Riskmaster.UI.DocumentService;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Models;


namespace Riskmaster.UI.Document
{
    public partial class EmailDocuments : System.Web.UI.Page
    {
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            // pen testing changes:atavaragiri MITS 27871
            //Psid.Value = AppHelper.GetValue("Psid");
            if (!String.IsNullOrEmpty(AppHelper.GetValue("Psid")))
            Psid.Value =AppHelper.HTMLCustomEncode( AppHelper.GetValue("Psid"));
            //END: pen testing
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
            NonMCMFormName.Value = AppHelper.GetValue("NonMCMFormName");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    SetInitialValues();
                    string shdocId = Request.Form["hdocId"].ToString();
                    FolderId.Value = Request.Form["FolderId"].ToString();
                    hdocId.Value = shdocId;
                    Psid.Value = Request.Form["Psid"].ToString();
                    flag.Value = Request.Form["flag"].ToString();
                    hdnPageNumber.Value = Request.Form["hdnPageNumber"].ToString();
                    hdnSortExpression.Value = Request.Form["hdnSortExpression"].ToString();
                    TableName.Value = AppHelper.GetQueryStringValue("TableName").ToString();
                    DocumentBusinessHelper dc = new DocumentBusinessHelper();

                    //commented by Nitin for Mits 15565 on 20-Apr-2009  
                    //UsersList Users = dc.GetEmailUsersList(Psid.Value);
                    //foreach (User user in Users.Users)
                    //{
                    //    ListItem item = new ListItem();
                    //    item.Text = user.FirstName + " " + user.LastName + " " + "(" + user.UserName + ")";
                    //    item.Value = user.Email;
                    //    UserEmailId.Items.Add(item);
                    //}
                    //changes by Nitin for Mits 15565 on 20-Apr-2009

                    if (flag.Value == "")
                    {
                        if (Regarding.Value != "")
                        {
                            EmailSubject.Value = Regarding.Value;
                        }
                    }

                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            DocumentBusinessHelper dc = new DocumentBusinessHelper();
            bool isError = false;
            try
            {
                dc.EmailDocuments();

            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                isError = true;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                isError = true;

            }
            if (!isError)
                Server.Transfer("DocumentList.aspx", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("DocumentList.aspx", true);
        }
    }
}
