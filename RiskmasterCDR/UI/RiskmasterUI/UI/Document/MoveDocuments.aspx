﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoveDocuments.aspx.cs" Inherits="Riskmaster.UI.Document.MoveDocuments" EnableViewStateMac="false"%>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3"  %>



<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Document Management</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body>
    <form id="frmData" runat="server">
      <table class="singleborder" align="center">
      <tr>
       <td colspan="2">
          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
      </td>
      </tr>
    <tr>
        <td class="ctrlgroup">
            <asp:Label ID="lblMoveDocuments" runat="server" Text="<%$ Resources:lblMoveDocumentsResrc %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td nowrap="" class="datatd">
            <asp:Label ID="lblSelectFolder" runat="server" Font-Bold="true" Text="<%$ Resources:lblSelectFolderResrc %>"></asp:Label>
        </td>
    </tr>
    <tr>
     <td class="datatd">
      <p align="center">
          <asp:ListBox ID="folderslist" runat="server" Rows="15"></asp:ListBox>
     
        </p>
     </td>
    </tr>
    <tr>
     <td align="center" nowrap="">
          <asp:Button ID="btnMove" runat="server"  Text="<%$ Resources:btnMoveResrc %>" CssClass="button" OnClick ="btnMove_Click"/>
         <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass="button"  OnClick="btnCancel_Click"/>
    </td>     
    </tr>
    <tr>
    <td>
     <asp:HiddenField ID="FolderId" runat="server" />
     <asp:HiddenField ID="hdocId" runat="server" />
      <asp:HiddenField ID="Psid" runat="server" />
      <asp:HiddenField ID="flag" runat="server" />
      <asp:HiddenField ID="hdnPageNumber" runat="server" />
      <asp:HiddenField ID="hdnSortExpression" runat="server" />
        <asp:HiddenField ID="AttachTableName" runat="server" />
     <asp:HiddenField ID="AttachRecordId" runat="server" />
      <asp:HiddenField ID="FormName" runat="server"/>
      <asp:HiddenField ID="Regarding" runat="server" Value="Files"/>
    </td>
    </tr>
   </table>
         	
    </form>
</body>
</html>
