﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="DocumentList.aspx.cs" Inherits="Riskmaster.UI.Document.DocumentList" validaterequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Document Management</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script language="JavaScript" src="../../Scripts/emaildocuments.js" type="text/javascript"></script>
    <%--tanwar2 - ImageRight interface - start--%>
    <link href="../../Scripts/jquery/themes/base/minified/jquery.ui.tabs.css" rel="stylesheet"
        type="text/css" />
    <style type="text/css">
       
    .ui-state-default
    {
        background-image: none;
      background-color:rgb(214, 215, 215);
    }
    .ui-state-active 
    {
        border: 1px solid rgb(197, 210, 229);
      background-image: none;
      background-color:rgb(197, 210, 229);
      border-bottom-color: rgb(197, 210, 229);
    }
    div.horizontalRule {
    min-height: 1px;
    clear:both; width:100%;
    border-bottom:1px solid rgb(197, 210, 229);
    height:1px; padding-top:3px;
    margin-top:3px;
    margin-bottom:3px;
    }
    .hide {
        display:none;
    }
        INPUT.hide {
        display:none;
        }
    .hyperlink a { 
       color:blue !important; 
    } 
    </style>
    <script src="../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery/ui/minified/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery/ui/minified/jquery.ui.tabs.min.js" type="text/javascript"></script>
    <%--tanwar2 - ImageRight interface - end--%>
    <script language="javascript" type="text/javascript">
    //<%--tanwar2 - ImageRight interface - start--%>
        $('document').ready(function () {
            if ($('#AttachTableName') && $.trim($('#AttachTableName').val()) === 'claim' && $('#hdnUseIR').val() === 'True') {
                var selectedTab = 0;
                if ($('#hdnLoadImageRight') && $('#hdnLoadImageRight').val() == 1) {
                    selectedTab = 1;
                }
                if ($('#hdnUseIRWS').val() === 'True') {
                    $('#lblFolderTab').text($('#lblFolder').text().substr(0, 18));
                    $('.horizontalRule').removeClass('hide');
                    $('#btnIRRefresh').removeClass('hide');
                    $('.msgheader').addClass('hide');
                    $("#tabs").tabs({
                        selected: selectedTab,
                        select: function (event, ui) {
                            onTabSelected(event, ui);
                        }
                    });

                    $('#tabs').show();
                }
                else {
                    $('#tabs > ul').addClass('hide');
                    $('#tabs').removeAttr('style');
                }
            }
            else {
                $('#tabs > ul').addClass('hide');
                $('#btnApprove').addClass('hide');
                $('#tabs').removeAttr('style');
            }

            if ($('#hdnSelectImgRightTab').val() == "1") {
                $('#hdnSelectImgRightTab').val("0");
                $("a[href='#tabs-ImgRight']").click();
            }

            $('#btnIRRefresh').click(function () {
                var myobj = {};
                myobj.index = 1;
                onTabSelected(null, myobj);
                return false;
            });
        });

    function approveClick() {
        var dId = 0;
        if ($('#hdocId')) {
            dId = $('#hdocId').val();
        }
        if (dId == '') {
            alert("Please select files to approve.");
            return false;
        }

        pleaseWait.Message = "Please Wait for ImageRight";
        pleaseWait.Show();
        return true;
    }

    function onTabSelected(event, ui) {
        //In jquery ui version 1.9 and above ui.newTab.index will work
        //In older versions ui.index will work
        var tabIndex;
        if ('index' in ui) {
            tabIndex = ui.index;
        }
        else {
            tabIndex = ui.newTab.index();
        }
        if (tabIndex === 1) {
            if ($('#hdnLoadImageRight')) {
                $('#hdnLoadImageRight').val(1);
                $('#PleaseWaitDialog1_pleaseWaitFrame').appendTo($('#tabs-ImgRight'));
                pleaseWait.Message = "Please Wait for ImageRight Webservice";
                pleaseWait.Show();
                $('#frmData').submit();
            }
        }
        else {
            if ($('#hdnLoadImageRight')) {
                $('#hdnLoadImageRight').val(0);
                $('#PleaseWaitDialog1_pleaseWaitFrame').appendTo($('#tabs-doc'));
            }
        }
    }

    function displayDoc(DocumentId) {
        try {
            //get values from claim page
            var sDrawer = "";
            var sFileType = "";
            var sClaimNumber = "";

            if (window.opener != null) {
                if (window.opener.document.getElementById('irdrawer')!=null) {
                    sDrawer = window.opener.document.getElementById('irdrawer').value;
                }
                if (window.opener.document.getElementById('irdrawer') != null) {
                    sFileType = window.opener.document.getElementById('irfiletype').value;
                }
                if (window.opener.document.getElementById('irdrawer') != null) {
                    sClaimNumber = window.opener.document.getElementById('claimnumber').value;
                }
            }

            var customLinkId = 'invisibleIRLink';
            var $irLink = document.getElementById(customLinkId);

            //create an invisible anchortag
            if ($irLink == null) {
                var anchor = document.createElement('a');
                //set href as empty - as value needs to be updated even if the control does not reach here
                anchor.href = "";
                anchor.innerHTML = "";
                anchor.setAttribute("id", customLinkId);
                //add display none
                anchor.setAttribute("style", "display:none;");
                //target needs to be set blank (#??) to handle chrome v30+ bug (Chrome bug#318788. Refer https://code.google.com/p/chromium/issues/detail?id=318788).
                anchor.setAttribute("target", "#");
                document.body.appendChild(anchor);

                //$irLink was null - Now it should be an object
                $irLink = document.getElementById(customLinkId);
            }

            //Drawer name, File type etc may have space between them. Accordingly sorrounding by double quotes (")
            var _url = 'IR:OpenImageRight?drawer="' + sDrawer + '&fileType=' + sFileType + '&fileNumber=' + sClaimNumber + '&documentID=' + DocumentId + '"';
            //At this place $irLink should not be null - checking null as a precaution
            if ($irLink != null) {
                //set the url
                $irLink.setAttribute('href', _url);
                //Invoke the link
                //$irLink.click();
                var $irWin = window.open(_url, '_blank');
                if ($irWin!=null) {
                    $irWin.close();
                }
            }
        }
        catch (e) {
            alert("An Error occurred: " + e.message);
            //If console log is available - write to the log
            if (window.console && console.log) {
                console.log(e.message);
            }
        }
        //window.open("./ImageRight/DisplayDocument.aspx?DocId="+DocumentId, "IRDoc", 'width=650,height=400' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes,modal=1');
    }
    //<%--tanwar2 - ImageRight interface - end--%>

        var theMailItem; //Reference to Outlook.Application 	
        var theApp; //Outook Mail Item	
        var milisec;
        var seconds;
        function CheckF(ctrl) {

            getCheckedDocFolders();

        }
        function CheckD(ctrl) {

            getCheckedDocFolders();

        }



        function delCheck() {
            var fId = document.forms[0].hfolderId.value;
            var dId = document.forms[0].hdocId.value;
            if (fId == '') {
                if (dId == '') {
                    //alert("Please select files and/or folders to delete.");
                    alert(DocumentListValidations.ValidConfirmDelete);
                    return false;
                }
                if (dId == '') {
                    //alert("Please select files and/or folders to delete.");
                    alert(DocumentListValidations.ValidConfirmDelete);
                    return false;
                }
            }

            //Manoj - LSS Documents
            for (var i = 0; i < document.forms[0].length; i++) {
                var objElem = document.forms[0].elements[i];
                if (objElem.checked == true) {
                    if (objElem.docinternaltype == '3') {
                        //alert("LSS document(s) can not be deleted from RMX. Please delete it from LSS.");
                        alert(DocumentListValidations.ValidLSSRestrictionWithDel)
                        return false;
                    }
                }
            }


            return true;

        }

        function copyCheck() {
            var dId = document.forms[0].hdocId.value;
            if (dId == '0') {
                //alert("Please select files you would like to copy.");
                alert(DocumentListValidations.ValidConfirmCopy)
                return false;
            }
            if (dId == '') {
                //alert("Please select files you would like to copy.");
                alert(DocumentListValidations.ValidConfirmCopy)
                return false;
            }

            //Manoj - LSS Documents
            for (var i = 0; i < document.forms[0].length; i++) {
                var objElem = document.forms[0].elements[i];
                if (objElem.checked == true) {
                    if (objElem.docinternaltype == '3') {
                        //alert("LSS document(s) can not be copied in RMX.");
                        alert(DocumentListValidations.ValidLSSRestrictionWithCopy)
                        return false;
                    }
                }
            }
            var screen = 'CopyDocument.aspx';
            var title = document.getElementById('hdn_lblFolder');
            var sAction = screen + "?Psid=" + document.getElementById('Psid').value + "&operation=c";
            if (title != null) {
                sAction += "&formsubtitle=" + title.value;
            }
            document.forms[0].action = sAction;
            document.forms[0].method = "post";
            document.forms[0].submit();
        }

        function transferCheck() {
            var dId = document.forms[0].hdocId.value;
            if (dId == '0') {
                //alert("Please select files you would like to transfer.");
                alert(DocumentListValidations.ValidConfirmTransfer)
                return false;
            }
            if (dId == '') {
                //alert("Please select files you would like to transfer.");
                alert(DocumentListValidations.ValidConfirmTransfer)
                return false;
            }

            //Manoj - LSS Documents
            for (var i = 0; i < document.forms[0].length; i++) {
                var objElem = document.forms[0].elements[i];
                if (objElem.checked == true) {
                    if (objElem.docinternaltype == '3') {
                        //alert("LSS document(s) can not be transferred in RMX.");
                        alert(DocumentListValidations.ValidLSSRestrictionWithTransfer)
                        return false;
                    }
                }
            }

            var screen = 'TransferDocument.aspx';
            var title = document.getElementById('hdn_lblFolder');
            var sAction = screen + "?Psid=" + document.getElementById('Psid').value + "&operation=t";
            if (title != null) {
                sAction += "&formsubtitle=" + title.value;
            }
            document.forms[0].action = sAction;
            document.forms[0].method = "post";
            document.forms[0].submit();
        }

        function emailCheck(iEmailMode) {
            document.forms[0].hdnFileNames.value = ""; //smishra25:clearing the list of files
            var stopFlag = false;
            var totalfileSize = 0.0;
            var dId = document.forms[0].hdocId.value;
            if (dId == '0') {
                //alert("Please select files you would like to e-mail.");
                alert(DocumentListValidations.ValidConfirmEmail)
                return false;
            }
            if (dId == '') {
                //alert("Please select files you would like to e-mail.");
                alert(DocumentListValidations.ValidConfirmEmail)
                return false;
            }

            //Manoj - LSS Documents
            var j;
            for (var i = 0; i < document.forms[0].length; i++) {
                var objElem = document.forms[0].elements[i];

                if (objElem.checked == true) {
                    if (objElem.docinternaltype == '3') {
                        //alert("LSS document(s) can not be e-mailed in RMX.");
                        alert(DocumentListValidations.ValidLSSRestrictionWithEmail)
                        return false;
                    }
                    // Code added by yatharth -- START -- MITS 15265
                    j = i + 1;
                    for (loop = 0; loop < 250; loop++) {

                        var objNewElem = document.forms[0].elements[j];
                        if (objNewElem != undefined && objNewElem != null) {
                            var value = objNewElem.value;
                            //rsharma220 MITS 36128
                            if (objNewElem.id.indexOf("hdnTitle") > -1 && value == '') {
                                value = "@@^^rmA^^@@";
                            }
                            if (objNewElem.type == 'checkbox')
                                break;

                            if (value != null && value != "") {
                                //if (value.indexOf(".fdf") > 0) {
                                   // alert("FDF document(s) cannot be e-mailed in RMX.");
                                    //return false;
                               // }
                                if (value.indexOf("Select All") > -1)//hack to stop
                                {

                                    stopFlag = true;
                                }

                                if (objNewElem.id.indexOf("hdnFileSize") > -1 && value.indexOf("MB") > -1) {
                                    var sFileSize = value.split(" ");
                                    totalfileSize = parseFloat(totalfileSize) + parseFloat(sFileSize[0]);
                                }
                                if (!stopFlag && objNewElem.id.indexOf("hdnFileSize") <= -1) {
                                    document.forms[0].hdnFileNames.value = document.forms[0].hdnFileNames.value + value + ",";
                                }
                            }
                        }
                        j++;

                    }
                    // Code -- END
                }
            }
            if (iEmailMode == 2) {
                var sizeLimitOutlook = parseInt(document.forms[0].hdnSizeLimitOutlook.value);
                if (totalfileSize > sizeLimitOutlook) {
                    //alert("Total File Size should be less than " + document.forms[0].hdnSizeLimitOutlook.value + " MB");
                    alert(DocumentListValidations.ValidFileSize + " " + document.forms[0].hdnSizeLimitOutlook.value + " MB");

                    return false;
                }
                if (confirm(DocumentListValidations.ValidOutlookExist)) {

                }
                else {

                    return false;
                }
                //pleaseWait.Message = "Opening Outlook Window.........";
                pleaseWait.Message = DocumentListValidations.ValidOpeningOutlook;
                pleaseWait.Show();
                document.getElementById('hdnEmailButtonClick').value = "1";
                document.forms[0].submit();
            }
            else {
                var screen = 'EmailDocuments.aspx';
                var title = document.getElementById('hdn_lblFolder');
                var sAction = screen + "?Psid=" + document.getElementById('Psid').value + "&operation=t";
                if (title != null) {
                    sAction += "&formsubtitle=" + title.value;
                }
                document.forms[0].action = sAction;
                document.forms[0].method = "post";
                document.forms[0].submit();
            }
        }

        function moveCheck() {
            var dId = document.forms[0].hdocId.value;
            if (dId == '0') {
                //alert("Please select files you would like to move.");
                alert(DocumentListValidations.ValidConfirmMove);
                return false;
            }
            if (dId == '') {
                //alert("Please select files you would like to move.");
                alert(DocumentListValidations.ValidConfirmMove);
                return false;
            }

            //Manoj - LSS Documents
            for (var i = 0; i < document.forms[0].length; i++) {
                var objElem = document.forms[0].elements[i];
                if (objElem.checked == true) {
                    if (objElem.docinternaltype == '3') {
                        //alert("LSS document(s) can not be moved in RMX.");
                        alert(DocumentListValidations.ValidLSSRestrictionWithMove);
                        return false;
                    }
                }

            }

            var screen = 'MoveDocuments.aspx';
            var title = document.getElementById('hdn_lblFolder');
            var sAction = screen + "?";
            if (title != null) {
                sAction += "formsubtitle=" + title.value;
            }
            document.forms[0].action = sAction;
            document.forms[0].method = "post";
            document.forms[0].submit();
        }
        function Trim(sString) {
            while (sString.substring(0, 1) == ' ') {
                sString = sString.substring(1, sString.length);
            }
            while (sString.substring(sString.length - 1, sString.length) == ' ') {
                sString = sString.substring(0, sString.length - 1);
            }
            return sString;
        }

        function createfolderCheck() {
            var fname = Trim(document.forms[0].newfoldername.value);
            if (fname == '') {
                //alert("Please enter folder name to create.");
                alert(DocumentListValidations.ValidFolderName);
                document.forms[0].newfoldername.value = "";
                document.forms[0].newfoldername.focus();
                return false;
            }
            if (fname.match(/[%*|\\:\/?<>]+/)) {
                //alert("An invalid name was specified for creating a new folder.");
                alert(DocumentListValidations.ValidConfirmFolderName);
                return false;
            }
            var screen = '';

            return true;
        }

        function AddNewIntegate(folderid, psid) {

            var screen = 'AddDocument.aspx';
            var title = document.getElementById('hdn_lblFolder');
            var sAction = screen + "?folderid=" + folderid + "&psid=" + psid + "&integrate=0";
            if (title != null) {
                sAction += "&formsubtitle=" + title.value;
            }
            document.forms[0].action = sAction;

            document.forms[0].method = "post";
            document.forms[0].submit();
        }
        function getCheckedMCMDocFolders() {
           
            for (var i = 0; i < document.forms[0].length; i++) {
                var objElem = document.forms[0].elements[i];
                var sID = new String(objElem.id);
                if ((sID.substring(0, 6) == "optD") && (objElem.checked == true)) {
                    document.getElementById('hdocId').value += '|' + objElem.value + '|';

                }
                else if ((sID.substring(0, 9) == "optF")) {
                    if (objElem.checked == true)
                        document.getElementById('hfolderId').value += '|' + objElem.value + '|';

                    if (objElem.value == "-1" || objElem.value == "-922")
                        objElem.style.visibility = 'hidden';
                }
                else if ((sID.substring(0, 11) == "optUpfolder"))
                    objElem.style.visibility = 'hidden';
                else if ((sID.substring(0, 3) == "opt")) {
                    if (objElem.value == "")
                        objElem.style.visibility = 'hidden';
                }

            }
        }
        function getCheckedDocFolders() {
            if (document.getElementById('hdocId') != null)
            { document.getElementById('hdocId').value = ""; }
            if (document.getElementById('hfolderId') != null)
            {
                document.getElementById('hfolderId').value = "";
            }
            if (document.getElementById('hdocIdListids') != null)
            //   MITS 23094 skhare7
            { document.getElementById('hdocIdListids').value = ""; }
       //     MITS 23094
            for (var i = 0; i < document.forms[0].length; i++) {
                var objElem = document.forms[0].elements[i];
                var sID = new String(objElem.id);

                if ((sID.substring(0, 6) == "optD") && (objElem.checked == true)) {
                    document.getElementById('hdocId').value += +objElem.value + ',';

                }
                else if ((sID.substring(0, 9) == "optF") && (objElem.checked == true)) {
                    if (objElem.value != '-1')
                        document.getElementById('hfolderId').value += objElem.value + ',';


                }
            }

        }
        function GetDocumentsInformation(pagenumber, parentfolderid, parentfoldername, folderid) {

            if (folderid == 'undefined' || folderid == null)
                folderid = 0;
            document.forms[0].FolderId.value = folderid;
            var screen = 'DocumentList.aspx';
            var title = document.getElementById('hdn_lblFolder');
            var sAction = screen + "?FolderId=" + folderid;
            if (title != null) {
                sAction += "&formsubtitle=" + title.value;
            }
            document.forms[0].action = sAction;
            document.forms[0].hdnIsMoveToDiffFolder.value = "move";
            document.forms[0].method = "post";
            document.forms[0].submit();
            return false;
        }
        function Refresh() {
            document.forms[0].submit();
            return false;
        }
        function GetDocument(id, fileSize) {
            //Merge 13013
           if ('<%#Model.ViewAttachment%>' != "false") {
                var screen = 'DisplayDocument.aspx';
                var title = document.getElementById('hdn_lblFolder');
                var sAction = screen + "?id=" + id + "^FileSize=" + fileSize;
                if (title != null) {
                    sAction += "^formsubtitle=" + title.value;
                }
                sAction = "DocumentList.aspx?RedirectUrl=" + sAction;
                document.forms[0].action = sAction;
                document.forms[0].method = "post";
                document.forms[0].submit();
                return false;
            }
            else {
                //alert('You do not have permission to complete requested operation. (No View / Download Permission in Attached Documents)');
                alert(DocumentListValidations.ValidConfirmPermission);
                return false;
            }
        }
        //zalam 08/26/2008 Mits:-13137 Start
        function CheckCheckAll() {
            inputs = document.getElementsByTagName("input");

            for (i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    inputs[i].checked = true;
                }
            }
            getCheckedDocFolders();
        }
        //MITS 20965 skhare7 Start
        function CheckSelected() {
        // skhare7:MITS 23094
            if (document.getElementById('hdocIdListids') != null) 
         {
             if (document.getElementById('hdocIdListids').value != "") 
             {
                 var CheckdDocId = document.getElementById('hdocIdListids').value;
                 var arrCheckedDocs = new Array();
                 if (CheckdDocId.match(',')) 
                 {
                     arrCheckedDocs = CheckdDocId.split(",");
                 }
                 else if (CheckdDocId.match('|'))
                  {
                     arrCheckedDocs = CheckdDocId.split("|");

                 }
                 inputs = document.getElementsByTagName("input");
                 for (i = 0; i < inputs.length; i++)
                  {

                     if (inputs[i].type == "checkbox")
                      {
                         var sID = new String(inputs[i].id);

                         if ((sID.substring(0, 6) == "optD") || (sID.substring(0, 9) == "optF")) 
                         {
                             for (var j = 0; j <= arrCheckedDocs.length - 1; j++)
                              {

                                 if (inputs[i].value == arrCheckedDocs[j])
                                  {

                                     inputs[i].checked = true;
                                 }

                             }
                         }
                     }
                 }
             }
           
            
                getCheckedDocFolders();
            }
              //MITS 20965 skhare7 End
        }

        function UnselectCheckAll() {
            inputs = document.getElementsByTagName("input");
            for (i = 0; i < inputs.length; i++) {
               
                if (inputs[i].type == "checkbox") {
                    inputs[i].checked = false;
                }
            }
            document.all.hfolderId.value = '';
            document.all.hdocId.value = '';
        }
        //zalam 08/26/2008 Mits:-13137 End

        function PageLoaded() {
         
            try {
                // MITS 20965 skhare7 

                if (document.getElementById('hdocIdListids').value !== "")
                    CheckSelected();
                //    MITS 20965 skhare7 
                if (parent != null)
                    parent.MDIScreenLoaded();
            }
            catch (e) { }

            document.forms[0].method = "post";
            //Jump to Acrosoft if enabled..Raman Bhatia . 08/03/2006
            if ('<%#Model.JumpToAcrosoft%>' == "1") {
                if ('<%#Model.JumpDestination%>' == "Attachments") {
                    sTableName = '<%#Model.AttachTable%>';
                    sFormName = '<%#Model.FormName%>';
                    sRecordId = '<%#Model.AttachRecordId%>';
                    sid = '<%#Model.AcrosoftSession%>';
                    sIndexKey = '<%#Model.AcrosoftAttachmentsTypeKey%>';
                    sEventFolderFriendlyName = '<%#Model.EventFolderFriendlyName%>';
                    sClaimFolderFriendlyName = '<%#Model.ClaimFolderFriendlyName%>';
                    sAsAnywhereLink = '<%#Model.AsAnywhereLink%>';
                    sAcrosoftSkin = '<%#Model.AcrosoftSkin%>';
                    sAcrosoftPath = "";
                    switch (sTableName)
                     {
                    
                   
                        case "claim": if (window.opener.document.forms[0].claimnumber) {
                                sClaimNumber = window.opener.document.forms[0].claimnumber.value;
                                sEventNumber = window.opener.document.forms[0].ev_eventnumber.value;
                                //skhare7:MITS 21681 add a parameter Module
                                sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sEventFolderFriendlyName + "=" + sEventNumber + "&" + sClaimFolderFriendlyName + "=" + sClaimNumber + "&sid=" + sid + "&app_func=selectFolder&Module=asdocs";
                            }
                            else { //MITS 17476: MCM does not work in readonly powerviews
                                sClaimNumber = window.opener.document.getElementById('claimnumber').innerText;
                                sEventNumber = window.opener.document.getElementById('ev_eventnumber').innerText;
                                sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sEventFolderFriendlyName + "=" + sEventNumber + "&" + sClaimFolderFriendlyName + "=" + sClaimNumber + "&sid=" + sid + "&app_func=selectFolder&Module=asdocs";
                            }
                            break;


                          //skhare7:MITS 22743 Cases plan,expert,litigation,adjusterdatedtext are added
                        case "claimant":
                        case "defendant":
                        case "funds":
                        //case "plan":
                        case "expert":
                        case "litigation":
                        case "adjuster dated text":
                        case "unit": 
						case "propertyloss" :
							//Change by kuladeep for mits:22743 07/20/2011 Start
                            //sClaimNumber = '<%#Model.ClaimNumber%>';
                            //sEventNumber = '<%#Model.EventNumber%>';
                            sClaimNumber = "<%#Model.ClaimNumber%>";
                            sEventNumber = "<%#Model.EventNumber%>";
                            if (sClaimNumber == null) {
                                if (window.opener.document.forms[0].claimnumber!=null)
                                    sClaimNumber = window.opener.document.forms[0].claimnumber.value;
                            }
                            if (sEventNumber == null) {
                                if (window.opener.document.forms[0].ev_eventnumber!=null)
                                    sEventNumber = window.opener.document.forms[0].ev_eventnumber.value;
                            }
                            //Change by kuladeep for mits:22743 07/20/2011 End
                            sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sEventFolderFriendlyName + "=" + sEventNumber + "&" + sClaimFolderFriendlyName + "=" + sClaimNumber + "&sid=" + sid + "&app_func=selectFolder&Module=asdocs";
                            break;
                        //skhare7:MITS 22743 Cases plan,expert,litigation,adjusterdatedtext are added  End
                        case "event":
                        case "piemployee":
                        case "pipatient":
                        case "piwitness":
                        case "eventdatedtext":

                            //MITS 17476: MCM does not work in readonly powerviews
                            if (window.opener.document.forms[0].eventnumber) {
                                sEventNumber = window.opener.document.forms[0].eventnumber.value;
                                sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sEventFolderFriendlyName + "=" + sEventNumber + "&" + sClaimFolderFriendlyName + "=0&sid=" + sid + "&app_func=selectFolder&Module=asdocs";
                            }
                            else { //MITS 17476: MCM does not work in readonly powerviews
                                sEventNumber = window.opener.document.getElementById('eventnumber').innerText;
                                sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sEventFolderFriendlyName + "=" + sEventNumber + "&" + sClaimFolderFriendlyName + "=0&sid=" + sid + "&app_func=selectFolder&Module=asdocs";
                            }

                            break;

                        case "policy": if (window.opener.document.forms[0].policyname.value != "") {
                                sPolicyName = window.opener.document.forms[0].policyname.value;
                                sIndexKey = '<%#Model.AcrosoftPolicyTypeKey%>'
                                sPolicyFolderFriendlyName = '<%#Model.PolicyFolderFriendlyName%>';
                                sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sPolicyFolderFriendlyName + "=" + sPolicyName + "&sid=" + sid + "&app_func=selectFolder&Module=asdocs";
                            }
                            break;
                            //skhare7:Mits 21889 Enhanced policy URL changed according to LOB
                        // Start Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan  
                        case "policyenh":
//                        case "policyenhpc":
//                        case "policyenhgl":
//                        case "policyenhal":
//                        case "policyenhwc": 
                         if (window.opener.document.forms[0].PolicyQuoteName.value != "") {
                                sPolicyName = window.opener.document.forms[0].PolicyQuoteName.value;
                                sIndexKey = '<%#Model.AcrosoftPolicyTypeKey%>';
                                sPolicyFolderFriendlyName = '<%#Model.PolicyFolderFriendlyName%>';
                                sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sPolicyFolderFriendlyName + "=" + sPolicyName + "&sid=" + sid + "&app_func=selectFolder&Module=asdocs";
                                //skhare7:End MITS 21681 add a parameter Module
                            }
                            break;
                       
                    
                        //skhare7:Mits 21889 
                        // End Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan


                        default: sAcrosoftPath = "";
                            //alert("MCM is enabled. Attachment functionality is not supported on this page.\nPress OK to close this window");
                            alert(DocumentListValidations.ValidMCMEnabled.split('\\n').join('\n'));
                            window.close();
                            break;
                    }

                    if (sAcrosoftPath != "") {
                        var wnd = self.parent.open(sAcrosoftPath, 'DocManagement',
          'width=800,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 800) / 2 + ',resizable=yes,scrollbars=yes');
                        window.close();

                    }
                }

                if ('<%#Model.JumpDestination%>' == "Users") {
                    sIndexKey = '<%#Model.AcrosoftUsersTypeKey %>';
                    sAsAnywhereLink = '<%#Model.AsAnywhereLink%>';
                    sAcrosoftSkin = '<%#Model.AcrosoftSkin%>';
                    sLoginName = '<%#Model.AcrosoftLoginName%>';
                    sUsersFolderFriendlyName = '<%#Model.UsersFolderFriendlyName%>';
                    sid = '<%#Model.AcrosoftSession%>';

                    sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sUsersFolderFriendlyName + "=" + sLoginName + "&sid=" + sid + "&app_func=selectFolder&Module=asdocs";

                    window.location.href = sAcrosoftPath;

                }
            }


            if (document.getElementById('newfoldername') != null)
                document.forms[0].newfoldername.value = "";

             //skhare7:Mits 21889 
            var objTemp = document.forms["frmData"].elements["hdocId"];
            if (objTemp !=null) {
                objTemp.value = "";
            }
            var objTemp = document.forms["frmData"].elements["hfolderId"];
            if (objTemp != null) {
                objTemp.value = "";
            }            
            getCheckedDocFolders();
            getCheckedMCMDocFolders();//Deb: Added this method to pageload 
            return false;



        }
        function CheckAndOpenOutlook() {
            //skhare7:update check null
            if (document.getElementById('hdnEmailButtonClick') != null) {
                if (document.getElementById('hdnEmailButtonClick').value == "1") {
                    //pleaseWait.Message = "Opening Outlook Window...";
                    pleaseWait.Message = DocumentListValidations.ValidOpeningOutlook;
                    pleaseWait.Show();
                    SaveFileAndOpenOutlook();
                }


                document.getElementById('hdnEmailButtonClick').value = "0";
                if (document.getElementById('hdnCounterShow') != null) {
                    if (document.getElementById('hdnCounterShow').value == "1") {
                        InitializeForDisplay();

                    } 
                }
            }

        }
        function SaveFileAndOpenOutlook() {


            try {
                theApp = new ActiveXObject("Outlook.Application");

                theMailItem = theApp.CreateItem(0);  // value 0 = MailItem
            }
            catch (Error) {
                /*alert("The following may have cause this error: \n" +
     "1. The Outlook express 2007 is not installed on the machine.\n" +
     "2. The msoutl.olb is not availabe at the location " +
     "C:\\Program Files\\Microsoft Office\\OFFICE11\\msoutl.old on client's machine " +
     "due to bad installation of the office 2007." +
     "Re-Install office2007 with default settings.\n" +
     "3. The Initialize and Scripts ActiveX controls not marked as safe is not set to enable." +
     "4. Outlook is not runnning on the machine.")*/
                alert(DocumentListValidations.ValidCommonError.split('\\n').join('\n'));
                return false;
            }

            var fso;
            var strForm;
            var strFile;
            var docIds = document.getElementById('hdnformfilecontent').value.split(",");
            for (i = 0; i < docIds.length; i++) {
                if (docIds[i] != null && docIds[i] != "") {
                    var id = "hdnFileContent" + i.toString();
                    var template = (document.getElementById(id).value);
                    fso = new ActiveXObject("Scripting.FileSystemObject");
                    var sFileName = docIds[i].split("~");
                    strFile = sFileName[1];
                    strForm = fso.GetSpecialFolder(2).Path + "\\" + strFile;
                    StreamAndSave(template, strForm)

                }
            }

            theMailItem.display()
            self.close();
            //smishra25 Start MITS 22414: Close the document list screen when Outook has opened.
            if (parent.document.getElementById('selectedScreenId') != null) {
                parent.closeScreen(parent.document.getElementById('selectedScreenId').value, false);
            }
            //smishra25 End MITS 22414: Close the document list screen when Outook has opened.
        }
        function SendAttach(strAttachPath) {


            //Attach Files to the email
            var attach1 = strAttachPath;

            //Construct the Email including To(address),subject,body
            if (document.forms[0].Regarding != null) {
                var subject = document.forms[0].Regarding.value;
            }
            //Create a object of Outlook.Application
            try {



                //Bind the variables with the email
                theMailItem.Subject = (subject);
                theMailItem.Attachments.Add(attach1)

            }
            catch (err) {
                /*alert("The following may have cause this error: \n" +
     "1. The Outlook express 2007 is not installed on the machine.\n" +
     "2. The msoutl.olb is not availabe at the location " +
     "C:\\Program Files\\Microsoft Office\\OFFICE11\\msoutl.old on client's machine " +
     "due to bad installation of the office 2007." +
     "Re-Install office2007 with default settings.\n" +
     "3. The Initialize and Scripts ActiveX controls not marked as safe is not set to enable." +
     "4. Outlook is not runnning on the machine.")*/
                alert(DocumentListValidations.ValidCommonError.split('\\n').join('\n'));
            }

        }
        function InitializeForDisplay() {
            milisec = 0;
            seconds = 10;
            var objTimerLabel = document.getElementById('Label1');
            //objTimerLabel.style.setAttribute('display', '');
            objTimerLabel.style.display = ''; //Browser compatibility
            //objTimerLabel.innerHTML = "Document list will auto refresh in " + seconds + " seconds";
            objTimerLabel.innerHTML = DocumentListValidations.ValidAutoRefresh1 + " " + seconds + " " + DocumentListValidations.ValidAutoRefresh2;
            display();

        }
        function display() {
            if (milisec <= 0) {
                milisec = 15;
                seconds -= 1;
            }
            if (seconds <= -1) {
                milisec = 0;
                seconds += 1;
            }
            else
                milisec -= 1;
            var objTimerLabel = document.getElementById('Label1');
            //objTimerLabel.innerHTML = "Document list will auto refresh in " + seconds + " seconds";
            objTimerLabel.innerHTML = DocumentListValidations.ValidAutoRefresh1 + " " + seconds + " " + DocumentListValidations.ValidAutoRefresh2;

            if (seconds == 0 && document.getElementById('hdnCounterShow').value == "1") {

                InitializeForDisplay();
                document.forms[0].btnGridRefresh.click();

            }
            else if (document.getElementById('hdnCounterShow').value == "0") {
                //objTimerLabel.style.setAttribute('display', 'none');
                // objTimerLabel.style.display = none;
                objTimerLabel.style.display = "none"; //Add by ttumula2 for RMA-9303

            }
            else if (seconds > 0 && document.getElementById('hdnCounterShow').value == "1") {
                setTimeout("display()", 100);
            }

        } 

    </script>
</head>
<body onload="PageLoaded();CheckAndOpenOutlook()">
    <form id="frmData" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%if (Model.JumpToAcrosoft != "1")
      { %>
      <!--tanwar2 - ImageRight interface - start-->
      <!--<div>-->
    <div id='tabs' style="display:none">
    <ul>
    <li><a href="#tabs-doc"><asp:Label ID="lblFolderTab" runat="server"></asp:Label></a></li>
    <li><a href="#tabs-ImgRight"><asp:Label runat="server" Text="<%$ Resources:ImageRightLink %>"></asp:Label></a></li>
  </ul>

  <div class = "horizontalRule hide" >&nbsp;</div>
  <div id="tabs-ImgRight">
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
        <script type="text/javascript">
            function GetSelectedRows() {
                masterTable = $find("<%=grdDoc.ClientID %>").get_masterTableView();
                var selectedRows = masterTable.get_selectedItems();
                if (selectedRows != null && selectedRows != undefined) {
                    return selectedRows[0];
                }
            }
            function RowSelected(sender, args) {
                selectedDocId = args.getDataKeyValue("DocId");
            }
          </script>
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        
    </telerik:RadAjaxManager>
      <div align="right"><asp:LinkButton ID="btnIRRefresh" CssClass="headerlink hide" Text="<%$ Resources:lnkRefreshResrc %>" runat="server" /></div>
      
    <telerik:RadGrid runat="server" ID="grdDoc"
        AllowPaging="true" PageSize="15" AllowFilteringByColumn="false" AllowSorting="true"
        AutoGenerateColumns="false" ItemStyle-CssClass="rowlight1" AlternatingItemStyle-CssClass="rowlight2"
         OnNeedDataSource="grdDoc_OnNeedDataSource" >
         <SelectedItemStyle CssClass="SelectedItem" />
        <ClientSettings>
                <ClientEvents OnRowSelected="RowSelected" />                    
                <Scrolling SaveScrollPosition="true" AllowScroll="true" ScrollHeight="500px"/>
                <Selecting AllowRowSelect="true" />
        </ClientSettings>

        <MasterTableView Width="100%" AllowCustomSorting="false" >
             <PagerStyle AlwaysVisible="true" Position="Top" mode="NextPrevAndNumeric"/>
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                 <NoRecordsTemplate>
                  <div>
                    <asp:Label runat="server" Text="<%$ Resources:divNoRecordToDisplay %>" ></asp:Label>
                  </div>
                </NoRecordsTemplate>
            <Columns>
                <telerik:GridBoundColumn DataField="DocId" HeaderText="<%$ Resources:gridColDocId %>" UniqueName="DocId" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridHyperLinkColumn DataTextField="Description" HeaderText="<%$ Resources:gridColDescription %>" DataNavigateUrlFields="DocId" UniqueName="Description" DataNavigateUrlFormatString="javascript:displayDoc('{0}');" >
                    <ItemStyle CssClass="hyperlink" />
                </telerik:GridHyperLinkColumn>
                <telerik:GridDateTimeColumn DataField="CreateDate" HeaderText="<%$ Resources:gridColDataCreated %>" UniqueName="CreateDate" DataFormatString="{0:MM/dd/yyyy}" ></telerik:GridDateTimeColumn>
                <telerik:GridBoundColumn DataField="DocType" HeaderText="<%$ Resources:gridColDocumentType %>" UniqueName="DocType">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Pages" HeaderText="<%$ Resources:gridColNoOfPages %>" UniqueName="Pages">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
            
    </telerik:RadGrid>
    
    
    </div>
  <div id="tabs-doc">
    
  
    <!--tanwar2 - ImageRight interface - end-->
    
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td colspan="5">
                    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="8" class="msgheader" bgcolor="#D5CDA4">
                    <asp:Label ID="lblFolder" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="headertext2">
                    <td nowrap="" align="left" class="headertext2">
                        <asp:Label ID="lblPageRangeTop" runat="server"></asp:Label><asp:Label ID="lblPagerTop"
                            runat="server"></asp:Label>
                    </td>
                    <td colspan="8" width="95%" nowrap="" align="right" class="headertext2">
                        <asp:LinkButton ID="lnkRefresh" CssClass="headerlink" OnCommand="LinkCommand_Refresh" Text="<%$ Resources:lnkRefreshResrc %>" runat="server" />
                        <asp:Label ID="lblSpace" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;</asp:Label>
                        <asp:LinkButton ID="lnkFirstTop" runat="server" OnCommand="LinkCommand_Click" CommandName="First" Text="<%$ Resources:lnkFirstTopResrc %>" />
                        <asp:LinkButton ID="lnkPrevTop" runat="server" OnCommand="LinkCommand_Click" CommandName="Prev" Text="<%$ Resources:lnkPrevTopResrc %>" />
                        <asp:LinkButton ID="lnkNextTop" runat="server" OnCommand="LinkCommand_Click" CommandName="Next" Text="<%$ Resources:lnkNextTopResrc %>" />
                        <asp:LinkButton ID="lnkLastTop" runat="server" OnCommand="LinkCommand_Click" CommandName="Last" Text="<%$ Resources:lnkLastTopResrc %>" />
                    </td>
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnGridRefresh" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Button ID="btnGridRefresh" runat="server" OnClick="btnGridRefresh_Click" Width="1"
                                Height="1" style="display:none" />
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" Width="100%"
                                OnRowDataBound="GridView1_RowDataBound" AllowPaging="false" AllowSorting="True"
                                PagerSettings-Mode="NumericFirstLast" OnSorting="GridView1_Sorting" OnRowCreated="GridView1_RowCreated"
                                ShowHeader="true" GridLines="None" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right"
                                PageSize="20">
                                <PagerStyle CssClass="headertext2" ForeColor="White" />
                                <HeaderStyle CssClass="colheader6" />
                                <AlternatingRowStyle CssClass="data2" />
                                <FooterStyle CssClass="colheader6" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                                        <ItemTemplate>
                                            <input type="checkbox" id='opt<%# DataBinder.Eval(Container.DataItem,"Type") %>'
                                                value='<%# DataBinder.Eval(Container.DataItem,"Id")%>' onclick="Check<%# DataBinder.Eval(Container.DataItem,"Type") %>(this)" />
                                            <asp:CheckBox ID="chkDoc" runat="server" Visible="false" />
                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %> ' />
                                            <asp:HiddenField ID="hdnType" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Type") %> ' />
                                        </ItemTemplate>
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="msgheader" FooterStyle-CssClass="msgheader">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="img" runat="server" />
                                            <asp:Label ID="lbl" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="1" HeaderText="<%$ Resources:gvHdrNameResrc %>" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader" FooterStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem,"Message") %>
                                            <asp:HiddenField ID="hdnTitle" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Title") %> ' />
                                            <asp:HiddenField ID="hdnName" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Name") %> ' />
                                            <asp:HyperLink ID="linkName" NavigateUrl="#" runat="server"></asp:HyperLink>
                                        </ItemTemplate>
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField SortExpression="2" HeaderText="<%$ Resources:gvHdrSubjectResrc %>" DataField="Subject" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField SortExpression="3" HeaderText="<%$ Resources:gvHdrTypeResrc %>" DataField="DocumentType" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField SortExpression="4" HeaderText="<%$ Resources:gvHdrClassResrc %>" DataField="Class" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField SortExpression="5" HeaderText="<%$ Resources:gvHdrCategoryResrc %>" DataField="Category" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField SortExpression="6" HeaderText="<%$ Resources:gvHdrDateCreatedResrc %>" DataField="CreateDate"
                                        HeaderStyle-CssClass="msgheader" FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="<%$ Resources:gvHdrFileSizeResrc %>" HeaderStyle-CssClass="msgheader" FooterStyle-CssClass="msgheader"
                                        FooterStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Image runat="server" ID="imgProgress" />
                                            <asp:Label ID="lblFileSize" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FileSize") %> ' />
                                            <asp:HiddenField ID="hdnFileSize" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"FileSize") %> ' />
                                            <!--  mkaran2 : Adding UploadStatus : MITS 33747 -->
                                             <asp:HiddenField ID="hdnUploadStatus" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"UploadStatus") %> ' /> 
                                        </ItemTemplate>
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="<%$ Resources:gvHdrUploadToIR %>" DataField="IRUpload"
                                        HeaderStyle-CssClass="msgheader" FooterStyle-CssClass="msgheader" ItemStyle-HorizontalAlign="Center" >
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="colheader6" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                            <!--MITS 21839: Hiddenfield hdnAlwaysAfterGridView is added for the hack to work.Control should always be placed after Grid control GridView1-->
                            <asp:HiddenField ID="hdnAlwaysAfterGridView" runat="server" value=" <%$ Resources:hfSelectAll %> "/>
                            <asp:HiddenField runat="server" ID="hdn_lblFolder" Value="" />
                            <asp:HiddenField ID="hdnCounterShow" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnPageNumber" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnOrderBy" runat="server" />
                            <asp:HiddenField ID="hdocId" runat="server" />
                            <!-- skhare7:MITS 23094-->
                             <asp:HiddenField ID="hdocIdListids" runat="server" />
                            <asp:HiddenField ID="hfolderId" runat="server" />
                            <asp:HiddenField ID="Psid" runat="server" Value="50" />
                            <asp:HiddenField ID="flag" runat="server" />
                            <asp:HiddenField ID="FolderId" runat="server" />
                            <asp:HiddenField ID="FolderName" runat="server" />
                            <asp:HiddenField ID="Regarding" runat="server" />
                            <asp:HiddenField ID="AttachTableName" runat="server" />
                            <asp:HiddenField ID="AttachRecordId" runat="server" />
                            <asp:HiddenField ID="FormName" runat="server" />
                            <asp:HiddenField ID="hdnSortExpression" runat="server" Value="1" />
                            <asp:HiddenField ID="hdTotalPages" runat="server" />
                            <asp:HiddenField ID="hdCurrentPage" runat="server" />
                            <asp:HiddenField ID="NonMCMFormName" runat="server" />
                            <asp:HiddenField ID="hdnformfilecontent" runat="server" />
                            <asp:HiddenField ID="hdnEmailButtonClick" runat="server" />
                            <asp:HiddenField ID="hdnFileNames" runat="server" />
                            <asp:HiddenField ID="hdnSizeLimitOutlook" runat="server" />
                            <asp:HiddenField ID="ParentFolder" runat="server" />
                            <asp:HiddenField ID="hdnIsMoveToDiffFolder" runat="server" />
                            <!-- Yukti, MITS 34595 -->
                            <%--akaushik5 Commented for MITS 36901 Starts--%>
                            <%--<asp:HiddenField ID="hdnSortVar" runat="server" />--%>  
							 <%--akaushik5 Commented for MITS 36901 Ends--%>
                               <%--tanwar2 - ImageRight - start--%>
                            <asp:HiddenField ID="hdnUseIR" runat="server" />
                            <asp:HiddenField ID="hdnUseIRWS" runat="server" />
                            <%--tanwar2 - ImageRight - end--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" class="headertext2">
                    <asp:Label ID="lblPageRangeDown" runat="server"></asp:Label>
                    <asp:Label ID="lblPagerDown" runat="server"></asp:Label>
                </td>
                <td colspan="6" width="100%" nowrap="" align="right" class="headertext2">
                    <asp:LinkButton ID="lnkFirstDown" runat="server" OnCommand="LinkCommand_Click" CommandName="First" Text="<%$ Resources:lnkFirstDownResrc %>" />
                    <asp:LinkButton ID="lnkPrevDown" runat="server" OnCommand="LinkCommand_Click" CommandName="Prev" Text="<%$ Resources:lnkPrevDownResrc %>" />
                    <asp:LinkButton ID="lnkNextDown" runat="server" OnCommand="LinkCommand_Click" CommandName="Next" Text="<%$ Resources:lnkNextDownResrc %>" />
                    <asp:LinkButton ID="lnkLastDown" runat="server" OnCommand="LinkCommand_Click" CommandName="Last" Text="<%$ Resources:lnkLastDownResrc %>" />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td class="PadLeft4">
                    <input type="button" class="button" onclick="CheckCheckAll();" runat="server" value="<%$ Resources:inpSelectAllResrc %>" />
                </td>
                <td class="PadLeft4">
                    <input type="button" class="button" onclick="UnselectCheckAll();" runat="server" value="<%$ Resources:inpUnselectAllResrc %>" />
                </td>
               <script type="text/javascript">
                   var sPid = '<%=Model.Pid%>' ; var sPsid = '<%=Model.Psid%>';
               </script>
                <% if (Model.ScreenFlag == "Files")
                   {%>
                <%if (Model.Create_allowed == "1")
                  {%>
                <td class="PadLeft4">
                 <input type="button" runat="server" value="<%$ Resources:inpAddNewResrc %>" class="button" onclick="return AddNewIntegate(sPid,sPsid);" />&nbsp;
                </td>
                <%} %>
                <%} %>
                <% if (Model.ScreenFlag != "Files")
                   {%>
                <%if (Model.Att_create_allowed == "1")
                  {%>
                <td class="PadLeft4">
                    <input type="button" runat="server" value="<%$ Resources:inpAddNewResrc %>" class="button" onclick="return AddNewIntegate(sPid, sPsid);" />&nbsp;
                </td>
                <%} %>
                <%} %>
                <%if (Model.Move_allowed == "1")
                  {%>
                <td class="PadLeft4">
                    <input type="button" name="" runat="server" value="<%$ Resources:inpMoveResrc %>" class="button" onclick="return moveCheck();; " />
                </td>
                <%} %>
                <%if (Model.Email_allowed == "1")
                  {%>
                <td class="PadLeft4">
                    <input type="button" name="" runat="server" value="<%$ Resources:inpEmailResrc %>" class="button" onclick="return emailCheck();; " />
                </td>
                <%} %>
                <%if (Model.Email_allowed == "1" && Model.UseOutlookToEmail)
                  {%>
                <td class="PadLeft4">
                    <input type="button" name="EmailOutlook" runat="server" value="<%$ Resources:inpEmailUsingOutlookResrc %>" class="button"
                        onclick="return emailCheck(2); " />
                </td>
                <%} %>
                <% if (Model.ScreenFlag == "Files")
                   {%>
                <%if (Model.Delete_allowed == "1")
                  {%>
                <td class="PadLeft4">
                    <asp:Button ID="Button2" Text="<%$ Resources:btnDeleteResrc %>" CssClass="button" runat="server" OnClick="btnDelete_Click"
                        UseSubmitBehavior="true" OnClientClick="return delCheck()" />
                </td>
                <%} %>
                <%} %>
                <% if (Model.ScreenFlag != "Files")
                   {%>
                <%if (Model.Att_delete_allowed == "1")
                  {%>
                <td class="PadLeft4">
                    <asp:Button ID="Button1" Text="<%$ Resources:btnDeleteResrc %>" CssClass="button" runat="server" OnClick="btnDelete_Click"
                        UseSubmitBehavior="true" OnClientClick="return delCheck()" />
                </td>
                <%} %>
                <%} %>
                <% if (Model.ScreenFlag == "Files")
                   {%>
                <%if (Model.Transfer_allowed == "1")
                  {%>
                <td class="PadLeft4">
                    <input type="button" name="" runat="server" value="<%$ Resources:inpTransferResrc %>" class="button" onclick="return transferCheck();; " />
                </td>
                <%} %>
                <%} %>
                <% if (Model.ScreenFlag == "Files")
                   {%>
                <%if (Model.Copy_allowed == "1")
                  {%>
                <td class="PadLeft4">
                    <input type="button" name="" runat="server" value="<%$ Resources:inpCopyResrc %>" class="button" onclick="return copyCheck();; " />
                </td>
                <%} %>
                <%} %>
                <%--tanwar2 - Image Right Interface--%>
                <% if (Model.ScreenFlag != "Files")
                   {%>
                <td class="PadLeft4">
                    <asp:Button ID="btnApprove" Text="<%$ Resources:btnApprove %>" CssClass="button" runat="server"
                        OnClientClick="return approveClick();" onClick="OnApproveClicked" />
                </td>
                <%} %>
            </tr>
        </table>
        <table border="0">
            <% if (Model.ScreenFlag == "Files")
               {%>
            <%if (Model.Createfolder_allowed == "1")
              {%>
            <%if (Model.TopFolder.FolderID == "0")
              { %>
            <%if (Model.Pid != "-1")
              { %>
            <tr>
                <td class="PadLeft4">
                    <asp:Label ID="lblFolderName" runat="server" Text="<%$ Resources:lblFolderName %>"></asp:Label><br />
                    <input type="text" name="newfoldername" value="" id="newfoldername" runat="server" />&nbsp;<asp:Button
                        ID="btnAddFolder" Text="<%$ Resources:inpCreateFolderResrc %>" CssClass="button" runat="server" OnClick="btnAddFolder_Click"
                        UseSubmitBehavior="true" OnClientClick="return createfolderCheck();" />
                </td>
            </tr>
            <%} %>
            <%} %>
            <%} %>
            <%} %>
    </div>
    <%--tanwar2 - ImageRight--%>
    <%} %>
    </div>
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    <asp:HiddenField ID="dummy" runat="server" />
    <%--tanwar2 - ImageRight - start--%>
    <asp:HiddenField ID="hdnLoadImageRight" runat="server" />
    <asp:HiddenField ID="hdnSelectImgRightTab" runat="server" />
    <%--tanwar2 - ImageRight - end--%>
    </form>
</body>
</html>
