﻿using System;
//using Riskmaster.UI.DocumentService;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Models;

namespace Riskmaster.UI.Document
{
    public partial class TransferDocument : System.Web.UI.Page
    {
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            // pen testing changes:atavaragiri MITS 27871
            //Psid.Value = AppHelper.GetValue("Psid");
            if (!String.IsNullOrEmpty(AppHelper.GetValue("Psid")))
            Psid.Value =AppHelper.HTMLCustomEncode( AppHelper.GetValue("Psid"));
            // End:pen testing
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {
                    SetInitialValues();
                    string shdocId = Request.Form["hdocId"].ToString();
                    FolderId.Value = Request.Form["FolderId"].ToString();
                    hdocId.Value = shdocId;
                    Psid.Value = Request.Form["Psid"].ToString();
                    flag.Value = Request.Form["flag"].ToString();
                    hdnPageNumber.Value = Request.Form["hdnPageNumber"].ToString();
                    hdnSortExpression.Value = Request.Form["hdnSortExpression"].ToString();
                    DocumentBusinessHelper dc = new DocumentBusinessHelper();

                    UsersList Users = dc.GetUsersList(FolderId.Value);
                    foreach (User user in Users.Users)
                    {
                        ListItem item = new ListItem();
                        item.Text = user.FirstName + " " + user.LastName + " " + "(" + user.UserName + ")";
                        item.Value = user.UserName;
                        Userslist.Items.Add(item);
                    }

                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("DocumentList.aspx", true);
        }
        protected void btnTransfer_Click(object sender, EventArgs e)
        {
            DocumentBusinessHelper dc = new DocumentBusinessHelper();
            bool isError = false;
            try
            {
                dc.TransferDocuments();
                
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                isError = true;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                isError = true;

            }
            if (!isError)
            Server.Transfer("DocumentList.aspx", true);
        }

    }
}
