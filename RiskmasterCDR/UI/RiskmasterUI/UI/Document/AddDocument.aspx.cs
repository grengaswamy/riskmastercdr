﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Riskmaster.UI.DocumentService;
using System.Web.UI.WebControls;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using CuteWebUI;
using Riskmaster.RMXResourceManager;
using System.IO;
using Riskmaster.Models;
namespace Riskmaster.UI.Document
{
    public partial class AddDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("AddDocument.aspx"), "AddDocumentValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "AddDocumentValidationScripts", sValidationResources, true);

            if (!Page.IsPostBack)
            {
                SetInitialValues();
                // pen  testing changes :atavaragiri MITS 27871
                //FolderId.Value = AppHelper.GetQueryStringValue("folderid");
                //Psid.Value = AppHelper.GetQueryStringValue("psid");
                if(!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("folderid")))
                FolderId.Value = AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("folderid"));
                if(!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("psid")))
                Psid.Value =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("psid"));
                // end pen testing:atavaragiri
                if (Request.QueryString["typecodeid"] != null)
                {
                    PopulateControlsFromPreviousPage();
                }
            }
            //tmalhotra3-changes for MITS 30999
            UploadDocumentAttachments.RemoveButtonText = GetResourceValue("lblRemove", "0");
            UploadDocumentAttachments.CancelText = GetResourceValue("btnCancel", "4");
            UploadDocumentAttachments.UploadingMsg = GetResourceValue("lblUploading", "0");
            UploadDocumentAttachments.TableHeaderTemplate = UploadDocumentAttachments.TableHeaderTemplate.Substring(0, 29) + GetResourceValue("lblFile", "0") + "</td>";
            UploadDocumentAttachments.CancelAllMsg = GetResourceValue("btnCancelAll", "4");
            //end changes for MITS 30999
            
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
       
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            // pen testing changes:atavaragiri MITS 27871
            //Psid.Value = AppHelper.GetValue("Psid");
            if(!String.IsNullOrEmpty(AppHelper.GetValue("Psid")))
            Psid.Value =AppHelper.HTMLCustomEncode( AppHelper.GetValue("Psid"));
            //END:pen testing changes
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
            NonMCMFormName.Value = AppHelper.GetValue("NonMCMFormName");
        }

        private void PopulateControlsFromPreviousPage()
        {
            DocumentType viewData = null;
            HtmlInputText cntrl = null;

            cntrl = (HtmlInputText)PreviousPage.FindControl("Title");
            Title.Value = cntrl.Value;

            cntrl = (HtmlInputText)PreviousPage.FindControl("Subject");
            Subject.Value = cntrl.Value;

            cntrl = (HtmlInputText)PreviousPage.FindControl("Type");
            documenttypecode.Value = cntrl.Value;

            cntrl = (HtmlInputText)PreviousPage.FindControl("Class");
            documentclasscode.Value = cntrl.Value;

            cntrl = (HtmlInputText)PreviousPage.FindControl("Category");
            documentcategorycode.Value = cntrl.Value;

            cntrl = (HtmlInputText)PreviousPage.FindControl("Notes");
            Notes.Value = cntrl.Value;

            cntrl = (HtmlInputText)PreviousPage.FindControl("Keywords");
            Keywords.Value = cntrl.Value;

            documenttypecode_cid.Value = AppHelper.GetQueryStringValue("typecodeid");
            documentclasscode_cid.Value = AppHelper.GetQueryStringValue("classcodeid");
            documentcategorycode_cid.Value = AppHelper.GetQueryStringValue("categorycodeid");

            ViewState["documentid"] = AppHelper.GetQueryStringValue("documentid");

        }

        protected void SaveData_Click(object sender, EventArgs e)
        {
            DocumentBusinessHelper dc = null;
            bool isError = false;
            int iFileSize = 0;
            string strFolderId = string.Empty;
            string strErrorFile = string.Empty;
            try
            {
                dc = new DocumentBusinessHelper();

                if (ViewState["documentid"] != null)
                {
                    if (ViewState["documentid"].ToString() != "" || ViewState["documentid"].ToString() != "0")
                    {
                        if (FolderId.Value == "0")
                            strFolderId = "";
                        
                        dc.Delete(strFolderId, ViewState["documentid"].ToString(), Psid.Value, "Files", "");
                    }
                }

                foreach (AttachmentItem item in UploadDocumentAttachments.Items)
                {
                    if (item.Checked)
                    {
                       
                        iFileSize=+ item.FileSize; 
                    }
                }

                //Added by Nitin
                //if total file size is greater than 35MB then call large file upload method
                if (iFileSize > 1288490188.8)
                {
                    //throw new ApplicationException("Size of attached Files is larger than 1.2 GB");
                    throw new ApplicationException(RMXResourceManager.RMXResourceProvider.GetSpecificObject("CathAttachmentError", RMXResourceManager.RMXResourceProvider.PageId("AddDocument.aspx"), "0").ToString());
                }
                else
                {
                    strErrorFile = Server.MapPath("~/UploaderTemp/");
                    dc.AddMultipleLargeFiles(UploadDocumentAttachments.Items, strErrorFile);
                }

            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                isError = true;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                isError = true;
            }           
            if (!isError)
            {
                //Added:Yukti,Dt:11/14/2013,MITS 30990
                //Server.Transfer("DocumentList.aspx");
                string sTrue = "true";
                Server.Transfer(String.Format("DocumentList.aspx?DocList=" + sTrue));
                //Ended:Yukti,MITS 30990
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            //mkaran2 - MITS #32088 - start                      
                foreach (AttachmentItem item in UploadDocumentAttachments.Items)
                {
                    //if (File.Exists(item.GetTempFilePath()) && BusinessHelpers.DocumentBusinessHelper.CheckFileAccess(item.GetTempFilePath()))
                    //{
                    //    File.Delete(item.GetTempFilePath());                        
                    //}
                    try //  Check File Access function is no longer in use - Code modified as per provided code review suggestions
                    {
                        File.Delete(item.GetTempFilePath());                       
                    }
                    catch
                    {

                    }
                }
             //mkaran2 - MITS #32088 - end
            Server.Transfer("DocumentList.aspx");
        }
        //tmalhotra3-changes for MITS 30999
        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("AddDocument.aspx"), strResourceType).ToString();
        } 
    }
}
