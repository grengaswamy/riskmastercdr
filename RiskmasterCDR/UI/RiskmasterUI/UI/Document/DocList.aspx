﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocList.aspx.cs" Inherits="Riskmaster.UI.Document.DocList" %>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3"  %>



<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Document Management</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script language="javascript" type="text/javascript">
                  
					
					function checkFolder(id,value)
					{
					
						var objElem = document.getElementById(id);
						var sID = value + ',';
						if(objElem.checked)
						{
						    document.getElementById('hfolderId').value += sID;
						}
						else
						{
						    var sIDs = document.getElementById('hfolderId').value;
							document.getElementById('hfolderId').value = sIDs.replace(sID, '');
						}
					}
					
					function checkDoc(id,value)
					{
			           
					    var objElem = document.getElementById(id);
						var sID = value + ',';
					    //alert(id +" "+value);
						if(objElem.checked)
						{
						    document.getElementById('hdocId').value += sID;
						}
						else
						{
						    var sIDs = document.getElementById('hdocId').value;
							document.getElementById('hdocId').value = sIDs.replace(sID, '');
						}
					}
				
					
					function delCheck()
					{
						var fId = document.forms[0].hfolderId.value;
						var dId = document.forms[0].hdocId.value;										
						if (fId=='')
						{
							if (dId=='0')
							{
							    //alert("Please select files and/or folders to delete.");
							    alert(DocListValidations.SelectFiles);
								return false;
							}
							if (dId=='')
							{
							    //alert("Please select files and/or folders to delete.");
							    alert(DocListValidations.SelectFiles);
								return false;
							}
						}						

						//Manoj - LSS Documents
						for (var i=0; i < document.forms[0].length; i++)
						{
							var objElem = document.forms[0].elements[i];
							if(objElem.checked==true)
							{
								if(objElem.docinternaltype=='3')
								{
								    //alert("LSS document(s) can not be deleted from RMX. Please delete it from LSS.");
								    alert(DocListValidations.DeleteLSSDoc);
									return false;								
								}
							}
						}	
					    
					    
					    return true;
											
					}
					
					function copyCheck()
					{
						var dId = document.forms[0].hdocId.value;										
						if (dId=='0')
						{
						    //alert("Please select files you would like to copy.");
						    alert(DocListValidations.SelectFileToCopy);
							return false;
						}
						if (dId=='')
						{
						    //alert("Please select files you would like to copy.");
						    alert(DocListValidations.SelectFileToCopy);
							return false;
						}
					
						//Manoj - LSS Documents
						for (var i=0; i < document.forms[0].length; i++)
						{
							var objElem = document.forms[0].elements[i];
							if(objElem.checked==true)
							{
								if(objElem.docinternaltype=='3')
								{
								    //alert("LSS document(s) can not be copied in RMX.");
								    alert(DocListValidations.CopyLSSDoc);
									return false;								
								}
							}
						}
							 var screen='CopyDocument.aspx';
							 document.forms[0].action = screen + "?Psid=" + document.getElementById('Psid').value + "&operation=c";
                        document.forms[0].method="post";
                        document.forms[0].submit();
					}
					
					function transferCheck()
					{
						var dId = document.forms[0].hdocId.value;										
						if (dId=='0')
						{
						    //alert("Please select files you would like to transfer.");
						    alert(DocListValidations.SelectFileToTransfer);
							return false;
						}
						if (dId=='')
						{
						    //alert("Please select files you would like to transfer.");
						    alert(DocListValidations.SelectFileToTransfer);
							return false;
						}

						//Manoj - LSS Documents
						for (var i=0; i < document.forms[0].length; i++)
						{
							var objElem = document.forms[0].elements[i];
							if(objElem.checked==true)
							{
								if(objElem.docinternaltype=='3')
								{
								    //alert("LSS document(s) can not be transferred in RMX.");
                                    alert(DocListValidations.TransferLSSDoc);
									return false;								
								}
							}
						}
						
						 var screen='TransferDocument.aspx';
						 document.forms[0].action = screen + "?Psid=" + document.getElementById('Psid').value + "&operation=t";
                        document.forms[0].method="post";
                        document.forms[0].submit();
					}
					
					function emailCheck()
					{
						var dId = document.forms[0].hdocId.value;										
						if (dId=='0')
						{
							//alert("Please select files you would like to e-mail.");
						    alert(DocListValidations.SelectFileToEmail);
							return false;
						}
						if (dId=='')
						{
						    //alert("Please select files you would like to e-mail.");
						    alert(DocListValidations.SelectFileToEmail);
							return false;
						}

						//Manoj - LSS Documents
						for (var i=0; i < document.forms[0].length; i++)
						{
							var objElem = document.forms[0].elements[i];
							if(objElem.checked==true)
							{
								if(objElem.docinternaltype=='3')
								{
								    //alert("LSS document(s) can not be e-mailed in RMX.");
								    alert(DocListValidations.EmailLSSDoc);
									return false;								
								}
							}
						}
						   var screen='EmailDocuments.aspx';
						   document.forms[0].action = screen + "?Psid=" + document.getElementById('Psid').value + "&operation=t";
                           document.forms[0].method="post";
                           document.forms[0].submit();
					}
					
					function moveCheck()
					{
						var dId = document.forms[0].hdocId.value;										
						if (dId=='0')
						{
						    //alert("Please select files you would like to move.");
						    alert(DocListValidations.SelectFileToMove);
							return false;
						}
						if (dId=='')
						{
						    //alert("Please select files you would like to move.");
						    alert(DocListValidations.SelectFileToMove);
							return false;
						}

						//Manoj - LSS Documents
						for (var i=0; i < document.forms[0].length; i++)
          {
          var objElem = document.forms[0].elements[i];
          if(objElem.checked==true)
          {
              if(objElem.docinternaltype=='3')
              {
                  //alert("LSS document(s) can not be moved in RMX.");
                  alert(DocListValidations.MoveLSSDoc);
              return false;
              }
          }
          
          }
          
           var screen='MoveDocuments.aspx';
           document.forms[0].action=screen;
           document.forms[0].method="post";
           document.forms[0].submit();
          }
            
          function createfolderCheck()
          {
            var fname = document.forms[0].newfoldername.value;
              if (fname=='')
              {
                  //alert("Please enter folder name to create.");
                  alert(DocListValidations.EnterFolderName);
                return false;
            }
              if (fname.match(/[%*|\\:\/?<>]+/))
              {
                  //alert("An invalid name was specified for creating a new folder.");
                  alert(DocListValidations.InvalidName);
                  return false;
              }
              
           var screen='';
           //document.forms[0].action=screen+ "?Foldername="+fname+"&ParentId="+document.all.FolderId.value;
           //document.forms[0].method="post";
           //document.forms[0].submit();
           return true;
          }
        
          function AddNewIntegate(folderid,psid)
          {
          //alert("home?pg=riskmaster/Integrate/Integration&amp;Screen=1&amp;folderid="+folderid+"&amp;psid="+psid+"&amp;screenflag=Files");
          //location.href="home?pg=riskmaster/Integrate/Integration&amp;Screen=1&amp;folderid="+folderid+"&amp;psid="+psid+"&amp;screenflag=Files";
           var screen='AddDocument.aspx';
           document.forms[0].action=screen+ "?folderid="+folderid+"&psid="+psid+"&integrate=0";
           document.forms[0].method="post";
           document.forms[0].submit();
          }
          function getCheckedDocFolders()
          {
          for (var i=0; i < document.forms[0].length; i++)
						{
							var objElem = document.forms[0].elements[i];
							var sID = new String(objElem.id);
							if((sID.substring(0,6)=="optDoc") && (objElem.checked==true))
							{
							    document.getElementById('hdocId').value += '|' + objElem.value + '|';
							}
							else if((sID.substring(0,9)=="optFolder") && (objElem.checked==true))
							{
							    document.getElementById('hfolderId').value += '|' + objElem.value + '|';
							}
						}
					}
					
					function GetDocumentsInformation(pagenumber,parentfolderid,parentfoldername,folderid)
					{
					   
		            if (folderid=='undefined' || folderid==null)
					            folderid=0;
			            document.forms[0].FolderId.value=folderid;
					    document.forms[0].action="DocList.aspx?type=move&FolderId="+folderid;
					    document.forms[0].method="post";
					    document.forms[0].submit();
					    return false;
					}
					function GetDocument(id)
					{
						document.forms[0].action="DisplayDocument.aspx?id="+id;
					    document.forms[0].method="post";
					    document.forms[0].submit();
					    return false;
					}
										//zalam 08/26/2008 Mits:-13137 Start
					function CheckCheckAll()
					{
					    inputs = document.getElementsByTagName("input");
						for (i = 0; i < inputs.length; i++)
						{
							if (inputs[i].type=="checkbox")
							{
							    inputs[i].checked = true;
							   
							}
						}
					}

					function UnselectCheckAll()
					{
					    inputs = document.getElementsByTagName("input");
						for (i = 0; i < inputs.length; i++)
						{
							if (inputs[i].type=="checkbox")
							{
								inputs[i].checked = false;
							}
						}
					}
					//zalam 08/26/2008 Mits:-13137 End
					
					function PageLoaded()
					{
					 document.forms[0].method="post";
						//Jump to Acrosoft if enabled..Raman Bhatia . 08/03/2006
						if('<%=Model.JumpToAcrosoft%>' =="true")
						{	
							if('<%=Model.JumpDestination%>'=="Attachments")
							{
								sTableName = '<%=Model.AttachTable%>';
								sFormName =  '<%=Model.FormName%>';
								sRecordId = '<%=Model.AttachRecordId%>';
								sid = '<%=Model.Sid%>';
								sIndexKey = '<%=Model.AcrosoftAttachmentsTypeKey%>';
								sEventFolderFriendlyName = '<%=Model.EventFolderFriendlyName%>';
								sClaimFolderFriendlyName = '<%=Model.ClaimFolderFriendlyName%>';
								sAsAnywhereLink = '<%=Model.AsAnywhereLink%>';
								sAcrosoftSkin = '<%=Model.AcrosoftSkin%>';
								sAcrosoftPath = "";
								switch(sTableName)
								{
									case "claim"      :		if(window.opener.document.forms[0].claimnumber)
															{	
																sClaimNumber = window.opener.document.forms[0].claimnumber.value;
																sEventNumber = window.opener.document.forms[0].ev_eventnumber.value;
																sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&amp;" + sEventFolderFriendlyName + "=" + sEventNumber + "&amp;" + sClaimFolderFriendlyName + "=" + sClaimNumber + "&amp;sid=" + sid + "&amp;skin_ddl1=" + sAcrosoftSkin;
															}													
															break; 
									
													
									case "claimant"   : 
									case "defendant"  :
									case "funds"	  : 
									case "unit"	  : sClaimNumber = Model.ClaimNumber;
															sEventNumber = '<%=Model.EventNumber%>';
															sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&amp;" + sEventFolderFriendlyName + "=" + sEventNumber + "&amp;" + sClaimFolderFriendlyName + "=" + sClaimNumber + "&amp;sid=" + sid + "&amp;skin_ddl1=" + sAcrosoftSkin;
															break; 
												      
									case "event"		:								
									case "piemployee"	: 
									case "pipatient"	: 
									case "piwitness"	:
									case "eventdatedtext"   :
												
														if(window.opener.document.forms[0].eventnumber.value!="")
														{	
															sEventNumber = window.opener.document.forms[0].eventnumber.value;
															sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&amp;" + sEventFolderFriendlyName + "=" + sEventNumber + "&amp;" + sClaimFolderFriendlyName + "=0&amp;sid=" + sid + "&amp;skin_ddl1=" + sAcrosoftSkin;
														}
														break;
									
									case "policy" :     if(window.opener.document.forms[0].policyname.value!="")
														{
															sPolicyName = window.opener.document.forms[0].policyname.value;
															sIndexKey = '<%=Model.AcrosoftPolicyTypeKey%>' 
															sPolicyFolderFriendlyName = '<%=Model.PolicyFolderFriendlyName%>';
															sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&amp;" + sPolicyFolderFriendlyName + "=" + sPolicyName + "&amp;sid=" + sid + "&amp;skin_ddl1=" + sAcrosoftSkin;
                            }
                            break;
                  // Start Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan
                  case "policyenh" :     if(window.opener.document.forms[0].PolicyQuoteName.value!="")
                            {
                              sPolicyName = window.opener.document.forms[0].PolicyQuoteName.value;
                              sIndexKey = '<%=Model.AcrosoftPolicyTypeKey%>';
                              sPolicyFolderFriendlyName = '<%=Model.PolicyFolderFriendlyName%>';
                              sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&amp;" + sPolicyFolderFriendlyName + "=" + sPolicyName + "&amp;sid=" + sid + "&amp;skin_ddl1=" + sAcrosoftSkin;
                            }
                            break;
                            // End Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan


                            default:	sAcrosoftPath = "";
                            break;
                            }

          if (sAcrosoftPath!="")
          {
          var wnd=self.parent.open(sAcrosoftPath,'progressWnd',
          'width=800,height=600'+',top='+(screen.availHeight-600)/2+',left='+(screen.availWidth-800)/2+',resizable=yes,scrollbars=yes');
          window.close();

          }
          }

          if('<%=Model.JumpDestination%>'=="Users")
							{
								sIndexKey = '<%=Model.AcrosoftUsersTypeKey %>';
								sAsAnywhereLink = '<%=Model.AsAnywhereLink%>';
								sAcrosoftSkin = '<%=Model.AcrosoftSkin%>';
								sLoginName = '<%=Model.AcrosoftLoginName%>';
								sUsersFolderFriendlyName = '<%=Model.UsersFolderFriendlyName%>';
								sid = '<%=Model.Sid%>';
																								
								sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&amp;" + sUsersFolderFriendlyName + "=" + sLoginName + "&amp;sid=" + sid + "&amp;skin_ddl1=" + sAcrosoftSkin;
								
								window.location.href = sAcrosoftPath;
								
							}
						}
						
										
							if (document.getElementById('newfoldername')!=null)
							document.forms[0].newfoldername.value = "";
							document.forms[0].hdocId.value = "";
							document.forms[0].hfolderId.value = "";
							getCheckedDocFolders();
							return false;
				
					
				
					}
			
    </script>
   
</head>
<body onload="PageLoaded();">
    <form id="frmData" runat="server" >
   
    <div>
    
     <asp:HiddenField ID="ParentFolder" runat="server"/>
     <table width="100%" cellspacing="0" cellpadding="0" border="0">
     <tr>
       <td>
          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
      </td>
      </tr>
     	<tr>
									<td   class="msgheader" bgcolor="#D5CDA4">
										 <asp:Label ID="lblFolder" runat="server"></asp:Label>
									</td>
								</tr>
		<tr>
		
		<td>
		
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" 
            Width="100%" onrowdatabound="GridView1_RowDataBound" AllowPaging="True" 
            AllowSorting="True" onpageindexchanging="GridView1_PageIndexChanging" 
            PagerSettings-Mode=NumericFirstLast onsorting="GridView1_Sorting" 
            onrowcreated="GridView1_RowCreated"  ShowHeader="true"
            GridLines=None PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign=Right PageSize=20>
            
            <PagerStyle CssClass="headertext2"  ForeColor=White/>
            <HeaderStyle CssClass="headertext2" />
            <FooterStyle  CssClass ="headertext2" />
            <Columns>
            
            <asp:TemplateField HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                <ItemTemplate>
                    <asp:CheckBox ID="chkDoc"  runat="server" />
                    <asp:HiddenField ID="hdnId" runat="server"  Value='<%# DataBinder.Eval(Container.DataItem,"Id") %> '/>
                    <asp:HiddenField ID="hdnType" runat="server"  Value='<%# DataBinder.Eval(Container.DataItem,"Type") %> '/>
                </ItemTemplate>

<FooterStyle CssClass="headertext2"></FooterStyle>

<HeaderStyle CssClass="headertext2"></HeaderStyle>
             </asp:TemplateField>    
            <asp:TemplateField HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                <ItemTemplate>
                                
                               <asp:ImageButton ID="img" runat="server"/>
                               <asp:Label ID="lbl" runat=server></asp:Label>
                </ItemTemplate>

<FooterStyle CssClass="headertext2"></FooterStyle>

<HeaderStyle CssClass="headertext2"></HeaderStyle>
            </asp:TemplateField>     
                   
            
            
            <asp:TemplateField SortExpression="1" HeaderText="<%$ Resources:gvHdrName %>" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
            <ItemTemplate>
            <asp:HiddenField ID="hdnTitle" runat="server"  Value='<%# DataBinder.Eval(Container.DataItem,"Title") %> '/>
             <asp:HiddenField ID="hdnName" runat="server"  Value='<%# DataBinder.Eval(Container.DataItem,"Name") %> '/>
            
                <asp:HyperLink ID="linkName"    NavigateUrl="#" runat="server"></asp:HyperLink>
            </ItemTemplate>

<FooterStyle CssClass="headertext2"></FooterStyle>

<HeaderStyle CssClass="headertext2"></HeaderStyle>
            </asp:TemplateField>  
            
            
            <asp:BoundField  SortExpression="2" HeaderText="<%$ Resources:gvHdrSubject %>"  DataField="Subject" 
                    HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
<FooterStyle CssClass="headertext2"></FooterStyle>

<HeaderStyle CssClass="headertext2"></HeaderStyle>
                </asp:BoundField>
                    <asp:BoundField  SortExpression="3" HeaderText="<%$ Resources:gvHdrType %>"  DataField="DocumentType" 
                    HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
<FooterStyle CssClass="headertext2"></FooterStyle>

<HeaderStyle CssClass="headertext2"></HeaderStyle>
                </asp:BoundField>
            <asp:BoundField  SortExpression="4" HeaderText="<%$ Resources:gvHdrClass %>"  DataField="Class" 
                    HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
<FooterStyle CssClass="headertext2"></FooterStyle>

<HeaderStyle CssClass="headertext2"></HeaderStyle>
                </asp:BoundField>
            <asp:BoundField  SortExpression="5" HeaderText="<%$ Resources:gvHdrCategory %>" DataField="Category" 
                    HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
<FooterStyle CssClass="headertext2"></FooterStyle>

<HeaderStyle CssClass="headertext2"></HeaderStyle>
                </asp:BoundField>
             <asp:BoundField  SortExpression="6" HeaderText="<%$ Resources:gvHdrDateCreated %>" 
                    DataField="CreateDate" HeaderStyle-CssClass="headertext2" 
                    FooterStyle-CssClass="headertext2">
<FooterStyle CssClass="headertext2"></FooterStyle>

<HeaderStyle CssClass="headertext2"></HeaderStyle>
                </asp:BoundField>
            </Columns>
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          
        </asp:GridView>
        </td>
		</tr>
		</table>
		
		<table>
		
		<tr>
		  <td class="PadLeft4"><input type="button" class="button" onclick="CheckCheckAll();" value="Select All"></td>
      <td class="PadLeft4"><input type="button" class="button" onclick="UnselectCheckAll();" value="Unselect All"></td>
		  <% if (Model.ScreenFlag == "Files")
       {%>
     <%if (Model.Create_allowed == "1")
       {%>
      <td class="PadLeft4">
      
      <input type="button" onserverclick="GotoAddNew" value="Add New" class="button" onclick="return AddNewIntegate('<%=Model.Pid%>','<%=Model.Psid %>');">&nbsp;</td>
      <%} %>
       <%} %>
        <% if (Model.ScreenFlag != "Files")
           {%>
     <%if (Model.Att_create_allowed == "1")
       {%>
      <td class="PadLeft4">
      
      <input type="button" value="Add New" class="button" onclick="return AddNewIntegate('<%=Model.Pid%>','<%=Model.Psid %>');">&nbsp;</td>
      <%} %>
       <%} %>
       
     
     <%if (Model.Move_allowed == "1")
       {%>
      <td class="PadLeft4"><input type="button" name="" value="   Move   " class="button" onclick="return moveCheck();; "></td>
      <%} %>
     
     
        
     <%if (Model.Email_allowed == "1")
       {%>
      <td class="PadLeft4"><input type="button" name="" value="  E-Mail  " class="button" onclick="return emailCheck();; "></td>
      <%} %>
       
       
         <% if (Model.ScreenFlag == "Files")
            {%>
     <%if (Model.Delete_allowed == "1")
       {%>
      <td class="PadLeft4">
      <asp:Button 
              ID="Button2" Text="<%$ Resources:btnButton1 %>" CssClass="button" runat="server"
               onclick="btnDelete_Click" UseSubmitBehavior="true"  OnClientClick="return delCheck()"/>
      </td>
      <%} %>
       <%} %>
       
         <% if (Model.ScreenFlag != "Files")
            {%>
     <%if (Model.Att_delete_allowed == "1")
       {%>
      <td class="PadLeft4">      <asp:Button 
              ID="Button1" Text="<%$ Resources:btnButton1 %>" CssClass="button" runat="server"
               onclick="btnDelete_Click" UseSubmitBehavior="true"  OnClientClick="return delCheck()"/></td>
      <%} %>
       <%} %>
       
         <% if (Model.ScreenFlag == "Files")
            {%>
     <%if (Model.Transfer_allowed == "1")
       {%>
      <td class="PadLeft4"><input type="button" name="" value="  Transfer  " class="button" onclick="return transferCheck();; "></td>
      <%} %>
       <%} %>
       
          <% if (Model.ScreenFlag != "Files")
             {%>
     <%if (Model.Att_transfer_allowed == "1")
       {%>
      <td class="PadLeft4"><input type="button" name="" value="  Transfer  " class="button" onclick="return transferCheck();; "></td>
      <%} %>
       <%} %>
       
         <% if (Model.ScreenFlag == "Files")
            {%>
     <%if (Model.Copy_allowed == "1")
       {%>
      <td class="PadLeft4"><input type="button" name="" value="  Copy  " class="button" onclick="return copyCheck();; "></td>
      <%} %>
       <%} %>
       
           <% if (Model.ScreenFlag != "Files")
              {%>
     <%if (Model.Att_copy_allowed == "1")
       {%>
      <td class="PadLeft4"><input type="button" name="" value="  Copy  " class="button" onclick="return copyCheck();; "></td>
      <%} %>
       <%} %>
     </tr>
    </table>
   	<table border="0">
  <% if (Model.ScreenFlag == "Files")
     {%>
     <%if (Model.Createfolder_allowed == "1")
       {%>
	<%if (Model.TopFolder.FolderID == "0")
   { %>
	<%if (Model.Pid != "-1")
   { %>
     <tr>
      <td class="PadLeft4">
          <asp:Label ID="lblEnterFolderName" runat="server" Text="<%$ Resources:lblEnterFolderName %>"></asp:Label><br><input type="text" name="newfoldername" value="" id="newfoldername" runat="server">&nbsp;<asp:Button 
              ID="btnAddFolder" Text="<%$ Resources:btnCreateFolder %>" CssClass="button" runat=server 
               onclick="btnAddFolder_Click" UseSubmitBehavior="true"  OnClientClick="return createfolderCheck();"/></td>
         
     </tr>
     <%} %>
     <%} %>
		<%} %>
     <%} %>						
					
    </div>
    <asp:HiddenField ID="hdnPageNumber" runat="server" Value="0" />
    <asp:HiddenField ID="hdnOrderBy" runat="server" />
     <asp:HiddenField ID="hdocId" runat="server"/>
     <asp:HiddenField ID="hfolderId" runat="server"/>
     <asp:HiddenField ID="Psid" runat="server" Value="50" />
     <asp:HiddenField ID="flag" runat="server"/>
     <asp:HiddenField ID="FolderId" runat="server" />
      <asp:HiddenField ID="FolderName" runat="server"/>
     <asp:HiddenField ID="Regarding" runat="server" />
     <asp:HiddenField ID="AttachTableName" runat="server" />
     <asp:HiddenField ID="AttachRecordId" runat="server" />
      <asp:HiddenField ID="FormName" runat="server"/>
     <asp:HiddenField ID="hdnSortExpression" runat="server" value="1"/>
     <asp:HiddenField ID="HiddenField1" runat="server" Value="Files"/>
     </form>
     
    </div>
    
</body>
</html>
