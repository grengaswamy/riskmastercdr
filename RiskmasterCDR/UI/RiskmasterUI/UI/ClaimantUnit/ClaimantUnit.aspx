﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimantUnit.aspx.cs" Inherits="Riskmaster.UI.UI.ClaimantUnit.ClaimantUnit"
    ValidateRequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>"Unit"</title>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>  
    <script src="../../Scripts/jquery/jquery-1.8.0.js"></script>
    <script type="text/javascript">
        //function SetParentCtrl() {
        //    var valArray = [];
        //    var unitArray = [];
        //    var unitId = "";
        //    var unitType = "";
        //    var unitNum = "";
        //    var option = "";
 
        //    //var lstBox = window.opener.$('#claimantunitlookup');
        //    var objCtrl = window.opener.document.getElementById("claimantunitlookup");
        //    var vVal = $('#hdnVehicle').val();
        //    if (vVal != null && vVal != '') {
        //        valArray = vVal.split('~');
        //        for (var i = 0; i < valArray.length; i++) {
        //            unitArray = valArray[i].split('|');
        //            if (unitArray.length == 3) {
        //                unitid = unitArray[0];
        //                unitType = unitArray[1];
        //                unitNum = unitArray[2];
        //            }
        //            if (unitType == 'V') {
        //                unitNum = "VIN: " + unitNum;
        //            }
        //            else if (unitType == "P") {
        //                unitNum = "PIN: " + unitNum;
        //            }
        //            else if (unitType == "S") {
        //                unitNum = "SITE: " + unitNum;
        //            }
        //            else if (unitType == "SU") {
        //                unitNum = "STAT: " + unitNum;
        //            }
        //            window.opener.SetClaimantUnitList(unitid, unitType, unitNum);
        //        }
        //        window.close();
        //        window.opener.setDataChanged(true);
        //    }
        //    else {
        //        alert(CreateClaimantUnitVal.SelectChkBox);
        //        return false;
        //    }
        //}


        //function CloseWindow() {
        //    window.close();
        //}
        

        $(document).ready(function () {
            $('input[type=checkbox]').change(function () {
               // var UnitType = "";
                var vval = "";
                var cval = "";

               
                var sVin = $(this).closest('tr').children('td:nth-child(2)');

                vval = $('#hdnVehicle').val();
                var checkboxvalue = $(this).val().split('|');

                cval = checkboxvalue[1] + "|" + checkboxvalue[2] + "|" + sVin.text();
                
                if ($(this).is(':checked')) {
                    if ($('#hdnVehicle') && $('#hdnVehicle').val() != '') {
                        $('#hdnVehicle').val($('#hdnVehicle').val() + "~" + cval);
                    }
                    else {
                        $('#hdnVehicle').val(cval);
                    }
                }
                else {
                    vval = vval.replace(cval, "").replace('~~', '~');
                    $('#hdnVehicle').val(vval);
                }
            });
        });

        
        </script>
</head>
<body onload="ClaimantXUnitLoaded()">
    <form id="frmData" runat="server">
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <input type="hidden" name="hTabName" />
        <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
            <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" valign="middle" height="32"> 
                         <asp:ImageButton ID="ttOK" ImageUrl="~/Images/tb_OK_active.png" class="bold" ToolTip="<%$ Resources:ttOK %>"
                            OnClientClick="return SetParentCtrl()" runat="server" />                      
                    </td>
                    <td align="center" valign="middle" height="32"> 
                         <asp:ImageButton ID="ttCancel" ImageUrl="~/Images/tb_close_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                            OnClientClick="return CloseWindow()" runat="server" />                 
                    </td>
                </tr>
            </table>
        </div>
    <div class="msgHeader" colspan="" id="formtitle">
        <asp:Label ID = "lblUnits" runat = "server" Text = "<%$ Resources:lblUnits %>" />
    </div>
        <div class="tabGroup" id="TabsDivGroup" runat="server">
            <%--<div class="Selected" nowrap="true" runat="server" name="TABSVehicleUnits" id="TABSVehicleUnits">
                <a class="Selected1" href="#" onclick="tabChange(this.name);" runat="server"
                    rmxref="" name="VehicleUnits" id="LINKTABSVehicleUnits">Vehicle Units</a>
            </div>--%>
            <div class="Selected" nowrap="true" runat="server" name="TABSVehicleUnits" id="TABSVehicleUnits">
                <a class="Selected1" href="#" onclick="tabChange(this.name);" runat="server"
                    rmxref="" name="VehicleUnits" id="LINKTABSVehicleUnits"><asp:Label ID = "lblTabVehicleUnit" runat = "server" Text = "<%$ Resources:lblTabVehicleUnit %>" /></a>
            </div>
            <div class="tabSpace" runat="server" id="Div1">
                <nbsp />
                <nbsp />
            </div>
            <%--<div class="NotSelected" nowrap="true" runat="server" name="TABSPropertyUnits" id="TABSPropertyUnits">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);" runat="server"
                    rmxref="" name="PropertyUnits" id="LINKTABSPropertyUnits">Property Units</a>
            </div>--%>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSPropertyUnits" id="TABSPropertyUnits">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);" runat="server"
                    rmxref="" name="PropertyUnits" id="LINKTABSPropertyUnits"><asp:Label ID = "lblTabPropertyUnit" runat = "server" Text = "<%$ Resources:lblTabPropertyUnit %>" /></a>
            </div>
            <div class="tabSpace" runat="server" id="Div3">
                <nbsp />
                <nbsp />
            </div>
            <%--<div class="NotSelected" nowrap="true" runat="server" name="TABSSiteUnits" id="TABSSiteUnits">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);" runat="server"
                    rmxref="" name="SiteUnits" id="LINKTABSSiteUnits">Site Units</a>
            </div>--%>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSSiteUnits" id="TABSSiteUnits">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);" runat="server"
                    rmxref="" name="SiteUnits" id="LINKTABSSiteUnits"><asp:Label ID = "lblTabSiteUnit" runat = "server" Text = "<%$ Resources:lblTabSiteUnit %>" /></a>
            </div>
            <div class="tabSpace" runat="server" id="Div5">
                <nbsp />
                <nbsp />
            </div>
            <%--<div class="NotSelected" nowrap="true" runat="server" name="TABSStatUnits" id="TABSStatUnits">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);" runat="server"
                    rmxref="" name="StatUnits" id="LINKTABSStatUnits">Stat Units</a>
            </div>--%>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSStatUnits" id="TABSStatUnits">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);" runat="server"
                    rmxref="" name="StatUnits" id="LINKTABSStatUnits"><asp:Label ID = "lblTabStatUnit" runat = "server" Text = "<%$ Resources:lblTabStatUnit %>" /></a>
            </div>
            <div class="tabSpace" runat="server" id="Div7">
                <nbsp />
                <nbsp />
            </div>
        </div>
        <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 100%; height: 83%; overflow: auto;">
            <div>
            <table border="0" cellspacing="0" cellpadding="0" name="FORMTABVehicleUnits" id="FORMTABVehicleUnits" width="100%">
                <tr id="Tr2">
                    <td>
                    <asp:GridView ID="gvVehicle" runat="server" AutoGenerateColumns="false" Width="100%"
                                 AllowPaging="False" AllowSorting="True" HorizontalAlign="Left"
                                PagerSettings-Mode="NumericFirstLast" 
                                ShowHeader="true" GridLines="None" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right"
                                PageSize="20">
                                <PagerStyle CssClass="headertext2" ForeColor="White" />
                                <HeaderStyle CssClass="colheader6" ForeColor="White" HorizontalAlign="Left" />
                                <AlternatingRowStyle CssClass="data2" />
                                <Columns>
                                   <asp:TemplateField HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                                        <ItemTemplate>
                                             <input id="chkVehicle" name="chkVehicle" runat="server" type="checkbox"
                                                    value='<%# Eval("ROW_ID")%>'/>
                                        </ItemTemplate>
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6"></HeaderStyle>
                                    </asp:TemplateField>
                                     <asp:BoundField HeaderText="<%$ Resources:gvVIN %>" DataField="VIN" HeaderStyle-CssClass="msgheader" 
                                        FooterStyle-CssClass="msgheader" FooterStyle-HorizontalAlign="Left">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:gvMake %>" DataField="VEHICLE_MAKE" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:gvYear %>" DataField="VEHICLE_YEAR" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:gvModel %>" DataField="VEHICLE_MODEL" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:gvInsured %>" DataField="ISINSURED" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:gvExternal %>" DataField="EXTERNAL" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                </Columns>
                            <HeaderStyle CssClass="colheader6" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
                </div>
            <div>
            <table border="0" style="display: none;" cellspacing="0" cellpadding="0" name="FORMTABPropertyUnits" id="FORMTABPropertyUnits" width ="100%">
                <tr id="Tr1">
                    <td>
                        <asp:GridView ID="gvPropertyUnit" runat="server" AutoGenerateColumns="false" Width="100%"
                             AllowPaging="false" AllowSorting="True"
                            PagerSettings-Mode="NumericFirstLast" 
                            ShowHeader="true" GridLines="None" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right"
                            PageSize="20">
                            <PagerStyle CssClass="headertext2" ForeColor="White" />
                            <HeaderStyle CssClass="colheader6" />
                            <AlternatingRowStyle CssClass="data2" />
                            <FooterStyle CssClass="colheader6" />
                            <Columns>
                            <asp:TemplateField HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                                <ItemTemplate>
                                    <input id="chkProperty" name="chkProperty" runat="server" type="checkbox"
                                                    value='<%# Eval("ROW_ID")%>' />
                                </ItemTemplate>
                                <FooterStyle CssClass="colheader6"></FooterStyle>
                                <HeaderStyle CssClass="colheader6"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="<%$ Resources:gvPIN %>" DataField="PIN" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader" FooterStyle-HorizontalAlign="Left">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvAddr %>" DataField="ADDR1" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvCity %>" DataField="CITY" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvState %>" DataField="STATE_ID" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvInsured %>" DataField="INSURED" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvExternal %>" DataField="EXTERNAL" HeaderStyle-CssClass="msgheader"
                                    FooterStyle-CssClass="msgheader">
                                    <FooterStyle CssClass="colheader6"></FooterStyle>
                                    <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="colheader6" Font-Bold="True" ForeColor="White" />
                        </asp:GridView> 
                    </td>
                </tr>
            </table>
            </div>
            <div>
            <table border="0" style="display: none;" cellspacing="0" cellpadding="0" name="FORMTABSiteUnits" id="FORMTABSiteUnits" width ="100%">
                <tr id="">
                    <td>
                        <asp:GridView ID="gvSiteUnit" runat="server" AutoGenerateColumns="false" Width="100%"
                             AllowPaging="false" AllowSorting="True"
                            PagerSettings-Mode="NumericFirstLast" 
                            ShowHeader="true" GridLines="None" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right"
                            PageSize="20">
                            <PagerStyle CssClass="headertext2" ForeColor="White" />
                            <HeaderStyle CssClass="colheader6" />
                            <AlternatingRowStyle CssClass="data2" />
                            <FooterStyle CssClass="colheader6" />
                            <Columns>
                                <asp:TemplateField HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                                <ItemTemplate>
                                     <input id="chkSite" name="chkSite" runat="server" type="checkbox"
                                                    value='<%# Eval("ROW_ID")%>' />
                                </ItemTemplate>
                                <FooterStyle CssClass="colheader6"></FooterStyle>
                                <HeaderStyle CssClass="colheader6"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="<%$ Resources:gvSITENUMBER %>"  DataField="SITE_NUMBER" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader" FooterStyle-HorizontalAlign="Left">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvAddr %>" DataField="ADDR1" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvCity %>" DataField="CITY" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvState %>" DataField="STATE_ID" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvInsured %>" DataField="ISINSURED" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvExternal %>" DataField="EXTERNAL" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>

                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>

            </table>
                </div>
            <div>
            <table border="0" style="display: none;" cellspacing="0" cellpadding="0" name="FORMTABStatUnits" id="FORMTABStatUnits" width ="100%">
                <tr id="">
                    <td>
                        <asp:GridView ID="gvStatUnit" runat="server" AutoGenerateColumns="false" Width="100%"
                             AllowPaging="false" AllowSorting="True" 
                            PagerSettings-Mode="NumericFirstLast" 
                            ShowHeader="true" GridLines="None" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right"
                            PageSize="20">
                            <PagerStyle CssClass="headertext2" ForeColor="White" />
                            <HeaderStyle CssClass="colheader6" />
                            <AlternatingRowStyle CssClass="data2" />
                            <FooterStyle CssClass="colheader6" />
                            <Columns>
                               <asp:TemplateField HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                                <ItemTemplate>
                                    <input id="chkStat" name="chkStat" runat="server" type="checkbox"
                                                    value='<%# Eval("ROW_ID")%>' />
                                </ItemTemplate>
                                <FooterStyle CssClass="colheader6"></FooterStyle>
                                <HeaderStyle CssClass="colheader6"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="<%$ Resources:gvLastName %>" DataField="LAST_NAME" HeaderStyle-CssClass="msgheader"
                                        FooterStyle-CssClass="msgheader" FooterStyle-HorizontalAlign="Left">
                                        <FooterStyle CssClass="colheader6"></FooterStyle>
                                        <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvFirstName %>" DataField="FIRST_NAME" HeaderStyle-CssClass="msgheader"
                                    FooterStyle-CssClass="msgheader">
                                    <FooterStyle CssClass="colheader6"></FooterStyle>
                                    <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvAddr %>" DataField="ADDR1" HeaderStyle-CssClass="msgheader"
                                    FooterStyle-CssClass="msgheader">
                                    <FooterStyle CssClass="colheader6"></FooterStyle>
                                    <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvCity %>" DataField="CITY" HeaderStyle-CssClass="msgheader"
                                    FooterStyle-CssClass="msgheader">
                                    <FooterStyle CssClass="colheader6"></FooterStyle>
                                    <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvState %>" DataField="STATE_ID" HeaderStyle-CssClass="msgheader"
                                    FooterStyle-CssClass="msgheader">
                                    <FooterStyle CssClass="colheader6"></FooterStyle>
                                    <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvInsured %>" DataField="ISINSURED" HeaderStyle-CssClass="msgheader"
                                    FooterStyle-CssClass="msgheader">
                                    <FooterStyle CssClass="colheader6"></FooterStyle>
                                    <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="<%$ Resources:gvExternal %>" DataField="EXTERNAL" HeaderStyle-CssClass="msgheader"
                                    FooterStyle-CssClass="msgheader">
                                    <FooterStyle CssClass="colheader6"></FooterStyle>
                                    <HeaderStyle CssClass="colheader6" HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <div>

    </div>
        <asp:HiddenField  runat= "server" ID="hdnVehicle" value=""/>
    </form>
</body>
</html>
