﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Data;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.BusinessHelpers;
using Riskmaster.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.ClaimantUnit
{
    public partial class ClaimantUnit : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            DataSet oSearchRecords = new DataSet();
            //string sPolCode = "";
            string Data = "";
            bool bReturnStatus = false;
            string sClaimID = string.Empty;
            string sClaimantEid = string.Empty;
            string PageID = RMXResourceManager.RMXResourceProvider.PageId("ClaimantUnit.aspx");
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();

            try
            {
                string sCulture = AppHelper.GetCulture().ToString();

                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ClaimantUnit.aspx"), "CreateClaimantUnitVal", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "CreateClaimantUnitVal", sValidationResources, true);
                if (!IsPostBack)
                {
                    sClaimID = AppHelper.GetQueryStringValue("ClaimId");
                    sClaimantEid = AppHelper.GetQueryStringValue("ClaimantEId");
                }
                XmlTemplate = GetMessageTemplate(sClaimID, sClaimantEid);
                bReturnStatus = CallCWS("ClaimantUnitAdaptor.GetUnitList", XmlTemplate, out Data, false, false);

                

                if (Data != null)
                {
                    XmlDocument objd = new XmlDocument();
                    objd.LoadXml(Data);

                     //TABSPropertyUnits.Disabled;
                    
                //    oSearchRecords.ReadXml(Data);
                    oSearchRecords.ReadXml(new XmlNodeReader(objd));
                    gvVehicle.DataSource = oSearchRecords.Tables["VehicleDetail"];
                    if ((oSearchRecords.Tables["VehicleDetail"] != null) && (oSearchRecords.Tables["VehicleDetail"].Rows.Count > 0))
                    {
                        gvVehicle.DataBind();
                    }
                    else
                        TABSVehicleUnits.Visible = false;
                   

                    gvPropertyUnit.DataSource = oSearchRecords.Tables["PropertyDetail"];
                    if ((oSearchRecords.Tables["PropertyDetail"]!=null) &&(oSearchRecords.Tables["PropertyDetail"].Rows.Count > 0))
                    {
                        gvPropertyUnit.DataBind();
                    }
                    else
                        TABSPropertyUnits.Visible=false;
                       
                    gvSiteUnit.DataSource = oSearchRecords.Tables["SiteUnitDetail"];
                    if ((oSearchRecords.Tables["SiteUnitDetail"] != null) && (oSearchRecords.Tables["SiteUnitDetail"].Rows.Count > 0))
                    {
                        gvSiteUnit.DataBind();
                    }
                    else
                        TABSSiteUnits.Visible=false;
                       
                           
                    gvStatUnit.DataSource = oSearchRecords.Tables["StatUnitDetail"];
                    if ((oSearchRecords.Tables["StatUnitDetail"] != null) && (oSearchRecords.Tables["StatUnitDetail"].Rows.Count > 0))
                    {
                        gvStatUnit.DataBind();
                    }
                    else
                        TABSStatUnits.Visible=false;

                    //if ((oSearchRecords.Tables["VehicleDetail"] == null) && (oSearchRecords.Tables["VehicleDetail"].Rows.Count < 0) &&
                     //  ((oSearchRecords.Tables["PropertyDetail"] == null) && (oSearchRecords.Tables["PropertyDetail"].Rows.Count < 0)) &&
                      //  (oSearchRecords.Tables["SiteUnitDetail"] == null) && (oSearchRecords.Tables["SiteUnitDetail"].Rows.Count < 0) &&
                       // (oSearchRecords.Tables["StatUnitDetail"] == null) && (oSearchRecords.Tables["StatUnitDetail"].Rows.Count < 0))
                    //{
                       // err.Add("DataIntegrator.Error", AppHelper.GetResourceValue(PageID, "NoUnitRecords", "3"), BusinessAdaptorErrorType.Message);
                    //}

                }
            }
            catch(Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors error = new BusinessAdaptorErrors();
                error.Add(objException, BusinessAdaptorErrorType.SystemError);
            }
        }

        private XElement GetMessageTemplate(string sClaimId, string sClaimantEID)
        {
         //   StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("ClaimantUnit.GetUnitList");
            sXml = sXml.Append("</Function></Call><Document><AddCode>");
            sXml = sXml.Append("<ClaimId>" + sClaimId + "</ClaimId>");
            sXml = sXml.Append("<ClaimantEid>" + sClaimantEID + "</ClaimantEid>");
            sXml = sXml.Append("</AddCode></Document></Message>");
            oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}