﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailDocument.aspx.cs"
    Inherits="Riskmaster.UI.EmailDocuments.EmailDocument" %>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register src="../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Send Message Regarding</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/cul.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../Scripts/emaildocuments.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../Scripts/CULscrollabletable.js" type="text/javascript"></script>

    <script language="JavaScript">

        function PageLoaded(i) {
            document.forms[0].method = "post";
            if (document.forms[0].emailinprogress.value == 'true')
                document.forms[0].emailsent.value = 'true';
            if (document.forms[0].emailsent.value == 'true') {
                alert("Email Sent Successfully");
                window.close();
            }
            return false;
        }				
        function EmailUsingOutlook() {
            
    if (confirm("Please make sure that Outlook is running before clicking OK button")) {

    }
    else {

        return false;
    }
    pleaseWait.Message = "Opening Outlook Window.........";
    pleaseWait.Show();
    document.getElementById('hdnEmailButtonClick').value = "1";
    document.forms[0].submit();
    return true;
}
function CheckAndOpenOutlook()
{
    if(document.getElementById('hdnEmailButtonClick').value == "1") {
        if (document.forms[0].IsActiveSummary.checked) {
            SaveFileAndOpenOutlook();
        }
        else {
            SendAttach("");
        }
        self.close();
    }
//    document.getElementById('hdnEmailButtonClick').value = "0";
    
}
function SaveFileAndOpenOutlook()
{
var	fso;
var strForm;
var strFile;
var template = (document.getElementById('hdnformfilecontent').value);
fso = new ActiveXObject("Scripting.FileSystemObject");
strForm = fso.GetSpecialFolder(2).Path + "\\" + "ExecutiveSummaryReport.pdf";
StreamAndSave(template, strForm)
//self.close();
}
function SendAttach(strAttachPath) {
    
	var theApp	//Reference to Outlook.Application 
	var theMailItem	//Outlook.mailItem
    //Attach Files to the email
    var attach1 = strAttachPath;
    if (document.forms[0].EmailSubject != null) {
        var subject = document.forms[0].EmailSubject.value;
    }
    if (document.forms[0].emailidto != null) {
        var emailidto = document.forms[0].emailidto.value;

    }
    if (document.forms[0].emailidcc != null) {
        var emailidcc = document.forms[0].emailidcc.value;
    }
    if (document.forms[0].body != null) {
        var body = document.forms[0].body.value;
    }
    
   
    //Create a object of Outlook.Application
    try
    {
         var theApp = new ActiveXObject("Outlook.Application")
            
	var theMailItem = theApp.CreateItem(0)
	theMailItem.to = (emailidto);
	theMailItem.cc = (emailidcc);
	theMailItem.Body = (body);
	theMailItem.Subject = (subject);
	if(attach1 != "")
	{
	theMailItem.Attachments.Add(attach1)
	}
	
	//Show the mail 
	theMailItem.display()
	}
    catch(err)
    {
    alert("The following may have cause this error: \n"+
     "1. The Outlook 2007 is not installed on the machine.\n"+
     "2. The msoutl.olb is not availabe at the location "+
     "C:\\Program Files\\Microsoft Office\\OFFICE11\\msoutl.old on client's machine "+
     "due to bad installation of the office 2007."+
     "Re-Install office 2007 with default settings.\n"+
     "3. The Initialize and Scripts ActiveX controls not marked as safe is not set to enable.")
    
    }
 } 			
    </script>

</head>
<body class="10pt" onload="PageLoaded();CheckAndOpenOutlook()">
    <form id="frmData" name="frmData" method="post" runat="server">
     <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage=""/>
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <table border="0" width="100%">
        <tr>
            <td class="ctrlgroup" colspan="8">
                New Memo
            </td>
        </tr>
        <tr>
            <td align="right" width="10%">
                <u>To:</u>
            </td>
            <td valign="bottom" width="80%">
                <input type="text" name="emailidto" value="" runat="server" id="emailidto" size="75" />
            </td>
            <td>
                <input type="image" src="../../Images/userlist.gif" alt="Pick User" id="culUsersTo "
                    class="LightBold" onclick="return AddCustomizedListUser('emaildocs','emailidto','','');" />
            </td>
        </tr>
        <tr>
            <td align="right" width="10%">
                <u>cc:</u>
            </td>
            <td width="80%">
                <input type="text" value="" runat="server" id="emailidcc" size="75">
            </td>
            <td>
                <input type="image" src="../../Images/userlist.gif" alt="Pick User" id="culUsersCc"
                    class="LightBold" onclick="return AddCustomizedListUser('emaildocs','emailidcc','','');" />
            </td>
            <!--src="/oxf/csc-Theme/riskmaster/img/userlist.gif"-->
        </tr>
        <tr>
            <td align="right" width="10%">
                Regarding:
            </td>
            <td width="80%">
                <input type="text" value="" disabled="true" id="regarding" size="75" runat="server">
            </td>
        </tr>
        <tr>
            <td align="right" width="10%">
                Subject:
            </td>
            <td width="80%">
                <input type="text" runat="server" id="EmailSubject" value="" size="75" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right" width="10%">
                <asp:CheckBox runat="server" ID="IsActiveSummary" rmxref="/Instance/Document/EmailDocuments/AttachExecutiveSummaryReport[ @name = 'summary']" /><!-- abansal23 :  MITS 15688 -->
            </td>
            <td align="left" width="80%">
                Attach Executive Summary Report
            </td>
        </tr>
        <tr>
            <td valign="top" align="right" width="10%">
                Body:
            </td>
            <td width="80%">
                <textarea runat="server" cols="75" wrap="soft" rows="15" id="body"></textarea>
            </td>
        </tr>
        <tr>
            <td nowrap="">
                &nbsp;
            </td>
            <td>
                <asp:Button name="E-Mail-Doc" ID="btnEmail" Text="  Send  " class="button" OnClientClick="return Validate();"
                    runat="server" OnClick="btnEmail_Click" />
                      <asp:Button name="E-Mail-Outlook" ID="btnOutlook" Text="Send Using Outlook" class="button" OnClientClick="return EmailUsingOutlook();"
                    runat="server" OnClick="btnOutlook_Click" />
                <input type="submit" value=" Cancel " class="button" onclick="window.close();" />
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="hdnemailto" rmxref="Instance/Document/EmailDocuments/EmailIdsTo"
        rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="hdnemailcc" rmxref="Instance/Document/EmailDocuments/EmailIdsCc"
        rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="hdnemailsubject" rmxref="Instance/Document/EmailDocuments/Subject"
        rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="hdnmessage" rmxref="Instance/Document/EmailDocuments/Message"
        rmxtype="hidden" />
        <!-- abansal23: MITS 15688 on 4/22/2009 Starts -->
         <asp:TextBox Style="display: none" runat="server" ID="hdnFormName" rmxref="/Instance/Document/EmailDocuments/AttachExecutiveSummaryReport[ @name = 'summary']/@FormName"
        rmxtype="hidden" />
         <asp:TextBox Style="display: none" runat="server" ID="hdmRecordId" rmxref="/Instance/Document/EmailDocuments/AttachExecutiveSummaryReport[ @name = 'summary']/@AttachRecordId"
        rmxtype="hidden" />
        <!-- abansal23: MITS 15688 on 4/22/2009 Ends -->
       
        <asp:HiddenField ID="hdnEmailButtonClick" runat="server" />
         <asp:HiddenField ID="hdnformfilecontent" runat="server" />
    <input type="hidden" value="false" id="emailsent" />
    <input type="hidden" value="false" id="emailinprogress" />
    <input type="hidden" id="totalusers" value="0" />
    </form>
</body>
</html>
