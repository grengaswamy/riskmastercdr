﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;//abansal23: MITS 15688 on 4/22/2009
using System.Xml.XPath;
using System.ServiceModel;
using Riskmaster.BusinessHelpers;
//using Riskmaster.UI.DocumentService;
using Riskmaster.Models;
namespace Riskmaster.UI.EmailDocuments
{
    public partial class EmailDocument : NonFDMBasePageCWS
    {
        XmlDocument XmlDoc = new XmlDocument();//abansal23: MITS 15688 on 4/22/2009
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if(Request.QueryString["Regarding"]!=null)
                {

                    regarding.Value = Request.QueryString["Regarding"].ToString();
                    EmailSubject.Value = "Re: " + Request.QueryString["Regarding"].ToString();
                }
                if (Request.QueryString["FormName"] != null)
                {
                    hdnFormName.Text = Request.QueryString["FormName"].ToString();//abansal23: MITS 15688 on 4/22/2009
                }
                if (Request.QueryString["AttachRecordId"] != null)
                {
                    hdmRecordId.Text = Request.QueryString["AttachRecordId"].ToString();//abansal23: MITS 15688 on 4/22/2009
                }

                DocumentBusinessHelper dc = new DocumentBusinessHelper();
                DocumentType document = new DocumentType();
                bool bIsOutlookSettingEnabled = dc.CheckOutlookSetting(document);
                if (!bIsOutlookSettingEnabled)
                {
                    btnOutlook.Visible = false;
                }
            }
        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            //abansal23: MITS 15688 on 4/22/2009 Starts
            try
            {
                bool bReturnStatus = false;
                string sreturnValue = "";

                hdnemailto.Text = emailidto.Value;
                hdnemailcc.Text = emailidcc.Value;
                hdnemailsubject.Text = EmailSubject.Value;
                hdnmessage.Text = body.Value;
                bReturnStatus = CallCWSFunction("EmailDocumentsAdaptor.EmailDocuments", out sreturnValue);
                XmlDoc.LoadXml(sreturnValue);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    //After the validation,there was a need to post back the parent page through javascript 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "SuccessClose();", true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            //abansal23: MITS 15688 on 4/22/2009 Ends 
        }  
        protected void btnOutlook_Click(object sender, EventArgs e)
        {
            

            bool bReturnStatus = false;
            string sreturnValue = string.Empty;
            int  iAttachRecordId = 0;
            try
            {
                if (IsActiveSummary.Checked)
                {
                    DocumentBusinessHelper dc = new DocumentBusinessHelper();
                    DocumentType document = new DocumentType();
                    if (int.TryParse(hdmRecordId.Text, out iAttachRecordId))
                    {
                        document.AttachRecordId = iAttachRecordId;
                    }
                    document.AttachTable = hdnFormName.Text;
                    document = dc.GetExecutiveSummaryFileDetails(document);
                    if (Page.FindControl("hdnformfilecontent") != null)
                    {
                        HiddenField hdnFile = (HiddenField)Page.FindControl("hdnformfilecontent");
                        hdnFile.Value = Convert.ToBase64String(document.FileContents);
                    }
                }
               
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
