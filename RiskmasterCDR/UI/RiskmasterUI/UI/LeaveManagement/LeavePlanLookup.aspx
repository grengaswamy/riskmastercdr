﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeavePlanLookup.aspx.cs" Inherits="Riskmaster.UI.LeavePlanLookup.LeavePlanLookup" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Leave Plan</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
</head>
<body class="10pt" onunload="handleUnload()" onload="handleOnload()">
<form id="frmData" runat="server">
 <script language="JavaScript" type="text/javascript">
	function handleUnload()
	{
        if(window.opener!=null)
		    window.opener.onCodeClose();
	}
	function selectPlan(sLPRowId, sPlanNumber, sPlanName,sUseLeavesFlag)
	{
		if(window.opener!=null)
			window.opener.setLeavePlan(sLPRowId, sPlanNumber, sPlanName,sUseLeavesFlag);
	}	
	function handleOnload()
	{
		window.opener.setPlanFieldName();
	}						
    </script>   
      <!-- abansal23: MITS 14649 Starts -->
    <div id="MainDiv" runat="server" visible="false">
        <table width="98%" border="0" align="center" class="singleborder">
            <thead>
                <tr>
                    <td class="ctrlgroup">
                        Plan Number
                    </td>
                    <td class="ctrlgroup">
                        Plan Name
                    </td>
                    <td class="ctrlgroup">
                        Eff. Date (From &amp; To)
                    </td>
                </tr>
            </thead>
            <%int i = 0; foreach (XElement item in result)
              {
                  string colclass = "";
                  if ((i % 2) == 1) colclass = "datatd1";
                  else colclass = "datatd";
                  i++;
            %>
            <tr>
                <td class="<%=colclass%>">
                    <%=item.Attribute("PlanNumber").Value%>
                </td>
                <td class="<%=colclass%>">
                    <a class="LightBold" href="#" onclick="selectPlan('<%=item.Attribute("LPRowId").Value%>', '<%=escapeStrings(item.Attribute("PlanNumber").Value)%>','<%=escapeStrings(item.Attribute("PlanName").Value)%>','<%=item.Attribute("UseLeavesFlag").Value%>');">
                        <%=item.Attribute("PlanName").Value%></a><!-- abansal23 on 4/23/2009 : for cases of plan name containing (') -->
                </td>
                <td class="<%=colclass%>">
                    <%=item.Attribute("EffDate").Value%>
                </td>
            </tr>
            <%}%>
        </table>
        <input type="button" class="button" value="Close" onclick="window.close();" />
        <div>
            <asp:TextBox Style="display: none" runat="server" ID="empid" rmxref="Instance/Document/LeavePlan/EmpEid"
                rmxtype="hidden" />
            <asp:TextBox Style="display: none" runat="server" ID="eligdate" rmxref="Instance/Document/LeavePlan/EligibilityDt"
                rmxtype="hidden" />
            <uc1:ErrorControl ID="ErrorControl" runat="server" />
        </div>
    </div>
    <div id="ChildDiv" runat="server" visible="true">
    <p class="gen" align="center"><br /><br /><br /><h3 align="center">No plans could be found for this employee!</h3>
    </div>
    <!-- abansal23: MITS 14649 Ends -->
    </form>
</body>
</html>
