﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PSOWebform.aspx.cs" Inherits="Riskmaster.UI.PSOForm.PSOWebform" MaintainScrollPositionOnPostback="true" ValidateRequest="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register tagprefix="uc2" Tagname="CodeLookUp"  src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register tagprefix="uc"  tagname="MultiCode" src="~/UI/Shared/Controls/MultiCodeLookup.ascx"   %>
<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register tagprefix="uc3" tagname="PleaseWaitDialog" src="~/UI/Shared/Controls/PleaseWaitDialog.ascx"   %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Patient Safety Event Report</title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
     <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
</head>
    <%--vkumar258 - RMA-6037 - Starts --%>
<%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; } </script>   
<script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>
<script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script> --%>
<link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
<link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
<script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
<script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
<script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
<%--vkumar258 - RMA-6037 - End --%>
<script type="text/javascript" language="JavaScript" src="../../Scripts/pso.js"></script>
<script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
<script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">    { var i; }</script>
<script language="JavaScript" type="text/javascript">
    window.onbeforeunload = confirmExit;

    function confirmExit() {
        if (document.getElementById('savebtnposted').value == "true") {
            return;
        }
        if (document.getElementById('posted').value != "true") {
            if (document.getElementById("SysPageDataChanged").value != "") {
                return "Any unsaved information you have entered will be lost.";
            }
        }
    }
</script>
<body class="" onload="CheckHiddenContrls();">
 <form id="frmData" runat="server"> 
 <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
 <div class="msgheader" id="pnlHealthCareForm_header">Healthcare Event Reporting Form(HERF)</div>
 <div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" EnableTheming="True" ShowMessageBox="false" />
</div> 
 <div id="pnlHealthCareForm" runat="server">
 <table border="0" cellspacing="0" cellpadding="0" id="pnlHealthCareForm_table" runat="server" width="100%">
     <tr id="psocdWhatReported_ctlRow" runat="server">
         <td width="60%">
              <asp:Label ID="psocdWhatReported_Title" runat="server" Text="What is being reported?" class="required"></asp:Label>
         </td>
         <td width="40%"> 
              <uc2:CodeLookUp runat="server" ID="psocdWhatReported" CodeTable="PSO_SAFETY_EVT_TYP" ControlName="psocdWhatReported" RMXRef="/Instance/Document/Event/EventPSO/psocdWhatReported" RMXType="code" />            
         </td>
     </tr>
     <%--<tr id="psocdEvidenceReport_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdEvidenceReport_ctlRow" runat="server">
         <td>      
            <asp:Label ID="psocdEvidenceReport_Title" runat="server" Text="Was there any evidence of harm to a patient at the time of this report?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdEvidenceReport" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdEvidenceReport" RMXRef="/Instance/Document/Event/EventPSO/psocdEvidenceReport" RMXType="code"  />             
         </td> 
     </tr>
     <%--<tr id="dtEvtDiscoveryDate_ctlRow" runat="server" style="display:none">--%>
     <tr id="dtEvtDiscoveryDate_ctlRow" runat="server" >
         <td>
            <asp:Label ID="dtEvtDiscoveryDate_Title" runat="server" Text="Event Discovery Date" class="required"></asp:Label>
         </td>
         <td>
            <asp:TextBox runat="server" FormatAs="date" ID="dtEvtDiscoveryDate" RMXRef="/Instance/Document/Event/EventPSO/dtEvtDiscoveryDate"
                        RMXType="date"  onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" ReadOnly="true" BackColor="#F2F2F2"/>            
         </td> 
     </tr>
     <%--<tr id="tmDiscoveryTime_ctlRow" runat="server" style="display:none">--%>
     <tr id="tmDiscoveryTime_ctlRow" runat="server">
         <td>
            <asp:Label ID="tmDiscoveryTime_Title" runat="server" Text="Event Discovery Time"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="tmDiscoveryTime" value="" Text="" rmxref="/Instance/Document/Event/EventPSOData/tmDiscoveryTime" size="10" onblur="timeLostFocus(this.id);" onchange="setDataChanged(true);" RMXType="time" runat="server" FormatAs="time" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
         </td> 
     </tr>
    <%-- <tr id="dtEvtDiscoveryDateNM_ctlRow" runat="server" style="display:none">--%>
     <tr id="dtEvtDiscoveryDateNM_ctlRow" runat="server" >
         <td>
            <asp:Label ID="dtEvtDiscoveryDateNM_Title" runat="server" Text="Event Discovery Date"></asp:Label>
         </td>
         <td>
            <asp:TextBox runat="server" FormatAs="date" ID="dtEvtDiscoveryDateNM" RMXRef="/Instance/Document/Event/EventPSO/dtEvtDiscoveryDateNM"
                        RMXType="date"  onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" ReadOnly="true" BackColor="#F2F2F2"/>           
         </td> 
     </tr>
     <%--<tr id="tmDiscoveryTimeNM_ctlRow" runat="server" style="display:none">--%>
     <tr id="tmDiscoveryTimeNM_ctlRow" runat="server">
         <td>
            <asp:Label ID="tmDiscoveryTimeNM_Title" runat="server" Text="Event Discovery Time"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="tmDiscoveryTimeNM" value="" Text="" rmxref="/Instance/Document/Event/EventPSOData/tmDiscoveryTimeNM" size="10" onblur="timeLostFocus(this.id);" onchange="setDataChanged(true);" RMXType="time" runat="server" FormatAs="time" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
         </td> 
     </tr>
     <%--<tr id="txtEventDescription_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtEventDescription_ctlRow" runat="server" >
         <td>
            <asp:Label ID="txtEventDescription_Title" runat="server" Text="Briefly describe the event that occurred or unsafe condition" class="required"></asp:Label>
         </td>
         <td> 
             <span class="formw">
             <asp:TextBox runat="Server" id="txtEventDescription" RMXRef="/Instance/Document/Event/EventPSOData/txtEventDescription" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
             <%--<input type="button" class="button" value="..." name="btnEventDescription" id="btnEventDescription" onclick="EditMemo('txtEventDescription','')" />--%>
             </span>
         </td>              
     </tr>   
     <%--<tr id="txtLocationDescription_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtLocationDescription_ctlRow" runat="server">
         <td>
            <asp:Label ID="txtLocationDescription_Title" runat="server" Text="Briefly describe the location where the event occurred or where the unsafe condition exists"></asp:Label>
         </td>
         <td> 
             <span class="formw">
             <asp:TextBox runat="Server" id="txtLocationDescription" RMXRef="/Instance/Document/Event/EventPSOData/txtLocationDescription" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3"/>
             </span>
         </td>              
     </tr>
     <%--<tr id="dtReportDate_ctlRow" runat="server" style="display:none">--%>
     <tr id="dtReportDate_ctlRow" runat="server" >
         <td>
             <asp:Label ID="dtReportDate_Title" runat="server" Text="Report Date"></asp:Label>
         </td>
         <td>             
             <asp:TextBox runat="server" FormatAs="date" ID="dtReportDate" RMXRef="/Instance/Document/Event/EventPSOData/dtReportDate"
                        RMXType="date"  onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" ReadOnly="true" BackColor="#F2F2F2"/>                   
         </td> 
     </tr>
     <%--<tr id="psocdIdentifyReporter_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdIdentifyReporter_ctlRow" runat="server">
         <td>
             <asp:Label ID="psocdIdentifyReporter_Title" runat="server" Text="Anonymous Reporter" class="required"></asp:Label>
         </td>
         <td> 
             <uc2:CodeLookUp runat="server" ID="psocdIdentifyReporter" CodeTable="YES_NO" ControlName="psocdIdentifyReporter" RMXRef="/Instance/Document/Event/EventPSOData/psocdIdentifyReporter" RMXType="code" Enabled="false" />             
         </td> 
     </tr>
     <%--<tr id="txtReporterName_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtReporterName_ctlRow" runat="server" >
         <td>
            <asp:Label ID="txtReporterName_Title" runat="server" Text="Reporter’s Name"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="txtReporterName" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/Event/EventPSOData/txtReporterName" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
         </td>              
     </tr>
     <%--<tr id="txtReporterPhoneNumber_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtReporterPhoneNumber_ctlRow" runat="server" >
         <td>
            <asp:Label ID="txtReporterPhoneNumber_Title" runat="server" Text="Telephone Number"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="txtReporterPhoneNumber" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/Event/EventPSOData/txtReporterPhoneNumber" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
         </td>              
     </tr>
     <%--<tr id="txtReporterEmail_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtReporterEmail_ctlRow" runat="server" >
         <td>
            <asp:Label ID="txtReporterEmail_Title" runat="server" Text="Email Address"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="txtReporterEmail" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/Event/EventPSOData/txtReporterEmail" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
         </td>              
     </tr>
     <%--<tr id="txtReporterJobPosition_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtReporterJobPosition_ctlRow" runat="server" >
         <td>
            <asp:Label ID="txtReporterJobPosition_Title" runat="server" Text="Reporter’s Job or Position"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="txtReporterJobPosition" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/Event/EventPSO/txtReporterJobPosition" onchange="setDataChanged(true);" MaxLength="100" ></asp:TextBox>
         </td>              
     </tr>
     <%--<tr id="txtPatientNum_ctlRow" runat="server" style="display:none">
         <td>
            <asp:Label ID="txtPatientNum_Title" runat="server" Text="How many patients did the incident reach"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="txtPatientNum" runat="server" RMXType="numeric" size="30" onblur="numLostFocus(this);" RMXRef=""></asp:TextBox>
         </td>              
     </tr>--%> 
     <%--<tr id="psocdEventcategory_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdEventcategory_ctlRow" runat="server" style="visibility:visible">
         <td>
            <asp:Label ID="psocdEventcategory_Title" runat="server" Text="Which of the following categories are associated with the event or unsafe condition?" class="required"></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdEventcategory" CodeTable="PSO_EVENT_CAT" ControlName="psocdEventcategory" RMXRef="/Instance/Document/Event/EventPSO/psocdEventcategory" RMXType="code"  />             
         </td> 
     </tr>
     <%--<tr id="psocdEventcategoryNM_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdEventcategoryNM_ctlRow" runat="server" style="visibility:visible">
         <td>
            <asp:Label ID="psocdEventcategoryNM_Title" runat="server" Text="Which of the following categories are associated with the event or unsafe condition?" class="required"></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdEventcategoryNM" CodeTable="PSO_EVENT_CAT" ControlName="psocdEventcategoryNM" RMXRef="/Instance/Document/Event/EventPSO/psocdEventcategoryNM" RMXType="code"  Filter=" CODES.SHORT_CODE NOT IN(\'A48\',\'A51\',\'A57\',\'A60\') " />             
         </td> 
     </tr> 
     <%--<tr id="psocdEventcategoryUSC_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdEventcategoryUSC_ctlRow" runat="server" style="visibility:visible">
         <td>
            <asp:Label ID="psocdEventcategoryUSC_Title" runat="server" Text="Which of the following categories are associated with the event or unsafe condition?" class="required"></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdEventcategoryUSC" CodeTable="PSO_EVENT_CAT" ControlName="psocdEventcategoryUSC" RMXRef="/Instance/Document/Event/EventPSO/psocdEventcategoryUSC" RMXType="code" Filter=" CODES.SHORT_CODE NOT IN(\'A48\',\'A51\',\'A57\',\'A60\',\'A63\') " />             
         </td> 
     </tr> 
     <tr id="txtOtherEventcategory_ctlRow" runat="server"  style="visibility:visible">
         <td>
            <asp:Label ID="txtOtherEventcategory_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
         </td>
         <td> 
             <span class="formw">
             <asp:TextBox runat="Server" id="txtOtherEventcategory" RMXRef="/Instance/Document/Event/EventPSO/txtOtherEventcategory" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
             <input type="button" class="button" value="..." name="btnOtherEventcategory" id="btnOtherEventcategory" onclick="EditMemo('txtOtherEventcategory','')" />
             </span>
         </td>              
     </tr>      
</table>
</div>
<div class="msgheader" id="pnlPIF_header" runat="server">Patient Information</div>
<div id="pnlPIF" runat="server">
<table border="0" cellspacing="0" cellpadding="0" id="pnlPIF_table" width="100%" runat="server">
     <tr id="txtPatientName_ctlRow" runat="server">
         <td>
            <asp:Label ID="txtPatientName_Title" runat="server" Text="Patient’s Name"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="txtPatientName" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/Event/PatientPSOData/txtPatientName" ReadOnly="true" BackColor="#F2F2F2" ></asp:TextBox>
         </td>              
     </tr>  
    <tr id="txtPatientDOB_ctlRow" runat="server">
         <td>
            <asp:Label ID="txtPatientDOB_Title" runat="server" Text="Patient’s Date of Birth"></asp:Label>
         </td>
         <td>            
             <asp:TextBox runat="server" FormatAs="date" ID="txtPatientDOB" RMXRef="/Instance/Document/Event/PatientPSOData/txtPatientDOB"
                        RMXType="date"  onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" ReadOnly="true" BackColor="#F2F2F2"/>
         </td> 
     </tr> 
     <tr id="txtMedicalRecordNo_ctlRow" runat="server">
         <td>
            <asp:Label ID="txtMedicalRecordNo_Title" runat="server" Text="Medical Record #"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="txtMedicalRecordNo" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/Event/PatientPSOData/txtMedicalRecordNo" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
         </td>              
     </tr>  
     <tr id="cdPatientGender_ctlRow" runat="server">
         <td>
             <asp:Label ID="cdPatientGender_Title" runat="server" Text="Patient’s Gender"></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdPatientGender" CodeTable="SEX_CODE" ControlName="cdPatientGender" RMXRef="/Instance/Document/Event/PatientPSOData/cdPatientGender" RMXType="code"  Enabled="false"/>             
         </td> 
     </tr>
     <tr id="psocdPatientAge_ctlRow" runat="server">
        <td width="60%">
            <asp:Label ID="psocdPatientAge_Title" runat="server" Text="At the time of the event what was the patient’s age?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdPatientAge" CodeTable="PSO_PAT_AGE" ControlName="psocdPatientAge" RMXRef="/Instance/Document/Event/PatientPSO/psocdPatientAge" RMXType="code" />            
        </td> 
     </tr>
     <tr id="cdPatientEthnicity_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdPatientEthnicity_Title" runat="server" Text="Is the patient’s ethnicity Hispanic or Latino?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdPatientEthnicity" CodeTable="PSO_ETHNIC" ControlName="cdPatientEthnicity" RMXRef="/Instance/Document/Event/PatientPSO/cdPatientEthnicity" RMXType="code"  />             
         </td> 
     </tr>
     <tr id="cdPatientRace_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdPatientRace_Title" runat="server" Text="What is the patient’s race?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdPatientRace" CodeTable="PSO_RACE" ControlName="cdPatientRace" RMXRef="/Instance/Document/Event/PatientPSO/cdPatientRace" RMXType="code"  />             
         </td> 
     </tr>
     <tr id="cdDiagnosisCode_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdDiagnosisCode_Title" runat="server" Text="Enter the patient’s ICD-9-CM principal diagnosis code at discharge (if available)" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdDiagnosisCode" CodeTable="CDC_DIAGNOSIS_CODE" ControlName="cdDiagnosisCode" RMXRef="/Instance/Document/Event/PatientPSO/cdDiagnosisCode" RMXType="code"  />             
         </td>              
     </tr>
     <tr id="cdExtentHarm_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdExtentHarm_Title" runat="server" Text="After discovery of the incident, what was the extent of harm to the patient (i.e., extent to which the patient’s functional ability is expected to be impaired subsequent to the incident and any attempts to minimize adverse consequences)?" class=""></asp:Label>
         </td>
         <td>
            <uc2:CodeLookUp runat="server" ID="cdExtentHarm" CodeTable="PSO_HARM_EXT" ControlName="cdExtentHarm" RMXRef="/Instance/Document/Event/PatientPSO/cdExtentHarm" RMXType="code" />             
         </td>              
     </tr>
     <tr id="cdAssessedHarm_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdAssessedHarm_Title" runat="server" Text="Approximately when after discovery of the incident was harm assessed?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdAssessedHarm" CodeTable="PSO_HARM_TIME" ControlName="cdAssessedHarm" RMXRef="/Instance/Document/Event/PatientPSO/cdAssessedHarm" RMXType="code"  />             
         </td> 
     </tr>
     <tr id="psocdRescuePatient_ctlRow" runat="server">
         <td>
            <asp:Label ID="psocdRescuePatient_Title" runat="server" Text="Was any intervention attempted in order to 'rescue' the patient (i.e., to prevent, to minimize, or to reverse harm)?" class="required"></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdRescuePatient" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdRescuePatient" RMXRef="/Instance/Document/Event/PatientPSO/psocdRescuePatient" RMXType="code"  />             
         </td> 
     </tr>
     
     <%--<tr id="psolstRescuePerform_ctlRow" runat="server" style="display:none">--%>
     <tr id="psolstRescuePerform_ctlRow" runat="server" >
         <td>
            <asp:Label ID="psolstRescuePerform_Title" runat="server" Text="Which of the following interventions (rescue) were performed?" class=""></asp:Label>
         </td>
         <td> 
            <uc:MultiCode runat="server" ID="psolstRescuePerform" width="200px" Height="88px" CodeTable="PSO_RESCUE_INTV" ControlName="psolstRescuePerform" RMXRef="/Instance/Document/Event/PatientPSO/psolstRescuePerform" RMXType="codelist" />
         </td> 
     </tr>
     <%--<tr id="txtOtherRescuePerform_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtOtherRescuePerform_ctlRow" runat="server" >
         <td>
            <asp:Label ID="txtOtherRescuePerform_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
         </td>
         <td> 
             <span class="formw">
                 <asp:TextBox runat="Server" id="txtOtherRescuePerform" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherRescuePerform" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                 <input type="button" class="button" value="..." name="btnOtherRescuePerform" id="btnOtherRescuePerform" onclick="EditMemo('txtOtherRescuePerform','')" />
             </span>
         </td>              
     </tr>
     <tr id="cdResultLength_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdResultLength_Title" runat="server" Text="Did, or will, the incident result in an increased length of stay?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdResultLength" CodeTable="PSO_INC_LEN_STAY" ControlName="cdResultLength" RMXRef="/Instance/Document/Event/PatientPSO/cdResultLength" RMXType="code" />             
         </td> 
     </tr>
     <tr id="cdIncidentNotified_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdIncidentNotified_Title" runat="server" Text="After the discovery of the incident, was the patient, patient’s family, or guardian notified?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdIncidentNotified" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdIncidentNotified" RMXRef="/Instance/Document/Event/PatientPSO/cdIncidentNotified" RMXType="code" />             
         </td> 
     </tr>   
    </table>
    </div>
<div class="msgheader" id="pnlBlood_header" runat="server">Blood Or Blood Product</div>
<div id="pnlBlood" runat="server">
<table border="0" cellspacing="0" cellpadding="0" id="pnlBlood_table" runat="server" width="100%">
    <tr id="psocdTypeBldProduct_ctlRow" runat="server">
        <td width="60%">
            <asp:Label ID="psocdTypeBldProduct_Title" runat="server" Text="What type of blood product was involved in the event?" class=""></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdTypeBldProduct" CodeTable="PSO_BLOOD_PROD" ControlName="psocdTypeBldProduct" RMXRef="/Instance/Document/Event/EventPSO/psocdTypeBldProduct" RMXType="code"  />             
        </td>
    </tr>
    <tr id="txtOtherTypeBldProduct_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherTypeBldProduct_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtherTypeBldProduct" RMXRef="/Instance/Document/Event/EventPSO/txtOtherTypeBldProduct" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtherTypeBldProduct" id="btnOtherTypeBldProduct" onclick="EditMemo('txtOtherTypeBldProduct','')" />
            </span>
        </td>              
    </tr>
    <tr id="psocdISBTProductCode_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdISBTProductCode_Title" runat="server" Text="‭What was the International Society of Blood Transfusion (ISBT) 8 digit product code for the product associated with this event?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdISBTProductCode" CodeTable="PSO_ISBT_CODE" ControlName="psocdISBTProductCode" RMXRef="/Instance/Document/Event/EventPSO/psocdISBTProductCode" RMXType="code"  />             
        </td>
    </tr>
    <%--<tr id="psocdDescribeEvent_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdDescribeEvent_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdDescribeEvent_Title" runat="server" Text="Which of the following best describes the event?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdDescribeEvent" CodeTable="PSO_BLD_PROD_EVENT" ControlName="psocdDescribeEvent" RMXRef="/Instance/Document/Event/EventPSO/psocdDescribeEvent" RMXType="code"  />             
        </td>
    </tr>
     <%--<tr id="psocdDescribeEventNM_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdDescribeEventNM_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdDescribeEventNM_Title" runat="server" Text="Which of the following best describes the event?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdDescribeEventNM" CodeTable="PSO_BLD_PROD_EVENT" ControlName="psocdDescribeEventNM" RMXRef="/Instance/Document/Event/EventPSO/psocdDescribeEventNM" RMXType="code"  />             
        </td>
    </tr>
    <%--<tr id="psocdIncorrectAction_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdIncorrectAction_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdIncorrectAction_Title" runat="server" Text="What incorrect action was involved in administering the blood or blood product?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdIncorrectAction" CodeTable="PSO_BLOOD_INCRT_AC" ControlName="psocdIncorrectAction" RMXRef="/Instance/Document/Event/EventPSO/psocdIncorrectAction" RMXType="code"  />             
        </td>
    </tr>
    <%--<tr id="txtOtherIncorrectAction_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherIncorrectAction_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherIncorrectAction_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtherIncorrectAction" RMXRef="/Instance/Document/Event/EventPSO/txtOtherIncorrectAction" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtherIncorrectAction" id="btnOtherIncorrectAction" onclick="EditMemo('txtOtherIncorrectAction','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdChkDocument_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdChkDocument_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdChkDocument_Title" runat="server" Text="Was a two-person, three-way check documented?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdChkDocument" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdChkDocument" RMXRef="/Instance/Document/Event/EventPSO/psocdChkDocument" RMXType="code" />             
        </td>
    </tr> 
    <%--<tr id="psocdBloodVolume_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdBloodVolume_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdBloodVolume_Title" runat="server" Text="What was the volume?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdBloodVolume" CodeTable="PSO_BLOOD_VOL" ControlName="psocdBloodVolume" RMXRef="/Instance/Document/Event/EventPSO/psocdBloodVolume" RMXType="code"  />             
        </td>
    </tr>  
    <%--<tr id="psocdAdminRate_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdAdminRate_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdAdminRate_Title" runat="server" Text="What was the rate of administration?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdAdminRate" CodeTable="PSO_BLOOD_TRANS_RT" ControlName="psocdAdminRate" RMXRef="/Instance/Document/Event/EventPSO/psocdAdminRate" RMXType="code"  />             
        </td>
    </tr>
    <%--<tr id="psocdProcessDiscovered_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdProcessDiscovered_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdProcessDiscovered_Title" runat="server" Text="During which process was the event discovered (regardless of the stage when it originated)?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdProcessDiscovered" CodeTable="PSO_DISC_PROC" ControlName="psocdProcessDiscovered" RMXRef="/Instance/Document/Event/EventPSO/psocdProcessDiscovered" RMXType="code"  />             
        </td>
    </tr>
    <%--<tr id="txtOtherProcessDiscovered_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherProcessDiscovered_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherProcessDiscovered_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtherProcessDiscovered" RMXRef="/Instance/Document/Event/EventPSO/txtOtherProcessDiscovered" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtherProcessDiscovered" id="btnOtherProcessDiscovered" onclick="EditMemo('txtOtherProcessDiscovered','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdProcessOriginated_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdProcessOriginated_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdProcessOriginated_Title" runat="server" Text="During which process did the event originate (regardless of the stage when it was discovered)?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdProcessOriginated" CodeTable="PSO_ORIG_PROC" ControlName="psocdProcessOriginated" RMXRef="/Instance/Document/Event/EventPSO/psocdProcessOriginated" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtOtherProcessOriginated_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherProcessOriginated_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherProcessOriginated_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtherProcessOriginated" RMXRef="/Instance/Document/Event/EventPSO/txtOtherProcessOriginated" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtherProcessOriginated" id="btnOtherProcessOriginated" onclick="EditMemo('txtOtherProcessOriginated','')" />
            </span>
        </td>              
    </tr>
</table>
</div>    
<div class="msgheader" id="pnlFall_header" runat="server">Fall</div>
<div id="pnlFall" runat="server"><table border="0" cellspacing="0" cellpadding="0" id="pnlFall_table" runat="server" width="100%">
        <tr id="psocdFallAssist_ctlRow" runat="server">
        <td width="60%">
            <asp:Label ID="psocdFallAssist_Title" runat="server" Text="Was the fall unassisted or assisted?" class=""></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdFallAssist" CodeTable="PSO_FALL_ASSIST" ControlName="psocdFallAssist" RMXRef="/Instance/Document/Event/EventPSO/psocdFallAssist" RMXType="code" />             
        </td>
        </tr>
        <tr id="psocdFallObserved_ctlRow" runat="server">
        <td><asp:Label ID="psocdFallObserved_Title" runat="server" Text="Was the fall observed?" class="required"></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdFallObserved" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdFallObserved" RMXRef="/Instance/Document/Event/EventPSO/psocdFallObserved" RMXType="code" />             
         </td>
        </tr>
        <%--<tr id="psocdWhoObserved_ctlRow" runat="server" style="display:none">--%>
        <tr id="psocdWhoObserved_ctlRow" runat="server" >
        <td><asp:Label ID="psocdWhoObserved_Title" runat="server" Text="Who observed the fall?" class=""></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdWhoObserved" CodeTable="PSO_FALLOBSVR" ControlName="psocdWhoObserved" RMXRef="/Instance/Document/Event/EventPSO/psocdWhoObserved" RMXType="code"  />             
         </td>
        </tr>
        <tr id="psocdPhysicalInjury_ctlRow" runat="server">
        <td><asp:Label ID="psocdPhysicalInjury_Title" runat="server" Text="Did the patient sustain a physical injury as a result of the fall?" class="required"></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdPhysicalInjury" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdPhysicalInjury" RMXRef="/Instance/Document/Event/EventPSO/psocdPhysicalInjury" RMXType="code" />             
         </td>
        </tr>
        <%--<tr id="psocdInjuryType_ctlRow" runat="server" style="display:none">--%>
        <tr id="psocdInjuryType_ctlRow" runat="server" >
        <td><asp:Label ID="psocdInjuryType_Title" runat="server" Text="What type of injury was sustained?" class=""></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdInjuryType" CodeTable="PSO_INJ_TYPE" ControlName="psocdInjuryType" RMXRef="/Instance/Document/Event/EventPSO/psocdInjuryType" RMXType="code" />             
         </td>
        </tr>
         <%--<tr  id="psotxtOtherInjuryType_ctlRow" runat="server" style="display:none">--%>
         <tr  id="psotxtOtherInjuryType_ctlRow" runat="server" >
        <td><asp:Label ID="psotxtOtherInjuryType_Title" runat="server" Text="If Other, Please Specify"></asp:Label></td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtOtherInjuryType" RMXRef="/Instance/Document/Event/EventPSO/txtOtherInjuryType" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherInjuryType" id="btnOtherInjuryType" onclick="EditMemo('txtOtherInjuryType','')" />
                </span>
         </td>              
         </tr>     
      <tr>
        <td ><asp:Label ID="psocdPatientDoing_Title" runat="server" Text="Prior to the fall, what was the patient doing or trying to do?" class="required"></asp:Label></td>
         <td > 
            <uc2:CodeLookUp runat="server" ID="psocdPatientDoing" CodeTable="PSO_PAT_ACTION" ControlName="psocdPatientDoing" RMXRef="/Instance/Document/Event/EventPSO/psocdPatientDoing" RMXType="code" />             
         </td>
        </tr>
         <%--<tr id="txtOtherPatientDoing_ctlRow" runat="server" style="display:none">--%>
         <tr id="txtOtherPatientDoing_ctlRow" runat="server" >
        <td><asp:Label ID="txtOtherPatientDoing_Title" runat="server" Text="If Other, Please Specify"></asp:Label></td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtOtherPatientDoing" RMXRef="/Instance/Document/Event/EventPSO/txtOtherPatientDoing" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherPatientDoing" id="btnOtherPatientDoing" onclick="EditMemo('txtOtherPatientDoing','')" />
                </span>
         </td>              
         </tr>
         <tr id="psocdRiskAssessment_ctlRow" runat="server">
        <td><asp:Label ID="psocdRiskAssessment_Title" runat="server" Text="Prior to the fall, was a fall risk assessment performed?" class="required"></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdRiskAssessment" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdRiskAssessment" RMXRef="/Instance/Document/Event/EventPSO/psocdRiskAssessment" RMXType="code" />             
         </td>
        </tr>
        <%--<tr id="psocdDeterminedForFall_ctlRow" runat="server" style="display:none">--%>
        <tr id="psocdDeterminedForFall_ctlRow" runat="server" >
        <td><asp:Label ID="psocdDeterminedForFall_Title" runat="server" Text="Was the patient determined to be at risk for a fall?" class=""></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdDeterminedForFall" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdDeterminedForFall" RMXRef="/Instance/Document/Event/EventPSO/psocdDeterminedForFall" RMXType="code" />             
         </td>
        </tr>
        <tr id="psolstProtocolFall_ctlRow" runat="server">
        <td><asp:Label ID="psolstProtocolFall_Title" runat="server" Text="What protocols/interventions were in place, or being used, to prevent falls for this patient?" class=""></asp:Label></td>
         <td> 
            <uc:MultiCode runat="server" ID="psolstProtocolFall" width="200px" Height="88px" CodeTable="PSO_INTERVENTION" ControlName="psolstProtocolFall" RMXRef="/Instance/Document/Event/EventPSO/psolstProtocolFall" RMXType="codelist" />
         </td>
        </tr>
        <%--<tr  id="psotxtOtherProtocolFall_ctlRow" runat="server" style="display:none">--%>
        <tr  id="psotxtOtherProtocolFall_ctlRow" runat="server" >
        <td><asp:Label ID="psotxtOtherProtocolFall_Title" runat="server" Text="If Other, Please Specify"></asp:Label></td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="psotxtOtherProtocolFall" RMXRef="/Instance/Document/Event/EventPSO/psotxtOtherProtocolFall" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherProtocolFall" id="btnOtherProtocolFall" onclick="EditMemo('psotxtOtherProtocolFall','')" />
                </span>
         </td>              
         </tr>
        <tr id="psocdMedicationFall_ctlRow" runat="server">
        <td><asp:Label ID="psocdMedicationFall_Title" runat="server" Text="At time of the fall, was the patient on medication known to increase the risk for a fall?" class="required"></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdMedicationFall" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdMedicationFall" RMXRef="/Instance/Document/Event/EventPSO/psocdMedicationFall" RMXType="code" />             
         </td>
        </tr>
        <%--<tr id="psocdMedicationContributed_ctlRow" runat="server" style="display:none">--%>
        <tr id="psocdMedicationContributed_ctlRow" runat="server" >
        <td><asp:Label ID="psocdMedicationContributed_Title" runat="server" Text="Was the medication considered to have contributed to the fall?" class=""></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdMedicationContributed" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdMedicationContributed" RMXRef="/Instance/Document/Event/EventPSO/psocdMedicationContributed" RMXType="code" />             
         </td>
        </tr>        
      </table>
    </div> 
 <div class="msgheader" id="pnlHAI_header" runat="server">Healthcare-Associated Infection</div>
 <div id="pnlHAI" runat="server">
 <table border="0" cellspacing="0" cellpadding="0" id="pnlHAI_table" runat="server" width="100%">
      <tr id="psocdInfectionPresent_ctlRow" runat="server">
           <td width="60%">
                <asp:Label ID="psocdInfectionPresent_Title" runat="server" Text="Was the infection determined to be present or incubating on admission?" class="required"></asp:Label>
           </td>
           <td width="40%"> 
                <uc2:CodeLookUp runat="server" ID="psocdInfectionPresent" CodeTable="PSO_INF_PRESENT" ControlName="psocdInfectionPresent" RMXRef="/Instance/Document/Event/PatientPSO/psocdInfectionPresent" RMXType="code"  />             
           </td>
      </tr>
      <%--<tr id="psocdBestDescription_ctlRow" runat="server" style="display:none">--%>
      <tr id="psocdBestDescription_ctlRow" runat="server" >
          <td>
             <asp:Label ID="psocdBestDescription_Title" runat="server" Text="Which of the following best describes the infection?" class="required"></asp:Label>
          </td>
          <td> 
               <uc2:CodeLookUp runat="server" ID="psocdBestDescription" CodeTable="PSO_INFECTION_DESC" ControlName="psocdBestDescription" RMXRef="/Instance/Document/Event/PatientPSO/psocdBestDescription" RMXType="code"  />             
          </td>
      </tr>
      <%--<tr id="psocdHAIPerson_ctlRow" runat="server" style="display:none">--%>
      <tr id="psocdHAIPerson_ctlRow" runat="server" >
         <td>
            <asp:Label ID="psocdHAIPerson_Title" runat="server" Text="Was the person who determined the infection to be a healthcare-associated infection (HAI) a healthcare professional with specific training in infectious disease and/or infection control?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdHAIPerson" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdHAIPerson" RMXRef="/Instance/Document/Event/PatientPSO/psocdHAIPerson" RMXType="code" />             
         </td>
     </tr>
     <%--<tr id="psocdHAITypeReported_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdHAITypeReported_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdHAITypeReported_Title" runat="server" Text="What type of HAI is being reported?" class="required"></asp:Label></td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdHAITypeReported" CodeTable="PSO_HAI_TYPE" ControlName="psocdHAITypeReported" RMXRef="/Instance/Document/Event/PatientPSO/psocdHAITypeReported" RMXType="code"  />             
        </td>
     </tr>
     <%--<tr id="psocdCLABSI_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdCLABSI_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdCLABSI_Title" runat="server" Text="Was it central line-associated (CLABSI)?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdCLABSI" CodeTable="PSO_YES_NO" ControlName="psocdCLABSI" RMXRef="/Instance/Document/Event/PatientPSO/psocdCLABSI" RMXType="code"  />             
        </td>
     </tr>
     <%--<tr id="psocdCentralLine_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdCentralLine_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdCentralLine_Title" runat="server" Text="Which type of central line?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdCentralLine" CodeTable="PSO_CL_TYP" ControlName="psocdCentralLine" RMXRef="/Instance/Document/Event/PatientPSO/psocdCentralLine" RMXType="code"  />             
        </td>
    </tr>
    <%--<tr id="psocdAssocPneumonia_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdAssocPneumonia_ctlRow" runat="server">
       <td>
         <asp:Label ID="psocdAssocPneumonia_Title" runat="server" Text="Was it a ventilator-associated pneumonia (VAP - i.e., the patient had a device to assist or control respiration continuously through a tracheostomy or by endotracheal intubation)?" class="required"></asp:Label>
       </td>
       <td> 
         <uc2:CodeLookUp runat="server" ID="psocdAssocPneumonia" CodeTable="PSO_YES_NO" ControlName="psocdAssocPneumonia" RMXRef="/Instance/Document/Event/PatientPSO/psocdAssocPneumonia" RMXType="code" />             
       </td>
    </tr>
     <%--<tr id="psocdVAPClassified_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdVAPClassified_ctlRow" runat="server" >
       <td>
          <asp:Label ID="psocdVAPClassified_Title" runat="server" Text="The VAP was classified as which of the following?" class=""></asp:Label>
       </td>
       <td> 
          <uc2:CodeLookUp runat="server" ID="psocdVAPClassified" CodeTable="PSO_VAP_CLASS" ControlName="psocdVAPClassified" RMXRef="/Instance/Document/Event/PatientPSO/psocdVAPClassified" RMXType="code"  />             
       </td>
    </tr>
    <%--<tr id="psocdCAUTI_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdCAUTI_ctlRow" runat="server" >
       <td>
          <asp:Label ID="psocdCAUTI_Title" runat="server" Text="Was it catheter-associated (CAUTI)?" class="required"></asp:Label>
       </td>
       <td> 
          <uc2:CodeLookUp runat="server" ID="psocdCAUTI" CodeTable="PSO_YES_NO" ControlName="psocdCAUTI" RMXRef="/Instance/Document/Event/PatientPSO/psocdCAUTI" RMXType="code"  />             
       </td>
    </tr>
    <%--<tr id="psocdDiagnosisCAUTI_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdDiagnosisCAUTI_ctlRow" runat="server">
       <td>
          <asp:Label ID="psocdDiagnosisCAUTI_Title" runat="server" Text="What was the urinary catheter status at the time of specimen collection that was the basis for diagnosis of CAUTI?" class=""></asp:Label>
       </td>
       <td> 
          <uc2:CodeLookUp runat="server" ID="psocdDiagnosisCAUTI" CodeTable="PSO_CAUTI_STAT" ControlName="psocdDiagnosisCAUTI" RMXRef="/Instance/Document/Event/PatientPSO/psocdDiagnosisCAUTI" RMXType="code"  />             
       </td>
    </tr>
    <%--<tr id="psocdClasifiedCAULI_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdClasifiedCAULI_ctlRow" runat="server" >
       <td>
          <asp:Label ID="psocdClasifiedCAULI_Title" runat="server" Text="The CAUTI was classified as which of the following?" class=""></asp:Label>
       </td>
       <td> 
          <uc2:CodeLookUp runat="server" ID="psocdClasifiedCAULI" CodeTable="PSO_CAUTI_CLASS" ControlName="psocdClasifiedCAULI" RMXRef="/Instance/Document/Event/PatientPSO/psocdClasifiedCAULI" RMXType="code"  />             
       </td>
    </tr>
    <%--<tr id="psocdSSIClasified_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdSSIClasified_ctlRow" runat="server">
       <td>
         <asp:Label ID="psocdSSIClasified_Title" runat="server" Text="The SSI was classified as which of the following?" class=""></asp:Label>
       </td>
       <td> 
         <uc2:CodeLookUp runat="server" ID="psocdSSIClasified" CodeTable="PSO_SSI_CLASS" ControlName="psocdSSIClasified" RMXRef="/Instance/Document/Event/PatientPSO/psocdSSIClasified" RMXType="code"  />             
       </td>
    </tr>
    <%--<tr id="psocdInfectionType_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdInfectionType_ctlRow" runat="server">
       <td>
          <asp:Label ID="psocdInfectionType_Title" runat="server" Text="Which other type of infection?" class=""></asp:Label>
       </td>
       <td> 
          <uc2:CodeLookUp runat="server" ID="psocdInfectionType" CodeTable="PSO_INF_TYP" ControlName="psocdInfectionType" RMXRef="/Instance/Document/Event/PatientPSO/psocdInfectionType" RMXType="code"  />             
       </td>
    </tr>
    <%--<tr id="txtOtherInfectionType_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherInfectionType_ctlRow" runat="server">
       <td>
          <asp:Label ID="txtOtherInfectionType_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
       </td>
       <td> 
          <span class="formw">
               <asp:TextBox runat="Server" id="txtOtherInfectionType" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherInfectionType" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
               <input type="button" class="button" value="..." name="btnOtherInfectionType" id="btnOtherInfectionType" onclick="EditMemo('txtOtherInfectionType','')" />
          </span>
       </td>              
    </tr>
    <%--<tr id="psocdInpatientLoc_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdInpatientLoc_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdInpatientLoc_Title" runat="server" Text="At which inpatient location was the patient assigned when the specimen that met the infection criteria was collected, or when the first clinical evidence of CLABSI, VAP, or CAUTI appeared? If the infection developed within 48 hours of transfer from one location to one or more other locations within this facility, select the patient’s first such inpatient location within the 48 hour period where the central line, urinary catheter, or ventilator was used." class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdInpatientLoc" CodeTable="PSO_INP_LOC" ControlName="psocdInpatientLoc" RMXRef="/Instance/Document/Event/PatientPSO/psocdInpatientLoc" RMXType="code" />             
        </td>
    </tr>
 </table>
</div> 
 
<div class="msgheader" id="pnlPerinatal_header" runat="server">Perinatal</div>
<div id="pnlPerinatal" runat="server">
 <table border="0" cellspacing="0" cellpadding="0" id="pnlPerinatal_table" width="100%" runat="server">
    <tr id="psocdEventInvolved_ctlRow" runat="server">
        <td width="60%">
            <asp:Label ID="psocdEventInvolved_Title" runat="server" Text="Which of the following did the event involve?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdEventInvolved" CodeTable="PSO_PN_EVNT_TYP" ControlName="psocdEventInvolved" RMXRef="/Instance/Document/Event/PatientPSO/psocdEventInvolved" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="psocdAffectedByEventBirthing_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdAffectedByEventBirthing_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdAffectedByEventBirthing_Title" runat="server" Text="Who was affected by the event?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdAffectedByEventBirthing" CodeTable="PSO_AFFEC_PAT" ControlName="psocdAffectedByEventBirthing" RMXRef="/Instance/Document/Event/PatientPSO/psocdAffectedByEventBirthing" RMXType="code"  />             
         </td> 
    </tr>
    <%--<tr id="psocdAffectedByEventIntrauterine_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdAffectedByEventIntrauterine_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdAffectedByEventIntrauterine_Title" runat="server" Text="Who was affected by the event?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdAffectedByEventIntrauterine" CodeTable="PSO_AFFEC_PAT" ControlName="psocdAffectedByEventIntrauterine" RMXRef="/Instance/Document/Event/PatientPSO/psocdAffectedByEventIntrauterine" RMXType="code" />             
         </td> 
    </tr>;
    <%--<tr id="cdPrimipara_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdPrimipara_ctlRow" runat="server" >
        <td>
            <asp:Label ID="cdPrimipara_Title" runat="server" Text="Was the mother a primipara?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdPrimipara" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdPrimipara" RMXRef="/Instance/Document/Event/PatientPSO/cdPrimipara" RMXType="code" />  
        </td> 
    </tr>
    <%--<tr id="txtFetusesNum_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtFetusesNum_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtFetusesNum_Title" runat="server" Text="How many fetuses were in this pregnancy?" class=""></asp:Label>
        </td>
        <td> 
            <asp:TextBox ID="txtFetusesNum" runat="server" RMXType="numeric" size="30" RMXRef="/Instance/Document/Event/PatientPSO/txtFetusesNum" onchange="setDataChanged(true);" ></asp:TextBox>
            <asp:Image runat="server" ID="txtFetusesNum_Image" ImageUrl="~/Images/HelpIcon.jpg" style="display:none;visibility:hidden"/>
        </td>              
    </tr>
    <%--<tr id="txtGestation_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdGestation_ctlRow" runat="server" >
        <td>
            <asp:Label ID="cdGestation_Title" runat="server" Text="Immediately prior to delivery, or at the time of the intrauterine procedure (prenatal), what was the best estimate of completed weeks of gestation?" class=""></asp:Label>
        </td>
        <td> 
            <%--<asp:TextBox ID="cdGestation" runat="server" RMXType="numeric" size="30" RMXRef="/Instance/Document/Event/PatientPSO/txtGestation" BackColor="#F2F2F2" ReadOnly="true"></asp:TextBox>--%>
            <uc2:CodeLookUp runat="server" ID="cdGestation" CodeTable="PSO_LEN_OF_GEST" ControlName="cdGestation" RMXRef="/Instance/Document/Event/PatientPSO/cdGestation" RMXType="code" /> 
        </td> 
    </tr>
    <%--<tr id="dtDeliveryDate_ctlRow" runat="server" style="display:none">--%>
    <tr id="dtDeliveryDate_ctlRow" runat="server">
        <td>
            <asp:Label ID="dtDeliveryDate_Title" runat="server" Text="What was the date of delivery?" class=""></asp:Label>
        </td>
        <td>
        <asp:TextBox runat="server" FormatAs="date" ID="dtDeliveryDate" RMXRef="/Instance/Document/Event/PatientPSO/dtDeliveryDate"
                        RMXType="date" onchange="CopyDateToValidate(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                    <asp:Button class="DateLookupControl" runat="server" ID="btnDeliveryDate"/>
                     <asp:TextBox ID="dtDeliveryDate_ValidateDate" Style="display: none" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/Event/PatientPSO/dtDeliveryDate"></asp:TextBox>
                        <%--<script type="text/javascript">
                            Zapatec.Calendar.setup({ inputField: "dtDeliveryDate", ifFormat: "%m/%d/%Y", button: "btnDeliveryDate" });
                        </script>--%>

                        <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#dtDeliveryDate").datepicker({
            showOn: "button",
            buttonImage: "../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
        </td> 
    </tr>
    <tr id="psocdLaborinduced_ctlRow" runat="server" >
        <td width="60%">
            <asp:Label ID="psocdLaborinduced_Title" runat="server" Text="Was labor induced or augmented?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdLaborinduced" CodeTable="YES_NO_JCAHO" ControlName="psocdLaborinduced" RMXRef="/Instance/Document/Event/PatientPSOData/psocdLaborinduced" RMXType="code" Enabled="false" BackColor="#F2F2F2" />             
        </td>              
    </tr>     
    <%--<tr id="psocdLaborYes_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdLaborYes_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdLaborYes_Title" runat="server" Text="Which one?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdLaborYes" CodeTable="PSO_LABR_TYP" ControlName="psocdLaborYes" RMXRef="/Instance/Document/Event/PatientPSO/psocdLaborYes" RMXType="code" />             
        </td>              
    </tr>
    <%--<tr id="cdDeliveryMode_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdDeliveryMode_ctlRow" runat="server" >
        <td>
            <asp:Label ID="cdDeliveryMode_Title" runat="server" Text="What was the final mode of delivery?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdDeliveryMode" CodeTable="PSO_DELV_MODE" ControlName="cdDeliveryMode" RMXRef="/Instance/Document/Event/PatientPSO/cdDeliveryMode" RMXType="code" />             
        </td>              
    </tr>
    <%--<tr id="txtLiveBirth_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtLiveBirth_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtLiveBirth_Title" runat="server" Text="Number of live births"></asp:Label>
        </td>
        <td> 
            <asp:TextBox ID="txtLiveBirth" runat="server" RMXType="numeric" size="30" RMXRef="/Instance/Document/Event/PatientPSOData/txtLiveBirth" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
        </td>              
    </tr>
    <%--<tr id="txtNeonateWieght_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtNeonateWieght_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtNeonateWieght_Title" runat="server" Text="What was the neonate’s birthweight?"></asp:Label>
        </td>
        <td> 
            <asp:TextBox ID="txtNeonateWieght" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/Event/PatientPSOData/txtNeonateWieght" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
        </td>              
    </tr>
    <tr id="psolstAdverseOutcome_ctlRow" runat="server" >
        <td width="60%">
            <asp:Label ID="psolstAdverseOutcome_Title" runat="server" Text="Which adverse outcome(s) did the mother sustain?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc:MultiCode runat="server" ID="psolstAdverseOutcome" width="200px" Height="88px" CodeTable="PSO_MAT_OUTCM" ControlName="psolstAdverseOutcome" RMXRef="/Instance/Document/Event/PatientPSO/psolstAdverseOutcome" RMXType="codelist" />
        </td>              
    </tr>
    <%--<tr id="txtOtherAdverseOutcome_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherAdverseOutcome_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherAdverseOutcome_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherAdverseOutcome" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherAdverseOutcome" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherAdverseOutcome" id="btnOtherAdverseOutcome" onclick="EditMemo('txtOtherAdverseOutcome','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdMeternalInfection_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdMeternalInfection_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdMeternalInfection_Title" runat="server" Text="Which of the following maternal infections?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdMeternalInfection" CodeTable="PSO_MAT_INF_TYP" ControlName="psocdMeternalInfection" RMXRef="/Instance/Document/Event/PatientPSO/psocdMeternalInfection" RMXType="code"  />             
        </td> 
    </tr>
    <%--<tr id="txtOtherMeternalInfection_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherMeternalInfection_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="txtOtherMeternalInfection_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherMeternalInfection" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherMeternalInfection" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherMeternalInfection" id="btnOtherMeternalInfection" onclick="EditMemo('txtOtherMeternalInfection','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psolstBodyPart_ctlRow" runat="server" style="display:none">--%>
    <tr id="psolstBodyPart_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psolstBodyPart_Title" runat="server" Text="Which body part(s) or organ(s)?" class="required"></asp:Label>
        </td>
        <td> 
            <uc:MultiCode runat="server" ID="psolstBodyPart" width="200px" Height="88px" CodeTable="PSO_MAT_INJ_TYP" ControlName="psolstBodyPart" RMXRef="/Instance/Document/Event/PatientPSO/psolstBodyPart" RMXType="codelist" />
        </td> 
    </tr>
    <%--<tr id="txtOtherBodyPart_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherBodyPart_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherBodyPart_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherBodyPart" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherBodyPart" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherBodyPart" id="btnOtherBodyPart" onclick="EditMemo('txtOtherBodyPart','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdFetusSustain_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdFetusSustain_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdFetusSustain_Title" runat="server" Text="What adverse outcome did the fetus sustain?" class=""></asp:Label>
        </td>
        <td>
            <uc2:CodeLookUp runat="server" ID="psocdFetusSustain" CodeTable="PSO_FT_OUTCM_TYP" ControlName="psocdFetusSustain" RMXRef="/Instance/Document/Event/PatientPSO/psocdFetusSustain" RMXType="code" />             
        </td> 
    </tr>
    <tr id="psolstAdverseOutcomeMother_ctlRow" runat="server" >
        <td width="60%">
            <asp:Label ID="psolstAdverseOutcomeMother_Title" runat="server" Text="Which adverse outcome(s) did the mother sustain?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc:MultiCode runat="server" ID="psolstAdverseOutcomeMother" width="200px" Height="88px" CodeTable="PSO_MAT_OUTCM" ControlName="psolstAdverseOutcomeMother" RMXRef="/Instance/Document/Event/PatientPSO/psolstAdverseOutcomeMother" RMXType="codelist" />
        </td>              
    </tr>
    <%--<tr id="txtOtherAdverseOutcomeMother_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherAdverseOutcomeMother_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherAdverseOutcomeMother_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherAdverseOutcomeMother" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherAdverseOutcomeMother" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherAdverseOutcomeMother" id="btnOtherAdverseOutcomeMother" onclick="EditMemo('txtOtherAdverseOutcomeMother','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdMeternalInfectionMother_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdMeternalInfectionMother_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdMeternalInfectionMother_Title" runat="server" Text="Which of the following maternal infections?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdMeternalInfectionMother" CodeTable="PSO_MAT_INF_TYP" ControlName="psocdMeternalInfectionMother" RMXRef="/Instance/Document/Event/PatientPSO/psocdMeternalInfectionMother" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="txtOtherMeternalInfectionMother_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherMeternalInfectionMother_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="txtOtherMeternalInfectionMother_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherMeternalInfectionMother" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherMeternalInfectionMother" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="OtherMeternalInfectionMother_btn" id="OtherMeternalInfectionMother_btn" onclick="EditMemo('txtOtherMeternalInfectionMother','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psolstBodyPartMother_ctlRow" runat="server" style="display:none">--%>
    <tr id="psolstBodyPartMother_ctlRow" runat="server">
        <td>
            <asp:Label ID="psolstBodyPartMother_Title" runat="server" Text="Which body part(s) or organ(s)?" class="required"></asp:Label>
        </td>
        <td> 
            <uc:MultiCode runat="server" ID="psolstBodyPartMother" width="200px" Height="88px" CodeTable="PSO_MAT_INJ_TYP" ControlName="psolstBodyPartMother" RMXRef="/Instance/Document/Event/PatientPSO/psolstBodyPartMother" RMXType="codelist" />
        </td> 
    </tr>
    <%--<tr id="txtOtherBodyPartMother_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherBodyPartMother_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherBodyPartMother_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherBodyPartMother" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherBodyPartMother" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherBodyPartMother" id="btnOtherBodyPartMother" onclick="EditMemo('txtOtherBodyPartMother','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdVaginalDeliveryMother_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdVaginalDeliveryMother_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdVaginalDeliveryMother_Title" runat="server" Text="Regardless of the final mode of delivery, was instrumentation used to assist vaginal (or attempted vaginal) delivery?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdVaginalDeliveryMother" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdVaginalDeliveryMother" RMXRef="/Instance/Document/Event/PatientPSO/psocdVaginalDeliveryMother" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="psocdInstrumentationMother_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdInstrumentationMother_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdInstrumentationMother_Title" runat="server" Text="What instrumentation was used?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdInstrumentationMother" CodeTable="PSO_INSTRMNT_TYP" ControlName="psocdInstrumentationMother" RMXRef="/Instance/Document/Event/PatientPSO/psocdInstrumentationMother" RMXType="code" />             
        </td> 
    </tr>
    <tr id="psolstAdverseOutcomeMotherNeonate_ctlRow" runat="server" >
        <td width="60%">
            <asp:Label ID="psolstAdverseOutcomeMotherNeonate_Title" runat="server" Text="Which adverse outcome(s) did the mother sustain?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc:MultiCode runat="server" ID="psolstAdverseOutcomeMotherNeonate" width="200px" Height="88px" CodeTable="PSO_MAT_OUTCM" ControlName="psolstAdverseOutcomeMotherNeonate" RMXRef="/Instance/Document/Event/PatientPSO/psolstAdverseOutcomeMotherNeonate" RMXType="codelist" />
        </td>              
    </tr>
    <%--<tr id="txtOtherAdverseOutcomeMotherNeonate_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherAdverseOutcomeMotherNeonate_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherAdverseOutcomeMotherNeonate_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherAdverseOutcomeMotherNeonate" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherAdverseOutcomeMotherNeonate" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherAdverseOutcomeMotherNeonate" id="btnOtherAdverseOutcomeMotherNeonate" onclick="EditMemo('txtOtherAdverseOutcomeMotherNeonate','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdMeternalInfectionMotherNeonate_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdMeternalInfectionMotherNeonate_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdMeternalInfectionMotherNeonate_Title" runat="server" Text="Which of the following maternal infections?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdMeternalInfectionMotherNeonate" CodeTable="PSO_MAT_INF_TYP" ControlName="psocdMeternalInfectionMotherNeonate" RMXRef="/Instance/Document/Event/PatientPSO/psocdMeternalInfectionMotherNeonate" RMXType="code"  />             
        </td> 
    </tr>
    <%--<tr id="txtOtherMeternalInfectionMotherNeonate_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherMeternalInfectionMotherNeonate_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="txtOtherMeternalInfectionMotherNeonate_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherMeternalInfectionMotherNeonate" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherMeternalInfectionMotherNeonate" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherMeternalInfectionMotherNeonate" id="btnOtherMeternalInfectionMotherNeonate" onclick="EditMemo('txtOtherMeternalInfectionMotherNeonate','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psolstBodyPartMotherNeonate_ctlRow" runat="server" style="display:none">--%>
    <tr id="psolstBodyPartMotherNeonate_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psolstBodyPartMotherNeonate_Title" runat="server" Text="Which body part(s) or organ(s)?" class="required"></asp:Label>
        </td>
        <td> 
            <uc:MultiCode runat="server" ID="psolstBodyPartMotherNeonate" width="200px" Height="88px" CodeTable="PSO_MAT_INJ_TYP" ControlName="psolstBodyPartMotherNeonate" RMXRef="/Instance/Document/Event/PatientPSO/psolstBodyPartMotherNeonate" RMXType="codelist" />
        </td> 
    </tr>
    <%--<tr id="txtOtherBodyPartMotherNeonate_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherBodyPartMotherNeonate_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherBodyPartMotherNeonate_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherBodyPartMotherNeonate" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherBodyPartMotherNeonate" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherBodyPartMotherNeonate" id="btnOtherBodyPartMotherNeonate" onclick="EditMemo('txtOtherBodyPartMotherNeonate','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdVaginalDeliveryMotherNeonate_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdVaginalDeliveryMotherNeonate_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdVaginalDeliveryMotherNeonate_Title" runat="server" Text="Regardless of the final mode of delivery, was instrumentation used to assist vaginal (or attempted vaginal) delivery?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdVaginalDeliveryMotherNeonate" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdVaginalDeliveryMotherNeonate" RMXRef="/Instance/Document/Event/PatientPSO/psocdVaginalDeliveryMotherNeonate" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="psocdInstrumentationMotherNeonate_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdInstrumentationMotherNeonate_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdInstrumentationMotherNeonate_Title" runat="server" Text="What instrumentation was used?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdInstrumentationMotherNeonate" CodeTable="PSO_INSTRMNT_TYP" ControlName="psocdInstrumentationMotherNeonate" RMXRef="/Instance/Document/Event/PatientPSO/psocdInstrumentationMotherNeonate" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="txtApgarScore_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtApgarScore_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtApgarScore_Title" runat="server" Text="What was the 5-minute Apgar score?"></asp:Label>
        </td>
        <td> 
            <asp:TextBox ID="txtApgarScore" runat="server" RMXType="text" size="30" RMXRef="/Instance/Document/Event/PatientPSOData/txtApgarScore" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
        </td>              
    </tr>
    <%--<tr id="psolstNeonateSustain_ctlRow" runat="server" style="display:none">--%>
    <tr id="psolstNeonateSustain_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psolstNeonateSustain_Title" runat="server" Text="Which adverse outcome(s) did the neonate sustain?" class="required"></asp:Label>
        </td>
        <td> 
            <uc:MultiCode runat="server" ID="psolstNeonateSustain" width="200px" Height="88px" CodeTable="PSO_NN_OUTCM_TYP" ControlName="psolstNeonateSustain" RMXRef="/Instance/Document/Event/PatientPSO/psolstNeonateSustain" RMXType="codelist" />
            <%--<uc2:CodeLookUp runat="server" ID="psocdNeonateSustain" CodeTable="PSO_NN_OUTCM_TYP" ControlName="psocdNeonateSustain" RMXRef="" RMXType="code" tabindex="" />             --%>
        </td>              
    </tr>
    <%--<tr id="txtOtherNeonateSustain_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherNeonateSustain_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherNeonateSustain_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherNeonateSustain" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherNeonateSustain" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherNeonateSustain" id="btnOtherNeonateSustain" onclick="EditMemo('txtOtherNeonateSustain','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdBirthTrauma_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdBirthTrauma_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdBirthTrauma_Title" runat="server" Text="Which birth trauma?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdBirthTrauma" CodeTable="PSO_TRUMA_TYP" ControlName="psocdBirthTrauma" RMXRef="/Instance/Document/Event/PatientPSO/psocdBirthTrauma" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="txtOtherBirthTrauma_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherBirthTrauma_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherBirthTrauma_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherBirthTrauma" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherBirthTrauma" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherBirthTrauma" id="btnOtherBirthTrauma" onclick="EditMemo('txtOtherBirthTrauma','')" />
            </span>
        </td>              
    </tr>  
</table>    
</div>   
<div class="msgheader" id="pnlNeonatal_header" runat="server">Neonatal Patient Information</div>
<div id="pnlNeonatal" runat="server">
<table border="0" cellspacing="0" cellpadding="0" id="pnlNeonatal_table" width="100%" runat="server">
     <tr id="txtPatientNameNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="txtPatientNameNeo_Title" runat="server" Text="Patient’s Name"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="txtPatientNameNeo" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/Event/PatientPSOData/txtPatientNameNeo" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
         </td>              
     </tr>  
    <tr id="txtPatientDOBNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="txtPatientDOBNeo_Title" runat="server" Text="Patient’s Date of Birth"></asp:Label>
         </td>
         <td>
         <asp:TextBox runat="server" FormatAs="date" ID="txtPatientDOBNeo" RMXRef="/Instance/Document/Event/PatientPSOData/txtPatientDOBNeo"
                        RMXType="date" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" ReadOnly="true" BackColor="#F2F2F2"/>
         </td> 
     </tr> 
     <tr id="txtMedicalRecordNoNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="txtMedicalRecordNoNeo_Title" runat="server" Text="Medical Record #"></asp:Label>
         </td>
         <td> 
            <asp:TextBox ID="txtMedicalRecordNoNeo" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/Event/PatientPSOData/txtMedicalRecordNoNeo" ReadOnly="true" BackColor="#F2F2F2"></asp:TextBox>
         </td>              
     </tr>  
     <tr id="cdPatientGenderNeo_ctlRow" runat="server">
         <td>
             <asp:Label ID="cdPatientGenderNeo_Title" runat="server" Text="Patient’s Gender"></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdPatientGenderNeo" CodeTable="SEX_CODE" ControlName="cdPatientGender_Neo" RMXRef="/Instance/Document/Event/PatientPSOData/cdPatientGenderNeo" RMXType="code" Enabled="false"/>             
         </td> 
     </tr>
     <tr id="psocdPatientAgeNeo_ctlRow" runat="server">
        <td width="60%">
            <asp:Label ID="psocdPatientAgeNeo_Title" runat="server" Text="At the time of the event what was the patient’s age?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdPatientAgeNeo" CodeTable="PSO_PAT_AGE" ControlName="psocdPatientAgeNeo" RMXRef="/Instance/Document/Event/PatientPSO/psocdPatientAgeNeo" RMXType="code" />             
        </td> 
     </tr>
     <tr id="cdPatientEthnicityNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdPatientEthnicityNeo_Title" runat="server" Text="Is the patient’s ethnicity Hispanic or Latino?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdPatientEthnicityNeo" CodeTable="PSO_ETHNIC" ControlName="cdPatientEthnicityNeo" RMXRef="/Instance/Document/Event/PatientPSO/cdPatientEthnicityNeo" RMXType="code" />             
         </td> 
     </tr>
     <tr id="cdPatientRaceNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdPatientRaceNeo_Title" runat="server" Text="What is the patient’s race?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdPatientRaceNeo" CodeTable="PSO_RACE" ControlName="cdPatientRaceNeo" RMXRef="/Instance/Document/Event/PatientPSO/cdPatientRaceNeo" RMXType="code"  />             
         </td> 
     </tr>
     <tr id="cdDiagnosisCodeNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdDiagnosisCodeNeo_Title" runat="server" Text="Enter the patient’s ICD-9-CM principal diagnosis code at discharge (if available)" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdDiagnosisCodeNeo" CodeTable="CDC_DIAGNOSIS_CODE" ControlName="cdDiagnosisCodeNeo" RMXRef="/Instance/Document/Event/PatientPSO/cdDiagnosisCodeNeo" RMXType="code" />             
         </td>              
     </tr>
     <tr id="cdExtentHarmNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdExtentHarmNeo_Title" runat="server" Text="After discovery of the incident, what was the extent of harm to the patient (i.e., extent to which the patient’s functional ability is expected to be impaired subsequent to the incident and any attempts to minimize adverse consequences)?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdExtentHarmNeo" CodeTable="PSO_HARM_EXT" ControlName="cdExtentHarmNeo" RMXRef="/Instance/Document/Event/PatientPSO/cdExtentHarmNeo" RMXType="code" />
         </td>              
     </tr>
     <tr id="cdAssessedHarmNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdAssessedHarmNeo_Title" runat="server" Text="Approximately when after discovery of the incident was harm assessed?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdAssessedHarmNeo" CodeTable="PSO_HARM_TIME" ControlName="cdAssessedHarmNeo" RMXRef="/Instance/Document/Event/PatientPSO/cdAssessedHarmNeo" RMXType="code" />             
         </td> 
     </tr>
     <tr id="psocdRescuePatientNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="psocdRescuePatientNeo_Title" runat="server" Text="Was any intervention attempted in order to “rescue” the patient (i.e., to prevent, to minimize, or to reverse harm)?" class="required"></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdRescuePatientNeo" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdRescuePatientNeo" RMXRef="/Instance/Document/Event/PatientPSO/psocdRescuePatientNeo" RMXType="code" />             
         </td> 
     </tr>
     <%--<tr id="psolstRescuePerformNeo_ctlRow" runat="server" style="display:none">--%>
     <tr id="psolstRescuePerformNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="psolstRescuePerformNeo_Title" runat="server" Text="Which of the following interventions (rescue) were performed?" class=""></asp:Label>
         </td>
         <td>
            <uc:MultiCode runat="server" ID="psolstRescuePerformNeo" width="200px" Height="88px" CodeTable="PSO_RESCUE_INTV" ControlName="psolstRescuePerformNeo" RMXRef="/Instance/Document/Event/PatientPSO/psolstRescuePerformNeo" RMXType="codelist" />             
         </td> 
     </tr>
     <%--<tr id="txtOtherRescuePerformNeo_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtOtherRescuePerformNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="txtOtherRescuePerformNeo_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
         </td>
         <td> 
             <span class="formw">
                 <asp:TextBox runat="Server" id="txtOtherRescuePerformNeo" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherRescuePerformNeo" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                 <input type="button" class="button" value="..." name="btnOtherRescuePerformNeo" id="btnOtherRescuePerformNeo" onclick="EditMemo('txtOtherRescuePerformNeo','')" />
             </span>
         </td>              
     </tr>
     <tr id="cdResultLengthNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdResultLengthNeo_Title" runat="server" Text="Did, or will, the incident result in an increased length of stay?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdResultLengthNeo" CodeTable="PSO_INC_LEN_STAY" ControlName="cdResultLengthNeo" RMXRef="/Instance/Document/Event/PatientPSO/cdResultLengthNeo" RMXType="code" />             
         </td> 
     </tr>
     <tr id="cdIncidentNotifiedNeo_ctlRow" runat="server">
         <td>
            <asp:Label ID="cdIncidentNotifiedNeo_Title" runat="server" Text="After the discovery of the incident, was the patient, patient’s family, or guardian notified?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="cdIncidentNotifiedNeo" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdIncidentNotifiedNeo" RMXRef="/Instance/Document/Event/PatientPSO/cdIncidentNotifiedNeo" RMXType="code" />             
         </td> 
     </tr>    
 </table>
</div>   
<div class="msgheader" id="pnlPressureUlcer_header" runat="server">Pressure Ulcer</div>
<div id="pnlPressureUlcer" runat="server">
<table border="0" cellspacing="0" cellpadding="0" id="pnlPressureUlcer_table" runat="server" width="100%">
    <tr id="psocdUlcerStage_ctlRow" runat="server">
        <td width="60%"> 
            <asp:Label ID="psocdUlcerStage_Title" runat="server" Text="What was the most advanced stage of the pressure ulcer or suspected Deep Tissue Injury being reported?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdUlcerStage" CodeTable="PSO_SDTI_ADV_STAG" ControlName="psocdUlcerStage" RMXRef="/Instance/Document/Event/PatientPSO/psocdUlcerStage" RMXType="code" />             
            <asp:Image runat="server" ID="psocdUlcerStage_Image" ImageUrl="~/Images/HelpIcon.jpg" style="display:none;visibility:hidden"/>
        </td>
    </tr>
    <%--<tr id="psocdTissueInjury_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdTissueInjury_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdTissueInjury_Title" runat="server" Text="What was the status of the suspected Deep Tissue Injury on admission?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdTissueInjury" CodeTable="PSO_SDTI_STAT" ControlName="psocdTissueInjury" RMXRef="/Instance/Document/Event/PatientPSO/psocdTissueInjury" RMXType="code" />             
        </td>
    </tr>        
    <%--<tr id="psocdUnstageablePressure_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdUnstageablePressure_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdUnstageablePressure_Title" runat="server" Text="What was the status of the Stage 3, 4, or unstageable pressure ulcer on admission?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdUnstageablePressure" CodeTable="PSO_STAGE_STAT" ControlName="psocdUnstageablePressure" RMXRef="/Instance/Document/Event/PatientPSO/psocdUnstageablePressure" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="cdSkinInspection_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdSkinInspection_ctlRow" runat="server" >
        <td>
            <asp:Label ID="cdSkinInspection_Title" runat="server" Text="On admission to this facility, was a skin inspection documented" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdSkinInspection" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdSkinInspection" RMXRef="/Instance/Document/Event/PatientPSO/cdSkinInspection" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psocdUlserAssessment_ctlRow" runat="server" style="display:none" >--%>
    <tr id="psocdUlserAssessment_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdUlserAssessment_Title" runat="server" Text="When was the first pressure ulcer risk assessment performed?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdUlserAssessment" CodeTable="PSO_FRST_RISK_TIME" ControlName="psocdUlserAssessment" RMXRef="/Instance/Document/Event/PatientPSO/psocdUlserAssessment" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="cdRiskAssessmentType_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdRiskAssessmentType_ctlRow" runat="server" >
        <td>
            <asp:Label ID="cdRiskAssessmentType_Title" runat="server" Text="What type of risk assessment was performed?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdRiskAssessmentType" CodeTable="PSO_RISK_TYPE" ControlName="cdRiskAssessmentType" RMXRef="/Instance/Document/Event/PatientPSO/cdRiskAssessmentType" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="cdUlcerRisk_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdUlcerRisk_ctlRow" runat="server">
        <td>
            <asp:Label ID="cdUlcerRisk_Title" runat="server" Text="As a result of the assessment, was the patient documented to be at increased risk for pressure ulcer?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdUlcerRisk" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdUlcerRisk" RMXRef="/Instance/Document/Event/PatientPSO/cdUlcerRisk" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psocdIntervention_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdIntervention_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdIntervention_Title" runat="server" Text="Was any preventive intervention implemented?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdIntervention" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdIntervention" RMXRef="/Instance/Document/Event/PatientPSO/psocdIntervention" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psolstInterventionType_ctlRow" runat="server" style="display:none">--%>
    <tr id="psolstInterventionType_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psolstInterventionType_Title" runat="server" Text="What intervention(s) was used?" class=""></asp:Label>
        </td>
        <td> 
            <uc:MultiCode runat="server" ID="psolstInterventionType" width="200px" Height="88px" CodeTable="PSO_INTERVN_TYP" ControlName="psolstInterventionType" RMXRef="/Instance/Document/Event/PatientPSO/psolstInterventionType" RMXType="codelist" />
        </td>
    </tr>
    <%--<tr id="txtOtehrInterventionType_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtehrInterventionType_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtehrInterventionType_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtehrInterventionType" RMXRef="/Instance/Document/Event/PatientPSO/txtOtehrInterventionType" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtehrInterventionType" id="btnOtehrInterventionType" onclick="EditMemo('txtOtehrInterventionType','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdUlcerdeviceUsed_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdUlcerdeviceUsed_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdUlcerdeviceUsed_Title" runat="server" Text="Was the use of a device or appliance involved in the development or advancement of the pressure ulcer?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdUlcerdeviceUsed" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdUlcerdeviceUsed" RMXRef="/Instance/Document/Event/PatientPSO/psocdUlcerdeviceUsed" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psocdApplianceType_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdApplianceType_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdApplianceType_Title" runat="server" Text="What was the type of device or appliance?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdApplianceType" CodeTable="PSO_DEV_INV_TYP" ControlName="psocdApplianceType" RMXRef="/Instance/Document/Event/PatientPSO/psocdApplianceType" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtOtherApplianceType_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherApplianceType_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherApplianceType_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtherApplianceType" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherApplianceType" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtherApplianceType" id="btnOtherApplianceType" onclick="EditMemo('txtOtherApplianceType','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdTubeType_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdTubeType_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdTubeType_Title" runat="server" Text="What was the type of tube?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdTubeType" CodeTable="PSO_TUBE_TYP" ControlName="psocdTubeType" RMXRef="/Instance/Document/Event/PatientPSO/psocdTubeType" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtOtherTubeType_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherTubeType_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherTubeType_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtherTubeType" RMXRef="/Instance/Document/Event/PatientPSO/txtOtherTubeType" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtherTubeType" id="btnOtherTubeType" onclick="EditMemo('txtOtherTubeType','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdSecondaryMorbidity_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdSecondaryMorbidity_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdSecondaryMorbidity_Title" runat="server" Text="During the patient’s stay at this facility, did the patient develop a secondary morbidity (e.g., osteomyelitis or sepsis)?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdSecondaryMorbidity" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdSecondaryMorbidity" RMXRef="/Instance/Document/Event/PatientPSO/psocdSecondaryMorbidity" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="cdMorbiditySuspected_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdMorbiditySuspected_ctlRow" runat="server" >
        <td>
            <asp:Label ID="cdMorbiditySuspected_Title" runat="server" Text="Was the secondary morbidity attributed to the presence of the pressure ulcer or suspected Deep Tissue Injury?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdMorbiditySuspected" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdMorbiditySuspected" RMXRef="/Instance/Document/Event/PatientPSO/cdMorbiditySuspected" RMXType="code" />             
        </td>
    </tr>
</table>
</div>
 <div class="msgheader" id="pnlSurgery_header" runat="server">Surgery Or Anesthesia</div>
 <div id="pnlSurgery" runat="server">
 <table border="0" cellspacing="0" cellpadding="0" id="pnlSurgery_table"  runat="server" width="100%">
      <tr id="txtDescProcdure_ctlRow" runat="server">
        <td width="60%">
          <asp:Label ID="txtDescProcdure_Title" runat="server" Text="Describe briefly the procedure associated with this event" class=""></asp:Label>
        </td>
        <td width="40%"> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtDescProcdure" RMXRef="/Instance/Document/Event/EventPSO/txtDescProcdure" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnDescProcdure" id="btnDescProcdure" onclick="EditMemo('txtDescProcdure','')" />
            </span>
         </td>              
      </tr>
      <tr id="cdICD9CM_ctlRow" runat="server">
        <td>
          <asp:Label ID="cdICD9CM_Title" runat="server" Text="Enter ICD-9-CM procedure code associated with this event" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdICD9CM" CodeTable="PSO_SURG_PROC" ControlName="cdICD9CM" RMXRef="/Instance/Document/Event/EventPSO/cdICD9CM" RMXType="code" />             
        </td>
      </tr>
      <tr id="psocdDocumentedASA_ctlRow" runat="server">
        <td>
          <asp:Label ID="psocdDocumentedASA_Title" runat="server" Text="What was the patient’s documented American Society of Anesthesiologists (ASA) Physical Classification System class?" class=""></asp:Label>
        </td>
        <td> 
          <uc2:CodeLookUp runat="server" ID="psocdDocumentedASA" CodeTable="PSO_ASA_CLASS" ControlName="psocdDocumentedASA" RMXRef="/Instance/Document/Event/EventPSO/psocdDocumentedASA" RMXType="code" />             
        </td>
      </tr>
      <tr id="cdProcEmergency_ctlRow" runat="server">
        <td>
           <asp:Label ID="cdProcEmergency_Title" runat="server" Text="Was the procedure performed as an emergency?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdProcEmergency" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdProcEmergency" RMXRef="/Instance/Document/Event/EventPSO/cdProcEmergency" RMXType="code" />             
        </td>
      </tr>
      <tr id="psocdEventDiscovered_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdEventDiscovered_Title" runat="server" Text="When was the event discovered?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdEventDiscovered" CodeTable="PSO_TIME_EVNT_DISC" ControlName="psocdEventDiscovered" RMXRef="/Instance/Document/Event/EventPSO/psocdEventDiscovered" RMXType="code" />             
        </td>
      </tr>
      <%--<tr id="psocdProcLength_ctlRow" runat="server" style="display:none">--%>
      <tr id="psocdProcLength_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdProcLength_Title" runat="server" Text="What was the total length of the procedure (i.e., induction of anesthesia to the end of anesthesia)?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdProcLength" CodeTable="PSO_PROC_DURTN" ControlName="psocdProcLength" RMXRef="/Instance/Document/Event/EventPSO/psocdProcLength" RMXType="code" />             
        </td>
      </tr>
      <tr id="psocdAnesthesiaType_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdAnesthesiaType_Title" runat="server" Text="What type of anesthesia or sedation was used?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdAnesthesiaType" CodeTable="PSO_SED_ANES_TYP" ControlName="psocdAnesthesiaType" RMXRef="/Instance/Document/Event/EventPSO/psocdAnesthesiaType" RMXType="code" />             
        </td>
      </tr>
      <%--<tr id="psocdSedationLevel_ctlRow" runat="server" style="display:none">--%>
      <tr id="psocdSedationLevel_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdSedationLevel_Title" runat="server" Text="What was the level of sedation?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdSedationLevel" CodeTable="PSO_SED_LVL" ControlName="psocdSedationLevel" RMXRef="/Instance/Document/Event/EventPSO/psocdSedationLevel" RMXType="code" />             
        </td>
      </tr>
      <tr id="psocdAdminAnesthesia_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdAdminAnesthesia_Title" runat="server" Text="Who administered (or, if the event occurred prior to administration of anesthesia, person who was scheduled to administer) the anesthesia?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdAdminAnesthesia" CodeTable="PSO_ANES_ADMIN" ControlName="psocdAdminAnesthesia" RMXRef="/Instance/Document/Event/EventPSO/psocdAdminAnesthesia" RMXType="code" />             
        </td>
      </tr>
      <%--<tr id="psocdAnesthesiologist_ctlRow" runat="server" style="display:none">--%>
      <tr id="psocdAnesthesiologist_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdAnesthesiologist_Title" runat="server" Text="Was there supervision by an anesthesiologist?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdAnesthesiologist" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdAnesthesiologist" RMXRef="/Instance/Document/Event/EventPSO/psocdAnesthesiologist" RMXType="code" />             
        </td>
      </tr>
      <tr id="psocdSugicalSpeciality_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdSugicalSpeciality_Title" runat="server" Text="What was the medical or surgical specialty of the provider who performed the procedure?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdSugicalSpeciality" CodeTable="PSO_PROVD_SPCL" ControlName="psocdSugicalSpeciality" RMXRef="/Instance/Document/Event/EventPSO/psocdSugicalSpeciality" RMXType="code" />             
            <asp:Image runat="server" ID="psocdSugicalSpeciality_Image" ImageUrl="~/Images/HelpIcon.jpg" style="display:none;visibility:hidden"/>
         </td>
      </tr>
      <%--<tr id="txtOtherSugicalSpeciality_ctlRow" runat="server" style="display:none">--%>
      <tr id="txtOtherSugicalSpeciality_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherSugicalSpeciality_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtOtherSugicalSpeciality" RMXRef="/Instance/Document/Event/EventPSO/txtOtherSugicalSpeciality" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherSugicalSpeciality" id="btnOtherSugicalSpeciality" onclick="EditMemo('txtOtherSugicalSpeciality','')" />
                </span>
         </td>              
      </tr>
      <tr id="psocdBestDecs_ctlRow" runat="server">
           <td>
                <asp:Label ID="psocdBestDecs_Title" runat="server" Text="What best describes the event?" class="required"></asp:Label>
           </td>
           <td> 
                <uc2:CodeLookUp runat="server" ID="psocdBestDecs" CodeTable="PSO_EVNT_DESC_TYP" ControlName="psocdBestDecs" RMXRef="/Instance/Document/Event/EventPSO/psocdBestDecs" RMXType="code" />             
           </td>
      </tr>
      <%--<tr id="psocdMajorComplications_ctlRow" runat="server" style="display:none">--%>
      <tr id="psocdMajorComplications_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdMajorComplications_Title" runat="server" Text="Which of the following major complications occurred?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdMajorComplications" CodeTable="PSO_SURG_COMPL" ControlName="psocdMajorComplications" RMXRef="/Instance/Document/Event/EventPSO/psocdMajorComplications" RMXType="code" />             
        </td>
      </tr>
      <%--<tr id="psocdRespiratorySupport_ctlRow" runat="server" style="display:none">--%>
      <tr id="psocdRespiratorySupport_ctlRow" runat="server">
         <td>
            <asp:Label ID="psocdRespiratorySupport_Title" runat="server" Text="Which of the following best describes the respiratory support provided?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdRespiratorySupport" CodeTable="PSO_RESP_SUPP_TYP" ControlName="psocdRespiratorySupport" RMXRef="/Instance/Document/Event/EventPSO/psocdRespiratorySupport" RMXType="code" />             
         </td>
     </tr>
     <%--<tr id="txtOtherRespiratorySupport_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtOtherRespiratorySupport_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherRespiratorySupport_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtOtherRespiratorySupport" RMXRef="/Instance/Document/Event/EventPSO/txtOtherRespiratorySupport" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherRespiratorySupport" id="btnOtherRespiratorySupport" onclick="EditMemo('txtOtherRespiratorySupport','')" />
                </span>
         </td>              
     </tr>
     <%--<tr id="txtOtherMajorComplications_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtOtherMajorComplications_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherMajorComplications_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtOtherMajorComplications" RMXRef="/Instance/Document/Event/EventPSO/txtOtherMajorComplications" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherMajorComplications" id="btnOtherMajorComplications" onclick="EditMemo('txtOtherMajorComplications','')" />
                </span>
        </td>              
     </tr>
    <tr id="psocdRestainedObject_ctlRow" runat="server" >
           <td width="60%">
                <asp:Label ID="psocdRestainedObject_Title" runat="server" Text="Was the surgical event an unintentionally retained object?" class="required"></asp:Label>
           </td>
           <td width="40%"> 
                <uc2:CodeLookUp runat="server" ID="psocdRestainedObject" CodeTable="PSO_YES_NO" ControlName="psocdRestainedObject" RMXRef="/Instance/Document/Event/EventPSO/psocdRestainedObject" RMXType="code" />             
           </td>
    </tr>
    <%--<tr id="psocdObjectRestained_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdObjectRestained_ctlRow" runat="server" style="visibility:visible">
        <td><asp:Label ID="psocdObjectRestained_Title" runat="server" Text="What type of object was retained?" class=""></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdObjectRestained" CodeTable="PSO_OBJ_TYP" ControlName="psocdObjectRestained" RMXRef="/Instance/Document/Event/EventPSO/psocdObjectRestained" RMXType="code" />             
         </td>
     </tr>
     <%--<tr id="txtOtherObjectRestained_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtOtherObjectRestained_ctlRow" runat="server" >
        <td><asp:Label ID="txtOtherObjectRestained_Title" runat="server" Text="If Other, Please Specify"></asp:Label></td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtOtherObjectRestained" RMXRef="/Instance/Document/Event/EventPSO/txtOtherObjectRestained" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherObjectRestained" id="btnOtherObjectRestained" onclick="EditMemo('txtOtherObjectRestained','')" />
                </span>
         </td>              
     </tr>
     <%--<tr id="psocdCountPerformed_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdCountPerformed_ctlRow" runat="server" style="visibility:visible">
        <td><asp:Label ID="psocdCountPerformed_Title" runat="server" Text="Was a count performed for the type of object that was retained?" class="required"></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdCountPerformed" CodeTable="PSO_CNNT_PERF" ControlName="psocdCountPerformed" RMXRef="/Instance/Document/Event/EventPSO/psocdCountPerformed" RMXType="code" />             
         </td>
     </tr>
     <%--<tr id="psocdCountStatus_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdCountStatus_ctlRow" runat="server" style="visibility:visible">
        <td><asp:Label ID="psocdCountStatus_Title" runat="server" Text="After counting, what was the reported count status?" class="required"></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdCountStatus" CodeTable="PSO_CNT_STAT" ControlName="psocdCountStatus" RMXRef="/Instance/Document/Event/EventPSO/psocdCountStatus" RMXType="code" />             
         </td>
     </tr>
     <%--<tr id="psocdXRayObtained_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdXRayObtained_ctlRow" runat="server" style="visibility:visible">
        <td><asp:Label ID="psocdXRayObtained_Title" runat="server" Text="Was an x-ray obtained before the end of the procedure to detect the retained object?" class="required"></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdXRayObtained" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdXRayObtained" RMXRef="/Instance/Document/Event/EventPSO/psocdXRayObtained" RMXType="code" />             
         </td>
     </tr>
     <%--<tr id="psocdRadiopaque_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdRadiopaque_ctlRow" runat="server" style="visibility:visible">
        <td><asp:Label ID="psocdRadiopaque_Title" runat="server" Text="Was the retained object radiopaque (i.e., detectable by x-ray)?" class=""></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdRadiopaque" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdRadiopaque" RMXRef="/Instance/Document/Event/EventPSO/psocdRadiopaque" RMXType="code" />             
         </td>
     </tr>
    <%--<tr id="psocdCharacterizedSurgery_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdCharacterizedSurgery_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdCharacterizedSurgery_Title" runat="server" Text="Which of the following best characterizes the surgical event?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdCharacterizedSurgery" CodeTable="PSO_SURG_ADV_OUTC" ControlName="psocdCharacterizedSurgery" RMXRef="/Instance/Document/Event/EventPSO/psocdCharacterizedSurgery" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psocdWhichOccured_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdWhichOccured_ctlRow" runat="server" >
        <td><asp:Label ID="psocdWhichOccured_Title" runat="server" Text="Which of the following occurred?" class=""></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdWhichOccured" CodeTable="PSO_BURN_FIRE_OCC" ControlName="psocdWhichOccured" RMXRef="/Instance/Document/Event/EventPSO/psocdWhichOccured" RMXType="code" />             
         </td>
    </tr> 
     <%--<tr id="psocdIncorrectProc_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdIncorrectProc_ctlRow" runat="server" >
        <td><asp:Label ID="psocdIncorrectProc_Title" runat="server" Text="What was incorrect about the surgical or invasive procedure?" class=""></asp:Label></td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdIncorrectProc" CodeTable="PSO_SURGINV_INC_AC" ControlName="psocdIncorrectProc" RMXRef="/Instance/Document/Event/EventPSO/psocdIncorrectProc" RMXType="code" />             
         </td>
     </tr>
     <%--<tr id="txtOtherIncorrectProc_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtOtherIncorrectProc_ctlRow" runat="server" >
        <td><asp:Label ID="txtOtherIncorrectProc_Title" runat="server" Text="If Other, Please Specify"></asp:Label></td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtOtherIncorrectProc" RMXRef="/Instance/Document/Event/EventPSO/txtOtherIncorrectProc" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherIncorrectProc" id="btnOtherIncorrectProc" onclick="EditMemo('txtOtherIncorrectProc','')" />
                </span>
         </td>              
     </tr>  
      <%--<tr id="txtOtherCharSurgery_ctlRow" runat="server" style="display:none">--%>
      <tr id="txtOtherCharSurgery_ctlRow" runat="server">
       <td>
            <asp:Label ID="txtOtherCharSurgery_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
       </td>
       <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtOtherCharSurgery" RMXRef="/Instance/Document/Event/EventPSO/txtOtherCharSurgery" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherCharSurgery" id="btnOtherCharSurgery" onclick="EditMemo('txtOtherCharSurgery','')" />
             </span>
       </td>              
    </tr>
     <tr id="psocdEventAnesthesia_ctlRow" runat="server" style="visibility:visible">
          <td>
                <asp:Label ID="psocdEventAnesthesia_Title" runat="server" Text="If the event involved anesthesia, which of the following best characterizes the event?" class="required"></asp:Label>
          </td>
          <td> 
                <uc2:CodeLookUp runat="server" ID="psocdEventAnesthesia" CodeTable="PSO_ANESEVNT_CHARC" ControlName="psocdEventAnesthesia" RMXRef="/Instance/Document/Event/EventPSO/psocdEventAnesthesia" RMXType="code" />             
          </td>
     </tr>
     <%--<tr id="txtOtherEventAnesthesia_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtOtherEventAnesthesia_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherEventAnesthesia_Title" runat="server" Text="If Other, Please Specify"></asp:Label></td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtOtherEventAnesthesia" RMXRef="/Instance/Document/Event/EventPSO/txtOtherEventAnesthesia" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherEventAnesthesia" id="btnOtherEventAnesthesia" onclick="EditMemo('txtOtherEventAnesthesia','')" />
                </span>
        </td>              
     </tr>
     <%--<tr id="psocdAirwayMngmnt_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdAirwayMngmnt_ctlRow" runat="server" style="visibility:visible">
         <td>
            <asp:Label ID="psocdAirwayMngmnt_Title" runat="server" Text="Which of the following best characterizes the airway management problem?" class=""></asp:Label>
         </td>
         <td> 
            <uc2:CodeLookUp runat="server" ID="psocdAirwayMngmnt" CodeTable="PSO_AIR_PROB" ControlName="psocdAirwayMngmnt" RMXRef="/Instance/Document/Event/EventPSO/psocdAirwayMngmnt" RMXType="code"  />             
         </td>
     </tr>
     <%--<tr id="txtOtherAirwayMngmnt_ctlRow" runat="server" style="display:none">--%>
     <tr id="txtOtherAirwayMngmnt_ctlRow" runat="server">
        <td><asp:Label ID="txtOtherAirwayMngmnt_Title" runat="server" Text="If Other, Please Specify"></asp:Label></td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtOtherAirwayMngmnt" RMXRef="/Instance/Document/Event/EventPSO/txtOtherAirwayMngmnt" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnOtherAirwayMngmnt" id="btnOtherAirwayMngmnt" onclick="EditMemo('txtOtherAirwayMngmnt','')" />
                </span>
         </td>              
    </tr>
</table>
</div>
<div class="msgheader" id="pnlDevice_header" runat="server">Device Or Medical/Surgical Supply</div><%--RMA-10825 msampathkuma--%>
<div id="pnlDevice" runat="server">
<table border="0" cellspacing="0" cellpadding="0" id="pnlDevice_table" runat="server" width="100%">
    <tr id="psocdDeviceEventDesc_ctlRow" runat="server">
        <td  width="60%">
            <asp:Label ID="psocdDeviceEventDesc_Title" runat="server" Text="Which of the following best describes the event or unsafe condition?" class="required"></asp:Label>
        </td>
        <td  width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdDeviceEventDesc" CodeTable="PSO_DEVIC_DESC" ControlName="psocdDeviceEventDesc" RMXRef="/Instance/Document/Event/EventPSO/psocdDeviceEventDesc" RMXType="code"  />             
        </td>
    </tr>
    <%--<tr id="psocdDeviceFailureType_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdDeviceFailureType_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdDeviceFailureType_Title" runat="server" Text="Which of the following best describes the device’s involvement in the event?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdDeviceFailureType" CodeTable="PSO_DEVIC_FAIL_DES" ControlName="psocdDeviceFailureType" RMXRef="/Instance/Document/Event/EventPSO/psocdDeviceFailureType" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psocdOperatorErrorType_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdOperatorErrorType_ctlRow" runat="server" >
        <td  width="60%">
            <asp:Label ID="psocdOperatorErrorType_Title" runat="server" Text="What type of operator error?" class=""></asp:Label>
        </td>
        <td  width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdOperatorErrorType" CodeTable="PSO_OPR_ERR" ControlName="psocdOperatorErrorType" RMXRef="/Instance/Document/Event/EventPSO/psocdOperatorErrorType" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtOtherOperatorErrorType_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherOperatorErrorType_ctlRow" runat="server">
         <td>
            <asp:Label ID="txtOtherOperatorErrorType_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
         </td>
         <td> 
             <span class="formw">
                 <asp:TextBox runat="Server" id="txtOtherOperatorErrorType" RMXRef="/Instance/Document/Event/EventPSO/txtOtherOperatorErrorType" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                 <input type="button" class="button" value="..." name="btnOtherOperatorErrorType" id="btnOtherOperatorErrorType" onclick="EditMemo('txtOtherOperatorErrorType','')" />
             </span>
         </td>              
    </tr>
    <tr id="psocdDeviceReuse_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdDeviceReuse_Title" runat="server" Text="Did the event involve reuse of a device intended for single use (including use of a reprocessed single-use device)?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdDeviceReuse" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdDeviceReuse" RMXRef="/Instance/Document/Event/EventPSO/psocdDeviceReuse" RMXType="code" />             
        </td>
    </tr>    
    <%--<tr id="DevicesGrid_ctlRow" runat="server">
        <td>
        <dg:UserControlDataGrid runat="server" ID="DevicesGrid" GridName="DevicesGrid" GridTitle="Device(s) Involved Details" Target="/Document/Event/EventPSO/DevicesInfoList" Ref="/Instance/Document/form//control[@name='DevicesGrid']" Unique_Id="DeviceRowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|SessionId|DeviceRowId|" textcolumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="600" PopupHeight="500" Type="GridAndButtons" />        
        </td>
    </tr> --%>   
</table>
<table border="0" cellspacing="0" cellpadding="0" id="Table1" runat="server" width="75%">
    <tr id="DevicesGrid_ctlRow" runat="server">
        <td>
            <dg:UserControlDataGrid runat="server" ID="DevicesGrid" GridName="DevicesGrid" GridTitle="Device(s) Involved Details" Target="/Document/Event/EventPSO/DevicesInfoList" Ref="/Instance/Document/form//control[@name='DevicesGrid']" Unique_Id="DeviceRowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|SessionId|DeviceRowId|" textcolumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="600" PopupHeight="500" Type="GridAndButtons" />        
        </td>
    </tr>
</table>
 <asp:TextBox Style="display: none" runat="server" ID="DevicesGridSelectedId" RMXType="id" />
 <asp:TextBox Style="display: none" runat="server" ID="DevicesGrid_RowDeletedFlag" RMXType="id" Text="false" />
 <asp:TextBox Style="display: none" runat="server" ID="DevicesGrid_Action" RMXType="id" />
 <asp:TextBox Style="display: none" runat="server" ID="DevicesGrid_RowAddedFlag" RMXType="id" Text="false" />
</div>    
<div class="msgheader" id="pnlMedication_header" runat="server">Medication Or Other Substance</div>
<div id="pnlMedication" runat="server">
<table border="0" cellspacing="0" cellpadding="0" id="pnlMedication_table" runat="server" width="100%">
    <%--<tr id="psocdSubstanceTypeINC_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdSubstanceTypeINC_ctlRow" runat="server">
        <td width="60%">
            <asp:Label ID="psocdSubstanceTypeINC_Title" runat="server" Text="What type of medication/substance was involved?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdSubstanceTypeINC" CodeTable="PSO_SUBS_TYP" ControlName="psocdSubstanceTypeINC" RMXRef="/Instance/Document/Event/EventPSO/psocdSubstanceTypeINC" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psocdSubstanceTypeNM_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdSubstanceTypeNM_ctlRow" runat="server">
        <td width="60%">
            <asp:Label ID="psocdSubstanceTypeNM_Title" runat="server" Text="What type of medication/substance was involved?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdSubstanceTypeNM" CodeTable="PSO_SUBS_TYP" ControlName="psocdSubstanceTypeNM" RMXRef="/Instance/Document/Event/EventPSO/psocdSubstanceTypeNM" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psocdSubstanceTypeUSC_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdSubstanceTypeUSC_ctlRow" runat="server">
        <td width="60%">
            <asp:Label ID="psocdSubstanceTypeUSC_Title" runat="server" Text="What type of medication/substance was involved?" class="required"></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdSubstanceTypeUSC" CodeTable="PSO_SUBS_TYP" ControlName="psocdSubstanceTypeUSC" RMXRef="/Instance/Document/Event/EventPSO/psocdSubstanceTypeUSC" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtOtherSubstanceType_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherSubstanceType_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherSubstanceType_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherSubstanceType" RMXRef="Instance/Document/Event/EventPSO/txtOtherSubstanceType" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherSubstanceType" id="Button2" onclick="EditMemo('txtOtherSubstanceType','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdMedicationTypeINC_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdMedicationTypeINC_ctlRow" runat="server">
            <td><asp:Label ID="psocdMedicationTypeINC_Title" runat="server" Text="What type of medication?" class="required"></asp:Label></td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdMedicationTypeINC" CodeTable="PSO_MED_TYPE" ControlName="psocdMedicationTypeINC" RMXRef="Instance/Document/Event/EventPSO/psocdMedicationTypeINC" RMXType="code" />             
        </td>
    </tr>
     <%--<tr id="psocdMedicationTypeNM_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdMedicationTypeNM_ctlRow" runat="server">
            <td><asp:Label ID="psocdMedicationTypeNM_Title" runat="server" Text="What type of medication?" class="required"></asp:Label></td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdMedicationTypeNM" CodeTable="PSO_MED_TYPE" ControlName="psocdMedicationTypeNM" RMXRef="Instance/Document/Event/EventPSO/psocdMedicationTypeNM" RMXType="code" />             
        </td>
    </tr>
     <%--<tr id="psocdMedicationTypeUSC_ctlRow" runat="server" style="display:none">--%>
     <tr id="psocdMedicationTypeUSC_ctlRow" runat="server">
            <td><asp:Label ID="psocdMedicationTypeUSC_Title" runat="server" Text="What type of medication?" class="required"></asp:Label></td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdMedicationTypeUSC" CodeTable="PSO_MED_TYPE" ControlName="psocdMedicationTypeUSC" RMXRef="Instance/Document/Event/EventPSO/psocdMedicationTypeUSC" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtIngredients_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtIngredients_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtIngredients_Title" runat="server" Text="Please list all ingredients" class=""></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtIngredients" RMXRef="Instance/Document/Event/EventPSO/txtIngredients" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnIngredients" id="btnIngredients" onclick="EditMemo('txtIngredients','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdBiologicalType_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdBiologicalType_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdBiologicalType_Title" runat="server" Text="What type of biological product?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdBiologicalType" CodeTable="PSO_BIO_PROD_TYP" ControlName="psocdBiologicalType" RMXRef="Instance/Document/Event/EventPSO/psocdBiologicalType" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtLotNum_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtLotNum_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtLotNum_Title" runat="server" Text="What was the lot number of the vaccine?" class=""></asp:Label>
        </td>
        <td> 
            <asp:TextBox ID="txtLotNum" runat="server" RMXType="text" size="30" RMXRef="Instance/Document/Event/EventPSO/txtLotNum" MaxLength="100"></asp:TextBox>
        </td>              
    </tr>
    <%--<tr id="psocdNutritionalProduct_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdNutritionalProduct_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdNutritionalProduct_Title" runat="server" Text="What type of nutritional product?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdNutritionalProduct" CodeTable="PSO_NUT_PROD_TYP" ControlName="psocdNutritionalProduct" RMXRef="Instance/Document/Event/EventPSO/psocdNutritionalProduct" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtOtherNutritionalProd_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherNutritionalProd_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherNutritionalProd_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherNutritionalProd" RMXRef="Instance/Document/Event/EventPSO/txtOtherNutritionalProd" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherNutritionalProd" id="btnOtherNutritionalProd" onclick="EditMemo('txtOtherNutritionalProd','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdBestEventINC_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdBestEventINC_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdBestEventINC_Title" runat="server" Text="Which of the following best characterizes the event?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdBestEventINC" CodeTable="PSO_MED_EVNT_CHARC" ControlName="psocdBestEventINC" RMXRef="Instance/Document/Event/EventPSO/psocdBestEventINC" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psocdBestEventNM_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdBestEventNM_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdBestEventNM_Title" runat="server" Text="Which of the following best characterizes the event?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdBestEventNM" CodeTable="PSO_MED_EVNT_CHARC" ControlName="psocdBestEventNM" RMXRef="Instance/Document/Event/EventPSO/psocdBestEventNM" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psocdBestEventUSC_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdBestEventUSC_ctlRow" runat="server" style="visibility:visible">
        <td>
            <asp:Label ID="psocdBestEventUSC_Title" runat="server" Text="Which of the following best characterizes the event?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdBestEventUSC" CodeTable="PSO_MED_EVNT_CHARC" ControlName="psocdBestEventUSC" RMXRef="Instance/Document/Event/EventPSO/psocdBestEventUSC" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psolstIncorrectAction_ctlRow" runat="server" style="display:none">--%>
    <tr id="psolstIncorrectAction_ctlRow" runat="server">
        <td>
            <asp:Label ID="psolstIncorrectAction_Title" runat="server" Text="What was the incorrect action?" class="required"></asp:Label>
        </td>
        <td> 
            <uc:MultiCode runat="server" ID="psolstIncorrectAction" width="200px" Height="88px" CodeTable="PSO_MED_INCRT_AC" ControlName="psolstIncorrectAction" RMXRef="Instance/Document/Event/EventPSO/psolstIncorrectAction" RMXType="codelist" />
        </td>
    </tr>
    <%--<tr id="txtOtherIncorrAction_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherIncorrAction_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherIncorrAction_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherIncorrAction" RMXRef="Instance/Document/Event/EventPSO/txtOtherIncorrAction" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherIncorrAction" id="btnOtherIncorrAction" onclick="EditMemo('txtOtherIncorrAction','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="cdIncorrectDose_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdIncorrectDose_ctlRow" runat="server">
        <td>
            <asp:Label ID="cdIncorrectDose_Title" runat="server" Text="Which best describes the incorrect dose(s)?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdIncorrectDose" CodeTable="PSO_MED_INCRT_DOSE" ControlName="cdIncorrectDose" RMXRef="Instance/Document/Event/EventPSO/cdIncorrectDose" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="cdIncorrTiming_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdIncorrTiming_ctlRow" runat="server">
        <td>
            <asp:Label ID="cdIncorrTiming_Title" runat="server" Text="Which best describes the incorrect timing?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdIncorrTiming" CodeTable="PSO_MED_INCRT_TIME" ControlName="cdIncorrTiming" RMXRef="Instance/Document/Event/EventPSO/cdIncorrTiming" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="cdIncorrRate_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdIncorrRate_ctlRow" runat="server">
        <td>
            <asp:Label ID="cdIncorrRate_Title" runat="server" Text="Which best describes the incorrect rate?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdIncorrRate" CodeTable="PSO_MED_INCRT_RATE" ControlName="cdIncorrRate" RMXRef="Instance/Document/Event/EventPSO/cdIncorrRate" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="cdIncorrStrength_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdIncorrStrength_ctlRow" runat="server">
        <td>
            <asp:Label ID="cdIncorrStrength_Title" runat="server" Text="Which best describes the incorrect strength or concentration?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdIncorrStrength" CodeTable="PSO_MED_INCRT_STRN" ControlName="cdIncorrStrength" RMXRef="Instance/Document/Event/EventPSO/cdIncorrStrength" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="dtExpireDate_ctlRow" runat="server" style="display:none">--%>
    <tr id="dtExpireDate_ctlRow" runat="server">
        <td>
            <asp:Label ID="dtExpireDate_Title" runat="server" Text="What was the expiration date?" class=""></asp:Label>
        </td>
        <td>
            <asp:TextBox runat="server" FormatAs="date" ID="dtExpireDate" RMXRef="Instance/Document/Event/EventPSO/dtExpireDate"
                        RMXType="date" onchange="CopyDateToValidate(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                    <asp:Button class="DateLookupControl" runat="server" ID="btnExpireDate" />
                    <asp:TextBox ID="dtExpireDate_ValidateDate" Style="display: none" runat="server" RMXType="text" size="" RMXRef="Instance/Document/Event/EventPSO/dtExpireDate"></asp:TextBox>
                        <%--<script type="text/javascript">
                            Zapatec.Calendar.setup({ inputField: "dtExpireDate", ifFormat: "%m/%d/%Y", button: "btnExpireDate" });
                        </script>--%>

                        <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#dtExpireDate").datepicker({
            showOn: "button",
            buttonImage: "../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
        </td> 
    </tr>
    <%--<tr id="cdDocHistory_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdDocHistory_ctlRow" runat="server">
        <td>
            <asp:Label ID="cdDocHistory_Title" runat="server" Text="Was there a documented history of allergies or sensitivities to the medication/substance administered?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdDocHistory" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdDocHistory" RMXRef="Instance/Document/Event/EventPSO/cdDocHistory" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="psocdContradictions_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdContradictions_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdContradictions_Title" runat="server" Text="What was the contraindication (potential or actual interaction)?" class=""></asp:Label></td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdContradictions" CodeTable="PSO_MED_CONTRAIND" ControlName="psocdContradictions" RMXRef="Instance/Document/Event/EventPSO/psocdContradictions" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtContradictions_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtContradictions_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtContradictions_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtContradictions" RMXRef="Instance/Document/Event/EventPSO/txtContradictions" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnContradictions" id="btnContradictions" onclick="EditMemo('txtContradictions','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdIntendedRoute_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdIntendedRoute_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdIntendedRoute_Title" runat="server" Text="What was the intended route of administration?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdIntendedRoute" CodeTable="PSO_INT_ROUTE" ControlName="psocdIntendedRoute" RMXRef="Instance/Document/Event/EventPSO/psocdIntendedRoute" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtOtherIntendedRoute_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherIntendedRoute_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherIntendedRoute_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherIntendedRoute" RMXRef="Instance/Document/Event/EventPSO/txtOtherIntendedRoute" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherIntendedRoute" id="btnOtherIntendedRoute" onclick="EditMemo('txtOtherIntendedRoute','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdActualRoute_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdActualRoute_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdActualRoute_Title" runat="server" Text="What was the actual route of administration (attempted or completed)?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdActualRoute" CodeTable="PSO_ACT_ROUTE" ControlName="psocdActualRoute" RMXRef="Instance/Document/Event/EventPSO/psocdActualRoute" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtOtherActualRoute_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherActualRoute_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherActualRoute_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherActualRoute" RMXRef="Instance/Document/Event/EventPSO/txtOtherActualRoute" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherActualRoute" id="btnOtherActualRoute" onclick="EditMemo('txtOtherActualRoute','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="psocdEventOriginate_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdEventOriginate_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdEventOriginate_Title" runat="server" Text="At what stage in the process did the event originate, regardless of the stage at which it was discovered?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdEventOriginate" CodeTable="PSO_ORIGIN_STAGE" ControlName="psocdEventOriginate" RMXRef="Instance/Document/Event/EventPSO/psocdEventOriginate" RMXType="code" />             
        </td>
    </tr>
    <%--<tr id="txtOtherEventOriginate_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherEventOriginate_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherEventOriginate_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
                <asp:TextBox runat="Server" id="txtOtherEventOriginate" RMXRef="Instance/Document/Event/EventPSO/txtOtherEventOriginate" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                <input type="button" class="button" value="..." name="btnOtherEventOriginate" id="btnOtherEventOriginate" onclick="EditMemo('txtOtherEventOriginate','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="INCMEDGrid_ctlRow" runat="server" style="display:none">--%>
    <%--<tr id="INCMEDGrid_ctlRow" runat="server">
        <td>
            <dg:UserControlDataGrid runat="server" ID="INCMEDGrid" GridName="INCMEDGrid" GridTitle="Medication(s) Involved Details" Target="/Document/Event/EventPSO/INCMedInfoList" Ref="/Instance/Document/form//control[@name='INCMEDGrid']" Unique_Id="MedRowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|SessionId|MedRowId|" textcolumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="600" PopupHeight="500" Type="GridAndButtons"/>
        </td>
    </tr>--%>
    <%--<tr id="NMMEDGrid_ctlRow" runat="server" style="display:none">--%>
    <%--<tr id="NMMEDGrid_ctlRow" runat="server">
        <td>
            <dg:UserControlDataGrid runat="server" ID="NMMEDGrid" GridName="NMMEDGrid" GridTitle="Medication(s) Involved Details" Target="Document/Event/EventPSO/NMMedInfoList" Ref="/Instance/Document/form//control[@name='NMMEDGrid']" Unique_Id="MedRowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|SessionId|MedRowId|" textcolumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="500" Type="GridAndButtons" />        
        </td>
    </tr>--%>
    <%--<tr id="USCMEDGrid_ctlRow" runat="server" style="display:none">--%>
    <%--<tr id="USCMEDGrid_ctlRow" runat="server">
        <td>
            <dg:UserControlDataGrid runat="server" ID="USCMEDGrid" GridName="USCMEDGrid" GridTitle="Medication(s) Involved Details" Target="Document/Event/EventPSO/USCMedInfoList" Ref="/Instance/Document/form//control[@name='USCMEDGrid']" Unique_Id="MedRowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|SessionId|MedRowId|" textcolumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="500" Type="GridAndButtons" />        
        </td>
    </tr> --%>
</table>
<table border="0" cellspacing="0" cellpadding="0" id="Table3" runat="server" width="75%">
    <%--<tr id="INCMEDGrid_ctlRow" runat="server" style="display:none">--%>
    <tr id="INCMEDGrid_ctlRow" runat="server">
        <td>
            <dg:UserControlDataGrid runat="server" ID="INCMEDGrid" GridName="INCMEDGrid" GridTitle="Medication(s) Involved Details" Target="/Document/Event/EventPSO/INCMedInfoList" Ref="/Instance/Document/form//control[@name='INCMEDGrid']" Unique_Id="MedRowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|SessionId|MedRowId|" textcolumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="600" PopupHeight="500" Type="GridAndButtons"/>
        </td>
    </tr>
    <%--<tr id="NMMEDGrid_ctlRow" runat="server" style="display:none">--%>
    <tr id="NMMEDGrid_ctlRow" runat="server">
        <td>
            <dg:UserControlDataGrid runat="server" ID="NMMEDGrid" GridName="NMMEDGrid" GridTitle="Medication(s) Involved Details" Target="Document/Event/EventPSO/NMMedInfoList" Ref="/Instance/Document/form//control[@name='NMMEDGrid']" Unique_Id="MedRowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|SessionId|MedRowId|" textcolumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="500" Type="GridAndButtons" />        
        </td>
    </tr>
    <%--<tr id="USCMEDGrid_ctlRow" runat="server" style="display:none">--%>
    <tr id="USCMEDGrid_ctlRow" runat="server">
        <td>
            <dg:UserControlDataGrid runat="server" ID="USCMEDGrid" GridName="USCMEDGrid" GridTitle="Medication(s) Involved Details" Target="Document/Event/EventPSO/USCMedInfoList" Ref="/Instance/Document/form//control[@name='USCMEDGrid']" Unique_Id="MedRowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|SessionId|MedRowId|" textcolumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="500" Type="GridAndButtons" />        
        </td>
    </tr> 
</table>
 <asp:TextBox Style="display: none" runat="server" ID="INCMEDGridSelectedId" RMXType="id" />
 <asp:TextBox Style="display: none" runat="server" ID="INCMEDGrid_RowDeletedFlag" RMXType="id" Text="false" />
 <asp:TextBox Style="display: none" runat="server" ID="INCMEDGrid_Action" RMXType="id" />
 <asp:TextBox Style="display: none" runat="server" ID="INCMEDGrid_RowAddedFlag" RMXType="id" Text="false" />
 <asp:TextBox Style="display: none" runat="server" ID="NMMEDGridSelectedId" RMXType="id" />
 <asp:TextBox Style="display: none" runat="server" ID="NMMEDGrid_RowDeletedFlag" RMXType="id" Text="false" />
 <asp:TextBox Style="display: none" runat="server" ID="NMMEDGrid_Action" RMXType="id" />
 <asp:TextBox Style="display: none" runat="server" ID="NMMEDGrid_RowAddedFlag" RMXType="id" Text="false" />
 <asp:TextBox Style="display: none" runat="server" ID="USCMEDGridSelectedId" RMXType="id" />
 <asp:TextBox Style="display: none" runat="server" ID="USCMEDGrid_RowDeletedFlag" RMXType="id" Text="false" />
 <asp:TextBox Style="display: none" runat="server" ID="USCMEDGrid_Action" RMXType="id" />
 <asp:TextBox Style="display: none" runat="server" ID="USCMEDGrid_RowAddedFlag" RMXType="id" Text="false" />
</div>
<div class="msgheader" id="pnlSIR_header" runat="server">Summary Of Initial Report(SIR)</div>
<div id="pnlSIR" runat="server">
<table border="0" cellspacing="0" cellpadding="0" id="pnlSIR_table" runat="server" width="100%">
    <tr id="dtReportedDate_ctlRow" runat="server">
        <td width="60%">
            <asp:Label ID="dtReportedDate_Title" runat="server" Text="What is the date of this report?"></asp:Label>
        </td>
        <td width="40%">
        <asp:TextBox runat="server" FormatAs="date" ID="dtReportedDate" RMXRef="/Instance/Document/Event/EventPSOData/dtReportedDate"
                        RMXType="date" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" ReadOnly="true" BackColor="#F2F2F2"/>
        </td>        
    </tr>
    <tr id="psocdEventLocation_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdEventLocation_Title" runat="server" Text="Where did the event occur, or, if an unsafe condition, where does it exist?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdEventLocation" CodeTable="PSO_EVT_LOC" ControlName="psocdEventLocation" RMXRef="/Instance/Document/Event/EventPSO/psocdEventLocation" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="txtOtherEventLocation_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherEventLocation_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherEventLocation_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtherEventLocation" RMXRef="/Instance/Document/Event/EventPSO/txtOtherEventLocation" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtherEventLocation" id="btnOtherEventLocation" onclick="EditMemo('txtOtherEventLocation','')" />
            </span>
        </td>              
    </tr>
    <tr id="psocdReportedUnsafe_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdReportedUnsafe_Title" runat="server" Text="Who reported the event or unsafe condition?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdReportedUnsafe" CodeTable="PSO_REPORTER_TYPE" ControlName="psocdReportedUnsafe" RMXRef="/Instance/Document/Event/EventPSO/psocdReportedUnsafe" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="psocdHealthcareProf_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdHealthcareProf_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdHealthcareProf_Title" runat="server" Text="What is the type of healthcare professional?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdHealthcareProf" CodeTable="PSO_HLTH_PROF" ControlName="psocdHealthcareProf" RMXRef="/Instance/Document/Event/EventPSO/psocdHealthcareProf" RMXType="code" />             
        </td>       
    </tr>
    <tr id="txtAdditionalDetails_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtAdditionalDetails_Title" runat="server" Text="Please describe any additional details about the event or unsafe condition discovered after completion of the HERF" class=""></asp:Label>
        </td>
        <td> 
            <span class="formw">
                  <asp:TextBox runat="Server" id="txtAdditionalDetails" RMXRef="/Instance/Document/Event/EventPSO/txtAdditionalDetails" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <input type="button" class="button" value="..." name="btnAdditionalDetails" id="btnAdditionalDetails" onclick="EditMemo('txtAdditionalDetails','')" />
                </span>
         </td>              
    </tr>
    <tr id="cdPreventedMiss_ctlRow" runat="server">
        <td width="60%">
            <asp:Label ID="cdPreventedMiss_Title" runat="server" Text="What prevented the near miss from reaching the patient?" class=""></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="cdPreventedMiss" CodeTable="PSO_MISS_ACTION" ControlName="cdPreventedMiss" RMXRef="/Instance/Document/Event/EventPSO/cdPreventedMiss" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="cdHandoverINC_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdHandoverINC_ctlRow" runat="server" >
        <td>
            <asp:Label ID="cdHandoverINC_Title" runat="server" Text="Was the event associated with a handover/handoff?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdHandoverINC" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdHandoverINC" RMXRef="/Instance/Document/Event/EventPSO/cdHandoverINC" RMXType="code" />             
        </td> 
    </tr>
      <%--<tr id="cdHandoverNM_ctlRow" runat="server" style="display:none">--%>
      <tr id="cdHandoverNM_ctlRow" runat="server" >
        <td>
            <asp:Label ID="cdHandoverNM_Title" runat="server" Text="Was the event associated with a handover/handoff?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdHandoverNM" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdHandoverNM" RMXRef="/Instance/Document/Event/EventPSO/cdHandoverNM" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="psocdEventFactor_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdEventFactor_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdEventFactor_Title" runat="server" Text="Are any contributing factors to the event known?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdEventFactor" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdEventFactor" RMXRef="/Instance/Document/Event/EventPSO/psocdEventFactor" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="psocdEventFactorNM_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdEventFactorNM_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdEventFactorNM_Title" runat="server" Text="Are any contributing factors to the event known?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdEventFactorNM" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdEventFactorNM" RMXRef="/Instance/Document/Event/EventPSO/psocdEventFactorNM" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="psolstFactors_ctlRow" runat="server" style="display:none">--%>
    <tr id="psolstFactors_ctlRow" runat="server">
        <td>
            <asp:Label ID="psolstFactors_Title" runat="server" Text="What factor(s) contributed to the event?" class=""></asp:Label>
        </td>
        <td> 
            <uc:MultiCode runat="server" ID="psolstFactors" width="200px" Height="88px" CodeTable="PSO_CONTRIB_FAC" ControlName="psolstFactors" RMXRef="/Instance/Document/Event/EventPSO/psolstFactors" RMXType="codelist" />
        </td> 
    </tr>
    <%--<tr id="psolstFactorsNM_ctlRow" runat="server" style="display:none">--%>
    <tr id="psolstFactorsNM_ctlRow" runat="server">
        <td>
            <asp:Label ID="psolstFactorsNM_Title" runat="server" Text="What factor(s) contributed to the event?" class=""></asp:Label>
        </td>
        <td> 
            <uc:MultiCode runat="server" ID="psolstFactorsNM" width="200px" Height="88px" CodeTable="PSO_CONTRIB_FAC" ControlName="psolstFactorsNM" RMXRef="/Instance/Document/Event/EventPSO/psolstFactorsNM" RMXType="codelist" />
        </td> 
    </tr>
    <%--<tr id="txtOtherFactors_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherFactors_ctlRow" runat="server" >
        <td>
            <asp:Label ID="txtOtherFactors_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtherFactors" RMXRef="/Instance/Document/Event/EventPSO/txtOtherFactors" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtherFactors" id="btnOtherFactors" onclick="EditMemo('txtOtherFactors','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="txtOtherFactorsNM_ctlRow" runat="server" style="display:none">--%>
    <tr id="txtOtherFactorsNM_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherFactorsNM_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtherFactorsNM" RMXRef="/Instance/Document/Event/EventPSO/txtOtherFactorsNM" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtherFactorsNM" id="btnOtherFactorsNM" onclick="EditMemo('txtOtherFactorsNM','')" />
            </span>
        </td>              
    </tr>
    <%--<tr id="cdHITImplicated_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdHITImplicated_ctlRow" runat="server">
        <td>
            <asp:Label ID="cdHITImplicated_Title" runat="server" Text="Was health information technology (HIT) implicated in this event?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdHITImplicated" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdHITImplicated" RMXRef="/Instance/Document/Event/EventPSO/cdHITImplicated" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="cdHITImplicatedNM_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdHITImplicatedNM_ctlRow" runat="server" >
        <td>
            <asp:Label ID="cdHITImplicatedNM_Title" runat="server" Text="Was health information technology (HIT) implicated in this event?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdHITImplicatedNM" CodeTable="PSO_YES_NO_JCAHO" ControlName="cdHITImplicatedNM" RMXRef="/Instance/Document/Event/EventPSO/cdHITImplicatedNM" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="psocdNQFEvent_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdNQFEvent_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdNQFEvent_Title" runat="server" Text="Was the event a National Quality Forum (NQF) Serious Reportable Event?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdNQFEvent" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdNQFEvent" RMXRef="/Instance/Document/Event/EventPSO/psocdNQFEvent" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="psocdNQFEventNM_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdNQFEventNM_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdNQFEventNM_Title" runat="server" Text="Was the event a National Quality Forum (NQF) Serious Reportable Event?" class="required"></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdNQFEventNM" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdNQFEventNM" RMXRef="/Instance/Document/Event/EventPSO/psocdNQFEventNM" RMXType="code" />             
        </td> 
    </tr>
    <%--<tr id="psocdSeriousEvent_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdSeriousEvent_ctlRow" runat="server" >
        <td>
            <asp:Label ID="psocdSeriousEvent_Title" runat="server" Text="What was the applicable Serious Reportable Event?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdSeriousEvent" CodeTable="PSO_NQF_TYP" ControlName="psocdSeriousEvent" RMXRef="/Instance/Document/Event/EventPSO/psocdSeriousEvent" RMXType="code" />            
        </td> 
    </tr>
    <%--<tr id="psocdSeriousEventNM_ctlRow" runat="server" style="display:none">--%>
    <tr id="psocdSeriousEventNM_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdSeriousEventNM_Title" runat="server" Text="What was the applicable Serious Reportable Event?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdSeriousEventNM" CodeTable="PSO_NQF_TYP" ControlName="psocdSeriousEventNM" RMXRef="/Instance/Document/Event/EventPSO/psocdSeriousEventNM" RMXType="code" />            
        </td> 
    </tr>
    <%--<tr id="cdPreventableIncident_ctlRow" runat="server" style="display:none">--%>
    <tr id="cdPreventableIncident_ctlRow" runat="server" >
        <td>
            <asp:Label ID="cdPreventableIncident_Title" runat="server" Text="How preventable was the incident?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdPreventableIncident" CodeTable="PSO_EVT_PREVENT" ControlName="cdPreventableIncident" RMXRef="/Instance/Document/Event/EventPSO/cdPreventableIncident" RMXType="code" />             
        </td>
    </tr>
    <%--<tr>
        <td>
            <dg:UserControlDataGrid runat="server" ID="LinkedEventsGrid" GridName="LinkedEventsGrid" GridTitle="Linked Event(s) Details" Target="/Document/Event/EventPSO/LinkedEventInfoList" Ref="/Instance/Document/form//control[@name='LinkedEventsGrid']" Unique_Id="LnkEventRowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|SessionId|LnkEventRowId|" textcolumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="600" PopupHeight="500" Type="GridAndButtons"/>        
        </td>
    </tr>--%>
</table>
<table border="0" cellspacing="0" cellpadding="0" id="Table2" runat="server" width="75%">
    <tr>
        <td>
            <dg:UserControlDataGrid runat="server" ID="LinkedEventsGrid" GridName="LinkedEventsGrid" GridTitle="Linked Event(s) Details" Target="/Document/Event/EventPSO/LinkedEventInfoList" Ref="/Instance/Document/form//control[@name='LinkedEventsGrid']" Unique_Id="LnkEventRowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|SessionId|LnkEventRowId|" textcolumn="SessionId" ShowHeader="True" LinkColumn="" PopupWidth="600" PopupHeight="500" Type="GridAndButtons"/>        
        </td>
    </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" id="tblVersionNumber" runat="server" width="100%">
<tr id="trVersionNumber" runat="server">
        <td>
            <asp:Label ID="lblVersionNumber" runat="server" Text="Version Number (System Generated)" class=""></asp:Label>
        </td>
        <td> 
            <asp:TextBox runat="Server" id="txtVersionNumber" RMXRef="/Instance/Document/Event/EventPSO/txtVersionNumber" RMXType="text" readonly="true" style="background-color: #F2F2F2;"  />
        </td> 
    </tr>
<tr id="trExtractStatus" runat="server">
        <td width="60%">
            <asp:Label ID="ExtractStatus" runat="server" Text="Extract Status" class=""></asp:Label>
        </td>
        <td width="40%"> 
            <uc2:CodeLookUp runat="server" ID="psocdExtractStatus" CodeTable="PSO_EXTRACT_STATUS" ControlName="psocdExtractStatus" RMXRef="/Instance/Document/Event/EventPSO/psocdExtractStatus" RMXType="code" Filter="CODES.SHORT_CODE IN(\'R\') "/> 
        </td> 
    </tr>
</table>
</div>
<div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnSave">
            <asp:button class="button" runat="server" id="btnPSOSave" RMXRef="" Text="OK" width="75px" onClientClick="confirmSubmit();return fnPSOOkClicked();" onclick="btnPSOOk_Click" ValidationGroup="btnSubmit"/>
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:button class="button" runat="server" id="btnPSOCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return fnPSOCancel();"  />
        </div>
 </div>
 <asp:TextBox Style="display: none" runat="server" ID="EvtPsoRowId" RMXRef="/Instance/Document/Event/EventPSO/EvtPsoRowId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="PatPsoRowId" RMXRef="/Instance/Document/Event/PatientPSO/PatPsoRowId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="PatInvEntId" RMXRef="/Instance/Document/Event/PatientPSO/PatInvEntId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="PatInvNeoEntId" RMXRef="/Instance/Document/Event/PatientPSO/PatInvNeoEntId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="hiddencontrols"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="pnlhiddencontrols"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="hiddencontrolstemp"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="pnlhiddencontrolstemp"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="requiredcontrols"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="saveclicked"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="formname" Text="PSOWebForm"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="posted"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="savebtnposted" ></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="gridsubmit"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtDeviceInfoSessionId" RMXRef="/Instance/Document/Event/EventPSOData/txtDeviceInfoSessionId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtPatEvtNeoId" RMXRef="/Instance/Document/Event/PatientPSOData/txtPatEvtNeoId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtINCMedInfoSessionId" RMXRef="/Instance/Document/Event/EventPSOData/txtINCMedInfoSessionId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtNMMedInfoSessionId" RMXRef="/Instance/Document/Event/EventPSOData/txtNMMedInfoSessionId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtUSCMedInfoSessionId" RMXRef="/Instance/Document/Event/EventPSOData/txtUSCMedInfoSessionId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtLnkEvtInfoSessionId" RMXRef="/Instance/Document/Event/EventPSOData/txtLnkEvtInfoSessionId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtDeviceInfoDeletedId" RMXRef="/Instance/Document/Event/EventPSOData/txtDeviceInfoDeletedId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtINCMedInfoDeletedId" RMXRef="/Instance/Document/Event/EventPSOData/txtINCMedInfoDeletedId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtNMMedInfoDeletedId" RMXRef="/Instance/Document/Event/EventPSOData/txtNMMedInfoDeletedId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtUSCMedInfoDeletedId" RMXRef="/Instance/Document/Event/EventPSOData/txtUSCMedInfoDeletedId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtLnkEvtInfoDeletedId" RMXRef="/Instance/Document/Event/EventPSOData/txtLnkEvtInfoDeletedId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtDeviceGridCount" RMXRef="/Instance/Document/Event/EventPSOData/txtDeviceGridCount"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtINCMedGridCount" RMXRef="/Instance/Document/Event/EventPSOData/txtINCMedGridCount"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtNMMedGridCount" RMXRef="/Instance/Document/Event/EventPSOData/txtNMMedGridCount"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtUSCMedGridCount" RMXRef="/Instance/Document/Event/EventPSOData/txtUSCMedGridCount"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtLnkEvtGridCount" RMXRef="/Instance/Document/Event/EventPSOData/txtLnkEvtGridCount"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtevtpsoevtid" RMXRef="/Instance/Document/Event/EventPSO/EventId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txtpatpsoevtid" RMXRef="/Instance/Document/Event/PatientPSO/EventId"></asp:TextBox>
 <asp:TextBox Style="display: none" runat="server" ID="txteventid" RMXRef="/Instance/Document/Event/EventPSOData/eventid"></asp:TextBox>
 <asp:TextBox style="display:none" runat="server" id="PatientCount" RMXRef="/Instance/Document/Event/EventPSOData/PatientCount" RMXType="hidden"/>
 <asp:TextBox Style="display: none" runat="server" ID="txtFunctionToCall" />
 <asp:TextBox style="display:none" runat="server" id="txtGridDataDeleted" RMXRef="/Instance/Document/Event/EventPSOData/txtGridDataDeleted" RMXType="hidden"/>
 <asp:TextBox Style="display: none" runat="server" ID="SysPageDataChanged" RMXRef="/Instance/Document/Event/SysPageDataChanged"></asp:TextBox>
 </form>
 <uc3:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
</body>
</html>
