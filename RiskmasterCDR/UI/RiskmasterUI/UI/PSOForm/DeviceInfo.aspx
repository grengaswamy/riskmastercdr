﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeviceInfo.aspx.cs" Inherits="Riskmaster.UI.UI.PSOForm.DeviceInfo" ValidateRequest="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register tagprefix="uc2" Tagname="CodeLookUp"  src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register tagprefix="uc"  tagname="MultiCode" src="~/UI/Shared/Controls/MultiCodeLookup.ascx"   %>
<%@ Register tagprefix="uc3" tagname="PleaseWaitDialog" src="~/UI/Shared/Controls/PleaseWaitDialog.ascx"   %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DEVICE OR MEDICAL/SURGICAL SUPPLY</title>    
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
</head>
<%--vkumar258 - RMA_6037- Starts--%>
<%-- <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; } </script>   
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">    { var i; }</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">    { var i; }</script> --%>
<link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
<link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
<script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
<script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
<script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
<%--vkumar258 - RMA-6037 - End --%>

<script type="text/javascript" language="JavaScript" src="../../Scripts/pso.js"></script>
<script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
<script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">    { var i; }</script>
<script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>
<body onload="CheckHiddenContrls();">
<form id="frmData" runat="server" name="frmData">
<table>
   <tr>
      <td colspan="2">
         <uc1:ErrorControl ID="ErrorControl1" runat="server" />
      </td>
  </tr>
 </table>
<div class="msgheader" id="pnlDeviceInfo_Header" runat="server">Device Or Medical/Surgical Supply</div>
<div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" EnableTheming="True" ShowMessageBox="false" />
</div>  
<div id="pnlDeviceInfo" runat="server">
  <table border="0" cellspacing="0" cellpadding="0" id="pnlDeviceInfo_table" runat="server" width="100%">
    <tr>
        <td>
            <asp:Label ID="psocdDeviceType_Title" runat="server" Text="What type of device was involved in the event?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdDeviceType" CodeTable="PSO_DEVICE_TYPE" ControlName="psocdDeviceType" RMXRef="/Instance/Document/DeviceInfo/DeviceType" RMXType="code"  />             
        </td>
    </tr>
    <tr id="psocdWasDevicePlaced_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdWasDevicePlaced_Title" runat="server" Text="At the time of the event, was the device placed within the patient’s tissue?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdWasDevicePlaced" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdWasDevicePlaced" RMXRef="/Instance/Document/DeviceInfo/ImpDevPlacement" RMXType="code"  />             
        </td>
    </tr>
    <tr id="psocdWasDeviceRemove_ctlRow" runat="server">
        <td>
            <asp:Label ID="psocdWasDeviceRemove_Title" runat="server" Text="Did the event result in the device being removed?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdWasDeviceRemove" CodeTable="PSO_YES_NO_JCAHO" ControlName="psocdWasDeviceRemove" RMXRef="/Instance/Document/DeviceInfo/ImpDevRemoval" RMXType="code"  />             
        </td>
    </tr>
    <tr id="txtDeviceName_ctlRow" runat="server">
        <td>
           <asp:Label ID="txtDeviceName_Title" runat="server" Text="What is the name (brand or generic) of the device, product, or medical/surgical supply?" class=""></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtDeviceName" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/DeviceInfo/DeviceBrandName" MaxLength="100"></asp:TextBox>
        </td>              
    </tr>
    <tr id="txtManufacturerName_ctlRow" runat="server">
        <td>
           <asp:Label ID="txtManufacturerName_Title" runat="server" Text="What is the name of the manufacturer?" class=""></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtManufacturerName" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/DeviceInfo/DevManfName" MaxLength="100"></asp:TextBox>
        </td>              
    </tr> 
    <tr>
        <td>
            <asp:Label ID="psolstIdentifiers_Title" runat="server" Text="Which of the following identifiers are known?" class=""></asp:Label>
        </td>
        <td> 
            <uc:MultiCode runat="server" ID="psolstIdentifiers" width="200px" Height="88px" CodeTable="PSO_DEVICE_INDENTI" ControlName="psolstIdentifiers" RMXRef="/Instance/Document/DeviceInfo/DevIdentifiers" RMXType="codelist" />
        </td>
    </tr>
    <tr id="txtModelNumber_ctlRow" runat="server">
        <td>
           <asp:Label ID="txtModelNumber_Title" runat="server" Text="What is the model number?" class=""></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtModelNumber" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/DeviceInfo/DevModelNumber" MaxLength="100"></asp:TextBox>
        </td>              
    </tr>    
    <tr id="txtLotNumber_ctlRow" runat="server">
        <td>
           <asp:Label ID="txtLotNumber_Title" runat="server" Text="What is the lot or batch number?" class=""></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtLotNumber" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/DeviceInfo/DevLotNumber" MaxLength="100"></asp:TextBox>
        </td>              
    </tr>
    <tr id="txtUniqueIdentifier_ctlRow" runat="server">
        <td>
           <asp:Label ID="txtUniqueIdentifier_Title" runat="server" Text="What is the type of other unique product identifier?" class=""></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtUniqueIdentifier" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/DeviceInfo/DevOtherNumber" MaxLength="100"></asp:TextBox>
        </td>              
    </tr>
    <tr id="dtExpirationDate_ctlRow" runat="server">
        <td>
           <asp:Label ID="dtExpirationDate_Title" runat="server" Text="What is the expiration date?" class=""></asp:Label>
        </td>
        <td> 
        <asp:TextBox runat="server" FormatAs="date" ID="dtExpirationDate" RMXRef="/Instance/Document/DeviceInfo/DevExpDate"
                        RMXType="date"  onchange="CopyDateToValidate(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                    <%-- <asp:Button class="DateLookupControl" runat="server" ID="btnExpirationDate"  />--%>
                    <%-- <asp:TextBox ID="dtExpirationDate_ValidateDate" Style="display: none" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/DeviceInfo/DevExpDate"></asp:TextBox>--%>
            <%--<input type="text" value="" size="12" onblur="dateLostFocus(this.id);" onchange="setDataChanged(true);" id="dtExpirationDate1" runat="server"/>
            <input type="button" runat="server" class="DateLookupControl" id="btnExpirationDate" value=""/>--%>
                        <%--vkumar258 - RMA_6037- Starts--%>
                        <%-- <script type="text/javascript">
                Zapatec.Calendar.setup({ inputField: "dtExpirationDate", ifFormat: "%m/%d/%Y", button: "btnExpirationDate" });      
            </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#dtExpirationDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../Images/calendar.gif",
                                   // buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                            });
                        </script>
                        <%--vkumar258 - RMA_6037- End--%>
        </td>              
    </tr>    
  </table>
</div>
<div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnSave">
            <asp:button class="button" runat="server" id="btnDeviceOk" RMXRef="" Text="OK" width="75px" onClientClick="return fnDeviceOk();" onclick="btnDeviceOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:button class="button" runat="server" id="btnDeviceCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return fnDeviceCancel();" />
        </div>
 </div>
    <br />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/Devices/control[@name ='RowId']" />
    <asp:TextBox Style="display: none" runat="server" ID="txtDeviceInfoSessionId" RMXRef="/Instance/Document/DeviceInfo/txtDeviceInfoSessionId"></asp:TextBox>
    <asp:TextBox style="display: none" runat="server" id="DeviceRowId" RMXRef="/Instance/Document/DeviceInfo/DeviceRowId" RMXType="hidden"/>
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="txtmode" />
    <asp:TextBox style="display: none" runat="server" ID="formname" value="deviceinfo" />
    <asp:TextBox Style="display: none" runat="server" ID="SessionId" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />    
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />  
    <asp:TextBox Style="display: none" runat="server" ID="txtFunctionToCall" />  
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="hiddencontrols"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="requiredcontrols"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="pnlhiddencontrols"></asp:TextBox>
</form>
<uc3:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
</body>
</html>
