﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Data;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.Shared.Controls;
using System.Xml.XPath;
//neha goel added the code for PSO -- 08262011

namespace Riskmaster.UI.PSOForm
{
    public partial class PSOWebform : NonFDMBasePageCWS
    {

        XmlDocument XMLDocument = new XmlDocument();      
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        bool bReturnStatus = false;
        string m_FUNCTION_SAVE_PSODATA = "PSOFormAdaptor.SavePSOData";
        

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                XMLDocument.Load(Server.MapPath("~/App_Data/PSOForm/MainForm.xml"));
                AppHelper.CreateControl(this.Page, XMLDocument);

                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes
                if (!IsPostBack)
                {
                    int iEventId = 0;
                    if (AppHelper.GetQueryStringValue("eventid") != "")
                    {
                        int.TryParse(AppHelper.GetQueryStringValue("eventid"), out iEventId);
                        txteventid.Text = Convert.ToString(iEventId);
                        txtevtpsoevtid.Text = Convert.ToString(iEventId);
                        txtpatpsoevtid.Text = Convert.ToString(iEventId);
                    }
                    int iPatPSORowID = 0;
                    if (AppHelper.GetQueryStringValue("patpsorowid") != "")
                    {
                        int.TryParse(AppHelper.GetQueryStringValue("patpsorowid"), out iPatPSORowID);
                        PatPsoRowId.Text = Convert.ToString(iPatPSORowID);
                    }
                    int iEvtPSORowID = 0;
                    if (AppHelper.GetQueryStringValue("evtpsorowid") != "")
                    {
                        int.TryParse(AppHelper.GetQueryStringValue("evtpsorowid"), out iEvtPSORowID);
                        EvtPsoRowId.Text = Convert.ToString(iEvtPSORowID);
                    }
                }
                string sFunctionToCall = txtFunctionToCall.Text;
                if (string.Compare(sFunctionToCall, m_FUNCTION_SAVE_PSODATA, true) != 0)
                {
                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWS("PSOFormAdaptor.Get", XmlTemplate, out sCWSresponse, true, true);
                    if (bReturnStatus)
                    {
                        ModifyControl_Hide(sCWSresponse);
                    }
                    else
                    {
                        btnPSOSave.Enabled = false;
                    }
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
            }
        }

        private void ModifyControl_Hide(string sCWSresponse)
        {
            XmlDocument xmlResult = new XmlDocument();
            bool bFailure = false;
            bool bCustomFailure = false;
            string sValue = string.Empty;
            string sHiddenFieldList = string.Empty;
            string sRequiredFieldList = string.Empty;
            string sPnlHiddenCtrlsList = string.Empty;
            string sPatientCount = string.Empty;            

            xmlResult.LoadXml(sCWSresponse);
            if (xmlResult.SelectSingleNode("//ResultMessage/MsgStatus/MsgStatusCd") != null)
            {
                sValue = xmlResult.SelectSingleNode("//ResultMessage/MsgStatus/MsgStatusCd").InnerText;
                if (string.Compare(sValue, "Error", true) == 0)
                {
                    bFailure = true;
                }
                else if (string.Compare(sValue, "Success", true) == 0)
                {
                    if (xmlResult.SelectSingleNode("//ResultMessage/MsgStatus/ExtendedStatus/ExtendedMsgType") != null)
                    {
                        string sExtendedErrorType = xmlResult.SelectSingleNode("//ResultMessage/MsgStatus/ExtendedStatus/ExtendedMsgType").InnerText;
                        if (string.Compare(sExtendedErrorType, "Error", true) == 0)
                        {
                            bCustomFailure = true;
                        }
                    }
                }
            }

            if (bFailure || bCustomFailure)
            {
                btnPSOSave.Enabled = false;
            }
            else
            {
                btnPSOSave.Enabled = true;
            }

            if (bCustomFailure)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "DeletePatRowId", "<script>fnDelPatRowId();</script>");
            }
            

            if (xmlResult.SelectSingleNode("//Event/HiddenFieldList") != null)
            {
                sHiddenFieldList = xmlResult.SelectSingleNode("//Event/HiddenFieldList").InnerText;
            }
            if (xmlResult.SelectSingleNode("//Event/RequiredFieldList") != null)
            {
                sRequiredFieldList = xmlResult.SelectSingleNode("//Event/RequiredFieldList").InnerText;
            }
            if (xmlResult.SelectSingleNode("//Event/pnlhiddencontrols") != null)
            {
                sPnlHiddenCtrlsList = xmlResult.SelectSingleNode("//Event/pnlhiddencontrols").InnerText;
            }
            
            hiddencontrols.Text = sHiddenFieldList;
            hiddencontrolstemp.Text = sHiddenFieldList;
            requiredcontrols.Text = sRequiredFieldList;
            pnlhiddencontrols.Text = sPnlHiddenCtrlsList;
            pnlhiddencontrolstemp.Text = sPnlHiddenCtrlsList;
            if (xmlResult.SelectSingleNode("//Event/PatientCount") != null)
            {
                sPatientCount = xmlResult.SelectSingleNode("//Event/PatientCount").InnerText;
            }
            PatientCount.Text = sPatientCount;
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("PSOFormAdaptor.Get");
            sXml = sXml.Append("</Function></Call><Document>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }

        protected void btnPSOOk_Click(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            XmlDocument xmlTempDoc = new XmlDocument();
            string sreturnValue = string.Empty;
            bReturnStatus = CallCWS("PSOFormAdaptor.SavePSOData", XmlTemplate, out sreturnValue, true, false);
            if (bReturnStatus)
            {
                savebtnposted.Text = "true";
                xmlTempDoc.LoadXml(sreturnValue);
                string sEvtPSORowId = xmlTempDoc.SelectSingleNode("//Event/EventPSO/EvtPsoRowId").InnerText;
                EvtPsoRowId.Text = sEvtPSORowId;
                string sPatPSORowId = xmlTempDoc.SelectSingleNode("//Event/PatientPSO/PatPsoRowId").InnerText;
                PatPsoRowId.Text = sPatPSORowId;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PSOOkClick", "<script>fnPSODataOk();</script>");
            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            string sFunctionToCall = txtFunctionToCall.Text;
            XmlDocument xmlTempDocument = new XmlDocument();
            XMLDocument.Load(Server.MapPath("~/App_Data/PSOForm/MainForm.xml"));
            XElement xmlEvent = Xelement.XPathSelectElement("./Document/Event");
            XElement xmlForm = new XElement("FormValues");
            xmlForm.Value = XMLDocument.SelectSingleNode("//form").OuterXml;
            if (xmlEvent != null)
            {
                xmlEvent.Add(xmlForm);
            }
            //New Code for PDF
            if (string.Compare(sFunctionToCall, m_FUNCTION_SAVE_PSODATA, true) == 0)
            {
                xmlTempDocument.Load(Server.MapPath("~/App_Data/PSOForm/DeviceInfo.xml"));
                xmlForm = new XElement("DeviceFormValues");
                xmlForm.Value = xmlTempDocument.SelectSingleNode("//form").OuterXml;
                if (xmlEvent != null)
                {
                    xmlEvent.Add(xmlForm);
                }
                xmlTempDocument.Load(Server.MapPath("~/App_Data/PSOForm/MedInfo.xml"));
                xmlForm = new XElement("MedicationFormValues");
                xmlForm.Value = xmlTempDocument.SelectSingleNode("//form").OuterXml;
                if (xmlEvent != null)
                {
                    xmlEvent.Add(xmlForm);
                }
                xmlTempDocument.Load(Server.MapPath("~/App_Data/PSOForm/LinkedEventInfo.xml"));
                xmlForm = new XElement("LnkEvtFormValues");
                xmlForm.Value = xmlTempDocument.SelectSingleNode("//form").OuterXml;
                if (xmlEvent != null)
                {
                    xmlEvent.Add(xmlForm);
                }
            }
            //New Code for PDF Ends
        }

    }
}