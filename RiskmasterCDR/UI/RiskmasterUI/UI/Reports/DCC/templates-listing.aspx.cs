﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class templates_listing : NonFDMBasePageCWS
    {
        private string sCWSresponse = "";
        private bool bReturnStatus;
        private XmlDocument oFDMPageDom = null;
        private IEnumerable result = null;
        private XElement rootElement = null;
        private DataTable dtGridData = null;
        private string allowEdit = string.Empty;//Added by Shivendu for MITS 17643
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    XElement oMessageElement = GetMessageTemplate();

                    bReturnStatus = CallCWS("DCCAdaptor.LoadAllTemplates", oMessageElement, out sCWSresponse, false, true);

                }

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            if (bReturnStatus)
            {
                BindGridData();
                AddRemoveButtons();//Added by Shivendu for MITS 17643
            }    
        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
              <Call>
               <Function></Function> 
              </Call>
              <Document>
               <DCC>
                 <EditPermission>true</EditPermission> 
                 <AddPermission>true</AddPermission> 
                 <DeletePermission>true</DeletePermission> 
                 <RestorePermission>true</RestorePermission> 
                 <ReportPermission>true</ReportPermission> 
 
               </DCC> 
             </Document>
           </Message>


            ");

            return oTemplate;
        }
        /// <summary>
        /// Function to Add and remove buttons on the basis of Module access security setting.
        /// </summary>
        private void AddRemoveButtons()
        {
            //Disable all the buttons
            if (Page.FindControl("btnAdd") != null)
            {
                ((Button)Page.FindControl("btnAdd")).Enabled = false;
            }
            if (Page.FindControl("btnDelete") != null)
            {
                ((Button)Page.FindControl("btnDelete")).Enabled = false;
            }
            if (Page.FindControl("btnRestore") != null)
            {
                ((Button)Page.FindControl("btnRestore")).Enabled = false;
            }
            

            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("//button");
            string sButtonName = string.Empty;
            foreach (XmlNode objNodes in xmlNodeList)
            {
                if (objNodes.Attributes["name"] != null)
                {
                    sButtonName = objNodes.Attributes["name"].Value.ToString();
                    switch (sButtonName)
                    {
                        case "Add":
                            if (Page.FindControl("btnAdd") != null)
                            {
                                ((Button)Page.FindControl("btnAdd")).Enabled = true;
                            }
                            break;
                        case "Delete":
                            if (Page.FindControl("btnDelete") != null)
                            {
                                ((Button)Page.FindControl("btnDelete")).Enabled = true;
                            }
                            break;
                        case "Restore":
                            if (Page.FindControl("btnRestore") != null)
                            {
                                ((Button)Page.FindControl("btnRestore")).Enabled = true;
                            }
                            break;
                        
                        default:
                            break;
                    }
                }
            }
        }
        private void BindGridData()
        {
            
            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            dtGridData = new DataTable();

            dtGridData.Columns.Add("templateName");
            dtGridData.Columns.Add("templateId");
            dtGridData.Columns.Add("allowEdit");

            DataRow objRow = null;

            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("//option");

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();
                string templateName = objNodes.InnerText.ToString();
                string templateId = objNodes.Attributes["value"].Value.ToString();
                allowEdit = objNodes.Attributes["editallow"].Value.ToString();

                objRow["templateName"] = templateName;
                objRow["templateId"] = templateId;
                objRow["allowEdit"] = allowEdit;
                dtGridData.Rows.Add(objRow);
            }

            
            templateDisplaygd.DataSource = dtGridData;
            templateDisplaygd.DataBind();
        }

        protected void templateDisplaygd_PreRender(object sender, EventArgs e)
        {
            for (int lcount = 0; lcount < dtGridData.Rows.Count; lcount++)
            {
                Label lbtn = ((Label)(templateDisplaygd.Rows[lcount].FindControl("TemplateName")));
                lbtn.Text = dtGridData.Rows[lcount]["templateName"].ToString();

                ImageButton imgbtn = ((ImageButton)(templateDisplaygd.Rows[lcount].FindControl("Edit")));
                 //if added for MITS 17643
                if (string.Compare(allowEdit, "-1") == 0)
                {
                    imgbtn.Enabled = true;
                    imgbtn.PostBackUrl = "../DCC/edit-dcc-template.aspx?templateid=" + dtGridData.Rows[lcount]["templateId"].ToString();
                }
                else
                {
                    imgbtn.Enabled = false;
                }
            }
        }

        protected void Add(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/add-dcc-template.aspx");
        }

        protected void Reports(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/reports-listing.aspx");
        }

        protected void Delete(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/delete-template-confirm.aspx?selectedvalues=" + selectedvalues.Text); 
        }

        protected void Restore(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/restore-dcc-template.aspx");
        }

    }
}
