﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class add_dcc_report : NonFDMBasePageCWS
    {
        private string sCWSresponse = "";
        private bool bReturnStatus;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                XElement oMessageElement = GetMessageTemplate();

                bReturnStatus = CallCWS("DCCAdaptor.LoadAllTemplates", oMessageElement, out sCWSresponse, false, true);

            }
        }
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
              <Call>
               <Function></Function> 
              </Call>
              <Document>
               <DCC>
                 <EditPermission>true</EditPermission> 
                 <AddPermission>true</AddPermission> 
                 <DeletePermission>true</DeletePermission> 
                 <RestorePermission>true</RestorePermission> 
                 <ReportPermission>true</ReportPermission> 
               </DCC> 
             </Document>
           </Message>


            ");

            return oTemplate;
        }

        protected void Save(object sender, EventArgs e)
        {
            this.templateId.Attributes.Add("rmxignoreset" , "true");
            NonFDMCWSPageLoad("DCCAdaptor.NewReport");

            Response.Redirect("../DCC/reports-listing.aspx");
        }

        protected void Refresh(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/reports-listing.aspx");        
        }

    }
}
