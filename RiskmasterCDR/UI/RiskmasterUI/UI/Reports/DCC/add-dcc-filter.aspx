﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-dcc-filter.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.add_dcc_filter" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>DCC Filter</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
    <script type ="text/javascript" language ="javascript" >
        function ShowPleaseWait()
        {
            pleaseWait.Show();
            return true;
        }
        function Validation()
        {
            if (document.getElementById("name").value == null || trimAll(document.getElementById("name").value) == "") 
            {
                alert("Filter Name Field cannot be left blank");
                document.forms[0].name.focus();
                return false;
            }
            return true;
        }
        function trimAll(sString) {
            while (sString.substring(0, 1) == ' ') {
                sString = sString.substring(1, sString.length);
            }
            while (sString.substring(sString.length - 1, sString.length) == ' ') {
                sString = sString.substring(0, sString.length - 1);
            }
            return sString;
        }
    </script>
</head>
<body>
    <form id="frmData" runat="server" method="post">
    
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" /> 
    
    <p align="center">
    <table cellspacing="2" cellpadding="2" class="singleborder">
     <tr>
      <td colspan="2" class="ctrlgroup">Add New Filter</td>
     </tr>
     <asp:TextBox style="display:none" runat="server" ID="templateid" rmxref="Instance/Document/DCC/TemplateId" />
     <tr>
      <td class="datatd"><b>Name:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="name" type="text" rmxref="Instance/Document/DCC/NewFilterName" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Type:</b></td>
      <td class="datatd"><asp:DropDownList runat="server" ID="type" rmxref="Instance/Document/DCC/FilterType">
        <asp:ListItem value="0">None</asp:ListItem>
        <asp:ListItem value="1">CheckBox</asp:ListItem>
        <asp:ListItem value="2">List</asp:ListItem>
        <asp:ListItem value="3">TopX</asp:ListItem>
        <asp:ListItem value="4">ComboBox</asp:ListItem>
        <asp:ListItem value="5">Spin</asp:ListItem>
        <asp:ListItem value="6">Date</asp:ListItem></asp:DropDownList></td>
     </tr>
     <tr>
      <td class="datatd"><b>SQL:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="sql" type="text" rmxref="Instance/Document/DCC/SQL" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>SQL From:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="sqlFrom" type="text" rmxref="Instance/Document/DCC/From" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>SQL Where:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="sqlWhere" type="text" rmxref="Instance/Document/DCC/Where" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>SQL Parameter:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="sqlParam" type="text" rmxref="Instance/Document/DCC/Parameter" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Order By:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="orderBy" type="text" rmxref="Instance/Document/DCC/OrderBy" size="50" /></td>
     </tr>
     <tr>
      <td colspan="2" align="center">
      <asp:Button runat="server" ID="btnSave" type="submit" Text="Save" class="button" OnClick="Save" OnClientClick ="return Validation();return ShowPleaseWait();" />  
      <asp:Button runat="server" ID="btnCancel" type="submit" Text="Cancel" class="button" OnClick="Cancel" />
      </td>
     </tr>
    </table>
   </p>
    </form>
</body>
</html>
