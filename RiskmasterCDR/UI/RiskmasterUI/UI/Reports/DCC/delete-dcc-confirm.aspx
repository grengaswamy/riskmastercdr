﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="delete-dcc-confirm.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.delete_dcc_confirm" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Confirm Report Deletion</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    
    <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
    
    <script type="text/javascript">    
        function ShowPleaseWait()
        {
            pleaseWait.Show();
            return true;
        }
    </script>  
    
</head>
<body>
    <form id="frmData" runat="server" method="post">
    
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    
    <p align="center">
    <table class="singleborder" align="center">
     <tr>
      <td class="ctrlgroup">Confirm DCC Report Delete</td>
     </tr>
     <tr>
      <td nowrap="" class="datatd"><b>Press 'Delete' to proceed with deletion of selected reports?<br/><br/>Reports can be restored by pressing the 'Restore' button on the previous form.</b><br/>&nbsp;
      </td>
     </tr>
     <tr>
      <td align="center" nowrap="">
      <asp:Button runat="server" ID="btnDelete" type="submit" Text="Delete" class="button" OnClick="deleteReport" OnClientClick ="return ShowPleaseWait();"/> 
      <asp:Button runat="server" ID="btnCancel" type="submit" Text="Cancel" class="button"  OnClick="Cancel" />
      </td>
     </tr>
    </table>
    <asp:TextBox style="display:none" runat="server" ID="Itemlist"  rmxref="Instance/Document/DCC/ItemList"/>
    <asp:TextBox style="display:none" runat="server" ID="Itemtype" Text="report" rmxref="Instance/Document/DCC/ItemType" />
    <asp:TextBox style="display:none" runat="server" ID="Templateid" Text="0" rmxref="Instance/Document/DCC/TemplateId" />
    </p>
    </form>
</body>
</html>
