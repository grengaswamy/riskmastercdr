﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="templates-listing.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.templates_listing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Templates</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script type="text/javascript" language="javascript" src="../../../Scripts/dcc.js" ></script>
</head>
<body>
    <form id="frmData" runat="server" method="post">
    
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    
    <h3>DCC Net</h3>
    <table width="100%" cellspacing="0" cellpadding="2" border="0">
      <tr class="ctrlgroup">
        <td colspan="3">Template Name</td>
      </tr>
    </table>
    <asp:GridView AlternatingRowStyle-CssClass="rowdark" runat="server" ID="templateDisplaygd" AutoGenerateColumns="false" CssClass="singleborder" OnPreRender="templateDisplaygd_PreRender">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <input type="checkbox" runat="server" ID="checking" value='<%#Eval("templateId")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField FooterStyle-Width="100%" ItemStyle-Width="100%"  ControlStyle-Width="100%">
                <ItemTemplate>
                    <asp:Label runat="server" ID="TemplateName" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton runat="server" ID="Edit" ImageUrl="~/Images/edittoolbar2.gif" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>    
    </asp:GridView>
    <br />
   <asp:Button ID="btnAdd" runat="server" type="submit" Text="Add" class="button" OnClientClick="return CheckSelection('Add','template');" OnClick="Add" /> 
   &#160;
   <asp:Button ID="btnDelete" runat="server" type="submit" Text="Delete" class="button" OnClientClick="return CheckSelection('Delete','template');" OnClick="Delete" /> 
   &#160;
   <asp:Button ID="btnRestore" runat="server" type="submit" Text="Restore" class="button" OnClick="Restore" /> 
   &#160;
   <asp:Button ID="btnReports" runat="server" type="submit" Text="Reports" class="button" OnClientClick="return CheckSelection('Reports','template');" OnClick="Reports" />
   <asp:TextBox runat="server" ID="selectedvalues" style="display:none" />
    </form>
</body>
</html>
