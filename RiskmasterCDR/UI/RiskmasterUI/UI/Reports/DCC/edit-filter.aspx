<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="edit-filter.aspx.cs" Inherits="Riskmaster.UI.DCC.edit_filter" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
 <head runat ="server">  
	  <title>Add New Report</title>
	  <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
	  
	  <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
	  <script type ="text/jscript" language="javascript" src="../../../Scripts/dcc.js"></script>
	  <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
      <script type="text/javascript">    
        function ShowPleaseWait() {
            
            pleaseWait.Show();
            return true;
        }
      </script>   
 </head>

 <body>
  <form runat ="server" id="frmData" method="post">
  
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" /> 
  
  <div align="center">
    <table cellspacing="2" cellpadding="2" class="singleborder">
     <tr>
      <td colspan="2" class="ctrlgroup">Edit Filter</td>
     </tr>
     <tr>
      <td class="datatd"><b>Name:</b></td>
      <td align ="left">
        <asp:Label ID="lblName" runat ="server" rmxref="/Instance/Document/DCC/FilterName" rmxignoreget="true"></asp:Label>
      </td>
     </tr>
     <tr>
      <td class="datatd"><b><asp:Label ID="lblType" runat="server"></asp:Label></b></td>
      <td align ="left">
        <asp:CheckBox id="chkAll" runat="server" type="checkbox" appearance="full" onclick="return CheckAll();" />
        <asp:CheckBox id="chkType1" runat="server" type="checkbox" appearance="full" />
        <asp:TextBox ID="txtchkAll" runat ="server" rmxref="/Instance/Document/DCC/FilterAll" rmxignoreget="true" style="display:none"></asp:TextBox>
        <asp:TextBox ID="txtTopX"  runat ="server" rmxignoreset="true" rmxref="/Instance/Document/Selectedfilter/filter/filterdata/@data"></asp:TextBox>
        <asp:TextBox ID="txthndTopX" runat ="server" rmxignoreget="true" rmxref="/Instance/Document/DCC/FilterData" style="display:none"></asp:TextBox>
      </td>
     </tr>
     <tr id="TRFilterType2" runat ="server">
      <td><b><asp:Label ID="lblFilterType2" runat ="server"></asp:Label></b></td>
      <td align ="left">
	      <asp:ListBox runat ="server" DataTextField ="data" DataValueField ="value" size="15" 
              id="selList" Height="224px" SelectionMode="Multiple" Width="155px"></asp:ListBox>    
          <asp:TextBox runat ="server" ID="txtSelList" style="display :none" rmxignoreget="true" rmxref="/Instance/Document/DCC/FilterData" ></asp:TextBox>
	  </td>
     </tr>
     <tr>
      <td colspan="2" align="center">
		  <asp:Button runat ="server" ID="btnSave" type="submit" Text="Save" 
              class="button" onclick="Save" OnClientClick ="return ShowPleaseWait();"/>
		  <asp:Button runat ="server" ID="btnClose" type="submit" Text="Close" 
              class="button" onclick="Close" />
     </td>
     </tr>
    </table>
   </div>	   
   </form>
 </body>
</html>
