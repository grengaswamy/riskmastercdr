﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class edit_dcc_report : NonFDMBasePageCWS
    {
        DataTable tbl = new DataTable("ReportTable");
        private string sCWSresponse = string.Empty;
        XElement xTemplate = null;
        private string templateid;
        private string reportid;

        protected void Page_Load(object sender, EventArgs e)
        {
            templateid = AppHelper.GetQueryStringValue("templateid");
            reportid = AppHelper.GetQueryStringValue("reportid");

            try
            {
                if (!IsPostBack)
                {
                    XElement oMessageElement = null;
                    if (oMessageElement == null)
                        oMessageElement = GetNonFDMCWSMessageTemplateForReport();

                    GetNonFDMCallCWS("DCCAdaptor.GetSelectedReport", oMessageElement, false);
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }                       
        }

        protected void EnableDisableLinkButton()
        {
            for (int lcount = 0; lcount < repterFilter.Items.Count; lcount++)
            {
                LinkButton lnkbtn = (LinkButton)repterFilter.Items[lcount].FindControl("lnkFilter");
                CheckBox chkbtn = (CheckBox)repterFilter.Items[lcount].FindControl("chkFilter");
                if (chkbtn.Checked)
                    lnkbtn.Enabled = true;
                else
                    lnkbtn.Enabled = false;
            }

        }

        protected bool GetNonFDMCallCWS(string CWSFunctionName,XElement oMsgElement,bool bGetControlData)
        {
                bool bReturnStatusCWSCall = false;
               

                oMsgElement.XPathSelectElement("./Document/DCC/ReportId").Value = reportid;
                oMsgElement.XPathSelectElement("./Document/DCC/TemplateId").Value = templateid;


                bReturnStatusCWSCall = CallCWS(CWSFunctionName, oMsgElement, out sCWSresponse, bGetControlData, true);
                if (bReturnStatusCWSCall)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sCWSresponse);

                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {
                        XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                        XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);

                        xTemplate = oMessageRespElement.XPathSelectElement("/report");

                        if (xTemplate.HasAttributes)
                        {
                            txtReportTitle.Text = xTemplate.Attribute("name").Value.ToString();
                            lblTemplateName.Text = xTemplate.Attribute("templatename").Value.ToString();
                        }

                        if (!tbl.Columns.Contains("name"))
                            tbl.Columns.Add(new DataColumn("name", typeof(string)));
                        if (!tbl.Columns.Contains("selected"))
                            tbl.Columns.Add(new DataColumn("selected", typeof(int)));
                        if (!tbl.Columns.Contains("disabled"))
                            tbl.Columns.Add(new DataColumn("disabled", typeof(int)));

                        GetFilterElements(repterFilter, xTemplate, "/report/filters");
                        GetGraphElements(repterGraphGroup, xTemplate, "/report/graphgrouplist");
                        GetGraphElements(repterGraphData, xTemplate, "/report/graphdatalist");

                        ddlGraphType.SelectedIndex = Convert.ToInt32(xTemplate.XPathSelectElement("/report/graphtype").Attribute("value").Value);

                        EnableDisableLinkButton();
                        return true;
                    }                    
                }
                   
            tbl.Clear();
            return false;
        }

        protected void GetGraphElements(Repeater repter, XElement xTemplate, string node)
        {
                tbl.Clear();
                XElement xNode = xTemplate.XPathSelectElement(node);
                if (xNode.HasElements)
                {
                    string sChildName = ((XElement)(xNode.FirstNode)).Name.ToString();
                    string sValue = xNode.ToString();

                    XElement oLB = XElement.Parse(sValue);

                    if (oLB.HasElements)
                    {
                        foreach (XElement oEle in oLB.Elements())
                        {
                            string sname = oEle.Attribute("name").Value;
                            string sSelected = oEle.Attribute("selected").Value;

                            tbl.Rows.Add(sname, sSelected);
                        }
                        repter.DataSource = tbl;
                        repter.DataBind();
                    }
                }
                

        }

        protected void GetFilterElements(Repeater repter, XElement Template, string node)
        {
            
                tbl.Clear();
                XElement w = Template.XPathSelectElement(node);
                if (w.HasElements)
                {
                    string sChildName = ((XElement)(w.FirstNode)).Name.ToString();
                    string sValue = w.ToString();

                    XElement oLB = XElement.Parse(sValue);

                    if (oLB.HasElements)
                    {
                        foreach (XElement oEle in oLB.Elements())
                        {
                            string sname = oEle.Attribute("name").Value;
                            string sSelected = oEle.Attribute("selected").Value;
                            string sDisabled = oEle.Attribute("disabled").Value;

                            tbl.Rows.Add(sname, sSelected, sDisabled);
                        }
                        repter.DataSource = tbl;
                        repter.DataBind();

                    }
                }
              

        }

        private XElement GetNonFDMCWSMessageTemplateForReport()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <DCC>
                        <ReportId></ReportId> 
                        <TemplateId></TemplateId> 
                    </DCC>
                </Document>
            </Message>
            ");
            return oTemplate;
        }

        protected void Save(object sender, EventArgs e)
        {
            bool bRetValue = false;
            try
            {
                if (chkShowAsGraph.Checked == true)
                {
                    txtShowAsGraph.Text = "on";
                }

                GetAllLinkTextValue();

                GetSelectedRadioButton();

                XElement oMessageElement = null;
                if (oMessageElement == null)
                    oMessageElement = GetNonFDMCWSMessageTemplateForReport();

               bRetValue= GetNonFDMCallCWS("DCCAdaptor.EditReport", oMessageElement, true);
               if (bRetValue)
               {
                   bRetValue = GetNonFDMCallCWS("DCCAdaptor.GetSelectedReport", oMessageElement, false);
               }
               
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }                   
        }

        protected void GetSelectedRadioButton()
        {
            for (int lcount = 0; lcount < repterGraphData.Items.Count; lcount++)
            { 
                RadioButton rdoGraphData=(RadioButton) repterGraphData.Items[lcount].FindControl("radioGraphData");
                if (rdoGraphData.Checked)
                {
                    txtradioGraphData.Text = rdoGraphData.Text;
                }
            }

            for (int lcount = 0; lcount < repterGraphGroup.Items.Count; lcount++)
            {
                RadioButton rdoGraphGroup = (RadioButton)repterGraphGroup.Items[lcount].FindControl("radioGraphGroup");
                if (rdoGraphGroup.Checked)
                {
                    txtradioGraphGroup.Text = rdoGraphGroup.Text;
                }
            }
            
        }

        protected void GetAllLinkTextValue()
        {
            hndtxtFilterList.Text = "";
            for (int lcount = 0; lcount < repterFilter.Items.Count; lcount++)
            {
                LinkButton lnkbtn = (LinkButton)repterFilter.Items[lcount].FindControl("lnkFilter");
                CheckBox chkbtn = (CheckBox)repterFilter.Items[lcount].FindControl("chkFilter");

                if (chkbtn.Checked)
                {
                    hndtxtFilterList.Text += lnkbtn.Text + ",";
                }
            }

            if (hndtxtFilterList.Text.Length > 0)
            {
                hndtxtFilterList.Text = hndtxtFilterList.Text.ToString().Substring(0, hndtxtFilterList.Text.Length - 1);
            }
        }

        protected void Close(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/reports-listing.aspx");   
        }

        protected void repterFilter_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                DataRowView drvGroup = (DataRowView)e.Item.DataItem;
                HiddenField hndFilter = (HiddenField)e.Item.FindControl("hndFilter");
                LinkButton lnkbtn = (LinkButton)e.Item.FindControl("lnkFilter");

                lnkbtn.PostBackUrl = "~/UI/Reports/DCC/edit-filter.aspx?reportid=" + reportid + "&templateid=" + templateid + "&filtername=" + drvGroup["name"].ToString();

                hndFilter.Value = "edit-filter.aspx?reportid=" + reportid + "&templateid=" + templateid + "&filtername=" + drvGroup["name"].ToString();

            }
        }      
    }
}
