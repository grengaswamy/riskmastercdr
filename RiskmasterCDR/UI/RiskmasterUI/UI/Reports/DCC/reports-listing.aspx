﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="reports-listing.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.reports_listing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>DCC</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script language="javascript" src="../../../Scripts/dcc.js" type="text/javascript"></script>       

</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server" method="post">
    
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />    
  
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">                
            </td>
        </tr>
        <tr class="msgheader">
            <td colspan="2">
                DCC
            </td>
        </tr>
        <tr class="ctrlgroup">
            <td colspan="3">
                Report Name
            </td>
        </tr>
    </table>
    <asp:GridView ID="reportDisplaygd" runat="server" AutoGenerateColumns="false" CssClass="singleborder"
         AlternatingRowStyle-CssClass="rowdark">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <input type="checkbox" runat="server" ID="checking" value='<%#Eval("reportId")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField FooterStyle-Width="100%" ItemStyle-Width="100%"  ControlStyle-Width="100%">
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="ReportName"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton runat="server" ID="Edit" ImageUrl="~/Images/edittoolbar2.gif" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <asp:Button class="button" ID="btnAdd" Text=" Add " runat="server" OnClientClick="return CheckSelection('Add','report');" OnClick="Add"
        Width="100px" />
    &#160;
    <asp:Button class="button" ID="btnDelete" Text=" Delete " runat="server" OnClientClick="return CheckSelection('Delete','report');" OnClick="Delete"
        Width="100px" />
    &#160;
    <asp:Button class="button" ID="btnRestore" Text=" Restore " runat="server" OnClick="Restore"
        Width="100px" />
    <%--Added by Nitin for the R4-R5 Merging of Mits 13184  on 28-Feb-2009--%>
    <% btnDCCDisplay.Text = valueDCCDisplay.Text; %>
    <asp:Button class="button" ID="btnDCCDisplay" runat="server"
        OnClientClick="return CheckSelection('DCCDisplay','report');" OnClick="DCCDisplay" Width="100px" />
    <asp:TextBox runat="server" ID="valueDCCDisplay" Style="display: none" rmxref="Instance/Document/reports/button[@name='DCCDisplay']/@value" />
    <%--Ended by Nitin for the R4-R5 Merging of Mits 13184  on 28-Feb-2009--%>
    <asp:Button class="button" ID="btnTemplates" Text=" Templates " runat="server" OnClientClick="return CheckSelection('Templates','report');" OnClick="Templates"
        Width="100px" />
    <asp:TextBox runat="server" ID="selectedvalues" style="display:none" />
    </form>
    
</body>
</html>
