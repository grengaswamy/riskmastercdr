﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class reports_listing : NonFDMBasePageCWS
    {
        private string sCWSresponse = "";
        private bool bReturnStatus;
        private XmlDocument oFDMPageDom = null;
        private DataTable dtGridData = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    XElement oMessageElement = GetMessageTemplate();

                    bReturnStatus = CallCWS("DCCAdaptor.LoadAllReports", oMessageElement, out sCWSresponse, false, true);

                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }

            }
            
            if (bReturnStatus)
            {
                BindGridData();
                AddRemoveButtons();//Added by Shivendu for MITS 17643
            }
            //bkumar33: MITS 16237
            if (valueDCCDisplay.Text == null || valueDCCDisplay.Text == "")
                valueDCCDisplay.Text = "DCC Display";
            //bkumar33: End Mits 16237


        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
              <Call>
               <Function></Function> 
              </Call>
              <Document>
               <DCC>
                 <EditPermission>true</EditPermission> 
                 <AddPermission>true</AddPermission> 
                 <DeletePermission>true</DeletePermission> 
                 <RestorePermission>true</RestorePermission> 
                 <DisplayPermission>true</DisplayPermission> 
                 <TemplatePermission>true</TemplatePermission> 
               </DCC> 
             </Document>
           </Message>


            ");

            return oTemplate;
        }
        /// <summary>
        /// Function to Add and remove buttons on the basis of Module access security setting.
        /// </summary>
        private void AddRemoveButtons()
        {
            //Disable all the buttons
            if (Page.FindControl("btnAdd") != null)
            {
                ((Button)Page.FindControl("btnAdd")).Enabled = false;
            }
            if (Page.FindControl("btnDelete") != null)
            {
                ((Button)Page.FindControl("btnDelete")).Enabled = false;
            }
            if (Page.FindControl("btnRestore") != null)
            {
                ((Button)Page.FindControl("btnRestore")).Enabled = false;
            }
            if (Page.FindControl("btnDCCDisplay") != null)
            {
                ((Button)Page.FindControl("btnDCCDisplay")).Enabled = false;
            }
            if (Page.FindControl("btnTemplates") != null)
            {
                ((Button)Page.FindControl("btnTemplates")).Enabled = false;
            }

            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("//button");
            string sButtonName = string.Empty;
            foreach (XmlNode objNodes in xmlNodeList)
            {
                if (objNodes.Attributes["name"] != null)
                {
                    sButtonName = objNodes.Attributes["name"].Value.ToString();
                    switch (sButtonName)
                    {
                        case "Add":
                            if (Page.FindControl("btnAdd") != null)
                            {
                                ((Button)Page.FindControl("btnAdd")).Enabled = true;
                            }
                            break;
                        case "Delete":
                            if (Page.FindControl("btnDelete") != null)
                            {
                                ((Button)Page.FindControl("btnDelete")).Enabled = true;
                            }
                            break;
                        case "Restore":
                            if (Page.FindControl("btnRestore") != null)
                            {
                                ((Button)Page.FindControl("btnRestore")).Enabled = true;
                            }
                            break;
                        case "DCCDisplay":
                            if (Page.FindControl("btnDCCDisplay") != null)
                            {
                                ((Button)Page.FindControl("btnDCCDisplay")).Enabled = true;
                            }
                            break;
                        case "Templates":
                            if (Page.FindControl("btnTemplates") != null)
                            {
                                ((Button)Page.FindControl("btnTemplates")).Enabled = true;
                            }

                            break;
                        default:
                            break;
                    }
                }
            }
        }
        private void BindGridData()
        {
            string allowEdit = string.Empty;
            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            dtGridData = new DataTable();

            dtGridData.Columns.Add("reportName");
            dtGridData.Columns.Add("reportId");
            dtGridData.Columns.Add("templateId");
            dtGridData.Columns.Add("allowEdit");

            DataRow objRow = null;

            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("//report");

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();
                string reportName = objNodes.Attributes["name"].Value.ToString();
                string reportId = objNodes.Attributes["id"].Value.ToString();
                string templateId = objNodes.Attributes["templateid"].Value.ToString();
                allowEdit = objNodes.Attributes["editallow"].Value.ToString();

                objRow["reportName"] = reportName;
                objRow["reportId"] = reportId;
                objRow["templateId"] = templateId;
                objRow["allowEdit"] = allowEdit;
                dtGridData.Rows.Add(objRow);
            }
            
            reportDisplaygd.DataSource = dtGridData;
            reportDisplaygd.DataBind();
            BeforePreRender(allowEdit);//passing allowEdit to BeforePreRender function for MITS 17643
            
        }


        private void BeforePreRender(string allowEdit)//allowEdit is added as function parameter for MITS 17643
        {
            if (dtGridData != null)
            {

                if (dtGridData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        LinkButton lbtn = ((LinkButton)(reportDisplaygd.Rows[i].FindControl("ReportName")));
                        lbtn.Text = dtGridData.Rows[i]["reportName"].ToString();
                        lbtn.PostBackUrl = "~/UI/Reports/DCC/run-dcc-report.aspx?reportid=" + dtGridData.Rows[i]["reportId"].ToString();
                        
                        ImageButton imgbtn = ((ImageButton)(reportDisplaygd.Rows[i].FindControl("Edit")));
                        //if added for MITS 17643
                        if (string.Compare(allowEdit, "-1") == 0)
                        {
                            imgbtn.Enabled = true;
                            imgbtn.PostBackUrl = "~/UI/Reports/DCC/Edit-dcc-report.aspx?reportid=" + dtGridData.Rows[i]["reportId"].ToString() + "&templateid=" + dtGridData.Rows[i]["templateId"].ToString();
                        }
                        else
                        {
                            imgbtn.Enabled = false;
                        }
                    }
                }
            }
        }
        
        protected void Delete(object sender, EventArgs e)
        { 
            Response.Redirect("../DCC/delete-dcc-confirm.aspx?selectedvalues="+selectedvalues.Text); 
        }

        protected void Restore(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/restore-dcc-report.aspx");
        }

        protected void Add(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/add-dcc-report.aspx");
        }

        protected void Templates(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/templates-listing.aspx");
        }
        protected void DCCDisplay(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/view-dcc-report-graph.aspx?reportid="+ selectedvalues.Text);
        }

    }
}

