﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class add_dcc_filter : NonFDMBasePageCWS
    {
        private string templateidHidden;
        protected void Page_Load(object sender, EventArgs e)
        {
            // have to get the template id from the edit template page in the form of query string
            if (!IsPostBack)
            {
                this.templateid.Text = AppHelper.GetQueryStringValue("templateid");
                ViewState["templateidHidden"] = this.templateid.Text;
            }
        }

        protected void Save(object sender, EventArgs e)
        {
            NonFDMCWSPageLoad("DCCAdaptor.CreateFilter");

            templateidHidden = ViewState["templateidHidden"].ToString();

            Response.Redirect("../DCC/edit-dcc-template.aspx?templateid=" + templateidHidden );
        
        }

        protected void Cancel(object sender, EventArgs e)
        {
            templateidHidden = ViewState["templateidHidden"].ToString();

            Response.Redirect("../DCC/edit-dcc-template.aspx?templateid=" + templateidHidden);
        }
    }
}
