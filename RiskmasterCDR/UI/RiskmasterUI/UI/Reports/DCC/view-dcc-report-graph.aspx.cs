﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class view_dcc_report_graph : NonFDMBasePageCWS
    {
        XmlNode oEle;
        XmlDocument oNonFdm;
        private bool bReturnStatus;
        private string sCWSResponse = null;
        private XElement oMessageElement = null;
        private string str="";
        private byte[] final;
        protected XmlNodeList objlist;
        protected XmlNodeList reporthead;
        protected string temp;
        protected XmlDocument Xmltemp;
        private DataTable dtGridData = null;
        private string contentField;
        private string[] strAttr = new string[20];
        private string fieldvalue;
        private string reportid="";

        protected void Page_Load(object sender, EventArgs e)
        {
            // retrieve report id from the query string
            reportid = AppHelper.GetQueryStringValue("reportid");
       
            oMessageElement = GetMessageTemplate();

            oMessageElement.XPathSelectElement("./Document/DCC/ReportId").Value = reportid;

            bReturnStatus = CallCWS("DCCAdaptor.GetChart", oMessageElement, out sCWSResponse, false, false);

            oNonFdm = Data;

            objlist = oNonFdm.SelectNodes("//file");
            int i = 1;
            foreach (XmlNode obj in objlist)
            {
                str = obj.InnerText.ToString();
               
                TextBox dynamic = new TextBox(); 
                dynamic.ID = "h_rptname"+ Convert.ToString(i);
                dynamic.Text = obj.Attributes["name"].Value;
                dynamic.Attributes.Add("style", "display:none");
                this.plcholder.Controls.Add(dynamic);
                
                
                /**********************IN CASE OF IMAGES*************************************************/
                if (obj.Attributes["extension"].Value == ".gif")
                {
                    FileStream fs = File.Create(Server.MapPath("" + obj.Attributes["name"].Value + ".gif"));
                    BinaryWriter bw = new BinaryWriter(fs);
                    final = Convert.FromBase64String(str);
                    bw.Write(final); 
                    bw.Close();
                    fs.Close();
                    imgReport2.Src = obj.Attributes["name"].Value + ".gif";
                }
                else
                { /**********************IN CASE OF HTMLS************************************************/
                    if (obj.Attributes["extension"].Value == ".html")
                    {
                        temp = obj.InnerXml;

                        temp = "<childnodes>" + temp + "</childnodes>";
                        Xmltemp = new XmlDocument();
                        
                        Xmltemp.LoadXml(temp);
                        
                        // Binding the data to the grid
                        BindGridData(Xmltemp);
                    }
                }
                i = i + 1;
            }
            
                        
        }
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
             <Call>
                <Function></Function> 
             </Call>
             <Document>
                <DCC>
                    <ReportId></ReportId> 
                </DCC>
             </Document>
            </Message>
            ");

            return oTemplate;
        }

        private void BindGridData(XmlDocument Xmltemp)
        {
            string contentField = "";
            int lcount = 1;
            int jcount;
            dtGridData = new DataTable();
            
            // Getting the values of the Grid headings
            reporthead = Xmltemp.SelectNodes("//columnheader");

            foreach (XmlNode Item in reporthead)
            {
                contentField = Item.Attributes["name"].Value;
                dtGridData.Columns.Add(contentField);
                strAttr[lcount] = contentField;
                lcount = lcount + 1;
            }

            DataRow objRow = null;
            XmlNodeList xmlNodeList = Xmltemp.SelectNodes("//row");

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();
                jcount = 1;
                XmlNodeList childlist = objNodes.ChildNodes;

                foreach (XmlNode childnode in childlist)
                {
                    fieldvalue = childnode.Attributes["value"].Value.ToString();
                    contentField = strAttr[jcount];
                    objRow[contentField] = fieldvalue;
                    jcount = jcount + 1;
                }

                dtGridData.Rows.Add(objRow);
            }

            reportDisplaygd.DataSource = dtGridData;
            reportDisplaygd.DataBind();
        }
        
    }
}
