﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class edit_dcc_filter : NonFDMBasePageCWS
    {
        private string templateidhidden;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.templateid.Text = AppHelper.GetQueryStringValue("templateid");
            this.initialFilterName.Text = AppHelper.GetQueryStringValue("filtername");

            ViewState["templateidhidden"] = this.templateid.Text;

            this.OldFilterName.Text = this.initialFilterName.Text;

            if (!IsPostBack)
            {
                try
                {
                    this.templateid.Attributes.Add("rmxignoreget", "true");

                    this.initialFilterName.Attributes.Add("rmxignoreget", "true");

                    // setting the values of attributes rmxignore set and get to true as we donot need these controls to
                    // be binded at the time of page load...

                    this.filterName.Attributes.Add("rmxignoreget", "true");
                    this.filterName.Attributes.Add("rmxignoreset", "true");
                    this.sql.Attributes.Add("rmxignoreget", "true");
                    this.sql.Attributes.Add("rmxignoreset", "true");
                    this.sqlFrom.Attributes.Add("rmxignoreget", "true");
                    this.sqlFrom.Attributes.Add("rmxignoreset", "true");
                    this.sqlOrderBy.Attributes.Add("rmxignoreget", "true");
                    this.sqlOrderBy.Attributes.Add("rmxignoreset", "true");
                    this.sqlParameter.Attributes.Add("rmxignoreget", "true");
                    this.sqlParameter.Attributes.Add("rmxignoreset", "true");
                    this.sqlWhere.Attributes.Add("rmxignoreget", "true");
                    this.sqlWhere.Attributes.Add("rmxignoreset", "true");
                    this.OldFilterName.Attributes.Add("rmxignoreget", "true");
                    this.OldFilterName.Attributes.Add("rmxignoreset", "true");

                    // we also need to set the rmxignoreset true for hidden controls that will only transfer data...

                    this.filterHiddenName.Attributes.Add("rmxignoreset", "true");
                    this.filterHiddenOrderBy.Attributes.Add("rmxignoreset", "true");
                    this.filterHiddenParameter.Attributes.Add("rmxignoreset", "true");
                    this.filterHiddenSQL.Attributes.Add("rmxignoreset", "true");
                    this.filterHiddenSqlFrom.Attributes.Add("rmxignoreset", "true");
                    this.filterHiddenSqlWhere.Attributes.Add("rmxignoreset", "true");
                    this.filterHiddenType.Attributes.Add("rmxignoreset", "true");


                    NonFDMCWSPageLoad("DCCAdaptor.GetFilterNode");


                    // assigning the values of hidden controls to the controls present on the page

                    this.filterName.Text = this.filterHiddenName.Text;
                    this.sqlType.SelectedValue = this.filterHiddenType.Text;
                    this.sql.Text = this.filterHiddenSQL.Text;
                    this.sqlFrom.Text = this.filterHiddenSqlFrom.Text;
                    this.sqlWhere.Text = this.filterHiddenSqlWhere.Text;
                    this.sqlParameter.Text = this.filterHiddenParameter.Text;
                    this.sqlOrderBy.Text = this.filterHiddenOrderBy.Text;
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }    
            }
        }

        protected void Save(object sender, EventArgs e)
        {
            // node original name has to be given else the code would create a replica of existing node
            // in case of editing the node....

            try
            {
                this.filterName.Attributes.Remove("rmxignoreget");
                this.filterName.Attributes.Remove("rmxignoreset");
                this.sql.Attributes.Remove("rmxignoreget");
                this.sql.Attributes.Remove("rmxignoreset");
                this.sqlFrom.Attributes.Remove("rmxignoreget");
                this.sqlFrom.Attributes.Remove("rmxignoreset");
                this.sqlOrderBy.Attributes.Remove("rmxignoreget");
                this.sqlOrderBy.Attributes.Remove("rmxignoreset");
                this.sqlParameter.Attributes.Remove("rmxignoreget");
                this.sqlParameter.Attributes.Remove("rmxignoreset");
                this.sqlWhere.Attributes.Remove("rmxignoreget");
                this.sqlWhere.Attributes.Remove("rmxignoreset");

                this.OldFilterName.Attributes.Remove("rmxignoreget");
                this.OldFilterName.Attributes.Remove("rmxignoreset");

                this.initialFilterName.Attributes.Add("rmxignoreset", "true");


                NonFDMCWSPageLoad("DCCAdaptor.CreateFilter");
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }    
        }

        protected void Cancel(object sender, EventArgs e)
        {
            templateidhidden = ViewState["templateidhidden"].ToString();

            Response.Redirect("../DCC/edit-dcc-template.aspx?templateid=" + templateidhidden);
        }
    }
}
