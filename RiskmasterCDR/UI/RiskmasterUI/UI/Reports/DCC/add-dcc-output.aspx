﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-dcc-output.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.add_dcc_output" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>DCC Output</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
    <script type ="text/javascript" language ="javascript" >
        function ShowPleaseWait()
        {
            pleaseWait.Show();
            return true;
        }
        function Validation() {
            if (document.getElementById("name").value == null || trimAll(document.getElementById("name").value) == "") {
                alert("Output Name Field cannot be left blank");
                document.forms[0].name.focus();
                return false;
            }
            return true;
        }
        function trimAll(sString) {
            while (sString.substring(0, 1) == ' ') {
                sString = sString.substring(1, sString.length);
            }
            while (sString.substring(sString.length - 1, sString.length) == ' ') {
                sString = sString.substring(0, sString.length - 1);
            }
            return sString;
        }
    </script>
</head>
<body>
    <form id="frmData" runat="server" method="post">
    
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" /> 
    
    <p align="center">
    <table cellspacing="2" cellpadding="2" class="singleborder">
     <tr>
      <td colspan="2" class="ctrlgroup">Add New Output</td>
     </tr>
     <asp:TextBox style="display:none" runat="server" ID="templateid" rmxref="Instance/Document/DCC/TemplateId" />
     <tr>
      <td class="datatd"><b>Name:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="name" type="text" rmxref="Instance/Document/DCC/NewOutputName" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Label:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="label" type="text" rmxref="Instance/Document/DCC/OutputLabel" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Graph:</b></td>
      <td class="datatd"><asp:DropDownList runat="server" ID="graph" rmxref="Instance/Document/DCC/GraphType">
        <asp:ListItem value="X">X</asp:ListItem>
        <asp:ListItem value="Y">Y</asp:ListItem></asp:DropDownList></td>
     </tr>
     <tr>
      <td class="datatd"><b>Data Field:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="datafield" type="text" rmxref="Instance/Document/DCC/DataField" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Join Select:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="joinselect" type="text" rmxref="Instance/Document/DCC/JoinSelect" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Join From:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="joinfrom" type="text" rmxref="Instance/Document/DCC/JoinFrom" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Join Where:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="joinwhere" type="text" rmxref="Instance/Document/DCC/JoinWhere" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Filter Dependencies:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="filterdepend" type="text" rmxref="Instance/Document/DCC/FilterDependencies" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Output Dependencies:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="outputdepend" type="text" rmxref="Instance/Document/DCC/OutputDependencies" size="50" /></td>
     </tr>
     <tr>
      <td colspan="2" align="center">
      <asp:Button runat="server" type="submit" ID="btnSave" Text="Save" class="button" OnClick="Save" OnClientClick ="return Validation();return ShowPleaseWait();" />  
      <asp:Button runat="server" type="submit" ID="btnCancel" Text="Cancel" class="button" OnClick="Cancel" />
      </td>
     </tr>
    </table>
   </p>   
 </form>
</body>
</html>
