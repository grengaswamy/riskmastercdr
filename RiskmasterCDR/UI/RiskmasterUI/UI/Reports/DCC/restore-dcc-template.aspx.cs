﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class restore_dcc_template : NonFDMBasePageCWS
    {
        private string sCWSresponse = "";
        private bool bReturnStatus;
        private XmlDocument oFDMPageDom = null;
        private IEnumerable result = null;
        private XElement rootElement = null;
        private DataTable dtGridData = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    XElement oMessageElement = GetMessageTemplate();

                    bReturnStatus = CallCWS("DCCAdaptor.LoadDeletedItem", oMessageElement, out sCWSresponse, false, true);

                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            if (bReturnStatus)
            {
                BindGridData();
            }
        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
              <Call>
               <Function></Function> 
              </Call>
              <Document>
               <DCC>
                 <ItemType>template</ItemType>
               </DCC> 
             </Document>
           </Message>


            ");

            return oTemplate;
        }

        private void BindGridData()
        {
            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            dtGridData = new DataTable();

            dtGridData.Columns.Add("templateId");
            dtGridData.Columns.Add("templateName");


            DataRow objRow = null;

            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("//item");

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();
                string templateId = objNodes.Attributes["id"].Value.ToString();
                string templateName = objNodes.Attributes["name"].Value.ToString();

                objRow["templateId"] = templateId;
                objRow["templateName"] = templateName;

                dtGridData.Rows.Add(objRow);
            }


            templateDisplaygd.DataSource = dtGridData;
            templateDisplaygd.DataBind();

        }

        protected void templateDisplaygd_PreRender(object sender, EventArgs e)
        {
            for (int lcount = 0; lcount < dtGridData.Rows.Count; lcount++)
            {
                Label report = ((Label)(templateDisplaygd.Rows[lcount].FindControl("templatename")));
                report.Text = dtGridData.Rows[lcount]["templateName"].ToString();
            }
        }

        protected void Restore(object sender, EventArgs e)
        {
            //need to bind a control to pass the selected reports ids

            this.itemlist.Text = this.selectedvalues.Text;
            this.itemtype.Text = "template";

            NonFDMCWSPageLoad("DCCAdaptor.RestoreItem");

            Response.Redirect("../DCC/templates-listing.aspx");
        }

        protected void Cancel(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/templates-listing.aspx");
        }
    }
}
