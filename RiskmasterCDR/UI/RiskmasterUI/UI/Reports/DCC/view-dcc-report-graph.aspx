﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="view-dcc-report-graph.aspx.cs"
    Inherits="Riskmaster.UI.Reports.Other_Reports.view_dcc_report_graph" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>    

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DCC Reports</title>

    <script language="javascript" src="../../../Scripts/dcc.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"  />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script type="text/javascript" language="javascript">
        function PageLoading() {
            win_init();
            winpage = new lib_doc_size();       
            /*
            Arguments for the AddWindow function:
            heading  - Heading for the window
            content - The content for the window
            left - The left position for the window
            top - The top position for the window
            width - The width of the window
            height - The height of the window
            bgcolor - If you want another backgroundcolor for only this window you can spesify that here
            bgcoloron - If you want another active backgroundcolor for only this window you can spesify that here

							There are 2 ways of adding windows.
            1. Use the addWindows and send the content as a variable or as a string:
            */
       }
    </script>
</head>
<body>
    <form id="frmData" runat="server" method="post">
    <script language="javascript" type="text/javascript">
        win_init();
        winpage = new lib_doc_size();
    </script>
    <table border="0" align="left" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td colspan="5">
                <a name="Images"></a>
                <asp:PlaceHolder runat ="server" ID="plcholder"></asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td width="65%">
            
               <script type="text/javascript" language="javascript">
                   var i = 1
               </script> 
                <% 
                    
                    foreach(XmlNode obj in objlist)
                   {%>
                         
                <script type="text/javascript" language="javascript">
                    //Start window
                    var controlname = 'h_rptname'+ i
                    addWindow(document.getElementById(controlname).value)
                    i=i+1
                </script>

                <table>
                    <tr>
                        <td>
                        <% if (obj.Attributes["extension"].Value.ToString() == ".gif")
                           { %>
                            
                            
                            <img id="imgReport2" name="imgReport" src="" runat="server" alt="" />
                            
                            <%}
                           else
                           { %>
                            <asp:GridView AlternatingRowStyle-CssClass="rowdark" HeaderStyle-CssClass="ctrlgroup" ID="reportDisplaygd" runat="server" AutoGenerateColumns="true" CssClass="singleborder" Width="100%" HeaderStyle-HorizontalAlign="Left">
                            </asp:GridView>
                           <%} %>
                        </td>
                    </tr>
                </table>

                <script type="text/javascript">
                    //End window
                    document.write(endWin())
                    //Create window (this have to be done manually. Just leave wins-1 in there)
                    create_window(wins-1,1,1,1,1)
                    // create_window(wins-1, 1, 1, 1, 1)
                    //Placing windows.
                    setWindows()
                    i=i+1
                </script>
            <% 
                    } %>
            </td>
            
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    
  </form>
 </body>
</html>
