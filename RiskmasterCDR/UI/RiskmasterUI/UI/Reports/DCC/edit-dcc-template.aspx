﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="edit-dcc-template.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.edit_dcc_template" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>DCC Template</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
    <script type="text/javascript">    
        function ShowPleaseWait()
        {
            pleaseWait.Show();
            return true;
        }
    </script> 
    
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server" method="post">
    
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
        
    <p align="center">
    <table cellspacing="2" cellpadding="2" class="singleborder">
     <tr>
      <td colspan="2" class="ctrlgroup">Edit Template</td>
     </tr>
     <tr>
      <td class="datatd"><b>Name:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="templatename" type="text" rmxref="Instance/Document/DCC/TemplateName" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Show Percent:</b></td>
      <td class="datatd"><asp:CheckBox runat="server" ID="percent" type="checkbox" rmxref="Instance/Document/DCC/ShowPercentage" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Show Graph:</b></td>
      <td class="datatd"><asp:CheckBox runat="server" ID="graph" type="checkbox" rmxref="Instance/Document/DCC/ShowGraph" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Filters:</b></td>
      <td class="datatd">
            <asp:Repeater runat ="server" ID="repeaterFilters" 
                onitemdatabound="repeaterFilters_ItemDataBound" >
                <ItemTemplate >
                    <asp:CheckBox ID="chkFilters" runat ="server" />
                    <asp:LinkButton ID="lnkFilters" runat ="server"></asp:LinkButton>
                </ItemTemplate>
                <SeparatorTemplate >
                    <br />
                </SeparatorTemplate>
            </asp:Repeater>
            <asp:Label runat ="server" ID="lblNoFilters" Text ="No Filters Defined" Visible ="true" ></asp:Label>
      </td>
     </tr>
     <tr>
      <td class="datatd"><b>Outputs:</b></td>
      <td class="datatd">
            <asp:Repeater runat ="server" ID="repeaterOutputs" 
                onitemdatabound="repeaterOutputs_ItemDataBound" >
                <ItemTemplate >
                    <asp:CheckBox ID="chkOutputs" runat ="server" />
                    <asp:LinkButton ID="lnkOutputs" runat ="server"></asp:LinkButton>
                </ItemTemplate>
                <SeparatorTemplate >
                    <br />                    
                </SeparatorTemplate>                
            </asp:Repeater>
            <asp:Label runat ="server" ID="lblNoOutputs" Text ="No Outputs Defined" Visible ="true" ></asp:Label>
      </td>
     </tr>
     <tr>
      <td colspan="2">&nbsp;</td>
     </tr>
     <tr>
      <td colspan="2" align="center">
        <asp:Button runat="server" ID="btnDel" type="submit" Text="Delete Selected" 
              class="button" onclick="Delete" />  
        <asp:Button runat="server" ID="btnAddFilter" type="submit" Text="Add Filter" class="button" OnClick="AddFilter" />  
        <asp:Button runat="server" ID="btnAddOutput" type="submit" Text="Add Output" class="button" OnClick="AddOutput" />  
        <asp:Button runat="server" ID="btnSave" type="submit" Text="Save" class="button" OnClick="Save" OnClientClick ="return ShowPleaseWait();" />  
        <asp:Button runat="server" ID="btnClose" type="submit" Text="Close" class="button" OnClick="Close" />
      </td>
     </tr>
    </table>
   </p>
   <asp:TextBox style="display:none" runat="server" ID="templateId" rmxref="Instance/Document/DCC/TemplateId" />
   <asp:TextBox style="display:none" runat="server" ID="templateIdHidden" rmxref="Instance/Document/template/@id" />
   <asp:TextBox style="display:none" runat="server" ID="templateNameHidden" rmxref="Instance/Document/template/@name" />
   <asp:TextBox style="display:none" runat="server" ID="templateShowPercentageHidden" rmxref="Instance/Document/template/@showpercentage" />
   <asp:TextBox style="display:none" runat="server" ID="templateShowGraphHidden" rmxref="Instance/Document/template/@showgraph" />
   <asp:TextBox style="display:none" runat="server" ID="itemlist" rmxref="Instance/Document/DCC/ItemList" />
   <asp:TextBox style="display:none" runat="server" ID="itemtype" rmxref="Instance/Document/DCC/ItemType" />
    </form>
</body>
</html>
