﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-dcc-template.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.add_dcc_template" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>DCC Template</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
    
    <script type="text/javascript">    
        function ShowPleaseWait()
        {
            pleaseWait.Show();
            return true;
        }
        function Validation() {
            if (document.getElementById("templatename").value == null || trimAll(document.getElementById("templatename").value) == "") {
                alert("Template Name Field cannot be left blank");
                document.forms[0].templatename.focus();
                return false;
            }
            return true;
        }
        function trimAll(sString) {
            while (sString.substring(0, 1) == ' ') {
                sString = sString.substring(1, sString.length);
            }
            while (sString.substring(sString.length - 1, sString.length) == ' ') {
                sString = sString.substring(0, sString.length - 1);
            }
            return sString;
        }
    </script>  
    
</head>
<body>
    <form id="frmData" runat="server" method="post">
    
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" /> 
  
    <p align="center">
    <table cellspacing="2" cellpadding="2" class="singleborder">
     <tr>
      <td colspan="2" class="ctrlgroup">Add New Template</td>
     </tr>
     <tr>
      <td class="datatd"><b>Name:</b></td>
      <td class="datatd"><asp:TextBox runat="server" ID="templatename"  type="text" rmxref="Instance/Document/DCC/TemplateName" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Show Percent:</b></td>
      <td class="datatd"><asp:CheckBox runat="server" ID="percent" type="checkbox" rmxref="Instance/Document/DCC/ShowPercentage" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Show Graph:</b></td>
      <td class="datatd"><asp:CheckBox runat="server" ID="graph" type="checkbox" rmxref="Instance/Document/DCC/ShowGraph" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Filters:</b></td>
      <td class="datatd">No Filters Defined</td>
     </tr>
     <tr>
      <td class="datatd"><b>Outputs:</b></td>
      <td class="datatd">No Outputs Defined</td>
     </tr>
     <tr>
      <td colspan="2">&nbsp;</td>
     </tr>
     <tr>
      <td colspan="2" align="center">
      <asp:Button ID="btnDel" runat="server" type="submit" Text="Delete Selected" class="button" disabled="true"/>
      <asp:Button ID="btnFilter" runat="server" type="submit" Text="Add Filter" class="button" disabled="true"/>
      <asp:Button ID="btnOutput" runat="server" type="submit" Text="Add Output" class="button" disabled="true"/>
      <asp:Button ID="btnSave" runat="server" type="submit" Text="Save" class="button" OnClick="Save" OnClientClick ="return Validation();return ShowPleaseWait();"/>
      <asp:Button ID="btnClose" runat="server" type="submit" Text="Close" class="button" OnClick="Close"/>
      </td>
     </tr>
    </table>
    <asp:TextBox style="display:none" runat="server" ID="templateId" rmxref="Instance/Document/template/@id" />
   </p>
  </form>
</body>
</html>
