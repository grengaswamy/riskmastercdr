<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="edit-dcc-report.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.edit_dcc_report" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html>
 <head runat="server">         
      <title>Add New Report</title>
      <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />  
      <link rel="stylesheet" href="../../../Content/system.css"  type="text/css" />      
      <script type ="text/javascript" language ="javascript" src ="../../../Scripts/dcc.js"></script> 
      <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
         
      <script type="text/javascript" language="javascript" >
        function UpdateOutputList()
        {
            var values="";        
                           
             for(var f=0;f<document.forms[0].selOutputBox.options.length;f++)
			    {
				   values += document.forms[0].selOutputBox.options[f].text + "," ;
			    }
			    document.forms[0].hndtxtselOutputBox.value = values;
			    
			    pleaseWait.Show();
                return true;
			    
        }
        
       function EnableDisableRadiobtn(nameregex, current) 
       {
            re = new RegExp(nameregex);
            for (i = 0; i < document.forms[0].elements.length; i++) 
            {
                elm = document.forms[0].elements[i]
                if (elm.type == 'radio')
                {
                    if (re.test(elm.name)) 
                    {
                    elm.checked = false;
                    }
                }
            }
            current.checked = true;
        } 
 
        
        function EnableDisable(value)
        {   
                   
            var chkControl= document.getElementById(value);
            
            var lnkControl =document.getElementById(value.substring(0,value.lastIndexOf('_')) + "_lnkFilter"); 
            
            var hndControl = document.getElementById(value.substring(0, value.lastIndexOf('_')) + "_hndFilter");

            PostBackURL = hndControl.value;        
           
            if (chkControl.checked)
            {
                lnkControl.disabled=false;
                
                lnkControl.setAttribute('href', PostBackURL);
                lnkControl.style.color="blue";
            }
            else
            {
                lnkControl.disabled=true;
                
                var href = lnkControl.getAttribute("href");
                
                    if(href && href != "" && href != null)
                    {
                        lnkControl.setAttribute('href_bak', href);
                    }
                    
                    lnkControl.removeAttribute('href');
                    lnkControl.style.color="gray";
            }
            
            return false;
        }
      
      </script>
      
      <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
    
    <script type="text/javascript">    
        function ShowPleaseWait()
        {
            pleaseWait.Show();
            return true;
        }
    </script> 
    
      <style type="text/css">
          .style1
          {
              width: 112px;
          }
      </style>
  </head>
  
 <body  onload="parent.MDIScreenLoaded();">
  <form id="frmData" runat ="server" method="post">
    
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    
    <p align="center" >    
    <table class="singleborder">
     <tr>
        <td colspan="2" class="ctrlgroup">Edit Report</td>
     </tr>
     
     <tr>
        <td align ="left" class="style1"><b>Template Name:</b></td>
        <td align ="left"><asp:Label ID="lblTemplateName" runat ="server" ></asp:Label></td>
     </tr>
     
     <tr>
        <td align ="left" class="style1"><b>Report Title:</b></td>
        <td>
            <asp:TextBox ID ="txtReportTitle" runat="server" type="text" rmxignoreget="true" rmxref="/Instance/Document/DCC/ReportName" size="50" style="border: 1px solid #104A7B;"></asp:TextBox>            
        </td>
     </tr>
     
     <tr>
        <td align ="left" class="style1"><b>Filters:</b></td>
        <td>
        <table border="0" align="left" cellpadding="0" cellspacing="0">        
            <tr>
                <td align ="left">
                <asp:TextBox runat="server"  id="hndtxtFilterList" rmxref="/Instance/Document/DCC/FilterList" style="display :none"></asp:TextBox>
                     <asp:Repeater ID="repterFilter" runat ="server" 
                        onitemdatabound="repterFilter_ItemDataBound">
                         <ItemTemplate >   
                            <asp:CheckBox ID ="chkFilter" rmxignoreget="true" runat ="server"  Checked ='<%# Convert.ToBoolean(Eval("selected")) %>' onclick='EnableDisable(id);' />
                            <asp:LinkButton ID="lnkFilter" rmxignoreget="true" runat ="server"  Text ='<%# (Eval("name")) %>'></asp:LinkButton>
                            <asp:HiddenField runat ="server" ID="hndFilter" />
                        </ItemTemplate>
                        <SeparatorTemplate >
                            <br />
                        </SeparatorTemplate>
                    </asp:Repeater>
                </td>
            </tr>            
       </table>
      </td>
     </tr>
     
     <tr>
      <td align ="left" class="style1"><b>Outputs:</b></td>
      <td valign="top">
       <table border="0" cellpadding="0" cellspacing="0" align="left">
        <tr>
         <td align ="left">
            <asp:DropDownList runat="server" ID="cboOutputBox" rmxignoreset="true" rmxref="/Instance/Document/DCC/outputs" ItemSetRef="/Instance/Document/report/outputs" size="1"></asp:DropDownList>
           <asp:Button runat ="server" ID="btnAdd" type="button" class="button" Text="Add" OnClientClick="return AddOutput();" />
         </td> 
         </tr>
         <tr>
          <td>
            <asp:ListBox runat="server" ID="selOutputBox" size="5" rmxignoreset="true" rmxref="/Instance/Document/DCC/OutputList" ItemSetRef="/Instance/Document/report/selectedoutputs" EnableViewState ="false" AutoPostBack ="false" ></asp:ListBox>
            <asp:TextBox runat="server"  id="hndtxtselOutputBox" rmxignoreget="true" rmxref="/Instance/Document/DCC/OutputList" style="display :none"></asp:TextBox>
           
           <a class="LightBold">
                <img src="../../../Images/up.gif" width="20" onclick ="MoveField(0);" height="20" border="0" alt="Up" title="Move Up" />
           </a>
           <a class="LightBold">
                <img src="../../../Images/down.gif" width="20" onclick ="MoveField(1);" height="20" border="0" alt="Down" title="Move Down" />
           </a>
           <a class="LightBold">
                <img src ="../../../Images/delete3.gif" width="20" onclick ="DeleteField();" height="20" border="0" alt="Delete" title="Remove" />
           </a>
           </td>
        </tr>
       </table>
      </td>
     </tr>
     
     <tr>
        <td align ="left" class="style1"><b>Show As Graph:</b></td>
        <td align ="left">
            <asp:CheckBox ID="chkShowAsGraph" runat ="server" type="checkbox"  rmxref="/Instance/Document/report/showgraph" rmxignoreset="true" appearance="full" />
            <asp:TextBox style="display:none"  ID ="txtShowAsGraph" runat ="server" rmxref="/Instance/Document/DCC/ShowGraph" rmxignoreget="true"></asp:TextBox>
        </td>
     </tr>
     
     <tr>
      <td align ="left" class="style1"><b>Graph Group:</b></td>
      <td>
       <table style="border: 1px solid #104A7B;" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align ="left">
                <asp:Repeater ID="repterGraphGroup" runat="server">
                    <ItemTemplate >
                         <asp:RadioButton ID="radioGraphGroup" runat ="server" value="" onclick="return EnableDisableRadiobtn('repterGraphGroup.*GraphGroup',this);"  GroupName="GraphGroup" Text ='<%# (Eval("name")) %>' Checked ='<%# Convert.ToBoolean(Convert.ToInt32(Eval("selected"))) %>' />
                    </ItemTemplate>
                    <SeparatorTemplate >
                      <br />
                    </SeparatorTemplate>
                </asp:Repeater>    
                <asp:TextBox runat ="server" id="txtradioGraphGroup" style="display:none" rmxref="/Instance/Document/DCC/GraphGroup"></asp:TextBox>            
            </td>
        </tr>        
       </table>       
      </td>
     </tr>
     
     <tr>
          <td align ="left" class="style1"><b>Graph Data:</b></td>
          <td>
                <table style="border: 1px solid #104A7B;" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align ="left">
                            <asp:Repeater ID="repterGraphData" runat ="server">
                                <ItemTemplate>                                
                                    <asp:RadioButton ID="radioGraphData" runat ="server" onclick="return EnableDisableRadiobtn('repterGraphData.*GraphData',this);" value="" GroupName="GraphData" Text ='<%# (Eval("name")) %>' Checked ='<%# Convert.ToBoolean(Convert.ToInt32(Eval("selected"))) %>' />
                                </ItemTemplate>
                                <SeparatorTemplate >
                                    <br />
                                </SeparatorTemplate>
                            </asp:Repeater>
                            <asp:TextBox runat ="server" id="txtradioGraphData" style="display:none" rmxref="/Instance/Document/DCC/GraphData"></asp:TextBox>
                        </td>
                    </tr>
                </table>
          </td>
     </tr>
     
     <tr>
      <td align ="left" class="style1"><b>Graph Type:</b></td>
      <td align ="left">
       <table border="0" cellpadding="0" cellspacing="0">
        <tr>
         <td>
            <asp:DropDownList runat="server" ID="ddlGraphType" rmxref="/Instance/Document/DCC/GraphType">
                <asp:ListItem Value ="0">2D-Pie</asp:ListItem>
                <asp:ListItem Value ="1">3D-Pie</asp:ListItem>
                <asp:ListItem Value ="2">2D-Bar</asp:ListItem>
                <asp:ListItem Value ="3">3D-Bar</asp:ListItem>               
            </asp:DropDownList>
         </td>
        </tr>
       </table>
      </td>
     </tr>     
     <tr>
      <td colspan="2" align="center">
        <asp:Button ID="btnSave" runat="server" type="submit" Text="Save" class="button" OnClientClick ="return UpdateOutputList();"
              onclick="Save" />              
        <asp:Button ID="btnClose" runat="server" type="submit" Text="Close" class="button" onclick="Close" />
      </td>
     </tr>         
    </table>  
        <input type="hidden" name="" value="claimstatus claimtype " id="hiddenoutputlist" />        
    </p>        
   </form>
 </body>
</html>
