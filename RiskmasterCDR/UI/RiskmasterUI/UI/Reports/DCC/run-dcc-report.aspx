<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="run-dcc-report.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.run_dcc_report" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  <title>Riskmaster.Net</title>
  <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"  />
  <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
</head>
<body>
    <form id="frmData" runat="server" method="post">
    <uc1:ErrorControl id="ErrorControl1" runat="server" />
    <h2><%
           if (reportname!= null)  
                reportheadings.Text = reportname.Attribute("name").Value; %>
    <asp:Label ID="reportheadings" runat="server" />
    </h2>
    <table width="100%" cellspacing="0" cellpadding="2" border="0">
    <tr>
    <asp:Button runat="server" type="submit" ID="btnback1" Text="Back" class="button" OnClick="Back" />
    </tr>
    <tr>
    <asp:GridView AlternatingRowStyle-CssClass="rowdark" HeaderStyle-CssClass="ctrlgroup" ID="reportDisplaygd" runat="server" AutoGenerateColumns="true" CssClass="singleborder" Width="100%" HeaderStyle-HorizontalAlign="Left">
    </asp:GridView>
    </tr>
   </table>
   <asp:Button runat="server" type="submit" Text="Back" ID="btnBack" class="button" OnClick="Back" />
   <asp:TextBox style="display:none" runat="server" ID="ReportId" rmxref="Instance/Document/DCC/ReportId" />
   </form>
</body>
</html>
