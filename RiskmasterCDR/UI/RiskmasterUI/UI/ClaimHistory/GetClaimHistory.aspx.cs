﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.ClaimHistory
{
    public partial class GetClaimHistory : NonFDMBasePageCWS
    {
        /// <summary>
        /// Contains web service output
        /// </summary>
        XmlDocument oFDMPageDom = null;
        public IEnumerable result = null;
        public XElement rootElement = null;
        string sReturn = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            oFDMPageDom = new XmlDocument();
            if (!Page.IsPostBack)
            {
                

                //Preparing XML to send to service
                XElement oMessageElement = GetMessageTemplate();

                XElement oElement = oMessageElement.XPathSelectElement("./Document/ClaimHistory/ClaimId");
                if (oElement != null)
                {      // Pen testing changes by atavaragiri :mits 27790
                    //oElement.Value = AppHelper.GetQueryStringValue("ClaimId");
                    oElement.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimId"));
                    //END: Pen testing changes by atavaragiri :mits 27790
                }

                oElement = oMessageElement.XPathSelectElement("./Document/ClaimHistory/ClaimantEId");
                if (oElement != null)
                {// Pen testing changes by atavaragiri :mits 27790
                    //oElement.Value = AppHelper.GetQueryStringValue("ClaimantEId");
                    oElement.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimantEId"));
                    // END Pen testing changes by atavaragiri :mits 27790
                }

                oElement = oMessageElement.XPathSelectElement("./Document/ClaimHistory/DefendantEId");
                if (oElement != null)
                {  
                    // Pen testing changes by atavaragiri :mits 27790
                    //oElement.Value = AppHelper.GetQueryStringValue("DefendantEId");
                    oElement.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("DefendantEId"));
                    // END Pen testing changes by atavaragiri :mits 27790
                }

                oElement = oMessageElement.XPathSelectElement("./Document/ClaimHistory/UnitId");
                if (oElement != null)
                {// Pen testing changes by atavaragiri :mits 27790
                    //oElement.Value = AppHelper.GetQueryStringValue("UnitId");
                    oElement.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("UnitId"));
                    // END Pen testing changes by atavaragiri :mits 27790
                }

                oElement = oMessageElement.XPathSelectElement("./Document/ClaimHistory/EventId");
                if (oElement != null)
                {
                    // Pen testing changes by atavaragiri :mits 27790
                    //oElement.Value = AppHelper.GetQueryStringValue("EventId");
                    oElement.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("EventId"));
                    // END Pen testing changes by atavaragiri :mits 27790
                }

                oElement = oMessageElement.XPathSelectElement("./Document/ClaimHistory/EventNumber");
                if (oElement != null)
                { 
                    // Pen testing changes by atavaragiri :mits 27790
                    //oElement.Value = AppHelper.GetQueryStringValue("EventNumber");
                    oElement.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("EventNumber"));
                    // END Pen testing changes by atavaragiri :mits 27790
                }
                // Ayush: MITS:18230, 01/22/2010 Start
                oElement = oMessageElement.XPathSelectElement("./Document/ClaimHistory/PropertyId");
                {  
                    // Pen testing changes by atavaragiri :mits 27790
                    //oElement.Value = AppHelper.GetQueryStringValue("PropertyId");
                    oElement.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PropertyId"));
                    //END Pen testing changes by atavaragiri :mits 27790
                }
                // Ayush: MITS:18230, 01/22/2010 End
                //Calling Service to get all PreBinded Data 
                bool bReturn = CallCWSFunction("ClaimHistoryAdaptor.GetClaimHistory" , oMessageElement);
                oFDMPageDom = Data;
                
                rootElement = XElement.Parse(oFDMPageDom.OuterXml);
                result = from c in rootElement.XPathSelectElements("//ClaimHistory/option")
                         select c;
            }
        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            // Ayush: MITS:18230, 01/22/2010 Start:
            //            XElement oTemplate = XElement.Parse(@"
            //            <Message>
            //              <Authorization>32187afd-8341-426a-9fa8-1e88fd92f8a8</Authorization> 
            //             <Call>
            //              <Function>ClaimHistoryAdaptor.GetClaimHistory</Function> 
            //              </Call>
            //            <Document>
            //                <ClaimHistory>
            //                  <ClaimId></ClaimId> 
            //                  <ClaimantEId></ClaimantEId> 
            //                  <DefendantEId /> 
            //                  <UnitId /> 
            //                  <EventId /> 
            //                  <EventNumber />
            //                </ClaimHistory>
            //            </Document>
            //              </Message>
            //            ");
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>32187afd-8341-426a-9fa8-1e88fd92f8a8</Authorization> 
             <Call>
              <Function>ClaimHistoryAdaptor.GetClaimHistory</Function> 
              </Call>
            <Document>
                <ClaimHistory>
                  <ClaimId></ClaimId> 
                  <ClaimantEId></ClaimantEId> 
                  <DefendantEId /> 
                  <UnitId /> 
                  <EventId /> 
                  <EventNumber /> 
	              <PropertyId /> 
              </ClaimHistory>
            </Document>

              </Message>


            ");
            // Ayush: MITS:18230 01/22/2010 End
            return oTemplate;
        }
    }
}
