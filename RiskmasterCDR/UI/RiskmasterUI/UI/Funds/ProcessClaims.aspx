﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProcessClaims.aspx.cs" Inherits="Riskmaster.UI.UI.Funds.ProcessClaims" %>
<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
        <title>Process Claims</title>
          <uc4:CommonTasks ID="CommonTasks1" runat="server" />
         <script language='javascript' type="text/javascript">
        function submitthisPage() {
        
            if (document.forms[0].hdnFirstTime != null) {
                if (document.forms[0].hdnFirstTime.value == "0") {
                    document.forms[0].hdnFirstTime.value = "1";
                    document.forms[0].hdnSelected.value = self.parent.window.opener.document.forms[0].hdnSelected.value;
                    document.forms[0].hdnToDate.value = self.parent.window.opener.document.forms[0].txtToDate.value;
                    document.forms[0].hdnFromDate.value = self.parent.window.opener.document.forms[0].txtFromDate.value;
		    //MITS 22409 by gbhatnagar : Start
                    document.forms[0].hdnIsPolicyTracking.value = self.parent.window.opener.document.forms[0].rbtnPolicyTracking.checked;
		    //MITS 22409 by gbhatnagar : End
                    document.forms[0].submit();
                    pleaseWait.Show();
                    document.getElementById("Message").style.visibility = "visible";

                }
                else {
                    document.getElementById("Message").style.visibility = "hidden";
                }
            }
        
         }  
         </script> 
    </head>
    <body onload="javascript:submitthisPage();" marginwidth="0" marginheight="0" bottommargin="0" leftmargin="0" rightmargin="0" scrolltop="0" scrollleft="0" topmargin="0">
        <form id="frmData" runat="server">
            <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" /> 
            <table align="center" width="100%" border="0" id="Message">
			    <tr>
                    <td width="100%" valign="middle" align="center" colspan="3">
                        <font face="Verdana, Arial" size="2">
                            <b>
                                <asp:Label ID="lblClaimHdr" runat="server" Text="<%$ Resources:lblClaimHdrResrc %>" /> 
                            </b>
                        </font>
                    </td>
                </tr>
			</table> 
            <asp:HiddenField ID="hdnFirstTime" runat="server"  Value="0"/>
            <asp:HiddenField ID="hdnSelected" runat="server"  Value="0"/>
            <asp:HiddenField ID="hdnToDate" runat="server"  Value="0"/>
            <asp:HiddenField ID="hdnFromDate" runat="server"  Value="0"/>
            <%-- MITS 22409 by gbhatnagar : Start --%>
            <asp:HiddenField ID="hdnIsPolicyTracking" runat="server"  Value="true"/>
            <%-- MITS 22409 by gbhatnagar : End --%>
    
            <%if (hdnFirstTime.Value=="1"){ %>
                <%if (Model.SelectSingleNode("//Errors/@NoPayments").Value != "0")
                { %>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center">
                                <a href="" onclick="window.parent.close()">
                                    <asp:Label ID="hyperClose" runat="server" Text="<%$ Resources:lblhyperCloseResrc %>" />
                                </a>
                            </td>
                        </tr>
                    </table>
                <%} %>
                <%if (Model.SelectSingleNode("//Errors/@NoPayments").Value == "0")
                { %>
                    <script language="javascript" type="text/javascript">
                        //alert("There are no payments to process.");
                        alert(ProcessClaimsValidations.ValidProcessIssue);
                        window.parent.close();
                    </script>
                <%} %>
                <%else if (Model.SelectSingleNode("//Errors/@NoPayments").Value != "0")
                { %>
                    <div id="divForms" class="divScroll">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="colheader5">
                            <asp:Label ID="lblClaimNumber" runat="server" Text="<%$ Resources:lblClaimNumberResrc %>" />
                        </td>
                        <td class="colheader5">
                            <asp:Label ID="lblControlNumber" runat="server" Text="<%$ Resources:lblControlNumberResrc %>" />
                        </td>
                        <td class="colheader5">
                            <asp:Label ID="lblMessage" runat="server" Text="<%$ Resources:lblMessageResrc %>" />
                        </td>
                    </tr>
                    <%foreach(XmlNode node in  Model.SelectNodes("//ClaimNumber"))
                    {
                        int iCount = 0;%> 
                        <%if ((iCount %2) !=0){ %>
                            <tr class="rowlight1">
                                <td>
                                    <%=node.InnerText%>
                                </td>
                                <td>
                                    <%=node.Attributes["ControlNumber"].Value%>
                                </td>			
                                <td>
                                    <asp:Label ID="lblProcessingComplete" runat="server" Text="<%$ Resources:lblProcessingCompleteResrc %>" />
                                </td>
                            </tr>
                        <%} %>
                        <%else{ %>
                            <tr class="rowdark1">
                                <td>
                                    <%=node.InnerText%>
                                </td>
                                <td>
                                    <%=node.Attributes["ControlNumber"].Value%>
                                </td>
                                <td>
                                    <asp:Label ID="lblProcessingComplete1" runat="server" Text="<%$ Resources:lblProcessingCompleteResrc %>" />
                                </td>
                            </tr>
                        <%} %>
                    <%} %>
                <%} %>
                </table>
                </div>	  
     
            <%} %>
            <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"   CustomMessage="_" />
        </form>
    </body>
</html>
