﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;
using System.Text.RegularExpressions;

namespace Riskmaster.UI.UI.Funds
{
    public partial class GeneratePayToTheOrderOf : NonFDMBasePageCWS
    {
        #region "Constants/Class Variables"

        private const string PayeePhraseLocation = "PAYEE_PHRASE_LOCATION";
        private const string PayeePhraseList = "PAYEE_PHRASE_LIST";
        private const string SepratorSplitVal = "!@|";
        private const string PayeeNameSplit = "*~*";
        private const string ReplacedVal = "^^^";
        private DataSet oDS = null;
        #endregion

        #region "Events"

        protected void Page_Load(object sender, EventArgs e)
        {
            ////if(!IsPostBack)
            ////    Page.ClientScript.RegisterStartupScript(this.GetType(), "SetHiddenFieldVal", "SetHiddenFieldVal()", true);
            string strPayeeList = string.Empty;

            if (Request.QueryString["PayeeList"] != null)
            {
                strPayeeList = Request.QueryString["PayeeList"].Replace(ReplacedVal,"&");

                BindGridData(strPayeeList);
            }
        }

        protected void gvPayToTheOrderOf_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
                DropDownList ddlPayeePhraseLocation;
                ddlPayeePhraseLocation = (DropDownList)e.Row.FindControl("lstPayeePhraseLocation");

                DropDownList ddlPayeePhrase;
                ddlPayeePhrase = (DropDownList)e.Row.FindControl("lstPayeePhrase");
                
                if(oDS != null)
                {
                    ListItem item = null;
                    for (int i = 0; i < oDS.Tables["option"].Rows.Count; i++)
                    {
                        item = new ListItem();
                        item.Text = oDS.Tables["option"].Rows[i]["option_text"].ToString();
                        item.Value = oDS.Tables["option"].Rows[i]["value"].ToString();

                        switch (oDS.Tables["option"].Rows[i]["table_name"].ToString().ToUpper())
                        {
                            case PayeePhraseLocation:
                                ddlPayeePhraseLocation.Items.Add(item);
                                break;
                            case PayeePhraseList:
                                ddlPayeePhrase.Items.Add(item);
                                break;
                        }
                    }

                    //srajindersin 6/12/2014 MITS 36540 - Adding empty option in Payee Phrase dropdown
                    item = new ListItem();
                    item.Text = "";
                    item.Value = "";
                    ddlPayeePhrase.Items.Add(item);
                }
            }
        }

        #endregion

        #region "Private Methods"

        private DataSet GetCodeType()
        {
            try
            {
                XElement XmlTemplate = null;
                DataSet oCodeTypeDataSet = new DataSet();
                string sPolCode = "";
                string Data = "";
                bool bReturnStatus = false;
                XmlTemplate = GetMessageTemplateForCodeType();

                //txtPolicyClaimLOB.Text = lstPolicyClaimLOB.SelectedValue;
                bReturnStatus = CallCWS("GeneratePayToTheOrderOfAdaptor.GetCodeType", XmlTemplate, out Data, false, true);
                if (Data != null && !string.IsNullOrEmpty(Data))
                {
                    oCodeTypeDataSet.ReadXml(new StringReader(Data));

                    return oCodeTypeDataSet;
                }
                return null;
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return null;
            }
        }

        private void BindGridData(string PayeeList)
        {
            try
            {
                string data = string.Empty;
                string[] stringSepratorSplitVal = new string[] { SepratorSplitVal };
                string[] stringPayeeNameSplit = new string[] { PayeeNameSplit };
                DataTable dtGridData = new DataTable();
                DataRow objRow = null;
                string[] arrPayeeNameID = PayeeList.Split(stringSepratorSplitVal, StringSplitOptions.None);
                string[] arrSplitedNameID = null;
                string sPayee = string.Empty;
                string sPayeeEId = string.Empty;
                int intCount = 0;

                dtGridData.Columns.Add("RowID");
                dtGridData.Columns.Add("Payee");
                dtGridData.Columns.Add("PayeePhraseLocation");
                dtGridData.Columns.Add("PayeePhrase");


                foreach (string payee in arrPayeeNameID)
                {
                    objRow = dtGridData.NewRow();
                    arrSplitedNameID = payee.Split(stringPayeeNameSplit, StringSplitOptions.None);
                    sPayee = arrSplitedNameID[0];
                    sPayeeEId = arrSplitedNameID[1];

                    objRow["RowID"] = sPayeeEId;
                    objRow["Payee"] = sPayee;

                    dtGridData.Rows.Add(objRow);
                    intCount++;
                }
                if (dtGridData.Rows.Count <= 0)
                {

                    objRow = dtGridData.NewRow();
                    objRow["RowID"] = "";
                    objRow["Payee"] = "";
                    objRow["PayeePhraseLocation"] = "";
                    objRow["PayeePhrase"] = "";

                    dtGridData.Rows.Add(objRow);
                }

                if (oDS == null)
                    oDS = GetCodeType();

                gvPayToTheOrderOf.DataSource = dtGridData;
                gvPayToTheOrderOf.DataBind();

                oDS = null;
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplateForCodeType()
        {
            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;
            string sPolSystemID = string.Empty;
            string sPolSysTypeID = string.Empty;
            sXML.Append("<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization>");
            sXML.Append("<Call><Function>GeneratePayToTheOrderOfAdaptor.GetCodeType</Function></Call>");
            sXML.Append("<Document><SystemTableName>");
            sXML.Append("'" + PayeePhraseLocation + "', '" + PayeePhraseList + "'");
            sXML.Append("</SystemTableName></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }

        #endregion      

    }

}