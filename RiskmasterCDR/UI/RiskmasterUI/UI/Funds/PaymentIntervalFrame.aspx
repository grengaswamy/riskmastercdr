﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentIntervalFrame.aspx.cs" Inherits="Riskmaster.UI.UI.Funds.PaymentIntervalFrame" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link rel="Stylesheet" href="../../Content/rmnet.css" type="text/css" />
<link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
<link rel="stylesheet" href="../../Content/system.css" type="text/css" />
<script type="text/javascript" language="JavaScript" src="../../Scripts/getcode.js">    { var i; }</script>
<script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">    { var i; }</script>
</head>
<body class="10pt" onload="OnPaymentIntervalFrameLoad();">
    <form name="frmData" id="frmData" runat="server">
        <div> 
            <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
             <tr>
                <td colspan="2">
                <div>
                    <asp:label runat="server" class="label" id="lbl_PaymentIntervalDays" Text="<%$ Resources:lblPaymentIntervalDaysResrc %>" />
                    <span class="formw">
                    <asp:TextBox runat="server" id="PaymentIntervalDays" onblur="numLostFocus(this);" RMXType="text"/>
                    </span>
                 </div>
                 </td>
                 </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr><td colspan="2">&nbsp;</td></tr>
              <tr><td>
              <div>
              <asp:button class="button" runat="server" id="Ok" RMXRef="" Text="<%$ Resources:btnOkResrc %>" width="100" onClientClick="return PaymentIntervalFrameOk();"/>
              <asp:button class="button" runat="server" id="Cancel" RMXRef="" Text="<%$ Resources:btnCancelResrc %>" width="100" onClientClick="window.close();" />
             </div>
             </td>
              </tr>
            </table>
      </div>
       <input type="hidden" value="" id="PayIntDays" runat="server" />
    </form>
</body>
</html>
