﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomPaymentNotification.aspx.cs" Inherits="Riskmaster.UI.UI.Funds.CustomPaymentNotification" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Payment Notification</title>
     <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
     
    <!-- Rakhel ML Changes - Start !-->
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
     <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
     <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">         { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
     <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">         { var i; } </script>	
     <script src="../../Scripts/form.js" type="text/javascript"></script>
     <!--Rakhel ML Changes - End !-->			
     <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <style>
       .divScroll
{
    BORDER-RIGHT: darkblue 1px solid;
    BORDER-TOP: darkblue 1px solid;
    OVERFLOW: auto;
    BORDER-LEFT: darkblue 1px solid;
    WIDTH: 99%;
    BORDER-BOTTOM: darkblue 1px solid;
    BACKGROUND-COLOR: #ffffff;
    height:230px !important
}
    </style>
</head>
 <body onload="fillDates();ToggleAllUsers();parent.MDIScreenLoaded();" onunload="Disp();">
                        <script language="javascript" type="text/javascript">
                            function fillDates() {
                                    if (document.getElementById('hdnfrom').value == '') {
                                        var today = new Date();
                                        var one_day = 1000 * 60 * 60 * 24;

                                        var d = today.getTime() - 6 * one_day;
                                        var d1 = new Date(d);
                                        document.getElementById('todate').value = today.getMonth() + 1 + "/" + today.getDate() + "/" + (today.getYear());
                                        document.getElementById('fromdate').value = d1.getMonth() + 1 + "/" + d1.getDate() + "/" + (d1.getYear());
                                    }
                                    else {
                                        document.getElementById('fromdate').value = document.getElementById('hdnfrom').value;
                                        document.getElementById('todate').value = document.getElementById('hdnto').value;
                                    }

                                    if (!document.getElementById('weekmonth').checked) {
                                        if (document.getElementById('hdnendof').value == 'Week')
                                            document.getElementById('week').checked = true;
                                        else
                                            document.getElementById('month').checked = true;
                                    }
                                }

                                function ToggleAllUsers() {
                                    if (document.getElementById('weekmonth').checked) {
                                        document.getElementById('fromdate').disabled = true;
                                        //Rakhel ML Changes
                                        //document.getElementById('btnfrom').disabled = true;
                                        document.getElementById('todate').disabled = true;
                                        //document.getElementById('btnto').disabled = true;
                                        var btnfrom = $("#fromdate"); var btnto = $("#todate");
                                        $.datepicker._disabledInputs = [btnfrom[0], btnto[0]];
                                        //Rakhel ML Changes
                                        document.getElementById('week').disabled = false;
                                        document.getElementById('month').disabled = false;

                                        document.getElementById('hdnfrom').value = document.getElementById('fromdate').value;
                                        document.getElementById('hdnto').value = document.getElementById('todate').value;
                                    }
                                    else {
                                        document.getElementById('fromdate').disabled = false;
                                        //Rakhel ML Changes
                                        //document.getElementById('btnfrom').disabled = false;
                                        document.getElementById('todate').disabled = false;
                                        //document.getElementById('btnto').disabled = false;
                                        $.datepicker._disabledInputs = [];
                                        //Rakhel ML Changes
                                        document.getElementById('week').disabled = true;
                                        document.getElementById('month').disabled = true;

                                        if (document.getElementById('week').checked)
                                            document.getElementById('hdnendof').value = 'Week';
                                        else if (document.getElementById('month').checked)
                                            document.getElementById('hdnendof').value = 'Month';
                                    }
                                }

                                function submitQuery() {
                                    var sFromDate = new String();
                                    var sToDate = new String();
                                    var dtFrom = new Date();
                                    var dtTo = new Date();

                                    if (document.getElementById('daterange').checked) {

                                        sFromDate = document.getElementById('fromdate').value;
                                        sToDate = document.getElementById('todate').value;

                                        if (sFromDate == "") {
                                            //alert("Please enter a start date");
                                            alert(CustomPaymentNotificationValidations.ValidEnterStartDt);
                                            return false;
                                        }
                                        if (sToDate == "") {
                                            //alert("Please enter an end date");
                                            alert(CustomPaymentNotificationValidations.ValidEnterEndDt);
                                            return false;
                                        }
                                        dtFrom = Date.parse(sFromDate);
                                        dtTo = Date.parse(sToDate)

                                        if (dtFrom > dtTo) {
                                            //alert('From Date should be less than or equal to the End date');
                                            alert(CustomPaymentNotificationValidations.ValidFromAndTo);
                                            return false;
                                        }
                                        document.getElementById('hdndueby').value = "DateRange";
                                    }
                                    else {
                                        document.getElementById('hdndueby').value = "WeekMonth";
                                    }

                                    document.getElementById('hdnfrom').value = document.getElementById('fromdate').value;
                                    document.getElementById('hdnto').value = document.getElementById('todate').value;

                                    if (document.getElementById('week').checked)
                                        document.getElementById('hdnendof').value = 'Week';
                                    else if (document.getElementById('month').checked)
                                        document.getElementById('hdnendof').value = 'Month';

                                    //document.getElementById('hdnaction').value = 'Submit';
                                    return true;
                                }


                                //Rakhel ML Changes - Start
                                function Disp() {
                                    $.datepicker._disabledInputs = [];
                                }
                                //Rakhel ML Changes - End
//                                function ShowReport() {
//                                    var sDueBy;
//                                    var sEndOf;
//                                    if (document.getElementById('weekmonth').checked)
//                                        sDueBy = 'WeekMonth';
//                                    else if (document.getElementById('daterange').checked)
//                                        sDueBy = 'DateRange';

//                                    if (document.getElementById('week').checked)
//                                        sEndOf = 'Week';
//                                    else if (document.getElementById('month').checked)
//                                        sEndOf = 'Month';

//                                    var sFromDate = document.getElementById('fromdate').value;
//                                    var sToDate = document.getElementById('todate').value;
//                                    var sQueryString = 'DueBy=' + sDueBy + '&EndOf=' + sEndOf + '&FromDate=' + sFromDate + '&ToDate=' + sToDate;

//                                    RMX.window.open('home?pg=riskmaster/Funds/PaymentNotificationReport&' + sQueryString, 'PaymentNotification',
//							'width=650,height=550,top=' + (screen.availHeight - 550) / 2 + ',left=' + (screen.availWidth - 650) / 2 + ',resizable=yes,scrollbars=yes');

//                                    return false;
                                //                                }
                                
                                
				</script> 
                
    <form id="frmData" runat="server">
   
        <table width="100%">    
            <tr>
                <td class="msgheader">
                    <asp:Label runat="server" ID="lblChecks"></asp:Label>         
                    <asp:Label ID="lblPrintedChecks" runat="server" Text="<%$ Resources:lblPrintedChecksResrc %>" />
                </td>
            </tr>
            <tr>
                <td>
                    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
                </td>
            </tr>
        </table>
        <input type="hidden" runat="server" value="" id="hdndueby" />
        <input type="hidden" runat="server" value="" id="hdnfrom" />
        <input type="hidden" runat="server" value="" id="hdnto" />
        <input type="hidden" runat="server" value="" id="hdnendof" />
        <table width="100%">
            <tr>
                <td>
                    <input type="radio" name="ShowPayment" value="WeekMonth" id="weekmonth"  onclick="ToggleAllUsers();" checked />
                    <asp:Label ID="lblPaymentDue" runat="server" Text="<%$ Resources:lblPaymentDueResrc %>" /><br />
                    <input type="radio" name="ShowPayment"  runat="server" value="DateRange" id="daterange" onclick="ToggleAllUsers();" />
                    <asp:Label ID="lblPaymentDateRange" runat="server" Text="<%$ Resources:lblPaymentDateRangeResrc %>" />
                </td>
                <td>
                    <input type="radio" name="dueby" value="Week" id="week" runat="server" />
                    <asp:Label ID="lblEndOfWeek" runat="server" Text="<%$ Resources:lblEndOfWeekResrc %>" />&nbsp;&nbsp;&nbsp;
                    <input type="radio" runat="server" name="dueby" value="Month" id="month" checked />
                    <asp:Label ID="lblEndOfMonth" runat="server" Text="<%$ Resources:lblEndOfMonthResrc %>" /><br />
                    <asp:Label ID="lblFrom" runat="server" Text="<%$ Resources:lblFromResrc %>" /> 
                    <input type="text" runat="server" value="" id="fromdate" size="20" onblur="dateLostFocus('fromdate');" disabled="disabled" />
                   
                 <!-- Rakhel ML Changes - Start !-->
                   
                     <script type="text/javascript">
                             $(function () {
                                 $("#fromdate").datepicker({
                                     showOn: "button",
                                     buttonImage: "../../Images/calendar.gif",
                                     buttonImageOnly: true,
                                     showOtherMonths: true,
                                     selectOtherMonths: true,
                                     changeYear: true
                                 });
                             });
                     </script>
                     <!--Rakhel ML Changes - End !-->
                    <asp:Label ID="lblTo" runat="server" Text="<%$ Resources:lblToResrc %>" /> 
                    <input type="text" runat="server" value="" id="todate" size="20" onblur="dateLostFocus('todate');" disabled="disabled" />
                   
                    <!-- Rakhel ML Changes - Start !-->
                   
                    <script type="text/javascript">
                        $(function () {
                            $("#todate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                    
                    </script>
                    <!--Rakhel ML Changes - End !-->
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnSubmit" UseSubmitBehavior="true" runat="server" Text="<%$ Resources:btnSubmitResrc %>" CssClass="button" OnClientClick="Javascript:return submitQuery();" onclick="btnRefresh_Click"/>
                </td>
            </tr>
        </table>
        <asp:Panel ID="divForms" runat="server" class="divScroll">
            <asp:GridView ID="gvSelectChecks" runat="server" AutoGenerateColumns="false" 
            AllowPaging="false" AllowSorting="true" Width="100%" ShowHeader="true"  EnableViewState="false" 
            EmptyDataText="<%$ Resources:gvNoChecks %>">
          
           
            <PagerStyle CssClass="headertext2"  ForeColor="White"/>
            <HeaderStyle CssClass="colheader6" />
            <AlternatingRowStyle CssClass="data2" /> 
            <FooterStyle  CssClass ="colheader6" />
                <Columns>   
             
                    <asp:BoundField  HeaderText="<%$ Resources:gvHdrCheckDate %>"  DataField="CheckDate" 
                    ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

                    </asp:BoundField>
                    <asp:BoundField   HeaderText="<%$ Resources:gvHdrControl %>"  DataField="ControlNumber"  ControlStyle-Width="20%"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

                    </asp:BoundField>
                               
                    <asp:BoundField   HeaderText="<%$ Resources:gvHdrPayee %>"  DataField="Payee"  ControlStyle-Width="20%"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

                    </asp:BoundField>
                    <asp:BoundField   HeaderText="<%$ Resources:gvHdrClaimNumber %>"  DataField="ClaimNumber"  ControlStyle-Width="20%"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

                    </asp:BoundField>
                
                    <asp:BoundField   HeaderText="<%$ Resources:gvHdrAmount %>"  DataField="Amount"  ControlStyle-Width="20%"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">

                    </asp:BoundField>
               
                </Columns>
            </asp:GridView>            
        </asp:Panel>
        <table>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <td align="right" width="75%" class="finlabel">
                        <asp:Label ID="lblTotalPayAmount" runat="server" Text="<%$ Resources:lblTotalPayAmountResrc %>" /> 
                    </td>
                    <td align="left" width="25%" class="fintotals">
                        <asp:Label id="lblTotal" runat="server"></asp:Label>											
                    </td>
                </td>
            </tr>
            <tr>
            <td>
            <asp:Button ID="btnPrint" UseSubmitBehavior="true"   runat="server" Text="<%$ Resources:btnPrintResrc %>" CssClass="button" onclick="btnPrint_Click"/>
            </td>
            </tr>
        </table>
        <table border="0">
            <tr>
       
                <td>

                </td> 
        
            </tr>
      
        </table>
    </form>
</body>
</html>

