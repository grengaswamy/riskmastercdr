﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoCheckList.aspx.cs" Inherits="Riskmaster.UI.UI.Funds.AutoCheckList" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Payment History</title>
    <link href="../../APP_Themes/rmnet.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
            .GridViewFixedHeader {
                /* So the overflow scrolls */
                overflow:auto;
            }
            .GridViewFixedHeader table th {
                /* Keep the header cells positioned as we scroll */
                /*position:relative; */
                position: relative;
                top: expression(document.getElementById("GridViewFixedHeader").scrollTop-2); 
                z-index: 10;
            }
                .GridViewFixedHeader table tbody {
                /* Keep the header cells positioned as we scroll */
                /*position:relative; */
                overflow-x:hidden;
            }

        </style>
    <script type="text/javascript" language="javascript">
        function BackToPaymentHistory() {
           
            var sClaimId = document.getElementById('ClaimId').value;
            var sClaimantEid = document.getElementById('ClaimantEid').value;
            var sUnitId = document.getElementById('UnitId').value;
            var sUnitRowId = document.getElementById('UnitRowId').value;
            var sCaption = document.getElementById('Caption').value;
            var sEntityId = document.getElementById('EntityId').value; //skhare7 R8 combined Payment 
            // aravi5 RMA-10586 Corprate financial screen is seen for carrier customers when clicked on back button of auto checks screen-CHROME Replaced &amp; with %26 in Query String Starts
            var sCheckMode = document.getElementById('BOBMode').value;
            if (document.getElementById('BOBMode').value != "1") {
                window.location.href = '/RiskmasterUI/UI/Funds/PaymentHistory.aspx?ClaimId=' + sClaimId + '%26ClaimantEid=' + sClaimantEid + '%26UnitID=' + sUnitId + '%26UnitRowID=' + sUnitRowId + '%26Caption=' + sCaption + '%26EntityId=' + sEntityId;
        }
        else {
            window.location.href = '/RiskmasterUI/UI/Funds/PaymentHistory.aspx?ClaimId=' + sClaimId + '%26ClaimantEid=' + sClaimantEid + '%26UnitID=' + sUnitId + '%26UnitRowID=' + sUnitRowId + '%26Caption=' + sCaption + '%26EntityId=' + sEntityId + '%26CheckMode=' + sCheckMode;

        }
        // aravi5 RMA-10586 Corprate financial screen is seen for carrier customers when clicked on back button of auto checks screen-CHROME Replaced &amp; with %26 in Query String Ends
            return false;
        }
    </script>

</head>
<body>
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/AutoCheckList/ClaimId"
            ID="ClaimId" runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="SubTitle" rmxref="/Instance/Document/AutoCheckList/SubTitle"
            runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="ClaimantEid" rmxref="/Instance/Document/AutoCheckList/ClaimantEid"
            runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="UnitId" rmxref="/Instance/Document/AutoCheckList/UnitId"
            runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="UnitRowId" rmxref="/Instance/Document/AutoCheckList/UnitRowId"
            runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="SortCol" rmxref="/Instance/Document/AutoCheckList/SortCol"
            runat="server" ></asp:TextBox>
        <asp:TextBox Style="display: none" ID="Ascending" rmxref="/Instance/Document/AutoCheckList/Ascending"
            runat="server" ></asp:TextBox>
            <asp:TextBox Style="display: none" ID="FormTitle" rmxref="/Instance/Document/AutoCheckList/FormTitle"
            runat="server"></asp:TextBox>
             <asp:TextBox Style="display: none" ID="EntityId" rmxref="/Instance/Document/AutoCheckList/EntityId"
            runat="server" rmxignoreget="true"></asp:TextBox>
        <td>
            <asp:Label runat="server" class="ctrlgroup2" ID="lblcaption" Width="100%"></asp:Label> 
            <asp:TextBox runat="server"  ID="Caption"  style="display:none"></asp:TextBox>    
        </td>
    
        <div id="GridViewFixedHeader" style="height:75%;width:99%;position:relative" class="GridViewFixedHeader">
            <asp:GridView ID="grdAutoCheckList" runat="server" Visible="true" class="GridViewFixedHeader" Width="95%" CellPadding="1"
                RowStyle-HorizontalAlign="Center" OnRowDataBound="grdAutoCheckList_RowDataBound" AllowSorting="true" OnSorting="grdAutoCheckList_Sorting">
            </asp:GridView>
        </div>  
                        
        <table width="99%">
            <tr class="rowdark1">
                <td align="right">
                    <asp:Label ID="lblTotal" runat="server" Text="<%$ Resources:lblTotalResrc %>" />
                    <asp:TextBox ID="TotalAll" rmxref="/Instance/Document/AutoCheckList/Total" runat="server" CssClass="fintotals"></asp:TextBox>
                </td>
            </tr>
        </table>
        <asp:Button runat="server" ID="btnBackToPaymentHistory" OnClientClick="return BackToPaymentHistory();" Text="<%$ Resources:btnBackToPaymentHistoryResrc %>" class="button" />
        <asp:TextBox Style="display: none" ID="BOBMode"  runat="server" rmxignoreget="true"></asp:TextBox>
    </form>
</body>
</html>
