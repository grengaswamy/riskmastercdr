﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.UI.Funds
{
    public partial class PaymentIntervalFrame : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           String sPayIntDays = AppHelper.GetQueryStringValue("PaymentIntDays");
           PayIntDays.Value = sPayIntDays;
        }
    }
}