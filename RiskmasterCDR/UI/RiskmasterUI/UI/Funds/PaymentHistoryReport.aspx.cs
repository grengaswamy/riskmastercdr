﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;

namespace Riskmaster.UI.UI.Funds
{
    public partial class PaymentHistoryReport : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";

            try
            {

                if (IsPostBack)
                {
                    // akaushik5 Commented for MITS 38377 Starts
                    //    ClaimId.Text = AppHelper.GetQueryStringValue("ClaimId");
                    //    ClaimantEid.Text = AppHelper.GetQueryStringValue("ClaimantEid");
                    //    UnitId.Text = AppHelper.GetQueryStringValue("UnitID");
                    //    SortCol.Text = AppHelper.GetQueryStringValue("SortCol");
                    //    //mkaran2- MITS 32340 Start changes
                    //    // akaushik5 Commented for MITS 37344 Starts
                    //    //switch (SortCol.Text)
                    //    //{
                    //    //    case "Name_SQL":
                    //    //            SortCol.Text = " ISNULL(FUNDS.FIRST_NAME + FUNDS.LAST_NAME,FUNDS.LAST_NAME)";
                    //    //            break;
                    //    //    case "Name_Oracle":
                    //    //            SortCol.Text = "nvl(CONCAT(FIRST_NAME,LAST_NAME),LAST_NAME)";                            
                    //    //            break;                                                                                                               
                    //    //    default: break;
                    //    //}
                    //    // akaushik5 Commented for MITS 37344 Ends
                    //    //mkaran2- MITS 32340 End changes   
                    //    Ascending.Text = AppHelper.GetQueryStringValue("Ascending");
                    //    FormTitle.Text = AppHelper.GetQueryStringValue("FormTitle");
                    ////skhare7 r8 combined Payment
                    //EntityId.Text = AppHelper.GetQueryStringValue("EntityId");
                    // akaushik5 Commented for MITS 38377 Ends

                    bReturnStatus = CallCWS("FundManagementAdaptor.CreatePaymentHistoryReport", XmlTemplate, out sCWSresponse, true, false);

                    XmlTemplate = XElement.Parse(sCWSresponse.ToString());

                    XElement oEle = XmlTemplate.XPathSelectElement("//File");

                    if (oEle != null)
                    {
                        string sFileContent = oEle.Value;
                        byte[] byteOrg = Convert.FromBase64String(sFileContent);

                        Response.Clear();
                        Response.Charset = "";
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", "inline;filename=PaymentHistory.pdf");
                        Response.BinaryWrite(byteOrg);
                        Response.End();
                    }

                    ErrorControl1.errorDom = sCWSresponse;
                    DoPostBack.Text = "false";   // MITS MANISH 
                }
                else
                {
                    // akaushik5 Added for MITS 38377 Starts
                    this.ClaimId.Text = AppHelper.GetQueryStringValue("ClaimId");
                    this.ClaimantEid.Text = AppHelper.GetQueryStringValue("ClaimantEid");
                    this.UnitId.Text = AppHelper.GetQueryStringValue("UnitID");
                    this.SortCol.Text = AppHelper.GetQueryStringValue("SortCol");
                    this.Ascending.Text = AppHelper.GetQueryStringValue("Ascending");
                    this.FormTitle.Text = AppHelper.GetQueryStringValue("FormTitle");
                    this.EntityId.Text = AppHelper.GetQueryStringValue("EntityId");
                    // akaushik5 Added for MITS 38377 Ends
                    DoPostBack.Text = "true";  // MITS MANISH 
                }
            }
            catch (Exception ee)
            {
                DoPostBack.Text = "false";
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
