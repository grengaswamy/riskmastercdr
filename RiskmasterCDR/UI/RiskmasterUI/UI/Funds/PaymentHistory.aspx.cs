﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 05/23/2014 | 34270   | aahuja21   | Added check memo in payment history grid
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Data;
using System.Collections;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Riskmaster.Common;
using System.Globalization;
using System.Threading;
using Riskmaster.RMXResourceManager;
using System.Text;

namespace Riskmaster.UI.UI.Funds
{
    public partial class PaymentHistory : NonFDMBasePageCWS
    {
        string sCWSresponse = string.Empty;
        private XElement XmlTemplate = null;
        private int iRows = 0;
        //rupal:start, for first & final payment
        bool bCarrierClaims = false;
        // int iIDX_FFPMT = 16;//index for the coulumn "first & Final payment"
        //int iIDX_FFPMT = 18;//mbahl3 Jira[RMA-356] // rkaur27 - reference by unique name and not index
        //rupal:end, for first & final payment
        // Manish payment variables to show Footer
        // akaushik5 Commented for MITS 31151 Starts
        // double dtotPay=0, dnetPay=0, dtotColl=0, dtotVoid=0;
        // akaushik5 Commented for MITS 31151 Ends
        DateTimeFormatInfo currentDateTimeFormat = null;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("PaymentHistory.aspx"), "PaymentHistoryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "PaymentHistoryValidationScripts", sValidationResources, true);

                if (!Page.IsPostBack)
                {
                    ClaimId.Text = AppHelper.GetQueryStringValue("ClaimId");
                    ClaimantEid.Text = AppHelper.GetQueryStringValue("ClaimantEid");
                    ClaimantRowId.Text = AppHelper.GetQueryStringValue("ClaimantRowId");
                    UnitId.Text = AppHelper.GetQueryStringValue("UnitID");
                    if ((UnitId.Text == "") || (UnitId.Text == "0"))
                    {
                        UnitId.Text = AppHelper.GetQueryStringValue("UnitEID");
                    }
                    UnitRowId.Text = AppHelper.GetQueryStringValue("UnitRowId");
                    ClaimNumber.Text = AppHelper.GetQueryStringValue("ClaimNumber");
                    FrozenFlag.Text = AppHelper.GetQueryStringValue("FrozenFlag");
                    SubTitle.Text = AppHelper.GetQueryStringValue("SubTitle");
                    //skhare7 r8 combined Payment
                    EntityId.Text = AppHelper.GetQueryStringValue("EntityId");
                    if (EntityId.Text == "0" || EntityId.Text == "")
                    {
                        btnBackToEntity.Visible = false;
                        //ddhiman : MITS 27009
                        btnBackToFinancials.Visible = true;
                        //End ddhiman
                    }
                    else
                    {
                        btnBackToEntity.Visible = true;
                        //ddhiman : MITS 27009
                        btnBackToFinancials.Visible = false;
                        //End ddhiman
                    }
                    //pgupta93: RA-4608 START
                    if (!(string.IsNullOrEmpty(AppHelper.GetQueryStringValue("FormName"))))
                    {
                        hdFDHButton.Value = AppHelper.GetQueryStringValue("FormName");
                    }

                    //pgupta93: RA-4608 END

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }       
        
        /// <summary>//Neha
        /// The event is called on when  grid is loaded or on postback of grid.
        ///   /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 27 Sept 2011     Changes made to    Manish Jain
        ///                  add footer in 
        ///                  Radgrid
        ///************************************************************
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void grdPaymentHistory_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            StringBuilder sbFilterExpression = new StringBuilder();
            //ANKIT MITS 29909 start changes
            foreach (GridColumn item in grdPaymentHistory.MasterTableView.Columns)
            {
                string filterFunction = item.CurrentFilterFunction.ToString();
                string filterValue = item.CurrentFilterValue;
                string filterColumn = item.UniqueName.ToString();
                // akaushik5 Changed for Performance Improvement Starts
                // if ((filterValue != "") & (filterFunction != "Between"))
                if (!string.IsNullOrEmpty(filterValue))
                {
                    if (!filterFunction.Equals("Between"))
                    // akaushik5 Changed for Performance Improvement Ends
                    {
                        if (sbFilterExpression.ToString() != "")
                            sbFilterExpression.Append(" AND ");
                        sbFilterExpression.Append("([");
                        sbFilterExpression.Append(filterColumn);
                        sbFilterExpression.Append("] ");
                        switch (filterFunction)
                        {
                            case "GreaterThan": filterFunction = ">"; break;
                            case "LessThan": filterFunction = "<"; break;
                            case "NotEqualTo": filterFunction = "<>"; break;
                            case "&lt;&gt;": filterFunction = "<>"; break;
                            case "GreaterThanOrEqualTo": filterFunction = ">="; break;
                            case "LessThanOrEqualTo": filterFunction = "<="; break;
                            case "Contains": filterFunction = "LIKE";
                                filterValue = "%" + filterValue + "%";
                                break;
                            case "DoesNotContain": filterFunction = "NOT LIKE";
                                filterValue = "%" + filterValue + "%";
                                break;
                            case "EqualTo": filterFunction = "="; break;
                            case "StartsWith": filterFunction = "LIKE";
                                filterValue = "" + filterValue + "%";
                                break;
                            case "EndsWith": filterFunction = "LIKE";
                                filterValue = "%" + filterValue + "";
                                break;
                            case "IsEmpty": filterFunction = "=";
                                filterValue = "";
                                break;
                            case "NotIsEmpty": filterFunction = "<>"; break;
                            default: break;
                        }
                        sbFilterExpression.Append(filterFunction);
                        sbFilterExpression.Append(" '");
                        //Praveen ML Change- Get formated Date filterValue for Dates
                        // if (item.UniqueName.Equals("FromDate") || item.UniqueName.Equals("ToDate") || item.UniqueName.Equals("CheckDate"))
                        if (item.UniqueName.Equals("FromDate") || item.UniqueName.Equals("ToDate") || item.UniqueName.Equals("CheckDate") || (item.UniqueName.Equals("ClearVoidFlagedFlag") && filterValue.Length > 6)) //35247
                            filterValue = AppHelper.GetRMDate(filterValue);
                        //Praveen ML Change
						//rkulavil :RMA-12789/MITS 38521 starts
                        //sbFilterExpression.Append(filterValue.ToLower());
                        sbFilterExpression.Append(filterValue.Replace("'","''"));
						//rkulavil :RMA-12789/MITS 38521 ends
                        sbFilterExpression.Append("')");
                    }
                    //if ((filterFunction == "IsEmpty") || (filterFunction == "IsNotEmpty"))
                    //{
                    //    if (sbFilterExpression.ToString() != "")
                    //        sbFilterExpression.Append(" AND ");
                    //    sbFilterExpression.Append("([");
                    //    sbFilterExpression.Append(filterColumn);
                    //    sbFilterExpression.Append("] ");
                    //        if (filterFunction == "IsEmpty")
                    //            filterFunction = "=";
                    //        else
                    //            filterFunction = "<>";
                    //        filterValue = "";                    
                    //    sbFilterExpression.Append(filterFunction);
                    //    sbFilterExpression.Append(" '");
                    //    sbFilterExpression.Append(filterValue.ToLower());
                    //    sbFilterExpression.Append("')");
                    //}
                    // akaushik5 Changed for Performance Improvement Starts
                    // if ((filterFunction == "Between") & (filterValue != ""))
                    if (filterFunction.Equals("Between"))
                    // akaushik5 Changed for Performance Improvement Ends
                    {
                        if (sbFilterExpression.ToString() != "")
                            sbFilterExpression.Append(" AND ");
                        string[] strs = filterValue.Split(' ');  //Praveen ML Chnages- get split values of fromdate and todate in TransDate                      
                        sbFilterExpression.Append("([");
                        sbFilterExpression.Append(filterColumn);
                        sbFilterExpression.Append("] ");
                        sbFilterExpression.Append(">= '");
                        sbFilterExpression.Append(AppHelper.GetRMDate(strs[0])); //get fromdate value in TransDate 
                        sbFilterExpression.Append("') AND ");
                        sbFilterExpression.Append("([");
                        sbFilterExpression.Append(filterColumn);
                        sbFilterExpression.Append("] ");
                        sbFilterExpression.Append("<= '");
                        sbFilterExpression.Append(AppHelper.GetRMDate(strs[1])); //todate value in TransDate 
                        sbFilterExpression.Append("')");
                    }
                    // akaushik5 Added for Performance Improvement Starts
                }
                // akaushik5 Added for Performance Improvement Ends
            }

            txtNextPageNum.Text = grdPaymentHistory.CurrentPageIndex.ToString();
            //txtFilterValue.Text = grdPaymentHistory.MasterTableView.FilterExpression.ToString();            
            txtFilterValue.Text = sbFilterExpression.ToString();
            ViewState["FilterValue"] = txtFilterValue.Text;
            grdPaymentHistory.MasterTableView.FilterExpression = "";
            if (ViewState["SortColumn"] != null)
                SortCol.Text = ViewState["SortColumn"].ToString();
            if (ViewState["SortOrder"] != null)
                Ascending.Text = ViewState["SortOrder"].ToString();
            //Ankit MITS 29909-End changes

            // rrachev MITS 36825 Begin changes
            //if (String.IsNullOrEmpty(SortCol.Text))
            //{
            //    // Set default order
            //    SortCol.Text = "CtlNumber";
            //    Ascending.Text = "false";
            //}	
            // rrachev MITS 36825 End changes

            CallCWS("FundManagementAdaptor.GetPaymentHistory", XmlTemplate, out sCWSresponse, true, true);
            BindDataToGrid(sCWSresponse);

            //MITS 26602 MANISH
            myHiddenVar.Value = grdPaymentHistory.MasterTableView.FilterExpression;
            // akaushik5 Commented for MITS 31151 Starts
            //dtotPay = dnetPay + dtotColl;
            //TotalAll.Text = string.Format("{0:C}", Math.Round(dnetPay, 3));
            //TotalCollect.Text = string.Format("{0:C}", Math.Round(dtotColl, 3));
            //TotalPay.Text = string.Format("{0:C}", Math.Round(dtotPay, 3));
            //TotalVoid.Text = string.Format("{0:C}", Math.Round(dtotVoid, 3));
            // akaushik5 Commented for MITS 31151 Ends

            XmlDocument objReturnXml = new XmlDocument();
            objReturnXml.LoadXml(sCWSresponse);
            string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
            if (sMsgStatus == "Success")
            {
                //lblcaption.Text = "Payment History (" + Caption.Text + ")";
                lblcaption.Text = GetResourceValue("lblcaptionResrc", "0") + " (" + Caption.Text + ")";
            }
            CustomizePage();

        }     
        protected void grdPaymentHisory_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridItem)
            {
                //Deb : ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                RadDatePicker txtToOrderDatePicker = ((GridItem)e.Item).FindControl("txtToOrderDatePicker") as RadDatePicker;
                if (txtToOrderDatePicker != null)
                {
                    txtToOrderDatePicker.Culture = new CultureInfo(sCulture);
                }
                RadDatePicker txtFromOrderDatePicker = ((GridItem)e.Item).FindControl("txtFromOrderDatePicker") as RadDatePicker;
                if (txtFromOrderDatePicker != null)
                {
                    txtFromOrderDatePicker.Culture = new CultureInfo(sCulture);
                }
                RadDatePicker txtFromPicker = ((GridItem)e.Item).FindControl("txtFromPicker") as RadDatePicker;
                if (txtFromPicker != null)
                {
                    txtFromPicker.Culture = new CultureInfo(sCulture); ;
                }
                RadDatePicker txtToPicker = ((GridItem)e.Item).FindControl("txtToPicker") as RadDatePicker;
                if (txtToPicker != null)
                {
                    txtToPicker.Culture = new CultureInfo(sCulture);
                }
                RadDatePicker txtCheckDatePicker = ((GridItem)e.Item).FindControl("txtCheckDatePicker") as RadDatePicker;
                if (txtCheckDatePicker != null)
                {
                    txtCheckDatePicker.Culture = new CultureInfo(sCulture);
                }
                //Deb : ML Changes
            }
        }
        protected void grdPaymentHistory_ItemDataBound(object sender, GridItemEventArgs e)//Changed the ID of the Grid
        {

            //Added by Amitosh to hide the pager combobox from the pagertemplate   
            if (e.Item is GridPagerItem)
            {
                GridPagerItem pager = (GridPagerItem)e.Item;
                Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                lbl.Visible = false;

                RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                combo.Visible = false;
            }
            //end Amitosh

            ////Start averma62 MITS 26999
            //Ashish Ahuja Mits 34270 to display ToolTip Starts
            if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
            {
                GridDataItem gdi = (GridDataItem)e.Item; //verify this and apply below

                Control targetEntity = e.Item.FindControl("btnEntity");
                if (!Object.Equals(targetEntity, null))
                {
                    //Add the button (target) id to the tooltip manager
                    if (!String.IsNullOrEmpty(gdi["CtlNumber"].Text) && gdi["CtlNumber"].Text != "\"" && gdi["TransId"].Text != "0")
                        targetEntity.Visible = true;
                }

                Control targetCheckMemo = e.Item.FindControl("btnCheckMemo");
                Label labelCheckMemo = (Label)e.Item.FindControl("lblCheckMemo");

                if (!Object.Equals(targetCheckMemo, null))
                {
                    //Add the button (target) id to the tooltip manager
                    if (!String.IsNullOrEmpty(gdi["CtlNumber"].Text) && gdi["CtlNumber"].Text != "\"" && gdi["LossType"].Text != "0" && labelCheckMemo.Text != "")
                        targetCheckMemo.Visible = true;

                }

            }
            //End averma62 MITS 26999
            //Ashish Ahuja Mits 34270 to display ToolTip Ends
            //Ashish Ahuja Mits 34270 Changed Indexes
            if (e.Item is GridDataItem)
            {
                GridDataItem gd = (GridDataItem)e.Item;
                if (!String.IsNullOrEmpty(gd["CtlNumber"].Text) && gd["CtlNumber"].Text != "&quot;")
                {
                    gd["CtlNumber"].Text = "<a href='/RiskmasterUI/UI/FDM/Funds.aspx?TransId=" + gd["TransId"].Text + "&SysCmd=0" + "'>" + gd["CtlNumber"].Text + "</a>";
                }
                if (!String.IsNullOrEmpty(gd["ManualCheck"].Text) && gd["ManualCheck"].Text != "&quot;")
                {
                    string sTemp = gd["Offset"].Text.ToString();
                    string sTempVoid = gd["VoidFlag"].Text.ToString();
                    string sTempAmount = gd["Amount"].Text.ToString();
                    string sTempType = gd["PaymentFlag"].Text.ToString();

                    sTempAmount = sTempAmount.Replace("$", "");
                    sTempAmount = sTempAmount.Replace(",", "");
                    if ((sTempVoid.ToLower() != "no") || (sTempType != "Payment") || (Conversion.ConvertStrToDouble(sTempAmount) < 0))
                    {
                        gd["Offset"].Text = "NA";
                    }
                    else if (sTemp.ToLower() == "sms")
                    {
                        sTemp = "0";
                        gd["Offset"].Text = "<a href='#' onclick='OpenOffset(" + sTemp + ");' >No</a>";
                    }
                    else if (sTemp.ToLower() == "false")
                    {
                        string sTransID = gd["TransId"].Text;
                        gd["Offset"].Text = "<a href='#' onclick='OpenOffset(" + sTransID + ");' >No</a>";
                    }
                    else
                    {
                        gd["Offset"].Text = "Yes";
                    }
                }

                // rkaur27 : Duplicacy removed. Correctly handled in grid prerender
                ////rupal:start, for first & final payment
                //if (!bCarrierClaims)
                //{
                //    grdPaymentHistory.Columns[iIDX_FFPMT].Visible = false;
                //    //Added by Amitosh for R8 enhancement adding columns to grid
                //    grdPaymentHistory.Columns[20].Visible = false;
                //    grdPaymentHistory.Columns[21].Visible = false;
                //    grdPaymentHistory.Columns[22].Visible = false;
                //    grdPaymentHistory.Columns[23].Visible = false;
                //}
                ////rupal:end, for first & final payment
            }
            //End Amitosh

        }
        protected void grdPaymentHistory_PreRender(object sender, EventArgs e)
        {
            GridFilterMenu menu = grdPaymentHistory.FilterMenu;
            int i = 0;
            while (i < menu.Items.Count)
            {
                //Ankit-MITS 29909-Start Changes
                //if (menu.Items[i].Text == "Between" || menu.Items[i].Text == "NotBetween" || menu.Items[i].Text == "IsNull" || menu.Items[i].Text == "NotIsNull")
                if (menu.Items[i].Text == "Between" || menu.Items[i].Text == "NotBetween" || menu.Items[i].Text == "IsNull" || menu.Items[i].Text == "NotIsNull" || menu.Items[i].Text == "IsEmpty" || menu.Items[i].Text == "NotIsEmpty")
                //ANkit-MITS 29909-End Changes
                {
                    menu.Items.RemoveAt(i);
                }
                else
                {
                    i = (i + 1);
                }
            }
            //igupta3 Mits# 34515
            SetHeaderConfig();

            // rkaur27 - corrected column show/hide handling
            //Added by Amitosh to remove header item when carrier claim is off
            if (!bCarrierClaims)
            {
                grdPaymentHistory.Columns.FindByUniqueName("IsFirstFinal").Visible = false;
                grdPaymentHistory.Columns.FindByUniqueName("PolicyName").Visible = false;
                grdPaymentHistory.Columns.FindByUniqueName("CoverageType").Visible = false;
                grdPaymentHistory.Columns.FindByUniqueName("Unit").Visible = false;
                grdPaymentHistory.Columns.FindByUniqueName("LossType").Visible = false;
            }

            //rkaur27 - moved from page_prerender to correct function
			//rkulavil :RMA-12789/MITS 38521 starts
            //grdPaymentHistory.Columns.FindByUniqueName("TransNumber").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
            //grdPaymentHistory.Columns.FindByUniqueName("TransDate").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
		
            grdPaymentHistory.Columns.FindByUniqueName("PaymentFlag").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
            grdPaymentHistory.Columns.FindByUniqueName("ClearedFlag").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
			
            //grdPaymentHistory.Columns.FindByUniqueName("CodeDesc").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
            //grdPaymentHistory.Columns.FindByUniqueName("SplitAmount").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
            grdPaymentHistory.Columns.FindByUniqueName("VoidFlag").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
            grdPaymentHistory.Columns.FindByUniqueName("StopPayFlag").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
            grdPaymentHistory.Columns.FindByUniqueName("CheckStatus").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
            grdPaymentHistory.Columns.FindByUniqueName("IsFirstFinal").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
            grdPaymentHistory.Columns.FindByUniqueName("CombinedPay").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
            grdPaymentHistory.Columns.FindByUniqueName("ManualCheck").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
            grdPaymentHistory.Columns.FindByUniqueName("Offset").CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
			//rkulavil :RMA-12789/MITS 38521 ends
            grdPaymentHistory.Rebind(); // psharma206 - rebinding required to implement header config.
        }
        protected void grdPaymentHistory_Sorting(object source, GridSortCommandEventArgs e)
        {
            //MITS 29909 STart changes-Ankit
            grdPaymentHistory.CurrentPageIndex = 0; //Set current page to zero so that upon sorting, the grid automatically displays the first page.
            txtNextPageNum.Text = grdPaymentHistory.CurrentPageIndex.ToString();
            SortCol.Text = e.SortExpression.ToString();
            ViewState["SortColumn"] = SortCol.Text;
            if (ViewState["FilterValue"] != null)
                txtFilterValue.Text = ViewState["FilterValue"].ToString();
            if (e.NewSortOrder == GridSortOrder.Ascending)
                Ascending.Text = "True";
            else if (e.NewSortOrder == GridSortOrder.Descending)
                Ascending.Text = "false";
            ViewState["SortOrder"] = Ascending.Text;
            //MITS 29909 End changes

            CallCWS("FundManagementAdaptor.GetPaymentHistory", XmlTemplate, out sCWSresponse, true, true);
            BindDataToGrid(sCWSresponse);

            //MITS 26602 MANISH
            myHiddenVar.Value = grdPaymentHistory.MasterTableView.FilterExpression;
            // akaushik5 Commented for MITS 31151 Starts
            //dtotPay = dnetPay + dtotColl;
            //TotalAll.Text = string.Format("{0:C}", Math.Round(dnetPay, 3));
            //TotalCollect.Text = string.Format("{0:C}", Math.Round(dtotColl, 3));
            //TotalPay.Text = string.Format("{0:C}", Math.Round(dtotPay, 3));
            //TotalVoid.Text = string.Format("{0:C}", Math.Round(dtotVoid, 3));
            // akaushik5 Commented for MITS 31151 Ends

            XmlDocument objReturnXml = new XmlDocument();
            objReturnXml.LoadXml(sCWSresponse);
            string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
            if (sMsgStatus == "Success")
            {
                lblcaption.Text = GetResourceValue("lblcaptionResrc", "0") + " (" + Caption.Text + ")";

            }
            CustomizePage();

        }        
        protected void grdPaymentHistory_ItemCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                StringBuilder sbFilterExpression = null;
                string sFilterExp;
                if (e.CommandName == RadGrid.FilterCommandName)
                {
                    Pair filterPair = (Pair)e.CommandArgument;
                    switch (filterPair.Second.ToString())
                    {
                        case "TransDate":
                            //Praveen ML Changes -overwrite Ankit MITS 29909 changes                          
                            this.startDate = ((e.Item as GridFilteringItem)[filterPair.Second.ToString()].FindControl("txtFromOrderDatePicker") as RadDatePicker).SelectedDate;
                            this.endDate = ((e.Item as GridFilteringItem)[filterPair.Second.ToString()].FindControl("txtToOrderDatePicker") as RadDatePicker).SelectedDate;
                            //Praveen ML Changes 
                            break;
                        //Ankit MITS 29909 Start changes
                        case "CheckDate":
                            this.checkDate = ((e.Item as GridFilteringItem)[filterPair.Second.ToString()].FindControl("txtCheckDatePicker") as RadDatePicker).SelectedDate;
                            break;
                        case "FromDate":
                            this.fsFromDate = ((e.Item as GridFilteringItem)[filterPair.Second.ToString()].FindControl("txtFromPicker") as RadDatePicker).SelectedDate;
                            break;
                        case "ToDate":
                            this.fsToDate = ((e.Item as GridFilteringItem)[filterPair.Second.ToString()].FindControl("txtToPicker") as RadDatePicker).SelectedDate;
                            break;
                        //Ankit MITS 29909 ENd changes
                        default:
                            break;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        protected void grdPaymentHisory_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            txtNextPageNum.Text = (e.NewPageIndex + 1).ToString();
            if (ViewState["SortColumn"] != null)
                SortCol.Text = ViewState["SortColumn"].ToString();
            if (ViewState["SortOrder"] != null)
                Ascending.Text = ViewState["SortOrder"].ToString();
            CallCWS("FundManagementAdaptor.GetPaymentHistory", XmlTemplate, out sCWSresponse, true, true);
            BindDataToGrid(sCWSresponse);

            //MITS 26602 MANISH
            myHiddenVar.Value = grdPaymentHistory.MasterTableView.FilterExpression;
            // akaushik5 Commented for MITS 31151 Starts
            //dtotPay = dnetPay + dtotColl;
            //TotalAll.Text = string.Format("{0:C}", Math.Round(dnetPay, 3));
            //TotalCollect.Text = string.Format("{0:C}", Math.Round(dtotColl, 3));
            //TotalPay.Text = string.Format("{0:C}", Math.Round(dtotPay, 3));
            //TotalVoid.Text = string.Format("{0:C}", Math.Round(dtotVoid, 3));
            // akaushik5 Commented for MITS 31151 Ends

            XmlDocument objReturnXml = new XmlDocument();
            objReturnXml.LoadXml(sCWSresponse);
            string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
            if (sMsgStatus == "Success")
            {

                lblcaption.Text = GetResourceValue("lblcaptionResrc", "0") + " (" + Caption.Text + ")";
            }
            CustomizePage();
        }

        /// <summary>//Manish
        /// This function  is called to bind radgrid.       
        /// </summary>
        private void BindDataToGrid(string sCWSresponse)
        {
            try
            {
                XElement objPaymentHistory = XElement.Parse(sCWSresponse);
                // start dvatsa - throw error and skip grid binding
                if ((objPaymentHistory.XPathSelectElement("//MsgStatus/MsgStatusCd") != null) && (objPaymentHistory.XPathSelectElement("//MsgStatus/MsgStatusCd").Value == "Error"))
                {
                	ErrorControl1.errorDom = sCWSresponse;
                    return;
                }
                // end dvatsa - throw error and skip grid binding

                //rupal:start, for first & final payment
                //Manish for multi currency MITS 27209
                if (objPaymentHistory.XPathSelectElement("//BaseCurrency") != null)
                {
                    string sBaseCurr = objPaymentHistory.XPathSelectElement("//BaseCurrency").Value;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(sBaseCurr);
                }

                if (objPaymentHistory.XPathSelectElement("//CarrierClaims") != null)
                {
                    if (objPaymentHistory.XPathSelectElement("//CarrierClaims").Value == "-1")
                        bCarrierClaims = true;
                    else
                        bCarrierClaims = false;
                }

                //rupal:end, for first & final payment
                //Ankit MITS 29834:Start Changes                
                if (objPaymentHistory.XPathSelectElement("//PageSize") != null)
                    grdPaymentHistory.PageSize = Conversion.ConvertStrToInteger(objPaymentHistory.XPathSelectElement("//PageSize").Value);
                else
                    grdPaymentHistory.PageSize = 0;
                if (objPaymentHistory.XPathSelectElement("//VirtualItemCount") != null)
                    grdPaymentHistory.VirtualItemCount = Conversion.ConvertStrToInteger(objPaymentHistory.XPathSelectElement("//VirtualItemCount").Value);
                else
                    grdPaymentHistory.VirtualItemCount = 0;
                //Ankit MITS 29834:End Changes
                if (objPaymentHistory.XPathSelectElements("//row") != null)
                {
                    grdPaymentHistory.HeaderStyle.CssClass = "colheader3";
                    grdPaymentHistory.HeaderStyle.ForeColor = System.Drawing.Color.White;

                    DataRow drPaymentHistory = null;
                    DataTable dtPaymentHistory = new DataTable();
                    DataTable dtfiltrPaymentHistory = new DataTable(); // MITS Manish data table for filter values

                    IEnumerable result = null;

                    dtPaymentHistory.Columns.Add("CtlNumber");
                    dtPaymentHistory.Columns.Add("TransNumber");
                    dtPaymentHistory.Columns.Add("TransDate");
                    dtPaymentHistory.Columns.Add("PaymentFlag");
                    dtPaymentHistory.Columns.Add("ClearedFlag");
                    dtPaymentHistory.Columns.Add("VoidFlag");
                    dtPaymentHistory.Columns.Add("StopPayFlag"); //Added by Swati Agarwal MITS # 33431
                    dtPaymentHistory.Columns.Add("CheckStatus");
                    dtPaymentHistory.Columns.Add("Name");
                    dtPaymentHistory.Columns.Add("Amount");
                    dtPaymentHistory.Columns.Add("FromDate");
                    dtPaymentHistory.Columns.Add("ToDate");
                    dtPaymentHistory.Columns.Add("InvoiceNumber");
                    dtPaymentHistory.Columns.Add("CodeDesc");
                    dtPaymentHistory.Columns.Add("SplitAmount");
                    dtPaymentHistory.Columns.Add("User");
                    dtPaymentHistory.Columns.Add("CheckDate");
                    dtPaymentHistory.Columns.Add("IsFirstFinal");
                    //skhare7 R8 Enhancement Conmbined Pay
                    dtPaymentHistory.Columns.Add("CombinedPay");
                    dtPaymentHistory.Columns.Add("PolicyName");
                    dtPaymentHistory.Columns.Add("CoverageType");
                    dtPaymentHistory.Columns.Add("Unit");
                    dtPaymentHistory.Columns.Add("LossType");
                    //skhare7 R8 end
                    dtPaymentHistory.Columns.Add("TransId");
                    dtPaymentHistory.Columns.Add("Payees_Name"); //averma62 MITS 26999
                    //Manish for multi currency MITS 27209
                    dtPaymentHistory.Columns.Add("DAmount");
                    dtPaymentHistory.Columns.Add("DSplitAmount");
                    dtPaymentHistory.Columns.Add("NoOfPayees");
                    //MITS MANISH
                    //Ankit Start : Financials Enhancements - Manual Check / Cleared, Void Date Changes 
                    dtPaymentHistory.Columns.Add("ManualCheck");
                    //Ankit End
                    //Ankit Start : MITS 31315
                    dtPaymentHistory.Columns.Add("IsCleared");
                    dtPaymentHistory.Columns.Add("IsVoided");
                    //Ankit End
                    //mcapps2 MITS 30236 Start
                    dtPaymentHistory.Columns.Add("Offset");
                    //mcapps2 MITS 30236 End

                    //Ashish Ahuja Mits 34270
                    dtPaymentHistory.Columns.Add("CheckMemo");
                    dtPaymentHistory.Columns.Add("CheckMemoToolTip");
                    dtPaymentHistory.Columns.Add("InvoiceAmount");

                    //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    dtPaymentHistory.Columns.Add("CheckTotal");
                    //End - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    dtPaymentHistory.Columns.Add("HoldReason");// JIRA 7810  Snehal

                    dtPaymentHistory.Columns.Add("ReserveType");
                    result = from row in objPaymentHistory.XPathSelectElements("//row")
                             select row;
                    foreach (XElement item in result)
                    {
                        drPaymentHistory = dtPaymentHistory.NewRow();

                        drPaymentHistory["CtlNumber"] = item.XPathSelectElement("CtlNumber").Value;//0
                        drPaymentHistory["TransNumber"] = item.XPathSelectElement("TransNumber").Value;//1
                        drPaymentHistory["TransDate"] = AppHelper.GetDate(item.XPathSelectElement("TransDate").Value);//2
                        drPaymentHistory["PaymentFlag"] = item.XPathSelectElement("PaymentFlag").Value;//3
                        //Ankit Start : MITS 31315
                        if (item.XPathSelectElement("IsCleared") != null && Common.Conversion.ConvertObjToBool(item.XPathSelectElement("IsCleared").Value))
                            drPaymentHistory["ClearedFlag"] = AppHelper.GetDate(item.XPathSelectElement("VoidDate").Value);//4
                        else
                            drPaymentHistory["Clearedflag"] = item.XPathSelectElement("ClearedFlag").Value;
                        if (item.XPathSelectElement("IsVoided") != null && Common.Conversion.ConvertObjToBool(item.XPathSelectElement("IsVoided").Value))
                            drPaymentHistory["VoidFlag"] = AppHelper.GetDate(item.XPathSelectElement("VoidDate").Value);//5
                        else
                            drPaymentHistory["VoidFlag"] = item.XPathSelectElement("VoidFlag").Value;
                        //Ankit End

                        //Added by Swati agarwal MITS# 33431
                        if (item.XPathSelectElement("IsStopPayed") != null && Common.Conversion.ConvertObjToBool(item.XPathSelectElement("IsStopPayed").Value))
                            drPaymentHistory["StopPayFlag"] = AppHelper.GetDate(item.XPathSelectElement("StopPayDate").Value);//6
                        else
                            drPaymentHistory["StopPayFlag"] = item.XPathSelectElement("StopPayFlag").Value;
                        //end by Swati
                        //numbering of the columns changed (increased by 1) as the new column has been inserted by Swati
                        drPaymentHistory["CheckStatus"] = item.XPathSelectElement("CheckStatus").Value;//7
                        drPaymentHistory["Name"] = item.XPathSelectElement("Name").Value;//8
                        drPaymentHistory["Amount"] = item.XPathSelectElement("Amount").Value;  //MITS MANISH//9
                        drPaymentHistory["FromDate"] = AppHelper.GetDate(item.XPathSelectElement("FromDate").Value);//10
                        drPaymentHistory["ToDate"] = AppHelper.GetDate(item.XPathSelectElement("ToDate").Value);//11
                        drPaymentHistory["InvoiceNumber"] = item.XPathSelectElement("InvoiceNumber").Value;//12
                        drPaymentHistory["CodeDesc"] = item.XPathSelectElement("CodeDesc").Value;//13
                        drPaymentHistory["SplitAmount"] = item.XPathSelectElement("SplitAmount").Value;  //MITS MANISH;//14
                        drPaymentHistory["User"] = item.XPathSelectElement("User").Value;//15
                        drPaymentHistory["CheckDate"] = AppHelper.GetDate(item.XPathSelectElement("CheckDate").Value);//16
                        //start:rupal,for first & final payment
                        drPaymentHistory["IsFirstFinal"] = item.XPathSelectElement("IsFirstFinal").Value;//17
                        //skhare7 r8 enhancement 
                        drPaymentHistory["CombinedPay"] = item.XPathSelectElement("CombinedPay").Value;//18
                        if (bCarrierClaims)
                        {
                            drPaymentHistory["PolicyName"] = item.XPathSelectElement("PolicyName").Value;//19
                            drPaymentHistory["CoverageType"] = item.XPathSelectElement("CoverageType").Value;//20
                            drPaymentHistory["Unit"] = item.XPathSelectElement("Unit").Value;//21
                            drPaymentHistory["LossType"] = item.XPathSelectElement("LossType").Value;//22
                        }
                        else
                        {
                            drPaymentHistory["PolicyName"] = string.Empty;//19
                            drPaymentHistory["CoverageType"] = string.Empty;//20
                            drPaymentHistory["Unit"] = string.Empty;//21
                            drPaymentHistory["LossType"] = string.Empty;//22
                        }
                        drPaymentHistory["TransId"] = item.XPathSelectElement("CtlNumber").Attribute("TransId").Value;//23
                        //end:rupal,for first & final payment

                        drPaymentHistory["Payees_Name"] = item.XPathSelectElement("Payees_Name").Value;  //averma62 MITS 26999//24
                        //Manish for multi currency MITS 27209
                        drPaymentHistory["DAmount"] = item.XPathSelectElement("DAmount").Value;//25
                        drPaymentHistory["DSplitAmount"] = item.XPathSelectElement("DSplitAmount").Value;//26
                        drPaymentHistory["NoOfPayees"] = item.XPathSelectElement("NoOfPayees").Value;//27
                        //Ankit Start : Financials Enhancements - Manual Check Changes
                        drPaymentHistory["ManualCheck"] = item.XPathSelectElement("ManualCheck").Value;//28
                        //Ankit End
                        //Ankit Start : MITS 31315
                        drPaymentHistory["IsCleared"] = item.XPathSelectElement("IsCleared").Value;//29
                        drPaymentHistory["IsVoided"] = item.XPathSelectElement("IsVoided").Value;//30
                        //Ankit End
                        //mcapps2 MITS 30236 Start
                        drPaymentHistory["Offset"] = item.XPathSelectElement("Offset").Value;//31
                        //mcapps2 MITS 30236 End


                        //Ashish Ahuja Mits 34270 start
                        string checkMemo = item.XPathSelectElement("CheckMemo").Value;
                        if (checkMemo.Length > 30)
                        {
                            checkMemo = checkMemo.Substring(0, 30) + "...";
                        }
                        drPaymentHistory["CheckMemo"] = checkMemo;//32
                        string checkMemoToolTip = item.XPathSelectElement("CheckMemo").Value;
                        if (checkMemoToolTip.Length > 247)
                        {
                            checkMemoToolTip = checkMemoToolTip.Substring(0, 247) + "...";
                        }
                        drPaymentHistory["CheckMemoToolTip"] = checkMemoToolTip;//33
                        //Ashish Ahuja Mits 34270 end
                        drPaymentHistory["InvoiceAmount"] = item.XPathSelectElement("InvoiceAmount").Value;//34

                        //Start - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                        drPaymentHistory["CheckTotal"] = item.XPathSelectElement("CheckTotal").Value;//35
                        //End - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                        drPaymentHistory["HoldReason"] = item.XPathSelectElement("HoldReason").Value;//JIRA 7810  Snehal
                        drPaymentHistory["ReserveType"] = item.XPathSelectElement("ReserveType").Value;
                        dtPaymentHistory.Rows.Add(drPaymentHistory);
                        iRows++;
                    }

                    //Deb MITS 30185
                    if (objPaymentHistory.XPathSelectElement("//Config") != null)
                    {
                        ViewState["PayHistHeaderConfig"] = objPaymentHistory.XPathSelectElement("//Config").Value;
                        // akaushik5 Commented for Performance Improvement Starts
                        // SetHeaderConfig();
                        // akaushik5 Commented for Performance Improvement Ends
                    }
                    //Deb MITS 30185
                    grdPaymentHistory.DataSource = dtPaymentHistory;
                    //Ankit MITS 29909-Start changes:Commenting code as filtering code has been explicitly written in DB layer
                    //MITS MANISH to check Filter expression
                    //Ankit MITS 29909:End changes
                    // akaushik5 Changed for MITS 31151 Starts
                    //SetFooterData(dtPaymentHistory);
                    this.TotalAll.Text = this.GetCurrencyFromXMLNode(objPaymentHistory.XPathSelectElement("//TotalPay"));
                    this.TotalCollect.Text = this.GetCurrencyFromXMLNode(objPaymentHistory.XPathSelectElement("//TotalCollect"));
                    this.TotalPay.Text = this.GetCurrencyFromXMLNode(objPaymentHistory.XPathSelectElement("//TotalAll"));
                    this.TotalVoid.Text = this.GetCurrencyFromXMLNode(objPaymentHistory.XPathSelectElement("//TotalVoid"));
                    // akaushik5 Changed for MITS 31151 Ends
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //Nadim for 16254(REM Customization)
        private void CustomizePage()
        {
            if (ShowAutoCheckButton.Text != "0")
            {
                btnAutoChecks.Visible = true;
            }
            else
            {
                btnAutoChecks.Visible = false;
            }
            if (AppHelper.GetQueryStringValue("CheckMode").ToString() == "1")
            {
                //   btnAutoChecks.Visible = false; //Commented by Amitosh for MITS 27707  

                BOBMode.Text = "1";
            }
            else
            {
                // btnAutoChecks.Visible = true;//Commented by Amitosh for MITS 27707  
                BOBMode.Text = "0";
            }
            //Neha mits 23601 -- added button back to claim. will be visble only if from claim is true
            if (AppHelper.GetQueryStringValue("FromFunds").ToString() == "1")
            {
                btnBackToClaim.Visible = true;
            }
        }
        //Deb MITS 30185        
        private void SetHeaderConfig()
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(ViewState["PayHistHeaderConfig"].ToString());
                if (xDoc.SelectSingleNode("//Control") != null)
                {
                    if (xDoc.SelectSingleNode("//Control").Attributes["selected"].Value == "0")
                    {
                        //grdPaymentHistory.Columns[0].Visible = false;
                        grdPaymentHistory.Columns.FindByUniqueName("CtlNumber").Visible = false;
                    }
                    else
                    {
                        //grdPaymentHistory.Columns[0].HeaderText = xDoc.SelectSingleNode("//ControlHeader").Attributes["value"].Value;
                        grdPaymentHistory.Columns.FindByUniqueName("CtlNumber").HeaderText = xDoc.SelectSingleNode("//ControlHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//Check") != null)
                {
                    if (xDoc.SelectSingleNode("//Check").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("TransNumber").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("TransNumber").HeaderText = xDoc.SelectSingleNode("//CheckHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//TransDate") != null)
                {
                    if (xDoc.SelectSingleNode("//TransDate").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("TransDate").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("TransDate").HeaderText = xDoc.SelectSingleNode("//TransDateHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//Type") != null)
                {
                    if (xDoc.SelectSingleNode("//Type").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("PaymentFlag").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("PaymentFlag").HeaderText = xDoc.SelectSingleNode("//TypeHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//Cleared") != null)
                {
                    if (xDoc.SelectSingleNode("//Cleared").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("ClearedFlag").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("ClearedFlag").HeaderText = xDoc.SelectSingleNode("//ClearedHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//Void") != null)
                {
                    if (xDoc.SelectSingleNode("//Void").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("VoidFlag").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("VoidFlag").HeaderText = xDoc.SelectSingleNode("//VoidHeader").Attributes["value"].Value;
                    }
                }

                //Added by swati agarwal for WWIG gap 10 mits # 33431
                if (xDoc.SelectSingleNode("//StopPay") != null)
                {
                    if (xDoc.SelectSingleNode("//StopPay").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("StopPayFlag").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("StopPayFlag").HeaderText = xDoc.SelectSingleNode("//StopPayHeader").Attributes["value"].Value;
                    }
                }
                //change end here by swati
                //JIRA 7810 Start Snehal
                if (xDoc.SelectSingleNode("//HoldReason") != null)
                {
                    if (xDoc.SelectSingleNode("//HoldReason").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("HoldReason").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("HoldReason").HeaderText = xDoc.SelectSingleNode("//HoldReasonHeader").Attributes["value"].Value;
                    }
                }
                //JIRA 7810 End

                if (xDoc.SelectSingleNode("//Status") != null)
                {
                    if (xDoc.SelectSingleNode("//Status").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CheckStatus").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CheckStatus").HeaderText = xDoc.SelectSingleNode("//StatusHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//Payee") != null)
                {
                    if (xDoc.SelectSingleNode("//Payee").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("Name").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("Name").HeaderText = xDoc.SelectSingleNode("//PayeeHeader").Attributes["value"].Value;
                    }
                }
                //Ashish Ahuja Mits 34270 start
                if (xDoc.SelectSingleNode("//CheckMemo") != null)
                {
                    if (xDoc.SelectSingleNode("//CheckMemo").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CheckMemo").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CheckMemo").HeaderText = xDoc.SelectSingleNode("//CheckMemoHeader").Attributes["value"].Value;
                    }
                }
                //Ashish Ahuja Mits 34270 end
                if (xDoc.SelectSingleNode("//CheckAmount") != null)
                {
                    if (xDoc.SelectSingleNode("//CheckAmount").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("Amount").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("Amount").HeaderText = xDoc.SelectSingleNode("//CheckAmountHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//FromDate") != null)
                {
                    if (xDoc.SelectSingleNode("//FromDate").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("FromDate").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("FromDate").HeaderText = xDoc.SelectSingleNode("//FromDateHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//ToDate") != null)
                {
                    if (xDoc.SelectSingleNode("//ToDate").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("ToDate").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("ToDate").HeaderText = xDoc.SelectSingleNode("//ToDateHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//Invoice") != null)
                {
                    if (xDoc.SelectSingleNode("//Invoice").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("InvoiceNumber").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("InvoiceNumber").HeaderText = xDoc.SelectSingleNode("//InvoiceHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//TransactionType") != null)
                {
                    if (xDoc.SelectSingleNode("//TransactionType").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CodeDesc").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CodeDesc").HeaderText = xDoc.SelectSingleNode("//TransactionTypeHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//SplitAmount") != null)
                {
                    if (xDoc.SelectSingleNode("//SplitAmount").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("SplitAmount").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("SplitAmount").HeaderText = xDoc.SelectSingleNode("//SplitAmountHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//User") != null)
                {
                    if (xDoc.SelectSingleNode("//User").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("User").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("User").HeaderText = xDoc.SelectSingleNode("//UserHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//CheckDate") != null)
                {
                    if (xDoc.SelectSingleNode("//CheckDate").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CheckDate").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CheckDate").HeaderText = xDoc.SelectSingleNode("//CheckDateHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//CombinedPay") != null)
                {
                    if (xDoc.SelectSingleNode("//CombinedPay").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CombinedPay").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CombinedPay").HeaderText = xDoc.SelectSingleNode("//CombinedPayHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//IsFirstFinal") != null)
                {
                    if (xDoc.SelectSingleNode("//IsFirstFinal").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("IsFirstFinal").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("IsFirstFinal").HeaderText = xDoc.SelectSingleNode("//IsFirstFinalHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//PolicyName") != null)
                {
                    if (xDoc.SelectSingleNode("//PolicyName").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("PolicyName").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("PolicyName").HeaderText = xDoc.SelectSingleNode("//PolicyNameHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//CoverageType") != null)
                {
                    if (xDoc.SelectSingleNode("//CoverageType").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CoverageType").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CoverageType").HeaderText = xDoc.SelectSingleNode("//CoverageTypeHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//Unit") != null)
                {
                    if (xDoc.SelectSingleNode("//Unit").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("Unit").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("Unit").HeaderText = xDoc.SelectSingleNode("//UnitHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//LossType") != null)
                {
                    if (xDoc.SelectSingleNode("//LossType").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("LossType").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("LossType").HeaderText = xDoc.SelectSingleNode("//LossTypeHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//ManualCheck") != null)
                {
                    if (xDoc.SelectSingleNode("//ManualCheck").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("ManualCheck").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("ManualCheck").HeaderText = xDoc.SelectSingleNode("//ManualCheckHeader").Attributes["value"].Value;
                    }
                }
                if (xDoc.SelectSingleNode("//InvoiceAmount") != null)
                {
                    if (xDoc.SelectSingleNode("//InvoiceAmount").Attributes["selected"].Value == "0")
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("InvoiceAmount").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("InvoiceAmount").HeaderText = xDoc.SelectSingleNode("//InvoiceAmountHeader").Attributes["value"].Value;
                    }
                }
                //mcapps2 MITS 30236 Start
                if (xDoc.SelectSingleNode("//Offset") != null)
                {
                    if (xDoc.SelectSingleNode("//Offset").Attributes["selected"].Value == "0")
                    {
                        //grdPaymentHistory.Columns[31].Visible = false;//sharishkumar commented JIRA RMA-313
                        //grdPaymentHistory.Columns[27].Visible = false;//sharishkumar JIRA RMA-313
                        grdPaymentHistory.Columns.FindByUniqueName("Offset").Visible = false;
                    }
                    else
                    {
                        //grdPaymentHistory.Columns[31].HeaderText = xDoc.SelectSingleNode("//OffsetHeader").Attributes["value"].Value;sharishkumar commented JIRA RMA-313
                        // grdPaymentHistory.Columns[27].HeaderText = xDoc.SelectSingleNode("//OffsetHeader").Attributes["value"].Value;//sharishkumar JIRA RMA-313
                        grdPaymentHistory.Columns.FindByUniqueName("Offset").HeaderText = xDoc.SelectSingleNode("//OffsetHeader").Attributes["value"].Value;
                    }
                }
                //mcapps2 MITS 30236 End
                //change end here by swati
                //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                if (xDoc.SelectSingleNode("//CheckTotal") != null)
                {
                    //  //Changed by Nikhil on 09/22/14.Code Review changes
                    //  if (xDoc.SelectSingleNode("//CheckTotal").Attributes["selected"].Value == "0")
                    if (string.Compare(xDoc.SelectSingleNode("//CheckTotal").Attributes["selected"].Value.ToString(), "0") == 0)
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CheckTotal").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("CheckTotal").HeaderText = xDoc.SelectSingleNode("//CheckTotalHeader").Attributes["value"].Value;
                    }
                }
                //End -  Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                if (xDoc.SelectSingleNode("//ReserveType") != null)
                {
                    if (string.Compare(xDoc.SelectSingleNode("//ReserveType").Attributes["selected"].Value.ToString(), "0") == 0)
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("ReserveType").Visible = false;
                    }
                    else
                    {
                        grdPaymentHistory.Columns.FindByUniqueName("ReserveType").HeaderText = xDoc.SelectSingleNode("//ReserveTypeHeader").Attributes["value"].Value;
                    }
                }
            }
            catch
            {

            }
        }
        //Deb MITS 30185          
        // akaushik5 Added for MITS 31151 Starts
        /// <summary>
        /// Gets the currency from XML node.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>
        /// Formated string value from node.
        /// </returns>
        private string GetCurrencyFromXMLNode(XElement node)
        {
            if (object.ReferenceEquals(node, null))
            {
                return string.Empty;
            }

            return string.Format("{0:C}", node.Value);
        }
        // akaushik5 Added for MITS 31151 Ends
        protected DateTime? startDate //Changed property type to string-ANkit MITS 29909         //Praveen ML changes- Changed Back to DateTime
        {
            set
            {
                ViewState["strD"] = value;
            }
            get
            {
                if (ViewState["strD"] != null)
                    return (DateTime)ViewState["strD"];
                else
                {
                    //return DateTime.Today.AddYears(-1);
                    return null; // mkaran2 - MITS 35712
                }
            }
        }
        protected DateTime? endDate //Changed property type to string-ANkit MITS 29909     //Praveen ML changes- Changed Back to DateTime
        {
            set
            {
                ViewState["endD"] = value;
            }
            get
            {
                if (ViewState["endD"] != null)
                    return (DateTime)ViewState["endD"];
                else
                {
                    //return DateTime.Today;                   
                    return null; // mkaran2 - MITS 35712
                }
            }
        }
        protected DateTime? checkDate
        {
            set
            {
                ViewState["checkDate"] = value;
            }
            get
            {
                if (ViewState["checkDate"] != null)
                    return (DateTime)ViewState["checkDate"];
                else
                {
                    //return DateTime.Today;
                    return null; // mkaran2 - MITS 35712
                }
            }
        }
        protected DateTime? fsFromDate
        {
            set
            {
                ViewState["fDate"] = value;
            }
            get
            {
                if (ViewState["fDate"] != null)
                    return (DateTime)ViewState["fDate"];
                else
                {
                    //return DateTime.Today;
                    return null; // mkaran2 - MITS 35712
                }
            }
        }
        protected DateTime? fsToDate
        {
            set
            {
                ViewState["tDate"] = value;
            }
            get
            {
                if (ViewState["tDate"] != null)
                    return (DateTime)ViewState["tDate"];
                else
                {
                    //return DateTime.Today;
                    return null; // mkaran2 - MITS 35712
                }
            }
        }

        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("PaymentHistory.aspx"), strResourceType).ToString();
        }

        #region Commented obsolete functions
        //start rkaur27       
        /// <summary>//Manish
        /// The event is called on when Radgrid Column filter is loaded
        ///   /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author

        ///************************************************************
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        //protected void Page_PreRender(object sender, EventArgs e)
        //{
        //    grdPaymentHistory.Columns[3].CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
        //    grdPaymentHistory.Columns[4].CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
        //    grdPaymentHistory.Columns[5].CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
        //    grdPaymentHistory.Columns[6].CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
        //    grdPaymentHistory.Columns[16].CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo;
        //    grdPaymentHistory.Columns[17].CurrentFilterFunction = Telerik.Web.UI.GridKnownFunction.EqualTo; //MITS 29909 Ankit            
        //}
        /// <summary>//Manish
        /// This function  is called to calculate total payment and collection
        /// for each row.
        ///   /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 27 Sept 2011     Changes made to    Manish Jain
        ///                  add footer in 
        ///                  Radgrid
        ///************************************************************
        /// </summary>
        // akaushik5 Commneted for MITS 31151 Starts
        //private void SetFooterData(DataTable dtPaymentHistory)
        //{
        //    double dblTotalAll = 0.0;
        //    double dblTotalPay = 0.0;
        //    double dblTotalCollect = 0.0;
        //    double dblTotalVoid = 0.0;
        //    bool bCalTotal = false;
        //    Hashtable htTranId = new Hashtable();
        //    bool bLeadingLine = false;
        //    string sPrevTransId = "";

        //    for (int i = 0; i < dtPaymentHistory.Rows.Count; i++)
        //    {
        //        if (i == 0)
        //        {
        //            bLeadingLine = true;
        //            htTranId.Add(dtPaymentHistory.Rows[i]["TRANSID"], dtPaymentHistory.Rows[i]["TRANSID"]);
        //            bCalTotal = true;
        //        }
        //        else
        //        {
        //            if (htTranId.Contains(dtPaymentHistory.Rows[i]["TRANSID"]))
        //                bCalTotal = false;
        //            else
        //            {
        //                bCalTotal = true;
        //                htTranId.Add(dtPaymentHistory.Rows[i]["TRANSID"], dtPaymentHistory.Rows[i]["TRANSID"]);
        //            }

        //            if (dtPaymentHistory.Rows[i]["TRANSID"].ToString().Equals(sPrevTransId))
        //                bLeadingLine = false;
        //            else
        //                bLeadingLine = true;
        //        }
        //        CreateRow(dtPaymentHistory.Rows[i],bLeadingLine, bCalTotal,
        //            ref dblTotalAll, ref dblTotalPay, ref dblTotalCollect, ref dblTotalVoid);
        //    }
        //}
        // akaushik5 Commneted for MITS 31151 Ends

        /// Name		: CreateRow
        /// Author		: Mihika Agrawal
        /// Date Created: 08/01/2005	
        /// /// Amendment History
        ///************************************************************
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 27 Sept 2011     Changes made to    Manish Jain
        ///                  add footer in 
        ///                  Radgrid
        ///************************************************************
        /// <summary>
        /// Creates a record in a report
        /// </summary>
        /// <param name="p_objData">DataRow object containing a row data</param>
        /// <param name="p_dblCurrentX">Current X position</param>
        /// <param name="p_dblCurrentY">Current Y position</param>
        /// <param name="p_arrColAlign">Columns alignment</param>
        /// <param name="p_dblLeftMargin">Left margin</param>
        /// <param name="p_dblLineHeight">Line height</param>
        /// <param name="p_arrColWidth">Column width</param>
        /// <param name="p_dblCharWidth">Character width</param>
        /// <param name="p_bLeadingLine">Leading Line Flag</param>
        /// <param name="p_bCalTotal">Calculate Total Flag</param>
        /// <param name="p_dblTotalAll">Over all Total</param>
        /// <param name="p_dblTotalPay">Total Payments</param>
        /// <param name="p_dblTotalCollect">Total Collections</param>
        /// <param name="p_dblTotalVoid">Total Voids</param>
        // akaushik5 Commented for MITS 31151 Starts
        ////private void CreateRow(DataRow p_objData, bool p_bLeadingLine, bool p_bCalTotal,
        ////    ref double p_dblTotalAll, ref double p_dblTotalPay, ref double p_dblTotalCollect, ref double p_dblTotalVoid)
        ////{
        ////    string sData = "";
        ////    string sCheckStatusCode = string.Empty;
        ////    string sCheckStatusDesc = string.Empty;
        ////    bool bPayment = false;
        ////    bool bVoid = false;
        ////    double dblAmount = 0.0;
        ////    double dblSplitAmount = 0.0;

        ////    try
        ////    {

        ////        for (int iIndexJ = 0; iIndexJ < 9; iIndexJ++)
        ////        {
        ////            sData = " ";
        ////            switch (iIndexJ)
        ////            {
        ////                case 0:
        ////                    if (p_bLeadingLine)
        ////                    {

        ////                    }
        ////                    break;
        ////                case 1:
        ////                    if (p_objData["TransNumber"] != null && String.Compare((p_objData["TransNumber"]).ToString(), "\"") != 0)
        ////                    {
        ////                        if (p_bLeadingLine)
        ////                            sData = Conversion.ConvertObjToStr(p_objData["TransNumber"]);
        ////                    }
        ////                    break;
        ////                case 2:
        ////                    if (p_bLeadingLine)
        ////                        sData = Conversion.ConvertObjToStr(p_objData["TransDate"]);
        ////                        //sData = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(p_objData["TransDate"]), "d");
        ////                    break;
        ////                case 3:
        ////                    if (p_objData["PaymentFlag"] != null && String.Compare((p_objData["PaymentFlag"]).ToString(), "\"") != 0)
        ////                    {
        ////                        if (Conversion.ConvertObjToStr(p_objData["PaymentFlag"]) == "Payment")
        ////                        {
        ////                            bPayment = true;
        ////                        }
        ////                        else
        ////                        {
        ////                            bPayment = false;
        ////                        }

        ////                        if (p_bLeadingLine)
        ////                            sData = bPayment ? "Payment" : "Collection";
        ////                    }
        ////                    break;
        ////                case 4:
        ////                    if (p_objData["ClearedFlag"] != null && String.Compare((p_objData["ClearedFlag"]).ToString(), "\"") != 0)
        ////                    {
        ////                        if (p_bLeadingLine)
        ////                            sData = Common.Conversion.ConvertObjToBool(p_objData["ClearedFlag"]) ? "Yes" : "No";
        ////                    }
        ////                        break;

        ////                case 5:
        ////                        if (p_objData["VoidFlag"] != null && String.Compare((p_objData["VoidFlag"]).ToString(), "\"") != 0)
        ////                        {
        ////                            if(p_objData["VoidFlag"].ToString().ToUpper() =="YES")
        ////                            {
        ////                                bVoid = true;
        ////                            }
        ////                            else
        ////                            {
        ////                            bVoid = false;

        ////                             }
        ////                            if (p_bLeadingLine)
        ////                                sData = bVoid ? "Yes" : "No";
        ////                        }
        ////                    break;
        ////                case 6:
        ////                    if (p_bLeadingLine)
        ////                    {

        ////                    }
        ////                    break;
        ////                case 7:
        ////                    if (p_bLeadingLine)
        ////                    {

        ////                    }
        ////                    break;
        ////                case 8:
        ////                    //Manish for multi currency MITS 27209
        ////                    if (p_objData["DAmount"] != null)
        ////                    //if (p_objData["DAmount"] != null && String.Compare((p_objData["DAmount"]).ToString(), "\"") != 0)
        ////                    {

        ////                        //if (p_objData["DAmount"].ToString().Replace("$", "").Replace(",", "").Contains('('))
        ////                        //{
        ////                        //    //dblAmount = float.Parse(p_objData["DAmount"].ToString().Replace("$", "").Replace(",", "").Replace("(", "").Replace(")", ""));  //MITS MANISH;
        ////                        //    dblAmount = float.Parse(p_objData["DAmount"].ToString());
        ////                        //    dblAmount = System.Math.Abs(dblAmount) * (-1);
        ////                        //    if (!bPayment)
        ////                        //        dblAmount = +dblAmount;
        ////                        //    if (p_bLeadingLine)
        ////                        //        sData = string.Format("{0:C}", dblAmount);
        ////                        //}
        ////                        //else
        ////                        {

        ////                            //dblAmount = float.Parse(p_objData["DAmount"].ToString().Replace("$", "").Replace(",", ""));  //MITS MANISH;
        ////                            dblAmount = float.Parse(p_objData["DAmount"].ToString());
        ////                            if (!bPayment)
        ////                                dblAmount = -dblAmount;
        ////                            if (p_bLeadingLine)
        ////                                sData = string.Format("{0:C}", dblAmount);

        ////                        }
        ////                    }

        ////                    break;

        ////            }


        ////            if (sData == "")
        ////                sData = " ";

        ////        }

        ////        if (p_bCalTotal)
        ////        {
        ////            //pmittal5 MITS:9777 04/07/08
        ////            //p_dblTotalAll = p_dblTotalAll + dblAmount;
        ////            if (bVoid)
        ////                p_dblTotalVoid = p_dblTotalVoid + dblAmount;
        ////            else
        ////            {
        ////                if (bPayment)
        ////                    p_dblTotalPay = p_dblTotalPay + dblAmount;
        ////                else
        ////                    //pmittal5 MITS:9777 04/07/08
        ////                    //p_dblTotalCollect = p_dblTotalCollect - dblAmount;
        ////                    p_dblTotalCollect = p_dblTotalCollect + dblAmount;
        ////            }
        ////        }

        ////        dtotVoid = p_dblTotalVoid;
        ////        dnetPay = p_dblTotalPay;
        ////        dtotColl = p_dblTotalCollect;

        ////    }

        ////    catch (Exception ee)
        ////    {
        ////        ErrorHelper.logErrors(ee);
        ////        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        ////        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        ////        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
        ////    }

        ////}
        // akaushik5 Commented for MITS 31151 Ends

        //Nadim for 16254(REM Customization)
        //private string GetSortColumn(string sortExpression)
        //{
        //    switch (sortExpression)
        //    {
        //        case "Control #":
        //            return "FUNDS.CTL_NUMBER";
        //        case "Check #":
        //            return "FUNDS.TRANS_NUMBER";
        //        case "Trans Date":
        //            return "FUNDS.TRANS_DATE";
        //        case "Type":
        //            return "FUNDS.PAYMENT_FLAG";
        //        case "Clr?":
        //            return "FUNDS.CLEARED_FLAG";
        //        case "Void?":
        //            return "FUNDS.VOID_FLAG";
        //        case "Status":
        //            return "FUNDS.STATUS_CODE";
        //        case "Payee":
        //            return "FUNDS.LAST_NAME";
        //        case "Check Amount":
        //            return "FUNDS.AMOUNT";
        //        case "From Date":
        //            return "FUNDS_TRANS_SPLIT.FROM_DATE";
        //        case "To Date":
        //            return "FUNDS_TRANS_SPLIT.TO_DATE";
        //        case "Invoice #":
        //            return "FUNDS_TRANS_SPLIT.INVOICE_NUMBER";
        //        case "Transaction Type":
        //            return "CODES_TEXT.CODE_DESC";
        //        case "SplitAmount":
        //            return "FUNDS_TRANS_SPLIT.AMOUNT";
        //        case "User":
        //            return "FUNDS_TRANS_SPLIT.ADDED_BY_USER";
        //        case "CheckDate":
        //            return "FUNDS.DATE_OF_CHECK";
        //        case "CombinedPay":
        //            return "FUNDS.COMBINED_PAY_FLAG";
        //        //MITS 35247 STARTS
        //        case "Offset":
        //            return "FUNDS.OFFSET_FLAG";
        //        //MITS 35247 ENDS
        //        case "Invoice Amount":
        //            return "FUNDS_TRANS_SPLIT.INVOICE_AMOUNT";
        //        default:
        //            return "";
        //    }
        //}

        //private int GetSortColumnIndex(string sortColumn)
        //{
        //    switch (sortColumn)
        //    {
        //        case "FUNDS.CTL_NUMBER":
        //            return 0;
        //        case "FUNDS.TRANS_NUMBER":
        //            return 1;
        //        case "FUNDS.TRANS_DATE":
        //            return 2;
        //        case "FUNDS.PAYMENT_FLAG":
        //            return 3;
        //        case "FUNDS.CLEARED_FLAG":
        //            return 4;
        //        case "FUNDS.VOID_FLAG":
        //            return 5;
        //        case "FUNDS.STATUS_CODE":
        //            return 6;
        //        case "FUNDS.LAST_NAME":
        //            return 7;
        //        case "FUNDS.AMOUNT":
        //            return 8;
        //        case "FUNDS_TRANS_SPLIT.FROM_DATE":
        //            return 9;
        //        case "FUNDS_TRANS_SPLIT.TO_DATE":
        //            return 10;
        //        case "FUNDS_TRANS_SPLIT.INVOICE_NUMBER":
        //            return 11;
        //        case "CODES_TEXT.CODE_DESC":
        //            return 12;
        //        case "FUNDS_TRANS_SPLIT.AMOUNT":
        //            return 13;
        //        case "FUNDS_TRANS_SPLIT.ADDED_BY_USER":
        //            return 14;
        //        case "FUNDS.DATE_OF_CHECK":
        //            return 15;
        //        case "FUNDS.COMBINED_PAY_FLAG":
        //            return 17;
        //        //MITS 35247 STARTS
        //        case "FUNDS.OFFSET_FLAG":
        //            return 18;
        //        //MITS 35247 ENDS
        //        case "FUNDS_TRANS_SPLIT.INVOICE_AMOUNT":
        //            return 19;
        //        default:
        //            return -1;
        //    }
        //}
        //end rkaur27 - removed obsolete functions
        #endregion

    }
}
