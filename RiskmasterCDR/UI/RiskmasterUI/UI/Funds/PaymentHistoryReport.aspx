﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentHistoryReport.aspx.cs" Inherits="Riskmaster.UI.UI.Funds.PaymentHistoryReport" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
     <script type="text/javascript">
         function getControlValues() {

             //var myHiddenVar = window.opener.document.getElementById("myHiddenVar");
             var myHiddenVar = window.opener.document.getElementById("hdnFilterExpression"); // mkaran2 - MITS 32340 : myHiddenVar on parent page was made to blank
             if (myHiddenVar != null) {

                 var DoPostBack = document.getElementById("DoPostBack");
                
                 if (document.getElementById("DoPostBack").value == "true") {
                     document.getElementById("FilterExp").value = myHiddenVar.value;
                     
                     //document.form1.submit();//nnithiyanand - date 12/05/2013 - commented for smoke issue.
                     document.getElementById('form1').submit(); //nnithiyanand - date 12/05/2013 - Added for smoke issue.

                 }
                 else {
                     return false;
                 }
             }
         }

        

    </script>
    </head>
<body onload="getControlValues();">
   <form id="form1" runat="server">
    <div>

     <table border="0" align="center"> 
        <tr>
      <td colspan="2">
        <uc3:ErrorControl ID="ErrorControl1" runat="server" />
       </td>
    </tr>        
    </table>            
    <asp:TextBox Style="display: none" rmxref="/Instance/Document/PaymentHistory/ClaimId"
            ID="ClaimId" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="ClaimNumber" rmxref="/Instance/Document/PaymentHistory/ClaimNumber"
            runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="FrozenFlag" rmxref="/Instance/Document/PaymentHistory/FrozenFlag"
            runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="SubTitle" rmxref="/Instance/Document/PaymentHistory/SubTitle"
            runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="ClaimantEid" rmxref="/Instance/Document/PaymentHistory/ClaimantEid"
            runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="UnitId" rmxref="/Instance/Document/PaymentHistory/UnitId"
            runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="SortCol" rmxref="/Instance/Document/PaymentHistory/SortCol"
            runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="Ascending" rmxref="/Instance/Document/PaymentHistory/Ascending"
            runat="server"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="FormTitle" rmxref="/Instance/Document/PaymentHistory/FormTitle"
            runat="server"></asp:TextBox>
              <asp:TextBox Style="display: none" ID="FilterExp" rmxref="/Instance/Document/PaymentHistory/FilterExp"
            runat="server"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="DoPostBack" runat="server"></asp:TextBox>
              <%--skhare7 R8 enhancement--%>
  <asp:TextBox Style="display: none" ID="EntityId" rmxref="/Instance/Document/PaymentHistory/EntityId"
            runat="server" rmxignoreget="true"></asp:TextBox>
           
             <%--skhare7 R8 enhancement--%>

    </div>
    </form>
</body>
</html>
