<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="reissue.aspx.cs"  Inherits="Riskmaster.UI.Funds.reissue" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
    <head id="Head1" runat="server">
        <base target="_self" />
        <title>Void and Reissue Transaction</title>
        <link rel="stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
        <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
        <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
        <script language="JavaScript" src="../../Scripts/drift.js" type="text/javascript">        { var i; }
        </script>
        <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }
        </script>
        <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
        </script>
        
        <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
        </script>
        <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
        </script>
        <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
        </script>
        <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
        </script>
        <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript">        { var i; }</script>
        

      <!-- Rakhel ML Changes - Start !-->
     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
     <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
     <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">         { var i; }  </script>
	 <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">	     { var i; } </script>
     <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">         { var i; } </script>
     <script type="text/javascript">
        function BtnDisable() {
            var btnReissueDate = $("#ReissueDate");
            $.datepicker._disabledInputs = [btnReissueDate[0]];
         }

         function Disp() {
             $.datepicker._disabledInputs = [];
         }
       
     </script>
      <!--Rakhel ML Changes - End !-->

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    </head>
    <body class="10pt" onload="pageLoaded();BtnDisable();" onunload="Disp();">
        <form name="frmData" id="frmData" runat="server">
            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
            <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
            <asp:TextBox style="display:none" runat="server" id="useraction" RMXType="id" />
            <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
            <asp:ScriptManager ID="SMgr" runat="server" />
            <div class="msgheader" id="div_formtitle" runat="server">
                <asp:label id="formtitle" runat="server" Text="<%$ Resources:lblformTitleResrc %>" />
                <asp:label id="formsubtitle" runat="server" Text="" />
            </div>
            <div runat="server" class="completerow" id="div_ReissueReasonCode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ReissueReasonCode" Text="<%$ Resources:lblReissueReasonCodeResrc %>" />
                <span class="formw">
                    <uc:CodeLookUp runat="server" ID="ReissueReasonCode" CodeTable="REISSUE_REASON_CODE" ControlName="ReissueReasonCode" RMXRef="/option/ReissueReasonCode" RMXType="code" />
                </span>
            </div>
            <div runat="server" class="completerow" id="div_ReissueDate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ReissueDate" Text="<%$ Resources:lblReissueDateResrc %>" />
                <span class="formw">
                    <asp:TextBox runat="server" FormatAs="date" id="ReissueDate" RMXRef="/option/ReissueDate" RMXType="date" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                   <!--Rakhel ML Changes - Start !-->
                   
                    <script type="text/javascript">
                     $(function () {
                         $("#ReissueDate").datepicker({
                             showOn: "button",
                             buttonImage: "../../Images/calendar.gif",
                             buttonImageOnly: true,
                             showOtherMonths: true,
                             selectOtherMonths: true,
                             changeYear: true
                         });
                     });
                    </script>
                    <!-- Rakhel ML Changes - End !-->
                </span>
            </div>
            <div runat="server" class="completerow" id="div_ReissuedBy" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ReissuedBy" Text="<%$ Resources:lblReissuedByResrc %>" />
                <span class="formw">
                    <asp:TextBox runat="server" onchange="setDataChanged(true);" id="ReissuedBy" RMXRef="/option/ReissuedBy" RMXType="text" />
                </span>
            </div>
            <div runat="server" class="half" id="div_isReissuePayment" style="display:none;" xmlns="">
                <span class="formw">
                    <asp:TextBox style="display:none" runat="server" id="isReissuePayment" RMXRef="/option/isReissuePayment" RMXType="hidden" />
                </span>
            </div>
            <div runat="server" class="half" id="div_RollUpCount" style="display:none;" xmlns="">
                <span class="formw">
                    <asp:TextBox style="display:none" runat="server" id="RollUpCount" RMXRef="/option/RollUpCount" RMXType="hidden" />
                </span>
            </div>
            <div id="Div1" class="errtextheader" runat="server">
                <asp:label id="formdemotitle" runat="server" Text="" />
            </div>
            <div class="tabGroup" id="TabsDivGroup" runat="server" />
            <div id="Div2" class="formButtonGroup" runat="server">
                <div class="formButton" runat="server" id="div_btnReissue">
                    <script language="JavaScript" src="">{var i;}
                    </script>
                    <asp:button class="button" runat="server" id="btnReissue" RMXRef="" Text="<%$ Resources:btnReissueResrc %>" width="175px" OnClientClick="ReissuePayment(); return false;" />
                </div>
                <div class="formButton" runat="server" id="div_btnCancel">
                    <script language="JavaScript" src="">{var i;}
                    </script>
                    <asp:button class="button" runat="server" id="btnCancel" RMXRef="" Text="<%$ Resources:btnCancelResrc %>" width="175px" OnClientClick="window.close(); return false;" />
                </div>
            </div>
            <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
            <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="Reissue" />
            <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="funds" />
            <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="transidid" />
            <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="reissue" />
            <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="parentid" />
            <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" name="formname" Text="reissue" />
            <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="ReissueReasonCode_codelookup_cid|" />
            <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="ReissueReasonCode|" />
            <input type="hidden" id="hdSaveButtonClicked" />
            <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
            <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
            <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="<%$ Resources:PleaseWaitDialog1Resrc %>" />
        </form>
    </body>
</html>