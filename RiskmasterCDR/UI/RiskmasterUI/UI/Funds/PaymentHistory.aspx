﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentHistory.aspx.cs"
    Inherits="Riskmaster.UI.UI.Funds.PaymentHistory" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Payment History</title>
    <link href="../../APP_Themes/rmnet.css" rel="Stylesheet" type="text/css" />
    <script src="../../Scripts/jquery/jquery-1.8.0.min.js" type="text/javascript"></script>
    <style type="text/css">
        .GridViewFixedHeader
        {
            /* So the overflow scrolls */
            overflow: auto;
        }
        .GridViewFixedHeader table th
        {
            /* Keep the header cells positioned as we scroll */ /*position:relative; */
            position: relative;
            top: expression(document.getElementById("GridViewFixedHeader").scrollTop-2);
            z-index: 10;
        }
        .GridViewFixedHeader table tbody
        {
            /* Keep the header cells positioned as we scroll */ /*position:relative; */
            overflow-x: hidden;
        }
        .SelectedItem
        {
            background: none repeat scroll 0 0 #6699FF !important;
        }
        
        a:hover { text-decoration: underline;}

        /* aravi5 RMA-12053 Entire loss type name is not diaplayed on Transaction history screen Starts */
        .RadGrid .rgClipCells .rgRow > td, .RadGrid .rgClipCells .rgAltRow>td
        {
            word-wrap: break-word;
        }
        /* aravi5 RMA-12053 Entire loss type name is not diaplayed on Transaction history screen Ends */
   </style>
    <script type="text/javascript" language="javascript">
        function ShowReport() {
            var sClaimId = document.getElementById('ClaimId').value;
            var sClaimantEid = document.getElementById('ClaimantEid').value;
            var sUnitId = document.getElementById('UnitId').value;
            var sSortCol = document.getElementById('SortCol').value;
            var sAscending = document.getElementById('Ascending').value;
            var sSubTitle = document.getElementById('SubTitle').value;
            //pmittal5 MITS 9906  04/11/08  -Start
            //            var sFormTitle = document.getElementById('FormTitle').value;
            var sFormTitle = document.getElementById('Caption').value;
            var sCheckMode = document.getElementById('BOBMode').value;
            //    MITS Manish
            //skhare7 R8 Combined Payment
            var sEntityId = document.getElementById('EntityId').value;
            var sFilterExp = document.getElementById('myHiddenVar').value;
            if (sFormTitle.search(/&amp;/) != -1) {
                sFormTitle = sFormTitle.replace("&amp;", "%26");    // %26 is escape character for query string
            }
            //    MITS Manish
            //var sQueryString = 'ClaimId='+sClaimId+'&amp;ClaimantEid='+sClaimantEid+'&amp;UnitId='+sUnitId+'&amp;SortCol='+sSortCol+'&amp;Ascending='+sAscending+'&amp;SubTitle='+sSubTitle ;

            //jramkumar for MITS 32753
            if (sSortCol.indexOf("upper(") != -1)
                sSortCol = sSortCol.replace("upper(", "").replace(")", "");

            //RMA-7081 : After printing payment history sorting of control numbers differ from payment history grid in Google chrome
            var sQueryString = 'ClaimId=' + sClaimId + '&ClaimantEid=' + sClaimantEid + '&CheckMode=' + sCheckMode + '&UnitId=' + sUnitId + '&SortCol=' + sSortCol + '&Ascending=' + sAscending + '&FormTitle=' + sFormTitle + '&EntityId=' + sEntityId;
            //pmittal5 MITS 9906  04/11/08  -End
            //mkaran2 - MITS 32340 : start
            var txtFilterValue = document.getElementById('txtFilterValue').value;
            if (txtFilterValue != null && document.getElementById('hdnFilterExpression') != null)
                document.getElementById('hdnFilterExpression').value = txtFilterValue;
            //mkaran2 - MITS 32340 : end               
            window.open('/RiskmasterUI/UI/Funds/PaymentHistoryReport.aspx?' + sQueryString, 'PaymentHistory',
						                'width=650,height=550,top=' + (screen.availHeight - 550) / 2 + ',left=' + (screen.availWidth - 650) / 2 + ',resizable=yes,scrollbars=yes');

            return false;
        }

        //MITS Manish
        function filterMenuShowing(sender, eventArgs) {

            //rupal:start,mits 28077
            var menu = eventArgs.get_menu();
            var items = menu._itemData;
            var i = 0;
            if ((eventArgs.get_column().get_uniqueName() == "Amount") || (eventArgs.get_column().get_uniqueName() == "SplitAmount")) {
                while (i < items.length) {

                    var item = menu._findItemByValue(items[i].value);
                    if (items[i].value == "Contains" || items[i].value == "DoesNotContain") {
                        if (item != null) {
                            item._element.style.display = "none";
                        }
                    }
                    else {
                        if (item != null) {
                            item._element.style.display = "";
                        }
                    }
                    i++;
                }
            }
            else if ((eventArgs.get_column().get_uniqueName() == "CheckDate") || (eventArgs.get_column().get_uniqueName() == "ToDate") || (eventArgs.get_column().get_uniqueName() == "FromDate")) {
                while (i < items.length) {
                    var item = menu._findItemByValue(items[i].value);
                    if (items[i].value == "Contains" || items[i].value == "DoesNotContain") {
                        if (item != null) {
                            item._element.style.display = "none";
                        }
                    }
                    else {
                        if (item != null) {
                            item._element.style.display = "";
                        }
                    }
                    i++;
                }
            }
            else if ((eventArgs.get_column().get_uniqueName() == "User") || (eventArgs.get_column().get_uniqueName() == "CodeDesc") || (eventArgs.get_column().get_uniqueName() == "Name") || (eventArgs.get_column().get_uniqueName() == "PolicyName") || (eventArgs.get_column().get_uniqueName() == "CoverageType") || (eventArgs.get_column().get_uniqueName() == "Unit") || (eventArgs.get_column().get_uniqueName() == "ReserveType")) {
                while (i < items.length) {
                    var item = menu._findItemByValue(items[i].value);
                    if (items[i].value == "GreaterThan" || items[i].value == "LessThan" || items[i].value == "GreaterThanOrEqualTo" || items[i].value == "LessThanOrEqualTo") {
                        if (item != null) {
                            item._element.style.display = "none";
                        }
                    }
                    else {
                        if (item != null) {
                            item._element.style.display = "";
                        }
                    }
                    i++;
                }
            }
                //rupal:end
            else {
                var menu = eventArgs.get_menu();
                var items = menu._itemData;

                var i = 0;
                while (i < items.length) {
                    var item = menu._findItemByValue(items[i].value);
                    if (item != null)
                        item._element.style.display = "";
                    i++;
                }
            }

        }


        function BackToFinancials() {

            var sClaimId = document.getElementById('ClaimId').value;
            var sClaimantEid = document.getElementById('ClaimantEid').value;
            var sClaimantRowId = document.getElementById('ClaimantRowId').value;
            var sUnitId = document.getElementById('UnitId').value;
            var sUnitRowId = document.getElementById('UnitRowId').value;
            // Geeta  17-Oct-06: Added for fixing MITS no. 8207							
            var sFrozenflag = document.getElementById('FrozenFlag').value;
            var sClaimNumber = document.getElementById('ClaimNumber').value;
            var sCheckMode = document.getElementById('BOBMode').value;
            var sFDH = document.getElementById('hdFDHButton').value; //pgupta93: RMA-4608

            var sBackToClaim = "";
            //Neha adding back to claim button kkk
            if (document.getElementById('btnBackToClaim') != null) {
                if (document.getElementById('btnBackToClaim').value)
                    sBackToClaim = "%26FromFunds=1"; //RMA-8787 : %26 is a escape character of '&' for query string
            }
            // Defect no. 1879: Back to Financials is not working properly, in case of detail level tracking
            // Should go back to the claimant/unit level
            // Geeta  17-Oct-06: Passing 'frozen flag' and 'claim number' back to the Reserve Listing page
            //bkuzhanthaim : RMA-8787 : User have to manually reload the frame to enable add / edit reserve, Replaced &amp; with & in Query String
            if (sCheckMode != "1") {
                if (sUnitId != "" && sUnitId != "0")
                    //window.location.href = '/RiskmasterUI/UI/Reserves/ReserveListing.aspx?ClaimId=' + sClaimId + '&amp;UnitID=' + sUnitId + '&amp;UnitRowId=' + sUnitRowId + '&amp;FrozenFlag=' + sFrozenflag + '&amp;ClaimNumber=' + sClaimNumber + sBackToClaim;
                    window.location.href = '/RiskmasterUI/UI/Reserves/ReserveListing.aspx?ClaimId=' + sClaimId + '&amp;UnitID=' + sUnitId + '&amp;UnitRowId=' + sUnitRowId + '&amp;FrozenFlag=' + sFrozenflag + '&amp;ClaimNumber=' + sClaimNumber + '&FormName=' + sFDH + sBackToClaim;
                else
                    //window.location.href = '/RiskmasterUI/UI/Reserves/ReserveListing.aspx?ClaimId=' + sClaimId + '&amp;ClaimantId=' + sClaimantRowId + '&amp;ClaimantEId=' + sClaimantEid + '&amp;FrozenFlag=' + sFrozenflag + '&amp;ClaimNumber=' + sClaimNumber + sBackToClaim;
                    window.location.href = '/RiskmasterUI/UI/Reserves/ReserveListing.aspx?ClaimId=' + sClaimId + '&amp;ClaimantId=' + sClaimantRowId + '&amp;ClaimantEId=' + sClaimantEid + '&amp;FrozenFlag=' + sFrozenflag + '&amp;ClaimNumber=' + sClaimNumber + '&FormName=' + sFDH + sBackToClaim;

            }
            else {
                window.location.href = '/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=' + sClaimId;

            }
            return false;
        }

        function AutoCheckList() {
            // Geeta  17-Oct-06: Modified for fixing MITS no. 8207								
            if (document.getElementById('FrozenFlag').value == 'true') {
                var sClaimNumber = document.getElementById('ClaimNumber').value;
                //alert("Claim No " + sClaimNumber + " has its payments frozen. No further payments may be made until it is unfrozen");
                alert(PaymentHistoryValidations.ValidFrozenAlert1 + " " + sClaimNumber + " " + PaymentHistoryValidations.ValidFrozenAlert2);
            }
            else {
                var sClaimId = document.getElementById('ClaimId').value;
                var sClaimantEid = document.getElementById('ClaimantEid').value;
                var sUnitId = document.getElementById('UnitId').value;
                var sUnitRowId = document.getElementById('UnitRowId').value;
                var sSubTitle = document.getElementById('SubTitle').value; //zalam 04/28/2008 Mits-8645
                //pmittal5 MITS 9906  04/11/08  -Start
                var sFormTitle = document.getElementById('FormTitle').value;
                var sCaption = document.getElementById('Caption').value;
                //skhare7 R8 Combined Payment
                var sEntityId = document.getElementById('EntityId').value;
                if (sFormTitle.search(/&amp;/) != -1) {
                    sFormTitle = sFormTitle.replace("&amp;", "%26");    // %26 is escape character for query string
                }
                //var sQueryString = 'ClaimId='+sClaimId+'&amp;ClaimantEid='+sClaimantEid+'&amp;UnitId='+sUnitId;
                //var sQueryString = 'ClaimId='+sClaimId+'&amp;ClaimantEid='+sClaimantEid+'&amp;UnitId='+sUnitId+'&amp;FormTitle='+sFormTitle ;
                //pmittal5 MITS 9906  04/11/08  -End
                //zalam 04/28/2008 Mits-8645 Start
                // aravi5 RMA-10586 Corprate financial screen is seen for carrier customers when clicked on back button of auto checks screen-CHROME Replaced &amp; with %26 in Query String Starts
                var sCheckMode = document.getElementById('BOBMode').value;
                if (document.getElementById('BOBMode').value != "1") {
                    var sQueryString = 'ClaimId=' + sClaimId + '%26ClaimantEid=' + sClaimantEid + '%26UnitId=' + sUnitId + '%26UnitRowId=' + sUnitRowId + '%26FormTitle=' + sFormTitle + '%26SubTitle=' + sSubTitle + '%26Caption=' + sCaption + '%26EntityId=' + sEntityId;
                }
                else
                    var sQueryString = 'ClaimId=' + sClaimId + '%26ClaimantEid=' + sClaimantEid + '%26UnitId=' + sUnitId + '%26UnitRowId=' + sUnitRowId + '%26FormTitle=' + sFormTitle + '%26SubTitle=' + sSubTitle + '%26Caption=' + sCaption + '%26EntityId=' + sEntityId + '%26CheckMode=' + sCheckMode;
                // aravi5 RMA-10586 Corprate financial screen is seen for carrier customers when clicked on back button of auto checks screen-CHROME Replaced &amp; with %26 in Query String Ends
                //zalam 04/28/2008 Mits-8645 End
                window.location.href = '/RiskmasterUI/UI/Funds/AutoCheckList.aspx?' + sQueryString;

            }
            return false;
        }
        //        function RedirectToFunds(TransId) {
        //            document.forms[0].all("TransId").value = TransId;
        //            document.forms[0].all("FromPaymentHistory").value = 1;
        //            document.forms[0].submit();
        //        }
        //Neha Mits 23601 Added a Back to claim button
        function LoadClaim() {
            var sClaimId = document.getElementById('ClaimId').value;
            var parentType = parent.MDIGetScreenInfo(window.frameElement.id, "PARENTSCREENTYPE").toLowerCase()

            //When payment screen is opened from a diary using attached record functionality then there is no way to navigate to claim
            if (parentType == "root") {
                parent.MDIShowScreen(sClaimId, 'claim');
                return false;
            }
        }
        //skhare7 r8 enhancement Combined Payment
        function BackToEntity() {

            var sURL = "entitymaint.aspx?recordID=" + document.forms[0].EntityId.value + "&action=Edit";

            window.location.href = sURL;
            return false;
        }
        function OpenOffset(TransId) {
            if (TransId == "0") {
                //alert(PaymentHistoryValidations.ValidFrozenAlert1 + " " + sClaimNumber + " " + PaymentHistoryValidations.ValidFrozenAlert2);
                alert("You are not allowed to offset payments.");
            }
            else {
                var sURL = "/RiskmasterUI/UI/Funds/Offset.aspx?TransId=" + TransId + "&SysCmd=0";
                // aravi5 changes for RMA-10237 Transcation history- offset- showmodal issue Starts
                //window.showModalDialog(sURL, "Modal Window", "dialogWidth:500px; dialogHeight:400px; dialogLeft:100px;resizable:yes");
                var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;  // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
                var isChrome = !!window.chrome && !isOpera; // Chrome 1+
                if (isChrome == true) {
                    popUpWin = parent.MDICreatePopUpWin(sURL, parent.MDIPopUpWinType.Search, window, 500, 400);
                    if (window.parent.parent.document.getElementById("overlaydiv") == null) {
                        $('#cphHeaderBody', window.parent.parent.document).prepend("<div id=\"overlaydiv\" class=\"overlay\">&nbsp;</div>");
                        $('.overlay', window.parent.parent.document).show();
                    }
                }
                else {
                    window.showModalDialog(sURL, "Modal Window", "dialogWidth:500px; dialogHeight:400px; dialogLeft:100px;resizable:yes");
                }
                // aravi5 changes for RMA-10237 Transcation history- offset- showmodal issue Ends
                window.location.reload();
                return false;
            }
        }

        function PageLoaded() {

            var parentType = parent.MDIGetScreenInfo(window.frameElement.id, "PARENTSCREENTYPE").toLowerCase();
            var btnLoadClaim = document.getElementById('btnBackToClaim');
            if ((parentType == "root") && ((document.forms[0].ClaimId.value != '') && (document.forms[0].ClaimId.value != '0'))) {
                if (btnLoadClaim != null) {
                    btnLoadClaim.style.display = "";
                }
            }
            else {
                if (btnLoadClaim != null) {
                    btnLoadClaim.style.display = "none";
                }
            }
        }
        function btnExport_click() {
            tableToExcel();
            return false;
        }
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
              , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
              , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
              , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function () {
                var headerWidth = $('#grdPaymentHistory_ctl00_Header colgroup').clone().html().replace(/<A[^>]*>[\S\s]*?<\/A>/g, "").replace(/<a[^>]*>[\S\s]*?<\/a>/g, "").replace(/<input[^>]*>|<\/input>/gi, "");
                var header = $('#grdPaymentHistory_ctl00_Header tr').clone().html().replace(/<A[^>]*>|<\/A>/g, "").replace(/<a[^>]*>|<\/a>/g, "").replace(/<input[^>]*>|<\/input>/gi, "");
                var table=headerWidth
                        + "<thead>" + header + "</thead></tbody>"
                        + $('#grdPaymentHistory_GridData').clone().html().replace(/<a[^>]*>|<\/a>/g, "").replace(/<input[^>]*>|<\/input>/gi, "")
                         .replace(/<img[^>]*>/gi, "").replace(/<div[^>]*>[\S\s]*?<\/div>/g, "");
                
                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");

                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                {
                    gridArea.document.open("txt/html", "replace");
                    gridArea.document.write("<table border='1px'>" + table.replace(/<table[^>]*>/gi, "<table border='1px'") + "</table>");
                    gridArea.document.close();
                    gridArea.focus();
                    sa = gridArea.document.execCommand("SaveAs", true, "Payment History WorkSheet.xls");
                }
                else {
                    var ctx = {
                        worksheet: 'Payment History WorkSheet', table: table
                    }
                    window.location.href = uri + base64(format(template, ctx))
                }
            }
        })()
    </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();PageLoaded();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
<asp:HiddenField runat="server" ID="hdFDHButton" /> <%--pgupta93: RMA-4608--%>

    <asp:TextBox Style="display: none" rmxref="/Instance/Document/PaymentHistory/ClaimId"
        ID="ClaimId" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="ClaimNumber" rmxref="/Instance/Document/PaymentHistory/ClaimNumber"
        runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="FrozenFlag" rmxref="/Instance/Document/PaymentHistory/FrozenFlag"
        runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="SubTitle" rmxref="/Instance/Document/PaymentHistory/SubTitle"
        runat="server"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="ClaimantEid" rmxref="/Instance/Document/PaymentHistory/ClaimantEid"
        runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="BOBMode" Text="0" runat="server"></asp:TextBox>

    <asp:TextBox Style="display: none" ID="ClaimantRowId" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="UnitId" rmxref="/Instance/Document/PaymentHistory/UnitId"
        runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="UnitRowId" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="SortCol" rmxref="/Instance/Document/PaymentHistory/SortCol"
        runat="server"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="Ascending" rmxref="/Instance/Document/PaymentHistory/Ascending"
        runat="server"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="FormTitle" rmxref="/Instance/Document/PaymentHistory/FormTitle"
        runat="server"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="PaymentsLessVoids" rmxref="/Instance/Document/PaymentHistory/PaymentsLessVoids"
        runat="server"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="TransId" rmxref="/Instance/Document/PaymentHistory/TransId"
        runat="server"></asp:TextBox>
    <%--Nadim for 16254(REM Customization)--%>
    <asp:TextBox Style="display: none" ID="ShowAutoCheckButton" rmxref="/Instance/Document/PaymentHistory/ShowAutoCheckbtn"
        runat="server"></asp:TextBox>
    <%--Nadim for 16254(REM Customization)--%>
    <asp:Label runat="server" class="ctrlgroup2" ID="lblcaption" Width="100%"></asp:Label>
    <%--nadim for 15388--%>
    <asp:TextBox runat="server" ID="Caption" Style="display: none" rmxref="/Instance/Document/PaymentHistory/Title"></asp:TextBox>
    <%--nadim for 15388--%>
     <%--skhare7 R8 enhancement--%>
  <asp:TextBox Style="display: none" ID="EntityId" rmxref="/Instance/Document/PaymentHistory/EntityId"
            runat="server" rmxignoreget="true"></asp:TextBox>
           
             <%--skhare7 R8 enhancement--%>
    <%--Start MITS 29834--%>
     <asp:TextBox Style="display: none" ID="txtNextPageNum" rmxref="/Instance/Document/PaymentHistory/NextPageNumber"
            runat="server" rmxignoreget="true"></asp:TextBox>  
     <asp:TextBox Style="display: none" ID="txtFilterColumn" rmxref="/Instance/Document/PaymentHistory/FilterColumn"
            runat="server" rmxignoreget="true"></asp:TextBox>  
     <asp:TextBox Style="display: none" ID="txtFilterValue" rmxref="/Instance/Document/PaymentHistory/FilterValue"
            runat="server" rmxignoreget="true"></asp:TextBox>  
    <%--End MITS 29834--%>

   <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
                   
                 
     <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
                  
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
         <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="grdPaymentHistory">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdPaymentHistory" LoadingPanelID="RadAjaxLoadingPanel1" />
                         <telerik:AjaxUpdatedControl ControlID="myHiddenVar" LoadingPanelID="RadAjaxLoadingPanel1" />
                          <telerik:AjaxUpdatedControl ControlID="labelcontrols" LoadingPanelID="RadAjaxLoadingPanel1" />
                          <%--Start MITS 29909--%>
                          <telerik:AjaxUpdatedControl ControlID="txtFilterValue" LoadingPanelID="RadAjaxLoadingPanel1" /> 
                          <%--End MITS 29909--%>
                          <%--Start mkaran2 - MITS 32340--%>
                           <telerik:AjaxUpdatedControl ControlID="SortCol" LoadingPanelID="RadAjaxLoadingPanel1" /> 
                           <telerik:AjaxUpdatedControl ControlID="Ascending" LoadingPanelID="RadAjaxLoadingPanel1" /> 
                            <%--End mkaran2 - MITS 32340--%>                          
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <input runat="server" type="hidden" id="myHiddenVar" enableviewstate = "true"/>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
         <%--Start averma62 MITS 26999--%>
         <%--Ashish Ahuja--Added width attribute--%>
        <telerik:RadToolTipManager ID="RadToolTipManager1" OffsetY="-1"  HideEvent="LeaveToolTip"
            Height="80" Width="200" runat="server" Animation="None" RelativeTo="Element"
            Position="MiddleRight">
        </telerik:RadToolTipManager>
         <%--End averma62 MITS 26999--%>

        <%--Ankit MITS 29834 : Removed hard-coded page size and VirtualItemCount proerpties from telerik grid. Both are now assigned at run time--%>
        <telerik:RadGrid ID="grdPaymentHistory" AllowFilteringByColumn="True" AllowCustomPaging="True" AllowPaging ="True" AllowSorting = "true"
           style="height:420px;" AllowCustomSorting = "true" runat="server" AutoGenerateColumns="false" 
            EnableLinqExpressions="false" Skin="Office2007"  OnItemCommand="grdPaymentHistory_ItemCommand" OnSortCommand ="grdPaymentHistory_Sorting" 
            OnNeedDataSource="grdPaymentHistory_NeedDataSource"  OnItemDataBound="grdPaymentHistory_ItemDataBound" OnPreRender="grdPaymentHistory_PreRender"
            OnPageIndexChanged = "grdPaymentHisory_PageIndexChanged" OnItemCreated="grdPaymentHisory_ItemCreated" >  
         <ClientSettings>                    
                                
          <Scrolling  AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True"  ScrollHeight="216px">
                </Scrolling>    
                
           <ClientEvents OnFilterMenuShowing="filterMenuShowing" />                     
                               
         </ClientSettings>
            <MasterTableView  AllowCustomSorting="true" Width="100%" TableLayout="Fixed">             
            
                <Columns>
                  
                    <telerik:GridBoundColumn DataField="CtlNumber" HeaderText="Control #" FilterControlWidth ="50px" UniqueName="CtlNumber"
                        SortExpression="CtlNumber">
                        <HeaderStyle Width="150px" /> 
                           <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TransNumber" FilterControlWidth ="50px" HeaderText="Check #" UniqueName="TransNumber" 
                         SortExpression="TransNumber">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="TransDate" FilterControlWidth ="50%" HeaderText="Trans Date"  UniqueName="TransDate"  SortExpression="TransDate">
                       <HeaderStyle Width="125px" /> <%--msampathkuma RMA-11601 --%>
                           <ItemStyle Width="125px" />
                          <FilterTemplate  >
                            <asp:Label ID="lblTransDateFrom" runat="server" Text="<%$ Resources:lblTransDateFrom %>"></asp:Label>
                            <telerik:RadDatePicker ID="txtFromOrderDatePicker" runat="server" Width="85px" ClientEvents-OnDateSelected="FromDateSelected"
                                  DbSelectedDate='<%# startDate %>'>
                                  <%--<DateInput DateFormat="MM/dd/yyyy">  
                                    </DateInput> --%>
                                  </telerik:RadDatePicker>
                                <br />
                            <asp:Label ID="lblTransDateTo" runat="server" Text="<%$ Resources:lblTransDateTo %>"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <telerik:RadDatePicker ID="txtToOrderDatePicker" runat="server" Width="85px" ClientEvents-OnDateSelected="ToDateSelected"
                                  DbSelectedDate='<%# endDate %>'>
                                  <%--<DateInput DateFormat="MM/dd/yyyy">  
                                    </DateInput> --%>
                                  </telerik:RadDatePicker>
                            <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

                                <script type="text/javascript">
                                    function FromDateSelected(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        var ToPicker = $find('<%# ((GridItem)Container).FindControl("txtToOrderDatePicker").ClientID %>');
                                        var fromDate = FormatSelectedDate(sender);
                                        var toDate = FormatSelectedDate(ToPicker);
										//rkulavil :RMA-12789/MITS 38521 starts
                                        PerformFilter(tableView, fromDate, toDate);
                                        //if (toDate != '') {
                                        //    tableView.filter("TransDate", fromDate + " " + toDate, "Between");
                                        //}
										//rkulavil :RMA-12789/MITS 38521 ends
                                    }
                                    function ToDateSelected(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        var FromPicker = $find('<%# ((GridItem)Container).FindControl("txtFromOrderDatePicker").ClientID %>');
                                        var fromDate = FormatSelectedDate(FromPicker);
                                        var toDate = FormatSelectedDate(sender);
										//rkulavil :RMA-12789/MITS 38521 starts
                                        PerformFilter(tableView, fromDate, toDate);
                                        //if (fromDate != '') {
                                        //    tableView.filter("TransDate", fromDate + " " + toDate, "Between");
                                        //}
										//rkulavil :RMA-12789/MITS 38521 ends
                                    }
                                    function FormatSelectedDate(picker) {
                                        var date = picker.get_selectedDate();
                                        var dateInput = picker.get_dateInput();
                                        var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());

                                        return formattedDate;
                                    }
									//rkulavil :RMA-12789/MITS 38521 starts
                                    function PerformFilter(tableView, fromDate, toDate) {
                                        if ((fromDate != "") && (toDate != ""))
                                            tableView.filter("TransDate", fromDate + " " + toDate, "Between");
                                        else if (fromDate != "")
                                            tableView.filter("TransDate", fromDate, "GreaterThanOrEqualTo");
                                        else if (toDate != "")
                                            tableView.filter("TransDate", toDate, "LessThanOrEqualTo");
                                        else
                                            tableView.filter("TransDate", fromDate, "NoFilter");
                                    }
									//rkulavil :RMA-12789/MITS 38521 ends
                                </script>

                            </telerik:RadScriptBlock>
                        </FilterTemplate>
                        
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PaymentFlag" HeaderText="Type" FilterControlWidth ="80%" UniqueName="PaymentFlag"
                        SortExpression="PaymentFlag" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="false">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ClearedFlag" HeaderText="Cleared?"  UniqueName="ClearedFlag" FilterControlWidth ="50px"
                       SortExpression="ClearedFlag" CurrentFilterFunction= "EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="False"  >
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>  <%-- mkaran2 - MITS 35712--%>
                    <telerik:GridBoundColumn  DataField="VoidFlag" HeaderText="Void?" UniqueName="VoidFlag" FilterControlWidth ="50px" 
                         SortExpression="VoidFlag" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="False"  >
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <%--Added by Swati Agarwal for MITS # 33431 WWIG--%>
                    <telerik:GridBoundColumn  DataField="StopPayFlag" HeaderText="Stop Pay?" UniqueName="StopPayFlag" FilterControlWidth ="50px"
                         SortExpression="StopPayFlag" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="False"  >
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                   <%-- End by Swati--%>
                    <telerik:GridBoundColumn DataField="CheckStatus" HeaderText="Status" FilterControlWidth ="70px"   UniqueName="CheckStatus"
                        SortExpression="CheckStatus"  CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="False"  >
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                   <%--Start averma62 MITS 26999--%>
                 <%--   <telerik:GridBoundColumn DataField="Name" HeaderText="Payee" UniqueName="Name" FilterControlWidth ="50px" 
                        SortExpression="Name">
                    </telerik:GridBoundColumn>--%>
                     <telerik:GridTemplateColumn HeaderText="Payee" FilterControlWidth ="50px" DataField="Name" UniqueName ="Name" SortExpression="Name">
                         <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbltaskSubject"  runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name")%>'></asp:Label>                                        
                                        <br />
                             <asp:Image runat="server" ID="btnEntity" ImageUrl="~/Images/secUsersNode.gif" Visible="false" />

                                        <telerik:RadToolTip ID="RadToolTip2" runat="server" TargetControlID="btnEntity" Width="200px"  Height="80px"
                                                  RelativeTo="Element" Position="MiddleRight" Animation="None"  OffsetY="-1" HideEvent="LeaveToolTip">
                                                  <%# DataBinder.Eval(Container, "DataItem.Payees_Name") %>
                                             </telerik:RadToolTip>
                                    </ItemTemplate> 
                     </telerik:GridTemplateColumn> 
                     <%--End averma62 MITS 26999--%>
                      <%-- Ashish Ahuja Mits 34270 start --%>
                     <telerik:GridTemplateColumn DataField="CheckMemo" HeaderText="Check Memo" FilterControlWidth ="50px" UniqueName="CheckMemo"
                        SortExpression="CheckMemo" >
                         <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                            <ItemTemplate>
                                        <asp:Label ID="lblCheckMemo"  runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CheckMemo")%>'></asp:Label>                                        
                                        <br />
                             <asp:Image runat="server" ID="btnCheckMemo" ImageUrl="~/Images/down.gif" Visible="false"/>
                                <telerik:RadToolTip ID="RadToolTip1" runat="server" TargetControlID="btnCheckMemo" Width="200px"  Height="80px"
                                                  RelativeTo="Element" Position="MiddleRight" Animation="None"  OffsetY="-1" HideEvent="LeaveToolTip">
                                                      <%# DataBinder.Eval(Container, "DataItem.CheckMemoToolTip") %>
                                             </telerik:RadToolTip>
                                    </ItemTemplate> 
                    </telerik:GridTemplateColumn>
                    <%-- Ashish Ahuja Mits 34270 end--%>
                    <telerik:GridBoundColumn DataField="Amount"  HeaderText="Check Amount" UniqueName="Amount" FilterControlWidth ="50px" DataFormatString="{0:C}"  
                        SortExpression="Amount">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FromDate" HeaderText="From Date" UniqueName="FromDate" FilterControlWidth ="50px" 
                         SortExpression="FromDate">
                        <HeaderStyle Width="100px" />
                         <FilterTemplate>
                         <telerik:RadDatePicker ID="txtFromPicker" runat="server" Width="85px" ClientEvents-OnDateSelected="MiscSelectedFromDate"
                                  DbSelectedDate='<%# fsFromDate %>'>
                                 <%-- <DateInput DateFormat="MM/dd/yyyy">  
                                    </DateInput> --%>
                                  </telerik:RadDatePicker>
                                  <telerik:RadScriptBlock ID="RadScriptBlock4" runat="server">
                                  <script type="text/javascript">
                                      function MiscSelectedFromDate(sender, args) {
                                          var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                          var datPicker = $find('<%# ((GridItem)Container).FindControl("txtFromPicker").ClientID %>');
                                          var myDate = FormSelectedDate(datPicker);
                                          tableView.filter("FromDate", myDate, "EqualTo");
                                      }
                                      function FormSelectedDate(picker) {
                                          var date = picker.get_selectedDate();
                                          var dateInput = picker.get_dateInput();
                                          var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                                          return formattedDate;
                                      }
                                </script>
                            </telerik:RadScriptBlock>
                         </FilterTemplate>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ToDate" HeaderText="To Date" UniqueName="ToDate" FilterControlWidth ="50px" 
                        SortExpression="ToDate">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                        <FilterTemplate>
                        <telerik:RadDatePicker ID="txtToPicker" runat="server" Width="85px" ClientEvents-OnDateSelected="MiscSelectedToDate"
                                  DbSelectedDate='<%# fsToDate %>'>
                       <%-- <DateInput DateFormat="MM/dd/yyyy">  
                        </DateInput> --%>
                        </telerik:RadDatePicker>
                        <telerik:RadScriptBlock ID="RadScriptBlock5" runat="server">
                                  <script type="text/javascript">
                                      function MiscSelectedToDate(sender, args) {
                                          var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                          var datPicker = $find('<%# ((GridItem)Container).FindControl("txtToPicker").ClientID %>');
                                          var myDate = FormSelectedDate(datPicker);
                                          tableView.filter("ToDate", myDate, "EqualTo");
                                      }
                                      function FormSelectedDate(picker) {
                                          var date = picker.get_selectedDate();
                                          var dateInput = picker.get_dateInput();
                                          var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                                          return formattedDate;
                                      }
                                </script>
                            </telerik:RadScriptBlock>
                         </FilterTemplate>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="InvoiceNumber" HeaderText="Invoice #" UniqueName="InvoiceNumber" FilterControlWidth ="50px" 
                      SortExpression="InvoiceNumber">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodeDesc" HeaderText="Transaction Type" UniqueName="CodeDesc" FilterControlWidth ="50px" 
                       SortExpression="CodeDesc">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SplitAmount" HeaderText="Split Amount" UniqueName="SplitAmount" FilterControlWidth ="50px" DataFormatString="{0:C}"
                      SortExpression="SplitAmount">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="User" HeaderText="User" UniqueName="User" FilterControlWidth ="50px" 
                      SortExpression="User">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CheckDate" HeaderText="Check Date" UniqueName="CheckDate" FilterControlWidth ="50px" 
                        SortExpression="CheckDate">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                        <FilterTemplate >
                        <telerik:RadDatePicker ID="txtCheckDatePicker" runat="server" Width="85px" ClientEvents-OnDateSelected="MiscSelectedDate"
                                  DbSelectedDate='<%# checkDate %>'>

                        <%--<DateInput DateFormat="MM/dd/yyyy">  
                        </DateInput> --%>
                        </telerik:RadDatePicker>
                        <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
                                <script type="text/javascript">
                                    function MiscSelectedDate(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        var datPicker = $find('<%# ((GridItem)Container).FindControl("txtCheckDatePicker").ClientID %>');
                                        var myDate = FormSelectedDate(datPicker);
                                        tableView.filter("CheckDate", myDate, "EqualTo");
                                    }
                                    function FormSelectedDate(picker) {
                                        var date = picker.get_selectedDate();
                                        var dateInput = picker.get_dateInput();
                                        var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                                        return formattedDate;
                                    }
                                </script>
                            </telerik:RadScriptBlock>
                        </FilterTemplate>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IsFirstFinal" HeaderText="First & Final Payment?" FilterControlWidth ="50px"
                        UniqueName="IsFirstFinal" SortExpression="IsFirstFinal" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="False">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <%--//skhare7 R8 combined Payment--%>
                    <telerik:GridBoundColumn DataField="CombinedPay" HeaderText="Combined Payment?"  UniqueName="CombinedPay" FilterControlWidth ="50px"
                       SortExpression="CombinedPay" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="False">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <%--<%--Amitosh R8 Coverage and policy added to Payment History--%>
                <telerik:GridBoundColumn DataField="PolicyName" HeaderText="Policy Name" UniqueName="PolicyName" FilterControlWidth ="50px" 
                      SortExpression="PolicyName"> <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                </telerik:GridBoundColumn>
               
                      <telerik:GridBoundColumn DataField="CoverageType" HeaderText="Coverage Type" UniqueName="CoverageType" FilterControlWidth ="50px" 
                       SortExpression="CoverageType"><HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" /> </telerik:GridBoundColumn>

                
                                       <telerik:GridBoundColumn DataField="Unit" HeaderText="Unit" UniqueName="Unit" FilterControlWidth ="50px" 
                      SortExpression="Unit" AllowFiltering="false" ><HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" /> </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="LossType" HeaderText="Loss Type" UniqueName="LossType" FilterControlWidth ="50px" 
                                        SortExpression="LossType" ><HeaderStyle Width="100px" /> <ItemStyle Width="100px"/>  </telerik:GridBoundColumn>
                        <%--Amitosh R8 Coverage and policy added to Payment History--%>
              
                    <telerik:GridBoundColumn DataField="TransId" HeaderText="TransId" UniqueName="TransId"  SortExpression="TransId" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NoOfPayees" HeaderText="NoOfPayees" UniqueName="NoOfPayees"  SortExpression="NoOfPayees" Visible="false">
                    </telerik:GridBoundColumn>
                    
                     <%--Ankit Start : Financials Enhancements - Manual Check Changes--%>
                    <telerik:GridBoundColumn DataField="ManualCheck" HeaderText="Manual Check"  UniqueName="ManualCheck" FilterControlWidth ="50px"
                       SortExpression="ManualCheck" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="False">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                     <%--Ankit End--%>

                    <%--mcapps2 MITS 30236 Start--%>
                    <%--pgupta61 MITS 35247 -added attributes ShowFilterIcon,CurrentFilterFunction,AutoPostBackOnFilter to Offset field--%>
                    <telerik:GridBoundColumn DataField="Offset" HeaderText="Offset" FilterControlWidth ="50px" UniqueName="Offset"
                        SortExpression="Offset"  CurrentFilterFunction= "EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="False">  <%-- mkaran2 - MITS 35712--%>
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="InvoiceAmount"  HeaderText="Invoice Amount" UniqueName="InvoiceAmount" FilterControlWidth ="50px" DataFormatString="{0:C}"  
                        SortExpression="InvoiceAmount">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>                   
   <%--Added by Nikhil on 07/29/14.Check Total  in payment history configuration--%>
                      <telerik:GridBoundColumn DataField="CheckTotal"  HeaderText="Check Total" UniqueName="CheckTotal" FilterControlWidth ="50px" DataFormatString="{0:C}"  
                        SortExpression="CheckTotal">
                          <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                           </telerik:GridBoundColumn>        
                    <%--End -Added by Nikhil on 07/29/14.Check Total  in payment history configuration--%>        
                    <telerik:GridBoundColumn DataField="TransId2" HeaderText="TransId2" UniqueName="TransId2"  SortExpression="TransId2" Visible="false">
                    </telerik:GridBoundColumn>


                      <%--JIRA 7810 Start Snehal--%>
                    <telerik:GridBoundColumn DataField="HoldReason" HeaderText="Hold Reason" FilterControlWidth ="70px"   UniqueName="HoldReason"
                        SortExpression="HoldReason"  AllowFiltering="false"  >
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                      <%--JIRA 7810 End--%>
                    <%--mcapps2 MITS 30236 End--%>
                    <telerik:GridBoundColumn DataField="ReserveType" HeaderText="Reserve Type" FilterControlWidth ="50px"   UniqueName="ReserveType"
                        SortExpression="ReserveType" DataType="System.String">
                        <HeaderStyle Width="100px" /> 
                           <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                </Columns>
                   <HeaderStyle CssClass="msgheader" />
            </MasterTableView>
            <PagerStyle  Mode="NextPrevAndNumeric" FirstPageToolTip="<%$ Resources:lblFirst %>" LastPageToolTip="<%$ Resources:lblLast %>"  PrevPageToolTip="<%$
             Resources:lblPrev %>"  NextPageToolTip="<%$ Resources:lblNext %>" Position="Top"  AlwaysVisible ="true"  />
                
        </telerik:RadGrid>
           
    <table width="99%" runat ="server" id="labelcontrols">
        <tr>
            <td align="right">
                <asp:Label ID="lblTotalPayments" runat="server" Text="<%$ Resources:lblTotalPaymentsResrc %>" />
                &nbsp;<asp:Label runat="server" ID="TotalAll" RMXRef="/Instance/Document/PaymentHistory/TotalPay"></asp:Label>
                <!-- MITS 14855 Raman Bhatia -->
                &nbsp;&nbsp;&nbsp;&nbsp; 
                <asp:Label ID="lblTotalCollections" runat="server" Text="<%$ Resources:lblTotalCollectionsResrc %>" />
                &nbsp;<asp:Label ID="TotalCollect" rmxref="/Instance/Document/PaymentHistory/TotalCollect" runat="server"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp; 
                <asp:Label ID="lblNetTotal" runat="server" Text="<%$ Resources:lblNetTotalResrc %>" />
                &nbsp;<asp:Label ID="TotalPay" rmxref="/Instance/Document/PaymentHistory/TotalAll" runat="server"></asp:Label>
                <!-- MITS 14855 Raman Bhatia -->
                &nbsp;&nbsp;&nbsp;&nbsp; 
                <asp:Label ID="lblTotalVoids" runat="server" Text="<%$ Resources:lblTotalVoidsResrc %>" />
                &nbsp;<asp:Label ID="TotalVoid" rmxref="/Instance/Document/PaymentHistory/TotalVoid" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button runat="server" ID="btnBackToFinancials" OnClientClick="return BackToFinancials();" Text="<%$ Resources:btnBackToFinancialsResrc %>" class="button" style="border:2px outset buttonface;"/>
    <asp:Button runat="server" ID="btnAutoChecks" OnClientClick="return AutoCheckList();" Text="<%$ Resources:btnAutoChecksResrc %>" class="button" style="border:2px outset buttonface;"/>
    <asp:Button runat="server" ID="btnPrint" OnClientClick="return ShowReport();" Text="<%$ Resources:btnPrintResrc %>" class="button" style="border:2px outset buttonface;"/>
    <asp:Button runat="server" ID="btnExortToExcel" OnClientClick="return btnExport_click();" Text="<%$ Resources:btnExortToExcel %>" class="button" style="border:2px outset buttonface;"/>
        <iframe id="gridArea" style="display:none"></iframe>
                <%--skhare7 R8 Enhancement--%>
          <asp:Button runat="server" ID="btnBackToEntity"  Text="<%$ Resources:btnBackToEntityResrc %>" OnClientClick="return BackToEntity();" class="button" style="border:2px outset buttonface;"/>
          <%--End--%>

    <asp:Button runat="server" class="button" Text="Back to Claim" ID="btnBackToClaim"
        Visible="false" OnClientClick="return LoadClaim();" />        
        <input type="hidden" id="hdnFilterExpression" /> <!-- mkaran2 - MITS 32340 -->           
    </form>
</body>
</html>
	