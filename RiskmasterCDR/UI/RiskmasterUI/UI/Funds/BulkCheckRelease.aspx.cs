﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Funds
{
    /// <summary>
    /// Class to handle the UI functionality for the Bulk Check Release Enhancement
    /// Author: Animesh Sahai 
    /// Date: 22-Jan-2010
    /// </summary>
    public partial class BulkCheckRelease : NonFDMBasePageCWS
    {
        public XmlDocument Model = null;
        string MergeMessageTemplate = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function></Function></Call><Document><BulkCheckRelease><AllClients></AllClients><SortExpression></SortExpression><SortDirection></SortDirection></BulkCheckRelease></Document></Message>";
        string sCWSresponse = "";
        private string m_SortDirection = "";
        private string m_strSortExp = "";
        /// <summary>
        /// Page Load event of the Bulk Check Release Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("BulkCheckRelease.aspx"), "BulkCheckReleaseValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "BulkCheckReleaseValidationScripts", sValidationResources, true);
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                if (IsPostBack)
                {
                    if (hdnSearchHit.Value == "true")
                    {
                        Model = new XmlDocument();
                        XElement XmlTemplate = null;
                        XmlTemplate = XElement.Parse(MergeMessageTemplate);
                        CallCWS("FundManagementAdaptor.GetBulkCheckData", XmlTemplate, out sCWSresponse, true, false);
                        Model.LoadXml(sCWSresponse);
                        BindpageControls(sCWSresponse);                        
                        DivDefault.Visible = false;
                        DivProcess.Visible = true;
                        DivMessage.Visible = true;
                    }
                }
                else
                {
                    Model = new XmlDocument();
                    XElement XmlTemplate = null;
                    XmlTemplate = XElement.Parse(MergeMessageTemplate);
                    CallCWS("FundManagementAdaptor.GetInitialScreenData", XmlTemplate, out sCWSresponse, true, true);
                    DivDefault.Visible = true;
                    DivProcess.Visible = false;
                    DivMessage.Visible = false;   
                }
                hdnSearchHit.Value = "";  
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Method to verify the Screen XML
        /// </summary>
        protected void FillSearchConditions()
        {
            try
            {
                XElement oElement = XElement.Parse(MergeMessageTemplate);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Method to bind the search result to the Datagrid control
        /// Author: Animesh Sahai
        /// Date: 2/5/2010
        /// </summary>
        /// <param name="sreturnValue">string that contains the data XML</param>
        private void BindpageControls(string sreturnValue)
        {
            DataSet usersRecordsSet = null;
            try
            {
                XmlDocument usersXDoc = new XmlDocument();
                usersXDoc.LoadXml(sreturnValue);
                usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);
                if (usersRecordsSet != null)
                {
                    if (usersRecordsSet.Tables.Count > 0)
                    {
                        gvSelectPayments.DataSource = usersRecordsSet.Tables[0].DefaultView;
                        gvSelectPayments.DataBind();
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        usersRecordsSet.Tables.Add(dt);
                        gvSelectPayments.DataBind();
                    }
                }
                //Binding the Listbox control.
                //****************************
                lstTransTypes_multicode.Items.Clear();
                XmlElement objElem = (XmlElement)usersXDoc.SelectSingleNode("//ListItem");
                if (objElem != null)
                {
                    lstTransTypes_multicode.Items.Clear();  
                    foreach (XmlNode objNode in usersXDoc.SelectNodes("//ListItem/Code"))
                    {
                        lstTransTypes_multicode.Items.Add(new ListItem(objNode.Attributes["description"].Value,objNode.Attributes["id"].Value));        
                    }
                }
                //****************************
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
            finally
            {
                if (usersRecordsSet != null)
                {
                    usersRecordsSet.Dispose();
                    usersRecordsSet = null;
                }
            }
        }

        /// <summary>
        /// Method to extract the search result.
        /// </summary>
        /// <param name="diaryDoc">Result returned from the business layer based on the search criteria</param>
        /// <returns>returns the Dataset obtained from the supplied XML</returns>
        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            double getTotAmount = 0;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(diaryDoc.SelectSingleNode("//bulkcheckgrid")));
                //Start ddhiman 12012010
                //Total Unselected Amount Functionality
                XmlNodeList xNodeList = diaryDoc.GetElementsByTagName("Payment");
                foreach (XmlElement xEleAmount in xNodeList)
                {
                    getTotAmount = getTotAmount + Conversion.ConvertObjToDouble((xEleAmount.Attributes["amount"].Value));
                }
               // txtUnsAmount.Text = "$" + getTotAmount.ToString();
                txtUnsAmount.Text = getTotAmount.ToString();
                hdnUnselectedAmount.Value = getTotAmount.ToString();
                //End ddhiman
                return dSet;
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return null;
            }
        }
        protected void gvSelectPayments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
		}
        protected void gvSelectPayments_Sorting(object sender, GridViewSortEventArgs e)
        {
            string exp = string.Empty;  
            m_strSortExp = e.SortExpression;
            exp = e.SortExpression;
            if (exp == hdnSortExpression.Value)
            {
                exp = (12 + Conversion.ConvertObjToInt(exp)).ToString();
            }
            if (exp != "")
                hdnSortExpression.Value = exp;
            else
                exp = hdnSortExpression.Value;
            if (Conversion.ConvertStrToInteger(exp) >= 1 && Conversion.ConvertStrToInteger(exp) <= 12)
            {
                m_SortDirection = "Ascending";
            }
            else
            {
                m_SortDirection = "Descending";
            }
            StringBuilder sXml = new StringBuilder();
            sXml= sXml.Append ("<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function></Function></Call>");
            sXml= sXml.Append ("<Document><BulkCheckRelease><AllClients></AllClients>");
            sXml= sXml.Append("<SortExpression>");
            sXml= sXml.Append(m_strSortExp);
            sXml= sXml.Append("</SortExpression><SortDirection>");
            sXml = sXml.Append(m_SortDirection);
            sXml= sXml.Append("</SortDirection></BulkCheckRelease></Document></Message>");
            Model = new XmlDocument();
            XElement XmlTemplate = null;
            XmlTemplate = XElement.Parse(sXml.ToString());
            CallCWS("FundManagementAdaptor.GetBulkCheckData", XmlTemplate, out sCWSresponse, true, false);
            Model.LoadXml(sCWSresponse);
            BindpageControls(sCWSresponse);
        }
    }
}
