﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Funds
{
    public partial class InsufficientReserves : System.Web.UI.Page
    {
        XElement oMessageElement = null;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("InsufficientReserves.aspx"), "InsufficientReservesValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "InsufficientReservesValidationScripts", sValidationResources, true);

            if (!IsPostBack)
            {
                ProcessReserves();
            }
        }

        private void ProcessReserves()
        {
            XmlDocument objXml = null;
            XElement objTempElement = null;
            oMessageElement = GetMessageTemplate();

            if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("SessionId")))
            {
                objTempElement = oMessageElement.XPathSelectElement("./Authorization");
                objTempElement.Value = AppHelper.ReadCookieValue("SessionId");
            }

            objXml = GetInsufficientReserves();

            XElement objInsufficientReserves = XElement.Parse(objXml.OuterXml.ToString());

            AssignReserveValues(objInsufficientReserves);

            hdInsuffResXml.Value = Server.HtmlEncode(objInsufficientReserves.ToString());
        }

        private XmlDocument GetInsufficientReserves()
        {
            XmlDocument objXml = new XmlDocument();

            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sReturn);
            XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");

            objXml.LoadXml(oInstanceNode.InnerXml);
            return objXml;
        }

        private void AssignReserveValues(XElement p_objInsufficientReserves)
        {
            var objInSuffRes = from InSufficientReserve in p_objInsufficientReserves.Descendants("insufres")

                               select InSufficientReserve;

            if (objInSuffRes.Count() != 0)
            {
                //Deb Multi Currency
                ClaimCurrency.Text = p_objInsufficientReserves.Element("./insufres/ClaimCurrency").Value;
                if (!string.IsNullOrEmpty(ClaimCurrency.Text))
                {
                    Culture = ClaimCurrency.Text;
                    UICulture = ClaimCurrency.Text;
                }
                lblCurrentBalance.Text = Conversion.ConvertStrToDouble(p_objInsufficientReserves.XPathSelectElement("./insufres/balance").Value).ToString("c");
                txtres.Text = p_objInsufficientReserves.XPathSelectElement("./insufres/reserve").Value;
                txtsetres.Text = p_objInsufficientReserves.XPathSelectElement("./insufres/setreserve").Value;
                lblCurrentReserve.Text = Conversion.ConvertStrToDouble(p_objInsufficientReserves.XPathSelectElement("./insufres/reserve").Value).ToString("c");
                lblPaymentAmount.Text = Conversion.ConvertStrToDouble(p_objInsufficientReserves.XPathSelectElement("./insufres/payment").Value).ToString("c");
                txtaddres.Text = p_objInsufficientReserves.XPathSelectElement("./insufres/addreserve").Value;
                lblReserveType.Text = p_objInsufficientReserves.XPathSelectElement("./insufres/rcode").Value;
                isreslimit.Value = p_objInsufficientReserves.Element("./insufres/isreslimit").Value;
                reslimit.Value = p_objInsufficientReserves.Element("./insufres/reslimit").Value;
                // Jira 6385- Incurred Limit starts
                isclminclimit.Value = p_objInsufficientReserves.Element("./insufres/isclminclimit").Value;
                clminclimit.Value = p_objInsufficientReserves.Element("./insufres/clminclimit").Value;
                currclminclimit.Value = p_objInsufficientReserves.Element("./insufres/currclminclimit").Value;
                // Jira 6385- Incurred Limit ends
                //Deb Multi Currency
            }
            else
            {
                //Deb Multi Currency
                ClaimCurrency.Text = p_objInsufficientReserves.Element("ClaimCurrency").Value;
                if (!string.IsNullOrEmpty(ClaimCurrency.Text))
                {
                    Culture = ClaimCurrency.Text;
                    UICulture = ClaimCurrency.Text;
                }
                lblCurrentBalance.Text = Conversion.ConvertStrToDouble(p_objInsufficientReserves.Element("balance").Value).ToString("c");
                txtres.Text = p_objInsufficientReserves.Element("reserve").Value;
                txtsetres.Text = p_objInsufficientReserves.Element("setreserve").Value;
                lblCurrentReserve.Text = Conversion.ConvertStrToDouble(p_objInsufficientReserves.Element("reserve").Value).ToString("c");
                lblPaymentAmount.Text = Conversion.ConvertStrToDouble(p_objInsufficientReserves.Element("payment").Value).ToString("c");
                txtaddres.Text = p_objInsufficientReserves.Element("addreserve").Value;
                lblReserveType.Text = p_objInsufficientReserves.Element("rcode").Value;
                isreslimit.Value = p_objInsufficientReserves.Element("isreslimit").Value;
                reslimit.Value = p_objInsufficientReserves.Element("reslimit").Value;
                // Jira 6385- Incurred Limit starts
                isclminclimit.Value = p_objInsufficientReserves.Element("isclminclimit").Value;
                clminclimit.Value = p_objInsufficientReserves.Element("clminclimit").Value;
                currclminclimit.Value = p_objInsufficientReserves.Element("currclminclimit").Value;
                // Jira 6385- Incurred Limit ends
                //Deb Multi Currency
            }
        }

        protected void ModifyReserves(object sender, EventArgs e)
        {
            XmlDocument objXml = null;
            XElement objInsufficientReserves = null;
            XElement objTempElement = null;

            if (Request.Form["hdInsuffResXml"] != null)
                objInsufficientReserves = XElement.Parse(Server.HtmlDecode(Request.Form["hdInsuffResXml"]));

            oMessageElement = GetModifyReserveXml();

            if (Request.Form["reason"] != null)
            {
                objTempElement = oMessageElement.XPathSelectElement("./Document/ReserveFunds/Reason");
                objTempElement.Value = Request.Form["reason"].ToString();
            }

            if (HttpContext.Current.Request.Cookies["SessionId"] != null)
            {
                objTempElement = oMessageElement.XPathSelectElement("./Authorization");
                objTempElement.Value = HttpContext.Current.Request.Cookies["SessionId"].Value;
            }

            XElement objXmlForInsReserveAmount = oMessageElement.XPathSelectElement("./Document/ReserveFunds/XmlForInsReserveAmount");
            objXmlForInsReserveAmount.Add(new XElement(objInsufficientReserves));


            objTempElement = objXmlForInsReserveAmount.XPathSelectElement("./insufres/newamount");
            objTempElement.Value = Request.Form["newamount"].ToString();


            objXml = GetInsufficientReserves();

            XElement objModifiedReserves = XElement.Parse(objXml.OuterXml.ToString());

            ProcessReserves();
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <Message>
                    <Authorization></Authorization> 
                        <Call>
                            <Function>FundManagementAdaptor.GetInsuffReserve</Function> 
                        </Call>
                    <Document>
				        <info></info>
				        <SetReserveAmount></SetReserveAmount>
				        <AddReserveAmount></AddReserveAmount>
				        <NewAmount></NewAmount>
				        <Reason></Reason>
				        <action />
                    </Document>
                </Message>
            ");

            return oTemplate;
        }

        private XElement GetModifyReserveXml()
        {
            XElement objModifyReserveXml = XElement.Parse(@"
                <Message>
                    <Authorization></Authorization> 
                        <Call>
                            <Function>ReserveFundsAdaptor.GetXmlForInsReserve</Function> 
                        </Call>
                    <Document>
						<ReserveFunds>
							<ClaimNumber /> 
							<ParentSecurityId /> 
							<SecurityId /> 
							<XmlForInsReserveAmount>
							</XmlForInsReserveAmount>
							<Reason></Reason>
						</ReserveFunds>
                    </Document>
                </Message>
            ");

            return objModifyReserveXml;
        }
    }
}
