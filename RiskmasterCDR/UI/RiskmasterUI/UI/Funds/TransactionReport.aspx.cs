﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.UI.UI.Funds
{
    public partial class TransactionReport : System.Web.UI.Page
    {
        XElement objTempElement = null;
        //public XmlDocument Model = null;
        private string sTransactionReportTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>FundManagementAdaptor.CreateTransReport</Function></Call><Document><TransactionReport><TransId></TransId></TransactionReport></Document></Message>";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    string sTransId = AppHelper.GetQueryStringValue("TransId");
                    sTransactionReportTemplate = AppHelper.ChangeMessageValue(sTransactionReportTemplate, "//TransId", sTransId);
                    string sReturn = AppHelper.CallCWSService(sTransactionReportTemplate.ToString());
                    objTempElement = XElement.Parse(sReturn);
                    objTempElement = objTempElement.XPathSelectElement("//TransactionReport/File");
                    if (objTempElement != null)
                    {
                        if (objTempElement.Value != "")
                        {
                            byte[] pdfbytes = Convert.FromBase64String(objTempElement.Value);
                            if (pdfbytes != null)
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Charset = "";
                                Response.AppendHeader("Content-Encoding", "none;");
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("Content-Disposition", string.Format("attachment; filename=TransactionDetails.pdf"));
                                Response.AddHeader("Accept-Ranges", "bytes");
                                Response.BinaryWrite(pdfbytes);
                                Response.Flush();
                                Response.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }
    }
}
