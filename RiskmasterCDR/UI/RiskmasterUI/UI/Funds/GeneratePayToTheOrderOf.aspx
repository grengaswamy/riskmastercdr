﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneratePayToTheOrderOf.aspx.cs" Inherits="Riskmaster.UI.UI.Funds.GeneratePayToTheOrderOf" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Generate Pay To The Order Of</title>
    <script type="text/JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/JavaScript" src="../../../Scripts/Utilities.js"></script>
    <script type="text/javascript">
        function SubmitClick() {
            var objParentPaytoTheOrder = eval("window.opener.document.forms[0].paytotheorder");
            //var hdParentPayeeListForGeneration = window.opener.document.getElementById('PayeeListForGeneration');
            var lblPreviewText = document.getElementById("lblPreviewText");

            if (lblPreviewText.textContent == "")
                PreviewClick();

            // objParentPaytoTheOrder.value = lblPreviewText.innerHTML;
            objParentPaytoTheOrder.value = lblPreviewText.textContent; //JIRA RMA-9254 nshah28
            window.close()
        }

        function PreviewClick() {
            var objPreviewText = '';
            var lblPreviewText = document.getElementById("lblPreviewText");
            var objGridView = document.getElementById("gvPayToTheOrderOf");
            var tbody = objGridView.tBodies[0];

            var row = '';
            var cPayee = '';
            var cPayeePhraseLocation = '';
            var cPayeePhrase = '';

            //Start : For Saving
            var objParentHiddenSavePayeePhrase = eval("window.opener.document.forms[0].SavePayeePhrase");
            var cPayeeEId = '';
            var cPayeePhraseLocationVal = '0';
            var cPayeePhraseVal = '';
            var objSavingText = '';
            //End

            //Start : For Setting DropDownList
            var btnGeneratePayToTheOrderOf = eval("window.opener.document.forms[0].btngeneratepaytotheorderof");
            var strGeneratePayToTheOrderOfTagVal = '';
            //End

            var cPayeePhraseLocationText = '';
            var cPayeePhraseText = '';

            for (var i = 1; i < tbody.rows.length; i++) {
                row = tbody.rows[i];
                var j = 0;
                if (navigator.appName.indexOf("Microsoft")==-1)//for other than IE browser
                    j = 1;
                cPayee = row.cells[0].innerHTML;
                cPayeePhraseLocation = row.cells[1].childNodes[j];
                cPayeePhrase = row.cells[2].childNodes[j];

                //Start : For Saving
                cPayeeEId = row.cells[3].childNodes[j].value;
                cPayeePhraseVal = cPayeePhrase.options[cPayeePhrase.selectedIndex].value;
                //End

                cPayeePhraseLocationText = cPayeePhraseLocation.options[cPayeePhraseLocation.selectedIndex].text;
                cPayeePhraseText = cPayeePhrase.options[cPayeePhrase.selectedIndex].text;

                switch (cPayeePhraseLocationText.toUpperCase())
                {
                    case 'AFTER':
                        //Start : For Saving
                        cPayeePhraseLocationVal = '0';
                        //End
                        if (objPreviewText != '')
                            objPreviewText = objPreviewText + ' ' + cPayee + ' ' + cPayeePhraseText;
                        else
                            objPreviewText = cPayee + ' ' + cPayeePhraseText;
                        break;
                    case 'BEFORE':
                        //Start : For Saving
                        cPayeePhraseLocationVal = '-1'
                        //End
                        if (objPreviewText != '')
                            objPreviewText = objPreviewText + ' ' + cPayeePhraseText + ' ' + cPayee;
                        else
                            objPreviewText = cPayeePhraseText + ' ' + cPayee;
                        break;
                }

                //Start : For Saving
                if (objSavingText != '')
                    objSavingText = objSavingText + '@' + cPayeeEId + '|' + cPayeePhraseVal + '|' + cPayeePhraseLocationVal
                else
                    objSavingText = cPayeeEId + '|' + cPayeePhraseVal + '|' + cPayeePhraseLocationVal
                //End

                //Start : For Setting DropDownList
                if (strGeneratePayToTheOrderOfTagVal != '')
                    strGeneratePayToTheOrderOfTagVal = strGeneratePayToTheOrderOfTagVal + '@' + cPayeePhraseLocationVal + '|' + cPayeePhraseVal;
                else
                    strGeneratePayToTheOrderOfTagVal = cPayeePhraseLocationVal + '|' + cPayeePhraseVal;
                //End
            }
            //Start : For Saving
            objParentHiddenSavePayeePhrase.value = objSavingText;
            //End
            //Start : For Setting DropDownList
            btnGeneratePayToTheOrderOf.Tag = strGeneratePayToTheOrderOfTagVal;
            //End
            lblPreviewText.innerHTML = objPreviewText;
        }

        function PageLoaded() {
            var objParentbtngeneratepaytotheorderof = eval("window.opener.document.forms[0].btngeneratepaytotheorderof");
            var objGridView = document.getElementById("gvPayToTheOrderOf");
            var tbody = objGridView.tBodies[0];
            var row = '';
            var objPayeePhraseLocation = '';
            var objPayeePhrase = '';
            //MITS 35314
            if (objParentbtngeneratepaytotheorderof != null && objParentbtngeneratepaytotheorderof.Tag != null) {
                var arrDDLsVal = objParentbtngeneratepaytotheorderof.Tag.split("@");
                var arrDDLVal = null;

                if (arrDDLsVal.length == tbody.rows.length - 1) {
                    for (var i = 1; i < tbody.rows.length; i++) {
                        arrDDLVal = arrDDLsVal[i - 1].split('|');
                        row = tbody.rows[i];
                        var j = 0;
                        if (navigator.appName.indexOf("Microsoft") == -1)//for other than IE browser
                            j = 1;
                        objPayeePhraseLocation = row.cells[1].childNodes[j];
                        objPayeePhrase = row.cells[2].childNodes[j];

                        SetSelectedIndex(objPayeePhraseLocation, arrDDLVal[0]);
                        SetSelectedIndex(objPayeePhrase, arrDDLVal[1]);
                    }
                }
            }
        }

        function SetSelectedIndex(dropdownlist, sVal) {
            if (dropdownlist != null) {
                for (i = 0; i < dropdownlist.length; i++) {
                    if (dropdownlist.options[i].value == sVal) {
                        dropdownlist.selectedIndex = i;
                        return true;
                    }
                }
            }
        }

    </script>
    <style type="text/css">
        .style1
        {
            width: 13%;
        }
        
        .hiddencol
        {
            display:none;
        }
        .viscol
        {
            display:block;
        }
    </style>
</head>
<body onload="PageLoaded();">
    <form id="frmGeneratePayToTheOrderOf" name="frmGeneratePayToTheOrderOf" style="height:300px; width:600px" method="post" runat="server">
         <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
        <table width="100%" border="0" cellspacing="3" cellpadding="3">
            <tr>
                <td>
                    <table width="100%" style="margin-right: 0px; height: 191px;">
                        <tr>
                            <td width="80%">
                                <div style="width: 98%; height: 200px; overflow: auto">
                                    <asp:GridView ID="gvPayToTheOrderOf" runat="server" onrowdatabound="gvPayToTheOrderOf_RowDataBound"
                                        AutoGenerateColumns="False"
                                        Font-Names="Tahoma" Font-Size="Smaller" CssClass="singleborder" Width="98%"
                                        CellPadding="4" ForeColor="#333333" HorizontalAlign="Left" 
                                        EnableModelValidation="True" AllowPaging="True" PageSize="25">
                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <Columns>                             
                                            <asp:BoundField DataField="Payee" HeaderText="Payee" HeaderStyle-CssClass="colheader5" >
                                                <HeaderStyle CssClass="colheader5" Width="50%" />
                                                <ItemStyle Width="50%" />
                                            </asp:BoundField>
                                             <asp:TemplateField HeaderText="Payee Phrase Location" ShowHeader="True">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="lstPayeePhraseLocation" Width="98%" type="combobox" runat="server"  />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="colheader5" Width="25%" />
                                                <ItemStyle Width="25%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payee Phrase" ShowHeader="True">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="lstPayeePhrase" Width="98%" type="combobox" runat="server"  />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="colheader5" Width="25%" />
                                                <ItemStyle Width="25%" />
                                            </asp:TemplateField>                                            
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <input  id="hdPayeeEId" name="hdPayeeEId" type="hidden" value='<%# Eval("RowID")%>'/>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="hiddencol" />
                                            </asp:TemplateField>   
                                        </Columns>
                                        <PagerStyle BackColor="#507CD1" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle CssClass="colheader3" />
                                        <EditRowStyle BackColor="#2461BF" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top">                    
                    <table width="95%" style="text-align:center;margin-right: 0px; height: 45px;">
                        <tr>
                            <td>
                                <asp:label runat="server" id="lblPreviewText" forecolor="#336699" backcolor="white" text="" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" valign="bottom">
                    <input type="button" name="btnPreview" id="btnPreview" value="Preview" onclick="PreviewClick()" />
                    <input type="button" name="btnSubmit" id="btnSubmit" value="Submit" onclick="SubmitClick()" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
