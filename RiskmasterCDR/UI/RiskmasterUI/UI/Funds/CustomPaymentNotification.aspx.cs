﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Collections;
using Riskmaster.RMXResourceManager;
using System.Globalization;


namespace Riskmaster.UI.UI.Funds
{
    public partial class CustomPaymentNotification : System.Web.UI.Page
    {
        public XmlDocument Model = null;
        string MergeMessageTemplate = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.GetCustomPayNotification</Function></Call><Document><PaymentNotification><DueBy>WeekMonth</DueBy><EndOf>Week</EndOf><FromDate /><ToDate /><TotalNumChecks /><TotalAmount /><PrevFrom/><PrevTo/><PrevEndOf/></PaymentNotification></Document></Message>";
        string MessageTemplatePrint = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.CreatePaymentNotificationReport</Function></Call><Document><PaymentNotification><DueBy/><EndOf/><FromDate/><ToDate/><TotalNumChecks /><TotalAmount /></PaymentNotification></Document></Message>";
        DateTimeFormatInfo fmt = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CustomPaymentNotification.aspx"), "CustomPaymentNotificationValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CustomPaymentNotificationValidationScripts", sValidationResources, true);
            string sCulture = AppHelper.GetCulture().ToString();
            fmt = (new CultureInfo(sCulture)).DateTimeFormat;
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScript(sCulture, this);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            if (!Page.IsPostBack)
            {
                divForms.Attributes.Add("style", "overflow:scroll;height:78%;width:95%;border-width:'0'");
                divForms.Attributes.Add("class", "divScroll");
                Model = new XmlDocument();
                string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                Model.LoadXml(sCWSresponse);
                BindpageControls(sCWSresponse);
                if (hdnfrom.Value == "" && hdnto.Value == "")
                {

                    hdnto.Value = DateTime.Now.ToString(fmt.ShortDatePattern);
                    hdnfrom.Value = DateTime.Now.AddDays(-6).ToString(fmt.ShortDatePattern);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="diaryDoc"></param>
        /// <returns></returns>
        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                DataTable dt = new DataTable();
                dt.Columns.Add("CheckDate");
                dt.Columns.Add("ControlNumber");
                dt.Columns.Add("Payee");
                dt.Columns.Add("ClaimNumber");
                dt.Columns.Add("Amount");
                //dSet.ReadXml(new XmlNodeReader(diaryDoc.SelectSingleNode("//PaymentNotification/Row")));
                foreach (XmlNode node in diaryDoc.SelectNodes("//PaymentNotification/Row"))
                {
                    DataRow dr = dt.NewRow();
                    dr["CheckDate"] = AppHelper.GetDate(node.SelectSingleNode("CheckDate").InnerText);
                    dr["ControlNumber"] = node.SelectSingleNode("ControlNumber").InnerText;
                    dr["Payee"] = node.SelectSingleNode("Payee").InnerText;
                    dr["ClaimNumber"] = node.SelectSingleNode("ClaimNumber").InnerText;
                    dr["Amount"] = node.SelectSingleNode("Amount").InnerText;
                    dt.Rows.Add(dr);
                }
                dSet.Tables.Add(dt);
                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        private void BindpageControls(string sreturnValue)
        {
            try
            {
                DataSet usersRecordsSet = null;
                XmlDocument usersXDoc = new XmlDocument();
                usersXDoc.LoadXml(sreturnValue);
                lblTotal.Text = AppHelper.GetInnerXmlFromDOM(usersXDoc, "TotalAmount");
                lblChecks.Text = AppHelper.GetInnerXmlFromDOM(usersXDoc, "TotalNumChecks");
                usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);

                gvSelectChecks.DataSource = usersRecordsSet.Tables[0].DefaultView;
                gvSelectChecks.DataBind();
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {

            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//DueBy", hdndueby.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//EndOf", hdnendof.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//FromDate", hdnfrom.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//ToDate", hdnto.Value);
            string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
            Model = new XmlDocument();
            Model.LoadXml(sCWSresponse);
            BindpageControls(sCWSresponse);
          
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            MessageTemplatePrint = AppHelper.ChangeMessageValue(MessageTemplatePrint, "//DueBy", hdndueby.Value);
            MessageTemplatePrint = AppHelper.ChangeMessageValue(MessageTemplatePrint, "//EndOf", hdnendof.Value);
            MessageTemplatePrint = AppHelper.ChangeMessageValue(MessageTemplatePrint, "//FromDate", hdnfrom.Value);
            MessageTemplatePrint = AppHelper.ChangeMessageValue(MessageTemplatePrint, "//ToDate", hdnto.Value);
            string sCWSresponse = AppHelper.CallCWSService(MessageTemplatePrint.ToString());
            Model = new XmlDocument();
            Model.LoadXml(sCWSresponse);
            byte[] pdfbytes = Convert.FromBase64String(AppHelper.GetInnerXmlFromDOM(Model, "File"));
            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            Response.AppendHeader("Content-Encoding", "none;");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=payments.pdf"));
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(pdfbytes);
            Response.Flush();
            Response.Close();
        }

    }
}
