﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;




namespace Riskmaster.UI.Funds
{
    public partial class ReservesInfoRender : System.Web.UI.Page
    {
        XElement oMessageElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement oSessionCmdElement = null;
            XmlDocument objXml = null;
            XmlDocument objEntityXml = new XmlDocument();
            XslCompiledTransform objXsl = new XslCompiledTransform();

            string sClaimId = string.Empty;
            string sTransTypeCode = string.Empty;
            string sClaimantEid = string.Empty;
            string sUnitId = string.Empty;
            string sReserveTypeCode = string.Empty; //added by rkaur7 - MITS 17241
            string sPolicyId = string.Empty;
            string sCovTypeCode = string.Empty;
            string sControlRequest = string.Empty;
            string sPolicyUnitRowId = string.Empty;//rupal:r8 unit implementation
            //skhare7
            XElement objReservesData = null;
            try
            {
                // Fetch the request parameters and set the corresponding XElement nodes.


                if (Request.QueryString["ControlRequest"] != null)
                {
                    sControlRequest = Request.QueryString["ControlRequest"];
                }

                if (string.IsNullOrEmpty(sControlRequest))
                {
                    oMessageElement = GetMessageTemplate();
                }
                else
                {
                    oMessageElement = GetMessageTemplateForControlRequest();
                }
                

                if (Request.QueryString["claimid"] != null)
                {
                    sClaimId = Request.QueryString["claimid"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ClaimId");
                    oSessionCmdElement.Value = sClaimId;
                }
                if (Request.QueryString["transtypecode"] != null)
                {
                    sTransTypeCode = Request.QueryString["transtypecode"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/TransTypeCode");
                    oSessionCmdElement.Value = sTransTypeCode;
                }
                //added by rkaur7 - MITS 17241
                if (Request.QueryString["ReserveTypeCode"] != null)
                {
                    sReserveTypeCode = Request.QueryString["ReserveTypeCode"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ReserveTypeCode");
                    oSessionCmdElement.Value = sReserveTypeCode;

                }
                //end rkaur7
                if (Request.QueryString["claimanteid"] != null)
                {
                    sClaimantEid = Request.QueryString["claimanteid"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ClaimantEid");
                    oSessionCmdElement.Value = sClaimantEid;
                }
                if (Request.QueryString["unitid"] != null)
                {
                    sUnitId = Request.QueryString["unitid"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/UnitId");
                    oSessionCmdElement.Value = sUnitId;
                }
                if (Request.QueryString["policyid"] != null)
                {
                    sPolicyId = Request.QueryString["policyid"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/PolicyId");
                    oSessionCmdElement.Value = sPolicyId;
                }
                if (Request.QueryString["lCoveCodeId"] != null)
                {
                    sCovTypeCode = Request.QueryString["lCoveCodeId"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/CovTypeCode");
                    oSessionCmdElement.Value = sCovTypeCode;
                }              

                if (Request.QueryString["DataChanged"] != null)
                {
                    hdDataChanged.Value = Request.QueryString["DataChanged"];
                }

                if (! string.IsNullOrEmpty(AppHelper.ReadCookieValue("SessionId")))
                {
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Authorization");
                    oSessionCmdElement.Value = AppHelper.ReadCookieValue("SessionId");
                }

                //rupal:start, r8 unit implementation
                if (Request.QueryString["PolicyUnitRowId"] != null)
                {
                    sPolicyUnitRowId = Request.QueryString["PolicyUnitRowId"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/PolicyUnitRowId");
                    oSessionCmdElement.Value = sPolicyUnitRowId;
                }
                //rupal:end
                //Deb Multi Currrency
                if (Request.QueryString["PmtCurrCode"] != null)
                {
                    string sPmtCurrCode = Request.QueryString["PmtCurrCode"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/PmtCurrCode");
                    oSessionCmdElement.Value = sPmtCurrCode;
                }
                //Deb Multi Currrency
                // Call the webservice to get the reserves information.
                //rupal:start
                if (Request.QueryString["CovgSeqNum"] != null)
                {
                    string sCovgSeqNum = Request.QueryString["CovgSeqNum"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/CovgSeqNum");
                    oSessionCmdElement.Value = sCovgSeqNum;
                }

                if (Request.QueryString["LossCode"] != null)
                {
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/LossCode");
                    oSessionCmdElement.Value = Request.QueryString["LossCode"];
                }

                if (Request.QueryString["TransSeqNum"] != null)
                {
                    string sTransSeqNum = Request.QueryString["TransSeqNum"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/TransSeqNum");
                    oSessionCmdElement.Value = sTransSeqNum;
                }
                //Ankit Start : Worked on MITS - 34297
                if (Request.QueryString["CoverageKey"] != null)
                {
                    string sCoverageKey = Request.QueryString["CoverageKey"];
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/CoverageKey");
                    oSessionCmdElement.Value = sCoverageKey;
                }
                //Ankit End
                objXml = GetReservesInfo();
                //skhare7 R8 supervisory approval
                   
                    if (objXml.OuterXml != string.Empty)
                    {
                        objReservesData = XElement.Parse(objXml.OuterXml.ToString());

                        AssignReserveValues(objReservesData);
                    }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlDocument GetReservesInfo()
        {
            XmlDocument objXml = new XmlDocument();
            XmlElement xmlIn = null;

            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sReturn);
            XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
            if (ErrorHelper.IsCWSCallSuccess(sReturn))
            {
                objXml.LoadXml(oInstanceNode.InnerXml);
               
            }
            return objXml;
        }

        private void AssignReserveValues(XElement p_objReservesData)
        {
            reservetypecode.Text = p_objReservesData.Element("ReserveType").Value;
            if (p_objReservesData.Element("ReserveBalance") != null)
            {
                reservebalance.Text = p_objReservesData.Element("ReserveBalance").Value;
            }
            reservetypecode_cid.Text = p_objReservesData.Element("ReserveType").Attribute("codeid").Value;
            //skhare7   R8 superviaory Approval
            if (p_objReservesData.Element("ReserveTypeStatus") != null)
            {
                hdnReserveStatus.Text = p_objReservesData.Element("ReserveTypeStatus").Attribute("shortcode").Value;
            }
            //skhare7   R8 superviaory Approval End
            //Deb Multi Currency
            if (p_objReservesData.Element("ClaimCurrReserveBal") != null)
            {
                ClaimCurrReserveBal.Text = p_objReservesData.Element("ClaimCurrReserveBal").Value;
            }
            //Deb Multi Currency
        }


        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            //updated by rkaur7 - MITS 17241 (added new tag ReserveTypeCode)
            //updated by Rupal-r8 unit imlementation, added new tag PolicyUnitRowId
            XElement oTemplate = XElement.Parse(@"
                <Message>
                    <Authorization></Authorization> 
                        <Call>
                            <Function>FundManagementAdaptor.GetReservesInfo</Function> 
                        </Call>
                    <Document>
                        <FundManager>
                            <ClaimId></ClaimId> 
                            <TransTypeCode></TransTypeCode>
                            <ReserveTypeCode></ReserveTypeCode> 
                            <ClaimantEid>0</ClaimantEid> 
                            <UnitId>0</UnitId> 
                            <TransID>0</TransID> 
                            <PolicyId></PolicyId>
                            <CovTypeCode></CovTypeCode>
                            <PolicyUnitRowId></PolicyUnitRowId>
                            <PmtCurrCode></PmtCurrCode>
                            <CovgSeqNum></CovgSeqNum>
                            <LossCode></LossCode>
                            <TransSeqNum></TransSeqNum>
                            <CoverageKey></CoverageKey>
                        </FundManager>
                    </Document>
                </Message>
            ");

            return oTemplate;
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplateForControlRequest()
        {
            //updated by rkaur7 - MITS 17241 (added new tag ReserveTypeCode)
            XElement oTemplate = XElement.Parse(@"
                <Message>
                    <Authorization></Authorization> 
                        <Call>
                            <Function>FundManagementAdaptor.GetReservesCodeInfo</Function> 
                        </Call>
                    <Document>
                        <FundManager>
                            <ClaimId></ClaimId> 
                            <TransTypeCode></TransTypeCode>
                        </FundManager>
                    </Document>
                </Message>
            ");

            return oTemplate;
        }
    }
}
