﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 12/16/2013 | 34082  | pgupta93   | Changes req for MultiCurrency
 **********************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Threading;
using System.Globalization;



namespace Riskmaster.UI.Funds
{
    public partial class SplitInfoRender : System.Web.UI.Page
    {
        XElement oMessageElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement oSessionCmdElement = null;
            XmlDocument objXml = null;
            //skhare7
            XElement obSplitData = null;
            oMessageElement = GetMessageTemplate();

            if (Request.QueryString["CurrencyTypeForClaim"] != null)
            {

                string sCurrencyypeCode = string.Empty;
                sCurrencyypeCode = Request.QueryString["CurrencyTypeForClaim"];
                oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/CurrencyTypeForClaim");
                oSessionCmdElement.Value = sCurrencyypeCode;

            }
            if (Request.QueryString["PaymenmtCurrencyType"] != null)
            {
                string spmtcurrencytype = string.Empty;
                spmtcurrencytype = Request.QueryString["PaymenmtCurrencyType"];

                Culture = spmtcurrencytype.Split('|')[1];
                UICulture = spmtcurrencytype.Split('|')[1];
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(spmtcurrencytype.Split('|')[1]);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(spmtcurrencytype.Split('|')[1]);
                spmtcurrencytype = spmtcurrencytype.Substring(0, 3) + " " + Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol;
                
                oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/pmtcurrencytype");
                oSessionCmdElement.Value = spmtcurrencytype;

            }
            if (Request.QueryString["DataChanged"] != null)
            {
                hdDataChanged.Value = Request.QueryString["DataChanged"];
            }

            objXml = GetExchRateAndEffDate();
            if (objXml.OuterXml != string.Empty)
            {
                obSplitData = XElement.Parse(objXml.OuterXml.ToString());
                AssignSplitValues(obSplitData);
            }
        }

        private XmlDocument GetExchRateAndEffDate()
        {
            XmlDocument objXml = new XmlDocument();
            XmlElement xmlIn = null;

            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sReturn);
            XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
            if (ErrorHelper.IsCWSCallSuccess(sReturn))
            {
                objXml.LoadXml(oInstanceNode.InnerXml);

            }
            return objXml;
        }

        private void AssignSplitValues(XElement p_obSplitData)
        {
            SplitToPaymentCurrRate.Text = p_obSplitData.Element("SplitToPaymentCurrRate").Value;
            ExchangeRateDate.Text = p_obSplitData.Element("ExchangeRateDate").Value;
            ExchangeRate.Text = p_obSplitData.Element("ExchangeRate").Value;
        }
        private XElement GetMessageTemplate()
        {


            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>FundManagementAdaptor.bGetExchRateAndEffDate</Function></Call>");
            sXml = sXml.Append("<Document>");
            sXml = sXml.Append("<FundManager>");
            sXml = sXml.Append("<CurrencyTypeForClaim>");
            sXml = sXml.Append("</CurrencyTypeForClaim>");
            sXml = sXml.Append("<pmtcurrencytype>");
            sXml = sXml.Append("</pmtcurrencytype>");
            sXml = sXml.Append("<SplitToPaymentCurrRate>");
            sXml = sXml.Append("0");
            sXml = sXml.Append("</SplitToPaymentCurrRate>");
            sXml = sXml.Append("<ExchangeRate>0</ExchangeRate><ExchangeRateDate>0</ExchangeRateDate>");
            sXml = sXml.Append("</FundManager>");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }
    }
}