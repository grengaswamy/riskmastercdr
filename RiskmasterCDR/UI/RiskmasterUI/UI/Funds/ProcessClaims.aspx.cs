﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.UI.Funds
{
    public partial class ProcessClaims : System.Web.UI.Page
    {
        public XmlDocument Model = null;
        //Changed by Gagan for MITS 22238 : Start
        string MergeMessageTemplate = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.ApplyPayments</Function></Call><Document><Data><Policies/><PolicyType/><FromDate></FromDate><ToDate></ToDate></Data></Document></Message>";
        //Changed by Gagan for MITS 22238 : End

        protected void Page_Load(object sender, EventArgs e)
        {
            Model = new XmlDocument();
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ProcessClaims.aspx"), "ProcessClaimsValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "ProcessClaimsValidationScripts", sValidationResources, true);

            if (Page.IsPostBack)
            {
                BindBackData();
                string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                Model.LoadXml(sCWSresponse);      
            }
        }
        private string BindBackData()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeMessageTemplate);
            //Changed by Gagan for MITS 22238 : Start
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Policies", hdnSelected.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//FromDate", hdnFromDate.Value);
            MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//ToDate", hdnToDate.Value);
            if(hdnIsPolicyTracking.Value == "true")
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//PolicyType", "Policy Tracking");
            else
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//PolicyType", "Policy Management");
           	//Changed by Gagan for MITS 22238 : End
            return doc.OuterXml;
        }
    }
}
