﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="DiaHistLookUp.aspx.cs"
    Inherits="Riskmaster.UI.PrimaryDiagHist.DiaHistLookUp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Primary Diagnosis History</title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" src="../../Scripts/form.js"></script>
<%--vkumar258 - RMA-6037 - Starts --%>
   <%-- <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>--%>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />

    <script type="text/javascript" language="JavaScript">
        function PageLoaded() {
            document.forms[0].method = "post";
            return false;
        }
        function fillReason(sText) {
            document.all.hdnReason.value = sText;
        }

        function checkDiagHist(usrname, pirowid) {
            var datearray = document.forms[0].datechanged.value.split("/");
            var datecur = datearray[2] + datearray[0] + datearray[1];

            if (document.forms[0].DIAGNOSIS_CODE_codelookup.value == "") {
                    if (document.getElementById('icdtype').value == '10')
                        alert("ICD10 (Primary Diagnosis) is needed for adding a Primary Diagnosis history entry.");
                    else
                        alert("ICD9 (Primary Diagnosis) is needed for adding a Primary Diagnosis history entry.");
                    return false;
            }
            // npadhy Start MITS 18544 Date is made mandatory as per RMWorld
            else if (Trim(document.forms[0].datechanged.value) == '') 
            {
                alert("Date is needed for adding a Primary Diagnosis history entry.");
                return false;
            }
            // npadhy End MITS 18544 Date is made mandatory as per RMWorld
	//prashbiharis ICD10 Changes(MITS 32423)start
                // akaushik5 Commented for MITS 37156 Starts
                //else if (Trim(document.forms[0].approvedby.value) == '') {
            //    alert("Approved By User is needed for adding a Primary Diagnosis history entry.");
            //    return false;
            //}
            //else if (Trim(document.forms[0].hdnReason.value) == '')
            //{
            //    alert("Reason is needed for adding a Primary Diagnosis history entry.");                
            //    return false;
            //}
                // akaushik5 Commented for MITS 37156 Ends
            else {
                     if (document.forms[0].hdnReason.value == "")
                         document.forms[0].hdnReason.value = "Reason Not Provided";
                    
                     var icdflag = window.opener.document.forms[0].hdnicdflag.value;

                    if (document.getElementById('icdtype').value == '10') {
                            window.opener.document.forms[0].icd10primarydiagnosis.value = document.forms[0].DIAGNOSIS_CODE_codelookup.value;
                            window.opener.document.forms[0].icd10primarydiagnosis_cid.value = document.forms[0].DIAGNOSIS_CODE_codelookup_cid.value;                 
                            window.opener.document.forms[0].hdnDateChangedICD10.value = document.forms[0].datechanged.value;
                            window.opener.document.forms[0].hdnDiagICD10ChangedByUser.value = document.forms[0].changedby.value;
                            window.opener.document.forms[0].hdnDiagICD10ApprovedByUser.value = document.forms[0].approvedby.value;
                            window.opener.document.forms[0].hdnDiagICD10Reason.value = document.forms[0].hdnReason.value;

                            window.opener.document.forms[0].hdnMDA10Topic.value = window.opener.document.forms[0].mdatopic.value;
                            window.opener.document.forms[0].hdnMDA10Factor.value = window.opener.document.forms[0].mdafactor.value;
                            window.opener.document.forms[0].hdnMDA10MinDays.value = window.opener.document.forms[0].mdaminimumdays1.value;
                            window.opener.document.forms[0].hdnMDA10MaxDays.value = window.opener.document.forms[0].mdamaximumdays1.value;
                            window.opener.document.forms[0].hdnMDA10OptDays.value = window.opener.document.forms[0].mdaoptimumdays1.value;


                            
                            //if (window.opener.document.forms[0].hdnicdflag.value == '1')
                            //    window.opener.document.forms[0].hdnicdflag.value = '3';
                            //else
                            //    window.opener.document.forms[0].hdnicdflag.value = '2';

                            switch (icdflag) {
                                case '0': 
                                case '2':
                                    window.opener.document.forms[0].hdnicdflag.value = '2'; break;
                                case '1': 
                                case '3': 
                                case '4':
                                    window.opener.document.forms[0].hdnicdflag.value = '4'; break;
                                default: break;
                            }
                            window.opener.setDataChanged(true);
                    }
                    else {
                            window.opener.document.forms[0].hdnDiagChangedByUser.value = document.forms[0].changedby.value;
                            //prashbiharis ICD10 Changes(MITS 32423)end
                            window.opener.document.forms[0].hdnDiagApprovedByUser.value = document.forms[0].approvedby.value;
                            //window.opener.forms[0].hdnDiagPIRowId.value=pirowid;
                            window.opener.document.forms[0].icd9primarydiagnosis.value = document.forms[0].DIAGNOSIS_CODE_codelookup.value;
                            window.opener.document.forms[0].icd9primarydiagnosis_cid.value = document.forms[0].DIAGNOSIS_CODE_codelookup_cid.value;
                            window.opener.document.forms[0].hdnDateChanged.value = document.forms[0].datechanged.value;
                            window.opener.document.forms[0].hdnDiagReason.value = document.forms[0].hdnReason.value;
                            //prashbiharis ICD10 Changes(MITS 32423) start
                            //if (window.opener.document.forms[0].hdnicdflag.value == '2')
                            //    window.opener.document.forms[0].hdnicdflag.value = '3';
                            //else
                            //    window.opener.document.forms[0].hdnicdflag.value = '1';
                        //prashbiharis ICD10 Changes(MITS 32423)end
                            window.opener.document.forms[0].hdnMDATopic.value = window.opener.document.forms[0].mdatopic.value;
                            window.opener.document.forms[0].hdnMDAFactor.value = window.opener.document.forms[0].mdafactor.value;
                            window.opener.document.forms[0].hdnmdaminimumdays.value = window.opener.document.forms[0].mdaminimumdays1.value;
                            window.opener.document.forms[0].hdnmdaoptimumdays.value = window.opener.document.forms[0].mdaoptimumdays1.value;
                            window.opener.document.forms[0].hdnmdamaximumdays.value = window.opener.document.forms[0].mdamaximumdays1.value;

                            switch (icdflag) {
                                case '0': 
                                case '1':
                                    window.opener.document.forms[0].hdnicdflag.value = '1';
                                    break;
                                case '2': 
                                case '3': 
                                case '4':
                                    window.opener.document.forms[0].hdnicdflag.value = '3';
                                    break;
                                default: break;
                            }
                            window.opener.setDataChanged(true);

                        }
                    window.close();
                    return true;
                }
        }
    </script>

</head>
<body onload="PageLoaded();">
    <form id="frmData" runat="server">
    <div align="center">
        <asp:HiddenField ID="formname" Value="document" runat="server" />
        <asp:TextBox ID="hdnReason" rmxref="/Instance/Document/Reason" runat="server" Style="display: none" />
        <asp:HiddenField ID="ClaimNumber" runat="server" />
        <asp:TextBox ID="sUserName" runat="server" Style="display: none" />
        <asp:TextBox ID="PiRowID" runat="server" Style="display: none" />
        <table>
            <tr>
                <td colspan="2">
                    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <table class="singleborder" width="90%">
            <tr>
                <td colspan="6" class="ctrlgroup">
                    Primary Diagnosis History [
                    <%=ClaimNumber.Value %>
                    ]
                </td>
            </tr>
            <tr>
                <td class="datatd">
                    <asp:label id="icdPrimaryDiagnosis" runat="server" text="ICD-9 Primary Diagnosis" style="font-weight:bold"></asp:label>
                </td>
                <td class="datatd">
                <!-- pmittal5 Mits 19037 01/20/10 - Added "DescSearch" attribute so that search can be made on the basis of Code description also.-->
                    <uc:CodeLookUp runat="server" ID="DIAGNOSIS_CODE" CodeTable="DIAGNOSIS_CODE" ControlName="DIAGNOSIS_CODE"
                        RMXRef="Instance/Document/PiDiagHist/DiagnosisCode" RMXType="code" DescSearch="1" ValidationGroup="vgSave" />
                </td>
            </tr>
            <tr>
                <td class="datatd">
                    <b>Date:</b>
                </td>
                <td class="datatd">
                    <asp:TextBox runat="server" FormatAs="date" ID="datechanged" onchange="dateLostFocus(this.id);" RMXRef="Instance/Document/PiDiagHist/DateChanged"
                        RMXType="date" />
                    <%--vkumar258 - RMA-6037 - Starts --%>

                    <%--<input type="button" class="DateLookupControl" name="datechangedbtn" />

                    <script type="text/javascript">
                        Zapatec.Calendar.setup(
					            {
					                inputField: "datechanged",
					                ifFormat: "%m/%d/%Y",
					                button: "datechangedbtn"
					            }
					            );
                    </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#datechanged").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                        });
                    </script>
                    <%--vkumar258 - RMA-6037 - End --%>

                </td>
            </tr>
            <tr>
                <td class="datatd">
                    <b>Changed By:</b>
                </td>
                <td class="datatd">
                <%--npadhy Start MITS 18588 Disabling the Changed By field as per RMWorld--%>
                    <asp:TextBox runat="server" ID="changedby" ReadOnly = "true" BackColor="Silver"/>
                <%--npadhy End MITS 18588 Disabling the Changed By field as per RMWorld--%>
                </td>
            </tr>
            <tr>
                <td class="datatd" nowrap="">
                    <b>Approved By:</b>
                </td>
                <td class="datatd">
                    <asp:TextBox runat="server" ID="approvedby" rmxref="Instance/Document/PiDiagHist/ApprovedByUser" />
                </td>
            </tr>
            <tr>
                <td class="datatd" nowrap="">
                    <b>Reason:</b>
                </td>
                <td class="datatd">
                    <asp:Button class="button" ID="btnReason" Text="..." runat="server" OnClientClick="fillReason('');EditMemo('hdnReason');return false;" />
                </td>
            </tr>
        </table>
        <table class="singleborder" width="90%">
            <tr>
                <td colspan="2">
                    <div id="div1" class="divScroll" style="height: 200; width: 500; overflow: auto">
                        <table align="center">
                            <tr>
                                <asp:GridView ID="GridViewDiagCode" AutoGenerateColumns="False" runat="server" Font-Bold="True"
                                    CellPadding="0" GridLines="Both" CellSpacing="0" Width="100%" OnRowDataBound="dgCodes_RowDataBound">
                                    <HeaderStyle CssClass="ctrlgroup" />
                                    <RowStyle CssClass="datatd" HorizontalAlign="Left" Font-Bold="false" />
                                    <AlternatingRowStyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />
                                    <Columns>
                                        <asp:TemplateField HeaderText=" Primary Diagnosis " HeaderStyle-HorizontalAlign="Left"
                                            ControlStyle-CssClass="msgheader" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <%# Eval("DiagnosisCode")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=" Date " HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader"
                                            HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <%# Eval("DateChanged")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=" Changed By " HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader"
                                            HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <%# Eval("ChgdByUser")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=" Approved By " HeaderStyle-HorizontalAlign="Left"
                                            ControlStyle-CssClass="msgheader" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <%# Eval("ApprovedByUser")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=" Reason " HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </tr>
                        </table>
                        <br />
                        <table width="90%">
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Button class="button" ID="btnOK" Text=" OK " runat="server" Width="50px" OnClientClick="return checkDiagHist('<%=changedby.Text %> ',' <%=PiRowID.Text %> ');" />
                                    &#160;
                                    <asp:Button class="button" ID="btnCancel" Text=" Cancel " runat="server" OnClientClick="window.close();return false;"
                                        Width="50px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
         <input id="DeptEid" type="hidden" value='<%=AppHelper.GetQueryStringValue("DeptEid")%>'	/><%--nadim 23191--%>
         <input id="SysFormName" type="hidden" value='<%=AppHelper.GetQueryStringValue("FormName")%>'	/><%--nadim 23191--%>
        <input id="icdtype" type="hidden" value='<%=AppHelper.GetQueryStringValue("ICDType")%>'	/> <%-- prashiharis ICD10 Changes --%>
    </div>
    </form>
</body>
</html>
