﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.PrimaryDiagHist
{
    public partial class DiaHistLookUp : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";
                string sClaimID = "";
                string sPiRowId = "";
                string ICDType = ""; //prashbiharis ICD10 changes (MITS 32423)
                XmlDocument XmlDoc = new XmlDocument();
                XmlNode xNode = null;
                XmlNode xDiagcode = null;
                DataRow objRow;

                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes
                if (!IsPostBack)
                {
                    sClaimID = Request.QueryString["claimid"];
                    sPiRowId = Request.QueryString["pirowid"];
				//prashbiharis ICD10 changes (MITS 32423)
                    if (Request.QueryString["ICDType"] != null)
                    { 
                        ICDType = Request.QueryString["ICDType"];
                        if (ICDType == "10")
                        {
                            icdPrimaryDiagnosis.Text = "ICD 10 Primary Diagnosis";
                            WebControl octrl = (Button)(this.Form.FindControl("DIAGNOSIS_CODE")).FindControl("codelookupbtn");
                            octrl.Attributes["onclick"] = "return selectCode('DIAGNOSIS_CODE_ICD10','DIAGNOSIS_CODE_codelookup','','1','');";

                        }
                        else
                        {
                            icdPrimaryDiagnosis.Text = "ICD 9 Primary Diagnosis";
                        }
                    }
                 //prashbiharis ICD10 changes (MITS 32423)
                    XmlTemplate = GetMessageTemplate(sClaimID, sPiRowId);
                    bReturnStatus = CallCWSFunction("PiDiagHistLookUpAdaptor.LoadListDiagHistory", out sreturnValue, XmlTemplate);
                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);
                        xNode = XmlDoc.SelectSingleNode("//PiDiagHist");
                        changedby.Text = xNode.SelectSingleNode("@username").Value;
                        //Neha
                        if (xNode.SelectSingleNode("@claimnumber") != null)
                        {
                            ClaimNumber.Value = xNode.SelectSingleNode("@claimnumber").Value;
                        }
                        // npadhy Start MITS 18544 Defaulting the Date field to the Current date sent by the server
                        // The Date is sent by the server and not defaulted here as the Date should be sent from server
                        datechanged.Text = xNode.SelectSingleNode("@currentdate").Value;
                        // npadhy End MITS 18544 
                        PiRowID.Text = xNode.SelectSingleNode("@PiRowId").Value;


                        XmlNodeList diaglist = XmlDoc.SelectNodes("//PiDiagHist/data");
                        DataTable objTable = new DataTable();
                        objTable.Columns.Add("DiagnosisCode");
                        objTable.Columns.Add("DateChanged");
                        objTable.Columns.Add("ChgdByUser");
                        objTable.Columns.Add("ApprovedByUser");
                        objTable.Columns.Add("Reason");
                        bool diagNoData= true;
                        for (int i = 0; i < diaglist.Count; i++)
                        {
                            xDiagcode = diaglist[i];
                            objRow = objTable.NewRow();
                           //prashbiharis ICD10 changes (MITS 32423)
                                if (ICDType == "10")
                                {
                                    if (xDiagcode.SelectSingleNode("@IsICD10") != null && xDiagcode.SelectSingleNode("@IsICD10").Value == "-1")
                                    {
                                        objRow["DiagnosisCode"] = xDiagcode.SelectSingleNode("@DiagnosisCode").Value;
                                        objRow["DateChanged"] = xDiagcode.SelectSingleNode("@DateChanged").Value;
                                        objRow["ChgdByUser"] = xDiagcode.SelectSingleNode("@ChgdByUser").Value;
                                        objRow["ApprovedByUser"] = xDiagcode.SelectSingleNode("@ApprovedByUser").Value;
                                        objRow["Reason"] = xDiagcode.SelectSingleNode("@Reason").Value;
                                        objTable.Rows.Add(objRow);
                                        diagNoData = false;
                                    }

                                }
                                else
                                {
                                    if (xDiagcode.SelectSingleNode("@IsICD10") != null && xDiagcode.SelectSingleNode("@IsICD10").Value != "-1")
                                    {
                                        objRow["DiagnosisCode"] = xDiagcode.SelectSingleNode("@DiagnosisCode").Value;
                                        objRow["DateChanged"] = xDiagcode.SelectSingleNode("@DateChanged").Value;
                                        objRow["ChgdByUser"] = xDiagcode.SelectSingleNode("@ChgdByUser").Value;
                                        objRow["ApprovedByUser"] = xDiagcode.SelectSingleNode("@ApprovedByUser").Value;
                                        objRow["Reason"] = xDiagcode.SelectSingleNode("@Reason").Value;
                                        objTable.Rows.Add(objRow);
                                        diagNoData = false;
                                    }
                                    else if (xDiagcode.SelectSingleNode("@IsICD10") == null) //JIRA RMA-1303
                                    {
                                        objRow["DiagnosisCode"] = xDiagcode.SelectSingleNode("@DiagnosisCode").Value;
                                        objRow["DateChanged"] = xDiagcode.SelectSingleNode("@DateChanged").Value;
                                        objRow["ChgdByUser"] = xDiagcode.SelectSingleNode("@ChgdByUser").Value;
                                        objRow["ApprovedByUser"] = xDiagcode.SelectSingleNode("@ApprovedByUser").Value;
                                        objRow["Reason"] = xDiagcode.SelectSingleNode("@Reason").Value;
                                        objTable.Rows.Add(objRow);
                                        diagNoData = false;
                                    }                                    
                                }                            
                            //prashbiharis ICD10 changes (MITS 32423)
                        }
                        //PJS : MITS 14136 - 02-10-09 : Done for displaying Grid if no result there
                        if (diagNoData == true)
                        {
                            objRow = objTable.NewRow();
                            objRow["DiagnosisCode"] = "";
                            objRow["DateChanged"] = "";
                            objRow["ChgdByUser"] = "";
                            objRow["ApprovedByUser"] = "";
                            objRow["Reason"] = "";
                            objTable.Rows.Add(objRow);
                        }
                        objTable.AcceptChanges();
                        GridViewDiagCode.Visible = true;
                        GridViewDiagCode.DataSource = objTable;
                        GridViewDiagCode.DataBind();
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate(string sClaimId, string sPiRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><LoadListDiagHist><ClaimId>");
            sXml = sXml.Append(sClaimId);
            sXml = sXml.Append("</ClaimId><PiRowId>");
            sXml = sXml.Append(sPiRowId);
            sXml = sXml.Append("</PiRowId></LoadListDiagHist></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        protected void dgCodes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnReason = new Button();
                String sReason = "";
                String sFieldName = "hdnReason";
                if (!String.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "DiagnosisCode").ToString()))
                {
                    if (DataBinder.Eval(e.Row.DataItem, "Reason") == null)
                    {
                        sReason = "";
                    }
                    else
                    {
                        sReason = DataBinder.Eval(e.Row.DataItem, "Reason").ToString();
                    }

                    btnReason.Attributes.Add("onclick", "javascript:fillReason('" + sReason + "');EditMemo('" + sFieldName + "');return false;");
                    btnReason.Attributes.Add("class", "button");
                    btnReason.Text = "...";
                    e.Row.Cells[4].Controls.Add(btnReason);
                    e.Row.Cells[4].CssClass = "Bold2";
                }
            }
        }
    }
}
