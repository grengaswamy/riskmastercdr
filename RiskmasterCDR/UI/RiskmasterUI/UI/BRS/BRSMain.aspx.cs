using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
using System.Text;
namespace Riskmaster.UI.BRS
{
    public partial class BRSMain : Riskmaster.UI.NonFDMBasePageCWS
    {
        private const string FLHOSP = "20";
        XElement otempbrsdata = null;
        string eventTarget = string.Empty;
        string eventArgument = string.Empty;
		string sTempBRSMode = string.Empty;  //kkaur8 added for MITS 33750       
        protected void Page_Load(object sender, EventArgs e)
        {
            string sBRSSplitData = string.Empty;          
            if (!Page.IsPostBack)
            {
                AddItemInProgress.Text = string.Empty;
                

                //Get BRS data from context.items
                if (Context.Items["BRSSplitData"] != null)
                {
                    sBRSSplitData = Context.Items["BRSSplitData"].ToString();

                    string sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
                    XElement oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);

                    XElement oStep = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/step");
                    if (oStep != null)
                        oStep.Value = ((int)eSteps.LoadBRSXML).ToString();

                    XElement oCmd = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/cmd");
                    if (oCmd != null)
                        oCmd.Value = string.Empty;
                  
              
                    XElement oFunction = oCurrentItemMessage.XPathSelectElement("./Call/Function");
                    oFunction.Value = "BRSAdaptor.BRSEntrypoint";

                    string sResponse = AppHelper.CallCWSService(oCurrentItemMessage.ToString());
                    lblError.Text = ErrorHelper.FormatServiceErrors(sResponse);
                    //Mona: R8.2 : BRS in Carrier claim
                    XElement oCarrierClaims = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/UseCarrierClaim");
                    if (oCarrierClaims != null)
                    {
                        if (oCarrierClaims.Value == "True")
                        {
                            GetPolicyList(ref sResponse);                           
                        }
                    }
                    BindBRSData2Controls(sResponse);

                    sBRSSplitData = BRSCommon.PutCurrentItemMessage(sResponse, sBRSSplitData);
                    oCurrentItemMessage = XElement.Parse(sResponse);                                                          
                    XElement oBRSSplitData = XElement.Parse(sBRSSplitData);
                    string sBRSMode = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits").Attribute("brsmode").Value;
					sTempBRSMode = sBRSMode;  //Added by kkaur8 for5 MITS 33750
                    if (sBRSMode == "edit")
                    {
                        XElement oFundsBRSSplits = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits");
                        XAttribute oattr = oFundsBRSSplits.Attribute("FromDateForDupChk");
                        if (oattr != null)
                        {
                            oattr.Remove();
                        }
                        oattr = new XAttribute("FromDateForDupChk", oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/FromDate").Value);
                        oFundsBRSSplits.Add(oattr);

                        oattr = oFundsBRSSplits.Attribute("ToDateForDupChk");
                        if (oattr != null)
                        {
                            oattr.Remove();
                        }
                        oattr = new XAttribute("ToDateForDupChk", oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/ToDate").Value);
                        oFundsBRSSplits.Add(oattr);

                        oattr = oFundsBRSSplits.Attribute("billingcodeForDupChk");
                        if (oattr != null)
                        {
                            oattr.Remove();
                        }
                        oattr = new XAttribute("billingcodeForDupChk", oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/billingcode").Value);
                        oFundsBRSSplits.Add(oattr);
                    }
                    sBRSSplitData = oBRSSplitData.ToString();
                   // ViewState["BRSSplitData"] = BRSCommon.PutCurrentItemMessage(sResponse, sBRSSplitData);
                    ViewState["BRSSplitData"] = sBRSSplitData;


                    //If cannot find transaction type, assume it's a new bill item
                    //oCurrentItemMessage = XElement.Parse(sResponse);
                    XElement oTransType = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/TransTypeCode");
                    if (oTransType != null)
                    {
                        if (oTransType.Attribute("codeid") != null)
                        {
                            string sTransTypeCode = oTransType.Attribute("codeid").Value;
                            if (string.IsNullOrEmpty(sTransTypeCode) || sTransTypeCode == "0")
                            {
                                AddItemInProgress.Text = "true";
                            }
                        }
                    }
                }

                PhysicianLookUpbtn.Attributes["onclick"] = "return lookupData('PhysicianLookUp','physician',-1,'" + PhysicianLookUp.ClientID + "',2);";
                //Added by kkaur8 for MITS 33750 Starts
                // akaushik5 Changed for MITS 37403 Starts
                //if (sTempBRSMode != "edit")
                if (sTempBRSMode.Equals("add"))
                // akaushik5 Changed for MITS 37403 Ends
                {
                    RefNo.Text = "1";       
                }
                //Added by kkaur8 for MITS 33750 Ends
            }
            else
            {
                //check for postbackaction if it's initiated by javascript (not from button click"
               

              
                Control objPostbactControl = DatabindingHelper.GetPostBackControl(this);
               
                if (objPostbactControl == null)
                {
                    string sPostbackAction = PostbackAction.Text;
                    switch (sPostbackAction)
                    { //MITS 23814 :skhare7
                        case "physicianlookup":
                            PhysicianLookup_Postback(0);
                            break;
                        case "datechanged":
                            PhysicianLookup_Postback(1);
                            //MITS 23814 :skhare7 End
                            break;
                        case "FIRST&FINAL_PAYMENT":
                            hTabName.Text = "BillItem";
                            DatabindingHelper.UpdateTabFocus(this);
                            break;                        
                        case "Return":
                        case "New":
                            ClientScript.GetPostBackEventReference(this, string.Empty);
                            eventTarget = Request["__EVENTTARGET"] ?? string.Empty;
                            eventArgument = Request["__EVENTARGUMENT"] ?? string.Empty;

                            switch (eventTarget)
                            {
                                case "UserConfirmationPostBack":
                                    if (Convert.ToBoolean(eventArgument))
                                    {
                                        if (string.Compare(sPostbackAction, "New", true) == 0)
                                        {
                                            btnNewAfterDupChk(BrsDataForDuplicateChk.Text);
                                        }
                                        else
                                        {
                                            btnReturnAfterDupChk(BrsDataForDuplicateChk.Text);
                                        }
                                    }
                                    else
                                    {
                                        return;
                                    }
                                    BrsDataForDuplicateChk.Text = "";
                                    PostbackAction.Text = "";
                                    break;
                            }

                            break;                       
                        default:
                            break;
                    }

                   
                    PostbackAction.Text = string.Empty;
                }
                else if (objPostbactControl.ID == "Policy")   //Mona: R8.2 : BRS in Carrier claim                                             
                {
                    
                    sBRSSplitData = ViewState["BRSSplitData"].ToString();
                    sBRSSplitData = BindControls2BRSData(sBRSSplitData);
                    string sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
                    XElement oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);                 
                    
                    XElement objXReserveTypeCodeNode = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/ReserveTypeCode");
                    if (objXReserveTypeCodeNode != null)
                    {
                        objXReserveTypeCodeNode.Value = "";
                        objXReserveTypeCodeNode.Attribute("codeid").Value = "";
                    }

                    XElement objXTransTypeCodeNode = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/TransTypeCode");
                    if (objXTransTypeCodeNode != null)
                    {
                        objXTransTypeCodeNode.Value = "";
                        objXTransTypeCodeNode.Attribute("codeid").Value = "";
                    }

                    XElement  objXPolicyID = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/PolicyID");
                    if (objXPolicyID != null)
                    {
                        objXPolicyID.Value = PolicyID.Text;
                    }

                    sCurrentItemMessage = oCurrentItemMessage.ToString();
                    GetPolicyList(ref sCurrentItemMessage);
                    sBRSSplitData = BRSCommon.PutCurrentItemMessage(sCurrentItemMessage, sBRSSplitData);                  
                   
                    BindBRSData2Controls(sBRSSplitData);
                    ViewState["BRSSplitData"] = sBRSSplitData;
                }

            }
        }

        protected void FeeTableChanged(object sender, EventArgs e)
        {//MITS 23814 :skhare7
            PhysicianLookup_Postback(2);
            //MITS 23814 :skhare7 End
        }
        //MITS 23814 :skhare7
        private void PhysicianLookup_Postback(int ivalue)
        {

            string sBRSSplitData = string.Empty;

            //Get BRS data from context.items
            sBRSSplitData = ViewState["BRSSplitData"].ToString();
            sBRSSplitData = BindControls2BRSData(sBRSSplitData);
           

            string sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
            XElement oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);
            XElement oStep = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/step");
            if (oStep != null)
                oStep.Value = ((int)eSteps.LoadBRSXML).ToString();

            XElement oFunction = oCurrentItemMessage.XPathSelectElement("./Call/Function");
            //MITS 23814 :skhare7
            XElement oPhyLook = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/PhysicianLookUp");
            if (ivalue == 0 )
            {
                if (oPhyLook != null)
                    oPhyLook.SetAttributeValue("PhyLookupchanged", "Y");

            }
            else
            {
                if (oPhyLook != null)
                    oPhyLook.SetAttributeValue("PhyLookupchanged", "N");
            }
            //MITS 23814 :skhare7 End
            oFunction.Value = "BRSAdaptor.BRSEntrypoint";

            string sResponse = AppHelper.CallCWSService(oCurrentItemMessage.ToString());
            lblError.Text = ErrorHelper.FormatServiceErrors(sResponse);
            if (ErrorHelper.IsCWSCallSuccess(sResponse))
            {
                BindBRSData2Controls(sResponse);
                sBRSSplitData = BRSCommon.PutCurrentItemMessage(sResponse, sBRSSplitData);
                ViewState["BRSSplitData"] = sBRSSplitData;
            }
           

        }
        //MITS 23814 :skhare7 end
        private void BindBRSData2Controls(string sBRSSplitData)
        {
            string sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
            XElement oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);
            XElement oDocument = oCurrentItemMessage.XPathSelectElement("./Document");
            BindData2Control(oDocument, this.Form.Controls);
            UpdateScreen(sCurrentItemMessage);
        }

        /// <summary>
        /// handle user controls visibility
        /// </summary>
        /// <param name="sBRSSplitData"></param>
        private void UpdateScreen(string sBRSSplitData)
        {
            XElement oBRSSplitData = XElement.Parse(sBRSSplitData);

            XElement oCmboSpecCds = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits/option/CmboSpecCds");
            string sCmboSpecCds = string.Empty;
            if (oCmboSpecCds != null)
            {
                sCmboSpecCds = oCmboSpecCds.Value.Trim();
            }

            //Added by kkaur8 for MITS 33750 starts          
            XElement oRefNo = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits/option/RefNo");
            if (RefNo != null)
            {                
                RefNo.Text = oRefNo.Value;
            }
            //Added by kkaur8 for MITS 33750 Ends 
            //Check if it's override step
            bool bOverrideStep = false;
            XElement oStep = oBRSSplitData.XPathSelectElement("//step");
            if (oStep.Value == ((int)eSteps.Override).ToString())
            {
                //If there are any Specialty codes, we need to get the specialty code first before override calcualted value
                if (string.IsNullOrEmpty(sCmboSpecCds))
                {
                    bOverrideStep = true;
                }
                else
                {
                    oStep.Value = eSteps.Calculate.ToString();
                    step.Text = eSteps.Calculate.ToString();
                }
            }

            //Toolbar buttons should only be visible for new/clone and not override step
            string sBRSMode = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits").Attribute("brsmode").Value;
            if ((sBRSMode == "add" || sBRSMode == "clone") && !bOverrideStep)
            {
                toolbardrift.Visible = true;
                btnShowSummary.Visible = true; // mkaran2 - MITS #28419
            }
            else
            {
                toolbardrift.Visible = false;
                btnShowSummary.Visible = false; // mkaran2 - MITS #28419
            }
            //kkaur8 added for MITS 32827 Starts
            if (sBRSMode == "clone")
            {
                amountreduced.Text = "";
                amountreducedpercent.Text = "";
            }
            //kkaur8 added for MITS 32827 Ends
			//Mona: R8.2 : BRS in Carrier claim:Start
            XElement oCarrierClaims = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits/option/UseCarrierClaim");
            if (oCarrierClaims != null)
            {
                if (oCarrierClaims.Value == "False")
                {
                    div_Policy.Visible = false;
                    div_Unit.Visible = false;
                    div_Coverage.Visible = false;
                    div_FirstFinalPayment.Visible = false;
                    div_LossType.Visible = false;
                    div_DisabilityCatCode.Visible = false;
                    div_DisabilityLossType.Visible = false;
                }
                else
                {
                    string sPaymentOrCollection = "payment";
                    XAttribute xaPaymentOrCollection = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits").Attribute("PaymentOrCollection");
                    if (xaPaymentOrCollection != null)
                    {
                       sPaymentOrCollection = xaPaymentOrCollection.Value;
                    }
                    if (string.Compare(sPaymentOrCollection, "collection") == 0)
                    {
                        FirstFinalPayment.Checked = false;
                        FirstFinalPayment.Enabled = false; 
                    }
                    else
                    {
                        IsFirstFinalQueryString.Text = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits/option/IsFirstFinal").Value;
                        EnableDisableFirstFinalPayment(oBRSSplitData);
                    }
                    XElement oCLob = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits/option/lineofbusinesscode");
                    if (oCLob != null)
                    {
                        if (oCLob.Value != "243")
                        {
                            div_DisabilityCatCode.Visible = false;
                            div_DisabilityLossType.Visible = false;
                        }
                        else
                        {
                            div_LossType.Visible = false;
                        }
                    }
                }
            }
            //Mona: R8.2 : BRS in Carrier claim:End

            //If there is no from date selected, don't try to hide any control.
            if (string.IsNullOrEmpty(dfromdate.Text))
                return;

            //Show the nodes which were dynamically hidden in the last postback 
            //(reset the visibility for those nodes).
            string sKilledNodes = "<SysKilledNodes>" + SysKilledNodes.Text + "</SysKilledNodes>";
            XElement oSysKilledNodes = XElement.Parse(sKilledNodes);
            DatabindingHelper.ShowNodes(oSysKilledNodes, this);

            //Hide the nodes which required to be invisible in this postback
            oSysKilledNodes = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits/option/SysKilledNodes");
            if (bOverrideStep)
            {
                oSysKilledNodes.Value = "Date|BillItem|contract|Calculate|btnPrevious|btnReturn|btnPreviousItem|btnNextItem";
                btnContinue.Visible = true;
                XElement oOption = oBRSSplitData.XPathSelectElement("//option");
                if (oOption.Attribute("subtitle") != null)
                {
                    Head1.Title = oOption.Attribute("subtitle").Value;
                }

                //set onclick event for override radio buttons
                override1ba.Checked = false;
                override1ba.Attributes.Add("onclick", "selectOption('" + ((int)OverrideOptions.override1ba).ToString() + "')");

                override1bad.Checked = false;
                override1bad.Attributes.Add("onclick", "selectOption('" + ((int)OverrideOptions.override1bad).ToString() + "')");

                override1ca.Checked = false;
                override1ca.Attributes.Add("onclick", "selectOption('" + ((int)OverrideOptions.override1ca).ToString() + "')");

                override1apd.Checked = false;
                override1apd.Attributes.Add("onclick", "selectOption('" + ((int)OverrideOptions.override1apd).ToString() + "')");

                override1cad.Checked = false;
                override1cad.Attributes.Add("onclick", "selectOption('" + ((int)OverrideOptions.override1cad).ToString() + "')");

                override1apdsl.Checked = false;
                override1apdsl.Attributes.Add("onclick", "selectOption('" + ((int)OverrideOptions.override1apdsl).ToString() + "')");

                override1fs.Checked = false;
                override1fs.Attributes.Add("onclick", "selectOption('" + ((int)OverrideOptions.override1fs).ToString() + "')");

                override1fsd.Checked = false;
                override1fsd.Attributes.Add("onclick", "selectOption('" + ((int)OverrideOptions.override1fsd).ToString() + "')");

                //show override options tab
                TABSoverridegroup.Visible = true;

                //set the default to override1fsd
                override1fsd.Checked = true;
                overrideoption.Text = ((int)OverrideOptions.override1fsd).ToString();

                XElement eleOverrideamount = oBRSSplitData.XPathSelectElement("//ovr_discschedamount");
                overrideamount.Text = eleOverrideamount.Value;
            }
            else
            {
                //Special handling for Contract related fields
                XElement oContractExists = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits/option/contractexists");
                if (oContractExists.Value == "1")
                {
                    lbl_feescheddesc.Text = "Contract Table 1";
                    TABScontract.Visible = true;
                }
                else
                {
                    lbl_feescheddesc.Text = "Table";
                    TABScontract.Visible = false;
                }

                string sTabName = hTabName.Text;
                if (string.Compare(sTabName, "overridegroup", true) == 0)
                {
                    hTabName.Text = "Calculate";
                }

                TABSoverridegroup.Visible = false;

                btnContinue.Visible = false;
                Head1.Title = "Bill Review System";
                DatabindingHelper.UpdateTabFocus(this);
            }

            //If there is no killed nodes list, use the old value (for click continue button in the override tab)
            if (oSysKilledNodes == null)
            {
                oSysKilledNodes = XElement.Parse(sKilledNodes);
            }
            DatabindingHelper.HideNodes(oSysKilledNodes, this);
            SysKilledNodes.Text = oSysKilledNodes.Value;

            //special handling for BillingCode
            if (billingcodelabel.Visible == false)
            {
                lbl_billingcode.Text = "Description";
                lbl_billingcode.Attributes["class"] = "label";
                SysRequired.Text = SysRequired.Text.Replace("|billingcode|", "|");
                billingcode.Attributes.Remove("onblur");
                //MITS 23086 Don't launch CPT code lookup if lookup button is hidden
                billingcode.Attributes["onchange"] = "setDataChanged(true);";
            }
            else
            {
                lbl_billingcode.Text = "Billing Code";
                lbl_billingcode.Attributes["class"] = "required";
                SysRequired.Text += "|billingcode|";
                SysRequired.Text = SysRequired.Text.Replace("||", "|");
                //MITS 23086 launch CPT code lookup if lookup button is visible
                billingcode.Attributes["onchange"] = "setDataChanged(true);billingcodeLostFocus(this);";
            }

            //special handling for billtype code
            XElement oStateCode = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits/option/statecode");
            if (oStateCode.Value == "FL")
            {
                lbl_billtype.Attributes["class"] = "required";
                SysRequired.Text += "|billtype_codelookup|";
                SysRequired.Text = SysRequired.Text.Replace("||", "|");
            }
            else
            {
                lbl_billtype.Attributes["class"] = "label";
                SysRequired.Text = SysRequired.Text.Replace("|billtype_codelookup|", "|");
            }

            //Reset Specialty amount so it can pop-up again when the user click on the calculate button again.
            //otherwise the user has to save it, reload the funds page and then be able to change the
            //specialty code
            CmboSpecCdsAmount.Text = string.Empty;

            if (!string.IsNullOrEmpty(feescheddesc2.Text))
            {
                div_feescheddesc2.Visible = true;
            }

            //Update all nodes meant to be read only in the page
            XElement oSysReadOnlyNodes = oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits/option/SysReadOnlyNodes");
            DatabindingHelper.UpdateReadOnlyControls(oSysReadOnlyNodes, this);
        }

//Mona: R8.2 : BRS in Carrier claim
        private void GetPolicyList(ref string p_sResponse )
        {
            string sBRSSplitData = string.Empty;
            ListItem oListItem = null;

            Policy.Items.Clear();
            Unit.Items.Clear();

            XElement XEResponse = XElement.Parse(p_sResponse);

            XElement XmlTemplate = GetTemplateForPolicyList(XEResponse);
            string sResponse = AppHelper.CallCWSService(XmlTemplate.ToString());     
            XmlTemplate=XElement.Parse(sResponse);


            foreach (XElement objItem in XmlTemplate.XPathSelectElements("//PolicyList/option"))
            {               
                if(objItem.Attribute("selected") != null && objItem.Attribute("selected").Value == "true")
                {
                    XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/PolicyID").Value = objItem.Value;
                    XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/PolicyID").SetAttributeValue("value", objItem.FirstAttribute.Value);
                }
            }
            foreach (XElement objItem in XmlTemplate.XPathSelectElements("//UnitList/option"))
            {               
                if (objItem.Attribute("selected") != null && objItem.Attribute("selected").Value == "true")
                {
                    XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/UnitID").Value = objItem.Value;
                    XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/UnitID").SetAttributeValue("value", objItem.FirstAttribute.Value);
                }              
            }
            if (string.Compare(XEResponse.XPathSelectElement("./Document/FundsBRSSplits").Attribute("brsmode").Value, "add", true) == 0)
            {
                if (XmlTemplate.XPathSelectElement("//CoverageTypeCode") != null)
                {
                    XElement objXElement = XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/CoverageTypeCode");
                    objXElement.Remove();
                    XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option").Add(XmlTemplate.XPathSelectElement("//CoverageTypeCode"));
                }
                //Deb
                if (XmlTemplate.XPathSelectElement("//LossTypeCode") != null)
                {
                    XElement objXElement = XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/LossTypeCode");
                    objXElement.Remove();
                    XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option").Add(XmlTemplate.XPathSelectElement("//LossTypeCode"));
                }
                if (XmlTemplate.XPathSelectElement("//DisabilityCatCode") != null)
                {
                    XElement objXElement = XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/DisabilityCatCode");
                    objXElement.Remove();
                    XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option").Add(XmlTemplate.XPathSelectElement("//DisabilityCatCode"));
                }
                if (XmlTemplate.XPathSelectElement("//DisabilityTypeCode") != null)
                {
                    XElement objXElement = XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/DisabilityTypeCode");
                    objXElement.Remove();
                    XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option").Add(XmlTemplate.XPathSelectElement("//DisabilityTypeCode"));
                }

                if (XmlTemplate.XPathSelectElement("//ReserveTypeCode") != null)
                {
                    XElement objXElement = XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/ReserveTypeCode");
                    objXElement.Remove();
                    XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option").Add(XmlTemplate.XPathSelectElement("//ReserveTypeCode"));
                }
            }
            if (XmlTemplate.XPathSelectElement("//PolicyList") != null)
            {
                XElement objXElement = XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/PolicyList");
                objXElement.Remove();
                XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option").Add(XmlTemplate.XPathSelectElement("//PolicyList"));
            }
            if (XmlTemplate.XPathSelectElement("//UnitList") != null)
            {
                XElement objXElement = XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/UnitList");
                objXElement.Remove();
                XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option").Add(XmlTemplate.XPathSelectElement("//UnitList"));
            }
            
            p_sResponse = XEResponse.ToString();
        }

//Mona: R8.2 : BRS in Carrier claim
        private XElement GetTemplateForPolicyList(XElement p_XEResponse)
        {
            
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>FundManagementAdaptor.GetPolicyList</Function></Call><Document><Policy><TransId>");
            sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/transid").Value);
            sXml = sXml.Append("</TransId>");
            if (string.Compare(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits").Attribute("brsmode").Value, "add", true) == 0)
            {
                sXml = sXml.Append("<ClaimId>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/claimid").Value);
                sXml = sXml.Append("</ClaimId>");
                sXml = sXml.Append("<PolicyId>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/PolicyID").Value);
                sXml = sXml.Append("</PolicyId>");
                sXml = sXml.Append("<UnitId>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/UnitID").Value);
                sXml = sXml.Append("</UnitId>");
                sXml = sXml.Append("<PolCvgID>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/CoverageId").Value);
                sXml = sXml.Append("</PolCvgID>");
                sXml = sXml.Append("<CvgLossID>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/CvgLossID").Value);
                sXml = sXml.Append("</CvgLossID>");
                sXml = sXml.Append("<RCRowID>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/RCRowId").Value);
                sXml = sXml.Append("</RCRowID>");
                sXml = sXml.Append("<LobCode>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/lineofbusinesscode").Value);
                sXml = sXml.Append("</LobCode>");
                //sXml = sXml.Append("<ResTypeCodeId>");
                //sXml = sXml.Append(ResTypeCodeId.Text);
                //sXml = sXml.Append("</ResTypeCodeId>");
            }
            if ((string.Compare(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits").Attribute("brsmode").Value, "edit", true) == 0) ||
                (string.Compare(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits").Attribute("brsmode").Value, "clone", true) == 0))  //&& (string.Compare(sTransId,"0") == 0))
            {
                sXml = sXml.Append("<ClaimId>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/claimid").Value);
                sXml = sXml.Append("</ClaimId>");
                sXml = sXml.Append("<PolicyId>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/PolicyID").Attribute("value").Value);
                sXml = sXml.Append("</PolicyId>");
                sXml = sXml.Append("<CvgLossID>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/CvgLossID").Value);
                sXml = sXml.Append("</CvgLossID>");
                sXml = sXml.Append("<RCRowID>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/RCRowId").Value);
                sXml = sXml.Append("</RCRowID>");
                sXml = sXml.Append("<LobCode>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/lineofbusinesscode").Value);
                sXml = sXml.Append("</LobCode>");
            }           
            //indicate that the payment is marked as first & final payment
            
                sXml = sXml.Append("<IsFirstFinal>");
                sXml = sXml.Append(p_XEResponse.XPathSelectElement("./Document/FundsBRSSplits/option/IsFirstFinalQueryString").Value);
                sXml = sXml.Append("</IsFirstFinal>");
            
           
            sXml = sXml.Append("<PolicyList></PolicyList></Policy></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        /// <summary>
        /// btnCalculate button's click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            string sBRSSplitData = string.Empty;

            //Get BRS data from context.items
            sBRSSplitData = ViewState["BRSSplitData"].ToString();
            sBRSSplitData = BindControls2BRSData(sBRSSplitData);

            string sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
            XElement oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);
            XElement oStep = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/step");
            if (oStep != null)
                oStep.Value = ((int)eSteps.Calculate).ToString();

            XElement oFunction = oCurrentItemMessage.XPathSelectElement("./Call/Function");
            oFunction.Value = "BRSAdaptor.BRSEntrypoint";

            string sResponse = AppHelper.CallCWSService(oCurrentItemMessage.ToString());
            lblError.Text = ErrorHelper.FormatServiceErrors(sResponse);
            if (ErrorHelper.IsCWSCallSuccess(sResponse))
            {
                //Set value for calculated
                XElement oResponse = XElement.Parse(sResponse);
                XElement oCalculated = oResponse.XPathSelectElement("//calculated");
                oCalculated.Value = "true";
                sResponse = oResponse.ToString();

                BindBRSData2Controls(sResponse);
                sBRSSplitData = BRSCommon.PutCurrentItemMessage(sResponse, sBRSSplitData);
                ViewState["BRSSplitData"] = sBRSSplitData;
            }
        }


        /// <summary>
        /// btnContinue button's click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOverrideContinue_Click(object sender, EventArgs e)
        {
            string sBRSSplitData = string.Empty;

            //Get BRS data from context.items
            sBRSSplitData = ViewState["BRSSplitData"].ToString();
            string sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
            XElement oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);
            
            //because set tab's visible set to false, the textbox will not be in the page. We just need to get the
            //following two values:overrideoption and overrideamount
            XElement eleOverrideoption = oCurrentItemMessage.XPathSelectElement("//overrideoption");
            eleOverrideoption.Value = overrideoption.Text;
            XElement eleOverrideamount = oCurrentItemMessage.XPathSelectElement("//overrideamount");
            eleOverrideamount.Value = overrideamount.Text;

            XElement oStep = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/step");
            if (oStep != null)
                oStep.Value = ((int)eSteps.Calculate).ToString();

            XElement oFunction = oCurrentItemMessage.XPathSelectElement("./Call/Function");
            oFunction.Value = "BRSAdaptor.BRSEntrypoint";

            string sResponse = AppHelper.CallCWSService(oCurrentItemMessage.ToString());
            lblError.Text = ErrorHelper.FormatServiceErrors(sResponse);
            BindBRSData2Controls(sResponse);
            sBRSSplitData = BRSCommon.PutCurrentItemMessage(sResponse, sBRSSplitData);
            ViewState["BRSSplitData"] = sBRSSplitData;
        }

        private string BindControls2BRSData(string sBRSSplitData)
        {
            XElement oBRSSplitData = XElement.Parse(sBRSSplitData);
            string sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
            XElement oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);
            BindNonFDMCWSControlCollection2Data(this.Form.Controls, oCurrentItemMessage);
            //Mona: R8.2 : BRS in Carrier claim:Start
            XElement oPolicy = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/PolicyID");
            if (Policy.SelectedIndex != -1)
                oPolicy.Value = Policy.SelectedItem.Text;

            XElement oUnitID = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/UnitID");
            if (Unit.SelectedIndex != -1)
                oUnitID.Value = Unit.SelectedItem.Text;

            XElement oFirstFinalPayment = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/IsFirstFinal");
            if (oFirstFinalPayment != null)
            {
                if (FirstFinalPayment.Checked)
                    oFirstFinalPayment.Value = "True";
                else
                    oFirstFinalPayment.Value = "False";
            }
            //Mona: R8.2 : BRS in Carrier claim:End

            //kkaur8 added for MITS 33750 starts
            XElement oRefNo = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/RefNo");
            if (RefNo != null)
            {
                RefNo.Text = oRefNo.Value;
            }
            //kkaur8 added for MITS 33750 ends

            sBRSSplitData = BRSCommon.PutCurrentItemMessage(oCurrentItemMessage.ToString(), sBRSSplitData);
            return sBRSSplitData;
        }


        protected void btnReturn_Click(object sender, EventArgs e)
        {
            string sBRSSplitData = string.Empty;
            sBRSSplitData = ViewState["BRSSplitData"].ToString();
            XElement oBRSMessage = XElement.Parse(sBRSSplitData);

            string sCurrentItemMessage = string.Empty;
            XElement oCurrentItemMessage = null;
            bool bIsDuplicate = false;
            //If the current record is being added and not need to be removed, save the change on the page
            if (AddCurrentBillItem.Text != "false")
            {
                sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
                oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);

                BindNonFDMCWSControlCollection2Data(this.Form.Controls, oCurrentItemMessage);

                //Mona: R8.2 : BRS in Carrier claim:Start
                XElement oPolicy = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/PolicyID");
                if (Policy.SelectedIndex != -1) 
                    oPolicy.Value = Policy.SelectedItem.Text;

                XElement oUnitID = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/UnitID");
                if (Unit.SelectedIndex != -1)
                    oUnitID.Value = Unit.SelectedItem.Text;
               
                XElement oFirstFinalPayment = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/IsFirstFinal");
                if (oFirstFinalPayment != null)
                {
                    if (FirstFinalPayment.Checked)
                        oFirstFinalPayment.Value = "True";
                    else
                        oFirstFinalPayment.Value = "False";
                }
                //Mona: R8.2 : BRS in Carrier claim:End

                sBRSSplitData = BRSCommon.PutCurrentItemMessage(oCurrentItemMessage.ToString(), sBRSSplitData);

                //Set the new row position to the list
                oBRSMessage = XElement.Parse(sBRSSplitData);
            }
            else
            {
                BRSCommon.RemoveCurrentBRSItem(ref oBRSMessage);

                //Chck if the next target item is after last one
                string sLastRowPosition = oBRSMessage.XPathSelectElement("//FundsBRSSplits").Attribute("lastrowposition").Value;
                
                //If there is no item entered, just close the popup window
                if (sLastRowPosition == "1")
                {
                    string script = "<script>window.close();</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", script);
                    return;
                }                
                
                oBRSMessage.XPathSelectElement("//FundsBRSSplits").Attribute("selectedrowposition").Value = sLastRowPosition;
                sBRSSplitData = oBRSMessage.ToString();
            }
            AddCurrentBillItem.Text = string.Empty;

            string sSelectedRowPosition = oBRSMessage.XPathSelectElement("./Document/FundsBRSSplits").Attribute("selectedrowposition").Value;
            XElement oLatestItem = oBRSMessage.XPathSelectElement("./Document/FundsBRSSplits/option[idx='" + sSelectedRowPosition + "']");
            string sCalcType = oLatestItem.XPathSelectElement("calctype").Value;
            if (sCalcType == FLHOSP)
            {
                //remove the blank entry and listhead
                string sBlankItem = oBRSMessage.XPathSelectElement("./Document/FundsBRSSplits/option[idx='-1']").ToString();
                oBRSMessage.XPathSelectElement("./Document/FundsBRSSplits/option[idx='-1']").Remove();
                string sListHead = oBRSMessage.XPathSelectElement("./Document/FundsBRSSplits/listhead").ToString();
                oBRSMessage.XPathSelectElement("./Document/FundsBRSSplits/listhead").Remove();

                //set the cmd to complete
                foreach (XElement oItem in oBRSMessage.XPathSelectElement("/Document/FundsBRSSplits").Elements())
                {
                    oItem.Element("cmd").Value = "complete";
                }

                XElement oFunction = oBRSMessage.XPathSelectElement("./Call/Function");
                oFunction.Value = "BRSAdaptor.BRSEntrypoint";

                string sResponse = AppHelper.CallCWSService(oBRSMessage.ToString());
                lblError.Text = ErrorHelper.FormatServiceErrors(sResponse);
                //If CWS call failed or any error happens during CWS call, stay in the BRS rather than 
                //return back to Funds page
                if (ErrorHelper.IsCWSCallSuccess(sResponse) && !ErrorHelper.IsAnyErrorInCWSCall(sResponse))
                {
                    oBRSMessage = XElement.Parse(sResponse);
                    oBRSMessage.XPathSelectElement("//FundsBRSSplits").AddFirst(XElement.Parse(sListHead));
                    oBRSMessage.XPathSelectElement("//FundsBRSSplits").Add(XElement.Parse(sBlankItem));
                    sBRSSplitData = oBRSMessage.ToString();
                }
                else
                {
                    return;
                }
            }

            //set SplitRowId to negative ticks if it's 0 or null
            sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
            oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);
            
            XElement oSplitRowId = oCurrentItemMessage.XPathSelectElement("//SplitRowId");
            string sSplitRowId = oSplitRowId.Value;
            if (string.IsNullOrEmpty(sSplitRowId) || sSplitRowId == "0")
            {
                long ticks = DateTime.Now.Ticks * (-1);
                oSplitRowId.Value = ticks.ToString();
            }
            sBRSSplitData = BRSCommon.PutCurrentItemMessage(oCurrentItemMessage.ToString(), sBRSSplitData);
            XElement oBRSSplitData = XElement.Parse(sBRSSplitData);

            oBRSSplitData = BRSCommon.FormatAmountCurrency(oBRSSplitData);
            //Mona: preventing duplication of BRS splits
            //In case new is clicked from the popup and then return is clicked
            if (string.Compare(PostbackAction.Text, "Return", true) == 0)
            {
                BrsDataForDuplicateChk.Text = oBRSSplitData.ToString();          
                bIsDuplicate = CheckForDuplication(oBRSSplitData, oCurrentItemMessage);
            }
           if (!bIsDuplicate)
           {
               btnReturnAfterDupChk(oBRSSplitData.ToString());
           }
        }


        protected void btnNew_Click(object sender, EventArgs e)
        {
            string sBRSSplitData = string.Empty;

            string sCurrentItemMessage = string.Empty;
            XElement oCurrentItemMessage = null;
            bool bIsDuplicate = false;

            if (ViewState["BRSSplitData"] != null)
            {
                sBRSSplitData = ViewState["BRSSplitData"].ToString();
                sBRSSplitData = BindControls2BRSData(sBRSSplitData);
            }

            XElement oBRSSplitData = XElement.Parse(sBRSSplitData);
            //Mona: preventing duplication of BRS splits
            sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
            oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);
            BrsDataForDuplicateChk.Text = sBRSSplitData;
            bIsDuplicate =   CheckForDuplication(oBRSSplitData, oCurrentItemMessage);
            if (!bIsDuplicate)
            {
                btnNewAfterDupChk(sBRSSplitData);
            }
            //Mona: preventing duplication of BRS splits           
            // mkaran2 - Start - MITS #28419
            XElement oDateOfService = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/FromDate");
            XElement oProcedureCode = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/billingcode");
            XElement oBilledAmt = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/amountbilled");
            XElement oPaidAmt = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/amounttopay");
            XElement oSavedAmt = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/amountsaved");
            StringBuilder sbBRSSummary = new StringBuilder();

            sbBRSSummary.Append("|");
            sbBRSSummary.Append(oDateOfService.Value + "^");
            sbBRSSummary.Append(oProcedureCode.Value + "^");
            sbBRSSummary.Append(oBilledAmt.Value + "^");
            sbBRSSummary.Append(oPaidAmt.Value + "^");
            sbBRSSummary.Append(oSavedAmt.Value);

            hdnBRSSummary.Value = hdnBRSSummary.Value + sbBRSSummary.ToString();            
           
            // mkaran2 - End - MITS #28419
        }

        protected void btnMoveFirst_Click(object sender, EventArgs e)
        {
            Navigation(2);
        }

        protected void btnMovePrevious_Click(object sender, EventArgs e)
        {
            int iCurrentRowPosition = int.Parse(selectedrowposition.Text);
            Navigation(iCurrentRowPosition - 1);
        }

        protected void btnMoveNext_Click(object sender, EventArgs e)
        {
            int iCurrentRowPosition = int.Parse(selectedrowposition.Text);
            Navigation(iCurrentRowPosition + 1);
        }

        protected void btnMoveLast_Click(object sender, EventArgs e)
        {
            int iLastRowPosition = int.Parse(lastrowposition.Text);
            Navigation(iLastRowPosition);
        }

        protected void Navigation(int iNextRowPosition)
        {
            if (ViewState["BRSSplitData"] != null)
            {
                string sBRSSplitData = ViewState["BRSSplitData"].ToString();
                XElement oBRSSplitData = XElement.Parse(sBRSSplitData);

                string sCurrentItemMessage = string.Empty;
                XElement oCurrentItemMessage = null;

                //If the current record is being added and not need to be removed, save the change on the page
                if (AddCurrentBillItem.Text != "false")
                {
                    sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
                    oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);

                    BindNonFDMCWSControlCollection2Data(this.Form.Controls, oCurrentItemMessage);
                    sBRSSplitData = BRSCommon.PutCurrentItemMessage(oCurrentItemMessage.ToString(), sBRSSplitData);

                    //Set the new row position to the list
                    oBRSSplitData = XElement.Parse(sBRSSplitData);
                    oBRSSplitData.XPathSelectElement("./Document/FundsBRSSplits").Attribute("selectedrowposition").Value = iNextRowPosition.ToString();
                }
                else
                {
                    BRSCommon.RemoveCurrentBRSItem(ref oBRSSplitData);

                    //Chck if the next target item is after last one
                    int iLastRowPosition = int.Parse(oBRSSplitData.XPathSelectElement("//FundsBRSSplits").Attribute("lastrowposition").Value);
                    if (iNextRowPosition > iLastRowPosition)
                        iNextRowPosition = iLastRowPosition;

                    oBRSSplitData.XPathSelectElement("//FundsBRSSplits").Attribute("selectedrowposition").Value = iNextRowPosition.ToString();
                }
                AddCurrentBillItem.Text = string.Empty;

                sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(oBRSSplitData.ToString());
                oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);

                XElement oStep = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/step");
                if (oStep != null)
                    oStep.Value = ((int)eSteps.LoadBRSXML).ToString();

                XElement oCmd = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/cmd");
                if (oCmd != null)
                    oCmd.Value = string.Empty;

                XElement oFunction = oCurrentItemMessage.XPathSelectElement("./Call/Function");
                oFunction.Value = "BRSAdaptor.BRSEntrypoint";

                string sResponse = AppHelper.CallCWSService(oCurrentItemMessage.ToString());

                BindBRSData2Controls(sResponse);
                ViewState["BRSSplitData"] = BRSCommon.PutCurrentItemMessage(sResponse, oBRSSplitData.ToString());
            }
        }

        //Mona: R8.2 : BRS in Carrier claim
        private void EnableDisableFirstFinalPayment(XElement objElem)
        {
            

            bool bConverted = false;
            bool bEnabledValue = false;
          
            if (objElem.XPathSelectElement("./Document/FundsBRSSplits/option/SplitRowId") != null)
            {
                long lSplitRowID = Convert.ToInt64(objElem.XPathSelectElement("./Document/FundsBRSSplits/option/SplitRowId").Value);


                if (lSplitRowID > 0)
                {
                    //split is saved in db so make firstffinalpayment disabled                        
                    FirstFinalPayment.Enabled = false;
                }
                else
                {
                    //check if querystring has any value
                    if (objElem.XPathSelectElement("./Document/FundsBRSSplits/option/IsFirstFinalControlReradOnlyQueryString") != null)
                    {
                        bEnabledValue = Convert.ToBoolean(objElem.XPathSelectElement("./Document/FundsBRSSplits/option/IsFirstFinalControlReradOnlyQueryString").Value);
                        FirstFinalPayment.Enabled = !(bEnabledValue);
                    }
                    else
                    {
                        //no value supplied, default behaviour                           
                        FirstFinalPayment.Enabled = true;
                    }
                }

            }
        }

        //Mona: preventing duplication of BRS splits
        private bool CheckForDuplication(XElement p_oXml, XElement p_oCurrentItem)
        {
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sbillingcode = string.Empty;

            StringBuilder javaScript = new StringBuilder();
            string ConfirmMessage = string.Empty;
            XAttribute objAttr = null;
            string sBrsMode = string.Empty;
            string sDupChkFlag = string.Empty;
            string sDupCriteria = string.Empty;

            string sPLFromDate = string.Empty;
            string sPLToDate = string.Empty;
            string sPLbillingcode = string.Empty;


            objAttr = p_oXml.XPathSelectElement("./Document/FundsBRSSplits").Attribute("brsmode");
            sBrsMode = objAttr.Value;

            if (string.Compare(sBrsMode, "clone") == 0)
            {
                return false;
            }

            objAttr = p_oXml.XPathSelectElement("./Document/FundsBRSSplits").Attribute("DuplicateChkFlag");
            sDupChkFlag = objAttr.Value;

            objAttr = p_oXml.XPathSelectElement("./Document/FundsBRSSplits").Attribute("DuplicateCriteria");
            sDupCriteria = objAttr.Value;
            //We will only chk in case of 
            if (string.Compare(sDupChkFlag, "-1", true) == 0) 
            {
                switch (sDupCriteria)
                {
                    case "1": //From ,to date and transaction type should not be same
                        XElement oFromDate = p_oCurrentItem.XPathSelectElement("./Document/FundsBRSSplits/option/FromDate");
                        sFromDate = oFromDate.Value;

                        XElement oToDate = p_oCurrentItem.XPathSelectElement("./Document/FundsBRSSplits/option/ToDate");
                        sToDate = oToDate.Value;

                        XElement oBillingCode = p_oCurrentItem.XPathSelectElement("./Document/FundsBRSSplits/option/billingcode");
                        sbillingcode = oBillingCode.Value;

                        //In case we are editing split and are not changing from,to date and transaction type,then prompt should not come
                       
                    if (sBrsMode == "edit")
                    {
                        objAttr = p_oXml.XPathSelectElement("./Document/FundsBRSSplits").Attribute("FromDateForDupChk");
                        sPLFromDate = objAttr.Value;

                        objAttr = p_oXml.XPathSelectElement("./Document/FundsBRSSplits").Attribute("FromDateForDupChk");
                        sPLToDate = objAttr.Value;

                        objAttr = p_oXml.XPathSelectElement("./Document/FundsBRSSplits").Attribute("billingcodeForDupChk");
                        sPLbillingcode = objAttr.Value;

                        if (sPLFromDate == sFromDate && sPLToDate == sToDate && sPLbillingcode == sbillingcode)
                        {
                            return false;
                        }
                        
                    }

                    IEnumerable<XElement> obj = p_oXml.XPathSelectElements("./Document/FundsBRSSplits/option[FromDate='" + sFromDate + "' and ToDate='" + sToDate + "' and billingcode='" + sbillingcode + "']");
                        int i = obj.Count();
                        if (i > 1)
                        {
                            ConfirmMessage = "This is a duplicate split. Click OK to proceed or Cancel to stay on same page?";
                            string scriptKey = "ConfirmationScript";

                            javaScript.AppendFormat("var userConfirmation = window.confirm('{0}');\n", ConfirmMessage);
                          
                            javaScript.Append("__doPostBack('UserConfirmationPostBack', userConfirmation);\n");

                            ClientScript.RegisterStartupScript(GetType(), scriptKey, javaScript.ToString(), true);
                            return true;
                        }
                        break;
                }                
            }
            return false;
        }

        private void btnNewAfterDupChk(string sBRSSplitData)
        {
            XElement oBRSSplitData = null;


            oBRSSplitData = XElement.Parse(sBRSSplitData);


            BRSCommon.AddNewBRSItem("add", oBRSSplitData, false);

            sBRSSplitData = oBRSSplitData.ToString();

            string sCurrentItemMessage = BRSCommon.GetCurrentItemMessage(sBRSSplitData);
            XElement oCurrentItemMessage = XElement.Parse(sCurrentItemMessage);

            XElement oStep = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/step");
            if (oStep != null)
                oStep.Value = ((int)eSteps.LoadBRSXML).ToString();

            XElement oCmd = oCurrentItemMessage.XPathSelectElement("./Document/FundsBRSSplits/option/cmd");
            if (oCmd != null)
                oCmd.Value = string.Empty;

            XElement oFunction = oCurrentItemMessage.XPathSelectElement("./Call/Function");
            oFunction.Value = "BRSAdaptor.BRSEntrypoint";

            string sResponse = AppHelper.CallCWSService(oCurrentItemMessage.ToString());
            lblError.Text = ErrorHelper.FormatServiceErrors(sResponse);
            BindBRSData2Controls(sResponse);
            ViewState["BRSSplitData"] = BRSCommon.PutCurrentItemMessage(sResponse, sBRSSplitData);

            lblError.Text = string.Empty;
            hTabName.Text = "Date";
            DatabindingHelper.UpdateTabFocus(this);
            AddItemInProgress.Text = "true";
        }

        private void btnReturnAfterDupChk(string sBRSSplitData)
        {
            XElement oBRSSplitData = null;


            oBRSSplitData = XElement.Parse(sBRSSplitData);
            txtData.Text = oBRSSplitData.XPathSelectElement("//FundsBRSSplits").ToString();
            ReturnButtonClicked.Text = "true";

            string scriptClose = "<script>SaveDataBack();</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", scriptClose);

            PleaseWaitDialog1.CustomMessage = "Saving data to Funds page ...";
        }
    }
}
