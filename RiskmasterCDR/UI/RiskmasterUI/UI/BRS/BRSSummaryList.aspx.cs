﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Collections;
using System.Text.RegularExpressions;
using Riskmaster.UI.BRS;

namespace Riskmaster.UI.UI.BRS
{
    public partial class BRSSummaryList : Riskmaster.UI.NonFDMBasePageCWS
    {
        // mkaran2 - BRSsummary Page - MITS #28419
        decimal grdTotalBilledAmt = 0;
        decimal grdTotalPaidAmt = 0;
        decimal grdTotalSavedAmt = 0;
        string dateofservice = string.Empty;
        string procedurecode = string.Empty;
        string billedamt = string.Empty;
        string paidamt = string.Empty;
        string savedamt = string.Empty;
        private BRSSummary brsRec;
        private List<BRSSummary> lstBRS = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {               
                if (Request.QueryString["dateofservice"] != null)
                {
                    dateofservice = Request.QueryString["dateofservice"];
                }
                if (Request.QueryString["procedurecode"] != null)
                {
                    procedurecode = Request.QueryString["procedurecode"];
                }
                if (Request.QueryString["billedamt"] != null)
                {
                    billedamt = Request.QueryString["billedamt"];
                }
                if (Request.QueryString["paidamt"] != null)
                {
                    paidamt = Request.QueryString["paidamt"];
                }
                if (Request.QueryString["savedamt"] != null)
                {
                    savedamt = Request.QueryString["savedamt"];
                }

                BindGrid();
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
            }
        }
        public void BindGrid()
        {
            try
            {
                lstBRS = new List<BRSSummary>();

                if (hdnBRSSummaryList.Value != null)
                {
                    string sBRSList = hdnBRSSummaryList.Value;
                    string[] sPairs = sBRSList.Split('|');
                    if (sPairs.Count() > 1)
                    {
                        for (int i = 1; i < sPairs.Count(); i++)
                        {
                            string[] sPairsData = sPairs[i].Split('^');

                            brsRec = new BRSSummary(sPairsData[0], sPairsData[1], sPairsData[2], sPairsData[3], sPairsData[4]);
                            lstBRS.Add(brsRec);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(dateofservice) && !string.IsNullOrEmpty(procedurecode) && billedamt != "$0.00")
                {
                    brsRec = new BRSSummary(dateofservice, procedurecode, billedamt, paidamt, savedamt);
                    lstBRS.Add(brsRec);
                }

                if (lstBRS.Count == 0)
                {
                    brsRec = new BRSSummary("", "", "0.00", "0.00", "0.00");
                    lstBRS.Add(brsRec);
                }

                gvBRSSummaryList.DataSource = lstBRS;
                gvBRSSummaryList.DataBind();
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
            }
        }
        protected void gvBRSSummaryList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[3].Text = Regex.Replace(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "BilledAmt")), @"[^0-9\.\,]", string.Empty);
                    e.Row.Cells[4].Text = Regex.Replace(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PaidAmt")), @"[^0-9\.\,]", string.Empty);
                    e.Row.Cells[5].Text = Regex.Replace(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SavedAmt")), @"[^0-9\.\,]", string.Empty);


                    grdTotalBilledAmt += Convert.ToDecimal(e.Row.Cells[3].Text);
                    grdTotalPaidAmt += Convert.ToDecimal(e.Row.Cells[4].Text);
                    grdTotalSavedAmt += Convert.ToDecimal(e.Row.Cells[5].Text);
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    Label lblTotalBilledAmt = (Label)e.Row.FindControl("lblTotalBilledAmt");
                    lblTotalBilledAmt.Text = string.Format("{0:N}", grdTotalBilledAmt);

                    Label lblTotalPaidAmt = (Label)e.Row.FindControl("lblTotalPaidAmt");
                    lblTotalPaidAmt.Text = string.Format("{0:N}", grdTotalPaidAmt);

                    Label lblTotalSavedAmt = (Label)e.Row.FindControl("lblTotalSavedAmt");
                    lblTotalSavedAmt.Text = string.Format("{0:N}", grdTotalSavedAmt);
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
            }
        }

    }
}