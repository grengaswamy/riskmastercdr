﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.BRS
{
    public partial class BillingCodeLookup : NonFDMBasePageCWS
    {
        protected string sCodeTable = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //zmohammad MITS 38406 Start
            if (!IsPostBack)
            {
                pageaction.Value = Request.RawUrl;
            }
            //zmohammad MITS 38406 End
            string sResponse = string.Empty;
            string sFunctionName = "BRSAdaptor.GetBillingCodes";
            XElement oRequestElement = GetMessageTemplate();

            //Get values from querystring  
            //Pen testing :atavaragiri :mits 27824
            //string sFeeScheduleID = AppHelper.GetQueryStringValue("feescheduleid");
            string sFeeScheduleID =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("feescheduleid"));
            //END Pen testing :atavaragiri :mits 27824
            XElement oEle = oRequestElement.XPathSelectElement("//Document/BillingCodesDoc/BillingCodesParams/FeeScheduleId");
            oEle.Value = sFeeScheduleID;
            //Pen testing :atavaragiri :mits 27824
            //string sFeeScheduleID2 = AppHelper.GetQueryStringValue("feescheduleid2");   
            string sFeeScheduleID2 = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("feescheduleid2"));
            //END Pen testing :atavaragiri :mits 27824
            oEle = oRequestElement.XPathSelectElement("//Document/BillingCodesDoc/BillingCodesParams/FeeScheduleId2");
            oEle.Value = sFeeScheduleID2;

            //Pen testing :atavaragiri :mits 27824
            //string sFind = AppHelper.GetQueryStringValue("find"); 
            string sFind = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("find"));
            //END Pen testing :atavaragiri :mits 27824
            oEle = oRequestElement.XPathSelectElement("//Document/BillingCodesDoc/BillingCodesParams/Find");
            oEle.Value = sFind;

            //page number
            oEle = oRequestElement.XPathSelectElement("//Document/BillingCodesDoc/BillingCodesParams/RequestedPageNumber");
            //Pen testing :atavaragiri :mits 27824
            //oEle.Value = pagenumber.Value; 
            oEle.Value = AppHelper.HTMLCustomEncode(pagenumber.Value);
            //END Pen testing :atavaragiri :mits 27824
            CallCWSFunctionBind(sFunctionName, out sResponse, oRequestElement);
            lblError.Text = ErrorHelper.FormatServiceErrors(sResponse);

            DataSet dSet = null;
            XmlDocument oResponseXDoc = new XmlDocument();
            oResponseXDoc.LoadXml(sResponse);
            XmlDocument oCodeXDoc = new XmlDocument();
            XmlNode oNode = oResponseXDoc.SelectSingleNode("//Document/BillingCodesDoc/codes");

            if (oNode == null)
            {
                divNoRecord.Style["display"] = string.Empty;
                lblNoRecord.Text = string.Format("Zero records match the criteria '{0}'", sFind);
                return;
            }

            pagecount.Value = oNode.Attributes["pagecount"].Value;
            if( oNode.Attributes["thispage"] != null )
                pagenumber.Value = oNode.Attributes["thispage"].Value;
            displayname.Value = "Billing Code";
            if( oNode.Attributes["nextpage"] != null )
                nextpage.Value = oNode.Attributes["nextpage"].Value;

            //If there is only one record, just close the page and put the value
            if (oNode.ChildNodes.Count == 1)
            {
                string sId = oNode.ChildNodes[0].Attributes["shortcode"].Value;
                string sDesc = sId + " - " + oNode.ChildNodes[0].Attributes["codedesc"].Value;
                pageBody.Attributes["onload"] = String.Format("selBillingCode('{0}', '{1}')", sDesc.Replace("'", "%27"), sId);
            }

            oCodeXDoc.LoadXml(oNode.OuterXml);
            dSet = new DataSet();
            dSet.ReadXml(new XmlNodeReader(oCodeXDoc));
            if (dSet.Tables["code"] != null)
            {
                dgCodes.DataSource = dSet.Tables["code"];
                dgCodes.DataBind();
            }
            else
            {
                divNoRecord.Style["display"] = string.Empty;
                lblNoRecord.Text = string.Format("Zero records match the criteria '{0}'", sFind);
            }
        }

        protected void dgCodes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String sShortCode = string.Empty;
                String sDesc = string.Empty;
                String sId = string.Empty;
                string sLink = string.Empty;
                if (DataBinder.Eval(e.Row.DataItem, "shortcode") == null)
                {
                    sShortCode = string.Empty;
                }
                else
                {
                    sShortCode = DataBinder.Eval(e.Row.DataItem, "shortcode").ToString();
                }
                if (DataBinder.Eval(e.Row.DataItem, "codedesc") == null)
                {
                    sDesc = string.Empty;
                }
                else
                {
                    sDesc = DataBinder.Eval(e.Row.DataItem, "codedesc").ToString();
                }
                
                if (DataBinder.Eval(e.Row.DataItem, "Id") == null)
                {
                    sId = string.Empty;
                }
                else
                {
                    sId = DataBinder.Eval(e.Row.DataItem, "Id").ToString();
                }

                sDesc = sId + " - " + sDesc;
                e.Row.Cells[1].Text = "<a class='HeaderNavy' href='#'>" + sDesc + "</a>";
                e.Row.Cells[1].Attributes["onclick"] = String.Format("selBillingCode('{0}', '{1}')", sDesc.Replace("'", "%27"), sId);
            }
        }
        protected void dgCodes_PreRender(object sender, EventArgs e)
        {
            if (dgCodes.Rows.Count >= 1)
            {
                dgCodes.Rows[dgCodes.Rows.Count - 1].Visible = false;  //done to show grid if no codes are there in code table
            }
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement objTemplate = XElement.Parse(@"
				<Message>
					<Authorization></Authorization>
					<Call>
						<Function>BRSAdaptor.GetBillingCodes</Function>
					</Call>
					<Document>
						<BillingCodesDoc>
							<BillingCodesParams>
								<FeeScheduleId>6</FeeScheduleId>
								<FeeScheduleId2>0</FeeScheduleId2>
								<CPT/>
								<Desc/>
								<RequestedPageNumber>0</RequestedPageNumber>
								<TotalRecordCount/>
								<PageCount>0</PageCount>
								<Unload>false</Unload>
								<code/>
								<LOB/>
								<Find/>
							</BillingCodesParams>
						</BillingCodesDoc>
					</Document>
				</Message>
            ");

            return objTemplate;
        }
    }
}
