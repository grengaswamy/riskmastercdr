<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillingCodeLookup.aspx.cs" Inherits="Riskmaster.UI.BRS.BillingCodeLookup" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Code Selection</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/getcode.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/brs.js"></script>
</head>
<body class="10pt" id="pageBody" runat="server">
    <form id="frmData" runat="server">
    <br />
    <input type="hidden" id="bUnload" />
    <asp:HiddenField ID ="pagecount" runat ="server" value="0"/>
    <asp:HiddenField ID ="pagenumber" runat ="server" value="0"/>
    <asp:HiddenField ID ="nextpage" runat ="server" value="0"/>
    <asp:HiddenField ID="displayname" runat ="server" /> 
    <table width="95%" cellspacing="0" cellpadding="0">
        <tr>
            <td class="ctrlgroup"><%=displayname.Value%></td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lblError" runat="server"></asp:Label>
    <%if (Convert.ToInt32(pagecount.Value)>1)   %>
    <%{ %>
    <table width="95%" cellspacing="0" cellpadding="1" border="0">
        <tr>
            <td colspan="4" class="headertext1">Page <%=Convert.ToInt32(pagenumber.Value)%> of <%=Convert.ToInt32(pagecount.Value)%>
            </td>
            <td nowrap="" colspan="2" align="right" class="headertext1">
      								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
      									<%if (Convert.ToInt32(pagenumber.Value) > 1) %>
      										<%{
                                                 nextpage.Value = (Convert.ToInt32(pagenumber.Value) - 1).ToString();  
                                            %>   
      										<a class="headerlink" href="#" onclick="getPage(1)">First </a> 
      										&nbsp;&nbsp;|&nbsp;
      										<a class="headerlink" href="#" onclick="getPage('<%=nextpage.Value%>')">&lt; Prev</a>
       										<%}%>
       										<%else %>
       										<%{%>
       										First 
       										&nbsp;&nbsp;|&nbsp;
      										&lt; Prev
       										<%}%> 
      									&nbsp;&nbsp;
      									<%if (Convert.ToInt32(pagenumber.Value) < Convert.ToInt32(pagecount.Value)) %>
      										<%{
                                                 nextpage.Value = (Convert.ToInt32(pagenumber.Value) + 1).ToString();
      										%>   
      								        <a class="headerlink" href="#" onclick="getPage('<%=nextpage.Value%>')">Next &gt;</a> 
      									    &nbsp;&nbsp;|&nbsp;&nbsp;
      									    <%nextpage.Value =pagecount.Value; %>
      								        <a class="headerlink" href="#" onclick="getPage('<%=nextpage.Value%>')">Last</a>
      									    &nbsp;
      									    <%}%>
       										<%else %>
       										<%{%>
       										Next &gt;
       										&nbsp;&nbsp;|&nbsp;&nbsp;
       										Last
       										&nbsp;
       										<%}%>
      							
     </td>
        </tr>
     </table>  
     <%} %>
    
    <asp:GridView ID="dgCodes" AutoGenerateColumns="False" runat="server" 
        Font-Bold="True" CellPadding="0"  PagerSettings-Mode="NumericFirstLast"
        GridLines="None" CellSpacing="3" Width="95%" OnRowDataBound ="dgCodes_RowDataBound"
        onprerender="dgCodes_PreRender" EnableViewState="false">
        <%--OnSorting="grdView_OnSorting" OnPageIndexChanging="grdView_PageIndexChanging" AllowSorting="True" AllowPaging="True" OnRowCreated="GridView_RowCreated" --%>
        <PagerStyle CssClass="headertext2" ForeColor="White" />
        <HeaderStyle CssClass="msgheader"/>
        <PagerSettings Mode="NumericFirstLast"></PagerSettings>
        <FooterStyle CssClass="headertext2" />
        <RowStyle CssClass="Bold2" />
        <Columns>
            <asp:TemplateField SortExpression="1" HeaderText="Code" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                <ItemTemplate>
                    <%# Eval("shortcode")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="2" HeaderText="Parent Code" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <%# Eval("codedesc")%>
                </ItemTemplate>
                <ControlStyle CssClass="msgheader" />
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
    </asp:GridView>
    <%if (Convert.ToInt32(pagecount.Value)>1)   %>
    <%{ %>
    <table width="95%" cellspacing="0" cellpadding="1" border="0">
        <tr>
            <td colspan="4" class="headertext1">Page <%=Convert.ToInt32(pagenumber.Value)%> of <%=Convert.ToInt32(pagecount.Value)%>
            </td>
            <td nowrap="" colspan="2" align="right" class="headertext1">
      								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
      									<%if (Convert.ToInt32(pagenumber.Value) > 1) %>
      										<%{
                                                 nextpage.Value = (Convert.ToInt32(pagenumber.Value) - 1).ToString();  
                                            %>   
      										<a class="headerlink" href="#" onclick="getPage(1)">First </a> 
      										&nbsp;&nbsp;|&nbsp;
      										<a class="headerlink" href="#" onclick="getPage('<%=nextpage.Value%>')">&lt; Prev</a>
       										<%}%>
       										<%else %>
       										<%{%>
       										First 
       										&nbsp;&nbsp;|&nbsp;
      										&lt; Prev
       										<%}%> 
      									&nbsp;&nbsp;
      									<%if (Convert.ToInt32(pagenumber.Value) < Convert.ToInt32(pagecount.Value)) %>
      										<%{
                                                 nextpage.Value = (Convert.ToInt32(pagenumber.Value) + 1).ToString();
      										%>   
      								        <a class="headerlink" href="#" onclick="getPage('<%=nextpage.Value%>')">Next &gt;</a> 
      									    &nbsp;&nbsp;|&nbsp;&nbsp;
      									    <%nextpage.Value =pagecount.Value; %>
      								        <a class="headerlink" href="#" onclick="getPage('<%=nextpage.Value%>')">Last</a>
      									    &nbsp;
      									    <%}%>
       										<%else %>
       										<%{%>
       										Next &gt;
       										&nbsp;&nbsp;|&nbsp;&nbsp;
       										Last
       										&nbsp;
       										<%}%>
      							
     </td>
        </tr>
     </table>  
     <%} %>  
    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Please wait while we process your request.." />
    <asp:HiddenField ID="hdnSortExpression" runat="server" Value="1" />
    <asp:HiddenField ID ="pageaction" runat ="server" />
    <div id="divNoRecord" runat="server" style="display:none">
        <table border="0" width="100%">
            <tr>
                <td  width="100%" valign="middle" align="center"><asp:Label ID="lblNoRecord" runat="server" Text="" Font-Size="12" Font-Bold="true" Font-Names="Verdana, Arial"></asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td width="100%" valign="middle" align="center"><input class="button" type="button" name="cmdClose" onClick="window.opener.ClearBillingCodeInput();window.close();" value=" Close "></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
