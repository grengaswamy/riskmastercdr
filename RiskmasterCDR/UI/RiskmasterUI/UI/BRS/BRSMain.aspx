<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSMain.aspx.cs" Inherits="Riskmaster.UI.BRS.BRSMain" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Bill Review System</title>
    <link rel="stylesheet" href="../../Scripts/zapatec/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" src="../../Scripts/brs.js"></script>
    <script language="JavaScript" src="../../Scripts/grid.js"></script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js"></script>
    <%--<script language="JavaScript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script language="JavaScript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script language="JavaScript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script language="JavaScript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>

     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <script language="JavaScript" src="../../Scripts/supportscreens.js"></script>
  </head>
<body class="10pt" onload="loadTabList();pageLoadedBrs(); addToolTip();">
    <form name="frmData" id="frmData" runat="server">
        <asp:Label ID="lblError" runat="server" Text="" />
        <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" Text="Date"/>
        <asp:ScriptManager ID="SMgr" runat="server" />
        <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server" visible="false">
        <div class="toolBarButton" runat="server" id="div_new" xmlns="">
          <asp:ImageButton runat="server" src="../../Images/tb_new_active.png" width="28" height="28" border="0" id="new" title="New" AlternateText="New" onMouseOver="this.src='../../Images/tb_new_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_new_active.png';this.style.zoom='100%'" OnClientClick="return OnNewBillItem();" OnClick="btnNew_Click"/>
        </div>
        <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
          <asp:ImageButton runat="server" src="../../Images/tb_first_active.png" width="28" height="28" border="0" id="movefirst" title="Move First" AlternateText="Move First" onMouseOver="this.src='../../Images/tb_first_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_first_active.png';this.style.zoom='100%'" OnClick="btnMoveFirst_Click" OnClientClick="return OnFirstBillItem();"/>
        </div>
        <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
          <asp:ImageButton runat="server" src="../../Images/tb_previous_active.png" width="28" height="28" border="0" id="moveprevious" title="Move Previous" AlternateText="Move Previous" onMouseOver="this.src='../../Images/tb_previous_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_previous_active.png';this.style.zoom='100%'" OnClick="btnMovePrevious_Click" OnClientClick="return OnPreviousBillItem();" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
          <asp:ImageButton runat="server" src="../../Images/tb_next_active.png" width="28" height="28" border="0" id="movenext" title="Move Next" AlternateText="Move Next" onMouseOver="this.src='../../Images/tb_next_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_next_active.png';this.style.zoom='100%'" OnClick="btnMoveNext_Click" OnClientClick="return OnNextBillItem();" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
          <asp:ImageButton runat="server" src="../../Images/tb_last_active.png" width="28" height="28" border="0" id="movelast" title="Move Last" AlternateText="Move Last" onMouseOver="this.src='../../Images/tb_last_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_last_active.png';this.style.zoom='100%'" OnClick="btnMoveLast_Click" OnClientClick="return OnLastBillItem();" />
        </div>
      </div>
        <div class="msgheader" id="div_formtitle" runat="server">
            <asp:label id="formtitle" runat="server" Text="Bill Review System" />
            <asp:label id="formsubtitle" runat="server" Text="" />
        </div>
        <div id="Div1" class="errtextheader" runat="server">
            <asp:label id="formdemotitle" runat="server" Text="" />
        </div>
        <div class="tabGroup" id="TabsDivGroup" runat="server">
            <div class="Selected" nowrap="true" runat="server" id="TABSDate">
              <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="Date" id="LINKTABSDate">Dates, Zip and Fee Table</a>
            </div>
            <div id="Div3" class="tabSpace" runat="server">
              <nbsp />
              <nbsp />
            </div>
            <div class="NotSelected" nowrap="true" runat="server" id="TABSBillItem">
              <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" name="BillItem" RMXRef="" id="LINKTABSBillItem">Bill Item Detail</a>
            </div>
            <div class="tabSpace">
              <nbsp />
              <nbsp />
            </div>
            <div class="NotSelected" nowrap="true" runat="server" id="TABScontract" visible="false">
              <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="contract" id="LINKTABScontract">Contract Information (read-only)</a>
            </div>
            <div class="tabSpace">
              <nbsp />
              <nbsp />
            </div>
            <div class="NotSelected" nowrap="true" runat="server" id="TABSoverridegroup" visible="false">
              <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="overridegroup" id="LINKTABSoverridegroup">Override Options</a>
            </div>
            <div class="tabSpace">
              <nbsp />
              <nbsp />
            </div>
            <div class="NotSelected" nowrap="true" runat="server" id="TABSCalculate">
              <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="Calculate" id="LINKTABSCalculate">Calculate</a>
            </div>
            <div class="tabSpace">
              <nbsp />
              <nbsp />
            </div>
        </div>
        <div class="singletopborder" runat="server" name="FORMTABDate" id="FORMTABDate" style="position:inherit;">
            <table width="98%" border="0" cellspacing="0" celpadding="0">
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_dfromdate" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_dfromdate" Text="From Date" />
                          <span class="formw">
                            <asp:TextBox runat="server" id="dfromdate" RMXType="date" tabindex="1" onchange="setDataChanged(true);PopulateToDate(this.id);" onblur="dateLostFocus(this.id);" RMXRef="/Instance/Document/FundsBRSSplits/option/FromDate"/>
                           <%-- <asp:button class="DateLookupControl" runat="server" id="dfromdatebtn" tabindex="5" />
                            <script type="text/javascript">
                            Zapatec.Calendar.setup(
                                {
                                    inputField: "dfromdate",
                                    ifFormat: "%m/%d/%Y",
                                    button: "dfromdatebtn"
                                }
                            );
                            </script>--%>
                              <script type="text/javascript">
                                  $(function () {
                                      $("#dfromdate").datepicker({
                                          showOn: "button",
                                          buttonImage: "../../Images/calendar.gif",
                                          //buttonImageOnly: true,
                                          showOtherMonths: true,
                                          selectOtherMonths: true,
                                          changeYear: true
                                      }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");
                                  });
                            </script>
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_dtodate" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_dtodate" Text="To Date" />
                          <span class="formw">
                           <asp:TextBox runat="server" id="dtodate" RMXType="date" tabindex="10" onchange="setDataChanged(true); PopulateToDate(this.id);" onblur="dateLostFocus(this.id);" RMXRef="/Instance/Document/FundsBRSSplits/option/ToDate"/>
                           <%--    mbahl3 Mit 20039--%>
                          <%-- <asp:TextBox runat="server" id="dtodate" RMXType="date" tabindex="10" onchange="setDataChanged(true); ToDateChanged();" onblur="dateLostFocus(this.id);" RMXRef="/Instance/Document/FundsBRSSplits/option/ToDate"/>--%>
                            <%--<asp:button class="DateLookupControl" runat="server" id="dtodatebtn" tabindex="15" />
                            <script type="text/javascript">
                                Zapatec.Calendar.setup(
					                {
					                    inputField: "dtodate",
					                    ifFormat: "%m/%d/%Y",
					                    button: "dtodatebtn"
					                }
					            );
				            </script>--%>
                              <script type="text/javascript">
                                  $(function () {
                                      $("#dtodate").datepicker({
                                          showOn: "button",
                                          buttonImage: "../../Images/calendar.gif",
                                          //buttonImageOnly: true,
                                          showOtherMonths: true,
                                          selectOtherMonths: true,
                                          changeYear: true
                                      }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "11");
                                  });
                            </script>
                          </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_feescheddesc" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_feescheddesc" Text="Table"/>
                          <span class="formw">
                            <asp:DropDownList ID="ddlFeeTable" runat="server" RMXRef="/Instance/Document/FundsBRSSplits/option/feesched/@codeid" ItemSetRef="/Instance/Document/FundsBRSSplits/option/feesched" onchange="setDataChanged(true);" OnSelectedIndexChanged="FeeTableChanged" AutoPostBack="true" TabIndex="20"></asp:DropDownList>
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_feescheddesc2" xmlns="" visible="false">
                          <asp:label runat="server" class="label" id="lbl_feescheddesc2" Text="Contract Table 2" />
                          <span class="formw">
                            <asp:TextBox runat="server" RMXRef="/Instance/Document/FundsBRSSplits/option/feescheddesc2" id="feescheddesc2" tabindex="25" style="background-color: silver;" readonly="True" />
                          </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_zip" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_zip" Text="Zip" />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="zip" RMXRef="/Instance/Document/FundsBRSSplits/option/zip" RMXType="zip" TabIndex="30" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_billtype" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_billtype" Text="Bill Type" />
                          <span class="formw">
                            <uc:CodeLookUp runat="server" ID="billtype" CodeTable="BILLTYPE" ControlName="billtype" RMXRef="/Instance/Document/FundsBRSSplits/option/billtype" RMXType="code" TabIndex="35" />
                          </span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="singletopborder" runat="server" name="FORMTABBillItem" id="FORMTABBillItem" style="display:none;height:75%; position:inherit;">
            <table width="98%" border="0" cellspacing="0" celpadding="0">
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_placeofservice" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_placeofservice" Text="Place of Service" />
                          <span class="formw">
                            <uc:CodeLookUp runat="server" ID="placeofservice" CodeTable="PLACE_OF_SERVICE" ControlName="placeofservice" RMXRef="/Instance/Document/FundsBRSSplits/option/placeofservice" RMXType="code" TabIndex="45" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_typeofservice" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_typeofservice" Text="Type of Service" />
                          <span class="formw">
                            <uc:CodeLookUp runat="server" ID="typeofservice" CodeTable="TYPE_OF_SERVICE" ControlName="typeofservice" RMXRef="/Instance/Document/FundsBRSSplits/option/typeofservice" RMXType="code" TabIndex="50"/>
                          </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_PhysicianLookUp" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_PhysicianLookUp" Text="Physician look-up" />
                          <span class="formw"><asp:Textbox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" RMXRef="/Instance/Document/FundsBRSSplits/option/PhysicianLookUp" RMXType="physicianlookup" id="PhysicianLookUp" TabIndex="55" />
                            <asp:button runat="server" class="button" id="PhysicianLookUpbtn" Text="..." OnClientClick="return lookupData('PhysicianLookUp','physician',-1,'PhysicianLookUp',2);" TabIndex="60" />
                            <asp:TextBox style="display:none" runat="server" id="PhysicianLookUp_cid" RMXRef="/Instance/Document/FundsBRSSplits/option/PhysicianLookUp/@codeid" RMXType="id" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_FLLicense" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_FLLicense" Text="FL License #" />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="FLLicense" RMXRef="/Instance/Document/FundsBRSSplits/option/FLLicense" RMXType="text" TabIndex="65"/>
                         
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_specialty_desc" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_specialty_desc" Text="Specialty" />
                          <span class="formw">
                            <asp:TextBox runat="server" RMXRef="/Instance/Document/FundsBRSSplits/option/specialty_desc" id="specialty_desc" tabindex="50" style="background-color: silver;" readonly="True" />
                          </span>
                        </div>
                </td>
            </tr>
            <%--Mona: R8.2 : BRS in Carrier claim:Start--%>
            <tr>
            <td>
             <div runat="server" class="completerow" id="div_FirstFinalPayment" xmlns="" xmlns:mc="remove">
                <asp:label runat="server" class="required" id="lbl_FirstFinalPayment" Text="First And Final Payment" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="FirstFinalPayment" RMXRef="/Instance/Document/FundsBRSSplits/option/IsFirstFinal/@value" RMXType="checkbox" onclick="FirstFinalPaymentFlagChanged(this)" Height="24px" />
                </span>
              </div>
            </td>
            </tr>

            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_Policy" xmlns="" xmlns:mc="remove">
                        <asp:Label runat="server" class="required" ID="lbl_Policy" Text="Policy" />
                        <span class="formw">
                            <asp:DropDownList runat="server" ID="Policy" AutoPostBack="true" RMXRef="/Instance/Document/FundsBRSSplits/option/PolicyID/@value"
                                RMXType="combobox" ItemSetRef="/Instance/Document/FundsBRSSplits/option/PolicyList" onchange="setDataChanged(true);PolicyChanged()" />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_Unit" xmlns="" xmlns:mc="remove">
                        <asp:Label runat="server" class="required" ID="lbl_Unit" Text="Unit" />
                        <span class="formw">
                            <asp:DropDownList runat="server" ID="Unit" RMXRef="/Instance/Document/FundsBRSSplits/option/UnitID/@value" RMXType="combobox"
                                ItemSetRef="/Instance/Document/FundsBRSSplits/option/UnitList" onchange="setDataChanged(true);UnitChanged()" Width="70 %" />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_Coverage" xmlns="" xmlns:mc="remove">
                        <asp:Label runat="server" class="required" ID="lbl_Coverage" Text="Coverage" />
                        <span class="formw">
                            <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);" ID="Coverage"
                                CodeTable="COVERAGE_TYPE" ControlName="Coverage" RMXRef="/Instance/Document/FundsBRSSplits/option/CoverageTypeCode"
                                CodeFilter="" RMXType="code" Required="true" ValidationGroup="vgSave" />
                        </span>
                    </div>
                </td>
            </tr>
            <asp:TextBox style="display:none" runat="server" id="PolCvgID" RMXRef="/Instance/Document/FundsBRSSplits/option/CoverageId" RMXType="hidden" Text="" />
            <asp:TextBox style="display:none" runat="server" id="PolicyID" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="UnitID" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="clm_entityid" RMXRef="/Instance/Document/FundsBRSSplits/option/clm_entityid" RMXType="hidden" Text="" />

         <asp:TextBox style="display:none" runat="server" id="IsFirstFinalQueryString" RMXRef="" RMXType="hidden" Text="" />
           
            <%--Mona: R8.2 : BRS in Carrier claim:End--%>
                        <tr>
                <td>
                    <div runat="server" class="completerow" id="div_LossType" xmlns="" xmlns:mc="remove">
                        <asp:Label runat="server" class="required" ID="lblLossType" Text="Loss Type" />
                        <span class="formw">
                            <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);" ID="LossType"
                                CodeTable="LOSS_CODES" ControlName="LossType" RMXRef="/Instance/Document/FundsBRSSplits/option/LossTypeCode"
                                CodeFilter="" RMXType="code" Required="true" ValidationGroup="vgSave" />
                        </span>
                    </div>
                </td>
            </tr>
            <asp:TextBox style="display:none" runat="server" id="CvgLossID" RMXRef="" RMXType="hidden" Text="" />
             <tr>
                <td>
                    <div runat="server" class="completerow" id="div_DisabilityCatCode" xmlns="" xmlns:mc="remove">
                        <asp:Label runat="server" class="required" ID="lblDisabilityCatCode" Text="Disability Category" />
                        <span class="formw">
                            <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);" ID="DisabilityCatCode"
                                CodeTable="DISABILITY_CATEGORY" ControlName="DisabilityCatCode" RMXRef="/Instance/Document/FundsBRSSplits/option/DisabilityCatCode"
                                CodeFilter="" RMXType="code" Required="true" ValidationGroup="vgSave" />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_DisabilityLossType" xmlns="" xmlns:mc="remove">
                        <asp:Label runat="server" class="required" ID="Label1" Text="Disability Loss Type" />
                        <span class="formw">
                            <%--//dbisht6 for mits 35554--%>
                            <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);" ID="DisabilityTypeCode"
                                CodeTable="DISABILITY_TYPE" ControlName="DisabilityTypeCode" RMXRef="/Instance/Document/FundsBRSSplits/option/DisabilityTypeCode"
                                Filter="CODES.RELATED_CODE_ID IN (DisabilityCatCode,0)" RMXType="code" Required="true" ValidationGroup="vgSave" />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                        <div runat="server" class="half" id="div_transtypecode" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_transtypecode" Text="Transaction Type" />
                          <span class="formw">
                            <uc:CodeLookUp runat="server" ID="transtypecode" CodeTable="TRANS_TYPES" ControlName="transtypecode" RMXRef="/Instance/Document/FundsBRSSplits/option/TransTypeCode" RMXType="code" Required="true" ValidationGroup="vgSave" TabIndex="70"/>
                          </span>
                        </div> 
                       <div runat="server" class="half" id="div_diagnosislist" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_diagnosislist" Text="ICD-9 Diagnosis Code(s)" />
                          <span class="formw">
                          <asp:Listbox runat="server" size="3" id="diagnosislist1" RMXRef="/Instance/Document/FundsBRSSplits/option/diagnosislist" RMXType="codedeslist" style="" Tabindex="80" />
                          <asp:button id="Button1" runat="server" class="CodeLookupControl" onclientclick="javascript: return callDiagSearch('diagnosislist1')" value="..." tabindex="85" />
                          <input type="button" class="BtnRemove" id="diagnosislist1btndel" onclick="deleteSelCode('diagnosislist1')" value="-" tabindex="90" />
                          <asp:Textbox runat="server" style="display:none" RMXRef="/Instance/Document/FundsBRSSplits/option/diagnosislist/@codeid" id="diagnosislist1_lst" />  
                          </span>
                        </div> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                         <div runat="server" class="half" id="div_modifierlist" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_modifierlist" Text="Modifier Code(s)" />
                          <span class="formw">
                            <uc:MultiCode runat="server" ID="modifierlist" CodeTable="MODIFIER_CODES" ControlName="modifierlist" RMXRef="/Instance/Document/FundsBRSSplits/option/modifierlist" RMXType="codelist" TabIndex="75" />
                          </span>
                         </div> 
                        <div runat="server" class="half" id="div_diagnosislisticd10" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_diagnosislisticd10" Text="ICD-10 Diagnosis Code(s)" />
                          <span class="formw">
                          <asp:Listbox runat="server" size="3" id="diagnosislisticd10" RMXRef="/Instance/Document/FundsBRSSplits/option/diagnosislisticd10" RMXType="codedeslist" style="" Tabindex="81" />
                          <asp:button id="Button2" runat="server" class="CodeLookupControl" onclientclick="javascript: return callDiagSearch('diagnosislisticd10')" value="..." tabindex="86" />
                          <input type="button" class="BtnRemove" id="diagnosislistICD10btndel" onclick="deleteSelCode('diagnosislisticd10')" value="-" tabindex="91" />
                          <asp:Textbox runat="server" style="display:none" RMXRef="/Instance/Document/FundsBRSSplits/option/diagnosislisticd10/@codeid" id="diagnosislisticd10_lst" />  
                          </span>
                        </div>
                   </td>
              </tr>
               <tr>
                    <td>    
                        <div runat="server" class="half" id="div_RevenueCode" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_RevenueCode" Text="Revenue Code" />
                          <span class="formw">
                            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="RevenueCode" MaxLength="4" RMXRef="/Instance/Document/FundsBRSSplits/option/RevenueCode" RMXType="numeric" onChange="ValidateNumericField(this)" TabIndex="95"/>
                          </span>
                        </div>    
                        <div runat="server" class="half" id="div_DenyLineItem" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_DenyLineItem" Text="Deny Line Item" />
                          <span class="formw">
                            <asp:CheckBox runat="server" ID="DenyLineItem" RMXRef="/Instance/Document/FundsBRSSplits/option/DenyLineItem" tabindex="180" onChange="setDataChanged(true);"/>
                          </span>
                        </div>                  
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_RefNo" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_RefNo" Text="Ref. No" />
                          <span class="formw">
                            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="RefNo" RMXRef="/Instance/Document/FundsBRSSplits/option/RefNo" RMXType="numeric" tabindex="100" maxlength="4" onChange="ValidateNumericField(this)" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_glaccountcode" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_glaccountcode" Text="GL Account" />
                          <span class="formw">
                            <asp:DropDownList runat="server" id="glaccountcode" tabindex="105" RMXRef="/Instance/Document/FundsBRSSplits/option/glaccountcode/@codeid" RMXType="combobox" ItemSetRef="/Instance/Document/FundsBRSSplits/option/glaccountcode" onchange="setDataChanged(true);" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" id="billingcodelabel" Text="(For Billing Code Search Enter Code and press button.) " />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_billingcode_cid" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_billingcode_cid" Text="CPT" />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="billingcode_cid" RMXRef="/Instance/Document/FundsBRSSplits/option/billingcode_cid" RMXType="text" TabIndex="110"/>
                          </span>
                        </div>                    
                        <div runat="server" class="half" id="div_billingcode" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_billingcode" Text="Billing Code" />
                          <span class="formw"><%-- aravi5 Fixes for Jira Id: 8128 Unable to create a BRS transaction in IE 11 getting message in Billing Code field as you must enter 30 or fewer character --%>
                            <asp:TextBox  MaxLength="100"  ID="billingcode" onchange="setDataChanged(true);billingcodeLostFocus(this);" runat="server" RMXRef="/Instance/Document/FundsBRSSplits/option/billingcode" TabIndex="115"/>
                            <asp:Button ID="billingcodebtn"  class="CodeLookupControl"  runat="server" OnClientClick="return BillingCode(this);" TabIndex="120"/>
                          </span>
                        </div>                   
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_phyPharmNDC_cid" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_phyPharmNDC_cid" Text="Physician Pharmacy NDC" />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="phyPharmNDC_cid" RMXRef="/Instance/Document/FundsBRSSplits/option/phyPharmNDC_cid" RMXType="text" TabIndex="125" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_phyPharmNDC_Des" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_phyPharmNDC_Des" Text="Description" />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="phyPharmNDC_Des" RMXRef="/Instance/Document/FundsBRSSplits/option/phyPharmNDC_Des" RMXType="text" TabIndex="130" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_percentile" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_percentile" Text="Percentile" />
                          <span class="formw">
                            <asp:DropDownList runat="server" id="percentile" tabindex="135" RMXRef="/Instance/Document/FundsBRSSplits/option/percentile/@codeid" RMXType="combobox" ItemSetRef="/Instance/Document/FundsBRSSplits/option/percentile" onchange="setDataChanged(true);" />
                          </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_prescripno" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_prescripno" Text="Prescription No." />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="prescripno" RMXRef="/Instance/Document/FundsBRSSplits/option/prescripno" RMXType="text" TabIndex="140"/>
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_prescripdate" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_prescripdate" Text="Prescription Date" />
                          <span class="formw">
                            <asp:TextBox runat="server" id="prescripdate" FormatAs="date" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);"  RMXRef="/Instance/Document/FundsBRSSplits/option/prescripdate" RMXType="date" tabindex="150" />
                            <%--<asp:button class="DateLookupControl" runat="server" id="prescripdatebtn" tabindex="155" />
                            <script type="text/javascript">
                                Zapatec.Calendar.setup(
					                {
					                    inputField: "prescripdate",
					                    ifFormat: "%m/%d/%Y",
					                    button: "prescripdatebtn"
					                }
					            );
				            </script>--%>
                              <script type="text/javascript">
                                  $(function () {
                                      $("#prescripdate").datepicker({
                                          showOn: "button",
                                          buttonImage: "../../Images/calendar.gif",
                                         // buttonImageOnly: true,
                                          showOtherMonths: true,
                                          selectOtherMonths: true,
                                          changeYear: true
                                      }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                                  });
                            </script>
                          </span>
                        </div>                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_prescripcert" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_prescripcert" Text="Prescription Certification" />
                          <span class="formw">
                            <uc:CodeLookUp runat="server" ID="prescripcert" CodeTable="FL_CERTIFICATION" ControlName="prescripcert" RMXRef="/Instance/Document/FundsBRSSplits/option/prescripcert" RMXType="code" TabIndex="160" />
                          </span>
                        </div>                    
                        <div runat="server" class="half" id="div_drugname" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_drugname" Text="Drug Name" />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="drugname" RMXRef="/Instance/Document/FundsBRSSplits/option/drugname" RMXType="text" TabIndex="165" />
                          </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_medquantity" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_medquantity" Text="Medication Quantity" />
                          <span class="formw">
                            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="medquantity" RMXRef="/Instance/Document/FundsBRSSplits/option/medquantity" RMXType="numeric" tabindex="170" fixed="0" min="1" onChange="setDataChanged(true);" />
                          </span>
                        </div>                   
                        <div runat="server" class="half" id="div_meddaysupply" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_meddaysupply" Text="Medication Days Supply" />
                          <span class="formw">
                            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="meddaysupply" RMXRef="/Instance/Document/FundsBRSSplits/option/meddaysupply" RMXType="numeric" tabindex="175" fixed="0" min="1" onChange="setDataChanged(true);" />
                          </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_supplychargeflag" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_supplychargeflag" Text="Supply Charge" />
                          <span class="formw">
                            <asp:CheckBox runat="server" ID="supplychargeflag" RMXRef="/Instance/Document/FundsBRSSplits/option/supplychargeflag" tabindex="180" onChange="setDataChanged(true);"/>
                          </span>
                        </div>                    
                        <div runat="server" class="half" id="div_usualcharge" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_usualcharge" Text="Pharmacy Usual Charge" />
                          <span class="formw">
                            <asp:TextBox runat="server" id="usualcharge" RMXRef="/Instance/Document/FundsBRSSplits/option/usualcharge" RMXType="currency" onblur="currencyLostFocus(this);" rmxforms:as="currency" tabindex="190" onchange="setDataChanged(true);" />
                          </span>
                        </div>
                    </td>
                </tr>      
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_prescripind" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_prescripind" Text="Prescription Indicator" />
                          <span class="formw">
                            <uc:CodeLookUp runat="server" ID="prescripind" CodeTable="FL_RX_IND" ControlName="prescripind" RMXRef="/Instance/Document/FundsBRSSplits/option/prescripind" RMXType="code" TabIndex="195" />
                          </span>
                        </div>                    
                        <div runat="server" class="half" id="div_purchasedind" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_purchasedind" Text="Purchased/Rental Indicator" />
                          <span class="formw">
                            <uc:CodeLookUp runat="server" ID="purchasedind" CodeTable="FL_PURCHASED_IND" ControlName="purchasedind" RMXRef="/Instance/Document/FundsBRSSplits/option/purchasedind" RMXType="code" TabIndex="200" />
                          </span>
                        </div>
                    </td>
                </tr>                
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_dispensed" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_dispensed" Text="Dispensed as Written" />
                          <span class="formw">
                            <uc:CodeLookUp runat="server" ID="dispensed" CodeTable="FL_DISPENSED" ControlName="dispensed" RMXRef="/Instance/Document/FundsBRSSplits/option/dispensed" RMXType="code" TabIndex="205" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_hcpcscode" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_hcpcscode" Text="HCPCS Level II Code" />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="hcpcscode" RMXRef="/Instance/Document/FundsBRSSplits/option/hcpcscode" RMXType="text" TabIndex="210"/>
                          </span>
                        </div>
                    </td>
                </tr>            
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_toothno" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_toothno" Text="Tooth No." />
                          <span class="formw">
                            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="toothno" RMXRef="/Instance/Document/FundsBRSSplits/option/toothno" RMXType="numeric" tabindex="220" onChange="setDataChanged(true);" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_surfacetext" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_surfacetext" Text="Surface Text" />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="surfacetext" RMXRef="/Instance/Document/FundsBRSSplits/option/surfacetext" RMXType="text" TabIndex="225"/>
                          </span>
                        </div>                   
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_unitsbillednum" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_unitsbillednum" Text="Units Billed" />
                          <span class="formw">
                            <asp:TextBox runat="server" onblur="numLostFocus(this);" id="unitsbillednum" RMXRef="/Instance/Document/FundsBRSSplits/option/unitsbillednum" RMXType="numeric" tabindex="230" fixed="0" min="1" onChange="setDataChanged(true);" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_unitsbilledctlgrp" xmlns="">
                          <asp:label runat="server" class="label" id="unitsbilledlabel" Text="Billed Units:" />
                          <span class="formw">
                            <asp:RadioButton runat="server" GroupName="units" id="unitsbilled" RMXRef="/Instance/Document/FundsBRSSplits/option/unitsbilledtype" RMXType="radio" onclientclick="setDataChanged(true);" Text="Units" value="1" TabIndex="235"/>
                            <asp:RadioButton runat="server" GroupName="units" id="unitsbilledminutes" RMXRef="/Instance/Document/FundsBRSSplits/option/unitsbilledtype" RMXType="radio" onclientclick="setDataChanged(true);" Text="Minutes" value="2" TabIndex="240"/>
                            <asp:RadioButton runat="server" GroupName="units" id="unitsbilledother" RMXRef="/Instance/Document/FundsBRSSplits/option/unitsbilledtype" RMXType="radio" onclientclick="setDataChanged(true);" Text="Other" value="3" TabIndex="245" />
                         <%-- MITS 23815:  Not required--%>
                        <%--    <asp:TextBox style="display:none" runat="server" id="unitsbilledtype" RMXRef="/Instance/Document/FundsBRSSplits/option/unitsbilledtype" RMXType="hidden" />--%>
                          </span>    
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_invoicedby" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_invoicedby" Text="Invoiced By" />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="invoicedby" RMXRef="/Instance/Document/FundsBRSSplits/option/invoicedby" RMXType="text" TabIndex="250" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_invoicenumber" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_invoicenumber" Text="Invoice Number" />
                          <span class="formw">
                            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="invoicenumber" RMXRef="/Instance/Document/FundsBRSSplits/option/invoicenumber" RMXType="text" TabIndex="255" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_invoicedate" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_invoicedate" Text="Invoice Date" />
                          <span class="formw">
                            <asp:TextBox runat="server" id="invoicedate" RMXRef="/Instance/Document/FundsBRSSplits/option/invoicedate" RMXType="date" tabindex="260" onblur="dateLostFocus(this.id);"/> <!--Added onblur event for Mits 18466-->
                            <%--<asp:button class="DateLookupControl" runat="server" id="invoicedatebtn" TabIndex="265"/>
                                <script type="text/javascript">
                                    Zapatec.Calendar.setup(
                                        {
                                            inputField: "invoicedate",
                                            ifFormat: "%m/%d/%Y",
                                            button: "invoicedatebtn"
                                        }
                                    );
                                </script>--%>
                              <script type="text/javascript">
                                  $(function () {
                                      $("#invoicedate").datepicker({
                                          showOn: "button",
                                          buttonImage: "../../Images/calendar.gif",
                                          //buttonImageOnly: true,
                                          showOtherMonths: true,
                                          selectOtherMonths: true,
                                          changeYear: true
                                      }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                                  });
                            </script>
                          </span>
                        </div>                   
                    </td>
                </tr>
            </table>
        </div>
        <div class="singletopborder" runat="server" name="FORMTABcontract" id="FORMTABcontract" style="display:none; position:inherit;">
            <table width="98%" border="0" cellspacing="0" celpadding="0">
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_providername" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_providername" Text="Provider" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/providername" id="providername" tabindex="300" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_contr_startdate" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_startdate" Text="Contract Start Date" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_startdate" id="contr_startdate" tabindex="315" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_contr_enddate" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_enddate" Text="Contract End Date" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_enddate" id="contr_enddate" tabindex="320" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_contr_comments" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_comments" Text="Contract Comments" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_comments" id="contr_comments" tabindex="325" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>                   
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_contr_amount" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_amount" Text="Contract Amount" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_amount" id="contr_amount" tabindex="330" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_contr_firstrate" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_firstrate" Text="First Rate" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_firstrate" id="contr_firstrate" tabindex="335" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_contr_firstratechanged" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_firstratechanged" Text="Day 2nd Rate Starts" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_firstratechanged" id="contr_firstratechanged" tabindex="340" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_contr_secondrate" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_secondrate" Text="Second Rate" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_secondrate" id="contr_secondrate" tabindex="345" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_contr_secondratechanged" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_secondratechanged" Text="Day Last Rate Starts" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_secondratechanged" id="contr_secondratechanged" tabindex="350" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_contr_lastrate" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_lastrate" Text="Last Rate" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_lastrate" id="contr_lastrate" tabindex="355" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_contr_dpercentage" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_dpercentage" Text="1st Sched Discount" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_dpercentage" id="contr_dpercentage" tabindex="360" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_contr_dpercentage2" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_dpercentage2" Text="2nd Sched Discount" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_dpercentage2" id="contr_dpercentage2" tabindex="365" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_contr_dpercentagebill" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_contr_dpercentagebill" Text="Bill/Contract Discount" />
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/contr_dpercentagebill" id="contr_dpercentagebill" tabindex="370" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="singletopborder" runat="server" name="FORMTABoverridegroup" id="FORMTABoverridegroup" style="display:none; position:inherit; ">
            <table width="98%" border="0" cellspacing="0" celpadding="0">
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_override1ba" xmlns="">
                          <span class="formw">
                            <li />
                            <asp:RadioButton runat="server" GroupName="override" id="override1ba" RMXRef="/Instance/Document/FundsBRSSplits/option/override" RMXType="radio" Text="Billed Amount" checked="" value="0" TabIndex="400"/>
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_ovr_billedamount" xmlns="">
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/ovr_billedamount" id="ovr_billedamount" tabindex="410" style="background-color: silver;" readonly="true" />
                          </span>
                        </div>                   
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_override1bad" xmlns="">
                          <span class="formw">
                            <li />
                            <asp:RadioButton runat="server" GroupName="override" id="override1bad" RMXRef="/Instance/Document/FundsBRSSplits/option/override" RMXType="radio" Text="Billed Amount with Discount" value="1" TabIndex="415"/>
                          </span>
                        </div>                    
                        <div runat="server" class="half" id="div_ovr_discbilledamount" xmlns="">
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/ovr_discbilledamount" id="ovr_discbilledamount" style="background-color: silver;" readonly="true" TabIndex="420"/>
                          </span>
                        </div>                   
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_override1ca" xmlns="">
                          <span class="formw">
                            <li />
                            <asp:RadioButton runat="server" GroupName="override" id="override1ca" RMXRef="/Instance/Document/FundsBRSSplits/option/override" RMXType="radio" Text="Contract Amount" value="2" TabIndex="425"/>
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_ovr_contractamount" xmlns="">
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/ovr_contractamount" id="ovr_contractamount" style="background-color: silver;" readonly="true" TabIndex="430"/>
                          </span>
                        </div>       
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_override1cad" xmlns="">
                          <span class="formw">
                            <li />
                            <asp:RadioButton runat="server" GroupName="override" id="override1cad" RMXRef="/Instance/Document/FundsBRSSplits/option/override" RMXType="radio" Text="Contract Amount with Discount" value="3" TabIndex="435" />
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_ovr_disccontractamount" xmlns="">
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/ovr_disccontractamount" id="ovr_disccontractamount" style="background-color: silver;" readonly="true" TabIndex="440" />
                          </span>
                        </div>     
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_override1fs" xmlns="">
                          <span class="formw">
                            <li />
                            <asp:RadioButton runat="server" GroupName="override" id="override1fs" RMXRef="/Instance/Document/FundsBRSSplits/option/override" RMXType="radio" Text="Fee Schedule" value="6"  TabIndex="445"/>
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_ovr_schedamount" xmlns="">
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/ovr_schedamount" id="ovr_schedamount" style="background-color: silver;" readonly="true"  TabIndex="450"/>
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox style="display:none" runat="server" id="ovr_schedamountalt" RMXRef="/Instance/Document/FundsBRSSplits/option/ovr_schedamountalt" RMXType="id" />
                        <div runat="server" class="half" id="div_override1fsd" xmlns="">
                          <span class="formw">
                            <li />
                            <asp:RadioButton runat="server" GroupName="override" id="override1fsd" RMXRef="/Instance/Document/FundsBRSSplits/option/override" RMXType="radio" Text="Fee Schedule with Discount" value="7" TabIndex="455"/>
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_ovr_discschedamount" xmlns="">
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/ovr_discschedamount" id="ovr_discschedamount" style="background-color: silver;" readonly="true" TabIndex="460"/>
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_override1apd" xmlns="">
                          <span class="formw">
                            <li />
                            <asp:RadioButton runat="server" GroupName="override" id="override1apd" RMXRef="/Instance/Document/FundsBRSSplits/option/override" RMXType="radio" Text="Apply Per Diem" value="4" TabIndex="465"/>
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_ovr_perdiemamount" xmlns="">
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/ovr_perdiemamount" id="ovr_perdiemamount" style="background-color: silver;" readonly="true" TabIndex="470"/>
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                       <div runat="server" class="half" id="div_override1apdsl" xmlns="">
                          <span class="formw">
                            <li />
                            <asp:RadioButton runat="server" GroupName="override" id="override1apdsl" RMXRef="/Instance/Document/FundsBRSSplits/option/override" RMXType="radio" Text="Apply Per Diem with Stop Loss" value="5" TabIndex="475"/>
                          </span>
                        </div>
                        <div runat="server" class="half" id="div_ovr_perdiemstoplossamount" xmlns="">
                          <span class="formw">
                            <asp:TextBox runat="Server" RMXRef="/Instance/Document/FundsBRSSplits/option/ovr_perdiemstoplossamount" id="ovr_perdiemstoplossamount" style="background-color: silver;" readonly="true" TabIndex="480"/>
                          </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="half" id="div_override" style="display:none;" xmlns="">
                          <span class="formw">
                            <asp:TextBox style="display:none" runat="server" id="override" RMXRef="/Instance/Document/FundsBRSSplits/option/override" RMXType="hidden" />
                          </span>
                        </div> 
                    </td>
                </tr>
            </table>
        </div>
        <div class="singletopborder" runat="server" name="FORMTABCalculate" id="FORMTABCalculate" style="display:none; position:inherit;">
            <table width="98%" border="0" cellspacing="0" celpadding="0">
                <tr>
                    <td>
                        <div runat="server" class="row" id="div_amountbilled" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_amountbilled" Text="Amount Billed" /><span class="formw">
                            <asp:TextBox runat="server" id="amountbilled" RMXRef="/Instance/Document/FundsBRSSplits/option/amountbilled" RMXType="currency" onblur="currencyLostFocus(this);" rmxforms:as="currency" tabindex="500" onchange="setDataChanged(true);" />
                            <asp:Button runat="server" ID="btnCalculate" class="button" Text="Calculate Amount Allowed" OnClientClick="return Calculate();" OnClick ="btnCalculate_Click" TabIndex="505" />
                          </span>
                        </div>            
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="row" id="div_amountallowed" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_amountallowed" Text="Amount Allowed" />
                          <span class="formw">
                            <asp:TextBox runat="server" id="amountallowed" RMXRef="/Instance/Document/FundsBRSSplits/option/amountallowed" RMXType="currency" onblur="currencyLostFocus(this);" rmxforms:as="currency" tabindex="510" onchange="setDataChanged(true);" readonly="true" style="background-color: silver;" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="row" id="div_amountsaved" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_amountsaved" Text="Amount Saved" />
                          <span class="formw">
                            <asp:TextBox runat="server" id="amountsaved" RMXRef="/Instance/Document/FundsBRSSplits/option/amountsaved" RMXType="currency" onblur="currencyLostFocus(this);" rmxforms:as="currency" tabindex="515" onchange="setDataChanged(true);" readonly="true" style="background-color: silver;" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="row" id="div_amountreduced" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_amountreduced" Text="Reduce Amount to Pay By Fixed Amount" />
                          <span class="formw">
                            <asp:TextBox runat="server" id="amountreduced" RMXRef="/Instance/Document/FundsBRSSplits/option/amountreduced" RMXType="currency" onblur="currencyLostFocus(this);" rmxforms:as="currency" tabindex="525" onchange="setDataChanged(true);" />
                            <asp:Button runat="server" class="button" Text="Reduce" OnClientClick="return Reduce(false);" ID="btnamountreduced" tabindex="365"/>
                          </span>
                        </div>                     
                    </td>
                </tr>
				<tr>
                    <td>
                        <div runat="server" class="row" id="div_amountreducedpercent" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_amountreducedpercent" Text="Reduce Amount to Pay By Percent" />
                          <span class="formw">
                            <asp:TextBox runat="server" id="amountreducedpercent" onblur ="numLostFocus(this)" RMXRef="/Instance/Document/FundsBRSSplits/option/amountreducedpercent"  tabindex="520" onchange="setDataChanged(true);" />
                            <asp:Button runat="server" class="button" Text="Reduce" OnClientClick="return Reduce(true);" ID="btnamountreducedpercent" TabIndex="358"/>
                          </span>
                        </div>                    
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <div runat="server" class="row" id="div_amounttopay" xmlns="">
                          <asp:label runat="server" class="required" id="lbl_amounttopay" Text="Amount To Pay" />
                          <span class="formw">
                            <asp:TextBox runat="server" id="amounttopay" RMXRef="/Instance/Document/FundsBRSSplits/option/amounttopay|/Instance/Document/FundsBRSSplits/option/Amount" RMXType="currency" onblur="currencyLostFocus(this);" rmxforms:as="currency" tabindex="530" onchange="setDataChanged(true);" />
                          </span>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="row" id="div_eoblist" xmlns="">
                          <asp:label runat="server" class="label" id="lbl_eoblist" Text="Explanation(s) of Benefits" />
                          <span class="formw">
                            <uc:MultiCode runat="server" ID="eoblist" CodeTable="EOB_CODES" ControlName="eoblist" RMXRef="/Instance/Document/FundsBRSSplits/option/eoblist" RMXType="codelist" TabIndex="535"/>
                          </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="row" id="div_folluplabel" xmlns="">
                          <asp:label runat="server" class="label" id="folluplabel" RMXRef="/Instance/Document/FundsBRSSplits/option/folluplabel" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <asp:Button ID="btnReturn" runat="server" Text="Return" class="button" OnClientClick="return OnReturnClicked();" OnClick="btnReturn_Click" TabIndex="540"/>
        <asp:Button ID="btnContinue" runat="server" Text="Continue" Visible="false" class="button" onclick="btnOverrideContinue_Click" TabIndex="545"/>
        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="button" OnClientClick="return OnCancel();" TabIndex="550"/>
        <!--  // mkaran2 - Start - MITS #28419  -->
        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnShowSummary" runat="server" Visible="false" Text="Summary" class="button" OnClientClick="return showBRSSummary();" TabIndex="550"/>
        <!--  // mkaran2 - End - MITS #28419  -->
        <asp:TextBox style="display:none" runat="server" id="sched_amt" RMXRef="/Instance/Document/FundsBRSSplits/option/sched_amt" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="brsmode" RMXRef="/Instance/Document/FundsBRSSplits/option/mode" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="resubmitmode" RMXRef="/Instance/Document/FundsBRSSplits/option/resubmitmode" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="step" RMXRef="/Instance/Document/FundsBRSSplits/option/step" RMXType="id" Text="2" />
        <asp:TextBox style="display:none" runat="server" id="substep" RMXRef="/Instance/Document/FundsBRSSplits/option/substep" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="idx" RMXRef="/Instance/Document/FundsBRSSplits/option/idx" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="cmd" Text="unlockcalc" RMXRef="/Instance/Document/FundsBRSSplits/option/cmd" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="transid" RMXRef="/Instance/Document/FundsBRSSplits/option/transid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="splitid" RMXRef="/Instance/Document/FundsBRSSplits/option/splitid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="/Instance/Document/FundsBRSSplits/option/claimid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="providereid" RMXRef="/Instance/Document/FundsBRSSplits/option/providereid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="calctype" RMXRef="/Instance/Document/FundsBRSSplits/option/calctype" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="feesched" RMXRef="/Instance/Document/FundsBRSSplits/option/feesched" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="feesched2" RMXRef="/Instance/Document/FundsBRSSplits/option/feesched2" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="stoplossflag" RMXRef="/Instance/Document/FundsBRSSplits/option/stoplossflag" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="feeschedused" RMXRef="/Instance/Document/FundsBRSSplits/option/feeschedused" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="contractexists" RMXRef="/Instance/Document/FundsBRSSplits/option/contractexists" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="studyid" RMXRef="/Instance/Document/FundsBRSSplits/option/studyid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="revision" RMXRef="/Instance/Document/FundsBRSSplits/option/revision" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="statecode" RMXRef="/Instance/Document/FundsBRSSplits/option/statecode" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="crc" RMXRef="/Instance/Document/FundsBRSSplits/option/crc" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="lineofbusinesscode" RMXRef="/Instance/Document/FundsBRSSplits/option/lineofbusinesscode" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="CreateNewSession" RMXRef="/Instance/Document/FundsBRSSplits/option/CreateNewSession" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="implements_split" RMXRef="/Instance/Document/FundsBRSSplits/option/implements_split" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="invoiceamount" RMXRef="/Instance/Document/FundsBRSSplits/option/invoiceamount" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="ponumber" RMXRef="/Instance/Document/FundsBRSSplits/option/ponumber" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="reservetypecode" RMXRef="/Instance/Document/FundsBRSSplits/option/ReserveTypeCode/@codeid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="reservetypecode_desc" RMXRef="/Instance/Document/FundsBRSSplits/option/ReserveTypeCode" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="missing" RMXRef="/Instance/Document/FundsBRSSplits/option/missing" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="param" RMXRef="/Instance/Document/FundsBRSSplits/option/param" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="CmboSpecCdsAmount" RMXRef="/Instance/Document/FundsBRSSplits/option/CmboSpecCdsAmount" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" ID="CmboSpecCds" RMXRef="/Instance/Document/FundsBRSSplits/option/CmboSpecCds" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="Specialty" RMXRef="/Instance/Document/FundsBRSSplits/option/Specialty" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="overrideoption" RMXRef="/Instance/Document/FundsBRSSplits/option/overrideoption" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="overrideamount" RMXRef="/Instance/Document/FundsBRSSplits/option/overrideamount" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="iccpt" RMXRef="/Instance/Document/FundsBRSSplits/option/iccpt" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="specialtyshortcode" RMXRef="/Instance/Document/FundsBRSSplits/option/specialtyshortcode" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="procedurecase" RMXRef="/Instance/Document/FundsBRSSplits/option/procedurecase" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="NextBillItem" RMXRef="/Instance/Document/FundsBRSSplits/option/NextBillItem" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="CurrentBillItemIndex" RMXRef="/Instance/Document/FundsBRSSplits/option/CurrentBillItemIndex" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="TotalBillItem" RMXRef="/Instance/Document/FundsBRSSplits/option/TotalBillItem" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="AddCurrentBillItem" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="AddItemInProgress" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="/Instance/Document/FundsBRSSplits/option/SysFormName" RMXType="id" Text="billreviewsystem" />
        <asp:TextBox style="display:none" runat="server" id="splitrowid" RMXRef="/Instance/Document/FundsBRSSplits/option/splitrowid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" ID="formname" Text="frmData" />
        <asp:TextBox style="display:none" runat="server" ID="SysRequired" Text="FLLicense|transtypecode_codelookup_cid|billingcode|drugname|toothno|surfacetext|unitsbillednum|amountbilled|amounttopay|prescripind|dispensed|prescripind_codelookup_cid|dispensed_codelookup_cid|" />
        <asp:TextBox style="display:none" runat="server" ID="SysFocusFields" Text="dfromdate|placeofservice|billingcode|billingcode_cid|phyPharmNDC_cid|percentile|prescripno|toothno|unitsbillednum|invoicedby|override1|amountbilled|" />
        <asp:TextBox style="display:none" runat="server" ID="hdSaveButtonClicked" />
        <asp:TextBox style="display:none" runat="server" id="SysInvisible" />
        <asp:TextBox style="display:none" runat="server" id="SysLookupClass" />
        <asp:TextBox style="display:none" runat="server" id="SysLookupRecordId" />
        <asp:TextBox style="display:none" runat="server" id="SysLookupAttachNodePath" />
        <asp:TextBox style="display:none" runat="server" id="SysLookupResultConfig" />
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading ..." />
        <asp:TextBox style="display:none" runat="server" id="SysFormIdName" Text="splitrowid" />
        <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" />
        <asp:TextBox style="display:none" runat="server" ID="SysCmd" />
        <asp:TextBox style="display:none" runat="server" ID="SysCmdConfirmSave" />
        <asp:TextBox style="display:none" runat="server" ID="SysCmdQueue" />
        <asp:TextBox style="display:none" runat="server" ID="SysClassName" />
        <asp:TextBox style="display:none" runat="server" ID="SysKilledNodes" />
        <asp:TextBox style="display:none" runat="server" id="txtData" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="ReturnButtonClicked" RMXType="id" Text="false" />
        <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="/Instance/Document/FundsBRSSplits/@mode" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXType="id" RMXRef="/Instance/Document/FundsBRSSplits/@selectedrowposition"/>
        <asp:TextBox style="display:none" runat="server" id="lastrowposition" RMXType="id" Text="0" RMXRef="/Instance/Document/FundsBRSSplits/@lastrowposition"/>
        <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="/Instance/Document/FundsBRSSplits/@gridname" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="SysPageDataChanged" RMXRef="/Instance/Document/FundsBRSSplits/@datachanged"/>
        <asp:TextBox style="display:none" runat="server" ID="PostbackAction" Text=""></asp:TextBox>
        <asp:TextBox style="display:none" runat="server" ID="calculated" Text="" RMXRef="/Instance/Document/FundsBRSSplits/option/calculated"></asp:TextBox>
        <asp:TextBox style="display:none" runat="server" id="TabNameList" RMXRef="" RMXType="hidden" Text="TABSDate|TABSBillItem|TABScontract|TABSoverridegroup|TABSCalculate" />
        <asp:TextBox style="display:none" runat="server" id="BrsDataForDuplicateChk" />
 <!--  // mkaran2 - Start - MITS #28419  -->
        <asp:HiddenField runat="server" id="hdnBRSSummary" />
          <!--  // mkaran2 - End - MITS #28419  -->
    </form>
</body>
</html>