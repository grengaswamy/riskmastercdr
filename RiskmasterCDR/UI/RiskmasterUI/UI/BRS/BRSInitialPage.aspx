﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSInitialPage.aspx.cs" Inherits="Riskmaster.UI.BRS.BRSInitialPage" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Bill Review System</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" src="../../Scripts/brs.js"></script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js"></script>
</head>
<body onload="brsInitialPageLoad()">
    <form id="frmData" runat="server">
    <div>
        <asp:TextBox style="display:none" runat="server" id="txtData" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="gridmode" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="brsmode" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="positionforlastrow" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="UniqueId" RMXType="id" Text="SplitRowId" />
        <asp:TextBox style="display:none" runat="server" id="gridname" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="claimid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="zip" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="transid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="providereid" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="lineofbusinesscode" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="resubmitflag" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="lastrowposition" RMXType="id" Text="0"/>
        <asp:TextBox style="display:none" runat="server" ID="step" Text=""></asp:TextBox>
        <%--Mona: R8.2 : BRS in Carrier claim--%>
        <asp:TextBox Style="display: none" runat="server" ID="PolicyID" RMXType="id" />
        <asp:TextBox Style="display: none" runat="server" ID="RcRowID" RMXType="id" />
        <asp:TextBox Style="display: none" runat="server" ID="PolCvgID" RMXType="id" />
        <asp:TextBox Style="display: none" runat="server" ID="IsFirstFinalQueryString" RMXType="id" />
         <asp:TextBox Style="display: none" runat="server" ID="clm_entityid" RMXType="id" />
         <asp:TextBox Style="display: none" runat="server" ID="PaymentOrCollection" RMXType="id" />
        
        <%--Mona: R8.2 : BRS in Carrier claim--%>
         <asp:TextBox Style="display: none" runat="server" ID="CvgLossID" RMXType="id" />
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading ..." />
    </div>
    </form>
</body>
</html>
