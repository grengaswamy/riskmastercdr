﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.BRS
{
    /// <summary>
    /// Enum containing eSteps.
    /// </summary>
    public enum eSteps : int
    {
        Preload = -1,
        Dates = 0,
        FeeSchedule = 1,
        LoadBRSXML = 2,
        Calculate = 3,
        Specialty = 4,
        ProcedureCase = 5,
        ICCPT = 6,
        Override = 7,
        ManualReduction = 8,
        AddNextBillItem = 9,
        PreviousBillItem = 10,
    };

    /// <summary>
    /// Enum for override radio button values
    /// </summary>
    public enum OverrideOptions : int
    {
        override1ba = 0,
        override1bad = 1,
        override1ca = 2,
        override1cad = 3,
        override1apd = 4,
        override1apdsl = 5,
        override1fs = 6,
        override1fsd = 7,
    };
    // mkaran2 - Start - MITS #28419
    public struct BRSSummary
    {
        public string DateOfService { get; set; }     
        public string ProcedureCode { get; set; }
        public string BilledAmt { get; set; }
        public string PaidAmt { get; set; }
        public string SavedAmt { get; set; }         

        public BRSSummary(string DateOfService, string ProcedureCode, string BilledAmt, string PaidAmt, string SavedAmt) : this()
        {
            this.DateOfService = DateOfService;
            this.ProcedureCode = ProcedureCode;
            this.BilledAmt = BilledAmt; 
            this.PaidAmt=PaidAmt;
            this.SavedAmt = SavedAmt;           
        } 
    }
    // mkaran2 - End - MITS #28419
    public class BRSCommon
    {
        private const char m_FieldDelimiter = '|';

        private static int m_iFirstRowPosition = 2;

        /// <summary>
        /// set idx value for each brs item to the same number of row position in the gird view. It
        /// will start from 2 and the last one will be -1 (blank one)
        /// </summary>
        /// <param name="oBRSSplits"></param>
        /// <returns></returns>
        public static XElement SetSplitItemIndex(XElement oBRSSplits)
        {
            IEnumerable<XElement> oSplitList = oBRSSplits.Elements("option");
            int iStartRowPosition = 2;
            XElement oIdx = null;
            for (int i = 0; i < oSplitList.Count() - 1; i++)
            {
                oIdx = (oSplitList.ElementAt(i) as XElement).XPathSelectElement("idx");
                oIdx.Value = (i + iStartRowPosition).ToString();
            }
            oIdx = (oSplitList.ElementAt(oSplitList.Count()-1) as XElement).XPathSelectElement("idx");
            oIdx.Value = "-1";

            return oBRSSplits;
        }

        /// <summary>
        /// If it's not the 1st time for creating a new split, try to copy date/fee schedule info from
        /// the most recent one.
        /// </summary>
        /// <param name="oBRSSplits"></param>
        /// <param name="bNewGroupd">For the 1st item, it should not copy the values</param>
        /// <returns></returns>
        public static string GetNewSplitTemplate(XElement oBRSSplits, bool bNewGroupd)
        {
            XElement oNewSplit = null;
            IEnumerable<XElement> oSplitList = oBRSSplits.Elements("option");
            int iCount = oSplitList.Count();
            XElement oBlankSplit = oSplitList.ElementAt(iCount - 1) as XElement;
            string sNewSplit = oBlankSplit.ToString();
            oNewSplit = XElement.Parse(sNewSplit);
            if (iCount > 1 && !bNewGroupd)
            {
                //Copy KeepOnNext field values
                XElement oExistingSplit = oSplitList.ElementAt(iCount-2) as XElement;

                XElement oKeepOnNext = oNewSplit.XPathSelectElement("//keeponnext");
                string sKeepOnNext = string.Empty;
                if (oKeepOnNext != null)
                {
                    sKeepOnNext = oKeepOnNext.Value;
                }
                string[] arrKeepOnNext = sKeepOnNext.Split(m_FieldDelimiter);
                foreach (string sFieldName in arrKeepOnNext)
                {
                    if (string.IsNullOrEmpty(sFieldName))
                        continue;

                    XElement oTargetField = oNewSplit.XPathSelectElement(sFieldName);
                    XElement oSourceField = oExistingSplit.XPathSelectElement(sFieldName);
                    oTargetField.ReplaceWith(oSourceField);
                }

                sNewSplit = oNewSplit.ToString();
            }

            return sNewSplit;
        }


        /// <summary>
        /// Add a new item to the list based on mode
        /// </summary>
        /// <param name="sMode">add -- add new item, clone -- clone an existing item</param>
        /// <param name="oBRSMessage"></param>
        /// <param name="bNewGroup">If it's the 1st item after clicking new button the in the funds page</param>
        /// <returns></returns>
        public static void AddNewBRSItem(string sMode, XElement oBRSMessage, bool bNewGroup)
        {
            XElement oBRSSplit = oBRSMessage.XPathSelectElement("//FundsBRSSplits");
            XElement oEmptyOption = oBRSSplit.XPathSelectElement("option[idx='-1']");
            string sSelectedRowPosition = oBRSSplit.Attribute("selectedrowposition").Value;
            string sBRSMode = oBRSSplit.Attribute("brsmode").Value;
            string sResubmitFlag = oBRSSplit.Attribute("resubmitmode").Value;
            int iItemCount = oBRSSplit.Elements("option").Count();

            if (sMode == "add" || sMode == "clone")
            {
                XElement oNewOption = null;
                XElement oEmplyOptionCopy = XElement.Parse(oEmptyOption.ToString());
                switch (sMode)
                {
                    case "add":
                        oNewOption = XElement.Parse(BRSCommon.GetNewSplitTemplate(oBRSSplit, bNewGroup));
                        break;
                    case "clone":
                        XElement oClonedOption = oBRSSplit.XPathSelectElement("option[idx='" + sSelectedRowPosition + "']");
                        oNewOption = XElement.Parse(oClonedOption.ToString());
                        break;
                }
                XElement oSplitRowId = oNewOption.XPathSelectElement("SplitRowId");
                oSplitRowId.Value = "0";

                iItemCount++;

                XElement oIdx = oNewOption.XPathSelectElement("//idx");
                oIdx.Value = iItemCount.ToString();
                XElement oBRSOptionMode = oNewOption.XPathSelectElement("mode");
                oBRSOptionMode.Value = sBRSMode;
                XElement oResbumitOptionMode = oNewOption.XPathSelectElement("resubmitmode");
                oResbumitOptionMode.Value = sResubmitFlag;

                oEmptyOption.Remove();
                oBRSSplit.Add(oNewOption);
                oBRSSplit.Add(oEmplyOptionCopy);

                oBRSSplit.SetAttributeValue("selectedrowposition", iItemCount.ToString());
            }
            oBRSSplit.SetAttributeValue("lastrowposition", iItemCount.ToString());
        }


        /// <summary>
        /// Get the current BRS item for databinding and service call
        /// </summary>
        /// <param name="sBRSMessage"></param>
        /// <returns></returns>
        public static string GetCurrentItemMessage(string sBRSMessage)
        {
            XElement oCurrentBRSMessage = XElement.Parse(sBRSMessage);
            XElement oBRSSplit = oCurrentBRSMessage.XPathSelectElement("Document/FundsBRSSplits");
            
            string sSelectedRowPosition = oBRSSplit.Attribute("selectedrowposition").Value;
            string sCurrentItem = oBRSSplit.XPathSelectElement("option[idx='" + sSelectedRowPosition + "']").ToString();
            oBRSSplit.RemoveNodes();
            oBRSSplit.Add(XElement.Parse(sCurrentItem));

            return oCurrentBRSMessage.ToString();
        }

        /// <summary>
        /// put the current brs item back to the list
        /// </summary>
        /// <param name="sBRSMessage">current brs item</param>
        /// <param name="sCurrentBRSMessage">brs item list</param>
        /// <returns></returns>
        public static string PutCurrentItemMessage(string sCurrentBRSMessage, string sBRSMessage)
        {
            XElement oBRSMessage = XElement.Parse(sBRSMessage);
            XElement oCurrentBRSMessage = XElement.Parse(sCurrentBRSMessage);
            string sSelectedRowPosition = oCurrentBRSMessage.XPathSelectElement("Document/FundsBRSSplits").Attribute("selectedrowposition").Value;

            XElement oExistingItem = oBRSMessage.XPathSelectElement("Document/FundsBRSSplits/option[idx='" + sSelectedRowPosition + "']");
            XElement oNewItem = oCurrentBRSMessage.XPathSelectElement("Document/FundsBRSSplits/option[idx='" + sSelectedRowPosition + "']");
            oExistingItem.ReplaceNodes(oNewItem.Elements());

            return oBRSMessage.ToString();
        }

        /// <summary>
        /// Remove the current item from the list
        /// </summary>
        /// <param name="oBRSMessage"></param>
        /// <returns></returns>
        public static void RemoveCurrentBRSItem(ref XElement oBRSMessage)
        {
            XElement oBRSSplit = oBRSMessage.XPathSelectElement("//FundsBRSSplits");
            string sLastRowPosition = string.Empty;
            int iLastRowPosition = 0;
            if (oBRSSplit.Attribute("lastrowposition") != null)
            {
                sLastRowPosition = oBRSSplit.Attribute("lastrowposition").Value;
                iLastRowPosition = int.Parse(sLastRowPosition) - 1;
                oBRSSplit.Attribute("lastrowposition").Value = iLastRowPosition.ToString();
            }
            XElement oRemovedOption = oBRSSplit.XPathSelectElement("option[idx='" + sLastRowPosition + "']");
            if (oRemovedOption != null)
            {
                oRemovedOption.Remove();
            }
        }

        /// <summary>
        /// format the amount to currency format
        /// </summary>
        /// <param name="oBRSMessage"></param>
        /// <returns></returns>
        public static XElement FormatAmountCurrency(XElement oBRSMessage)
        {
            IEnumerable<XElement> oAmountElements = oBRSMessage.XPathSelectElements("//FundsBRSSplits/option/Amount");
            foreach (XElement oAmount in oAmountElements)
            {
                string sAmount = oAmount.Value;
                if (sAmount.StartsWith("$"))
                    break;

                if (string.IsNullOrEmpty(sAmount))
                    sAmount = "0.00";
                decimal dAmount = decimal.Parse(sAmount);
                oAmount.Value = string.Format("{0:C}", dAmount);
            }

            return oBRSMessage;
        }
    }
}
