﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSSummaryList.aspx.cs" Inherits="Riskmaster.UI.UI.BRS.BRSSummaryList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"  TagPrefix="uc" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BRS Summary List</title>
    <link rel="stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="javaScript" src="../../Scripts/brs.js" type="text/javascript"></script>
    <script language="javaScript" src="../../Scripts/WaitDialog.js" type="text/javascript"></script>    
   
</head>
<body onload="LoadedBrsSummary();">
<!-- // mkaran2 - BRSsummary Page - MITS #28419-->
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID = "ErrorControl1" runat="server" />
    <div>
    <asp:GridView ID="gvBRSSummaryList" runat="server" AutoGenerateColumns="false" AllowPaging="false" Width="100%" ShowHeader="true"  ShowFooter="true" GridLines="both"  
                  EmptyDataText="There is no BRS Summary to display." CellPadding="8"  OnRowDataBound="gvBRSSummaryList_RowDataBound">
            <AlternatingRowStyle CssClass="datatd" />
          <RowStyle CssClass="datatd1" />
         <HeaderStyle CssClass="msgheader" ForeColor="White"/>
             <Columns>  
                <asp:TemplateField  HeaderText="Line No" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Left">   
                    <ItemTemplate> 
                        <%#Container.DataItemIndex+1 %>    
                    </ItemTemplate>  
                    <FooterTemplate>
                        <asp:Label ID="lblTotal" runat="server" Text="Total" />
                    </FooterTemplate>    
                </asp:TemplateField> 
                <asp:TemplateField  HeaderText="Date Of Service" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Left" SortExpression="DateOfService">
                    <ItemTemplate>
                        <asp:Label ID="lblDateOfService" runat="server" Text='<%#Eval("DateOfService") %>'/>
                    </ItemTemplate>                    
                </asp:TemplateField>              
                <asp:TemplateField  HeaderText="Procedure Code" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Left" SortExpression="ProcedureCode">
                    <ItemTemplate>
                        <asp:Label ID="lblProcedureCode" runat="server" Text='<%#Eval("ProcedureCode") %>'/>
                    </ItemTemplate>                    
                </asp:TemplateField>               
                <asp:TemplateField  HeaderText="$ Billed" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Left" SortExpression="BilledAmt">
                    <ItemTemplate>
                        <asp:Label ID="lblBilledAmt" runat="server" Text='<%#Eval("BilledAmt") %>'/>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalBilledAmt" runat="server" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField  HeaderText="$ Paid" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Left" SortExpression="PaidAmt">
                    <ItemTemplate>
                        <asp:Label ID="lblPaidAmt" runat="server" Text='<%#Eval("PaidAmt") %>'/>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalPaidAmt" runat="server" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField  HeaderText="$ Saved" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Left" SortExpression="SavedAmt">
                    <ItemTemplate>
                        <asp:Label ID="lblSavedAmt" runat="server" Text='<%#Eval("SavedAmt") %>'/>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalSavedAmt" runat="server" />
                    </FooterTemplate>
                </asp:TemplateField>
             </Columns>                           
    </asp:GridView>
    </div>
    <div>
    <table  width="100%" style="margin-top:30px;">  
        <tr align="center">
            <td>                
                 <asp:Button ID="close" runat="server" Text="   Close   " class="button"   OnClientClick="return SelectClose();" />
            </td>           
        </tr>
    </table>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading ..." />      
     <asp:HiddenField runat="server" id="hdnBRSSummaryList" />
    </form>
</body>
</html>