﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="trans.aspx.cs" Inherits="Riskmaster.UI.UI.ReserveApproval.trans" %>
<%@ Import Namespace="System.Xml" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reserve Approval</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script src="../../Scripts/rsw.js" type="text/javascript"></script>
    <script language='javascript' type="text/javascript">
    
        function diarydetails(obj)
        {
        var tdname=obj.value;
        showDetails(tdname);
        }

         function showDetails(tdname)
        {
	        document.all["lblDateSubmitted"].innerHTML="Date Submitted: "+document.getElementById("SubmittedOn_"+tdname).value;
	        document.all["lblSubmittedBy"].innerHTML= "Submitted By: " + document.getElementById("SubmittedBy_" + tdname).value;
	        document.all["claimstatus"].value=document.getElementById("ClaimStatusCode_"+tdname).value;
	        document.all["claimid"].value=document.getElementById("ClaimID_"+tdname).value;
	        document.getElementById("hdnRswId").value = tdname;
	        document.getElementById("hdnResHistRowId").value = tdname;  
        }

         function onLoad()
         {
             document.getElementById("txtAppRejReqCom").value = "";
             var ctlHoldReason = document.getElementById('HoldReason');//sachin 7810
             var bRadio = document.getElementsByTagName("input");
             for( var i = 0 ; i < bRadio.length - 1 ; i++ )
             {
                 if( bRadio[i].type == "radio" )
                 {
                     bRadio[i].click();
                     bRadio[i].focus();
                     break;
                 }
             }
             if (document.getElementById("CallFor").value == "No")
             {                 
                 if (ctlHoldReason != null) {
                     alert("The worksheet is being sent to your supervisor for approval as " + ctlHoldReason.value + ".");
                 }                
             }
                 //sachin 6385 starts
             else if(document.getElementById("CallFor").value == "NoClaimInc")
             {
                 if (ctlHoldReason != null) {
                     alert("The worksheet is being sent to your supervisor for approval as " + ctlHoldReason.value + ".");
                 }                 
                
             }//sachin 6385 ends
             document.getElementById("CallFor").value = "";
             document.getElementById("HoldReason").value = "";//sachin 7810
        }        
    
    </script>
    
    <style type="text/css">
        .style1
        {
            height: 21px;
        }
    </style>
    
</head>
<body onload="parent.MDIScreenLoaded();onLoad();">
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <%try
      {
    %>
    <% if (Model.SelectSingleNode("//ErrorTransactions") != null)
       {%>
    <%foreach (XmlNode node in Model.SelectNodes("//ErrorTransactions"))
      {
          int iCount = 0; %>
    <tr>
        <td align="left" colspan="4">
            <font size="4" color="red">
                <%=node.InnerText %>
            </font>
        </td>
    </tr>
    <%iCount++;%>
    <%}%>
    <%         } %>
    <%}
      catch (Exception ee)
      {
          ErrorHelper.logErrors(ee);
          lblErrors.Text = ErrorHelper.FormatErrorsForUI("Error getting data for Supervisor Approval Of Funds Transactions");
      }%>
      
      <div id="Browsercheck" runat="server">
    <table width="100%" cellspacing="0" cellpadding="2" border="0">
        <tr>
            <td class="msgheader" bgcolor="#D5CDA4">
                Reserve Approval
            </td>
        </tr>
    
        <asp:GridView ID="gvListReserveApproval" runat="server" 
            AutoGenerateColumns="false" Width="100%" EnableViewState="False" 
            AlternatingRowStyle-CssClass="datatd" RowStyle-CssClass="datatd1" 
            ondatabound="gvListReserveApproval_DataBound">
            <HeaderStyle CssClass="msgheader" />
            <Columns>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" 
                    ItemStyle-Width="3%">
                    <ItemTemplate>
                        <input type="radio" name="valueupper" id='<%# DataBinder.Eval(Container.DataItem,"Id") %>' value='<%# DataBinder.Eval(Container.DataItem,"RswRowId")%>' onclick="diarydetails(this)")> 
<%--                        <asp:HiddenField ID="hdnRswId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"RswRowId") %>' />
                        <asp:HiddenField ID="claimstatus" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"RswStatus") %>' />--%>
                        <input type="hidden" id="SubmittedBy_<%# DataBinder.Eval(Container.DataItem,"RswRowId")%>"  value="<%# DataBinder.Eval(Container.DataItem,"SubmittedBy") %>" />
                        <input type="hidden" id="SubmittedOn_<%# DataBinder.Eval(Container.DataItem,"RswRowId")%>" value="<%# DataBinder.Eval(Container.DataItem,"DateSubmitted") %>" />
                        <input type="hidden" id="ClaimStatusCode_<%# DataBinder.Eval(Container.DataItem,"RswRowId")%>"  value="<%# DataBinder.Eval(Container.DataItem,"ClaimStatusCode") %>" />
                        <input type="hidden" id="ClaimID_<%# DataBinder.Eval(Container.DataItem,"RswRowId")%>" value="<%# DataBinder.Eval(Container.DataItem,"claimID") %>" />
                    </ItemTemplate>
                </asp:TemplateField>
           </Columns>
            
        </asp:GridView>
    </table>
    
    <table width="100%" border="0" cellpadding="2">
        <tr>
          <td colspan="2" height="30px"></td>
        </tr>
        <tr>
          <td width="20%">
            <table width="100%" border="0"> 
              <tr>
                <td>
                <asp:Label runat="server" ID="lblDateSubmitted" Text="Date Submitted: " Width="100%" />
                </td>
              </tr>
            </table>
          </td>

          <td width="80%">
            <table width="100%" border="0">
              <tr>
                <td>
                    <asp:label runat="server" ID="lblSubmittedBy" Text="Submitted By: " width="100%" />
                </td>
              </tr>
            </table>
          </td>
        </tr>
        
		<tr>
			  <td width="20%">
			  <asp:Label runat="server" ID="lblReason" Text="Reason: " />
              </td>
              <td width="20%">
              <asp:TextBox runat="server" ID="txtAppRejReqCom" Columns="50" Rows="4" TextMode="MultiLine" />
				  <!--<xforms:textarea xhtml:cols="50" xhtml:rows="4" ref="/Instance/Document/AppRejReqCom" xhtml:id="txtAppRejReqCom"></xforms:textarea>-->
                  <asp:CheckBox ID="chkShowAllItem" runat="server" AutoPostBack="true" RMXRef="/Instance/Document/Reserve/ShowAllItem"  OnCheckedChanged="chkShowAllItem_CheckedChanged" Text="Show All Items" />
			  </td>
		  </tr>
        <tr>
          <td colspan="2" height="30px"></td>
        </tr>
          </table> 
    
    
    <table width="100%" cellspacing="0" cellpadding="2" border="0">
        <tr class="ctrlgroup">
            <td colspan="10">
            </td>
        </tr>
    </table>
    <table width="60%" cellspacing="0" cellpadding="2" border="0" align="left">
    <tr class="datatd">
        <td width="5%"></td>
        <td width="5%">
        <%--<asp:Button runat="server" ID="btnApprove" class="button" Style="width:105px" Text="Approve Selected" OnClick="return ApproveFromApprovalScreen();"/>--%>
        <asp:Button runat="server" ID="btnApprove" class="button" Style="width:105px" Text="Approve Selected" OnClientClick ="return ApproveFromApprovalScreen();" OnClick="btnApprove_Click"/>
        </td>
		<td width="5%">		
        <%--<asp:Button runat="server" ID="btnReject" class="button" Style="width:105px" Text="Reject Selected" OnClick="RejectFromApprovalScreen();"/>--%>
        <asp:Button runat="server" ID="btnReject" class="button" Style="width:105px" Text="Reject Selected" OnClientClick ="return RejectFromApprovalScreen();" OnClick="btnReject_Click"/>
		</td>
        <td width="5%">
        <%--<asp:Button runat="server" ID="btnPrint" class="button" Style="width:105px" Text="Print Worksheet" OnClick="PrintWorksheet();"/>--%>
        <asp:Button runat="server" ID="btnPrint" class="button" Style="width:105px" Text="Print Worksheet" OnClientClick ="return PrintWorksheet();"/>
        </td>
    </tr>
	</table>
 </div>
    <!--To be binded -->
    <input type="hidden" id="claimid" runat="server" />
    <input type="hidden" id="claimstatus" runat="server" />
    <input type="hidden" id="hdnRswId" value="-1" runat="server" RMXRef="/Instance/Document/RswId" />
    <asp:TextBox Style="display: none" id="hdnResHistRowId" Text ="-1" runat="server" RMXRef="/Instance/Document/Reserve/ResHistRowId" />
    <asp:TextBox Style="display: none" id="hdnApproveorRejectRsv" Text ="" runat="server" RMXRef="/Instance/Document/Reserve/ApproveorRejectRsv" />
    <asp:TextBox Style="display: none" id="hdnScreenName" runat="server" />
    <asp:Label ID="lblErrors" class ="warntext "  runat="server"></asp:Label>
    <input type="hidden" runat="server" id="CallFor" />
        <input type="hidden" runat="server" id="HoldReason" /><%--sachin 7810--%>
    </form>
</body>
</html>
