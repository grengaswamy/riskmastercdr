﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransAsync.aspx.cs" Inherits="Riskmaster.UI.UI.ReserveApproval.TransAsync" ValidateRequest="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reserve Approval</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../Content/ng-grid.css" />
    <script src="../../Scripts/jquery/jquery-1.8.0.min.js"></script>
    <script src="../../Scripts/jquery/jquery-ui-1.9.2.min.js"></script>
    <script src="../../Scripts/angularjs/angular.min.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid.debug.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid-layout.js"></script>

    <!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start: Include Export to excel Js in code -->
   <%-- <script src="../../Scripts/angularjs/ng-grid-csv-export.js"></script>--%> 
    <!-- End CUSTOMIZATION-->
    <script src="../../Scripts/rsw.js" type="text/javascript"></script>
    <script src="../../Scripts/angularjs/reserveApproval.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ngGridExportPlugin.js"></script>
    <script type="text/javascript" src="../../Scripts/form.js"></script>
    <!-- RMA-8253 pgupta93 -->
    <style type="text/css">
        .style1 {
            height: 21px;
        }
    </style>


</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="form1" runat="server">
        <div id="divError" style="min-height: 0px; max-height: 120px; overflow-y: auto;">
            <rmcontrol:errorcontrol id="ErrorControl1" runat="server" />
        </div>
        <!--To be binded -->
        <input type="hidden" id="claimid" runat="server" />
        <input type="hidden" id="claimstatus" runat="server" />
        <input type="hidden" id="hdnRswId" value="-1" runat="server" rmxref="/Instance/Document/RswId" />
        <input type="hidden" id="hdnSelectedVersion" value="" runat="server" clientidmode="static" />
        <asp:textbox style="display: none" id="hdnResHistRowId" text="-1" runat="server" rmxref="/Instance/Document/Reserve/ResHistRowId" />
        <asp:textbox style="display: none" id="hdnApproveorRejectRsv" text="" runat="server" rmxref="/Instance/Document/Reserve/ApproveorRejectRsv" />
        <asp:textbox style="display: none" id="hdnScreenName" runat="server" clientidmode="Static" />
        <input type="hidden" runat="server" id="hdQuery" clientidmode="Static" />
        <input type="hidden" runat="server" id="hdnParent" clientidmode="Static" /><!--RMA 8253 pgupta93 -->
        <asp:label id="lblErrors" runat="server"></asp:label>
        <input type="hidden" runat="server" id="CallFor" />
    </form>
    <div ng-app="rmaApp" id="ng-app">
        <div ng-controller="reserveController">
            <!-- Added By Nitika For JIRA 8253 START -->
              <!--achouhan3    Modified to handle Multiselect Checkbox in case of My pending Reserve Starts -->
            <div style="float: right; clear:both;" id="divapp" runat="server">
                <div id="divShowItems" runat="server" style="float: left;">
                    <input type="checkbox" id="chkShowAllItem" ng-model="reserveModel.isShowAllItems" ng-change="ShowAllItems()" />
                    <span id="spnShowAllItem" style="width: 100%" runat="server"></span>
                </div>
                <div id="divCheckItems" style="float: right;">
                    <input type="checkbox" id="chkTrans" ng-model="reserveModel.isShowMyTrans" ng-change="ShowMyTrans()" /><span id="lblPendingItems" runat="server"></span>
                </div>
            </div>
            <!-- Added By Nitika For JIRA 8253 END -->
              <!--achouhan3    Modified to handle Multiselect Checkbox in case of My pending Reserve Ends -->
            <div id="divImage" style="display: none;">
                <div class="image">
                    <input type="image" src="../../Images/tb_save_active.png" style="width: 28px; height: 28px; border: 0px;" id="save" alt="<%$ Resources:ttSavePref %>" runat="server"
                        title="<%$ Resources:ttSavePref %>" onmouseover="this.src='../../Images/tb_save_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_save_active.png';this.style.zoom='100%'" ng-click="SavePreferences()" tooltip="<%$ Resources:ttSavePref %>" />
                </div>
                <!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start: Add button to redirect click event, better UI  -->
                <!-- Raman : Gatekeeper Action: Export to Excel functionality need to be reworked upon. It should be exporting to excel and not csv as its currently doing.
                    Also formatting need to be retained and export should be readable. Currently it does not add any value to the client.
                <!--CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start: Add button to redirect click event, better UI  -->
                
                <div class="image">
                    <input type="image" src="../../Images/tb_exportexcel_active.png" style="width: 28px; height: 28px; border: 0px;" id="btnExport" alt="Export" runat="server"
                        title="Export to Excel" onmouseover="this.src='../../Images/tb_exportexcel_mo.png';this.style.zoom='110%'"  
                        onmouseout="this.src='../../Images/tb_exportexcel_active.png';this.style.zoom='100%'" onclick="redirectE2E()" />
                </div>     
                <!-- End CUSTOMIZATION-->
               </div>
       <div style="clear: both"></div>
       <table width="100%" cellspacing="0" cellpadding="2" border="0" id="tblReserveApproval">
            <tr>
                <td class="msgheader" bgcolor="#D5CDA4">
                    <span id="spnHeader" runat ="server" >  </span>
                </td>
            </tr>
        </table>
            <div style="clear: both"></div>
            <div class="gridStyle" ng-grid="gridOptions" style="display: none;" id="divReserveGrid"></div>
                 <div style="text-align:center;" >
             <label style="display: none; " id="lblEmptyData" />
             </div>   
            <div style="clear: both"></div>   
            <div id ="divbottom" runat="server">
            <table width="100%" border="0" cellpadding="2"> <!-- id given By Nitika for JIRA 8253 -->
                <tr>
                    <td colspan="2" height="30px"></td>
                </tr>
               <%-- <tr>
                    <td width="20%">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span id="lblDateSubmitted" style="width: 100%">Date Submitted: </span>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td width="80%">
                       <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span id="lblSubmittedBy" style="width: 100%">Submitted By: </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
                <!--RMA-10194   achopuhan3  Code Commented since these two columns are not partof grid starts-->
                 <%--<tr>
                    <td colspan="2">
                         <span id="lblDateSubmitted" style="width: 100%" runat="server" > :</span>
                    </td>
                </tr>

                <tr>
                    <td width="20%" id="tblReason">
                        <span id="lblReason" style="width: 100%">Reason: </span>
                    </td>
                </tr>--%>
                <!--RMA-10194   achopuhan3  Code Commented since these two columns are not partof grid starts ends-->
                <tr>
                    <td width="20%">
                        <span id="lblReason" style="width: 100%" runat="server" > :</span>
                    </td>
                    <td width="80%">
                        <textarea cols="50" rows="4" id="txtAppRejReqCom"></textarea>
                        <%--<asp:TextBox runat="server" ID="txtAppRejReqCom" Columns="50" Rows="4" TextMode="MultiLine" />--%>
                        <!--<xforms:textarea xhtml:cols="50" xhtml:rows="4" ref="/Instance/Document/AppRejReqCom" xhtml:id="txtAppRejReqCom"></xforms:textarea>-->
                        <%--<asp:CheckBox ID="chkShowAllItem" runat="server" AutoPostBack="true" RMXRef="/Instance/Document/Reserve/ShowAllItem"  OnCheckedChanged="chkShowAllItem_CheckedChanged" Text="Show All Items" />--%>
                        <%--<input type="checkbox" id="chkShowAllItem" ng-model="reserveModel.isShowAllItems" ng-change="ShowAllItems()" /><span id="ShAllitems">Show All Items</span>
                        <input type="checkbox" id="chkTrans" ng-model="reserveModel.isShowMyTrans" ng-change="ShowMyTrans()"/>Show My Transcations--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" height="30px"></td>
                </tr>
            </table>

            <table width="100%" cellspacing="0" cellpadding="2" border="0">
                <tr class="ctrlgroup">
                    <td colspan="10"></td>
                </tr>
            </table>
              </div>
            <table width="60%" cellspacing="0" cellpadding="2" border="0" align="left" id ="tblButton">
                <tr class="datatd">
                    <td width="5%"></td>
                    <td width="5%">
                        <%--<asp:button runat="server" id="btnApprove" class="button" style="width: 105px" text="Approve Selected" onclientclick="return ApproveFromApprovalScreen();" onclick="btnApprove_Click" />--%>
                        <input type="button" id="btnApprove" class="button" style="width: 105px" value="<%$ Resources:btnApproveSelected %>" ng-click="ApproveClick()" runat="server"/>
                    </td>
                    <td width="5%">
                        <%--<asp:button runat="server" id="btnReject" class="button" style="width: 105px" text="Reject Selected" onclientclick="return RejectFromApprovalScreen();" onclick="btnReject_Click" />--%>
                        <input type="button" id="btnReject" class="button" style="width: 105px" value="<%$ Resources:btnRejectSelected %>" ng-click="RejectClick()" runat="server"/>
                    </td>
                    <td width="5%">
                        <%--<asp:button runat="server" id="btnPrint" class="button" style="width: 105px" text="Print Worksheet" onclientclick="return PrintWorksheet();" />--%>
                        <input type="button" id="btnPrint" class="button" style="width: 105px" value="<%$ Resources:btnPrintWorksheet %>" runat="server" visible="false" onclick="return PrintWorksheet();"  />
                    </td>
                </tr>
            </table>
              <!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start:Added a footer  --> <!--RMA - 7520  achouhan3  please ensure thsi Iframe is put at the end os page Starts-->
            <div class="footer">
                <iframe id="csvDownloadFrame" style="display:none" />
            </div>
            <!-- End CUSTOMIZATION-->  <!--RMA - 7520  achouhan3  please ensure thsi Iframe is put at the end os page Ends-->
        </div>
    </div>

</body>
</html>
