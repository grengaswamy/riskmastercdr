﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

using System.ServiceModel;
using Riskmaster.UI.Shared;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.BESGroupDef
{
    public partial class BESGroupDef : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                XmlDocument oFDMPageDom = new XmlDocument();
                string sReturn = "";
                //Preparing XML to send to service
                XElement oMessageElement = LoadBESGroupTemplate();
                //Modify XML 
                XElement oRowId = oMessageElement.XPathSelectElement("./Document/BESGroup/RowId");
                if (oRowId != null)
                {
                    if (Request.QueryString["selectedid"] != null)
                    {
                        oRowId.Value = Request.QueryString["selectedid"];
                        rowId.Value = oRowId.Value;
                    }
                }
                // Nadim changed for Assisgning Oracle role to bes user
                XElement oOracleRole = oMessageElement.XPathSelectElement("./Document/BESGroup/OracleRole");
                if (oOracleRole != null)
                {
                    oOracleRole.Value = AppHelper.GetQueryStringValue("OracleRole");
                }
                // Nadim changed for Assisgning Oracle role to bes user
                XElement oConfRec = oMessageElement.XPathSelectElement("./Document/BESGroup/ConfRec");
                if (oConfRec != null)
                {
                    oConfRec.Value = AppHelper.GetQueryStringValue("ConfRec");
                    ConfRec.Value = AppHelper.GetQueryStringValue("ConfRec");
                }
                //nadim for 16299
                setTitle();
                //nadim for 16299
                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                if (oFDMPageDom.InnerXml != "")
                {
                    ViewState["oFDMPageDom"] = oFDMPageDom.OuterXml;
                }
                if (oFDMPageDom.SelectSingleNode("//GroupName") != null)
                {
                    txtGroupName.Value = oFDMPageDom.SelectSingleNode("//GroupName").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//optOrgEntitiesValue") != null)
                {
                    if (oFDMPageDom.SelectSingleNode("//optOrgEntitiesValue").InnerText == "0")
                    {
                        optSome.Checked = true;
                        //Org.Enabled = true;
                        //Orgbtn.Enabled = true;
                        //Orgbtndel.Enabled = true;
                        optAll.Checked = false;
                    }
                    else
                    {
                        optAll.Checked = true;
                        optSome.Checked = false;
                        //Org.Enabled = false;
                        //Orgbtn.Enabled = false;
                        //Orgbtndel.Enabled = false;
                    }
                }
                if (oFDMPageDom.SelectSingleNode("//SelEntities/listrow") != null)
                {
                    DataSet aDataSet = new DataSet();
                    aDataSet.ReadXml(new StringReader(oFDMPageDom.SelectSingleNode("//SelEntities/listrow").OuterXml));
                    Org.DataSource = aDataSet.Tables["rowtext"];
                    Org.DataTextField = "title";
                    Org.DataValueField = "value";
                    Org.DataBind();
                }
                if (oFDMPageDom.SelectSingleNode("//SelEntitiesListItems") != null)
                {
                    SelEntitiesListItems.Value = oFDMPageDom.SelectSingleNode("//SelEntitiesListItems").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//DBType") != null)
                {
                    DBType.Value = oFDMPageDom.SelectSingleNode("//DBType").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//BrokerSupport") != null)
                {
                    BrokerSupport.Value = oFDMPageDom.SelectSingleNode("//BrokerSupport").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//InsurerSupport") != null)
                {
                    InsurerSupport.Value = oFDMPageDom.SelectSingleNode("//InsurerSupport").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//IsOptAll") != null)
                {
                    IsOptAll.Value = oFDMPageDom.SelectSingleNode("//IsOptAll").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//ViewEnhanceBES") != null)
                {
                    ViewEnhanceBES.Value = oFDMPageDom.SelectSingleNode("//ViewEnhanceBES").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//PassWord") != null)
                {
                    PassWord.Value = oFDMPageDom.SelectSingleNode("//PassWord").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//Defer") != null)
                {
                    Defer.Value = oFDMPageDom.SelectSingleNode("//Defer").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//OveridePassFlag") != null)
                {
                    hidOveridePassFlag.Value = oFDMPageDom.SelectSingleNode("//OveridePassFlag").InnerText;
                }
                //PJS :02-18-2009 -(Boeing) UseBrokerBes Tag added for setting USE_BROKER_BES 
                if (oFDMPageDom.SelectSingleNode("//UseBrokerBes") != null)
                {
                    hdnUseBrokerBes.Value = oFDMPageDom.SelectSingleNode("//UseBrokerBes").InnerText;
                }
                //    searchtype.DataBind();
                //    if (Dropdown_Selection_value(aDataSet.Tables["level"], "selected", "1", "id") != "")
                //    {
                //        searchtype.SelectedValue = Dropdown_Selection_value(aDataSet.Tables["level"], "selected", "1", "id");
                //        OriginalType.Value = searchtype.SelectedValue;
                //    }
                //    Label2.Value = searchtype.SelectedValue;
                //    //searchtype.SelectedIndex = aDataSet.Tables["level"].Rows.IndexOf(aDataSet.Tables["level"].Rows.Find("1"));

                //}

            }
        }
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);
            
        }
        /// <summary>
        /// CWS request message template for BES-Setup
        /// </summary>
        /// <returns></returns>
        private XElement LoadBESGroupTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>ORGSECAdaptor.LoadGroupDef</Function> 
                      </Call>
                      <Document>
                          <BESGroup>
                                <RowId></RowId> 
                                <IsOptAll></IsOptAll> 
                                <BrokerSupport></BrokerSupport>
                                <labGroupName></labGroupName> 
                                <GroupId></GroupId>
                                <InsurerSupport></InsurerSupport>         
                                <DBType></DBType> 
                                <GroupName></GroupName> 
                                                        <optOrgEntitiesValue></optOrgEntitiesValue>
                                <SelEntities>
                                    <listhead>
                                      <rowhead colname='OrgName'/> 
                                    </listhead>
                                </SelEntities> 
                                <SelEntitiesListItems></SelEntitiesListItems> 
                                                        <optBrokersValue></optBrokersValue> 
                                <Brokers>
                                    <listhead>
                                    <rowhead colname='BrokerFirmsName'/> 
                                    </listhead>
                                </Brokers> 
                                                        <optInsurersValue></optInsurersValue> 
                                <Insurers>
                                  <listhead>
                                    <rowhead colname='InsurersName'/> 
                                  </listhead>
                                </Insurers> 
                                                        <optAdjTextTypesValue></optAdjTextTypesValue> 
                                <AdjTextTypes>
                                    <listhead>
                                        <rowhead colname='AdjTextTypesCode'/>
                                    </listhead>
                                </AdjTextTypes> 
                                <AdjusterListItems></AdjusterListItems>
                                                        <optEnhNoteTypesValue></optEnhNoteTypesValue> 
                                <EnhNoteTypes>
                                    <listhead>
                                        <rowhead colname='EnhNoteTypesCode'/>
                                    </listhead>
                                </EnhNoteTypes> 
                                <NotesListItems></NotesListItems>
                                <Users>
                                      <listhead>
                                        <rowhead colname='UserName'/> 
                                      </listhead>
                                </Users> 
                                <UserListItems></UserListItems>
                                <DB2Password></DB2Password>
                                <PassWord></PassWord>
                                <UserId></UserId>
                                <ViewEnhanceBES></ViewEnhanceBES>
                                <OveridePassFlag></OveridePassFlag>
                                <BrokersListItems></BrokersListItems>
                                <InsurersListItems></InsurersListItems>
                                    <SelectAdjusterGroupTitle></SelectAdjusterGroupTitle>
                                    <SelectBrokerInsurerDefGroupTitle></SelectBrokerInsurerDefGroupTitle>
                                    <SelectUserAssigGroupTitle></SelectUserAssigGroupTitle>
                                    <DB2LoginAccountInfoGroupTitle></DB2LoginAccountInfoGroupTitle>
                                    <CommonToAllGroupTitle></CommonToAllGroupTitle>
                                    <SelectOrgEntitiesGroupTitle></SelectOrgEntitiesGroupTitle>
                                    <Defer></Defer>
                                    <IsClose></IsClose>
                                    <OracleRole></OracleRole>
                                    <UseBrokerBes></UseBrokerBes>
                                    <ConfRec></ConfRec>
                                                    
                          </BESGroup>
                      </Document>
                </Message>


            ");
            return oTemplate;
        }

        public void SkiptoPreviousStep(object sender, WizardNavigationEventArgs e)
        {
            MaintainListBox();
            ((Button)Wizardlist.FindControl("StepNavigationTemplateContainerID").FindControl("StepNextButton")).Enabled = true;
            //nadim for 16299
            setTitle();
            //nadim for 16299

        }

        public void SkiptoNextStep(object sender, WizardNavigationEventArgs e)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
                        
            switch (e.CurrentStepIndex)
            {
                case 0:
                    #region rough
                    //if (searchtype.SelectedValue != "20")
                    //{
                    //    Wizardlist.ActiveStepIndex = 2;
                    //    XElement oFunctionName = oMessageElement.XPathSelectElement("./Call/Function");
                    //    if (oFunctionName != null)
                    //    {
                    //        oFunctionName.Value = "SearchWizardAdaptor.GetStep2n3";
                    //    }
                    //    FillStep2(ref oMessageElement);
                    //    DisplayStep2(ref oMessageElement);
                    //}
                    //else
                    //{
                    //    XElement oFunctionName = oMessageElement.XPathSelectElement("./Call/Function");
                    //    if (oFunctionName != null)
                    //    {
                    //        oFunctionName.Value = "SearchWizardAdaptor.GetAdmTrckStep";
                    //    }
                    //    FillStep2(ref oMessageElement);
                    //    DisplayStep1a(ref oMessageElement);
                    //}
                    #endregion
                    #region rough 1 
                    // if(DBType.value == "4")
                    // {
                    //    if((BrokerSupport.value == "1" || document.forms[0].InsurerSupport.value == "1") && optAll.Checked == false)
                    //    {
                    //        hidNextPage.value = "GoToPage3";
                    //        IsOptAll.value = "0";
                    //    }
                    //    else
                    //    {	
                    //        hidNextPage.value = "GoToPage2";
                    //        if(optAll.Checked == true)
                    //            IsOptAll.value = "1";
                    //        else
                    //            IsOptAll.value = "0";
                    //    }
                    //}
                    //else
                    //{
                    //    if(optAll.Checked == true)
                    //    {
                    //      hidNextPage.value = "GoToPage2";
                    //      IsOptAll.value = "1";
                    //    }
                    //    else
                    //    {
                    //      hidNextPage.value = "GoToPage4";
                    //      IsOptAll.value = "0";
                    //    }
                    // }
                    #endregion
                    if (hidNextPage.Value == "GoToPage2")
                    {
                        Wizardlist.ActiveStepIndex = 3;
                        if (hdnVisited4stPage.Value == "")
                        {
                            if (ViewState["oFDMPageDom"] != null)
                                oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                            DisplayStep4(ref oFDMPageDom);//Parijat: 15583 & 15581
                        }
                        DisableNextButton();
                        //nadim for 16299
                        setTitle();
                        //nadim for 16299
                    }
                    else if (hidNextPage.Value == "GoToPage3")
                    {
                        Wizardlist.ActiveStepIndex = 2;
                        if (hdnVisited3stPage.Value == "")
                        {
                            if (ViewState["oFDMPageDom"] != null)
                                oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                            DisplayStep3(ref oFDMPageDom);
                        }
                        //nadim for 16299
                        setTitle();
                        //nadim for 16299
                    }
                    else if (hidNextPage.Value == "GoToPage5")//Boeing :USE_BROKER_BES
                    {
                        Wizardlist.ActiveStepIndex = 4;
                        if (hdnVisited5stPage.Value == "")
                        {
                            if (ViewState["oFDMPageDom"] != null)
                                oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                            DisplayStep5(ref oFDMPageDom);
                        }
                        //nadim for 16299
                        setTitle();
                        //nadim for 16299
                       
                    }
                    else
                    {
                        if (hdnVisited2stPage.Value == "")
                        {
                            if (ViewState["oFDMPageDom"] != null)
                                oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                            DisplayStep2(ref oFDMPageDom);
                        }
                    }
                    MaintainListBox();
                    //nadim for 16299
                    setTitle();
                    //nadim for 16299
                    break;
                case 1:

                    //Wizardlist.ActiveStepIndex = 3;
                    //if (hdnVisited4stPage.Value == "")
                    //{
                    //    if (ViewState["oFDMPageDom"] != null)
                    //        oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                    //    DisplayStep4(ref oFDMPageDom);
                    //}
                    //DisableNextButton();
                    Wizardlist.ActiveStepIndex = 5;
                    if (hdnVisited6stPage.Value == "")
                    {
                        if (ViewState["oFDMPageDom"] != null)
                            oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                        DisplayStep6(ref oFDMPageDom);
                    }
                    MaintainListBox();
                    //nadim for 16299
                    setTitle();
                    //nadim for 16299
                    //Temporary
                    //if (hdnVisited3stPage.Value == "")
                    //{
                    //    if (ViewState["oFDMPageDom"] != null)
                    //        oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                    //    DisplayStep3(ref oFDMPageDom);
                    //}

                    break;
                case 2:
                    if (hidNextPage.Value == "GoToPage4")
                    {
                        Wizardlist.ActiveStepIndex = 1;
                        if (hdnVisited2stPage.Value == "")
                        {
                            if (ViewState["oFDMPageDom"] != null)
                                oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                            DisplayStep2(ref oFDMPageDom);
                        }
                        //nadim for 16299
                        setTitle();
                        //nadim for 16299
                    }
                    else if (hidNextPage.Value == "GoToPage2")
                    {
                        if (hdnVisited4stPage.Value == "")
                        {
                            if (ViewState["oFDMPageDom"] != null)
                                oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                            DisplayStep4(ref oFDMPageDom);
                        }
                        DisableNextButton();
                    }
                    MaintainListBox();
                    //nadim for 16299
                    setTitle();
                    //nadim for 16299
                    break;
                case 4://Boeing
                    Wizardlist.ActiveStepIndex = 1;
                    if (hdnVisited2stPage.Value == "")
                    {
                        if (ViewState["oFDMPageDom"] != null)
                            oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                        DisplayStep2(ref oFDMPageDom);
                    }
                    MaintainListBox();
                    //nadim for 16299
                    setTitle();
                    //nadim for 16299
                    break;
                case 5:
                    Wizardlist.ActiveStepIndex = 3;
                    if (hdnVisited4stPage.Value == "")
                    {
                        if (ViewState["oFDMPageDom"] != null)
                            oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                        DisplayStep4(ref oFDMPageDom);
                    }
                    DisableNextButton();

                    MaintainListBox();
                    setTitle();
                    break;
            }
        }

        public void DisplayStep2(ref XmlDocument oFDMPageDom)
        {
            hdnVisited2stPage.Value = "yes";
            if (oFDMPageDom.SelectSingleNode("//optAdjTextTypesValue").InnerText == "1" || oFDMPageDom.SelectSingleNode("//optAdjTextTypesValue").InnerText == "")
            {
                optAllAdjTextTypes.Checked = true;
                optSomeAdjTextTypes.Checked = false;
                optExceptAdjTextTypes.Checked = false;
                //((ListBox)lstAdjTextTypes.FindControl("multicode")).Enabled = false;
                //((Button)lstAdjTextTypes.FindControl("multicodebtn")).Enabled= false;
                //((Button)lstAdjTextTypes.FindControl("multicodebtndel")).Enabled = false;
                
            }
            else if(oFDMPageDom.SelectSingleNode("//optAdjTextTypesValue").InnerText == "2")
            {
                optSomeAdjTextTypes.Checked = true;
                optAllAdjTextTypes.Checked = false;
                optExceptAdjTextTypes.Checked = false;
                //((ListBox)lstAdjTextTypes.FindControl("multicode")).Enabled = true;
                //((Button)lstAdjTextTypes.FindControl("multicodebtn")).Enabled = true;
                //((Button)lstAdjTextTypes.FindControl("multicodebtndel")).Enabled = true;
                
            }
            else if (oFDMPageDom.SelectSingleNode("//optAdjTextTypesValue").InnerText == "3")
            {
                optExceptAdjTextTypes.Checked = true;
                optSomeAdjTextTypes.Checked = false;
                optAllAdjTextTypes.Checked = false;
                //((ListBox)lstAdjTextTypes.FindControl("multicode")).Enabled = true;
                //((Button)lstAdjTextTypes.FindControl("multicodebtn")).Enabled = true;
                //((Button)lstAdjTextTypes.FindControl("multicodebtndel")).Enabled = true;
               
            }
            if (oFDMPageDom.SelectSingleNode("//AdjTextTypes/listrow") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.SelectSingleNode("//AdjTextTypes/listrow").OuterXml));
                ((ListBox)lstAdjTextTypes.FindControl("multicode")).DataSource = aDataSet.Tables["rowtext"];
                ((ListBox)lstAdjTextTypes.FindControl("multicode")).DataTextField = "title";
                ((ListBox)lstAdjTextTypes.FindControl("multicode")).DataValueField = "value";
                ((ListBox)lstAdjTextTypes.FindControl("multicode")).DataBind();
            }
            //nadim for 16299
            setTitle();
            //nadim for 16299
            
        }
        public void DisplayStep3(ref XmlDocument oFDMPageDom)
        {
            hdnVisited3stPage.Value = "yes";

            if (oFDMPageDom.SelectSingleNode("//optBrokersValue").InnerText == "1" || oFDMPageDom.SelectSingleNode("//optBrokersValue").InnerText == "")
            {
                optAllBrokers.Checked = true;
                optSomeBrokers.Checked = false;
                //Nadim for Rem brokers and insurers
                lstBrokerFirm2.Enabled = false;
                //lstBrokerFirmbtn.Disabled = true;
                lstBrokerFirmbtn2.Disabled = true;
                lstBrokerFirmbtnde2.Disabled = true;
                //Nadim for Rem brokers and insurers
            }
            else if (oFDMPageDom.SelectSingleNode("//optBrokersValue").InnerText == "2")
            {
                optAllBrokers.Checked = false;
                optSomeBrokers.Checked = true;
                //Nadim for Rem brokers and insurers
                lstBrokerFirm2.Enabled = true;
                lstBrokerFirmbtn2.Disabled = false;
                lstBrokerFirmbtnde2.Disabled = false;
                //Nadim for Rem brokers and insurers
            }
            if (oFDMPageDom.SelectSingleNode("//Brokers/listrow") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.SelectSingleNode("//Brokers/listrow").OuterXml));
                //Nadim for Rem brokers and insurers
                //((ListBox)lstBrokers.FindControl("multicode")).DataSource = aDataSet.Tables["rowtext"];
                //((ListBox)lstBrokers.FindControl("multicode")).DataTextField = "title";
                //((ListBox)lstBrokers.FindControl("multicode")).DataValueField = "value";
                //((ListBox)lstBrokers.FindControl("multicode")).DataBind();


                lstBrokerFirm2.DataSource = aDataSet.Tables["rowtext"];
                lstBrokerFirm2.DataTextField = "title";
                lstBrokerFirm2.DataValueField = "value";
                lstBrokerFirm2.DataBind();
                //Nadim for Rem brokers and insurers
            }
            //Insurer
            if (oFDMPageDom.SelectSingleNode("//optInsurersValue").InnerText == "1" || oFDMPageDom.SelectSingleNode("//optInsurersValue").InnerText == "")
            {
                optAllInsurers.Checked = true;
                optSomeInsurers.Checked = false;
                //Nadim for Rem brokers and insurers
                //nadim
                //lstInsurers.Enabled = false;
                lstInsurerFirm3.Enabled = false;


                //lstBrokerFirm1.Enabled = false;
                //lstBrokerFirmbtn.Disabled = true;
                lstInsurerFirmbtn3.Disabled = true;
                lstInsurerFirmbtnde3.Disabled = true;
                //Nadim for Rem brokers and insurers
            }
            else if (oFDMPageDom.SelectSingleNode("//optInsurersValue").InnerText == "2")
            {
                optAllInsurers.Checked = false;
                optSomeInsurers.Checked = true;
                //Nadim for Rem brokers and insurers
                lstInsurerFirm3.Enabled = true;
                lstInsurerFirmbtn3.Disabled = false;
                lstInsurerFirmbtnde3.Disabled = false;
                //Nadim for Rem brokers and insurers
            }
            if (oFDMPageDom.SelectSingleNode("//Insurers/listrow") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.SelectSingleNode("//Insurers/listrow").OuterXml));
                //Nadim for Rem brokers and insurers
                //((ListBox)lstBrokers.FindControl("multicode")).DataSource = aDataSet.Tables["rowtext"];
                //((ListBox)lstBrokers.FindControl("multicode")).DataTextField = "title";
                //((ListBox)lstBrokers.FindControl("multicode")).DataValueField = "value";
                //((ListBox)lstBrokers.FindControl("multicode")).DataBind();
                lstInsurerFirm3.DataSource = aDataSet.Tables["rowtext"];
                lstInsurerFirm3.DataTextField = "title";
                lstInsurerFirm3.DataValueField = "value";
                lstInsurerFirm3.DataBind();
                //Nadim for Rem brokers and insurers
            }


            //nadim for 16299
            setTitle();
            //nadim for 16299
            
        }
        public void DisplayStep4(ref XmlDocument oFDMPageDom)
        {
            hdnVisited4stPage.Value = "yes";
            if (oFDMPageDom.SelectSingleNode("//Users/listrow") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.SelectSingleNode("//Users/listrow").OuterXml));
                lstUsers.DataSource = aDataSet.Tables["rowtext"];
                lstUsers.DataTextField = "title";
                lstUsers.DataValueField = "value";
                lstUsers.DataBind();
            }

            if (IsOptAll.Value == "0")
            {
                //pmittal5 - Password change functionality in case of SQL also.
                //if (DBType.Value == "4")
				if(ConfRec.Value == "0")
                {

                    if (rowId.Value != "")
                    {
                        ////Nadim-in case of oracle  db group name is coming different in case of new and edit.
                        if (Request.QueryString["mode"] != null)
                        {
                            if (Request.QueryString["mode"] == "add")
                            {
                                if (oFDMPageDom.SelectSingleNode("//labGroupName") != null)
                                {
                                    txtGroupId.Value = oFDMPageDom.SelectSingleNode("//labGroupName").InnerText;
                                    //rowId.Value = txtGroupId.Value;
                                }
                            }
                            else
                            {
                                //txtGroupId.Value = "sg" + rowId.Value;
                                if (oFDMPageDom.SelectSingleNode("//UserId") != null)
                                {
                                    txtGroupId.Value = oFDMPageDom.SelectSingleNode("//UserId").InnerText;
                                }
                            }
                        }
                        
                        //txtGroupId.Value = "sg" + rowId.Value;
                        //Nadim-in case of oracle  db group name is coming different in case of new and edit.
                    }
                    txtPass.Value = PassWord.Value;
                }
            }
            //nadim changed for defer check box
            //----------------------------------------------------------------------------
            //if (ViewEnhanceBES.Value == "yes")
            //{
            //    if (rowId.Value != "")
            //    {
            //        if (DBType.Value == "4")
            //        {
            //            if (Defer.Value == "1")
            //                defer3.Checked = true;
            //            else
            //                defer3.Checked = false;
            //        }
            //    }
            //}
            //nadim changed for defer check box
            //----------------------------------------------------------------------
            //nadim for 16299
            setTitle();
            //nadim for 16299

        }
        public void DisplayStep5(ref XmlDocument oFDMPageDom)//Boeing
        {
            hdnVisited5stPage.Value = "yes";

            if (oFDMPageDom.SelectSingleNode("//optBrokersValue").InnerText == "1" || oFDMPageDom.SelectSingleNode("//optBrokersValue").InnerText == "")
            {
                optAllBrokers1.Checked = true;
                optSomeBrokers1.Checked = false;
                //Broker Changes 
                lstBrokerFirm1.Enabled = false;
                //lstBrokerFirmbtn.Disabled = true;
                lstBrokerFirmbtn.Disabled = true;
                lstBrokerFirmbtndel.Disabled = true;
            }
            else if (oFDMPageDom.SelectSingleNode("//optBrokersValue").InnerText == "2")
            {
                optAllBrokers1.Checked = false;
                optSomeBrokers1.Checked = true;
               //broker changes
                lstBrokerFirm1.Enabled = true;
                lstBrokerFirmbtn.Disabled = false;
                lstBrokerFirmbtndel.Disabled = false;
            }
            if (oFDMPageDom.SelectSingleNode("//Brokers/listrow") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.SelectSingleNode("//Brokers/listrow").OuterXml));
               //Broker Changes
                //((ListBox)lstBrokerFirm.FindControl("multicode")).DataSource = aDataSet.Tables["rowtext"];
                //((ListBox)lstBrokerFirm.FindControl("multicode")).DataTextField = "title";
                //((ListBox)lstBrokerFirm.FindControl("multicode")).DataValueField = "value";
                //((ListBox)lstBrokerFirm.FindControl("multicode")).DataBind();

                lstBrokerFirm1.DataSource = aDataSet.Tables["rowtext"];
                lstBrokerFirm1.DataTextField = "title";
                lstBrokerFirm1.DataValueField = "value";
                lstBrokerFirm1.DataBind();
            }
            //nadim for 16299
            setTitle();
            //nadim for 16299
        }
        public void DisplayStep6(ref XmlDocument oFDMPageDom)
        {
            hdnVisited6stPage.Value = "yes";
            if (oFDMPageDom.SelectSingleNode("//optEnhNoteTypesValue").InnerText == "1" || oFDMPageDom.SelectSingleNode("//optEnhNoteTypesValue").InnerText == "")
            {
                optAllEnhanceNoteTypes.Checked = true;
                optSomeEnhanceNoteTypes.Checked = false;
                optExceptEnhanceNoteTypes.Checked = false;
            }
            else if (oFDMPageDom.SelectSingleNode("//optEnhNoteTypesValue").InnerText == "2")
            {
                optSomeEnhanceNoteTypes.Checked = true;
                optAllEnhanceNoteTypes.Checked = false;
                optExceptEnhanceNoteTypes.Checked = false;
            }
            else if (oFDMPageDom.SelectSingleNode("//optEnhNoteTypesValue").InnerText == "3")
            {
                optExceptEnhanceNoteTypes.Checked = true;
                optSomeEnhanceNoteTypes.Checked = false;
                optAllEnhanceNoteTypes.Checked = false;
            }
            if (oFDMPageDom.SelectSingleNode("//EnhNoteTypes/listrow") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.SelectSingleNode("//EnhNoteTypes/listrow").OuterXml));
                ((ListBox)lstEnhanceNoteTypes.FindControl("multicode")).DataSource = aDataSet.Tables["rowtext"];
                ((ListBox)lstEnhanceNoteTypes.FindControl("multicode")).DataTextField = "title";
                ((ListBox)lstEnhanceNoteTypes.FindControl("multicode")).DataValueField = "value";
                ((ListBox)lstEnhanceNoteTypes.FindControl("multicode")).DataBind();
            }
            //nadim for 16299
            setTitle();
            //nadim for 16299

        }
        protected void WizardFinish_Click(object sender, EventArgs e)
        {
            MaintainListBox();
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            ////Preparing XML to send to service
            //XElement oMessageElement = LoadBESGroupTemplate();

            if (ViewState["oFDMPageDom"] != null)
                oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
            //Modify XML 
          
            oFDMPageDom.SelectSingleNode("./Document/BESGroup/RowId").InnerText = rowId.Value;
            oFDMPageDom.SelectSingleNode("//IsOptAll").InnerText = IsOptAll.Value;
            if(optAll.Checked ==true)
                oFDMPageDom.SelectSingleNode("//optOrgEntitiesValue").InnerText = "1";
            else if(optSome.Checked == true)
                oFDMPageDom.SelectSingleNode("//optOrgEntitiesValue").InnerText = "0";

            oFDMPageDom.SelectSingleNode("//SelEntitiesListItems").InnerText = SelEntitiesListItems.Value;
            oFDMPageDom.SelectSingleNode("//IsClose").InnerText = "1";
            oFDMPageDom.SelectSingleNode("//GroupName").InnerText = txtGroupName.Value;
            // Nadim changed for defer check box
            if (ViewEnhanceBES.Value == "yes")
            {
                    if (DBType.Value == "4")
                    {
                        if(defer1.Checked ==true)
                            oFDMPageDom.SelectSingleNode("//Defer").InnerText = "1";
                    }
                
            }
            if (hdnVisited2stPage.Value != "")
            {
                oFDMPageDom.SelectSingleNode("//AdjusterListItems").InnerText = AdjusterListItems.Value;
                if(optAllAdjTextTypes.Checked ==true)
                    oFDMPageDom.SelectSingleNode("//optAdjTextTypesValue").InnerText = "1";
                else if(optSomeAdjTextTypes.Checked ==true)
                    oFDMPageDom.SelectSingleNode("//optAdjTextTypesValue").InnerText = "2";
                else if(optExceptAdjTextTypes.Checked ==true)
                    oFDMPageDom.SelectSingleNode("//optAdjTextTypesValue").InnerText = "3";
              //nadim changed for defer check box
                //if (ViewEnhanceBES.Value == "yes")
                //{
                //    if (rowId.Value == "")
                //    {
                //        if (DBType.Value == "4")
                //        {
                //            if (defer2.Checked == true)
                //                oFDMPageDom.SelectSingleNode("//Defer").InnerText = "1";
                //        }
                //    }
                //}
                //nadim changed for defer check box
            }
            if (hdnVisited3stPage.Value != "")
            {
                //Nadim for Rem brokers and insurers
                //oFDMPageDom.SelectSingleNode("//InsurersListItems").InnerText = InsurersListItems.Value;
                //oFDMPageDom.SelectSingleNode("//BrokersListItems").InnerText = BrokersListItems.Value;
                oFDMPageDom.SelectSingleNode("//InsurersListItems").InnerText = InsurersListItems3.Value;
                oFDMPageDom.SelectSingleNode("//BrokersListItems").InnerText = BrokersListItems2.Value;
                //Nadim for Rem brokers and insurers
                if(optAllBrokers.Checked ==true)
                        oFDMPageDom.SelectSingleNode("//optBrokersValue").InnerText = "1";
                else if(optSomeBrokers.Checked ==true)
                        oFDMPageDom.SelectSingleNode("//optBrokersValue").InnerText = "2";
                if (optAllInsurers.Checked == true)
                    oFDMPageDom.SelectSingleNode("//optInsurersValue").InnerText = "1";
                else if (optSomeInsurers.Checked == true)
                    oFDMPageDom.SelectSingleNode("//optInsurersValue").InnerText = "2";
                //nadim changed for defer check box
                //  if (ViewEnhanceBES.Value == "yes")
                //{
                //    if (rowId.Value == "")
                //    {
                //        if (DBType.Value == "4")
                //        {
                //            if (defer3.Checked == true)
                //                oFDMPageDom.SelectSingleNode("//Defer").InnerText = "1";
                //        }
                //    }
                //}
                //nadim changed for defer check box
            }
            if (hdnVisited4stPage.Value != "")
            {
                oFDMPageDom.SelectSingleNode("//UserListItems").InnerText = UserListItems.Value;
                oFDMPageDom.SelectSingleNode("//OveridePassFlag").InnerText = hidOveridePassFlag.Value;
                if (IsOptAll.Value == "0")
                {
                    //pmittal5 - Password change functionality in case of SQL also.
                    //if (DBType.Value == "4")
				if(ConfRec.Value == "0")
                    {
                        if (rowId.Value != "")
                        {
                            oFDMPageDom.SelectSingleNode("//GroupId").InnerText = txtGroupId.Value;
                        }
                        if (chkOveridePass.Checked == true)
                        {
                            oFDMPageDom.SelectSingleNode("//PassWord").InnerText = txtPass.Value;
                        }
                    }
                }
                //nadim changed for defer check box
                //if (ViewEnhanceBES.Value == "yes")
                //{
                //    if (rowId.Value == "")
                //    {
                //        if (DBType.Value == "4")
                //        {
                //            if (defer4.Checked == true)
                //                oFDMPageDom.SelectSingleNode("//Defer").InnerText = "1";
                //        }
                //    }
                //}
                //nadim changed for defer check box
            }
            if (hdnVisited6stPage.Value != "")
            {
                oFDMPageDom.SelectSingleNode("//NotesListItems").InnerText = NotesListItems.Value;
                if (optAllEnhanceNoteTypes.Checked == true)
                    oFDMPageDom.SelectSingleNode("//optEnhNoteTypesValue").InnerText = "1";
                else if (optSomeEnhanceNoteTypes.Checked == true)
                    oFDMPageDom.SelectSingleNode("//optEnhNoteTypesValue").InnerText = "2";
                else if (optExceptEnhanceNoteTypes.Checked == true)
                    oFDMPageDom.SelectSingleNode("//optEnhNoteTypesValue").InnerText = "3";
            }
            //Boeing
            if (hdnUseBrokerBes.Value != "" || hdnUseBrokerBes.Value != null)
            {
                if (hdnVisited5stPage.Value != "")
                {
                    oFDMPageDom.SelectSingleNode("//BrokersListItems").InnerText = BrokerFirmList.Value;
                    if (optAllBrokers1.Checked == true)
                        oFDMPageDom.SelectSingleNode("//optBrokersValue").InnerText = "1";
                    else if (optSomeBrokers1.Checked == true)
                        oFDMPageDom.SelectSingleNode("//optBrokersValue").InnerText = "2";
                  
                }
            }
            string function;
            if (AppHelper.GetQueryStringValue("selectedid") == "")
                function = "<Function>ORGSECAdaptor.SaveGroupDef</Function>";
            else
                function = "<Function>ORGSECAdaptor.UpdateGroupDef</Function>";
            XElement oMessageElement = XElement.Parse(@"<Message>
                                    <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                                    <Call>
                                    " + function + " </Call>" + oFDMPageDom.SelectSingleNode("//Document").OuterXml + "</Message>");
                     
           
            oFDMPageDom.RemoveAll();
            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
            //nadim for 14334..error comes in case of duplicate user
                XmlDocument xmlReturnErrorTest = new XmlDocument();
                xmlReturnErrorTest.LoadXml(sReturn);
                if (xmlReturnErrorTest.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {
                    lblerrorMessage.Text = xmlReturnErrorTest.SelectSingleNode("//MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                    //nadim for 16299
                    setTitle(); 
                    //nadim for 16299
                    return;
                    //csm.RegisterClientScriptBlock(this.GetType(), "ee", "<Script>alert('Userexist');</Script>");
                }
                else
                {                    
                    ClientScriptManager csm = Page.ClientScript;
                    csm.RegisterClientScriptBlock(this.GetType(), "ee", "<Script>window.opener.document.location.reload(true);window.close();</Script>");
                }
            //Nadim for 14334
            //Deb
            foreach (DictionaryEntry entry in Cache)
            {
               if (((string)(entry.Key)).StartsWith("RetOrgXml"))
               {
                 Cache.Remove(entry.Key.ToString());
               }
            }
            //Deb
        }
        public void DisableNextButton()
        {
            if (DBType.Value == "6" && IsOptAll.Value == "0")
                ((Button)Wizardlist.FindControl("StepNavigationTemplateContainerID").FindControl("StepNextButton")).Enabled = true;
            else
                ((Button)Wizardlist.FindControl("StepNavigationTemplateContainerID").FindControl("StepNextButton")).Enabled = false;
        }
        //nadim for 16299
        public void setTitle()
        {
            if (Request.QueryString["mode"] != null)
            {
                if (Request.QueryString["mode"] == "edit")
                {
                    Header.Title = "Edit Security Group (Data View Group Definition)";
                }
                else
                {
                    Header.Title = "New Security Group (Data View Group Definition)";
                }
            }
        }
        //nadim for 16299
          
        public void MaintainListBox()
        {
            ListBox lst =new ListBox();
            HtmlInputHidden hdnCtrl =new HtmlInputHidden();
            bool flag = false;
            if (Request.Form["Wizardlist$index0"] != "" && Request.Form["Wizardlist$index0"] != null)
            {
                hdnCtrl = hdnOrg;
                lst = Org;
                flag = true;
            }
            else if (Request.Form["Wizardlist$index1"] != "" && Request.Form["Wizardlist$index1"] != null)
            {
                hdnCtrl = hdnlstAdjTextTypes;
                lst = ((ListBox)lstAdjTextTypes.FindControl("multicode"));
                flag = true;
            }
            else if (Request.Form["Wizardlist$index2"] != "" && Request.Form["Wizardlist$index2"] != null)
            {
                flag = true;
                //Nadim for Rem brokers and insurers
                //lst = ((ListBox)lstBrokers.FindControl("multicode"));
                lst = lstBrokerFirm2;
                //Nadim for Rem brokers and insurers
                hdnCtrl=hdnlstBrokerFirms2;
                FillListBox(flag, ref lst, ref hdnCtrl);
                hdnCtrl = hdnlstInsurerFirms3;
                //Nadim for Rem brokers and insurers
                //lst = ((ListBox)lstInsurers.FindControl("multicode"));
                lst = lstInsurerFirm3;
                FillListBox(flag, ref lst, ref hdnCtrl);
                //Nadim for Rem brokers and insurers
            }
            else if (Request.Form["Wizardlist$index3"] != "" && Request.Form["Wizardlist$index3"] != null)
            {
                hdnCtrl = hdnlstUsers;
                lst = lstUsers;
                flag = true;
            }
            else if (Request.Form["Wizardlist$index4"] != "" && Request.Form["Wizardlist$index4"] != null)//Boeing
            {
                flag = true;
                lst = lstBrokerFirm1;//broker changes
                hdnCtrl = hdnlstBrokerFirms;
               
            }
            else if (Request.Form["Wizardlist$index5"] != "" && Request.Form["Wizardlist$index5"] != null)
            {
                hdnCtrl = hdnlstEnhanceNoteTypes;
                lst = ((ListBox)lstEnhanceNoteTypes.FindControl("multicode"));
                flag = true;
            }
            FillListBox(flag,ref lst,ref hdnCtrl);
        }
        public void FillListBox(bool flag,ref ListBox lst,ref HtmlInputHidden hdnCtrl)
        {
            if (flag)
            {
                lst.Items.Clear();
                char[] delimiterChars = { '|' };
                string[] vals = hdnCtrl.Value.Split(delimiterChars);
                if (vals.Length > 0)
                {
                    for (int x = 0; x < vals.Length; x++)
                    {
                        if (vals[x].IndexOf('^') != -1)//Parijat: 15583 & 15581
                        {
                            string text = vals[x].Substring(0, (vals[x].IndexOf('^')));//Parijat: 15583 & 15581
                            string value = vals[x].Substring(vals[x].IndexOf("^") + 1);//Parijat: 15583 & 15581
                            ListItem ls = new ListItem(text, value);
                            lst.Items.Add(ls);
                        }
                    }
                }
            }
        }
    }
}
