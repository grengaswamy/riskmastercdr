﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Text;

//namespace Riskmaster.UI.Shared.Controls
namespace Riskmaster.UI.Utilities.SSO
{

    public partial class SSOC_LDAPConfig : System.Web.UI.UserControl
    {

        //public bool IsAuthEnabled
        //{
        //    get
        //    {   
        //        System.Web.UI.WebControls.CheckBox Enabled = (System.Web.UI.WebControls.CheckBox) frmLDAPConfig.FindControl("chkEnabled");
        //        return Enabled.Checked;
        //    }
        //} // property IsEnabled

        public System.Web.UI.WebControls.CheckBox Enabled
        {
            get
            {
                return (System.Web.UI.WebControls.CheckBox)frmLDAPConfig.FindControl("chkEnabled");
            }
        }

        public string Hostname
        {
            get
            {
                System.Web.UI.WebControls.TextBox txtHostname = (System.Web.UI.WebControls.TextBox)frmLDAPConfig.FindControl("txtHostName");
                return txtHostname.Text.ToString();
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            if (! Page.IsPostBack)
            {
                //BindLDAPServerTypes();
            }//if
        }

        ///// <summary>
        ///// Gets whether or not LDAP authentication has been enabled
        ///// </summary>
        //public bool IsEnabled
        //{
        //    get
        //    {
        //        bool blnChecked = false;

        //        CheckBox chkEnabled = frmLDAPConfig.FindControl("chkEnabled") as CheckBox;

        //        blnChecked = chkEnabled.Checked;

        //        return blnChecked;
        //    }
        //} // property IsEnabled


        /// <summary>
        /// Binds the LDAP Server types to the DropDownList
        /// </summary>
        protected void BindLDAPServerTypes()
        {
            Dictionary<string, string> dictServerTypes = new Dictionary<string, string>();

            dictServerTypes.Add("lds", "Lotus Domino Server");
            dictServerTypes.Add("rhds", "Redhat Directory Server");
            dictServerTypes.Add("olds", "OpenLDAP Directory Server");

            //ddlLDAPServerType.DataSource = dictServerTypes;
            //ddlLDAPServerType.DataValueField = "key";
            //ddlLDAPServerType.DataTextField = "value";
            //ddlLDAPServerType.DataBind();

        }

        protected void chkEnabled_CheckedChanged(object sender, EventArgs e)
        {
            SSOConfig myParent = (SSOConfig)Parent.Page;
            SSOC_WindowsADConfig ucAD = myParent.WindowsADConfig;
            
            if(this.Enabled.Checked && ucAD.Enabled != null && ucAD.Enabled.Checked) 
            {
                StringBuilder sWarning = new StringBuilder();
                
                sWarning.Append("<script>window.alert('By choosing to enable \\'");
                sWarning.Append(Hostname);
                sWarning.Append("\\' you will be disabling \\'");
                sWarning.Append(ucAD.Hostname);
                sWarning.Append("\\'');");
                sWarning.Append("</script>");

                myParent.RegisterStartupScript("SourceWarning", sWarning.ToString());

                ucAD.Enabled.Checked = false;

            } // if


        }


    }

}