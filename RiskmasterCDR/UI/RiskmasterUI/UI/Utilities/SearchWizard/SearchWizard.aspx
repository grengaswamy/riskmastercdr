﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="SearchWizard.aspx.cs" Inherits="Riskmaster.UI.Utilities.SearchWizard.SearchWizard" ValidateRequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Query Wizard</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css">

    <script language="JavaScript" src="../../../Scripts/form.js">
    </script>

    <script language="JavaScript" src="../../../Scripts/query_design.js">
    </script>

</head>

<script type="text/javascript" language="javascript">
</script>

<body onload="Javascript:return ToggleAllUsers();">
    <form id="frmData" runat="server" >
    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:HiddenField ID="OriginalType" runat="server" Value="" />
    <input type="hidden" name="$node^9" value="" id="Label2" runat="server" />
    <input type="hidden" name="$node^9" value="" id="hdnActiveIndex" runat="server" />
    <input type="hidden" name="$node^9" value="" id="hdnAdminTrackingFlag" runat="server" /><%--Parijat:15084--%>
    <input type="hidden" name="$node^9" value="" id="hdnEditModeFlag" runat="server" />
    <input type="hidden" name="$node^9" value="" id="hdnAdminSysName" runat="server" /><%--Parijat:14905 regarding showing of data incase of Admintracking in edit mode--%>
    <input type="hidden" name="$node^9" value="" id="hdnDefaultChkboxValue" runat="server" />
    <asp:TextBox rmxref="/Instance/Document/EventId" ID="eventid" Style="display: none"
        runat="server" />
    <asp:TextBox rmxref="/Instance/Document/PitTracking" ID="pitracking" Style="display: none"
        runat="server" />
    <asp:TextBox rmxref="/Instance/Document/Recommend" ID="recommend" Style="display: none"
        runat="server" />
    <asp:TextBox rmxref="/Instance/Document/RecordableFlag" ID="recordableflag" Style="display: none"
        runat="server" />
    <asp:TextBox rmxref="/Instance/Document/PanelId" ID="panelid" Style="display: none"
        runat="server" />
    <asp:TextBox rmxref="/Instance/Document/PiFlags" ID="piflags" Style="display: none"
        runat="server" />
    <asp:TextBox rmxref="/Instance/Document/PiNames" ID="pinames" Style="display: none"
        runat="server" />
    <asp:TextBox rmxref="/Instance/Document/submitflag" ID="submitflag" Style="display: none"
        runat="server" />
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:Wizard ID="Wizardlist" runat="server" DisplaySideBar="false" ActiveStepIndex="0"
            OnNextButtonClick="SkipNextStep" OnFinishButtonClick ="Save">
            <StartNavigationTemplate>
                <asp:Button class="button" ID="StepPreviousButton" disabled="true" runat="server"
                    CausesValidation="True" CommandName="MovePrevious" Text="<%$ Resources:btnStepPreviousButton %>" /> <%--nsharma202 : mits 35789 --%>
                <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext"
                    OnClientClick="return validate('Next');" Text="<%$ Resources:btnStepNextButton %>"  CausesValidation="True" /><%--abansal23 on 05/18/2009 for MITS 16614--%>
                <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="<%$ Resources:btnStepCancelButton %>" OnClientClick="Javascript:return window.close();" />
                <asp:Button class="button" ID="StepFinishButton" disabled="true" runat="server" CommandName="Finish"
                    Text="<%$ Resources:btnStepFinishButton %>" CausesValidation="True" OnClientClick="FinishClick();return false;" />
            </StartNavigationTemplate>
            <StepNavigationTemplate>
                <%--nsharma202 : mits 35789 --%>
                <asp:Button class="button" ID="StepPreviousButton" runat="server" CausesValidation="True"
                    CommandName="MovePrevious" OnClientClick="return validate('Back');" Text="<%$ Resources:btnStepPreviousButton %>"  OnClick ="StepPreviousButton_Click" /><%--bkumar33 for MITS 14509--%>
                <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext"
                    OnClientClick="return validate('Next');" Text="<%$ Resources:btnStepNextButton %>" CausesValidation="True" /><%--abansal23 on 05/18/2009 for MITS 16614--%>
                <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="<%$ Resources:btnStepCancelButton %>" OnClientClick="Javascript:return window.close();" />
                <asp:Button class="button" ID="StepFinishButton" disabled="true" runat="server" CommandName="Finish"
                    Text="<%$ Resources:btnStepFinishButton %>" CausesValidation="True" OnClientClick="FinishClick();return false;" />
            </StepNavigationTemplate>
            <FinishNavigationTemplate>
                <%--nsharma202 : mits 35789 --%>
                <asp:Button class="button" ID="StepPreviousButton" runat="server"
                    CausesValidation="True" CommandName="MovePrevious" Text="<%$ Resources:btnStepPreviousButton %>" />
                <asp:Button class="button" ID="StepNextButton" disabled="true" runat="server" CommandName="MoveNext"
                    Text="<%$ Resources:btnStepNextButton %>" CausesValidation="True" />
                <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="<%$ Resources:btnStepCancelButton %>" OnClientClick="Javascript:return window.close();" />
                <asp:Button class="button" ID="StepFinishButton" runat="server" CommandName="MoveComplete"
                    Text="<%$ Resources:btnStepFinishButton %>" CausesValidation="True"  OnClientClick="return SaveQuery();"/><%--OnClientClick="FinishClick();return false;"--%>
            </FinishNavigationTemplate>
            <WizardSteps>
                <asp:WizardStep ID="WizardStep1" runat="server" Title="Search Query Wizard - Step 1 of 4 (Basic Information)">
                    <table>
                        <tr class="ctrlgroup">
                            <td colspan="2">
                                <asp:label ID="lblSearchWizStep1" runat="server" Text="<%$ Resources:lblSearchWizStep1 %>"></asp:label>                              
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                                <input type="hidden" name="$node^9" value="" id="index0" />
                            </td>
                        </tr>
                        <tr height="100px">
                            <td width="30%" align="center">
                                <img src="../../../Images/querywizard.jpg" alt="querywizard" />
                            </td>
                            <td valign="top" width="70%">
                                <asp:label ID="lblSearchInfo" runat="server" Text="<%$ Resources:lblSearchInfo %>"></asp:label>                        
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                        <tr height="250px">
                            <td valign="top" colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td nowrap="1" colspan="2">
                                            <asp:label ID="lblSearchName" runat="server" Text="<%$ Resources:lblSearchName %>"></asp:label> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20px">
                                        </td>
                                        <td>
                                            <input type="text" name="$node^50" value="ss" id="searchname" size="50" runat="server">
                                        &nbsp;</input></td>
                                    </tr>
                                    <tr>
                                        <td nowrap="1" colspan="2">
                                            <asp:label ID="lblSearchDesc" runat="server" Text="<%$ Resources:lblSearchDesc %>"></asp:label> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20px">
                                        </td>
                                        <td>
                                            <input type="text" name="$node^54" value="aa" id="searchdesc" size="50" runat="server">
                                        &nbsp;</input></td>
                                    </tr>
                                    <tr>
                                        <td nowrap="1" colspan="2">
                                            <asp:label ID="lblSearchCat" runat="server" Text="<%$ Resources:lblSearchCat %>"></asp:label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">
                                        </td>
                                        <td>
                                            <asp:DropDownList name="searchtype"  OnSelectedIndexChanged="searchtype_SelectedIndexChanged"
                                                ID="searchtype" runat="server">
                                            </asp:DropDownList>
                                            <!-- abansal23: MITS 14905 -->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep2" runat="server" Title="Search Query Wizard - Step 2 of 4 (Query Field Selection)">
                    <table>
                        <tr class="ctrlgroup">
                            <td colspan="2">
                                <asp:label ID="lblStep1a" runat="server" Text="<%$ Resources:lblStep1a %>"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                                <input type="hidden" name="$node^9" value="" id="index1" />
                            </td>
                        </tr>
                        <tr height="100px">
                            <td width="30%" align="center">
                                <img src="../../../Images/querywizard.jpg">
                            </td>
                            <td valign="top" width="70%">
                                <asp:label ID="lblAdminTrackTable" runat="server" Text="<%$ Resources:lblAdminTrackTable %>"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                        <tr height="250px">
                            <td valign="top" colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td nowrap="1" colspan="2">
                                            <asp:label ID="lblAdminTrackSearch" runat="server" Text="<%$ Resources:lblAdminTrackSearch %>"></asp:label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20px">
                                        </td>
                                        <td>
                                            <asp:DropDownList name="admtrcktable"  OnSelectedIndexChanged="admtrcktable_SelectedIndexChanged"
                                                ID="admtrcktable" runat="server">
                                            </asp:DropDownList>
                                            <!-- abansal23: MITS 14905 -->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep3" runat="server" Title="OSHA Recordability Wizard -  Step 3">
                    <table>
                        <tr class="ctrlgroup">
                            <td colspan="2">
                                <asp:label ID="lblStep2of4" runat="server" Text="<%$ Resources:lblStep2of4 %>"></asp:label>
                            </td>
                            <%--<%=(Wizardlist.ActiveStepIndex)+1%>--%>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                                <input type="hidden" name="$node^9" value="" id="FieldDisplay" runat="server" />
                                <input type="hidden" name="$node^9" value="" id="hdnlist" runat="server" />
                                <input type="hidden" name="$node^9" value="" id="hdnSelectedFields" runat="server" />
                                <input type="hidden" name="$node^9" value="" id="hdnFirstAccess2" runat="server" />
                                <input type="hidden" name="$node^9" value="" id="index2" />
                            </td>
                        </tr>
                        <tr height="100px">
                            <td width="30%" align="center">
                                <img src="../../../Images/querywizard.jpg">
                            </td>
                            <td valign="top" width="70%">
                                <asp:label ID="lblSearchFields" runat="server" Text="<%$ Resources:lblSearchFields %>"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                        <tr height="250px">
                            <td valign="top" colspan="2">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td colspan="3">
                                                    <asp:CheckBox ID="defCheckBoxStatus" runat="server" />
                                                    <asp:label runat="server" ID="lblVoidFlag" Text="<%$ Resources:lblVoidFlag %>"></asp:label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <hr>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="40%">
                                                    <asp:label ID="lblFieldsAvSearch" runat="server" Text="<%$ Resources:lblFieldsAvSearch %>"></asp:label>
                                                </td>
                                                <td width="20%">
                                                </td>
                                                <td width="40%">
                                                    <asp:label ID="lblFieldsInSearch" runat="server" Text="<%$ Resources:lblFieldsInSearch %>"></asp:label>
                                                     &nbsp;
                                                    <input type="image" name="$actionImg^" src="../../../Images/prop.jpg" alt="" class="button"
                                                        onclick="Javascript:return ShowProp();">
                                                    &nbsp;
                                                    <input type="image" name="$actionImg^" src="../../../Images/up1.gif" alt="" class="button"
                                                        onclick="Javascript:return MoveUp();">
                                                    &nbsp;
                                                    <input type="image" name="$actionImg^" src="../../../Images/down1.gif" alt="" class="button"
                                                        onclick="Javascript:return MoveDown();">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="40%" valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                                                        <ContentTemplate>
                                                            <asp:DropDownList name="$node^128" ID="searchcats" runat="server" OnSelectedIndexChanged="LoadFieldCategory"
                                                                AutoPostBack="true">
                                                                <%--onchange="Javascript:return LoadFieldCategory();"--%>
                                                            </asp:DropDownList>
                                                            <%--<option value="" selected>Select a Category</option>--%>
                                                            <br>
                                                            <asp:ListBox name="$node^164" id="fields" style="width: 162px"  Rows="9" ondblclick="AddSingle(this);"
                                                                SelectionMode="Multiple"  runat="server"><%-- abansal23: MITS 16525 on 06/22/2009 --%>                                                            </asp:ListBox><%--<option value="" selected>No Category Selected</option>--%>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td width="20%" valign="middle">
                                                    <br>
                                                    <br>
                                                    <asp:Button name="$action^" Text=" &gt;&gt; " ID="addField" class="button" OnClick="AddSelected"
                                                        runat="server" /><br>
                                                    <%--onclick="Javascript:return AddSelected();"--%>
                                                    <br>
                                                    <asp:Button name="$action^" Text=" << " class="button" ID="removeField" OnClick="RemoveSelected"
                                                        runat="server" /><%--onclick="Javascript:return RemoveSelected();"--%>
                                                </td>
                                                <td width="40%">
                                                    <asp:ListBox name="$node^133" id="selectedfields" multiple="true" style="width: 162px" Rows="9"
                                                        ondblclick="Javascript:return RemoveSingle(this);"
                                                        SelectionMode="Multiple"  size="10" runat="server"><%-- abansal23: MITS 16525 on 06/22/2009 --%>
                                                    </asp:ListBox>
                                                </td>
                                                <%--<option value="60043_Reviewed By Abbrev._0_1">Reviewed By Abbrev.</option>--%>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep4" runat="server" Title="OSHA Recordability Wizard -  Step 4">
                    <table>
                        <tr class="ctrlgroup">
                            <td colspan="2">
                                <asp:label ID="lblStep3of4" runat="server" Text="<%$ Resources:lblStep3of4 %>"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                                <input type="hidden" name="$node^9" value="" id="index3" />
                                <input type="hidden" name="$node^9" value="" id="hdnFirstAccess4" runat="server" />
                                <input type="hidden" name="$node^9" value="" id="hdnlist1" runat="server" />
                                <input type="hidden" name="$node^9" value="" id="hdnSelectedFields1" runat="server" />
                            </td>
                        </tr>
                        <tr height="100px">
                            <td width="30%" align="center">
                                <img src="../../../Images/querywizard.jpg">
                            </td>
                            <td valign="top" width="70%">
                                <asp:label ID="lblSearchDispResult" runat="server" Text="<%$ Resources:lblSearchDispResult %>"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                        <tr height="250px">
                            <td valign="top" colspan="2">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td width="40%">
                                                    <asp:label ID="lblFieldsAvDisplay" runat="server" Text="<%$ Resources:lblFieldsAvDisplay %>"></asp:label>  
                                                </td>
                                                <td width="20%">
                                                </td>
                                                <td width="40%">
                                                    <asp:label ID="lblFieldsToDisplay" runat="server" Text="<%$ Resources:lblFieldsToDisplay %>"></asp:label>  
                                                    &nbsp;
                                                    <input type="image" name="$actionImg^" src="../../../Images/up1.gif" alt="" class="button"
                                                        onclick="Javascript:return MoveUp();">
                                                    &nbsp;
                                                    <input type="image" name="$actionImg^" src="../../../Images/down1.gif" alt="" class="button"
                                                        onclick="Javascript:return MoveDown();">
                                                </td>
                                            </tr>
                                            <tr>
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <td width="40%" valign="top">
                                                            <asp:DropDownList name="$node^147" ID="searchcats1" OnSelectedIndexChanged="LoadFieldCategory"
                                                                runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                            <br>
                                                            <%--onchange="Javascript:return LoadFieldCategory();"<option value="" selected>Select a Category</option>--%>
                                                            <asp:ListBox name="$node^164" id="fields1" style="width: 162px" Rows="9" ondblclick="AddSingle(this);"
                                                               SelectionMode="Multiple" runat="server"><%-- abansal23" MITS 16525 on 06/22/2009 --%>
                                                            </asp:ListBox>
                                                        </td>
                                                        <td width="20%" valign="middle">
                                                            <br>
                                                            <br>
                                                            <asp:Button name="$action^" ID="addField1" Text=" &gt;&gt; " class="button" runat="server"
                                                                OnClick="AddSelected" /><br>
                                                            <%--onclick="Javascript:return AddSelected();"--%>
                                                            <br>
                                                            <%--onclick="Javascript:return RemoveSelected();"--%>
                                                            <asp:Button Text=" << " class="button" ID="removeField2" OnClick="RemoveSelected"
                                                                runat="server" />
                                                        </td>
                                                        <td width="40%">
                                                            <asp:ListBox name="$node^151" id="selectedfields1" multiple="true" style="width: 162px"
                                                                ondblclick="Javascript:return RemoveSingle(this);"
                                                                SelectionMode="Multiple" Rows="9" runat="server">
                                                            </asp:ListBox>
                                                        </td>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep5" runat="server" Title="OSHA Recordability Wizard - Finished">
                    <table>
                        <tr class="ctrlgroup">
                            <td colspan="2">
                                <asp:label ID="lblStep4of4" runat="server" Text="<%$ Resources:lblStep4of4 %>"></asp:label>  
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                                <input type="hidden" name="$node^9" value="" id="index4" />
                                <input type="hidden" name="$node^9" value="" id="hdnFirstAccess5" runat="server" />
                                <input type="hidden" name="$node^9" value="" id="hdnlist2" runat="server" />
                                <input type="hidden" name="$node^9" value="" id="hdnSelectedFields2" runat="server" />
                            </td>
                        </tr>
                        <tr height="100px">
                            <td width="30%" align="center">
                                <img src="../../../Images/querywizard.jpg">
                            </td>
                            <td valign="top" width="70%">
                                <asp:label ID="lblUserGroups" runat="server" Text="<%$ Resources:lblUserGroups %>"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                        <tr height="250px">
                            <td valign="top" colspan="2">
                             <asp:UpdatePanel ID="UpdatePanel5" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td colspan="3">
                                            <input type="radio" name="$node^168" value="AllUsers" onclick="ToggleAllUsers();"
                                                id="allowall" runat="server"><asp:label ID="lblAllUsers" runat="server" Text="<%$ Resources:lblAllUsers %>"></asp:label><br>
                                            <input type="radio" name="$node^168" value="SelectUsers" onclick="ToggleAllUsers();"
                                                id="selectusers" runat="server"><asp:label ID="lblSelectUsers" runat="server" Text="<%$ Resources:lblSelectUsers %>"></asp:label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <hr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:label ID="lblAvUsersGroups" runat="server" Text="<%$ Resources:lblAvUsersGroups %>"></asp:label>
                                            <br>
                                            <asp:ListBox name="$node^176" id="usersgroups" ondblclick="AddSingle(this.options[this.selectedIndex]);"
                                                SelectionMode="Multiple" style="width: 162px" Rows="8" runat = "server">
                                                
                                            </asp:ListBox><br>
                                            <font size="-2">
                                                <asp:label ID="lblSignifiesUser" runat="server" Text="<%$ Resources:lblSignifiesUser %>"></asp:label>
                                            </font>
                                        </td>
                                        <td width="20%" valign="middle">
                                            <br>
                                            <br>
                                          <%--onclick="Javascript:return AddSelectedUserGroup();"--%>
                                            
                                            <%--onclick="Javascript:return RemoveSelected();"--%>
                                             <asp:Button name="$action^" ID="btnAdd1" Text=" &gt;&gt; " class="button" runat="server"
                                                            OnClick="AddSelected" />
                                             <br>
                                             <br>
                                             <asp:Button Text=" << " class="button" ID="btnRemove1" OnClick="RemoveSelected"
                                                     runat="server"  />
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:label ID="lblSelectedUserGroups" runat="server" Text="<%$ Resources:lblSelectedUserGroups %>"></asp:label>
                                            <br>
                                            <asp:ListBox name="$node^184"  id="selectedfields2" ondblclick="Javascript:return RemoveSingle(this);"
                                                 SelectionMode="Multiple"  style="width: 162px" Rows="8" runat= "server"  >
                                               
                                            </asp:ListBox>
                                        </td>
                                    </tr>
                                </table>
                                 </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
            </WizardSteps>
        </asp:Wizard>
    </div>
    <%--Bijender has changed for Mits 14530--%>
    <asp:TextBox runat ="server" ID="hdnsenttoprop" style="display :none" ></asp:TextBox>
    <%--Bijender End Mits 14530--%>
    <asp:TextBox runat="server" ID="hdnIsEdit" EnableViewState="true" Style="display: none"></asp:TextBox> <%--abansal23: MITS 16614 on 05/19/2009 --%>
    </form>
</body>
</html>
