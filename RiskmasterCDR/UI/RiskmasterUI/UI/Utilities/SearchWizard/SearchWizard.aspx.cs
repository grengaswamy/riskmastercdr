﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

using System.ServiceModel;
using Riskmaster.UI.Shared;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Utilities.SearchWizard
{
    public partial class SearchWizard : System.Web.UI.Page
    {
        private bool m_IsEdit = false;//abansal23: MITS 16614 on 05/19/2009
        private string sPageID = RMXResourceProvider.PageId("SearchWizard.aspx");
        private string sSearchPageID = RMXResourceProvider.PageId("SearchLocalization.aspx");
        public string sLangId = AppHelper.GetLanguageCode();              
       
        protected void Page_Load(object sender, EventArgs e)
        {
            string  clientArgument = string.Empty;
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("SearchWizard.aspx"), "SearchWizardValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "SearchWizardValidations", sValidationResources, true);
            if (!Page.IsPostBack)
            {
                XmlDocument oFDMPageDom = new XmlDocument();
                string sReturn = "";
                //Preparing XML to send to service
                XElement oMessageElement = LoadSearchWizardTemplate();
                //Modify XML 
                XElement oViewID = oMessageElement.XPathSelectElement("./Document/SearchWizard/ViewID");
                if (oViewID != null)
                {
                    if (Request.QueryString["selectedid"] != null)
                    {
                        oViewID.Value = Request.QueryString["selectedid"];
                        hdnEditModeFlag.Value = "1";
                    }
                    else
                    {
                        hdnEditModeFlag.Value = "0";
                    }
                }
                XElement oLangId = oMessageElement.XPathSelectElement("./Document/SearchWizard/LangId");
                if (oLangId != null)
                {
                    oLangId.Value = sLangId;
                }
                XElement PageId = oMessageElement.XPathSelectElement("./Document/SearchWizard/PageId");
                if(PageId != null)
                {
                    PageId.Value = sSearchPageID;
                }
                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                if (oFDMPageDom.SelectSingleNode("//SearchName") != null)
                {
                    searchname.Value = oFDMPageDom.SelectSingleNode("//SearchName").InnerText;
                }
                //Parijat :14905 Admin Tracking sysformname's value to be maintained for next step
                if (oFDMPageDom.SelectSingleNode("//AdmTable") != null)
                {
                    hdnAdminSysName.Value = oFDMPageDom.SelectSingleNode("//AdmTable/@sysname").Value;
                }
                if (oFDMPageDom.SelectSingleNode("//SearchDesc") != null)
                {
                    searchdesc.Value = oFDMPageDom.SelectSingleNode("//SearchDesc").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//DefaultChkboxValue") != null)
                {
                    hdnDefaultChkboxValue.Value = oFDMPageDom.SelectSingleNode("//DefaultChkboxValue").InnerText;
                }
                if (oFDMPageDom.SelectSingleNode("//SearchType") != null)
                {
                    DataSet aDataSet = new DataSet();
                    aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("SearchType")[0].OuterXml));
                    
                    //Mits id:32662- Fixed by govind - Start

                    //get the dataview of table "Country", which is default table name'
                    DataView dvSorting=new DataView();
                    dvSorting=aDataSet.Tables["level"].DefaultView;

                    dvSorting.Sort="name";
     
                    //now bind the dropdownlist to the dataview'
                    searchtype.DataSource = dvSorting;
     
                    
                    //searchtype.DataSource = aDataSet.Tables["level"];
                    //Mits id:32662- Fixed by govind - End

                    searchtype.DataTextField = "name";
                    searchtype.DataValueField = "id";
                    
                    searchtype.DataBind();
                    if (Dropdown_Selection_value(aDataSet.Tables["level"], "selected", "1", "id") != "")
                    {
                        searchtype.SelectedValue = Dropdown_Selection_value(aDataSet.Tables["level"], "selected", "1", "id");
                        OriginalType.Value = searchtype.SelectedValue;
                    }
                    Label2.Value = searchtype.SelectedValue;
                    //abansal23: MITS 14905 Starts
                    if (ViewState["SearchType"] == null)
                    {
                        ViewState["SearchType"] = searchtype.SelectedValue;
                    }
                    //abansal23: MITS 14905 Ends
                    //searchtype.SelectedIndex = aDataSet.Tables["level"].Rows.IndexOf(aDataSet.Tables["level"].Rows.Find("1"));
                    
                }

            }

            clientArgument = Request.Params.Get("__EVENTARGUMENT");

            if (!string.IsNullOrEmpty(clientArgument))
            {

                if (clientArgument.Contains("DoubleClickedMode") && clientArgument.Contains("index"))
                {
                    string[] arrArg;
                    arrArg = clientArgument.Split(';');

                    string[] arrDoubleClickMode;
                    arrDoubleClickMode = arrArg[0].Split('=');
                    string[] arrControlIndex;
                    arrControlIndex = arrArg[1].Split('=');
                    UpdateControlsOnDoubleClick(arrDoubleClickMode[1], arrControlIndex[1]);
                }
            }
           
        }

        /// <summary>
        /// This Functions updates the Listbox Present on the Left and right hand side.
        /// </summary>
        /// <param name="sControlName"> The Listbox where the double click is done. It has to vales From and To
        /// If the Value is From, then the Selected item from the Left Hand Side is added to the Listbox at Right
        /// hand side. If the value is To, then the Selected Item is removed from the Right hande side Listbox.
        /// </param>
        /// <param name="sControlIndex"> The Page where Double click is done. 
        /// If it is 2, then the Page is "Fields Available for Search",
        /// If it is 3, then the Page is "Fields Available for Display",
        /// If it is 4, then the page is "Query Use Permissions"</param>
        private void UpdateControlsOnDoubleClick(string sControlName, string sControlIndex)
        {
            if (sControlName == "From")
            {
                ListBox select1;
                ListBox select2;
                if (sControlIndex == "2")
                {
                    select1 = (ListBox)fields;
                    select2 = (ListBox)selectedfields;
                }
                else if (sControlIndex == "3")
                {
                    select1 = (ListBox)fields1;
                    select2 = (ListBox)selectedfields1;
                }
                else
                {
                    select1 = (ListBox)usersgroups;
                    select2 = (ListBox)selectedfields2;
                }

                if (select1.SelectedIndex != -1)
                {
                    ListItem item = select1.Items[select1.SelectedIndex];
                    if (item != null)
                    {
                        if (select2.Items.IndexOf(item) == -1)
                        {
                            select2.Items.Add(item);
                            select2.SelectedIndex = select2.Items.IndexOf(item);
                        }
                    }
                }
            }
            else if (sControlName == "To")
            {
                if (sControlIndex == "2")
                {
                    if (selectedfields.SelectedIndex != -1)
                    {
                        ListItem item = selectedfields.Items[selectedfields.SelectedIndex];
                        if (item != null)
                            selectedfields.Items.Remove(item);
                    }
                }
                else if (sControlIndex == "3")
                {
                    if (selectedfields1.SelectedIndex != -1)
                    {
                        ListItem item = selectedfields1.Items[selectedfields1.SelectedIndex];
                        if (item != null)
                            selectedfields1.Items.Remove(item);
                    }
                }
                else if (sControlIndex == "4")
                {
                    if (selectedfields2.SelectedIndex != -1)
                    {
                        ListItem item = selectedfields2.Items[selectedfields2.SelectedIndex];
                        if (item != null)
                            selectedfields2.Items.Remove(item);
                    }
                }
            }
        }
        /// <summary>
        /// This function returns the dropdown selcted value from a datatable where a column acts as a selection indicator
        /// </summary>
        /// <param name="dt">datatable</param>
        /// <param name="colNameContainer">Column that contains the value to be found for selection</param>
        /// <param name="valueToBeCompared">value which has to be compared</param>
        /// <param name="colNameUsedAsValue">Column which has been binded to the dropdown </param>
        /// <returns></returns>
        protected string Dropdown_Selection_value(DataTable dt,string colNameContainer,string valueToBeCompared,string colNameUsedAsValue)
        {
            DataRow[] dr = dt.Select(colNameContainer + "='" + valueToBeCompared + "'");
            if (dr.Length == 1)
            {
                DataRow dr1 = dr[0];
                //return dt.Rows.IndexOf(dr1[]);
                return dr1[colNameUsedAsValue].ToString();
            }
            return "";
        }
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }
        /// <summary>
        /// CWS request message template for SearchWizard
        /// </summary>
        /// <returns></returns>
        private XElement LoadSearchWizardTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>SearchWizardAdaptor.GetStep1</Function> 
                      </Call>
                      <Document>
                          <SearchWizard>
                                <ViewID></ViewID> 
                                <SearchName></SearchName> 
                                <SearchDesc></SearchDesc>
                                <DefaultChkboxValue></DefaultChkboxValue>
                                <AdmTable sysname=''></AdmTable> 
                                <AdmTableSelectedId></AdmTableSelectedId>
                                <Label3></Label3>         
                                <SearchType></SearchType> 
                                <SelectedSearchTypeId></SelectedSearchTypeId> 
                                    <OriginalType></OriginalType>
                                    <Label2></Label2> 
                                    <ChkDisplay>False</ChkDisplay> 
                                    <FldsAvlSrch></FldsAvlSrch> 
                                    <FldsAvlDisp></FldsAvlDisp> 
                                        <FldsToSrch></FldsToSrch> 
                                        <SrchList></SrchList> 
                                        <FldsToDisp></FldsToDisp> 
                                    <DispList></DispList> 
                                    <AllUsersSelected></AllUsersSelected> 
                                    <AllUsers></AllUsers>
                                    <SelectUsersSelected></SelectUsersSelected>
                                    <Users><listrow /></Users> 
                                    <Groups><listrow /></Groups>
                                    <UsersPermitted><listrow /></UsersPermitted>
                                    <GroupsPermitted><listrow /></GroupsPermitted>
                                    <UserGroupList></UserGroupList>
                                <Step2FirstAccess>true</Step2FirstAccess>
                                <Step3FirstAccess>true</Step3FirstAccess>
                                <Step4FirstAccess>true</Step4FirstAccess>
                                <Step5FirstAccess>true</Step5FirstAccess>
                                <LangId></LangId>  
                                <PageId></PageId>                              
                          </SearchWizard>
                      </Document>
                </Message>
            ");
            return oTemplate;
        }

        //abansal23 :  MITS 14905 on 4/15/2009 Starts
        protected void searchtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ViewState["SearchType"] == null)
            {
                ViewState["SearchType"] = searchtype.SelectedValue;
            }
            else if (ViewState["SearchType"].ToString() != searchtype.SelectedValue)
            {
                fields.Items.Clear();
                selectedfields.Items.Clear();
                fields1.Items.Clear();
                selectedfields1.Items.Clear();
                selectedfields2.Items.Clear();
                ViewState["SearchType"] = searchtype.SelectedValue;
                if(hdnEditModeFlag.Value =="1")
                    ViewState["SearchTypeChanged"] = true;//abansal23: MITS 15610 on 4/25/2009 
                else
                    ViewState["SearchTypeChanged"] = false;
            }
            else
            {
                ViewState["SearchTypeChanged"] = false;
            }
        }
               

        protected void admtrcktable_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ViewState["AdminTrac"] == null)
            {
                ViewState["AdminTrac"] = admtrcktable.SelectedValue;
            }
            else if (ViewState["AdminTrac"].ToString() != admtrcktable.SelectedValue)
            {
                fields.Items.Clear();
                selectedfields.Items.Clear();
                fields1.Items.Clear();
                selectedfields1.Items.Clear();
                selectedfields2.Items.Clear();
                ViewState["AdminTrac"] = admtrcktable.SelectedValue;
            }
        }
        //abansal23 :  MITS 14905 on 4/15/2009 Ends

        //bkumar33  :  Mits 14509 
        protected void StepPreviousButton_Click(object sender, EventArgs e)
        {
            if (WizardIndex == "1")
            {
                if (ViewState["SelectedIndex"] != null)
                {
                    if (Convert.ToInt32(ViewState["SelectedIndex"]) != admtrcktable.SelectedIndex)
                    {
                        fields.Items.Clear();
                        selectedfields.Items.Clear();
                        fields1.Items.Clear();
                        selectedfields1.Items.Clear();
                        selectedfields2.Items.Clear();
                        ViewState["AdminTrac"] = admtrcktable.SelectedValue;
                    }
                }
            }
            else
            {
                ViewState["SelectedIndex"] = admtrcktable.SelectedIndex;
            }
        }
        //bkumar33  :  Mits 14509  Ends

        public string WizardIndex
        {
            get { return Wizardlist.ActiveStepIndex.ToString(); }
        }   
        public void SkipPrevStep(object sender, WizardNavigationEventArgs e)
        {

       
            switch (e.CurrentStepIndex)
            {
                case 1:

                    Wizardlist.ActiveStepIndex = 0;
                    break;
                case 2:
                    Wizardlist.ActiveStepIndex = 1;
                   
                    break;
                case 3:
                     Wizardlist.ActiveStepIndex = 2;
                    break;
            }
            //DisplayPanel(OshaWizardlist.ActiveStepIndex);
        }

        public void SkipNextStep(object sender, WizardNavigationEventArgs e)
        {
            #region rough
            //if (optAccident.SelectedIndex == 0)
            //{
            //    Acc_Set.Value = "true";
            //}
            //switch (e.CurrentStepIndex)
            //{
            //    case 0:
            //        if (!(optAccident.SelectedIndex == 0))
            //            OshaWizardlist.ActiveStepIndex = 3;
            //        else if (optAccident.SelectedIndex == 0 && !(optResult.SelectedIndex == 2))
            //            OshaWizardlist.ActiveStepIndex = 1;
            //        else if (!(optPremise.SelectedIndex == 0))
            //            OshaWizardlist.ActiveStepIndex = 2;
            //        else
            //            OshaWizardlist.ActiveStepIndex = 3;
            //        break;
            //    case 1:
            //        if (!(optPremise.SelectedIndex == 0))
            //            OshaWizardlist.ActiveStepIndex = 2;
            //        else
            //            OshaWizardlist.ActiveStepIndex = 3;
            //        break;
            //    case 2:
            //        OshaWizardlist.ActiveStepIndex = 3;
            //        break;
            //    case 3:
            //        OshaWizardlist.ActiveStepIndex = 4;
            //        if (!(optTracking.SelectedIndex == 0))
            //        {
            //        }
            //        break;
            //}
            //DisplayPanel(OshaWizardlist.ActiveStepIndex);
            #endregion
            XmlDocument oFDMPageDom = new XmlDocument();
            //Preparing XML to send to service
            XElement oMessageElement = LoadSearchWizardTemplate();
            //Modify XML 
            XElement oViewID = oMessageElement.XPathSelectElement("./Document/SearchWizard/ViewID");
            if (oViewID != null)
            {
                if (Request.QueryString["selectedid"] != null)
                {
                    oViewID.Value = Request.QueryString["selectedid"];
                }
            }
            
            XElement oLangId = oMessageElement.XPathSelectElement("./Document/SearchWizard/LangId");
            if (oLangId != null)
            {
                oLangId.Value = sLangId;
            }
            XElement PageId = oMessageElement.XPathSelectElement("./Document/SearchWizard/PageId");
            if (PageId != null)
            {
                PageId.Value = sSearchPageID;
            }
            switch (e.CurrentStepIndex)
            {
                case 0:
                    if (searchtype.SelectedValue != "20")
                    {
                        Wizardlist.ActiveStepIndex = 2;
                        XElement oFunctionName = oMessageElement.XPathSelectElement("./Call/Function");
                        if (oFunctionName != null)
                        {
                            oFunctionName.Value = "SearchWizardAdaptor.GetStep2n3";
                        }
                        FillStep2(ref oMessageElement);
                        DisplayStep2(ref oMessageElement);
                        hdnAdminTrackingFlag.Value = "0";//parijat:15084
                    }
                    else
                    {
                        XElement oFunctionName = oMessageElement.XPathSelectElement("./Call/Function");
                        if (oFunctionName != null)
                        {
                            oFunctionName.Value = "SearchWizardAdaptor.GetAdmTrckStep";
                        }
                        FillStep2(ref oMessageElement);
                        DisplayStep1a(ref oMessageElement);
                        hdnAdminTrackingFlag.Value = "1";//parijat:15084
                    }
                    //bkumar33  :  Mits 14509 
                    ViewState["SelectedIndex"] = admtrcktable.SelectedIndex;                   
                    //End
                    break;
                case 1:
                    XElement oFunctionName1 = oMessageElement.XPathSelectElement("./Call/Function");
                    if (oFunctionName1 != null)
                    {
                        oFunctionName1.Value = "SearchWizardAdaptor.GetStep2n3";
                    }
                    FillStep2(ref oMessageElement);
                    FillStep1a2(ref oMessageElement);
                    DisplayStep2(ref oMessageElement);
                    break;
                case 2:
                    XElement oFunctionName2 = oMessageElement.XPathSelectElement("./Call/Function");
                    if (oFunctionName2 != null)
                    {
                        oFunctionName2.Value = "SearchWizardAdaptor.GetStep2n3";
                    }
                    if(hdnAdminTrackingFlag.Value =="1")//parijat:15084
                        FillStep1a2(ref oMessageElement);
                    FillStep2(ref oMessageElement);
                    FillStep3(ref oMessageElement);
                    DisplayStep3(ref oMessageElement);
                    break;
                case 3:
                    XElement oFunctionName3 = oMessageElement.XPathSelectElement("./Call/Function");
                    if (oFunctionName3 != null)
                    {
                        oFunctionName3.Value = "SearchWizardAdaptor.GetStep4";
                    }
                    
                    FillStep2(ref oMessageElement);
                    if (hdnAdminTrackingFlag.Value == "1")//parijat:15084
                        FillStep1a2(ref oMessageElement);
                    FillStep4(ref oMessageElement);
                    DisplayStep4(ref oMessageElement);
                    break;
            }
        }
        #region Step1a
        public void DisplayStep1a(ref XElement oMessageElement)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            //Parijat :14905 filling the sysname attribute value of AdmTable  
            XElement oAdmTable = oMessageElement.XPathSelectElement("./Document/SearchWizard/AdmTable");
            if (oAdmTable != null)
                oAdmTable.SetAttributeValue("sysname",hdnAdminSysName.Value);
            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
            if (oFDMPageDom.SelectSingleNode("//AdmTable") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("AdmTable")[0].OuterXml));
                admtrcktable.DataSource = aDataSet.Tables["level"];
                admtrcktable.DataTextField = "name";
                admtrcktable.DataValueField = "id";

                admtrcktable.DataBind();
                //abansal23: MITS 14905 Starts
                if (ViewState["AdminTrac"] == null)
                {
                    ViewState["AdminTrac"] = admtrcktable.SelectedValue;
                }
                //abansal23 : MITS 15885 on 4/25/2009 Starts
                else
                {
                    admtrcktable.SelectedValue = ViewState["AdminTrac"].ToString();
                }
                //abansal23 : MITS 15885 on 4/25/2009 Stops
                //abansal23: MITS 14905 Stops
                string str = Dropdown_Selection_value(aDataSet.Tables["level"], "selected", "1", "id");
                if (str != "")
                {
                    admtrcktable.SelectedValue = str;
                    ViewState["admtbltype"] = str;
                }

            }
        }
        #endregion
        
        #region Step2
        public void FillStep1a2(ref XElement oMessageElement)
        {
            XElement oAdmTableSelectedId = oMessageElement.XPathSelectElement("./Document/SearchWizard/AdmTableSelectedId");
            if (oAdmTableSelectedId != null)
                oAdmTableSelectedId.Value = admtrcktable.SelectedValue;
            XElement oLabel3 = oMessageElement.XPathSelectElement("./Document/SearchWizard/Label3");
            if (oLabel3 != null)
            {
                if (ViewState["admtbltype"] == null)//Parijat: changes for 14905--due to this was not able to fetch the dis[play fields(FlsToSrch)
                    oLabel3.Value = "notchanged";
                else
                {
                    if ((ViewState["admtbltype"].ToString() != admtrcktable.SelectedValue))//ViewState["admtbltype"] == null) || Parijat: changes for 14905--due to this was not able to fetch the dis[play fields(FlsToSrch)
                    {
                        oLabel3.Value = "changed";
                    }
                    else
                        oLabel3.Value = "notchanged";
                }
            }
        }
        public void FillStep2(ref XElement oMessageElement)
        {

            //
            Label2.Value = searchtype.SelectedValue;
            XElement oLabel2 = oMessageElement.XPathSelectElement("./Document/SearchWizard/Label2");
            if (oLabel2 != null)
                oLabel2.Value = searchtype.SelectedValue;
            //
            XElement oDefaultChkboxValue = oMessageElement.XPathSelectElement("./Document/SearchWizard/DefaultChkboxValue");
            if(oDefaultChkboxValue !=null)
            {
                oDefaultChkboxValue.Value = defCheckBoxStatus.Checked.ToString();
            } // if
            if (OriginalType.Value != "")
            {
                XElement oOriginalType = oMessageElement.XPathSelectElement("./Document/SearchWizard/OriginalType");
                if (oOriginalType != null)
                    oOriginalType.Value = OriginalType.Value;
            }
            OriginalType.Value = Label2.Value;
            //
            XElement oSelectedSearchTypeId = oMessageElement.XPathSelectElement("./Document/SearchWizard/SelectedSearchTypeId");
            if (oSelectedSearchTypeId != null)
                oSelectedSearchTypeId.Value = searchtype.SelectedValue;

            XElement oSearchName = oMessageElement.XPathSelectElement("./Document/SearchWizard/SearchName");
            if (oSearchName != null)
                oSearchName.Value = searchname.Value;
            XElement oSearchDesc = oMessageElement.XPathSelectElement("./Document/SearchWizard/SearchDesc");
            if (oSearchDesc != null)
                oSearchDesc.Value = searchdesc.Value;
        }

        public void DisplayStep2(ref XElement oMessageElement)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";

            //XElement oCheckdisplay = oMessageElement.XPathSelectElement("./Document/SearchWizard/ChkDisplay");
            //if (oCheckdisplay != null)
            //    oCheckdisplay.Value = Boolean.TrueString; 

            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
            if(hdnDefaultChkboxValue.Value == "True")
            {
                this.defCheckBoxStatus.Checked = true;
            } // if
            else
            {
                this.defCheckBoxStatus.Checked = false;
            } // else

            if (oFDMPageDom.SelectSingleNode("//FldsAvlSrch") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("FldsAvlSrch")[0].OuterXml));
                searchcats.DataSource = aDataSet.Tables["DisplayCategory"];
                searchcats.DataTextField = "name";
                searchcats.DataValueField = "name";

                searchcats.DataBind();
                //ListItem ls = new ListItem("Select a Category", "1st");
                ListItem ls = new ListItem(AppHelper.GetResourceValue(this.sPageID, "lblSelectCategory", "0"), "1st");              
                searchcats.Items.Insert(0, ls);
                searchcats.SelectedValue = "1st";

            }
            if (oFDMPageDom.SelectSingleNode("//FldsAvlDisp") != null)
            {
                ViewState["FieldDisplay"] = oFDMPageDom.GetElementsByTagName("FldsAvlDisp")[0].OuterXml;
            }
            //
            if (oFDMPageDom.SelectSingleNode("//FldsToSrch") != null)
            {
                DataSet aDataSet1 = new DataSet();
                aDataSet1.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("FldsToSrch")[0].OuterXml));
                if (selectedfields.Items.Count == 0)
                {
                    if (aDataSet1.Tables["FieldToSearch"] != null)
                    {
                        foreach (DataRow dr in aDataSet1.Tables["FieldToSearch"].Rows)
                        {
                            ListItem li = new ListItem(dr["name"].ToString(), dr["id"].ToString() + "_" + dr["name"].ToString() + "_" + dr["suppflag"].ToString() + "_" + dr["querytype"].ToString());
                            selectedfields.Items.Add(li);
                        }
                    }
                }
            }
            //
            if (oFDMPageDom.SelectSingleNode("//Step2FirstAccess") != null)
            {
                if (oFDMPageDom.GetElementsByTagName("Step2FirstAccess")[0].InnerXml != "")
                {
                    hdnFirstAccess2.Value = oFDMPageDom.GetElementsByTagName("Step2FirstAccess")[0].InnerXml;
                }
            }
        }
        protected void LoadFieldCategory(object sender, EventArgs e)
        {
            DropDownList obj = (DropDownList)sender;
            if (obj.ID == "searchcats")
                fields.Items.Clear();
            else
                fields1.Items.Clear();
            if (obj.SelectedValue != "1st")
            {
                XmlDocument xmlDocument = new XmlDocument();

                // Ishan : Multi Currency
                if (ViewState["ChkDisplay"] != null)
                {
                    if (ViewState["ChkDisplay"].ToString() == Boolean.TrueString)
                    {
                        xmlDocument.LoadXml(ViewState["FieldDisplay1"].ToString());

                    }
                    else
                    {
                        xmlDocument.LoadXml(ViewState["FieldDisplay"].ToString());
                    }
                }
                else
                {
                    xmlDocument.LoadXml(ViewState["FieldDisplay"].ToString());
                }

                //string xpathQuery = "/FldsAvlDisp/DisplayCategory[contains(@name, '.Text')="+searchcats.SelectedValue+"]";
                string xpathQuery = "/FldsAvlDisp/DisplayCategory[@name = '" + obj.SelectedValue + "']";
                XmlNodeList xmlNodeList = xmlDocument.SelectNodes(xpathQuery);

                //foreach (XmlNode xmlNode in xmlNodeList)
                //{
                //    textBox1.Text += xmlNode.InnerText;
                //}
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(xmlNodeList[0].OuterXml));
                if (aDataSet.Tables["DisplayField"] != null)
                {
                    foreach (DataRow dr in aDataSet.Tables["DisplayField"].Rows)
                    {
                        ListItem li = new ListItem(dr["name"].ToString(), dr["id"].ToString() + "_" + dr["name"].ToString() + "_" + dr["suppflag"].ToString() + "_" + dr["querytype"].ToString());
                        if (obj.ID == "searchcats")
                            fields.Items.Add(li);
                        else
                            fields1.Items.Add(li);
                    }
                }

            }
        }
        #endregion

        #region Step3
        public void FillStep3(ref XElement oMessageElement)
        {
            //HtmlSelect B = (HtmlSelect)Page.Form.FindControl("selectedfields");
            ////B.Attributes.Add("OnClick", "SelectOnlyOneRadio(" + B.ClientID + ", " + "'GridView1'" + ")");
            //Page.RegisterClientScriptBlock("ee", "<Script>SaveList(" + B.ClientID + ")</Script>");

            XElement oFldsAvlSrch = oMessageElement.XPathSelectElement("./Document/SearchWizard/FldsAvlSrch");
            if (oFldsAvlSrch != null)
                oFldsAvlSrch.Value = searchcats.SelectedValue;
            XElement oFldsToSrch = oMessageElement.XPathSelectElement("./Document/SearchWizard/FldsToSrch");
            if (oFldsToSrch != null)
                oFldsToSrch.Value = hdnSelectedFields.Value;
            XElement oSrchList = oMessageElement.XPathSelectElement("./Document/SearchWizard/SrchList");
            if (oSrchList != null)
                oSrchList.Value = hdnlist.Value;
            XElement oStep2FirstAccess = oMessageElement.XPathSelectElement("./Document/SearchWizard/Step2FirstAccess");
            if(oStep2FirstAccess != null)
            {
                if (hdnFirstAccess2.Value == "true")
                {
                    oStep2FirstAccess.Value = "false";
                    hdnFirstAccess2.Value = "false";
                }
                else
                {
                    oStep2FirstAccess.Value = "true";
                    hdnFirstAccess2.Value = "true";
                }
            }
        }

        public void DisplayStep3(ref XElement oMessageElement)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            // Ishan : Multi Currency
            XElement oCheckdisplay = oMessageElement.XPathSelectElement("./Document/SearchWizard/ChkDisplay");
            if (oCheckdisplay != null)
                oCheckdisplay.Value = Boolean.TrueString; 

            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
            if (oFDMPageDom.SelectSingleNode("//FldsAvlSrch") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("FldsAvlSrch")[0].OuterXml));
                searchcats1.DataSource = aDataSet.Tables["DisplayCategory"];
                searchcats1.DataTextField = "name";
                searchcats1.DataValueField = "name";

                searchcats1.DataBind();
                //ListItem ls = new ListItem("Select a Category", "1st");
                ListItem ls = new ListItem(AppHelper.GetResourceValue(this.sPageID, "lblSelectCategory", "0"), "1st");
                searchcats1.Items.Insert(0, ls);
                searchcats1.SelectedValue = "1st";

            }
            if (oFDMPageDom.SelectSingleNode("//ChkDisplay") != null)
            {
                ViewState["ChkDisplay"] = oFDMPageDom.SelectSingleNode("//ChkDisplay").InnerText;
            }
            if (oFDMPageDom.SelectSingleNode("//FldsAvlDisp") != null)
            {
                ViewState["FieldDisplay1"] = oFDMPageDom.GetElementsByTagName("FldsAvlDisp")[0].OuterXml;//can be just FieldDisplay as earlier
            }
            if (oFDMPageDom.SelectSingleNode("//FldsToDisp") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("FldsToDisp")[0].OuterXml));
                //abansal23: MITS 16614 on 05/19/2009 Started
                if (hdnIsEdit.Text != "")
                    m_IsEdit = true;
                if (!m_IsEdit)
                {
                    hdnIsEdit.Text = "true";
                    if (aDataSet.Tables["FieldToDisplay"] != null)
                    {
                        foreach (DataRow dr in aDataSet.Tables["FieldToDisplay"].Rows)
                        {
                            ListItem li = new ListItem(dr["name"].ToString(), dr["id"].ToString() + "_" + dr["name"].ToString() + "_" + dr["suppflag"].ToString() + "_" + dr["querytype"].ToString());
                            selectedfields1.Items.Add(li);
                        }
                    }
                }
                //abansal23: MITS 16614 on 05/19/2009 Ends
            }
            //abansal23: MITS 15610 on 4/25/2009 Starts
            if ((ViewState["SearchTypeChanged"] != null) && (ViewState["SearchTypeChanged"].ToString() == bool.TrueString))
            {
                selectedfields1.Items.Clear();
                for (int i = 0; i < selectedfields.Items.Count; i++)
                {
                    selectedfields1.Items.Add(selectedfields.Items[i]);
                }
            }
            //abansal23: MITS 15610 on 4/25/2009 Ends
           if (oFDMPageDom.SelectSingleNode("//Step4FirstAccess") != null)
            {
                if (oFDMPageDom.GetElementsByTagName("Step4FirstAccess")[0].InnerXml != "")
                {
                    hdnFirstAccess4.Value = oFDMPageDom.GetElementsByTagName("Step4FirstAccess")[0].InnerXml;
                }
            }
        }

        protected void AddSelected(object sender, EventArgs e)
        {
            
            ListBox select1;
            ListBox select2;
            if (((Button)sender).ID == "addField")
            {
                select1 = (ListBox)fields;
                select2 = (ListBox)selectedfields;
            }
            else if(((Button)sender).ID == "addField1")
            {
                select1 = (ListBox)fields1;
                select2 = (ListBox)selectedfields1;
            }
            else
            {
                select1 = (ListBox)usersgroups;
                select2 = (ListBox)selectedfields2;
            }
            
                foreach (ListItem item in select1.Items)
                {
                    //ListItem item = select1.Items[select1.SelectedIndex];
                    if (item.Selected)
                    {
                        if (select2.Items.IndexOf(item) == -1)
                        {
                            select2.Items.Add(item);
                            select2.SelectedIndex = select2.Items.IndexOf(item);
                        }
                    }
                }

        }

        protected void RemoveSelected(object sender, EventArgs e)
        {
            if (((Button)sender).ID == "removeField")
            {
                while (selectedfields.SelectedItem != null)
                {
                    selectedfields.Items.Remove(selectedfields.SelectedItem);   
                }

            }
            else if(((Button)sender).ID == "removeField2")
            {

                while (selectedfields1.SelectedItem != null)
                {
                    selectedfields1.Items.Remove(selectedfields1.SelectedItem);
                }
            }
            else if (((Button)sender).ID == "btnRemove1")
            {
                while (selectedfields2.SelectedItem != null)
                {
                    selectedfields2.Items.Remove(selectedfields2.SelectedItem);
                }
            }
        }
        #endregion

        #region Step4
        public void FillStep4(ref XElement oMessageElement)
        {
            XElement oStep4FirstAccess = oMessageElement.XPathSelectElement("./Document/SearchWizard/Step4FirstAccess");
            if (oStep4FirstAccess != null)
            {
                if (hdnFirstAccess4.Value == "true")
                {
                    oStep4FirstAccess.Value = "false";
                    hdnFirstAccess4.Value = "false";
                }
                else
                {
                    oStep4FirstAccess.Value = "true";
                    hdnFirstAccess4.Value = "true";
                }
            }
            XElement oFldsAvlDisp = oMessageElement.XPathSelectElement("./Document/SearchWizard/FldsAvlDisp");
            if (oFldsAvlDisp != null)
                oFldsAvlDisp.Value = searchcats1.SelectedValue;
            XElement oFldsToDisp = oMessageElement.XPathSelectElement("./Document/SearchWizard/FldsToDisp");
            if (oFldsToDisp != null)
                oFldsToDisp.Value = hdnSelectedFields1.Value;
            XElement oDispList = oMessageElement.XPathSelectElement("./Document/SearchWizard/DispList");
            if (oDispList != null)
                oDispList.Value = hdnlist1.Value;

            //<FldsAvlDisp></FldsAvlDisp> 
            //                            <FldsToSrch></FldsToSrch> 
            //                            <SrchList></SrchList> 
            //                            <FldsToDisp></FldsToDisp> 
            //                        <DispList></DispList> 
        }
        public void DisplayStep4(ref XElement oMessageElement)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
            if (oFDMPageDom.SelectSingleNode("//Users/listrow") != null)
            {
                DataSet aDataSet1 = new DataSet();
                aDataSet1.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("Users")[0].InnerXml));
                
                if (aDataSet1.Tables["rowtext"] != null)
                {
                    foreach (DataRow dr in aDataSet1.Tables["rowtext"].Rows)
                    {
                        ListItem li = new ListItem(dr["title"].ToString(), "user_"+ dr["id"].ToString() + "_" + dr["title"].ToString());
                        if (usersgroups.Items.IndexOf(li) == -1)
                            usersgroups.Items.Add(li);
                    }
                }
            }
            if (oFDMPageDom.SelectSingleNode("//Groups/listrow") != null)
            {
                DataSet aDataSet1 = new DataSet();
                aDataSet1.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("Groups")[0].InnerXml));

                if (aDataSet1.Tables["rowtext"] != null)
                {
                    foreach (DataRow dr in aDataSet1.Tables["rowtext"].Rows)
                    {
                        ListItem li = new ListItem(dr["title"].ToString(), "group_" + dr["id"].ToString() + "_" + dr["title"].ToString());
                        if(usersgroups.Items.IndexOf(li) == -1)
                            usersgroups.Items.Add(li);
                    }
                }
            }
            if (oFDMPageDom.SelectSingleNode("//UsersPermitted/listrow") != null)
            {
                DataSet aDataSet1 = new DataSet();
                aDataSet1.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("UsersPermitted")[0].InnerXml));

                if (aDataSet1.Tables["rowtext"] != null)
                {
                    foreach (DataRow dr in aDataSet1.Tables["rowtext"].Rows)
                    {
                        ListItem li = new ListItem(dr["title"].ToString(), "user_" + dr["id"].ToString() + "_" + dr["title"].ToString());
                        if (selectedfields2.Items.IndexOf(li) == -1)
                            selectedfields2.Items.Add(li);
                    }
                }
            }
            if (oFDMPageDom.SelectSingleNode("//GroupsPermitted/listrow") != null)
            {
                DataSet aDataSet1 = new DataSet();
                aDataSet1.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("GroupsPermitted")[0].InnerXml));

                if (aDataSet1.Tables["rowtext"] != null)
                {
                    foreach (DataRow dr in aDataSet1.Tables["rowtext"].Rows)
                    {
                        ListItem li = new ListItem(dr["title"].ToString(), "group_" + dr["id"].ToString() + "_" + dr["title"].ToString());
                        if (selectedfields2.Items.IndexOf(li) == -1)
                            selectedfields2.Items.Add(li);
                    }
                }
            }
            if (oFDMPageDom.GetElementsByTagName("AllUsersSelected")[0].InnerXml == "1")
            {
                allowall.Checked = true;
                //usersgroups.Disabled = true;
                //btnAdd1.Enabled =false;
                //btnRemove1.Enabled =false;
                //selectedfields2.Disabled = true;
            }
            else if (oFDMPageDom.GetElementsByTagName("SelectUsersSelected")[0].InnerXml == "1")
            {
                selectusers.Checked = true;
                //usersgroups.Disabled = false;
                //btnAdd1.Enabled = true;
                //btnRemove1.Enabled = true;
                //selectedfields2.Disabled = false;
            }
            if (oFDMPageDom.SelectSingleNode("//Step5FirstAccess") != null)
            {
                if (oFDMPageDom.GetElementsByTagName("Step5FirstAccess")[0].InnerXml != "")
                {
                    hdnFirstAccess5.Value = oFDMPageDom.GetElementsByTagName("Step5FirstAccess")[0].InnerXml;
                }
            }
            //<AllUsersSelected></AllUsersSelected> 
            //                        <AllUsers></AllUsers>
            //                        <SelectUsersSelected></SelectUsersSelected>
        }
       
        #endregion

        #region Save
        
        public void Save(object sender, WizardNavigationEventArgs e)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            //Preparing XML to send to service
            XElement oMessageElement = LoadSearchWizardTemplate();
            FillStep5Save(ref oMessageElement);
            FillStep2(ref oMessageElement);
            if(searchtype.SelectedValue == "20")
                 FillStep1a2(ref oMessageElement);
            FillStep3(ref oMessageElement);
            FillStep4(ref oMessageElement);
            //calling service for save
            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

            //abansal23: MITS 16225 on 05/06/2009 Starts

            ViewState["SearchType"] = null;//abansal23: MITS 14905
            ViewState["AdminTrac"] = null;//abansal23: MITS 14905

            ////After the validation,there was a need to post back the parent page through javascript 
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);

            ClientScriptManager csm = Page.ClientScript;
            csm.RegisterClientScriptBlock(this.GetType(), "ee", "<Script>window.opener.ReSubmitPage();window.close();</Script>");
            //abansal23: MITS 16225 on 05/06/2009 Stops
        }
        public void FillStep5Save(ref XElement oMessageElement)
        {
            //Modify XML 
            XElement oViewID = oMessageElement.XPathSelectElement("./Document/SearchWizard/ViewID");
            if (oViewID != null)
            {
                if (Request.QueryString["selectedid"] != null)
                {
                    oViewID.Value = Request.QueryString["selectedid"];
                }
            }
            XElement oFunctionName = oMessageElement.XPathSelectElement("./Call/Function");
            if (oFunctionName != null)
            {
                oFunctionName.Value = "SearchWizardAdaptor.Save";
            }
            XElement oStep5FirstAccess = oMessageElement.XPathSelectElement("./Document/SearchWizard/Step5FirstAccess");
            if (oStep5FirstAccess != null)
            {
                if (hdnFirstAccess5.Value == "true")
                {
                    oStep5FirstAccess.Value = "false";
                    hdnFirstAccess5.Value = "false";
                }
                else
                {
                    oStep5FirstAccess.Value = "true";
                    hdnFirstAccess5.Value = "true";
                }
            }
            XElement oAllUsers = oMessageElement.XPathSelectElement("./Document/SearchWizard/AllUsers");
            XElement oAllUsersSelected = oMessageElement.XPathSelectElement("./Document/SearchWizard/AllUsersSelected");
            XElement oSelectUsersSelected = oMessageElement.XPathSelectElement("./Document/SearchWizard/SelectUsersSelected");
            if (allowall.Checked == true)
            {
                oAllUsers.Value = "AllUsers";
                oAllUsersSelected.Value = "1";
                oSelectUsersSelected.Value = "0";

            }
            else
            {
                oAllUsers.Value = "SelectUsers";
                oSelectUsersSelected.Value = "1";
                oAllUsersSelected.Value = "0";
            }
            XElement oUserGroupList = oMessageElement.XPathSelectElement("./Document/SearchWizard/UserGroupList");
            if (oUserGroupList != null)
                oUserGroupList.Value = hdnlist2.Value;

            //                   <Users><listrow /></Users> 
            //                   <Groups><listrow /></Groups>
            //                   <UsersPermitted><listrow /></UsersPermitted>
            //                   <GroupsPermitted><listrow /></GroupsPermitted>

        }
        
        #endregion

    }
}
