﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
//using Riskmaster.Models;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.DisableBES
{
    public partial class DisableBES : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }

        /// <summary>
        /// CWS request message template for IsOrgSetFirstTime
        /// </summary>
        /// <returns></returns>
        private XElement DisableOrgSecTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                      <Message>
                          <Authorization>a7abceb9-2cec-4b1e-9a2b-5938d7c09be1</Authorization> 
                          <Call>
                            <Function>ORGSECAdaptor.DisableOrgSec</Function> 
                          </Call>
                          <Document>
                              <Document>
                                <DisableBESFlag></DisableBESFlag> 
                              </Document>
                          </Document>
                      </Message>




            ");
            return oTemplate;
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            //Preparing XML to send to service
            XElement oMessageElement = DisableOrgSecTemplate();
            //Modify XML 
            XElement oDisableBESFlag = oMessageElement.XPathSelectElement("./Document/Document/DisableBESFlag");
            if (oDisableBESFlag != null)
            {
                if (optPartial.Checked == true)
                {
                    oDisableBESFlag.Value = "0";
                }
                else if (optFull.Checked == true)
                {
                    oDisableBESFlag.Value = "1";
                }
            }
            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
            
            Response.Redirect("~/UI/Utilities/BES/BES.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Utilities/BES/BES.aspx");
        }

    }
}
