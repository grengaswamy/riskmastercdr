<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FundsDormancy.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.FundsDormancy.FundsDormancy" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
   <title>Funds Dormancy</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css"  type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">        { var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
     <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Funds Dormancy" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/option/DormancyRowId"
        rmxignoreset='true' />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="JurisdictionIDLoadvalue" />
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>          
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblJurisdiction" Text="Jurisdiction:" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc2:codelookup runat="server" ID="Jurisdiction" CodeTable="STATES" ControlName="Jurisdiction"
                        RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='Jurisdiction']" type="code"
                        tabindex="1" required="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblUnclaimedDormancy" Text="Unclaimed Days:" class="required" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" onchange="setDataChanged(true);" ID="UnclaimedDays"  onblur="return numLostFocusNonNegative(this);"
                        RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='UnclaimedDays']"
                        MaxLength = "9"   TabIndex="2" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblEscheatDays" Text="Escheat Days:" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" onchange="setDataChanged(true);" ID="EscheatDays" onblur="return numLostFocusNonNegative(this);"
                        RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='EscheatDays']"
                        MaxLength = "9" TabIndex="3" type="text" />
                </td>
            </tr>
        </tbody>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnOk">
            <%-- tkatsarski: 11/27/14 RMA-4878: Added setDataChanged(true) so the forms fields can be checked. --%>
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="setDataChanged(true); return FundsDormancy_onOk();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return FundsDormancy_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    </form>
</body>
</html>
