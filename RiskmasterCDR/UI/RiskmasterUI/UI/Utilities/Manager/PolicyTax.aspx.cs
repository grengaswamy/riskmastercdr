﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.Xml;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    /// <summary>
    /// Animesh Inserted a New Class for Policy Taxes.
    /// </summary>
    public partial class PolicyTax : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    if (mode.Text.ToLower() != "add")
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("TaxParmsAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);
                    }
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";
                    Control c = DatabindingHelper.GetPostBackControl(this.Page);
                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("TaxParmsAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            if (mode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>f5a2fbdc-8a07-417c-99fb-d662df89adbc</Authorization>");
            sXml = sXml.Append("<Call><Function>TaxParmsAdaptor.Get</Function></Call>");
            sXml = sXml.Append("<Document><Document><Tax>");
            sXml = sXml.Append("<use>" + useTax.Checked.ToString() + "</use>");
            sXml = sXml.Append("<amount>" + Amount.Text.Trim() + "</amount>");
            sXml = sXml.Append("<effectivedate>" + EffectiveDate.Text.Trim() + " </effectivedate>");
            sXml = sXml.Append("<expirationdate>" + ExpirationDate.Text.Trim() + "</expirationdate>");
            sXml = sXml.Append("<taxtype codeid='" + TaxType.CodeIdValue + "'>" + TaxType.CodeTextValue + "</taxtype>");
            sXml = sXml.Append("<lob codeid='" + LOBcode.CodeIdValue + "'>" + LOBcode.CodeTextValue + "</lob>");
            sXml = sXml.Append("<state codeid='" + Statecode.CodeIdValue + "'>" + Statecode.CodeTextValue + "</state>");
            sXml = sXml.Append("<PolicyTaxrowid>" + RowId.Text + "</PolicyTaxrowid>");
            sXml = sXml.Append("</Tax></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("TaxParmsAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    //After the validation,there was a need to post back the parent page through javascript 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
