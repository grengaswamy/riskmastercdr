<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdjusterScreenSetup.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.AdjusterScreenSetup" ValidateRequest = "false" %>
<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Auto Assign Adjuster Setup</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>
    <script type="text/javascript" src="../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Auto Assign Adjuster Setup" />
    </div>
<br />
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />

    <asp:TextBox Style="display: none" runat="server" ID="txtRowId" RMXRef="/Instance/Document/AdjusterScreens/control[@name ='AutoAssignID']"/>
    <asp:TextBox Style="display: none" runat="server" ID="txtSelectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="txtReloadNA" Text="" />
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <asp:TextBox Style="display: none" runat="server" ID="FormMode" />
    <div runat="server" class="full" id="div_adjlastname" xmlns="">
    <asp:label runat="server" class="required" id="lbl_adjlastname" Text="Adjuster" Width = "120"/>
        <span>
            <asp:Textbox runat="server" ReadOnly="false" Width = "200"  onchange="lookupTextChanged(this);setDataChanged(true);" tabindex="1" onblur="CustomlookupLostFocus(this);" RMXRef="/Instance/Document/AdjusterScreens/control[@name ='AdjusterEID']" RMXType="entitylookup" id="adjlastfirstname" />
            <%--<asp:Textbox runat="server" style="display:none" Text="1" id="adjlastfirstname_creatable" /> rsushilaggar MITS 27851 date 03/23/2012--%>
            <asp:button runat="server" class="EllipsisControl" id="adjlastfirstnamebtn" tabindex="2" onclientclick="return CustomlookupData('adjlastfirstname','ADJUSTERS',4,'adj',1);" />
            <asp:TextBox style="display:none" runat="server" onchange="setDataChanged(true);" id="adjentityid" RMXRef="/Instance/Document/AdjusterScreens/control[@name ='AdjusterEID']/@codeid" RMXType="id" />            
        </span>
    </div>
       <%-- rsharma220: MITS 32304 --%> 
    <div id="div_MultiOrg" runat="server" class="full" visible="false">
	<asp:Label runat="server" class="label" ID="lblMultiOrg" Text="Departments" Width = "120"/>
	<uc:MultiCode runat="server" ID="Orgs" width="205px" Height="70px" CodeTable="orgh" ParentNode="DEPARTMENT" 
        	ControlName="Orgs" RMXRef="/Instance/Document/AdjusterScreens/control[@name ='DeptEIds']"
                RMXType="multiorg" />
        <asp:Label runat="server" class="small" ID="lblDescOrg" Text="No selection means all!" Width = "200"/>
            <asp:TextBox Style="display: none" onchange="setDataChanged(true);" ID="Orgs_multicode_cid" runat="server" RMXRef = ""/>
    </div>

    <div id="div_MultiLOB" runat="server" class="full" visible="false">
	<asp:Label runat="server" class="label" ID="lblMultiLOB" Text="LOBs" Width = "120"/>
	<uc:MultiCode runat="server" ID="LOBs" MaxSize="50" width="205px" Height="70px" CodeTable="LINE_OF_BUSINESS"
        	ControlName="LOBs" RMXRef="/Instance/Document/AdjusterScreens/control[@name ='LOBCodes']"
                RMXType="multilob" />
        <asp:Label runat="server" class="small" ID="lblDescLOB" Text="No selection means all!" Width = "200"/>
    </div>

    <div id="div_MultiClaimType" runat="server" class="full" visible="false">
	<asp:Label runat="server" class="label" ID="lblMultiClaimType" Text="Claim Type" Width = "120"/>
	<uc:MultiCode runat="server" ID="ClaimType" MaxSize="50" width="205px" Height="70px" CodeTable="CLAIM_TYPE"
        	ControlName="ClaimType" RMXRef="/Instance/Document/AdjusterScreens/control[@name ='ClaimTypeCodes']"
                RMXType="multiclaimtype" />
        <asp:Label runat="server" class="small" ID="lblDescClaimType" Text="No selection means all!" Width = "200"/>
    </div>

    <div id="div_MultiJurisdiction" runat="server" class="full" visible="false">
	<asp:Label runat="server" class="label" ID="lblMultiJurisdiction" Text="Jurisdiction" Width = "120"/>
	<uc:MultiCode runat="server" ID="Jurisdiction" MaxSize="50" width="205px" Height="70px" CodeTable="STATES"
        	ControlName="Jurisdiction" RMXRef="/Instance/Document/AdjusterScreens/control[@name ='JurisdictionCodes']"
                RMXType="multijurisdiction" />
        <asp:Label runat="server" class="small" ID="lblDescJurisdiction" Text="No selection means all!" Width = "200"/>
    </div>

    <div runat="server" class="full" id="div_SkipWorkItems" visible="false">
        <asp:label runat="server" class="label" id="lbl_SkipWorkItems" Text="Skip Work Items" Width = "120"/>
        <span>
            <asp:TextBox runat="server" id="SkipWorkItems" MaxLength="6" Width = "200" RMXRef="/Instance/Document/AdjusterScreens/control[@name ='SkipWorkItems']" RMXType="numeric" onChange="setDataChanged(true);" /> <!-- mkaran2 : MITS 35940-->
        </span>
    </div>

    <div runat="server" id="div_ForceWorkItems" visible="false">
        <asp:label runat="server" class="label" id="lbl_ForceWorkItems" Text="Force Work Items" Width = "120"/>
        <span>
            <asp:TextBox runat="server" id="ForceWorkItems" MaxLength="6" Width = "200" RMXRef="/Instance/Document/AdjusterScreens/control[@name ='ForceWorkItems']" RMXType="numeric" onChange="setDataChanged(true);" /> <!-- mkaran2 : MITS 35940-->
        </span>
    </div>
    <br /> 
  <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return OnAdjusterScreenSave();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return AdjusterScreen_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
  <br /> 
  <br /> 
  <br /> 
  <div class="msgheader" id="divNonAvail">
        <asp:Label ID="Label1" runat="server" Text="Non Availability Plan" />
  </div> 
  <div>
      <span>
        <dg:UserControlDataGrid runat="server" ID="NonAvailGrid" GridName="NonAvailGrid" GridTitle="Non Availability Plan" Target="/Document/AdjusterScreens/NonAvailPlan" Ref="/Instance/Document/form//control[@name='NonAvailGrid']" Unique_Id="RowId" ShowRadioButton="true" Width="250" Height="60%" hidenodes="|RowId|" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="250" Type="GridAndButtons" RowDataParam="listrow"/>
      </span>
  </div>
  <asp:TextBox style="display:none" runat="server" id="NonAvailGridSelectedId"  RMXType="id" />
  <asp:TextBox style="display:none" runat="server" id="NonAvailGrid_RowDeletedFlag"  RMXType="id" Text="false" />
  <asp:TextBox style="display:none" runat="server" id="NonAvailGrid_Action"  RMXType="id" />
  <asp:TextBox style="display:none" runat="server" id="NonAvailGrid_RowAddedFlag"  RMXType="id" Text="false"  />
  </form>
</body>
</html>
