﻿<%@ Page Title="" Language="C#" theme="RMX_Default" MasterPageFile="~/UI/Utilities/UtilityTemplate.Master" AutoEventWireup="true" CodeBehind="ClaimTypeChangeOptions.aspx.cs" Inherits="Riskmaster.UI.Utilities.ClaimTypeChangeOptions" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <title>
        Claim Type Change Options
    </title>
</asp:content>
<asp:content ID="Content2" ContentPlaceHolderID="cphUtilityBody" runat="server">

<cc1:tabcontainer id="tcClaimTypeChangeOptions" cssclass="ajax__tab_xp2" runat="server" activetabindex="0">
    <cc1:TabPanel runat="server" HeaderText="Claim Type Change Options"  ID="TabPanel1">
        <contenttemplate>
            <table border="0" width="100%">
                <tr>
                    <td>
                        <asp:gridview id="gvClaimTypeChangeOptions" name="gvClaimTypeChangeOptions" runat="server" />
                    </td>
                    <td valign="top">
                        <asp:imagebutton id="btNew" name="btNew" imageurl="~/Images/new.gif" runat="server" />
                        <br />
                        <asp:imagebutton id="btEdit" name="btEdit" imageurl="~/Images/edittoolbar.gif" runat="server" />
                        <br />
                        <asp:imagebutton id="btRemove" name="btRemove" imageurl="~/Images/delete.gif" runat="server" />
                    </td>
                </tr>
            </table>
        </contenttemplate>
    </cc1:TabPanel>
</cc1:tabcontainer>
    
</asp:content>
