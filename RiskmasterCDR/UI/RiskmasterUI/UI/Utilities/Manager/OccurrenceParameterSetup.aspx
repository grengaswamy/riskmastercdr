<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OccurrenceParameterSetup.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.Manager.OccurrenceParameterSetup" %>
<%@ Register Src="~/UI/Shared/Controls/MultiCodeLookup.ascx" TagName="MultiCodeLookUp"
    TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <title>Occurrence / Event Parameters</title>
    <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
    </style>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/Utilities.js"></script>

    <script type="text/JavaScript" src="../../../Scripts/WaitDialog.js"></script>

    <script type="text/JavaScript" src="/csc-Theme/riskmaster/common/javascript/drift.js"></script>

    <script type="text/javascript" src="/csc-Theme/riskmaster/common/javascript/dhtml-div.js"></script>

    <script type="text/javascript" src="/csc-Theme/riskmaster/common/javascript/dhtml-help-setup.js"></script>

    <script src="/../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script src="/../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>

    <script src="/../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>

    <script type="text/javascript" src="/../../Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="/../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="/../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>

    <script type="text/javascript" src="/../../Scripts/calendar-alias.js"></script>

    <script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid.js"></script>

    <script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js"></script>

    <script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js"></script>

    <script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid-query.js"></script>

    <script type="text/javascript" src="/../../Scripts/rmx-common-ref.js">
    </script>

</head>
<body class="10pt" onload="pageLoaded();">
    <form runat="server" id="frmData"  method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" /><input type="hidden"
        name="" value="" /><input type="hidden" name="" value="" /><input type="hidden" name=""
            value="" /><input type="hidden" name="" value="" /><input type="hidden" name="hTabName"/>
            
      
    <table border="0" class="toolbar" cellspacing="0">
        <tr>
            <td align="center" valign="middle" style="height: 32pt">
               
                <asp:ImageButton ID="Savebtn" runat="server" class="bold" ImageUrl="../../../Images/tb_save_active.png"
                    OnClientClick="return OccurrenceParmsSelectAll();" OnClick="Save" ToolTip="Save" />
        </tr>
    </table>
    <br />
    <td>
    </td>
    <div class="msgheader" id="Div1">
        Occurrence / Event Parameters</div>
    <div class="errtextheader">
    </div>
    <br>
    <table border="0">
        <%--<table border="0" cellspacing="0" cellpadding="0">--%>
            <tr>
                <td>
                <div class="tabGroup" id="TabsDivGroup" runat="server">
                    <div class="Selected" nowrap="true" runat="server" name="TABSOccParmsFacInfo" id="TABSOccParmsFacInfo">
                        <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="OccParmsFacInfo" id="LINKTABSOccParmsFacInfo">Facility Information</a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSOccParmsFacInfo">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSOccParmsEventConfig" id="TABSOccParmsEventConfig">
                        <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="OccParmsEventConfig" id="LINKTABSOccParmsEventConfig">Event Configurations</a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSOccParmsEventConfig">
                        <nbsp />
                        <nbsp />
                    </div>
                 </div>
                <%--</td>
            </tr>
        </table>
    </table>--%>
    <div class="singletopborder" style="position:relative;left:0;top:0;width:800px;height:315px;overflow:auto">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0" id="FORMTABOccParmsFacInfo">
                    <tr>
                        <td>
                            Facility Name:&nbsp;&nbsp;
                        </td>
                        <asp:TextBox Style="display: none" ID="RowId" type="id" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='RowId']" />
                        <td>
                            <div title="" style="padding: 0px; margin: 0px">
                                <asp:TextBox size="30" TabIndex="1" ID="FacilityName" type="text" onchange="setDataChanged(true);"
                                    MaxLength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='FacilityName']" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Facility Address:&nbsp;&nbsp;
                        </td>
                        <td>
                            <div title="" style="padding: 0px; margin: 0px">
                                <asp:TextBox size="30" TabIndex="2" ID="FacilityAddress" type="text" onchange="setDataChanged(true);"
                                    MaxLength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='FacilityAddress']" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Address Line 2:&nbsp;&nbsp;
                        </td>
                        <td>
                            <div title="" style="padding: 0px; margin: 0px">
                                <asp:TextBox size="30" TabIndex="3" ID="Address2" type="text" onchange="setDataChanged(true);"
                                    MaxLength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Address2']" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            City:&nbsp;&nbsp;
                        </td>
                        <td>
                            <div title="" style="padding: 0px; margin: 0px">
                                <asp:TextBox size="30" TabIndex="4" ID="City" type="text" onchange="setDataChanged(true);"
                                    MaxLength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='City']" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            State/Zip:&nbsp;&nbsp;
                        </td>
                        <td>
                            <div title="" style="padding: 0px; margin: 0px">
                                <asp:TextBox size="30" TabIndex="5" ID="StateZip" type="text" onchange="setDataChanged(true);"
                                    MaxLength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='StateZip']" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            HCFA Number:&nbsp;&nbsp;
                        </td>
                        <td>
                            <div title="" style="padding: 0px; margin: 0px">
                                <asp:TextBox size="30" TabIndex="6" ID="HCFANumber" type="text" onchange="setDataChanged(true);"
                                    MaxLength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='HCFANumber']" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Contact Person:&nbsp;&nbsp;
                        </td>
                        <td>
                            <div title="" style="padding: 0px; margin: 0px">
                                <asp:TextBox size="30" TabIndex="7" ID="ContactPerson" type="text" onchange="setDataChanged(true);"
                                    MaxLength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='ContactPerson']" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Facility Phone:&nbsp;&nbsp;
                        </td>
                        <td>
                            <div title="" style="padding: 0px; margin: 0px">
                                <asp:TextBox size="30" TabIndex="8" ID="FacilityPhone" type="phone" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" MaxLength="30" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='FacilityPhone']" /></div>
                    </tr>
                </table>
                <table border="0" cellspacing="0" cellpadding="0" id="FORMTABOccParmsEventConfig"
                    style="display: none;">
                    <tr>
                        <td>
                            Fall Event Categories:&nbsp;&nbsp;
                        </td>
                        <td>
                        <uc:MultiCodeLookUp runat="server" ID="FallEventCateg" CodeId="" width="200px" CodeTable="EVENT_CATEGORY" ControlName="FallEventCateg" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='FallEventCateg']" type="codelist"/>
                           
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Medication Event Categories:&nbsp;&nbsp;
                        </td>
                        <td>
                        
                       <uc:MultiCodeLookUp runat="server" ID="Medication"   CodeId="" width="200px" CodeTable="EVENT_CATEGORY" ControlName="Medication" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='Medication']" type="codelist" />
                           
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sentinel Event Indicators:&nbsp;&nbsp;
                        </td>
                        <td>
                        
                        <uc:MultiCodeLookUp  runat="server" ID="Sentinel" width="200px" CodeTable="EVENT_INDICATOR" ControlName="Sentinel" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='Sentinel']" type="codelist" />
                           
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Equipment Event Categories:&nbsp;&nbsp;
                        </td>
                        <td>
                        
                        <uc:MultiCodeLookUp  runat="server" ID="Equipment" width="200px" CodeTable="EVENT_CATEGORY" ControlName="Equipment" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='Equipment']" type="codelist" />
                           
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
            </td>
        </tr>
    </table>
    </div>
    <table>
        <tr>
            <td>
                <asp:TextBox runat="server" ID="SysViewType" Style="display: none" />
                <asp:TextBox runat="server" ID="SysCmd" Style="display: none" />
                <asp:TextBox runat="server" ID="SysCmdConfirmSave" Style="display: none" />
                <asp:TextBox runat="server" ID="SysCmdQueue" Style="display: none" />
                <asp:TextBox runat="server" name="" ID="SysCmdText" Style="display: none" rmxforms:value="Navigate" />
                <asp:TextBox runat="server" ID="SysClassName" Style="display: none" rmxforms:value="" />
                <asp:TextBox runat="server" ID="SysSerializationConfig" Style="display: none" />
                <asp:TextBox runat="server" ID="SysFormIdName" Style="display: none" rmxforms:value="RowId"
                    Text="occurence" />
                <asp:TextBox runat="server" ID="occurence" Style="display: none" rmxforms:value="RowId" />
                <asp:TextBox runat="server" ID="SysFormPIdName" Style="display: none" rmxforms:value="RowId" />
                <asp:TextBox type="text" runat="server" value="" ID="SysFormPForm" Style="display: none"
                 rmxforms:value="OccurrenceParms" />
                <asp:TextBox runat="server" value="" ID="SysInvisible" Style="display: none" rmxforms:value="" />
                <asp:TextBox runat="server" ID="SysFormName" Style="display: none" rmxforms:value="OccurrenceParms" />
                <asp:TextBox runat="server" value="" ID="SysRequired" Style="display: none" />
                <asp:TextBox runat="server" value="FacilityName" ID="SysFocusFields" Style="display: none" />
            </td></tr></table>
            </td>
        </tr>
    </table>
    <input type="hidden" value="rmx-widget-handle-3" id="SysWindowId" />
    
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
