﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class SystemParameterUseEnhPol : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (hdnValue.Value == "1")
                {
                    NonFDMCWSPageLoad("SystemParameterAdaptor.UseEnhPol");
                    if (Success.Text == "0")
                    {
                        password.Visible = false;
                        lblActivation.Text = "Incorrect activation code. Please contact RISKMASTER sales if you are interested in purchasing the Policy Management System.";
                        hdnValue.Value = string.Empty;
                        Activation_temp.Visible = false;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
