﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager.AdministrativeTracking
{
    public partial class AdminTracking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
                BindToGridView();
          
        }


        #region Private Functions


        private void BindToGridView()
        {
            DataSet dsAdminData = null;
            dsAdminData = GetAdministrativeTracking();

            try
            {
                GridView1.DataSource = dsAdminData.Tables[1];
                GridView1.DataBind();

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        private DataSet GetAdministrativeTracking()
        {
            string strAdmintrackingSrvcOutput = string.Empty;
            XmlDocument xmlAdmintrackingDoc = new XmlDocument();
            XmlNode inputDocNode = null;
            XmlNode xmlNode = null;
            string serviceMethodToCall = string.Empty;
            DataSet dSet = null;

            try
            {
                inputDocNode = InputForCWS();

                strAdmintrackingSrvcOutput = AppHelper.CallCWSService(inputDocNode.InnerXml);

                ErrorControl1.errorDom = strAdmintrackingSrvcOutput;

                xmlAdmintrackingDoc.LoadXml(strAdmintrackingSrvcOutput);

                if (strAdmintrackingSrvcOutput != null)
                {
                    dSet = new DataSet();
                    dSet.ReadXml(new XmlNodeReader(xmlAdmintrackingDoc.SelectSingleNode("ResultMessage/Document/AdminTrackingList")));
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }


            return dSet;
   
        }

        private XmlNode InputForCWS()
        {            
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
              <Message>
              <Authorization></Authorization>
              <Call>
                <Function>AdministrativeTrackingAdaptor.Get</Function>
              </Call>
              <Document>
                   <AdminTrackingList>
                   <listhead>
                   <UserTableName>Table Name</UserTableName> 
                   <SysTableName>System Table Name</SysTableName>
                   <RowId>RowId</RowId>  
                   </listhead>
                   </AdminTrackingList> 
             </Document>
             </Message>  
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;

        }


        #endregion

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //jramkumar MITS 33400
                javascriptSelectRow = "SelectAdminRow('" + DataBinder.Eval(e.Row.DataItem, "RowId").ToString().Replace("'", "") + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }

        protected void DeleteAdminTrackingRow(Object s, EventArgs e)
        {
            string strAdmintrackingSrvcOutput = string.Empty;
            XmlDocument xmlAdmintrackingDoc = new XmlDocument();
            XmlNode inputDocNode = null;
            XmlNode xmlNode = null;
            string serviceMethodToCall = string.Empty;
            XmlDocument xmlServiceDoc = null;

            try
            {
                inputDocNode = InputForCWS();
                xmlNode = inputDocNode.SelectSingleNode("Message/Call/Function");
                xmlNode.InnerXml = "TableManagerAdaptor.DeleteTable";
                string[] strSplit = currentSelRowId.Value.Split(new char[] { '|' });

                string strInputDoc = @"<form name='NewTable'>
                                    <group name='NewTable' selected='1' title='New Table'>
                                    <displaycolumn>
                                    <control name='UserTableName' title='User Table Name:' type='text'>" + strSplit[0].ToString() + @"</control> 
                                    </displaycolumn>
                                    <displaycolumn>
                                    <control name='SysTableName' title='System Table Name:' type='text'>" + strSplit[1].ToString() + @"</control> 
                                    <control name='TableType' title='Table type 8 for AdminTracking tables' >8</control> 
                                    </displaycolumn>
                                    </group>
                                    </form>";

                xmlNode = inputDocNode.SelectSingleNode("Message/Document");
                xmlNode.InnerXml = strInputDoc;

                strAdmintrackingSrvcOutput = AppHelper.CallCWSService(inputDocNode.InnerXml);

                ErrorControl1.errorDom = strAdmintrackingSrvcOutput;

                xmlServiceDoc = new XmlDocument();
                xmlServiceDoc.LoadXml(strAdmintrackingSrvcOutput);

                if ((xmlServiceDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
                {
                    BindToGridView();
                }
              
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }


            
   

        }
    }
}
