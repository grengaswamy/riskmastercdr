﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewTable.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.AdministrativeTracking.NewTable" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>New Table</title>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/Utilities.js"></script>
</head>
<body>
    <form id="frmData" name="frmData" runat="server">
    <div>
        <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    </div>
    
   <table class="toolbar" cellSpacing="0" cellPadding="0" border="0">
     <tbody>
      <tr>
       <td align="center" valign="middle" HEIGHT="32">
       <asp:ImageButton type="image" name="Save" src="../../../../Images/save.gif" alt="" 
                  id="btnSave" runat="server" 
                  onmouseover="&#xA;javascript:document.all[&#34;btnSave&#34;].src='../../../../Images/save2.gif';&#xA;" 
                  onmouseout="&#xA;javascript:document.all[&#34;btnSave&#34;].src='../../../../Images/save.gif';&#xA;" 
                  title="Save" OnClientClick="return NewTable_Save();" 
               onclick="btnSave_Click"  />
       
       </td>
      
      
      </tr>
     </tbody>
    </table>
   <br /><br />
   <div class="msgheader" id="formtitle">New Table</div>
  
   <table border="0">
        <tbody>
         <%--<tr>
          <td class="ctrlgroup" colSpan="2">New Table</td>
         </tr>--%>
         <tr>
         <input type="text" value="" style="display:none" id="RowId" runat="server" /></tr>
         <tr>
          <td><u>User Table Name</u>:&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <input type="text" value="" size="30" id="UserTableName" runat="server" /></div>
          </td>
         </tr>
         <tr>
          <td><u>System Table Name</u>:&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <input type="text" value="" size="30" id="SysTableName" runat="server" /></div>
          </td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" value="" style="display:none" id="TableType"  runat="server" /></td>
         </tr>
         <tr>
          <td>
          <input type="text" value="" style="display:none" id="Org_orglist"  runat="server" /></td>
         </tr>
         <tr>
          <td></td>
          <td>
          <input type="text" value="" style="display:none" id="FormMode" runat="server" /></td>
         </tr>
        </tbody>
       </table>
       
    
    </form>
</body>
</html>
