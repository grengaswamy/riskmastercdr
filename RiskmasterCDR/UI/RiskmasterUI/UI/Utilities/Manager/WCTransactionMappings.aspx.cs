﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;
using System.Text;

namespace Riskmaster.UI.Utilities
{
    public partial class WCTransactionMappings : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                if (!IsPostBack)
                {
                    GetWCTransData();
                }
        }


        private void GetWCTransData()
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            XmlDocument XmlDoc = new XmlDocument();
            WCBenefitsList.Text = "";
            DataTable dtGridData = new DataTable();
            WCBenefits.Items.Clear();
            WCTransactionType.Items.Clear();

            try
            {

                bReturnStatus = CallCWSFunction("WorkersCompTransMappingAdaptor.Get", out sreturnValue);
                ErrorControl1.errorDom = sreturnValue;
                if (bReturnStatus)
                {
                    XmlDoc.LoadXml(sreturnValue);
                }

                if (!IsPostBack)
                {
                    FillJurisdictionsCombo(XmlDoc.SelectSingleNode("ResultMessage/Document/form/control[@name ='WCJurisdictions']"), WCJurisdictions);
                }

                if (!FillJurisdictionsCombo(XmlDoc.SelectSingleNode("ResultMessage/Document/form/control[@name ='WCBenefits']"), WCBenefits))
                {
                    if (WCJurisdictions.SelectedIndex > 0)
                    {
                        ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script type='text/javascript'>alert('No benifit types found for this Jurisdiction.');</script>");
                    }
                }


                if (!FillJurisdictionsCombo(XmlDoc.SelectSingleNode("ResultMessage/Document/form/control[@name ='WCTransactionType']"), WCTransactionType))
                {
                    if (WCJurisdictions.SelectedIndex > 0)
                    {
                        ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script type='text/javascript'>alert('No transaction types found for Workers' Compensation.');</script>");
                    }
                }

                //XmlDoc.LoadXml(sreturnValue);
                XmlNode xmlNodeWCBenefitsList = XmlDoc.SelectSingleNode("ResultMessage/Document/form/control[@name ='WCBenefitsList']");
                if (xmlNodeWCBenefitsList != null)
                {
                    if (GridHeaderAndData(xmlNodeWCBenefitsList.SelectSingleNode("listhead"), xmlNodeWCBenefitsList.SelectNodes("listrow"), dtGridData))
                    {
                        gvWorkersCompensation.DataSource = dtGridData;
                        gvWorkersCompensation.DataBind();
                    }

                    if (xmlNodeWCBenefitsList.Attributes["value"] != null)
                    {
                        hdnWCBenefitsList.Text = xmlNodeWCBenefitsList.Attributes["value"].Value;
                    }
                }


            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        


        /// <summary>
        /// Fills the WCJurisdictions combo with the data returned from webservice
        /// </summary>
        /// <param name="objWCJurisdictionsList">xml node containing WCJurisdictions data</param>
        private bool FillJurisdictionsCombo(XmlNode objJurisdictionsList, DropDownList objJurisdictions)
        {
            ListItem objListItem = null;
            objJurisdictions.Items.Clear();
            
            if (objJurisdictionsList != null && objJurisdictionsList.InnerXml != "0" && objJurisdictionsList.InnerXml != "")
            {
                foreach (XmlElement objElement in objJurisdictionsList)
                {
                    objListItem = new ListItem(objElement.InnerText, objElement.GetAttribute("value"));
                    objJurisdictions.Items.Add(objListItem);
                }
            }

            if (objJurisdictions.Items.Count > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private XmlDocument GetMessageTemplate()
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
            <Message><Authorization></Authorization><Call><Function></Function></Call>
            <Document><form><control name='WCJurisdictions'>" + WCJurisdictions.SelectedValue + 
            "</control><control name='WCBenefits'>" + WCBenefits.SelectedValue + 
            "</control><control name='WCTransactionType'>" + WCTransactionType.SelectedValue +
            "</control><AllMappings>" + hdnWCBenefitsList.Text + "</AllMappings><UseCalc></UseCalc><DeleteMapping></DeleteMapping>" +
            "</form></Document></Message>"
            );

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;
        }

        protected void WCJurisdictions_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetWCTransData();
        }

        protected void btSaveMapping_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetMessageTemplate();
            XmlDocument XmlOutDoc = new XmlDocument();

            try
            {
                XmlNode xmlNode = xmlDoc.SelectSingleNode("Message/Call/Function");
                xmlNode.InnerXml = "WorkersCompTransMappingAdaptor.Save";
                string sreturnValue;

                sreturnValue = AppHelper.CallCWSService(xmlDoc.InnerXml);

                ErrorControl1.errorDom = sreturnValue;
               

                GetWCTransData();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
                   
        
        }


        private bool GridHeaderAndData(XmlNode objGridHeaders, XmlNodeList objGridData, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;
            bool blnheaderadded = false;

            if (objGridHeaders != null && objGridHeaders.InnerXml != "")
            {
                //to store value attribute of each row
                dcGridColumn = new DataColumn();
                dcGridColumn.ColumnName = "value";
                dtGridData.Columns.Add(dcGridColumn);

                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    //Add Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();
                                  // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.GetAttribute("colname");
                   

                    dtGridData.Columns.Add(dcGridColumn);

                    blnheaderadded = true;

                }
            }
            //Add rows to be associated with Datasource of grid
            if (objGridData != null && objGridData.Count != 0)
            {
               

                foreach (XmlNode objNodes in objGridData)
                {
                    dr = dtGridData.NewRow();
                    dr[0] = objNodes.Attributes["value"].Value;
                    
                    iColumnIndex = 1;
                    foreach (XmlElement objElem in objNodes)
                    {
                        dr[iColumnIndex++] = objElem.GetAttribute("title");

                    }
                    dtGridData.Rows.Add(dr);

                }
                //added by amrit for mits:14629
                dr = dtGridData.NewRow();
                dtGridData.Rows.Add(dr);
                //mits:14629
            }
            else
            {
                dr = dtGridData.NewRow();
                dtGridData.Rows.Add(dr);
            }

            return blnheaderadded;

        }

        private string SetRowsToDelete()
        {
            string strDeleteMapping = "";

            if (gvWorkersCompensation != null)
            {
                foreach (GridViewRow row in gvWorkersCompensation.Rows)
                {
                    // Access the CheckBox
                    CheckBox cb = (CheckBox)row.FindControl("selectchk");
                    if (cb != null && cb.Checked)
                    {
                        // Get the value associated with check box
                        strDeleteMapping = strDeleteMapping + cb.Attributes["value"] + " ";


                    }
                }
            }

            return strDeleteMapping.Trim();
        }

        protected void gvWorkersCompensation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Access the CheckBox
                CheckBox cb = (CheckBox)e.Row.FindControl("selectchk");
                cb.Attributes.Add("value", DataBinder.Eval(e.Row.DataItem, "value").ToString());  
              
            }
        }
        //added by amrit for mits:14629
        protected void gvWorkersCompensation_DataBound(object sender, EventArgs e)
        {
           gvWorkersCompensation.Rows[gvWorkersCompensation.Rows.Count-1].CssClass = "hiderowcol";
        }
       // mits:14629 ends
        protected void btDeleteMapping_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = GetMessageTemplate();
            XmlDocument XmlOutDoc = new XmlDocument();

            try
            {
                XmlNode xmlNode = xmlDoc.SelectSingleNode("Message/Document/form/DeleteMapping");
                xmlNode.InnerXml = SetRowsToDelete();

                xmlNode = xmlDoc.SelectSingleNode("Message/Call/Function");
                xmlNode.InnerXml = "WorkersCompTransMappingAdaptor.DeleteMapping";

                string sreturnValue;
                sreturnValue = AppHelper.CallCWSService(xmlDoc.InnerXml);
                GetWCTransData();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
          
        }



    }
}
