﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.Utilities.Manager
{
    /// <summary>
    /// Class Name : AdjusterScreenSetup
    /// Author : Animesh Sahai
    /// MITS 18738
    /// </summary>
    public partial class AdjusterScreenSetup : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();

        /// <summary>
        /// Event handler for the Page load event
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (AppHelper.GetQueryStringValue("LOBs") == (1).ToString())
                    {
                        this.Form.FindControl("div_MultiLOB").Visible = true;
                    }
                    if (AppHelper.GetQueryStringValue("Departments") == (1).ToString())
                    {
                        this.Form.FindControl("div_MultiOrg").Visible = true;
                    }
                    if (AppHelper.GetQueryStringValue("ClaimTypes") == (1).ToString())
                    {
                        this.Form.FindControl("div_MultiClaimType").Visible = true;
                    }
                    if (AppHelper.GetQueryStringValue("Jurisdictions") == (1).ToString())
                    {
                        this.Form.FindControl("div_MultiJurisdiction").Visible = true;
                    }
                    if (AppHelper.GetQueryStringValue("SkipWorkItems") == (1).ToString())
                    {
                        this.Form.FindControl("div_SkipWorkItems").Visible = true;
                    }
                    if (AppHelper.GetQueryStringValue("ForceWorkItems") == (1).ToString())
                    {
                        this.Form.FindControl("div_ForceWorkItems").Visible = true;
                    }
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    if (mode.Text.ToLower() == "edit")
                    {
                        txtSelectedid.Text = AppHelper.GetQueryStringValue("selectedid");
                        adjlastfirstname.ReadOnly = true;   
                        adjlastfirstnamebtn .Enabled = false;
                    }
                    else
                    {
                        adjlastfirstnamebtn.Enabled = true ;
                        adjlastfirstname.ReadOnly = false;
                        SkipWorkItems.Text = "0";
                        ForceWorkItems.Text = "0";
                    }
                }
                
                if (txtReloadNA.Text.ToLower() == "true")
                {
                    //MITS 26266 Manish Jain ,Passing reload template
                    XmlTemplate = GetMessageTemplate("reload");
                    CallCWSFunctionBind("AdjusterScreenAdaptor.ReloadNonAvail", out sCWSresponse, XmlTemplate);
                    txtReloadNA.Text = "";
                }
                else
                {
                    if (NonAvailGrid_RowDeletedFlag.Text.ToLower() == "true")
                    {
                        string selectedRowId = NonAvailGridSelectedId.Text;
                        XmlTemplate = GetMessageTemplate(Convert.ToInt32(selectedRowId));
                        CallCWSFunctionBind("NonAvailScreenAdaptor.Delete", out sCWSresponse, XmlTemplate);
                        NonAvailGrid_RowDeletedFlag.Text = "false";
                        //MITS 26266 Manish Jain ,Passing reload template
                        XmlTemplate = GetMessageTemplate("reload");
                        CallCWSFunctionBind("AdjusterScreenAdaptor.ReloadNonAvail", out sCWSresponse, XmlTemplate);

                    }
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";
                    Control c = DatabindingHelper.GetPostBackControl(this.Page);
                    if (c == null)
                    {
                        if (mode.Text.ToLower() == "edit")
                        {
                            XmlTemplate = GetMessageTemplate("edit");
                            CallCWSFunction("AdjusterScreenAdaptor.Get", XmlTemplate);
                        }
                        else
                        {
                            XmlTemplate = GetMessageTemplate("add");
                            BindData2Control(XmlTemplate, Page.Form.Controls);
                        }
                    }
                    
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Method to generate the XML template for Adjuster set up screen.
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="strValue">Mode of the screen</param>
        /// <returns></returns>
        private XElement GetMessageTemplate(string strValue)
        {
            StringBuilder sXml = new StringBuilder("");
            try
            {
                switch (strValue.ToLower())
                {
                    case "edit":
                        sXml = sXml.Append("<Message>");
                        sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                        sXml = sXml.Append("<Call><Function></Function></Call>");
                        sXml = sXml.Append("<Document><AdjusterScreens>");
                        sXml = sXml.Append("<NonAvailPlan><listhead>");
                        sXml = sXml.Append("<RowId>Rowid</RowId>");
                        sXml = sXml.Append("<StartDateTime type='Date'>Start Date Time</StartDateTime>");
                        sXml = sXml.Append("<EndDateTime type='Date'>End Date Time</EndDateTime>");
                        sXml = sXml.Append("</listhead></NonAvailPlan>");
                        sXml = sXml.Append("<control name='AutoAssignID' type='id'>");
                        sXml = sXml.Append(txtSelectedid.Text);
                        sXml = sXml.Append("</control>");
                        sXml = sXml.Append("<control name='AdjusterEID' type='entitylookup' codeid=''></control>");
                        sXml = sXml.Append("<control name='DeptEIds' type='multicode' lookuptype='orgh' codeid=''></control>");
                        sXml = sXml.Append("<control name='LOBCodes' type='multicode' lookuptype='LINE_OF_BUSINESS' codeid=''></control>");
                        sXml = sXml.Append("<control name='ClaimTypeCodes' type='multicode' lookuptype='CLAIM_TYPE' codeid=''></control>");
                        sXml = sXml.Append("<control name='JurisdictionCodes' type='multicode' lookuptype='STATES' codeid=''></control>");
                        sXml = sXml.Append("<control name='SkipWorkItems' type='numeric'></control>");
                        sXml = sXml.Append("<control name='ForceWorkItems' type='numeric'></control>");
                        sXml = sXml.Append("</AdjusterScreens>");
                        sXml = sXml.Append("</Document></Message>");
                        break;
                    case "add":
                        sXml = sXml.Append("<Document><AdjusterScreens>");
                        sXml = sXml.Append("<NonAvailPlan><listhead>");
                        sXml = sXml.Append("<RowId>Rowid</RowId>");
                        sXml = sXml.Append("<StartDateTime type='Date'>Start Date Time</StartDateTime>");
                        sXml = sXml.Append("<EndDateTime type='Date'>End Date Time</EndDateTime>");
                        sXml = sXml.Append("</listhead></NonAvailPlan></AdjusterScreens>");
                        sXml = sXml.Append("</Document>");
                        break;
                    case "normal":
                        sXml = sXml.Append("<Message>");
                        sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                        sXml = sXml.Append("<Call><Function></Function></Call>");
                        sXml = sXml.Append("<Document><AdjusterScreens>");
                        sXml = sXml.Append("<NonAvailPlan><listhead>");
                        sXml = sXml.Append("<RowId>Rowid</RowId>");
                        sXml = sXml.Append("<StartDateTime type='Date'>Start Date Time</StartDateTime>");
                        sXml = sXml.Append("<EndDateTime type='Date'>End Date Time</EndDateTime>");
                        sXml = sXml.Append("</listhead></NonAvailPlan>");
                        sXml = sXml.Append("</AdjusterScreens>");
                        sXml = sXml.Append("</Document></Message>");
                        break;
                    //MITS 26266 Manish Jain ,Created reload template
                    case "reload":
                        sXml = sXml.Append("<Message>");
                        sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                        sXml = sXml.Append("<Call><Function></Function></Call>");
                        sXml = sXml.Append("<Document><AdjusterScreens>");
                        sXml = sXml.Append("<NonAvailPlan><listhead>");
                        sXml = sXml.Append("<RowId>Rowid</RowId>");
                        sXml = sXml.Append("<StartDateTime type='Date'>Start Date Time</StartDateTime>");
                        sXml = sXml.Append("<EndDateTime type='Date'>End Date Time</EndDateTime>");
                        sXml = sXml.Append("</listhead></NonAvailPlan>");
                        sXml = sXml.Append("<control name='AutoAssignID' type='id'>");
                        sXml = sXml.Append(txtSelectedid.Text);
                        sXml = sXml.Append("</control>");
                        //sXml = sXml.Append("<control name='AdjusterEID' type='entitylookup' codeid=''></control>");
                        sXml = sXml.Append("<control name='DeptEIds' type='multicode' lookuptype='orgh' codeid=''></control>");
                        sXml = sXml.Append("<control name='LOBCodes' type='multicode' lookuptype='LINE_OF_BUSINESS' codeid=''></control>");
                        sXml = sXml.Append("<control name='ClaimTypeCodes' type='multicode' lookuptype='CLAIM_TYPE' codeid=''></control>");
                        sXml = sXml.Append("<control name='JurisdictionCodes' type='multicode' lookuptype='STATES' codeid=''></control>");
                        //sXml = sXml.Append("<control name='SkipWorkItems' type='numeric'></control>");
                        //sXml = sXml.Append("<control name='ForceWorkItems' type='numeric'></control>");
                        sXml = sXml.Append("</AdjusterScreens>");
                        sXml = sXml.Append("</Document></Message>");
                        break;

                }
                XElement oElement = XElement.Parse(sXml.ToString());
                return oElement;
            }
            finally
            {
                sXml = null; 
            }
        }
        /// <summary>
        /// Method to generate the XML template for Non availability plan grid.
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="iSelectedRowId">Selected record</param>
        /// <returns></returns>
        private XElement GetMessageTemplate(int iSelectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            try
            {
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document><AdjusterScreens>");
                sXml = sXml.Append("<NonAvailPlan><listhead>");
                sXml = sXml.Append("<RowId>Rowid</RowId>");
                sXml = sXml.Append("<StartDateTime type='Date'>Start Date Time</StartDateTime>");
                sXml = sXml.Append("<EndDateTime type='Date'>End Date Time</EndDateTime>");
                sXml = sXml.Append("</listhead></NonAvailPlan>");
                sXml = sXml.Append("<control name = 'NonAvailGrid'>");
                sXml = sXml.Append(iSelectedRowId.ToString());
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("</AdjusterScreens></Document>");
                sXml = sXml.Append("</Message>");
                XElement oElement = XElement.Parse(sXml.ToString());
                return oElement;
            }
            finally
            {
                sXml = null;  
            }
        }

        /// <summary>
        /// Event handler for the click event of the button btnOK
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate("normal");
                CallCWSFunction("AdjusterScreenAdaptor.Save", out sCWSresponse, XmlTemplate);
                if (ErrorControl1.Text == string.Empty)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        /// <summary>
        /// Event handler for the click event of the button btnCancel
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }
    }
}
