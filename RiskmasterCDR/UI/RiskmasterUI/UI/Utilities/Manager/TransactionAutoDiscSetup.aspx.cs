﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class TransactionAutoDiscSetup : NonFDMBasePageCWS
    {

        private bool bReturnStatus;
        private string sCWSresponse;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CallOnPageLoad();
            }
        }

        protected void CallOnPageLoad()
        {
            try
            {
                XmlDocument xmlOutDoc = new XmlDocument();
                XmlNode inputDocNode = null;
                sCWSresponse = string.Empty;

                inputDocNode = GetMessageTemplate("AutoDiscTransReserveMappingAdapter.Get");
                sCWSresponse = AppHelper.CallCWSService(inputDocNode.InnerXml);

                xmlOutDoc.LoadXml(sCWSresponse);
                BindData(xmlOutDoc);
               

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void BindData(XmlDocument xmlOutDoc)
        {

            XmlNodeList xmlNodeList = xmlOutDoc.SelectNodes("ResultMessage/Document/ReserveTransSetup/TransactionTypes/option");
            ListItem objItem;
            foreach (XmlNode objNodes in xmlNodeList)
            {
                objItem = new ListItem(HttpUtility.HtmlDecode(objNodes.InnerText), objNodes.Attributes["value"].Value);  //Aman MITS 27754
                lstTransType.Items.Add(objItem); 
            }

            xmlNodeList = xmlOutDoc.SelectNodes("ResultMessage/Document/ReserveTransSetup/ReserveTypes/option");
            foreach (XmlNode objNodes in xmlNodeList)
            {
                objItem = new ListItem(HttpUtility.HtmlDecode(objNodes.InnerText), objNodes.Attributes["value"].Value);  //Aman MITS 27754
                lstReserveType.Items.Add(objItem);
                
            }


            DataTable dtGridData = new DataTable();
            bool bBlankRowAdded;
            bBlankRowAdded = GridHeaderAndData(xmlOutDoc.SelectSingleNode("ResultMessage/Document/ReserveTransSetup/Mapping/listhead"), xmlOutDoc.SelectNodes("ResultMessage/Document/ReserveTransSetup/Mapping/option"), dtGridData);
            GrdMapping.DataSource = dtGridData;
            GrdMapping.DataBind();

            if (bBlankRowAdded)
            {
                GrdMapping.Rows[0].Visible = false;
            }
        
        }


        private bool GridHeaderAndData(XmlNode objGridHeaders, XmlNodeList objGridData, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;
            bool bBlankRowAdded = false;
            hdnAddedTransTypes.Text = "";
            hdnAddedReserveTypes.Text = "";
            if (objGridHeaders != null && objGridHeaders.InnerXml != "")
            {
                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    //Add Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();
                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.Name;

                    dtGridData.Columns.Add(dcGridColumn);

                }
            }
            //Add rows to be associated with Datasource of grid
            if (objGridData != null && objGridData.Count != 0)
            {
                foreach (XmlNode objNodes in objGridData)
                {
                    dr = dtGridData.NewRow();

                    iColumnIndex = 0;
                    foreach (XmlElement objElem in objNodes)
                    {
                        //dr[iColumnIndex++] = objElem.InnerXml; 
                        dr[iColumnIndex++] = HttpUtility.HtmlDecode(objElem.InnerText);  //Aman MITS 27754
                        if (objElem.Name == "TransCodeId")
                        {
                            hdnAddedTransTypes.Text = hdnAddedTransTypes.Text + objElem.InnerXml + ",";
                        }

                        if (objElem.Name == "ReserveCodeid")
                        {
                            hdnAddedReserveTypes.Text = hdnAddedReserveTypes.Text + objElem.InnerXml + ",";
                        }
                    }
                    dtGridData.Rows.Add(dr);

                }
            }
            else
            {
                dr = dtGridData.NewRow();
                dtGridData.Rows.Add(dr);
                bBlankRowAdded = true;
            }

            return bBlankRowAdded;

        }

        private XmlNode GetMessageTemplate(string sFunctionToCall)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();

            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
              <Call>
               <Function>" + sFunctionToCall + @"</Function> 
              </Call>
              <Document>
                <ReserveTransSetup>
                    <option>
                        <TransCodeId></TransCodeId>
                        <ReserveCodeid></ReserveCodeid>
                    </option>
                 <DeleteRowId>
                </DeleteRowId>                
                </ReserveTransSetup>
                <DeleteRowId>
                </DeleteRowId>
              </Document>
           </Message>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;
        }

        protected void SaveMapping(object sender, EventArgs e)
        {


            XmlDocument xmlOutDoc = new XmlDocument();
            XmlNode inputDocNode = null;
            sCWSresponse = string.Empty;

            try
            {
                inputDocNode = GetMessageTemplate("AutoDiscTransReserveMappingAdapter.SaveMapping");
                XmlNode objXNode = inputDocNode.SelectSingleNode("/Message/Document/ReserveTransSetup/option/TransCodeId");
                objXNode.InnerXml = lstTransType.SelectedItem.Value;

                objXNode = inputDocNode.SelectSingleNode("/Message/Document/ReserveTransSetup/option/ReserveCodeid");
                objXNode.InnerXml = lstReserveType.SelectedItem.Value;


                sCWSresponse = AppHelper.CallCWSService(inputDocNode.InnerXml);

                CallOnPageLoad();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void DeleteMapping(object sender, EventArgs e)
        {
            XmlDocument xmlOutDoc = new XmlDocument();
            XmlNode inputDocNode = null;
            sCWSresponse = string.Empty;

            try
            {
                inputDocNode = GetMessageTemplate("AutoDiscTransReserveMappingAdapter.DeleteMapping");
                XmlNode objXNode = inputDocNode.SelectSingleNode("/Message/Document/ReserveTransSetup/DeleteRowId");
                objXNode.InnerXml = hdnSelected.Text;

                sCWSresponse = AppHelper.CallCWSService(inputDocNode.InnerXml);
                hdnSelected.Text = "";
                CallOnPageLoad();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void GrdMapping_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}
