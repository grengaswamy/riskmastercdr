﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class ClaimTypeVersusACORDFormMapping : NonFDMBasePageCWS
    {
        public IEnumerable result = null;
        public IEnumerable resultClaimAccord = null;
        public XElement rootElement = null;
        string sreturnValue = "";
        bool bReturnStatus = false;

        protected void Page_Load(object sender, EventArgs e)
        { 
            if (!IsPostBack)
            {
                GetAccordClaimMapping();               
            }
        }

        protected void saveMapbtn_Click(object sender, EventArgs e)
        {
            hdnAcordParam.Text =Convert.ToString(lstACORDForm.SelectedItem.Value );
            hdnClaimParam.Text = Convert.ToString(lstClaimType.SelectedItem.Value);
            bReturnStatus = CallCWS("ClaimTypeVsAcordFormSetupAdaptor.Save", null, out sreturnValue, true, true);
            GetAccordClaimMapping();
        }

        protected void delMapbtn_Click(object sender, EventArgs e)
        {
            hdnAcordParam.Text  = hdnSelected.Value;
            hdnClaimParam.Text  = "";
            bReturnStatus = CallCWS("ClaimTypeVsAcordFormSetupAdaptor.DeleteMapping", null, out sreturnValue, true, true);
            GetAccordClaimMapping();
        }

        private void GetAccordClaimMapping()
        {
            XmlDocument claimAccXmlDoc = new XmlDocument();
            lstACORDForm.Items.Clear();
            lstClaimType.Items.Clear();
            try
            {
                bReturnStatus = CallCWS("ClaimTypeVsAcordFormSetupAdaptor.Get", null, out sreturnValue, true, true);
                if (bReturnStatus)
                {
                    claimAccXmlDoc.LoadXml(sreturnValue);
                    rootElement = XElement.Parse(claimAccXmlDoc.OuterXml);
                    result = from accordClaimData in rootElement.XPathSelectElements("//control[@name='AcordClaimTypeList']/listrow")
                             select accordClaimData;                  

                    resultClaimAccord = from accordFormData in rootElement.XPathSelectElements("//control[@name='ACORDForm']/option")
                                   select accordFormData;

                    foreach (XElement item in resultClaimAccord)
                    {
                        lstACORDForm.Items.Add(new ListItem(item.Value, item.Attribute("value").Value));
                    }

                    resultClaimAccord = from c in rootElement.XPathSelectElements("//control[@name='ClaimType']/option")
                                   select c;
                    foreach (XElement item in resultClaimAccord)
                    {
                        lstClaimType.Items.Add(new ListItem(item.Value, item.Attribute("value").Value));
                    }
                }
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
       
    }
}
