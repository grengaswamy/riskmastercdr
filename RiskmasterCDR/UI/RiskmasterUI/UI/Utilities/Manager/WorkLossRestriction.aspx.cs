﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class WorkLossRestriction : NonFDMBasePageCWS
    {
        const string WCLOB = "243";
        protected void Page_Load(object sender, EventArgs e)
        {          
            if (!IsPostBack)
            {       
                BindData();
                //As the LOB was set when the checkbox was clicked hardcoding the LOB on page load so that it works in case checkbox is not
                //clicked.
                lineofbusinesscode.Value = WCLOB;
            }

        }
        /// <summary>
        /// CWS request message template for fetching grid data
        /// </summary>
        /// <returns></returns>
        //rsharma220 MITS 34565: Added setting node
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
            <Call>
              <Function>WorkLossRestrictionsAdaptor.Get</Function> 
              </Call>
            <Document>
               <WorkLossRestrictionList>
						<listhead>
							    <LOBId type='code'>Line of Business</LOBId>
                                <RecordType>Record Type</RecordType>                                
                                <TransTypeCode type='code'>Transaction Type</TransTypeCode>
							    <RowId>RowId</RowId>
                      </listhead>
                    <Setting>
                            <CarrierClaim>Carrier Claim</CarrierClaim> 
                    </Setting>
				</WorkLossRestrictionList>
             </Document>
          </Message>
          
            ");
            return oTemplate;
        }

        /// <summary>
        /// server event to cater to add record scenario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Add_Record_Click(object sender, EventArgs e)
        {
            bool bError = false;
            //Test if record already exists
            foreach (GridViewRow oRow in ((GridView)(WorkLossRestrictionsGrid.FindControl("gvData"))).Rows)
            {
                if (oRow.Cells[3].Text.ToLower().Trim() == LOBId.SelectedItem.Text.ToLower().Trim() &&
                oRow.Cells[5].Text.ToLower().Trim() == RecordType.SelectedValue.ToLower().Trim() &&
                (((TextBox)TransTypeCode.FindControl("codelookup")).Text.ToLower().Trim().Replace(" ", "").Contains(oRow.Cells[7].Text.ToLower().Trim().Replace(" ", "")) ||
                oRow.Cells[7].Text.ToLower().Trim().Replace(" ", "").Contains(((TextBox)TransTypeCode.FindControl("codelookup")).Text.ToLower().Trim().Replace(" ", ""))))
                {
                    bError = true;
                    break;
                }

            }

            if (!bError)
            {

                NonFDMCWSPageLoad("WorkLossRestrictionAdaptor.Save");

                BindData();

            }
            else
            {
                ErrorControl1.Text =  ErrorHelper.FormatErrorsForUI("Record already exists." , true);
            }
        }

        /// <summary>
        /// server event to cater for delete scenario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Remove_Record_Click(object sender, EventArgs e)
        {
            //Preparing XML to send to service
            XElement oMessageElement = GetDeleteMessageTemplate();

            XElement oElement = oMessageElement.XPathSelectElement("./Document/WorkLossRestrictionList/control[@name='RowId']");
            if (oElement != null)
            {
                oElement.Value = RowId.Text;
            }

            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            ErrorControl1.errorDom = sReturn;

            if (!ErrorControl1.errorFlag)
            {
                BindData();
            }


        }

        /// <summary>
        /// CWS request message template for deleting grid row
        /// </summary>
        /// <returns></returns>
        private XElement GetDeleteMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
            <Call>
              <Function>WorkLossRestrictionAdaptor.Delete</Function> 
              </Call>
            <Document>
               <WorkLossRestrictionList>
						<control name='RowId'>
						</control>
				</WorkLossRestrictionList>
             </Document>
          </Message>
          
            ");
            return oTemplate;
        }

        private void BindData()
        {
            string sCWSresponse = "";

            //Preparing XML to send to service
            XElement oMessageElement = GetMessageTemplate();

            //Calling Service to get all PreBinded Data 
            CallCWS("WorkLossRestrictionsAdaptor.Get", oMessageElement, out sCWSresponse, false, true);

            //Binding Error Control
            ErrorControl1.errorDom = sCWSresponse;

            //rsharma220 MITS 34565 Start
            XElement oReturn = XElement.Parse(sCWSresponse);
            if (!string.IsNullOrEmpty(oReturn.XPathSelectElement("//Settingrow").Value))
            {
                string sMsgStatusCode = oReturn.XPathSelectElement("//Settingrow").Value;

                if (sMsgStatusCode.Equals("true"))
                {
                    LOBId.Items[1].Enabled = false;
                }
            }
            //rsharma220 MITS 34565 End
            //Blanking out transaction type code
            ((TextBox)Page.FindControl("TransTypeCode").FindControl("codelookup")).Text = "";

            //Blanking out rowid marked for deletion
            RowId.Text = "";
        }
    }
}
