﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Text;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class ClaimTypeChangeOptions : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            try
            {
                if (ClaimTypeGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = ClaimTypeSelectedId.Text;
                    XmlTemplate = GetMessageTemplate(selectedRowId);
                    CallCWS("ClaimTypeChangeOptionAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                    ClaimTypeGrid_RowDeletedFlag.Text = "false";
                }
                
                XmlTemplate = GetMessageTemplate();
                CallCWSFunctionBind("ClaimTypeChangeOptionsAdaptor.Get", out sCWSresponse, XmlTemplate);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<ClaimList><listhead>");
            sXml = sXml.Append("<Lob type='code'>Line Of Business</Lob>");
            sXml = sXml.Append("<CurClaimType type='code'>Current Claim type</CurClaimType>");
            sXml = sXml.Append("<NewClaimType type='code'>New Claim type</NewClaimType>");
            sXml = sXml.Append("<TrigType type='code'>Triggering Transaction Type</TrigType>");
            sXml = sXml.Append("<IncSchChecks type='code'>Include AutoChecks</IncSchChecks>");
            sXml = sXml.Append("<RowId>RowId</RowId>");
            sXml = sXml.Append("</listhead></ClaimList>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
        private XElement GetMessageTemplate(string selectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ClaimTypeChangeOption>");
            sXml = sXml.Append("<DeletionListName>");
            sXml = sXml.Append("ClaimList");
            sXml = sXml.Append("</DeletionListName>");
            sXml = sXml.Append("<control name='RowId'>");
            sXml = sXml.Append(selectedRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</ClaimTypeChangeOption></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
    }
}
