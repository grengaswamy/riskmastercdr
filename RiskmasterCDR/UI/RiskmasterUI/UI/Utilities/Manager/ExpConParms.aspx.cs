﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.Xml;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    /// <summary>
    /// coded by: Ashutosh Kashyap : akashyap3
    /// completed on 10-Feb-2009
    /// </summary>
    public partial class ExpConParms : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");

                    //// Added By Amit Bansal
                    if (mode.Text.ToLower() != "add")
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("ExpConParmsAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);
                    }
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("ExpConParmsAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate()
        {
            if (mode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                //string strRowId = AppHelper.GetQueryStringValue("selectedrowposition");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>f5a2fbdc-8a07-417c-99fb-d662df89adbc</Authorization>");
            sXml = sXml.Append("<Call><Function>ExpConParmsAdaptor.Get</Function></Call>");
            sXml = sXml.Append("<Document><Document><ExpCon>");
            sXml = sXml.Append("<use>" + useExpCon.Checked.ToString() + "</use>");
            sXml = sXml.Append("<amount>" + Amount.Text.Trim() + "</amount>");
            sXml = sXml.Append("<effectivedate>"+EffectiveDate.Text.Trim() +" </effectivedate>");
            sXml = sXml.Append("<expirationdate>"+ExpirationDate.Text.Trim() +"</expirationdate>");
            sXml = sXml.Append("<expcontype codeid='" + ExpConType.CodeIdValue + "'>" + ExpConType.CodeTextValue + "</expcontype>");
            sXml = sXml.Append("<lob codeid='" + LOBcode.CodeIdValue + "'>" + LOBcode.CodeTextValue + "</lob>");
            sXml = sXml.Append("<state codeid='" + Statecode.CodeIdValue + "'>" + Statecode.CodeTextValue + "</state>");
            sXml = sXml.Append("<expconrowid>" + RowId.Text + "</expconrowid>");
            sXml = sXml.Append("</ExpCon></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }
        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("ExpConParmsAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    //After the validation,there was a need to post back the parent page through javascript 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
