﻿<%@ Page Title="" Language="C#" theme="RMX_Default" MasterPageFile="~/UI/Utilities/UtilityTemplate.Master" AutoEventWireup="true" CodeBehind="NewHoliday.aspx.cs" Inherits="Riskmaster.UI.Utilities.NewHoliday" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <title>Holiday Setup</title>
</asp:content>
<asp:content ID="Content2" ContentPlaceHolderID="cphUtilityBody" runat="server">

    <table border="0" width="100%">
        <tr>
            <td>
                <asp:imagebutton id="btSave" runat="server" ImageUrl="~/Images/save.gif" ToolTip="Save"/>
            </td>
        </tr>
        <tr>
            <td class="msgheader" colspan="2">
                Holiday Setup
            </td>
        </tr>
        
        <tr>
            <td class="ctrlgroup" colspan="2">
                Holiday Setup
            </td>
        </tr>
    
        <tr>
            <td>
                Date :
            </td>
            <td>
                <asp:textbox id="txtHolidayDate" name="txtHolidayDate" runat="server" />
                <cc1:calendarextender id="ceHolidayDate" targetcontrolid="txtHolidayDate" runat="server" />
                <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" 
                    errormessage="Please Enter Date" controltovalidate="txtHolidayDate" />
            </td>
        </tr>

        <tr>
            <td>
                Description :
            </td>
            <td>
                <asp:textbox id="txtDescription" name="txtDescription" runat="server" />
                <asp:requiredfieldvalidator id="rfvDescription" runat="server" errormessage="Please enter Description" controltovalidate="txtDescription" />
            </td>
        </tr>
        
        <tr>
            <td>
                Organization :
            </td>
            <td>
                <asp:listbox id="lbOrganization" name="lbOrganization" runat="server" />
                <asp:button id="btAddOrganization" name="btAddOrganization" runat="server" text="..." causesvalidation="false"/>
                <asp:button id="btRemoveOrganization" name="btRemoveOrganization" runat="server" text="-" causesvalidation="false"/>
                <asp:requiredfieldvalidator id="rfvOrganization" runat="server" errormessage="Please select an Organization" controltovalidate="lbOrganization" />
            </td>
        </tr>
    
    </table>

</asp:content>
