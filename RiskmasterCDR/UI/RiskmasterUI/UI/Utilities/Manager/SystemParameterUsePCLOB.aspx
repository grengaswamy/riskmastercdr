﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SystemParameterUsePCLOB.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.SystemParameterUsePCLOB" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Activation Code Required</title>
    
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
    <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">{var i;}</script>
    <script language="javascript" type="text/javascript" src="../../../Scripts/Utilities.js"></script>
</head>
<body class="10pt" onload="SystemParameterUsePCLOB_Load();">
  <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  <input type="hidden" id="wsrp_rewrite_action_1" name="" value="">
  <div class="msgheader" id="formtitle">Activation Code Required - Property Claim</div>
   <div class="errtextheader"></div>
   <table border="0">
    <tbody>
     <tr>
      <td>
       <table border="0">
        <tbody>
         <tr>
          <td>
          <asp:HiddenField ID="hdnValue" runat="server" />
          <asp:Label ID="lblActivation" type="titleOrlabel" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Label']" runat="server" Text=""></asp:Label>
          </td>
         </tr>
         <tr>
          <td><asp:Label runat="server" ID="Activation_temp" Text="Activation Code"/> &nbsp;
          </td>
          <td>
          <asp:TextBox ID="password" title="" TextMode="Password" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Password']" value="" size="30" runat="server" autocomplete="off"></asp:TextBox>
          </td>
         </tr>
         <tr>
          <td>
         <asp:TextBox ID="LabelMsg"  style="display:none" value="Incorrect activation code. Please contact RISKMASTER sales if you are interested in purchasing the RISKMASTER Property Claim." rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='LabelMsg']" runat="server"></asp:TextBox>
           </td>
         </tr>
         <tr>
          <td></td>
          <td>
          <asp:TextBox ID="FormMode" type="hidden" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FormMode']" value="" style="display:none" runat="server"></asp:TextBox>
          </td>
         </tr>
         <tr>
          <td></td>
          <td>
           <asp:TextBox ID="Success" type="hidden" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Success']" value="" style="display:none" runat="server"></asp:TextBox>
          </td>
         </tr>
         <tr>
          <td>
           <asp:TextBox ID="LOB" type="hidden" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='LOB']" value="PC" style="display:none" runat="server"></asp:TextBox>
          </td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td>
          <script language="JavaScript" type="text/javascript">
          {var i;}
          </script>
          <input type="button" class="button" name="Ok" value="OK" onclick="SystemParameterUseLOB_Ok();" id="Ok" />
          </td>
         </tr>
        </tbody>
       </table>
       <input type="text" name="" value="" id="SysViewType" style="display:none" />
       <input type="text" name="" value="" id="SysCmd" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdQueue" style="display:none">
       <input type="text" name="" value="" id="SysCmdText" style="display:none" rmxforms:value="Navigate"/>
       <input type="text" name="" value="" id="SysClassName" style="display:none" rmxforms:value=""/>
       <input type="text" name="" value="" id="SysSerializationConfig" style="display:none"/>
       <input type="text" name="" value="" id="SysFormIdName" style="display:none" rmxforms:value="RowId"/>
       <input type="text" name="" value="" id="SysFormPIdName" style="display:none" rmxforms:value="RowId"/>
       <input type="text" name="" value="" id="SysFormPForm" style="display:none" rmxforms:value="SystemParameterUsePCLOB"/>
       <input type="text" name="" value="" id="SysInvisible" style="display:none" rmxforms:value=""/>
       <input type="text" name="" value="" id="SysFormName" style="display:none" rmxforms:value="SystemParameterUsePCLOB"/>
       <input type="text" name="" value="" id="SysRequired" style="display:none"/>
       </td>
      <td valign="top"></td>
     </tr>
    </tbody>
   </table>
   <input type="hidden" name="$node^5" value="" id="SysWindowId">
   </form>
 </body>
</html>
