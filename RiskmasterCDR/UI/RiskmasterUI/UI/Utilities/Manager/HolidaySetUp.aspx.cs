﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class HolidaySetUp : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();
      
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes

                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("HolidayAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);
                    }
                }
            }
            catch (Exception ee)
            {

                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("HolidayAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                Org_orglist.Text = ""; //zmohammad MITS 31442
            }

        }

        private XElement GetMessageTemplate()
        {
            string strDate = string.Empty;
            string strOrgId = string.Empty;
            if (mode.Text.ToLower() == "edit")
            {
                string strSelectedRowId = AppHelper.GetQueryStringValue("selectedid");
                if(!string.IsNullOrEmpty(Date.Text))
                    strDate = Date.Text;
                else
                    strDate = strSelectedRowId.Substring(0, strSelectedRowId.IndexOf("|"));
                strOrgId = strSelectedRowId.Substring(strSelectedRowId.IndexOf("|") + 1);
                OrgId.Text = strOrgId; //current OrgId
            }
            else
            {
                strDate = Date.Text;
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><Holidays>");
            sXml = sXml.Append("<control name='RowId' type='id'>");
            sXml = sXml.Append(RowId.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Date' type='date'>");
            sXml = sXml.Append(strDate);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Desc' type='text'>");
            sXml = sXml.Append(Description.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Org' currentid='" + OrgId.Text + "' orgid='" + strOrgId + "' type='orglist'>");
            sXml = sXml.Append(Org.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Org_orglist' type='hidden'>");
            sXml = sXml.Append(Org_orglist.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</Holidays></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }

    }
}
