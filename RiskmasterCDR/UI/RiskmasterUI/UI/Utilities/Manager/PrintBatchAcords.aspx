<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintBatchAcords.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.JurisdictionalOptions.PrintBatchAcords" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
      <title>Acord Batch Printing Options</title>
       
        <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
      <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css"/>
      <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
      <script type ="text/jscript" language="JavaScript" src="../../../Scripts/WaitDialog.js">          { var i; }</script> 
         <%--vkumar258 - RMA-6037 - Starts --%>
<%-- <script src="../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
      <script type="text/javascript" language="JavaScript" src="../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
      <script type="text/javascript" language="JavaScript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
      <script type="text/javascript" language="JavaScript" src="../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>     
<link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
         <script type="text/javascript">
             var ns, ie, ieversion;
             var browserName = navigator.appName;
             var browserVersion = navigator.appVersion;


             function ValidateDateActivityLog() {
                 var sfromdate = document.forms[0].FromDate.value;
                 var stodate = document.forms[0].ToDate.value;
                 var stxtFromDate = sfromdate.substring(6, 10) + sfromdate.substring(0, 2) + sfromdate.substring(3, 5);
                 var stxtToDate = stodate.substring(6, 10) + stodate.substring(0, 2) + stodate.substring(3, 5);
                 if (stxtToDate != "" && stxtFromDate > stxtToDate) {
                     alert("From Date must be less than To Date");
                     document.forms[0].FromDate.value = "";
                     document.forms[0].ToDate.value = "";
                 }
             }
             //MITS 21800-start
             function SelectUnselectAll(bSelect) {

                 var gridElementsCheckBox = document.getElementsByName('MyCheckBox');
                 if (bSelect == true) {
                     if (gridElementsCheckBox != null && gridElementsCheckBox.length > 0) {
                         document.forms[0].PrintBatchAcordSelectedId.value = '';
                         for (var i = 0; i < gridElementsCheckBox.length - 1; i++) {
                             var gridName = gridElementsCheckBox[i].name;
                             if (gridName == "PrintBatchAcordGrid") {
                                 gridElementsCheckBox[i].checked = bSelect;

                                 if (document.forms[0].PrintBatchAcordSelectedId.value == "")
                                     document.forms[0].PrintBatchAcordSelectedId.value = gridElementsCheckBox[i].value;
                                 else
                                     document.forms[0].PrintBatchAcordSelectedId.value = document.forms[0].PrintBatchAcordSelectedId.value + ' ' + gridElementsCheckBox[i].value;


                             }
                         }
                     }
                 }
                 else if (bSelect == false) {
                     if (gridElementsCheckBox != null && gridElementsCheckBox.length > 0) {
                         document.forms[0].PrintBatchAcordSelectedId.value = '';
                         for (var i = 0; i < gridElementsCheckBox.length - 1; i++) {
                             var gridName = gridElementsCheckBox[i].name;
                             if (gridName == "PrintBatchAcordGrid") {
                                 gridElementsCheckBox[i].checked = bSelect;
                             }
                         }
                     }
                     document.forms[0].PrintBatchAcordSelectedId.value = '';
                 }
                 
                 return false;

             }
             //MITS 21800-end
             function SelectAcord() 
             {

                 if (document.forms[0].State1.checked == true) 
                 {
                     return true;
                 }
                 else if (document.forms[0].State2.checked == true) 
                 {
                     return true;
                 }
                 else 
                 {
                     alert("Please select among Date of Claim/Date Created");
                     return false;
                 }

             }
             //nzafar-21800
             function ProcessSelectAcord() 
             {
                     if (document.forms[0].PrintBatchAcordSelectedId.value == "") 
                     {
                         alert("Please select Acord to process");
                         return false;
                     }

                 
             }
                    

	  </script>
</head>

<body class="" onload="parent.MDIScreenLoaded();">
  <form id="frmData"  method="post" runat="server">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  
   
   <div class="msgheader" id="formtitle">Print Batch Acords Options</div>
   <div class="errtextheader"></div>
   
   <table border="0">  
         <tr><td><asp:TextBox runat="server"  value="1" style="display:none" type="id" id="RowId" rmxref="Instance/Document/form/group/displaycolumn/control[@name='RowId']" /></td></tr>
         
         <tr id="DateofClaim">
            <td colspan="2">
            <asp:RadioButton ID="State1" type="radio" value="0" rmxref="Instance/Document/form/group/displaycolumn/control[@name='State1']" onclick="setDataChanged(true);" runat="server" GroupName="RB0" />Select Claim by Date of Claim</td>           
          <td><asp:Label runat="server"  id="Label1"  type="labelonly" Text="From: " /></td>
          <td>
            <asp:TextBox runat="server" FormatAs="date" ID="FromDate"  RMXRef="Instance/Document/form/group/displaycolumn/control[@name='FromDate']"
                TabIndex="1" onchange="setDataChanged(true);ValidateDateActivityLog();" onblur="dateLostFocus(this.id);ValidateDateActivityLog();" />
                   <%--vkumar258 - RMA-6037 - Starts --%>
<%--<asp:Button class="DateLookupControl" runat="server" ID="Button2" TabIndex="2" />
            <script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "FromDate",
					        ifFormat: "%m/%d/%Y",
					        button: "Button2"
					    }
					    );
            </script>--%>
<script type="text/javascript">
    $(function () {
        $("#FromDate").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
      </td>          
           <td><asp:Label runat="server"  id="Label3"  type="labelonly" Text="To: " /></td> 
           <td>
            <asp:TextBox runat="server" FormatAs="date" ID="ToDate" RMXRef="Instance/Document/form/group/displaycolumn/control[@name='ToDate']"
                TabIndex="3" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
           <%-- <asp:Button class="DateLookupControl" runat="server" ID="Button1" TabIndex="4" />
            <script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "ToDate",
					        ifFormat: "%m/%d/%Y",
					        button: "Button1"
					    }
					    );
            </script>--%>
               <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#ToDate").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "4");
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
        </td>
    </tr>
         <tr id="DateCreated">
            <td colspan="2">
            <asp:RadioButton ID="State2"  rmxref="Instance/Document/form/group/displaycolumn/control[@name='State2']" type="radio" value="1" onclick="setDataChanged(true);" GroupName="RB0" runat="server" />Select Claim by Date Created</td> 
         </tr> 
         
         <tr>                  
            <td>Printed Claims to include:</td>          
            <td>
                <asp:DropDownList ID="PrintedClaimsToInclude"  rmxref="./Document/PassToWebService/PrintedClaimsToInclude/@value" onchange="setDataChanged(true);" type="combobox" runat="server">                
                 <asp:ListItem Value="-1">No Item Selected</asp:ListItem>
                 <asp:ListItem Value="0">Only Batch</asp:ListItem>
                 <asp:ListItem Value="1">Only Single</asp:ListItem>
                 <asp:ListItem Value="2">Both batch and Single</asp:ListItem>                 
                </asp:DropDownList>
            </td>
            <td> 
                <asp:button  name="Refresh" align="right" id ="btnRefersh" Text="&nbsp;&nbsp;Refresh &nbsp;&nbsp;"
                class="button"  runat ="server" onClick="btnRefersh_Click" OnClientClick="return SelectAcord()"></asp:Button>
            </td>    
         </tr>         
        <tr>
         
            <table border="0">
                                  <tbody>          
                                        <tr>
                                            <td width="100%">
                                                <div style="&#xa; width: 600px; height: 320px; &#xa; &#xa;">                                                  
                                                    <span>
                                                        <dg:usercontroldatagrid runat="server" id="PrintBatchAcordGrid" gridname="PrintBatchAcordGrid"
                                                             target="/Document/PassToWebService/PrintBatchAcord" ref="Instance/Document/form/group/displaycolumn/control[@name ='PrintBatchAcordGrid']"
                                                            unique_id="RowId" ShowCheckBox="true" HideButtons="New|Edit|Delete|Clone" onclick="KeepRowForEdit('PrintBatchAcordGrid');"
                                                            width="100%" height="280px" hidenodes="|RowId|" showheader="true" linkcolumn="" Type="Grid" RowDataParam ="Option"/>
                                                    </span>         
                                                    <asp:TextBox Style="display: none" runat="server" ID="PrintBatchAcordSelectedId" RMXType="id" />                                           
                                                </div>
                                            </td>                                           
                                        </tr>                                    
                                    </tbody>
                     </table>            
        </tr>
             <tr>
                    <td>
                    <%--//MITS 21800---%>
                        <asp:Button  name="SelectedAll" align="right" id ="btnSelectALL" Text="&nbsp;&nbsp;Select All &nbsp;&nbsp;"
                            class="button"  runat ="server" 
      onclientClick="return SelectUnselectAll(true)"></asp:Button>
                    </td>
                    <td>
                        <asp:Button  name="UnSelectedAll" align="right" id ="btnUnSelectALL" Text="&nbsp;&nbsp;UnSelect All &nbsp;&nbsp;"
                            class="button"  runat ="server" 
      onclientClick="return SelectUnselectAll(false)"></asp:Button>
                    </td>
                    <td>
                        <asp:Button  name="OK" align="right" id ="btnOK" Text="&nbsp;&nbsp;Process Selected &nbsp;&nbsp;"
                            class="button"  runat ="server" 
      onClick="btnOK_Click" onclientClick="return ProcessSelectAcord()"></asp:Button> <%--//nzafar-21800--%>
                    </td>            
                    <td>
                        <asp:Button  name="Cancel" id ="btnCancel" Text="&nbsp;&nbsp;Cancel &nbsp;&nbsp;"
                            class="button"  runat ="server" ></asp:Button>
                                <%--//MITS 21800---%>
                    </td>
             </tr>
         <%--<tr>
                    <td><asp:TextBox  type="hidden" runat="server" value="1" style="display:none" id="hdDataChanged" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdDataChanged']"/></td>
         </tr>   --%>     
       </table>
     
  </form> 
 </body>
</html>