﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SupplementalData.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.SupplementalData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Supplemental Data</title>
    <link rel="stylesheet" href="../../../../Content/system.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script language="javascript" type="text/javascript" >
        var qString = "";
        
        //Setting query string on row selection
        function SelectSupplemental(strRowId)
        {
            //qString =  strRowId;
           

        }

        function SelRadio(radio) {
            qString = radio.value;
            

        }
    </script>
</head>
<body class="10pt" onload="parent.MDIScreenLoaded();">
    <form id="frmSupplementalData" runat="server">
    <div>
     
        <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
     
    </div>
    <div>
       <table  id="tblGrid" runat="server"  width="780px" cellspacing="0" cellpadding="0" border="0">
         <tr>
				<td colspan="2" class="ctrlgroup">
                    <asp:Label ID="lblSuppData" runat="server" Text="<%$ Resources:lblSuppData %>"></asp:Label>   
				</td>
		</tr>
		<tr>
		        <td colspan="2"><b></b></td>
		</tr>
		<tr>
		       <td width="95%" colspan="1" class="singleborder" >
		          <div id="divGrid" style="overflow:auto; height:470px"  >
		           <asp:GridView ID="GridView1" runat="server" AllowPaging="false" Width="95%"  AutoGenerateColumns="false"  
                            GridLines="None" onrowdatabound="GridView1_RowDataBound" >
		              <HeaderStyle CssClass="msgheader" />
		              <AlternatingRowStyle CssClass="data2" />
		              <Columns>
		                  <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                      <input type="radio" id="selectrdo" onclick="SelRadio(this);" value='<%# DataBinder.Eval(Container, "DataItem.RowId") %>'  name="SupplementalDataGrid"  />
                                    </ItemTemplate> 
                          </asp:TemplateField> 
                          <asp:TemplateField HeaderText="Row Id" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RowId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrTableName %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserTableName")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrSystemTableName %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblSysTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SysTableName")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
		              </Columns> 
		           </asp:GridView>
		           </div>
		       </td>
		       <td width="5%" valign="top" colspan="1">
		         <input type="image" runat="server"  src="../../../../Images/edittoolbar.gif" title="<%$ Resources:ttEdit %>" onclick="openGridAddEditWindow('SupplementalDataGrid', 'edit', 800, 450); return false;"  
		         id="Edit_SupplementalDataGrid" onmouseover="&#xA;javascript:document.all[&#34;Edit_SupplementalDataGrid&#34;].src='../../../../Images/edittoolbar2.gif';&#xA;" 
		         onmouseout="&#xA;javascript:document.all[&#34;Edit_SupplementalDataGrid&#34;].src='../../../../Images/edittoolbar.gif';&#xA;"
		         />
		       </td>
		</tr>
		 
		 
       </table>
    </div>
    </form>
</body>
</html>
