﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GridProperty.aspx.cs" EnableEventValidation="false" ValidateRequest="false" Inherits="Riskmaster.UI.Utilities.Manager.SupplementalFieldData.GridProperty" %>

<%@ Register src="../../../Shared/Controls/ErrorControl.ascx" tagname="ErrorControl" tagprefix="uc1" %>

<%--<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc2" %>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Grid Property</title>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script type="text/javascript" language="javascript">
      
       function EnableAddCol()
       {
          if(CheckColCount())
          {
           divGrid.style.display = "none"
           divAddCol.style.display = "";
          }
       }
       
       function EnableGrid() {
           document.getElementById('txtColName').value = "";
           divGrid.style.display = ""
           divAddCol.style.display = "none";
       }
       function trim(str) {
           if (!str || typeof str != 'string')
               return null;
           return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
         }
       
       function VerifyColName() {

           if (trim(document.getElementById('txtColName').value) == "" || trim(document.getElementById('txtColName').value) == null) {
              alert("Column name cannot be blank");
              return false;
             
          }
         for(var intgrdCellcount = 0 ; intgrdCellcount < 50 ; intgrdCellcount++)
         {
           objCell = document.getElementById('grdhd' + intgrdCellcount);
           
           if(objCell != null && objCell.innerText == document.getElementById('txtColName').value)
           {
             alert("Column with this name already exists in the grid");
             return false;
             break;
          }
           
         }
       }
       
       function SelectColumn(cellIndex)
       {
         var objCell ;
         
         for(var intgrdCellcount = 0 ; intgrdCellcount < 50 ; intgrdCellcount++)
         {
           objCell = document.getElementById('grd' + intgrdCellcount);
           if(objCell != null)
           {
           objCell.style.backgroundColor = "#ff9933";
           }
           else
           {
           break;
           }
         }
         
         objCell = document.getElementById('grd' + cellIndex);
         objCell.style.backgroundColor = "#ff6600";
         
         document.getElementById('hdnRemove').value = document.getElementById('grdhd' + cellIndex).innerText;
         document.getElementById('buttonRemove').disabled = false;      
         alert("To remove the above column from the grid click remove button");
         
         
       }
       
       function CheckColCount()
       {
         if(document.getElementById('maxCols').value != "" && document.getElementById('colCount').value == document.getElementById('maxCols').value)
         {
           alert("You have reached the maximum allowable columns limit. No more columns can be added to the Grid");
           return false;
         }
         return true;
         
       }
       
       function ConfirmPopup()
       {
          if(document.getElementById('colCount').value != "")
          {
                var alertMsg = "By removing any column you will lose the associated data. Are you sure you want to remove selected column? ";
	            if( parseInt(document.getElementById('colCount').value) < 2)
	            {
	              var alertStr = "There should be at least 1 column in the Grid. ";
	              alertMsg = alertStr + alertMsg;
	            }      
	            var answer = confirm(alertMsg);
	                if (answer){
		                return true;
	                }
	                else{
		                return false;
	                }
	      }
	      else
	      {
	        return false;
	      }
       }
       
       function MoveToParent()
       {
         if(document.getElementById('colCount').value != "0" && document.getElementById('colCount').value != "") {

             if (document.getElementById('colCount').value < document.forms[0].minCols.value) {
                 alert('Minimum allowed columns in grid is ' + document.forms[0].minCols.value + '. Please add atleast ' + document.forms[0].minCols.value + ' column(s) to continue.');
                 return false;
             }
             //Added:Yukti,Dt01/02/2014, MITS 34768
         //window.opener.forms[0].hdnGridXml.value = document.getElementById('hdnGridXml').value;
         //window.opener.forms[0].hdnColCnt.value = document.getElementById('colCount').value;
         //window.opener.forms[0].ColCount.value = document.getElementById('colCount').value;
         //window.opener.forms[0].hdnColName.value = document.getElementById('hdnColumns').value;
             //window.opener.forms[0].Columns.value = document.getElementById('hdnColumns').value;
             //Bharani - MITS : 35826 - Start
             window.opener.document.getElementById('hdnGridXml').value = document.getElementById('hdnGridXml').value;
             //var str = document.getElementById('hdnGridXml').value;
             //str = str.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
             //window.opener.document.getElementById('hdnGridXml').value = str;             
             //Bharani - MITS : 35826 - End
             window.opener.document.getElementById('hdnColCnt').value = document.getElementById('colCount').value;
             window.opener.document.getElementById('ColCount').value = document.getElementById('colCount').value;
             window.opener.document.getElementById('hdnColName').value = document.getElementById('hdnColumns').value;
             window.opener.document.getElementById('Columns').value = document.getElementById('hdnColumns').value;
                
             //window.opener.forms[0].submit();
             window.opener.document.forms[0].submit();
             //Ended:Yukti,Dt01/02/2014, MITS 34768
         window.close();
         }
         else
         {
             alert('Minimum allowed columns in grid is ' + document.forms[0].minCols.value + '. Please add atleast ' + document.forms[0].minCols.value + ' column(s) to continue.');
            
         }
       }
    </script>
   
   </head>
<body>
    <form id="frmData" name="frmData" runat="server">
    <div id="divGrid" runat="server">
    <input type="hidden"  value="" runat="server" id="hdnIsExistingColSet" />
        <input type="hidden"  value="" runat="server" id="hdnColumns" />
        <input type="hidden"  value="" runat="server" id="hdnGridXml" />
        <input type="hidden"  value="" runat="server" id="colXML" />
        <input type="hidden"  value="" runat="server" id="colName" />
        <input type="hidden"  value="" runat="server" id="colCount" />
        <input type="hidden"  value="" runat="server" id="colToBeRemoved" />
        <input type="hidden"  value="" runat="server" id="columns" />
        <input type="hidden"  value="" runat="server" id="maxCols" />
        <input type="hidden"  value="" runat="server" id="minCols" />
        <input type="hidden"  value="" runat="server" id="duplicateField" />
        <input type="hidden"  value="" runat="server" id="sysFieldName" />
        <input type="hidden"  value="" runat="server" id="rowId" />
        <input type="hidden"  value="" runat="server" id="existingColumns" />
        <input type="hidden"  value="" runat="server" id="gridProperty" />
        <input type="hidden"  value="" runat="server" id="hdnRemove" />
        <table border="0">
        <tr>
          <td>
           
              <%--<uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"></uc2:PleaseWaitDialog>--%>
           
          </td>
        </tr>
        <tr>
          <td>
          
              <uc1:ErrorControl ID="ErrorControl1" runat="server" />
          
          </td>
        </tr>
        <tr>
        <td class="msgheader" colspan="5" id="GridHeader" runat="server" >GridView</td>
        </tr>
        <tr>
         <td></td>
         <td>
          <div id="gridContainer" style="width:450px;height:150px;overflow:scroll">
            <asp:GridView  ID="grdView" runat="server" onrowdatabound="grdView_RowDataBound">
            <HeaderStyle BackColor="#4183b5" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle"  />
            <RowStyle BackColor="#f2f3f8" />
           
            </asp:GridView> 
          </div>
         </td>
        </tr>
        <tr>
         <td></td>
         <td><input type="button"  id="btnAdd"  value="Add Column" class="button" onclick="EnableAddCol();" />
         <asp:Button ID="buttonRemove" Text="Remove" class="button" runat="server"  OnClientClick="return ConfirmPopup();" 
                 onclick="buttonRemove_Click" />
         <asp:Button id="btnRemvAll" Text="Remove All" class="button" runat="server" 
                 onclick="btnRemvAll_Click" OnClientClick="return ConfirmPopup();" />
          <input id="btnClose" value="Close" type="button" class="button" runat="server" onclick="MoveToParent()"  />
         <input type="hidden" value="" id="hdnaction" runat="server" />
     
        </tr>
       </table><input type="hidden" name="$node^5" value="rmx-widget-handle-6" id="SysWindowId" />
     </div>
     
     
     <div id="divAddCol" runat="server" style="display: none" >
        <table>
            <tr>
             <td colspan="2">	&nbsp;&nbsp;	&nbsp;</td>
             <td colspan="2">	&nbsp;&nbsp;	&nbsp;</td>
             <td colspan="2">	&nbsp;&nbsp;	&nbsp;</td>
             <td colspan="2">	&nbsp;&nbsp;	&nbsp;</td>
             <td colspan="2">	&nbsp;&nbsp;	&nbsp;</td>
             <td class="msgheader" colspan="5">Add Column</td>
            </tr>
            <tr>
             <td colspan="2"></td>
             <td colspan="2"></td>
             <td colspan="2"></td>
             <td colspan="2"></td>
             <td colspan="2"></td>
             <td></td>
             <td>
              Column Name
              :<input type="text"  value="" id="txtColName" runat="server" maxlength="50" onkeypress="checkEnter(event)" /></td>
            </tr>
            <tr>
             <td colspan="2"></td>
             <td colspan="2"></td>
             <td colspan="2"></td>
             <td colspan="2"></td>
             <td colspan="2"></td>
             <td></td>
             <td><asp:Button Text="OK" id="btnOK" class="button" runat="server" OnClientClick="return VerifyColName(); "   
                     onclick="btnOK_Click"  />
             <input type="submit" name="$action^" value="Cancel" id="btncancel" class="button" onclick="EnableGrid(); " />
             <script language="javascript" type="text/javascript">
              document.getElementById('buttonRemove').disabled = true;
             
             if(document.getElementById('hdnaction').value == "")
             {
                 //Added:Yukti,Dt01/02/2014, MITS 34768
              //document.getElementById('hdnGridXml').value = window.opener.forms[0].hdnGridXml.value  ;
              //document.getElementById('colCount').value = window.opener.forms[0].hdnColCnt.value ;
                 //document.getElementById('hdnColumns').value = window.opener.forms[0].hdnColName.value ;
                 document.getElementById('hdnGridXml').value = window.opener.document.getElementById('hdnGridXml').value;
                 document.getElementById('colCount').value = window.opener.document.getElementById('hdnColCnt').value;
                 document.getElementById('hdnColumns').value = window.opener.document.getElementById('hdnColName').value;
                 //Ended:Yukti,Dt01/02/2014, MITS 34768
              document.getElementById('hdnaction').value = "true";
              document.forms[0].submit();
              }
              
            
             </script>
             </td>
            </tr>
           </table>
     
     
     </div>
    </form>
</body>
</html>
