﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.Utilities.Manager.SupplementalFieldData
{
    public partial class TableField : System.Web.UI.Page
    {

        private bool isEditMode = false;
        private Hashtable objHashSourceFldValue = new Hashtable();

        protected void Page_Init(object sender, EventArgs e)
        {

        }
          /// <summary>
          /// Page load event
          /// </summary>
          /// <param name="sender"></param>
          /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Request.QueryString["tabletype"] != null)
            {
                TableType.Value = AppHelper.GetQueryStringValue("tabletype");
            }

            if (Request.QueryString["systablename"] != null)
            {
                SysTableName.Value = AppHelper.GetQueryStringValue("systablename");
                if (hdnXmlFormName.Value == "")
                {
                    switch (TableType.Value)
                    {
                        case "6" :
                            XmlDocument xmlNodeDoc = new XmlDocument();
                            xmlNodeDoc.Load(Server.MapPath("~/App_Data/SupplementalUpgradeList.xml"));
                            XmlElement objXmlSourceElement = (XmlElement)xmlNodeDoc.SelectSingleNode("SystemTableNameMapping/SystemTable[@name ='" + SysTableName.Value + "']");
                            hdnXmlFormName.Value = objXmlSourceElement.InnerXml;
                            break;
                        case "8" :
                            hdnXmlFormName.Value = "admintracking.xml";
                            break;
                        case "9" :
                            hdnXmlFormName.Value = "";
                            break;
                     }
                }
            }

            if (AppHelper.GetQueryStringValue("selectedid") != "")
            {
                RowId.Value = Request.QueryString["selectedid"];
                FormTitle.Text = "Modify Field";
                isEditMode = true;
                SubHeading.InnerText = "Modify Field";//MGaba2:MITS 17194:Wrong label shown on Modify Grid screen
            }
            else
            {
                FormTitle.Text = "Add Field";
                freeform.Checked = true;//abansal23: MITS 14287
                SubHeading.InnerText = "Add Field";//MGaba2:MITS 17194:Wrong label shown on Modify Grid screen
            }

            if (!IsPostBack)
            {
                GetFieldData();
            }

            
            if (GAListbtn.Visible == true && SourceField.SelectedIndex >= 1)
            {
                string[] strSplit = SourceField.SelectedValue.Split(new Char[] { '*' });
                MultiCodeLookup1.CodeTable = strSplit[0];
            }
            else
            {
                MultiCodeLookup1.CodeTable = "";
            }
            

            if (!isEditMode)
            {
                TableField_EnableDisable();
            }
            if (!((AppHelper.ReadCookieValue("EnableFAS").Equals("-1")) && (FASSupplementalTableList.IsTableFASReportable(SysTableName.Value))))
            {
                FASReportable.Style.Add("display", "none");
            }
         
        }

        /// <summary>
        /// Sends filled/empty xml to webservice and gets xml with data 
        /// </summary>
        private void GetFieldData()
        {
            XmlDocument xmlDoc = null;
            XmlDocument xmlServiceDoc = null;
            XmlNode xmlNode = null;
            SupplementalHelper suppHelper = null;
            string serviceMethodToCall = string.Empty;
            string strServiceOutput = string.Empty;
//Start:Added by Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010       
            XmlNode xmlEnhNode = null;
            XmlNode xmlTrackNode = null;
            string sEnhXml = string.Empty;
            string sTrackXml = string.Empty;
//End:Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
            try
            {
                xmlDoc = InputDocForTableField();
                serviceMethodToCall = "TableManagerAdaptor.GetField";

                suppHelper = new SupplementalHelper();
                strServiceOutput = suppHelper.CallCWSFunctionForSupplemental("", xmlDoc, serviceMethodToCall);
                ErrorControl1.errorDom = strServiceOutput;

                xmlServiceDoc = new XmlDocument();
                xmlServiceDoc.LoadXml(strServiceOutput);
                //Start:Added by Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
                xmlTrackNode = xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/internal[@name ='TrackPolFlag']");
                if (xmlTrackNode != null)
                {
                    sTrackXml = xmlTrackNode.InnerText;
                }
                if (sTrackXml == "0")
                {
                    FieldType.Items.FindByText("Policy Tracking Lookup").Enabled = false;
                }

                xmlEnhNode = xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/internal[@name ='EnhPolFlag']");
                if (xmlEnhNode != null)
                {
                    sEnhXml = xmlEnhNode.InnerText;
                }
                if (sEnhXml == "0")
                {
                    FieldType.Items.FindByText("Policy Management Lookup").Enabled = false;
                }
                //End:Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
                FillSourceFieldCombo(xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='SourceField']"));

                if (isEditMode)
                {
                     FillDataToControl(xmlServiceDoc);
                     TableField_EnableDisable();
                }
                
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            finally
            {
                 xmlEnhNode = null;
                 xmlTrackNode = null;
            }

        }


        /// <summary>
        /// Event which gets called the change of drop down field type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FieldType_SelectedIndexChanged(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = null;
            XmlDocument xmlServiceDoc = null;
            XmlNode xmlNode = null;
            SupplementalHelper suppHelper = null;
            string serviceMethodToCall = string.Empty;
            string strServiceOutput = string.Empty;
            try
            {
                hdnFieldType.Value = FieldType.SelectedValue;
                xmlDoc = InputDocForTableField();
                serviceMethodToCall = "TableManagerAdaptor.GetField";

                suppHelper = new SupplementalHelper();
                strServiceOutput = suppHelper.CallCWSFunctionForSupplemental("", xmlDoc, serviceMethodToCall);
                ErrorControl1.errorDom = strServiceOutput;

                xmlServiceDoc = new XmlDocument();
                xmlServiceDoc.LoadXml(strServiceOutput);

                FillCodeFileCombo(xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='CodeFileId']"));

                TableField_EnableDisable();


            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }


        /// <summary>
        /// Sets enable/disable of form controls based on selection
        /// </summary>
        private void TableField_EnableDisable()
        {
            filetype.InnerText = "Code File:  ";
            ModifyGrid.Enabled = false;
            freeform.Disabled = true;
            patterned.Disabled = true;
            Pattern.Enabled = false;
            fldsize.Visible = false;
            CodeFileTr.Visible = false;
            ControlChanged.Value = "FIELDTYPE";
            hdnCodeFile.Value = "CODEFILE";
            if (TableType.Value != "8")         //csingh7 MITS 17361
                lookUpFldTr.Visible = false;
            DeleteFlag.Disabled = true;
            trHtmltextconfig.Visible = false;
            //sharishkumar Jira 6415 starts
            UserlookupTr.Visible = false;
            MultiSelectTr.Visible = false;
            //sharishkumar Jira 6415 ends

            switch (FieldType.SelectedValue)
            {
                case "6":
                case "5":
                case "14":
                    ControlChanged.Value = "FIELDTYPE";
                    CodeFileTr.Visible = true;
                    hdnCodeFile.Value = "CODEFILE";
                    break;
                case "8":
                case "16":
                    CodeFileTr.Visible = true;
                    filetype.InnerText = "Entitiy File:  ";
                    hdnCodeFile.Value = "ENTITYFILE";
                    ControlChanged.Value = "FIELDTYPE";
                    break;
                case "0":
                    freeform.Disabled = false;
                    patterned.Disabled = false;
                    fldsize.Visible = true;
                    Pattern.Enabled = false;//abansal23: MITS 14287
                    break;
                case "17":
                    ModifyGrid.Enabled = true;
                    break;
                case "31"://MITS # 29793 - 20120926 - bsharma33 - BR Id # 5.1.22 
                    RequiredFlag.Checked = false;
                    RequiredFlag.Disabled = true;
                    hdnCodeFile.Value = "";
                    break;
                //sharishkumar Jira 6415 starts
                case "22":
                    MultiSelectTr.Visible = true;
                    UserlookupTr.Visible = true;
                    break;
                //sharishkumar Jira 6415 ends
                case "25"://asharma326 JIRA 6422 for HTML Text
                    trHtmltextconfig.Visible = true;
                    break;
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                case "23":
                    CodeFileTr.Visible = true;
                    filetype.InnerText = "Hyperlink :  ";
                    hdnCodeFile.Value = "HYPERLINKFILE";
                    ControlChanged.Value = "FIELDTYPE";
                    break;
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                default:
                    hdnCodeFile.Value = "";
                    break;

            }

            if (isEditMode)
            {
                FieldType.Enabled = false;
                freeform.Disabled = true;
                patterned.Disabled = true;
                Pattern.Enabled = false;
                SysFieldName.Disabled = true;
                DeleteFlag.Disabled = false;
                FieldSize.Disabled = true;
                if (IsPattern.Value != "0")
                {
                    fldsize.Visible = false;
                }
                // npadhy - RMA - 6415 We should not allow the user to edit the type of User lookup (MultiSelect or SinglrSelect, User or Group).
                MultiSelect.Disabled = true;
                ddUsersGroups.Enabled = false;
            }
            //For adminTacking Screen
            if (TableType.Value == "8" || TableType.Value == "9")
            {
                GrpAssocTR.Visible = false;
                GrpAssocTR2.Visible = false;
                GrpAssocTR3.Visible = false;
            }
            
        }


        /// <summary>
        /// Fills the source field combo with the data returned from webservice
        /// </summary>
        /// <param name="objSourceFieldList">xml node containing source field data</param>
        private void FillSourceFieldCombo(XmlNode objSourceFieldList)
        {

            ListItem objListItem = null;
            SourceField.Items.Clear();
            objHashSourceFldValue.Clear();
            foreach (XmlElement objElement in objSourceFieldList)
            {
                objListItem = new ListItem(objElement.InnerText, objElement.GetAttribute("value") + "," + objElement.GetAttribute("fieldname"));
                if (!objHashSourceFldValue.Contains(objElement.GetAttribute("value")))
                {
                    objHashSourceFldValue.Add(objElement.GetAttribute("value"), objElement.GetAttribute("value") + "," + objElement.GetAttribute("fieldname"));
                    SourceField.Items.Add(objListItem);
                }
                
            }
           
        }
        
        /// <summary>
        /// Fills the code field combo with the data returned from webservice
        /// </summary>
        /// <param name="objCodeFileList">xml node containing source field data</param>
        private void FillCodeFileCombo(XmlNode objCodeFileList)
        {
            ListItem objListItem = null;
            CodeFileId.Items.Clear();
            
            if (objCodeFileList.InnerXml != "0")
            {
                foreach (XmlElement objElement in objCodeFileList)
                {
                    objListItem = new ListItem(Server.HtmlDecode(objElement.InnerText), objElement.GetAttribute("value"));
                    CodeFileId.Items.Add(objListItem);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SourceField_SelectedIndexChanged(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = null;
            XmlDocument xmlServiceDoc = null;
            XmlNode xmlNode = null;
            SupplementalHelper suppHelper = null;
            string serviceMethodToCall = string.Empty;
            string strServiceOutput = string.Empty;

            try
            {
                xmlDoc = InputDocForTableField();
                serviceMethodToCall = "TableManagerAdaptor.GetField";


                suppHelper = new SupplementalHelper();
                strServiceOutput = suppHelper.CallCWSFunctionForSupplemental("", xmlDoc, serviceMethodToCall);
                ErrorControl1.errorDom = strServiceOutput;

                xmlServiceDoc = new XmlDocument();
                xmlServiceDoc.LoadXml(strServiceOutput);
                string strGAlistType = "";
                if (xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name='GAList']") != null)
                {
                    XmlElement objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name='GAList']");
                    strGAlistType = objXmlSourceElement.GetAttribute("type");
                }

                if (strGAlistType != "eidlookup")
                {
                    //changed by Gagan for MITS 18708/18739 : start
                    if (strGAlistType == "orglist")
                    {
                        strGAlistType = (SourceField.SelectedItem.Value.Split(new Char[] { '*' })[0]);
                    }
                    else if(strGAlistType == "entitylookup")
                    {
                        strGAlistType = "eidlookup";
                    }
                    else
                    {
                        strGAlistType = SourceField.SelectedItem.Text.Trim();
                    }
                }
                //changed by Gagan for MITS 18708/18739 : end

                switch (strGAlistType.ToUpper())
                    {
                        case "DEPARTMENT":
                        case "FACILITY":
                        case "LOCATION":
                        case "DIVISION":
                        case "REGION":
                        case "OPERATION":
                        case "COMPANY":
                        case "CLIENT":
                            OrgControls.Visible = true;
                            GAListbtn.Visible = false;
                            entLookup.Visible = false;
                            hdnorgLevel.Value = strGAlistType.ToUpper();//abansal23 for MITS 14847
                            break;
                        case "EIDLOOKUP":
                            entLookup.Visible = true;
                            OrgControls.Visible = false;
                            GAListbtn.Visible = false;
                            //changed by Gagan for MITS 18708/18739 : start
                            //Case when Source Field is Insurer in Policy Tracking Supplementals
                            if (SourceField.SelectedItem.Text == "Insurer")
                            {                                
                                GAEntLookupbtn.Attributes.Add("onclick", "lookupData('GAEntLookup','INSURERS',4,'GAEntLookup',2)");
                            }
                            //changed by Gagan for MITS 18708/18739 : end   
                            break;
                        default:
                            OrgControls.Visible = false;
                            GAListbtn.Visible = true;
                            entLookup.Visible = false;
                            break;
                    }
                
                //clearing controls value

                ListBox multiCod = (ListBox)MultiCodeLookup1.FindControl("multicode");
                multiCod.Items.Clear();

                GAList.Items.Clear();//abansal23 on 05/15/2009 for MITS 14847
                TextBox txtBox = (TextBox)MultiCodeLookup1.FindControl("multicode_lst");
                txtBox.Text = "";
                GAEntLookup_cid.Value = "";
                GAList.Value = "";
                GAList_lst.Value = "";
                GAList_cid.Value = "";
                GAListHidden.Value = "";

               
                
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }


        /// <summary>
        /// sends xml with data to web servicce for save operation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            XmlDocument xmlDoc = null;
            XmlDocument xmlServiceDoc = null;
            XmlNode objectXmlNode = null;
            SupplementalHelper suppHelper = null;
            string serviceMethodToCall = string.Empty;
            string strServiceOutput = string.Empty;
            string strCodes = "";
            try
            {
                
                xmlDoc = InputDocForTableField();
                serviceMethodToCall = "TableManagerAdaptor.SaveField";

                if (GAListbtn.Visible)
                {
                    objectXmlNode = xmlDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='GAList']");
                    objectXmlNode.InnerXml = MultiCodeLookup1.ListCodes;
                    objectXmlNode.Attributes["codeid"].Value = MultiCodeLookup1.ListCodes;

                    objectXmlNode = xmlDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='GAListHidden']");
                    objectXmlNode.InnerXml = MultiCodeLookup1.ListCodes;
                }
                else
                {
                    
                    objectXmlNode = xmlDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='GAList']");
                    objectXmlNode.InnerXml = GAListHidden.Value.Trim();
                    objectXmlNode.Attributes["codeid"].Value = GAListHidden.Value.Trim();
                    objectXmlNode = xmlDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='GAListHidden']");
                    objectXmlNode.InnerXml = GAListHidden.Value;

                }

                suppHelper = new SupplementalHelper();
                strServiceOutput = suppHelper.CallCWSFunctionForSupplemental("", xmlDoc, serviceMethodToCall);

                ErrorControl1.errorDom = strServiceOutput;

                xmlServiceDoc = new XmlDocument();
                xmlServiceDoc.LoadXml(strServiceOutput);

                //bkuzhanthaim  : RMA-803
                if (xmlServiceDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success")
                {
                    //Ritesh:Start-MITS 26890 
                    if ((xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/internal[@name ='Duplicate_Flag']").InnerText == "Yes"))
                    {
                        ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script type='text/javascript'>alert('Duplicate System Field Name not allowed');</script>");
                    }
                    else
                    {
                       ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                    }
                    //Ritesh:End-MITS 26890 
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
           
        }

        #region XMLinput/output


        private void FillDataToControl(XmlDocument xmlServiceDoc)
        {
            XmlElement objXmlSourceElement;
            XmlNode objXmlNode;
            if (xmlServiceDoc != null)
            {
                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='RowId']");
                RowId.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='DeleteFlag']");
                DeleteFlag.Value = objXmlSourceElement.InnerXml;
                if (DeleteFlag.Value.ToLower() == "-1")
                {
                    DeleteFlag.Checked = true;
                }
                else
                {
                    DeleteFlag.Checked = false;
                }


                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='UserPrompt']");
                UserPrompt.Value = HttpUtility.HtmlDecode(objXmlSourceElement.InnerXml);   //Ritesh-MITS 27533-Handling special characters

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='SysFieldName']");
                SysFieldName.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='RequiredFlag']");
                RequiredFlag.Value = objXmlSourceElement.InnerXml;

                if (RequiredFlag.Value.ToLower() == "true")
                {
                    RequiredFlag.Checked = true;
                }
                else
                {
                    RequiredFlag.Checked = false;
                }
                //Asharma326 MITS 32386 Starts
                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='FASReportableflag']");
                FASReportableflag.Value = objXmlSourceElement.InnerXml;

                if (FASReportableflag.Value.ToLower() == "true")
                {
                    FASReportableflag.Checked = true;
                }
                else
                {
                    FASReportableflag.Checked = false;
                }
                //Asharma326 MITS 32386 Ends
                //MGaba2: In case hist tracking is on for that field
                //then an alert needs to be prompted to user in case of deletion
                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/control[@name ='hdnHistoryTracking']");
                if (objXmlSourceElement != null)
                {
                    hdnHistTrack.Value = objXmlSourceElement.InnerXml;
                }

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='LookupFlag']");
                LookupFlag.Value = objXmlSourceElement.InnerXml;

                if (LookupFlag.Value.ToLower() == "true")   //csingh7 MITS 17361
                {
                    LookupFlag.Checked = true;
                }
                else
                {
                    LookupFlag.Checked = false;
                }

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn//control[@id ='free-form']");
                freeform.Value = objXmlSourceElement.GetAttribute("value");

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn//control[@name ='IsPattern']");
                IsPattern.Value = objXmlSourceElement.InnerXml;
                if (IsPattern.Value == "0")
                {
                    freeform.Checked = true;
                }
                else
                {
                    patterned.Checked = true;
                }

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn//control[@id ='patterned']");
                patterned.Value = objXmlSourceElement.GetAttribute("value");

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn//control[@name ='Pattern']");
                if (objXmlSourceElement.GetAttribute("value") != "")
                {
                    for (int itemsCount = 0; itemsCount < Pattern.Items.Count; itemsCount++)
                    {
                        if (Pattern.Items[itemsCount].Text == objXmlSourceElement.GetAttribute("value"))
                        {
                            Pattern.SelectedIndex = itemsCount;
                            break;
                        }
                    }
                }

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='FieldSize']");
                FieldSize.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='SourceField']");
                if (objHashSourceFldValue.Count > 0)
                {
                    SourceField.Text = objHashSourceFldValue[objXmlSourceElement.GetAttribute("value")].ToString();
                }

                if(SourceField.SelectedItem != null)
                    hdnorgLevel.Value = SourceField.SelectedItem.Text;//abansal23 on 05/18/2009 for MITS 14847
                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='FieldType']");
                FieldType.Text = objXmlSourceElement.GetAttribute("value");

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='GrpAssoFlag']");
                GrpAssoFlag.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='GAListHidden']");
                GAListHidden.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='ListType']");
                ListType.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='AssoField']");
                AssoField.Value = objXmlSourceElement.InnerXml;


                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='Columns']");
                Columns.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='ColCount']");
                ColCount.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='SuppTblName']");
                SuppTblName.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='HelpContext']");
                HelpContext.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='GridXML']");
                hdnGridXml.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='Columns']");
                hdnColName.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='ColCount']");
                hdnColCnt.Value = objXmlSourceElement.InnerXml;

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='CodeFileId']");
                FillCodeFileCombo(xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='CodeFileId']"));

                if (objXmlSourceElement.GetAttribute("value") != "")
                {
                    for (int indexCount = 0; indexCount < CodeFileId.Items.Count; indexCount++)
                    {
                        if (CodeFileId.Items[indexCount].Value == objXmlSourceElement.GetAttribute("value"))
                        {
                            CodeFileId.SelectedIndex = indexCount;
                            break;
                        }
                    }
                }

                // npadhy - Starts - JIRA 6415 Set the value of Controls in Edit mode
                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='MultiSelect']");
                MultiSelect.Value = objXmlSourceElement.InnerXml;
                if (MultiSelect.Value.ToLower() == "-1" || MultiSelect.Value.ToLower() == "true")
                {
                    MultiSelect.Checked = true;
                }
                else
                {
                    MultiSelect.Checked = false;
                }

                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='ddUsersGroups']");
                ddUsersGroups.SelectedValue = objXmlSourceElement.GetAttribute("value");

                // npadhy - Ends - JIRA 6415 Set the value of Controls in Edit mode


                //asharma326 JIRA# 6422 Starts 
                objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='chkhtmltextconfig']");
                chkhtmltextconfig.Value = objXmlSourceElement.InnerXml;
                if (chkhtmltextconfig.Value.Equals("-1") || chkhtmltextconfig.Value.ToLower() == "true")
                {
                    chkhtmltextconfig.Checked = true;
                }
                else
                {
                    chkhtmltextconfig.Checked = false;
                }
                //asharma326 JIRA# 6422 Ends
                
        
                objXmlNode = xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='GAList']");
                if (((XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='GAList']")).GetAttribute("type") == "codelist")
                {
                    GAListbtn.Visible = true;
                    OrgControls.Visible = false;
                    entLookup.Visible = false;
                    ListItem objListItem = null;
                    foreach (XmlElement objElement in objXmlNode)
                    {
                        objListItem = new ListItem(objElement.InnerXml, objElement.GetAttribute("value"));
                        MultiCodeLookup1.AddItems = objListItem;
                        MultiCodeLookup1.AddListCodes = objElement.GetAttribute("value");
                    }
                }
                //changed by Gagan for MITS 18708/18739 : start
                else if (((XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='GAList']")).GetAttribute("type") == "eidlookup" || 
                    ((XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='GAList']")).GetAttribute("type") == "entitylookup")
                {
                    OrgControls.Visible = false;
                    GAListbtn.Visible = false;
                    entLookup.Visible = true;
                    foreach (XmlElement objElement in objXmlNode)
                    {
                        GAEntLookup.Value = objElement.InnerXml;
                        GAListHidden.Value = objElement.GetAttribute("value");
                    }
                    string sTable = ((XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='GAList']")).GetAttribute("codetable");
                    if (sTable != "")
                    {
                        sTable = (sTable.Split(new Char[] { '*' })[0]);
                        if (sTable.ToUpper() == "INSURERS")
                        {
                            GAEntLookupbtn.Attributes.Add("onclick", "lookupData('GAEntLookup','INSURERS',4,'GAEntLookup',2)");
                        }
                    }
                 //changed by Gagan for MITS 18708/18739 : end
                }
                else
                {
                    OrgControls.Visible = true;
                    GAListbtn.Visible = false;
                    entLookup.Visible = false;
                    ListItem objListItem = null;
                    foreach (XmlElement objElement in objXmlNode)
                    {
                        objListItem = new ListItem(objElement.InnerXml, objElement.GetAttribute("value"));
                        GAList.Items.Add(objListItem);
                        GAListHidden.Value = GAListHidden.Value + objElement.GetAttribute("value") + " ";
                    }
                }

              // objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='GAListHidden']");
               //GAListHidden.Value = objXmlSourceElement.InnerXml;
               GAListHidden.Value = GAListHidden.Value.Trim();

            }

        }

        /// <summary>
        /// Prepares xml instance to be sent to webservice
        /// </summary>
        /// <returns>input xml to webservice</returns>
        private XmlDocument InputDocForTableField()
        {
//Added by Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking, two hidden flag(EnhPolFlag and TrackPolFlag) at the end of XML ,06/10/2010
//Ritesh-MITS 27533-Handling special characters in UserPrompt
            XmlDocument xmlNodeDoc = new XmlDocument();
            XmlElement objNewElm = null;
            XmlNode objectXmlNode = null;
            string strAssocField = string.Empty;
            XElement oTemplate = XElement.Parse(@"
             <form name='TableField' supp='' title=''>
                <group name='TableField' selected='1' title='Add Field'>
                <displaycolumn>
                 <control name='RowId' type='id' /> 
                 <control name='FieldType' required='yes' value='' type='combobox'></control>
                 <control name='DeleteFlag' title='Deleted' >" + DeleteFlag.Checked.ToString() + @"</control>
                 </displaycolumn>
                 <displaycolumn>
                 <control name='UserPrompt' required='yes' title='User Prompt' type='text' >" + HttpUtility.HtmlEncode(UserPrompt.Value) + @"</control> 
                 <control name='SysFieldName' required='yes' title='System Field Name' type='text' >" + SysFieldName.Value + @"</control> 
                 </displaycolumn>
                 <displaycolumn>
                 <control name='CodeFileId' type='combobox' title='Code File' value='' ></control> 
                 <control name='MultiSelect' type='checkbox' title='MultiSelect' >" + MultiSelect.Checked.ToString() + @"</control> 
                  <control name='ddUsersGroups' type='combobox' title='' value='" + ddUsersGroups.SelectedValue + @"'>
                 <option value='Users'>Users</option> 
                 <option value='Groups'>Groups</option>
                 </control>
                <control name='chkhtmltextconfig' type='checkbox' title='chkhtmltextconfig' >" + chkhtmltextconfig.Checked.ToString() + @"</control> 
                 <control name='ModifyGrid' type='button'  title='Modify Grid' /> 
                 </displaycolumn>
                 <displaycolumn>
                 <control name='RequiredFlag' type='checkbox' title='Required Field' >" + RequiredFlag.Checked.ToString() + @"</control> 
                 <control name='FASReportableflag' type='checkbox' title='FAS Reportable Field' >" + FASReportableflag.Checked.ToString() + @"</control> 
                 <control name='LookupFlag' type='checkbox' title='Lookup Field' >" + LookupFlag.Checked.ToString() + @"</control>  
                 </displaycolumn>
                 <displaycolumn>
                 <control name='lblInput' title='Input Format'  /> 
                 <control id='free-form' type='radio' label='Free-Form' name='IsPatternRad' value='" + freeform.Value + @"' /> 
                 <control name='IsPattern' >0</control> 
                 <control type='controlgroup'>
                 <control id='patterned' type='radio' label=' Patterned' name='IsPatternRad'  value='" + patterned.Value + @"' /> 
                 <control name='Pattern' type='combobox' title='' value='" + Pattern.SelectedValue + @"'>
                 <option value='' /> 
                 <option value='(###) ###-#### Ext. #####'>(###) ###-#### Ext. #####</option> 
                 <option value='(###) ###-####'>(###) ###-####</option> 
                 <option value='###-##-####'>###-##-####</option> 
                 <option value='#####-####'>#####-####</option>
                 </control>
                 </control>
                 </displaycolumn>
                 <displaycolumn>
                 <control name='FieldSize' type='numeric' title='Field Size'>" + FieldSize.Value + @"</control> 
                 </displaycolumn>
                 <displaycolumn>
                 <control id='GrpAssocia' name='lblGroup' sectiontitle='Group Association' /> 
                 <control name='SourceField' type='combobox' title='Source Field' value=''>
                 <option value=''>None</option> 
                 </control>
                 <control codeid='' type='codelist' codetable='' name='GAList' title='Codes/Entities' value='' /> 
                 <control name='GAListHidden' title='' value='" + GAListHidden.Value + "'>" + GAListHidden.Value + @"</control> 
                 <control name='ListType' title=''  /> 
                 <control name='JuridictionHidden' title='' value='' /> 
                 <control name='bPatterned' title='' value='' >" + bPatterned.Value + @"</control> 
                 </displaycolumn>
                 <displaycolumn>
                 <control name='AssoField' title='' /> 
                 <control name='GrpAssoFlag' title='' value='" + GrpAssoFlag.Value + @"'>" + GrpAssoFlag.Value + @"</control> 
                 <control name='SeqNo' title=''/> 
                 <control name='SuppTblName' title='' /> 
                 <control name='HelpContext' title='' >" + HelpContext.Value + @"</control> 
                 <control name='hdnCodeFile' title='' >" + hdnCodeFile.Value + @"</control> 
                 <control name='GroupAssoList' title=''/> 
                 <control name='ControlChanged' title='' >" + ControlChanged.Value + @"</control> 
                 <control name='Columns' title='' >" + Columns.Value + @"</control> 
                 <control name='ColCount' title='' >" + ColCount.Value + @"</control> 
                 <control name='GridXML' title='' >" + hdnGridXml.Value + @"</control> 
                 <control name='GridProperty' title='' /> 
                 </displaycolumn>
                 <displaycolumn>
                 <control name='FormMode' title=''  /> 
                 <control name='hdnFieldType' title=''>" + hdnFieldType.Value + @"</control>
                 </displaycolumn>
                 </group>
                 <control name='TableType' >" + TableType.Value + @"</control> 
                 <control name='SysTableName' >" + SysTableName.Value + @"</control> 
                 <control name='XmlFormToUpgrade' >" + hdnXmlFormName.Value + @"</control>
                 <control name='hdnHistoryTracking' >" + hdnHistTrack .Value + @"</control>
                 <internal name='sys_formidname' value='RowId' /> 
                 <internal name='sys_formpidname' value='RowId' /> 
                 <internal name='sys_formpform' value='TableField' /> 
                 <internal name='hdnAction' title='' /> 
                 <internal name='EnhPolFlag'>" + 0 + @"</internal> 
                 <internal name='TrackPolFlag'>" + 0 + @"</internal> 
                 <internal name='Duplicate_Flag'>No</internal>            
               </form>
            ");

            try
            {


                using (XmlReader reader = oTemplate.CreateReader())
                {
                    xmlNodeDoc.Load(reader);
                }


                //Updating source field values in xml doc
                string strListType = "";
                for (int count = 0; count < SourceField.Items.Count; count++)
                {
                    XmlNode objXmlNode = xmlNodeDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='SourceField']");

                    objNewElm = xmlNodeDoc.CreateElement("option");
                    string[] strSplit = SourceField.Items[count].Value.Split(new Char[] { ',' });
                    objNewElm.SetAttribute("value", strSplit[0].Trim());
                    objNewElm.SetAttribute("fieldname", strSplit[1].Trim());
                    objNewElm.InnerText = SourceField.Items[count].Text;
                    objXmlNode.AppendChild(objNewElm);
                }
                XmlElement objXmlSourceElement = (XmlElement)xmlNodeDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='SourceField']");
                if (SourceField.SelectedValue.Length > 1)
                {
                    string[] strSplit = SourceField.SelectedValue.Split(new Char[] { ',' });
                    objXmlSourceElement.SetAttribute("value", strSplit[0].Trim());
                    if (strSplit[0].Trim() != "0")
                    {
                        strListType = strSplit[0].Trim();
                    }
                    else
                    {
                        strListType = "";
                    }
                    strAssocField = strSplit[1].Trim();
                }
                else
                {
                    objXmlSourceElement.SetAttribute("value", "");
                    strAssocField = "";
                }

                objectXmlNode = xmlNodeDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='ListType']");
                objectXmlNode.InnerXml = strListType;

                //Updating fieldtype values in xml doc
                for (int count = 0; count < FieldType.Items.Count; count++)
                {
                    XmlNode objXmlNode = xmlNodeDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='FieldType']");

                    objNewElm = xmlNodeDoc.CreateElement("option");
                    objNewElm.SetAttribute("value", FieldType.Items[count].Value);
                    objNewElm.InnerText = FieldType.Items[count].Text;
                    objXmlNode.AppendChild(objNewElm);
                }
                objXmlSourceElement = (XmlElement)xmlNodeDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='FieldType']");
                if (FieldType.SelectedValue != "")
                {
                    objXmlSourceElement.SetAttribute("value", FieldType.SelectedValue);
                }
                else
                {
                    objXmlSourceElement.SetAttribute("value", "");
                }

                //updating codefileid values in xml doc

                for (int count = 0; count < CodeFileId.Items.Count; count++)
                {
                    XmlNode objXmlNode = xmlNodeDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='CodeFileId']");

                    objNewElm = xmlNodeDoc.CreateElement("option");
                    objNewElm.SetAttribute("value", CodeFileId.Items[count].Value);
                    objNewElm.InnerText = CodeFileId.Items[count].Text;
                    objXmlNode.AppendChild(objNewElm);
                }
                objXmlSourceElement = (XmlElement)xmlNodeDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='CodeFileId']");
                //if (CodeFileId.Items.Count != 0 && CodeFileId.Items[CodeFileId.SelectedIndex].Value.Length > 1)
                if (CodeFileId.Items.Count != 0 && CodeFileId.Items[CodeFileId.SelectedIndex].Value.Length >= 1)//WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691 (Changed from >1 to >=1)
                {
                    objXmlSourceElement.SetAttribute("value", CodeFileId.Items[CodeFileId.SelectedIndex].Value);
                }
                else
                {
                    objXmlSourceElement.SetAttribute("value", "");
                }

                if (RowId.Value != "")
                {
                    objXmlSourceElement = (XmlElement)xmlNodeDoc.SelectSingleNode("form/group/displaycolumn//control[@name ='RowId']");
                    objXmlSourceElement.InnerXml = RowId.Value;
                }

 

                objXmlSourceElement = (XmlElement)xmlNodeDoc.SelectSingleNode("form/group/displaycolumn//control[@name ='IsPattern']");
                if (freeform.Checked)
                {
                    objXmlSourceElement.InnerXml = "0";

                }
                else
                {
                    if (patterned.Checked)
                    {
                        objXmlSourceElement.InnerXml = "-1";
                    }
                    else
                    {
                        objXmlSourceElement.InnerXml = "";
                    }
                }


                objectXmlNode = xmlNodeDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='AssoField']");
                objectXmlNode.InnerXml = strAssocField;

   
               if (isEditMode)
                {
                    objectXmlNode = xmlNodeDoc.SelectSingleNode("form/group/displaycolumn/control[@name ='RowId']");
                    objectXmlNode.InnerXml = RowId.Value;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            return xmlNodeDoc;


        }

#endregion
    
    }
}
