﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddIndex.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.SupplementalFieldData.AddIndex" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Index</title>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/Utilities.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>

    <script type="text/javascript" language="javascript">

        function EnableOk() 
        {
            document.forms[0].btnOk.disabled = false;
        }
        function loadForm()
        {
            document.forms[0].btnOk.disabled = true;
        }
        //abansal23 :  MITS 14693 Starts
        function CheckList()
        {
            var SelList = document.getElementById('SelFieldsList');
            if (SelList.options.length == 0)
            {
                alert('Please add at least one field to Index');
                return false;
            }
            return true;
        }
        //abansal23 :  MITS 14693 Stop
    
    </script>

</head>
<body>
    <form id="frmData" name="frmData" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        Index Name:
                        <asp:TextBox name="IndexName" Text="" type="text" ID="IndexName" Width="50%" onchange="setDataChanged(true);EnableOk();"
                            runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='IndexName']"></asp:TextBox>
                        <asp:TextBox name="$node^212" Text="" type="hidden" Style="display: none" runat="server"
                            ID="FormMode" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FormMode']"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width="40%">
                                    Fields in Table
                                    <br>
                                    <asp:ListBox name="$node^41" Height="150" runat="server" Style="width: 90%" ID="AvlFieldsList"
                                        type="listbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AvlFieldsList']">
                                    </asp:ListBox>
                                    <asp:TextBox runat="server" Style="display: none" ID="hdnAvlFieldsList"></asp:TextBox>
                                </td>
                                <td width="20%" valign="middle" align="center">
                                    <asp:Button Text="Add >" class="button" Width="80" ID="ADD" runat="server" OnClick="ADD_Click" />
                                    <br>
                                    <br>
                                    <asp:Button Text="< Remove" ID="btnRemove" class="button" Width="80" runat="server"
                                        OnClick="btnRemove_Click" />
                                </td>
                                <td width="40%">
                                    Fields in Index
                                    <br>
                                    <asp:ListBox name="SelFieldsList" type="listbox" Height="150" Style="width: 90%"
                                        ID="SelFieldsList" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SelFieldsList']">
                                    </asp:ListBox>
                                    <asp:TextBox runat="server" Style="display: none" ID="hdnSelFieldsList"></asp:TextBox>
                                    <asp:TextBox name="SelectedFieldList" type="hidden" Style="display: none" ID="SelectedFieldList"
                                        runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SelectedFieldList']"></asp:TextBox>
                                    <asp:TextBox name="SysTableName" type="hidden" Style="display: none" ID="SysTableName"
                                        runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SysTableName']"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="group">
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td colspan="2" align="center">
                <asp:Button Text="Ok" ID="btnOk" Enabled="false" class="button" Width="80" runat="server" OnClick="btnOk_Click" /><!-- abansal23 :  MITS 14693 -->
                <asp:Button ID="Button1" Text="Cancel" class="button" Width="80" OnClientClick="javascript:window.close();return false;; "
                    runat="server" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
