﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccomSupplemental.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.SupplementalFieldData.AccomSupplemental" %>

<%@ Register src="../../../Shared/Controls/ErrorControl.ascx" tagname="ErrorControl" tagprefix="uc1" %>
<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title runat="server" id="Title">Table :Accomodation Supplemental</title>
     <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
     <script type="text/javascript" language="javascript" src="../../../../Scripts/WaitDialog.js"></script>
    <script language="javascript" type="text/javascript" >
        var selRowId = "";
        var selRadio;
       
        //Setting query string on row selection
        function SelectSupplemental(strRowId) {
            
                       // selRowId = strRowId;
                       // document.getElementById('currentSelRowId').value = selRowId;
         }


         function ShowWait() {
             pleaseWait.Show();
             return true;
         }


         function showIcon() {
             window.setTimeout('ShowWait()', 5);
         } 


        
        
function New_FieldListGrid_onclick() {

}

function RunPVonUnload() {
    alert(document.forms[0].IsPVSaved.value);
    
}

window.onbeforeunload =
 function() {
     if ((window.event.clientX < 0) || (window.event.clientY < 0)) { //if close window
         if (document.forms[0].IsPVSaved.value == "False") {
             return "You have not saved the recent changes. Final changes will be reflected only after you save changes. ";
         }
      

     }

 }



 function SetActionName(ActionName) {
     if (ActionName == "Move") {
         if (selRowId == "") {
             alert('Please select a record to move');
             return false;
         }
     }

     document.forms[0].Action.value = ActionName;
     return true;
}

function SelectRadioBtnOnLoad() {
    var id = document.forms[0].selectedRadionbtn.value;

    if (id != null && id != "") {
        objRadionbtn = document.getElementById(id);
        objRadionbtn.checked = true;
        SelRadio(objRadionbtn);
    }
}

function SelRadio(radio) {
    document.getElementById('selectedRadionbtn').value = radio.id;
    selRowId = radio.value;
    document.getElementById('currentSelRowId').value = selRowId;
}

    </script>
</head>
<body onload="SelectRadioBtnOnLoad()" >
    <form id="frmData" runat="server" name="frmData" >
     <input type="text" name="TableType" value="" style="display:none" id="TableType" runat="server" />
     <input type="text" name="SysTableName" value="" style="display:none" runat="server" id="SysTableName" />
     <input type="text" name="selectedRadionbtn" style="display:none" runat="server" id="selectedRadionbtn" />
     <input type="text" name="IsPVSaved" style="display:none" runat="server" id="IsPVSaved" />
     <input type="text" id="Action" style="display:none" runat="server" />
     <div>
         <uc3:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
         <uc1:ErrorControl ID="ErrorControl1" runat="server" />
      
      </div>
     <table border="0" width="100%" cellspacing="0" cellpadding="0" >
      <tr>
       <td valign="top">
       <div class="msgheader" id="formtitle" runat="server">Table :Accomodation Supplemental</div>
       <div class="errtextheader"></div>
       </td>
      </tr>
      <tr>
      <td>
      <div >
      <table border="0" width="90%" >
       <tr>
         <td></td>
       </tr>
       <tr>
          <td colspan="2" class="ctrlgroup" ></td>
       </tr>
          <tr id="FASReportable" runat="server">
              <td>Mark all Fields as FAS reportable:&nbsp;&nbsp;
                 <asp:CheckBox ID="chkAllFASReportable" runat="server" AutoPostBack="True" OnCheckedChanged="chkAllFASReportable_CheckedChanged" />
              </td>
          </tr>
       <tr>
       <td width="95%" valign="top" class="singleborder" >
         <asp:GridView ID="GridView1" runat="server"   AutoGenerateColumns="false" 
               AllowPaging="false"  width="100%"   GridLines="None" 
               onrowdatabound="GridView1_RowDataBound">
		    <HeaderStyle CssClass="msgheader" />
		     <AlternatingRowStyle CssClass="data2" />
		    <Columns>
		        <asp:TemplateField  ItemStyle-CssClass="data" ControlStyle-Width="5%">
                    <ItemTemplate>
                       <input type="radio" id='<%# DataBinder.Eval(Container.DataItem, "Row Id")%>' onclick="SelRadio(this);" value='<%# DataBinder.Eval(Container.DataItem, "Row Id")%>' name="FieldListGrid"  />
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="#" ControlStyle-Width="10.555555555555555556%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblSNO" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "#")%>' ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="System Field Name" ControlStyle-Width="10.555555555555555556%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblSysFieldName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "System Field Name")%>' ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="User Prompt" ControlStyle-Width="10.555555555555555556%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblUsrPrompt" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "User Prompt")%>' ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Field Type" ControlStyle-Width="10.555555555555555556%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblFieldType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Field Type")%>'  ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Size" ControlStyle-Width="10.555555555555555556%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblSize" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Size")%>'  ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Req.?" ControlStyle-Width="10.555555555555555556%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblReq" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Req")%>'  ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Del.?" ControlStyle-Width="10.555555555555555556%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblDel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Del")%>' ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="FAS rep.?" ControlStyle-Width="10.555555555555555556%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblFAS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FAS rep")%>' ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pattern" ControlStyle-Width="10.555555555555555556%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblPattern" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Pattern")%>' ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Group Association" ControlStyle-Width="10.555555555555555556%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblGroupAssociation" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Group Association")%>' ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sq" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                    <ItemTemplate>
                        <asp:Label ID="lblSeqNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SeqNo")%>' ></asp:Label>
                    </ItemTemplate> 
                </asp:TemplateField>  
             </Columns>
		     
		     
		 </asp:GridView>
       </td>
       <td width="5%" valign="top">
         <asp:ImageButton runat="server"  ImageUrl="~/Images/save.gif"  alt="" id="Save_PV" 
               onmouseover="&#xA;javascript:document.all[&#34;Save_PV&#34;].src='../../../../Images/save2.gif';&#xA;" 
               onmouseout="&#xA;javascript:document.all[&#34;Save_PV&#34;].src='../../../../Images/save.gif';&#xA;" 
               title="Save" OnClientClick="showIcon();SetActionName('Save_PV');" onclick="Save_PV_Click"  /><br />
         <input type="image" onclick="openGridAddEditWindow('FieldListGrid','add',500,400);return false;" runat="server"   src="../../../../Images/tb_new_mo.png" alt="" id="New_FieldListGrid" onmouseover="&#xA;javascript:document.all[&#34;New_FieldListGrid&#34;].src='../../../../Images/tb_new_active.png';&#xA;" onmouseout="&#xA;javascript:document.all[&#34;New_FieldListGrid&#34;].src='../../../../Images/tb_new_mo.png';&#xA;" title="New"  />
         <input type="image" onclick="openGridAddEditWindow('FieldListGrid','edit',500,400);return false;" id="Edit_FieldListGrid" src="../../../../Images/edittoolbar.gif" onmouseover="&#xA;javascript:document.all[&#34;Edit_FieldListGrid&#34;].src='../../../../Images/edittoolbar2.gif';&#xA;" onmouseout="&#xA;javascript:document.all[&#34;Edit_FieldListGrid&#34;].src='../../../../Images/edittoolbar.gif';&#xA;" title="Edit" /><br />
         
         <asp:ImageButton type="image" runat="server"  
               src="../../../../Images/moveup.gif"  alt="Move Up" id="FieldListGrid_moveup" 
               onmouseover="&#xA;javascript:document.all[&#34;FieldListGrid_moveup&#34;].src='../../../../Images/moveup.gif';&#xA;" 
               onmouseout="&#xA;javascript:document.all[&#34;FieldListGrid_moveup&#34;].src='../../../../Images/moveup.gif';&#xA;" OnClientClick="return SetActionName('Move')"
               onclick="FieldListGrid_moveup_Click" /><br/>
         <asp:ImageButton type="image" runat="server"  
               src="../../../../Images/movedown.gif" alt="Move Down" 
               id="FieldListGrid_movedown" 
               onmouseover="&#xA;javascript:document.all[&#34;FieldListGrid_movedown&#34;].src='../../../../Images/movedown.gif';&#xA;" 
               onmouseout="&#xA;javascript:document.all[&#34;FieldListGrid_movedown&#34;].src='../../../../Images/movedown.gif';&#xA;" OnClientClick="return SetActionName('Move')"
               onclick="FieldListGrid_movedown_Click" /><br/>
       </td>
       </tr>
       <tr>
         <td width="5%" valign="top">
          <input type="text" style="display:none" runat="server" id="currentSelRowId" value="" />
          
         </td>
       </tr>
       
      </table>
      </div>
      </td>
      </tr>
      <tr>
          <td colspan="2" ></td>
       </tr>
      <tr>
       <td width="500px" >
         
         <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
             <td width ="95%" class="singleborder">
             <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false" 
                     AllowPaging="false"  width="100%"   GridLines="None" 
                     onrowdatabound="GridView2_RowDataBound">
		        <HeaderStyle CssClass="msgheader" />
		        <AlternatingRowStyle CssClass="data2" />
		         <Columns>
		             <asp:TemplateField  ItemStyle-CssClass="data" ControlStyle-Width="5%">
                      <ItemTemplate>
                       <input type="radio" id="selectrdo"  name="AccomSupplemental"  />
                       
                       </ItemTemplate> 
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Index Names" ControlStyle-Width="31.666666666666666667%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                      
                      <ItemTemplate>
                        <asp:Label ID="lblIndexNames" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Index Names")%>' ></asp:Label>
                      </ItemTemplate> 
                      </asp:TemplateField>
		              <asp:TemplateField HeaderText="Index Fields" ControlStyle-Width="31.666666666666666667%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                      <ItemTemplate>
                        <asp:Label ID="lblIndexFields" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Index Fields")%>' ></asp:Label>
                      </ItemTemplate> 
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Unique?" ControlStyle-Width="31.666666666666666667%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                      <ItemTemplate>
                        <asp:Label ID="lblUnique" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Unique")%>' ></asp:Label>
                      </ItemTemplate> 
                      </asp:TemplateField>
		              
		         </Columns>
		     
		     </asp:GridView>
		     
             </td>
             <td width="5%" valign="top">
                <input type="image"  onclick="return openGridAddEditWindow('IndexListGrid','add',500,400)" name="New_IndexListGrid" src="../../../../Images/tb_new_mo.png" alt="" id="New_IndexListGrid" onmouseover="&#xA;javascript:document.all[&#34;New_IndexListGrid&#34;].src='../../../../Images/tb_new_active.png';&#xA;" onmouseout="&#xA;javascript:document.all[&#34;New_IndexListGrid&#34;].src='../../../../Images/tb_new_mo.png';&#xA;" title="New" /><br />
             </td>
            
            </tr>
         
         </table>
         
      </td>
      </tr>
     </table>
     
    </form>
</body>
</html>
