﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Text;
using Riskmaster.UI.FDM;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class AddScheduleDate : GridPopupBase
    {
        XElement XmlTemplate = null;
        string sreturnValue = string.Empty;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - Yukti-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                if (!IsPostBack)
                {

                    //NonFDMCWSPageLoad("CheckOptionsAdaptor.GetAddNotifyValues");
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedid.Text = AppHelper.GetQueryStringValue("selectedid");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    ScheduleId.Text = selectedid.Text;
                    NonFDMCWSPageLoad("CheckOptionsAdaptor.GetScheduleDates");

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnOk_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("CheckOptionsAdaptor.SaveSchedule", XmlTemplate, out sreturnValue, true, false);
                if (bReturnStatus)
                {
                    string script = "<script>RefereshParentPage();</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "RefereshParentPage", script);
                    //this.Page.ClientScript.RegisterStartupScript(typeof(string), "RefereshParentPage", "window.opener.location.reload();", true); 
                }
                else
                {
                    //ecErrorControl.errorDom = sreturnValue;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
           
        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("CheckOptionsAdaptor.SaveSchedule");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><ScheduleDate>");
            sXml = sXml.Append("<ScheduleDate>" + Date.Text + "</ScheduleDate>");
            sXml = sXml.Append("<ScheduleDescription>" + Description.Text + "</ScheduleDescription>");
            if (ScheduleId.Text != "")
            {
                sXml = sXml.Append("<ScheduleId>" + ScheduleId.Text + "</ScheduleId>");
            }
            else//when user will add the new Record/Schedule
            {
                sXml = sXml.Append("<ScheduleId>-1</ScheduleId>");
            }
            sXml = sXml.Append("<control name='AddedByUser'/><control name='DttmRcdAdded'/><control name='DttmRcdLastUpd'/><control name='UpdatedByUser'/>");
            sXml = sXml.Append("</ScheduleDate></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}