﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class MedicalInfoSetup : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                NonFDMCWSPageLoad("MedicalInfoAdaptor.Get");
            }
        }

        protected void Save(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            bReturnStatus = CallCWSFunction("MedicalInfoAdaptor.Save");
            if (bReturnStatus)
                bReturnStatus = CallCWSFunction("MedicalInfoAdaptor.Get");
        }
        
    }
}
