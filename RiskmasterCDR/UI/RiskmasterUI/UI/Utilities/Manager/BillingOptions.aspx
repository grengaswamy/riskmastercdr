﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillingOptions.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.Manager.BillingOptions" ValidateRequest ="false" %>

<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove"
xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head runat="server">
    <title>Billing System Setup</title>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">{var i;}
    </script>

</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <div class="ctrlgroup" style="width:100%">
        <asp:Label runat="server" Text="Billing System Options" />
        </div>
        <br />
        <div>
            <span>
                <dg:UserControlDataGrid runat="server" ID="BillingRulesGrid" GridName="BillingRulesGrid"
                    GridTitle="Billing Rules" Target="/Document/BillingOptions/form/group/BillingRules"
                    Ref="/Instance/Document/form//control[@name='BillingRulesGrid']" Unique_Id="BRRowId"
                    ShowRadioButton="true" Width="680" Height="150" HideNodes="|BRRowId|" ShowHeader="True"
                    LinkColumn="" PopupWidth="500" PopupHeight="600" Type="GridAndButtons" RowDataParam="BillingRule"
                    OnClick="KeepRowForEdit('BillingRulesGrid');" />
            </span>
        </div>
        <div>
            <asp:TextBox Style="display: none" runat="server" ID="BillingRulesSelectedId" />
            <asp:TextBox Style="display: none" runat="server" ID="BillingRulesGrid_Action" />
            <asp:TextBox Style="display: none" runat="server" ID="BillingRulesGrid_RowAddedFlag" />
            <asp:TextBox Style="display: none" runat="server" ID="BillingRulesGrid_RowDeletedFlag" />
        </div>
        <div>
            <span>
                <dg:UserControlDataGrid runat="server" ID="PayPlansGrid" GridName="PayPlansGrid"
                    GridTitle="Pay Plans" Target="/Document/BillingOptions/form/group/PayPlans" Ref="/Instance/Document/form//control[@name='PayPlansGrid']"
                    Unique_Id="PPRowId" ShowRadioButton="true" Width="680" Height="150" HideNodes="|PPRowId|"
                    ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="650" Type="GridAndButtons"
                    RowDataParam="PayPlan" OnClick="KeepRowForEdit('PayPlansGrid');" />
            </span>
        </div>
        <div>
            <asp:TextBox Style="display: none" runat="server" ID="PayPlansSelectedId" />
            <asp:TextBox Style="display: none" runat="server" ID="PayPlansGrid_Action" />
            <asp:TextBox Style="display: none" runat="server" ID="PayPlansGrid_RowAddedFlag" />
            <asp:TextBox Style="display: none" runat="server" ID="PayPlansGrid_RowDeletedFlag" />
        </div>
        <div>
            <span>
                <dg:UserControlDataGrid runat="server" ID="EarlyPayDiscountsGrid" GridName="EarlyPayDiscountsGrid"
                    GridTitle="Early Pay Discounts" Target="/Document/BillingOptions/form/group/EarlyPayDiscounts"
                    Ref="/Instance/Document/form//control[@name='EarlyPayDiscountsGrid']" Unique_Id="EPRowId"
                    ShowRadioButton="true" Width="680" Height="150" HideNodes="|EPRowId|" ShowHeader="True"
                    LinkColumn="" PopupWidth="500" PopupHeight="350" Type="GridAndButtons" RowDataParam="EarlyPayDiscount"
                    OnClick="KeepRowForEdit('EarlyPayDiscountsGrid');" />
            </span>
        </div>
        <div>
            <asp:TextBox Style="display: none" runat="server" ID="EarlyPayDiscountsSelectedId" />
            <asp:TextBox Style="display: none" runat="server" ID="EarlyPayDiscountsGrid_Action" />
            <asp:TextBox Style="display: none" runat="server" ID="EarlyPayDiscountsGrid_RowAddedFlag" />
            <asp:TextBox Style="display: none" runat="server" ID="EarlyPayDiscountsGrid_RowDeletedFlag" />
        </div>
        <asp:TextBox runat="server" ID="SelectedGrid" style="display:none" />
    </div>
    </form>
</body>
</html>
