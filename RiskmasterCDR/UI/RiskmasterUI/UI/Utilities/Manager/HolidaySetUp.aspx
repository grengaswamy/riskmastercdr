<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HolidaySetUp.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.HolidaySetUp"  ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Holiday Setup</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>
    <%--vkumar258 - RMA-6037 - Starts --%>

    <%--<script type="text/javascript" src="../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>--%>
    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Holiday Setup" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <%--<div class="Selected" id="TABSHolidaySetup">
        <span>
            <asp:Label runat="server" name="HolidaySetup" ID="HolidaySetup" Text="Holiday Setup" />
        </span>
    </div>--%>
    <br />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/Holidays/control[@name ='RowId']" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <div id="div_Date" class="full">
        <asp:Label runat="server" class="required" ID="lblDate" Text="Date:" />
        <span>
            <asp:TextBox runat="server" FormatAs="date" ID="Date" RMXRef="/Instance/Document/Holidays/control[@name ='Date']"
                TabIndex="1" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                <%--vkumar258 - RMA-6037 - Starts --%>
                <%-- <asp:Button class="DateLookupControl" runat="server" ID="btnDate" TabIndex="2" />

            <script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "Date",
					        ifFormat: "%m/%d/%Y",
					        button: "btnDate"
					    }
					    );
            </script>--%>
                <script type="text/javascript">
                    $(function () {
                        $("#Date").datepicker({
                            showOn: "button",
                            buttonImage: "../../../Images/calendar.gif",
                            //buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");
                    });
                </script>
                <%--vkumar258 - RMA_6037- End--%>
        </span>
    </div>
    <div id="div_Description" class="full">
        <asp:Label runat="server" class="required" ID="lbl_Description" Text="Description:" />
        <span>
            <asp:TextBox runat="server" onchange="setDataChanged(true);" ID="Description" RMXRef="/Instance/Document/Holidays/control[@name ='Desc']"
                TabIndex="3" />
        </span>
    </div>
    <div id="div_OrgId" class="full">
        <asp:Label runat="server" class="required" ID="lblOrgId" Text="Organization:" />
        <span>
            <asp:ListBox SelectionMode="Multiple" Rows="3" orgid="" ID="Org" runat="server" RMXRef="/Instance/Document/Holidays/control[@name ='Org']" />
            <asp:TextBox Style="display: none" ID="Org_lst" runat="server" /><asp:Button ID="Orgbtn"
                OnClientClick="return selectCode('orgh','Org','ALL')" Text="..." runat="server" /><asp:Button
                    ID="Orgbtndel" OnClientClick="return deleteSelCode('Org')" Text="-" runat="server" />
            <asp:TextBox Style="display: none" ID="Org_cid" runat="server" />
            <asp:TextBox Style="display: none" ID="Org_orglist" runat="server" RMXRef="/Instance/Document/Holidays/control[@name ='Org_orglist']" />
            <asp:TextBox Style="display: none" ID="OrgId" runat="server" />
        </span>
    </div>
    <br />
    <br />
    <br />
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" style="left:1%"> <%-- igupta3 Mits: 33301--%>
        <div class="formButton" id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return Holidays_onOk();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return Holidays_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
