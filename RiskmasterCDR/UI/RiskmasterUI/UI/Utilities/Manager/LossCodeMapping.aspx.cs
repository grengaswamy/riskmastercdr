﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Riskmaster.BusinessHelpers;
using Riskmaster.UI.PolicyInterfaceService;
using System.ServiceModel;
using Riskmaster.Common;
using Telerik.Web.UI;//abhal3 MITS 36046

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class LossCodeMapping : NonFDMBasePageCWS
    {//abhal3 MITS 36046 start
        double  dFileZise=1288490188.8;
        private bool _isFNOLChecked = false;
        protected void GridPolicyCodemapping_PreRender(object sender, System.EventArgs e)
        {
            GridFilterMenu menu = GridPolicyCodemapping.FilterMenu;
            int i = 0;
            while (i < menu.Items.Count)
            {
                if (menu.Items[i].Text == "NoFilter" || menu.Items[i].Text == "Contains" || menu.Items[i].Text == "DoesNotContain"
                    || menu.Items[i].Text == "StartsWith" || menu.Items[i].Text == "EndsWith" || menu.Items[i].Text == "EqualTo"
                    || menu.Items[i].Text == "NotEqualTo" || menu.Items[i].Text == "IsEmpty" || menu.Items[i].Text == "NotIsEmpty")
                {
                    i++;
                }
                else
                {
                    menu.Items.RemoveAt(i);
                }
            }
        }
		//abhal3 MITS 36046 end
        #region "Constants"

        internal const string PolicyClaimLOB = "POLICY_CLAIM_LOB";
        internal const string CoverageType = "COVERAGE_TYPE";
        internal const string LossCodes = "LOSS_CODES";

        #endregion

        //Ankit Start : Worked on MITS - 34657
        #region "Class Level Variables"
        string sRMALoss = "RMA Loss Of Code";
        string sPointLoss = "Policy System Loss Codes";
        //Ankit Start : Worked for MITS - 35096
        private bool UsePolicyInterface
        {
            get
            {
                bool success;
                return Conversion.CastToType<bool>(ViewState["UsePI"].ToString(), out success);
            }
            set
            {
                ViewState["UsePI"] = value;
            }
        }
        //Ankit End
        #endregion
        //Ankit End

        #region "Page Methods"
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetCodeType();
                //Ankit Start : Worked on MITS - 34657
                lstCoverageType.AutoPostBack = true;        //Ankit Start : Worked on MITS - 36045
                if (UsePolicyInterface)
                    GetLossType(false);
                else
                    GetLossType(true);
                //BindGridData();
                //Ankit End
            }
            //Ankit Start : Worked for MITS - 35096
            trPolicySystemNames.Visible = UsePolicyInterface;
            trPSLossCodes.Visible = UsePolicyInterface;
            //Ankit End
            if (string.Compare(hdnFNOL.Value, "FNOLReserve", true) == 0)
            {
                _isFNOLChecked = true;
            }
            //abhal3 MITS 3604 start
			if (UsePolicyInterface)
            {
                TABSImportLossCodes.Visible = true;
                TABSExportLossCodes.Visible = true;
                TABSReplication.Visible = true;
            }
            else
            {
                TABSImportLossCodes.Visible = false;
                TABSExportLossCodes.Visible = false;
                TABSReplication.Visible = false;
            }
			//abhal3 MITS 36046 end

        }
        #endregion

        protected void GridPolicyCodemapping_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            (sender as RadGrid).DataSource = BindGridData();
        }

        #region "Control Events"
        protected void addMappingBtn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            XElement XmlTemplate = null;
            try
            {
                XmlTemplate = GetMessageTemplateForAdd();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("LossCodeMappingAdaptor.AddNewMapping", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }
                    else
                    {
                        if (UsePolicyInterface)        //Ankit Start : Worked for MITS - 35096
                            GetLossType(true);      //Ankit Start : Worked on MITS - 34657
                        //BindGridData();
                        GridPolicyCodemapping.Rebind();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void removeMappingBtn_Click(object sender, EventArgs e)
        {
            try
            {
                XElement XmlTemplate = null;
                string sPolCode = "";
                XmlTemplate = GetMessageTemplateForDeletion();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("LossCodeMappingAdaptor.DeleteCodeMapping", XmlTemplate, out Data, true, false);

                if (UsePolicyInterface)        //Ankit Start : Worked for MITS - 35096
                    GetLossType(true);      //Ankit Start : Worked on MITS - 34657
                //BindGridData();
                GridPolicyCodemapping.Rebind();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        /// <summary>
      	//abhal3 MITS 36046 start
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
	
        protected void ExportMappingBtn_Click(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper objHelper = null;
            ExportLossCodeMapping objRequest = null;
            string sFileContent = string.Empty;
            byte[] excelbytes = null;
            try
            {

                objHelper = new PolicyInterfaceBusinessHelper();
                objRequest = new ExportLossCodeMapping();
                objRequest.PolicySystemId = ExportFromPolicySystem.SelectedValue.Split('_')[0];
                sFileContent = objHelper.ExportMapping(objRequest);
                if (!string.IsNullOrEmpty(sFileContent))
                {
                    excelbytes = Convert.FromBase64String(sFileContent);
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Charset = "";
                    Response.AppendHeader("Content-Encoding", "none;");
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment; filename=ExportLossCodeMapping.xlsx"));
                    Response.AddHeader("Accept-Ranges", "bytes");
                    Response.BinaryWrite(excelbytes);
                    Response.Flush();
                    Response.Close();
                }
                else
                {
                    throw new Exception("The file to be downloaded does not exist.");
                }
                GetSelectedTab();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                objHelper = null;
                objRequest = null;
                excelbytes = null;
            }
        }
	
        /// <summary>
       /// </summary>
        private void GetSelectedTab()   //abhal3 MITS 36046
        {
            this.ClientScript.RegisterStartupScript(this.GetType(), "script", "tabChange('" + SelectedTab.Value + "');", true);
        }
        /// <summary>
        /// Mits 36046 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void replicateMappingBtn_Click(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper objHelper = null;
            ReplicateLossCodeMapping objReplicateLossCodeMapping = null;
            LossCodeMappingOutput oOutPut = null;
            try
            {

                objHelper = new PolicyInterfaceBusinessHelper();
                objReplicateLossCodeMapping = new ReplicateLossCodeMapping();
                objReplicateLossCodeMapping.ReplicateFromPolicySystemId = ReplicateFromPolicySystem.SelectedValue.Split('_')[0];
                objReplicateLossCodeMapping.ReplicateToPolicySystemIds = hdnLstBoxSelectedItem.Value;
                oOutPut = objHelper.ReplicateMapping(objReplicateLossCodeMapping);
                if (oOutPut.HasError)
                {
                    ViewState["replicatelogFileContent"] = oOutPut.FileContent;
                    lbSuccess.Visible = true;
                    ErrorControl1.Visible = false;
                    btnReplicateLog.Visible = true;
                    lbSuccess.Text = " Mapping is successfully replicated with errors";
                }
                else
                {
                    lbSuccess.Visible = true;
                    ErrorControl1.Visible = false;
                    btnReplicateLog.Visible = false;
                    if (!oOutPut.TaskScheduled)
                    {
                        lbSuccess.Text = " Mapping is successfully replicated";

                    }
                    else
                    {
                        lbSuccess.Text = "Task Manager Task is scheduled to replicate Loss Codes";
                    }
                }

                GetSelectedTab();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                objHelper = null;
                objReplicateLossCodeMapping = null;
                oOutPut = null;
            }

        }
	
        /// <summary>
        /// Mits 36046 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void importMappingBtn_Click(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper objHelper = null;
            LossCodeMappingFileContent objLossCodeMappingFileContent = null;
            LossCodeMappingOutput oOutPut = null;
            try
            {
                if (uploadFile.FileBytes.Length > dFileZise)
                {
                    throw new ApplicationException("Size of attached Files is larger than 1.2 GB");
                }
                objHelper = new PolicyInterfaceBusinessHelper();
                objLossCodeMappingFileContent = new LossCodeMappingFileContent();
                objLossCodeMappingFileContent.FileContent = Convert.ToBase64String(uploadFile.FileBytes);
                objLossCodeMappingFileContent.PolicySystemIds = hdnLstBoxSelectedItem.Value;
                oOutPut = objHelper.ImportMappingFromFile(objLossCodeMappingFileContent);
                if (oOutPut.HasError)
                {
                    ViewState["importlogFileContent"] = oOutPut.FileContent;
                    lbSuccess.Visible = true;
                    ErrorControl1.Visible = false;
                    btnDownLoadLog.Visible = true;
                    lbSuccess.Text = " Mapping is successfully imported with errors";
                }
                else
                {
                    lbSuccess.Visible = true;
                    ErrorControl1.Visible = false;
                    btnDownLoadLog.Visible = false;

                    if (!oOutPut.TaskScheduled)
                    {
                        lbSuccess.Text = " Mapping is successfully imported";

                    }
                    else
                    {
                        lbSuccess.Text = "Task Manager Task is scheduled to import Loss Codes";
                    }
                }
                GetSelectedTab();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                objLossCodeMappingFileContent = null;
                objHelper = null;
                oOutPut = null;
            }
        }
        /// <summary>
        /// Mits 36046 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDownLoadLog_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Conversion.ConvertObjToStr(ViewState["importlogFileContent"])))
            {
                byte[] excelbytes = Convert.FromBase64String(Conversion.ConvertObjToStr(ViewState["importlogFileContent"]));

                Response.Buffer = true;
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = "";
                Response.AppendHeader("Content-Encoding", "none;");
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", string.Format("attachment; filename=ImportLossCodeMapping.pdf"));
                Response.AddHeader("Accept-Ranges", "bytes");
                Response.BinaryWrite(excelbytes);
                Response.Flush();
                Response.Close();
            }
            GetSelectedTab();
        }
        /// <summary>
        ///   Mits 36046 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReplicateLog_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Conversion.ConvertObjToStr(ViewState["replicatelogFileContent"])))
            {
                byte[] excelbytes = Convert.FromBase64String(Conversion.ConvertObjToStr(ViewState["replicatelogFileContent"]));

                Response.Buffer = true;
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = "";
                Response.AppendHeader("Content-Encoding", "none;");
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", string.Format("attachment; filename=ReplicateLossCodeMapping.pdf"));
                Response.AddHeader("Accept-Ranges", "bytes");
                Response.BinaryWrite(excelbytes);
                Response.Flush();
                Response.Close();
            }
            GetSelectedTab();
        }
		//abhal3 MITS 36046 end
        //protected void gvLossCodeMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    gvLossCodeMapping.PageIndex = e.NewPageIndex;
        //    BindGridData();
        //}

        protected void lstPolicyClaimLOB_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Ankit Start : Worked on MITS - 34657
            if (UsePolicyInterface)        //Ankit Start : Worked for MITS - 35096
                GetLossType(false);
            else
            GetLossType(true);
            //Ankit End
            //BindGridData();
            GridPolicyCodemapping.Rebind();//abhal3 MITS 36046
        }
        //Ankit Start : Worked on MITS - 34657
        protected void lstPolicySystemNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            GetLossType(false);
        //    GetLossType(true);
           // BindGridData();
            GridPolicyCodemapping.Rebind();
        }

        protected void lstCoverageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UsePolicyInterface)      //Ankit Start : Worked for MITS - 35096
                GetLossType(true);		 //aravi5 RMA-12151
            //BindGridData();
            GridPolicyCodemapping.Rebind();//abhal3 MITS 36046
        }

        protected void lstPolicySystemLosscode_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetLossType(true);
            //BindGridData();
            GridPolicyCodemapping.Rebind();//abhal3 MITS 36046
        }
        //Ankit End
        //Ankit Start : Worked for MITS - 35096
        protected void lstLossOfCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!UsePolicyInterface)
                //BindGridData();
                GridPolicyCodemapping.Rebind();//abhal3 MITS 36046
        }
        //Ankit End
        #endregion

        #region "Private Methods"
        //Ankit Start : Worked on MITS - 34657
        private void GetPolicySystem()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            PolicySystemList oResponse = null;
            DataSet oDS = new DataSet();
            try
            {
                oResponse = objHelper.GetPolicySystemList();
                if (oResponse != null && !string.IsNullOrEmpty(oResponse.ResponseXML))
                {
                    oDS.ReadXml(new StringReader(oResponse.ResponseXML));
                    ListItem item = null;
                    for (int i = 0; i < oDS.Tables["option"].Rows.Count; i++)
                    {
                        item = new ListItem();
                        item.Text = oDS.Tables["option"].Rows[i]["option_Text"].ToString();
                        item.Value = oDS.Tables["option"].Rows[i]["value"].ToString() + "_" + oDS.Tables["option"].Rows[i]["policy_system_type"].ToString();
                        //lstPolicySystemNames.Items.Add(item);
                        listPolicySystem.Items.Add(item);
                        // abahl3 Mits 36046 start
                        ImportToPolicySystem.Items.Add(item); //abhal3 MITS 36046
                        ExportFromPolicySystem.Items.Add(item); //abhal3 MITS 36046
                        ReplicateFromPolicySystem.Items.Add(item);//abhal3 MITS 36046
                        ReplicateToPolicySystem.Items.Add(item);//abhal3 MITS 36046
                        //abahl3 Mits 36046 end
                    }
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oResponse != null)
                    oResponse = null;
            }
        }
        //Ankit End
        private void GetLossType(bool blnIsRMALossType) //Ankit Start : Worked on MITS - 34657
        {
            XElement XmlTemplate = null;
            DataSet oDS =null;
            string sPolCode = "";
            string Data = "";
            bool bReturnStatus = false;
         
            try
            {
                oDS=  new DataSet();

                XmlTemplate = GetMessageTemplateForLossType(blnIsRMALossType);  //Ankit Start : Worked on MITS - 34657

                //txtPolicyClaimLOB.Text = lstPolicyClaimLOB.SelectedValue;
                bReturnStatus = CallCWS("LossCodeMappingAdaptor.GetLossType", XmlTemplate, out Data, false, true);
                if (Data != null && !string.IsNullOrEmpty(Data))
                {
                    oDS.ReadXml(new StringReader(Data));
                    ListItem item = null;
					
                    if (oDS != null)		//Ankit Start : Worked on MITS - 34657
                    {						//Ankit Start : Worked on MITS - 34657
                        if (oDS.Tables["LossCodeMapping"].Rows.Count > 0)
                        {
                            //Ankit Start : Worked on MITS - 34657
                            if (string.Equals(oDS.Tables["LossCodeMapping"].Rows[0]["IsWorkersComp"].ToString(), "true"))
                            {
                                sRMALoss = "RMA Disability Loss Type";
                                if (UsePolicyInterface)         //Ankit Start : Worked for MITS - 35096
                                    sPointLoss = "Policy System Disability Loss Type";
                            }
                            else
                            {
                                sRMALoss = "RMA Loss Of Code";
                                if (UsePolicyInterface)         //Ankit Start : Worked for MITS - 35096
                                    sPointLoss = "Policy System Loss Codes";
                            }
                        }
                        //Ankit End
                    }
                      //  IsWorkersComp
                    //Ankit Start : Worked on MITS - 36045
                    item = new ListItem();
                    item.Text = "Select";
                    item.Value = "-1";
                    //End Ankit
                    if (blnIsRMALossType)			//Ankit Start : Worked on MITS - 34657
                    {
                        lstLossOfCode.Items.Clear();
                        lstLossOfCode.Items.Add(item);      //Ankit Start : Worked on MITS - 36045
                    }
                    else if (UsePolicyInterface)			//Ankit Start : Worked on MITS - 34657
                    {
                        lstPolicySystemLosscode.Items.Clear();			//Ankit Start : Worked on MITS - 34657
                        lstPolicySystemLosscode.Items.Add(item);       //Ankit Start : Worked on MITS - 36045

                        if (lstLossOfCode.Items.Count == 0)
                        {
                            lstLossOfCode.Items.Clear();
                            lstLossOfCode.Items.Add(item);
                        }
                    }
                    if (oDS != null)				//Ankit Start : Worked on MITS - 34657
                    {
                        if (oDS.Tables["option"] != null)			//Ankit Start : Worked on MITS - 34657
                        {			//Ankit Start : Worked on MITS - 34657
                            for (int i = 0; i < oDS.Tables["option"].Rows.Count; i++)
                            {
                                item = new ListItem();
                                item.Text = oDS.Tables["option"].Rows[i]["option_text"].ToString();
                                item.Value = oDS.Tables["option"].Rows[i]["value"].ToString();
                                if (blnIsRMALossType)			//Ankit Start : Worked on MITS - 34657
                                    lstLossOfCode.Items.Add(item);
                                else if (UsePolicyInterface)				//Ankit Start : Worked on MITS - 34657
                                    lstPolicySystemLosscode.Items.Add(item);			//Ankit Start : Worked on MITS - 34657
                            }
                        }
                    }			//Ankit Start : Worked on MITS - 34657
                }				//Ankit Start : Worked on MITS - 34657
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        protected void btnpolicysystem_Click(object sender, EventArgs e)//abhal3 MITS 36046
        {
            GetLossType(false);
            //GetLossType(true);
            GridPolicyCodemapping.Rebind();
            //BindGridData();
        }
        protected void GridPolicyCodemapping_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)//abhal3 MITS 36046
            {
                GridDataItem item = (GridDataItem)e.Item;
                TableCell cell = (TableCell)item["Uniquename"]; //accessing Table cell
                string value = DataBinder.Eval(e.Item.DataItem, "DataField").ToString();
            }
        }
        private DataTable BindGridData()
        {
            try
            {
                string data = string.Empty;
                DataTable dtGridData = new DataTable();
                DataRow objRow = null;
                XElement XmlTemplate = null;
                bool bReturnStatus = false;

                hdValidateKey.Value = string.Empty;
                XmlTemplate = GetMessageTemplateForAllMappings();
                bReturnStatus = CallCWS("LossCodeMappingAdaptor.GetAllMapping", XmlTemplate, out data, false, true);
                if (bReturnStatus)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(data);
                    dtGridData.Columns.Add("RowID");
                    dtGridData.Columns.Add("PolicyLob");
                    dtGridData.Columns.Add("CvgTypeCode");
                    dtGridData.Columns.Add("LossCode");
                    dtGridData.Columns.Add("policyname");//abhal3 MITS 36046

                    XmlNodeList xmlNodeList = xmldoc.SelectNodes("//MappedCodes");
                    if (xmlNodeList.Count > 0)
                    {
                        foreach (XmlNode objNodes in xmlNodeList)
                        {
                            objRow = dtGridData.NewRow();
                            //Ankit Start : Worked on MITS - 34657
                            objRow["RowID"] = objNodes.Attributes["RowID"].Value.ToString();
                            objRow["PolicyLob"] = objNodes.Attributes["PolicyLob"].Value.ToString();
                            objRow["CvgTypeCode"] = objNodes.Attributes["CvgTypeCode"].Value.ToString();
                            objRow["LossCode"] = objNodes.Attributes["LossCode"].Value.ToString();
                            objRow["policyname"] = objNodes.Attributes["PolicyName"].Value.ToString();//abhal3 MITS 36046

                            //Ankit End

                            dtGridData.Rows.Add(objRow);

                            hdValidateKey.Value = hdValidateKey.Value + "|" + objNodes.Attributes["HiddenKey"].Value.ToString();
                        }
                        if (_isFNOLChecked)//abhal3 MITS 36046
                        {
                            GridPolicyCodemapping.Columns[6].Visible = true;
                        }
                    }
                }
                if (dtGridData.Rows.Count <= 0)
                {
                    //GridPolicyCodemapping.DataSource = string.Empty;
                    //GridPolicyCodemapping.DataBind();

                    //objRow = dtGridData.NewRow();
                    //objRow["RowID"] = "";
                    //objRow["PolicyLob"] = "";
                    //objRow["CvgTypeCode"] = "";
                    //objRow["LossCode"] = "";
                    //objRow["policyname"] = "";

                    //dtGridData.Rows.Add(objRow);
                    //gvLossCodeMapping.DataSource = dtGridData;
                    //gvLossCodeMapping.DataBind();

                    if (_isFNOLChecked)//abhal3 MITS 36046
                    {
                        GridPolicyCodemapping.Columns[6].Visible = true;
                    }

                    //if (string.Compare(hdnFNOL.Value, "FNOLReserve", true) == 0)
                    //{
                    //    GridPolicyCodemapping.Columns[4].Visible = true;
                    //}

                    //if (gvLossCodeMapping.Rows[0].FindControl("cbLossCodeMappingGrid") != null)
                    //{
                    //    System.Web.UI.HtmlControls.HtmlInputCheckBox chkLossCodeMapping = ((System.Web.UI.HtmlControls.HtmlInputCheckBox)gvLossCodeMapping.Rows[0].FindControl("cbLossCodeMappingGrid"));
                    //    chkLossCodeMapping.Visible = false;
                    //}
                }
                lbLossType.Text = sRMALoss;
                //Ankit Start : Worked for MITS - 35096
                if (UsePolicyInterface)
                {
                    lblPolicySystemLossCodes.Text = sPointLoss;
                    GridPolicyCodemapping.Columns[5].HeaderText = string.Concat(sRMALoss, " (", sPointLoss, ")"); //abhal3 MITS 36046
                    GridPolicyCodemapping.Columns[2].Visible = true;//abhal3 MITS 36046

                }
                else
                {
                    GridPolicyCodemapping.Columns[2].Visible = false;//abhal3 MITS 36046
                    GridPolicyCodemapping.Columns[5].HeaderText = sRMALoss;//abhal3 MITS 36046
                }
                //Ankit End
                return dtGridData;//abhal3 MITS 36046
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            return null;

        }

        private void GetCodeType()
        {
            try
            {
                //Ankit Start : Worked on MITS - 34657
//                GetPolicySystem();
                //Ankit End

                XElement XmlTemplate = null;
                DataSet oDS = new DataSet();
                string sPolCode = "";
                string Data = "";
                bool bReturnStatus = false;
                XmlTemplate = GetMessageTemplateForCodeType();

                //txtPolicyClaimLOB.Text = lstPolicyClaimLOB.SelectedValue;
                bReturnStatus = CallCWS("LossCodeMappingAdaptor.GetCodeType", XmlTemplate, out Data, false, true);
                if (Data != null && !string.IsNullOrEmpty(Data))
                {
                    //Added By Nitika for FNOL Reserve
                    XmlDocument xmlRequest = new XmlDocument();
                    xmlRequest.LoadXml(Data);
                    if (xmlRequest.SelectSingleNode("//LossCodeMapping/FNOLReserve") != null)
                    {
                        if (xmlRequest.SelectSingleNode("//LossCodeMapping/FNOLReserve").InnerText == "Yes")
                        {
                            hdnFNOL.Value = "FNOLReserve";
                            XmlNode selectFilter = null;
                            selectFilter = xmlRequest.SelectSingleNode("ResultMessage/Document/LossCodeMapping/FNOLReserve");
                            xmlRequest.SelectSingleNode("ResultMessage/Document/LossCodeMapping").RemoveChild(selectFilter);
                        }
                    }
                    //Added By Nitika for FNOL Reserve
                    //Ankit Start : Worked for MITS - 35096
                    if (xmlRequest.SelectSingleNode("//LossCodeMapping/UsePI") != null)
                    {
                        bool success;
                        UsePolicyInterface = Conversion.CastToType<bool>(xmlRequest.SelectSingleNode("//LossCodeMapping/UsePI").InnerText, out success);
                    }
                    //Ankit End
                    if (UsePolicyInterface)//abhal3 MITS 36046
                    {
                        //Ankit Start : Worked on MITS - 34657
                        GetPolicySystem();
                        //Ankit End
                    }
                    oDS.ReadXml(new StringReader(Data));
                    ListItem item = null;

                    //Ankit Start : Worked on MITS - 36045
                    item = new ListItem();
                    item.Text = "Select";
                    item.Value = "-1";
                    lstCoverageType.Items.Add(item);
                    //End Ankit
                    for (int i = 0; i < oDS.Tables["option"].Rows.Count; i++)
                    {
                        item = new ListItem();
                        item.Text = oDS.Tables["option"].Rows[i]["option_text"].ToString();
                        item.Value = oDS.Tables["option"].Rows[i]["value"].ToString();

                        switch (oDS.Tables["option"].Rows[i]["table_name"].ToString().ToUpper())
                        {
                            case PolicyClaimLOB:
                                lstPolicyClaimLOB.Items.Add(item);
                                break;
                            case CoverageType:
                                lstCoverageType.Items.Add(item);
                                break;
                            //case LossCodes:
                            //    lstLossOfCode.Items.Add(item);
                            //    break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplateForCodeType()
        {
            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;
            string sPolSystemID = string.Empty;
            string sPolSysTypeID = string.Empty;
            //GetPolicySystemSelectedValue(ref sPolSystemID, ref sPolSysTypeID);
            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>LossCodeMappingAdaptor.GetCodeType</Function></Call>");
            sXML.Append("<Document><SystemTableName>");
            sXML.Append("'" + PolicyClaimLOB + "', '" + CoverageType + "', '" + LossCodes + "'");
            sXML.Append("</SystemTableName></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }
        private XElement GetMessageTemplateForLossType(bool blnIsRMALossType)   //Ankit Start : Worked on MITS - 34657
        {
            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;
            string sPolSystemID = string.Empty;
            string sPolSysTypeID = string.Empty;
            //GetPolicySystemSelectedValue(ref sPolSystemID, ref sPolSysTypeID);
            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>LossCodeMappingAdaptor.GetLossType</Function></Call>");
            //Ankit Start : Worked on MITS - 34657
            sXML.Append("<Document><LossCodeMappingAdaptor>");
            sXML.Append("<PolicyLOB>");
            sXML.Append(lstPolicyClaimLOB.SelectedValue);
            sXML.Append("</PolicyLOB>");
            sXML.Append("<CvgTypeCode>");
            sXML.Append(lstCoverageType.SelectedValue);
            sXML.Append("</CvgTypeCode>");
            //Ankit Start : Worked for MITS - 35096
            if (UsePolicyInterface)
            {
                string selectedPolicyvalue = string.Empty;//abhal3 MITS 36046
                string sPolicyTypeID = string.Empty;//abhal3 MITS 36046
                GetPolicySystemSelectedValue(ref selectedPolicyvalue, ref sPolicyTypeID);//abhal3 MITS 36046

                sXML.Append("<PolicySystemID>");
                //sXML.Append(lstPolicySystemNames.SelectedValue.Split('_')[0]);
                sXML.Append(selectedPolicyvalue);
                sXML.Append("</PolicySystemID>");
                sXML.Append("<PolicySystemType>");
                //sXML.Append(lstPolicySystemNames.SelectedValue.Split('_')[1]);
                sXML.Append(sPolicyTypeID);//abhal3 MITS 36046
                sXML.Append("</PolicySystemType>");
                sXML.Append("<PSCodeID>");
                sXML.Append(lstPolicySystemLosscode.SelectedValue);
                sXML.Append("</PSCodeID>");
            }
            else
            {
                sXML.Append("<PolicySystemID>0</PolicySystemID>");
                sXML.Append("<PolicySystemType>0</PolicySystemType>");
                sXML.Append("<PSCodeID>0</PSCodeID>");
            }
            //Ankit End
            sXML.Append("<IsRMALossType>");
            sXML.Append(blnIsRMALossType);
            sXML.Append("</IsRMALossType>");
            sXML.Append("</LossCodeMappingAdaptor></Document></Message>");
            //Ankit End
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplateForAdd()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("LossCodeMappingAdaptor.AddNewMapping");
            sXml = sXml.Append("</Function></Call><Document><LossCodeMappingAdaptor><AddCode>");
            sXml = sXml.Append("<PolLob>" + lstPolicyClaimLOB.SelectedValue + "</PolLob>");
            sXml = sXml.Append("<CvgTypeCode>" + lstCoverageType.SelectedValue + "</CvgTypeCode>");
            sXml = sXml.Append("<LossCode>" + lstLossOfCode.SelectedValue + "</LossCode>");
            //Ankit Start : Worked for MITS - 35096
            if (UsePolicyInterface)
            {
                string selectedPolicyvalue = string.Empty;//abhal3 MITS 36046
                string sPolicyTypeID = string.Empty;//abhal3 MITS 36046
                GetPolicySystemSelectedValue(ref selectedPolicyvalue, ref sPolicyTypeID);

                sXml = sXml.Append("<PolicySystemID>" + selectedPolicyvalue + "</PolicySystemID>");        //Ankit Start : Worked on MITS - 34657
                //sXml = sXml.Append("<PolicySystemID>" + lstPolicySystemNames.SelectedValue.Split('_')[0] + "</PolicySystemID>");        //Ankit Start : Worked on MITS - 34657

            }
            else
                sXml = sXml.Append("<PolicySystemID>0</PolicySystemID>");
            //Ankit End
            sXml = sXml.Append("</AddCode></LossCodeMappingAdaptor></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
		//abhal3 MITS 36046 start
        private void GetPolicySystemSelectedValue(ref string sPolSystemID, ref string sPolSysTypeID)
        {
            if (!string.IsNullOrEmpty(this.hdnPolicyDetails.Value))
            {
                string[] lstPolicySelectedValues = this.hdnPolicyDetails.Value.Split('|');
                foreach (string item in lstPolicySelectedValues.Distinct())
                {
                    string[] sarrPolSys = item.Split('_');
                    if (sarrPolSys.Length == 2)
                    {
                        if (!string.IsNullOrEmpty(sPolSystemID))
                        {
                            sPolSystemID = sPolSystemID + " , " + sarrPolSys[0];
                            if (!sPolSysTypeID.Contains(sarrPolSys[1]))
                                sPolSysTypeID = sPolSysTypeID + " , " + sarrPolSys[1];
                        }
                        else
                        {
                            sPolSystemID = sarrPolSys[0];
                            sPolSysTypeID = sarrPolSys[1];
                        }
                    }
                }
            }
            else if (listPolicySystem.Items.Count > 0)
            {
                string[] sarrPolSys = listPolicySystem.Items[0].Value.Split('_');
                if (sarrPolSys.Length == 2)
                {
                    sPolSystemID = sarrPolSys[0];
                    sPolSysTypeID = sarrPolSys[1];
                }
            }
        }
		//abhal3 MITS 36046 end
        private XElement GetMessageTemplateForAllMappings()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("LossCodeMappingAdaptor.GetAllMapping");
            sXml = sXml.Append("</Function></Call><Document><LossCodeMappingAdaptor>");
            sXml = sXml.Append("<AddCode><PolLob>" + lstPolicyClaimLOB.SelectedValue + "</PolLob>");
            //Ankit Start : Worked on MITS - 34657
            //Ankit Start : Worked for MITS - 35096
            if (UsePolicyInterface)
            {
                string selectedPolicyvalue = string.Empty;
                string sPolicyTypeID = string.Empty;
                GetPolicySystemSelectedValue(ref selectedPolicyvalue, ref sPolicyTypeID);

                //sXml = sXml.Append("<PolicySystemID>" + lstPolicySystemNames.SelectedValue.Split('_')[0] + "</PolicySystemID>");
                sXml = sXml.Append("<PolicySystemID>" + selectedPolicyvalue + "</PolicySystemID>");
                sXml = sXml.Append("<CodeID>" + lstPolicySystemLosscode.SelectedValue + "</CodeID>");
            }
            else
            {
                sXml = sXml.Append("<PolicySystemID>0</PolicySystemID>");
                sXml = sXml.Append("<CodeID>" + lstLossOfCode.SelectedValue + "</CodeID>");
            }
            //Ankit End
            sXml = sXml.Append("<CvgTypeCodeID>" + lstCoverageType.SelectedValue + "</CvgTypeCodeID>");
            //Ankit End
            sXml = sXml.Append("</AddCode>");
            sXml = sXml.Append("</LossCodeMappingAdaptor></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        private XElement GetMessageTemplateForDeletion()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("LossCodeMappingAdaptor.DeleteCodeMapping");
            sXml = sXml.Append("</Function></Call><Document><LossCodeMappingAdaptor><DeleteCode>");
            sXml = sXml.Append("<DeletedCode>" + this.hdnRowIds.Value + "</DeletedCode>");
            sXml = sXml.Append("</DeleteCode></LossCodeMappingAdaptor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        protected void GridPolicyCodemapping_DataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)//abhal3 MITS 36046
        {
            if (_isFNOLChecked)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    string RowID = Convert.ToString(((TableCell)item["RowID"]).Text);
                    TableCell cell = (TableCell)item["FNOLAddReserve"]; //accessing Table cell
                    cell.Attributes["onclick"] = String.Format("openWindow('{0}')", RowID);
                    cell.Text = "<u>Add Reserve</u>";
                }
            }
        }

        //protected void gvLossCodeMapping_RowDataBound(object Sender, GridViewRowEventArgs e)
        //{
        //     string sAddreserve = string.Empty;
        //     string RowID = string.Empty;
        //     if (string.Compare(hdnFNOL.Value, "FNOLReserve", true) == 0)
        //     {
        //         if (e.Row.RowType == DataControlRowType.DataRow)
        //         {

        //             if (e.Row.Cells[0].Text == "" && e.Row.Cells[1].Text.Replace("&nbsp;", "") == "" && e.Row.Cells[1].Text.Replace("&nbsp;", "") == "" && e.Row.Cells[1].Text.Replace("&nbsp;", "") == "")
        //             {
        //                 e.Row.Cells[4].Text = "";
        //             }
        //             else
        //             {
        //                 sAddreserve = "AddReserve";
        //                 RowID = DataBinder.Eval(e.Row.DataItem, "RowID").ToString();
        //                 e.Row.Cells[4].Text = "<a href='#'>" + sAddreserve + "</a>";
        //                 e.Row.Cells[4].Attributes["onclick"] = String.Format("openWindow('{0}')", RowID);
        //             }
        //         }
        //     }
        // }
        #endregion
    }
}