﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimLetterSetup.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ClaimLetterSetup" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>System Setup Parameters</title>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
    <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">{var i;}</script>
    
<script type="text/javascript">
				function AddFilter(mode)
					{
						var optionRank;
						var optionObject;						
						selectObject=document.getElementById('lstExcAdjusters');
						
						if (mode=="selected")
						{
							//Add selected Available Values
							select=document.getElementById('lstIncAdjusters');
							for (var x = select.options.length - 1; x >= 0 ; x--)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
									
									//remove from available list	
									removeOption('lstIncAdjusters',select.options[x].value);							
									setDataChanged(true);
								}
							}
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('lstIncAdjusters');
							if (select.options.length > 0)
							{
							    for (var x = select.options.length-1; x >= 0; x--)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
								}
								select.options.length=0;
							}
						}
						return false;
					}
					
					function RemoveFilter(mode)
					{
						var optionRank;
						var optionObject;
						selectObject=document.getElementById('lstIncAdjusters');
						
						if (mode=="selected")
						{
							//Add selected Available Values
							select=document.getElementById('lstExcAdjusters');
							for (var x = select.options.length-1; x >=0; x--)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
									
									//remove from available list	
									removeOption('lstExcAdjusters',select.options[x].value);							
									setDataChanged(true);
								}
							}
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('lstExcAdjusters');
							if (select.options.length > 0)
							{
							    for (var x = select.options.length-1; x >= 0; x--)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
								}
								select.options.length=0;
							}
						}
						return false;
					}
					
				function AddToTopdownLayout() {
				var selIndex = document.forms[0].lstExcAdjusters.selectedIndex;
				if(document.forms[0].lstExcAdjusters.selectedIndex<0)
					return false;
				var sValue=document.forms[0].lstExcAdjusters.options[document.forms[0].lstExcAdjusters.selectedIndex].value;
				var sText=document.forms[0].lstExcAdjusters.options[document.forms[0].lstExcAdjusters.selectedIndex].text;
				
				for(var f=0;f<document.forms[0].lstIncAdjusters.options.length;f++)
					if(document.forms[0].lstIncAdjusters.options[f].value==sValue)
						return false;

				document.forms[0].lstExcAdjusters.options[selIndex] = null;
				document.forms[0].lstExcAdjusters.selectedIndex = selIndex - 1;
				if(document.forms[0].lstExcAdjusters.selectedIndex == -1)
					document.forms[0].lstExcAdjusters.selectedIndex = 0;

				var opt=new Option(sText, sValue, false, false);
				document.forms[0].lstIncAdjusters.options[document.forms[0].lstIncAdjusters.options.length]=opt;
				m_DataChanged=true;
				return false;
				}
				
				function SelectAll() 
				{				
					hndlstExcAdjusters1 = document.getElementById('hdnExcAdjusters');
					//hndlstExcAdjusters = document.getElementById('hdnExcAdjusters');
					hdnIncAdjusters = document.getElementById('hdnIncAdjusters');
					var values = "";

					for (var f = 0; f < document.forms[0].lstIncAdjusters.options.length; f++) {
					    values += document.forms[0].lstIncAdjusters.options[f].value + "=" + document.forms[0].lstIncAdjusters.options[f].text + "|^|";
					}
					hdnIncAdjusters.value += values;

					values = "";
					for (var f = 0; f < document.forms[0].lstExcAdjusters.options.length; f++) {
					    values += document.forms[0].lstExcAdjusters.options[f].value + "=" + document.forms[0].lstExcAdjusters.options[f].text + "|^|";
					}
					hndlstExcAdjusters1.value = values;
					//hndlstExcAdjusters.value = values;
					pleaseWait.Show();
					return true;
				}
					
					function removeOption(selectName,id)
					{
						select=document.getElementById(selectName);
						Ids=new Array();
						Names=new Array();

						for(var x = 0; x < select.options.length ; x++)
						{
							if(select.options[x].value!=id)
							{
								Ids[Ids.length] = select.options[x].value;
								Names[Names.length]=select.options[x].text;
							}
						}

						select.options.length = Ids.length;

						for(var x = 0;x < select.options.length; x++)
						{
							select.options[x].text=Names[x];
							select.options[x].value=Ids[x];
						}
					}													
				</script>    
  <%--<script language="javascript" type="text/javascript" src="../../../Scripts/Utilities.js"></script>--%>
  </head>
  <%--<body class="" onload="SystemParameter_Load();loadTabList();parent.MDIScreenLoaded();" style="height:90%">--%>
 <body class="" onload="loadTabList();parent.MDIScreenLoaded();" style="height:90%">
    <form id="frmData" runat="server" >
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <input type="hidden" name="hTabName" />
        <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
         <tr>
          <td align="center" valign="middle" height="32">
          <asp:ImageButton tabindex="1" id="Save" ImageUrl="~/Images/tb_save_active.png" class="bold" ToolTip="Save" runat="server" onclick="Save_Click" OnClientClick = "return SelectAll();"/>
          </td>
         </tr>
        </table>
        </div>
        <div class="singletopborder" style="position:relative;left:0;top:0;width:100%;height:83%;overflow:auto;">                            
            <table border="0" cellspacing="0" cellpadding="0" id="FORMTABAck" >
                <tr id="Tr1">
                <td colspan="5">
                 <table width="100%" align="left">
                  <tr>
                   <%--<td class="ctrlgroup" width="100%">Acknowledgement (Ack) - Closed Claim Letter/Printer Settings</td>--%>
                   <td class="ctrlgroup" width="100%">Acknowledgement (Ack) - Closed Claim Letter Settings</td>                   
                  </tr>
                    <tr>
                        <td>                                    
                            <asp:HiddenField ID ="hdnIncAdjusters" runat="server" />                            
                            <asp:TextBox runat ="server" rmxref="/Instance/Document/Adjuster/control[@name='IncAdjusters']" rmxignoreget="true" ID="txtIncAdjusters" style="display :none"></asp:TextBox>
                        </td>
                    </tr>                  
                 </table>
                </td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <td width="100%">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width="30%" align="left">Excluded Adjusters</td>                                
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="30%" align="left">Included Adjusters</td>                            
                            </tr>                           
                            <tr> 
                                <td width="40%" valign="top">
                                    <asp:ListBox ToolTip="Adjusters from this list when assigned to a Claim or are edited/saved will NOT trigger Ack/Closed Claim Letters." tabindex="5" rmxref="/Instance/Document/Adjuster/control[@name='ExcAdjusters']" rmxignoreset="true" SelectionMode="Multiple" ID="lstExcAdjusters" runat="server" name="$node^26" size="10" width="208px"></asp:ListBox>                                    
                                </td>
                                <td>&nbsp;</td>
                                <td width="20%" valign="middle" align="left">
                                    <%--<asp:Button tabindex="3" runat="server" Text="     Add ->>   " class="button" Style="width: 100"
                                        ID="Add_Adjuster" OnClientClick="return AddFilter('selected');" /><br />
                                    <br />
                                    <asp:Button tabindex="4" runat="server" Text="<<- Remove" class="button" Style="width: 100" ID="Remove_Adjuster"
                                        OnClientClick="return RemoveFilter('selected');" />--%>
                                        <asp:Button tabindex="4" runat="server" Text="     Add ->>   " class="button" Style="width: 100" ID="Remove_Adjuster"
                                        OnClientClick="return RemoveFilter('selected');" />                                        
                                    <br />
                                    <asp:Button tabindex="3" runat="server" Text="<<- Remove" class="button" Style="width: 100"
                                        ID="Add_Adjuster" OnClientClick="return AddFilter('selected');" /><br />
                                    
                                </td>
                                <td>&nbsp;</td>
                                <td width="40%" valign="top" align="left">                                            
                                    <asp:ListBox ToolTip="Adjusters from this list when assigned to a Claim or are edited/saved will trigger Ack/Closed Claim Letters." tabindex="2" rmxref="/Instance/Document/Adjuster/control[@name='IncAdjusters']" rmxignoreset="true" SelectionMode="Multiple" ID="lstIncAdjusters" runat="server" name="$node^26" width="208px"></asp:ListBox>                                                                    
                                    <asp:HiddenField  ID ="hdnExcAdjusters" runat="server" />  
                                    <asp:TextBox runat ="server" rmxref="/Instance/Document/Adjuster/control[@name='ExcAdjusters']" rmxignoreget="true" ID="txtExcAdjusters" style="display :none"></asp:TextBox>                                    
                                </td>
                            </tr>                       
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr valign="top">                    
                    <td>
                        <table>
                            <tr><td align="left">Excluded Claim Type/s:</td></tr>
                            <tr>                                
                                <td>
                                    <uc:MultiCode ToolTip="Claims having claim type that is in this list are not eligible to receive Ack/Closed Claim Letters." rmxref="/Instance/Document/Adjuster/control[@name='XClaimTypes']" runat="server" ID="lstClaimTypes" width="200px" height="88px"  CodeTable="CLAIM_TYPE" ControlName="lstClaimTypes" RMXType="codelist"/>
                                </td>
                                <td valign="top">
                                    <table width="100%">
<%--Chubb does not needs printing
                                        <tr>
                                            <td valign="top" >Printer Settings:</td>
                                            <td>                                                        
                                                <asp:DropDownList ID="ddlPrinters" onchange="setDataChanged(true);" runat="server" rmxref="/Instance/Document/Adjuster/control[@name='PrinterName']" AutoPostBack='true' onselectedindexchanged="ddlPrinters_SelectedIndexChanged"></asp:DropDownList>                                                
                                           </td>                                                    
                                        </tr>                                                --%>
                                        <tr>
                                            <td valign="top" align="right">
                                                Template for Ack Claim Letter: 
                                            </td>
                                            <td>                                                       
                                                <asp:DropDownList ID="ddlAckTemplates" runat="server" 
                                                                  rmxref="/Instance/Document/Adjuster/control[@name ='ACKTemplates']/@value"
                                                                  itemSetref="/Instance/Document/Adjuster/control[@name ='ACKTemplates']"
                                                                  onchange="setDataChanged(true);">
                                                </asp:DropDownList>
                                           </td>
                                        </tr>                                    
                                        <tr>
                                            <td valign="top" align="right">
                                                Template for Closed Claim Letter: 
                                            </td>
                                            <td>                                                        
                                                <asp:DropDownList ID="ddlCLTemplates" runat="server" 
                                                                  rmxref="/Instance/Document/Adjuster/control[@name='CLTemplates']/@value"
                                                                  itemSetref="/Instance/Document/Adjuster/control[@name='CLTemplates']"
                                                                  onchange="setDataChanged(true);">
                                                </asp:DropDownList>
                                           </td>
                                        </tr>                                    
                                    </table>
                                </td>                                
                            </tr>
                        </table>
                    </td>
                </tr>
                                       
                <tr id="Tr8">
                    <td colspan=5>
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Closed Claim Letter (Specific)</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr valign="top">
                    <td>
                        <table>
                            <tr><td>Claim Status:</td></tr>
                            <tr>                                
                                <td>
                                  <uc:MultiCode ToolTip="Claims having status that is in this list are eligible to receive Ack/Closed Claim Letters." rmxref="/Instance/Document/Adjuster/control[@name='ClaimStatus']" runat="server" ID="ClaimStatus" width="200px" height="88px"  CodeTable="CLAIM_STATUS" ControlName="ClaimStatus" RMXType="codelist"/>
                                </td>                            
                            </tr>
                        </table>
                    </td>                    
                </tr>
            </table>          
        </div>
      <input type="text" name="" value="" id="SysViewType" style="display:none">
      <input type="text" name="" value="" id="SysCmd" style="display:none">
      <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none">
      <input type="text" name="" value="" id="SysCmdQueue" style="display:none">
      <input type="text" name="" value="" id="SysCmdText" style="display:none" >
      <input type="text" name="" value="" id="SysClassName" style="display:none" >
      <input type="text" name="" value="" id="SysSerializationConfig" style="display:none">
      <input type="text" name="" value="" id="SysFormIdName" style="display:none ">
      <input type="text" name="" value="" id="SysFormPIdName" style="display:none ">
      <input type="text" name="" value="" id="SysFormPForm" style="display:none">
      <input type="text" name="" value="" id="SysInvisible" style="display:none" >
      <input type="text" name="" value="" id="SysFormName" style="display:none" >
      <input type="text" name="" value="" id="SysRequired" style="display:none">
      <input type="text" name="" value="" id="SysFocusFields" style="display:none"></td>
        <script language="javascript" type="text/javascript">
            try {
                var elem = document.getElementById("dOracleCaseInsensitive");
                var elem1 = document.getElementById("dOracleCaseInsensitiveCheckBox"); 
                var ctrl = document.getElementById("hdnOracleCaseInsensitive");
                if (ctrl.value == 'true') {
                    elem.style.display = 'none';
                    elem1.style.display = 'none';
                }
                else {
                    elem.style.display = '';
                    elem1.style.display = '';
                }
    	      
            } catch (ex) { }
	        </script>	
    <asp:TextBox ID="SysWindowId" type="hidden" value="rmx-widget-handle-3" style="display:none"  runat="server"></asp:TextBox>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
