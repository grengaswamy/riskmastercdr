﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calculator.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.calculator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
     <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"/>
     <script language="Javascript" type="text/javascript">
<!--
/* Variablen definieren */
var plussign = ' + ';
var minussign = ' - ';
var multiplysign = ' * ';
var dividesign = ' / ';
var decimalsign = ' . ';
var negativesign = ' -';
var leftbracket = ' (';
var rightbracket = ') ';
var rad = 3.141592654 / 180;
var base_10 = Math.LN10;
var base_2 = Math.LN10;
function numLostFocus(ctrValue)
{
	if(ctrValue.length==0)
		return;
		
	if(isNaN(parseFloat(ctrValue)))
		ctrValue="";
	else
	{
		var ss=ctrValue.toString().split(" ");
		ctrValue=ss[0]+"."+ss[2];
		ctrValue=parseFloat(ctrValue);
	}
	return ctrValue;
}
/* Rechenfunktionen */
function calculate(arg) {
	//arg.expr.value = eval(arg.expr.value)

	
	arg.expr.value=numLostFocus(arg.expr.value)
	window.opener.CalculateValSelected(arg.expr.value);
	self.close();
}

function enter(arg, string) {
	arg.expr.value += string
	try
	{
	arg.expr.value = eval(arg.expr.value)
	}catch(ex){}
}

function clear_display(arg) {
	arg.expr.value = ' '
}

function calc_sqrt(form) {
	form.expr.value = (Math.sqrt(form.expr.value))
}

function calc_sqr(form) {
	form.expr.value = ((form.expr.value * 1) * (form.expr.value * 1))
}

function sin_form(form) {
	form.expr.value = (Math.sin (form.expr.value * rad))
}

function cos_form(form) {
	form.expr.value = (Math.cos (form.expr.value * rad))
}								

function tan_form(form) {
	form.expr.value = (Math.tan(form.expr.value * rad))
}

function inverse(form) {
	form.expr.value = ( 1 / (form.expr.value))
}

function base10_log(form) {
	form.expr.value = (Math.log(form.expr.value) / base_10)
}

function base2_log(form) {
	form.expr.value = (Math.log(form.expr.value) / base_2)
}
//-->
</script>
</head>
<body>
    <form id="form1" runat="server">
    <table border="0" width="100" bgcolor="#CCCCCC" align="center">
  <tr>
    <td width="100%" colspan="5"><input type="text" name="expr" size="35"
      action="calculate(this.form)" readonly></td>
  </tr>
  <tr>
    <td width="20%"><input type="button" value="    7    " onClick="enter(this.form, 7)"></td>
    <td width="20%"><input type="button" value="    8    " onClick="enter(this.form, 8)"></td>
    <td width="20%"><input type="button" value="    9    " onClick="enter(this.form, 9)"></td>
    <td width="40%" colspan="2"><input type="button" value="         C         " onClick="clear_display(this.form)"></td>
  </tr>
  <tr>
    <td width="20%"><input type="button" value="    4    " onClick="enter(this.form, 4)"></td>
    <td width="20%"><input type="button" value="    5    " onClick="enter(this.form, 5)"></td>
    <td width="20%"><input type="button" value="    6    " onClick="enter(this.form, 6)"></td>
    <td width="20%"><input type="button" value="    *     " onClick="enter(this.form,
multiplysign)"> </td>
    <td width="20%"> <input  type="button" value="     /   " onClick="enter(this.form,
dividesign)"></td>
  </tr>
  <tr>
    <td width="20%"><input type="button" value="    1    " onclick="enter(this.form, 1)"></td>
    <td width="20%"><input type="button" value="    2    " onclick="enter(this.form, 2)"></td>
    <td width="20%"><input type="button" value="    3    " onclick="enter(this.form, 3)"></td>
    <td width="20%"><input type="button" value="    +    " onClick="enter(this.form,
plussign)"> </td>
    <td width="20%"> <input  type="button" value="    -    " onClick="enter(this.form,
minussign)">
    </td>
  </tr>
  <tr>
    <td width="20%"><input type="button" value="    0    " onClick="enter(this.form, 0)"></td>
    <td width="20%"><input type="button" value="    .     " onClick="enter(this.form,
decimalsign)"></td>
    <td width="20%"><input type="button" value=" neg  " onClick="enter(this.form,
negativesign)"></td>
    <td width="40%" colspan="2"><input type="button" value="         =         " onClick="calculate(this.form)"></td>
  </tr>
</table>
    <div>
    
    </div>
    </form>
</body>
</html>
