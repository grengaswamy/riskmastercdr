﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransactionAutoDiscSetup.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.TransactionAutoDiscSetup" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Transaction - Reserver Type Mapping</title>
    <script type="text/javascript">

        function Selected() {
            var transType = document.forms[0].lstTransType.value;
            var reserveType = document.forms[0].lstReserveType.value;
            if (trim(transType) == "" || trim(reserveType) == "") {
                alert("Transaction Type and Reserve Type must be selected before adding to mappings.");
                return false;
            }
            return true;
        }

        function DeleteMapping() {
            var selected = "";
            var controlname = "";
            var flag = "0";
           
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        controlname = document.forms[0].elements[i].name;
                        if (controlname.indexOf("selMapping") > -1) {
                            if (document.forms[0].elements[i].checked == true) {
                                flag = "1";
                                if (selected == "") {
                                    selected = document.forms[0].elements[i].value;
                                }
                                else {
                                    selected = selected + "," + document.forms[0].elements[i].value;
                                }
                            }
                        }
                    }
                }
                document.forms[0].hdnSelected.value = selected;
                if (flag == "1")
                    return true;
                else
                    return false;
           
           
        }





        function SaveMapping() {
            var selected;
            
            var selTransType = document.forms[0].hdnAddedTransTypes.value.split(",");
            var selReserveType = document.forms[0].hdnAddedReserveTypes.value.split(",");


            if (document.forms[0].hdnAddedTransTypes.value != ",") {
                for (i = 0; i < selTransType.length; i++) {

                    if (selTransType[i] == document.forms[0].lstTransType.value) {

                        alert("Selected Transaction Type already in use");
                        return false;
                    }
                }
            }

            if (document.forms[0].hdnAddedReserveTypes.value != ",") {
                for (i = 0; i < selReserveType.length; i++) {

                    if (selReserveType[i] == document.forms[0].lstReserveType.value) {

                        alert("Selected Reserve Type already in use");
                        return false;
                    }
                }

            }

            return true;

            
        }
  
  
  
  
  
  
  
  
  
	</script>
	<script language="javascript" src="../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
	<script language="javascript" src="../../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
	<script language="javascript" src="../../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/calendar-alias.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js"></script>
</head>
<body  style="height:93%;width:99.5%" onload="parent.MDIScreenLoaded();">
  <form id="frmData" runat="server" method="post">
   <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  <table width="99%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td class="msgheader" colspan="4">Transaction - Reserve Type Mapping for Auto Discount Transactions</td>
    </tr>
    <tr>
     <td class="ctrlgroup" colspan="4">Only one Reserve Type could be mapped to single Transaction Type.<br />A Transaction Type should be used only one time.
     </td><%--skhare7:MITS  22930 't' of 'Reserve Type' is converted into Upper case'--%>
    </tr>
   </table>
   <table width="99%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
     <td class="ctrlgroup" colspan="4">Transaction Type - Reserve Type Mapping</td><%--skhare7:MITS  22930 Removed one 'type'--%>
    </tr>
   </table>
   <table cellspacing="0" cellpadding="0" border="0" width="99%">
    <tr>
     <td width="50%" align="left">Transaction Type</td>
     <td width="50%" align="left">Reserve Type</td>
     <td>
     
     <asp:TextBox style="display:none" runat="server" type="hidden" id="hdnSelected" />
     <asp:TextBox style="display:none" runat="server" type="hidden" id="hdnAddedTransTypes" />
     <asp:TextBox style="display:none" runat="server" type="hidden" id="hdnAddedReserveTypes" />
     </td>
    </tr>
    <tr>
     <td width="50%" align="left">
       <asp:DropDownList runat="server" ID="lstTransType">
       </asp:DropDownList></td>
     <td width="50%" align="left">
       <asp:DropDownList runat="server" ID="lstReserveType" >
       </asp:DropDownList></td>
     <td></td>
    </tr>
   </table>
   <div id="divForms" class="divScroll">
    <table width="98%" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <asp:GridView Width="98%" runat="server" ID="GrdMapping" 
             AutoGenerateColumns="false" AlternatingRowStyle-CssClass="rowdark" 
             onrowdatabound="GrdMapping_RowDataBound">
       <Columns>
        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="colheader5" ItemStyle-Width="5%">
            <ItemTemplate>
                <input type="checkbox" name="selMapping" id="selMapping" value='<%# DataBinder.Eval(Container, "DataItem.MapRowId")%>' runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
        
           <asp:TemplateField HeaderText="MapRowId" ItemStyle-Width="" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left" Visible="false">
            <ItemTemplate>
                <asp:Label runat="server" ID="MapRowId" Text='<%# DataBinder.Eval(Container, "DataItem.MapRowId")%>' />
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="TransCodeId" ItemStyle-Width="" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left" Visible="false">
            <ItemTemplate>
                <asp:Label runat="server" ID="TransCodeId" Text='<%# DataBinder.Eval(Container, "DataItem.TransCodeId")%>' />
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Transaction Type" ItemStyle-Width="50%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:Label runat="server" ID="transactionName" Text='<%# DataBinder.Eval(Container, "DataItem.TransType")%>' />
            </ItemTemplate>
        </asp:TemplateField>
        
         <asp:TemplateField HeaderText="ReserveCodeid" ItemStyle-Width="" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left" Visible="false">
            <ItemTemplate>
                <asp:Label runat="server" ID="ReserveCodeid" Text='<%# DataBinder.Eval(Container, "DataItem.ReserveCodeid")%>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Reserve Type" ItemStyle-Width="50%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:Label runat="server" ID="ReserveName" Text='<%# DataBinder.Eval(Container, "DataItem.ReserveType")%>' />
            </ItemTemplate>
        </asp:TemplateField>
       </Columns>
       
      </asp:GridView>  
     </tr>
    </table>
   </div>
      
   <table width="99%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td width="99%" align="right">
      <asp:Button runat="server" ID="btnSave" type="submit" Text="Save Mapping" class="button" OnClientClick="return SaveMapping();" OnClick="SaveMapping" />
      <asp:Button runat="server" ID="btnDelete" type="submit" Text="Delete Mapping" class="button" OnClientClick="return DeleteMapping();" OnClick="DeleteMapping" />
     </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
   </table>
 
   
   </form>
 </body>
</html>

