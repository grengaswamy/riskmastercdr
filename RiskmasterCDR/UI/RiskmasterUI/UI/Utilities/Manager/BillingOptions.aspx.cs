﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Text;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class BillingOptions : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            string selectedRowId = string.Empty;
            string sGridName = string.Empty;
            string sFunctionName = string.Empty;
            try
            {

                if (!string.IsNullOrEmpty(SelectedGrid.Text))
                {
                    sGridName = SelectedGrid.Text;
                    TextBox txtGridDeletedFlag = (TextBox)this.FindControl(sGridName + "_RowDeletedFlag");
                    if (txtGridDeletedFlag != null && txtGridDeletedFlag.Text.ToLower() == "true")
                    {
                        string sTempName=sGridName.Substring(0,sGridName.IndexOf("Grid"));
                        TextBox txtSelectedRowId = (TextBox)this.FindControl(sTempName + "SelectedId");
                        if (txtSelectedRowId != null)
                        {
                            selectedRowId = txtSelectedRowId.Text;
                            XmlTemplate = GetMessageTemplate(selectedRowId);
                            if(sTempName.IndexOf("BillingRules")!= -1)
                                sFunctionName = "BillingRuleAdaptor.Delete";
                            else if (sTempName.IndexOf("PayPlans") != -1)
                                sFunctionName = "BRPayPlanAdaptor.Delete";
                            else if (sTempName.IndexOf("EarlyPayDiscounts") != -1)
                                sFunctionName = "EarlyPayDiscountsAdaptor.Delete";

                            CallCWS(sFunctionName, XmlTemplate, out sCWSresponse, false, false);
                            txtGridDeletedFlag.Text = "false";
                        }
                    }
                }
                XmlTemplate = GetMessageTemplate();
                CallCWSFunctionBind("BillingOptionsAdaptor.Get", out sCWSresponse, XmlTemplate);
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                SelectedGrid.Text = "";
            }

        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<BillingOptions><form>");
            sXml = sXml.Append("<group></group>");
            sXml = sXml.Append("</form></BillingOptions>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
        private XElement GetMessageTemplate(string selectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><DeletionData>");
            sXml = sXml.Append("<control name='RowId'>");
            sXml = sXml.Append(selectedRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</DeletionData></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
    }
}
