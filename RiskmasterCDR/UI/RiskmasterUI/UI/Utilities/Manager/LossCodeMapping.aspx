﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LossCodeMapping.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.LossCodeMapping" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"  TagPrefix="mc" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Loss Code Mapping</title>
    <script type="text/JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/JavaScript" src="../../../Scripts/Utilities.js"></script>
    <script type="text/javascript">
        //function SelectAll(chkID) {
        //    for (var i = 0; i < document.forms[0].elements.length; i++) {
        //        if (document.forms[0].elements[i].type == 'checkbox') {
        //            document.forms[0].elements[i].checked = chkID.checked;
        //        }
        //    }
        //}
        function SelectedlistPolicySystem(list) {
           
            var hdn = document.getElementById('hdnPolicyDetails');
            if (hdn != null) {
                hdn.value = "";
                for (var count = 0; count < list.options.length; count++) {
                    //if the option is selected, delete the option
                    if (list.options[count].selected == true) {
                        hdn.value = hdn.value + '|' + list.options[count].value;
                    }
                }
            }
            //var btn = document.getElementById('btnpolicysystem');
            //if (btn != null)
            //    btn.click();
        }

        function ValidateAdd() {
            var flag = true;
            var checkText;
            var ddlPolicyClaimLOB = document.getElementById("lstPolicyClaimLOB");
            var ddlCoverageType = document.getElementById("lstCoverageType");
            var ddlLossOfCode = document.getElementById("lstLossOfCode");
            var dllPolicySystemLosscode = document.getElementById("lstPolicySystemLosscode");
                     ddlPolicyClaimLOB = ddlPolicyClaimLOB.options[ddlPolicyClaimLOB.selectedIndex].value;            
            ddlCoverageType = ddlCoverageType.options[ddlCoverageType.selectedIndex].value;
            if (ddlLossOfCode.selectedIndex != -1)      //Ankit Start : Worked on MITS - 34657
                ddlLossOfCode = ddlLossOfCode.options[ddlLossOfCode.selectedIndex].value;
            checkText = ddlPolicyClaimLOB + ddlCoverageType + ddlLossOfCode;

            if (document.getElementById('hdValidateKey').value.indexOf(checkText) > 0) {
                flag = false;
                alert("Can not Insert Duplicate Mapping.");
                return flag;
            }
            

            if (ddlPolicyClaimLOB != null) {
                if (ddlPolicyClaimLOB == -1) {
                    flag = false;
                    alert("Please Select Policy LOB.");
                    return flag;
                }
            }
            
            //ajohari2 JIRA RMA-5392: Start 
            if (ddlCoverageType != null) {
                if (ddlCoverageType == -1) {
                    flag = false;
                    alert("Please Select Coverage Type.");
                    return flag;
                }
            }

            
            if (dllPolicySystemLosscode != null && dllPolicySystemLosscode.value == "-1") {
                flag = false;
                alert("Please Select Policy System Loss Codes.");
                return flag;
            }

            if (ddlLossOfCode == -1) {
                flag = false;
                alert("Please Select RMA Loss Of Code.");
                return flag;
            }
            //ajohari2 JIRA RMA-5392: End

            return flag;
        }

        function DeleteCode() {
            
            var selectedVal = "";
            if (SelectionValid()) {
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        if (document.forms[0].elements[i].checked == true) {
                            if (selectedVal == "") {
                                selectedVal = document.forms[0].elements[i].value;
                            }
                            else {
                                selectedVal = selectedVal + "," + document.forms[0].elements[i].value;
                            }
                        }
                    }
                }
                document.forms[0].hdnSelected.value = selectedVal;
                return true;
            }
            else {
                return false;
            }
        }

        function SelectionValid() {
          
            for (var i = 0; i < document.forms[0].elements.length; i++)
                if (document.forms[0].elements[i].type == 'checkbox')
                    if (document.forms[0].elements[i].checked == true)
                        return true;
            alert("Select a record to delete")//sharishkumar Jira 6288
            return false;
        }

        function onPageLoaded() {
            if (ie) {
                if ((eval("document.all.divForms") != null) && (ieversion >= 6)) {
                    eval("document.all.divForms").style.height = 300;
                }
            }
            else {
                var o_divforms;
                o_divforms = document.getElementById("divForms");
                if (o_divforms != null) {
                    o_divforms.style.height = window.frames.innerHeight * 0.70;
                    o_divforms.style.width = window.frames.innerWidth * 0.995;
                }
            }
            loadTabList();
        }

        function getValue(ctrlName) {
          

            if (ctrlName == "ImportToPolicySystem") {
                var filename = document.getElementById("uploadFile");
                if (filename.value == "") {
                    alert("Please provide the excel file to be uploaded");
                    return false;
                }



                if (filename.value.indexOf(".xlsx", filename.value - ".xlsx".length) <=0 ) {
                    alert("Only .xlsx file format is supported");
                    return false;
                }
            }

            var listboxname = document.getElementById(ctrlName);
            var selectedvalue = "";
            if (listboxname != null) {
                for (var i = 0; i < listboxname.options.length; i++) {
                    if (listboxname.options[i].selected == true) {
                        if(selectedvalue=="")
                            selectedvalue = listboxname.options[i].value.split('_')[0];
                        else
                            selectedvalue = selectedvalue + "," + listboxname.options[i].value.split('_')[0];
                    }
                }
            }

            if (selectedvalue.trim() == "") {
                alert("Please select policy systems");
                return false;
            }
            var hdnLstBoxSelectedItem = document.getElementById("hdnLstBoxSelectedItem");
            if (hdnLstBoxSelectedItem != null) {
                hdnLstBoxSelectedItem.value = selectedvalue;
              
            }
        }
        function openWindow(RowID)
        {
            window.open('FNOL.aspx?RowID=' + RowID, 'open_window', ' width=640, height=480, left=0, top=0'); 
        }    
        
        function getSelectedTab(tabname) {

            var SelectedTab = document.getElementById("SelectedTab");
            if (SelectedTab != null) {
                SelectedTab.value = tabname;
            }
            return true;
        }
    </script>
    <style type="text/css">
        .style1
        {
            width: 13%;
        }
    </style>
</head>
<body onload="javascript:onPageLoaded();loadTabList();parent.MDIScreenLoaded()">
    <form id="frmLossCodeMapping" name="frmLossCodeMapping" method="post" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <asp:ScriptManager ID="ScriptManager2" runat="server" EnablePageMethods="true"/>
        <asp:Label ID="lbSuccess" runat="server" Visible="false"  Style="font-weight: bold;
                        color: blue"></asp:Label>
          <input type="hidden" name="hTabName" />
          <input id="SelectedTab" type="hidden" runat="server" value="" />
    <table width="100%" border="0" cellspacing="3" cellpadding="3">
        <tr>
            <td class="ctrlgroup" colspan="4" width="100%">
                Loss Code Mapping

            </td>
        </tr>
        </table>
         <div class="tabGroup" id="TabsDivGroup" runat="server">
            <div class="Selected" nowrap="true" runat="server" name="TABSLossCodeMapping" id="TABSLossCodeMapping">
                <a class="Selected" href="#" onclick="tabChange(this.name);getSelectedTab(this.name);return false;" runat="server"
                    rmxref="" name="LossCodeMapping" id="LINKTABSLossCodeMapping">Loss Code Mapping</a>
            </div>
            <div class="tabSpace" runat="server" id="Div1">

                <nbsp />
                <nbsp />
            </div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSImportLossCodes" id="TABSImportLossCodes">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);getSelectedTab(this.name);return false;" runat="server"
                    rmxref="" name="ImportLossCodes" id="LINKTABSImportLossCodes">Import Loss Code Mapping</a>
            </div>
            <div class="tabSpace" runat="server" id="TBSImportLossCodes">
                <nbsp />
                <nbsp />
            </div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSExportLossCodes" id="TABSExportLossCodes">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);getSelectedTab(this.name);return false;" runat="server"
                    rmxref="" name="ExportLossCodes" id="LINKTABSExportLossCodes">Export Loss Code Mapping</a>
            </div>
            <div class="tabSpace" runat="server" id="TBSExportLossCodes">
                <nbsp />
                <nbsp />
            </div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSReplication" id="TABSReplication">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);getSelectedTab(this.name);return false;" runat="server"
                    rmxref="" name="Replication" id="LINKTABSReplication">Replicate Loss Code Mapping</a>
            </div>
            <div class="tabSpace" runat="server" id="TBSReplication">
                <nbsp />
                <nbsp />
            </div>
           
        </div>


    <table width="100%" border="0" cellspacing="3" cellpadding="3"  name="FORMTABLossCodeMapping" id="FORMTABLossCodeMapping">
        <tr>
            <td colspan="2" class="style1">
                <asp:TextBox runat="server" value="1" Style="display: none" ID="hdnAlreadyExists"></asp:TextBox>
                <asp:TextBox runat="server" Style="display: none" rmxignorevalue="true" ID="hdnSelected"></asp:TextBox>

            </td>
        </tr>
        <!--Ankit Start : Worked for MITS - 34657-->
        <tr id="trPolicySystemNames" runat="server">
            <td width="5%">
                Policy System Names
            </td>
            <td width="30%">
                <%--<asp:DropDownList ID="lstPolicySystemNames" AutoPostBack="true" type="combobox" 
                    runat="server" 
                    onselectedindexchanged="lstPolicySystemNames_SelectedIndexChanged" />--%>
                <asp:ListBox type="combobox"
                            runat="server"
                            ID="listPolicySystem" TabIndex="1"
                            size="3" Height="100px" Style="margin-top: 0px" Width="150px"    
                            onClick="javascript:SelectedlistPolicySystem(this);" AutoPostBack="True"  
                            SelectionMode="Multiple"  onselectedindexchanged="lstPolicySystemNames_SelectedIndexChanged"></asp:ListBox>
              <%--  <asp:Button ID="btnpolicysystem" runat="server" Text="Button" Style="display: none" OnClick="btnpolicysystem_Click"/>--%>
                <asp:HiddenField ID="hdnPolicyDetails" runat="server" Value="" />
            </td>
        </tr>
        <tr>
            <td width="5%">
        <!--Ankit End-->
                Policy LOB
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstPolicyClaimLOB" AutoPostBack="true" type="combobox" 
                    runat="server" 
                    onselectedindexchanged="lstPolicyClaimLOB_SelectedIndexChanged" />
            </td>
        </tr>
        <!--Ankit Start : Worked for MITS - 34657-->
        <tr>
            <td>
                Coverage Type
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstCoverageType" type="combobox"
                    runat="server"
                    onselectedindexchanged="lstCoverageType_SelectedIndexChanged" />
            </td>
        </tr>
        <tr id="trPSLossCodes" runat="server">
            <td width="5%">
                <asp:Label ID="lblPolicySystemLossCodes" runat="server" Text = "Policy System Loss Codes"/>
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstPolicySystemLosscode" AutoPostBack="true" type="combobox" 
                    runat="server" 
                    onselectedindexchanged="lstPolicySystemLosscode_SelectedIndexChanged" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbLossType" runat="server" Text = "RMA Loss Of Code"/>
        <!--Ankit End-->
            </td>
            <td>
                <asp:DropDownList ID="lstLossOfCode" type="combobox" AutoPostBack="true" 
                    runat="server" 
                    onselectedindexchanged="lstLossOfCode_SelectedIndexChanged" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" Text="Add Mapping" class="button" ID="addMappingBtn" OnClick="addMappingBtn_Click" OnClientClick="return ValidateAdd();" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button runat="server" Text="Remove Mapping" class="button" OnClientClick="return DeleteCode();" OnClick="removeMappingBtn_Click" ID="removeMappingBtn" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%" style="margin-right: 0px; height: 191px;">
                    <tr>
                        <td width="80%">
                            <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
                                <script type="text/javascript">
                                    var selected = {};
                                    function OnRowClick(sender, eventArgs) {
                                        var sCBIds = "";
                                        var CBId = eventArgs.getDataKeyValue("RowID");
                                        if (!selected[CBId]) {
                                            selected[CBId] = true;
                                        }
                                        for (x in selected) {
                                            if (selected[x]) {
                                                //iSelectdItemsCnt = iSelectdItemsCnt + 1;
                                                if (sCBIds == "") {
                                                    sCBIds = x;
                                                }
                                                else {
                                                    sCBIds = sCBIds + "," + x;
                                                }
                                            }
                                        }
                                        if (document.getElementById('hdnRowIds') != null) {
                                            document.getElementById('hdnRowIds').value = sCBIds;
                                        }
                                        return false;
                                    }
                                    function OnRowDeselect(sender, e) {
                                        var sCBIds = "";
                                        var CBId = e.getDataKeyValue("RowID");
                                        if (!selected[CBId]) {
                                            selected[CBId] = null;
                                        }
                                        for (x in selected) {
                                            if (selected[x]) {
                                                if (sCBIds == "") {
                                                    sCBIds = x;
                                                }
                                                else {
                                                    sCBIds = sCBIds + "," + x;
                                                }
                                            }
                                        }
                                        if (document.getElementById('hdnRowIds') != null) {
                                            document.getElementById('hdnRowIds').value = sCBIds;
                                        }
                                        return false;
                                    }
                                    function GridCreated(sender, eventArgs) {
                                        var x;
                                        for (x in selected) {
                                            selected[x] = null;
                                        }
                                        return false;
                                    }
                                    function RadGrid1_RowCreated(sender, args) {
                                        return false;
                                    }
                                </script>
                                </telerik:RadCodeBlock>
                            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                                <AjaxSettings>
                                    <telerik:AjaxSetting AjaxControlID="GridPolicyCodemapping">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="GridPolicyCodemapping" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                </AjaxSettings>
                            </telerik:RadAjaxManager>
                            <asp:HiddenField ID="hdnRowIds" runat="server" Value="" />
                            <br />
                            <br />
                            <telerik:RadGrid runat="server" EnableLinqExpressions="false" AutoGenerateColumns="false"
                                AllowFilteringByColumn="true" ID="GridPolicyCodemapping" AllowPaging="true" AllowSorting="true" OnNeedDataSource="GridPolicyCodemapping_NeedDataSource" 
                                Skin="Office2007" GridLines="None" OnPreRender="GridPolicyCodemapping_PreRender" AllowMultiRowSelection="true" OnItemDataBound="GridPolicyCodemapping_DataBound">

                                <SelectedItemStyle CssClass="SelectedItem" />
                                <ClientSettings AllowKeyboardNavigation="true">
                                    <ClientEvents OnRowSelected="OnRowClick" OnRowDeselected="OnRowDeselect" OnGridCreated="GridCreated" OnRowCreated="RadGrid1_RowCreated"  />
                                    <Scrolling SaveScrollPosition="true" AllowScroll="true" />
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                                <MasterTableView Width="97.9%"  ClientDataKeyNames="RowID" DataKeyNames="RowID">
                                    <RowIndicatorColumn>
                                        <HeaderStyle Width="20px" />
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn>
                                        <HeaderStyle Width="20px" />
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridClientSelectColumn ItemStyle-Width="5px">
                                        </telerik:GridClientSelectColumn>
                                        <telerik:GridBoundColumn DataField="RowID" HeaderText="Row Id" Visible="false"
                                            SortExpression="RowID" UniqueName="RowID">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="policyname" HeaderText="Policy System" Visible="false"
                                            SortExpression="policyname" UniqueName="policyname">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="PolicyLob" HeaderText="Policy Claim LOB"
                                            SortExpression="PolicyLob" UniqueName="PolicyLob">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CvgTypeCode" HeaderText="Coverage Type"
                                            SortExpression="CvgTypeCode" UniqueName="CvgTypeCode">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="LossCode" HeaderText="RMA Loss Code (Policy System Loss Code)"
                                            SortExpression="LossCode" UniqueName="LossCode" >
                                        </telerik:GridBoundColumn>
                                        <%--<telerik:GridBoundColumn DataField="FNOLAddReserve" HeaderText="Add Reserve"
                                            SortExpression="FNOLAddReserve" UniqueName="FNOLAddReserve">
                                        </telerik:GridBoundColumn>--%>
                                       <telerik:GridHyperLinkColumn DataNavigateUrlFormatString="FNOLAddReserve" AllowFiltering="false" UniqueName="FNOLAddReserve" HeaderText="Add Reserve" Visible="false">
                                        </telerik:GridHyperLinkColumn>
                                    </Columns>
                                    <ItemStyle CssClass="datatd1" />
                                    <AlternatingItemStyle CssClass="datatd" />
                                    <HeaderStyle CssClass="msgheader" />
                                </MasterTableView>
                                <PagerStyle Mode="NextPrevAndNumeric" NextPageToolTip="<%$ Resources:ttNext %>" PrevPageToolTip="<%$ Resources:ttPrev %>"
                                    FirstPageToolTip="<%$ Resources:ttFirst %>" LastPageToolTip="<%$ Resources:ttLast %>" Position="Top" AlwaysVisible="true" />
                            </telerik:RadGrid>
                            <div style="width: 98%; height: 580px; overflow: auto">
                                <%--<asp:GridView ID="gvLossCodeMapping" runat="server" AutoGenerateColumns="False"
                                    Font-Names="Tahoma" Font-Size="Smaller" CssClass="singleborder" Width="98%"
                                    CellPadding="4" ForeColor="#333333" HorizontalAlign="Left" 
                                    EnableModelValidation="True" AllowPaging="True" PageSize="25" 
                                    onpageindexchanging="gvLossCodeMapping_PageIndexChanging" OnRowDataBound="gvLossCodeMapping_RowDataBound">
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <input id="cbHdrSelectAll" name="cbHdrSelectAll" runat="server" type="checkbox" onclick="javascript:SelectAll(this)" value='-1' />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <input id="cbLossCodeMappingGrid" name="LossCodeMappingGrid" runat="server" type="checkbox"
                                                    value='<%# Eval("RowID")%>' />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="colheader5" Width="10%" />
                                            <ItemStyle Width="10%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="PolicyLob" HeaderText="Policy Claim LOB" HeaderStyle-CssClass="colheader5" >
                                            <HeaderStyle CssClass="colheader5" Width="20%"></HeaderStyle>
                                        <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CvgTypeCode" HeaderText="Coverage Type" HeaderStyle-CssClass="colheader5" >
                                            <HeaderStyle CssClass="colheader5" Width="20%"></HeaderStyle>
                                        <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LossCode" HeaderText="RMA Loss Code (Policy System Loss Code)" HeaderStyle-CssClass="colheader5" >
                                            <HeaderStyle CssClass="colheader5" Width="50%"></HeaderStyle>
                                        <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                        <asp:TemplateField Visible ="false">
                                                 <HeaderTemplate>                                  
                                                 </HeaderTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerStyle BackColor="#507CD1" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle CssClass="colheader3" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>--%>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
           <table border="0" cellspacing="0" cellpadding="0" name="FORMTABImportLossCodes" id="FORMTABImportLossCodes"
                style="display: none;">

                   <tr >
            <td width="5%">
                Import To Policy Systems
            </td>
              
            <td width="30%">
                <asp:listbox ID="ImportToPolicySystem" AutoPostBack="false"   SelectionMode="multiple"
                    runat="server"  />
                    <%--onselectedindexchanged="lstPolicySystemNames_SelectedIndexChanged" />--%>
            </td>
        </tr>
               <tr>
                   <td>Select Import File Path</td><td>
                 <asp:FileUpload ID="uploadFile" runat="server"  /> (Only .xlsx file format is supported)</td></tr> 
               <tr><td>
               <asp:Button runat="server" Text="Import Mapping" class="button" OnClientClick="return getValue('ImportToPolicySystem');" OnClick="importMappingBtn_Click" ID="btnImportMapping" />
                   </td></tr>
               <tr><td>
                 <asp:Button ID="btnDownLoadLog" runat="server" Text="Download Import Log" OnClick="btnDownLoadLog_Click" Visible="false" />
                   </td></tr>
               </table>
         <table border="0" cellspacing="0" cellpadding="0" name="FORMTABExportLossCodes" id="FORMTABExportLossCodes"
                style="display: none;">

               <tr>
            <td width="5%">
                Export From Policy System
            </td>
                
            <td width="30%">
                
                <asp:DropDownList ID="ExportFromPolicySystem" AutoPostBack="false" type="combobox" runat="server" />
                    <%--onselectedindexchanged="lstPolicySystemNames_SelectedIndexChanged" />--%>
            </td>
        </tr>
             <tr><td>
               <asp:Button runat="server" Text="Export Mapping" class="button"  OnClick="ExportMappingBtn_Click" ID="btnExportMapping" />
                 </td></tr>
             <tr>
                 <td>

                      <asp:Button ID="btnReplicateLog" runat="server" Text="Download Replication Log" OnClick="btnReplicateLog_Click" Visible="false" />
                 </td>
             </tr>
               </table>
         <table border="0" cellspacing="0" cellpadding="0" name="FORMTABReplication" id="FORMTABReplication"
                style="display: none;">

               <tr id="tr1" runat="server">
            <td width="5%">
                Import From Policy System
            </td>
                
            <td width="30%">
                <asp:DropDownList ID="ReplicateFromPolicySystem" AutoPostBack="false" type="combobox" 
                    runat="server"  />
                   <%-- onselectedindexchanged="lstPolicySystemNames_SelectedIndexChanged" />--%>
            </td>
        </tr>

               <tr id="tr2" runat="server">
            <td width="5%">
                Import To Policy System
            </td>
            <td width="30%">
                <asp:listbox ID="ReplicateToPolicySystem" AutoPostBack="false"  SelectionMode="multiple" 
                    runat="server" />
                    <%--onselectedindexchanged="lstPolicySystemNames_SelectedIndexChanged" />--%>
            </td>
        </tr>
             <tr><td>
             <asp:Button runat="server" Text="Replicate Mapping" class="button" OnClientClick="return getValue('ReplicateToPolicySystem');" OnClick="replicateMappingBtn_Click" ID="btnReplicate" />
                 </td></tr>
               </table>
        <input id="hdValidateKey" type="hidden" runat="server" value="" />
        <input id="hdnLstBoxSelectedItem" type="hidden" runat="server" value="" />
        <asp:HiddenField  runat= "server" ID="hdnEditValue" />
        <asp:HiddenField  runat= "server" ID="hdnFNOL" />

    </form>
</body>
</html>
