﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    /// <summary>
    /// Class Name : AdjusterScreens
    /// Author : Animesh Sahai
    /// MITS 18738
    /// </summary>
    public partial class AdjusterScreens : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        XmlDocument XmlResponse = new XmlDocument();
        XmlNodeList XmlHeadList = null;
        string sCWSresponse = "";
        protected void Page_Init(object sender, EventArgs e)
        {
                   }
        /// <summary>
        /// event handler for the Page load event
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            TextBox tControl = null;
            XmlDocument XmlResponse = new XmlDocument();
            XmlNodeList XmlHeadList = null;
            string sCWSresponse = "";
            try
            {
                if (AdjusterScreensGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = AdjusterScreensSelectedId.Text;
                    XmlTemplate = GetMessageTemplate(selectedRowId);
                    CallCWS("AdjusterScreenAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                    AdjusterScreensGrid_RowDeletedFlag.Text = "false";
                }

                XmlTemplate = GetMessageTemplate();
                CallCWSFunctionBind("AdjusterScreensAdaptor.Get", out sCWSresponse, XmlTemplate);
                if (!IsPostBack)
                {
                   
                    
                    tControl = (TextBox)this.Form.FindControl("AdjusterScreensGrid_OtherParams");
                    XmlResponse.LoadXml(sCWSresponse);
                    XmlHeadList = XmlResponse.SelectNodes("//listhead/*");
                    foreach (XmlNode XmlElm in XmlHeadList)
                    {
                        switch (XmlElm.Name)
                        {
                            case "LOBs":
                            case "Departments":
                            case "ClaimTypes":
                            case "Jurisdictions":
                            case "ForceWorkItems":
                            case "SkipWorkItems":
                                tControl.Text += "&" + XmlElm.Name + "=1";
                                break;
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                XmlTemplate = null;  
            }
        }

        /// <summary>
        /// Method to generate the XML template for Adjuster list screen
        /// Author : Animesh Sahai
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            try
            {
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document>");
                sXml = sXml.Append("<AdjusterScreensList><listhead>");
                sXml = sXml.Append("<RowId>RowId</RowId>");
                sXml = sXml.Append("<AdjusterEID>Adjuster</AdjusterEID>");
                sXml = sXml.Append("<LOBs>LOBs</LOBs>");
                sXml = sXml.Append("<Departments>Departments</Departments>");
				sXml = sXml.Append("<ClaimTypes>Claim Type</ClaimTypes>");
				sXml = sXml.Append("<Jurisdictions>Jurisdiction</Jurisdictions>");
                //smishra54: MITS 26222- Added the integer type for the fields for supporting number sorting on teleric grid
				sXml = sXml.Append("<ForceWorkItems type='Integer'>Force Work Items</ForceWorkItems>");
                sXml = sXml.Append("<ClaimCounter type='Integer'>Work Items</ClaimCounter>");
                sXml = sXml.Append("<SkipWorkItems type='Integer'>Skip Work Items</SkipWorkItems>");
                //smishra54:End
                sXml = sXml.Append("<CurrentlyAvailable>Currently Available</CurrentlyAvailable>");
                sXml = sXml.Append("</listhead></AdjusterScreensList>");
                sXml = sXml.Append("</Document></Message>");
                XElement oElement = XElement.Parse(sXml.ToString());
                return oElement;
            }
            finally
            {
                sXml = null;  
            }
            
        }

        /// <summary>
        /// Method to generate the XML template for the selected record.
        /// Author : Animesh Sahai
        /// </summary>
        /// <param name="strSelectedRowId">selected record id</param>
        /// <returns></returns>
        private XElement GetMessageTemplate(string strSelectedRowId)
        {   
            StringBuilder sXml = new StringBuilder("<Message>");
            try
            {
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document><AdjusterScreens>");
                sXml = sXml.Append("<control name='AutoAssignID'>");
                sXml = sXml.Append(strSelectedRowId);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("</AdjusterScreens></Document>");
                sXml = sXml.Append("</Message>");
                XElement oElement = XElement.Parse(sXml.ToString());
                return oElement;
            }
            finally
            {
                sXml = null;
            }
        }
    }
}
