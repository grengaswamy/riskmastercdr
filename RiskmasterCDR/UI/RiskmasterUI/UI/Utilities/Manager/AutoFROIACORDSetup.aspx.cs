﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class AutoFROIACORDSetup : NonFDMBasePageCWS
    {
        private XElement XmlTemplate = null;
        XElement oMessageElement = null;
        private bool bReturnStatus = false;
        private string sreturnValue = string.Empty;
        private XmlDocument XmlDoc = new XmlDocument();
        private string sNode = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    oMessageElement = GetMessageTemplate();
                    CallCWS("AutoFroiAcordAdaptor.Get", XmlTemplate, out sreturnValue, true, true);                   
                }
                else
                {
                    hdnIncAdjusters.Text = lstTabs.SelectedValue;                     
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                UpdateListItems();                
                bReturnStatus = CallCWS("AutoFroiAcordAdaptor.Save", null, out sreturnValue, true, false);
                if (bReturnStatus)
                {                    
                    bReturnStatus = CallCWSFunction("AutoFroiAcordAdaptor.Get");
                    ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>AutoFroiAcordAdaptor.Get</Function></Call>");
            sXml = sXml.Append("<Document><Adjuster><control name=\"ExcAdjusters\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"IncAdjusters\" >");
            sXml = sXml.Append(hdnIncAdjusters.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"EmailFrom\" >");            // hdnIncAdjusters
            sXml = sXml.Append("</control>");            
            sXml = sXml.Append("</Adjuster></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void UpdateListItems()
        {
            lstTabs.Items.Clear();
            lstTopDowns.Items.Clear();
            txtTopDown.Text = "";
            txtlstTabs.Text = "";
			//rsharma220 MITS 35401 Start
            int countm = -1;
            char[] splitter = { ';' };
            string svalues = string.Empty;
            string[] lsthndValues = hndlstTabs.Value.Split(splitter);
            if (lsthndValues.Length > 1)
            {
                char[] splitterEqual = { '=' };
                for (int Icount = 0; Icount < lsthndValues.Length - 1; Icount++)
                {
                    string[] svalue = lsthndValues[Icount].Split(splitterEqual);
                    if (svalue.Length > 1)
                    {
                        ListItem listitem = new ListItem(svalue[1], svalue[0]);
                        try
                        {
                            if (!lstTabs.Items.Contains(listitem))
                            {
                                lstTabs.Items.Add(listitem);
                                svalues += svalue[0] + " ";
                                countm++;
                            }

                            if (lstTopDowns.Items.Contains(listitem))
                            {
                                lstTopDowns.Items.Remove(listitem);

                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                        lstTabs.Items[countm].Selected = true;
                    }
                }
                if (svalues.Length > 0)
                {
                    txtlstTabs.Text = svalues.Substring(0, svalues.Length - 1);
                }
                svalues = "";
                hndlstTabs.Value = "";
            }

            countm = -1;
            lsthndValues = hndTopDown.Value.Split(splitter);
            if (lsthndValues.Length > 1)
            {
                char[] splitterEqual = { '=' };
                for (int Icount = 0; Icount < lsthndValues.Length - 1; Icount++)
                {
                    string[] svalue = lsthndValues[Icount].Split(splitterEqual);
                    if (svalue.Length > 1)
                    {
                        ListItem listitem = new ListItem(svalue[1], svalue[0]);
                        try
                        {
                            if (lstTabs.Items.Contains(listitem))
                            {
                                lstTabs.Items.Remove(listitem);

                            }
                            if (!lstTopDowns.Items.Contains(listitem))
                            {
                                lstTopDowns.Items.Add(listitem);
                                svalues += svalue[0] + " ";
                                countm++;
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                        lstTopDowns.Items[countm].Selected = true;
                    }
                }
				//rsharma220 MITS 35401 End
                if (svalues.Length > 0)
                {
                    txtTopDown.Text = svalues.Substring(0, svalues.Length - 1);
                }
                svalues = "";
                hndTopDown.Value = "";
            }
        }

    }
}
