﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectedDisc.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.SelectedDisc" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc3" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--VACo Changes STARTS--%>
    <title>Add/Modify Discount/Surcharge Selection</title>
    <%--VACo Changes Ends--%>
    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />
    <uc3:CommonTasks ID="CommonTasks1" runat="server" />
    <%--   <link rel="stylesheet" href="csc-Theme/riskmaster/common/style/rmnet.css" type="text/css">--%>
    <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
    </style>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/drift.js"></script>

    <%--<script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/dhtml-div.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/dhtml-help-setup.js"></script>

    <script src="csc-Theme/riskmaster/common/javascript/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/window.js"
        type="text/javascript"></script>

    <script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/dialog.js"
        type="text/javascript"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar-setup.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/calendar-alias.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-xml.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-editable.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-query.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/rmx-common-ref.js"></script>--%>
</head>
<body class="10pt" onload="CopyGridRowDataToPopup();EnableDisableUserEdit();">
    <form id="frmData" method="post" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
    <div class="toolbardrift" id="toolbardrift" name="toolbardrift">
        <table class="toolbar" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td align="center" valign="middle" height="32">
                        <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="ImageButton1" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                            onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" OnClientClick="return ValidateFieldsforSelectedDisc();"
                            OnClick="save_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br />
    <br />
    <div class="msgheader" id="formtitle">
    <%--VACo Changes STARTS--%>
        Add/Modify Discount/Surcharge Selection</div>
    <%--VACo Changes Ends--%>    
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                <%--VACo Changes STARTS--%>
                                    Selected Discount/Surcharge Parameters
                                <%--VACo Changes ENDS--%>    
                                </td>
                            </tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="RowId"></asp:TextBox></tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="AddedByUser"></asp:TextBox></tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="DttmRcdAdded"></asp:TextBox></tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="DttmRcdLastUpd"></asp:TextBox></tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="UpdatedByUser"></asp:TextBox></tr>
                            <tr>
                                <td>
                                    Use Discount:&nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" Checked="False" RMXref="/Instance/Document//control[@name='UseDiscTier']"
                                        ID="UseDiscTier" onclick="setDataChanged(true);" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Allow user edit:&nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" Checked="False" RMXref="/Instance/Document//control[@name='Override']"
                                        ID="Override" onclick="setDataChanged(true);" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Style="display: none" RMXref="/Instance/Document//control[@name='UseVolumeDiscount']"
                                        ID="UseVolumeDiscount"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_LOBcode" Text="Line of Business" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                <%--VACo Changes STARTS--%>
                                    <uc2:CodeLookUp runat="server" ID="LOBcode" CodeTable="POLICY_LOB" ControlName="LOBcode"
                                        RMXRef="/Instance/Document/Document/SelectedDisc/control[@name='LOBcode']" CodeFilter=" AND CODES.SHORT_CODE IN(\'WC\',\'GL\') " type="code"
                                        Required="true" />
                                <%--VACo Changes ENDS--%>        
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_Statecode" Text="State" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <uc2:CodeLookUp runat="server" ID="Statecode" CodeTable="states" ControlName="Statecode"
                                        
                                        RMXRef="/Instance/Document/Document/SelectedDisc/control[@name='Statecode']" 
                                        type="code" Required="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <%--VACo Changes STARTS--%>
                                    <asp:Label runat="server" ID="lbl_Discount_Name" Text="Discount/Surcharge Name" class="required" />&nbsp;&nbsp;
                                </td>
                                <%--VACo Changes ENDS--%>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                    <!--Start:Added by Nitin Goel: 05/21/2010,MITS#20744,added attribute readonly and back-color-->
                                        <asp:TextBox runat="server" size="30" ID="DiscountName" RMXref="/Instance/Document//control[@name='DiscountName']" ReadOnly="true" BackColor="#F2F2F2"  onchange="setDataChanged(true);"></asp:TextBox>
                                    <!--End:Nitin Goel,05/21/2010,MITS#20744, added attribute readonly and back-color-->
                                    <asp:Button runat="server" class="button" OnClientClick="return OpenSelectedDiscDiscountNameWindow();"
                                        Text="..." /></div>
                                </td>
                            </tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" RMXref="/Instance/Document//control[@name='DiscountName']/@codeid" ID="DiscountName_cid"></asp:TextBox></tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_Applicable_Level" Text="Applicable Level" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                    <!--Start:Added by Nitin Goel: 05/21/2010,MITS#20744,added attribute readonly and back-color-->
                                        <asp:TextBox runat="server" size="30" ID="ParentLevel" RMXref="/Instance/Document//control[@name='ParentLevel']" ReadOnly="true"  BackColor="#F2F2F2" onchange="setDataChanged(true);"></asp:TextBox>
                                    <!--End:Nitin Goel,05/21/2010,MITS#20744,added attribute readonly and back-color-->
                                    <asp:Button ID="Button1" runat="server" class="button" OnClientClick="return OpenSelectedDiscParentLevelWindow();"
                                        Text="..." /></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <asp:TextBox runat="server" Style="display: none" RMXref="/Instance/Document//control[@name='ParentLevel']/@codeid" ID="ParentLevel_cid"></asp:TextBox></tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox runat="server" Style="display: none" ID="FormMode"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox runat="server" Style="display: none" ID="FormTitle"></asp:TextBox>
                </td>
            </tr>
        </tbody>
    </table>
    <table>
        <tbody>
            <tr>
                <td>
                </td>
            </tr>
        </tbody>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="gridname"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="mode"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="txtvalidate"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="txtData"></asp:TextBox>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    <%--<input type="text" name="" value="" id="SysViewType" style="display: none"><input
        type="text" name="" value="" id="SysCmd" style="display: none"><input type="text"
            name="" value="" id="SysCmdConfirmSave" style="display: none"><input type="text"
                name="" value="" id="SysCmdQueue" style="display: none"><input type="text" name=""
                    value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate"><input type="text"
                        name="" value="" id="SysClassName" style="display: none" rmxforms:value=""><input
                            type="text" name="" value="" id="SysSerializationConfig" style="display: none"><input
                                type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId"><input
                                    type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId"><input
                                        type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="SelectedDisc"><input
                                            type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value=""><input
                                                type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="SelectedDisc"><input
                                                    type="text" name="" value="LOBcode_cid|Statecode_cid|DiscountName|ParentLevel|"
                                                    id="SysRequired" style="display: none">--%>
    <%-- </td>
    <td valign="top">
    </td>
    </tr> </tbody> </table>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-3" id="SysWindowId">--%></form>
</body>
</html>
