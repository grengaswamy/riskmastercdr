﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Xml;
using Riskmaster.UI.Shared.Controls;
using System.Drawing.Printing;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class PaymentParameterSetup : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sreturnValue = string.Empty;
        bool bStatus = false;
        XmlDocument XmlDoc=new XmlDocument();
        string sDeletedIds = string.Empty;
        int Counter = 0;
        string sNode = null;
        bool bReturnStatus = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                PrintDocument oPrintDocument = new PrintDocument();
                if (!IsPostBack)
                {

                    bReturnStatus = CallCWS("CheckOptionsAdaptor.Get", XmlTemplate, out sreturnValue, true, true);
                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);

                        // NonFDMCWSPageLoad("CheckOptionsAdaptor.Get");
                        hdnPayment.Text = hdnPaymentNotifyValue.Text;
						// npadhy JIRA 6418 Starts Commented the existing Print Option dropdown
                        //sNode = null;
                        //sNode = (string)XmlDoc.SelectSingleNode("//form/control[@name='PrntChecktoFile']/option[@selected='1']/@value").Value;
                        //PrntChecktoFile.SelectedValue = sNode;
						// npadhy JIRA 6418 Ends Commented the existing Print Option dropdown
                        //gagnihotri: Payment Approval Start
                        sNode = null;
                        sNode = XmlDoc.SelectSingleNode("//form/control[@name='TimeInterval']/option[@selected='1']/@value").Value;
                        TimeInterval.SelectedValue = sNode;
                        //gagnihotri: Payment Approval End

                        //Mgaba2:R6:Start
                        sNode = null;
                        sNode = (string)XmlDoc.SelectSingleNode("//form/control[@name='TimeIntervalAppRes']/option[@selected='1']/@value").Value;
                        DDLTimeFrame.SelectedValue = sNode;
                        //Mgaba2:R6:End
                        if (DupPayCheck.Checked == true)
                        {
                            sNode = null;
                            sNode = (string)XmlDoc.SelectSingleNode("//form/control[@name='DupCriteria']/option[@selected='1']/@value").Value;
                            DupCriteria.SelectedValue = sNode;
                        }
                        //For Printer DropDownSelection
                        //New Functionality Start
                        ddlPrinters.Items.Clear();
                        ddlPrinters.Items.Add("");
                        //tkatsarski: 02/17/15 - RMA-6802: When the server's printing service is stopped ("print spooler"), PrinterSettings.InstalledPrinters is throwing exception
                        try
                        {
                            foreach (string printer in PrinterSettings.InstalledPrinters)
                            {

                                ListItem item = new ListItem(printer);
                                ddlPrinters.Items.Add(printer);
                            }
                        }
                        catch (System.ComponentModel.Win32Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                        }
                        sNode = null;
                        sNode = (string)XmlDoc.SelectSingleNode("//form/PrinterName").InnerText;
                        ddlPrinters.SelectedValue = sNode;
                        oPrintDocument.PrinterSettings.PrinterName = ddlPrinters.SelectedValue;
                        ddlPbfc.Items.Add("");
                        foreach (PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
                        {
                            ListItem lstitem = new ListItem(oPaperSource.SourceName, oPaperSource.RawKind.ToString());
                            ddlPbfc.Items.Add(lstitem);

                        }
                        foreach (PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
                        {
                            ListItem lstitem = new ListItem(oPaperSource.SourceName, oPaperSource.RawKind.ToString());
                            ddlpbfeob.Items.Add(lstitem);
                        }
                        //New Functionality End

                        sNode = null;
                        sNode = (string)XmlDoc.SelectSingleNode("//form/PaperBinForChecks").InnerText;
                        ddlPbfc.SelectedValue = sNode;
                        sNode = null;
                        sNode = (string)XmlDoc.SelectSingleNode("//form/PaperBinForEobs").InnerText;
                        ddlpbfeob.SelectedValue = sNode;

                    }
                }
                if (hdnAction.Text != "Save")
                {
                    if (IsPostBack)
                    {
                        if (PaymentNotificationGrid_RowDeletedFlag.Text.ToLower() == "true")
                        {
                            hdnNotifyUsers.Text = "";
                            hdnNotifyPeriod.Text = "";
                            hdnLOB.Text = "";
                            hdnDateCriteria.Text = "";
                            UserNotify.Text = "";
                            string selectedRowId = PaymentNotificationSelectedId.Text;
                            hdnDeletedValues.Text = hdnDeletedValues.Text + "," + selectedRowId;

                            if (hdnDeletedValues.Text.Length > 0)
                            {
                                Counter = hdnDeletedValues.Text.IndexOf(",");
                                if (Counter == 0)
                                    hdnDeletedValues.Text = hdnDeletedValues.Text.Remove(0, 1);

                            }
                            //Changing values of PaymentnotifyValues
                            string[] strPaymentNotifyArr = hdnPayment.Text.Split("**".ToCharArray());
                            string strReplace = string.Empty;
                            for (int i = 0; i < strPaymentNotifyArr.Length; i++)
                            {
                                if (!strPaymentNotifyArr[i].Trim().Equals(""))
                                {
                                    string[] strTempArr = strPaymentNotifyArr[i].ToString().Split('|');

                                    for (int j = 0; j < strTempArr.Length; j++)
                                    {
                                        if (strTempArr[4] != selectedRowId)
                                        {
                                            strReplace = strReplace + strPaymentNotifyArr[i] + "**";
                                            break;
                                        }
                                    }

                                }

                            }
                            hdnPayment.Text = strReplace;
                            CallCWS("CheckOptionsAdaptor.GetDummyData", XmlTemplate, out sreturnValue, true, true);
                            PaymentNotificationGrid_RowDeletedFlag.Text = "false";
                            PaymentNotificationGrid_Action.Text = string.Empty;
                        }
                        else if (hdnAction.Text == "setCriteria")
                        {
                            bStatus = false;
                            sreturnValue = "";
                            XmlTemplate = null;
                            XmlTemplate = GetMessageTemplateForCriterea();
                            bStatus = CallCWS("CheckOptionsAdaptor.SetDuplicateCriteria", XmlTemplate, out sreturnValue, false, false);
                            if (bStatus)
                            {
                                ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                                XmlDoc.LoadXml(sreturnValue);
                                DupCriteria.Items.Clear();
                                XmlNodeList DupClaimCriteria = XmlDoc.SelectNodes("//DupCriteria/option");
                                foreach (XmlNode xNode in DupClaimCriteria)
                                {
                                    DupCriteria.Items.Add(new ListItem(xNode.InnerText, xNode.SelectSingleNode("@value").Value));
                                }
                                DupCriteria.SelectedValue = ViewState["DupCriteriaSelectedValue"].ToString();
                            }
                            hdnNotifyUsers.Text = "";
                            hdnNotifyPeriod.Text = "";
                            hdnLOB.Text = "";
                            hdnDateCriteria.Text = "";
                            UserNotify.Text = "";
                            CallCWS("CheckOptionsAdaptor.GetDummyData", XmlTemplate, out sreturnValue, true, true);
                        }
                        else if (PaymentNotificationGrid_RowDeletedFlag.Text == "false" && hdnAction.Text != "SelectedIndexChange")
                        {
                            hdnNotifyUsers.Text = "";
                            hdnNotifyPeriod.Text = "";
                            hdnLOB.Text = "";
                            hdnDateCriteria.Text = "";
                            UserNotify.Text = "";
                            bReturnStatus = false;
                            bReturnStatus = CallCWS("CheckOptionsAdaptor.Get", XmlTemplate, out sreturnValue, true, true);
                            if (bReturnStatus)
                            {
                                ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                                XmlDoc.LoadXml(sreturnValue);
                                sNode = null;
								// npadhy JIRA 6418 Starts Commented the existing Print Option dropdown
                                //sNode = (string)XmlDoc.SelectSingleNode("//form/control[@name='PrntChecktoFile']/option[@selected='1']/@value").Value;
                                //PrntChecktoFile.SelectedValue = sNode;
								// npadhy JIRA 6418 Ends Commented the existing Print Option dropdown
                                if (DupPayCheck.Checked == true)
                                {
                                    sNode = null;
                                    sNode = (string)XmlDoc.SelectSingleNode("//form/control[@name='DupCriteria']/option[@selected='1']/@value").Value;
                                    DupCriteria.SelectedValue = sNode;
                                }
                            }

                        }
                        if (hdnAction.Text == "SelectedIndexChange")
                        {
                            hdnNotifyUsers.Text = "";
                            hdnNotifyPeriod.Text = "";
                            hdnLOB.Text = "";
                            hdnDateCriteria.Text = "";
                            UserNotify.Text = "";
                            CallCWS("CheckOptionsAdaptor.GetDummyData", XmlTemplate, out sreturnValue, true, true);
                        }
                        ////Added By Ashutosh Scheduled Date deletion
                        if (ScheduleDateGrid_RowDeletedFlag.Text.ToLower() == "true")
                        {
                            hdnScheduleId.Text = ScheduleDateGridSelectedId.Text;
                            XmlTemplate = GetMessageTemplateForScheduleDelete();

                            bReturnStatus = CallCWS("CheckOptionsAdaptor.DeleteSchedule", XmlTemplate, out sreturnValue, false, false);
                            

                            ScheduleDateGrid_RowDeletedFlag.Text = "false";
                            ScheduleDateGrid_Action.Text = string.Empty;
                            XmlTemplate = null;

                            string script = "<script>window.location.href=window.location.href;</script>";
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "RefereshParentPage", script);

                          
                        }

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        public override void ModifyXml(ref XElement xelement)
        {
            XElement Xelement = null;
            XElement XNode=null;
            string sfunctionName = (string)xelement.XPathSelectElement("//Call/Function").Value;
            XElement Xoption = null; //MGaba2:R6
            if (sfunctionName == "CheckOptionsAdaptor.Get")
            {
				// npadhy JIRA 6418 Starts Commented the existing code for filling Print Option dropdown
//                Xelement = null;
//                Xelement = xelement.XPathSelectElement(".//Document/form/control[@name='PrntChecktoFile']");
//                Xelement.Value = "";
////                XElement Xoption = new XElement("option");
//                Xoption = new XElement("option");
//                Xoption.Add(new XAttribute("value", "0"));
//                Xoption.Value = "To Printer Only";
//                Xelement.Add(Xoption);
//                Xoption = null;
//                Xoption = new XElement("option");
//                Xoption.Add(new XAttribute("value", "1"));
//                Xoption.Value = "To File Only";
//                Xelement.Add(Xoption);
//                Xoption = null;
//                Xoption = new XElement("option");
//                Xoption.Add(new XAttribute("value", "2"));
//                Xoption.Value = "To Printer and File";
//                Xelement.Add(Xoption);
//                Xoption = null;
//                Xoption = new XElement("option"); //MITS 30623 mcapps2 12/07/2012 Start
//                Xoption.Add(new XAttribute("value", "3"));
//                Xoption.Value = "To Custom File Only";
//                Xelement.Add(Xoption);
//                Xoption = null;
//                Xoption = new XElement("option");
//                Xoption.Add(new XAttribute("value", "4"));
//                Xoption.Value = "To Printer and Custom File"; //MITS 30623 mcapps2 12/07/2012 end
//                Xelement.Add(Xoption);
				// npadhy JIRA 6418 Starts Commented the existing code for filling Print Option dropdown

                //Notify Period
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/form/NotifyPeriod");
                Xoption = null;
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "0"));
                Xoption.Value = "Every Log In";
                Xelement.Add(Xoption);
                Xoption = null;
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "1"));
                Xoption.Value = "Monday";
                Xelement.Add(Xoption);
                Xoption = null;
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "2"));
                Xoption.Value = "Tuesday";
                Xelement.Add(Xoption);
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "3"));
                Xoption.Value = "Wednesday";
                Xelement.Add(Xoption);
                Xoption = null;
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "4"));
                Xoption.Value = "Thursday";
                Xelement.Add(Xoption);
                Xoption = null;
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "5"));
                Xoption.Value = "Friday";
                Xelement.Add(Xoption);

                //Date Criterea

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/form/DateCriteria");
                Xoption = null;
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "10"));
                Xoption.Value = "less/equal current date";
                Xelement.Add(Xoption);
                Xoption = null;
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "20"));
                Xoption.Value = "by end of week";
                Xelement.Add(Xoption);
                Xoption = null;
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "30"));
                Xoption.Value = "by end of month";
                Xelement.Add(Xoption);
                if (PaymentNotificationGrid_RowDeletedFlag.Text =="true")
                {
                    Xelement = null;
                    Xelement = xelement.XPathSelectElement(".//Document/form/PaymentNotifyValues");
                    Xelement.Value = hdnPayment.Text;
                }

                //Time Interval
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/form/control[@name='TimeInterval']");
                Xelement.Value = "";
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "0"));
                Xoption.Value = "Days";
                Xelement.Add(Xoption);
                Xoption = null;
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "1"));
                Xoption.Value = "Hours";
                Xelement.Add(Xoption);

                //MGaba2:R6:Approval settings related to Reserve WorkSheet:Start
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/form/control[@name='TimeIntervalAppRes']");
                Xelement.Value = "";
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "0"));
                Xoption.Value = "Days";
                Xelement.Add(Xoption);
                Xoption = null;
                Xoption = new XElement("option");
                Xoption.Add(new XAttribute("value", "1"));
                Xoption.Value = "Hours";
                Xelement.Add(Xoption);
                //MGaba2:R6:End
            }
            if (sfunctionName == "CheckOptionsAdaptor.Save")

            {
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/form");
                XElement xDeletedIds = new XElement("DeletedIds");
                xDeletedIds.Value = hdnDeletedValues.Text;
                Xelement.Add(xDeletedIds);
                MultiCodeLookUp ctrlTransCodes = (MultiCodeLookUp)this.Form.FindControl("TransCodes");
                string strTemp = ctrlTransCodes.CodeId;
                string strTmp = string.Empty;
                string strTmpCodes = string.Empty;
                if (strTemp != string.Empty)
                {
                    strTmp = strTemp.Trim();
                    strTmpCodes = strTmp.Replace(" ", ",");
                    strTmpCodes = strTmpCodes + ",";

                }
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/form/TransValues");
                Xelement.Value = strTmpCodes;


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/form/control[@name='DupCriteria']");
                Xelement.Value =DupCriteria.SelectedIndex.ToString();
                if(hdnCriteria.Text=="")
                {
                     Xelement = null;
                     Xelement = xelement.XPathSelectElement(".//Document/form/SetCriteria");
                     Xelement.Value = "N";
                    
                }
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/form/PaymentNotifyValues");
                Xelement.Value = hdnPayment.Text;
               


            }

            if (sfunctionName == "CheckOptionsAdaptor.GetDummyData" && PaymentNotificationGrid_RowDeletedFlag.Text == "false")
            {
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/SetCriteria");
                if (Xelement != null)
                {
                    Xelement.Remove();
                }

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/form/NotifyUsers/PaymentNotification");
                XElement Xnode = new XElement("User");
                Xnode.Add(new XAttribute("type", "new"));
                Xelement.Add(Xnode);
                Xnode= new XElement("UNotify");
                Xelement.Add(XNode);
                Xnode = null;
                XNode = new XElement("LBusiness");
                Xelement.Add(XNode);
                Xnode = null;
                XNode = new XElement("NPeriod");
                Xelement.Add(XNode);
                Xnode = null;
                XNode = new XElement("DCriteria");
                Xelement.Add(XNode);
                Xnode = null;
                XNode = new XElement("RecordId");
                Xelement.Add(XNode);
         

            } 

        }
       
        //Criterea Template
        private XElement GetMessageTemplateForCriterea()
        {
            ViewState["DupCriteriaSelectedValue"] = DupCriteria.SelectedItem.Value;
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization></Authorization>");
            sXml.Append("<Call><Function></Function></Call><Document><SetCriteria><control name=\"DupCriteria\">");
            sXml.Append(DupCriteria.SelectedItem.Value);
            sXml.Append("</control>");
            sXml.Append("<DupNoOfDays>");
            sXml.Append(hdnValue.Text);
            sXml.Append("</DupNoOfDays>");
            sXml.Append("</SetCriteria>");
            sXml.Append("</Document>");
            sXml.Append("</Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        private XElement GetMessageTemplateForScheduleDelete()
        {
          
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization></Authorization>");
            sXml.Append("<Call><Function></Function></Call><Document><SetCriteria>");
            sXml.Append("<ScheduleId>");
            sXml.Append(hdnScheduleId.Text);
            sXml.Append("</ScheduleId>");
            sXml.Append("</SetCriteria></Document>");
            
            sXml.Append("</Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("CheckOptionsAdaptor.Save", XmlTemplate, out sreturnValue, true, false);
                hdnAction.Text = "";
                if (bReturnStatus)
                {
                    ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                    MultiCodeLookUp MuserControl = (MultiCodeLookUp)this.Form.FindControl("TransCodes");
                    ListBox lstUserControl = (ListBox)MuserControl.FindControl("multicode");
                    lstUserControl.Items.Clear();

                    bReturnStatus = false;
                    bReturnStatus = CallCWS("CheckOptionsAdaptor.Get", XmlTemplate, out sreturnValue, true, true);
                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);
						// npadhy JIRA 6418 Starts Commented the existing code for filling Print Option dropdown
                        //sNode = null;
                        //sNode = (string)XmlDoc.SelectSingleNode("//form/control[@name='PrntChecktoFile']/option[@selected='1']/@value").Value;
                        //PrntChecktoFile.SelectedValue = sNode;
						// npadhy JIRA 6418 Starts Commented the existing code for filling Print Option dropdown
                        sNode = null;
                        sNode = (string)XmlDoc.SelectSingleNode("//form/control[@name='TimeInterval']/option[@selected='1']/@value").Value;
                        TimeInterval.SelectedValue = sNode;
                        //MGaba2:R6 Adding settings related to approval of Reserve WorkSheet:Start
                        sNode = null;
                        sNode = (string)XmlDoc.SelectSingleNode("//form/control[@name='TimeIntervalAppRes']/option[@selected='1']/@value").Value;
                        DDLTimeFrame.SelectedValue = sNode;
                        //MGaba2:R6:End
                    }
                }
                hdnPayment.Text = hdnPaymentNotifyValue.Text;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void ddlPrinters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PrintDocument oPrintDocument = new PrintDocument();

                oPrintDocument.PrinterSettings.PrinterName = ddlPrinters.SelectedValue;
                ddlPbfc.Items.Clear();
                ddlpbfeob.Items.Clear();
                foreach (PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
                {
                    ListItem item = new ListItem(oPaperSource.SourceName, oPaperSource.RawKind.ToString());
                    ddlPbfc.Items.Add(item);
                    ddlpbfeob.Items.Add(item);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
