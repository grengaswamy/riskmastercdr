﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EarlyPayDiscounts.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.Manager.EarlyPayDiscounts" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="CodeLookUp" Src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>"Early Discount Parameters</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">        { var i; }</script>
    <%--vkumar258 - RMA-6037 - Starts --%>
   <%-- <script type="text/javascript" src="../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>--%>


    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>

</head>
<body onload="CopyGridRowDataToPopup();">
    <form id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Early Discount Parameters" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/EarlyPayDiscountData/control[@name='RowId']"
        rmxignoreset='true' />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="AddedByUser" RMXRef="/Instance/Document//EarlyPayDiscountData/control[@name='AddedByUser']"
        type="id" />
    <asp:TextBox Style="display: none" runat="server" ID="DttmRcdAdded" RMXRef="/Instance/Document//EarlyPayDiscountData/control[@name='DttmRcdAdded']"
        type="id" />
    <asp:TextBox Style="display: none" runat="server" ID="DttmRcdLastUpd" RMXRef="/Instance/Document//EarlyPayDiscountData/control[@name='DttmRcdLastUpd']"
        type="id" />
    <asp:TextBox Style="display: none" runat="server" ID="UpdatedByUser" RMXRef="/Instance/Document//EarlyPayDiscountData/control[@name='UpdatedByUser']"
        type="id" />
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <%--<tr class="ctrlgroup2">
                <td colspan="2">
                    <asp:Label runat="server" name="EarlyDiscountParams" ID="EarlyDiscountParams" Text="Early Discount Parameters" />
                </td>
            </tr>--%>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <asp:Label runat="server" Text="Use Early Pay Discount:" ID="lblUseEarlyPayDiscount" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="UseEarlyPayDiscount" RMXRef="/Instance/Document/EarlyPayDiscountData/control[@name='UseEarlyPayDiscount']"
                        type="checkbox" onclick="return setDataChanged(true);" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblLOB" Text="Line of Business:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="LOB" CodeTable="POLICY_LOB" ControlName="LOB" RMXRef="/Instance/Document/EarlyPayDiscountData/control[@name='LOB']"
                        type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblAmount" Text="Amount:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Amount" RMXRef="/Instance/Document/EarlyPayDiscountData/control[@name='Amount']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblDollarOrPercent" Text="Dollar Or Percent:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="DollarOrPercent" CodeTable="INSTALL_AMT_TYPE" ControlName="DollarOrPercent"
                        RMXRef="/Instance/Document/EarlyPayDiscountData/control[@name='DollarOrPercent']"
                        type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblDeadlineEligibilDate" Text="Deadline Eligibility Date:" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DeadlineEligibilDate" RMXRef="/Instance/Document/EarlyPayDiscountData/control[@name ='DeadlineEligibilDate']"
                        TabIndex="1" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);"
                        type='date' />
                    <%--vkumar258 - RMA-6037 - Starts --%>
                       <%-- <asp:Button class="DateLookupControl" runat="server" ID="btnDeadlineEligibilDate" />

                    <script type="text/javascript">
                        Zapatec.Calendar.setup(
					    {
					        inputField: "DeadlineEligibilDate",
					        ifFormat: "%m/%d/%Y",
					        button: "btnDeadlineEligibilDate"
					    }
					    );
                    </script>--%>
                        
                        <script type="text/javascript">
                            $(function () {
                                $("#DeadlineEligibilDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");
                            });
                        </script>
                        <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
        </tbody>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return EarlyPayDiscounts_onOk();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return EarlyPayDiscounts_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
