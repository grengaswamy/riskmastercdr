﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;
using System.Text;


namespace Riskmaster.UI.Utilities
{
    public partial class FiscalCopy : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            XmlDocument XmlDoc = new XmlDocument();
            
            //bijender MIts 14547
            if (!IsPostBack)
            {
                try
                {

                    hdnFromLineOfBusiness.Text = AppHelper.GetQueryStringValue("frmLob");
                    hdnFromOrganization.Text = AppHelper.GetQueryStringValue("frmOrg");
                    lobcode_cid.Text = AppHelper.GetQueryStringValue("toLob");
                    orgcode_cid.Text = AppHelper.GetQueryStringValue("toOrg");
                    lblFsYear.Text = AppHelper.GetQueryStringValue("fYear");
                    hdnFiscalYear.Text = AppHelper.GetQueryStringValue("fYear");


                    bReturnStatus = CallCWSFunctionBind("FiscalCopyAdaptor.Get", out sreturnValue);
                    ErrorControl1.errorDom = sreturnValue;

                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);
                        BindControl(XmlDoc);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
           
           
        }


        private void BindControl(XmlDocument XmlDoc)
        {
            XmlNode xmlNode = XmlDoc.SelectSingleNode("ResultMessage/Document/FiscalCopy/FromLineOfBusiness");

            if (xmlNode != null && xmlNode.InnerXml != "")
            {
               txtFromLineOfBusiness.Text = xmlNode.InnerXml;
            }
            xmlNode = XmlDoc.SelectSingleNode("ResultMessage/Document/FiscalCopy/FromOrganization");
            if (xmlNode != null && xmlNode.InnerXml != "")
            {
                txtFromOrganization.Text = xmlNode.InnerXml;
            }

            xmlNode = XmlDoc.SelectSingleNode("ResultMessage/Document/FiscalCopy/ToLineOfBusiness");
            if (xmlNode != null && xmlNode.InnerXml != "")
            {
                lobcode.Text = xmlNode.InnerXml;
            }

            xmlNode = XmlDoc.SelectSingleNode("ResultMessage/Document/FiscalCopy/ToOrganization");
            if (xmlNode != null && xmlNode.InnerXml != "")
            {
                orgcode.Text = xmlNode.InnerXml;
            }

              
        }

        protected void btCopy_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            XmlDocument XmlDoc = new XmlDocument();
            try
            {

                bReturnStatus = CallCWSFunctionBind("FiscalCopyAdaptor.Save", out sreturnValue);
                ErrorControl1.errorDom = sreturnValue;
                if (bReturnStatus)
                {
                  ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
