﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyCodeMapping.aspx.cs"
    ValidateRequest="false" Inherits="Riskmaster.UI.Utilities.Manager.PolicyCodeMapping" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Policy Code Mapping</title>
    <script type="text/JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/JavaScript" src="../../../Scripts/Utilities.js"></script>
    <script type="text/javascript">

        function ValidateAdd() {
            var objRow, checkText;
            var objGrid, objIsClaimType;
            var iClaimType, iReserveType;
            var obj = document.getElementById("lstPolicySystem");
            if (obj.options.length == 0) {
                alert("Policy System can not be empty.");
                return false;
            }
            obj = document.getElementById("lstCodeType");
            if (obj.options.length == 0) {
                alert("Code Type can not be empty.");
                return false;
            }
            objIsClaimType = obj.options[obj.selectedIndex].value;
            
            obj = document.getElementById("lstRMCodes");
            if (obj.options.length == 0) {
                alert("Riskmaster Codes are not defined for selected code type.");
                return false;
            }

            iReserveType = obj.options[obj.selectedIndex].value;

            obj = document.getElementById("lstPolSysCodes");
            if (obj.options.length == 0) {
                alert("Policy System Interface Codes are not defined for selected code type.");
                return false;
            }
            if (objIsClaimType.split('|')[1] == "1") {
                obj = document.getElementById("lstClaimTypes");
                if (obj.options.length == 0) {
                    alert("Claim types are not defined for the selected code type.");
                    return false;
                }
               
                iClaimType = obj.options[obj.selectedIndex].value;
                checkText = iReserveType + iClaimType;
                if (document.getElementById('hdValidateKey').value.indexOf(checkText) > 0) {
                    alert("Can not Insert Duplicate Mapping.");
                    return false;
                }
            }
        }

        function DeleteCode() {

            var selectedVal = "";
            if (SelectionValid()) {
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        if (document.forms[0].elements[i].checked == true) {
                            if (selectedVal == "") {
                                selectedVal = document.forms[0].elements[i].value;
                            }
                            else {
                                selectedVal = selectedVal + "," + document.forms[0].elements[i].value;
                            }
                        }
                    }
                }
                document.forms[0].hdnSelected.value = selectedVal;
                return true;
            }
            else {

                return false;
            }
        }
        function SelectionValid() {
            for (var i = 0; i < document.forms[0].elements.length; i++)
                if (document.forms[0].elements[i].type == 'checkbox')
                    if (document.forms[0].elements[i].checked == true)
                        return true;

            return false;
        }

        function onPageLoaded() {
            if (ie) {
                if ((eval("document.all.divForms") != null) && (ieversion >= 6)) {
                    eval("document.all.divForms").style.height = 300;
                }
            }
            else {
                var o_divforms;
                o_divforms = document.getElementById("divForms");
                if (o_divforms != null) {
                    o_divforms.style.height = window.frames.innerHeight * 0.70;
                    o_divforms.style.width = window.frames.innerWidth * 0.995;
                }
            }
            loadTabList();
        }
    </script>
    <style type="text/css">
        .style1
        {
            width: 13%;
        }
    </style>
</head>
<body onload="javascript:onPageLoaded();parent.MDIScreenLoaded()">
    <form id="frmData" name="frmData" method="post" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <table width="100%" border="0" cellspacing="3" cellpadding="3">
        <tr>
            <td class="ctrlgroup" colspan="4" width="100%">
                Policy Codes
            </td>
        </tr>
        <tr>
            <td class="style1">
                <asp:TextBox runat="server" value="1" Style="display: none" ID="hdnAlreadyExists"></asp:TextBox>
                <asp:TextBox runat="server" Style="display: none" rmxignorevalue="true" rmxref="/Instance/Document/PolicyCodeMapping/DeleteMapping"
                    ID="hdnSelected"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td>
                Select Policy System
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstPolicySystem" type="combobox"  runat="server" AutoPostBack="true"
                    OnSelectedIndexChanged="lstPolicySystem_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:TextBox runat="server" ID="txtPolicySystemId" Style="display: none" rmxref="/Instance/Document/PolicyCodeMapping/SelectedPolicySystem"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Select Code Type
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstCodeType" type="combobox" rmxref="/Instance/Document/PolicyCodeMapping/CodeTypes"
                    ItemSetRef="/Instance/Document/PolicyCodeMapping/CodeTypes" runat="server" AutoPostBack="true"
                    OnSelectedIndexChanged="lstCodeType_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:TextBox runat="server" ID="txtCodeTypeId" Style="display: none" rmxref="/Instance/Document/PolicyCodeMapping/SelectedCodeType"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="trClaimType" >
        <td>
        <asp:Label Text="Riskmaster Claim Type" runat="server" ID="lblClaimtype" ></asp:Label>
        </td>
        <td>
            <asp:DropDownList runat="server" ID="lstClaimTypes" type="combobox" rmxref="/Instance/Document/PolicyCodeMapping/RMClaimTypes" 
             AutoPostBack="false" ItemSetRef="/Instance/Document/PolicyCodeMapping/RMClaimTypes"></asp:DropDownList>
        </td>
        <td>
        </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="txtRMCode" Text="Riskmaster Codes" runat="server" Visible="false"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="lstRMCodes" type="combobox" rmxref="/Instance/Document/PolicyCodeMapping/RMCodes"
                    ItemSetRef="/Instance/Document/PolicyCodeMapping/RMCodes" runat="server" AutoPostBack="false"
                    Visible="false">
                </asp:DropDownList>
                <asp:TextBox runat="server" ID="txtRMCodeId" Style="display: none" rmxref="/Instance/Document/PolicyCodeMapping/PolMapValues/PolicyCode[@value='PolicyCode']/@RMSelectedCodeId"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="txtPolSysCode" Text="Policy System Interface Codes"
                    Visible="false"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="lstPolSysCodes" type="combobox" rmxref="/Instance/Document/PolicyCodeMapping/PolSysCodes"
                    ItemSetRef="/Instance/Document/PolicyCodeMapping/PolSysCodes" runat="server"
                    AutoPostBack="false" Visible="false">
                </asp:DropDownList>
                <asp:TextBox runat="server" ID="txtPolSysCodeId" Style="display: none" rmxref="/Instance/Document/PolicyCodeMapping/PolMapValues/PolicyCode[@value='PolicyCode']/@PolSysSelectedCodeId"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button runat="server" Text="Add Policy Code" class="button" Visible="false"
                    ID="addSPolicybtn" OnClick="addPolicy_btn_Click" OnClientClick="return ValidateAdd();" />
            </td>
            <td colspan="2" align="left">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button runat="server" Text="Remove Policy Code" class="button" OnClientClick="return DeleteCode();"
                    Visible="false" OnClick="removePolicybtn_Click" ID="removePolicybtn" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table width="100%" style="margin-right: 0px; height: 191px;">
                    <tr>
                        <td width="80%">
                            <div style="width: 98%; height: 300px; overflow: auto">
                                <asp:GridView ID="gvPolicyCodeMappingGrid" runat="server" AutoGenerateColumns="False"
                                    Font-Names="Tahoma" Font-Size="Smaller" CssClass="singleborder" Width="98%" Height=""
                                    CellPadding="4" ForeColor="#333333" GridLines="Both" HorizontalAlign="Left">
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <input id="cbPolicyCodeMappingGrid" name="PolicyCodeMappingGrid" type="checkbox"
                                                    value='<%# Eval("CBId")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CBId" HeaderText="RowId" Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ClaimCd" HeaderText="Claim Type Code" HeaderStyle-CssClass="colheader5" Visible="false" />
                                        <asp:BoundField DataField="rmcode" HeaderText="RM Code" HeaderStyle-CssClass="colheader5" />
                                        <asp:BoundField DataField="polsyscode" HeaderText="Policy System Code" HeaderStyle-CssClass="colheader5" />
                                    </Columns>
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle CssClass="colheader3" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="hdnCbId" runat="server" Style="display: none" RMXType="id"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="SysWindowId"></asp:TextBox>
    <input id="Hidden1" type="hidden" runat="server" value="" />
 <input id="hdValidateKey" type="hidden" runat="server" value="" />
    </form>
</body>
</html>
