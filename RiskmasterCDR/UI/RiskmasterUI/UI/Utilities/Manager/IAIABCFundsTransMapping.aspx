<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IAIABCFundsTransMapping.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.IAIABCFundsTransMapping" ValidateRequest = "false"%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>IAIABC Funds Transaction Mapping</title>
    <script type="text/javascript">
        var ns, ie, ieversion;
        var browserName = navigator.appName;                   // detect browser 
        var browserVersion = navigator.appVersion;
        if (browserName == "Netscape") {
            ie = 0;
            ns = 1;
        }
        else		//Assume IE
        {
            ieversion = browserVersion.substr(browserVersion.search("MSIE ") + 5, 1);
            ie = 1;
            ns = 0;
        }
        function DeleteMapping() {
            var selected = "";
            if (SelectionValid()) {
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox')
                        if (document.forms[0].elements[i].checked == true) {
                        if (selected == "") {
                            selected = document.forms[0].elements[i].value;
                        }
                        else {
                            selected = selected + " " + document.forms[0].elements[i].value;
                        }
                    }
                }
                document.forms[0].hdnSelected.value = selected;
                return true;
            }
            else {
                return false;
            }
        }
        function trim(value) {
            value = value.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
            return value;
        }
        function Selected() {
            var acordForm = document.forms[0].lstACORDForm.value;
            var ClaimType = document.forms[0].lstClaimType.value;
            if (trim(acordForm) == "" || trim(ClaimType) == "") {
                alert("Acord Form and Claim Type must be selected before adding to mappings.");
                return false;
            }
            return true;
        }
        function SaveMapping() {
            var selected;
            var acordForm = document.forms[0].lstACORDForm.value;
            var ClaimType = document.forms[0].lstClaimType.value;
            if (Selected()) {
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        selected = document.forms[0].elements[i].value.split('_');
                        if (selected[0] == acordForm && selected[1] == ClaimType) {
                            alert("The combination of ACORD Form and Claim Type are currently part of the mapping and will not be added.");
                            return false;
                        }
                    }
                }
                return true;
            }
            else {
                return false;
            }
        }
        function SelectionValid() {
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    if (document.forms[0].elements[i].checked == true) {
                        return true;
                    }
                }
            }
            return false;
        }
        function onPageLoaded() {
            if (ie) {
                if ((eval("document.all.divForms") != null) && (ieversion >= 6)) {
                    eval("document.all.divForms").style.height = '30%';
                    //alert(window.frames.frameElement);
                    //eval("document.all.divForms").style.height=window.frames.Height*0.70;
                }
                if ((eval("document.all.divForms1") != null) && (ieversion >= 6)) {
                    eval("document.all.divForms1").style.height = '30%';
                    //alert(window.frames.frameElement);
                    //eval("document.all.divForms1").style.height=window.frames.Height*0.70;
                }
            }
            else {
                var o_divforms;
                o_divforms = document.getElementById("divForms");
                if (o_divforms != null) {
                    o_divforms.style.height = window.frames.innerHeight * 0.70;
                    o_divforms.style.width = window.frames.innerWidth * 0.995;
                }
                var o_divforms1;
                o_divforms1 = document.getElementById("divForms1");
                if (o_divforms != null) {
                    o_divforms.style.height = window.frames.innerHeight * 0.70;
                    o_divforms.style.width = window.frames.innerWidth * 0.995;
                }
            }
        }

        function CheckBenefitsExists() {
            if ((document.forms[0].lstBenefits.value == "") || (document.forms[0].lstTransactionTypeBenefits.value == "")) {
                return false;
            }
            arr = document.forms[0].hdnBenefits.value.split("**");
            str = "|" + document.forms[0].lstBenefits.value + "|" + document.forms[0].lstTransactionTypeBenefits.value + "|";
            for (i = 0; i < arr.length; i++) {
                if (arr[i] == str) {
                    alert("The combination of Benefit and Transaction Type are currently \n part of the mapping and will not be added.");
                    return false;
                }
            }
            return true;
        }
        function CheckPaidToDateExists() {
            if (document.forms[0].lstPaidToDate.value == "" || document.forms[0].lstTransactionTypePaidToDate.value == "") {
                return false;
            }
            arr = document.forms[0].hdnPaidToDate.value.split("**");
            str = "|" + document.forms[0].lstPaidToDate.value + "|" + document.forms[0].lstTransactionTypePaidToDate.value + "|";
            for (i = 0; i < arr.length; i++) {
                if (arr[i] == str) {
                    alert("The combination of Paid To Date and Transaction Type are currently \n part of the mapping and will not be added.");
                    return false;
                }
            }
            return true;
        }

        function DeletePaidToDateMapping() {
            var selected = "";
            var controlname = "";
            var flag = "0";
            if (SelectionValid()) {
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        controlname = document.forms[0].elements[i].name;
                        if (controlname.indexOf('selPaidToDateMapping') > -1) {
                            if (document.forms[0].elements[i].checked == true) {
                                flag = "1";
                                if (selected == "") {
                                    selected = document.forms[0].elements[i].value;
                                }
                                else {
                                    selected = selected + " " + document.forms[0].elements[i].value;
                                }
                            }
                        }
                    }
                }
                document.forms[0].hdnSelectedPaidToDate.value = selected;
                if (flag == "1")
                    return true;
                else
                    return false;
            }
            else {
                return false;
            }
        }
        function DeleteBenefitsMapping() {
            var selected = "";
            var controlname = "";
            var flag = "0";
            if (SelectionValid()) {
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        controlname = document.forms[0].elements[i].name;
                        if (controlname.indexOf("selBenefitsMapping") > -1 ) {
                            if (document.forms[0].elements[i].checked == true) {
                                flag = "1";
                                if (selected == "") {
                                    selected = document.forms[0].elements[i].value;
                                }
                                else {
                                    selected = selected + " " + document.forms[0].elements[i].value;
                                }
                            }
                        }
                    }
                }
                document.forms[0].hdnSelectedBenefits.value = selected;
                if (flag == "1")
                    return true;
                else
                    return false;    
            }
            else {
                return false;
            }
        }
	</script>
	<script language="javascript" src="../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
	<script language="javascript" src="../../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
	<script language="javascript" src="../../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/calendar-alias.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js"></script>
	<script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js"></script>
</head>
<body onload="javascript:onPageLoaded();parent.MDIScreenLoaded();" style="height:93%;width:99.5%">
  <form id="frmData" runat="server" method="post">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  <table width="99%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td class="msgheader" colspan="4">IAIABC Funds Transaction Mapping</td>
    </tr>
    <tr>
     <td class="ctrlgroup" colspan="4">A benefit can have an unlimited number of Transaction Types.<br />A Transaction Type should be used only one time.
     </td>
    </tr>
   </table>
   <table width="99%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
     <td class="ctrlgroup" colspan="4">IAIABC Funds Mapping</td>
    </tr>
   </table>
   <table cellspacing="0" cellpadding="0" border="0" width="99%">
    <tr>
     <td width="50%" align="left">IAIABC Benefits</td>
     <td width="50%" align="left">Transaction Type</td>
     <td>
     <asp:TextBox style="display:none" runat="server" type="hidden" id="hdnBenefits" />
     <asp:TextBox style="display:none" runat="server" type="hidden" id="hdnSelectedBenefits" />
     <asp:TextBox style="display:none" runat="server" type="hidden" id="hdnSelectedPaidToDate" />
     <asp:TextBox style="display:none" runat="server" type="hidden" id="hdnPaidToDate" />
     </td>
    </tr>
    <tr>
     <td width="50%" align="left">
       <asp:DropDownList type="combobox" runat="server" ID="lstBenefits" rmxref="Instance/Document/form/group/displaycolumn/control[@name='IAIABCBenefits']" ItemSetRef="Instance/Document/form/group/displaycolumn/control[@name='IAIABCBenefits']">
       </asp:DropDownList></td>
     <td width="50%" align="left">
       <asp:DropDownList type="combobox" runat="server" ID="lstTransactionTypeBenefits" rmxref="Instance/Document/form/group/displaycolumn/control[@name='TransactionTypeBenefits']" ItemSetRef="Instance/Document/form/group/displaycolumn/control[@name='TransactionTypeBenefits']">
       </asp:DropDownList></td>
     <td></td>
    </tr>
   </table>
   <div id="divForms" class="divScroll">
    <table width="98%" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <asp:GridView Width="98%" runat="server" ID="GdIAIABCBenefitMapping" AutoGenerateColumns="false" AlternatingRowStyle-CssClass="rowdark">
       <Columns>
        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="colheader5" ItemStyle-Width="5%">
            <ItemTemplate>
                <input type="checkbox" name="selBenefitsMapping" id="selBenefitsMapping" value='<%#Eval("CheckValue")%>' runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="IAIABC Benefits" ItemStyle-Width="50%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:Label runat="server" ID="benefitName" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Transaction Type" ItemStyle-Width="50%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:Label runat="server" ID="transactionName" />
            </ItemTemplate>
        </asp:TemplateField>
       </Columns>
       <EmptyDataTemplate>
            <asp:Label runat="server" Text="" Width="5%" ID="headerCheck" CssClass="colheader5" />
            <asp:Label runat="server" Text="IAIABC Benefits" Width="46%" ID="headerBenefits" CssClass="colheader5" />
            <asp:Label runat="server" Text="Transaction Type" Width="48%" ID="headerTransaction" CssClass="colheader5" />
       </EmptyDataTemplate> 
      </asp:GridView>  
     </tr>
    </table>
   </div>
   <asp:TextBox type="radiolist" runat="server" ID="IAIABCBenefitsList" style="display:none" rmxref="Instance/Document/form/group/displaycolumn/control[@name='IAIABCBenefitsList']" />
      
   <table width="99%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td width="99%" align="right">
      <asp:Button runat="server" ID="btnSaveBenefit" type="submit" Text="Save Benefits Mapping" class="button" OnClientClick="return CheckBenefitsExists();" OnClick="SaveBenefitMapping" />
      <asp:Button runat="server" ID="btnDeleteBenefit" type="submit" Text="Delete Benefits Mapping" class="button" OnClientClick="return DeleteBenefitsMapping();" OnClick="DeleteBenefitMapping" />
     </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
   </table>
   <table width="99%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td class="ctrlgroup" colspan="4">Paid to Date Items</td>
    </tr>
   </table>
   <table cellspacing="0" cellpadding="0" border="0" width="99%">
    <tr>
     <td width="50%" align="left">IAIABC Paid To Date</td>
     <td width="50%" align="left">Transaction Type</td>
    </tr>
    <tr>
     <td width="50%" align="left">
      <asp:DropDownList type="combobox" runat="server" ID="lstPaidToDate" ItemSetRef="Instance/Document/form/group/displaycolumn/control[@name='IAIABCPaidToDate']" rmxref="Instance/Document/form/group/displaycolumn/control[@name='IAIABCPaidToDate']">
       </asp:DropDownList></td>
     <td width="50%" align="left">
       <asp:DropDownList type="combobox" runat="server" id="lstTransactionTypePaidToDate" ItemSetRef="Instance/Document/form/group/displaycolumn/control[@name='TransactionTypePaidToDate']" rmxref="Instance/Document/form/group/displaycolumn/control[@name='TransactionTypePaidToDate']">
       </asp:DropDownList></td>
    </tr>
   </table>
   <div id="divForms1" class="divScroll">
    <table width="99%" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <asp:GridView Width="99%" runat="server" ID="GdIAIABCPaidMapping" AutoGenerateColumns="false" AlternatingRowStyle-CssClass="rowdark">
       <Columns>
        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="colheader5" ItemStyle-Width="5%">
            <ItemTemplate>
                <input type="checkbox" name="selPaidToDateMapping" id="selPaidToDateMapping" value='<%#Eval("CheckValue")%>' runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="IAIABC PTD Type" ItemStyle-Width="50%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:Label runat="server" ID="ptdName" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Transaction Type" ItemStyle-Width="50%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:Label runat="server" ID="transactionType" />
            </ItemTemplate>
        </asp:TemplateField>
       </Columns> 
      </asp:GridView>  
     </tr>
    </table>
   </div>
   <asp:TextBox type="radiolist" style="display:none" runat="server" ID="IAIABCPaidToDateList" rmxref="Instance/Document/form/group/displaycolumn/control[@name='IAIABCPaidToDateList']" />
   <table width="99%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td width="99%" align="right">
      <asp:Button runat="server" type="submit" ID="btnSavePaid" Text="Save Paid To Date Mapping" class="button" OnClientClick="return CheckPaidToDateExists();" OnClick="SavePaidToDateMapping" />
      <asp:Button runat="server" type="submit" ID="btnDeletePaid" Text="Delete Paid To Date Mapping" class="button" OnClientClick="return DeletePaidToDateMapping();" OnClick="DeletePaidToDateMapping" />
     </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
   </table>
  </form>
 </body>
</html>
