﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddScheduleDate.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.AddScheduleDate" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Schedule Dates</title>
	  <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"/>
	  <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
	  <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
	  <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
	  <script language="javascript" type="text/javascript" src="../../../Scripts/checkoptions.js"></script>
	  <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>
	  <script language="javascript" type="text/javascript" src="../../../Scripts/grid.js"></script>
        <script language="javascript" type="text/javascript" src="../../../Scripts/AddScheduleDate.js"></script>
	 
	 <%-- <script src="../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
	  <script type="text/javascript" language="JavaScript" src="../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
	  <script type="text/javascript" language="JavaScript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
	  <script type="text/javascript" language="JavaScript" src="../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>   --%>  

    <%--vkumar258 - RMA-6037 - Starts --%>
<link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>
</head>
<body class="10pt" onload="CopyGridRowDataToPopup();">
	<form id="frmData" runat="server" style="width:99.8%">
      <uc1:ErrorControl ID="ErrorControl1" runat="server" />
	<div>
	<table>
	
	<tr>
	<td> 
	
	   <asp:TextBox Style="display: none" runat="server" ID="ScheduleId"  RMXRef="/Instance/Document/SearchParam/control[@name ='RowID']"  />
		  <asp:TextBox Style="display: none" runat="server" ID="txtData" />
		  <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
		  <asp:TextBox Style="display: none" runat="server" ID="mode" />
		  <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
		  <asp:TextBox Style="display: none" runat="server" ID="gridname" />
		  <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
		  <asp:TextBox Style="display: none" runat="server" ID="FormMode" />
		  <asp:TextBox Style="display: none" runat="server" ID="txtComboType" Text="combotext" />
	
	</td>
	<td colspan="2">
		 <asp:ImageButton id="btnOk"  OnClientClick="javascript:return AddSchedule()"
			   onmouseover="javascript:document.all.btnOk.src='/RiskmasterUI/Images/save2.gif'" 
			   onmouseout="javascript:document.all.btnOk.src='/RiskmasterUI/Images/save.gif'" 
			   ImageUrl="~/Images/save.gif" class="bold" ToolTip="Save" runat="server"   onclick="btnOk_Click" 
			   />
			   </td>
	</tr>
		 <tr>
					 <td align="left" width="25%">Date:</td>
					 <td width="40%">        
						 <asp:TextBox runat="server" type="text" FormatAs="date"  TabIndex ="1" rmxref="/Instance/Document/SearchParam/control[@name='ScheduleDate']" value="" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" id="Date" size="12" Width="70%"></asp:TextBox>
						 <%--<input type="button" class="DateLookupControl" id="btndate1" tabindex="2"/>
						 <script type="text/javascript">
							 Zapatec.Calendar.setup(
																{
																	inputField: "Date",
																	ifFormat: "%m/%d/%Y",
																	button: "btndate1"
																}
																);
						</script>--%>

                         <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#Date").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
					</td>
				</tr>
				 <tr>
					 <td align="left" width="25%">Description:</td>
						<td width="40%">                                         
							<asp:TextBox runat="server" type="text"     TextMode="MultiLine"   TabIndex ="3" rmxref="/Instance/Document/SearchParam/control[@name='Description']"  size="30" id="Description" Width="70%" onkeyUp="MaxLength();" onkeyPress="MaxLength();" onblur="MaxLength()"   ></asp:TextBox>
						</td>
					 
				 </tr>
				 <tr>
		<input type="text" name="" value="" id="SysViewType" style="display:none"/>
	   <input type="text" name="" value="" id="SysCmd" style="display:none"/>
	   <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none"/>
	   <input type="text" name="" value="" id="SysCmdQueue" style="display:none"/>
	   <input type="text" name="" value="" id="SysCmdText" style="display:none" rmxforms:value="Navigate"/>
	   <input type="text" name="" value="" id="SysClassName" style="display:none" rmxforms:value=""/>
	   <input type="text" name="" value="" id="SysSerializationConfig" style="display:none"/>
	   <input type="text" name="" value="" id="SysFormIdName" style="display:none" rmxforms:value="RowId"/>
	   <input type="text" name="" value="" id="SysFormPIdName" style="display:none" rmxforms:value="RowId"/>
	   <input type="text" name="" value="" id="SysInvisible" style="display:none" rmxforms:value=""/>
	   <input type="text" name="" value="" id="SysFormName" style="display:none" rmxforms:value="ScheduleNotify"/>
	   <input type="text" name="" value="" id="SysRequired" style="display:none"/></tr>

	</table>
	
	</div>
	  <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
	</form>
</body>
</html>
