﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PremiumRangeCriteria.aspx.cs" ValidateRequest="false"
    Inherits="Riskmaster.UI.UI.Utilities.Manager.PremiumRangeCriteria" %>

<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
    <%@ Register TagPrefix="uc4" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
    <%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>
    <%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
    </style>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>
 
 <uc1:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<%-- Start: Neha Suresh Jain, 05/05/2010, MITS:20527, change width for correct display--%>

<body class="10pt" onload="pageLoaded();" style="width: 470px;">
    <form id="frmData" runat="server" method="post">
     <table>
        <tr>
            <td colspan="2">
                <uc4:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" /><div class="msgheader" style="width: 470px;"
        id="formtitle">
    </div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Premium Range Criteria
                                </td>
                            </tr>
                            <tr>
                                <td width="450px">
                                    <b></b>
                                    <table width="100%">
                                        <tr>
                                            <td width="100%">
                                                <div style="width: 100%; height: 150px;">
                                                    <%-- <table width="100%" class="singleborder" cellspacing="0" cellpadding="0">
                                                        <tr class="msgheader">
                                                            <td width="5%">
                                                            </td>
                                                            <td width="47.5%">
                                                                Amount
                                                            </td>
                                                            <td width="47.5%">
                                                                Beginning Range
                                                            </td>
                                                            <td width="47.5%">
                                                                End Range
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                            <td width="20%" valign="top">
                                                <table width="100%" valign="top" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <input type="image" name="$actionImg^setvalue%26node-ids%268%26content%26New_PremiumRangeCriteriaGrid"
                                                                src="csc-Theme/riskmaster/img/new.gif" alt="" id="New_PremiumRangeCriteriaGrid"
                                                                onmouseover="&#xA;javascript:document.all[&#34;New_PremiumRangeCriteriaGrid&#34;].src='csc-Theme/riskmaster/img/new2.gif';&#xA;"
                                                                onmouseout="&#xA;javascript:document.all[&#34;New_PremiumRangeCriteriaGrid&#34;].src='csc-Theme/riskmaster/img/new.gif';&#xA;"
                                                                title="New" onclick="&#xA;return openGridAddEditWindow(&#34;PremiumRangeCriteriaGrid&#34;,&#34;add&#34;,500,320);&#xA;; "><br>
                                                            <input type="image" name="$actionImg^setvalue%26node-ids%268%26content%26Edit_PremiumRangeCriteriaGrid"
                                                                src="csc-Theme/riskmaster/img/edittoolbar.gif" alt="" id="Edit_PremiumRangeCriteriaGrid"
                                                                onmouseover="&#xA;javascript:document.all[&#34;Edit_PremiumRangeCriteriaGrid&#34;].src='csc-Theme/riskmaster/img/edittoolbar2.gif';&#xA;"
                                                                onmouseout="&#xA;javascript:document.all[&#34;Edit_PremiumRangeCriteriaGrid&#34;].src='csc-Theme/riskmaster/img/edittoolbar.gif';&#xA;"
                                                                title="Edit" onclick="&#xA;return openGridAddEditWindow(&#34;PremiumRangeCriteriaGrid&#34;,&#34;edit&#34;,500,320);&#xA;; "><br>
                                                            <input type="image" name="$actionImg^setvalue%26node-ids%268%26content%26Delete_PremiumRangeCriteriaGrid"
                                                                src="csc-Theme/riskmaster/img/delete.gif" alt="" id="Delete_PremiumRangeCriteriaGrid"
                                                                onmouseover="&#xA;javascript:document.all[&#34;Delete_PremiumRangeCriteriaGrid&#34;].src='csc-Theme/riskmaster/img/delete2.gif';&#xA;"
                                                                onmouseout="&#xA;javascript:document.all[&#34;Delete_PremiumRangeCriteriaGrid&#34;].src='csc-Theme/riskmaster/img/delete.gif';&#xA;"
                                                                title="Delete" onclick="&#xA;return validateGridForDeletion(&#34;PremiumRangeCriteriaGrid&#34;);&#xA;; "><br>
                                                        </td>
                                                    </tr>
                                                </table>--%>
                                                    <dg:UserControlDataGrid runat="server" ID="PremiumRangeCriteriaGrid" GridName="PremiumRangeCriteriaGrid"
                                                        GridTitle="" Target="/discountrangelist" Ref="/Instance/Document/form//control[@name='PremiumRangeCriteriaGrid']"
                                                        Unique_Id="discountrangerowid" TextColumn="Extra" RowDataParam="discountrange" ShowRadioButton="True"
                                                        Width="100%" Height="80px" HideNodes="|addedbyuser|dttmrcdadded|updatedbyuser|dttmrcdlastupd|discountrangerowid|Extra"
                                                        ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="340" HideButtons=""
                                                        Type="GridAndButtons" />
                                                    <asp:TextBox Style="display: none" runat="server" ID="PremiumRangeCriteriaGridSelectedId"
                                                        RMXType="id"></asp:TextBox>
                                                    <asp:TextBox Style="display: none" runat="server" ID="PremiumRangeCriteriaGrid_RowDeletedFlag"
                                                        RMXType="id" Text="false"></asp:TextBox>
                                                    <asp:TextBox Style="display: none" runat="server" ID="PremiumRangeCriteriaGrid_Action"
                                                        RMXType="id"></asp:TextBox>
                                                    <asp:TextBox Style="display: none" runat="server" ID="PremiumRangeCriteriaGrid_RowAddedFlag"
                                                        RMXType="id" Text="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                  <%--End: Neha Suresh Jain, 05/05/2010--%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:TextBox Style="display: none" runat="server" ID="gridname"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="mode"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtvalidate"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtData"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="HdnActionSave" Text="" />
                    <asp:TextBox Style="display: none" runat="server" ID="HdnSelectedIdForDeletion" Text="" />
                    <asp:TextBox Style="display: none" runat="server" ID="HdnListNameForDeletion" Text="" />
                    <input type="text" name="" value="" id="SysViewType" style="display: none" />
                    <input type="text" name="" value="" id="SysCmd" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdConfirmSave" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdQueue" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate" />
                    <input type="text" name="" value="" id="SysClassName" style="display: none" rmxforms:value="" />
                    <input type="text" name="" value="" id="SysSerializationConfig" style="display: none" />
                    <input type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId" />
                    <input type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId" />
                    <input type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="DiscountParms" />
                    <input type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value="" />
                    <input type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="PremiumRangeCriteria" />
                    <input type="text" name="" value="" id="SysRequired" style="display: none" />
                    <asp:TextBox runat=server style="display: none" id="UseVolumeDiscount"></asp:TextBox>
                        
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-3" id="SysWindowId" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
