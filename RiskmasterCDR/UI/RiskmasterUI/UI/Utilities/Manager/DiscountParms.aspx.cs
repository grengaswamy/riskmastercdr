﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.Xml;
using Riskmaster.UI.Shared.Controls;

///////////////////////////////
// Developed By: abansal23
// Completed On: 11th Feb, 2009
///////////////////////////////
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class DiscountParms : NonFDMBasePageCWS
    {
        private XmlDocument oFDMPageDom = null;
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes

                if (!IsPostBack)
                {
                    Session["RowCount"] = -1;
                    Session["discountrangelist"] = GetDiscountRangeList();
                    Session["PostBackdiscountrangelist"] = string.Empty;//abansal23 12/8/2009 MITS 19054 : Error comes while saving volume discount without date ranges
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");

                    // Added By Amit Bansal
                    if (mode.Text.ToLower() != "add")
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("DiscountParmsAdaptor.Get", XmlTemplate, out sCWSresponse, false, false);
                        BindControls();
                    }
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        if (FunctionToCall.Text != "")
                        {
                            CallCWS(FunctionToCall.Text, XmlTemplate, out sCWSresponse, false, false);
                        }
                        else
                        {
                            CallCWS("DiscountParmsAdaptor.Get", XmlTemplate, out sCWSresponse, false, false);
                        }
                        BindControls();
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            if (mode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("DiscTierAdaptor.Get");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><discount>");
            sXml = sXml.Append("<UseVolDisc>" + UseVolDisc.Checked + "</UseVolDisc>");
            sXml = sXml.Append("<discountname>" + DiscountName.Text + "</discountname>");
            sXml = sXml.Append("<discountnametemp >" + DiscountNameTemp.Text + "</discountnametemp>");
            sXml = sXml.Append("<lob codeid='" + LOBcode.CodeIdValue + "'>" + LOBcode.CodeTextValue + "</lob>");
            sXml = sXml.Append("<lobtemp>" + LOBcodetemp.Text + "</lobtemp>");
            sXml = sXml.Append("<lobCodeidtemp>" + LOBcodeidtemp.Text + "</lobCodeidtemp>");
            sXml = sXml.Append("<state  codeid='" + Statecode.CodeIdValue + "'>" + Statecode.CodeTextValue + "</state>");
            sXml = sXml.Append("<discounttype codeid='" + DiscountType.CodeIdValue + "'>" + DiscountType.CodeTextValue + "</discounttype>");
            sXml = sXml.Append("<disocunttypecodeidtemp >" + DiscountTypeCodeIdTemp.Text + "</disocunttypecodeidtemp >");
            sXml = sXml.Append("<disocunttypetemp>" + DiscountTypeTemp.Text + "</disocunttypetemp>");
            sXml = sXml.Append("<amount>" + Amount.Text + "</amount>");
            sXml = sXml.Append("<amounttemp>" + AmountTemp.Text + "</amounttemp>");
            sXml = sXml.Append("<effectivedate>" + EffectiveDate.Text + "</effectivedate>");
            sXml = sXml.Append("<expirationdate>" + ExpirationDate.Text + "</expirationdate>");
            sXml = sXml.Append("<discountrowid>" + RowId.Text + "</discountrowid>");
            if (Session["discountrangelist"] != null)
            {
                string strTemp = Session["discountrangelist"].ToString();
                if (strTemp.LastIndexOf("<discountrangerowid>-") > 0)
                {
                    Session["PostBackdiscountrangelist"] = Session["discountrangelist"].ToString();
                    string strTempChar = strTemp[strTemp.LastIndexOf("<discountrangerowid>-") + 21].ToString();
                    for (int i = 1; i <= Int32.Parse(strTempChar); i++)
                    {
                        Session["discountrangelist"] = Session["discountrangelist"].ToString().Replace("-" + i, "");
                    }
                }
                sXml = sXml.Append(Session["discountrangelist"].ToString());
            }
            sXml = sXml.Append("</discount></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("DiscountParmsAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    //After the validation,there was a need to post back the parent page through javascript 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);
                    if (Session["discountrangelist"] != null)
                        Session.Remove("discountrangelist");
                    if (Session["PostBackdiscountrangelist"] != null)
                        Session.Remove("PostBackdiscountrangelist");
                    if (Session["RowCount"] != null)
                        Session.Remove("RowCount");
                }
                else
                {
                    if (Session["PostBackdiscountrangelist"] != null)
                    {
                        //abansal23 12/8/2009 MITS 19054 : Error comes while saving volume discount without date ranges 
                       if(!string.IsNullOrEmpty(Session["PostBackdiscountrangelist"].ToString()))
                        Session["discountrangelist"] = Session["PostBackdiscountrangelist"].ToString();
                        
                        //if (strTemp.LastIndexOf("<discountrangerowid>-") > 0)
                        //{
                        //    string strTempChar = strTemp[strTemp.LastIndexOf("<discountrangerowid>-") + 21].ToString();
                        //    for (int i = 1; i <= Int32.Parse(strTempChar); i++)
                        //    {
                        //        Session["discountrangelist"] = Session["discountrangelist"].ToString().Replace("-" + i, "");
                        //    }
                        //}
                    }
                    validate.Text = "validatefail";//abansal23 :  MITS 15213 on 04/09/2008
                }
                //else
                //{
                //    ErrorControl1.errorDom = sCWSresponse;
                //}
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Gets The discount list fot PremiumRangeCriteria
        /// </summary>
        /// <returns></returns>
        private string GetDiscountRangeList()
        {
            StringBuilder sList = new StringBuilder("<discountrangelist>");
            sList = sList.Append("<listhead>");
            sList = sList.Append("<addedbyuser>Added By User</addedbyuser>");
            sList = sList.Append("<dttmrcdadded>Dttm Rcd Added</dttmrcdadded>");
            sList = sList.Append("<updatedbyuser>Updated By User</updatedbyuser>");
            sList = sList.Append("<dttmrcdlastupd>Dttm Rcd Last Upd</dttmrcdlastupd>");
            sList = sList.Append("<amount>Amount</amount>");
            sList = sList.Append("<beginningrange>Beginning Range</beginningrange>");
            sList = sList.Append("<discountrangerowid>Discount Range Row Id</discountrangerowid>");
            sList = sList.Append("<endrange>End Range</endrange></listhead></discountrangelist>");
            return sList.ToString();
        }

        /// <summary>
        /// Binds data to the controls
        /// </summary>
        internal void BindControls()
        {
            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            XmlElement objExpRatesXMLEle = null;
            objExpRatesXMLEle = (XmlElement)oFDMPageDom.SelectSingleNode("//discount");

            DiscountName.Text = objExpRatesXMLEle.GetElementsByTagName("discountname").Item(0).InnerText;
            DiscountNameTemp.Text = objExpRatesXMLEle.GetElementsByTagName("discountnametemp").Item(0).InnerText;
            Amount.Text = objExpRatesXMLEle.GetElementsByTagName("amount").Item(0).InnerText;
            AmountTemp.Text = objExpRatesXMLEle.GetElementsByTagName("amounttemp").Item(0).InnerText;
            ((TextBox)LOBcode.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("lob").Item(0).InnerText;
            ((TextBox)LOBcode.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("lob").Item(0).Attributes["codeid"].Value;
            LOBcodetemp.Text = objExpRatesXMLEle.GetElementsByTagName("lobtemp").Item(0).InnerText;
            LOBcodeidtemp.Text = objExpRatesXMLEle.GetElementsByTagName("lobCodeidtemp").Item(0).InnerText;
            UseVolDisc.Checked = Boolean.Parse(objExpRatesXMLEle.GetElementsByTagName("UseVolDisc").Item(0).InnerText);
            EffectiveDate.Text = objExpRatesXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
            ExpirationDate.Text = objExpRatesXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;

            ((TextBox)DiscountType.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("discounttype").Item(0).InnerText;
            ((TextBox)DiscountType.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("discounttype").Item(0).Attributes["codeid"].Value;
            DiscountTypeTemp.Text = objExpRatesXMLEle.GetElementsByTagName("disocunttypetemp").Item(0).InnerText;
            DiscountTypeCodeIdTemp.Text = objExpRatesXMLEle.GetElementsByTagName("disocunttypecodeidtemp").Item(0).InnerText;
            ((TextBox)Statecode.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("state").Item(0).InnerText;
            ((TextBox)Statecode.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("state").Item(0).Attributes["codeid"].Value;
            RowId.Text = objExpRatesXMLEle.GetElementsByTagName("discountrowid").Item(0).InnerText;
            Session["discountrangelist"] = objExpRatesXMLEle.SelectSingleNode("//discountrangelist").OuterXml;
            AddExtraCol();
        }


        /// <summary>
        /// Adds Extra Column in Discount list xml for discount range datagrid 
        /// </summary>
        internal void AddExtraCol()
        {
            string strData = Session["discountrangelist"].ToString();
            XmlDocument xdata = new XmlDocument();
            xdata.LoadXml(strData);
            XmlNodeList nodes = xdata.SelectNodes("//discountrange");
            foreach (XmlElement node in nodes)
            {
                XmlElement extra = xdata.CreateElement("Extra");
                //rupal:start,r8 multicurrency
                //XmlText extraValue = xdata.CreateTextNode(node.ChildNodes[5].InnerText + "|" + node.ChildNodes[7].InnerText + "|" + node.ChildNodes[6].InnerText);
                XmlText extraValue = xdata.CreateTextNode(((XmlElement)(node.ChildNodes[5])).GetAttribute("Amount") + "|" + ((XmlElement)(node.ChildNodes[7])).GetAttribute("Amount") + "|" + node.ChildNodes[6].InnerText);
                extra.AppendChild(extraValue);
                node.AppendChild(extra);
                //rupal end
            }
            Session["discountrangelist"] = xdata.OuterXml;
        }
    }
}
