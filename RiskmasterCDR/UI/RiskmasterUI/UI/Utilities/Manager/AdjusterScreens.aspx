﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdjusterScreens.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.AdjusterScreens" ValidateRequest="false" %>
<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="dgt" TagName="UserControlDataGridTelerik" Src="~/UI/Shared/Controls/UserControlDataGridTelerik.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove"
xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head runat="server">
    <title>Auto Assign Adjuster</title>
    <style type="text/css">
        .SelectedItem
        {
            background: none repeat scroll 0 0 #6699FF !important;
        }
        
        .GridFilterRow_Telerik 
        {
            font: normal 8pt Verdana, Arial, Sans-serif;
            height: 18px;
            background: green;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <div>
        <uc1:errorcontrol id="ErrorControl1" runat="server" />
        <div>
            <span>
                <asp:TextBox id="AdjusterScreensGrid_OtherParams" style="display: none" runat="server"
                    Text="" />
                <dgt:usercontroldatagridtelerik runat="server" id="AdjusterScreensGrid" gridname="AdjusterScreensGrid"
                    gridtitle="Auto Assign Adjuster" target="/Document/AdjusterScreensList" ref="/Instance/Document/form//control[@name='AdjusterScreensGrid']"
                    unique_id="RowId" showradiobutton="true" width="" height="100%" hidenodes="|RowId|"
                    showheader="True" linkcolumn="" popupwidth="720" popupheight="510" type="GridAndButtons"
                    rowdataparam="listrow" />
            </span>
        </div>
        <asp:TextBox style="display: none" runat="server" id="AdjusterScreensSelectedId"
            RMXType="id" />
        <asp:TextBox style="display: none" runat="server" id="AdjusterScreensGrid_RowDeletedFlag"
            RMXType="id" Text="false" />
        <asp:TextBox style="display: none" runat="server" id="AdjusterScreensGrid_Action"
            RMXType="id" />
        <asp:TextBox style="display: none" runat="server" id="AdjusterScreensGrid_RowAddedFlag"
            RMXType="id" Text="false" />
    </div>
    </form>
</body>
</html>
