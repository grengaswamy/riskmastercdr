﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimActivityLogSetup.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ClaimActivityLogSetup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Claim Activity Log Setup</title>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="javascript" >
        function SelectGridRow(logId) {

            document.forms[0].hdnLogId.value = logId;
            
        }
    </script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <input type="hidden" id="hdnLogId" runat="server" />
    <div>
     <table  id="tblGrid" runat="server"  width="780px" cellspacing="0" cellpadding="0" border="0">
         <tr>
				<td colspan="2" class="ctrlgroup">
				  Claim Activity Log Setup
				</td>
		</tr>
		<tr>
		        <td colspan="2"><b></b></td>
		</tr>
		<tr>
		       <td width="95%" colspan="1" class="singleborder" >
		          <div id="divGrid" style="overflow:auto; height:470px"  >
		           <asp:GridView ID="GridView1" runat="server" AllowPaging="false" Width="100%"  AutoGenerateColumns="false"  
                            GridLines="None" onrowdatabound="GridView1_RowDataBound"  >
		              <HeaderStyle CssClass="msgheader" />
		              <AlternatingRowStyle CssClass="data2" />
		              <Columns>
		                  <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                      <input type="radio" id="selectrdo" name="ActLogSetupGrid"  />
                                    </ItemTemplate> 
                          </asp:TemplateField> 
                          <asp:TemplateField HeaderText="Table" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Table")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Activity Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTypeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ActType")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Operation" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblOpName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OpType")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="Log Text" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblLogText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LogText")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                      </Columns> 
		           </asp:GridView>
		           </div>
		       </td>
		       <td width="5%" valign="top" colspan="1">
		        
		        <input type="image" src="../../../Images/new.gif" alt="" id="New_ActLogSteupGrid" onmouseover="javascript:document.all['New_ActLogSteupGrid'].src='../../../Images/new2.gif'" onmouseout="javascript:document.all['New_ActLogSteupGrid'].src='../../../Images/new.gif'" title="New" onclick="return openGridAddEditWindow('ActLogSetupGrid','add',500,580);" /><br />
		        <input type="image" src="../../../Images/edittoolbar.gif" alt="" id="Edit_ActLogSteupGrid" onmouseover="javascript:document.all['Edit_ActLogSteupGrid'].src='../../../Images/edittoolbar2.gif'" onmouseout="javascript:document.all['Edit_ActLogSteupGrid'].src='../../../Images/edittoolbar.gif'" title="Edit" onclick="return openGridAddEditWindow('ActLogSetupGrid','edit',500,580);" /><br />
		        <asp:ImageButton runat="server" src="../../../Images/delete.gif" alt="" 
                       id="Delete_ActLogSteupGrid" 
                       onmouseover="javascript:document.all['Delete_ActLogSteupGrid'].src='../../../Images/delete2.gif'" 
                       onmouseout="javascript:document.all['Delete_ActLogSteupGrid'].src='../../../Images/delete.gif'" 
                       title="Delete" 
                       OnClientClick="return validateGridForDeletion('ActLogSetupGrid');" 
                       onclick="Delete_ActLogSteupGrid_Click" />
		        
		       </td>
		</tr>
       </table>
    </div>
    </form>
</body>
</html>
