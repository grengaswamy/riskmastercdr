﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.Xml;
using Riskmaster.UI.Shared.Controls;
///////////////////////////////
// Developed By: abansal23
// Completed On: 6th Feb, 2009
///////////////////////////////
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class DiscTier : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes

                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");

                    // Added By abansal23
                    if (mode.Text.ToLower() != "add")
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("DiscTierAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);
                    }
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("DiscTierAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            if (mode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("DiscTierAdaptor.Get");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><DiscTier>");
            sXml = sXml.Append("<control name='RowId' type='id'>" + RowId.Text + "</control>");
            sXml = sXml.Append("<control name='UseDiscTier' type='checkbox'>" + UseDiscTier.Checked+ "</control>");
            sXml = sXml.Append("<control name='TierName' type='text'>" + TierName.Text + "</control>");
            sXml = sXml.Append("<control name='LOBcode' type='code' codetable='POLICY_LOB' codeid='" + LOBcode.CodeIdValue + "'>" + LOBcode.CodeTextValue + "</control>");
            sXml = sXml.Append("<control name='Statecode' type='code' codetable='states' codeid='" + Statecode.CodeIdValue + "'>" + Statecode.CodeTextValue + "</control>");
            sXml = sXml.Append("<control name='EffectiveDate' type='date'>" + EffectiveDate.Text + "</control>");
            sXml = sXml.Append("<control name='ExpirationDate' type='date'>" + ExpirationDate.Text + "</control>");
            sXml = sXml.Append("<control name='AddedByUser'/><control name='DttmRcdAdded'/><control name='DttmRcdLastUpd'/><control name='UpdatedByUser'/>");
            sXml = sXml.Append("<control name='ParentLevel' codeid='" + ParentLevel_cid.Text + "'>" + ParentLevel.Text + "</control>");
            sXml = sXml.Append("</DiscTier></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("DiscTierAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    //After the validation,there was a need to post back the parent page through javascript 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
