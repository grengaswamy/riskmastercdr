﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using Telerik.Web.UI;
using System.Text;

namespace Riskmaster.UI.UI.Utilities.Manager.ImageRight
{
    public partial class DocumentMapping : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.Compare(hdnRebind.Value,"True",true)==0)
            {
                grdDocMap.Rebind();
            }
        }

        protected void grdDocMap_OnNeedDataSource(Object sender, EventArgs e)
        {
            string sReturnMsg = string.Empty;
            bool bReturnValue = false;
            XmlDocument xDoc = new XmlDocument();
            bReturnValue = CallCWS("DocumentMapAdaptor.Get", GetDefaultTemplate(), out sReturnMsg, false, false);
            if (bReturnValue)
            {
                DataSet oMapDataSet = new DataSet();
                DataTable oMapDataTable = oMapDataSet.Tables.Add("Mapping");
                DataRow oMapDataRow = null;

                oMapDataTable.Columns.Add("IrDocMapId", typeof(string));
                oMapDataTable.Columns.Add("rmADocClass", typeof(string));
                oMapDataTable.Columns.Add("rmADocCategory", typeof(string));
                oMapDataTable.Columns.Add("rmADocType", typeof(string));
                oMapDataTable.Columns.Add("IRFolder", typeof(string));
                oMapDataTable.Columns.Add("IRDocType", typeof(string));
                oMapDataTable.Columns.Add("IRDesc", typeof(string));

                xDoc.LoadXml(sReturnMsg);
                foreach (XmlNode xNode in xDoc.SelectNodes("//Map"))
                {
                    oMapDataRow = oMapDataTable.NewRow();
                    if (xNode.SelectSingleNode("IrDocMapId") != null)
                    {
                        oMapDataRow["IrDocMapId"] = xNode.SelectSingleNode("IrDocMapId").InnerText;
                    }
                    if (xNode.SelectSingleNode("RmaDocClass") != null)
                    {
                        oMapDataRow["rmADocClass"] = xNode.SelectSingleNode("RmaDocClass").InnerText;
                    }
                    if (xNode.SelectSingleNode("RmaDocCat") != null)
                    {
                        oMapDataRow["rmADocCategory"] = xNode.SelectSingleNode("RmaDocCat").InnerText;
                    }
                    if (xNode.SelectSingleNode("RmaDocType") != null)
                    {
                        oMapDataRow["rmADocType"] = xNode.SelectSingleNode("RmaDocType").InnerText;
                    }
                    if (xNode.SelectSingleNode("IrDocType") != null)
                    {
                        oMapDataRow["IRDocType"] = xNode.SelectSingleNode("IrDocType").InnerText;
                    }
                    if (xNode.SelectSingleNode("IrFolder") != null)
                    {
                        oMapDataRow["IRFolder"] = xNode.SelectSingleNode("IrFolder").InnerText;
                    }
                    if (xNode.SelectSingleNode("IrDesc") != null)
                    {
                        oMapDataRow["IRDesc"] = xNode.SelectSingleNode("IrDesc").InnerText;
                    }

                    oMapDataTable.Rows.Add(oMapDataRow);
                }

                grdDocMap.DataSource = oMapDataSet;

                oMapDataTable.Dispose();
                oMapDataTable.Dispose();
            }
            hdnRebind.Value = "True";
        }

        protected void grdDocMap_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                RadioButton grdRadio = null;
                GridDataItem item = e.Item as GridDataItem;
                Control radioControl = item["grdRadio"];
                if (radioControl != null)
                {
                    grdRadio = radioControl.FindControl("gdRadio") as RadioButton;
                    if (grdRadio != null)
                    {
                        grdRadio.Attributes.Add("OnClick", "selectSingleRadio(" + grdRadio.ClientID + ", " + "'grdDocMap'" + ") ");
                    }
                }
            }
        }

        private XElement GetDefaultTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Data />
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        /// <summary>
        /// Function to create and return a XElement XML Element which contains current row number 
        /// </summary>
        /// <createdby>Varun - vchouhan6</createdby>
        /// <returns>XElement</returns>
        private XElement DeleteTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<IrDocMapId>");
            sXml = sXml.Append(hdnRowID.Value);
            sXml = sXml.Append("</IrDocMapId>");
            sXml = sXml.Append("</Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());            
            return oTemplate;
        }

        /// <summary>
        /// function to delete the radgrid's selected row
        /// </summary>
        /// <createdby>Varun - vchouhan6</createdby>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            string sRowId = hdnRowID.Value;
            string sReturnMsg = string.Empty;
            bool bReturnValue = false;
            bReturnValue = CallCWS("DocumentMapAdaptor.Delete", DeleteTemplate(), out sReturnMsg, false, false);
            if (bReturnValue)
            {
                grdDocMap.Rebind();
            }
        }
    }
}