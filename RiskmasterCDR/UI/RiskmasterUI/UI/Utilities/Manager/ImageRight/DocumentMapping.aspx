﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentMapping.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ImageRight.DocumentMapping" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type='text/javascript'>
        var selectedDocId = '';
        $('document').ready(function () {
            if (parent.MDIScreenLoaded != null) {
                parent.MDIScreenLoaded();
            }

            $('#btnNew').mouseover(function () {
                this.src = "../../../../Images/tb_new_mo.png";
            });
            $('#btnNew').mouseout(function () {
                this.src = "../../../../Images/tb_new_active.png";
            });
            $('#btnNew').click(function () {
                window.open("./DocMapForm.aspx", "docMapFormWin", 'width=500,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes,modal=1');
                return false;
            });

            $('#btnEdit').mouseover(function () {
                this.src = "../../../../Images/tb_edit_mo.png";
            });
            $('#btnEdit').mouseout(function () {
                this.src = "../../../../Images/tb_edit_active.png";
            });

            $('#btnEdit').click(function () {
                var selectedRow = GetSelectedRows();
                if (selectedRow != null && selectedRow != undefined) {
                    if (selectedDocId != '') {
                        window.open("./DocMapForm.aspx?mode=edit&Id=" + selectedDocId, "docMapFormWin", 'width=500,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes,modal=1');
                    }
                }
                return false;
            });

            $('input[type=radio]').change(function () {
                $('input:checked').closest('tr').trigger('click');
            });

            $('tr').click(function () {
                $('input[type=radio]').prop('checked', false);
                $(this).find('input[type=radio]').prop('checked', true);
            });
		    $('#btnDelete').mouseover(function () {
                this.src = "../../../../Images/tb_delete_mo.png";
            });
            $('#btnDelete').mouseout(function () {
                this.src = "../../../../Images/tb_delete_active.png";
            });

            $('#btnDelete').click(function () {
                //get the checked/selected row
                //1. to find out if radio button of a row is checked or not
                var selectedRow = GetSelectedRows();
                if (selectedRow == null && selectedRow == undefined) {
                    //2.if not checked then ask user to select row
                    alert("Please select the record first");
                    return false;
                }
                else {
                    //3. else delete
                    if (!confirm('Are you sure you want to delete this mapping?')) {
                        return false;
                    }
                    else
                    {
                        if (selectedDocId != '')
                        {
                            $('#hdnRowID').val(selectedDocId);
                            $('#hdnRebind').val(false);
                            return true;
                        }
                    }
                }
            });
        });

        ///Code from:
        ///http ://www.telerik.com/community/code-library/aspnet-ajax/grid/single-radiobutton-check-at-a-time-with-row-selection.aspx
        function selectSingleRadio(objRadioButton, grdName) {
            var i, obj, pageElements;

            if (navigator.userAgent.indexOf("MSIE") != -1) {
                //IE browser  
                pageElements = document.all;
            }
            else if (navigator.userAgent.indexOf("Mozilla") != -1 || navigator.userAgent.indexOf("Opera") != -1) {
                //FireFox/Opera browser  
                pageElements = document.documentElement.getElementsByTagName("input");
            }
            for (i = 0; i < pageElements.length; i++) {
                obj = pageElements[i];

                if (obj.type == "radio") {
                    if (objRadioButton.id.substr(0, grdName.length) == grdName) {
                        if (objRadioButton.id == obj.id) {
                            obj.checked = true;
                        }
                        else {
                            obj.checked = false;
                        }
                    }
                }
            }
        }
    </script>
</head>
<body>
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="toolBarButton" runat="server" id="div_create" xmlns="">
       <asp:ImageButton ID="btnNew" ImageUrl="~/Images/tb_new_active.png" class="bold" ToolTip="<%$ Resources:btnNewResrc %>" runat="server" />
       <asp:ImageButton ID="btnEdit" ImageUrl="~/Images/tb_edit_active.png" class="bold" ToolTip="<%$ Resources:btnEditResrc %>" runat="server" />
        <asp:ImageButton ID="btnDelete" ImageUrl="~/Images/tb_delete_active.png" class="bold" OnClick="btnDelete_Click" ToolTip="<%$ Resources:btnDeleteResrc %>" runat="server" />
    </div>
    <div>
        <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="99.7%" Text="<%$ Resources:lblDocMappingResrc %>"></asp:Label>  
    </div>
    <div>
        <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
        <script type="text/javascript">
            function GetSelectedRows() {
                masterTable = $find("<%=grdDocMap.ClientID %>").get_masterTableView();
                var selectedRows = masterTable.get_selectedItems();
                if (selectedRows != null && selectedRows != undefined) {
                    return selectedRows[0];
                }
            }
            function RowSelected(sender, args) {
                selectedDocId = args.getDataKeyValue("IRDocMapId");
            }
          </script>
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        
    </telerik:RadAjaxManager>

    <telerik:RadGrid runat="server" ID="grdDocMap"
        AllowPaging="true" PageSize="15" AllowFilteringByColumn="false" AllowSorting="true"
        AutoGenerateColumns="false" ItemStyle-CssClass="rowlight1" AlternatingItemStyle-CssClass="rowlight2"
         OnNeedDataSource="grdDocMap_OnNeedDataSource" OnItemDataBound="grdDocMap_OnItemDataBound">
         <SelectedItemStyle CssClass="SelectedItem" />
        <ClientSettings>
                <ClientEvents OnRowSelected="RowSelected" />                    
                <Scrolling SaveScrollPosition="true" AllowScroll="true" ScrollHeight="500px"/>
                <Selecting AllowRowSelect="true" />
        </ClientSettings>

        <MasterTableView Width="100%" AllowCustomSorting="false" ClientDataKeyNames="IRDocMapId">
             <PagerStyle AlwaysVisible="true" Position="Top" mode="NextPrevAndNumeric"/>
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                 <NoRecordsTemplate>
                  <div>
                   <asp:Label runat="server" Text="<%$ Resources:divNoRecordToDisplayResrc %>" ></asp:Label>
                  </div>
                </NoRecordsTemplate>
            <Columns>
                <telerik:GridTemplateColumn UniqueName="grdRadio">
                        <ItemTemplate>
                            <asp:RadioButton ID="gdRadio" runat="server" AutoPostBack="false"/>
                        </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="IrDocMapId" HeaderText=" <%$ Resources:gvHdrIRDocMapIdResrc %>" UniqueName="IrDocMapId" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="rmADocClass" HeaderText="<%$ Resources:gvHdrRMADocClassRersrc %>" UniqueName="rmADocClass">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="rmADocType" HeaderText="<%$ Resources:gvHdrRMADocTypeRersrc %>" UniqueName="rmADocType">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="rmADocCategory" HeaderText="<%$ Resources:gvHdrRMADocCategoryRersrc %>" UniqueName="rmADocCategory">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IRDocType" HeaderText="<%$ Resources:gvHdrIRDocTypeResrc %>" UniqueName="IRDocType">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IRFolder" HeaderText="<%$ Resources:gvHdrIRFolderResrc %>" UniqueName="IRFolder">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IRDesc" HeaderText="<%$ Resources:gvHdrDescriptionResrc %>" UniqueName="IRDesc">
                </telerik:GridBoundColumn>             
            </Columns>
        </MasterTableView>
            
    </telerik:RadGrid>
    </div>
    <asp:HiddenField ID="hdnRebind" runat="server" />
    <asp:HiddenField ID="hdnRowID" runat="server" />
    </form>
    
</body>
</html>
