﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;

namespace Riskmaster.UI.UI.Utilities.Manager.ImageRight.FUP
{
    public partial class UserDataMap : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hdnCloseWindow.Value = "False";
            if (!IsPostBack)
            {
                hdnType.Value= AppHelper.GetQueryStringValue("type");

                BindCmbUserData();
                BindCmbTableName();
                string sMode = AppHelper.GetQueryStringValue("mode");
                if (string.Compare(sMode, "edit", true) == 0)
                {
                    XmlDocument xReturnDoc = new XmlDocument();
                    UserdataMapId.Text = AppHelper.GetQueryStringValue("Id");
                    string sCWSResponse = string.Empty;
                    bool bReturnValue = CallCWS("UserDataMapping.Get", GetLoadTemplate(), out sCWSResponse, false, false);
                    if (bReturnValue)
                    {
                        xReturnDoc.LoadXml(sCWSResponse);
                        if (string.Compare(xReturnDoc.SelectSingleNode("//MsgStatusCd").InnerText, "Success", true) == 0)
                        {
                            if (xReturnDoc.SelectSingleNode("//UserData") != null)
                            {
                                cmbUserData.SelectedValue = xReturnDoc.SelectSingleNode("//UserData").InnerText;
                            }
                            if (xReturnDoc.SelectSingleNode("//ImgRightName") != null)
                            {
                                txtIRName.Text = xReturnDoc.SelectSingleNode("//ImgRightName").InnerText;
                            }
                            if (xReturnDoc.SelectSingleNode("//TableName") != null)
                            {
                                if (!string.IsNullOrEmpty(xReturnDoc.SelectSingleNode("//TableName").InnerText))
                                {
                                    cmbTableName.SelectedValue = xReturnDoc.SelectSingleNode("//TableName").InnerText;
                                    BindCmbColumnName();
                                }
                            }
                            if (xReturnDoc.SelectSingleNode("//FieldName") != null)
                            {
                                if (!string.IsNullOrEmpty(xReturnDoc.SelectSingleNode("//FieldName").InnerText))
                                {
                                    cmbColumnName.SelectedValue = xReturnDoc.SelectSingleNode("//FieldName").InnerText;
                                }
                            }
                        }
                    }
                }
                else
                {
                    BindCmbColumnName();
                }
            }
            
        }

        private void BindCmbUserData()
        {
            cmbUserData.Items.Clear();
            cmbUserData.Items.Add("Userdata1");
            cmbUserData.Items.Add("Userdata2");
            cmbUserData.Items.Add("Userdata3");
            cmbUserData.Items.Add("Userdata4");
            cmbUserData.Items.Add("Userdata5");

            cmbUserData.DataBind();
        }

        protected void cmbTableName_OnSelectedIndexChange(object sender, EventArgs e)
        {
            BindCmbColumnName();
        }

        private void BindCmbTableName()
        {
            cmbTableName.Items.Clear();
            cmbTableName.Items.Add("Claim");
            cmbTableName.Items.Add("Claim Supplemental");
            cmbTableName.Items.Add("Event");
            cmbTableName.Items.Add("Event Supplemental");
            cmbTableName.Items.Add("Current Adjuster");
            cmbTableName.Items.Add("Primary Claimant");
            cmbTableName.Items.Add("Policy (Primary/First)");
            //cmbTableName.Items.Add("Reserve");

            cmbTableName.DataBind();
        }

        private void BindCmbColumnName()
        {
            bool bSupp = false;
            string sSuppName = string.Empty;
            cmbColumnName.Items.Clear();
            switch (cmbTableName.SelectedValue.ToUpper())
            {
                case "CLAIM":
                    cmbColumnName.Items.Add("[Claim Number]");
                    cmbColumnName.Items.Add("[Date Of Claim]");
                    break;
                case "EVENT":
                    cmbColumnName.Items.Add("[Event Number]");
                    cmbColumnName.Items.Add("[Department]");
                    cmbColumnName.Items.Add("[Date Of Event]");
                    break;
                case "CURRENT ADJUSTER":
                    cmbColumnName.Items.Add("[Last Name]");
                    cmbColumnName.Items.Add("[LastName], [FirstName]");
                    break;
                case "PRIMARY CLAIMANT":
                    cmbColumnName.Items.Add("[Last Name]");
                    cmbColumnName.Items.Add("[LastName], [FirstName]");
                    cmbColumnName.Items.Add("[Claimant Type]");
                    break;
                case "POLICY (PRIMARY/FIRST)":
                    cmbColumnName.Items.Add("[Policy Name]");
                    cmbColumnName.Items.Add("[Policy Number]");
                    break;
                case "RESERVE":
                    cmbColumnName.Items.Add("LOSS [Amount] EXP [Amount]");
                    cmbColumnName.Items.Add("LOSS [Balance] EXP [Balance]");
                    cmbColumnName.Items.Add("[Balance]");
                    cmbColumnName.Items.Add("[Amount]");
                    break;
                case "CLAIM SUPPLEMENTAL":
                    bSupp = true;
                    sSuppName = "CLAIM_SUPP";
                    break;
                case "EVENT SUPPLEMENTAL":
                    bSupp = true;
                    sSuppName = "EVENT_SUPP";
                    break;
                default:
                    break;
            }
            if (bSupp)
            {
                XmlDocument xReturnDoc = new XmlDocument();
                string sCWSResponse = string.Empty;
                bool bReturnValue = CallCWS("UserDataMapping.GetSuppValue", GetSuppTemplate(sSuppName), out sCWSResponse, false, false);
                if (bReturnValue)
                {
                    xReturnDoc.LoadXml(sCWSResponse);
                    if (string.Compare(xReturnDoc.SelectSingleNode("//MsgStatusCd").InnerText, "Success", true) == 0)
                    {
                        foreach (XmlNode xNode in xReturnDoc.SelectNodes("//FieldName"))
                        {
                            if (!string.IsNullOrEmpty(xNode.InnerText))
                            {
                                cmbColumnName.Items.Add(xNode.InnerText);
                            }
                        }
                    }
                }
            }
            cmbColumnName.DataBind();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            string sCWSResponse = string.Empty;
            XmlDocument xReturnDoc = new XmlDocument();
            bool bReturnValue = CallCWS("UserDataMapping.Save", GetSaveTemplate(), out sCWSResponse, false, false);
            if (bReturnValue)
            {
                xReturnDoc.LoadXml(sCWSResponse);
                if (string.Compare(xReturnDoc.SelectSingleNode("//MsgStatusCd").InnerText, "Success", true) == 0)
                {
                    hdnCloseWindow.Value = "True";
                }
            }
        }

        private XElement GetSaveTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Mapping>
                        <UserDataMapId>" + UserdataMapId.Text + @"</UserDataMapId>
                        <UserData>" + cmbUserData.SelectedValue + @"</UserData>
                        <ImgRightName>" + txtIRName.Text + @"</ImgRightName>
                        <TableName>" + cmbTableName.SelectedValue + @"</TableName>
                        <FieldName>" + cmbColumnName.SelectedValue + @"</FieldName>
                        <UserDataType>" + hdnType.Value + @"</UserDataType>
                    </Mapping>
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        private XElement GetLoadTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Mapping>
                        <UserDataMapId>" + UserdataMapId.Text + @"</UserDataMapId>
                        <UserDataType>" + hdnType.Value + @"</UserDataType>
                    </Mapping>
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        private XElement GetSuppTemplate(string p_sSuppTable)
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Supp>
                        <SuppTable>" + p_sSuppTable+ @"</SuppTable>
                    </Supp>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
    }
}