﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserDataMapping.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ImageRight.FUP.UserDataMapping" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type='text/javascript'>
        var selectedDocId = '';
        $('document').ready(function () {
            //Reset hdnAction value
            if ($('#hdnAction')) {
                $('#hdnAction').val('');
            }

            if (parent.MDIScreenLoaded != null) {
                parent.MDIScreenLoaded();
            }

            if ($('#lblcaption') && $('#hdnCaption')) {
                $('#lblcaption').text($('#hdnCaption').val());
            }

            $('#btnNew').mouseover(function () {
                this.src = "../../../../../Images/tb_new_mo.png";
            });
            $('#btnNew').mouseout(function () {
                this.src = "../../../../../Images/tb_new_active.png";
            });
            $('#btnNew').click(function () {
                var userDataType = "0";
                if ($('#hdnUserDataType')) {
                    userDataType = $('#hdnUserDataType').val()
                }
                window.open("./UserDataMapForm.aspx?type=" + userDataType, "docMapFormWin", 'width=650,height=400' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes,modal=1');
                return false;
            });

            $('#btnEdit').mouseover(function () {
                this.src = "../../../../../Images/tb_edit_mo.png";
            });
            $('#btnEdit').mouseout(function () {
                this.src = "../../../../../Images/tb_edit_active.png";
            });
            $('#btnEdit').click(function () {
                var userDataType = "0";
                if ($('#hdnUserDataType')) {
                    userDataType = $('#hdnUserDataType').val()
                }

                var selectedRow = GetSelectedRows();
                if (selectedRow != null && selectedRow != undefined) {
                    if (selectedDocId != '') {
                        window.open("./UserDataMapForm.aspx?type=" + userDataType + "&mode=edit&Id=" + selectedDocId, "docMapFormWin", 'width=500,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes,modal=1');
                    }
                }
                return false;
            });

            $('#btnDelete').mouseover(function () {
                this.src = "../../../../../Images/tb_delete_mo.png";
            });
            $('#btnDelete').mouseout(function () {
                this.src = "../../../../../Images/tb_delete_active.png";
            });
            $('#btnDelete').click(function () {
                var userDataType = "0";
                if ($('#hdnUserDataType')) {
                    userDataType = $('#hdnUserDataType').val()
                }

                var selectedRow = GetSelectedRows();
                if (selectedRow != null && selectedRow != undefined) {
                    if (selectedDocId != '') {
                        if (confirm("This will permanantly delete the selected data")) {
                            if ($('#hdnAction')) {
                                $('#hdnAction').val('Delete');
                                $('#frmData').submit();
                            }
                        }
                    }
                }
                return false;
            });

            $('input[type=radio]').change(function () {
                $('input:checked').closest('tr').trigger('click');
            });

            $('tr').click(function () {
                $('input[type=radio]').prop('checked', false);
                $(this).find('input[type=radio]').prop('checked', true);
            });
        });

        ///Code from:
        ///http ://www.telerik.com/community/code-library/aspnet-ajax/grid/single-radiobutton-check-at-a-time-with-row-selection.aspx
        function selectSingleRadio(objRadioButton, grdName) {
            var i, obj, pageElements;

            if (navigator.userAgent.indexOf("MSIE") != -1) {
                //IE browser  
                pageElements = document.all;
            }
            else if (navigator.userAgent.indexOf("Mozilla") != -1 || navigator.userAgent.indexOf("Opera") != -1) {
                //FireFox/Opera browser  
                pageElements = document.documentElement.getElementsByTagName("input");
            }
            for (i = 0; i < pageElements.length; i++) {
                obj = pageElements[i];

                if (obj.type == "radio") {
                    if (objRadioButton.id.substr(0, grdName.length) == grdName) {
                        if (objRadioButton.id == obj.id) {
                            obj.checked = true;
                        }
                        else {
                            obj.checked = false;
                        }
                    }
                }
            }
        }
    </script>
</head>
<body>
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="toolBarButton" runat="server" id="div_create" xmlns="">
       <asp:ImageButton ID="btnNew" ImageUrl="~/Images/tb_new_active.png" class="bold" ToolTip="<%$ Resources:btnNewResrc %>" runat="server" />
       <asp:ImageButton ID="btnEdit" ImageUrl="~/Images/tb_edit_active.png" class="bold" ToolTip="<%$ Resources:btnEditResrc %>" runat="server" />
       <asp:ImageButton ID="btnDelete" ImageUrl="~/Images/tb_delete_active.png" class="bold" ToolTip="<%$ Resources:btnDeleteResrc %>" runat="server" />
    </div>
    <div>
        <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="99.7%" Text="<%$ Resources:lblUserdataMappingResrc %>"></asp:Label>  
    </div>
    <div>
        <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
        <script type="text/javascript">
            function GetSelectedRows() {
                masterTable = $find("<%=grdUserDataMap.ClientID %>").get_masterTableView();
                var selectedRows = masterTable.get_selectedItems();
                if (selectedRows != null && selectedRows != undefined) {
                    return selectedRows[0];
                }
            }
            function RowSelected(sender, args) {
                selectedDocId = args.getDataKeyValue("UserDataMapId");
            }
          </script>
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        
    </telerik:RadAjaxManager>

    <telerik:RadGrid runat="server" ID="grdUserDataMap"
        AllowPaging="true" PageSize="15" AllowFilteringByColumn="false" AllowSorting="false"
        AutoGenerateColumns="false" ItemStyle-CssClass="rowlight1" AlternatingItemStyle-CssClass="rowlight2"
         OnNeedDataSource="grdUserDataMap_OnNeedDataSource" OnItemDataBound="grdUserDataMap_OnItemDataBound">
         <SelectedItemStyle CssClass="SelectedItem" />
        <ClientSettings>
                <ClientEvents OnRowSelected="RowSelected" />                    
                <Scrolling SaveScrollPosition="true" AllowScroll="true" ScrollHeight="500px"/>
                <Selecting AllowRowSelect="true" />
        </ClientSettings>

        <MasterTableView Width="100%" AllowCustomSorting="false" ClientDataKeyNames="UserDataMapId">
             <PagerStyle AlwaysVisible="true" Position="Top" mode="NextPrevAndNumeric"/>
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                 <NoRecordsTemplate>
                  <div>
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:divNoRecordToDisplayResrc %>" ></asp:Label>
                  </div>
                </NoRecordsTemplate>
            <Columns>
                <telerik:GridTemplateColumn UniqueName="grdRadio">
                        <ItemTemplate>
                            <asp:RadioButton ID="gdRadio" runat="server" AutoPostBack="false"/>
                        </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="UserDataMapId" HeaderText=" <%$ Resources:gvHdrUserDataMapIdResrc %>" UniqueName="UserDataMapId" visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="UserData" HeaderText="<%$ Resources:gvHdrUserDataResrc %>" UniqueName="UserData">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ImgRightName" HeaderText="<%$ Resources:gvHdrImageRightNameResrc %>" UniqueName="ImgRightName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="TableName" HeaderText="<%$ Resources:gvHdrTableNameResrc %>" UniqueName="tableName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FieldName" HeaderText="<%$ Resources:gvHdrFieldNameResrc %>" UniqueName="FieldName">
                </telerik:GridBoundColumn>         
            </Columns>
        </MasterTableView>
            
    </telerik:RadGrid>
    </div>
    <asp:HiddenField ID="hdnRebind" runat="server" />;
    <asp:HiddenField ID="hdnCaption" runat="server" />
    <asp:HiddenField ID="hdnUserDataType" runat="server" />
    <asp:HiddenField ID="hdnAction" runat="server" />
    </form>
    
</body>
</html>
