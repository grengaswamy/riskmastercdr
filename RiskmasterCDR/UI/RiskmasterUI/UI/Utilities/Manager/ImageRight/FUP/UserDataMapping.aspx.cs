﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Xml;
using System.Data;
using System.Xml.Linq;

namespace Riskmaster.UI.UI.Utilities.Manager.ImageRight.FUP
{
    public partial class UserDataMapping : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.Compare(AppHelper.GetQueryStringValue("type"), "FUP", false) == 0)
                {
                    hdnCaption.Value = "FUP - Userdata Mapping";
                    hdnUserDataType.Value = "1";
                }
                else
                {
                    hdnCaption.Value = "Cold Import - Userdata Mapping";
                    hdnUserDataType.Value = "0";
                }
            }
            else
            {
                if (string.Compare(hdnAction.Value, "Delete", true) == 0)
                {
                    DeleteRecord();
                }
            }
            
            if (string.Compare(hdnRebind.Value, "True", true) == 0)
            {
                grdUserDataMap.Rebind();
            }
        }

        protected void grdUserDataMap_OnNeedDataSource(Object sender, EventArgs e)
        {
            string sReturnMsg = string.Empty;
            bool bReturnValue = false;
            XmlDocument xDoc = new XmlDocument();
            bReturnValue = CallCWS("UserDataMapping.Get", GetDefaultTemplate(), out sReturnMsg, false, false);
            if (bReturnValue)
            {
                DataSet oMapDataSet = new DataSet();
                DataTable oMapDataTable = oMapDataSet.Tables.Add("Mapping");
                DataRow oMapDataRow = null;

                oMapDataTable.Columns.Add("UserDataMapId", typeof(string));
                oMapDataTable.Columns.Add("UserData", typeof(string));
                oMapDataTable.Columns.Add("ImgRightName", typeof(string));
                oMapDataTable.Columns.Add("TableName", typeof(string));
                oMapDataTable.Columns.Add("FieldName", typeof(string));

                xDoc.LoadXml(sReturnMsg);
                foreach (XmlNode xNode in xDoc.SelectNodes("//Map"))
                {
                    oMapDataRow = oMapDataTable.NewRow();
                    if (xNode.SelectSingleNode("UserDataMapId") != null)
                    {
                        oMapDataRow["UserDataMapId"] = xNode.SelectSingleNode("UserDataMapId").InnerText;
                    }
                    if (xNode.SelectSingleNode("UserData") != null)
                    {
                        oMapDataRow["UserData"] = xNode.SelectSingleNode("UserData").InnerText;
                    }
                    if (xNode.SelectSingleNode("ImgRightName") != null)
                    {
                        oMapDataRow["ImgRightName"] = xNode.SelectSingleNode("ImgRightName").InnerText;
                    }
                    if (xNode.SelectSingleNode("TableName") != null)
                    {
                        oMapDataRow["TableName"] = xNode.SelectSingleNode("TableName").InnerText;
                    }
                    if (xNode.SelectSingleNode("FieldName") != null)
                    {
                        oMapDataRow["FieldName"] = xNode.SelectSingleNode("FieldName").InnerText;
                    }

                    oMapDataTable.Rows.Add(oMapDataRow);
                }

                grdUserDataMap.DataSource = oMapDataSet;

                oMapDataTable.Dispose();
                oMapDataTable.Dispose();
            }
            hdnRebind.Value = "True";
        }

        protected void grdUserDataMap_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                RadioButton grdRadio = null;
                GridDataItem item = e.Item as GridDataItem;
                Control radioControl = item["grdRadio"];
                if (radioControl != null)
                {
                    grdRadio = radioControl.FindControl("gdRadio") as RadioButton;
                    if (grdRadio != null)
                    {
                        grdRadio.Attributes.Add("OnClick", "selectSingleRadio(" + grdRadio.ClientID + ", " + "'grdUserDataMap'" + ") ");
                    }
                }
            }
        }

        private void DeleteRecord()
        {
            string sReturnMsg = string.Empty;
            string sSelectedID = string.Empty;

            //Only one Row can be selected
            foreach (GridDataItem selectedItem in grdUserDataMap.SelectedItems)
            {
                sSelectedID = selectedItem["UserDataMapId"].Text;
            }

            if (!string.IsNullOrEmpty(sSelectedID))
            {
                CallCWS("UserDataMapping.Delete", GetDeleteTemplate(sSelectedID), out sReturnMsg, false, false);
            }
        }

        private XElement GetDefaultTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Data>
                        <UserDataType>" + hdnUserDataType.Value + @"</UserDataType>
                    </Data>
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        private XElement GetDeleteTemplate(string p_sId)
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Data>
                        <UserDataMapId>" + p_sId + @"</UserDataMapId>
                    </Data>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
    }
}