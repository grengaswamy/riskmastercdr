﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using Telerik.Web.UI;
using System.Xml.Linq;
using System.Text;

namespace Riskmaster.UI.UI.Utilities.Manager.ImageRight.FUP
{
    public partial class FileMarkMappings : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.Compare(hdnRebind.Value, "True", true) == 0)
            {
                grdFileMarkaMap.Rebind();
            }
        }
        protected void grdFileMarkaMap_OnNeedDataSource(Object sender, EventArgs e)
        {
            string sReturnMsg = string.Empty;
            bool bReturnValue = false;
            XmlDocument xDoc = new XmlDocument();
            bReturnValue = CallCWS("FUPFile.GetFileMarks", GetDefaultTemplate(), out sReturnMsg, false, false);
            if (bReturnValue)
            {
                DataSet oMapDataSet = new DataSet();
                DataTable oMapDataTable = oMapDataSet.Tables.Add("FileMarkMapping");
                DataRow oMapDataRow = null;

                oMapDataTable.Columns.Add("FileMarkId", typeof(string));
                oMapDataTable.Columns.Add("IRMarkId", typeof(string));
                oMapDataTable.Columns.Add("IrFileMarkName", typeof(string));
                oMapDataTable.Columns.Add("Count", typeof(string));
                oMapDataTable.Columns.Add("TableName", typeof(string));
                oMapDataTable.Columns.Add("FieldName", typeof(string));
                oMapDataTable.Columns.Add("Operator", typeof(string));
                oMapDataTable.Columns.Add("Value", typeof(string));

                xDoc.LoadXml(sReturnMsg);
                foreach (XmlNode xNode in xDoc.SelectNodes("//Map"))
                {
                    oMapDataRow = oMapDataTable.NewRow();
                    if (xNode.SelectSingleNode("IRFileMarkMappingId") != null)
                    {
                        oMapDataRow["FileMarkId"] = xNode.SelectSingleNode("IRFileMarkMappingId").InnerText;
                    }
                    if (xNode.SelectSingleNode("IRMarkId") != null)
                    {
                        oMapDataRow["IRMarkId"] = xNode.SelectSingleNode("IRMarkId").InnerText;
                    }
                    if (xNode.SelectSingleNode("IRFileMarkName") != null)
                    {
                        oMapDataRow["IrFileMarkName"] = xNode.SelectSingleNode("IRFileMarkName").InnerText;
                    }
                    if (xNode.SelectSingleNode("Count") != null)
                    {
                        oMapDataRow["Count"] = xNode.SelectSingleNode("Count").InnerText;
                    }
                    if (xNode.SelectSingleNode("TableName") != null)
                    {
                        oMapDataRow["TableName"] = xNode.SelectSingleNode("TableName").InnerText;
                    }
                    if (xNode.SelectSingleNode("FieldName") != null)
                    {
                        oMapDataRow["FieldName"] = xNode.SelectSingleNode("FieldName").InnerText;
                    }
                    if (xNode.SelectSingleNode("Operator") != null)
                    {
                        oMapDataRow["Operator"] = xNode.SelectSingleNode("Operator").InnerText;
                    }
                    if (xNode.SelectSingleNode("Value") != null)
                    {
                        oMapDataRow["Value"] = xNode.SelectSingleNode("Value").InnerText;
                    }

                    oMapDataTable.Rows.Add(oMapDataRow);
                }

                grdFileMarkaMap.DataSource = oMapDataSet;

                oMapDataTable.Dispose();
                oMapDataTable.Dispose();
            }
            hdnRebind.Value = "True";
        }

        protected void grdFileMarkaMap_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                RadioButton grdRadio = null;
                GridDataItem item = e.Item as GridDataItem;
                Control radioControl = item["grdRadio"];
                if (radioControl != null)
                {
                    grdRadio = radioControl.FindControl("gdRadio") as RadioButton;
                    if (grdRadio != null)
                    {
                        grdRadio.Attributes.Add("OnClick", "selectSingleRadio(" + grdRadio.ClientID + ", " + "'grdFileMarkaMap'" + ") ");
                    }
                }
            }
        }

        private XElement GetDefaultTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Data />
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        /// <summary>
        /// Function to create and return a XElement XML Element which contains current row number 
        /// </summary>
        /// <createdby>Varun - vchouhan6</createdby>
        /// <returns>XElement</returns>
        private XElement DeleteTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<IRFileMarkMappingId>");
            sXml = sXml.Append(hdnRowID.Value);
            sXml = sXml.Append("</IRFileMarkMappingId>");
            sXml = sXml.Append("</Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        /// <summary>
        /// function to delete the radgrid's selected row
        /// </summary>
        /// <createdby>Varun - vchouhan6</createdby>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            string sRowId = hdnRowID.Value;
            string sReturnMsg = string.Empty;
            bool bReturnValue = false;
            bReturnValue = CallCWS("FUPFile.DeleteFileMarkMapping", DeleteTemplate(), out sReturnMsg, false, false);
           
            if (bReturnValue)
            {
                grdFileMarkaMap.Rebind();
            }

        }
    }
}