﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileMarkMapForm.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ImageRight.FUP.FileMarkMap" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="usc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    .left{
      width: 49%;
      float: left;
      margin-top:5px;
      vertical-align:middle;
    }
    .right{
      margin-top:5px;
      vertical-align:middle;
    }
    .hide{
        display:none;
    }
    </style>
    <script src="../../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($.trim($('#hdnCloseWindow').val()) === 'True') {
                if (window.opener.$('#frmData')) {
                    window.opener.$('#frmData').submit();
                }
                window.close();
            }

            chkCountSelect();

            $('#chkCount').change(function () {
                chkCountSelect();
            });

            $('#btnSave').click(function () {
                if (!Validate()) {
                    return false;
                }
            });

            $('#btnCancel').click(function () {
                window.close();
            });

            
        });

        function ShowHide() {
            if ($('#hdnFieldType').val() === 'code') {
                $('[RMXType=code]').removeClass('hide');
                $('#txtValue').addClass('hide');

                if ($('#FileMarkValue_codelookupbtn')) {
                    $('#FileMarkValue_codelookupbtn').removeAttr('onclick');
                    $('#FileMarkValue_codelookupbtn').click(function () {
                        return selectCode($('#hdnCodeTableName').val(), 'FileMarkValue_codelookup', '', '', '');
                    });
                }
            }
            else {
                $('[RMXType=code]').addClass('hide');
                $('#txtValue').removeClass('hide');
            }
        }

        function chkCountSelect() {
            if ($('#chkCount').is(':checked')) {
                $('[RMXType=code]').addClass('hide');
                $('#txtValue').removeClass('hide');

                $('#cmbFieldName').prop('disabled', 'disabled');
                
                $('#cmbOperator').addClass('hide');
                $('#cmbOperatorCount').removeClass('hide');
            }
            else {
                ShowHide();

                $('#cmbFieldName').removeProp('disabled');

                $('#cmbOperator').removeClass('hide');
                $('#cmbOperatorCount').addClass('hide');
            }
        }

        function Validate() {
            var bValid = true;
            if ($('#cmbOperator').is(':visible') && ($('#cmbOperator').val() == 'IS NULL' || $('#cmbOperator').val() == 'IS NOT NULL')) {
                $('#FileMarkValue_codelookup').attr('disabled', 'disabled').val('');
                $('#FileMarkValue_codelookup_cid').val('0');
                $('#txtValue').attr('disabled', 'disabled').val('');
            }

            $('input[type=text]').not(':disabled,:hidden').each(function () {
                if ($.trim($(this).val()) === '') {
                    bValid = false;
                    alert("Missing required field(s)");
                }
            });
            
            //If still valid - do additional validations
            if (bValid) {
                if ($('#chkCount').is(':checked')) {
                    if (!$('#txtValue').val().match(/^\d+$/)) {
                        $('#txtValue').val('');
                        bValid = false;
                        alert("Only Numbers allowed in the field \"Value\"");
                        $('#txtValue').focus();
                    }
                }
            }

            return bValid;
        }
    </script>
</head>
<body>
    <form id="frmData" runat="server">
    <div>
        <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
        <div>
        <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_savemapping_active.png" class="bold" ToolTip="<%$ Resources:btnSaveResrc %> "
                            runat="server" OnClick="OnSaveClick"/>
        <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:btnCancelResrc %>"
                            runat="server" />

        <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="99%" Text="<%$ Resources:lblFileMarkResrc %>"></asp:Label>
            <div>
                <asp:Label ID="lblFileMarkId" runat="server" CssClass="left required" Text="<%$ Resources:lblIRFileMarkIdResrc %> "></asp:Label>
                <asp:TextBox ID="txtFileMarkId" runat="server" CssClass="right" OnChange="setDataChanged(true);"></asp:TextBox>
            </div>
            <div>
                <asp:Label ID="lblFileMarkName" runat="server" CssClass="left required" Text="<%$ Resources:lblIRFileMarkNameResrc %> "></asp:Label>
                <asp:TextBox ID="txtFileMarkName" runat="server" CssClass="right" OnChange="setDataChanged(true);"></asp:TextBox>
            </div>
            <div>
                <asp:Label ID="lblIsCount" runat="server" CssClass="left required" Text="<%$ Resources:lblCountResrc %> "></asp:Label>
                <asp:CheckBox runat="server" CssClass="right" ID="chkCount" />
            </div>
            <div style="clear:both" />
            <div>
                <asp:Label ID="lblTableName" runat="server" CssClass="left required" Text="<%$ Resources:lblTableNameResrc %> "></asp:Label>
                <asp:DropDownList runat="server" CssselesClass="right" ID="cmbTableName" OnChange="setDataChanged(true);"
                    AutoPostBack="true" OnSelectedIndexChanged="OnTableChange"/>
            </div>
            <div>
                <asp:Label ID="lblFieldName" runat="server" CssClass="left required" Text="<%$ Resources:lblFieldNameResrc %> "></asp:Label>
                <asp:DropDownList runat="server" CssClass="right" ID="cmbFieldName" OnChange="setDataChanged(true);" AutoPostBack="true" 
                    OnSelectedIndexChanged="OnFieldChange"/>
                <asp:HiddenField runat="server" ID="hdnFieldType" Value="" />
                <asp:HiddenField runat="server" ID="hdnCodeTableName" Value="" />
            </div>
             <div>
                <asp:Label ID="lblOperator" runat="server" CssClass="left required" Text="<%$ Resources:lblOperatorResrc %> "></asp:Label>
                <asp:DropDownList runat="server" CssselesClass="right" ID="cmbOperator" OnChange="setDataChanged(true);"/>
                <asp:DropDownList runat="server" CssselesClass="right" ID="cmbOperatorCount" OnChange="setDataChanged(true);"/>
            </div>
            <div>
                <asp:Label ID="lblValue" runat="server" CssClass="left required" Text="<%$ Resources:lblValueResrc %> "></asp:Label>
                <asp:TextBox ID="txtValue" runat="server" CssClass="right" OnChange="setDataChanged(true);"></asp:TextBox>
                <usc:CodeLookUp runat="server" CssClass="right" OnChange="setDataChanged(true);" ID="FileMarkValue" CodeTable="" ControlName="FileMarkValue" RMXType="code"/>
            </div>
        </div>
    </div>
    <asp:TextBox style="display:none" ID="FileMarkMappingId" runat="server"></asp:TextBox>
    <asp:HiddenField ID='hdnCloseWindow' runat="server" />
    </form>
</body>
</html>
