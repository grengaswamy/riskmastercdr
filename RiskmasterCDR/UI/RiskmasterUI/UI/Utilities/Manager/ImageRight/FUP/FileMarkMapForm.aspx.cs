﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.Common;

namespace Riskmaster.UI.UI.Utilities.Manager.ImageRight.FUP
{
    public partial class FileMarkMap : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hdnCloseWindow.Value = "False";
            bool bSuccess = false;
            if (!IsPostBack)
            {
                BindCmbTableName();
                string sMode = AppHelper.GetQueryStringValue("mode");
                if (string.Compare(sMode, "edit", true) == 0)
                {
                    XmlDocument xReturnDoc = new XmlDocument();
                    FileMarkMappingId.Text = AppHelper.GetQueryStringValue("Id");
                    string sCWSResponse = string.Empty;
                    int iCodeId = 0;
                    bool bReturnValue = CallCWS("FUPFile.GetFileMarks", GetLoadTemplate(), out sCWSResponse, false, false);
                    if (bReturnValue)
                    {
                        xReturnDoc.LoadXml(sCWSResponse);
                        if (string.Compare(xReturnDoc.SelectSingleNode("//MsgStatusCd").InnerText, "Success", true) == 0)
                        {
                            if (xReturnDoc.SelectSingleNode("//IRMarkId") != null)
                            {
                                txtFileMarkId.Text = xReturnDoc.SelectSingleNode("//IRMarkId").InnerText;
                            }
                            if (xReturnDoc.SelectSingleNode("//IRFileMarkName") != null)
                            {
                                txtFileMarkName.Text = xReturnDoc.SelectSingleNode("//IRFileMarkName").InnerText;
                            }
                            if (xReturnDoc.SelectSingleNode("//Count") != null)
                            {
                                chkCount.Checked = Conversion.CastToType<bool>(xReturnDoc.SelectSingleNode("//Count").InnerText, out bSuccess);
                            }
                            if (xReturnDoc.SelectSingleNode("//TableName") != null)
                            {
                                if (!string.IsNullOrEmpty(xReturnDoc.SelectSingleNode("//TableName").InnerText))
                                {
                                    cmbTableName.SelectedValue = xReturnDoc.SelectSingleNode("//TableName").InnerText;
                                    BindOtherDropdowns();
                                }
                            }
                            if (xReturnDoc.SelectSingleNode("//FieldName") != null)
                            {
                                if (!string.IsNullOrEmpty(xReturnDoc.SelectSingleNode("//FieldName").InnerText))
                                {
                                    if (chkCount.Checked)
                                    {
                                        cmbOperatorCount.SelectedValue = xReturnDoc.SelectSingleNode("//FieldName").InnerText;
                                    }
                                    else
                                    {
                                        cmbOperator.SelectedValue = xReturnDoc.SelectSingleNode("//FieldName").InnerText;
                                    }
                                }
                            }
                            if (xReturnDoc.SelectSingleNode("//Operator") != null)
                            {
                                cmbOperator.SelectedValue = xReturnDoc.SelectSingleNode("//Operator").InnerText;
                            }
                            if (xReturnDoc.SelectSingleNode("//SysTableName") != null)
                            {
                                hdnCodeTableName.Value = xReturnDoc.SelectSingleNode("//SysTableName").InnerText;
                                if (!string.IsNullOrEmpty(hdnCodeTableName.Value) && string.Compare(hdnCodeTableName.Value,"0")!=0)
                                {
                                    hdnFieldType.Value = "code";
                                    if (xReturnDoc.SelectSingleNode("//Map")!=null && xReturnDoc.SelectSingleNode("//Map").Attributes["codeid"]!=null)
                                    {
                                        iCodeId = Conversion.CastToType<int>(xReturnDoc.SelectSingleNode("//Map").Attributes["codeid"].Value, out bSuccess);
                                    }
                                    else
                                    {
                                        iCodeId = 0;
                                    }
                                }
                                else
                                {
                                    hdnFieldType.Value = "text";
                                }
                            }
                            if (xReturnDoc.SelectSingleNode("//Value") != null)
                            {
                                if (string.Compare(hdnFieldType.Value, "text", true)==0)
                                {
                                    txtValue.Text = xReturnDoc.SelectSingleNode("//Value").InnerText;
                                }
                                else
                                {
                                    FileMarkValue.CodeText = xReturnDoc.SelectSingleNode("//Value").InnerText;
                                    FileMarkValue.CodeId = iCodeId.ToString();
                                }
                            }
                        }
                    }
                }
                else
                {
                    BindOtherDropdowns();
                }
            }
            
        }

        protected void OnConditionChange(object sender, EventArgs e)
        {
            BindOtherDropdowns();
        }

        protected void OnTableChange(object sender, EventArgs e)
        {
            FileMarkValue.CodeText = string.Empty;
            FileMarkValue.CodeId = "0";
            BindOtherDropdowns();
        }

        protected void OnFieldChange(object sender, EventArgs e)
        {
            FileMarkValue.CodeText = string.Empty;
            FileMarkValue.CodeId = "0";
            if (!chkCount.Checked)
            {
                SetHiddenVaraibles();
            }
        }

        private void BindCmbTableName()
        {
            cmbTableName.Items.Clear();
            cmbTableName.Items.Add("Claim");
            cmbTableName.Items.Add("Claimant");
            cmbTableName.Items.Add("Litigation");
            cmbTableName.Items.Add("Subrogation");
            cmbTableName.Items.Add("Arbitration");
            cmbTableName.Items.Add("Claim Supp");
            cmbTableName.Items.Add("Claimant Supp");
            cmbTableName.Items.Add("Litigation Supp");
        }

        private void BindOtherDropdowns()
        {
            cmbFieldName.Items.Clear();

            BindForCount();
            BindForValue();
            
        }

        private void BindForValue()
        {
            bool bSupp = false;
            string sSuppName = string.Empty;
            switch (cmbTableName.SelectedValue.ToUpper())
            {
                case "CLAIM":
                    cmbFieldName.Items.Add("Claim Status");
                    cmbFieldName.Items.Add("Claim Type");
                    break;
                case "CLAIMANT":
                    cmbFieldName.Items.Add("Claimant Type");
                    cmbFieldName.Items.Add("Name Type");
                    break;
                case "LITIGATION":
                    cmbFieldName.Items.Add("Litigation Type");
                    cmbFieldName.Items.Add("Litigation Status");
                    break;
                case "SUBROGATION":
                    cmbFieldName.Items.Add("Subrogation Type");
                    cmbFieldName.Items.Add("Subrogation Status");
                    break;
                case "ARBITRATION":
                    cmbFieldName.Items.Add("Arbitration Type");
                    cmbFieldName.Items.Add("Arbitration Status");
                    cmbFieldName.Items.Add("Arbitration Party");
                    break;
                case "CLAIM SUPP":
                    bSupp = true;
                    sSuppName = "CLAIM_SUPP";
                    break;
                case "CLAIMANT SUPP":
                    bSupp = true;
                    sSuppName = "CLAIMANT_SUPP";
                    break;
                case "LITIGATION SUPP":
                    bSupp = true;
                    sSuppName = "LITIGATION_SUPP";
                    break;
                default:
                    break;
            }

            if (bSupp)
            {
                GetAndBindSuppValues(sSuppName);
            }

            cmbOperator.Items.Clear();

            cmbOperator.Items.Add("=");
            cmbOperator.Items.Add("<>");
            cmbOperator.Items.Add("IS NULL");
            cmbOperator.Items.Add("IS NOT NULL");

            SetHiddenVaraibles();
        }
        private void BindForCount()
        {
            hdnFieldType.Value = "text";
            cmbOperatorCount.Items.Clear();

            cmbOperatorCount.Items.Add("=");
            cmbOperatorCount.Items.Add("<>");
            cmbOperatorCount.Items.Add(">");
            cmbOperatorCount.Items.Add("<");
            cmbOperatorCount.Items.Add(">=");
            cmbOperatorCount.Items.Add("<=");
        }

        private void SetHiddenVaraibles()
        {
            switch (cmbFieldName.SelectedValue.ToUpper())
            {
                case "CLAIM STATUS":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "CLAIM_STATUS";
                    break;
                case "CLAIM TYPE":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "CLAIM_TYPE";
                    break;
                case "CLAIMANT TYPE":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "CLAIMANT_TYPE";
                    break;
                case "NAME TYPE":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "ENTITY_NAME_TYPE";
                    break;
                case "LITIGATION TYPE":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "LIT_TYPE";
                    break;
                case "LITIGATION STATUS":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "LITIGATION_STATUS";
                    break;
                case "SUBROGATION TYPE":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "SUBRO_TYPE";
                    break;
                case "SUBROGATION STATUS":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "SUBRO_STATUS";
                    break;
                case "ARBITRATION TYPE":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "ARBITRATION_TYPE";
                    break;
                case "ARBITRATION STATUS":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "ARBITRATION_STATUS";
                    break;
                case "ARBITRATION PARTY":
                    hdnFieldType.Value = "code";
                    hdnCodeTableName.Value = "ARBITRATION_PARTY";
                    break;
                default:
                    hdnFieldType.Value = "text";
                    GetSuppFieldType();
                    break;
            }
        }

        private void GetAndBindSuppValues(string p_sSuppTableName)
        {
            XmlDocument xReturnDoc = new XmlDocument();
            string sCWSResponse = string.Empty;
            bool bReturnValue = CallCWS("UserDataMapping.GetSuppValue", GetSuppTemplate(p_sSuppTableName, string.Empty), out sCWSResponse, false, false);
            if (bReturnValue)
            {
                xReturnDoc.LoadXml(sCWSResponse);
                if (string.Compare(xReturnDoc.SelectSingleNode("//MsgStatusCd").InnerText, "Success", true) == 0)
                {
                    foreach (XmlNode xNode in xReturnDoc.SelectNodes("//FieldName"))
                    {
                        if (!string.IsNullOrEmpty(xNode.InnerText))
                        {
                            cmbFieldName.Items.Add(xNode.InnerText);
                        }
                    }
                }
            }
        }

        private void GetSuppFieldType()
        {
            bool bSupp = false;
            string sSuppName = string.Empty;
            string sFieldName = string.Empty;
            XmlDocument xReturnDoc = new XmlDocument();
            XmlNode xNode = null;
            string sCWSResponse = string.Empty;
            switch (cmbTableName.SelectedValue.ToUpper())
            {
                case "CLAIM SUPP":
                    bSupp = true;
                    sSuppName = "CLAIM_SUPP";
                    break;
                case "CLAIMANT SUPP":
                    bSupp = true;
                    sSuppName = "CLAIMANT_SUPP";
                    break;
                default:
                    hdnFieldType.Value = "text";
                    break;
            }

            if (bSupp)
            {
                sFieldName = cmbFieldName.SelectedValue;

                bool bReturnValue = CallCWS("FUPFile.GetSuppType", GetSuppTemplate(sSuppName, sFieldName), out sCWSResponse, false, false);
                if (bReturnValue)
                {
                    xReturnDoc.LoadXml(sCWSResponse);
                    if (string.Compare(xReturnDoc.SelectSingleNode("//MsgStatusCd").InnerText, "Success", true) == 0)
                    {
                        xNode = xReturnDoc.SelectSingleNode("//TableName");
                        if (xNode!=null)
                        {
                            if (!string.IsNullOrEmpty(xNode.InnerText))
                            {
                                hdnCodeTableName.Value = xNode.InnerText;
                                hdnFieldType.Value = "code";
                            }
                        }
                    }
                }
            }
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            string sCWSResponse = string.Empty;
            XmlDocument xReturnDoc = new XmlDocument();
            bool bReturnValue = CallCWS("FUPFile.SaveFileMarks", GetSaveTemplate(), out sCWSResponse, false, false);
            if (bReturnValue)
            {
                xReturnDoc.LoadXml(sCWSResponse);
                if (string.Compare(xReturnDoc.SelectSingleNode("//MsgStatusCd").InnerText, "Success", true) == 0)
                {
                    hdnCloseWindow.Value = "True";
                }
            }
        }

        private XElement GetSuppTemplate(string p_sSuppTable, string p_sFieldName)
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Supp>
                        <SuppTable>" + p_sSuppTable + @"</SuppTable>
                        <FieldName>" + p_sFieldName + @"</FieldName>
                    </Supp>
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        private XElement GetSaveTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Mapping>
                        <IRFileMarkMappingId>" + FileMarkMappingId.Text + @"</IRFileMarkMappingId>
                        <IRMarkId>" + txtFileMarkId.Text + @"</IRMarkId>
                        <IRFileMarkName>" + txtFileMarkName.Text + @"</IRFileMarkName>
                        <Count>" + chkCount.Checked + @"</Count>
                        <TableName>" + cmbTableName.SelectedValue + @"</TableName>
                        <FieldName>" + cmbFieldName.SelectedValue + @"</FieldName>
                        <Operator>" + Server.HtmlEncode(chkCount.Checked ? cmbOperatorCount.SelectedValue : cmbOperator.SelectedValue) + @"</Operator>
                        <Value>" + (chkCount.Checked ? txtValue.Text :
                                 (string.Compare(hdnFieldType.Value, "code", true) == 0 ? FileMarkValue.CodeId : txtValue.Text)) + @"</Value>
                        <SysTableName>" + ((chkCount.Checked || string.Compare(hdnFieldType.Value, "code", true) != 0) ? "0" : hdnCodeTableName.Value) + @"</SysTableName>
                    </Mapping>
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        private XElement GetLoadTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Data>
                        <IRFileMarkMappingId>" + FileMarkMappingId.Text + @"</IRFileMarkMappingId>
                    </Data>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
    }
}