﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;

namespace Riskmaster.UI.UI.Utilities.Manager.ImageRight.FUP
{
    public partial class FUPFileLayout : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void grdFUPFileLayout_OnNeedDataSource(object sender, EventArgs e)
        {
            string sReturnMsg = string.Empty;
            bool bReturnValue = false;
            string sChangedValue = string.Empty;
            Regex oRegex = null;
            XmlDocument xDoc = new XmlDocument();
            bReturnValue = CallCWS("FUPFile.GetFupFileLayout", GetDefaultTemplate(string.Empty), out sReturnMsg, false, false);
            if (bReturnValue)
            {
                DataSet oMapDataSet = new DataSet();
                DataTable oMapDataTable = oMapDataSet.Tables.Add("FupFileLayout");
                DataRow oMapDataRow = null;

                oMapDataTable.Columns.Add("FupFileLayoutId", typeof(string));
                oMapDataTable.Columns.Add("FieldName", typeof(string));
                oMapDataTable.Columns.Add("StartPosition", typeof(string));
                oMapDataTable.Columns.Add("Length", typeof(string));

                sChangedValue = hdnChangedValues.Value;

                xDoc.LoadXml(sReturnMsg);
                foreach (XmlNode xNode in xDoc.SelectNodes("//FieldName"))
                {
                    oMapDataRow = oMapDataTable.NewRow();
                    oMapDataRow["FieldName"] = xNode.InnerText;

                    if (xNode.Attributes["FileLayoutId"] != null)
                    {
                        oMapDataRow["FupFileLayoutId"] = xNode.Attributes["FileLayoutId"].Value;
                    }

                    if (xNode.Attributes["StartPosition"] != null)
                    {
                        oMapDataRow["StartPosition"] = xNode.Attributes["StartPosition"].Value;
                    }

                    if (xNode.Attributes["StartPosition"] != null)
                    {
                        oRegex = new Regex(xNode.InnerText + "(.*?)~");
                        Match oMatch = oRegex.Match(sChangedValue);
                        if (string.IsNullOrEmpty(oMatch.Value))
                        {
                            oMapDataRow["Length"] = xNode.Attributes["Length"].Value;
                        }
                        else
                        {
                            if (oMatch.Value.Split(':').Length == 2)
                            {
                                oMapDataRow["Length"] = oMatch.Value.Split(':')[1].Replace("~", string.Empty);
                            }
                        }
                    }
                    

                    oMapDataTable.Rows.Add(oMapDataRow);
                }

                grdFUPFileLayout.DataSource = oMapDataSet;

                oMapDataTable.Dispose();
                oMapDataTable.Dispose();
            }
        }

        protected void grdFUPFileLayout_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && e.Item.IsInEditMode)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
                //Hides the Update button for each edit form
                dataItem["EditCommandColumn"].Controls[0].Visible = false;
            }
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            string sFileLayoutId = string.Empty;
            string sLength = string.Empty;
            string sReturnMsg = string.Empty;
            string sFieldName = string.Empty;
            bool bReturnValue = false;
            StringBuilder sbTemplate = new StringBuilder();
            foreach (GridDataItem item in grdFUPFileLayout.MasterTableView.Items)
            {
                if (item.Edit)
                {
                    sFileLayoutId = ((TextBox)item["FupFileLayoutId"].Controls[0]).Text;
                    sFieldName = ((TextBox)item["FieldName"].Controls[0]).Text;
                    sLength = ((RadNumericTextBox)item["Length"].Controls[0]).Text;

                    sbTemplate.Append(string.Format("<FieldName FileLayoutId=\"{0}\" Length=\"{1}\" >{2}</FieldName> ", sFileLayoutId, sLength, sFieldName));
                }
            }

            bReturnValue = CallCWS("FUPFile.SaveFupFileLayout", GetDefaultTemplate(sbTemplate.ToString()), out sReturnMsg, false, false);
            if (bReturnValue)
            {
                hdnChangedValues.Value = string.Empty;
                grdFUPFileLayout.MasterTableView.ClearEditItems();
                grdFUPFileLayout.Rebind();
            }
        }

        private XElement GetDefaultTemplate(string p_sData)
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization />
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Data>" + p_sData + @"</Data>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
    }
}