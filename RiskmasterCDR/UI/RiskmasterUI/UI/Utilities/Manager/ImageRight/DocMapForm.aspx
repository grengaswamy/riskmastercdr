﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocMapForm.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ImageRight.DocMapForm" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="usc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<style type="text/css">
    .left{
      width: 49%;
      float: left;
      margin-top:5px;
    }
    .right{
      width: 100%;
      margin-top:5px;
    }
    .clear
    {
        clear:both;
    }
    </style>
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type='text/javascript'>
        $('document').ready(function () {
            if ($.trim($('#hdnCloseWindow').val()) === 'True') {
                if (window.opener.$('#frmData')) {
                    window.opener.$('#frmData').submit();
                }
                window.close();
            }

            if ($('#chkDescription').is(':checked')) {
                $('#txtIRDesc').prop('disabled', 'disabled');
            }
            else if ($('#chkDocName').is(':checked')) {
                $('#txtIRDesc').prop('disabled', 'disabled');
            }

            $('#btnSave').click(function () {
                if (!Validate()) {
                    alert('Required Field Missing');
                    return false;
                }
            });

            $('#btnCancel').click(function () {
                window.close();
            });

            $('#chkDescription').change(function () {
                if ($('#chkDescription').is(':checked')) {
                    $('#txtIRDesc').prop('disabled', 'disabled');
                    $('#chkDocName').prop('checked', false);
                }
                else {
                    $('#txtIRDesc').removeProp('disabled');
                }
            });
            $('#chkDocName').change(function () {
                if ($('#chkDocName').is(':checked')) {
                    $('#txtIRDesc').prop('disabled', 'disabled');
                    $('#chkDescription').prop('checked', false);
                }
                else {
                    $('#txtIRDesc').removeProp('disabled');
                }
            });
            $('#txtIRDesc').change(function () {
                var convertedText = "";
                var initialText = $('#txtIRDesc').val();

                $('#txtIRDesc').val($('#txtIRDesc').val().replace(/(\r\n|\n|\r|\\|\/|\*|\?|\:|<|>|\||")/gm, "~"));

                convertedText = $('#txtIRDesc').val();

                if (initialText !== convertedText) {
                    alert("Following characters are not allowed - Newline, \\, /, *, ?, |, :, <, >, \"");
                }

            });
        });

        function Validate() {
            var bValid = true;
            $('.required').next('input[type=text]').each(function () {
                if ($.trim($(this).val()) === '') {
                    bValid = false;
                }
            });

            return bValid;
        }
    </script>
    <title></title>
</head>
<body>
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
    <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_savemapping_active.png" class="bold" ToolTip="<%$ Resources:btnSaveResrc %>"
                        runat="server" OnClick="OnBtnSaveClick" />
    <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:btnCancelResrc %>"
                        runat="server" />

    <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="99%" Text="<%$ Resources:lblDocumentMappingResrc %>"></asp:Label>
        <div>
            <asp:Label ID="lblDocClass" runat="server" CssClass="left" Text="<%$ Resources:lblRMADocClassResrc %>"></asp:Label>
            <usc:CodeLookUp runat="server" CssClass="right" OnChange="setDataChanged(true);" ID="rmADocClass" CodeTable="DOCUMENT_CLASS" ControlName="rmADocClass" RMXType="code" rmxref="Instance/Document/Mappings/Map/RmaDocClass"/>
        </div>
        <div>
            <asp:Label runat="server" CssClass="left required" Text="<%$ Resources:lblRMADocTypeResrc %>"></asp:Label>
            <usc:CodeLookUp runat="server" CssClass="right" OnChange="setDataChanged(true);" ID="rmADocType" CodeTable="DOCUMENT_TYPE" ControlName="rmADocType" RMXType="code" rmxref="Instance/Document/Mappings/Map/RmaDocType"/>
        </div>
        <div>
            <asp:Label runat="server" CssClass="left" Text="<%$ Resources:lblRMADocCategoryResrc %>"></asp:Label>
            <usc:CodeLookUp runat="server" CssClass="right" OnChange="setDataChanged(true);" ID="rmADocCat" CodeTable="DOCUMENT_CATEGORY" ControlName="rmADocCat" RMXType="code" rmxref="Instance/Document/Mappings/Map/RmaDocCat" />
        </div>
        <div>
            <asp:Label runat="server" CssClass="left required" Text="<%$ Resources:lblIRDocTypeResrc %>"></asp:Label>
            <usc:CodeLookUp runat="server" CssClass="right" OnChange="setDataChanged(true);" ID="IRDocType" CodeTable="IR_DOCUMENT_TYPE" ControlName="IRDocType" RMXType="code" rmxref="Instance/Document/Mappings/Map/IrDocType"/>
        </div>
        <div>
            <asp:Label runat="server" CssClass="left required" Text="<%$ Resources:lblIRFolderResrc %>"></asp:Label>
            <usc:CodeLookUp runat="server" CssClass="right" OnChange="setDataChanged(true);" ID="IRFolder" CodeTable="IR_FOLDER_TYPE" ControlName="IRFolder" RMXType="code" rmxref="Instance/Document/Mappings/Map/IrFolder"/>
        </div>
        <div>
            <asp:Label ID="Label1" runat="server" CssClass="left" Text="<%$ Resources:lblDocTypeAsDescriptionResrc %> "></asp:Label>
            <asp:CheckBox runat="server" id="chkDescription" CssClass="right" rmxref="Instance/Document/Mappings/Map/IrDescChk"></asp:CheckBox>
        </div>
        <div class="clear" />
        <div>
            <asp:Label ID="Label2" runat="server" CssClass="left" Text="<%$ Resources:lblDocNameAsDescriptionResrc %> "></asp:Label>
            <asp:CheckBox runat="server" id="chkDocName" CssClass="right" rmxref="Instance/Document/Mappings/Map/IrDescChkDocName"></asp:CheckBox>
        </div>
        <div class="clear" />
        <div>
            <asp:Label runat="server" CssClass="left" Text="<%$ Resources:lblDocMapFormDescriptionResrc %> "></asp:Label>
            <asp:TextBox runat="server" id="txtIRDesc" Rows="3" Columns="30" TextMode="MultiLine" rmxref="Instance/Document/Mappings/Map/IrDesc"></asp:TextBox>
        </div>
        
    </div>
    <asp:HiddenField ID='hdnCloseWindow' runat="server" />
    <asp:TextBox style="display:none" ID='IRDocMapId' runat="server" rmxref="Instance/Document/Mappings/Map/IrDocMapId"></asp:TextBox>
    </form>
</body>
</html>
