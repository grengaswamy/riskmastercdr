﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckStubMapping.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.CheckStubMapping" ValidateRequest = "false" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Check Stub Text Configuration</title>
     <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
     <script type="text/javascript" language="javascript">
     function SelectRow() {
        var gridElementsRadio = document.getElementsByName('MyRadioButton');
         //igupta3 Mits:33301
        if (gridElementsRadio.length == 0 && !window.ie) {
            var num = 0;
            var gridElementsnew = new Array();
            try {
                var input = document.getElementsByTagName('input');
                for (i = 0; i < input.length; i++) {
                    if (input[i].id == "MyRadioButton") {
                        //gridElements[num] = document.getElementById('MyRadioButton');
                        gridElementsnew[num] = input[i];
                        num++;
                    }
                }
                gridElementsRadio = gridElementsnew;
            }
            catch (e) {
            }
        }
        if (gridElementsRadio != null) {
            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    selectedValue = gridElementsRadio[i].value;
                }
            }
        }
        document.getElementById('RowId').value = selectedValue;
        document.getElementById('Edit_Record').value = 'Edit';
        document.getElementById('EditFlag').value = 'EDIT';
    }
    function ValidateRecord() {
        //msampathkuma RMA-11406
        var t = document.getElementById("States_multicode").length;
        var p = document.getElementById("TransTypeCode_multicode").length;

        if (document.getElementById('txt_CheckText').value != "" && t != 0 && p != 0) {
            return true;
        }
        else {
            alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
            return false;
        }
        

    }
    function ValidateRecordForDetetion() {
        if (document.getElementById('RowId').value == "") {
            alert("Please select a row to delete.");
            return false;
        }
        return true;

    }
    function ValidateRecordForEdit() {
        if (document.getElementById('RowId').value == "") {
            alert("Please select a row to edit.");
            return false;
        }
        return true;
    }
    //nsachdeva2 - MITS:28267 - 6/1/2012
    function limitText() 
    {
       if (document.getElementById('txt_CheckText').value != "") 
           {
               if (document.getElementById('txt_CheckText').value.length > 800) {
                   alert("You have reached the maximum allowed length of check stub text.");
                    document.getElementById('txt_CheckText').value = document.getElementById('txt_CheckText').value.substring(0, 799);
                } 
            }
    }
    //End MITS:28267 
    </script>
</head>
<body class="10pt" onload="parent.MDIScreenLoaded();SelectAfterPostback('CheckStubMappingsGrid');">
    <form id="frmData" runat="server" >
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
        <div runat="server" class="completerow" id="div_States" xmlns="">
            <b><asp:label runat="server" class="label" id="lbl_States" Text="Jurisdiction" /></b>
            <span class="formw">
              <uc:MultiCode runat="server" ID="States" CodeTable="STATES" ControlName="States" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name='States']" RMXType="codelist" Tabindex="10" width="150" ShowCheckBox="true" />
            </span>
            <span><br />&nbsp;<br /></span>
       </div>
       
       <div runat="server" class="completerow" id="div_TransactionType" xmlns="">
            <b><asp:label runat="server" class="label" id="lbl_TransactionType" Text="Transaction Type" /></b>
            <span class="formw">
              <uc:MultiCode runat="server" ID="TransTypeCode" CodeTable="TRANS_TYPES" ControlName="TransTypeCode" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name='TransTypeCode']" RMXType="codelist" Tabindex="10" width="50" ShowCheckBox="true" />
            </span>
            <span><br />&nbsp;<br /></span>
       </div>
       <div runat="server" class="completerow" id="div_CheckText" xmlns="">
           <u><b> <asp:label runat="server" class="label" id="lbl_CheckText" Text="Check Stub Text" /></b></u>
            <span class="formw">
                <asp:TextBox ID="txt_CheckText" runat="server"     
                RMXRef="/Instance/Document/form/group/displaycolumn/control[@name='CheckStubText']" 
                RMXType="text" Tabindex="10" Height="75px" TextMode="MultiLine" 
                Width="300px" MaxLength="254"></asp:TextBox>
            </span>
       </div>
       <div style="height:50px"></div>
       <div style='width:135px;float:left'><span><br />&nbsp;<br /></span></div>
       <div>
       <span>
       <asp:Button runat="server" Text="Add New" class="button" 
                ID="Add_Record" onclick="Add_Record_Click" OnClientClick="return ValidateRecord();"/>
        </span>
        <span>
        <asp:Button runat="server" Text="Edit" class="button" ID="Edit_Record" onclick="Edit_Record_Click" OnClientClick="return ValidateRecordForEdit()"
                />
        </span>
        <span>
        <asp:Button runat="server" Text="Remove" class="button" ID="Remove_Record" onclick="Remove_Record_Click" OnClientClick="return ValidateRecordForDetetion()"
                />
        </span>
       </div>
    </div>
    
    <div style="width: 100%; height: 250; overflow: auto;">
        <dg:UserControlDataGrid runat="server" ID="CheckStubMappingsGrid" GridName="CheckStubMappingsGrid"
            HideButtons="New|Edit|Delete" Target="/Document/CheckStubTextMappingList"
            Ref="/Document/CheckStubTextMappingList"
            Unique_Id="RowId" ShowRadioButton="true" Height="350px" HideNodes="|RowId|"
            ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" Type="Grid" Width="770px" Align="true"
            RowDataParam="listrow" OnClick="SelectRow();KeepRowForEdit('CheckStubMappingsGrid');"/>
    </div>
    <asp:TextBox style="display:none" runat="server" id="RowId" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name='RowId']" RMXType="id" />
    <asp:TextBox style="display:none" runat="server" id="setGridDataChanged"  RMXType="id" Text="false"  />
    <asp:TextBox style="display:none" runat="server" id="CheckStubMappingsSelectedId"  RMXType="id" />
    <asp:TextBox style="display:none" runat="server" id="EditFlag"  Text="EDIT" />
    </form>
</body>
</html>
