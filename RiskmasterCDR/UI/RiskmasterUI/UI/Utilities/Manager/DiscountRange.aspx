﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiscountRange.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.Manager.DiscountRange" %>
 <%@ Register TagPrefix="uc4" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
 <%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl" TagPrefix="mc" %>
 <%--<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Add/Modify Discount Range...</title>
    <link rel="stylesheet" href="/RiskmasterUI/Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/system.css" type="text/css" />
    <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
    </style>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>

</head>
<body class="10pt" onload="CheckDiscountRangeWindow();">
    <form id="frmData" runat="server" method="post">
     <table>
        <tr>
            <td colspan="2">
                <uc4:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
    <div class="toolbardrift" id="toolbardrift" name="toolbardrift">
        <table class="toolbar" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td align="center" valign="middle" height="32">
                        <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="ImageButton1" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                            onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" OnClientClick="return ValidateFieldsforDiscountRange();"
                            OnClick="save_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    <br>
    <div class="msgheader" id="formtitle">
        Add/Modify Discount Range...</div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Range Parameters
                                </td>
                            </tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="RowId"></asp:TextBox></tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_LowerBound" Text="Lower Bound:" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <mc:CurrencyTextBox id="LowerBound" runat="server" MaxLength="11" onchange="setDataChanged(true);"></mc:CurrencyTextBox>                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_UpperBound" Text="Upper Bound:" />&nbsp;&nbsp;
                                </td>
                                <td>
                                <mc:CurrencyTextBox id="UpperBound" runat="server" MaxLength="11" onchange="setDataChanged(true);"></mc:CurrencyTextBox>   
                                <asp:Label id="lblUpperBoundMsg" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_Amount" Text="Amount:" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" ID="Amount" MaxLength="11"
                                        onchange="setDataChanged(true);"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Style="display: none" ID="FormMode"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Style="display: none" ID="FormValid"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Style="display: none" ID="UseVolumeDiscount"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                     <asp:TextBox Style="display: none" runat="server" ID="gridname"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="mode"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtvalidate" ></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtData" ></asp:TextBox>
   <%--                 <input type="text" name="" value="" id="SysViewType" style="display: none"><input
                        type="text" name="" value="" id="SysCmd" style="display: none"><input type="text"
                            name="" value="" id="SysCmdConfirmSave" style="display: none"><input type="text"
                                name="" value="" id="SysCmdQueue" style="display: none"><input type="text" name=""
                                    value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate"><input type="text"
                                        name="" value="" id="SysClassName" style="display: none" rmxforms:value=""><input
                                            type="text" name="" value="" id="SysSerializationConfig" style="display: none"><input
                                                type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId"><input
                                                    type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId"><input
                                                        type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="DiscountRange"><input
                                                            type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value=""><input
                                                                type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="DiscountRange"><input
                                                                    type="text" name="" value="LowerBound|Amount|" id="SysRequired" style="display: none">--%>
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
   <%-- <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />--%>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-5" id="SysWindowId" />
   <asp:TextBox ID="currencytype" Style="display:none" runat="server" ignoreget="true" ></asp:TextBox>
    </form>
</body>
</html>
