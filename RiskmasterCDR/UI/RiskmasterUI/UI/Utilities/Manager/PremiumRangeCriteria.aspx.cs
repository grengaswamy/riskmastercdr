﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class PremiumRangeCriteria : NonFDMBasePageCWS
    {
        string sCWSresponse = "";
        XElement XmlTemplate = null;
        Boolean bStatus = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                UseVolumeDiscount.Text = AppHelper.GetQueryStringValue("usevolumediscount");
                string strData = Session["discountrangelist"].ToString();
                XmlDocument xdata = new XmlDocument();
                if (PremiumRangeCriteriaGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    HdnSelectedIdForDeletion.Text = PremiumRangeCriteriaGridSelectedId.Text;
                    HdnListNameForDeletion.Text = "discountrangelist";
                    XmlTemplate = GetMessageTemplateForDeletion();
                    bStatus = CallCWS("DiscountParmsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                    PremiumRangeCriteriaGrid_RowDeletedFlag.Text = "false";

                    if (bStatus)
                    {
                        xdata.LoadXml(strData);
                        XmlNodeList nodes = xdata.SelectNodes("//discountrange");
                        foreach (XmlElement node in nodes)
                        {
                            if (node.ChildNodes[6].InnerText == HdnSelectedIdForDeletion.Text)
                            {
                                xdata.FirstChild.RemoveChild(node);
                            }
                        }
                        Session["discountrangelist"] = xdata.OuterXml;
                        PremiumRangeCriteriaGrid.BindData(xdata);
                    }
                }
                strData = Session["discountrangelist"].ToString();
                xdata = new XmlDocument();
                xdata.LoadXml(strData);
                PremiumRangeCriteriaGrid.BindData(xdata);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplateForDeletion()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("DiscountParmsAdaptor.Delete");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><DiscountRange>");
            sXml = sXml.Append("<discountrangerowid>");
            sXml = sXml.Append(HdnSelectedIdForDeletion.Text);
            sXml = sXml.Append("</discountrangerowid></DiscountRange></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
