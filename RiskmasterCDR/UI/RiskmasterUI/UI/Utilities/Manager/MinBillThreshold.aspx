﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MinBillThreshold.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.MinBillThreshold" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Add/Modify Threshold</title>
    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>
    <uc3:commontasks id="CommonTasks1" runat="server" />
    <%--vkumar258 - RMA-6037 - Starts --%>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
</head>
<body onload="CopyGridRowDataToPopup();">
    <form id="frmData" method="post" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
            <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                border="0" ID="save" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" OnClientClick="return ValidateFieldsforMinBillThreshold();"
                OnClick="save_Click" /></div>
    </div>
    <div class="msgheader" id="formtitle">
        Add/Modify Threshold</div>
    <table border="0">
        <tr>
            <td class="ctrlgroup" colspan="2">
                Minimum Billed Threshold
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_UseThreshold" Text="Use Threshold:" />
            </td>
            <td>
                <asp:CheckBox runat="server" Checked="true" ref="/Instance/Document//control[@name='UseThreshold']"
                    ID="UseThreshold" onchange="ApplyBool(this);" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_LOBcode" Text="Line of Business:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" name="LOBcode" ID="LOBcode" type="code" CodeTable="POLICY_LOB"
                    class="required" ref="/Instance/Document//control[@name='LOBcode']" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_Statecode" Text="State:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" name="Statecode" ID="Statecode" type="code" CodeTable="states"
                    class="required" ref="/Instance/Document//control[@name='Statecode']" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_Amount" Text="Amount:" class="required" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="Amount" ref="/Instance/Document//control[@name='Amount']" />
            </td>
        </tr>     
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_EffectiveDate" Text="Effective Date:" class="required" />&nbsp;&nbsp;
            </td>
            <td>
                <asp:TextBox runat="server" FormatAs="date" ID="EffectiveDate" RMXRef="/Instance/Document//control[@name='EffectiveDate']"
                    RMXType="date" TabIndex="2" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                    <%--vkumar258 - RMA-6037 - Starts --%>
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="EffectiveDatebtn" TabIndex="3" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "EffectiveDate",
				                ifFormat: "%m/%d/%Y",
				                button: "EffectiveDatebtn"
				            }
				            );
                </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#EffectiveDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                               // buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "3");
                        });
                    </script>
                    <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="label" id="lbl_ExpirationDate" text="Expiration Date:" />
                    &nbsp;&nbsp;
                </td>
                <td>
                    <asp:textbox runat="server" formatas="date" id="ExpirationDate" rmxref="/Instance/Document//control[@name='ExpirationDate']"
                        rmxtype="date" tabindex="4" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                    <%--vkumar258 - RMA-6037 - Starts --%>
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="ExpirationDatebtn" TabIndex="3" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "ExpirationDate",
				                ifFormat: "%m/%d/%Y",
				                button: "ExpirationDatebtn"
				            }
				            );
                </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#ExpirationDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "5");
                        });
                    </script>
                    <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
        </table>
        <asp:textbox style="display: none" runat="server" id="RowId" rmxref="/Instance/Document/Document/ExpCon/expconrowid" />
        <asp:textbox style="display: none" runat="server" id="selectedrowposition" />
        <asp:textbox style="display: none" runat="server" id="gridname" />
        <asp:textbox style="display: none" runat="server" id="txtPostBack" />
        <asp:textbox style="display: none" runat="server" id="validate" />
        <asp:textbox style="display: none" runat="server" id="mode" />
        <asp:textbox style="display: none" runat="server" id="txtData" />
    </form>
</body>
</html>
