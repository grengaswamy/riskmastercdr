﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Data.SqlClient;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor;
namespace Riskmaster.UI.Utilities.Manager
{
    public partial class TaxAndOffsetMapping : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("TaxMappingAdaptor.Get", null, out Data, true, true);
                if (bReturnStatus)
                {
                    ErrorControl1.errorDom = Data;
                    BindGridData(Data);
                }

            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            try
            {
                XElement xnode = null;
                string sfunctionname = (string)Xelement.XPathSelectElement("//Call/Function").Value;
                if (sfunctionname == "TaxMappingAdaptor.SaveStateTaxInfo")
                {
                    xnode = Xelement.XPathSelectElement("//StateTax[@name='STATE']");
                    xnode.Add(new XAttribute("value", "STATE"));
                    Xelement.Add(xnode);

                    IEnumerable<XElement> objStateTaxes = Xelement.XPathSelectElements("./Document/TaxMapping/TaxMapValues/StateTax");
                    foreach (XElement objTmpNode in objStateTaxes)
                    {
                        if (objTmpNode.Attribute("payeeid") == null)
                        {
                            objTmpNode.Remove();
                            continue;
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }


        private void BindGridData(string data)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(data);
                DataTable dtGridData = new DataTable();
                DataRow objRow = null;
                dtGridData.Columns.Add("CBId");
                dtGridData.Columns.Add("State");
                dtGridData.Columns.Add("payee");
                dtGridData.Columns.Add("transType");
                dtGridData.Columns.Add("taxPercent");
                XmlNodeList xmlNodeList = xmldoc.SelectNodes("//StateTax");
                foreach (XmlNode objNodes in xmlNodeList)
                {
                    objRow = dtGridData.NewRow();
                    string waitRowId = objNodes.InnerText.ToString();
                    string sState = objNodes.Attributes["stateId"].Value.ToString();
                    string sPayee = objNodes.Attributes["payee"].Value.ToString();
                    string sTransType = objNodes.Attributes["transType"].Value.ToString();
                    string sPercent = objNodes.Attributes["taxPercent"].Value.ToString();
                    objRow["CBId"] = waitRowId;
                    objRow["State"] = sState;
                    objRow["payee"] = sPayee;
                    objRow["transType"] = sTransType;
                    objRow["taxPercent"] = sPercent;
                    dtGridData.Rows.Add(objRow);
                }

                objRow = dtGridData.NewRow();
                dtGridData.Rows.Add(objRow);
                gvTaxMappingGrid.DataSource = dtGridData;
                gvTaxMappingGrid.DataBind();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void saveFederalTax_btn_Click(object sender, EventArgs e)
        {
            try
            {
                string Data = "";
                bool bReturnStatus = false;
                tbFederalTax_cid.Text = txtFederalTax_cid.Text;
                tbTaxPercentFed.Text = txtTaxPercentageFed.Text;
                tbFedValue.Text = "FEDERAL";
                txtFedTranstypeid.Text = lstTransTypeFed.SelectedValue.ToString();

                tbSOCIAL_SECURITY_cid.Text = txtSSNTax_cid.Text;
                tbTaxPercentSST.Text = txtTaxPercentageSSN.Text;
                tbSSTValue.Text = "SOCIAL_SECURITY";
                txtSSNTransTypeId.Text = lstTransTypeSSN.SelectedValue.ToString();

                tbMEDTax_cid.Text = txtMEDTax_cid.Text;
                tbTaxPercentageMED.Text = txtTaxPercentageMED.Text;
                tbMedicareValue.Text = "MEDICARE";
                txtMedTransTypeId.Text = lstTransTypeMED.SelectedValue.ToString();

                bReturnStatus = CallCWS("TaxMappingAdaptor.SaveFederalTaxInfo", null, out Data, true, false);
                if (bReturnStatus)
                    bReturnStatus = CallCWS("TaxMappingAdaptor.Get", null, out Data, true, true);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void saveOffsets_Click(object sender, EventArgs e)
        {
            string Data = "";
            try
            {
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("TaxMappingAdaptor.SaveOffsets", null, out Data, true, false);

                if (bReturnStatus)
                    bReturnStatus = CallCWS("TaxMappingAdaptor.Get", null, out Data, true, true);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void gvTaxMappingGrid_DataBound(object sender, EventArgs e)
        {
            gvTaxMappingGrid.Rows[gvTaxMappingGrid.Rows.Count - 1].CssClass = "hiderowcol";
        }

        protected void addStateMap_btn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            XElement XmlTemplate = null;
            try
            {
                XmlTemplate = GetMessageTemplate_Save();
                string Data = "";
                bool bReturnStatus = false;
                txtTransTypeStateId.Text = lstTransTypeState.SelectedValue.ToString();
                txtState.Text = lstStates.SelectedValue.ToString();
                bReturnStatus = CallCWS("TaxMappingAdaptor.SaveStateTaxInfo", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }

                    else
                    {
                        CallCWS("TaxMappingAdaptor.Get", null, out Data, true, true);
                        BindGridData(Data);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }



        protected void removeStateMapbtn_Click(object sender, EventArgs e)
        {
            try
            {
                XElement XmlTemplate = null;
                string sStateTax = "";
                XmlTemplate = GetMessageTemplate(sStateTax);
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("TaxMappingAdaptor.DeleteStateTaxInfo", XmlTemplate, out Data, true, false);
                bReturnStatus = CallCWS("TaxMappingAdaptor.Get", null, out Data, true, true);
                if (bReturnStatus)
                {
                    BindGridData(Data);

                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplate(string sStateTax)
        {
            XElement oTemplate = XElement.Parse(@"
                                                    <Message>
                                                      <Authorization></Authorization> 
                                                     <Call>
                                                      <Function>TaxMappingAdaptor.DeleteStateTaxInfo</Function> 
                                                      </Call>
                                                        <Document>
                                                            <TaxMapping>
                                                                    <TaxMapValues><FederalTax></FederalTax><StateTax></StateTax></TaxMapValues>
                                                                <DeleteMapping></DeleteMapping> 
                                                            </TaxMapping>
                                                        </Document>
                                                    </Message>");

            return oTemplate;

        }


        private XElement GetMessageTemplate_Save()
        {
            XElement oTemplate = XElement.Parse(@"
                                                    <Message>
                                                      <Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization> 
                                                     <Call>
                                                      <Function>TaxMappingAdaptor.SaveStateTaxInfo</Function> 
                                                      </Call>
                                                        <Document>
                                                            <TaxMapping>
                                                                    <TaxMapValues></TaxMapValues>
                                                             </TaxMapping>
                                                        </Document>
                                                    </Message>");
            return oTemplate;
        }
    }

}

