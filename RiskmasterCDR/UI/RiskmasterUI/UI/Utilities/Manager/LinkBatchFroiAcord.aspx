<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LinkBatchFroiAcord.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.LinkBatchFroiAcord" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
      <title>Link for Batch FROI / ACORD</title>
   
        <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
      <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css"/>
      <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
      <script type ="text/jscript" language="JavaScript" src="../../../Scripts/WaitDialog.js">          { var i; }</script>      
                <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
         <script language="javascript" type="text/javascript">
             function selectPDF(sDocId,sFilename) 
             {

                 var width = screen.availWidth - 60;
                 var height = screen.availHeight - 60;
                 window.location = "/RiskmasterUI/UI/Utilities/Manager/DownloadFROIAccord.aspx?DocumentId=" + sDocId + "&filename=" + sFilename;

             }
             function selectFroiAcord(objCheckBox) {
                
                var txtSelectedIds = document.getElementById('hdnFroiAcordSelectedId');
                
                 if (objCheckBox.checked) {
                     txtSelectedIds.value = txtSelectedIds.value + '|' + objCheckBox.value;
                 }
                 else {
                 if(txtSelectedIds.value.indexOf('|' + objCheckBox.value + '|') >= 0)
                     txtSelectedIds.value = txtSelectedIds.value.replace('|' + objCheckBox.value + '|', '|');
                 else if(txtSelectedIds.value.indexOf('|' + objCheckBox.value) >= 0)
                    txtSelectedIds.value = txtSelectedIds.value.replace('|' + objCheckBox.value, '');
                 }
             document.forms[0].hdnFroiAcordSelectedId.value = txtSelectedIds.value;
             
         }

         function selectFroiAcordDelete() 
         {
             
             var fId = document.forms[0].hdnFroiAcordSelectedId.value;
						
						if (fId=='')
						{
								alert("Please select FROIs/ACORDs to delete.");
								return false;
						}
				//document.forms[0].hdnFroiAcordSelectedDocId.value = '';
         }
         function SelectUnselectAll(bSelect) {
             var gridElementsCheckBox = document.getElementsByName('chkFroi');

             if (gridElementsCheckBox != null && gridElementsCheckBox.length > 0) {
                 document.forms[0].hdnFroiAcordSelectedId.value = '';
                 for (var i = 0; i < gridElementsCheckBox.length; i++) {
                     gridElementsCheckBox[i].checked = bSelect;
		     		if( bSelect )
		     		{
                     	document.forms[0].hdnFroiAcordSelectedId.value = document.forms[0].hdnFroiAcordSelectedId.value + '|' + gridElementsCheckBox[i].value;
		     		}
                 }
                 if (bSelect == false) {
                     document.forms[0].hdnFroiAcordSelectedId.value = '';
                 }
             }
             return false;

         }   		
	</script>  
</head>

<body class="" onload="parent.MDIScreenLoaded();">
  <form id="frmData"  method="post" runat="server">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
   <div class="msgheader" id="div_FroiTitle">
        <asp:Label ID="Froi_title" runat="server" Text="Print FROI" /><%--nzafar-mits-22448--%>
    </div>
    <table>
            <tr>
                <td>
                    <asp:gridview ID="gvFroiList" runat="server" 
                        AutoGenerateColumns="False" allowpaging="True"    allowsorting="True" onsorting="OnSortFroi"
                        CssClass="singleborder" 
                        Width="560px" onrowdatabound="gvFroiList_RowDataBound" OnPageIndexChanging="gvFroiList_PageChange" >
                        <HeaderStyle HorizontalAlign="Left" />
                        <Columns>  
                        <asp:TemplateField ControlStyle-Width="10%" HeaderText="Select FROI" Visible="true" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate >
                                      <%--<asp:CheckBox ID="chkFROI" runat="server" va Text='<%# DataBinder.Eval(Container, "DataItem.DocId")%>' onclick="selectFroiAcord(this);" ></asp:CheckBox>--%>
                                       <input  id="chkFroi" type="checkbox"  onclick="selectFroiAcord(this);" value= '<%# Eval("DocId") %>' />
                                    </ItemTemplate> 
                          </asp:TemplateField>          
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="S.No" Visible="true" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate >
                                      <asp:LinkButton ID="SNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo")%>' ></asp:LinkButton>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Doc Id" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate >
                                      <asp:Label ID="DocId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Batch Number" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                      <asp:Label ID="BatchId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BatchId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="File Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" sortexpression="Batch Id">
                                    <ItemTemplate>
                                      <asp:Label ID="FileName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FileName")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Number of Files" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="FileCount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FileCount")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="100%" HeaderText="Date Created" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" sortexpression="Date Created">
                                    <ItemTemplate>
                                      <asp:Label ID="DateCreated" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DateCreated")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  

                        </Columns>
                        </asp:gridview>
                    <asp:Label ID="lblMessageFroi" runat="server"></asp:Label>
                </td>   
                           
            </tr>

           
                    
              
    </table> 
       <div class="msgheader" id="div_AcordTitle">
        <asp:Label ID="Acord_title" runat="server" Text="Print ACORD" /><%--nzafar-mits-22448--%>
    </div>
    <table>
            <tr>
                <td>
                    <asp:gridview ID="gvAcordList" runat="server" 
                         AutoGenerateColumns="False"  allowpaging="True"    allowsorting="True" onsorting="OnSortAcord"
                        CssClass="singleborder" 
                        Width="560px" onrowdatabound="gvAcordList_RowDataBound" OnPageIndexChanging="gvAcordList_PageChange" >
                        <HeaderStyle HorizontalAlign="Left" />
                        <Columns>     
                         <asp:TemplateField ControlStyle-Width="10%" HeaderText="Select ACORD" Visible="true" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate >
                                      <%--<asp:CheckBox ID="chkACORD" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.chkACORD")%>' onclick="selectFroiAcord(this); return false;"></asp:CheckBox>--%>
                                       <input  id="chkACORD" type="checkbox"  onclick="selectFroiAcord(this);" value= '<%# Eval("DocId") %>' />
                                    </ItemTemplate> 
                          </asp:TemplateField>              
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="S.No" Visible="true" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate >
                                      <asp:LinkButton ID="SNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo")%>' ></asp:LinkButton>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Doc Id" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="DocId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Batch Number" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                      <asp:Label ID="BatchId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BatchId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="File Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White"  sortexpression="File Name">
                                    <ItemTemplate>
                                      <asp:Label ID="FileName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FileName")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Number of Files" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="FileCount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FileCount")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="100%" HeaderText="Date Created" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" sortexpression="Date Created">
                                    <ItemTemplate>
                                      <asp:Label ID="DateCreated" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DateCreated")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                        </Columns>
                        </asp:gridview>
                    <asp:Label ID="lblMessageAcord" runat="server"></asp:Label>
                </td>        
                      
            </tr>
            <%-- <tr>
                    <td>
                        <asp:Button id="btnDeleteAcord" Text="Delete Acord" CssClass="button" 
                            runat="server" onclick="btnDeleteAcord_Click" />
                    </td>
               </tr>--%>
           
    </table> 
        <table> 
                 <tr>
                        <td>
                        <asp:Button  name="SelectedAll" align="right" id ="btnSelectALL" Text="&nbsp;&nbsp;Select All &nbsp;&nbsp;"
                            class="button"  runat ="server" 
      onclientClick="return SelectUnselectAll(true)"></asp:Button>
                    </td>
                    <td>
                        <asp:Button  name="UnSelectedAll" align="right" id ="btnUnSelectALL" Text="&nbsp;&nbsp;UnSelect All &nbsp;&nbsp;"
                            class="button"  runat ="server" 
      onclientClick="return SelectUnselectAll(false)"></asp:Button>
                    </td>
                       <td>
                                <asp:Button id="btnRefresh" Text="Refresh" CssClass="button"  runat="server" 
                                    onclick="btnRefresh_Click" />
                        </td>
                        <td>
                        <asp:Button id="btnDeleteFroi" onclick="btnDeleteFroi_Click" CssClass="button" Text="Delete" runat="server" OnClientClick="return selectFroiAcordDelete()"/> 
                    </td>    
                 </tr>
          </table> 
      <asp:textbox ID="action" style="display:none" RMXRef="" runat="server" />
      <asp:textbox ID="filename" style="display:none" RMXRef="" runat="server" />
      <asp:textbox ID="DocumentId" style="display:none" RMXRef="" runat="server" />
      <asp:textbox ID="hdnFroiAcordSelectedId" style="display:none" RMXRef="" runat="server" />
      <asp:textbox ID="hdnFroiAcordSelectedDocId" style="display:none" RMXRef="" runat="server" />
      <asp:HiddenField ID="FolderId" runat="server" />
      <asp:HiddenField ID="hfolderId" runat="server" />
      <asp:HiddenField ID="hdocId" runat="server" />
      <asp:HiddenField ID="Psid" runat="server" />
      <asp:HiddenField ID="flag" runat="server" />
      <asp:HiddenField ID="hdnPageNumber" runat="server" />
      <asp:HiddenField ID="hdnSortExpression" runat="server" />
      <asp:HiddenField ID="AttachTableName" runat="server" />
      <asp:HiddenField ID="AttachRecordId" runat="server" />
      <asp:HiddenField ID="FormName" runat="server"/>
      <asp:HiddenField ID="Regarding" runat="server"/>
      <asp:HiddenField ID="NonMCMFormName" runat="server" />
  </form> 
 </body>
</html>
