<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ActivityLog.aspx.cs" EnableViewState ="true" Inherits="Riskmaster.UI.Utilities.Manager.ActivityLog" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Import Namespace ="System.Xml.Linq"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
 <head runat="server">
      <title>Activity Log</title>
      <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
      <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css"/>
      <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
      <script type ="text/jscript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;}</script> 
      <%--<script src="../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
      <script type="text/javascript" language="JavaScript" src="../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
      <script type="text/javascript" language="JavaScript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
      <script type="text/javascript" language="JavaScript" src="../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>     --%>
    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
     
      <script type="text/javascript">
				var ns, ie, ieversion;
				var browserName = navigator.appName;                   // detect browser 
				var browserVersion = navigator.appVersion;
				if (browserName == "Netscape")
				{
					ie=0;
					ns=1;
				}
				else		//Assume IE
				{
				ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
				ie=1;
				ns=0;
				}
				
				function onPageLoaded()
				{
					if (ie)
					{
						if ((eval("document.all.divForms")!=null) && (ieversion>=6))
						{
							eval("document.all.divForms").style.height=350;
						}
					}
					else
					{
						var o_divforms;
						o_divforms=document.getElementById("divForms");
						if (o_divforms!=null)
						{
							o_divforms.style.height=window.frames.innerHeight*0.70;
							o_divforms.style.width=window.frames.innerWidth*0.995;
						}
					}
				}
				
				function ClearForm()
				{
					document.forms[0].txtLastName.value="";
					document.forms[0].txtFirstName.value="";
					document.forms[0].txtFromDate.value="";
					document.forms[0].txtToDate.value="";
					document.forms[0].lstOperations.selectedIndex=0;
					document.forms[0].lstAccessType.selectedIndex=0;
					document.forms[0].lstUserLogin.selectedIndex=0;
				}
				
				function ValidateDateActivityLog()
                {
	                var sfromdate = document.forms[0].txtFromDate.value;
	                var stodate = document.forms[0].txtToDate.value;
	                var stxtFromDate = sfromdate.substring(6,10)+sfromdate.substring(0,2)+sfromdate.substring(3,5);
	                var stxtToDate = stodate.substring(6,10)+stodate.substring(0,2)+stodate.substring(3,5);
	                if(stxtToDate != "" && stxtFromDate > stxtToDate )
	                {
	                   alert("From Date must be less than To Date");
	                   document.forms[0].txtFromDate.value ="";
	                   document.forms[0].txtToDate.value ="";
	                }
				   <%--
				    var sfromdate = document.forms[0].txtFromDate.value;
				    var stodate = document.forms[0].txtToDate.value;

				    var inst = $.datepicker._getInst(sfromdate[0]);
				    var secinst = $.datepicker._getInst(stodate[0]);

				    fdate = inst.selectedDay;
				    fmonth = inst.selectedMonth;
				    fyear = inst.selectedYear;

				    secdate = secinst.selectedDay;
				    secmonth = secinst.selectedMonth;
				    secyear = secinst.selectedYear;

				    var effDate = new Date();
				    effDate.setFullYear(fyear, fmonth, fdate);

				    var expDate = new Date();
				    expDate.setFullYear(secyear, secmonth, secdate);

				    if (stodate != "" && effDate > expDate) {
				        //mbahl3  Mits  30940
				        alert("From Date must be less than To Date");
				        document.forms[0].txtFromDate.value = "";
				        document.forms[0].txtToDate.value = "";
				    }--%>

                }
                
                
	            function ShowPleaseWait()
	            {
	                 pleaseWait.Show();
	                 return true;
	            }
	  

	  </script>
 </head>
 
 <body onload="&#xA;javascript:onPageLoaded();parent.MDIScreenLoaded();">
     <form id="frmData" runat="server" style="width:99.8%" >
         <uc1:ErrorControl ID="ErrorControl1" runat="server" />
         <div id="maindiv" style="height:100%;width:99%;overflow:auto">
             <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
             
             <table width="100%" border="0" cellspacing="0" cellpadding="4">
                 <tr>
                    <td class="msgheader" colspan="4">Activity Log</td>
                 </tr>
             </table>
             
             <table cellspacing="0" cellpadding="0" border="0" width="100%">
                 <tr>
                     <td align="left" width="25%">Patient Last Name:</td>
                        <td width="40%">
                            <asp:TextBox runat="server" type="text" TabIndex ="1" rmxref="/Instance/Document/SearchParam/PatientLastName"  size="30" id="txtLastName" Width="70%"></asp:TextBox>
                        </td>
                     <td>&nbsp;&nbsp;</td>
                 </tr>
                 
                 <tr>
                     <td align="left" width="25%">Patient First Name:</td>
                     <td width="40%">
                        <asp:TextBox runat="server" type="text" TabIndex ="2" rmxref="/Instance/Document/SearchParam/PatientFirstName" value="" size="30" id="txtFirstName" Width="70%"></asp:TextBox>
                     </td>
                 </tr>
                 
                 <tr>
                     <td align="left" width="25%">Operation:</td>
                     <td width="40%">
                        <asp:DropDownList runat="server" TabIndex ="3" rmxref="/Instance/Document/SearchParam/Operation" EnableViewState ="true" type="combobox" ItemSetRef="/Instance/Document/SearchResults/operation" id="lstOperations" Width="72%">                
                        </asp:DropDownList>
                     </td>
                 </tr>
                 <tr><td></td></tr>
                 <tr>
                     <td align="left" width="25%">Activity Type:</td>
                     <td width="40%">
                         <asp:DropDownList runat="server" TabIndex ="4" rmxref="/Instance/Document/SearchParam/AccessType" EnableViewState ="true" type="combobox" ItemSetRef="/Instance/Document/SearchResults/accesstype" id="lstAccessType" Width="72%">                    
                         </asp:DropDownList>
                     </td>
                 </tr>
                 
                 <tr>
                     <td align="left" width="25%">From Date:</td>
                     <td width="40%">        
                         <asp:TextBox runat="server" type="text"  TabIndex ="5" rmxref="/Instance/Document/SearchParam/FromDate" value="" onchange="setDataChanged(true);ValidateDateActivityLog();" onblur="dateLostFocus(this.id);ValidateDateActivityLog();" id="txtFromDate" size="12" Width="70%"></asp:TextBox>
                         <%--<input type="button" class="DateLookupControl" id="btndate1" tabindex="8"/>
                         <script type="text/javascript">
					                                Zapatec.Calendar.setup(
									                            {
										                            inputField : "txtFromDate",
										                            ifFormat : "%m/%d/%Y",
										                            button : "btndate1"
									                            }
									                            );
                        </script>--%>
                         <script type="text/javascript">
                             $(function () {
                                 $("#txtFromDate").datepicker({
                                     showOn: "button",
                                     buttonImage: "../../../Images/calendar.gif",
                                     showOtherMonths: true,
                                     selectOtherMonths: true,
                                     changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "6");

                             });
                    </script>
                    </td>
                </tr>
                
                <tr>
                    <td align="left" width="25%">To Date:</td>         
                    <td width="40%">
                        <asp:TextBox runat="server" type="text" TabIndex ="7" rmxref="/Instance/Document/SearchParam/ToDate" value="" onchange="setDataChanged(true);ValidateDateActivityLog();" onblur="dateLostFocus(this.id);ValidateDateActivityLog();" id="txtToDate" size="12" Width="70%"></asp:TextBox>
                        <%--<input type="button" class="DateLookupControl" id="btndate2" tabindex="10"/>
                        <script type="text/javascript">
    							                            Zapatec.Calendar.setup(
									                            {
										                            inputField : "txtToDate",
										                            ifFormat : "%m/%d/%Y",
										                            button : "btndate2"
									                            }
									                            );
                        </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#txtToDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../../Images/calendar.gif",
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "8");
                            });
                    </script>
                    </td>
                </tr>
                
                <tr>
                    <td align="left" width="25%">User Login:</td>
                    <td width="40%">
                        <asp:DropDownList runat="server" TabIndex ="9" rmxref="/Instance/Document/SearchParam/UserLogin" id="lstUserLogin" type="combobox" EnableViewState ="true" ItemSetRef="/Instance/Document/SearchResults/userlogin" Width="72%">                              
                        </asp:DropDownList>
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;&nbsp;</td>
                </tr>
                
                <tr>
                    <td align="left" colspan="2">
                        <asp:Button ID="btnBeginSearch" runat="server" OnClientClick ="return ShowPleaseWait();" TabIndex="10" type="submit" name="" Text="Begin Search" class="button"  onclick="btnBeginSearch_Click1" />&nbsp;&nbsp;
                        <input type="button" name="reset" tabindex ="11" value="Clear" class="button" onclick="ClearForm()" />                       
                    </td>                   
                </tr>
                
                <tr>
                    <td>&nbsp;&nbsp;</td>
                </tr>   
                <tr>
                   <td>
                        <asp:TextBox runat="server" Visible="false" type="hidden" value="" Text="" id="txtFirstTime" rmxref="Instance/Document/SearchParam/FirstTime" />
                   </td>
                </tr>             
           </table>  
          <div id="divForms" class="divScroll" style="width:99.6%">
            <table width="97.5%" border="0" >
                <tr>
                    <td>
                        <asp:Label ID="lblPagerTop" runat="server"></asp:Label>
                    </td>
                    <td align="right">                        
                        <asp:Label ID="lblPageRangeTop" runat="server"></asp:Label>
                        <asp:LinkButton id="lnkFirstTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="First">First|</asp:LinkButton> 
                        <asp:LinkButton id="lnkPrevTop" runat="server" ForeColor="black" OnCommand = "LinkCommand_Click" CommandName="Prev">Previous</asp:LinkButton>
                        <asp:LinkButton id="lnkNextTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next">|Next</asp:LinkButton>
                        <asp:LinkButton id="lnkLastTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last">|Last</asp:LinkButton>
                    </td>
                </tr>
             </table>
             <table width="97.5%" border="0" >   
                <tr>
                <td>
                    <asp:GridView ID="gdActivityLog" Width ="100%" runat ="server" 
                        AutoGenerateColumns ="false" AlternatingRowStyle-CssClass="rowdark" 
                        CssClass="singleborder" HeaderStyle-CssClass="ctrlgroup" RowStyle-HorizontalAlign="Left">
                        <Columns>
                            <asp:BoundField HeaderText ="NAME" DataField ="name"/>
                            <asp:BoundField HeaderText ="OPERATION" DataField ="operation"/>
                            <asp:BoundField HeaderText ="USER LOGIN" DataField ="login"/>
                            <asp:BoundField HeaderText ="ACCESS TYPE" DataField ="accesstype"/>
                            <asp:BoundField HeaderText ="DATE" DataField ="date"/>
                            <asp:BoundField HeaderText ="TIME" DataField ="time"/>
                        </Columns>
                    </asp:GridView>
                </td>
                </tr>
             </table>            
        </div>
        <asp:HiddenField ID="hdCurrentPage" runat="server" />           
            <input type="hidden" name="" value="rmx-widget-handle-3" id="SysWindowId" />
            <asp:TextBox style="display:none" id="hdTotalRows" runat="server" rmxignoreset="true" rmxref="./SearchResults/search/@TotalRows"></asp:TextBox>
            <asp:TextBox style="display:none" id="hdPageSize" runat="server" rmxignoreset="true" rmxref="./SearchResults/search/@PageSize"></asp:TextBox>
            <asp:TextBox style="display:none" id="hdPageNumber" runat="server" rmxignoreset="true" rmxref="./SearchResults/search/@PageNumber"></asp:TextBox>
            <asp:TextBox style="display:none" id="hdTotalPages" runat="server" rmxignoreset="true" rmxref="./SearchResults/search/@TotalPages"></asp:TextBox>

       </div>
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
