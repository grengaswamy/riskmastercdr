﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CodeRelationships.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.CodeRelationships" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
      <title>Code Relationships</title>
   
   
       <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
    <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>
  
      <script type="text/javascript">
          function removeOption(selectName, id) {
              select = document.getElementById(selectName);
              selecttxtObject = document.getElementById('txtRelatedComponents');
              Ids = new Array();
              Names = new Array();

              for (var x = 0; x < select.options.length; x++) {
                  if (select.options[x].value != id) {
                      Ids[Ids.length] = select.options[x].value;
                      Names[Names.length] = select.options[x].text;

                  }
              }

              select.options.length = Ids.length;
              if (selectName == 'lstRelatedLossComponents') {
                  selecttxtObject.value = ""
              }
              for (var x = 0; x < select.options.length; x++) {
                  select.options[x].text = Names[x];
                  select.options[x].value = Ids[x];
                  if (selectName == 'lstRelatedLossComponents') {
                  if(selecttxtObject.value == "")
                  {
                      selecttxtObject.value =  Ids[x];
                  }
                  else
                  {
					    selecttxtObject.value = selecttxtObject.value + ',' + Ids[x];								
				    }               
                  }
              }
          }		
      function AddFilter(mode)
					{
						var optionRank;
						var optionObject;
						var element;    //rkapoor29 changes JIRA 6832
						var element1;   //rkapoor29 changes JIRA 6832
						var element2;   //rkapoor29 changes JIRA 6832
                         
						selectObject = document.getElementById('lstRelatedLossComponents');
						selecttxtObject = document.getElementById('txtRelatedComponents');
                        //rkapoor29 changes JIRA 6832
						element = document.getElementById("lstClaimType");  
						element1 = document.getElementById("lblCode1");   
						element2 = document.getElementById("lblCode2");
                        
						if (mode=="selected")
						{
                       
							//Add selected Available Values
						    select = document.getElementById('lstAvailableLossComponents');
						    //rkapoor29 changes to add alert box JIRA 6832 start 
						    if(element.selectedIndex == -1)
						    {
						        alert("Please select "+element1.innerText+" before selecting " + element2.innerText);
						    }
						    //rkapoor29 changes to add alert box JIRA 6832 end
						    else  // rkapoor29 added else block JIRA 6832  
						    {    
							for (var x = select.options.length - 1; x >= 0 ; x--)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
								    optionObject = new Option(select.options[x].text, select.options[x].value);
								    
									optionRank = selectObject.options.length;
									selectObject.options[optionRank] = optionObject;
						                if (selecttxtObject.value == "") {
                                        selecttxtObject.value =  select.options[x].value;
                                    }
						                else {
									    selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;								
									}								
									//remove from available list	
									removeOption('lstAvailableLossComponents', select.options[x].value);							
									setDataChanged(true);
								}								
							}
						}
						}
						else
						{
							//Add All Available Values
						    select = document.getElementById('lstAvailableLossComponents');
                            
						    //rkapoor29 changes to add alert box JIRA 6832 start 
						    if (element.selectedIndex == -1)
							{
						        alert("Please select " + element1.innerText + " before selecting " + element2.innerText);
						    }
						     //rkapoor29 changes to add alert box JIRA 6832 end
						    else   // rkapoor29 added else block JIRA 6832
								{
						        if (select.options.length > 0) {
						            for (var x = select.options.length - 1; x >= 0; x--) {
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
						                if (selecttxtObject.value == "") {
                                        selecttxtObject.value =  select.options[x].value;
                                    }
						                else {
									    selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;								
									}
									selectObject.options[optionRank]=optionObject;
									setDataChanged(true);
								}
								select.options.length=0;								
						        }
							}
						}						
						return false;
					}
					
					function RemoveFilter(mode)
					{
						var optionRank;
						var optionObject;
						selectObject = document.getElementById('lstAvailableLossComponents');
						selecttxtObject = document.getElementById('txtRelatedComponents');
						if (mode=="selected")
						{
							//Add selected Available Values
						    select = document.getElementById('lstRelatedLossComponents');
							for (var x = select.options.length-1; x >=0; x--)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
									
									//remove from available list	
									removeOption('lstRelatedLossComponents', select.options[x].value);							
									setDataChanged(true);
								}
							}
						}
						else
						{
							//ADD All Available Values
						    select = document.getElementById('lstRelatedLossComponents');
						   
							if (select.options.length > 0) {
							   
							    for (var x = select.options.length-1; x >= 0; x--) {
							        
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;


									selectObject.options[optionRank] = optionObject;

									selecttxtObject.value = "";
									
                                     setDataChanged(true);
                                    
                                     
								}
								select.options.length=0;
							}
						}						
						return false;
					}
					
				
                </script>
                   <style type="text/css">
          .style1
          {
              width: 3%;
          }
      </style>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <input type="hidden" value="" id="hiddenSelectedUser" />
    <input type="hidden" value="DiaryConfig" id="hTabName" />
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <div id="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
         <tr>
          <td align="center" valign="middle" height="32">         
          <asp:ImageButton runat="server" id="Save" ImageUrl="~/Images/tb_save_active.png" class="bold"
          ToolTip="Save" TabIndex = "8" onclick="Save_Click" />
          </td>
         </tr>
        </table>
        </div>
    <div class="msgheader" id="formtitle">
       Code Relationships
    </div>
    <table cellspacing="4" cellpadding="0" border="0"  width="99%" >
    <tr><td width="20%" align="left">Relationship Type</td> <td align="left">
       <asp:DropDownList type="combobox" runat="server" ID="cboRelationshipType" 
            rmxref="/Instance/Document/CodeRelationships/control[@name='cboRelationshipType']" 
            ItemSetRef="/Instance/Document/CodeRelationships/control[@name='cboRelationshipType']" 
            Width="500px" 
            onselectedindexchanged="cboRelationshipType_SelectedIndexChanged" AutoPostBack="true">
       </asp:DropDownList></td><td></td></tr>
       <tr><td width="20%" align="left">Relationship SubType</td> <td align="left">
       <asp:DropDownList type="combobox" runat="server" ID="cboRelationshipSubType" 
            rmxref="/Instance/Document/CodeRelationships/control[@name='cboRelationshipSubType']" 
            ItemSetRef="/Instance/Document/CodeRelationships/control[@name='cboRelationshipSubType']" 
            Width="500px" 
            onselectedindexchanged="cboRelationshipSubType_SelectedIndexChanged" AutoPostBack="true">
       </asp:DropDownList></td><td></td> 
      </tr>
        <tr><td width="20%" align="left" valign="top"><asp:Label ID="lblCode1" runat="server" />  <%--<asp:Button class="button" Width="150"  runat="server" type="buttonscript" OnClientClick="return selectCode('Claim_Type', 'Code_DESC', '','', '', '')" id="btnCode1" />--%></td> <td  align="left" colspan="2">
       <asp:ListBox  type="combobox" 
                          rmxref="/Instance/Document/CodeRelationships/control[@name='lstClaimType']" 
                          runat="server" 
                          
                          ID="lstClaimType"  TabIndex = "1"
                          size="10" Height="150px" style="margin-top: 0px" Width="500" AutoPostBack="true"                          
                onselectedindexchanged="lstClaimType_SelectedIndexChanged"></asp:ListBox></td></tr></table>
    <table >
                <tr><td width="20%" align="left" valign="top" colspan=3><asp:Label ID="lblCode2" runat="server" /> <%--<asp:Button UseSubmitBehavior="false" class="button" Width="150"  runat="server" type="buttonscript" id="btnCode2" OnClientClick=" return selectCode('LOSS_COMPONENT', 'CODE_DESC', '', '')"/>--%></td><//tr>
                 <tr>
                  <td width="5%" nowrap="true"><b>Available:</b></td>
                  <td class="style1"></td>
                  <td width="*" nowrap="true"><b>Related:</b></td>
                 </tr>
                 <tr>
                  <td width="5%" nowrap="true" valign="top">
                   <asp:ListBox   type="combobox" 
                          rmxref="/Instance/Document/CodeRelationships/control[@name='lstAvailableLossComponents']" 
                          rmxignoreset="true" runat="server" 
                          ID="lstAvailableLossComponents"  TabIndex = "1"
                          size="10" Height="150px" style="margin-top: 0px" Width="300px" 
                          SelectionMode="Multiple"></asp:ListBox>
                  <asp:HiddenField  ID ="hndPagetype" value="N"  runat="server" />  
                  <asp:TextBox runat ="server" rmxref="/Instance/Document/CodeRelationships/control[@name='txtRelatedComponents']"  ID="txtRelatedComponents" style="display:none"></asp:TextBox>                                    
                  <%--tanwar2 - mits 30910 - 01/04/2013 start--%>
                  <asp:TextBox runat ="server" rmxref="/Instance/Document/CodeRelationships/control[@name='txtRemoveComponents']"  ID="txtRemoveComponents" style="display:none" ></asp:TextBox>
                  <asp:ListBox 
                          type="combobox" rmxref="/Instance/Document/CodeRelationships/control[@name='lstAddComponents']"  
                          runat="server" ID="lstAddComponents" rmxignoreset="true"
                          style="margin-top: 0px; display:none;" Width="300px" EnableViewState="true" SelectionMode="Multiple"></asp:ListBox>
                  <%--tanwar2 - mits 30910 - 01/04/2013 end--%>
                  </td>
                 
                  <td valign="top" align="center" class="style1">
                  
                  <asp:Button runat="server" ID="btnAddAll" Text="&gt;&gt;" class="button" TabIndex = "2"
                          style="width:95" OnClientClick="return AddFilter('all');" Height="26px" 
                          Width="50px" UseSubmitBehavior="False"/><br/><br/>
                  <asp:Button runat="server" ID="btnAddSelected" Text="&gt;" class="button" TabIndex = "2"
                          style="width:95" OnClientClick="return AddFilter('selected');" Height="26px" 
                          Width="50px" UseSubmitBehavior="False"/><br/><br/>
                 <asp:Button runat="server" ID="btnRemoveSelected" Text="&lt;" class="button" TabIndex = "4"
                          style="width:95; margin-left: 0px;" 
                          OnClientClick="return RemoveFilter('selected');" Height="26px" 
                          Width="50px" UseSubmitBehavior="False"/><br/><br/>
                     <asp:Button runat="server" ID="btnRemoveAll" Text="&lt;&lt;" class="button" TabIndex = "4"
                          style="width:95; margin-left: 0px;" 
                          OnClientClick="return RemoveFilter('all');" Height="26px" 
                          Width="50px" UseSubmitBehavior="False"/><br/><br/>
                 
                  </td>
                  <td nowrap="true" width="*" valign="top">
                                  
                 <asp:ListBox 
                          type="combobox" rmxref="/Instance/Document/CodeRelationships/control[@name='lstRelatedLossComponents']"  
                          runat="server"   TabIndex = "6"
                           ID="lstRelatedLossComponents" rmxignoreset="true"  
                          size="10" Height="150px" style="margin-top: 0px" Width="300px" SelectionMode="Multiple"  EnableViewState="true"></asp:ListBox>
                   </td>         
                  </tr>
                </table>
   </form>
</body>
</html>
