﻿
/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
* 06/04/2014 | 33371   | ajohari2   | Added new page for Policy search data mapping 
**********************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class PolicySearchDataMapping : NonFDMBasePageCWS
    {
        //MITS 33371 ajohari2: Start
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            try
            {
                if (PolicySearchDataMappingGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = PolicySearchDataMappingSelectedId.Text;
                    XmlTemplate = GetMessageTemplate(selectedRowId);
                    CallCWS("PolicySearchDataSetupAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                    PolicySearchDataMappingGrid_RowDeletedFlag.Text = "false";
                }
                XmlTemplate = GetMessageTemplate();
                CallCWSFunctionBind("PolicySearchDataMappingAdaptor.Get", out sCWSresponse, XmlTemplate);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<PolicySearchDataMappingList><listhead>");
            sXml = sXml.Append("<PolSys>Policy System</PolSys>");
            sXml = sXml.Append("<AgentCode>Agent Code</AgentCode>");
            sXml = sXml.Append("<MCO>MCO</MCO>");
            sXml = sXml.Append("<PCO>PCO</PCO>");
            sXml = sXml.Append("<BusUnit>Business Unit</BusUnit>");
            sXml = sXml.Append("<BusSeg>Business Segment</BusSeg>");
            sXml = sXml.Append("<SecGroup>Security Group</SecGroup>");
            sXml = sXml.Append("<RowId>RowId</RowId>");
            sXml = sXml.Append("</listhead></PolicySearchDataMappingList>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
        private XElement GetMessageTemplate(string selectedRowId)
        {
            string strRowId = selectedRowId;

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><PolicySearchDataMapping>");
            sXml = sXml.Append("<control name='RowId'>");
            sXml = sXml.Append(strRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</PolicySearchDataMapping></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
        //MITS 33371 ajohari2: End

    }
}