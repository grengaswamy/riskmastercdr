﻿using System;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
///<Summary>
//Added for VACo Changes 
///
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class RateRangeCriteria : NonFDMBasePageCWS
    {
        string sCWSresponse = string.Empty;
        XElement XmlTemplate = null;
        Boolean bStatus = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string strData = Session["raterangelist"].ToString();
                XmlDocument xdata = new XmlDocument();
                if (RateRangeCriteriaGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    HdnSelectedIdForDeletion.Text = RateRangeCriteriaGridSelectedId.Text;
                    HdnListNameForDeletion.Text = "raterangelist";
                    XmlTemplate = GetMessageTemplateForDeletion();
                    bStatus = CallCWS("ExpRateParmsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                    RateRangeCriteriaGrid_RowDeletedFlag.Text = "false";

                    if (bStatus)
                    {
                        xdata.LoadXml(strData);
                        XmlNodeList nodes = xdata.SelectNodes("//raterange");
                        foreach (XmlElement node in nodes)
                        {
                            if (node.ChildNodes[8].InnerText == HdnSelectedIdForDeletion.Text)
                            {
                                xdata.FirstChild.RemoveChild(node);
                            }
                        }
                        Session["raterangelist"] = xdata.OuterXml;
                        RateRangeCriteriaGrid.BindData(xdata);
                    }
                }
                strData = Session["raterangelist"].ToString();
                xdata = new XmlDocument();
                xdata.LoadXml(strData);
                //Start: Neha Suresh Jain, 05/10/2010, MITS 20659, to add % on modifier/deductible 
                XmlNodeList mNodes = xdata.SelectNodes("//raterange/modifier");
                XmlNodeList dNodes = xdata.SelectNodes("//raterange/Deductible");
                
                if (Request.QueryString["FlatPer"] != null)
                {
                    if (Request.QueryString["FlatPer"].ToString() == "F")
                    {
                        foreach (XmlNode mxn in mNodes)
                        {
                            if (mxn.InnerXml != "")
                            {
                                if (mxn.InnerXml.IndexOf("%") >= 0)
                                {
                                    mxn.InnerXml = mxn.InnerXml.Replace("%", "");
                                    //mxn.InnerXml = mxn.InnerXml.Insert(0, "$"); //rupal:r8 multicurrency enh
                                }
                                //rupal:start,r8 multicurrency enh
                                /*
                                else if (mxn.InnerXml.IndexOf("$") < 0)
                                {
                                    mxn.InnerXml = mxn.InnerXml.Insert(0, "$");
                                }
                                //rupal:end
                                */
                            }
                        }
                        foreach (XmlNode dxn in dNodes)
                        {
                            if (dxn.InnerXml != "")
                            {
                                if (dxn.InnerXml.IndexOf("%") >= 0)
                                {
                                    dxn.InnerXml = dxn.InnerXml.Replace("%", "");
                                    //dxn.InnerXml = dxn.InnerXml.Insert(0, "$");//rupal:r8 multicurrency enh
                                }                                
                                //rupal:start,r8 multicurrency enh
                                /*
                                else if (dxn.InnerXml.IndexOf("$") < 0)
                                {
                                    dxn.InnerXml = dxn.InnerXml.Insert(0, "$");
                                }
                                //rupal:end
                                */
                            }
                        }
                    }
                    else
                    {
                        foreach (XmlNode mxn in mNodes)
                        {
                            if (mxn.InnerXml != "")
                            {
                                if (mxn.InnerXml.IndexOf("$") >= 0)
                                {
                                    mxn.InnerXml = mxn.InnerXml.Replace("$", "");
                                    mxn.InnerXml = mxn.InnerXml.Insert(mxn.InnerXml.Length, "%");
                                }
                                else if (mxn.InnerXml.IndexOf("%") < 0)
                                {
                                    mxn.InnerXml = mxn.InnerXml.Insert(mxn.InnerXml.Length, "%");
                                }                               
                            }
                        }
                        foreach (XmlNode dxn in dNodes)
                        {
                            if (dxn.InnerXml != "")
                            {
                                if (dxn.InnerXml.IndexOf("$") >= 0)
                                {
                                    dxn.InnerXml = dxn.InnerXml.Replace("$", "");
                                    dxn.InnerXml = dxn.InnerXml.Insert(dxn.InnerXml.Length, "%");
                                }
                                else if (dxn.InnerXml.IndexOf("%") < 0)
                                {
                                    dxn.InnerXml = dxn.InnerXml.Insert(dxn.InnerXml.Length, "%");
                                }                                
                            }
                        }
                    }
                }
                Session["raterangelist"] = xdata.InnerXml;
                //End: Neha Suresh Jain, 05/10/2010, MITS 20659
                RateRangeCriteriaGrid.BindData(xdata);                 
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplateForDeletion()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("DiscountParmsAdaptor.Delete");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><RateRange>");
            sXml = sXml.Append("<raterangerowid>");
            sXml = sXml.Append(HdnSelectedIdForDeletion.Text);
            sXml = sXml.Append("</raterangerowid></RateRange></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
