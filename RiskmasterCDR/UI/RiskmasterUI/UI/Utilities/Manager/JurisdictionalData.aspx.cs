﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class JurisdictionalData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindToGridView();
            }
        }


        private void BindToGridView()
        {
            DataSet dsAdminData = null;
            dsAdminData = GetAdministrativeTracking();

            try
            {
                GridView1.DataSource = dsAdminData.Tables[1];
                GridView1.DataBind();

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        private DataSet GetAdministrativeTracking()
        {
            string strAdmintrackingSrvcOutput = string.Empty;
            XmlDocument xmlAdmintrackingDoc = new XmlDocument();
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            DataSet dSet = null;

            try
            {
                inputDocNode = InputForCWS();

                strAdmintrackingSrvcOutput = AppHelper.CallCWSService(inputDocNode.InnerXml);

                ErrorControl1.errorDom = strAdmintrackingSrvcOutput;

                xmlAdmintrackingDoc.LoadXml(strAdmintrackingSrvcOutput);

                if (strAdmintrackingSrvcOutput != null)
                {
                    dSet = new DataSet();
                    dSet.ReadXml(new XmlNodeReader(xmlAdmintrackingDoc.SelectSingleNode("ResultMessage/Document/JurisdictionalList")));
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }


            return dSet;
   
        }

        private XmlNode InputForCWS()
        {            
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
              <Message>
              <Authorization></Authorization>
              <Call>
                <Function>JurisdictionalDataAdaptor.Get</Function>
              </Call>
              <Document>
                   <JurisdictionalList>
                   <listhead>
                   <UserTableName>Table Name</UserTableName> 
                   <SysTableName>System Table Name</SysTableName>
                   <RowId>RowId</RowId> 
                   </listhead>
                   </JurisdictionalList> 
             </Document>
             </Message>  
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;

        }


        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectAdminRow('" + DataBinder.Eval(e.Row.DataItem, "RowId").ToString() + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }


       
    }
}
