﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FiscalYearWizard.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.FiscalYearWizard" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Fiscal Year Setup Wizard</title>
      <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../../../Content/system.css" type="text/css"/>
    <link rel="stylesheet" href="../../../Scripts/zapatec/zpcal/themes/system.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script type="text/jscript" language="javascript" src="../../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" language="javascript" src="../../../Scripts/rmx-common-ref.js"></script>
    <script src="../../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
    <script src="../../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
    <script type="text/javascript" src="../../../Scripts/calendar-alias.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js"></script>
    
    <script type="text/javascript" language="javascript">
           function BuildPeriods()
				{
					if (Mandatory())
					{
					
					return true;
					}
					else
					{ 
					 return false;
					}
				}
				function trim(value)
				{
				  value = value.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
				  return value;
				}
				function Mandatory()
				{
					var sPeriods = trim(document.forms[0].Wizard1_txtPeriods.value);
					if (!(sPeriods == '1' || sPeriods == "2" || sPeriods == "3" || sPeriods == "4" || sPeriods == "6" || sPeriods=="12" || sPeriods==""))
					{
						alert('A fiscal year can be only be divided into 1, 2, 3, 4, 6 or 12 number of periods.');
						document.forms[0].Wizard1_txtPeriods.focus();
						return false;
					}
					if (document.forms[0].hdnAction.value=='1')
					{
						  window.alert("The application is busy looking up data for a previous field.\nPlease wait for the results before modifying additional fields.");
						  return false;
					}
					if (trim(document.forms[0].Wizard1_txtFiscalYear.value)=='')
					{
					alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
					document.forms[0].Wizard1_txtFiscalYear.focus();
					return false;
					}
					if (trim(document.forms[0].Wizard1_txtPeriods.value)=='')
					{
					alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
					document.forms[0].Wizard1_txtPeriods.focus();
					return false;
					}
					if (IsNumeric(trim(document.forms[0].Wizard1_txtFiscalYear.value)))
					{
					 if (!((parseInt(document.forms[0].Wizard1_txtFiscalYear.value) >= 1900) && (parseInt(document.forms[0].Wizard1_txtFiscalYear.value) <= 2100)))
					 {
						 alert("Please enter a four digit fiscal year between 1900 and 2100");
						 document.forms[0].Wizard1_txtFiscalYear.focus();
						 return false;
					 }
					}
					else
					{
						 alert("Please enter a four digit fiscal year between 1900 and 2100");
						 document.forms[0].Wizard1_txtFiscalYear.focus();
						 return false;
					}
					if (IsNumeric(trim(document.forms[0].Wizard1_txtPeriods.value)))
					{
					 if (!((parseInt(document.forms[0].Wizard1_txtPeriods.value) >= 0) && (parseInt(document.forms[0].Wizard1_txtPeriods.value) <= 12)))
					 {
						 alert("Please enter number between 0 and 12");
						 document.forms[0].Wizard1_txtPeriods.focus();
						 return false;
					 }
					}
					else
					{
					 	 alert("Please enter number between 0 and 12");
						 document.forms[0].Wizard1_txtPeriods.focus();
						 return false;
					}
					return true;
				}
			function IsNumeric(sText)
			{
			var ValidChars = "0123456789.";
			var IsNumber=true;
			var Char;
			for (i = 0; i < sText.length && IsNumber == true; i++) 
				{ 
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) == -1) 
					{
					IsNumber = false;
					}
				}
			return IsNumber;
			   
			}
			
			
			function SetPeriod()
			{
			  document.forms[0].hdnNoperiod.value = document.forms[0].Wizard1_txtPeriods.value;
			}
			
			function SetHiddenCtrl()
			{
			  document.forms[0].hdnFiscalYear.value = document.forms[0].Wizard1_txtFiscalYear.value;
			}
//----------------For step2
				
				function setValue(ctrl)
				{
					document.forms[0].hdnSelected.value=ctrl.value;
				}
				function selectDate(sFieldName)
				{
					if(m_codeWindow!=null)
						m_codeWindow.close();
						
					m_sFieldName=sFieldName;
					
					m_codeWindow=window.open('csc-Theme/riskmaster/common/html/calendar.html','codeWnd',
						'width=230,height=230'+',top='+(screen.availHeight-230)/2+',left='+(screen.availWidth-230)/2+',resizable=yes,scrollbars=no');
					return false;
				}
				function ValidateDate(sCtrlName)
				{
					var sDateSeparator;
					var iDayPos=0, iMonthPos=0;
					var d=new Date(1999,11,22);
					var s=d.toLocaleString();
					var sRet="";
					var objFormElem=eval('document.forms[0].'+sCtrlName);
					
					var sDate=new String(objFormElem.value);
					
					var iMonth=0, iDay=0, iYear=0;
					var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
					if(sDate=="")
						return "";
					iDayPos=s.indexOf("22");
					iMonthPos=s.indexOf("11");
					sDateSeparator="/";
					var sArr=sDate.split(sDateSeparator);
					if(sArr.length==3)
					{
						sArr[0]=new String(parseInt(sArr[0],10));
						sArr[1]=new String(parseInt(sArr[1],10));
						sArr[2]=new String(parseInt(sArr[2],10));
						// Classic leap year calculation
						if (((parseInt(sArr[2],10) % 4 == 0) && (parseInt(sArr[2],10) % 100 != 0)) || (parseInt(sArr[2],10) % 400 == 0))
							monthDays[1] = 29;
						if(iDayPos < iMonthPos)
						{
							// Date should be as dd/mm/yyyy
							if(parseInt(sArr[1],10)< 1 || parseInt(sArr[1],10)>12 ||  parseInt(sArr[0],10)< 0 || parseInt(sArr[0],10)>monthDays[parseInt(sArr[1],10)-1])
								objFormElem.value="";
						}
						else
						{
							// Date is something like mm/dd/yyyy
							if(parseInt(sArr[0],10)< 1 || parseInt(sArr[0],10)> 12 ||  parseInt(sArr[1],10)< 0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
								objFormElem.value="";
						}
						// Check the year
						if(parseInt(sArr[2],10)< 10 || (sArr[2].length!=4 && sArr[2].length!=2))
							objFormElem.value="";
						// If date has been accepted
						if(objFormElem.value!="")
						{
							// Format the date
							if(sArr[0].length==1)
								sArr[0]="0" + sArr[0];
							if(sArr[1].length==1)
								sArr[1]="0" + sArr[1];
							if(sArr[2].length==2)
								sArr[2]="19"+sArr[2];
							if(iDayPos < iMonthPos)
								objFormElem.value=formatRMDate(sArr[2] + sArr[1] + sArr[0]);
							else
								objFormElem.value=formatRMDate(sArr[2] + sArr[0] + sArr[1]);
						}
					}
					else
						objFormElem.value="";
						return false;
				}
				
				function formatRMDate(sParamDate)
				{
					var sDateSeparator;
					var iDayPos=0, iMonthPos=0;
					var d=new Date(1999,11,22);
					var s=d.toLocaleString();
					var sRet="";
					var sDate=new String(sParamDate);
					if(sDate=="")
						return "";
					iDayPos=s.indexOf("22");
					iMonthPos=s.indexOf("11");
					//if(IE4)
					//	sDateSeparator=s.charAt(iDayPos+2);
					//else
						sDateSeparator="/";
					if(iDayPos<iMonthPos)
						sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
					else
						sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
					return sRet;
					
				}
				
					function CheckBack()
					{
					if (document.forms[0].hdnAction.value=='1')
					{
					window.alert("The application is busy looking up data for a previous field.\nPlease wait for the results before modifying additional fields.");
					return false;
					}
					}
					
					function dateSelected(sDay, sMonth, sYear)
					{
					    var objCtrl=null;
					    if(m_codeWindow!=null)
					    m_codeWindow.close();
					    objCtrl=eval('document.forms[0].'+m_sFieldName);
					    if(objCtrl!=null)
					    {
					    sDay=new String(sDay);
					    if(sDay.length==1)
					    sDay="0"+sDay;
					    sMonth=new String(sMonth);
					    if(sMonth.length==1)
					    sMonth="0"+sMonth;
					    sYear=new String(sYear);
					    document.forms[0].Wizard1_hdnSelectedYear.value=sYear;
					    objCtrl.value=formatRMDate(sYear+sMonth+sDay);
					    objCtrl.focus();
					    }
					    m_sFieldName="";
					    m_codeWindow=null;
					    setDataChange(true);
					    if (document.forms[0].Wizard1_hdnChange.value=='1')
					    {
					    document.forms[0].Wizard1_hdnChange.value="0";
					    CreatePeriods();
					    return true;
					    }
					}
					
					
					function setDataChange(b)
					{
					  m_DataChanged=b;
                      
	                 return b;
					}
					
					function CreatePeriods()
					{
					
					if(m_DataChanged)
					{
					  ValidateDate('Wizard1_txtdatestart')
					if (document.forms[0].Wizard1_txtdatestart.value=='')
					{
					return;
					}
					document.forms[0].Wizard1_hdnBuildPeriod.value = "Build";
					document.forms[0].hdnNewMode.value = "Build";
					document.forms[0].submit();
					}
					
					}
					
					function CheckForUpdation()
					{
					    document.forms[0].Wizard1_txtdatestart.value = "";
					    document.forms[0].Wizard1_txtdateend.value = "";
					    document.forms[0].Wizard1_txtQ1start.value = "";
					    document.forms[0].Wizard1_txtQ1end.value = "";
					    document.forms[0].Wizard1_txtQ2start.value = "";
					    document.forms[0].Wizard1_txtQ2end.value = "";
					    document.forms[0].Wizard1_txtQ3start.value = "";
					    document.forms[0].Wizard1_txtQ3end.value = "";
					    document.forms[0].Wizard1_txtQ4start.value = "";
					    document.forms[0].Wizard1_txtQ4end.value = "";
					    alert("Error in the date selected.\nFiscal year's starting or ending date is overlapping with existing fiscal year setup");
					 
					}
					
					
					function VerfiyInput()
					{
					
					if( document.forms[0].Wizard1_txtdatestart == null)
					{
					  //Call is not from step 2
					  return true;
					}
					
					if (trim(document.forms[0].Wizard1_txtdatestart.value)=='')
					{
					alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
					document.forms[0].Wizard1_txtdatestart.focus();
					return false;
					}
					if (trim(document.forms[0].Wizard1_txtdateend.value)=='')
					{
					alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
					document.forms[0].Wizard1_txtdateend.focus();
					return false;
					}
					for (i=1;i<=4;i++)
					{
					ctrl=document.getElementById('Wizard1_txtQ'+i+'start');
					if (trim(ctrl.value)=='')
					{
					alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
					ctrl.focus();
					return false;
					}
					ctrl=document.getElementById('Wizard1_txtQ'+i+'end');
					if (trim(ctrl.value)=='')
					{
					alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
					ctrl.focus();
					return false;
					}
					}
					arr=document.forms[0].Wizard1_txtdatestart.value.split('/');
					
					arrStartDate = document.forms[0].Wizard1_txtdatestart.value.split('/');
					arrEndDate = document.forms[0].Wizard1_txtdateend.value.split('/');
					if (arrStartDate[2] > arrEndDate[2])
					{
					alert("Invalid Date Range!\nStart Date cannot be after End Date!");
					return false;
					}
					else if (arrStartDate[1] > arrEndDate[1])
					{
					if (arrStartDate[2] == arrEndDate[2])
					{
					alert("Invalid Date Range!\nStart Date cannot be after End Date!");
					return false;
					}
					}
					else if (arrStartDate[0] > arrEndDate[0])
					{
					if(arrStartDate[1] == arrEndDate[1])
					{
					if(arrStartDate[2] == arrEndDate[2])
					{
					alert("Invalid Date Range!\nStart Date cannot be after End Date!");
					return false;
					}
					}
					//abisht MITS 10334
					if (document.forms[0].Wizard1_hdnOverlapFlag.value == "True")
					{
					alert("Error in the date selected.\nFiscal year's starting or ending date is overlapping with existing fiscal year setup");
					return false;
					}
					}
					
					
					if (arrStartDate[2] != document.forms[0].hdnFiscalYear.value && arrEndDate[2] != document.forms[0].hdnFiscalYear.value)
					{
					alert('Please enter the Fiscal Year Start Date & End Date as per the Fiscal Year setting in previous screen');
					return false;
					}

					return true;

					}
					
					
				</script>
</head>
<body>
    <form id="frmData" name="frmData" runat="server">
    <div>
        <input type="text" id="hdnNoperiod" runat="server" value="0" style="display:none" />
        <input type="text" id="hdnLOB" runat="server" size="30" style="display:none" />
        <input type="text" runat="server" id="hdnOrg"   size="30" style="display:none" />
        <asp:TextBox runat="server" style="display:none" id="hdnFromLineOfBusiness"></asp:TextBox>	
        <asp:textbox style="display:none" runat="server" id="hdnFromOrganization" ></asp:textbox>
        <asp:textbox style="display:none" runat="server" id="hdnPeriods" ></asp:textbox>
        <asp:textbox style="display:none" runat="server" id="hdnQuarters" ></asp:textbox>
        <input id="hdnFiscalYear" runat="server" type="hidden" /> 
        <input id="hdnFiscalWeekDay" runat="server" type="hidden" />
         <input id="hdnMode" runat="server" type="hidden" />
         <input id="hdnNewMode" runat="server" type="hidden" />
         <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
        <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0"    
             
            OnNextButtonClick="Wizard1_NextButtonClick" 
            onfinishbuttonclick="Wizard1_FinishButtonClick1" DisplaySideBar="False">
                <StartNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="Javascript:return window.close();" />
                  <asp:Button class="button" ID="StepPreviousButton" disabled="true" runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" OnClientClick="return Mandatory();" /> 
                  <asp:Button class="button" ID="StepFinishButton" runat="server" Text="Finish" disabled="true"  /> 
                </StartNavigationTemplate>  
                <StepNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="Javascript:return window.close();" />
                  <asp:Button class="button" ID="StepPreviousButton"  runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" OnClientClick="return VerfiyInput();" /> 
                  <asp:Button class="button" ID="StepFinishButton"  runat="server" Text="Finish" disabled="true"  />
               </StepNavigationTemplate>
               <FinishNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="Javascript:return window.close();" />
                  <asp:Button class="button" ID="StepPreviousButton"  runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" disabled="true" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton" runat="server" Text="Finish" 
                       onclick="StepFinishButton_Click"   /> 
               </FinishNavigationTemplate>
            
            <WizardSteps>
              <asp:WizardStep runat="server" ID="step1" title="Step 1">
                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                <tr>
                 <td class="ctrlgroup">
                    
                     Fiscal Year Setup Wizard - Step 1 of 3 - Basic information</td>
                </tr>
                <tr>
                 <td><input type="hidden" value="" id="hdnValue" >&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
                    <input id="hdnRefresh" type="hidden" value="" />
                    <input name="hdnAction" type="hidden" value="0" />
                                              
                             </td>
                </tr>
               </table>
               <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                 <td><img src="../../../Images/fiscalYear.gif" /></td>
                 <td>
      					            The fiscal year setup wizard defines the fiscal year for reporting <br >
      					            and other purposes. This step defines how many periods
                                    <br />
                                    make up the fiscal year and the last day of the week. 
                                    
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                 </td>
                </tr>
                <tr>
                 <td></td>
                </tr>
               </table>
               <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                 <td colspan="2">
                  <hr>
                 </td>
                </tr>
                <tr>
                 <td colspan="2"> </td>
                </tr>
                <tr>
                 <td width="25%">Line of Business:</td>
                 <td width="10%"><input type="text" id="txtLOB" runat="server"  value="" 
                         readonly="readonly" size="30" /> </td>
                 <td width="*"></td>
                </tr>
                <tr>
                 <td width="25%">Organization:</td>
                 <td width="10%"><input type="text" runat="server" id="txtOrg"   value="" 
                         size="30" readonly="readonly" /> </td>
                 <td width="*"></td>
                </tr>
                <tr>
                 <td width="50%"><u>What is the four digit fiscal year?</u></td>
                 <td width="10%"><input type="text" value="" size="12" 
                         id="txtFiscalYear" onblur="SetHiddenCtrl()" runat="server"  /> </td>
                 <td width="*"></td>
                </tr>
                <tr>
                 <td width="80%"><u>How many periods make up the fiscal year?</u></td>
                 <td width="10%">
                    <asp:TextBox ID="txtPeriods" runat="server"   size="10" onblur="SetPeriod();"  
                         OnTextChanged="txtPeriods_TextChanged"  AutoPostBack="true" ></asp:TextBox></td>
              
                 <td width="*"></td>
                </tr>
                <tr>
                 <td width="80%">
      						            <u>What day ends the week?</u></td>
                    <td width="10%">
                        <asp:DropDownList ID="ddlFiscalYear" runat="server">
                        </asp:DropDownList>
                         
                          
                        </td>
                    <td width="*">
                        </td>
                </tr>
                <tr>
                 <td colspan="2">
      							        
                  						
                 </td>
                </tr>
                <tr>
                 <td colspan="2">
                     
                 </td>
                </tr>
                   <tr>
                       <td colspan="2">
                           <hr />
                       </td>
                   </tr>
               </table>
               <table border="0">
                <tr>
                 <td></td>
                </tr>
               </table>
               
               
   </asp:WizardStep>
   
 
   
              <asp:WizardStep ID="step2" runat="server" Title="Step 2" >
                           <input type="text" runat="server" id="hdnBuildPeriod" style="display:none" />
                           <input type="text" runat="server" id="hdnOverlapFlag" style="display:none" />
                           <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                             <td class="ctrlgroup">Fiscal Year Setup Wizard - Step 2 of 3 - Quarters setup</td>
                            </tr>
                            <tr>
                             <td>
                              
                             </td>
                            </tr>
                           </table>
                           <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                             <td>
                                 <img src="../../../Images/fiscalYear.gif" /></td>
                             <td>
                                 This step defines the start and end dates of the
                                 <br>fiscal year and its quarters. Defining Quarters
                                 <br />
                                 helps schedule reports using Event Task Scheduler.
                                 <br>
                                 <br></br>
                                 <br></br>
                                 </br>
                                 </br>
                                </td>
                            </tr>
                            <tr>
                             <td></td>
                            </tr>
                            <tr>
                             <td colspan="2">
                              <hr>
                             </td>
                            </tr>
                           </table>
                           <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                             <td width="75%" cellspacing="0" cellpadding="0">
                              <table border="0" cellspacing="0" cellpadding="0">
                               <tr>
                                <td cellpadding="0" cellspacing="0">
                                 							
                                </td>
                                <td colspan="5">
         								                        
                                 							
                                    What are fiscal year&#39;s starting &amp; ending dates?
         								                        
                                 							
                                </td>
                               </tr>
                               <tr cellspacing="0" cellpadding="0">
                                <td width="10%"> 
                                   </td>
                                <td width="10%">
         						    <input ID="txtdatestart" runat="server" 
                                        onblur="ValidateDate('Wizard1_txtdatestart');" 
                                        onchange="setDataChange(true);CreatePeriods();" size="12" type="text"></input>
         						</td>
                                <td width="5%">
                                    <%--JIRA 9952 added id- msampathkuma --%>
                                    <input class="button" name="datereportedbtn1" id="datereportedbtn1"
                                        onClick="document.forms[0].hdnChange.value='1';" type="button" value="..." /><script 
                                        type="text/javascript">



									                        Calendar.setup(
									                        {
										                        inputField : "Wizard1_txtdatestart",
										                        ifFormat : "%m/%d/%Y",
										                        button : "datereportedbtn1"
									                        }
									                        );
								                        </script></td>
                                <td width="5%">
                                    <u>to</u></td>
                                <td width="10%"><input type="text" runat="server" value="" size="12" onblur="ValidateDate('Wizard1_txtdateend');" id="txtdateend" />
                                </td>
                                <td width="5%"><input type="button" class="button" value="..." name="datereportedbtn2" id="datereportedbtn2" /><script type="text/javascript">
									                        Calendar.setup(
									                        {
										                        inputField : "Wizard1_txtdateend",
										                        ifFormat : "%m/%d/%Y",
										                        button : "datereportedbtn2"
									                        }
									                        );
								                        </script></td>
                               </tr>
                               <tr>
                                <td colspan="6" width="100%">What are starting &amp; ending dates for each of four quarters?</td>
                               </tr>
                               <tr>
                                <td width="10%">
                                    <u>1st Quarter</u></td>
                                <td width="10%">
                                    <input ID="txtQ1start" runat="server" 
                                        onblur="ValidateDate('Wizard1_txtQ1start');" size="12" type="text"></input> </td>
                                <td width="5%">
                                    <input class="button" name="datereportedbtn3" id="datereportedbtn3" type="button" value="..." /><script 
                                        type="text/javascript">



									                        Calendar.setup(
									                        {
										                        inputField : "Wizard1_txtQ1start",
										                        ifFormat : "%m/%d/%Y",
										                        button : "datereportedbtn3"
									                        }
									                        );
								                        </script></td>
                                <td width="5%">
                                    <u>to</u></td>
                                <td width="10%"><input type="text" runat="server" value="" size="12" onblur="ValidateDate('Wizard1_txtQ2end');" id="txtQ1end" />
                                </td>
                                <td width="5%"><input type="button" class="button" value="..." name="datereportedbtn4" id="datereportedbtn4" /><script type="text/javascript">
									                        Calendar.setup(
									                        {
										                        inputField : "Wizard1_txtQ1end",
										                        ifFormat : "%m/%d/%Y",
										                        button : "datereportedbtn4"
									                        }
									                        );
								                        </script></td>
                               </tr>
                               <tr>
                                <td width="10%">
                                    <u>2nd Quarter</u></td>
                                <td width="10%">
                                    <input ID="txtQ2start" runat="server" 
                                        onblur="ValidateDate('Wizard1_txtQ2start');" size="12" type="text"></input> </td>
                                <td width="5%">
                                    <input class="button" name="datereportedbtn5" id="datereportedbtn5" type="button" value="..." /><script 
                                        type="text/javascript">



									                        Calendar.setup(
									                        {
										                        inputField : "Wizard1_txtQ2start",
										                        ifFormat : "%m/%d/%Y",
										                        button : "datereportedbtn5"
									                        }
									                        );
								                        </script></td>
                                <td width="5%">
                                    <u>to</u></td>
                                <td width="10%"><input type="text" name="$node^29" value="" size="12" runat="server" onblur="ValidateDate('Wizard1_txtQ2end');" id="txtQ2end">
                                </td>
                                <td width="5%"><input type="button" class="button" value="..." name="datereportedbtn6" id="datereportedbtn6"><script type="text/javascript">
									                        Calendar.setup(
									                        {
										                        inputField : "Wizard1_txtQ2end",
										                        ifFormat : "%m/%d/%Y",
										                        button : "datereportedbtn6"
									                        }
									                        );
								                        </script></td>
                               </tr>
                               <tr>
                                <td width="10%">
                                    <u>3rd Quarter</u></td>
                                <td width="10%">
                                    <input ID="txtQ3start" runat="server" 
                                        onblur="ValidateDate('Wizard1_txtQ3start');" size="12" type="text"></input> </td>
                                <td width="5%">
                                    <input class="button" name="datereportedbtn7" id="datereportedbtn7" type="button" value="..."><script 
                                        type="text/javascript">



									                        Calendar.setup(
									                        {
										                        inputField : "Wizard1_txtQ3start",
										                        ifFormat : "%m/%d/%Y",
										                        button : "datereportedbtn7"
									                        }
									                        );
								                        </script></input></td>
                                <td width="5%">
                                    <u>to</u></td>
                                <td width="10%"><input type="text" name="$node^31" value="" runat="server" size="12" onblur="ValidateDate('Wizard1_txtQ3end');" id="txtQ3end" /> </td>
                                <td width="5%"><input type="button" class="button" value="..." name="datereportedbtn8" id="datereportedbtn8"><script type="text/javascript">
									                        Calendar.setup(
									                        {
										                        inputField : "Wizard1_txtQ3end",
										                        ifFormat : "%m/%d/%Y",
										                        button : "datereportedbtn8"
									                        }
									                        );
								                        </script></td>
                               </tr>
                               <tr>
                                <td width="10%">
                                    <u>4th Quarter</u></td>
                                <td width="10%">
                                    <input ID="txtQ4start" runat="server" 
                                        onblur="ValidateDate('Wizard1_txtQ4start');" size="12" type="text"></input> </td>
                                <td width="5%">
                                    <input class="button" name="datereportedbtn9" id="datereportedbtn9"
                                        onClick="selectDate('txtQ4start')" type="button" value="..."><script 
                                        type="text/javascript">



									                        Calendar.setup(
									                        {
										                        inputField : "Wizard1_txtQ4start",
										                        ifFormat : "%m/%d/%Y",
										                        button : "datereportedbtn9"
									                        }
									                        );
								                        </script></input></td>
                                <td width="5%">
                                    <u>to</u></td>
                                <td width="10%"><input type="text" name="$node^33" value="" runat="server" size="12" onblur="ValidateDate('Wizard1_txtQ4end');" id="txtQ4end" /> </td>
                                <td width="5%"><input type="button" class="button" value="..." name="datereportedbtn10" id="datereportedbtn10" onClick="selectDate('txtQ4end')" /><script type="text/javascript">
									                        Calendar.setup(
									                        {
										                        inputField : "Wizard1_txtQ4end",
										                        ifFormat : "%m/%d/%Y",
										                        button : "datereportedbtn10"
									                        }
									                        );
								                        </script></td>
                                   <%--Jira 9952- msampathkuma--%>
                               </tr>
                              </table>
                             </td>
                             <td width="*"></td>
                            </tr>
                            <tr>
                             <td colspan="2">
                              <hr />
                             </td>
                            </tr>
                           </table>
                           
               </asp:WizardStep>
   
   
   
              <asp:WizardStep ID="step3"  runat="server" Title="Step 3" >
              
                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                <tr>
                <td class="ctrlgroup">Fiscal Year Setup Wizard - Step 3 of 3 - Periods  setup</td>
                </tr>
                <tr>
                <td></td>
                </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                <td><img SRC="../../../Images//fiscalYear.gif" /></td>
                <td>This step defines the start and end dates of the <br>
                periods for the fiscal year.  Defining Periods <br>
                    helps schedule reports using Event Task Scheduler.

                </td>
                </tr>
                <tr>
                <td></td>
                </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                <td>
                <hr />
                </td>
                </tr>
                <tr>
                <td width="75%">
                <div id="divForms" class="divScroll">
                 
                 <asp:GridView runat="server" ID="dlPeriod" AutoGenerateColumns="false" width="100%" >
                 <HeaderStyle CssClass="colheader5" />
		         <AlternatingRowStyle CssClass="data2" />
                  <Columns>
                     <asp:TemplateField HeaderText="Period Number" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="colheader5"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblRow1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PeriodNumber")%>' ></asp:Label>
                                </ItemTemplate> 

<HeaderStyle HorizontalAlign="Left" CssClass="colheader5" ForeColor="White"></HeaderStyle>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Start Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="colheader5"   HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblRow2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate")%>' ></asp:Label>
                                </ItemTemplate> 

<HeaderStyle HorizontalAlign="Left" CssClass="colheader5" ForeColor="White"></HeaderStyle>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="End Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="colheader5"   HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblRow3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndDate")%>' ></asp:Label>
                                </ItemTemplate> 

<HeaderStyle HorizontalAlign="Left" CssClass="colheader5" ForeColor="White"></HeaderStyle>
                      </asp:TemplateField>
                  
                  </Columns>
                 </asp:GridView>
                 
                 </div>
                 </td>
                 </tr>
                <tr>
                <td>
                <hr />
                </td>
                </tr>
                </table>
              
              </asp:WizardStep>
   
    </WizardSteps>
    </asp:Wizard>
    
    </div>
    </form>
</body>
</html>
