<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MedicalInfoSetup.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.MedicalInfoSetup" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
 <head runat="server">
        <title>Medical Info Setup</title>
        <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
        <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
        <script type ="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>   
      
        <script type="text/javascript">
            function ShowPleaseWait() {
                pleaseWait.Show();
                return true;
            }
        </script> 
 </head>

 <body class="10pt" onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();"> 
 <form id="frmData" method="post" runat="server">
  
     <uc1:ErrorControl ID="ErrorControl1" runat="server" />
     <div >
         <input type="hidden" id="wsrp_rewrite_action_1" name="" value="">  
            
         <div class="toolbardrift" id="toolbardrift" name="toolbardrift">  
             <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
             <tr>
                 <td height="32">
                     <asp:imagebutton id="btnSave" runat="server" class="bold" onmouseover="src='../../../Images/tb_save_mo.png';" onmouseout="src='../../../Images/tb_save_active.png';" onclick="Save" ImageUrl = "../../../Images/tb_save_active.png" ToolTip="Save" TabIndex ="5" OnClientClick="return ShowPleaseWait();" />
                 </td>
             </tr>
             </table>    
         </div>
         <div class="msgheader" id="formtitle">Medical Disability Advisor Setup</div>
         <div class="errtextheader"></div>
         <table border="0">
             <tbody>
                 <tr>
                     <td>
                         <table border="0">
                             <tbody>        
                                 <%--<tr>
                                    <td class="ctrlgroup" colSpan="2">Medical Disability Advisor Setup</td>
                                 </tr>--%>
                                 
                                 <tr>
                                    <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WcMdaDsnName']" value="WC_MDA_DSN_NAME" style="display:none" id="WcMdaDsnName"></asp:TextBox>
                                 </tr>
                                 
                                 <tr>
                                     <td>MDA DSN Name:&nbsp;&nbsp;</td>
                                     <td>
                                         <div title="" style="padding: 0px; margin: 0px">
                                            <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DsnName']" value="" size="30" id="DsnName" onchange="setDataChanged(true);" maxlength="50" TabIndex ="1"></asp:TextBox>
                                         </div>
                                     </td>
                                 </tr>
                                     
                                 <tr>
                                        <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WcMdaUserId']" value="WC_MDA_USER_ID" style="display:none" id="WcMdaUserId"></asp:TextBox>
                                 </tr>
                                         
                                 <tr>
                                     <td>User Id:&nbsp;&nbsp;</td>                                     
                                     <td>
                                         <div title="" style="padding: 0px; margin: 0px">
                                            <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UserId']" value="" size="30" id="UserId" onchange="setDataChanged(true);" TabIndex ="2"></asp:TextBox>
                                         </div>
                                     </td>
                                 </tr>
                                        
                                 <tr>
                                        <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WcMdaPswd']" value="WC_MDA_PSWD" style="display:none" id="WcMdaPswd"></asp:TextBox>
                                 </tr>
                                          
                                 <tr>
                                     <td>Password:&nbsp;&nbsp;</td>
                                     <td>
                                         <asp:TextBox TextMode="Password" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Password']" value="" size="30" id="Password" type="password" onchange="setDataChanged(true);" EnableViewState ="false" TabIndex ="3" autocomplete="off"></asp:TextBox>
                                     </td>
                                 </tr>
                                         
                                 <tr>
                                        <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WcMdaHelpFile']" value="WC_MDA_HELP_FILE" style="display:none" id="WcMdaHelpFile"></asp:TextBox>
                                 </tr>
                                 
                                 <tr>
                                     <td>MDA Help File:&nbsp;&nbsp;</td>
                                     <td>
                                        <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='HelpFile']" value="" size="30" onchange="setDataChanged(true);" id="HelpFile" TabIndex ="4"></asp:TextBox>
                                     </td>
                                 </tr>
                             </tbody>
                         </table>                         
                              <input type="text" name="" value="" id="SysViewType" style="display:none"/>
                              <input type="text" name="" value="" id="SysCmd" style="display:none"/>
                              <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none"/>
                              <input type="text" name="" value="" id="SysCmdQueue" style="display:none" />
                              <input type="text" name="" value="" id="SysCmdText" style="display:none" />
                              <input type="text" name="" value="" id="SysClassName" style="display:none" />
                              <input type="text" name="" value="" id="SysSerializationConfig" style="display:none"/>
                              <input type="text" name="" value="claiminformationsetup" id="SysFormIdName" style="display:none" />
                              <input type="text" name="" value="" id="claiminformationsetup" style="display:none"/>
                              <input type="text" name="" value="" id="SysFormPIdName" style="display:none" />
                              <input type="text" name="" value="" id="SysFormPForm" style="display:none" />
                              <input type="text" name="" value="" id="SysInvisible" style="display:none" />
                              <input type="text" name="" value="" id="SysFormName" style="display:none" />
                              <input type="text" name="" value="" id="SysRequired" style="display:none"/>
                              <input type="text" name="" value="ClientName|" id="SysFocusFields" style="display:none"/></td>
                     
                     <td valign="top"></td>
                 </tr>
             </tbody>
         </table>                        
     </div>
     <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />           
 </form>
 </body>
</html>
