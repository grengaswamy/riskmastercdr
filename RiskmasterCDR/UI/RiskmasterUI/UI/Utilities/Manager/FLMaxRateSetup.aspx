﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FLMaxRateSetup.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.FLMaxRateSetup" ValidateRequest = "false" %>

<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
  <title>FL Max Rate</title>
  <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">{var i;}
</script>
  </head>
  <body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <div>
          <span>
            <dg:UserControlDataGrid runat="server" ID="FLMaxRateSetupGrid" GridName="FLMaxRateSetupGrid" GridTitle="FL Max Rate" Target="/Document/form/group/FLMaxRates" Ref="/Instance/Document/form//control[@name='FLMaxRateSetupGrid']" Unique_Id="MaxRateId" ShowRadioButton="true" Width="" Height="" hidenodes="|MaxRateId|" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="250" Type="GridAndButtons" RowDataParam="FLMaxRate" OnClick="KeepRowForEdit('FLMaxRateSetupGrid');"/>
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="FLMaxRateSetupSelectedId"  RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="FLMaxRateSetupGrid_RowDeletedFlag"  RMXType="id" Text="false" />
       <asp:TextBox style="display:none" runat="server" id="FLMaxRateSetupGrid_Action"  RMXType="id" />
       <asp:TextBox style="display:none" runat="server" id="FLMaxRateSetupGrid_RowAddedFlag"  RMXType="id" Text="false"  />
      </div>
      
    </form>
  </body>
</html>