<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimTypeChangeOption.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.Manager.ClaimTypeChangeOption" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Claim Type Option</title>
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

    <script type="text/javascript" language="javascript">
        //tkatsarski: 11/28/14 - JIRA-4878: The form fields are checked on load of the form and when Ok is pressed. If the fields are not changed, then the form is closed.
        var sLobOld = "";
        var sCurClaimTypeOld = "";
        var sNewClaimTypeOld = "";
        var sTrigTypeOld = "";
        var sIncSchChecksOld = "";

        function GetFormFields() {
            sLobOld = document.forms[0].Lob_codelookup.value;
            sCurClaimTypeOld = document.forms[0].CurClaimType_codelookup.value;
            sNewClaimTypeOld = document.forms[0].NewClaimType_codelookup.value;
            sTrigTypeOld = document.forms[0].TrigType_codelookup.value;
            sIncSchChecksOld = document.forms[0].IncSchChecks_codelookup.value;
        }
        function CheckFormFields() {
            var dataChanged = true;
            if ((sLobOld == document.forms[0].Lob_codelookup.value) &&
                (sCurClaimTypeOld == document.forms[0].CurClaimType_codelookup.value) &&
                (sNewClaimTypeOld == document.forms[0].NewClaimType_codelookup.value) &&
                (sTrigTypeOld == document.forms[0].TrigType_codelookup.value) &&
                (sIncSchChecksOld == document.forms[0].IncSchChecks_codelookup.value)) {
                dataChanged = false;
            }

            SetDataChanged(dataChanged);
        }
    </script>
</head>
<body onload="CopyGridRowDataToPopup();GetFormFields();">
    <form name="frmData" id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Claim Type Change Option" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/ClaimTypeChangeOption/control[@name ='RowId']" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <%--<tr>
                <td colspan="3">
                    <asp:Label runat="server" name="lblClaimTypeChangeOption" ID="lblClaimTypeChangeOption"
                        Text="Claim Types" />
                </td>
            </tr>--%>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblLob" Text="Line Of Business:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="Lob" CodeTable="LINE_OF_BUSINESS" ControlName="Lob"
                        RMXRef="/Instance/Document/ClaimTypeChangeOption/control[@name ='Lob']" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblCurClaimType" Text="Current Claim type:" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="CurClaimType" CodeTable="CLAIM_TYPE" ControlName="CurClaimType"
                        RMXRef="/Instance/Document/ClaimTypeChangeOption/control[@name ='CurClaimType']" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblNewClaimType" Text="New Claim type:" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="NewClaimType" CodeTable="CLAIM_TYPE" ControlName="NewClaimType"
                        RMXRef="/Instance/Document/ClaimTypeChangeOption/control[@name ='NewClaimType']" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblTrigType" Text="Triggering Transaction Type:" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="TrigType" CodeTable="TRANS_TYPES" ControlName="TrigType"
                        RMXRef="/Instance/Document/ClaimTypeChangeOption/control[@name ='TrigType']" />
                </td>
            </tr>
            <tr>
                <td>
					<asp:Label ID="Label3" Text="Include Scheduled Checks:" runat="server"></asp:Label>
														
				</td>
                <td>
                    &nbsp;
                </td>
				<td>
                    <uc:CodeLookUp runat="server" ID="IncSchChecks" CodeTable="PSO_YES_NO" ControlName="IncSchChecks" RMXRef="/Instance/Document/ClaimTypeChangeOption/control[@name ='IncSchChecks']" />
				</td>
            </tr>
        </tbody>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="CheckFormFields(); return ClaimTypeOptions_onOk();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return ClaimTypeOptions_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
