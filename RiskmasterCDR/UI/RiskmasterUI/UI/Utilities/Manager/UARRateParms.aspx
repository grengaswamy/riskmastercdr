﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UARRateParms.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.UARRateParms" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc3" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Add/Modify Unit at Risk</title>
    <link rel="stylesheet" href="/RiskmasterUI/Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/system.css" type="text/css" />
    <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
        .style1
        {
            height: 30px;
        }
    </style>
    
    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/drift.js"></script>
    
     <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/EnhPolicy.js"></script>

    <uc3:CommonTasks ID="CommonTasks1" runat="server" />
    <%--vkumar258 - RMA-6037 - Starts --%>

    <%--   <script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
</head>
<body class="10pt" onload="CopyGridRowDataToPopup();DisableExposure();">
    <form id="frmData" method="post" runat="server">
      <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" /><div class="toolbardrift"
        id="toolbardrift" name="toolbardrift">
        <table class="toolbar" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td align="center" valign="middle" height="32">
                        <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="ImageButton1" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                            onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" OnClientClick="return ValidateFieldsforExpRateParms();"
                            OnClick="save_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br />
    <br />
    <div class="msgheader" id="formtitle">
        Add/Modify Unit at Risk</div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Unit at Risk Parameters
                                </td>
                            </tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="RowId"></asp:TextBox>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_LineofBusiness" Text="Line of Business:" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                 <!--Start:Added by Nitin Goel,02/25/2010,MITS#18231-->
                                   <%-- <uc2:CodeLookUp runat="server" ID="uarlob" CodeTable="POLICY_LOB" ControlName="uarlob"
                                        RMXRef="/Instance/Document/Document/ExpRates/control[@name='LOB']" type="code"
                                        TabIndex="0" Required="true" CodeFilter=" AND CODES.SHORT_CODE NOT IN(\'WC\') "/>--%>
                                         <uc2:CodeLookUp runat="server" ID="uarlob" CodeTable="POLICY_LOB" ControlName="uarlob"
                                        RMXRef="/Instance/Document/Document/ExpRates/control[@name='LOB']" type="code"
                                        TabIndex="0" Required="true" Filter="CODES.SHORT_CODE NOT IN(\'WC\') "/>
                                        <!--End:Nitin Goel,02/25/2010,MITS#18231-->
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_unittype" Text="Type :" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <uc2:CodeLookUp runat="server" ID="Unit_Type" CodeTable="PROP_CAT" ControlName="Unit_Type"
                                        RMXRef="/Instance/Document/Document/ExpRates/control[@name='Exposure']" type="code"
                                        TabIndex="2" Required="true" />
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="style1">
                                    <asp:Label runat="server" ID="lbl_Statecode" Text="State:" class="required" />&nbsp;&nbsp;
                                </td>
                                <td class="style1">
                                    <uc2:CodeLookUp runat="server" ID="Statecode" CodeTable="states" ControlName="Statecode"
                                        RMXRef="/Instance/Document/Document/ExpRates/control[@name='Statecode']" type="code"
                                        TabIndex="4" Required="true" />
                                </td>
                            </tr>
                             <!--smahajan6m-->
            <tr>
                <td>
                    <asp:Label runat="server" ID="lbl_OrgHierarchyCode" Text="Org. Hierarchy:" />
                </td>
                <td>          
                    <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                         ID="OrgHierarchyCode" ControlName="OrgHierarchyCode" RMXRef="/Instance/Document/Document/discount/OrgHierarchyCode"
                         RMXType="orgh" name="OrgHierarchyCode" cancelledvalue="" TabIndex="6" />
                    <asp:Button runat="server" class="CodeLookupControl" Text="" 
                        ID="OrgHierarchyCodebtn" 
                        OnClientClick="return selectCode('orgh','OrgHierarchyCode','ALL');" 
                        TabIndex="6"/>
                    <asp:TextBox Style="display: none" runat="server" ID="OrgHierarchyCode_cid" RMXRef="/Instance/Document/Document/discount/OrgHierarchyCode" cancelledvalue="" />
                </td>
            </tr>
            <!--smahajan6m-->
                            <tr>
                                <td>
                                    <%--Start:Added by Nitin Goel,02/11/2010,MITS#18231--%>
                                    <%--<asp:Label runat="server" ID="lbl_Amount" Text="Amount:" />&nbsp;&nbsp;--%>
                                    <asp:Label runat="server" ID="lbl_Amount" Text="Amount:" class="required" />&nbsp;&nbsp;
                                    <%--End:Nitin Goel,02/11/2010,MITS#18231--%>
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                    <%--Start:Added by Nitin Goel,02/11/2010,MITS#18231--%>
                                        <%--<asp:TextBox runat="server" ID="Amount" RMXRef="/Instance/Document/Document/ExpRates/control[@name='Amount']"
                                            onchange="setDataChanged(true);" TabIndex="7"></asp:TextBox>--%>
                                            <!--Start:Added by Nitin Goel: 05/24/2010,call method to validate the alphabetic character -->
                                            <asp:TextBox runat="server" ID="UarAmount" RMXRef="/Instance/Document/Document/ExpRates/control[@name='Amount']"
                                            onchange="setDataChanged(true);numLostFocus(this);" TabIndex="7" Required = "true" ></asp:TextBox></div>
                                             <!--End:Nitin Goel,05/24/2010, call method to validate the alphabetic character-->
                                            <%--End:Nitin Goel,02/11/2010,MITS#18231--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_Flat" Text="Flat Or Percent:" class="required" />&nbsp;&nbsp
                                </td>
                                <td>
                                    <uc2:CodeLookUp runat="server" ID="ExpRateType" CodeTable="DISCOUNT_TYPE" ControlName="ExpRateType"
                                        RMXRef="/Instance/Document/Document/ExpRates/control[@name='ExpRateType']" type="code"
                                        TabIndex="8" Required="true" />
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_Fixed" Text="Fixed Or Pro-Rata:" class="required" />&nbsp;&nbsp
                                </td>
                                <td>
                                    <uc2:CodeLookUp runat="server" ID="FixedOrProRata" CodeTable="RATING_TYPE" ControlName="FixedOrProRata"
                                        RMXRef="/Instance/Document/Document/ExpRates/control[@name='FixedOrProRata']"
                                        type="code" TabIndex="10" Required="true" />
                                 
                                </td>
                            </tr>
                           <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_BaseRate" Text="Base Rate:" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox runat="server" ID="BaseRate" RMXRef="/Instance/Document/Document/ExpRates/control[@name='BaseRate']"
                                            onchange="setDataChanged(true);" TabIndex="11"></asp:TextBox></div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_EffectiveDate" Text="Effective Date:" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox runat="server" FormatAs="date" ID="EffectiveDate" RMXRef="/Instance/Document//control[@name='EffectiveDate']"
                                        RMXType="date" onchange="setDataChanged(true);" 
                                        onblur="dateLostFocus(this.id);" TabIndex="12" />
                                        <%--vkumar258 - RMA-6037 - Starts --%>
                                        <%-- <asp:Button class="DateLookupControl" runat="server" ID="EffectiveDatebtn" 
                                        TabIndex="13" />

                                    <script type="text/javascript">
                                        Zapatec.Calendar.setup(
				            {
				                inputField: "EffectiveDate",
				                ifFormat: "%m/%d/%Y",
				                button: "EffectiveDatebtn"
				            }
				            );
                                    </script>--%>
                                        <script type="text/javascript">
                                            $(function () {
                                                $("#EffectiveDate").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "../../../Images/calendar.gif",
                                                    //buttonImageOnly: true,
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    changeYear: true
                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "13");
                                            });
                                        </script>
                                        <%--vkumar258 - RMA_6037- End--%>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" class="label" ID="lbl_ExpirationDate" Text="Expiration Date:" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox runat="server" FormatAs="date" ID="ExpirationDate" RMXRef="/Instance/Document//control[@name='ExpirationDate']"
                                        RMXType="date" onchange="setDataChanged(true);" 
                                        onblur="dateLostFocus(this.id);" TabIndex="14" />
                                        <%--vkumar258 - RMA-6037 - Starts --%>
                                        <%-- <asp:Button class="DateLookupControl" runat="server" ID="ExpirationDatebtn" 
                                        TabIndex="15" />

                                    <script type="text/javascript">
                                        Zapatec.Calendar.setup(
				            {
				                inputField: "ExpirationDate",
				                ifFormat: "%m/%d/%Y",
				                button: "ExpirationDatebtn"
				            }
				            );
                                    </script>--%>
                                        <script type="text/javascript">
                                            $(function () {
                                                $("#ExpirationDate").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "../../../Images/calendar.gif",
                                                    //buttonImageOnly: true,
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    changeYear: true
                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "15");
                                            });
                                        </script>
                                        <%--vkumar258 - RMA_6037- End--%>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Style="display: none" ID="FormMode"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Style="display: none" ID="txtvalidate"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Style="display: none" ID="EditAddMode"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:TextBox Style="display: none" runat="server" ID="gridname"></asp:TextBox>
                                    <asp:TextBox Style="display: none" runat="server" ID="mode"></asp:TextBox>
                                    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition"></asp:TextBox>
                                    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
                                    <asp:TextBox Style="display: none" runat="server" ID="TextBox1"></asp:TextBox>
                                    <asp:TextBox Style="display: none" runat="server" ID="txtData"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="text" name="" value="ExpRateParms" id="title" style="display: none" />   <%--csingh7 MITS 15220--%>
                    <input type="text" name="" value="" id="SysViewType" style="display: none" />
                    <input type="text" name="" value="" id="SysCmd" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdConfirmSave" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdQueue" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate" />
                    <input type="text" name="" value="" id="SysClassName" style="display: none" rmxforms:value="" />
                    <input type="text" name="" value="" id="SysSerializationConfig" style="display: none" />
                    <input type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId" />
                    <input type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId" />
                    <input type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="ExpRateParms" />
                    <input type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value="" />
                    <input type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="ExpRateParms" />
                    <input type="text" name="" value="Exposure_cid|Statecode_cid|ExpRateType_cid|FixedOrProRata_cid|EffectiveDate|"
                        id="SysRequired" style="display: none" />
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-3" id="SysWindowId" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
