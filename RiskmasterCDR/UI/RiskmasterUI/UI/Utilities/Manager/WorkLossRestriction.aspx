﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkLossRestriction.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.WorkLossRestriction" ValidateRequest = "false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>WorkLoss-Restriction Mapping</title>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">{var i;}
</script>
<script type="text/javascript" language="javascript" src="../../../Scripts/WaitDialog.js"></script>
<script type="text/javascript" language="javascript">
    function SetLOB(iLob) {
        document.forms[0].lineofbusinesscode.value = iLob;
    }
    
    function SelectRow() {
        var gridElementsRadio = document.getElementsByName('MyRadioButton');

        if (gridElementsRadio != null) {
            for (var i = 0; i < gridElementsRadio.length; i++) {
                if (gridElementsRadio[i].checked) {
                    selectedValue = gridElementsRadio[i].value;
                }
            }
        }
        document.getElementById('RowId').value = selectedValue;
    }
    function ValidateRecord() {
        if (document.getElementById('TransTypeCode_codelookup').value == "") {
            alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
            return false;
        }
        return true;

    }
    function ValidateRecordForDetetion() {
        if (document.getElementById('RowId').value == "") {
            alert("Please select a row to delete.");
            return false;
        }
        return true;

    }
</script>
</head>
<body class="10pt" onload="parent.MDIScreenLoaded();">
    <form id="frmData" name="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
    <div id="toolbardrift">
    
   </div><div class="msgheader" id="formtitle">WorkLoss-Restriction Mapping</div>
    </div>
    <br />
   <table width="100%">
    <tr>
        <td width="30%">
                <b><u>Line of Business:</u></b>
               
                <asp:RadioButtonList ID="LOBId" runat="server" RepeatDirection="Horizontal" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='LOBId']" RMXType="RadioButtonList">
                    <asp:ListItem Value="243" onclick="SetLOB(243)" Text="WC Workers' Compensation" Selected="True" o/>
                    <asp:ListItem Value="844" onclick="SetLOB(844)" Text="DI Non-Occupational Claims" />
                </asp:RadioButtonList>
        </td>
                
        <td width="20%">
                          <b><u>Record Type:</u></b>
               
                <asp:RadioButtonList ID="RecordType" runat="server" RepeatDirection="Horizontal" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='RecordType']" RMXType="RadioButtonList">
                    <asp:ListItem Value="Work Loss" Text="Work Loss" Selected="True"/>
                    <asp:ListItem Value="Restrictions" Text="Restrictions" />
                </asp:RadioButtonList>
        </td>
        
        <td width="20%">
                        <b><u>Transaction Type:</u></b>
                        <br />
                          <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);" ID="TransTypeCode" CodeTable="TRANS_TYPES" ControlName="TransTypeCode" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name='TransTypeCode']" RMXType="code" Required="true" ValidationGroup="vgSave" />
                        
                      
        </td>
     </tr>
   </table>
            <br />
   <table width="65%">
   
   <tr align="right">
         <td>
                                                                    <asp:Button runat="server" Text="  Add  " class="button" 
                                                                        ID="Add_Record" onclick="Add_Record_Click" OnClientClick="return ValidateRecord();"/>
                                                                  </td>
                                                                 
                                                                  <td> 
                                                                    <asp:Button runat="server" Text="Remove" class="button" ID="Remove_Record" onclick="Remove_Record_Click" OnClientClick="return ValidateRecordForDetetion()"
                                                                        />
                                                                </td>
   </tr>
   </table>
           
    <div style="width: 100%; height: 250; overflow: auto;">
                                                            <dg:UserControlDataGrid runat="server" ID="WorkLossRestrictionsGrid" GridName="WorkLossRestrictionsGrid"
                                                                HideButtons="New|Edit|Delete" Target="/Document/WorkLossRestrictionList"
                                                                Ref="/Document/WorkLossRestrictionList"
                                                                Unique_Id="RowId" ShowRadioButton="true" Height="350px" HideNodes="|RowId|"
                                                                ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" Type="Grid" Width="770px" Align="true"
                                                                RowDataParam="listrow" OnClick="SelectRow();"/>
                                                        </div>
           
         
   
      
    <asp:TextBox style="display:none" runat="server" id="RowId" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name='RowId']" RMXType="id" />
    <asp:HiddenField ID="lineofbusinesscode" runat="server" />
    <asp:HiddenField ID="SysFormName" runat="server" />
    </form>
</body>
</html>
