﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRPayPlan.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.BRPayPlan" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="CodeLookUp" Src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add/Modify Pay Plan</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/utilities.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>
    <%--npadhy RMSC retrofit Starts--%>

    <%--<script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
    <%--npadhy RMSC retrofit Ends--%>

     
 <%--vkumar258 - RMA-6037 - Starts --%>
<link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>


</head>
<body onload="CopyGridRowDataToPopup();">
    <form id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Add/Modify Pay Plan" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document//BRPayPlan/control[@name='RowId']"
        rmxignoreset='true' />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="AddedByUser" RMXRef="/Instance/Document//BRPayPlan/control[@name='AddedByUser']"
        type="id" />
    <asp:TextBox Style="display: none" runat="server" ID="DttmRcdAdded" RMXRef="/Instance/Document//BRPayPlan/control[@name='DttmRcdAdded']"
        type="id" />
    <asp:TextBox Style="display: none" runat="server" ID="DttmRcdLastUpd" RMXRef="/Instance/Document//BRPayPlan/control[@name='DttmRcdLastUpd']"
        type="id" />
    <asp:TextBox Style="display: none" runat="server" ID="UpdatedByUser" RMXRef="/Instance/Document//BRPayPlan/control[@name='UpdatedByUser']"
        type="id" />
    <table  cellspacing="0" cellpadding="0" border="0">
        <tbody>
         <%--<tr class="ctrlgroup2">
                <td colspan="2">
                    <asp:Label runat="server" name="BRPanPlan" ID="BRPanPlan" Text="Add/Modify Pay Plan" />
                </td>
            </tr>--%>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <asp:Label runat="server" Text="Use Pay Plan:" ID="lblUsePayPlan" />&nbsp;&nbsp;
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="UsePayPlan" RMXRef="/Instance/Document//BRPayPlan/control[@name='UsePayPlan']"
                        type="checkbox" onclick="return setDataChanged(true);" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblPayPlanCode" Text="Pay Plan Code:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="PayPlanCode" CodeTable="PAY_PLAN_CODE" ControlName="PayPlanCode"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='PayPlanCode']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblBillingRule" Text="Billing Rule:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <div title="" style="padding: 0px; margin: 0px">
                        <asp:TextBox runat="server" ID="billingrulecode" RMXRef="/Instance/Document//BRPayPlan/control[@name='BillingRule']"
                            type="text" onchange="return setDataChanged(true);" onblur="codeLostFocus(this.id);" />
                        <asp:Button runat="server" ID="btnBillingRule" Text="..." class="button" OnClientClick="return lookupBillingRule();" />
                        <asp:TextBox runat="server" ID="billingrulecode_cid" RMXRef="/Instance/Document//BRPayPlan/internal[@name='BillingRule_cid']"
                            type="text" Style="display: none" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblLOB" Text="Line of Business:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="LOB" CodeTable="POLICY_LOB" ControlName="LOB" RMXRef="/Instance/Document//BRPayPlan/control[@name='LOB']"
                        type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblState" Text="State:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="State" CodeTable="STATES" ControlName="State" RMXRef="/Instance/Document//BRPayPlan/control[@name='State']"
                        type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblLateIndicator" Text="Late Start Indicator:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="LateIndicator" CodeTable="LATE_START_IND" ControlName="LateIndicator"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='LateIndicator']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblApplyAddPremiumto" Text="Apply Addtnl Premium To:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="ApplyAddPremiumto" CodeTable="ADDTNL_PREM_INDIC"
                        ControlName="ApplyAddPremiumto" RMXRef="/Instance/Document//BRPayPlan/control[@name='ApplyAddPremiumto']"
                        type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblInstGenType" Text="Installment Generation Type:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstGenType" CodeTable="INSTALL_GEN_TYPE" ControlName="InstGenType"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstGenType']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblOffsetFrom" Text="Offset From:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="OffsetFrom" CodeTable="INSTALL_OFFSET_IND" ControlName="OffsetFrom"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='OffsetFrom']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblFrequency" Text="Frequency:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="Frequency" CodeTable="INSTALL_FREQUENCY" ControlName="Frequency"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='Frequency']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDay" Text="Generation Day:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="GenDay" CodeTable="DAY_OF_MONTH" ControlName="GenDay"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDay']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate']" RMXType="date"
                        onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="GenDatebtn" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDatebtn"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblInstall1" Text="Installment 1:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install1" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install1']"
                        MaxLength="10" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" /><br />
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblInstallType1" Text=""></asp:Label>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType1" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType1"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType1']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblGenLagDays1" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays1" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays1']"
                        MaxLength="10" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblDueDays1" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays1" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays1']"
                        MaxLength="10" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate1" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate1" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate1']" RMXType="date"
                        onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn1" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate1",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn1"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate1").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
          //  buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate1" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate1" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate1']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn1" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate1",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn1"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate1").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall2" Text="Installment 2:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install2" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install2']"
                        MaxLength="20" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType2" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType2"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType2']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays2" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays2" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays2']"
                        MaxLength="20" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays2" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays2" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays2']"
                        MaxLength="20" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate2" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate2" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate2']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn2" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate2",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn2"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate2").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate2" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate2" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate2']" RMXType="date"
                       onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn2" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate2",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn2"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate2").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall3" Text="Installment 3:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install3" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install3']"
                        MaxLength="30" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType3" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType3"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType3']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays3" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays3" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays3']"
                        MaxLength="30" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays3" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays3" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays3']"
                        MaxLength="30" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate3" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate3" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate3']" RMXType="date"
                        onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn3" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate3",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn3"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate3").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate3" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate3" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate3']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn3" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate3",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn3"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate3").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall4" Text="Installment 4:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <div title="" style="padding: 0px; margin: 0px">
                        <asp:TextBox runat="server" ID="Install4" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install4']"
                            MaxLength="40" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                            type="text" /></div>
                </td>
                <tr>
                    <td>
                    </td>
                    <td>
                        <uc:CodeLookUp runat="server" ID="InstallType4" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType4"
                            RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType4']" type="code" />
                    </td>
                </tr>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays4" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays4" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays4']"
                        MaxLength="40" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays4" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays4" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays4']"
                        MaxLength="40" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate4" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate4" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate4']" RMXType="date"
                       onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn4" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate4",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn4"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate4").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate4" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate4" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate4']" RMXType="date"
                        onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn4" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate4",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn4"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate4").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall5" Text="Installment 5:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install5" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install5']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType5" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType5"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType5']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays5" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays5" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays5']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays5" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays5" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays5']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate5" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate5" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate5']" RMXType="date"
                        onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn5" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate5",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn5"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate5").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate5" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate5" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate5']" RMXType="date"
                        onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn5" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate5",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn5"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate5").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall6" Text="Installment 6:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install6" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install6']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType6" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType6"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType6']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays6" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays6" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays6']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays6" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays6" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays6']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate6" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate6" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate6']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn6" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate6",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn6"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate6").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate6" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate6" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate6']" RMXType="date"
                      onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn6" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate6",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn6"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate6").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall7" Text="Installment 7:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install7" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install7']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType7" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType7"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType7']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays7" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays7" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays7']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays7" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays7" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays7']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate7" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate7" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate7']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn7" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate7",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn7"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate7").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate7" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate7" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate7']" RMXType="date"
                      onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn7" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate7",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn7"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate7").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall8" Text="Installment 8:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install8" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install8']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType8" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType8"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType8']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays8" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays8" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays8']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays8" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays8" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays8']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate8" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate8" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate8']" RMXType="date"
                        onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn8" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate8",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn8"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate8").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate8" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate8" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate8']" RMXType="date"
                       onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn8" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate8",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn8"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate8").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall9" Text="Installment 9:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install9" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install9']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType9" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType9"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType9']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays9" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays9" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays9']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays9" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays9" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays9']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate9" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate9" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate9']" RMXType="date"
                        onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn9" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate9",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn9"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate9").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate9" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate9" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate9']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn9" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate9",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn9"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate9").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall10" Text="Installment 10:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install10" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install10']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType10" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType10"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType10']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays10" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays10" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays10']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays10" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays10" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays10']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate10" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate10" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate10']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn10" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate10",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn10"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate10").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate10" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate10" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate10']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn10" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate10",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn10"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate10").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall11" Text="Installment 11:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install11" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install11']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType11" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType11"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType11']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays11" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays11" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays11']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays11" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays11" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays11']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate11" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate11" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate11']" RMXType="date"
                        onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn11" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate11",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn11"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate11").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate11" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate11" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate11']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn11" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate11",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn11"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate11").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInstall12" Text="Installment 12:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Install12" RMXRef="/Instance/Document//BRPayPlan/control[@name='Install12']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="InstallType12" CodeTable="INSTALL_AMT_TYPE" ControlName="InstallType12"
                        RMXRef="/Instance/Document//BRPayPlan/control[@name='InstallType12']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenLagDays12" Text="Generation Lag Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="GenLagDays12" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenLagDays12']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays12" Text="Due Days:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays12" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDays12']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);EnableDueDays(this.id);"
                        type="text" />
                </td>
            </tr>
            <%--npadhy RMSC retrofit Starts--%>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblGenDate12" Text="Generation Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="GenDate12" RMXRef="/Instance/Document//BRPayPlan/control[@name='GenDate12']" RMXType="date"
                        onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="GenDateBtn12" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "GenDate12",
				                ifFormat: "%m/%d/%Y",
				                button: "GenDateBtn12"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#GenDate12").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDate12" Text="Due Date:"></asp:Label>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="DueDate12" RMXRef="/Instance/Document//BRPayPlan/control[@name='DueDate12']" RMXType="date"
                         onchange="setDataChanged(true);EnableGenDueDate(this.id);" onblur="PayPlanDateLostFocus(this.id);" />
                   <%-- <asp:Button class="DateLookupControl" runat="server" ID="DueDatebtn12" TabIndex="3" />

                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "DueDate12",
				                ifFormat: "%m/%d/%Y",
				                button: "DueDatebtn12"
				            }
				            );
                    </script>--%>
                    <%--vkumar258 - RMA-6037 - Starts --%>
<script type="text/javascript">
    $(function () {
        $("#DueDate12").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
        </tbody>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return BRPayPlan_onOk();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return BRPayPlan_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>

<script type="text/javascript" language="javascript">
    function codeLostFocus(sCtrlName) {


        var objFormElem = eval('document.forms[0].' + sCtrlName);
        if (objFormElem.value == "") {
            objFormElem = eval('document.forms[0].' + sCtrlName + "_cid");
            objFormElem.value = "0";
            m_LookupTextChanged = false;
        }
        else {
            if (m_LookupTextChanged) {
                var objFormElemButton = eval('document.forms[0].' + sCtrlName + "btn");
                objFormElemButton.click()

            }
            else {
                objFormElem = eval('document.forms[0].' + sCtrlName + "_cid");
                if (objFormElem.value == "0" || objFormElem.value == "") {
                    var objFormElem = eval('document.forms[0].' + sCtrlName);
                    objFormElem.value = "";
                }
            }
        }
        if (sCtrlName == "InstGenType_codelookup")
            {
                EnableDisable();
            }
        else if (sCtrlName == "GenDay_codelookup") 
        {
            EnableDueDays('GenDay_codelookup');        
        }
        return true;
    }
    function codeSelected(sCodeText, lCodeId) 
    {
        var objCtrl = null;
        if (m_codeWindow != null)
            m_codeWindow.close();
        objCtrl = eval('document.forms[0].' + m_sFieldName);
        objCtrl.cancelledvalue = sCodeText;
        objCtrl.value = sCodeText;
        objCtrl = eval('document.forms[0].' + m_sFieldName + "_cid");
        objCtrl.value = lCodeId;
        objCtrl.cancelledvalue = lCodeId;
        setDataChanged(true);
        if (m_sFieldName == "InstGenType_codelookup") 
        {
            EnableDisable();
        }
        else if (m_sFieldName=="GenDay_codelookup") 
        {
            EnableDueDays('GenDay_codelookup');        
        }
        return true;
    }
    EnableDisable();

</script>

</html>
