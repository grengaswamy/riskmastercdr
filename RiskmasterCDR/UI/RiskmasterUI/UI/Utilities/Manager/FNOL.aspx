﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FNOL.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.FNOL" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"  TagPrefix="mc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <script type="text/javascript">
         function ValidateAdd() {
             if (document.forms[0].lstReserveType.value == "")
             {
                 alert(FNOLValidations.ReserveExist);
                 return false;
              }
         }  
      </script>
   <%--//dbisht6 starts for mits 35537--%>
       <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>

    

    <script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <%--//dbisht6 end--%>
   

</head>
<body>
  <form id="frmData" runat="server">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  <table width="100%" cellspacing="0" cellpadding="0">
		
		<tr class="msgheader">
		<!-- Label control added by Shivendu for MITS 16148 -->
		    <td colspan="2"><asp:Label ID="lblReservepage" runat="server" Text="Label"></asp:Label></td>
		</tr>
	</table>
	<br/>
    <table width="100%" cellspacing="0" cellpadding="0">
		<tr>
            <td>
            <asp:Label ID="lblReserveType" runat="server" Text = "Reserve Type"/>
                
            </td>
            <td>
                <asp:DropDownList ID="lstReserveType" type="combobox"  runat="server" />
            </td>
        </tr>
        <tr>
            <td>
            <asp:Label ID="lblReserveAmnt" runat="server" Text = "Reserve Amount"/>
                
            </td>
            <td>              
                    <mc:CurrencyTextbox runat="server" size="30" onchange="setDataChanged(true);" id="txtReserveAmnt"
                        maxlength="50"  />
            </td>
        </tr>
    </table> 

    <br/>
    <div class="divScroll" style="width: 100%; height: 42%;">
    <table width="100%" cellspacing="0" cellpadding="0">
		<tr class="ctrlgroup">
		    <asp:GridView ID="GridViewReserveStatus" AutoGenerateColumns="False" runat="server" Font-Bold="True" 
		    CellPadding="0" GridLines="None" CellSpacing="0" Width="100%"  EmptyDataText ="No Reserve Available" >
		    <HeaderStyle CssClass="ctrlgroup"/>
            <rowstyle CssClass ="datatd" HorizontalAlign="Left" Font-Bold="false"  />
            <alternatingrowstyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />

                <Columns>
                    <asp:TemplateField HeaderText="Reserve Type" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("ReserveType")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reserve Amount" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("ReserveAmount")%>
                        </ItemTemplate> 
                    </asp:TemplateField>
                </Columns> 
            </asp:GridView>     
		</tr>
	</table> 
    </div>
    
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
            <td>
			    <asp:Button  class="button" id="btnOK" Text =" OK " runat="server" OnClick="addReserveBtn_Click" OnClientClick = "return ValidateAdd();" Width ="50px" />
				&#160;
				<asp:Button  class="button" id="btnCancel" Text =" Cancel " runat="server" OnClientClick ="window.close();" Width ="50px" />
				&#160;
			</td>
		</tr>
	</table>
     <asp:HiddenField  runat= "server" ID="hdnRowIDValue" />
  </form>
    
</body>
</html>
