﻿<%@ Page Title="" Language="C#" theme="RMX_Default" MasterPageFile="~/UI/Utilities/UtilityTemplate.Master" AutoEventWireup="true" CodeBehind="NewPaymentNotificationSetup.aspx.cs" Inherits="Riskmaster.UI.Utilities.NewPaymentNotificationSetup" %>
<asp:content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    
    <title>Payment Notification Setup</title>

</asp:content>
<asp:content ID="Content2" ContentPlaceHolderID="cphUtilityBody" runat="server">

    <table border="0" width="400px">
    
        <tr>
            <td colspan="2">
                <asp:imagebutton id="btSave" name="btSave" imageurl="~/Images/save.gif" runat="server" />
            </td>
        </tr>
    
        <tr>
            <td colspan="2" class="msgheader">
                Payment Notification Setup
            </td>
        </tr>
        
        <tr>
            <td colspan="2" class="ctrlgroup">
                Payment Notification Setup
            </td>
        </tr>
        
        <tr>
            <td>
                Line of Business :
            </td>
            <td>
                <asp:dropdownlist id="ddlLineOfBusiness" name="ddlLineOfBusiness" runat="server" />
            </td>
        </tr>
        
        <tr>
            <td>
                User to notify :
            </td>
            <td>
                <asp:dropdownlist id="ddlUserToNotify" name="ddlUserToNotify" runat="server" />
            </td>
        </tr>
    
        <tr>
            <td>
                Notify period :
            </td>
            <td>
                <asp:dropdownlist id="ddlNotifyPeriod" name="ddlNotifyPeriod" runat="server" />
            </td>
        </tr>
        
        <tr>
            <td>
                Date Criteria :
            </td>
            <td>
                <asp:dropdownlist id="ddlDateCriteria" name="ddlDateCriteria" runat="server" />
            </td>
        </tr>
    </table>

</asp:content>
