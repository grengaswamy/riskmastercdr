﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.Xml;
using Riskmaster.UI.Shared.Controls;

///////////////////////////////
// Developed By: abansal23
// Completed On: 6th Feb, 2009
///////////////////////////////
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class ExpRateParms : NonFDMBasePageCWS
    {
        private XmlDocument oFDMPageDom = null;
        XElement XmlTemplate = null;
        string sCWSresponse = string.Empty  ;
        XmlDocument XmlDoc = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes


                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    EditAddMode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");

                    // Added By Amit Bansal
                    if (EditAddMode.Text.ToLower() != "add")
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("ExpRateParmsAdaptor.Get", XmlTemplate, out sCWSresponse, false, false);
                        BindControls();
                    }
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control CtlPostBack = DatabindingHelper.GetPostBackControl(this.Page);

                    if (CtlPostBack == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("ExpRateParmsAdaptor.Get", XmlTemplate, out sCWSresponse, false, false);
                        BindControls();
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            if (EditAddMode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("ExpRateParmsAdaptor.Get");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><ExpRates>");
            sXml = sXml.Append("<amount>" + Amount.Text + "</amount>");
            //Added bY Tushar for MITS 18231
            sXml = sXml.Append("<LINEOFBUSINESS codeid='" + explob.CodeIdValue + "'>" + explob.CodeText + "</LINEOFBUSINESS>"); 
            sXml = sXml.Append("<RateMaintenanceType>E</RateMaintenanceType>");
            //Added bY Tushar for MITS 18231 Ends
            sXml = sXml.Append("<exposureid codeid='" + Exposure.CodeIdValue + "'>" + Exposure.CodeTextValue + "</exposureid>");
            sXml = sXml.Append("<effectivedate>" + EffectiveDate.Text + "</effectivedate>");
            sXml = sXml.Append("<expirationdate>" + ExpirationDate.Text + "</expirationdate>");
            sXml = sXml.Append("<expratetype codeid='" + ExpRateType.CodeIdValue + "'>" + ExpRateType.CodeTextValue + "</expratetype>");
            sXml = sXml.Append("<state  codeid='" + Statecode.CodeIdValue + "'>" + Statecode.CodeTextValue + "</state>");
            sXml = sXml.Append("<fixedorprorate codeid='" + FixedOrProRata.CodeIdValue + "'>" + FixedOrProRata.CodeTextValue + "</fixedorprorate >");
            sXml = sXml.Append("<baserate>" + BaseRate.Text + "</baserate>");
            //Added bY Tushar for MITS 18231
            sXml = sXml.Append("<OrgHierarchyCode codeid='" + OrgHierarchyCode_cid.Text + "'>" + OrgHierarchyCode_cid.Text + "</OrgHierarchyCode >");
            //Added bY Tushar for MITS 18231 End
            sXml = sXml.Append("<expraterowid>" + RowId.Text + "</expraterowid>");
            if (EditAddMode.Text.ToLower() == "edit")
            {
                sXml = sXml.Append("<EditAddMode>" + EditAddMode.Text + "</EditAddMode>");
            }
            else
            {
                sXml = sXml.Append("<EditAddMode />");
            }
            sXml = sXml.Append("</ExpRates></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("ExpRateParmsAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    //After the validation,there was a need to post back the parent page through javascript 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Binds data to the controls
        /// </summary>
        internal void BindControls()
        {
            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

           XmlElement objExpRatesXMLEle = null;
           objExpRatesXMLEle = (XmlElement)oFDMPageDom.SelectSingleNode("//ExpRates");


           Amount.Text = objExpRatesXMLEle.GetElementsByTagName("amount").Item(0).InnerText;
            //Added BY Tushar for MITS 18231
           ((TextBox)explob.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("LINEOFBUSINESS").Item(0).InnerText;
           ((TextBox)explob.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("LINEOFBUSINESS").Item(0).Attributes["codeid"].Value;
            //End

           ((TextBox)Exposure.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("exposureid").Item(0).InnerText;
           ((TextBox)Exposure.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("exposureid").Item(0).Attributes["codeid"].Value;
           EffectiveDate.Text = objExpRatesXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
           ExpirationDate.Text = objExpRatesXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;
           ((TextBox)ExpRateType.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("expratetype").Item(0).InnerText;
           ((TextBox)ExpRateType.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("expratetype").Item(0).Attributes["codeid"].Value;
           ((TextBox)Statecode.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("state").Item(0).InnerText;
           ((TextBox)Statecode.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("state").Item(0).Attributes["codeid"].Value;
           ((TextBox)FixedOrProRata.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("fixedorprorate").Item(0).InnerText;
           ((TextBox)FixedOrProRata.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("fixedorprorate").Item(0).Attributes["codeid"].Value;
           //Added bY Tushar for MITS 18231
           ((TextBox)OrgHierarchyCode.FindControl("OrgHierarchyCode")).Text = objExpRatesXMLEle.GetElementsByTagName("OrgHierarchyCode").Item(0).InnerText;
           ((TextBox)OrgHierarchyCode.FindControl("OrgHierarchyCode_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("OrgHierarchyCode").Item(0).Attributes["codeid"].Value;
           //Added bY Tushar for MITS 18231 End
           BaseRate.Text = objExpRatesXMLEle.GetElementsByTagName("baserate").Item(0).InnerText;
        }
    }
}
