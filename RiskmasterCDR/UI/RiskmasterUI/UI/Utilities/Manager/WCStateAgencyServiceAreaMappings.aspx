﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WCStateAgencyServiceAreaMappings.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.WCStateAgencyServiceAreaMappings1" EnableEventValidation="false" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head id="Head1" runat="server">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>WC State Agency Service Area Mapping</title>
      <uc:CommonTasks id="CommonTasks1" runat="server" />
      <link rel="stylesheet" href="../../../Content/system.css" type="text/css"/>
      <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css"/>

      <script type="text/javascript">
                function pageLoaded() {
                    var value = document.forms[0].hdNotSupported.value;
                    var valueoffice = document.forms[0].hdNoOffice.value
                    if (value != "") {
                        alert(value);
                        document.forms[0].hdNotSupported.value = '';
                    }
                    if (valueoffice != "") {
                        alert(valueoffice);
                        document.forms[0].hdNotSupported.value = '';
                    }
                }
                
				function trim(value)
				{
					value = value.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
					return value;
				}
				function removeOption(selectName,id) {
					select=document.getElementById(selectName);
					Ids=new Array();
					Names=new Array();

					for(var x = 0; x < select.options.length ; x++)
					{
						if(select.options[x].value!=id)
						{
							Ids[Ids.length] = select.options[x].value;
							Names[Names.length]=select.options[x].text;
						}
					}

					select.options.length = Ids.length;

					for(var x = 0;x < select.options.length; x++)
					{
						select.options[x].text=Names[x];
						select.options[x].value=Ids[x];
					}
				}
				function AddFilter(mode) {
					var optionRank;
					var optionObject;
						
					selectObject=document.getElementById('lstJurisdictionalTableName');
						
					if (mode=="selected")
					{
						//Add selected Available Values
						select=document.getElementById('lstServiceAreaTableName');
						for(var x = 0; x < select.options.length ; x++)
						{
							if(select.options[x].selected == true)
							{
								//add to selected list
								optionObject = new Option(select.options[x].text,select.options[x].value);
								optionRank = selectObject.options.length;
								selectObject.options[optionRank]=optionObject;
									
									//remove from available list	
									removeOption('lstServiceAreaTableName',select.options[x].value);							
								}
							}
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('lstServiceAreaTableName');
							if (select.options.length > 0)
							{
								for(var x = 0; x < select.options.length ; x++)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
								}
							select.options.length=0;
							}
					    }
					return false;
				}
					
				function RemoveFilter(mode)
				{
					var optionRank;
					var optionObject;
						
					selectObject=document.getElementById('lstServiceAreaTableName');
						
					if (mode=="selected")
					{
						//Add selected Available Values
						select=document.getElementById('lstJurisdictionalTableName');
						for(var x = 0; x < select.options.length ; x++)
						{
							if(select.options[x].selected == true)
							{
								//add to selected list
								optionObject = new Option(select.options[x].text,select.options[x].value);
								optionRank = selectObject.options.length;
								selectObject.options[optionRank]=optionObject;
									
								//remove from available list	
								removeOption('lstJurisdictionalTableName',select.options[x].value);							
							}
						}
					}
					else
					{
						//Add All Available Values
						select=document.getElementById('lstJurisdictionalTableName');
						if (select.options.length > 0)
						{
							for(var x = 0; x < select.options.length ; x++)
							{
								//add to selected list
								optionObject = new Option(select.options[x].text,select.options[x].value);
								optionRank = selectObject.options.length;
								selectObject.options[optionRank]=optionObject;
							}
							select.options.length=0;
						}
					}
					return false;
				}

				var ns, ie, ieversion;
				var browserName = navigator.appName;                   // detect browser 
				var browserVersion = navigator.appVersion;
				if (browserName == "Netscape")
				{
					ie=0;
					ns=1;
				}
				else		//Assume IE
				{
			        ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
				    ie=1;
				    ns=0;
				}
				function DeletingAgents()
				{
				  if (trim(document.forms[0].lstAgents.value)=="")
					{
						alert("Please select a Agents before deleting an agent.");
						return false;
					}
				}
				function AddAgents()
				{
					if (trim(document.forms[0].lstJurisdictions.value).length==0)
					{
						alert("Please select Jurisdictions.");
						return false;
					}
					if (trim(document.forms[0].lstOfficeFunction.value).length==0)
					{
						alert("Please select Function.");
						return false;
					}
					if (trim(document.forms[0].txtBusinessName.value) == "") {
					    alert("Business Name is a required.");
					    document.forms[0].txtBusinessName.focus();
					    return false;
					}
					
				}
				function DeleteAgents()
				{
					if (trim(document.forms[0].lstAgents.value)=="")
					{
						alert("Please select an agent before deleting an agent.");
						return false;
					}
				}
				function ValidationJurisdiction()
				{
				    if (trim(document.forms[0].lstJurisdictions.value)=="" || trim(document.forms[0].lstOfficeFunction.value)=="")
					{
						alert("Please select a Jurisdictions and Function before adding a new Postal Code.");
						return false;
				    }

//                  zmohammad MITS 31598 : commenting out this validation as there is already one attached to the textbox which is working for ML. 
//					if (id==1)
//					{
//						if (trim(document.forms[0].txtJurisCodeAdd.value).length!=6)
//						{
//							alert("Only 5 digit Postal Codes are permitted.");
//							document.forms[0].txtJurisCodeAdd.focus();
//							return false;
//						}
//					}
//					if (id==2)
//					{
//						if (trim(document.forms[0].txtJurisCodDelete.value).length!=6)
//						{
//							alert("Only 5 digit Postal Codes are permitted.");
//							document.forms[0].txtJurisCodDelete.focus();
//							return false;
//			            }
//			    	}

					return true;
				}
				function AddNew() {

					document.forms[0].txtBusinessName.value="";
					document.forms[0].txtAddress1.value="";
					document.forms[0].lstAgents.selectedIndex=0;
					document.forms[0].txtAddress2.value="";
					document.forms[0].txtContact.value="";
					document.forms[0].txtCity.value="";
					document.forms[0].txtContactPhone.value="";
					document.forms[0].txtState.value="";
					document.forms[0].txtPostCode.value = "";
					document.forms[0].hdnSelected.value = "Add New";					
					return false;
				}
				function SelectAll() {

				    var values = "";
				    lstServiceAreaTableName1 = document.getElementById('lstServiceAreaTableName');

				    if (trim(document.forms[0].lstAgents.value) == '') 
				    {
				        alert("Please select Agents");
				        return false;
				    }
			    for(var f=0;f<document.forms[0].lstServiceAreaTableName.options.length;f++)
			    {
			       document.forms[0].lstServiceAreaTableName.options[f].selected = true;
				   values += document.forms[0].lstServiceAreaTableName.options[f].value + "=" + document.forms[0].lstServiceAreaTableName.options[f].text + ";";
			    }
			    document.forms[0].hndlstServiceAreaTableName.value +=values;		   
			    	
			    values="";
			    for (var f = 0; f < document.forms[0].lstJurisdictionalTableName.options.length; f++)
			    {
			        values += document.forms[0].lstJurisdictionalTableName.options[f].value + "=" + document.forms[0].lstJurisdictionalTableName.options[f].text + ";";
			    }
			    document.forms[0].hndlstJurisdictionalTableName.value += values;
			      return true;

			}	
					
					
				function trim(value)
				{
				  value = value.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
				  return value;
				}
				function Selected()
				{
				    var acordForm=document.forms[0].lstACORDForm.value;
				    var ClaimType=document.forms[0].lstClaimType.value;
				    if (trim(acordForm)==""  || trim(ClaimType)=="")
					{
						 alert("Acord Form and Claim Type must be selected before adding to mappings.");	
						 return false;
					}
					return true;
				}
				
				function SelectionValid()
				{
					for(var i = 0; i<document.forms[0].elements.length;i++) 
						if(document.forms[0].elements[i].type=='checkbox') 
							if(document.forms[0].elements[i].checked == true)
								return true;

					return false;
				}
				function submitValue(ctr)
				{
					if (ctr=="OfficeFunction")
					{
						document.forms[0].lstAgents.selectedIndex=0;
					}
					if (ctr=="Jurisdictions")
					{
						document.forms[0].lstAgents.selectedIndex=0;
						document.forms[0].lstOfficeFunction.selectedIndex=0;
					}
					document.forms[0].hdnSelected.value='';
					document.forms[0].submit();
				}
				function Find(ctr)
				{
				    if (trim(document.forms[0].txtFindZip.value) == '') {
				        alert("Please enter zip");
				        document.forms[0].txtFindZip.focus();
				        ctr = '';
				        return false;
				    }
				    document.forms[0].hdnFind.value = ctr;
				}
			</script>
      <style type="text/css">
          .style1
          {
              width: 155px;
          }
          .style2
          {
              width: 134px;
          }
      </style>
 </head>
 <body onload="pageLoaded();parent.MDIScreenLoaded();">
  <form id="frmData" name="frmData" runat ="server" method="post">
    <uc1:ErrorControl id="ErrorControl" runat="server" />
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""/>
    <table width="90%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="msgheader" colspan="4">Agent Service Area</td>
        </tr>
        <tr>
        <td class="ctrlgroup" colspan="4">Agents</td>
        </tr>
   </table>
   <asp:TextBox runat="server" type="text" style="display:none" name=""  value="" id="hdnSelected"/>
   <asp:TextBox runat="server" style="display:none" id="hdJurisSelected" />
   <asp:TextBox runat="server" style="display:none" id="hdOfficeSelected" />
   <asp:TextBox runat="server" style="display:none" ID="hdAgentsSelected" />
   <asp:TextBox runat="server" style="display:none" ID="hdnFind" />
   <asp:TextBox runat="server" style="display:none" ID="hdFindZip" />
   <asp:TextBox runat="server" style="display:none" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='Jurisdictions']/@NotSupported" id="hdNotSupported" />
   <asp:TextBox runat="server" style="display:none" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='NoOfficeSelected']/@NoOffice" id="hdNoOffice" />
   <table enableviewstate="true" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
     <td width="15%">Jurisdictions</td>
     <td width="15%"><asp:DropDownList runat ="server" id="lstJurisdictions" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='Jurisdictions']" ItemSetRef="/Instance/Document/WorkCompAgentServiceArea/control[@name='Jurisdictions']" onChange="submitValue('Jurisdictions');" EnableViewState="true" OnSelectedIndexChanged="DrpGet" style="width:200px"></asp:DropDownList>
       
      </td>
     <td width="2%">&nbsp;</td>
     <td width="20%">Business Name</td>
     <td width="15%"><asp:TextBox runat="server" name="" value="" id="txtBusinessName" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='BusinessName']"/></td>
     <td width="*">&nbsp;</td>
    </tr>
    <tr>
     <td width="15%">Function</td>
     <td width="15%"><asp:DropDownList runat="server" id="lstOfficeFunction" rmxref ="Instance/Document/WorkCompAgentServiceArea/control[@name='OfficeFunction']" ItemSetRef="/Instance/Document/WorkCompAgentServiceArea/control[@name='OfficeFunction']" onChange="submitValue('OfficeFunction');" OnSelectedIndexChanged="DrpGet" style="width:200px">
       </asp:DropDownList>
     </td>
     <td width="2%">&nbsp;</td>
     <td width="15%">Address</td>
     <td width="15%"><asp:TextBox runat="server" name="" value="" class="text" id="txtAddress1" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='Address1']"/></td>
     <td width="*">&nbsp;</td>
    </tr>
    <tr>
     <td width="15%">Agents</td>
     <td width="15%"><asp:DropDownList runat="server" id="lstAgents" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='Agents']" ItemSetRef="/Instance/Document/WorkCompAgentServiceArea/control[@name='Agents']"  onChange="submitValue('Agents');" OnSelectedIndexChanged="DrpGet" style="width:200px" >
       </asp:DropDownList></td>
     <td width="2%">&nbsp;</td>
     <td width="15%">&nbsp;</td>
     <td width="15%"><asp:TextBox runat="server" name="" value="" class="text" id="txtAddress2" rmxref ="/Instance/Document/WorkCompAgentServiceArea/control[@name='Address2']" /></td>
     <td width="*">&nbsp;</td>
    </tr>
    <tr>
     <td width="15%">Contact</td>
     <td width="15%"><asp:TextBox runat="server" name="" value="" class="text" id="txtContact" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='Contact']"/></td>
     <td width="2%">&nbsp;</td>
     <td width="15%">&nbsp;</td>
     <td width="15%"><asp:TextBox runat="server" name="" value="" class="text" id="txtAddress3" rmxref ="/Instance/Document/WorkCompAgentServiceArea/control[@name='Address3']" /></td>
     <td width="*">&nbsp;</td>
    </tr>
    <tr>
     <td width="15%">Phone</td>
     <td width="15%"><asp:TextBox runat="server" name="" value="" class="text" id="txtContactPhone" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='ContactPhone']" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);"/></td>
     <td width="2%">&nbsp;</td>
     <td width="15%">&nbsp;</td>
     <td width="15%"><asp:TextBox runat="server" name="" value="" class="text" id="txtAddress4" rmxref ="/Instance/Document/WorkCompAgentServiceArea/control[@name='Address4']" /></td>
     <td width="*">&nbsp;</td>
    </tr>
    <tr>
     <td width="15%">&nbsp;</td>
     <td width="15%">&nbsp;</td>
     <td width="2%">&nbsp;</td>
     <td width="15%">City</td>
     <td width="15%"><asp:TextBox runat="server" name="" value="" class="text" id="txtCity" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='City']"/></td>
     <td width="*">&nbsp;</td>
    </tr>
    <tr>
     <td width="15%">&nbsp;</td>
     <td width="15%">&nbsp;</td>
     <td width="2%">&nbsp;</td>
     <td width="15%">State</td>
     <td width="15%"><asp:TextBox runat="server" name="" value="" class="text" id="txtState" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='State']"/></td>
     <td width="*">&nbsp;</td>
    </tr>
    <tr>
     <td width="15%">&nbsp;</td>
     <td width="15%">
      							&nbsp;
      						
     </td>
     <td width="2%">&nbsp;</td>
     <td width="*">&nbsp;</td>
    </tr>
    <tr>
     <td class="ctrlgroup" colspan="2">Zip?</td>
     <td width="2%">&nbsp;</td>
     <td width="15%">Postal Code</td>
     <td width="15%"><asp:TextBox runat="server" name="" value="" class="text" id="txtPostCode" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='PostCode']" onchange="setDataChanged(true);" onblur="zipLostFocus(this);"/></td>
     <td width="*">
      						&nbsp;
      						
     </td>
    </tr>
   </table>
   <table width="90%" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td width="15%"><asp:Textbox runat="server" name="" value="" class="text" id="txtFindZip" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='FindZip']" onchange="setDataChanged(true);" onblur="zipLostFocus(this);"/></td>
     <td width="15%"><asp:Button class="button" ID="FindZip" runat="server" text="Find" value="Find" onclick="FindZipCode" onclientclick="return Find('Find');" Width="90px" /></td>
     <td width="2%">&nbsp;</td>
     <td width="15%">&nbsp;</td>
     <td class="style1">
      							&nbsp;
      						
     </td>
     <td width="*">&nbsp;</td>
    </tr>
    <tr>
     <td width="15%"><asp:TextBox runat="server" name="" value="" class="text" id="txtAgentsByZip" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='AgentsByZip']/option"/></td>
     <td width="15%">&nbsp;</td>
     <td width="16%">&nbsp;</td>
     <td colspan="2"><asp:Button class="button" ID="AddNewAgent" runat="server" name="" text="Add New" value="Add New" OnClientClick="return AddNew();" Width="90px"/>&nbsp;&nbsp;
      						<asp:Button class="button" ID="DeleteAgent" runat="server" name="" text="Delete" value="Delete" OnClick="DeleteAgents" onClientClick="return DeleteAgents();" Width="90px"/>&nbsp;&nbsp;
      						<asp:Button class="button" ID="SaveAgent" runat="server" name="" text="Save" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" OnClick="SaveAgents" onClientClick="return AddAgents();" Width="90px" /></td>
     <td width="*">
      						&nbsp;
      						
     </td>
    </tr>
   </table>
   <table width="90%" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
     <td class="ctrlgroup" colspan="4">Postal Codes</td>
    </tr>
   </table>
   <table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
     <td width="5%" nowrap="true"><b>Service Area Postal Code</b></td>
     <td class="style2"></td>
     <td width="*" nowrap="true"><b>Jurisdictional Postal Code</b></td>
    </tr>
    <tr>
     <td width="5%" nowrap="true"><asp:ListBox runat="server" name="" id="lstServiceAreaTableName" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='ServiceAreaTableName']" style="width:90px" Height="180px"  size="10" SelectionMode="Multiple" EnableViewState="false"   rmxignoreset="true" ></asp:ListBox>
      <asp:HiddenField  ID ="hndlstServiceAreaTableName" runat="server" />
      <asp:TextBox runat ="server" ID="txtlstServiceAreaTableName" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='ServiceAreaTableName']" rmxignoreget="true" style="display :none"></asp:TextBox>
     
     </td>
     <td nowrap="true" align="center" class="style2">
        <asp:Button class="button" ID="AddCode" runat="server" name="" value="Add &gt;" text="Add >"  onClientClick="return AddFilter('selected');; " Width="90px"/>
        <br/><br/>
        <asp:Button class="button" ID="AddAllCode" runat="server" name="" value="Add All &gt;" text="Add All >"  onClientClick="return AddFilter('all');; " Width="90px"/>
        <br/><br/>
        <asp:Button class="button" ID="RemoveCode" runat="server" name="" value="< Remove" text="< Remove"  onClientClick="return RemoveFilter('selected');; " Width="90px"/>
        <br/><br/>
        <asp:Button class="button" ID="RemoveAllCodes" runat="server" name="" value="< Remove All" text="< Remove All" Width="90px" onClientClick="return RemoveFilter('all');; "/>
     </td>
     <td nowrap="true" width="*">
         <asp:ListBox runat="server" name="" id="lstJurisdictionalTableName" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='JurisdictionalTableName']" style="width:90px" Height="180px" size="10" rmxignoreset="true" EnableViewState="false" SelectionMode="Multiple" ></asp:ListBox>
         <asp:HiddenField  ID ="hndlstJurisdictionalTableName" runat="server" />  
         <asp:TextBox runat ="server"  ID="txtlstJurisdictionalTableName" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='JurisdictionalTableName']"  rmxignoreget="true" style="display :none"></asp:TextBox>
     </td>
     <td valign="top">
      <table>
       <tr>
        <td class="ctrlgroup" colspan="2">New Jurisdictional Postal Code</td>
       </tr>
       <tr>
        <td><asp:TextBox runat="server" name="" value="" class="text" id="txtJurisCodeAdd" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='JurisCodeAdd']" onblur="zipLostFocus(this);"/>
        <asp:Button class="button" ID="AddJurisCode" runat="server" name="" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Text="Add" OnClick="AddJurisPostalCodes" onClientClick="return ValidationJurisdiction();" Width="90px"/>
        </td>
       </tr>
       <tr>
        <td class="ctrlgroup" colspan="2">Delete Jurisdictional Postal Code</td>
       </tr>
       <tr>
        <td><asp:TextBox runat="server" name="" value="" class="text" id="txtJurisCodDelete" rmxref="/Instance/Document/WorkCompAgentServiceArea/control[@name='JurisCodDelete']"  onchange="setDataChanged(true);" onblur="zipLostFocus(this);"/>
        <asp:Button class="button" ID="DeleteJurisCode" runat="server" name="" value="&nbsp;&nbsp;&nbsp;Delete&nbsp;&nbsp;&nbsp;" Text="Delete" OnClick="DeletePostalCode" onClientClick="return ValidationJurisdiction();" Width="90px"/>
        </td>
       </tr>
       <tr>
        <td>&nbsp;</td>
       </tr>
       <tr>
        <td>&nbsp;</td>
       </tr>
       <tr>
        <td nowrap="true"><asp:Button class="button" ID="SaveCode" runat="server" name="" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Text="Save" OnClick="SavePostalCode" onClientClick="return SelectAll();" Width="90px"/></td>
        <td nowrap="true" width="*">
         							&nbsp;
         							
        </td>
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td></td>
    </tr>
   </table>
   <input type="hidden" name="" value="rmx-widget-handle-3" id="SysWindowId"/>
   </form>
 </body>
</html>