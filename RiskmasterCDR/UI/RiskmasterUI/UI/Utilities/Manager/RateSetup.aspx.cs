﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.AppHelpers;


///<Summary>
// Developed By: abansal23
// Completed On: 6th Feb, 2009
///</Summary>
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class RateDisc : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = string.Empty  ;
        XmlDocument XmlDoc = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");

                    // Added By Amit Bansal
                    if (mode.Text.ToLower() != "add")
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("SelectedDiscAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);
                    }
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control CtlPostBack = DatabindingHelper.GetPostBackControl(this.Page);

                    if (CtlPostBack == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("SelectedDiscAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Creates Message Template for input xml
        /// </summary>
        /// <returns>Message Element</returns>
        private XElement GetMessageTemplate()
        {
            if (mode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SelectedDiscAdaptor.Get");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><SelectedDisc>");
            sXml = sXml.Append("<control name='RowId' type='id'>" + RowId.Text + "</control>");
            sXml = sXml.Append("<control name='UseDiscTier' type='checkbox'>" + UseDiscTier.Checked + "</control>");
            sXml = sXml.Append("<control name='Override' type='checkbox'>" + Override.Checked + "</control>");
            sXml = sXml.Append("<control name='LOBcode' type='code' codetable='POLICY_LOB' codeid='" + LOBcode.CodeIdValue + "'>" + LOBcode.CodeTextValue + "</control>");
            sXml = sXml.Append("<control name='Statecode' type='code' codetable='states' codeid='" + Statecode.CodeIdValue + "'>" + Statecode.CodeTextValue + "</control>");
            sXml = sXml.Append("<control name='DiscountName' codeid='" + CoverageName_cid.Text + "'>" + CoverageName.Text.Replace("&", "&amp;") + "</control>");
            sXml = sXml.Append("<control name='ParentLevel' codeid='" + ParentLevel_cid.Text + "'>" + ParentLevel.Text.Replace("&", "&amp;") + "</control>");
            sXml = sXml.Append("<control name='ISRateSetup' type='id'>1</control>");
            sXml = sXml.Append("<control name='FormTitle' type='text' />"); 
            sXml = sXml.Append("<control name='AddedByUser'/><control name='DttmRcdAdded'/><control name='DttmRcdLastUpd'/><control name='UpdatedByUser'/>");

            sXml = sXml.Append("</SelectedDisc></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("SelectedDiscAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    //After the validation,there was a need to post back the parent page through javascript 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
