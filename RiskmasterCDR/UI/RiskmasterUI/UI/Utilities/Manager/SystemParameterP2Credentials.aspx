﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SystemParameterP2Credentials.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.SystemParameterP2Credentials" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Patriot Protector Credentials</title>
    
    
      <script language="javascript" type="text/javascript" src="../../../Scripts/Utilities.js"></script>
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
</head>
<body class="10pt" onload="P2Credentials_Load();">
  <form id="frmData" runat="server" >
  <div class="msgheader" id="formtitle">Patriot Protector Credentials</div>
   <div class="errtextheader"></div>
   <table border="0">
    <tbody>
     <tr>
      <td>
       <table border="0">
        <tbody>
         <tr>
          <%--<td class="ctrlgroup" colSpan="2">Patriot Protector Credentials</td>--%>
         </tr>
         <tr>
          <td colspan="2" width="100%">
           <table border="0" cellpadding="0" cellspacing="0">
            <tr>
             <tr bgcolor="">
              <td colspan="5"><b><FONT color="">You must have credentials assigned by Patriot Protector product support before integrated Patriot Protector Services can be
                 used.</FONT></b></td>
             </tr>
             <tr>
              <td colspan="5">&nbsp;</td>
             </tr>
            </tr>
           </table>
          </td>
         </tr>
         <tr>
          <td colspan="2">
           <table border="0" cellpadding="0" cellspacing="0">
            <tr>
             <td>Please enter the User ID and Password provided into the boxes below</td>
            </tr>
           </table>
          </td>
         </tr>
         <tr>
          <td>User ID:&nbsp;&nbsp;</td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
               <asp:TextBox ID="User"  onchange="setDataChanged(true);" type="text" value="" size="30" runat="server"></asp:TextBox>
          </div>
          </td>
         </tr>
         <tr>
          <td>Password:&nbsp;&nbsp;</td>
          <td>
              <input id="Pwd"  value="" size="30" type="password" runat="server" autocomplete="off"/>
            
          </td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td>
          <script language="JavaScript" type="text/javascript" >
          {var i;}
          </script>
          <input type="button" class="button" name="Ok" value="  OK  " onClick="SystemParameterP2_Ok();" id="Ok" />
          <script language="JavaScript" type="text/javascript" SRC="">
          {var i;}
          </script>
          <input type="button" class="button" name="Cancel" value="Cancel" onClick="SystemParameterP2_Cancel();" id="Cancel" />
          </td>
         </tr>
        </tbody>
       </table>
       <input type="text" name="" value="" id="SysViewType" style="display:none" />
       <input type="text" name="" value="" id="SysCmd" style="display:none" />
       <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none" />
       <input type="text" name="" value="" id="SysCmdQueue" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdText" style="display:none" rmxforms:value="Navigate"/>
       <input type="text" name="" value="" id="SysClassName" style="display:none" rmxforms:value=""/>
       <input type="text" name="" value="" id="SysSerializationConfig" style="display:none"/>
       <input type="text" name="" value="" id="SysFormIdName" style="display:none" rmxforms:value="RowId"/>
       <input type="text" name="" value="" id="SysFormPIdName" style="display:none" rmxforms:value="RowId"/>
       <input type="text" name="" value="" id="SysFormPForm" style="display:none" rmxforms:value="SystemParameterP2Credentials"/>
       <input type="text" name="" value="" id="SysInvisible" style="display:none" rmxforms:value=""/>
       <input type="text" name="" value="" id="SysFormName" style="display:none" rmxforms:value="SystemParameterP2Credentials"/>
       <input type="text" name="" value="" id="SysRequired" style="display:none">
       </td>
      <td valign="top"></td>
     </tr>
    </tbody>
   </table>
   <input type="hidden" value="" id="SysWindowId"/>
   
   </form>
 </body>
</html>
