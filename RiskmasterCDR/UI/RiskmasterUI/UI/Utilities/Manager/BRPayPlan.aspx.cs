﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class BRPayPlan : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        bool bReturnStatus = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - Yukti-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);//vkumar258 ML Changes
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("BRPayPlanAdaptor.Get", XmlTemplate, out sCWSresponse, true, true);

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {

            try
            {
                XmlTemplate = GetMessageTemplate();
                //bReturnStatus=CallCWS("BRPayPlanAdaptor.Save", XmlTemplate, out sCWSresponse, true, true);  csingh7 MITS 15326
                bReturnStatus=CallCWS("BRPayPlanAdaptor.Save", XmlTemplate, out sCWSresponse, true, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }
        private XElement GetMessageTemplate()
        {
            string sRowId = string.Empty;
            string sBillingRule = string.Empty;
            if (mode.Text.ToLower() == "edit")
            {
                sRowId = AppHelper.GetQueryStringValue("selectedid");
            }
            else
            {
                sRowId = "-1";
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><BRPayPlan>");
            sXml = sXml.Append("<control name='RowId' type='id'>");
            sXml = sXml.Append(sRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</BRPayPlan></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        //Added for MITS 15703 : Start
        public override void ModifyXml(ref XElement Xelement)
        {

            XmlDocument xmlNodeDoc = new XmlDocument();
            using (XmlReader reader = Xelement.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            XmlElement objXmlElement = (XmlElement)xmlNodeDoc.SelectSingleNode("Message/Document/BRPayPlan/control[@name='State']");
            objXmlElement.SetAttribute("codetable", "STATES");

            Xelement = XElement.Parse(xmlNodeDoc.InnerXml);
        }
        //Added for MITS 15703 : End

    }
}
