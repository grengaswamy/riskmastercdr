﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class HolidaysSetUp : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            try
            {
                if (HolidaysGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = HolidaysSelectedId.Text;
                    XmlTemplate = GetMessageTemplate(selectedRowId);
                    CallCWS("HolidayAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                    HolidaysGrid_RowDeletedFlag.Text = "false";
                }
                    XmlTemplate = GetMessageTemplate();
                    CallCWSFunctionBind("HolidaysAdaptor.Get", out sCWSresponse, XmlTemplate);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<HolidaysList><listhead>");
            sXml = sXml.Append("<Date type='date'>Date</Date>");
            sXml = sXml.Append("<Desc>Description</Desc>");
            sXml = sXml.Append("<OrgId>Org. Hierarchy</OrgId>");
            sXml = sXml.Append("<RowId>RowId</RowId>");
            sXml = sXml.Append("</listhead></HolidaysList>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
        private XElement GetMessageTemplate(string selectedRowId)
        {
            string strDate = selectedRowId.Substring(0, selectedRowId.IndexOf("|"));
            string strOrgId = selectedRowId.Substring(selectedRowId.IndexOf("|")+1);
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><Holidays>");
            sXml = sXml.Append("<control name='Date'>");
            sXml = sXml.Append(strDate);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Org'>");
            sXml = sXml.Append(strOrgId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</Holidays></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
    }
}
