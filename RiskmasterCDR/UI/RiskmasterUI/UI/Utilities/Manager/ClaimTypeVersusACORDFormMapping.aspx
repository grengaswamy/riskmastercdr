﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimTypeVersusACORDFormMapping.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.Manager.ClaimTypeVersusACORDFormMapping" %>
    
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Claim Type Versus ACORD Form Setup</title>

    <script type="text/javascript">
        var ns, ie, ieversion;
        var browserName = navigator.appName;                   // detect browser 
        var browserVersion = navigator.appVersion;
        if (browserName == "Netscape") {
            ie = 0;
            ns = 1;
        }
        else		//Assume IE
        {
            ieversion = browserVersion.substr(browserVersion.search("MSIE ") + 5, 1);
            ie = 1;
            ns = 0;
        }

        function DeleteMapping() {
            var selected = "";
            if (SelectionValid()) {
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox')
                        if (document.forms[0].elements[i].checked == true) {
                        if (selected == "") {
                            selected = document.forms[0].elements[i].value;
                        }
                        else {
                            selected = selected + " " + document.forms[0].elements[i].value;
                        }
                    }
                }

                document.forms[0].hdnSelected.value = selected;              
                
                return true;
            }
            else {
                return false;
            }
        }
        function trim(value) {
            value = value.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
            return value;
        }
        function Selected() {
            var acordForm = document.forms[0].lstACORDForm.value;
            var ClaimType = document.forms[0].lstClaimType.value;
            if (trim(acordForm) == "" || trim(ClaimType) == "") {
                alert("ACORD Form and Claim Type must be selected before adding to mappings.");
                return false;
            }
            return true;
        }
        function SaveMapping() {
            var selected;
            var acordForm = document.forms[0].lstACORDForm.value;
            var ClaimType = document.forms[0].lstClaimType.value;
            if (Selected()) {
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        selected = document.forms[0].elements[i].value.split('_');
                        if (selected[0] == acordForm && selected[1] == ClaimType) {
                            alert("The combination of ACORD Form and Claim Type are currently part of the mapping and will not be added.");
                            return false;
                        }
                    }
                }
                return true;
            }
            else {
                return false;
            }
        }
        function SelectionValid() {
            for (var i = 0; i < document.forms[0].elements.length; i++)
                if (document.forms[0].elements[i].type == 'checkbox')
                if (document.forms[0].elements[i].checked == true)
                return true;

            return false;
        }
        function onPageLoaded() 
        {
            if (ie) {
                if ((eval("document.all.divForms") != null) && (ieversion >= 6)) {
                    eval("document.all.divForms").style.height = 300               
                }
            }
            else {
                var o_divforms;
                o_divforms = document.getElementById("divForms");
                if (o_divforms != null) {
                    o_divforms.style.height = window.frames.innerHeight * 0.70;
                    o_divforms.style.width = window.frames.innerWidth * 0.995;
                }
            }
        }
        function SelectAll() 
        {
            for (var i = 0; i < document.forms[0].elements.length; i++) 
            {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = true;
                }
            }
            return true;
        }
        function InvertSelection() 
        {            
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = false;
                }
            }
            return true;
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 13%;
        }
    </style>
</head>
<body onload="javascript:onPageLoaded();parent.MDIScreenLoaded()">
    <form id="frmData" name="frmData" method="post" runat="server">  
    <uc1:ErrorControl ID="ErrorControl1" runat="server" /> 
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td class="msgheader" colspan="4">
                Administration
            </td>
        </tr>
        <tr>
            <td class="ctrlgroup" colspan="4">
                Claim Type Versus ACORD Form Setup
            </td>
        </tr>
    </table>
    <div></div>
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td width="20%">
                ACORD Form
            </td>
             <td width="20%">
            Claim Type
            </td>
        </tr>
        <tr>
            <td width="20%">
                <asp:DropDownList ID="lstACORDForm" type="combobox" runat="server" selected="true">                    
                </asp:DropDownList>
            </td>
            <td width="20%">
                <asp:DropDownList ID="lstClaimType" type="combobox" runat="server" selected="true">                    
                </asp:DropDownList>
            </td>
            <td width="5%">
                <asp:Button runat="server" Text="Save Mapping" class="button" OnClientClick="return SaveMapping();"
                    ID="saveMapbtn" onclick="saveMapbtn_Click" />
            </td>
            <td width="5%">
                <asp:Button runat="server" Text="Delete Mapping" class="button" OnClientClick="return DeleteMapping();"
                    ID="delMapbtn" onclick="delMapbtn_Click" />
            </td>
            <td width="*">
                &nbsp;
            </td>
        </tr>
    </table>   
    <table border="1" width="100%" cellspacing="0" cellpadding="2">
        <thead>
            <tr>
                <td class="msgheader">  
                </td>
                <td class="msgheader">
                   ACORD Form                    
                </td>
                <td class="msgheader" nowrap="1">
                   Claim Type
                </td>               
            </tr>
        </thead>            
        <%int i = 0; foreach (XElement item in result)
          {
              string colclass = "";
              if ((i % 2) == 1) colclass = "datatd1";
              else colclass = "datatd";
              i++;
        %>
        <tr>      
             <td class="<%=colclass%>">
                <input type="checkbox" id="selMapping" name="selMapping"  value="<%=item.Attribute("value").Value%>"/>         
            </td>       
            <td class="<%=colclass%>">
                <%=item.Element("rowtextaccord").Attribute("title").Value %> 
            </td>
            <td class="<%=colclass%>">
                <%=item.Element("rowtextclaim").Attribute("title").Value  %>
            </td>          
        </tr>
        <%}%>
    </table>
    <br />
    <table>
    <tr>
    <td> <input type="button" class="button" id="selectAllbtn" runat="server" value="Select All"  onclick="return SelectAll();" />  </td>
    <td> <input type="button" class="button" id="unselectAll" runat="server" value="Clear All"  onclick="return InvertSelection();"/>  </td>
    </tr>
    </table>
       <asp:TextBox Style="display: none" runat="server" ID="hdnAcordParam" RMXRef="/Instance/Document/ClaimTypeVsAcordFormSetup/ACORDFormSel" />    
       <asp:TextBox Style="display: none" runat="server" ID="hdnClaimParam" RMXRef="/Instance/Document/ClaimTypeVsAcordFormSetup/ClaimTypeSel" />    
       <input id="hdnSelected" type="hidden" runat="server" />
 
    </form>
</body>
</html>
