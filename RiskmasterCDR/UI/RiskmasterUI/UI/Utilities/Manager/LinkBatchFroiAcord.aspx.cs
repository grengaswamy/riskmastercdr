﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
using System.Net;
using System.IO;
//using Riskmaster.UI.DocumentService;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.ServiceModel;
using Riskmaster.Common;
using Riskmaster.Models;


namespace Riskmaster.UI.UI.Utilities.Manager

{
    public partial class LinkBatchFroiAcord : NonFDMBasePageCWS
    {
        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
        string sSortFroi = string.Empty;
        string sSortAcord = string.Empty;
        string sSortColumnFroi = string.Empty;
        string sSortColumnAcord = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            bool bReturnStatus = false;

            if (!Page.IsPostBack)
            {

                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWSFunction("PrintBatchFroiAdaptor.GetPath", out sReturn, XmlTemplate);
                if (bReturnStatus && ErrorHelper.IsCWSCallSuccess(sReturn))
                {
                    XmlDoc.LoadXml(sReturn);
                    NavigateUrl(XmlDoc,0,0);
                }
            }

        }
        private XElement GetMessageTemplate()
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>PrintBatchFroiAdaptor.GetPath</Function></Call><Document>");
            sXml = sXml.Append("<PassToWebService><LinkBatchFroiAcord><listhead>");
            sXml = sXml.Append("<PathBatchFroiAcord>Path BatchFroiAcord</PathBatchFroiAcord>");
            sXml = sXml.Append("<SortExprFroi>");
            sXml = sXml.Append(sSortFroi);
            sXml = sXml.Append("</SortExprFroi>");
            sXml = sXml.Append("<SortExprAcord>");
            sXml = sXml.Append(sSortAcord);
            sXml = sXml.Append("</SortExprAcord>");
            sXml = sXml.Append("<SortColumnFroi>");
            sXml = sXml.Append(sSortColumnFroi);
            sXml = sXml.Append("</SortColumnFroi>");
            sXml = sXml.Append("<SortColumnAcord>");
            sXml = sXml.Append(sSortColumnAcord);
            sXml = sXml.Append("</SortColumnAcord>");
            sXml = sXml.Append("</listhead></LinkBatchFroiAcord></PassToWebService>");
            sXml = sXml.Append("</Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        private XElement GetMessageTemplate4Delete()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>0d2cd9cd-08e4-488b-ac7f-9a1c2779a3a0</Authorization>");
            sXml = sXml.Append("<Call><Function>BillingAdaptor.RemoveFileFromDisk</Function></Call><Document><BillingScheduler><FileName>");
            sXml = sXml.Append(filename.Text);
            sXml = sXml.Append("</FileName></BillingScheduler></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            ErrorControl1.errorDom = p_sReturn;
            Exception ex = new Exception("Service Error");
            if (ErrorControl1.errorFlag == true)
                throw ex;
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }

        public void NavigateUrl(XmlDocument p_objXmlDoc, int PageIndexFroi, int PageIndexAccord)
        {
            BindFroiGrid(p_objXmlDoc, PageIndexFroi);
            BindAcordgrid(p_objXmlDoc, PageIndexAccord);
        }

        private void BindFroiGrid(XmlDocument p_objXmlDoc, int PageNumber)
        {
            gvFroiList.HeaderStyle.CssClass = "msgheader";
            gvFroiList.RowStyle.CssClass = "datatd1";
            gvFroiList.AlternatingRowStyle.CssClass = "datatd";

            XmlNodeList objFroiNodeList = p_objXmlDoc.SelectNodes("//LinkBatchFroiAcord/FroiFileName/*");
            if (objFroiNodeList.Count != 0)
            {
                DataTable objDataTable = null;
                XmlNode xNode;
                DataRow objRow;

                objDataTable = new DataTable("Froi");
                objDataTable.Columns.Add("chkFROI");
                objDataTable.Columns.Add("SNo");
                objDataTable.Columns.Add("DocId");
                objDataTable.Columns.Add("BatchId");
                objDataTable.Columns.Add("FileName");
                objDataTable.Columns.Add("FileCount");
                objDataTable.Columns.Add("DateCreated");

                for (int i = 0; i < objFroiNodeList.Count; i++)
                {
                    xNode = objFroiNodeList[i];
                    objRow = objDataTable.NewRow();

                    objRow["chkFROI"] = xNode.SelectSingleNode("chkFROI").InnerText;
                    objRow["SNo"] = xNode.SelectSingleNode("SNO").InnerText;
                    objRow["DocId"] = xNode.SelectSingleNode("DocId").InnerText;
                    objRow["BatchId"] = xNode.SelectSingleNode("BatchId").InnerText;
                    objRow["FileName"] = xNode.SelectSingleNode("FileName").InnerText;
                    objRow["FileCount"] = xNode.SelectSingleNode("FileCount").InnerText;
                    objRow["DateCreated"] = xNode.SelectSingleNode("DateCreated").InnerText;
                    objDataTable.Rows.Add(objRow);
                }
                gvFroiList.DataSource = objDataTable;
                gvFroiList.PageIndex = PageNumber;
                gvFroiList.DataBind();
            }
            else
            {
                gvFroiList.Visible = false;
                //nzafar-22448
                lblMessageFroi.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;                    There are no FROI for printing!</h3></p>";
            }
        }

        private void BindAcordgrid(XmlDocument p_objXmlDoc, int PageNumber)
        {
            gvAcordList.HeaderStyle.CssClass = "msgheader";
            gvAcordList.RowStyle.CssClass = "datatd1";
            gvAcordList.AlternatingRowStyle.CssClass = "datatd";

            XmlNodeList objAcordNodeList = p_objXmlDoc.SelectNodes("//LinkBatchFroiAcord/AcordFileName/*");
            if (objAcordNodeList.Count != 0)
            {
                DataTable objDataTable = null;
                XmlNode xNode;
                DataRow objRow;

                objDataTable = new DataTable("Acord");
                objDataTable.Columns.Add("chkACORD");
                objDataTable.Columns.Add("SNo");
                objDataTable.Columns.Add("DocId");
                objDataTable.Columns.Add("BatchId");
                objDataTable.Columns.Add("FileName");
                objDataTable.Columns.Add("FileCount");
                objDataTable.Columns.Add("DateCreated");

                for (int i = 0; i < objAcordNodeList.Count; i++)
                {
                    xNode = objAcordNodeList[i];
                    objRow = objDataTable.NewRow();

                    objRow["chkACORD"] = xNode.SelectSingleNode("chkACORD").InnerText;
                    objRow["SNo"] = xNode.SelectSingleNode("SNO").InnerText;
                    objRow["DocId"] = xNode.SelectSingleNode("DocId").InnerText;
                    objRow["BatchId"] = xNode.SelectSingleNode("BatchId").InnerText;
                    objRow["FileName"] = xNode.SelectSingleNode("FileName").InnerText;
                    objRow["FileCount"] = xNode.SelectSingleNode("FileCount").InnerText;
                    objRow["DateCreated"] = xNode.SelectSingleNode("DateCreated").InnerText;

                    objDataTable.Rows.Add(objRow);
                }
                gvAcordList.DataSource = objDataTable;
                gvAcordList.PageIndex = PageNumber;
                gvAcordList.DataBind();
            }
            else
            {
                gvAcordList.Visible = false;
                //nzafar-22448
                lblMessageAcord.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;                    There are no ACORD for printing!</h3></p>";
            }
        }

        protected void btnDeleteFroi_Click(object sender, EventArgs e)
        {
            string sId = string.Empty;
            string sFolderId = string.Empty;
            int iDocid = 0;
            string[] strArray; 
            string sListText=string.Empty;

                DocumentBusinessHelper dc = new DocumentBusinessHelper();
                string sfolderId = AppHelper.GetFormValue("hfolderId").ToString();
                string shdocId = AppHelper.GetFormValue("hdocId").ToString();
                bool isError = false;
                bool bIsRedirect = false;


                XElement XmlTemplate = null;
                string sCWSresponse = "";
                XmlDocument XmlDoc = new XmlDocument();
                XmlDocument oFDMPageDom = new XmlDocument();
                string sReturn = "";
                bool bReturnStatus = false;


                try
                {
                    if (hdnFroiAcordSelectedId != null)
                    {
                        sListText = hdnFroiAcordSelectedId.Text;
                        
                    }
                    if ((sListText != ""))
                    {
                        sListText = sListText.Substring(1, (sListText.Length - 1));


                        strArray = sListText.Split('|');

                        if (strArray.Length > 0)
                        {
                            for (int i = 0; i < strArray.Length; i++)
                            {

                                iDocid = Conversion.ConvertStrToInteger(strArray[i].ToString());
                                dc.Delete(sFolderId, iDocid.ToString(), "", "", "");
                                dc.Delete(sFolderId, iDocid.ToString(), "", "", "");
                                bIsRedirect = true;
                            }
                        }

                    
                    }

                  

                        XmlTemplate = GetMessageTemplate();
                        bReturnStatus = CallCWSFunction("PrintBatchFroiAdaptor.GetPath", out sReturn, XmlTemplate);
                        if (bReturnStatus && ErrorHelper.IsCWSCallSuccess(sReturn))
                        {
                            XmlDoc.LoadXml(sReturn);
                            NavigateUrl(XmlDoc,gvFroiList.PageIndex,gvAcordList.PageIndex);
                        }
                    

                }
                catch (FaultException<RMException> ee)
                {
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                    isError = true;
                    bIsRedirect = false;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    isError = true;
                    bIsRedirect = false;
                }

            
        }
        private SortDirection GridViewSortDirectionFroi
        {
            get
            {
                if (ViewState["sortDirectionFroi"] == null)
                    ViewState["sortDirectionFroi"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirectionFroi"];
            }
            set { ViewState["sortDirectionFroi"] = value; }
        }
        private SortDirection GridViewSortDirectionAcord
        {
            get
            {
                if (ViewState["sortDirectionAcord"] == null)
                    ViewState["sortDirectionAcord"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirectionAcord"];
            }
            set { ViewState["sortDirectionAcord"] = value; }
        }
       

        protected void OnSortFroi(Object sender, GridViewSortEventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            string sReturn = "";
            bool bReturnStatus = false;

            string sortExpression = e.SortExpression;
            switch (sortExpression)
            {
                case "Batch Id":
                    sSortColumnFroi = "DOCUMENT_ID";
                    break;
                case "File Name":
                    sSortColumnFroi = "DOCUMENT_FILENAME";
                    break;
                case "Date Created":
                    sSortColumnFroi = "CREATE_DATE";
                    break;
                default:
                    break;

            }
            ViewState["SortExpression"] = sortExpression;

            if (GridViewSortDirectionFroi == SortDirection.Ascending)
            {
                GridViewSortDirectionFroi = SortDirection.Descending;
                sSortFroi = " DESC";
            }
            else
            {
                GridViewSortDirectionFroi = SortDirection.Ascending;
                sSortFroi = " ASC";
            }
            XmlTemplate = GetMessageTemplate();
            bReturnStatus = CallCWSFunction("PrintBatchFroiAdaptor.GetPath", out sReturn, XmlTemplate);
            if (bReturnStatus && ErrorHelper.IsCWSCallSuccess(sReturn))
            {
                XmlDoc.LoadXml(sReturn);
                NavigateUrl(XmlDoc,0,gvAcordList.PageIndex);

            }

        }

        protected void OnSortAcord(Object sender, GridViewSortEventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();            
            string sReturn = "";
            bool bReturnStatus = false;

            string sortExpression = e.SortExpression;
            switch (sortExpression)
            {
                case "Batch Id":
                    sSortColumnAcord = "DOCUMENT_ID";
                    break;
                case "File Name":
                    sSortColumnAcord = "DOCUMENT_FILENAME";
                    break;
                case "Date Created":
                    sSortColumnAcord = "CREATE_DATE";
                    break;
                default:
                    break;

            }
            ViewState["SortExpression"] = sortExpression;

            if (GridViewSortDirectionAcord == SortDirection.Ascending)
            {
                GridViewSortDirectionAcord = SortDirection.Descending;              
                sSortAcord = " DESC";
            }
            else
            {
                GridViewSortDirectionAcord = SortDirection.Ascending;
                sSortAcord = " ASC";                
            }
            XmlTemplate = GetMessageTemplate();
            bReturnStatus = CallCWSFunction("PrintBatchFroiAdaptor.GetPath", out sReturn, XmlTemplate);
            if (bReturnStatus && ErrorHelper.IsCWSCallSuccess(sReturn))
            {
                XmlDoc.LoadXml(sReturn);
                NavigateUrl(XmlDoc,gvFroiList.PageIndex,0);
                         
            }                      
            
        }
        
        protected void gvFroiList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
                LinkButton lbLinkColumn;
                lbLinkColumn = (LinkButton)e.Row.FindControl("SNo");
                lbLinkColumn.Attributes.Add("href", "#");
                //lbLinkColumn.Attributes.Add("onclick", "selectPDF('" + ((System.Web.UI.WebControls.Label)(e.Row.FindControl("FileName"))).Text + "'); return false;");
                lbLinkColumn.Attributes.Add("onclick", "selectPDF('" + ((System.Web.UI.WebControls.Label)(e.Row.FindControl("DocId"))).Text + "','" + ((System.Web.UI.WebControls.Label)(e.Row.FindControl("FileName"))).Text + "'); return false;");
            }
        }

        protected void gvAcordList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
                LinkButton lbLinkColumn;
                lbLinkColumn = (LinkButton)e.Row.FindControl("SNo");
                lbLinkColumn.Attributes.Add("href", "#");
                
                //lbLinkColumn.Attributes.Add("onclick", "selectPDF('" + ((System.Web.UI.WebControls.Label)(e.Row.FindControl("FileName"))).Text + "'); return false;");
               // lbLinkColumn.Attributes.Add("onclick", "selectPDF('" + ((System.Web.UI.WebControls.Label)(e.Row.FindControl("DocId"))).Text + "'); return false;");
                lbLinkColumn.Attributes.Add("onclick", "selectPDF('" + ((System.Web.UI.WebControls.Label)(e.Row.FindControl("DocId"))).Text + "','" + ((System.Web.UI.WebControls.Label)(e.Row.FindControl("FileName"))).Text + "'); return false;");
            }
        }

        protected void btnDeleteAcord_Click(object sender, EventArgs e)
        {
           
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("/RiskmasterUI/UI/Utilities/Manager/LinkBatchFroiAcord.aspx");
        }

        protected void gvFroiList_PageChange(object sender, GridViewPageEventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            bool bReturnStatus = false;

            XmlTemplate = GetMessageTemplate();
            bReturnStatus = CallCWSFunction("PrintBatchFroiAdaptor.GetPath", out sReturn, XmlTemplate);

            if (bReturnStatus && ErrorHelper.IsCWSCallSuccess(sReturn))
            {
                XmlDoc.LoadXml(sReturn);
                gvFroiList.PageIndex = e.NewPageIndex;
                NavigateUrl(XmlDoc,gvFroiList.PageIndex,gvAcordList.PageIndex);
            }
        }

        protected void gvAcordList_PageChange(object sender, GridViewPageEventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            bool bReturnStatus = false;

            XmlTemplate = GetMessageTemplate();
            bReturnStatus = CallCWSFunction("PrintBatchFroiAdaptor.GetPath", out sReturn, XmlTemplate);

            if (bReturnStatus && ErrorHelper.IsCWSCallSuccess(sReturn))
            {
                XmlDoc.LoadXml(sReturn);
                gvAcordList.PageIndex = e.NewPageIndex;
                NavigateUrl(XmlDoc, gvFroiList.PageIndex, gvAcordList.PageIndex);
            }
        }
        
    }
}
