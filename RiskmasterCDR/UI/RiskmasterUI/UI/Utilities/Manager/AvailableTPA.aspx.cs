﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class AvailableTPA : NonFDMBasePageCWS
    {
        #region "Constants"
        
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //DataTable dtGridData = new DataTable();
            //DataRow objRow;
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("AvailableTPA.aspx"), "AvailableTPAValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "AvailableTPAValidations", sValidationResources, true);
            if (!IsPostBack)
            {
                lblTPApage.Text = "Available TPA(s)";
                BindGridData();
            }
        }

        protected void addTPABtn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            XElement XmlTemplate = null;
            try
            {
                XmlTemplate = GetMessageTemplateForAdd();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.AddNewMapping", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        txtTPASystemName.Text = "";
                        txtTPAName.Text = "";
                    }

                    else
                    {
                        BindGridData();
                        txtTPASystemName.Text = "";
                        txtTPAName.Text = "";

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void removeTPBtn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            try
            {
                XElement XmlTemplate = null;
                string sPolCode = "";
                XmlTemplate = GetMessageTemplateForDeletion();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.DeleteTPAMapping", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        txtTPASystemName.Text = "";
                        txtTPAName.Text = "";
                    }

                    else
                    {
                        BindGridData();
                        txtTPASystemName.Text = "";
                        txtTPAName.Text = "";
                    }
                }
              
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void refreshTPBtn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            XElement XmlTemplate = null;
            try
            {
                XmlTemplate = GetMessageTemplateForRefresh();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.RefershNewMapping", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        txtTPASystemName.Text = "";
                        txtTPAName.Text = "";
                    }

                    else
                    {
                        BindGridData();
                        txtTPASystemName.Text = "";
                        txtTPAName.Text = "";

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplateForAdd()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.AddNewMapping");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMappingAdaptor><AddCode>");
            sXml = sXml.Append("<TPASystemName>" + txtTPASystemName.Text + "</TPASystemName>");
            sXml = sXml.Append("<TPAName>" + txtTPAName.Text + "</TPAName>");
            sXml = sXml.Append("</AddCode></TPACodeMappingAdaptor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private void BindGridData()
        {
            try
            {
                string data = string.Empty;
                DataTable dtGridData = new DataTable();
                DataRow objRow = null;
                XElement XmlTemplate = null;
                bool bReturnStatus = false;

                XmlTemplate = GetMessageTemplateForAllMappings();
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.GetTPAMapping", XmlTemplate, out data, false, true);
                dtGridData.Columns.Add("CBId");
                dtGridData.Columns.Add("TPASystemName");
                dtGridData.Columns.Add("TPAName");
                if (bReturnStatus)
                {
                    
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(data);
                    string sRowId = string.Empty;
                    string sTPASystemName = string.Empty;
                    string sTPAName = string.Empty;
                    XmlNodeList xmlNodeList = xmldoc.SelectNodes("//AvailableTPAList/TPA");
                    if (xmlNodeList.Count > 0)
                    {
                        foreach (XmlNode objNodes in xmlNodeList)
                        {
                            objRow = dtGridData.NewRow();
                            sRowId = objNodes.Attributes["TPARowID"].Value.ToString();
                            sTPASystemName = objNodes.Attributes["TPASystemName"].Value.ToString(); //spahariya MITS 30911
                            sTPAName = objNodes.Attributes["TPAName"].Value.ToString();//spahariya MITS 30911
                            objRow["CBId"] = sRowId;
                            objRow["TPASystemName"] = sTPASystemName;
                            objRow["TPAName"] = sTPAName;
                            dtGridData.Rows.Add(objRow);
                        }
                        GridViewAvailableTPA.DataSource = dtGridData;
                        GridViewAvailableTPA.DataBind();
                    }
                }
                if (dtGridData.Rows.Count <= 0)
                {
                    objRow = dtGridData.NewRow();
                    objRow["CBId"] = "";
                    objRow["TPASystemName"] = "";
                    objRow["TPAName"] = "";

                    dtGridData.Rows.Add(objRow);
                    GridViewAvailableTPA.DataSource = dtGridData;
                    GridViewAvailableTPA.DataBind();
                    GridViewAvailableTPA.Rows[GridViewAvailableTPA.Rows.Count - 1].Visible = false;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplateForAllMappings()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.GetTPAMapping");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMappingAdaptor>");
            sXml = sXml.Append("<AddCode></AddCode>");
            sXml = sXml.Append("</TPACodeMappingAdaptor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplateForDeletion()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.DeleteTPAMapping");
            sXml = sXml.Append("</Function></Call><Document><TPAMapping><DeleteCode>");
            sXml = sXml.Append("<DeletedCode>" + hdnSelected.Text + "</DeletedCode>");
            sXml = sXml.Append("</DeleteCode></TPAMapping></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        private XElement GetMessageTemplateForRefresh()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.RefershNewMapping");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMappingAdaptor><RefreshCode>");
            sXml = sXml.Append("</RefreshCode></TPACodeMappingAdaptor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        



    }
}
