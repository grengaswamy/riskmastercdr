﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateExistingTasks.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.Manager.UpdateExistingTasks" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Update Existing Tasks</title>
    <script type="text/javascript">
        function lstDataSources_DataSourceChange() 
        {
            document.forms[0].submit();
        }

        function UpdateTasks() 
        {
            if (document.forms[0].lstTasks.selectedIndex == -1 || document.forms[0].lstTasks.length == 0) {
                alert("Pleases select some task to update")
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div id="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" height="32">
                    <asp:ImageButton TabIndex="1" ID="Save" ImageUrl="~/Images/tb_save_active.png" class="bold"
                        ToolTip="Save" runat="server" OnClick="Save_Click" OnClientClick="return UpdateTasks();" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <%if (UpdatedFlag.Value == "True")
                      {
                          Response.Write("Data has been updated successfully !");
                      }
                    %>
                </td>
            </tr>
        </table>
    </div>  
        <table border="0" cellspacing="0" cellpadding="0" id="FORMTABAck">
            <tr id="Tr1">
                <td colspan="5">
                    <table width="100%" align="left">
                        <tr>
                            <td class="ctrlgroup" width="100%">
                                Assign User and DataSource to Scheduled Tasks
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table>
                        <tr>
                            <td align="left">
                                <b>Scheduled Task List</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox runat="server" ID="lstTasks" SelectionMode="Multiple" rmxignoreset="true"
                                    rmxref="/Instance/Document/ScheduledTasks/Tasks"></asp:ListBox>
                                <asp:TextBox ID="txtTasks" runat="server" rmxignoreget="true" rmxref="/Instance/Document/ScheduledTasks/Tasks"
                                    Style="display: none"></asp:TextBox>
                                    <asp:TextBox ID="txtTaskIndex" runat="server" Style="display:none"></asp:TextBox>                                   
                            </td>
                            <td valign="top">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td valign="top" align="left">
                                            <b>DataSources</b>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="lstDataSources" runat="server" onchange="lstDataSources_DataSourceChange();"
                                                rmxignoreset="true" rmxref="/Instance/Document/Adjuster/control[@name ='DataSources']/@value"
                                                itemSetref="/Instance/Document/ScheduledTasks/control[@name='DataSources']" OnSelectedIndexChanged="lstDataSources_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtDataSourceId" runat="server" rmxignoreget="true" rmxref="/Instance/Document/ScheduledTasks/DataSourceID"
                                                Style="display: none"></asp:TextBox>
                                            <asp:TextBox ID="txtDataSourceName" runat="server" rmxignoreget="true" rmxref="/Instance/Document/ScheduledTasks/DataSourceName"
                                                Style="display: none"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td valign="top" align="right" id="tdUsers" runat="server">
                                            <b>Assigned Users</b>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="lstUsers" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Adjuster/control[@name ='Users']/@value"
                                                itemSetref="/Instance/Document/ScheduledTasks/control[@name='Users']">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtUsers" runat="server" rmxignoreget="true" rmxref="/Instance/Document/ScheduledTasks/User"
                                                Style="display: none"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" value="Close" class="button" id="btnClose" onclick="window.close();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>   
    <input type="text" name="" value="" id="UpdatedFlag" style="display: none" runat="server" />
    </form>
</body>
</html>
