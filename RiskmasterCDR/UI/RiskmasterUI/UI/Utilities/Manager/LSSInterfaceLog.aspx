﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LSSInterfaceLog.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.LSSInterfaceLog" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
 <head runat="server">
  <title>LSS Interface Log Utility</title>
  <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"/>
  <link rel="stylesheet" href="../../../Content/system.css" type="text/css"/>
  <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css"/>
  <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
  <script type="text/javascript">
				var ns, ie, ieversion;
				var browserName = navigator.appName;                   // detect browser 
				var browserVersion = navigator.appVersion;
				if (browserName == "Netscape")
				{
					ie=0;
					ns=1;
				}
				else		//Assume IE
				{
				ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
				ie=1;
				ns=0;
				}
				function onPageLoaded()
				{
					if (ie)
					{
						if ((eval("document.all.divForms")!=null) && (ieversion>=6))
						{
							eval("document.all.divForms").style.height=350;
						}
					}
					else
					{
						var o_divforms;
						o_divforms=document.getElementById("divForms");
						if (o_divforms!=null)
						{
							o_divforms.style.height=window.frames.innerHeight*0.70;
							o_divforms.style.width=window.frames.innerWidth*0.995;
						}
					}
				}
				function ClearForm()
				{
					document.forms[0].txtRequestId.value="";
					document.forms[0].txtFromDate.value="";
					document.forms[0].txtToDate.value="";
					document.forms[0].lstPacketType.selectedIndex=0;
					document.forms[0].lstRequestType.selectedIndex=0;
					document.forms[0].lstStatus.selectedIndex=0;
					return false;
				}
				function ShowPacket(s){
				    var iIndex = 0;
				    iIndex = s.indexOf("(##)=");
				    while (iIndex > -1) {
				            s = s.replace("(##)=", "\'");
				         iIndex = s.indexOf("(##)=");
				     }
				     iIndex = s.indexOf("(##)+");
				     while (iIndex > -1) {
				         s = s.replace("(##)+", "\"");
				         iIndex = s.indexOf("(##)+");
				     }
				     
				     iIndex = s.indexOf("(##)");
				     while (iIndex > -1) {
				         s = s.replace("(##)", "\\");
				         iIndex = s.indexOf("(##)");
				     }
					i = s.indexOf("#?#%?%#?#");
					while(i > -1){ 
						s = s.replace("#?#%?%#?#", "'");
						i = s.indexOf("#?#%?%#?#");
						}
					//gagnihotri 04/02/2009 Retrofitted the MITS 14565 changes into base.
					if (s!=null)
					   initTraverse(s);
					else
					   alert(s);

					return false;
				}
				//gagnihotri 04/02/2009 Retrofitted the MITS 14565 changes into base.
				function ShowEvent(s)
				{		
					i = s.indexOf("#?#%?%#?#");
					while(i > -1){ 
						s = s.replace("#?#%?%#?#", "'");
						i = s.indexOf("#?#%?%#?#");
						}
					alert(s);
					return false;
				}

	</script>
	<script src="../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
	<script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;}</script>
    <%--vkumar258 - RMA-6037 - Starts --%>
   <%-- <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
	<script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
	<script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
   <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>
</head>
 <body onload="javascript:onPageLoaded();if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();" style="height:99%">
  <form id="frmData" name="frmData" method="post" runat="server">
  <uc1:ErrorControl id="ErrorControl1" runat="server" />
   <table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
     <td class="msgheader" colspan="2">LSS Interface Log</td>
    </tr>
    <tr>
     <td align="left" width="15%">Request Id:</td>
     <td><asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/RequestId"  size="30" id="txtRequestId"/></td>
    </tr>
    <tr>
     <td align="left">From Date:</td>
     <td>
        <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/FromDate" value="" onblur="dateLostFocus(this.id);" id="txtFromDate" size="12"/>
                    <%--vkumar258 - RMA-6037 - Starts --%>
                   <%-- <input type="button"  class="DateLookupControl" value="..." name="datebtn1"/>
            <script type="text/javascript">
									Zapatec.Calendar.setup(
									{
										inputField : "txtFromDate",
										ifFormat : "%m/%d/%Y",
										button : "datebtn1"
									}
									);
			</script>--%>
                   
                     <script type="text/javascript">
                         $(function () {
                             $("#txtFromDate").datepicker({
                                 showOn: "button",
                                 buttonImage: "../../../Images/calendar.gif",
                                // buttonImageOnly: true,
                                 showOtherMonths: true,
                                 selectOtherMonths: true,
                                 changeYear: true
                             }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                         });
                                                            </script>
                    <%--vkumar258 - RMA_6037- End--%>
	 </td>
    </tr>
    <tr>
     <td align="left">To Date:</td>
     <td>
        <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/ToDate" value="" onblur="dateLostFocus(this.id);" id="txtToDate" size="12"/>
                    <%--vkumar258 - RMA-6037 - Starts --%>
                    <%--<input type="button" class="DateLookupControl" value="..." name="datebtn2"/>
            <script type="text/javascript">
									Zapatec.Calendar.setup(
									{
										inputField : "txtToDate",
										ifFormat : "%m/%d/%Y",
										button : "datebtn2"
									}
									);
			</script>--%>
                  <script type="text/javascript">
                      $(function () {
                          $("#txtToDate").datepicker({
                              showOn: "button",
                              buttonImage: "../../../Images/calendar.gif",
                              //buttonImageOnly: true,
                              showOtherMonths: true,
                              selectOtherMonths: true,
                              changeYear: true
                          }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                      });
                                                            </script>
                    <%--vkumar258 - RMA_6037- End--%>
	 </td>
    </tr>
    <tr>
     <td align="left">Packet Type:</td>
     <td>
        <asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PacketType" id="lstPacketType" ItemSetRef="/Instance/Document/SearchResults/PacketType" />
     </td>
    </tr>
    <tr>
     <td align="left">Request Type:</td>
     <td>
        <asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/RequestType" id="lstRequestType" ItemSetRef="/Instance/Document/SearchResults/RequestType"/>
     </td>
    </tr>
    <tr>
     <td align="left">Status:</td>
     <td>
        <asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/Status" id="lstStatus" ItemSetRef="/Instance/Document/SearchResults/Status"/>
      </td>
    </tr>
    <asp:TextBox runat="server" Visible="false" type="hidden" value="" Text="" id="FirstTime" rmxref="Instance/Document/SearchParam/FirstTime" />
    <tr>
     <td align="left" colspan="2">
        <asp:Button runat="server" type="button"  id="Search" Text="Search" class="button" OnClick="Search_But"/>&nbsp;&nbsp;
      	<asp:Button runat="server" type="button" id="reset" Text="Clear" class="button" OnClientClick="return ClearForm();"/>
     </td>
    </tr>
   </table>
   <div id="divForms1" style="width:99.5%;height:57%;overflow:auto">
    <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" class="colheader5">Request Id</td>
      <td align="center" class="colheader5">Packet Type</td>
      <td align="center" class="colheader5">Request Type</td>
      <td align="center" class="colheader5">Status</td>
      <td align="center" class="colheader5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="colheader5">&nbsp;&nbsp;&nbsp;&nbsp;Time&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="colheader5">Request Packet</td>
      <td align="center" class="colheader5">Response Packet</td>
      <td align="center" class="colheader5">Events</td>
      <td align="center" class="colheader5">Exceptions</td>
     </tr>
     <%int i = 0;
         
       if (result != null)
       {
           foreach (XElement item in result)
           {
               string rowclass = "";
               if ((i % 2) == 1) rowclass = "rowlight";
               else rowclass = "rowdark";
               i++;
     %>
     <!--gagnihotri MITS 16393 05/11/2009-->
     <%Regex regex = new Regex(@">\s*<");
       Regex regex1 = new Regex(@"\r\n");
       string RequestXML = "";
       string ResponseXML = "";%>
     
     <tr class="<% =rowclass %>">
        <td align="center">
            <% if (item.Attribute("RequestId").Value != null)
                   RequestId.Text = item.Attribute("RequestId").Value; %>
            <asp:Label ID="RequestId" runat="server"></asp:Label>
        </td>
        <td align="center">
            <% PacketType.Text = item.Attribute("PacketType").Value; %>
            <asp:Label ID="PacketType" runat="server"></asp:Label>
        </td>
        <td align="center">
            <% RequestType.Text = item.Attribute("RequestType").Value; %>
            <asp:Label ID="RequestType" runat="server"></asp:Label>
        </td>
        <td align="center">
            <% Status.Text = item.Attribute("Status").Value; %>
            <asp:Label ID="Status" runat="server"></asp:Label>
        </td>
        <td align="center">
            <% Date.Text = item.Attribute("Date").Value; %>
            <asp:Label ID="Date" runat="server"></asp:Label>
        </td>
        <td align="center">
            <% Time.Text = item.Attribute("Time").Value; %>
            <asp:Label ID="Time" runat="server"></asp:Label>
        </td>
        <td>
            <!--gagnihotri MITS 16393 05/11/2009-->
            <% RequestXML = item.Attribute("RequestPacket").Value;
               RequestXML = regex.Replace(RequestXML, "><").Trim();
               RequestXML = ReplaceEscChars(RequestXML).Trim();//Added by Amitosh for mits 27659
               ResponseXML = regex1.Replace(RequestXML, " ").Trim();
               RequestPacket.OnClientClick = "return ShowPacket ('" + RequestXML + "');"; %>
            <asp:Button ID="RequestPacket" runat="server" Text="View Request" CssClass="button"/>
        </td>
        <td>
            <!--gagnihotri MITS 16393 05/11/2009-->
            <% ResponseXML = item.Attribute("ResponsePacket").Value;
               ResponseXML = regex.Replace(ResponseXML, "><").Trim();
               ResponseXML = regex1.Replace(ResponseXML, " ").Trim();
               ResponseXML = ReplaceEscChars(ResponseXML).Trim();//Added by Amitosh for mits 27659
               ResponsePacket.OnClientClick = "return ShowPacket ('" + ResponseXML + "');"; %>
            <asp:Button ID="ResponsePacket" runat="server" Text="View Response" CssClass="button"/>
        </td>
        <td>
            <!--gagnihotri 04/02/2009 Retrofitted the MITS 14565 changes into base-->
            <% Events.OnClientClick = "return ShowEvent ('" + item.Attribute("Events").Value + "');"; %>
            <asp:Button ID="Events" runat="server" Text="View Events" CssClass="button"/>
        </td>
        <td align="center">
            <% IsException.Text = item.Attribute("IsException").Value; %>
            <asp:Label ID="IsException" runat="server"></asp:Label>
        </td>
        <% }
       }
            %> 
     </tr>

     
    </table>
   </div>
   </form>
 </body>
</html>
