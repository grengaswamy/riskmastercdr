﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaxAndOffsetMapping.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.Manager.TaxAndOffsetMapping" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Tax Mapping</title>
    <script type="text/JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/JavaScript" src="../../../Scripts/Utilities.js"></script>
    <script type="text/javascript">
				var ns, ie, ieversion;
				var browserName = navigator.appName;                   // detect browser 
				var browserVersion = navigator.appVersion;
				if (browserName == "Netscape")
				{
					ie=0;
					ns=1;
				}
				else
				{
				ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
				ie=1;
				ns=0;
				}
				function TabSelected(ctrl)
				{
				try
				{
					if (ctrl.className="Selected")
					{
						document.forms[0].SysTabSelected.value=ctrl.name+"**"+ctrl.className;
					}
				}catch(ex){}
				}
				function submitJurisdictions()
				{
				  document.forms[0].submit();
				}
				function DeleteMapping() {
				    var selectedVal="";
				     if (SelectionValid())
					 {
						for(var i = 0; i<document.forms[0].elements.length;i++)
                       {
                         if(document.forms[0].elements[i].type=='checkbox')
                        {
                          if(document.forms[0].elements[i].checked == true)
                          {
                             if (selectedVal=="")
                            {
                                selectedVal=document.forms[0].elements[i].value;
                            }
                             else
                             {
                                 selectedVal=selectedVal+" "+document.forms[0].elements[i].value;
                             }
                          }
                       }
                     }
                   document.forms[0].hdnSelected.value=selectedVal;
                   return true;
                   }
                   else
                  {
                   return false;
                  }
               }
          function trim(value)
          {
          value = value.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
          return value;
          }
          function Selected()
          {
          var benefitsList=document.forms[0].lstBenefitsList.value;
          var transactionType=document.forms[0].lstTransactionType.value;
          if (trim(benefitsList)==""  || trim(transactionType)=="")
          {
          alert("Benefit and Transaction Type must be selected before adding to mappings.");
          return false;
          }
          return true;
          }

          function SaveFeredalMapping()
          {
          if (trim(document.forms[0].txtFederalTax.value)=="")
          {
          alert("Please select Federal Tax Entity/Payee.");
          document.forms[0].txtFederalTax.focus();
          return false;
          }
          if (trim(document.forms[0].txtFederalTax_cid.value)=="")
          {
          alert("Please select Federal Tax Entity/Payee.");
          document.forms[0].txtFederalTax.focus();
          return false;
          }
          if (trim(document.forms[0].lstTransTypeFed.value)=="")
          {
          alert("Please select Trans Type.");
          return false;
          }

          if (trim(document.forms[0].txtTaxPercentageFed.value) == "")
          {
          alert("Please enter valid Federal Tax Percentage.");
          document.forms[0].txtTaxPercentageFed.focus();
          return false;
          }

          if (trim(document.forms[0].txtSSNTax.value)=="")
          {
          alert("Please select Social Security Tax Entity/Payee.");
          document.forms[0].txtSSNTax.focus();
          return false;
          }

          if (trim(document.forms[0].txtSSNTax_cid.value)=="")
          {
          alert("Please select Social Security Tax Entity/Payee.");
          document.forms[0].txtSSNTax.focus();
          return false;
          }

          if (trim(document.forms[0].txtTaxPercentageSSN.value)=="")
          {
          alert("Please enter Social Security Tax Percentage.");
          document.forms[0].txtTaxPercentageSSN.focus();
          return false;
          }
          if (trim(document.forms[0].txtMEDTax.value)=="")
          {
          alert("Please select Medicare Tax Entity/Payee.");
          document.forms[0].txtMEDTax.focus();
          return false;
          }

          if (trim(document.forms[0].txtMEDTax_cid.value)=="")
          {
          alert("Please select Medicare Tax Entity/Payee.");
          document.forms[0].txtMEDTax.focus();
          return false;
          }

          if (trim(document.forms[0].lstTransTypeMED.value)=="")
          {
          alert("Please select Trans Type.");
          return false;
          }
          if (trim(document.forms[0].txtTaxPercentageMED.value) == "") 
          {
          alert("Please enter Medicare Tax Percentage.");
          document.forms[0].txtTaxPercentageMED.focus();
          return false;
          }
          //ybhaskar:START: MITS 15457: Validation function for the tax to be less than 100%
          if (document.getElementById("txtTaxPercentageFed") != null && document.getElementById("txtTaxPercentageSSN") != null && document.getElementById("txtTaxPercentageMED") != null) {
              if ((parseFloat(document.getElementById("txtTaxPercentageFed").value) + parseFloat(document.getElementById("txtTaxPercentageSSN").value) + parseFloat(document.getElementById("txtTaxPercentageMED").value)) >= 100) {
                  alert('Sum of Taxes cannot be greater than 100%');
                  return false;
              }
          }
          //ybhaskar : END


          //Changed by Gagan for MITS 22209 : Start          
          if (document.forms[0].lstTransTypeFed.value == document.forms[0].lstTransTypeMED.value
            || document.forms[0].lstTransTypeFed.value == document.forms[0].lstTransTypeSSN.value
            || document.forms[0].lstTransTypeMED.value == document.forms[0].lstTransTypeSSN.value)             
          {
              alert("Please select a different Trans Type for each Tax");
              return false;
          }
          //Changed by Gagan for MITS 22209 : End

          // Changed by Amitosh for MITS 22209 : Start

          if (checkMapping()) {
              alert("Please select a different Trans Type for each Tax and Offset ");
              return false;
          }
          //Changed by Amitosh for MITS 22209 : End

          return true;
      }

      //Changed by Gagan for MITS 22209 : Start
      function SaveOffSetMapping() {

          var lstTransTypePension = document.forms[0].lstTransTypePension.value;
          var lstTransTypeSSN1 = document.forms[0].lstTransTypeSSN1.value;
          var lstTransTypeOI = document.forms[0].lstTransTypeOI.value;
          var lstTransTypeOthOff1 = document.forms[0].lstTransTypeOthOff1.value;
          var lstTransTypeOthOff2 = document.forms[0].lstTransTypeOthOff2.value;
          var lstTransTypeOthOff3 = document.forms[0].lstTransTypeOthOff3.value;
          var lstTransTypePostTax01 = document.forms[0].lstTransTypePostTax01.value;
          var lstTransTypePostTax02 = document.forms[0].lstTransTypePostTax02.value;

          if (lstTransTypePension == lstTransTypeSSN1 ||
              lstTransTypePension == lstTransTypeOI ||
              lstTransTypePension == lstTransTypeOthOff1 ||
              lstTransTypePension == lstTransTypeOthOff2 ||
              lstTransTypePension == lstTransTypeOthOff3 ||
              lstTransTypePension == lstTransTypePostTax01 ||
              lstTransTypePension == lstTransTypePostTax02) {
              alert("Please select a different Trans Type for each Offset");
              return false;
          }

          if (lstTransTypeSSN1 == lstTransTypeOI ||
              lstTransTypeSSN1 == lstTransTypeOthOff1 ||
              lstTransTypeSSN1 == lstTransTypeOthOff2 ||
              lstTransTypeSSN1 == lstTransTypeOthOff3 ||
              lstTransTypeSSN1 == lstTransTypePostTax01 ||
              lstTransTypeSSN1 == lstTransTypePostTax02) {
             alert("Please select a different Trans Type for each Offset");
             return false;
            }

             if (lstTransTypeOI == lstTransTypeOthOff1 ||
                lstTransTypeOI == lstTransTypeOthOff2 ||
                lstTransTypeOI == lstTransTypeOthOff3 ||
                lstTransTypeOI == lstTransTypePostTax01 ||
                lstTransTypeOI == lstTransTypePostTax02)
                {
                    alert("Please select a different Trans Type for each Offset");
                    return false;
                }

                if (lstTransTypeOthOff1 == document.forms[0].lstTransTypeOthOff2.value ||
                    lstTransTypeOthOff1 == lstTransTypeOthOff3 ||
                    lstTransTypeOthOff1 == lstTransTypePostTax01 ||
                    lstTransTypeOthOff1 == lstTransTypePostTax02) {
                    alert("Please select a different Trans Type for each Offset");
                    return false;
                }

                if (document.forms[0].lstTransTypeOthOff2.value == lstTransTypeOthOff3 ||
                    document.forms[0].lstTransTypeOthOff2.value == lstTransTypePostTax01 ||
                    document.forms[0].lstTransTypeOthOff2.value == lstTransTypePostTax02) {
                    alert("Please select a different Trans Type for each Offset");
                    return false;
                }

                if(lstTransTypeOthOff3 == lstTransTypePostTax01 ||
                    lstTransTypeOthOff3 == lstTransTypePostTax02) {
                    alert("Please select a different Trans Type for each Offset");
                    return false;
                }

                if (lstTransTypePostTax01 == lstTransTypePostTax02) {
                    alert("Please select a different Trans Type for each Offset");
                    return false;
                }

                //Changed by Amitosh for MITS 22209 : Start


                if (checkMapping()) {
                    alert("Please select a different Trans Type for each Offset and Tax");
                    return false;
                }
                //Changed by Amitosh for MITS 22209 : End

                return true;
            }
            //Changed by Gagan for MITS 22209 : End

            //Changed by Amitosh for MITS 22209 : Start
            function checkMapping() {
                if (document.forms[0].lstTransTypeMED.value == document.forms[0].lstTransTypePension.value ||
                    document.forms[0].lstTransTypeMED.value == document.forms[0].lstTransTypeSSN1.value ||
                    document.forms[0].lstTransTypeMED.value == document.forms[0].lstTransTypeOI.value ||
                    document.forms[0].lstTransTypeMED.value == document.forms[0].lstTransTypeOthOff1.value ||
                    document.forms[0].lstTransTypeMED.value == document.forms[0].lstTransTypeOthOff2.value ||
                    document.forms[0].lstTransTypeMED.value == document.forms[0].lstTransTypeOthOff3.value ||
                    document.forms[0].lstTransTypeMED.value == document.forms[0].lstTransTypePostTax01.value ||
                    document.forms[0].lstTransTypeMED.value == document.forms[0].lstTransTypePostTax02.value) {
                    return true;
                }
                if (document.forms[0].lstTransTypeSSN.value == document.forms[0].lstTransTypePension.value ||
                    document.forms[0].lstTransTypeSSN.value == document.forms[0].lstTransTypeSSN1.value ||
                    document.forms[0].lstTransTypeSSN.value == document.forms[0].lstTransTypeOI.value ||
                    document.forms[0].lstTransTypeSSN.value == document.forms[0].lstTransTypeOthOff1.value ||
                    document.forms[0].lstTransTypeSSN.value == document.forms[0].lstTransTypeOthOff2.value ||
                    document.forms[0].lstTransTypeSSN.value == document.forms[0].lstTransTypeOthOff3.value ||
                    document.forms[0].lstTransTypeSSN.value == document.forms[0].lstTransTypePostTax01.value ||
                    document.forms[0].lstTransTypeSSN.value == document.forms[0].lstTransTypePostTax02.value) {
                    return true;
                }
                if (document.forms[0].lstTransTypeFed.value == document.forms[0].lstTransTypePension.value ||
                    document.forms[0].lstTransTypeFed.value == document.forms[0].lstTransTypeSSN1.value ||
                    document.forms[0].lstTransTypeFed.value == document.forms[0].lstTransTypeOI.value ||
                    document.forms[0].lstTransTypeFed.value == document.forms[0].lstTransTypeOthOff1.value ||
                    document.forms[0].lstTransTypeFed.value == document.forms[0].lstTransTypeOthOff2.value ||
                    document.forms[0].lstTransTypeFed.value == document.forms[0].lstTransTypeOthOff3.value ||
                    document.forms[0].lstTransTypeFed.value == document.forms[0].lstTransTypePostTax01.value ||
                    document.forms[0].lstTransTypeFed.value == document.forms[0].lstTransTypePostTax02.value) {
                    return true;
                }

                return false;

            }
            //Changed by Amitosh for MITS 22209 : End

          function SaveStateMapping() 
          {
             
          if (trim(document.forms[0].hdnAlreadyExists.value)!="1")
          {
          alert("Please save Federal tax mapping before saving State tax mapping information.");
          return false;
          }
          if (trim(document.forms[0].lstStates.value)=="")
          {
          alert("Please select state.");
          return false;
          }
          if (trim(document.forms[0].txtStateTax.value)=="")
          {
          alert("Please select Tax Entity /Payee.");
          document.forms[0].txtStateTax.focus();
          return false;
          }
          if (trim(document.forms[0].txtStateTax_cid.value)=="")
          {
          alert("Please select Tax Entity /Payee.");
          document.forms[0].txtStateTax.focus();
          return false;
          }
          if (trim(document.forms[0].lstTransTypeState.value)=="")
          {
          alert("Please select Trans Type.");
          return false;
          }

          if (trim(document.forms[0].txtTaxPercentageState.value)=="" || trim(document.forms[0].txtTaxPercentageState.value)== 0)
          {
          alert("Please enter Tax Percentage.");
          document.forms[0].txtTaxPercentageState.focus();
          return false;
          }
          return true;
          }
          function SelectionValid()
          {
                     for(var i = 0; i<document.forms[0].elements.length;i++) 
						if(document.forms[0].elements[i].type=='checkbox') 
							if(document.forms[0].elements[i].checked == true)
								return true;

					return false;
				}
				function onPageLoaded()
				{
					if (ie)
					{
						if ((eval("document.all.divForms")!=null) && (ieversion>=6))
						{
							eval("document.all.divForms").style.height=300;
						}
					}
					else
					{
						var o_divforms;
						o_divforms=document.getElementById("divForms");
						if (o_divforms!=null)
						{
							o_divforms.style.height=window.frames.innerHeight*0.70;
							o_divforms.style.width=window.frames.innerWidth*0.995;
						}
					}
					loadTabList();
				}
    </script>
    <script type="text/javascript" src="/../../scripts/rmx-common-ref.js">function lstTransTypeSSN_onclick() {}

    </script>

    <style type="text/css">
        .style1
        {
            width: 13%;
        }
    </style>

</head>
<body onload="javascript:onPageLoaded();parent.MDIScreenLoaded()">
    <form id="frmData" name="frmData" method="post" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <table border="0" cellspacing="0" cellpadding="0">
       <tr>
        <td class="Selected"  nowrap="nowrap" id="TABSTaxMapping"><a class="Selected" href="#" onclick="tabChange(this.name);return false;" name="TaxMapping" id="LINKTABSTaxMapping"><span style="text-decoration:none">Tax Mapping</span></a></td>
        <td   nowrap="nowrap" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
        <td class="NotSelected" nowrap="nowrap" id="TABSOffsets"><a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="Offsets" id="LINKTABSOffsets"><span style="text-decoration:none">Offsets</span></a></td>
        <td nowrap="nowrap" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
        <td valign="top" nowrap="nowrap"></td>
       </tr>
      </table>
   <%-- <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="Selected" nowrap="true" runat="server" name="TABSTaxMapping" id="TABSTaxMapping">
                        <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="TaxMapping" id="LINKTABSTaxMapping">Tax Mapping</a>
                    </td><td class="tabSpace" runat="server" id="TBSTaxMapping">
                        <nbsp />
                        <nbsp />
                    </td><td class="NotSelected" nowrap="true" runat="server" name="TABSOffsets" id="TABSOffsets">
                        <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="Offsets" id="LINKTABSOffsets">Offsets</a>
                    
            </td>
            <td>--%>
                <asp:TextBox runat="server" type="text" ID="SysFocusFields" Style="display: none"></asp:TextBox>
                <input type="hidden" id="hTabName" name="hTabName" />
                <input type="hidden" name="" value="" id="SysTabSelected" />
                <input type="hidden" id="SysFormName" value="TaxMapping" />
         <%--   </td>
        </tr>
    </table>--%>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" name="FORMTABTaxMapping"
        id="FORMTABTaxMapping">
       <%-- <tr>
            <td class="msgheader" colspan="5">
                Tax Mapping
            </td>
        </tr>--%>
        <tr>
            <td class="style1">
                <asp:TextBox runat="server" value="1" Style="display: none" ID="hdnAlreadyExists"></asp:TextBox>
                <asp:TextBox runat="server" Style="display: none"  rmxignorevalue="true" rmxref="/Instance/Document/TaxMapping/DeleteMapping"
                    ID="hdnSelected"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td class="ctrlgroup" colspan="5">
                Federal Tax
            </td>
        </tr>
        <tr>
            <td class="style1">  
                &nbsp;
            </td>
            <td width="30%">
                Tax Entity/Payee
            </td>
            <td width="30%">
                Trans Type
            </td>
            <td width="30%">
                Tax Percentage
            </td>
        </tr>
        <tr>
            <td class="style1">
                Federal Tax:
            </td>
            <td width="30%">
                <asp:TextBox runat="server" ID="tbFedValue" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='FEDERAL']/@value"></asp:TextBox>
                <asp:TextBox size="16" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='FEDERAL']/@payee" onblur="codeLostFocus(this.id);" ID="txtFederalTax"></asp:TextBox>
                <asp:Button runat="server" class="button" ID="txtFederalTaxbtn" OnClientClick="return lookupData('txtFederalTax','TAX_ENTITY',4,'txtFederalTax',2)" Text="..." />
                <asp:TextBox Style="display: none" ID="txtFederalTax_cid" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='FEDERAL']/@payeeid" value=""></asp:TextBox>
                <asp:TextBox Style="display: none" ID="tbFederalTax_cid" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='FEDERAL']/@payeeid" value=""></asp:TextBox>
                
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstTransTypeFed" type="combobox" runat="server" selected="true" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='FEDERAL']/@transType" itemsetref="/Instance/Document/TaxMapping/Transactions"></asp:DropDownList>
                <asp:TextBox runat="server" ID="txtFedTranstypeid" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='FEDERAL']/@transTypeid"></asp:TextBox>
                </td>
            <td width="30%">
                <asp:TextBox runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='FEDERAL']/@taxPercent"
                    value="" size="13" min="0" max="100" onchange="return numLostFocus(this)" ID="txtTaxPercentageFed"></asp:TextBox>
                <asp:Button runat="server" class="button" ID="btnTaxPercentageFed" OnClientClick="return openCalculator('txtTaxPercentageFed')"
                    Text="..." />
                <asp:TextBox type="text" runat="server" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='FEDERAL']/@taxPercent"
                    value="" min="0" max="100" ID="tbTaxPercentFed"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Social Security Tax:
            </td>
            <td width="30%">
                <asp:TextBox runat="server" ID="tbSSTValue" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='SOCIAL_SECURITY']/@value"></asp:TextBox>
                <asp:TextBox size="16" type="text" runat="server" ID="txtSSNTax" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='SOCIAL_SECURITY']/@payee" onblur="codeLostFocus(this.id);" ></asp:TextBox>
                <asp:Button runat="server" class="button" ID="txtSSNTaxbtn" OnClientClick="return lookupData('txtSSNTax','TAX_ENTITY',4,'txtSSNTax',2)" Text="..." />
                <asp:TextBox Style="display: none" ID="txtSSNTax_cid" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='SOCIAL_SECURITY']/@payeeid"></asp:TextBox>
                <asp:TextBox Style="display: none" ID="tbSOCIAL_SECURITY_cid" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='SOCIAL_SECURITY']/@payeeid"></asp:TextBox>                
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstTransTypeSSN" selected="true" type="combobox" runat="server"
                    rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='SOCIAL_SECURITY']/@transType"
                    ItemSetRef="/Instance/Document/TaxMapping/Transactions">
                </asp:DropDownList>
                <asp:TextBox runat="server" ID="txtSSNTransTypeId" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='SOCIAL_SECURITY']/@transTypeid"></asp:TextBox>
            </td>
            <td width="30%">
                <asp:TextBox type="text" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='SOCIAL_SECURITY']/@taxPercent"
                    size="13" min="0" max="100" onchange="return numLostFocus(this)" ID="txtTaxPercentageSSN"></asp:TextBox>
                <asp:Button class="button" runat="server" ID="btnTaxPercentageSSN" OnClientClick="return openCalculator('txtTaxPercentageSSN')"
                    Text="..." />
                <asp:TextBox type="text" runat="server" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='SOCIAL_SECURITY']/@taxPercent"
                    value="" min="0" max="100" onchange="return numLostFocus(this)" ID="tbTaxPercentSST"></asp:TextBox>
            </td>
            <td width="*">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style1">
                Medicare Tax:
            </td>
            <td width="30%">
                <asp:TextBox runat="server" ID="tbMedicareValue" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='MEDICARE']/@value"></asp:TextBox>
                <asp:TextBox size="16" type="text" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='MEDICARE']/@payee" onchange="lookupTextChanged(this);" onblur="codeLostFocus(this.id);" ID="txtMEDTax"></asp:TextBox>
                <asp:Button runat="server" class="button" ID="Button1" OnClientClick="return lookupData('txtMEDTax','TAX_ENTITY',4,'txtMEDTax',2)" Text="..." />
                <asp:TextBox Style="display: none" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='MEDICARE']/@payeeid" value="" ID="txtMEDTax_cid"></asp:TextBox>
                <asp:TextBox Style="display: none" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='MEDICARE']/@payeeid" value="" ID="tbMEDTax_cid"></asp:TextBox>                
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstTransTypeMED" selected="true" type="combobox" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='MEDICARE']/@transType" ItemSetRef="/Instance/Document/TaxMapping/Transactions"></asp:DropDownList>
                <asp:TextBox runat="server" ID="txtMedTransTypeId" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='MEDICARE']/@transTypeid"></asp:TextBox>
            </td>
            <td width="30%">
                <asp:TextBox type="text" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@value='MEDICARE']/@taxPercent"
                    size="13" min="0" max="100" onchange="return numLostFocus(this)" ID="txtTaxPercentageMED"></asp:TextBox>
                <asp:Button class="button" runat="server" ID="btnTaxPercentageMED" OnClientClick="return openCalculator('txtTaxPercentageMED')"
                    Text="..." />
                <asp:TextBox type="text" runat="server" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/FederalTaxes[@name='MEDICARE']/@taxPercent"
                    min="0" max="100" onchange="return numLostFocus(this)" ID="tbTaxPercentageMED"></asp:TextBox>                
            </td>
            <td width="*">
                &nbsp;
            </td>
        </tr>
        
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button runat="server" Text="Save Federal Tax" OnClick="saveFederalTax_btn_Click" class="button" OnClientClick="return SaveFeredalMapping();" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="ctrlgroup" colspan="4" width="100%">
                State Tax
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <tr>
                    <td class="style1">
                        State
                    </td>
                    <td width="30%">
                        Tax Entity/Payee
                    </td>
                    <td width="30%">
                        Trans Type
                    </td>
                    <td width="25%">
                        Tax Percentage
                    </td>
                </tr>
            </td>
        </tr>
       <td class="style1">
            <tr>
                <td class="style1">
                <%--<asp:TextBox runat="server" ID="tbStateValue" Style="display: none" rmxref="/Instance/Document/TaxMapping/StateTax[@name='STATE']/@value"></asp:TextBox>--%>
                
                <asp:DropDownList ID="lstStates" type="combobox" selected="true" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/StateTax[@value='STATE']/@state" ItemSetRef="/Instance/Document/TaxMapping/States" AutoPostBack="false"></asp:DropDownList>
                <asp:TextBox runat="server" ID="txtState" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/StateTax[@name='STATE']/@stateId"></asp:TextBox>
                </td>
                <td width="30%">
                <asp:TextBox size="16" runat="server" type="text" rmxref="/Instance/Document/TaxMapping/TaxMapValues/StateTax[@name='STATE']/@payee" cancelledvalue="" onchange="lookupTextChanged(this);" onblur="codeLostFocus(this.id);" ID="txtStateTax"></asp:TextBox>
                <asp:Button type="button" class="button" ID="txtStateTaxbtn" runat="server" OnClientClick="return lookupData('txtStateTax','TAX_ENTITY',4,'txtStateTax',2)" Text="..." />
                <asp:TextBox runat="server" type="text"  style="display:none" id="txtStateTax_cid" cancelledvalue="" rmxref="/Instance/Document/TaxMapping/TaxMapValues/StateTax[@name='STATE']/@payeeid" />
                   
                </td>
                <td width="30%">
                   <asp:DropDownList ID="lstTransTypeState" selected="true" type="combobox" runat="server" itemsetref="/Instance/Document/TaxMapping/Transactions" RMXRef="/Instance/Document/TaxMapping/TaxMapValues/StateTax[@value='STATE']/@transType" AutoPostBack="false"></asp:DropDownList>
                   <asp:TextBox runat="server" ID="txtTransTypeStateId" Style="display: none" rmxref="/Instance/Document/TaxMapping/TaxMapValues/StateTax[@name='STATE']/@transTypeid"></asp:TextBox>
                </td>
                <td width="30%">
                    <asp:TextBox type="text" runat="server" rmxref="/Instance/Document/TaxMapping/TaxMapValues/StateTax[@name='STATE']/@taxPercent"
                         size="13" min="0" max="100" onchange="return numLostFocus(this)" ID="txtTaxPercentageState"></asp:TextBox>
                    <%--<asp:TextBox type="text" runat="server"  style="display:none"  rmxref="/Instance/Document/TaxMapping/TaxMapValues/StateTax[@value='test']/@taxPercent"
                        size="30" min="0" max="100" onchange="return numLostFocus(this)" ID="tbTaxPercentageState"></asp:TextBox>--%>
                    <asp:Button type="button" runat="server" class="button" ID="btnTaxPercentageState"
                        OnClientClick="return openCalculator('txtTaxPercentageState')" Text="..." />
                </td>
                <td width="*">
                    &nbsp;
                </td>
            </tr>
      </td>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button runat="server" Text="Add State Map" class="button" OnClientClick="return SaveStateMapping();"
                    ID="addStateMapbtn" OnClick="addStateMap_btn_Click" />
            </td>
            <td colspan="2" align="left">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button runat="server" Text="Remove State Map" class="button" OnClientClick="return DeleteMapping();"
                    OnClick="removeStateMapbtn_Click" ID="removeStateMapbtn" />
            </td>
        </tr>
        
        <tr>
        <td colspan="4">
        <table width="100%" style="margin-right: 0px; height: 191px;">
            <tr>
                <td width="80%">
                    <div style="width:98%; height: 300px; overflow: auto">
                        <asp:GridView ID="gvTaxMappingGrid" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma"
                            Font-Size="Smaller" CssClass="singleborder" Width="98%" Height="" 
                            CellPadding="4" ForeColor="#333333" GridLines="None" 
                            HorizontalAlign="Left" ondatabound="gvTaxMappingGrid_DataBound">
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <input id="cbTaxMappingGrid" name="TaxMappingGrid" type="checkbox" value='<%# Eval("CBId")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="CBId" HeaderText="RowId" Visible="false" ItemStyle-HorizontalAlign="Center" >
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="state" HeaderText="State" />
                                <asp:BoundField DataField="payee" HeaderText="Payee" />
                                <asp:BoundField DataField="transType" HeaderText="Trans Type"/>
                                <asp:BoundField DataField="taxPercent" HeaderText="Tax Percentage" />
                            </Columns>
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <HeaderStyle CssClass="colheader3"/>
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </div>
                </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="hdnCbId" runat="server" Style="display: none" RMXType="id"></asp:TextBox>
                    </td>
                </tr>
        </table>
        </td>
        </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="4" name="FORMTABOffsets"
        id="FORMTABOffsets" style="display: none;">
        <tr>
            <td width="15%">
                &nbsp;
            </td>
            <td width="85%">
                Trans Type: 
        </tr>
        <tr>
            <td width="15%">
                Pension:
            </td>
            <td width="85%">
                <asp:DropDownList ID="lstTransTypePension" type="combobox" selected="true" runat="server"
                    rmxref="Instance/Document/TaxMapping/Offsets/Offset[@name='PENSION']" ItemSetRef="/Instance/Document/TaxMapping/Transactions"
                    AutoPostBack="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td width="15%">
                Social Security:
            </td>
            <td width="85%">
                <asp:DropDownList ID="lstTransTypeSSN1" type="combobox" selected="true" runat="server"
                    rmxref="/Instance/Document/TaxMapping/Offsets/Offset[@name='SOCIAL_SECURITY']"
                    ItemSetRef="/Instance/Document/TaxMapping/Transactions" AutoPostBack="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td width="15%">
                Other Income:
            </td>
            <td width="85%">
                <asp:DropDownList ID="lstTransTypeOI" type="combobox" selected="true" runat="server"
                    rmxref="Instance/Document/TaxMapping/Offsets/Offset[@name='OTHER_INCOME']" ItemSetRef="/Instance/Document/TaxMapping/Transactions"
                    AutoPostBack="false">
                </asp:DropDownList>
            </td>
        </tr>
        <!--pmittal5 Mits 14841 04/27/09 - GHS Enhancements-->
        <tr>
            <td width="15%">
                Other Offset #1:
            </td>
            <td width="85%">
                <asp:DropDownList ID="lstTransTypeOthOff1" type="combobox" selected="true" runat="server"
                    rmxref="Instance/Document/TaxMapping/Offsets/Offset[@name='OTHEROFFSET1']" ItemSetRef="/Instance/Document/TaxMapping/Transactions"
                    AutoPostBack="false">
                </asp:DropDownList>
            </td>
        </tr>
                <tr>
            <td width="15%">
                Other Offset #2:
            </td>
            <td width="85%">
                <asp:DropDownList ID="lstTransTypeOthOff2" type="combobox" selected="true" runat="server"
                    rmxref="Instance/Document/TaxMapping/Offsets/Offset[@name='OTHEROFFSET2']" ItemSetRef="/Instance/Document/TaxMapping/Transactions"
                    AutoPostBack="false">
                </asp:DropDownList>
            </td>
        </tr>
                <tr>
            <td width="15%">
                Other Offset #3:
            </td>
            <td width="85%">
                <asp:DropDownList ID="lstTransTypeOthOff3" type="combobox" selected="true" runat="server"
                    rmxref="Instance/Document/TaxMapping/Offsets/Offset[@name='OTHEROFFSET3']" ItemSetRef="/Instance/Document/TaxMapping/Transactions"
                    AutoPostBack="false">
                </asp:DropDownList>
            </td>
        </tr>
                <tr>
            <td width="15%">
                Post Tax Deduction #1:
            </td>
            <td width="85%">
                <asp:DropDownList ID="lstTransTypePostTax01" type="combobox" selected="true" runat="server"
                    rmxref="Instance/Document/TaxMapping/Offsets/Offset[@name='POST_TAX_DED1']" ItemSetRef="/Instance/Document/TaxMapping/Transactions"
                    AutoPostBack="false">
                </asp:DropDownList>
            </td>
        </tr>
                <tr>
            <td width="15%">
                Post Tax Deduction #2:
            </td>
            <td width="85%">
                <asp:DropDownList ID="lstTransTypePostTax02" type="combobox" selected="true" runat="server"
                    rmxref="Instance/Document/TaxMapping/Offsets/Offset[@name='POST_TAX_DED2']" ItemSetRef="/Instance/Document/TaxMapping/Transactions"
                    AutoPostBack="false">
                </asp:DropDownList>
            </td>
        </tr>
        <!--End - pmittal5 -->
        <tr>
            <td colspan="4" align="center">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <!--Changed by Gagan for MITS 22209 : Start-->
                <asp:Button ID="Save_Offssetbtn" Type="button" runat="server" Text="Save Offsets" OnClientClick="return SaveOffSetMapping();" 
                    OnClick="saveOffsets_Click" class="button" />
                <!--Changed by Gagan for MITS 22209 : End-->
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="SysWindowId"></asp:TextBox>
    <input type="hidden" runat="server" value="" />
    </form>
</body>
</html>
