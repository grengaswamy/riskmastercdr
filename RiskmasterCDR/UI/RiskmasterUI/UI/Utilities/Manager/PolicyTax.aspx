﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyTax.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.PolicyTax" %>
<%@ Register TagName="PleaseWaitDialog" TagPrefix="uc2" Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add/Modify Tax.</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>
    <%--vkumar258 - RMA-6037 - Starts --%>
    <%--<script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/drift.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/dhtml-div.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/dhtml-help-setup.js"></script>
    <uc3:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form id="frmData" method="post" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
            <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                border="0" ID="save" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" OnClientClick="return ValidateFieldsforTax();"
                OnClick="save_Click" /></div>
    </div>
    <div class="msgheader" id="formtitle">
        Add/Modify Tax</div>
    <table border="0">
        <tr>
            <td class="ctrlgroup" colspan="2">
                Tax Parameters
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_useTax" Text="Use Tax:" />
            </td>
            <td>
                <asp:CheckBox runat="server" name="useTax" rmxref="/Instance/Document/Document/Tax/use" ID="useTax" onchange="ApplyBool(this);"/>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_LOBcode" Text="Line of Business:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" ID="LOBcode" CodeTable="POLICY_LOB" ControlName="LOBcode" 
                    type="code" class="required" rmxref="/Instance/Document/Document/Tax/lob"/>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_Statecode" Text="State:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" ID="Statecode" CodeTable="states" ControlName="Statecode"
                    type="code" class="required" rmxref="/Instance/Document/Document/Tax/state" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_Amount" Text="Amount:" class="required" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="Amount" rmxref="/Instance/Document/Document/Tax/amount" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_TaxType" Text="Flat Or Percent:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" ControlName="TaxType" ID="TaxType" type="code"
                    CodeTable="DISCOUNT_TYPE" class="required" rmxref="/Instance/Document/Document/Tax/taxtype" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_EffectiveDate" Text="Effective Date:" class="required" />&nbsp;&nbsp;
            </td>
            <td>
                <asp:TextBox runat="server" FormatAs="date" ID="EffectiveDate" RMXRef="/Instance/Document/Document/Tax/effectivedate"
                    RMXType="date" TabIndex="2" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                    <%--vkumar258 - RMA-6037 - Starts --%>
                    <%-- <asp:Button class="DateLookupControl" runat="server" ID="EffectiveDatebtn" TabIndex="3" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "EffectiveDate",
				                ifFormat: "%m/%d/%Y",
				                button: "EffectiveDatebtn"
				            }
				            );
                </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#EffectiveDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../../Images/calendar.gif",
                                //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "3");
                        });
                    </script>
                    <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="label" id="lbl_ExpirationDate" text="Expiration Date:" />
                    &nbsp;&nbsp;
                </td>
                <td>
                    <asp:textbox runat="server" formatas="date" id="ExpirationDate" rmxref="/Instance/Document/Document/Tax/expirationdate"
                        rmxtype="date" tabindex="4" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                    <%--vkumar258 - RMA-6037 - Starts --%>
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="ExpirationDatebtn" TabIndex="3" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "ExpirationDate",
				                ifFormat: "%m/%d/%Y",
				                button: "ExpirationDatebtn"
				            }
				            );
                </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#ExpirationDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../../Images/calendar.gif",
                                //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "5");
                        });
                    </script>
                    <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
        </table>
        <uc2:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="Loading" />
        <asp:textbox style="display: none" runat="server" id="RowId" rmxref="/Instance/Document/Document/Tax/PolicyTaxrowid" />
        <asp:textbox style="display: none" runat="server" id="selectedrowposition" />
        <asp:textbox style="display: none" runat="server" id="gridname" />
        <asp:textbox style="display: none" runat="server" id="txtPostBack" />
        <asp:textbox style="display: none" runat="server" id="txtvalidate" />
        <asp:textbox style="display: none" runat="server" id="mode" />
        <asp:textbox style="display: none" runat="server" id="FormMode" />
        <asp:textbox style="display: none" runat="server" id="txtData" />
    </form>
</body >
</html>
