﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoFROIACORDSetup.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.AutoFROIACORDSetup" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
    <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">{var i;}</script>
     <script type="text/javascript">
				function AddFilter(mode)
					{
						var optionRank;
						var optionObject;						
						selectObject=document.getElementById('lstTabs');
						
						if (mode=="selected")
						{
							//Add selected Available Values
							select=document.getElementById('lstTopDowns');
							for (var x = select.options.length - 1; x >= 0 ; x--)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;									
									//remove from available list	
									removeOption('lstTopDowns',select.options[x].value);							
									setDataChanged(true);
								}								
							}
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('lstTopDowns');
							if (select.options.length > 0)
							{
							    for (var x = select.options.length-1; x >= 0; x--)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
									setDataChanged(true);
								}
								select.options.length=0;								
							}
						}						
						return false;
					}
					
					function RemoveFilter(mode)
					{
						var optionRank;
						var optionObject;
						selectObject=document.getElementById('lstTopDowns');
						
						if (mode=="selected")
						{
							//Add selected Available Values
							select=document.getElementById('lstTabs');
							for (var x = select.options.length-1; x >=0; x--)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
									
									//remove from available list	
									removeOption('lstTabs',select.options[x].value);							
									setDataChanged(true);
								}
							}
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('lstTabs');
							if (select.options.length > 0)
							{
							    for (var x = select.options.length-1; x >= 0; x--)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
									setDataChanged(true);
								}
								select.options.length=0;
							}
						}						
						return false;
					}
					
				function AddToTopdownLayout() {
				var selIndex = document.forms[0].lstTabs.selectedIndex;
				if(document.forms[0].lstTabs.selectedIndex<0)
					return false;
				var sValue=document.forms[0].lstTabs.options[document.forms[0].lstTabs.selectedIndex].value;
				var sText=document.forms[0].lstTabs.options[document.forms[0].lstTabs.selectedIndex].text;
				
				for(var f=0;f<document.forms[0].lstTopDowns.options.length;f++)
					if(document.forms[0].lstTopDowns.options[f].value==sValue)
						return false;

				document.forms[0].lstTabs.options[selIndex] = null;
				document.forms[0].lstTabs.selectedIndex = selIndex - 1;
				if(document.forms[0].lstTabs.selectedIndex == -1)
					document.forms[0].lstTabs.selectedIndex = 0;

				var opt=new Option(sText, sValue, false, false);
				document.forms[0].lstTopDowns.options[document.forms[0].lstTopDowns.options.length]=opt;
				m_DataChanged=true;
				return false;
				}

                //Start by Shivendu :MITS 22270 email address validation
				function trim(svalue) {
				    svalue = svalue.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
				    return svalue;
				}
				function ValidateEmailAddress() {
				    var fromAddress = document.forms[0].EmailFrom.value;
				    fromAddress = trim(fromAddress);
				    if (fromAddress != "") {
				        validRegExp = /^[^@]+@[^@]+.[a-z]{2,}$/i;
				        if (fromAddress.search(validRegExp) == -1) {
				            alert("Enter a valid email address");
				            document.forms[0].EmailFrom.focus();
				            return false;
				        }
				    }
				    return true;

				}
				//End by Shivendu :MITS 22270 email address validation
				

				function SelectAll() {
				    //Start by Shivendu :MITS 22270 email address validation
				    if (!ValidateEmailAddress()) {
				        return false;
				    }
				    //End by Shivendu :MITS 22270 email address validation
                    pleaseWait.Show();
					hndlstTabs1 = document.getElementById('hndlstTabs');
					hndTopDown = document.getElementById('hndTopDown');
					var values = "";

					for (var f = 0; f < document.forms[0].lstTopDowns.options.length; f++) {
					    values += document.forms[0].lstTopDowns.options[f].value + "=" + document.forms[0].lstTopDowns.options[f].text + ";";
					}
					hndTopDown.value += values;

					values = "";
					for (var f = 0; f < document.forms[0].lstTabs.options.length; f++) {
					    values += document.forms[0].lstTabs.options[f].value + "=" + document.forms[0].lstTabs.options[f].text + ";";
					}
					hndlstTabs1.value = values;					
					return true;
									
				}
					
					function removeOption(selectName,id)
					{
						select=document.getElementById(selectName);
						Ids=new Array();
						Names=new Array();

						for(var x = 0; x < select.options.length ; x++)
						{
							if(select.options[x].value!=id)
							{
								Ids[Ids.length] = select.options[x].value;
								Names[Names.length]=select.options[x].text;
							}
						}

						select.options.length = Ids.length;

						for(var x = 0;x < select.options.length; x++)
						{
							select.options[x].text=Names[x];
							select.options[x].value=Ids[x];
						}
					}													
				</script>
    <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">{var i;}</script>  
    <style type="text/css">
        .style1
        {
            width: 382px;
        }
    </style>
  </head>  
 <body class="" onload="loadTabList();parent.MDIScreenLoaded();" style="height:90%">
    <form id="frmData" runat="server" >
        <uc1:ErrorControl  ID="ErrorControl1" runat="server" />
        <input type="hidden" name="hTabName" />
        <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
         <tr>
          <td align="center" valign="middle" height="32">         
          <asp:ImageButton runat="server" id="Save" ImageUrl="~/Images/tb_save_active.png" class="bold"
          ToolTip="Save" TabIndex = "8" onclick="Save_Click" OnClientClick = "return SelectAll();"/>
          </td>
         </tr>
        </table>
        </div>
        <div class="msgheader" id="formtitle">Auto FROI ACORD Settings</div>
        <div class="singletopborder" style="position:relative;left:0;top:0;width:80%;height:83%;overflow:auto;">                            
            <table border="0" cellspacing="0" cellpadding="0" id="FORMTABAck" >
                <tr id="Tr1">
                <td colspan="5">
                 <table width="100%" align="left">
                  <tr>
                   <td><asp:TextBox Style="display: none" runat="server" id="hdnIncAdjusters"></asp:TextBox></td>                    
                  </tr>
                 </table>
                </td>
                </tr>
                <table>
                 <tr>
                  <td width="5%" nowrap="true"><b>Excluded Adjusters:</b></td>
                  <td width="5%"></td>
                  <td width="*" nowrap="true"><b>Included Adjusters:</b></td>
                 </tr>
                 <tr>
                  <td width="5%" nowrap="true">
                  
                  <asp:ListBox  ToolTip = "Current Adjusters from this list when assigned to a Claim or are edited/saved will NOT trigger Auto FROI ACORD." type="combobox" 
                          rmxref="/Instance/Document/Adjuster/control[@name='ExcAdjusters']" 
                          rmxignoreset="true" runat="server" 
                          ID="lstTopDowns" EnableViewState="false" TabIndex = "1"
                          size="10" Height="150px" style="margin-top: 0px" Width="136px" 
                          SelectionMode="Multiple"></asp:ListBox>
                  <asp:HiddenField  ID ="hndTopDown" runat="server" />  
                  <asp:TextBox runat ="server" rmxref="/Instance/Document/Adjuster/control[@name='ExcAdjusters']" rmxignoreget="true" ID="txtTopDown" style="display :none"></asp:TextBox>                                    
                 
                  <td width="40%" valign="middle" align="center">
                  <br/>
                  <asp:Button runat="server" ID="btn1" Text="&gt;&gt; Add &gt;&gt;" class="button" TabIndex = "2"
                          style="width:95" OnClientClick="return AddFilter('selected');" Height="26px" 
                          Width="150px"/><br/><br/>
                  <%--<asp:Button runat="server" ID="btn2" Text="Add All &gt;&gt;" class="button" TabIndex = "3"
                          style="width:80; margin-left: 0px;" OnClientClick="return AddFilter('all');" 
                          Height="20px" Width="128px"/><br/><br/>--%>
                  <asp:Button runat="server" ID="btn3" Text="<< Remove <<" class="button" TabIndex = "4"
                          style="width:95; margin-left: 0px;" 
                          OnClientClick="return RemoveFilter('selected');" Height="26px" 
                          Width="150px"/><br/><br/>
                  <%--<asp:Button runat="server" ID="btn4" Text="<< Remove All" class="button" TabIndex = "5"
                          style="width:80" OnClientClick="return RemoveFilter('all');" Height="20px" 
                          Width="128px"/>--%>
                  </td>
                  <td nowrap="true" width="*">
                                  
                 <asp:ListBox ToolTip = "Current Adjusters from this list when assigned to a Claim or are edited/saved will trigger Auto FROI ACORD." type="combobox" rmxref="/Instance/Document/Adjuster/control[@name='IncAdjusters']"  
                          runat="server"   TabIndex = "6"
                          OnClientClick="return AddToTopdownLayout();" ID="lstTabs" rmxignoreset="true"  
                          size="10" Height="150px" style="margin-top: 0px" Width="136px" SelectionMode="Multiple" 
                          ></asp:ListBox>
                          <asp:HiddenField  ID ="hndlstTabs" runat="server" />  
                 <asp:TextBox runat ="server"  ID="txtlstTabs" rmxref="/Instance/Document/Adjuster/control[@name='IncAdjusters']" rmxignoreget="true" style="display :none"></asp:TextBox>
                 </tr>
                </table>
                
                <tr>
                    <td width="100%">                                                                      
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td class="style1">
                                         <br/>
                                         <b>Sender Email Address:  </b>
                                </td>                                
                                <td><asp:TextBox ID="EmailFrom" size="30" type="text" MaxLength="100" TabIndex = "7" onchange="setDataChanged(true);" value="" rmxref="/Instance/Document/Adjuster/control[@name='EmailFrom']" runat="server"> </asp:TextBox></td>                                
                            </tr>
                            <%--<tr ><td></td>asd<td></td></tr>--%>
                            <tr><td height ="20px"></td><td></td></tr>
                            <tr><%--MITS 20166 start--%>
                                <td class="style1">
                                         Use Additional fields in email [ACORD] </td>                                
                                <td>
                                         
                                         <asp:CheckBox ID="chkEmailAcord"  runat="server" rmxref="/Instance/Document/Adjuster/control[@name='chkEmailAcord']" />
                                         <%--rmxref="/Instance/Document/Adjuster/control[@name='EmailFrom']"--%>
                                        </td>                                
                            </tr>
                            <tr>
                                <td class="style1">
                                         Use Additional fields in email [FROI]
                                         </td>                                
                                <td>
                                         
                                         <asp:CheckBox ID="chkEmailFroi"  rmxref="/Instance/Document/Adjuster/control[@name='chkEmailFroi']"                                             
                                             runat="server" />
                                         
                                </td>                                <%--rmxref="/Instance/Document/Adjuster/control[@name='EmailFrom']" --%>
                            </tr><%--MITS 20166 End--%>
                        </table>                                  
                    </td>                  
                </tr> 
            </table>                         
        </div>
      <input type="text" name="" value="" id="SysViewType" style="display:none">
      <input type="text" name="" value="" id="SysCmd" style="display:none">
      <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none">
      <input type="text" name="" value="" id="SysCmdQueue" style="display:none">
      <input type="text" name="" value="" id="SysCmdText" style="display:none" >
      <input type="text" name="" value="" id="SysClassName" style="display:none" >
      <input type="text" name="" value="" id="SysSerializationConfig" style="display:none">
      <input type="text" name="" value="" id="SysFormIdName" style="display:none ">
      <input type="text" name="" value="" id="SysFormPIdName" style="display:none ">
      <input type="text" name="" value="" id="SysFormPForm" style="display:none">
      <input type="text" name="" value="" id="SysInvisible" style="display:none" >
      <input type="text" name="" value="" id="SysFormName" style="display:none" >
      <input type="text" name="" value="" id="SysRequired" style="display:none">
      <input type="text" name="" value="" id="SysFocusFields" style="display:none"></td>
        <script language="javascript" type="text/javascript">
            try {
                var elem = document.getElementById("dOracleCaseInsensitive");
                var elem1 = document.getElementById("dOracleCaseInsensitiveCheckBox"); 
                var ctrl = document.getElementById("hdnOracleCaseInsensitive");
                if (ctrl.value == 'true') {
                    elem.style.display = 'none';
                    elem1.style.display = 'none';
                }
                else {
                    elem.style.display = '';
                    elem1.style.display = '';
                }
    	      
            } catch (ex) { }
	        </script>	
    <asp:TextBox ID="SysWindowId" type="hidden" value="rmx-widget-handle-3" style="display:none"  runat="server"></asp:TextBox>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    
  
    </form>
</body>
</html>
