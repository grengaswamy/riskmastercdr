﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiscTier.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.DiscTier" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc3" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Add/Modify Discount Tier</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />
   <%-- <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
    </style>--%>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>

    <uc3:commontasks id="CommonTasks1" runat="server" />
    <%--vkumar258 - RMA-6037 - Starts --%>
    <%--<script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>

    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>

    <%--  <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/dhtml-div.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/dhtml-help-setup.js"></script>

    <script src="csc-Theme/riskmaster/common/javascript/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/window.js"
        type="text/javascript"></script>

    <script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/dialog.js"
        type="text/javascript"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar-setup.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/calendar-alias.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-xml.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-editable.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-query.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/rmx-common-ref.js"></script>--%>
</head>
<%--<body class="10pt" onload="CheckForWindowClosing();">--%>
<body onload="CopyGridRowDataToPopup();">
    <form runat="server" id="frmData" method="post">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
    <div class="toolbardrift" id="toolbardrift" name="toolbardrift">
        <table class="toolbar" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td align="center" valign="middle" height="32">
                        <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="ImageButton1" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                            onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" OnClientClick="return ValidateFieldsforDiscTier();"
                            OnClick="save_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    <br>
    <div class="msgheader" id="formtitle">
        Add/Modify Discount Tier</div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Discount Tier Parameters
                                </td>
                            </tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="RowId"></asp:TextBox></tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="AddedByUser"></asp:TextBox></tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="DttmRcdAdded"></asp:TextBox></tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="DttmRcdLastUpd"></asp:TextBox></tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="UpdatedByUser"></asp:TextBox></tr>
                            <tr>
                                <td>
                                    Use Discount Tier:&nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" Checked="False" RMXref="/Instance/Document//control[@name='UseDiscTier']"
                                        ID="UseDiscTier" onclick="setDataChanged(true);" />
                                    <%--                                        <input type="text" name="$node^56" value=""
                                            style="display: none" cancelledvalue="" id="UseDiscTier_mirror">--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_TierName" Text="Tier Name" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox runat="server" RMXref="/Instance/Document//control[@name='TierName']"
                                            size="30" ID="TierName" onchange="setDataChanged(true);"></asp:TextBox></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_LOBcode" Text="Line of Business" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <%--<input type="text" name="$node^69" value="" size="30" onchange="lookupTextChanged(this);"
                                        id="LOBcode" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;"><input
                                            type="button" class="CodeLookupControl" id="LOBcodebtn" onclick="selectCode('POLICY_LOB','LOBcode')"><input
                                                type="text" name="$node^70" value="" style="display: none" id="LOBcode_cid" cancelledvalue="">--%>
                                    <uc2:CodeLookUp runat="server" ID="LOBcode" CodeTable="POLICY_LOB" ControlName="LOBcode"
                                        RMXRef="/Instance/Document/Document/DiscTier/control[@name='LOBcode']" type="code"
                                        Required="true" TabIndex="0" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_Statecode" Text="State" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <uc2:CodeLookUp runat="server" ID="Statecode" CodeTable="states" ControlName="Statecode"
                                        RMXRef="/Instance/Document/Document/DiscTier/control[@name='Statecode']" type="code"
                                        TabIndex="0" Required="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_EffectiveDate" Text="Effective Date:" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox runat="server" FormatAs="date" ID="EffectiveDate" RMXRef="/Instance/Document//control[@name='EffectiveDate']"
                                        RMXType="date" onchange="setDataChanged(true);" 
                                        onblur="dateLostFocus(this.id);" />
                                        <%--vkumar258 - RMA-6037 - Starts --%>
                                        <%-- <asp:Button class="DateLookupControl" runat="server" ID="EffectiveDatebtn" />

                                    <script type="text/javascript">
                                        Zapatec.Calendar.setup(
				            {
				                inputField: "EffectiveDate",
				                ifFormat: "%m/%d/%Y",
				                button: "EffectiveDatebtn"
				            }
				            );
                                    </script>--%>

                                        <script type="text/javascript">
                                            $(function () {
                                                $("#EffectiveDate").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "../../../Images/calendar.gif",
                                                   // buttonImageOnly: true,
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    changeYear: true
                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                            });
                                        </script>
                                        <%--vkumar258 - RMA_6037- End--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:label runat="server" class="label" id="lbl_ExpirationDate" text="Expiration Date:" />
                                        &nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <asp:textbox runat="server" formatas="date" id="ExpirationDate" rmxref="/Instance/Document//control[@name='ExpirationDate']"
                                            rmxtype="date" onchange="setDataChanged(true);"
                                            onblur="dateLostFocus(this.id);" />
                                        <%--vkumar258 - RMA-6037 - Starts --%>
                                        <%--<asp:Button class="DateLookupControl" runat="server" ID="ExpirationDatebtn" />

                                    <script type="text/javascript">
                                        Zapatec.Calendar.setup(
				            {
				                inputField: "ExpirationDate",
				                ifFormat: "%m/%d/%Y",
				                button: "ExpirationDatebtn"
				            }
				            );
                                    </script>--%>

                                        <script type="text/javascript">
                                            $(function () {
                                                $("#ExpirationDate").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "../../../Images/calendar.gif",
                                                    //buttonImageOnly: true,
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    changeYear: true
                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                            });
                                        </script>
                                        <%--vkumar258 - RMA_6037- End--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_Parent_Level" Text="Parent Level:" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox runat="server" size="30" ID="ParentLevel" RMXref="/Instance/Document//control[@name='ParentLevel']" onchange="setDataChanged(true);"></asp:TextBox>
                                    <asp:Button runat="server" class="button" OnClientClick="return OpenDiscTierParentLevelWindow();"
                                        Text="..." /></div>
                                </td>
                            </tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" RMXref="/Instance/Document//control[@name='ParentLevel']/@codeid" ID="ParentLevel_cid"></asp:TextBox></tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Style="display: none" ID="FormMode"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:TextBox Style="display: none" runat="server" ID="gridname"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="mode"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtvalidate" ></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtData" ></asp:TextBox>
                    <%--                 <asp:TextBox runat="server" ID="SysViewType" Style="display: none"></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysCmd" Style="display: none"></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysCmdConfirmSave" Style="display: none"></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysCmdQueue" Style="display: none"></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysCmdText" Style="display: none" rmxforms:value="Navigate"></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysClassName" Style="display: none" rmxforms:value=""></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysSerializationConfig" Style="display: none"></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysFormIdName" Style="display: none" rmxforms:value="RowId">
                    </asp:TextBox>
                    <asp:TextBox runat="server" ID="SysFormPIdName" Style="display: none" rmxforms:value="RowId"></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysFormPForm" Style="display: none" rmxforms:value="DiscTier"></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysInvisible" Style="display: none" rmxforms:value=""></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysFormName" Style="display: none" rmxforms:value="DiscTier"></asp:TextBox>
                    <asp:TextBox runat="server" ID="SysRequired" Style="display: none"></asp:TextBox>
                    <asp:TextBox runat="server" Style="display: none" ID="validate" rmxforms:value=""></asp:TextBox>--%>
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-3" id="SysWindowId" />
     <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
