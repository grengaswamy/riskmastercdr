﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProcessCodeMapping.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ProcessCodeMapping" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"  TagPrefix="mc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js"></script>
     <script type="text/javascript">
         $(document).ready(function () {
             $('tbody input[type=checkbox]').change(function () {
                 if ($(this).closest('tr').hasClass('ctrlgroup')) {
                     if ($(this).is(':checked')) {
                         SelectAll();
                     }
                     else {
                         UnSelectAll();
                     }
                 }
                 else {
                     if ($(this).is(':checked')) {
                         SelectHeader();
                     }
                     else {
                         UnSelectHeader();
                     }

                 }
             });
         });

         function SelectAll() {
             $('tbody input[type=checkbox]').prop('checked', true);
             //for (var i = 0; i < document.getElementById('frmData').elements.length; i++) {
             //    if (document.getElementById('frmData').elements(i).type == 'checkbox') {
             //        document.getElementById('frmData').elements(i).checked = chkID.checked;
             //    }
             //}
         }
         function UnSelectAll() {
             $('tbody input[type=checkbox]').prop('checked', false);
         }

         function SelectHeader() {
             if ($('tbody input:checked').length === ($('tbody input[type=checkbox]').length - 1)) {
                 $('th input[type=checkbox]').prop('checked', true);
             }
         }

         function UnSelectHeader() {
             $('th input[type=checkbox]').prop('checked', false);
         }

         function ValidateAdd() {
             if (document.getElementById('txtProcessTableId').value == "") {

                 //debugger;
                 alert(ProcessCodeMappingValidations.ThirdPartyTableExist);
                 return false;
             }
             if (document.getElementById('txtRMTableId').value == "") {
                 alert(ProcessCodeMappingValidations.RMATableExist);
                 return false;
             }

         }

         function DeleteCode() {
             var selectedVal = "";
             if (SelectionValid()) {
                 ret = confirm(ProcessCodeMappingValidations.RemoveMappingRecord);
                 if (ret == true) {
                     $('input:checked').each(function () {
                         if (!$(this).closest('tr').hasClass('ctrlgroup')) {
                             if (selectedVal == "") {
                                 selectedVal = $(this).val();
                             }
                             else {
                                 selectedVal = selectedVal + "," + $(this).val();
                             }
                         }
                     });

                     //for (var i = 0; i < document.getElementById('frmData').elements.length; i++)
                     //{
                     //    if (document.getElementById(i).type == 'checkbox') {
                     //        if (document.getElementById('frmData').elements(i).checked == true) {
                     //            if (selectedVal == "") {
                     //                selectedVal = document.getElementById('frmData').elements(i).value;
                     //            }
                     //            else {
                     //                selectedVal = selectedVal + "," + document.getElementById('frmData').elements(i).value;
                     //            }
                     //        }
                     //    }
                     //}
                     document.getElementById('hdnSelected').value = selectedVal;
                     return true;
                 }
                 else {
                     return false;
                 }
             }
             else {
                 alert(ProcessCodeMappingValidations.TableMappingRemove);
                 return false;
             }
         }

         function SelectionValid() {
             if ($('input:checked').length > 0) {
                 return true;
             }

             //for (var i = 0; i < document.getElementById('frmData').elements.length; i++)
             //    if (document.getElementById('frmData').elements(i).type == 'checkbox')
             //        if (document.getElementById('frmData').elements(i).checked == true)
             //            return true;

             return false;
         }

         function SelectedCode() {
             var selectedVal = "";
             for (var i = 0; i < document.getElementById('frmData').elements.length; i++) {
                 if (document.getElementById('frmData').elements(i).type == 'checkbox') {
                     if (document.getElementById('frmData').elements(i).checked == true) {
                         selectedVal = "true";
                     }
                     else {
                         if (document.getElementById('frmData').elements(i).checked == false) {
                             selectedVal = "false";
                         }
                         break;
                     }
                 }
             }

             if (selectedVal == "false") {
                 if (document.getElementById('GridViewAvailableTPA_ctl01_cbHdrSelectAll').checked == true) {
                     document.getElementById('GridViewAvailableTPA_ctl01_cbHdrSelectAll').checked = false;
                 }
             }
         }

      </script>
</head>
<body>
  <form id="frmData" runat="server">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  <table width="100%" cellspacing="0" cellpadding="0">
		
		<tr class="msgheader">
		<!-- Label control added by Shivendu for MITS 16148 -->
		    <td colspan="2"><asp:Label ID="lblTPApage" runat="server" Text="Label"></asp:Label></td>
		</tr>
	</table>
	<br/>
    <table width="100%" cellspacing="0" cellpadding="0">
		<tr>
            <td>
             <asp:Label ID="lblProcessTableName" runat="server" Text="<%$ Resources:lblProcessTableNamedr %>"></asp:Label>   
            </td>
            <td>
                <asp:DropDownList ID="lstProcessTableName" type="combobox"  runat="server" AutoPostBack="true" ></asp:DropDownList>
                <asp:TextBox runat="server" ID="txtProcessTableId" Style="display: none" rmxref="/Instance/Document/TPACodeMapping/SelectedProcessTable"></asp:TextBox>
                <%--<asp:TextBox ID="txtTPASystemName" type="text"  runat="server" />--%>
            </td>
        </tr>

        <tr>
            <td>
            <asp:Label ID="lblRMTableName" runat="server" Text="<%$ Resources:lblRMTableNamedr %>"></asp:Label>  
            </td>
            <td>
                <asp:DropDownList ID="lstRMTableName" type="combobox"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstRMTableName_SelectedIndexChanged"></asp:DropDownList>
                <asp:TextBox runat="server" ID="txtRMTableId" Style="display: none" rmxref="/Instance/Document/TPACodeMapping/SelectedRMTable"></asp:TextBox>              
                <%--<asp:TextBox ID="txtTPAName" type="text"  runat="server" />  --%>
            </td>
        </tr>
        <tr></tr>
         <tr></tr>
        <tr>
            <td align="right">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button runat="server" Text="Add Mapping " class="button" 
                    ID="btnadd" OnClick="addTPABtn_Click" OnClientClick="return ValidateAdd();" />
            </td>
            <td align="center">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button runat="server" Text="Remove Mapping" class="button" ID="btnremove" 
                OnClick="removeTPBtn_Click" OnClientClick="return DeleteCode();" />
            </td>             
        </tr>

        <tr>
            <td class="style1">
                <asp:TextBox runat="server" Style="display: none" rmxignorevalue="true" ID="hdnSelected"></asp:TextBox>
            </td>
        </tr>

        <tr class="ctrlgroup">
		    <asp:GridView ID="GridViewAvailableTPA" AutoGenerateColumns="False" runat="server" Font-Bold="True" 
		    CellPadding="0" GridLines="None" CellSpacing="0" Width="100%"  EmptyDataText ="No TPA Available" >
		    <HeaderStyle CssClass="ctrlgroup"/>
            <rowstyle CssClass ="datatd" HorizontalAlign="Left" Font-Bold="false"  />
            <alternatingrowstyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />
                 <Columns>
                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader">
                     <HeaderTemplate>
                        <input id="cbHdrSelectAll" name="cbHdrSelectAll" runat="server" type="checkbox"  value='-1'  />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <input id="cbAvailableTPAGrid" name="AvailableTPAMappingGrid" runat="server" type="checkbox"
                            value='<%# Eval("CBId")%>'  />
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Third Party Table Name" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("ProcessTable")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RMA Table Name" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("RMATable")%>
                        </ItemTemplate> 
                    </asp:TemplateField>
                </Columns> 
            </asp:GridView>     
		</tr>


    </table> 

    <br/>

     
  </form>
    
</body>
</html>
