﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class PolicyBatchFile : NonFDMBasePageCWS
    {

        string sCWSresponse = string.Empty;
        XElement XmlTemplate = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;

            try
            {
                
                if(!IsPostBack)
                NonFDMCWSPageLoad("PolicyBatchFileAdaptor.GetPolicySystems");
                //bReturnStatus = CallCWS("PolicyBatchFileAdaptor.GetTransactions", XmlTemplate, out sCWSresponse, true, true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("PolicyBatchFileAdaptor.GetTransactions");
            sXml = sXml.Append("</Function></Call><Document><PolicySystems><FunctionToCall/><SelectedPolicy>");
            sXml = sXml.Append(lstPolicySys.SelectedValue);
            sXml = sXml.Append("</SelectedPolicy>");
            sXml = sXml.Append("</PolicySystems></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());

            return XmlTemplate;

        }

        
        protected void btnCreateFile_Click(object sender, EventArgs e)
        {
            try
            {

            NonFDMCWSPageLoad("PolicyBatchFileAdaptor.CreateFile");
             }
             catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void lstPolicySys_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XmlDocument objXml = null;
            try
            {
        // XmlTemplate = GetMessageTemplate();
                SelectedPolId.Text = lstPolicySys.SelectedValue;
           bReturnStatus= CallCWS("PolicyBatchFileAdaptor.GetTransactions", XmlTemplate, out sCWSresponse, true, true);
            objXml = new XmlDocument();
            objXml.LoadXml(sCWSresponse);

            if (bReturnStatus && (objXml.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success"))
            {
                lstPolicySys.SelectedValue = SelectedPolId.Text;

            }
           
             }
             catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


  
    }
}