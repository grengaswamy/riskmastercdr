<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FROIBatchPrintingOptions.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.FROIBatchPrintingOptions" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
  <title>FROI Batch Printing Options</title>
  <link rel="stylesheet" href="../../../Content/system.css" type="text/css"/>
  <script type="text/javascript" src="../../../Scripts/form.js"></script>
  <script type="text/javascript" src="../../../Scripts/drift.js"></script>
  <script type="text/javascript" src="../../../Scripts/Utilities.js">{var i;}</script>
</head>

<body class="" onload="FROIBatchPrinting_Load();parent.MDIScreenLoaded();">
  <form id="frmData"  method="post" runat="server">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
   
  <div class="" id="toolbardrift" >
    <table class="toolbar" cellspacing="0" cellpadding="0" border="0">
     <tbody>
      <tr>
       <td align="center" valign="middle" height="32"><asp:ImageButton id="btnSave" runat="server"  ImageUrl="~/Images/tb_save_active.png" alt=""   title="Save" onclientclick="return UtilitiesForm_Save();" ToolTip="Save" OnClick="Save" /></td>
      </tr>
     </tbody>
    </table>
   </div>
   
   <div class="msgheader" id="formtitle">FROI Batch Printing Options</div>
   <div class="errtextheader"></div>
   <table border="0">
    <tbody>
     <tr>
      <td>
       <table border="0">
        <tbody>
         <tr><td><asp:TextBox runat="server"  value="1" style="display:none" type="id" id="RowId" rmxref="Instance/Document/form/group/displaycolumn/control[@name='RowId']" /></td></tr>
         <tr>
          <td><asp:Label runat="server"  id="DescLabel"  type="labelonly" Text="RiskMaster can display two levels of the Org-Hierarchy in the FROI Batch Printing Window." /></td>
         </tr>
         <tr>
          <td>High Level:&nbsp;&nbsp;</td>
          <td><asp:DropDownList id="OrgHighLevel" type="combobox" runat="server" ItemSetRef="/Instance/Document/form/group/displaycolumn/control[@name='OrgHighLevel']" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='OrgHighLevel']/@value"  onchange="FROIBatchPrinting_LoadLowLevel();" AutoPostBack="false" OnSelectedIndexChanged="Drp_Save">
              </asp:DropDownList></td>
         </tr>
         <tr>
          <td>Low Level:&nbsp;&nbsp;</td>
          <td><asp:DropDownList  id="OrgLowLevel" type="combobox" ItemSetRef="/Instance/Document/form/group/displaycolumn/control[@name='OrgLowLevel']" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='OrgLowLevel']/@value"  onchange="setDataChanged(true);" AutoPostBack="false" >
             </asp:DropDownList></td>
         </tr>
         <tr>
          <td>Exclude Master Status Closed Claims:&nbsp;&nbsp;</td>
          <td><asp:CheckBox value="True" type="checkbox" appearance="full" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='ExcludeMaster']" id="ExcludeMaster"  OnClientCheckedChanged="ApplyBool(this);"/>
              <asp:TextBox  runat="server" value="" style="display:none"  id="ExcludeMaster_mirror"/>
          </td>
         </tr>
         <tr>
          <td><asp:TextBox runat="server"  value="" style="display:none" id="hdAction" type="hidden"/></td>
         </tr>
         <tr>
          <td><asp:TextBox  type="hidden" runat="server" value="1" style="display:none" id="hdDataChanged" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdDataChanged']"/></td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td></td>
         </tr>
        </tbody>
       </table><input type="text" name="" value="" id="SysViewType" style="display:none"/>
       <input type="text" name="" value="" id="SysCmd" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdQueue" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdText" style="display:none" />
       <input type="text" name="" value="" id="SysClassName" style="display:none" />
       <input type="text" name="" value="" id="SysSerializationConfig" style="display:none"/>
       <input type="text" name="" value="" id="SysFormIdName" style="display:none" />
       <input type="text" name="" value="" id="SysFormPIdName" style="display:none" />
       <input type="text" name="" value="" id="SysFormPForm" style="display:none" />
       <input type="text" name="" value="" id="SysInvisible" style="display:none" />
       <input type="text" name="" value="" id="SysFormName" style="display:none" />
       <input type="text" name="" value="" id="SysRequired" style="display:none"/></td>
      <td valign="top"></td>
     </tr>
    </tbody>
   </table>
  </form> 
 </body>
</html>