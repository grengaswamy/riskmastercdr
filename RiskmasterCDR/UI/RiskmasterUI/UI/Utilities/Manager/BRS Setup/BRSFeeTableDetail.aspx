﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="BRSFeeTableDetail.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSFeeTableDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../../../Scripts/zapatec/zpcal/themes/system.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script type="text/jscript" language="javascript" src="../../../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/rmx-common-ref.js"></script>
    <script src="../../../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
    <script src="../../../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>

    <!--
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
    -->
    <!--MITS 34721 : hlv ML-->    
    <link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <!--34721 : hlv ML-->

    <script type="text/javascript" src="../../../../Scripts/calendar-alias.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="javascript">

        //MITS 34721 hlv begin
        function getRealDateStr(datepart) {
            var tmp = "00" + datepart;
            return tmp.substr(tmp.length - 2, 2);
        }
        //MITS 34721 hlv end

        function BRSFeeTableDetail_Save() {
            //zalam 05/30/2008 Mits:-12269 Start
            var FeeSched = document.getElementById("FeeSchedule");
            var ucr = document.getElementById("UCRPerc");
            var def = document.getElementById("DefPerc");

            //This section is used for validating dates Start
            if (document.forms[0].StartDt.value != "") {
                //var strfd = document.forms[0].StartDt.value;

                if (document.forms[0].EndDt.value != "") {
                    // var strtd = document.forms[0].EndDt.value;

                    //MITS 34721 hlv begin
                    var tmpfd = $("#StartDt").datepicker('getDate');
                    var tmptd = $("#EndDt").datepicker('getDate');

                    var strfd = "" + tmpfd.getFullYear() + getRealDateStr(tmpfd.getMonth()) + getRealDateStr(tmpfd.getDate());
                    var strtd = "" + tmptd.getFullYear() + getRealDateStr(tmptd.getMonth()) + getRealDateStr(tmptd.getDate());
                    //MITS 34721 hlv end
                    /*
                    var arrfd = strfd.split("/");
                    var arrtd = strtd.split("/");

                    if (arrfd[0].length != 2) {
                        arrfd[0] = '0' + arrfd[0];
                    }
                    if (arrfd[1].length != 2) {
                        arrfd[1] = '0' + arrfd[1];
                    }
                    if (arrtd[0].length != 2) {
                        arrtd[0] = '0' + arrtd[0];
                    }
                    if (arrtd[1].length != 2) {
                        arrtd[1] = '0' + arrtd[1];
                    }
                    */
                    //var strfd = arrfd[2] + arrfd[0] + arrfd[1];
                    //var strtd = arrtd[2] + arrtd[0] + arrtd[1];
                }
            }
            //Validation dates End

            if (document.forms[0].UserTableName.value == "" || document.forms[0].DbType.value == '-1' ||
               document.forms[0].StartDt.value == "" || document.forms[0].state.value == "" ||
               document.forms[0].priority.value == "") {
                //alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
                alert(BRSFeeTableDetailValidations.valMain);
                return false;
            }
            else if (FeeSched.parentElement.parentElement.style.display != "none" && (document.forms[0].FeeSchedule.value == '-1' || document.forms[0].FeeSchedule.value == '') && document.forms[0].DbType.value != '12') {
                //alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
                alert(BRSFeeTableDetailValidations.valMain);
                return false;
            }
            else if (ucr.parentElement.parentElement.style.display != "none" && document.forms[0].UCRPerc.value == '0') {
                //alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
                alert(BRSFeeTableDetailValidations.valMain);
                return false;
            }
            else if (def.parentElement.parentElement.parentElement.style.display != "none" && document.forms[0].DefPerc.value == "") {
                //alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
                alert(BRSFeeTableDetailValidations.valMain);
                return false;
            }
            else if (strfd > strtd) {
                //alert("End Date must be greator or equal to Start Date or it should be Blank");
                alert(BRSFeeTableDetailValidations.valDate);
                return false;
            }
            else {

                return true;
            }

        }

    </script>
</head>
<body class="10pt"  >
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    
    <div>
       <table class="toolbar" cellSpacing="0" cellPadding="0" border="0">
     <tbody>
      <tr>
       <td align="center" valign="middle" HEIGHT="32">
       <asp:ImageButton src="../../../../Images/save.gif" alt="" id="Save" 
               onmouseover="javascript:document.all['Save'].src='../../../../Images/save2.gif'" 
               onmouseout="javascript:document.all['Save'].src='../../../../Images/save.gif'" 
               title="<%$ Resources:ttSave %>" OnClientClick="return BRSFeeTableDetail_Save();" runat="server" 
               onclick="Save_Click"  /></td>
      </tr>
     </tbody>
    </table>
   </div><br /><br /><div class="msgheader" id="formtitle"><asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label></div>
   
  
       <table border="0">
        <tbody>
         <tr>
          <td class="ctrlgroup" colSpan="2"><asp:Label ID="lblGroup" runat="server" Text="<%$ Resources:lblGroup %>"></asp:Label></td>
         </tr>
         <tr>
         
         <asp:TextBox rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='RowId']"  value="" style="display:none" id="RowId" runat="server" ></asp:TextBox>
         </tr>
         <tr>
          <td><u><asp:Label ID="lblUserTableName" runat="server" Text="<%$ Resources:lblUserTableName %>"></asp:Label></u>&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px"><asp:TextBox rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UserTableName']" runat="server" size="30" id="UserTableName" onchange="setDataChanged(true);" ></asp:TextBox></div>
          </td>
         </tr>
       
         <tr>
          <td><u><asp:Label ID="lblDbType" runat="server" Text="<%$ Resources:lblDbType %>"></asp:Label></u>&nbsp;&nbsp;
          </td>
          <td>
          <asp:DropDownList runat="server" AutoPostBack="true" id="DbType" 
                  itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='DbType']" 
                  rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DbType']/@value" 
                  onchange="BRSFeeTableDetail_DbType();" type="combobox" 
                  onselectedindexchanged="DbType_SelectedIndexChanged"></asp:DropDownList>
              </td>
         </tr>
         <tr>
          <td><u><asp:Label ID="lblFeeSchedule" runat="server" Text="<%$ Resources:lblFeeSchedule %>"></asp:Label></u>&nbsp;&nbsp;
          </td>
          <td><asp:DropDownList runat="server" 
                  itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='FeeSchedule']" 
                  rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FeeSchedule']/@value"  
                  id="FeeSchedule" onchange="BRSFeeTableDetail_FeeSchedule();" type="combobox" 
                  onselectedindexchanged="FeeSchedule_SelectedIndexChanged">
          
          </asp:DropDownList>
            </td>
         </tr>
         <tr>
          <td colspan="2" ><asp:Label ID="FileName" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FileName']" ></asp:Label></td>
         </tr>
         <tr>
          <td><u><asp:Label ID="lblStartDt" runat="server" Text="<%$ Resources:lblStartDt %>"></asp:Label></u>&nbsp;&nbsp;
          </td>
          <td><asp:TextBox rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='StartDt']"  value="" runat="server" size="30" id="StartDt" onchange="&#xA;                        setDataChanged(true);dateLostFocus(this.id);  &#xA;                        " onblur="dateLostFocus(this.id);&#xA;"></asp:TextBox>
              <!--<input type="button" class="DateLookupControl" id="StartDtbtn" />-->
              <script type="text/javascript">
                  /*
                  Zapatec.Calendar.setup(
                  {
                      inputField : "StartDt",
                      ifFormat : "%m/%d/%Y",
                      button : "StartDtbtn"
                  }
                  );
                  */
                  $(function () {
                      $("#StartDt").datepicker({
                          showOn: "button",
                          buttonImage: "../../../../Images/calendar.gif",
                         // buttonImageOnly: true,
                          showOtherMonths: true,
                          selectOtherMonths: true,
                          changeYear: true
                      }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                  });
										</script></td>
         </tr>
         <tr>
          <td><asp:Label ID="lblEndDt" runat="server" Text="<%$ Resources:lblEndDt %>"></asp:Label>&nbsp;&nbsp;</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EndDt']"  value="" size="30" rmxforms:as="date" id="EndDt" onchange="&#xA;                        setDataChanged(true);dateLostFocus(this.id);  &#xA;                        " onblur="dateLostFocus(this.id);&#xA;"></asp:TextBox>
              <!--<input type="button" class="DateLookupControl" id="EndDtbtn">-->
              <script type="text/javascript">
                  /*
                  Zapatec.Calendar.setup(
                  {
                      inputField : "EndDt",
                      ifFormat : "%m/%d/%Y",
                      button : "EndDtbtn"
                  }
                  );
                  */
                  $(function () {
                      $("#EndDt").datepicker({
                          showOn: "button",
                          buttonImage: "../../../../Images/calendar.gif",
                          //buttonImageOnly: true,
                          showOtherMonths: true,
                          selectOtherMonths: true,
                          changeYear: true
                      }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                  });
										</script></td>
         </tr>
         <tr>
          <td><u><asp:Label ID="lblState" runat="server" Text="<%$ Resources:lblState %>"></asp:Label></u>&nbsp;&nbsp;
          </td>
          <td><asp:TextBox runat="server" type="code" codetable="STATES" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='state']"  size="30" onchange="lookupTextChanged(this);" id="state" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;"></asp:TextBox>
            <input type="button" class="CodeLookupControl" id="statebtn" onclick="selectCode('STATES', 'state')" />
            <asp:TextBox runat="server" style="display:none" id="state_cid" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='state']/@codeid" Text="" />
          </td>
         </tr>
         <tr>
          <td><u><asp:Label ID="lblPriority" runat="server" Text="<%$ Resources:lblPriority %>"></asp:Label></u>&nbsp;&nbsp;
          </td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='priority']"  value="" size="30" onblur="numLostFocus(this);" id="priority" onchange="setDataChanged(true);"></asp:TextBox></td>
         </tr>
         <tr>
          <td><u><asp:Label ID="lblUCRPerc" runat="server" Text="<%$ Resources:lblUCRPerc %>"></asp:Label></u>&nbsp;&nbsp;
          </td>
          <td><asp:DropDownList rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UCRPerc']/@value" itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='UCRPerc']" runat="server" id="UCRPerc" type="combobox" onchange="BRSFeeTable_ChangeDefaultPercentile()"></asp:DropDownList>
           </td>
         </tr>
         <tr>
          <td><u><asp:Label ID="lblDefPerc" runat="server" Text="<%$ Resources:lblDefPerc %>"></asp:Label></u>&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px"><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DefPerc']"  value="" size="30" id="DefPerc" onchange="&#xA;                        ;setDataChanged(true);&#xA;                      "></asp:TextBox></div>
          </td>
         </tr>
         <tr>
          <td><asp:Label ID="lblEobFormName" runat="server" Text="<%$ Resources:lblEobFormName %>"></asp:Label>&nbsp;&nbsp;</td>
          <td>
           <div title="" style="padding: 0px; margin: 0px"><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EobFormName']" value="" size="30" id="EobFormName" onchange="&#xA;                        ;setDataChanged(true);&#xA;                      "></asp:TextBox></div>
          </td>
         </tr>
         <tr>
          <td><asp:Label ID="lblEobCode" runat="server" Text="<%$ Resources:lblEobCode %>"></asp:Label>&nbsp;&nbsp;</td>
          <td><asp:DropDownList  runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EobCode']"  itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='EobCode']" id="EobCode" type="combobox" onchange=""></asp:DropDownList></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdnTableType']"  value="1" style="display:none" id="hdnTableType"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CalculationType']"  value="" style="display:none" id="CalculationType"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='iCalculationType']" value="" style="display:none" id="iCalculationType"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='bFeeSchdChanaged']" value="false" style="display:none" id="bFeeSchdChanaged"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='TableId']"  value="" style="display:none" type="hidden" id="TableId"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FormMode']" value="" style="display:none" id="FormMode"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DefPercMin']" value="" style="display:none" id="DefPercMin"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DefPercMax']" value="" style="display:none" id="DefPercMax"></asp:TextBox></td>
         </tr>
           <tr>
          <td id="tdTableType" runat="server" style="display:none"><asp:Label ID="lblTableType" runat="server" Text="Table Type:"></asp:Label>&nbsp;&nbsp;</td>
          <td></td>
         </tr>
         <tr>
          <td colspan="2" id="tdTableType2" runat="server" style="display:none">
           <asp:RadioButton ID="TableType1" runat="server" value="1" Text="Third Party" GroupName="TableType" />
           <asp:RadioButton ID="TableType2" runat="server" value="1" Text="User Defined"  GroupName="TableType" />
           <asp:TextBox runat="server" ID="tblType" Text="1" style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='TableType']" ></asp:TextBox>
          </td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td>
              <script language="JavaScript" type="text/javascript" SRC="">{var i;}</script>
              <!--<input type="button" class="button" name="btnFactor" value="Factors" onClick="BRSFeeTableDetail_Factors();" style="display:none" id="btnFactor">-->
              <asp:Button ID="btnFactor" Text="<%$ Resources:btnFactors %>" class="button" style="display:none" OnClientClick="BRSFeeTableDetail_Factors();return false;" runat="server" />
          </td>
         </tr>
        </tbody>
       </table>
       
       <input type="hidden" runat="server" id="hdntitle" />
    
    </form>
</body>
</html>
