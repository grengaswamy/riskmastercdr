﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSSettings : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement oXmlTemplate = null;
            string sReturnValue = string.Empty;
            if (!IsPostBack)
            {
                CallCWS("BRSSettingAdaptor.Get", oXmlTemplate, out sReturnValue, true, true);
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            XElement oXmlTemplate = null;
            string sReturnValue = string.Empty;
            CallCWS("BRSSettingAdaptor.Save", oXmlTemplate, out sReturnValue, true, true);

            RemoveLitItems(sReturnValue);
        }

        /// <summary>
        /// if returned list is empty, we have to remove the list manually because
        /// databindinghelper will not remove the existing items
        /// </summary>
        /// <param name="sReturn">The XML come back from web service</param>
        private void RemoveLitItems(string sReturnValue)
        {
            //if returned list is empty, we have to remove the list manually because
            //databindinghelper will not remove the existing items
            XElement oReturn = XElement.Parse(sReturnValue);
            XElement oFieldList = oReturn.XPathSelectElement("//Settings/CandidateFields");
            if (oFieldList != null)
            {
                if (!oFieldList.HasElements)
                    lstFields.Items.Clear();
            }

            XElement oKeepOnList = oReturn.XPathSelectElement("//Settings/KeepOnNextFields");
            if (oKeepOnList != null)
            {
                if (!oKeepOnList.HasElements)
                    lstKeepOnNextFields.Items.Clear();
            }
        }

    }
}
