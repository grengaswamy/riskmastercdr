﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSHospitalPerDiem : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            TableId.Text =  AppHelper.GetQueryStringValue("tableid");
            CalcTypeCode.Text = AppHelper.GetQueryStringValue("calctype");
            UserTableName.Text = AppHelper.GetQueryStringValue("usertablename");

            if (IsPostBack != true )
            {
                string sCWSresponse = string.Empty;
                bool bResult = false;
                bResult = CallCWS("BRSHospitalPerDiemValuesAdaptor.Get", null, out sCWSresponse, true, true);
                ErrorControl1.errorDom = sCWSresponse;
            }
        }

        protected void btnFinancials_Click(object sender, EventArgs e)
        {
            string sCWSresponse = string.Empty;
            bool bResult = false;
            bResult = CallCWS("BRSHospitalPerDiemValuesAdaptor.Save", null, out sCWSresponse, true, false); //csingh7 MITS 15195 Setting bSetControl as false
            ErrorControl1.errorDom = sCWSresponse;
            if (bResult)
            {
                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            }
        }
    }
}
