﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.RMXResourceManager; //MITS 34530 - hlv

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSCPTBasicCodeDetails : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //MITS 34530 - hlv begin
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("BRSCPTBasicCodeDetails.aspx"), "BRSCPTBasicCodeDetails",
                                            ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "BRSCPTBasicCodeDetails", sValidationResources, true);
            //MITS 34530 - hlv end
            
            hdnTableId.Text = AppHelper.GetQueryStringValue("tableid");
            
            //Added:Yukti,ML Changes
            //RowId.Text =  AppHelper.GetQueryStringValue("rowid");
            RowId.Text = HttpUtility.ParseQueryString(Request.RawUrl).Get("rowid");
            //Added:Yukti,ML Changes
            if (IsPostBack != true && RowId.Text != "")
            {
                string sCWSresponse = string.Empty;
                
                CPTCode.Text = RowId.Text;
                CPTCodeComp.Text = RowId.Text;
                
                CallCWS("BRSCPTBasicCodeDetailsAdaptor.Get", null, out sCWSresponse, true, true);
                SetSpecialityCode(sCWSresponse);
                
            }
        }

        private void SetSpecialityCode(string sCWSresponse)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(sCWSresponse);

            if (xmlDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerXml == "Success")
            {
                XmlElement objXmlElement = (XmlElement)xmlDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name='SpecialityCode']");
                SpecialityCode_cid.Value = objXmlElement.GetAttribute("codeid");
            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            using (XmlReader reader = Xelement.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            XmlElement objXmlElement = (XmlElement)xmlNodeDoc.SelectSingleNode("Message/Document/form/group/displaycolumn/control[@name='SpecialityCode']");
            objXmlElement.SetAttribute("codeid", SpecialityCode_cid.Value);
            objXmlElement.SetAttribute("codetable", "SPECIALTY");

            Xelement = XElement.Parse(xmlNodeDoc.InnerXml);
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            bool bReturnStatus = false;
            bReturnStatus = CallCWSFunction("BRSCPTBasicCodeDetailsAdaptor.Save");

            if (bReturnStatus)
            {
                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            }
        }
    }
}
