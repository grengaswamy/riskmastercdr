﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSCPTCodesListing.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSCPTCodesListing" %>
<%@ Register src="../../../Shared/Controls/CommonTasks.ascx" tagname="CommonTasks" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>CPT Codes Listing</title>
    <script language="javascript" type="text/javascript">        
        var bSelected = false;
        function SelectGridRow(RowId) {

            document.forms[0].hdnSelectedId.value = RowId;
            bSelected = true;
        }

        function openAddBasicCode() {
            //Added if condition for MITS:16451-Extended BRS Type
            if (document.forms[0].hdnCalcType.value == "14") {
                window.open("BRSCPTExtendedCodeDetails.aspx?tableid=" + document.forms[0].hdnTableId.value + "&amp;usertablename=" + document.forms[0].hdnUserTableName.value, "BRSModifierValue",
					'width=400,height=500,top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=yes');
            }
            else {

                window.open("BRSCPTBasicCodeDetails.aspx?tableid=" + document.forms[0].hdnTableId.value + "&amp;usertablename=" + document.forms[0].hdnUserTableName.value, "BRSModifierValue",
					'width=400,height=500,top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=yes');
            }
            return false;
        }

        function openEditBasicCode() {

            if (bSelected) {
                //Added for MITS:16451-Extended BRS Type
                //Added:Yukti,ML Changes
                if (document.forms[0].hdnCalcType.value == "14") {
                    window.open("BRSCPTExtendedCodeDetails.aspx?tableid=" + document.forms[0].hdnTableId.value + "&amp;rowid=" + encodeURIComponent(document.forms[0].hdnSelectedId.value), "BRSModifierValue",
					'width=400,height=500,top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=yes');
                }
                else {
                    //Added:Yukti,ML Changes
                    window.open("BRSCPTBasicCodeDetails.aspx?tableid=" + document.forms[0].hdnTableId.value + "&amp;rowid=" + encodeURIComponent(document.forms[0].hdnSelectedId.value), "BRSModifierValue",
					'width=400,height=500,top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=yes');
                }

            }
            return false;
        }
    
    </script>
</head>
<body>
    <form id="frmData" runat="server">
     <uc1:CommonTasks ID="CommonTasks1" runat="server" />
     
     <input type="hidden" runat="server" id="hdnUserTableName" />
    <input type="hidden" runat="server" id="hdnTableId" />
    <input type="hidden"  id="hdnSelectedId" runat="server" />
    <input type="hidden" id="hdnCalcType" runat="server" />
    <input type="hidden" id="hdnMode" runat="server" />
    <div>
     <table  id="tblGrid" runat="server"  width="550px" cellspacing="0" cellpadding="0" border="0">
         <tr>
				
				<td class="ctrlgroup" colSpan="2">
				  <asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label>
				</td>
		</tr>
		<tr>
		        <td colspan="2"><b></b>&nbsp;</td>
		</tr>
		<tr>
          <td ><div class="msgheader" id="formtitle"><asp:Label ID="lblUserTableName" runat="server" ></asp:Label> </div></td>
         </tr>
         <b></b>&nbsp;
		<tr>
		       <td width="95%" colspan="1" class="singleborder" >
		          <div id="divGrid" style="overflow:auto; height:400px"  >
		           <asp:GridView ID="GridView1" runat="server" AllowPaging="false" Width="100%"  AutoGenerateColumns="false"  
                            GridLines="None" onrowdatabound="GridView1_RowDataBound"  >
		              <HeaderStyle CssClass="msgheader" />
		              <AlternatingRowStyle CssClass="data2" />
		              <Columns>
		                  <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                      <input type="radio" id="selectrdo" name="BRSFeeTableGrid" />
                                    </ItemTemplate> 
                          </asp:TemplateField> 
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrCPT %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CPT")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrDes %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Desc")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrAmt %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblSysTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                         </Columns> 
		           </asp:GridView>
		           </div>
		       </td>
		       <td width="5%" valign="top" colspan="1">
		        
		        <!--<input type="image" src="../../../../Images/new.gif" alt="" id="New_BRSFeeTableGrid" onmouseover="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new2.gif'" onmouseout="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new.gif'" title="New" onclick="return openAddBasicCode();" /><br />-->
		        <asp:ImageButton runat="server" src="../../../../Images/new.gif" alt="" 
                       id="New_BRSFeeTableGrid" 
                       onmouseover="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new2.gif'" 
                       onmouseout="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new.gif'" 
                       title="<%$ Resources:ttNew %>" 
                       OnClientClick="return openAddBasicCode();"
                        /><br />
		        <!--<input type="image" src="../../../../Images/edittoolbar.gif" alt="" id="Edit_BRSFeeTableGrid" onmouseover="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar2.gif'" onmouseout="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar.gif'" title="Edit" onclick="return openEditBasicCode();" /><br />-->
		        <asp:ImageButton runat="server" src="../../../../Images/edittoolbar.gif" alt="" 
                       id="Edit_BRSFeeTableGrid" 
                       onmouseover="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar2.gif'" 
                       onmouseout="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar.gif'" 
                       title="<%$ Resources:ttEdit %>" 
                       OnClientClick="return openEditBasicCode();"
                        /><br />
		        <asp:ImageButton runat="server" src="../../../../Images/delete.gif" alt="" 
                       id="Delete_BRSFeeTableGrid" 
                       onmouseover="javascript:document.all['Delete_BRSFeeTableGrid'].src='../../../../Images/delete2.gif'" 
                       onmouseout="javascript:document.all['Delete_BRSFeeTableGrid'].src='../../../../Images/delete.gif'" 
                       title="<%$ Resources:ttDel %>" 
                       OnClientClick="return validateGridForDeletion('BRSFeeTableGrid');" onclick="Delete_BRSFeeTableGrid_Click" 
                        />
		        
		        
		       </td>
		</tr>
       	 
       </table>
    </div>
    </form>
</body>
</html>
