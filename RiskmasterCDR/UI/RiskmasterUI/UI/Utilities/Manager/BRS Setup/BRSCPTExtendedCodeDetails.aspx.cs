﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSCPTExtendedCodeDetails : NonFDMBasePageCWS
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                hdnTableId.Text = AppHelper.GetQueryStringValue("tableid");

                RowId.Text = AppHelper.GetQueryStringValue("rowid");
                if (IsPostBack != true && RowId.Text != "")
                {
                    string sCWSresponse = string.Empty;

                    CPTCode.Text = RowId.Text;

                    CallCWS("BRSCPTExtendedCodeDetailsAdaptor.Get", null, out sCWSresponse, true, true);

                }
            }
            catch (Exception ee)
            {

                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            string sCWSresponse = string.Empty;
            try
            {
                CallCWSFunction("BRSCPTExtendedCodeDetailsAdaptor.Save", out sCWSresponse);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
