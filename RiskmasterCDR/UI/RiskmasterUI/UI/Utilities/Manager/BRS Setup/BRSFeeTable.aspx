﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSFeeTable.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSFeeTable" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Fee Tables</title>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="javascript" >
        
        function SelectGridRow(rowId, tableName, CalcTypeCode, tblType, BrsModCount) {

            document.forms[0].hdnCalcTypeCode.value = CalcTypeCode;
            //Added:Yukti, MITS:34790
            //document.forms[0].hdnUserTableName.value = tableName;
            document.forms[0].hdnUserTableName.value = encodeURIComponent(tableName);
            document.forms[0].hdnTableId.value = rowId;
            document.forms[0].hdnTableType.value = tblType;

            //document.forms[0].btnModifierValues.value = "Modifier Values" + "(" + BrsModCount + ")";
            document.forms[0].btnModifierValues.value = BRSFeeTableButtons.btnModifierValues + "(" + BrsModCount + ")"; //MITS 34712 hlv
            BRSFeeTable_RadiobtnClicked(CalcTypeCode);
        }

        function BRSFeeTable_RadiobtnClicked(CalcTypeCode) {

            switch (CalcTypeCode) {
                case "12":

                    //document.forms[0].btnBasicTable.value = "Basic Table";
                    document.forms[0].btnBasicTable.value = BRSFeeTableButtons.btnBasicTable; //MITS 34712 hlv
                    document.forms[0].btnBasicTable.disabled = false;
                    document.forms[0].btnModifierValues.disabled = true;
                    document.forms[0].btnPerDiem.disabled = true;

                    break;
                case "14":
                    //document.forms[0].btnBasicTable.value = "Extended Table"; 
                    document.forms[0].btnBasicTable.value = BRSFeeTableButtons.btnExtendTable; //MITS 34712 hlv
                    document.forms[0].btnBasicTable.disabled = false;
                    document.forms[0].btnModifierValues.disabled = false;
                    document.forms[0].btnPerDiem.disabled = true;
                    break;
                case "15":
                case "16":
                case "17":
                    document.forms[0].btnBasicTable.disabled = true;
                    document.forms[0].btnModifierValues.disabled = true;
                    document.forms[0].btnPerDiem.disabled = true;
                    break;
                case "20":
                    //BRS FL Merge  : Umesh
                    document.forms[0].btnBasicTable.disabled = true;
                    document.forms[0].btnModifierValues.disabled = true;
                    //document.forms[0].btnPerDiem.value = "Per Diem";
                    document.forms[0].btnPerDiem.value = BRSFeeTableButtons.btnPerDiem; //MITS 34712 hlv
                    document.forms[0].btnPerDiem.disabled = false;
                    break;
                default:
                    document.forms[0].btnBasicTable.disabled = true;
                    document.forms[0].btnModifierValues.disabled = false;
                    document.forms[0].btnPerDiem.disabled = true;
                    break;
                //does nothing 
            }

            return true;
        }
    
    </script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData"   runat="server">
    <input type="hidden" id="hdnUserTableName" runat="server" />
     <input type="hidden" id="hdnCalcTypeCode" runat="server" />
     <input type="hidden" id="hdnTableId" runat="server" />
     <input type="hidden" id="hdnTableType" />
     <input type="hidden" runat="server" id="hdnMode" />
     <div>
       <table  id="tblGrid" runat="server"  width="780px" cellspacing="0" cellpadding="0" border="0">
         <tr>
				<td colspan="2" class="ctrlgroup">
				  <asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label>
				</td>
		</tr>
		<tr>
		        <td colspan="2"><b></b></td>
		</tr>
		<tr>
		       <td width="95%" colspan="1" class="singleborder" >
		          <div id="divGrid" style="overflow:auto; height:470px"  >
		           <asp:GridView ID="GridView1" runat="server" AllowPaging="false" Width="100%"  AutoGenerateColumns="false"  
                            GridLines="None" onrowdatabound="GridView1_RowDataBound"  >
		              <HeaderStyle CssClass="msgheader" />
		              <AlternatingRowStyle CssClass="data2" />
		              <Columns>
		                  <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                      <input type="radio" id="selectrdo" name="BRSFeeTableGrid"  />
                                    </ItemTemplate> 
                          </asp:TemplateField> 
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrTableName %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserTableName")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrType %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalcType")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrEffDate %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblSysTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDt")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
		              </Columns> 
		           </asp:GridView>
		           </div>
		       </td>
		       <td width="5%" valign="top" colspan="1">
		        <!--
		        <input type="image" src="../../../../Images/new.gif" alt="" id="New_BRSFeeTableGrid" onmouseover="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new2.gif'" onmouseout="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new.gif'" title="New" onclick="return openGridAddEditWindow('BRSFeeTableGrid','add',500,580);" /><br />
		        <input type="image" src="../../../../Images/edittoolbar.gif" alt="" id="Edit_BRSFeeTableGrid" onmouseover="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar2.gif'" onmouseout="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar.gif'" title="Edit" onclick="return openGridAddEditWindow('BRSFeeTableGrid','edit',500,580);" /><br />
                -->
              <asp:ImageButton runat="server" src="../../../../Images/new.gif" alt="" 
                       id="New_BRSFeeTableGrid" 
                       onmouseover="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new2.gif'" 
                       onmouseout="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new.gif'" 
                       title="<%$ Resources:ttNew %>" 
                       OnClientClick="return openGridAddEditWindow('BRSFeeTableGrid','add',500,580);"
                       /><br />

              <asp:ImageButton runat="server" src="../../../../Images/edittoolbar.gif" alt="" 
                       id="Edit_BRSFeeTableGrid" 
                       onmouseover="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar2.gif'" 
                       onmouseout="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar.gif'" 
                       title="<%$ Resources:ttEdit %>" 
                       OnClientClick="return openGridAddEditWindow('BRSFeeTableGrid','edit',500,580);"
                       /><br />

		        <asp:ImageButton runat="server" src="../../../../Images/delete.gif" alt="" 
                       id="Delete_BRSFeeTableGrid" 
                       onmouseover="javascript:document.all['Delete_BRSFeeTableGrid'].src='../../../../Images/delete2.gif'" 
                       onmouseout="javascript:document.all['Delete_BRSFeeTableGrid'].src='../../../../Images/delete.gif'" 
                       title="<%$ Resources:ttDelete %>" 
                       OnClientClick="return validateGridForDeletion('BRSFeeTableGrid');" 
                       onclick="Delete_BRSFeeTableGrid_Click" />
		        
		        
		       </td>
		</tr>
        <tr>
         <td>
             <!--
          <input type="button" class="button" name="btnBasicTable" value="Basic Table"  onClick="BRSFeeTable_BasicTable();" id="btnBasicTable" />
          <input type="button" class="button" name="btnModifierValues" value="Modifier Values" onClick="BRSFeeTable_ModifierValues();" id="btnModifierValues" />
          <input type="button" class="button" name="btnPerDiem" value="Per Diem" onClick="BRSFeeTable_PerDiem();" id="btnPerDiem" />
             -->
             <asp:Button ID="btnBasicTable" Text="<%$ Resources:btnBasicTable %>" class="button" OnClientClick="BRSFeeTable_BasicTable();return false;" runat="server" />
             <asp:Button ID="btnModifierValues" Text="<%$ Resources:btnModifierValues %>" class="button" OnClientClick="BRSFeeTable_ModifierValues();return false;" runat="server" />
             <asp:Button ID="btnPerDiem" Text="<%$ Resources:btnPerDiem %>" class="button" OnClientClick="BRSFeeTable_PerDiem();return false;" runat="server" />
         </td>
        </tr>		 
       </table>
    </div>
    </form>
</body>
</html>
