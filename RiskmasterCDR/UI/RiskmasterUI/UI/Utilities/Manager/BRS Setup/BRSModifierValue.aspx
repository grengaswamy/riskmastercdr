﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSModifierValue.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSModifierValue" %>

<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc2" %>

<%@ Register src="../../../Shared/Controls/CommonTasks.ascx" tagname="CommonTasks" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/Utilities.js" ></script>
    <script type="text/javascript" language="javascript" >
        function BRSModifier_Save() {

            return BRSModifierValue_Save();
        }
    </script>
</head>
<body>
    <form id="frmData" runat="server">
  <uc1:CommonTasks ID="CommonTasks1" runat="server" />

    <div>
       <table class="toolbar" cellSpacing="0" cellPadding="0" border="0">
     <tbody>
      <tr>
       <td align="center" valign="middle" HEIGHT="32">
       <asp:ImageButton runat="server" src="../../../../Images/save.gif" alt="" id="Save" 
               onmouseover="javascript:document.all['Save'].src='../../../../Images/save2.gif'" 
               onmouseout="javascript:document.all['Save'].src='../../../../Images/save.gif'" 
               title="<%$ Resources:ttSave %>" onclick="Save_Click" OnClientClick="return BRSModifier_Save();" /></td>
      </tr>
     </tbody>
    </table>
   </div>
    
    <br><br><div class="msgheader" id="formtitle"><asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label></div>
   <div class="errtextheader">
       <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    </div>
    <div>
   
       <table border="0">
        <tbody>
         <tr>
          <td class="ctrlgroup" colSpan="2"><asp:Label ID="lblTitle2" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label></td>
         </tr>
         <tr><asp:TextBox style="display:none" id="RowId" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='RowId']" ></asp:TextBox></tr>
         <tr>
          <td><asp:Label ID="lblModifierCode" runat="server" Font-Underline="true" Text="<%$ Resources:lblModifierCode %>"></asp:Label>&nbsp;&nbsp;
          </td>
          <td><asp:TextBox runat="server" type="code" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ModifierCode']" size="30" onchange="lookupTextChanged(this);" id="ModifierCode" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" ></asp:TextBox>
          <input type="button" class="CodeLookupControl" id="ModifierCodebtn" onclick="selectCode('MODIFIER_CODES','ModifierCode')" />
          <asp:TextBox runat="server" style="display:none" id="ModifierCode_cid" cancelledvalue="" ></asp:TextBox></td>
         </tr>
         <tr>
          <td><asp:Label ID="lblStartCPT" runat="server" Font-Underline="true" Text="<%$ Resources:lblStartCPT %>"></asp:Label>&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='StartCPT']" size="30" id="StartCPT" onchange="&#xA;;setDataChanged(true);&#xA;" ></asp:TextBox></div>
          </td>
         </tr>
         <tr>
          <td><asp:Label ID="lblEndCPT" runat="server" Font-Underline="true" Text="<%$ Resources:lblEndCPT %>"></asp:Label>&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EndCPT']" size="30" id="EndCPT" onchange="&#xA;;setDataChanged(true);&#xA;" ></asp:TextBox></div>
          </td>
         </tr>
         <tr>
          <td><asp:Label ID="lblModValue" runat="server" Font-Underline="true" Text="<%$ Resources:lblModValue %>"></asp:Label>&nbsp;&nbsp;
          </td>
          <td>
          <asp:TextBox runat="server" type="numeric" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ModValue']" size="30" onblur="numLostFocus(this);" id="ModValue" onchange="setDataChanged(true);" ></asp:TextBox></td>
         </tr>
         <tr>
          <td><asp:Label ID="lblMaxRLV" runat="server" Text="<%$ Resources:lblMaxRLV %>"></asp:Label>&nbsp;&nbsp;</td>
          <td>
          <asp:TextBox runat="server" type="numeric" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='MaxRLV']" size="30" onblur="numLostFocus(this);" id="MaxRLV" onchange="setDataChanged(true);" ></asp:TextBox></td>
         </tr>
         <tr>
          <td>
          <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='TableId']" value="1" style="display:none" id="hdnTableId" ></asp:TextBox></td>
         </tr>
         <tr>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UserTableName']" style="display:none" id="hdnUserTableName" ></asp:TextBox></td>
         </tr>
         <tr>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FormMode']" style="display:none" id="FormMode" ></asp:TextBox></td>
         </tr>
        </tbody>
       </table>
       
    </div>
    <input type="text" name="" value="" id="SysFormName" style="display:none">
    </form>
</body>
</html>
