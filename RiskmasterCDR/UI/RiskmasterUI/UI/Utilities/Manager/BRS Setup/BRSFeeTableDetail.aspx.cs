﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.RMXResourceManager; //MITS 34721 - hlv

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSFeeTableDetail : NonFDMBasePageCWS
    {
        private string pageID = RMXResourceProvider.PageId("BRSFeeTableDetail.aspx");

        protected void Page_Load(object sender, EventArgs e)
        {
            //MITS 34721 - hlv - begin
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                AppHelper.TimeClientScript(sCulture, this);
            }

            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, pageID, "BRSFeeTableDetailValidations",
                ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "BRSFeeTableDetailValidations", sValidationResources, true);

            string sLabelResources = JavaScriptResourceHandler.ProcessRequest(this.Context, pageID, "BRSFeeTableDetailLabels",
                ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "BRSFeeTableDetailLabels", sLabelResources, true);
            //MITS 34721 - hlv end

            if (!IsPostBack)
            {
                bool bReturnStatus = false;
                
                if (AppHelper.GetQueryStringValue("tabletype") == "1")
                {
                    TableType1.Checked = true;
                }
                else
                {
                    TableType2.Checked = true;
                }
                hdnTableType.Text = "1";//AppHelper.GetQueryStringValue("tabletype");
                hdntitle.Value = AppHelper.GetQueryStringValue("usertablename");
                if (hdntitle.Value == "")
                {
                    //hdntitle.Value = "New";
                    hdntitle.Value = AppHelper.GetResourceValue(pageID, "lblGroup", "0"); //MITS 34721 hlv
                }
                RowId.Text = AppHelper.GetQueryStringValue("selectedid");
                TableId.Text = RowId.Text;
                bReturnStatus = CallCWSFunction("BRSFeeTableDetailAdaptor.Get");
                if (EndDt.Text == "00000000" || EndDt.Text == "00/00/0000")
                {
                    EndDt.Text = "";
                }
                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>BRSFeeTableDetail_Load();</script>");
            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            XmlDocument xmlDoc = new XmlDocument();

            using (XmlReader reader = Xelement.CreateReader()) 
            {
                xmlDoc.Load(reader); 
            }

           XmlNode objXmlNode =  xmlDoc.SelectSingleNode("Message/Document/form/group");
           XmlElement xmlEle = (XmlElement)objXmlNode;
            
           xmlEle.SetAttribute("name","BRSFeeTableDetail");
           xmlEle.SetAttribute("title", hdntitle.Value);

           objXmlNode = xmlDoc.SelectSingleNode("Message/Document/form/group/displaycolumn/control[@name ='state']");
           xmlEle = (XmlElement)objXmlNode;
           xmlEle.SetAttribute("codetable", "STATES");
           //xmlEle.SetAttribute("codeid", state_cid.Text);
           Xelement = XElement.Parse(xmlDoc.InnerXml);
        }

        private string FormatDate(string sDate)
        {
            if (sDate != "" && sDate.IndexOf(AppHelper.GetUIDateSeparator()) == -1)
            {
                sDate = sDate.Replace(AppHelper.GetUIDateSeparator(), "");
                /*
                string sYear = sDate.Substring(0, 4);
                string sMonth = sDate.Substring(4, 2);
                string sDay = sDate.Substring(6, 2);

                return sMonth + "/" + sDay + "/" + sYear;
                */

                //MITS 34721 hlv begin
                int iYear = Convert.ToInt32(sDate.Substring(0, 4));
                int iMonth = Convert.ToInt32(sDate.Substring(4, 2));
                int iDay = Convert.ToInt32(sDate.Substring(6, 2));

                return new DateTime(iYear, iMonth, iDay).ToString(AppHelper.GetDateFormat());
                //MITS 34721 hlv end
            }
            else
            {
                return sDate;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (hdnTableType.Text == "1")
            {
                tdTableType.Visible = false;
                tdTableType2.Visible = false;
            }

            FeeSchedule.Items.Insert(0, "");

            StartDt.Text = FormatDate(StartDt.Text);
            EndDt.Text = FormatDate(EndDt.Text);
        }

        protected void DbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            FeeSchedule.Items.Clear();
            FileName.Text = "";
            bReturnStatus = CallCWSFunction("BRSFeeTableDetailAdaptor.Get");
            ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>BRSFeeTableDetail_Load();</script>");
        }

        protected void FeeSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            FileName.Text = "";
            bReturnStatus = CallCWSFunction("BRSFeeTableDetailAdaptor.Get");
            ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>BRSFeeTableDetail_Load();</script>");
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {            
            bool bReturnStatus = false;
            string sCWSresponse;
            TableId.Text = RowId.Text;
            bReturnStatus = CallCWSFunction("BRSFeeTableDetailAdaptor.Save", out sCWSresponse);
            if (bReturnStatus)
            {
              ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            }
        }
    }
}
