﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager; //MITS 34606 hlv

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSCPTCodesListing : System.Web.UI.Page
    {
        private string PageID = RMXResourceProvider.PageId("BRSCPTCodesListing.aspx");

        protected void Page_Load(object sender, EventArgs e)
        {
            hdnUserTableName.Value = AppHelper.GetQueryStringValue("usertablename");
            hdnCalcType.Value = AppHelper.GetQueryStringValue("calctype");
            hdnTableId.Value = AppHelper.GetQueryStringValue("tableid");
            if (hdnMode.Value != "Delete")
            {
                LoadCPRList();
            }
        }


        private XmlNode InputXMLDoc(string sFunctionToCall)
        {

            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
                <Message>
                <Authorization></Authorization> 
                <Call>
                <Function>" + sFunctionToCall + @"</Function> 
                </Call>
                <Document>
                    <PassToWebService>
                        <UserTableName>" + hdnUserTableName.Value + @"</UserTableName> 
                        <tableid>" + hdnTableId.Value + @"</tableid> 
                        <CalcTypeCode>" + hdnCalcType.Value + @"</CalcTypeCode>
                        <RecordCount /> 
                        <BRSCPTCodesListingList>
                        <listhead>
                        <CPT>CPT</CPT> 
                        <Desc>Description</Desc> 
                        <Amount>Amount</Amount> 
                        <RowId>Row Id</RowId> 
                        </listhead>
                        </BRSCPTCodesListingList>
                        </PassToWebService>
                </Document>
                </Message>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;
        }

        private void LoadCPRList()
        {
            XmlNode inputDocNode = null;
            string strCWSSrvcOutput = string.Empty;
            XmlDocument xmlOutDoc = new XmlDocument();
            DataTable dtGridData = new DataTable();
            XmlNode xmlNode = null;
            inputDocNode = InputXMLDoc("BRSCPTCodesListingAdaptor.Get");
            strCWSSrvcOutput = AppHelper.CallCWSService(inputDocNode.InnerXml);

            xmlOutDoc.LoadXml(strCWSSrvcOutput);

            GridHeaderAndData(xmlOutDoc.SelectSingleNode("ResultMessage/Document/PassToWebService/BRSCPTCodesListingList/listhead"), xmlOutDoc.SelectNodes("ResultMessage/Document/PassToWebService/BRSCPTCodesListingList/listrow"), dtGridData);
            xmlNode = xmlOutDoc.SelectSingleNode("ResultMessage/Document/PassToWebService/RecordCount");
            //lblUserTableName.Text = "BasicFl: No of Items :-" + xmlNode.InnerXml;
            lblUserTableName.Text = AppHelper.GetResourceValue(this.PageID, "lblUTableName", "0") + xmlNode.InnerXml; //MITS 34606 hlv
            //Added if condition for MITS:16451
            if (xmlNode.InnerText == "0")
            {
                GridView1.Columns[0].Visible = false;
            }
            else
            {
                GridView1.Columns[0].Visible = true;
            }
            //Added if condition for MITS:16451
            GridView1.DataSource = dtGridData;
            GridView1.DataBind();
        }


        private void GridHeaderAndData(XmlNode objGridHeaders, XmlNodeList objGridData, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;


            if (objGridHeaders != null && objGridHeaders.InnerXml != "")
            {
                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    //Add Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();
                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.Name;

                    dtGridData.Columns.Add(dcGridColumn);

                }
            }
            //Add rows to be associated with Datasource of grid
            if (objGridData != null && objGridData.Count != 0)
            {
                foreach (XmlNode objNodes in objGridData)
                {
                    dr = dtGridData.NewRow();

                    iColumnIndex = 0;
                    foreach (XmlElement objElem in objNodes)
                    {
                        dr[iColumnIndex++] = objElem.InnerXml;
                    }
                    dtGridData.Rows.Add(dr);

                }
            }
            else
            {
                dr = dtGridData.NewRow();
                dtGridData.Rows.Add(dr);
            }



        }


        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectGridRow('" + DataBinder.Eval(e.Row.DataItem, "CPT").ToString() + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }

        protected void Delete_BRSFeeTableGrid_Click(object sender, ImageClickEventArgs e)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
                                <Message>
                                <Authorization></Authorization> 
                                <Call>
                                <Function>BRSCPTCodesListingAdaptor.Delete</Function> 
                                </Call>
                                <Document>
                                <form>
                                <group name='BRSCPTCodesListing'>
                                <displaycolumn>
                                <control name='CPTCode'>" + hdnSelectedId.Value + @"</control> 
                                <control name='TableId'>" + hdnTableId.Value +@"</control> 
                                </displaycolumn>
                                </group>
                                </form>
                                </Document>
                                </Message>");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }
            hdnMode.Value = "";
            string strCWSSrvcOutput = string.Empty;
            XmlDocument xmlOutDoc = new XmlDocument();


            strCWSSrvcOutput = AppHelper.CallCWSService(xmlNodeDoc.InnerXml);

            LoadCPRList();
        }

    }
}
