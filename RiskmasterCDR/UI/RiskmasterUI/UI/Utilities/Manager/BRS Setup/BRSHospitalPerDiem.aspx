﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="BRSHospitalPerDiem.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSHospitalPerDiem" %>

<%@ Register src="../../../Shared/Controls/CommonTasks.ascx" tagname="CommonTasks" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Hospital Per Diem</title>
     <script type="text/javascript" language="JavaScript" src="../../../../Scripts/Utilities.js"></script>
   
</head>
<body>
    <form id="frmData" runat="server">
    <uc1:CommonTasks ID="CommonTasks1" runat="server" /> 
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />  
    <input type="hidden" name="hTabName"><div class="msgheader" id="formtitle">
        <asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label>
    </div>
   
 
   
    <br />
    <table border="0">
    <tr>
     
      <td><asp:TextBox runat="server" style="display:none" rmxref="/Instance/Document/form/control[@name ='UserTableName']" id="UserTableName"></asp:TextBox></td>
      <td><asp:TextBox runat="server" style="display:none" rmxref="/Instance/Document/form/control[@name ='TableId']" id="TableId"></asp:TextBox></td>
      <td><asp:TextBox runat="server" style="display:none" rmxref="/Instance/Document/form/control[@name ='CalcTypeCode']" id="CalcTypeCode"></asp:TextBox></td>
      <td></td>
      <td><asp:TextBox runat="server" style="display:none" rmxref="/Instance/Document/form/control[@name ='FormMode']" id="FormMode"></asp:TextBox></td>
      <td><asp:TextBox runat="server" style="display:none"  id="hdnaction"></asp:TextBox></td>
      <td><asp:TextBox runat="server" style="display:none" rmxref="/Instance/Document/form/control[@name ='FunctionToCall']" id="FunctionToCall"></asp:TextBox></td>
      <table border="0" cellspacing="0" cellpadding="0">
       <tr>
        <td class="Selected" nowrap="true" name="TABSInpatientCharges" id="TABSInpatientCharges">
        <a class="Selected" HREF="#" onClick="tabChange(this.name);" name="InpatientCharges" id="LINKTABSInpatientCharges"><span style="text-decoration:none"><asp:Label ID="lblInpChg" runat="server" Text="<%$ Resources:lblInpChg %>"></asp:Label></span></a></td>
        <td nowrap="true" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
        <td class="NotSelected" nowrap="true" name="TABSOutpatientCharges" id="TABSOutpatientCharges"><a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="OutpatientCharges" id="LINKTABSOutpatientCharges"><span style="text-decoration:none"><asp:Label ID="lblOutChg" runat="server" Text="<%$ Resources:lblOutChg %>"></asp:Label></span></a></td>
        <td nowrap="true" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
        <td class="NotSelected" nowrap="true" name="TABSAmbSugCnt" id="TABSAmbSugCnt"><a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="AmbSugCnt" id="LINKTABSAmbSugCnt"><span style="text-decoration:none"><asp:Label ID="lblAmbSurCenter" runat="server" Text="<%$ Resources:lblAmbSurCenter %>"></asp:Label></span></a></td>
        <td nowrap="true" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
        <td valign="top" nowrap="true"></td>
       </tr>
      </table>
      <div class="singletopborder" style="position:relative;left:0;top:0;width:800px;height:315px;overflow:auto">
       <table border="0" cellspacing="0" cellpadding="0">
        <tr>
         <td>
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABInpatientCharges" id="FORMTABInpatientCharges">
           <tr id="">
            <td><asp:Label ID="lblAcuteCare" runat="server" Text="<%$ Resources:lblAcuteCare %>"></asp:Label>&nbsp;&nbsp;</td>
            <td></td>
            <td>&nbsp;&nbsp;</td>
            <td><asp:Label ID="lblTraumaCenter" runat="server" Text="<%$ Resources:lblTraumaCenter %>"></asp:Label>&nbsp;&nbsp;</td>
            <td></td>
           </tr>
           <tr id="Tr1">
            <td><asp:Label ID="lblSurStay" runat="server" Text="<%$ Resources:lblSurStay %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ACPSurgicalStay']" size="30" tabindex="71" id="ACPSurgicalStay" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td><asp:Label ID="lblTSurStay" runat="server" Text="<%$ Resources:lblSurStay %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='TCSurgicalStay']" size="30" tabindex="71" id="TCSurgicalStay" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
           </tr>
           <tr id="Tr2">
            <td><asp:Label ID="lblNonSurStay" runat="server" Text="<%$ Resources:lblNonSurStay %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ACPNonSurgicalStay']" size="30" tabindex="71" id="ACPNonSurgicalStay" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td><asp:Label ID="lblTNonSurStay" runat="server" Text="<%$ Resources:lblNonSurStay %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='TCNonSurgicalStay']" size="30" tabindex="71" id="TCNonSurgicalStay" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
           </tr>
           <tr id="Tr3">
            <td></td>
            <td></td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
            <td></td>
           </tr>
           <tr id="Tr4">
            <td><asp:Label ID="lblStopLossAmt" runat="server" Text="<%$ Resources:lblStopLossAmt %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='StopLossAmt']" size="30" tabindex="71" id="StopLossAmt" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td><asp:Label ID="lblStopLossPer" runat="server" Text="<%$ Resources:lblStopLossPer %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='StopLossPer']" size="30" tabindex="71" id="StopLossPer" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
           </tr>
           <tr id="Tr5">
            <td><asp:Label ID="lblRehabPsy" runat="server" Text="<%$ Resources:lblRehabPsy %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Rehab']" size="30" tabindex="71" id="Rehab" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
            <td></td>
           </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABOutpatientCharges" id="FORMTABOutpatientCharges" style="display:none;">
           <tr id="Tr6">
            <td><asp:Label ID="lblHospCare" runat="server" Text="<%$ Resources:lblHospCare %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='HospCarePer']" size="30" tabindex="71" id="HospCarePer" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td><asp:Label ID="lblScheSurg" runat="server" Text="<%$ Resources:lblScheSurg %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SchedSurgPer']" size="30" tabindex="71" id="SchedSurgPer" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
           </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABAmbSugCnt" id="FORMTABAmbSugCnt" style="display:none;">
           <tr id="Tr7">
            <td><asp:Label ID="lblBillFee" runat="server" Text="<%$ Resources:lblBillFee %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='BillFeeSch']" size="30" tabindex="71" id="BillFeeSch" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
            <td></td>
           </tr>
           <tr id="Tr8">
            <td><asp:Label ID="lblNotInFee" runat="server" Text="<%$ Resources:lblNotInFee %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='NotInFeeSch']" size="30" tabindex="71" id="NotInFeeSch" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td><asp:Label ID="lblFeeSche" runat="server" Text="<%$ Resources:lblFeeSche %>"></asp:Label>&nbsp;&nbsp;</td>
            <td><asp:DropDownList runat="server" type="combobox" itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='FeeSche']" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FeeSche']/@value" id="FeeSche" tabindex="72" onchange="setDataChanged(true);">
               
              </asp:DropDownList></td>
           </tr>
           <tr id="Tr9">
            <td><asp:Label ID="lblBillFeeSec" runat="server" Text="<%$ Resources:lblBillFeeSec %>"></asp:Label>&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
             <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='BilledlessFeeSch']" size="30" tabindex="71" id="BilledlessFeeSch" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="50"></asp:TextBox></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
            <td></td>
           </tr>
          </table>
         </td>
         <td valign="top"></td>
        </tr>
       </table>
      </div>
      <table>
       <tr>
        <td><script language="JavaScript" SRC="">{var i;}</script>
        <asp:Button class="button" name="btnFinancials"  Text="<%$ Resources:btnOK %>" 
                runat="server" id="btnFinancials" 
                onclick="btnFinancials_Click" />
        <script language="JavaScript" SRC="">{var i;}</script>
        <!--<input type="button" class="button" name="btnFinancials" value="Cancel" onClick="return BRSHospPerDiem_Cancel();" id="btnFinancials"></td>-->
        <asp:Button class="button" name="btnFinancials"  Text="<%$ Resources:btnCancel %>" 
                runat="server" id="btnFinancialsC" OnClientClick="return BRSHospPerDiem_Cancel();" />
       </tr>
      </table>
    
      
      </td>
    </tr>
   </table>
    
    
    </form>
</body>
</html>
