﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSFactor.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSFactor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/form.js"></script>
    <script type="text/javascript" language="javascript">

        function Validate() {
            if (document.forms[0].ClinicalCat.value == "" || document.forms[0].StartCpt.value == "" || document.forms[0].EndCpt.value == "" || document.forms[0].ConvFact.value == "") {
                alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
                return false;
            }
            else {
                return true;
            }
        }
    
    </script>
</head>
<body>
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
     <table class="toolbar" cellSpacing="0" cellPadding="0" border="0">
     <tbody>
      <tr>
       <td align="center" valign="middle" HEIGHT="32">
        <asp:ImageButton src="../../../../Images/save.gif" id="Save" 
               onmouseover="javascript:document.all['Save'].src='../../../../Images/save2.gif'" 
               onmouseout="javascript:document.all['Save'].src='../../../../Images/save.gif'" 
                 ToolTip="Save"  runat="server" OnClientClick="return Validate();" onclick="Save_Click" /></td>
      </tr>
     </tbody>
    </table>
   </div><br><br><div class="msgheader" id="formtitle">Factor</div>
   <div>
   <table border="0">
    <tbody>
     <tr>
      <td>
       <table border="0">
        <tbody>
         <tr>
          <td class="ctrlgroup" colSpan="2">Add</td>
         </tr>
         <tr><asp:TextBox  style="display:none" type="id" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='RowId']" id="RowId"></asp:TextBox></tr>
         <tr>
          <td><u>-Clinical Category</u>:&nbsp;&nbsp;
          </td>
          <td>
          <asp:TextBox size="30" runat="server" type="code"  onchange="lookupTextChanged(this);" id="ClinicalCat" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ClinicalCat']" cancelledvalue="" onblur="codeLostFocus(this.id)"></asp:TextBox>
          <input type="button" class="CodeLookupControl" id="ClinicalCatbtn" onclick="selectCode('CLINICAL_CATEGORY','ClinicalCat')">
          <asp:TextBox style="display:none" id="ClinicalCat_cid" runat="server" cancelledvalue=""></asp:TextBox></td>
         </tr>
         <tr>
          <td><u>-Start CPT Code</u>:&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <asp:TextBox size="30" id="StartCpt" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='StartCpt']"  onchange="setDataChanged(true);"></asp:TextBox></div>
          </td>
         </tr>
         <tr>
          <td><u>-End CPT Code</u>:&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <asp:TextBox size="30" id="EndCpt" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EndCpt']" onchange="setDataChanged(true);" runat="server" ></asp:TextBox></div>
          </td>
         </tr>
         <tr>
          <td><u>-Conversion Factor</u>:&nbsp;&nbsp;
          </td>
          <td><asp:TextBox size="30" onblur="numLostFocus(this);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ConvFact']" id="ConvFact" runat="server"  onchange="setDataChanged(true);"></asp:TextBox></td>
         </tr>
         <tr>
          <td><asp:TextBox value="30" style="display:none" type="hidden" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='TableId']" runat="server" id="TableId"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox style="display:none" id="FormMode"  runat="server"></asp:TextBox></td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td></td>
         </tr>
        </tbody>
       </table>
       <input  name="sys_formidname" type="hidden" value="RowId"/>
		<input  name="sys_formpidname" type="hidden" value="RowId"/>
		<input  name="sys_formpform" type="hidden" value="BRSFactor"/>
		<input type="text" name="SysFormName" id="SysFormName" style="display:none" value="Factor" />
    </div>
    </form>
</body>
</html>
