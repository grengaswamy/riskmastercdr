﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.UI.Utilities.Manager.RateAndUnits
{
    public partial class rate_tables : NonFDMBasePageCWS 
    {
        string sreturnValue = String.Empty;
        XElement XmlTemplate = null;
        bool bReturnStatus;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {                    
                    BindingToGrid();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml.Append("<Call><Function>");
            sXml.Append("TimeExpenseAdaptor.GetXmlData");
            sXml.Append("</Function></Call><Document><Document><FunctionToCall/><TimeExpense><FrmName>");
            sXml.Append("ratetables</FrmName><InputXml><TandE/></InputXml><TableId1/><TableId2/><TableId3/>");
            sXml.Append("</TimeExpense></Document></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private void BindingToGrid()
        {
            DataTable objTable = new DataTable();
            XmlDocument XmlDoc = new XmlDocument();
            DataRow objRow;
            XmlNode xNode = null;

            XmlTemplate = GetMessageTemplate();
            CallCWS("TimeExpenseAdaptor.GetXmlData", XmlTemplate, out sreturnValue, false, false);
            bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);
            XmlDoc.LoadXml(sreturnValue);
            XmlNodeList FormList = XmlDoc.SelectNodes("//rateTableData");
            objTable.Columns.Add("Client");
            objTable.Columns.Add("RateTable");
            objTable.Columns.Add("EffDate");
            objTable.Columns.Add("ExDate");   
            objTable.Columns.Add("Id");

            if (FormList.Count == 0)
            {
                objRow = objTable.NewRow();
                objRow["Client"] = String.Empty;
                objRow["RateTable"] = String.Empty;
                objRow["EffDate"] = String.Empty;
                objRow["ExDate"] = String.Empty;
                objRow["Id"] = String.Empty;
                objTable.Rows.Add(objRow);
                GridDisplayTables.Columns[4].Visible = false;
            }
            else
            {
                for (int i = 0; i < FormList.Count; i++)
                {
                    xNode = FormList[i];
                    objRow = objTable.NewRow();
                    objRow["Client"] = xNode.Attributes["custname"].Value;
                    objRow["RateTable"] = xNode.Attributes["name"].Value;
                    objRow["EffDate"] = xNode.Attributes["efdate"].Value;
                    objRow["ExDate"] = xNode.Attributes["exdate"].Value;

                    //START - corrects date format to have same appearance as R4 - mpalinski
                    if (!String.IsNullOrEmpty(objRow["EffDate"].ToString()))
                    {
                        DateTime effdate = Conversion.ToDate(objRow["EffDate"].ToString());
                        objRow["EffDate"] = String.Format("{0:MM/dd/yyyy}", effdate);
                    } // if

                    if (!String.IsNullOrEmpty(objRow["ExDate"].ToString()))
                    {
                        DateTime exdate = Conversion.ToDate(objRow["ExDate"].ToString());
                        objRow["ExDate"] = String.Format("{0:MM/dd/yyyy}", exdate);
                    } // if
                    //END - corrects date format to have same appearance as R4 - mpalinski

                    objRow["Id"] = xNode.Attributes["id"].Value;
                    objTable.Rows.Add(objRow);
                }
            }

            objTable.AcceptChanges();
            GridDisplayTables.Visible = true;
            GridDisplayTables.DataSource = objTable;
            GridDisplayTables.DataBind();
        }

        private void DeleteTable(object sender, ImageClickEventArgs e)
        {
            XmlTemplate = GetMessageTemplateForDelete();           
            CallCWS("TimeExpenseAdaptor.DelRateTable", XmlTemplate, out sreturnValue, false, false);
            bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);
            BindingToGrid();
        }

        private XElement GetMessageTemplateForDelete()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml.Append("<Call><Function>");
            sXml.Append("TimeExpenseAdaptor.DelRateTable");
            sXml.Append("</Function></Call><Document><Document><FunctionToCall/><TimeExpense><TableId>");
            sXml.Append(ratetableid.Text);
            sXml.Append("</TableId></TimeExpense></Document></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void GridDisplayTables_RowDataBound(object Sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("RateTable");
                if (lnkbtn != null)
                {
                    lnkbtn.PostBackUrl = "~/UI/Utilities/Manager/RateAndUnits/add-rate-table.aspx?tableId=" + lnkbtn.Attributes["value"].ToString();
                }
            }
        }
        
        protected void Delete_ItemCommand(object sender, CommandEventArgs e) //button to delete table
        {
            ratetableid.Text =  e.CommandArgument.ToString();
            XmlTemplate = GetMessageTemplateForDelete();
            bReturnStatus = CallCWSFunction("TimeExpenseAdaptor.DelRateTable", XmlTemplate);
            BindingToGrid();
        }
    }
}
