﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="rate-tables.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.RateAndUnits.rate_tables" %>

<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Time and Expense Rate Tables</title>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/TandE.js"></script>  
    <script type="text/javascript">
        function PageLoaded() {
            document.forms[0].method = "post";
            return false;
        }
    </script>

</head>
<body onload="PageLoaded();parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
     <div id="maindiv" style="height:100%;width:99%;overflow:auto">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr class="msgheader">
            <td colspan="10">
                Miscellaneous Administration
            </td>
        </tr>
        <tr>
            <td class="ctrlgroup2" colspan="10">
                Rate Tables
            </td>
        </tr>
    </table>
    <div>
    </div>   
    <asp:GridView ID="GridDisplayTables" runat="server" AutoGenerateColumns="false" CssClass="singleborder" OnRowDataBound="GridDisplayTables_RowDataBound" GridLines="Vertical" Width="100%" >
        <HeaderStyle CssClass="colheader3" HorizontalAlign="Left" />
        <RowStyle CssClass="datatd" HorizontalAlign="Left" Font-Bold="false" />
        <AlternatingRowStyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />
        <Columns>       
            <asp:TemplateField HeaderText="Client" >
                <ItemTemplate>
                    <asp:Label runat="server" ID="Client" Text='<%# Eval("Client")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Rate Table">
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="RateTable" PostBackUrl="" Text='<%# Eval("RateTable")%>' value='<%# Eval("Id")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Eff.Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                        <asp:Label runat="server" ID="EffDate" Text='<%# Eval("EffDate")%>' />
                    </itemtemplate>
                </asp:TemplateField>
            <asp:TemplateField HeaderText="Ex.Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                     <itemtemplate>
                        <asp:Label runat="server" ID="ExDate" Text='<%# Eval("ExDate")%>' />
                    </itemtemplate>
                 </asp:TemplateField>
            <asp:TemplateField HeaderText="" >
                <itemtemplate>
                    <asp:ImageButton ID="Delete" runat="server" Text="Delete" ImageUrl="../../../../Images/tb_delete_active.png"
                        ImageAlign="Middle" CommandArgument='<%# Eval("Id")%>' 
                            CommandName="delete" OnCommand="Delete_ItemCommand"/>
                </itemtemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <br />
    <table width="100%">
        <tr>
            <td align="center">
                <asp:Button class="button" ID="btnAdd" Text=" Add New " runat="server" PostBackUrl="~/UI/Utilities/Manager/RateAndUnits/add-rate-table.aspx?tableId=0" />
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="actionid" rmxref="/Instance/ui/action" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="ratetableid" rmxtype="hidden" />
        </div> 
    </form>
</body>
</html>
