﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-rate-details.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.Manager.RateAndUnits.add_rate_details" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Detail Items</title>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/TandE.js"></script>    
    <script type="text/javascript" language="javascript" src="../../../../Scripts/form.js"></script>
</head>
<body class="Margin0">
    <form id="frmData" runat="server">
    <div style="height:75px">
    </div>
        <table class="singleborder" align ="center" >
            <tr>
                <td class="headertext1" align="center" colspan="4">
                    Rate Detail
                </td>
            </tr>
            <tr>
                <td>
                    <b><u>Trans Type :</u></b>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="transactiontypecode" ControlName="transactiontypecode" CodeTable="TRANS_TYPES" RMXRef="/Instance/Document/data/option/@transtype"
                        RMXType="code" TabIndex="1" Required="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <b><u>Unit :</u></b>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="unit" ControlName="unit" CodeTable="UNIT" RMXRef="/Instance/Document/data/option/@unit"
                        RMXType="code" TabIndex="2" Required="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <b><u>Rate :</u></b>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="rate" RMXRef="/Instance/Document/data/option/@rate"
                        TabIndex="3" Required="true" onchange="setDataChanged(true);"/>
                </td>
            </tr>
            <tr>
			    <td colspan="2">
				    <asp:button ID="btnOk" runat="server" Text="Save & Close" CssClass="button" 
                        onClientClick="return saveRatetabledetail();"  tabindex="4" />
					&#160;
					<asp:button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" 
                        onClientClick="window.close();" tabindex="5" />
				</td>
			</tr>
        </table>
        <asp:TextBox Style="display: none" runat="server" ID="trackid" rmxtype="hidden" />
         <asp:TextBox Style="display: none" runat="server" ID="mode" rmxtype="hidden" />
          <asp:TextBox Style="display: none" runat="server" ID="rowid" rmxtype="hidden" />
           <asp:TextBox Style="display: none" runat="server" ID="user" rmxtype="hidden" />
            <asp:TextBox Style="display: none" runat="server" ID="datetime" rmxtype="hidden" />
    </form>
</body>
</html>
