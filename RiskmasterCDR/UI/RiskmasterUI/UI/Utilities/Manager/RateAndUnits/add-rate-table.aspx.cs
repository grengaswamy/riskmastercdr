﻿using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.Manager.RateAndUnits
{
    public partial class add_rate_table : NonFDMBasePageCWS 
    {
        bool bReturnStatus;
        string sreturnValue = String.Empty;
        XElement XmlTemplate = null;
        DataTable objTable = new DataTable();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes

                if (!IsPostBack)
                {
                    ratetableid.Text = AppHelper.GetQueryStringValue("tableId");
                    XmlTemplate = GetMessageTemplate();
                    //required automatic binding of some controls(except grid)
                    CallCWS("TimeExpenseAdaptor.GetXmlData", XmlTemplate, out sreturnValue, false, true);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);
                    BindingToGrid(sreturnValue);
                }
                else
                {
                    if (mode.Text == "new")
                    {
                        AddRowToGrid();
                    }
                    else if (mode.Text == "edit")
                    {
                        EditRowOfGrid();
                    }
                    else if (mode.Text == "save")
                    {
                        SaveGrid();
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {         
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml.Append("<Call><Function>");
            sXml.Append("TimeExpenseAdaptor.GetXmlData");
            sXml.Append("</Function></Call><Document><Document><FunctionToCall/><TimeExpense><FrmName>");
            sXml.Append("ratetable</FrmName><InputXml><form><group><control name='rtname' type='text'/><control name='customer' type='textandlist'/>");
            sXml.Append("<control name='CustomerFieldData'/><control name='efdate' type='date'/><control name='exdate' type='date'/><control name='tableid'/>");
            sXml.Append("<control name='ratetable' type='ratetable'/><control name='mode'/>");
            sXml.Append("</group></form></InputXml><TableId1>");
            sXml.Append(ratetableid.Text);
            sXml.Append("</TableId1><TableId2/><TableId3/>");
            sXml.Append("</TimeExpense></Document></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private void BindingToGrid(string outXml)
        {
            XmlDocument XmlDoc = new XmlDocument();
            DataRow objRow;
            XmlNode xNode = null;

            XmlDoc.LoadXml(outXml);

            XmlNodeList TransactionList = XmlDoc.SelectNodes("//option");
            objTable.Columns.Add("rowid");
            objTable.Columns.Add("trackid");
            objTable.Columns.Add("transtypecode");
            objTable.Columns.Add("transtype");
            objTable.Columns.Add("unit");
            objTable.Columns.Add("rate");
            objTable.Columns.Add("status");
            objTable.Columns.Add("user");
            objTable.Columns.Add("datetime");

            if (TransactionList.Count == 0)
            {//In order to show header if there is no row
                objRow = objTable.NewRow();
                objRow["rowid"] = "-1";
                objRow["trackid"] = String.Empty;
                objRow["transtypecode"] = String.Empty;
                objRow["transtype"] = String.Empty;
                objRow["unit"] = String.Empty;
                objRow["rate"] = String.Empty;
                objRow["status"] = String.Empty;
                objRow["user"] = String.Empty;
                objRow["datetime"] = String.Empty;
                objTable.Rows.Add(objRow);
                GridDisplayTable.Columns[8].HeaderStyle.CssClass = "hiderowcol";
                GridDisplayTable.Columns[8].ItemStyle.CssClass = "hiderowcol";
            }
            else
            {
                for (int i = 0; i < TransactionList.Count; i++)
                {
                    xNode = TransactionList[i];
                    objRow = objTable.NewRow();
                    objRow["rowid"] = i;
                    objRow["trackid"] = xNode.Attributes["trackid"].Value;
                    objRow["transtypecode"] = xNode.Attributes["transtypecode"].Value;                
                    objRow["transtype"] = xNode.Attributes["transtype"].Value;                
                    objRow["unit"] = xNode.Attributes["unit"].Value;
                    objRow["rate"] = xNode.Attributes["rate"].Value;
                    objRow["status"] = "U";
                    objRow["user"] = xNode.Attributes["user"].Value;
                    objRow["datetime"] = xNode.Attributes["datetime"].Value;
                    objTable.Rows.Add(objRow);
                }
            }

            objTable.AcceptChanges();

            if (TransactionList.Count != 0)
            {
                ViewState["dtRateDetail"] = objTable;   //do not need to maintain viewstate in case there is no row
            }

            GridDisplayTable.Visible = true;
            GridDisplayTable.DataSource = objTable;
            GridDisplayTable.DataBind();
        }

        protected void GridDisplayTable_RowDataBound(object Sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //We dont need to set the onclientclick attribute when there is no detail row
                if (ViewState["dtRateDetail"] != null)
                {
                    HiddenField hdnRowId = (HiddenField)e.Row.FindControl("RowId");
                    HiddenField hdnTrackId = (HiddenField)e.Row.FindControl("TrackId");
                    LinkButton lnkbtnTransType = (LinkButton)e.Row.FindControl("TransactionType");
                    Label lblRate = (Label)e.Row.FindControl("Rate");
                    Label lblUnit = (Label)e.Row.FindControl("Unit");
                    HiddenField hdnUser = (HiddenField)e.Row.FindControl("User");
                    HiddenField hdnDateTime = (HiddenField)e.Row.FindControl("DateTime");

                    if (lnkbtnTransType != null)
                    {
                        lnkbtnTransType.OnClientClick = "return fnOpenDetailPage(" + hdnRowId.Value.ToString() + "," + hdnTrackId.Value.ToString() + "," + lnkbtnTransType.Attributes["value"].ToString() + ",'" + lnkbtnTransType.Text + "'," + lblRate.Text + ",'" + lblUnit.Text + "','edit','" + hdnUser.Value.ToString() + "','" + hdnDateTime.Value.ToString() + "')";
                    }
                }
            }
        }

        /// <summary>
        /// button to delete table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_ItemCommand(object sender, CommandEventArgs e)
        {
            SelectedRowId.Text = e.CommandArgument.ToString();
            DeleteRowFromGrid();
            //MGaba2:MITS 16561
            if (objTable.Rows.Count == 0)
            {
                DataRow objRow = objTable.NewRow();
                objRow["rowid"] = "-1";
                objRow["trackid"] = String.Empty;
                objRow["transtypecode"] = String.Empty;
                objRow["transtype"] = String.Empty;
                objRow["unit"] = String.Empty;
                objRow["rate"] = String.Empty;
                objRow["status"] = String.Empty;
                objRow["user"] = String.Empty;
                objRow["datetime"] = String.Empty;
                objTable.Rows.Add(objRow);
                GridDisplayTable.Columns[8].HeaderStyle.CssClass = "hiderowcol";
                GridDisplayTable.Columns[8].ItemStyle.CssClass = "hiderowcol";
                objTable.AcceptChanges();
                ViewState["dtRateDetail"] = null;
            }
            GridDisplayTable.DataSource = objTable;
            GridDisplayTable.DataBind(); 
        }

        private void DeleteRowFromGrid()
        {
            int iRowId =Convert.ToInt32(SelectedRowId.Text);

            if (ViewState["dtRateDetail"] != null)
            {
                objTable = (DataTable)ViewState["dtRateDetail"];
            }

            for (int i = iRowId + 1; i < objTable.Rows.Count; i++)
            {
                objTable.Rows[i]["rowid"] = Convert.ToInt32(objTable.Rows[i]["rowid"].ToString()) - 1;
            }

            objTable.Rows[iRowId].Delete();
            objTable.AcceptChanges();
            ViewState["dtRateDetail"] = objTable;
        }

        private void AddRowToGrid()
        {
            if (mode.Text == "new")
            {
                if (ViewState["dtRateDetail"] != null)
                {
                    objTable = (DataTable)ViewState["dtRateDetail"];
                }
                else
                {
                    objTable.Columns.Add("rowid");
                    objTable.Columns.Add("trackid");
                    objTable.Columns.Add("transtypecode");
                    objTable.Columns.Add("transtype");
                    objTable.Columns.Add("unit");
                    objTable.Columns.Add("rate");
                    objTable.Columns.Add("status");
                    objTable.Columns.Add("user");
                    objTable.Columns.Add("datetime");

                    GridDisplayTable.Columns[8].HeaderStyle.CssClass = "visiblerowcol";
                    GridDisplayTable.Columns[8].ItemStyle.CssClass = "visiblerowcol";
                }
            }

            DataRow objRow = objTable.NewRow();
            objRow["rowid"] = objTable.Rows.Count;
            objRow["trackid"] = h_trackid.Text;
            objRow["transtypecode"] = h_transtypecode.Text;
            objRow["transtype"] = h_transtype.Text;
            objRow["unit"] = h_unit.Text;
            objRow["rate"] = h_rate.Text;

            if(mode.Text == "new")
                objRow["status"] = "N";
            else
                objRow["status"] = "E";

            objRow["user"] = h_user.Text ;
            objRow["datetime"] = h_datetime.Text ;
            h_trackid.Text = String.Empty;
            h_transtypecode.Text = String.Empty;
            h_transtype.Text = String.Empty;
            h_unit.Text = String.Empty;
            h_rate.Text = String.Empty;
            mode.Text = String.Empty;
            h_user.Text = String.Empty;
            h_datetime.Text = String.Empty;

            objTable.Rows.Add(objRow);
            objTable.AcceptChanges();
            ViewState["dtRateDetail"] = objTable;
            GridDisplayTable.DataSource = objTable;
            GridDisplayTable.DataBind();
        }

        private void EditRowOfGrid()
        {
            SelectedRowId.Text = h_rowid.Text;
            DeleteRowFromGrid();
            AddRowToGrid();
        }

        private void SaveGrid()
        {
            XmlTemplate = GetMessageTemplateForSave();
            CallCWS("TimeExpenseAdaptor.SaveRateTableXml", XmlTemplate, out sreturnValue, false, false);
            bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);
            if(bReturnStatus)
            Response.Redirect("/RiskmasterUI/UI/Utilities/Manager/RateAndUnits/rate-tables.aspx");
        }

        private XElement GetMessageTemplateForSave()
        {
            StringBuilder sXml = new StringBuilder();

            sXml.Append("<Message><Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml.Append("<Call><Function>TimeExpenseAdaptor.SaveRateTableXml</Function></Call>");
            sXml.Append("<Document><Document><FunctionToCall/><TimeExpense><FrmName>ratetable</FrmName>");
            sXml.Append("<InputXml><form><group name='ratetable'>");
            sXml.AppendFormat("<control name='rtname' type='text' value=\"{0}\"/>", RateTableName.Text);
            //MGaba2:MITS 17248:screen crashes on save when customer field contains apostrophe
            //Because XML was not getting parsed when customer with apostrophe was saved
            //sXml.AppendFormat("<control name='customer' type='textandlist' id='{0}' value='{1}'/>", customer_cid.Text, customer.Text);
            sXml.AppendFormat("<control name='customer' type='textandlist' id='{0}' value='{1}'/>", customer_cid.Text, customer.Text.Replace("'", "&apos;"));
            sXml.AppendFormat("<control name='CustomerFieldData' type='id'>{0}</control>", CustomerFieldData.Text);
            sXml.AppendFormat("<control name='efdate' type='date' value='{0}'/>", EffectiveDate.Text);
            sXml.AppendFormat("<control name='exdate' type='date' value=' {0}'/>", ExpirationDate.Text);

            sXml.Append("<control name='mode' type='id' value='");
                if (ratetableid.Text != "0")
                    sXml.Append("edit");
                else
                    sXml.Append("new");
            sXml.Append("'/>");

            sXml.AppendFormat("<control name='tableid' type='id' value='{0}'/>", ratetableid.Text);
            sXml.Append("</group><group><control name='ratetable' type='ratetable'>");

            if (ViewState["dtRateDetail"] != null)
            {
                objTable = (DataTable)ViewState["dtRateDetail"];
            }

            foreach(DataRow dtrow in objTable.Rows )
            {                
                sXml.AppendFormat("<option trackid='{0}' ", dtrow["trackid"]);
                sXml.AppendFormat("transtypecode='{0}' ", dtrow["transtypecode"]);
                sXml.AppendFormat("transtype='{0}' ", dtrow["transtype"]);
                sXml.AppendFormat("unit='{0}' ", dtrow["unit"]);
                sXml.AppendFormat("rate='{0}' ", dtrow["rate"]);
                sXml.AppendFormat("status='{0}' ", dtrow["status"]);
                sXml.AppendFormat("user='{0}' ", dtrow["user"]);
                sXml.AppendFormat("datetime='{0}' />", dtrow["datetime"]);              
            }

            sXml.Append("</control></group></form></InputXml></TimeExpense></Document></Document></Message>");
            sXml.Replace("&", "&amp;");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
