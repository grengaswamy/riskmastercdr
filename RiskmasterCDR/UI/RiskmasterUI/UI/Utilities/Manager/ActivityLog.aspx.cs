﻿using System;
using System.Collections;
using System.Linq;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common.Extensions;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class ActivityLog : NonFDMBasePageCWS 
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        XmlDocument oFDMPageDom = null;

        public string sOperation = null;
        public string sAccessType = null;
        public string sUserlogin = null;
        private bool bReturnStatus = false;

        XElement XmlTemplate = null;
        string sReturn = string.Empty;

        int iTotalRows = 0;
        int iPageSize = 0;
        int iCurrentPage = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.txtFirstTime.Text = "";
            //Start - Yukti-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //Ended:Yukti ML changes

            if (!IsPostBack)
            {
                this.txtFirstTime.Text = "1";
                NonFDMCWSPageLoad("ActivityLogAdaptor.Search");
                oFDMPageDom = new XmlDocument();
                oFDMPageDom = Data;
                rootElement = XElement.Parse(oFDMPageDom.OuterXml);
                BindDataToGrid(rootElement);

                int iPageSize = hdPageSize.Text.ConvertToInt32();
                int iTotalRows = hdTotalRows.Text.ConvertToInt32();

                if (iTotalRows == 0)
                {
                    lnkFirstTop.Enabled = false;
                    lnkLastTop.Enabled = false;
                    lnkPrevTop.Enabled = false;
                    lnkNextTop.Enabled = false;
                }
                else
                {
                    PagerSettings(iTotalRows, iPageSize);
                }
            }

          if(this.lstOperations.Items.Count>0) 
					this.lstOperations.Items[0].Value = "";
            
			   if(this.lstAccessType.Items.Count>0) 
                    this.lstAccessType.Items[0].Value = "";
            
                if(lstUserLogin.Items.Count>0) 
                    this.lstUserLogin.Items[0].Value = "";


            ViewState["FirstName"] = this.txtFirstName.Text;
            ViewState["LastName"] = this.txtLastName.Text;
            ViewState["FromDate"] = this.txtFromDate.Text;
            ViewState["ToDate"] = this.txtToDate.Text;
            sOperation  = this.lstOperations.SelectedValue;
            sAccessType = this.lstAccessType.SelectedValue;
            sUserlogin = this.lstUserLogin.SelectedValue;
        }

        protected void LinkCommand_Click(object sender, CommandEventArgs e)
        {  
            int iPreviousPage = 0;
            int iPageSize = hdPageSize.Text.ConvertToInt32();
            int iTotalRows = hdTotalRows.Text.ConvertToInt32();

          
            hdCurrentPage.Value = hdPageNumber.Text;
            lnkFirstTop.Enabled = true;
            lnkLastTop.Enabled = true;
            lnkPrevTop.Enabled = true;
            lnkNextTop.Enabled = true;

            switch (e.CommandName.ToUpper())
            {
                case "FIRST":
                    {
                        iCurrentPage = 0;

                        lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Text;

                        lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " of " + iTotalRows + " records ";

                        lnkFirstTop.Enabled = false;
                        lnkPrevTop.Enabled = false;
                        iCurrentPage = 1;                   
                        break;
                    }
                case "LAST":
                    {
                        iCurrentPage = hdTotalPages.Text.ConvertToInt32() - 1;

                        lblPageRangeTop.Text = "Page " + hdTotalPages.Text + " of " + hdTotalPages.Text;
                        lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " records ";

                        lnkLastTop.Enabled = false;
                        lnkNextTop.Enabled = false;
                        iCurrentPage = hdTotalPages.Text.ConvertToInt32();
                        break;
                    }
                case "PREV":
                    {
                        iCurrentPage = hdCurrentPage.Value.ConvertToInt32();

                        iPreviousPage = iCurrentPage - 1;

                        if (iPreviousPage == 1)
                        {
                            lnkFirstTop.Enabled = false;
                            lnkPrevTop.Enabled = false;
                        }

                        lblPageRangeTop.Text = "Page " + iPreviousPage + " of " + hdTotalPages.Text;
                        lblPagerTop.Text = (((iPreviousPage - 1) * iPageSize) + 1) + " - " + ((((iPreviousPage - 1) * iPageSize) + iPageSize)) + " of " + iTotalRows + " records ";
                        iCurrentPage = iPreviousPage;
                        break;
                    }
                case "NEXT":
                    {
                        iCurrentPage = hdCurrentPage.Value.ConvertToInt32();

                        if ((iCurrentPage + 1) == hdTotalPages.Text.ConvertToInt32())
                        {
                            lnkLastTop.Enabled = false;
                            lnkNextTop.Enabled = false;
                        }

                        lblPageRangeTop.Text = "Page " + (iCurrentPage + 1) + " of " + hdTotalPages.Text;

                        if (((iCurrentPage * iPageSize) + iPageSize) > iTotalRows) // Last Page
                        {
                            lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " records ";
                        }
                        else
                        {
                            lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " of " + iTotalRows + " records ";
                        }       
                        iCurrentPage=iCurrentPage+1;
                        break;
                    }
            }            
            NonFDMCWSPageLoad("ActivityLogAdaptor.Search");
            oFDMPageDom = new XmlDocument();
            oFDMPageDom = Data;
            rootElement = XElement.Parse(oFDMPageDom.OuterXml);
            BindDataToGrid(rootElement);
        }


        
        private void PagerSettings(int iTotalRows, int iPageSize)
        {
            lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Text;
            if (iPageSize > iTotalRows) // There is only 1 page.
            {
                lblPagerTop.Text = 1 + " - " + iTotalRows + " of " + iTotalRows + " records ";

                lnkFirstTop.Enabled = false;
                lnkLastTop.Enabled = false;
                lnkPrevTop.Enabled = false;
                lnkNextTop.Enabled = false;
            }
            else
            {
                lblPagerTop.Text = 1 + " - " + iPageSize + " of " + iTotalRows + " records ";

                lnkFirstTop.Enabled = false;
                lnkPrevTop.Enabled = false;
                lnkNextTop.Enabled = true;
                lnkLastTop.Enabled = true;
            }
        }


        private void BindDataToGrid(XElement p_objResultXml)
        {
            
            int count = rootElement.XPathSelectElements("SearchResults/row").Count();
            if (count <= 0)
            {
                XElement xele = rootElement.XPathSelectElement("SearchResults");
                XElement objNewElm = new XElement("row");
                objNewElm.SetAttributeValue("name", "");
                objNewElm.SetAttributeValue("operation", "");
                objNewElm.SetAttributeValue("login", "");
                objNewElm.SetAttributeValue("accesstype", "");
                objNewElm.SetAttributeValue("date", "");
                objNewElm.SetAttributeValue("time", "");
                xele.Add(objNewElm);
            }
                var result = from c1 in rootElement.XPathSelectElements("SearchResults/row")
                             select new
                             {                                 
                                 name = c1.Attribute("name").Value,
                                 operation = c1.Attribute("operation").Value,
                                 login = c1.Attribute("login").Value,
                                 accesstype = c1.Attribute("accesstype").Value,
                                 date = c1.Attribute("date").Value,
                                 time = c1.Attribute("time").Value
                             };
                gdActivityLog.DataSource = result;
                gdActivityLog.DataBind();
                if (count <= 0)
                {
                    gdActivityLog.Rows[0].Visible = false;
                }

              
        }
        protected void btnBeginSearch_Click1(object sender, EventArgs e)
        {
            try
            {
                NonFDMCWSPageLoad("ActivityLogAdaptor.Search");

                oFDMPageDom = new XmlDocument();

                oFDMPageDom = Data;

                rootElement = XElement.Parse(oFDMPageDom.OuterXml);

                BindDataToGrid(rootElement);

				//Bijender solved Mits 14631 :- Error occured If there is no enty for Operation, AccessType and users in the database.                 
				if(this.lstOperations.Items.Count>0) 
					this.lstOperations.Items[0].Value = "";
            
			   if(this.lstAccessType.Items.Count>0) 
                    this.lstAccessType.Items[0].Value = "";
            
                if(lstUserLogin.Items.Count>0) 
                    this.lstUserLogin.Items[0].Value = "";
				// End 14631

                this.txtFirstName.Text = ViewState["FirstName"].ToString();
                this.txtLastName.Text = ViewState["LastName"].ToString();
                this.txtFromDate.Text = ViewState["FromDate"].ToString();
                this.txtToDate.Text = ViewState["ToDate"].ToString();

                this.lstOperations.SelectedValue = sOperation;

                this.lstAccessType.SelectedValue = sAccessType;

                this.lstUserLogin.SelectedValue = sUserlogin;
                iTotalRows = hdTotalRows.Text.ConvertToInt32();
                iPageSize = hdPageSize.Text.ConvertToInt32();

                hdCurrentPage.Value = hdPageNumber.Text;

                PagerSettings(iTotalRows, iPageSize);

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }       

        public override void ModifyXml(ref XElement Xelement)
        {
            XElement xele = Xelement.XPathSelectElement("//Document/SearchParam");
            XElement objNewElm = new XElement("PageNumber");
            objNewElm.Value = iCurrentPage.ToString();
            xele.Add(objNewElm);
        }

        protected void gdActivityLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
             GridViewRow gvr   = e.Row;
    
            if (gvr.RowType == DataControlRowType.Header )
            {
                GridViewRow row  = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                TableCell cell  = new TableCell();
                cell.ColumnSpan = 2;
                cell.HorizontalAlign = HorizontalAlign.Center;
                cell.Text = "Product";
               row.Cells.Add(cell);
   
               cell = new TableCell();
               cell.ColumnSpan = 2;
               cell.HorizontalAlign = HorizontalAlign.Center;
               cell.Text = "Category";
               row.Cells.Add(cell);
   
               gdActivityLog.Controls.AddAt(0, row);
            }
        }       
    }
}
