<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiLanguage.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.MultiLanguage" ValidateRequest="false" %>


<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
   <title>MultiLanguage</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css"  type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">      { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">      { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">      { var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
        <div class="msgheader" id="div_formtitle">
            <asp:label id="formtitle" runat="server" text="<%$ Resources:lblMultiLangHdr %>" />
        </div>
        <uc1:errorcontrol id="ErrorControl1" runat="server" />
        <asp:textbox style="display: none" runat="server" id="RowId" rmxref="/option/RowId" rmxignoreset='true' />
        <asp:textbox style="display: none" runat="server" id="selectedid" />
        <asp:textbox style="display: none" runat="server" id="mode" />
        <asp:textbox style="display: none" runat="server" id="selectedrowposition" />
        <asp:textbox style="display: none" runat="server" id="gridname" />
        <asp:textbox style="display: none" runat="server" id="UniqueId" text="RowId" />
        <asp:textbox style="display: none" runat="server" id="MultiLanguageIDLoadvalue" />
        <table cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td>
                        <asp:imagebutton runat="server" src="../../../Images/save.gif" width="28" height="28"
                            border="0" id="ImageSave" alternatetext="<%$ Resources:ttSave %>" validationgroup="vgSave" onclick="btnOk_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label runat="server" class="required" id="lblLangName" text="<%$ Resources:lblLangName %>" />
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;
                   <asp:textbox runat="server" enabled="false" id="txtLanguageName"
                       rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='LanguageName']" tabindex="1" maxlength="9" type="text" />
                    </td>
                </tr>
                <tr>
                    &nbsp;
                </tr>
                <tr>
                    <td>
                        <asp:label runat="server" class="required" id="lblSupported" text="<%$ Resources:lblSupported %>" />
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;
                    <asp:dropdownlist id="drpSuppFlag" type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Supported']/@value"
                        onchange="setDataChanged(true);" runat="server">
                        <asp:ListItem Value="-1" text="<%$ Resources:liYes %>"></asp:ListItem>
                        <asp:ListItem Value="0" text="<%$ Resources:liNo %>"></asp:ListItem>
                    </asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    &nbsp;
                </tr>
                <tr>
                    <td>
                        <asp:label runat="server" class="required" id="lblDateFormat" text="<%$ Resources:lblDateFormat %>" />
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;
                        <asp:dropdownlist id="drpZipCodeFormats" type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FormatDate']/@value"
                            itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='FormatDate']" onchange="setDataChanged(true);" runat="server">
                    </asp:dropdownlist>
                    </td>
                </tr>
            </tbody>
        </table>
         <asp:textbox style="display: none" runat="server" id="txtPageId" rmxref="/Instance/Document/form/PageId"></asp:textbox>
         <asp:textbox style="display: none" runat="server" id="txtLangCode" rmxref="/Instance/Document/form/LangId"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="txtData" />
        <asp:textbox style="display: none" runat="server" id="txtPostBack" />
        <br />
        
    </form>
</body>
</html>
