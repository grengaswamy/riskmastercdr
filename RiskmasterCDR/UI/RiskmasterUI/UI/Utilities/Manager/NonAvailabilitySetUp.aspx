<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NonAvailabilitySetUp.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.NonAvailabilitySetUp" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
 <title>Non Availability Plan</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>
       <%--vkumar258 - RMA-6037 - Starts --%>
 <%-- <script type="text/javascript" src="../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>--%>
<link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body> <!-- onload = " return CheckForNAWindowClosing();"> --> 
    <form name="frmData" id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Non Availability Plan" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox Style="display: none" runat="server" RMXType="id" ID="txtRowID" RMXRef="/Instance/Document/NAPlan/control[@name ='RowID']" />
    <asp:TextBox Style="display: none" runat="server" RMXType="id" ID="txtAdjusterID" RMXRef="/Instance/Document/NAPlan/control[@name ='AdjusterID']" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="FormMode" />
    <div runat="server" class="full" id="div_sDate" xmlns="">
    <asp:Label runat="server" class="required" ID="lblStartDate" Text="Start Date:" />
        <span>
            <asp:TextBox runat="server" FormatAs="date" ID="txtStDate" RMXRef="/Instance/Document/NAPlan/control[@name ='sDate']"
                TabIndex="1" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" RMXType="date" />
                      <%--vkumar258 - RMA-6037 - Starts --%>
 <%-- <asp:Button class="DateLookupControl" runat="server" ID="btnsDate" TabIndex="1" />
            <script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "txtStDate",
					        ifFormat: "%m/%d/%Y",
					        button: "btnsDate"
					    }
					    );
            </script>--%>
<script type="text/javascript">
    $(function () {
        $("#txtStDate").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
        </span>
    </div>
    <div runat="server" class="full" id="div_sTime" xmlns="">
    <asp:label runat="server" class="required" id="lbl_sTime" Text="Start Time" />
    <span>
        <asp:TextBox runat="server" FormatAs="time" onchange="setDataChanged(true);" onblur="timeLostFocus(this.id);" 
        id="txtStTime" RMXRef="/Instance/Document/NAPlan/control[@name ='sTime']" RMXType="time" tabindex="2" />
    </span>
    </div>
    <div runat="server" class="full" id="div_eDate" xmlns="">
    <asp:Label runat="server" class="required" ID="lbleDate" Text="End Date:" />
        <span>
            <asp:TextBox runat="server" FormatAs="date" ID="txtEndDate" RMXRef="/Instance/Document/NAPlan/control[@name ='eDate']"
                TabIndex="3" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" RMXType="date"/>
                        <%--vkumar258 - RMA-6037 - Starts --%>
<%--<asp:Button class="DateLookupControl" runat="server" ID="btneDate" TabIndex="3" />
            <script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "txtEndDate",
					        ifFormat: "%m/%d/%Y",
					        button: "btneDate"
					    }
					    );
            </script>--%>
<script type="text/javascript">
    $(function () {
        $("#txtEndDate").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "4");
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
        </span>
    </div>
    <div runat="server" class="full" id="div_eTime" xmlns="">
    <asp:label runat="server" class="required" id="lbleTime" Text="End Time" />
    <span>
        <asp:TextBox runat="server" FormatAs="time" onchange="setDataChanged(true);" onblur="timeLostFocus(this.id);" 
        id="txtEndTime" RMXRef="/Instance/Document/NAPlan/control[@name ='eTime']" RMXType="time" tabindex="4" />
    </span>
    </div>
    <br />
    <br />    
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return OnNonAvailDateTimeSave();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return NonAvail_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
