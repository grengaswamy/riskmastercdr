﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintFroiAcordFrame.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.PrintFroiAcordFrame" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head" runat="server">
    <meta http-equiv="Pragma" content="no-cache" />
    <title>Froi & Acord</title>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/Utilities.js">        { var i; }</script>
</head>
<body onload="OpenPrintFroiAcord();">
    <form id="frmData" runat="server">
    <div>
        <table border="0" align="center">
            <tr>
                <td colspan="2">
                    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <asp:TextBox ID="filename" Style="display: none" RMXRef="Instance/Document/PrintFroiAcord/FileName"
            runat="server" />
    </div>
    </form>
</body>
</html>
