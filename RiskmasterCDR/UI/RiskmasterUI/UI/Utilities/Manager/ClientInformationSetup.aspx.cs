﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Text;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class ClientInformationSetup : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";

            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes

            if (!IsPostBack)
            {
                NonFDMCWSPageLoad("ClientInformationAdaptor.Get");
            }
            if (CMMSEAXDataGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                string selectedRowId = CMMSEAXDataSelectedId.Text;
                XmlTemplate = GetDeletionTemplate(selectedRowId);
                CallCWS("CMMSEAXDataAdaptor.DeleteMMSEAXData", XmlTemplate, out sCWSresponse, false, false);
                CMMSEAXDataGrid_RowDeletedFlag.Text = "false";
                NonFDMCWSPageLoad("ClientInformationAdaptor.Get");
            }
            if (setGridDataChanged.Text== "true")
            {
                NonFDMCWSPageLoad("ClientInformationAdaptor.Get");
            }
        }
        protected void Save(object sender, EventArgs e)
        {
            string sCWSResponse=string.Empty;
            string sMsgStatus = string.Empty;
            XmlDocument objReturnXml=null;
            try
            {
                bool bReturnStatus = false;
                foreach (Control oCtrl in this.Form.Controls)
                {
                    if (oCtrl is WebControl)
                    {
                        Type controlType = oCtrl.GetType();
                        string sType = controlType.ToString();
                        int index = sType.LastIndexOf(".");
                        sType = sType.Substring(index + 1);
                        switch (sType)
                        {
                            case "TextBox":
                                string sValue = ((TextBox)oCtrl).Text;
                             //tmalhotra3-Mits 31324,these changes were no longer required as the query has been parametrized during pen testing.  
                              //if (sValue.IndexOf("'") > 0)
                                //{
                                //    sValue = sValue.Replace("'", "''");
                                //}
                                ((TextBox)oCtrl).Text = sValue;
                                break;
                        }
                    }
                }
                bReturnStatus = CallCWSFunction("ClientInformationAdaptor.Save",out sCWSResponse);
                objReturnXml=new XmlDocument();
                objReturnXml.LoadXml(sCWSResponse);
                sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus=="Success")
                    bReturnStatus = CallCWSFunction("ClientInformationAdaptor.Get");
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //sgoel6 Medicare 05/20/2009
        private XElement GetDeletionTemplate(string selectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><MMSEAXDataDetails>");
            sXml = sXml.Append("<EntMMSEARowID>");
            sXml = sXml.Append(selectedRowId);
            sXml = sXml.Append("</EntMMSEARowID>");
            sXml = sXml.Append("</MMSEAXDataDetails></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
    }
}
