﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillingRule.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.BillingRule" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="CodeLookUp" Src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add/Modify Billing Rule</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>

</head>
<body onload="CopyGridRowDataToPopup();">
    <form id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Add/Modify Billing Rule" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document//BillingRuleData/control[@name='RowId']"
        rmxignoreset='true' />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="AddedByUser" RMXRef="/Instance/Document//BillingRuleData/control[@name='AddedByUser']"
        type="id" />
    <asp:TextBox Style="display: none" runat="server" ID="DttmRcdAdded" RMXRef="/Instance/Document//BillingRuleData/control[@name='DttmRcdAdded']"
        type="id" />
    <asp:TextBox Style="display: none" runat="server" ID="DttmRcdLastUpd" RMXRef="/Instance/Document//BillingRuleData/control[@name='DttmRcdLastUpd']"
        type="id" />
    <asp:TextBox Style="display: none" runat="server" ID="UpdatedByUser" RMXRef="/Instance/Document//BillingRuleData/control[@name='UpdatedByUser']"
        type="id" />
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
        <tr><td>&nbsp;</td></tr>
            <tr class="ctrlgroup">
            
                <td colspan="3">
                    <asp:Label runat="server" name="BillingRuleDetails" ID="BillingRuleDetails" Text="Billing Rule Details" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" Text="Use Billing Rule:" ID="lblUseFlag" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="InUseFlag" RMXRef="/Instance/Document//BillingRuleData/control[@name='InUseFlag']"
                        type="checkbox" onclick="return setDataChanged(true);" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblBillingRuleCode" Text="Billing Rule Code:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="BillingRuleCode" CodeTable="BILLING_RULES" ControlName="BillingRuleCode"
                        RMXRef="/Instance/Document//BillingRuleData/control[@name='BillingRuleCode']"
                        type="code" />
                </td>
            </tr>
            <tr>
            </tr>
            
            <tr class="ctrlgroup">
                <td colspan="3">
                    <asp:Label runat="server" name="NoticeSettings" ID="NoticeSettings" Text="Notice Settings" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblNoticeOffsetInd" Text="Offset From:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="NoticeOffsetInd" CodeTable="NOTICE_OFFSET_IND"
                        ControlName="NoticeOffsetInd" RMXRef="/Instance/Document//BillingRuleData/control[@name='NoticeOffsetInd']"
                        type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblFirstNoticeDays" Text="First Notice Days:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="FirstNoticeDays" RMXRef="/Instance/Document//BillingRuleData/control[@name='FirstNoticeDays']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblCancelPendDiary" Text="Run Cancel Pending Diary:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="CancelPendDiary" CodeTable="CP_DIARY_SCHED" ControlName="CancelPendDiary"
                        RMXRef="/Instance/Document//BillingRuleData/control[@name='CancelPendDiary']"
                        type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblSecondNoticeDays" Text="Second Notice Days:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="SecondNoticeDays" RMXRef="/Instance/Document//BillingRuleData/control[@name='SecondNoticeDays']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblCancelDiary" Text="Run Cancellation Diary:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="CancelDiary" CodeTable="CP_DIARY_SCHED" ControlName="CancelDiary"
                        RMXRef="/Instance/Document//BillingRuleData/control[@name='CancelDiary']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblThirdNoticeDays" Text="Third Notice Days:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="ThirdNoticeDays" RMXRef="/Instance/Document//BillingRuleData/control[@name='ThirdNoticeDays']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblFourthNoticeDays" Text="Fourth Notice Days:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="FourthNoticeDays" RMXRef="/Instance/Document//BillingRuleData/control[@name='FourthNoticeDays']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
            <tr>
            </tr>
            
            <tr class="ctrlgroup">
                <td colspan="3">
                    <asp:Label runat="server" name="PremiumAmounts" ID="PremiumAmounts" Text="How To Collect Additional Premium Amounts" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblOutstandBalIndic" Text="Additional Amounts Indicator:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="OutstandBalIndic" CodeTable="NON_PAYPLAN_AMTS"
                        ControlName="OutstandBalIndic" RMXRef="/Instance/Document//BillingRuleData/control[@name='OutstandBalIndic']"
                        type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblNonPayInd" Text="Non-Pay Indicator:"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="NonPayInd" CodeTable="NON_PAY_INDIC" ControlName="NonPayInd"
                        RMXRef="/Instance/Document//BillingRuleData/control[@name='NonPayInd']" type="code" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblMaxNumRebills" Text="Max Number Re-Bills:" class="required"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MaxNumRebills" RMXRef="/Instance/Document//BillingRuleData/control[@name='MaxNumRebills']"
                        MaxLength="50" type="text" onChange="setDataChanged(true);" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDueDays" Text="Due Days:" class="required"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DueDays" RMXRef="/Instance/Document//BillingRuleData/control[@name='DueDays']"
                        MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text" />
                </td>
            </tr>
        </tbody>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return BillingRule_onOk();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return BillingRule_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
