﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimTypeChangeOptions.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.ClaimTypeChangeOptions" ValidateRequest = "false" %>
<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head runat="server">
  <title>Claim Type Change Option Setup</title>
  <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">{var i;}
</script>
  </head>
  <body onload="parent.MDIScreenLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <div>
          <span>
            <dg:UserControlDataGrid runat="server" ID="ClaimTypeGrid" GridName="ClaimTypeGrid" GridTitle="Claim Type Change Options" Target="/Document/ClaimList" Ref="/Instance/Document/form//control[@name='ClaimTypeGrid']" Unique_Id="RowId" ShowRadioButton="true" Width="" Height="100%" hidenodes="|RowId|" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="300" Type="GridAndButtons" RowDataParam="listrow" OnClick="KeepRowForEdit('ClaimTypeGrid');" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="ClaimTypeSelectedId"  RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="ClaimTypeGrid_Action" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="ClaimTypeGrid_RowAddedFlag"  RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="ClaimTypeGrid_RowDeletedFlag" RMXType="id" />
      </div>
      
    </form>
  </body>
</html>
