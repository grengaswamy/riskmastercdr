﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.Common;
using System.Threading;
using System.Globalization;

///////////////////////////////
// Developed By: abansal23
// Completed On: 11th Feb, 2009
///////////////////////////////
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class DiscountRange : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //rupal:start, r8 multicurrency
            if (!IsPostBack)
            {
                string sCWSResponse = string.Empty;
                XmlDocument xDoc = new XmlDocument();
                CallCWS("RMUtilitiesAdaptor.GetBaseCurrency", GetMessageTemplate(), out sCWSResponse, false, false);
                xDoc.LoadXml(sCWSResponse);
                if (xDoc.SelectSingleNode("//BaseCurrencyType") != null && !string.IsNullOrEmpty(xDoc.SelectSingleNode("//BaseCurrencyType").InnerText))
                {
                    currencytype.Text = "|" + xDoc.SelectSingleNode("//BaseCurrencyType").InnerText;
                    InitializeCulture();
                }
            }
            //rupal:end
            if (!IsPostBack)
            {               
                gridname.Text = AppHelper.GetQueryStringValue("gridname");
                mode.Text = AppHelper.GetQueryStringValue("mode");
                selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedid");
                UseVolumeDiscount.Text = AppHelper.GetQueryStringValue("usevolumediscount");
                if (mode.Text.ToLower() != "add")
                {
                    string strData = Session["discountrangelist"].ToString();
                    XmlDocument xdata = new XmlDocument();
                    xdata.LoadXml(strData);
                    XmlNodeList nodes = xdata.SelectNodes("//discountrange");
                    string sUpperBound = string.Empty;
                    foreach (XmlElement node in nodes)
                    {
                        if (node.ChildNodes[6].InnerText == selectedrowposition.Text)
                        {
                            //rupal:r8 multicurrency                            
                            LowerBound.AmountInString = ((XmlElement)(node.ChildNodes[5])).GetAttribute("Amount");
                            sUpperBound = ((XmlElement)(node.ChildNodes[7])).GetAttribute("Amount");
                            if ((UseVolumeDiscount.Text == "true") && sUpperBound == string.Empty)
                            {
                                UpperBound.AmountInString = "0";
                            }
                            else
                            {
                                UpperBound.AmountInString = sUpperBound;
                            }
                            //LowerBound.AmountInString = node.ChildNodes[5].InnerText;
                            //UpperBound.AmountInString = node.ChildNodes[7].InnerText;
                            //Amount.Text = node.ChildNodes[4].InnerText;
                            Amount.Text = ((XmlElement)(node.ChildNodes[4])).GetAttribute("Amount");
                            //rupal:end
                            
                        }
                    }
                 
                    //Session["discountrangelist"] = xdata.OuterXml;
                }
                //rupal:start, mits 28025
                if (UseVolumeDiscount.Text == "true")
                {
                    lblUpperBoundMsg.Text = "(Zero indicates blank Upper Bound)";
                    lblUpperBoundMsg.Visible = true;
                }
                else
                {
                    lblUpperBoundMsg.Text = string.Empty;
                    lblUpperBoundMsg.Visible = false;
                }
                //rupal:end
            }  
        }
        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                mode.Text = AppHelper.GetQueryStringValue("mode");
                selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedid");
                string strData = Session["discountrangelist"].ToString();
                XmlDocument xdata = new XmlDocument();
                xdata.LoadXml(strData);
                XmlNodeList nodes = xdata.SelectNodes("//discountrange");

                if (mode.Text.ToLower() != "add")
                {
                    
                   
                    foreach (XmlElement node in nodes)
                    {
                        if (node.ChildNodes[6].InnerText == selectedrowposition.Text)
                        {
                            
                           
                            //rupal:multicurrency
                            node.ChildNodes[5].InnerText = LowerBound.Text;                            

                            ((XmlElement)(node.ChildNodes[5])).SetAttribute("Amount", LowerBound.AmountInString);
                            if ((UseVolumeDiscount.Text == "true") && (UpperBound.Amount == 0))
                            {
                                node.ChildNodes[7].InnerText = string.Empty;
                                ((XmlElement)(node.ChildNodes[7])).SetAttribute("Amount", string.Empty);
                            }
                            else
                            {
                                node.ChildNodes[7].InnerText = UpperBound.Text;
                                ((XmlElement)(node.ChildNodes[7])).SetAttribute("Amount", UpperBound.AmountInString);
                            }
                            node.ChildNodes[4].InnerText = Amount.Text;
                            ((XmlElement)(node.ChildNodes[4])).SetAttribute("Amount", Amount.Text);                            
                            //node.ChildNodes[8].InnerText = node.ChildNodes[5].InnerText + "|" + node.ChildNodes[7].InnerText + "|" + node.ChildNodes[6].InnerText;
                            node.ChildNodes[8].InnerText = ((XmlElement)(node.ChildNodes[5])).GetAttribute("Amount") + "|" + ((XmlElement)(node.ChildNodes[7])).GetAttribute("Amount") + "|" + node.ChildNodes[6].InnerText;
                            //rupal:end
                        }
                    }
                    Session["discountrangelist"] = xdata.OuterXml;
                }
                else
                {
                    XmlNode node = xdata.SelectSingleNode("//listhead");
                    string strTempNode = node.OuterXml;
                    XmlDocument xTempData = new XmlDocument();
                    xTempData.LoadXml(strTempNode);
                    xTempData.FirstChild.ChildNodes[0].InnerText = "";
                    xTempData.FirstChild.ChildNodes[1].InnerText = "";
                    xTempData.FirstChild.ChildNodes[2].InnerText = "";
                    xTempData.FirstChild.ChildNodes[3].InnerText = "";
                    xTempData.FirstChild.ChildNodes[6].InnerText = Session["RowCount"].ToString();
                    
                    xTempData.FirstChild.ChildNodes[5].InnerText = LowerBound.Text;
                    
                    //rupal:multicurrency
                    ((XmlElement)(xTempData.FirstChild.ChildNodes[5])).SetAttribute("Amount", LowerBound.AmountInString);
                    
                    if ((UseVolumeDiscount.Text == "true") && (UpperBound.Amount == 0))
                    {
                        xTempData.FirstChild.ChildNodes[7].InnerText = string.Empty;
                        ((XmlElement)(xTempData.FirstChild.ChildNodes[7])).SetAttribute("Amount", string.Empty);
                    }
                    else
                    {
                        xTempData.FirstChild.ChildNodes[7].InnerText = UpperBound.Text;
                        ((XmlElement)(xTempData.FirstChild.ChildNodes[7])).SetAttribute("Amount", UpperBound.AmountInString);
                    }
                    xTempData.FirstChild.ChildNodes[4].InnerText = Amount.Text;
                    ((XmlElement)xTempData.FirstChild.ChildNodes[4]).SetAttribute("Amount", Amount.Text);                                             
                    //rupal:end
                    XmlElement extra = xTempData.CreateElement("Extra");
                    //rupal:start, multicurency enh
                    //XmlText extraValue = xTempData.CreateTextNode(xTempData.FirstChild.ChildNodes[5].InnerText + "|" + xTempData.FirstChild.ChildNodes[7].InnerText + "|" + xTempData.FirstChild.ChildNodes[6].InnerText);
                    XmlText extraValue = xTempData.CreateTextNode(((XmlElement)(xTempData.FirstChild.ChildNodes[5])).GetAttribute("Amount") + "|" + ((XmlElement)(xTempData.FirstChild.ChildNodes[7])).GetAttribute("Amount") + "|" + xTempData.FirstChild.ChildNodes[6].InnerText);
                    //rupal:end
                    extra.AppendChild(extraValue);
                    xTempData.FirstChild.AppendChild(extra);                   
                    strTempNode = xTempData.OuterXml;
                    strTempNode = strTempNode.Replace("listhead", "discountrange");
                    Session["discountrangelist"] = strData.Replace("</discountrangelist>", strTempNode + "</discountrangelist>");
                    Session["RowCount"] = Int32.Parse(Session["RowCount"].ToString()) - 1;

                }
                FormValid.Text = "valid";
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        //rupal:r8 multicurrency enh
        private XElement GetMessageTemplate()
        {
            string sMesage = @"<Message><Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization><Call><Function /></Call><Document><BaseCurrencyType></BaseCurrencyType></Document></Message>";
            return XElement.Parse(sMesage);
        }

        protected override void InitializeCulture()
        {
            if (Request.Form["currencytype"] != null)
            {
                if (!string.IsNullOrEmpty(Request.Form["currencytype"]))
                {
                    Culture = Request.Form["currencytype"].Split('|')[1];
                    UICulture = Request.Form["currencytype"].Split('|')[1];
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.Form["currencytype"].Split('|')[1]);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(Request.Form["currencytype"].Split('|')[1]);
                }
            }
            else if ((currencytype != null) && (!string.IsNullOrEmpty(currencytype.Text)))
            {
                Culture = currencytype.Text.Split('|')[1];
                UICulture = currencytype.Text.Split('|')[1];
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(currencytype.Text.Split('|')[1]);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(currencytype.Text.Split('|')[1]);
            }            
        }
        //rupal:end
    }
}
