﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TierList.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.TierList" %>

<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <%--    <link rel="stylesheet" href="csc-Theme/riskmaster/common/style/rmnet.css" type="text/css">--%>
    <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
    </style>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/drift.js"></script>

    <%--    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/dhtml-div.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/dhtml-help-setup.js"></script>

    <script src="csc-Theme/riskmaster/common/javascript/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/window.js"
        type="text/javascript"></script>

    <script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/dialog.js"
        type="text/javascript"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar-setup.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/calendar-alias.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-xml.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-editable.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-query.js"></script>

    <script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/rmx-common-ref.js"></script>--%>
</head>
<body class="10pt">
    <form id="frmData" name="frmData" method="post" runat="server">
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" /><div class="msgheader"
        id="formtitle">
    </div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Select Applicable Level
                                    <asp:TextBox runat="server" ID="tb_LOB_Code" Style="display: none" RMXref="/Instance/Document/PassToWebService/LOBCode[ @name ='LobCode']"></asp:TextBox>
                                    <asp:TextBox runat="server" ID="tb_State_Code" Style="display: none" RMXref="/Instance/Document/PassToWebService/StateCode[ @name ='StateCode']"></asp:TextBox>
                                    <!-- rsushilaggar MITS 25274 date 06/24/2011-->
                                    <asp:TextBox runat="server" ID="tb_Parent_Code" Style="display: none" RMXref="/Instance/Document/PassToWebService/ParentCode[ @name ='ParentCode']"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="500px">
                                    <b></b>
                                    <table width="100%">
                                        <tr>
                                            <td width="80%">
                                                <div style="&#xa; width: 500px; height: 400px; &#xa; &#xa;">
                                                    <dg:UserControlDataGrid runat="server" ID="TierListGrid" GridName="TierListGrid"
                                                        HideButtons="New|Edit|Delete" Target="/Document/PassToWebService/TierList" Ref="/Instance/Document/form//control[@name='TierListGrid']"
                                                        Unique_Id="RowId" Width="680px" Height="150px" HideNodes="|RowId|ParentLevel|" ShowHeader="True"
                                                        LinkColumn="Level Name" PopupWidth="500px" PopupHeight="250px" Type="Grid" RowDataParam="listrow" />
                                                    <asp:TextBox ID="tbTierListGrid" Style="display: none" runat="server" rmxignoreset="true"
                                                        RMXref="/Instance/Document/PassToWebService/TierList[ @name ='txtTierListGrid']"></asp:TextBox>
                                                    <asp:TextBox ID="tbTierListLevelName" Style="display: none" runat="server" Text="Level Name"
                                                        RMXref="/Instance/Document/PassToWebService/TierList/listhead/LevelName[ @name ='txtTierListGridLevelName']"></asp:TextBox>
                                                    <asp:TextBox ID="tbTierListState" Style="display: none" runat="server" Text="State"
                                                        RMXref="/Instance/Document/PassToWebService/TierList/listhead/State[ @name ='txtTierListGridState']"></asp:TextBox>
                                                    <asp:TextBox type="date" ID="tbTierListEffectiveDate" Style="display: none" runat="server" Text="Effective Date"
                                                        RMXref="/Instance/Document/PassToWebService/TierList/listhead/EffectiveDate[ @name ='txtTierListGridEffectiveDate']"></asp:TextBox>                                                        
                                                    <asp:TextBox type="date" ID="tbTierListExpirationDate" Style="display: none" runat="server" Text="Expiration Date"
                                                        RMXref="/Instance/Document/PassToWebService/TierList/listhead/ExpirationDate[ @name ='txtTierListGridExpirationDate']"></asp:TextBox>
                                                        <!-- rsushilaggar added the parent level hidden colum in the grid -->
                                                    <asp:TextBox ID="tbTierListParent" Style="display: none" runat="server" Text="ParentLevel"
                                                        RMXref="/Instance/Document/PassToWebService/TierList/listhead/ParentLevel[ @name ='txtTierListGridParentLevel']"></asp:TextBox>
                                                    <asp:TextBox ID="tbreserveGridRowID" Style="display: none" runat="server" Text="RowId"
                                                        RMXref="/Instance/Document/PassToWebService/TierList/listhead/RowId[ @name ='txtTierListGridRowId']"></asp:TextBox>
                                                    

                                                    <asp:TextBox Style="display: none" runat="server" ID="TierListGrid_Action" Text="" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="text" name="" value="" id="SysViewType" style="display: none" />
                    <input type="text" name="" value="" id="SysCmd" style="display: none">
                    <input type="text" name="" value="" id="SysCmdConfirmSave" style="display: none">
                    <input type="text" name="" value="" id="SysCmdQueue" style="display: none">
                    <input type="text" name="" value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate" />
                    <input type="text" name="" value="" id="SysClassName" style="display: none" rmxforms:value="" />
                    <input type="text" name="" value="" id="SysSerializationConfig" style="display: none" />
                    <input type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId" />
                    <input type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId" />
                    <input type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="TierList" />
                    <input type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value="" />
                    <input type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="TierList" />
                    <input type="text" name="" value="" id="SysRequired" style="display: none" />
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-3" id="SysWindowId" /></form>
</body>
</html>
