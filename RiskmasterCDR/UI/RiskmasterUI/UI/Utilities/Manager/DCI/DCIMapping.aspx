<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DCIMapping.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.DCIMapping" ValidateRequest = "false"%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="usc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <%--DCI Mapping SetUp--%>
    <title><asp:Label ID="lblDCIBenefitCodeMappingtitle" runat="server" Text="<%$ Resources:lblDCIMappingSetup %>"></asp:Label></title>
    <script language="javascript" src="../../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
    <script language="javascript" src="../../../../Scripts/form.js" type="text/javascript"></script>
    <script type="text/javascript">
        var ns, ie, ieversion;
        var browserName = navigator.appName;                   // detect browser 
        var browserVersion = navigator.appVersion;
        if (browserName == "Netscape") {
            ie = 0;
            ns = 1;
        }
        else		//Assume IE
        {
            ieversion = browserVersion.substr(browserVersion.search("MSIE ") + 5, 1);
            ie = 1;
            ns = 0;
        }

        function SelectionValid() {
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    if (document.forms[0].elements[i].checked == true) {
                        return true;
                    }
                }
            }
            return false;
        }

        function SelectionValidBenefitsMapping() {
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    controlname = document.forms[0].elements[i].name;
                    if (controlname.indexOf("selBenefitsMapping") > -1) {
                        if (document.forms[0].elements[i].checked == true) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        function SelectionValidFieldsMapping() {
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    controlname = document.forms[0].elements[i].name;
                    if (controlname.indexOf("selFieldsMapping") > -1) {
                        if (document.forms[0].elements[i].checked == true) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        function onPageLoaded() {
            if (ie) {
                if ((eval("document.all.divForms") != null) && (ieversion >= 6)) {
                    eval("document.all.divForms").style.height = '55%';
                }
            }
            else {
                var o_divforms;
                o_divforms = document.getElementById("divForms");
                if (o_divforms != null) {
                    o_divforms.style.height = window.frames.innerHeight * 0.70;
                    o_divforms.style.width = window.frames.innerWidth * 0.995;
                }
            }
        }

        function CheckBenefitsExists() {

            if ((document.forms[0].lstBenefits.value == "") || (document.forms[0].TransactionTypeBenefits_codelookup_cid.value == "")) {
                //alert("DCI Benefit and Transaction Type are required");
                alert(parent.CommonValidations.DCIBenefitAndTransactionTypeRequired);
                return false;
            }
            arr = document.forms[0].hdnBenefits.value.split("**");
            str = "|" + document.forms[0].lstBenefits.value + "|" + document.forms[0].TransactionTypeBenefits_codelookup_cid.value + "|";
            for (i = 0; i < arr.length; i++) {
                if (arr[i] == str) {
                    //alert("The combination of Benefit and Transaction Type are currently \n part of the mapping and will not be added.");
                    alert(parent.CommonValidations.DCIMappingCombinationValidation);
                    return false;
                }
            }
            return true;
        }

        function DeleteBenefitsMapping() {
            var selected = "";
            var controlname = "";
            var flag = "0";
            if (SelectionValidBenefitsMapping()) {
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        controlname = document.forms[0].elements[i].name;
                        if (controlname.indexOf("selBenefitsMapping") > -1) {
                            if (document.forms[0].elements[i].checked == true) {
                                flag = "1";
                                if (selected == "") {
                                    selected = document.forms[0].elements[i].value;
                                }
                                else {
                                    selected = selected + " " + document.forms[0].elements[i].value;
                                }
                            }
                        }
                    }
                }
                document.forms[0].hdnSelectedBenefits.value = selected;
                if (flag == "1")
                    return true;
                else {
                    return false;
                }
            }
            else {
                alert(parent.CommonValidations.DCIBenefitsMappingRemove);
                return false;
            }
        }

        function CheckDCIFieldExist() {

            if ((document.forms[0].lstDCIFields.value == "") || (document.forms[0].lstReserveType.value == "")) {
                //alert("DCI Financial Fields and Reserve Type are required");
                alert(parent.CommonValidations.DCIBenefitAndReserveRequired);
                return false;
            }

            arr = document.forms[0].hdnFields.value.split("**");
            str = "|" + document.forms[0].lstDCIFields.value + "|" + document.forms[0].lstReserveType.value + "|" + document.forms[0].lstTransactionType.value + "|";
            for (i = 0; i < arr.length; i++) {
                if (arr[i] == str) {
                    alert(parent.CommonValidations.DCIFinancialFieldsMappingCombinationValidation);
                    return false;
                }
            }

            return true;
        }

        function DeleteDCIFieldsMapping() {
            var selected = "";
            var controlname = "";
            var flag = "0";
            if (SelectionValidFieldsMapping()) {
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        controlname = document.forms[0].elements[i].name;
                        if (controlname.indexOf("selFieldsMapping") > -1) {
                            if (document.forms[0].elements[i].checked == true) {
                                flag = "1";
                                if (selected == "") {
                                    selected = document.forms[0].elements[i].value;
                                }
                                else {
                                    selected = selected + " " + document.forms[0].elements[i].value;
                                }
                            }
                        }
                    }
                }
                document.forms[0].hdnSelectedFields.value = selected;
                if (flag == "1")
                    return true;
                else
                    return false;
            }
            else {
                alert(parent.CommonValidations.DCIFinancialFieldsMappingRemove);
                return false;
            }
        }
	</script>
	    <script language="javascript" src="../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
	    <script language="javascript" src="../../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
	    <script language="javascript" src="../../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
	    <script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
	    <script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
	    <script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
	    <script language="javascript" type="text/javascript" src="../../../Scripts/calendar-alias.js"></script>
	    <script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid.js"></script>
	    <script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js"></script>
	    <script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js"></script>
	    <script language="javascript" type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js"></script>
</head>
<body onload="loadTabList();parent.MDIScreenLoaded();" style="height: 93%; width: 99.5%">
    <form id="frmData" runat="server" method="post">
        <uc1:errorcontrol id="ErrorControl1" runat="server" />
        <input type="hidden" name="hTabName" />
        <div class="msgheader" id="formtitle"><%--DCI Mapping SetUp--%><asp:Label ID="lblDCIMappingSetup" runat="server" Text="<%$ Resources:lblDCIMappingSetup %>"></asp:Label></div>
        <div class="errtextheader"></div>

        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="Selected" nowrap="nowrap" id="TABSBenefitMapping"><a class="Selected" href="#" onclick="tabChange(this.name);return false;" name="BenefitMapping" id="LINKTABSBenefitMapping"><span style="text-decoration: none"><asp:Label ID="lblDCIBenefitCodeMapping" runat="server" Text="<%$ Resources:lblDCIBenefitCodeMapping %>"></asp:Label><%--DCI Benefit Code Mapping--%></span></a></td>
                <td nowrap="nowrap" style="border-bottom: none; border-left: none; border-right: none; border-top: none;">&nbsp;&nbsp;</td>
                <td class="NotSelected" nowrap="nowrap" id="TABSFinancialMapping"><a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="FinancialMapping" id="LINKTABSFinancialMapping"><span style="text-decoration: none"><asp:Label ID="lblDCIFinFieldMapping" runat="server" Text="<%$ Resources:lblDCIFinFieldMapping %>"></asp:Label><%--DCI Financial Fields Mapping--%></span></a></td>
                <td nowrap="nowrap" style="border-bottom: none; border-left: none; border-right: none; border-top: none;">&nbsp;&nbsp;</td>
                <td valign="top" nowrap="nowrap"></td>
            </tr>
        </table>
        <div style="position:relative;left:0;top:0;width:100%;height:88%;overflow-x:hidden;overflow-y:auto;" class="singletopborder">
        <table width="98%" border="0" cellspacing="0" cellpadding="0" name="FORMTABBenefitMapping"
            id="FORMTABBenefitMapping">
            <tr>
                <td class="ctrlgroup" colspan="3"><asp:Label ID="lblBenefit1" runat="server" Text="<%$ Resources:lblBenefit1 %>"></asp:Label><%--A benefit can have an unlimited number of Transaction Types.--%><br />
                    <asp:Label ID="lblBenefit2" runat="server" Text="<%$ Resources:lblBenefit2 %>"></asp:Label><%--A Transaction Type should be used only one time.--%>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td class="ctrlgroup" colspan="3"><asp:Label ID="lblDCIBenefitTabHead" runat="server" Text="<%$ Resources:lblDCIBenefitCodeMapping %>"></asp:Label><%--DCI Benefit Code Mapping--%></td>
            </tr>
            <tr>
                <td width="37%" align="left"><asp:Label ID="lblColDCIBeneFit" runat="server" Text="<%$ Resources:lblColDCIBeneFit %>"></asp:Label><%--DCI Benefits--%></td>
                <td width="32%" align="left"><asp:Label ID="lblColDCITransactionType" runat="server" Text="<%$ Resources:lblColDCITransactionType %>"></asp:Label><%--Transaction Type--%></td>
                <td width="30%" align="left"><asp:Label ID="lblColWeeklyBenefitAmt" runat="server" Text="<%$ Resources:lblColWeeklyBenefitAmt %>"></asp:Label><%--Weekly Benefit Amount--%></td>
            </tr>
            <tr>
                <td align="left">
                    <asp:dropdownlist type="combobox" runat="server" id="lstBenefits" onchange="setDataChanged(true);" onselectedindexchanged="lstBenefits_SelectedIndexChanged" rmxref="Instance/Document/form/group/displaycolumn/control[@name='DCIBenefits']" itemsetref="Instance/Document/form/group/displaycolumn/control[@name='DCIBenefits']" autopostback="true">
               </asp:dropdownlist>
                </td>
                <td align="left">
                    <usc:codelookup runat="server" onchange="setDataChanged(true);" id="TransactionTypeBenefits" codetable="TRANS_TYPES" controlname="TransactionTypeBenefits" rmxtype="code" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='TransactionTypeBenefits']" />
                </td>
                <td align="left">
                    <asp:textbox runat="server" id="BenefitAmount" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='BenefitAmount']" rmxtype="currency" onblur="currencyLostFocus(this);" rmxforms:as="currency" onchange="setDataChanged(true);"></asp:textbox>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="divForms" style="height:120px;overflow-x:hidden;overflow-y:auto;" class="divScroll">
                                <asp:gridview width="100%" runat="server" id="GdDCIBenefitMapping" autogeneratecolumns="false" alternatingrowstyle-cssclass="rowdark">
	                                <Columns>
		                                <asp:TemplateField HeaderText="" HeaderStyle-CssClass="colheader5" ItemStyle-Width="5%">
			                                <ItemTemplate>
				                                <input type="checkbox" name="selBenefitsMapping" id="selBenefitsMapping" value='<%#Eval("CheckValue")%>' runat="server" />
			                                </ItemTemplate>
		                                </asp:TemplateField>
		                                <asp:TemplateField HeaderText="<%$ Resources:lblColDCIBeneFit %>" ItemStyle-Width="32%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
			                                <ItemTemplate>
				                                <asp:Label runat="server" ID="benefitName"></asp:Label>
			                                </ItemTemplate>
		                                </asp:TemplateField>
		                                <asp:TemplateField HeaderText="<%$ Resources:lblColDCITransactionType %>" ItemStyle-Width="32%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
			                                <ItemTemplate>
				                                <asp:Label runat="server" ID="transactionName"></asp:Label>
			                                </ItemTemplate>
		                                </asp:TemplateField>
		                                <asp:TemplateField HeaderText="<%$ Resources:lblColWeeklyBenefitAmt %>" ItemStyle-Width="32%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
			                                <ItemTemplate>
				                                <asp:Label runat="server" ID="benefitAmount"></asp:Label>
			                                </ItemTemplate>
		                                </asp:TemplateField>
	                                </Columns>
	                                <EmptyDataTemplate>
		                                <asp:Label runat="server" Text="" Width="4%" ID="headerCheck" CssClass="colheader5"></asp:Label>
		                                <asp:Label runat="server" Text="<%$ Resources:lblColDCIBeneFit %>" Width="32%" ID="headerBenefits" CssClass="colheader5"></asp:Label>
		                                <asp:Label runat="server" Text="<%$ Resources:lblColDCITransactionType %>" Width="32%" ID="headerTransaction" CssClass="colheader5"></asp:Label>
		                                <asp:Label runat="server" Text="<%$ Resources:lblColWeeklyBenefitAmt %>" Width="32%" ID="headerBenefitAmount" CssClass="colheader5"></asp:Label>
	                                </EmptyDataTemplate> 
                                </asp:gridview>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:textbox type="radiolist" runat="server" id="DCIBenefitsList" style="display: none" rmxref="Instance/Document/form/group/displaycolumn/control[@name='DCIBenefitsList']"></asp:textbox>
                </td>
                <td colspan="2">
                    <asp:textbox style="display: none" runat="server" type="hidden" id="hdnBenefits"></asp:textbox>
                    <asp:textbox style="display: none" runat="server" type="hidden" id="hdnSelectedBenefits"></asp:textbox>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" width="99%" align="center">
                    <asp:button runat="server" id="btnSaveBenefit" type="submit" text="<%$ Resources:btnSaveDCIBenefit %>" class="button" onclientclick="return CheckBenefitsExists();" onclick="SaveBenefitMapping" />
                    <asp:button runat="server" id="btnDeleteBenefit" type="submit" text="<%$ Resources:btnDeleteDCIBenefit %>" class="button" onclientclick="return DeleteBenefitsMapping();" onclick="DeleteBenefitMapping" />
                </td>
            </tr>
            <%--<tr>
                <td colspan="3">&nbsp;</td>
            </tr>--%>
        </table>
        <table width="98%" border="0" cellspacing="0" cellpadding="0" name="FORMTABFinancialMapping"
            id="FORMTABFinancialMapping" style="display: none;">

            <tr>
                <td class="ctrlgroup" colspan="3"><asp:Label ID="lblFinField1" runat="server" Text="<%$ Resources:lblFinField1 %>"></asp:Label><%--DCI requires Financials Amount Reporting.--%><br />
                    <asp:Label ID="lblFinField2" runat="server" Text="<%$ Resources:lblFinField2 %>"></asp:Label><%--Amounts to be reported will be calcualated based on Transaction Type. A DCI field can be mapped to different Reserves and Transaction types.--%>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td class="ctrlgroup" colspan="3"><asp:Label ID="lblDCIFinFieldTabHead" runat="server" Text="<%$ Resources:lblDCIFinFieldMapping %>"></asp:Label><%--DCI Financial Fields Mapping--%></td>
            </tr>
            <tr>
                <td width="38%" align="left"><asp:Label ID="lblColFinField" runat="server" Text="<%$ Resources:lblColFinField %>"></asp:Label><%--DCI Financial Fields--%></td>
                <td width="32%" align="left"><asp:Label ID="lblColFinFieldReserveType" runat="server" Text="<%$ Resources:lblColFinFieldReserveType %>"></asp:Label><%--Reserve Type--%></td>
                <td width="30%" align="left"><asp:Label ID="lblColFinFieldTransactionType" runat="server" Text="<%$ Resources:lblColFinFieldTransactionType %>"></asp:Label><%--Transaction Type--%></td>
            </tr>

            <tr>
                <td align="left">
                    <asp:dropdownlist type="combobox" runat="server" id="lstDCIFields" rmxref="Instance/Document/form/group/displaycolumn/control[@name='DCIFields']" itemsetref="Instance/Document/form/group/displaycolumn/control[@name='DCIFields']" >
                   </asp:dropdownlist>
                </td>
                <td align="left">
                    <asp:dropdownlist type="combobox" runat="server" id="lstReserveType" rmxref="Instance/Document/form/group/displaycolumn/control[@name='ReserveType']" itemsetref="Instance/Document/form/group/displaycolumn/control[@name='ReserveType']" onselectedindexchanged="lstReserveType_SelectedIndexChanged" autopostback="true">
                   </asp:dropdownlist>
                </td>
                <td align="left">
                    <asp:dropdownlist type="combobox" runat="server" id="lstTransactionType" rmxref="Instance/Document/form/group/displaycolumn/control[@name='TransactionType']" itemsetref="Instance/Document/form/group/displaycolumn/control[@name='TransactionType']" enabled="false">
                   </asp:dropdownlist>
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="div1" style="height:120px;overflow-x:hidden;overflow-y:auto;" class="divScroll">
                                <asp:gridview width="100%" runat="server" id="GdDCIFieldsMapping" autogeneratecolumns="false" alternatingrowstyle-cssclass="rowdark">
                       <Columns>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="colheader5" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <input type="checkbox" name="selFieldsMapping" id="selFieldsMapping" value='<%#Eval("CheckValue")%>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:lblColFinField %>" ItemStyle-Width="33%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="FieldsName"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:lblColFinFieldReserveType %>" ItemStyle-Width="33%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="ReserveName"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:lblColFinFieldTransactionType %>" ItemStyle-Width="33%" HeaderStyle-CssClass="colheader5" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="DCITransName"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       </Columns>
                       <EmptyDataTemplate>
                            <asp:Label runat="server" Text="" Width="5%" ID="Label3" CssClass="colheader5"></asp:Label>
                            <asp:Label runat="server" Text="<%$ Resources:lblColFinField %>" Width="33%" ID="headerFields" CssClass="colheader5"></asp:Label>
                            <asp:Label runat="server" Text="<%$ Resources:lblColFinFieldReserveType %>" Width="33%" ID="headerReserve" CssClass="colheader5"></asp:Label>
                            <asp:Label runat="server" Text="<%$ Resources:lblColFinFieldTransactionType %>" Width="33%" ID="Label4" CssClass="colheader5"></asp:Label>
                       </EmptyDataTemplate> 
                      </asp:gridview>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:textbox type="radiolist" runat="server" id="DCIFieldsList" style="display: none" rmxref="Instance/Document/form/group/displaycolumn/control[@name='DCIFieldsList']"></asp:textbox>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:textbox style="display: none" runat="server" type="hidden" id="hdnFields"></asp:textbox>
                    <asp:textbox style="display: none" runat="server" type="hidden" id="hdnSelectedFields"></asp:textbox>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" width="99%" align="center">
                    <asp:button runat="server" id="btnSaveDCIFields" type="submit" text="<%$ Resources:btnSaveDCIFinField %>" class="button" onclientclick="return CheckDCIFieldExist();" onclick="SaveDCIFieldsMapping" />
                    <asp:button runat="server" id="btnDeleteDCIFields" type="submit" text="<%$ Resources:btnDeleteDCIFinField %>" class="button" onclientclick="return DeleteDCIFieldsMapping();" onclick="DeleteDCIFieldsMapping" />
                </td>
            </tr>
            <%--<tr>
                <td colspan="3">&nbsp;</td>
            </tr>--%>
        </table>
        </div>
    </form>
</body>
</html>
