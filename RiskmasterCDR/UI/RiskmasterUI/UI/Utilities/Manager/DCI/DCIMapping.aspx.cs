﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

//Neha GOEL: Added the file for DCI -- MITS# 36997 - 08302014 - RMA-5497
namespace Riskmaster.UI.UI.Utilities.Manager
{
    ///************************************************************** 
    ///* $File		: DCIMapping.aspx.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 30-August-2014
    ///* $Author	: Neha Goel/Ankit Gupta
    ///* $Comment	: MITS# 36997 GAP 06  - RMA-5497
    ///* $Source	: 
    ///**************************************************************

    public partial class DCIMapping : NonFDMBasePageCWS
    {
        private bool bReturnStatus;
        private string sCWSresponse = string.Empty;
        private DataTable dtGridData = null;
        private XmlDocument oFDMPageDom = null;
        private XmlNode BenefitList = null;
        private XmlNode DCIFieldsListValues = null;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CallOnPageLoad();
            }
        }

        /// <summary>
        /// Saves the benefit mapping.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void SaveBenefitMapping(object sender, EventArgs e)
        {
            try
            {

                XElement oMessageElement = GetMessageTemplate("Benefit");
                XElement oTempElement = null;

                oTempElement = oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='DCIBenefits']");
                if(oTempElement != null)
                    oTempElement.Value = lstBenefits.SelectedValue;

                oTempElement = oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='TransactionTypeBenefitID']");
                if (oTempElement != null)
                    oTempElement.Value = TransactionTypeBenefits.CodeId;
                
                oTempElement = oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='TransactionTypeBenefitText']");
                if (oTempElement != null)
                    oTempElement.Value = TransactionTypeBenefits.CodeText;
                
                oTempElement = oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='BenefitAmount']");
                if (oTempElement != null)
                    oTempElement.Value = BenefitAmount.Text;

                bReturnStatus = CallCWS("DCIMappingAdaptor.SaveBenefits", oMessageElement, out sCWSresponse, false, false);

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            // need to call the same functions as on page load to display changes in grid

            if (bReturnStatus)
                CallOnPageLoad();

        }

        /// <summary>
        /// Deletes the benefit mapping.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void DeleteBenefitMapping(object sender, EventArgs e)
        {
            try
            {
                XElement oMessageElement = GetMessageTemplate("Benefit");
                XElement oTempElement = null;
                oTempElement = oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='DCIBenefitsList']");
                if (oTempElement != null)
                    oTempElement.Value = this.hdnSelectedBenefits.Text;
                bReturnStatus = CallCWS("DCIMappingAdaptor.DeleteBenefitsMapping", oMessageElement, out sCWSresponse, false, false);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

            if (bReturnStatus)
                CallOnPageLoad();
        }

        /// <summary>
        /// Saves the dci fields mapping.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void SaveDCIFieldsMapping(object sender, EventArgs e)
        {
            try
            {
                XElement oMessageElement = GetMessageTemplate("Financial");
                XElement oTempElement = null;
                oTempElement = oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='DCIFields']");
                if (oTempElement != null)
                    oTempElement.Value = lstDCIFields.SelectedValue;
                oTempElement = oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='ReserveTypeID']");
                if (oTempElement != null)
                    oTempElement.Value = lstReserveType.SelectedValue;
                oTempElement = oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='TransactionTypeID']");
                if (oTempElement != null)
                    oTempElement.Value = lstTransactionType.SelectedValue;

                bReturnStatus = CallCWS("DCIMappingAdaptor.SaveDCIFieldsTransMapping", oMessageElement, out sCWSresponse, false, false);

                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    lstTransactionType.ClearSelection();
                    lstTransactionType.Enabled = false;
                    CallOnPageLoad();
                }
                else 
                {
                    ErrorControl1.errorDom = sCWSresponse;
                }
                ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('FinancialMapping')};", true);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Deletes the dci fields mapping.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void DeleteDCIFieldsMapping(object sender, EventArgs e)
        {
            try
            {
                XElement oMessageElement = GetMessageTemplate("Financial");
                XElement oTempElement = null;
                oTempElement = oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='DCIFieldsList']");
                if (oTempElement != null)
                    oTempElement.Value = this.hdnSelectedFields.Text;
                bReturnStatus = CallCWS("DCIMappingAdaptor.DeleteDCIFieldsTransMapping", oMessageElement, out sCWSresponse, false, false);
                lstTransactionType.ClearSelection();
                lstTransactionType.Enabled = false;
                ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('FinancialMapping')};", true);

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

            if (bReturnStatus)
                CallOnPageLoad();
        }

        /// <summary>
        /// Gets the message template.
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate(string sType)
        {
            XElement oTemplate = null;

            if (sType.Equals("Benefit", StringComparison.OrdinalIgnoreCase))
            {
                oTemplate = XElement.Parse(@"
                <Message>
                  <Authorization></Authorization> 
                  <Call>
                   <Function></Function> 
                  </Call>
                  <Document>
                    <FundsMapping>
                        <control name='DCIBenefits'></control> 
                        <control name='TransactionTypeBenefitID'></control>
                        <control name='TransactionTypeBenefitText'></control>
                        <control name='BenefitAmount'></control>
                        <control name='DCIBenefitsList' rmxignoreget='true' rmxignoreset='true'></control> 
                        <control name='flagInputXML'>1</control> 
                    </FundsMapping>
                  </Document>
               </Message>
                ");
            }
            else if (sType.Equals("Financial", StringComparison.OrdinalIgnoreCase))
            {
                oTemplate = XElement.Parse(@"
                <Message>
                  <Authorization></Authorization> 
                  <Call>
                   <Function></Function> 
                  </Call>
                  <Document>
                    <FundsMapping>
                        <control name='DCIFields'></control> 
                        <control name='ReserveTypeID'></control>
                        <control name='ReserveType'></control>
                        <control name='TransactionTypeID'></control>
                        <control name='TransactionType'></control>                    
                        <control name='DCIFieldsList' rmxignoreget='true' rmxignoreset='true'></control> 
                        <control name='flagInputXML'>1</control>                     
                    </FundsMapping>
                  </Document>
               </Message>
                ");
            }

            return oTemplate;
        }

        /// <summary>
        /// Modifies the XML.
        /// </summary>
        /// <param name="Xelement">The xelement.</param>
        public override void ModifyXml(ref XElement Xelement)
        {
            XElement objNewElm = null;
            XElement oTempElement = null;
            if (Xelement.XPathSelectElement("./Document/FundsMapping/control[@name='flagInputXML']") != null && Xelement.XPathSelectElement("./Document/FundsMapping/control[@name='flagInputXML']").Value == "1")
            {
            }
            else
            {
                //Clearing the text of TransactionTypeBenefits user control's textbox value and its hidden field value.
                oTempElement = Xelement.XPathSelectElement("//control[@name='TransactionTypeBenefits']");
                if (oTempElement != null)
                    oTempElement.Value = string.Empty;

                oTempElement = Xelement.XPathSelectElement("//control[@name='TransactionTypeBenefits']");
                if (oTempElement != null)
                    oTempElement.SetAttributeValue("codeid", string.Empty);

                oTempElement = Xelement.XPathSelectElement("//control[@name='DCIBenefitsList']");
                if (oTempElement != null)
                    oTempElement.Value = string.Empty;

                XElement DCIBenefit = Xelement.XPathSelectElement("//control[@name='DCIBenefitsList']");

                if (DCIBenefit != null)
                {
                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "BenefitsRowId");
                    objNewElm.Value = "Row Id";
                    DCIBenefit.Add(objNewElm);

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "DCIBenefitType");
                    objNewElm.Value = "DCI Benefit Type";
                    DCIBenefit.Add(objNewElm);

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "DCITransactionType");
                    objNewElm.Value = "DCI Transaction Type";
                    DCIBenefit.Add(objNewElm);

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "DCIWeeklyAmount");
                    objNewElm.Value = "DCI Weekly Amount";
                    DCIBenefit.Add(objNewElm);
                }

                oTempElement = Xelement.XPathSelectElement("//control[@name='DCIFieldsList']");
                if (oTempElement != null)
                    oTempElement.Value = string.Empty;

                XElement DCIFund = Xelement.XPathSelectElement("//control[@name='DCIFieldsList']");
                if (DCIFund != null)
                {
                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "DCIFieldsRowId");
                    objNewElm.Value = "Row Id";
                    DCIFund.Add(objNewElm);

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "DCIFieldId");
                    objNewElm.Value = "DCI Field Id";
                    DCIFund.Add(objNewElm);

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "DCIReserveType");
                    objNewElm.Value = "DCI Reserve Type";
                    DCIFund.Add(objNewElm);

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "DCITransactionType");
                    objNewElm.Value = "DCI Transaction Type";
                    DCIFund.Add(objNewElm);
                }
            }
        }

        /// <summary>
        /// Binds the grid to data.
        /// </summary>
        protected void BindGridToData()
        {
            int lCount = 1;
            string checkValue = string.Empty;
            string benefitName = string.Empty;
            string transactionName = string.Empty;
            string benefitAmount = string.Empty;

            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            dtGridData = new DataTable();

            dtGridData.Columns.Add("CheckValue");
            dtGridData.Columns.Add("benefitName");
            dtGridData.Columns.Add("transactionName");
            dtGridData.Columns.Add("benefitAmount");

            DataRow objRow = null;

            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("Document/form/group/displaycolumn/control[@name='DCIBenefitsList']/listrow");

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();

                lCount = 1;

                XmlNodeList xmlChildElements = objNodes.ChildNodes;

                foreach (XmlNode objChildNode in xmlChildElements)
                {
                    switch (lCount)
                    {
                        case 1:
                            checkValue = objChildNode.Attributes["value"].Value.ToString();
                            break;
                        case 2:
                            benefitName = objChildNode.Attributes["title"].Value.ToString();
                            break;
                        case 3:
                            transactionName = objChildNode.Attributes["title"].Value.ToString();
                            break;
                        case 4:
                            benefitAmount = objChildNode.Attributes["title"].Value.ToString();
                            break;
                    }
                    lCount = lCount + 1;
                }
                objRow["CheckValue"] = checkValue;
                objRow["benefitName"] = benefitName;
                objRow["transactionName"] = transactionName;
                objRow["benefitAmount"] = benefitAmount;
                dtGridData.Rows.Add(objRow);
            }

            GdDCIBenefitMapping.DataSource = dtGridData;
            GdDCIBenefitMapping.DataBind();
            BeforePrerender("1");

            // To show the headers if the there is no data in the grid
            if (dtGridData.Rows.Count == 0)
            {
                dtGridData.Rows.Add(dtGridData.NewRow());
                GdDCIBenefitMapping.DataSource = dtGridData;
                GdDCIBenefitMapping.DataBind();
                GdDCIBenefitMapping.Rows[0].Visible = false;
            }
            dtGridData.Dispose();

            BindFinancialGridData();
        }

        /// <summary>
        /// Binds the financial grid data.
        /// </summary>
        protected void BindFinancialGridData()
        {
            int lCount = 1;
            string checkValue = string.Empty;
            string transactionName = string.Empty;
            string fieldName = string.Empty;
            string reserveName = string.Empty;

            checkValue = string.Empty;

            dtGridData = new DataTable();

            dtGridData.Columns.Add("CheckValue");
            dtGridData.Columns.Add("fieldName");
            dtGridData.Columns.Add("reserveName");
            dtGridData.Columns.Add("DCITransName");

            DataRow objRow = null;
            
            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("Document/form/group/displaycolumn/control[@name='DCIFieldsList']/listrow");

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();

                lCount = 1;

                XmlNodeList xmlChildElements = objNodes.ChildNodes;

                foreach (XmlNode objChildNode in xmlChildElements)
                {
                    switch (lCount)
                    {
                        case 1:
                            checkValue = objChildNode.Attributes["value"].Value.ToString();
                            break;
                        case 2:
                            fieldName = objChildNode.Attributes["title"].Value.ToString();
                            break;
                        case 3:
                            reserveName = objChildNode.Attributes["title"].Value.ToString();
                            break;
                        case 4:
                            transactionName = objChildNode.Attributes["title"].Value.ToString();
                            break;
                    }
                    lCount = lCount + 1;
                }
                objRow["CheckValue"] = checkValue;
                objRow["fieldName"] = fieldName;
                objRow["reserveName"] = reserveName;
                objRow["DCITransName"] = transactionName;
                dtGridData.Rows.Add(objRow);
            }

            GdDCIFieldsMapping.DataSource = dtGridData;
            GdDCIFieldsMapping.DataBind();
            BeforePrerender("2");

            // To show the headers if the there is no data in the grid
            if (dtGridData.Rows.Count == 0)
            {
                dtGridData.Rows.Add(dtGridData.NewRow());
                GdDCIFieldsMapping.DataSource = dtGridData;
                GdDCIFieldsMapping.DataBind();
                GdDCIFieldsMapping.Rows[0].Visible = false;
            }
            dtGridData.Dispose();
        }

        /// <summary>
        /// Befores the prerender.
        /// </summary>
        /// <param name="GdValue">The gd value.</param>
        protected void BeforePrerender(string GdValue)
        {
            if (dtGridData != null)
            {

                if (dtGridData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        if (GdValue == "1")
                        {
                            Label benefitName = ((Label)(GdDCIBenefitMapping.Rows[i].FindControl("benefitName")));
                            benefitName.Text = dtGridData.Rows[i]["benefitName"].ToString();

                            Label transactionName = ((Label)(GdDCIBenefitMapping.Rows[i].FindControl("transactionName")));
                            transactionName.Text = dtGridData.Rows[i]["transactionName"].ToString();

                            Label benefitAmount = ((Label)(GdDCIBenefitMapping.Rows[i].FindControl("benefitAmount")));
                            benefitAmount.Text = dtGridData.Rows[i]["benefitAmount"].ToString();
                        }
                        else if (GdValue == "2")
                        {
                            Label FieldsName = ((Label)(GdDCIFieldsMapping.Rows[i].FindControl("FieldsName")));
                            FieldsName.Text = dtGridData.Rows[i]["fieldName"].ToString();

                            Label ReserveType = ((Label)(GdDCIFieldsMapping.Rows[i].FindControl("ReserveName")));
                            ReserveType.Text = dtGridData.Rows[i]["reserveName"].ToString();

                            Label TransactionType = ((Label)(GdDCIFieldsMapping.Rows[i].FindControl("DCITransName")));
                            TransactionType.Text = dtGridData.Rows[i]["DCITransName"].ToString();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Calls the on page load.
        /// </summary>
        protected void CallOnPageLoad()
        {
            try
            {
                bReturnStatus = CallCWS("DCIMappingAdaptor.Get", null, out sCWSresponse, true, true);

                if (bReturnStatus)
                {
                    BindGridToData();
                }
                // setting the option value="" for first value of dropdowns
                if (this.lstBenefits.Items.Count > 0)
                    this.lstBenefits.Items[0].Value = string.Empty;

                BenefitAmount.Text = string.Empty;

                BenefitList = Data.SelectSingleNode("//BenefitListValues");
                if (BenefitList != null)
                {
                    this.hdnBenefits.Text = BenefitList.InnerText.ToString();
                }


                if (this.lstDCIFields.Items.Count > 0)
                    this.lstDCIFields.Items[0].Value = string.Empty;

                if (this.lstTransactionType.Items.Count > 0)
                    this.lstTransactionType.Items[0].Value = string.Empty;

                if (this.lstReserveType.Items.Count > 0)
                    this.lstReserveType.Items[0].Value = string.Empty;

                DCIFieldsListValues = Data.SelectSingleNode("//DCIFieldsListValues");
                if (DCIFieldsListValues != null)
                {
                    this.hdnFields.Text = DCIFieldsListValues.InnerText.ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the lstBenefits control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void lstBenefits_SelectedIndexChanged(object sender, EventArgs e)
        {
            XmlNode objNode = null;
            try
            {
                if (!string.IsNullOrWhiteSpace(lstBenefits.SelectedValue))
                {
                    bReturnStatus = CallCWS("DCIMappingAdaptor.GetBenefitAmount", null, out sCWSresponse, true, false);

                    if (bReturnStatus)
                    {
                        oFDMPageDom = new XmlDocument();
                        oFDMPageDom = Data;
                        objNode = oFDMPageDom.SelectSingleNode("//control[@name='BenefitAmount']");
                        if (objNode != null && objNode.InnerText != null)
                        {
                            BenefitAmount.Text = objNode.InnerText;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the lstReserveType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void lstReserveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(lstReserveType.SelectedValue) > 0)
                {
                    bReturnStatus = CallCWS("DCIMappingAdaptor.GetDCIFieldsTransMapping", null, out sCWSresponse, true, false);

                    if (bReturnStatus)
                    {
                        oFDMPageDom = new XmlDocument();
                        oFDMPageDom = Data;
                        lstTransactionType.Enabled = true;

                        dtGridData = new DataTable();
                        dtGridData.Columns.Add("Text");
                        dtGridData.Columns.Add("Value");

                        DataRow objRow = null;
                        XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("Document/form/group/displaycolumn/control[@name='TransactionType']/option");

                        foreach (XmlNode objNode in xmlNodeList)
                        {
                            objRow = dtGridData.NewRow();
                            objRow["Text"] = objNode.InnerText;
                            objRow["Value"] = objNode.Attributes["value"].Value.ToString();
                            dtGridData.Rows.Add(objRow);
                        }
                        lstTransactionType.DataTextField = dtGridData.Columns["Text"].ToString();
                        lstTransactionType.DataValueField = dtGridData.Columns["Value"].ToString();
                        lstTransactionType.DataSource = dtGridData;
                        lstTransactionType.DataBind();
                        dtGridData.Dispose();
                    }
                    else 
                    {
                        lstTransactionType.ClearSelection();
                        lstTransactionType.Enabled = false;
                    }

                    ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('FinancialMapping')};", true);
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
