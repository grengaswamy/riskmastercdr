﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Xml.Linq;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class ClaimActivityLogSetup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadActLogConfig();
        }

        private void LoadActLogConfig()
        {
            XmlNode inputDocNode = null;
            string strCWSSrvcOutput = string.Empty;
            XmlDocument xmlOutDoc = new XmlDocument();
            DataTable dtGridData = new DataTable();

            inputDocNode = InputXMLDoc("ClaimActLogAdaptor.GetConfig");
            strCWSSrvcOutput = AppHelper.CallCWSService(inputDocNode.InnerXml);

            xmlOutDoc.LoadXml(strCWSSrvcOutput);

            GridHeaderAndData(xmlOutDoc.SelectSingleNode("ResultMessage/Document/PassToWebService/ClaimActSetupLists/listhead"), xmlOutDoc.SelectNodes("ResultMessage/Document/PassToWebService/ClaimActSetupLists/row"), dtGridData);

            GridView1.DataSource = dtGridData;
            GridView1.DataBind();
        }

        private XmlNode InputXMLDoc(string p_sFunctionToCall)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
                <Message>
                <Authorization></Authorization> 
                <Call>
                <Function>" + p_sFunctionToCall + @"</Function> 
                </Call>
                <Document>
                    <PassToWebService>
                        <ClaimActSetupLists>
                        <listhead>
                        <LogId>Log ID</LogId>
                        <Table type='table'>Table Name</Table>
                        <ActType type='code'>Activity Type</ActType> 
                        <OpType type='code'>Operation</OpType>
                        <LogText>Log Text</LogText> 
                        </listhead>
                        </ClaimActSetupLists>
                    </PassToWebService>
                </Document>
                </Message>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectGridRow('" + DataBinder.Eval(e.Row.DataItem, "LogId").ToString() + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }

        private void GridHeaderAndData(XmlNode objGridHeaders, XmlNodeList objGridData, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;


            if (objGridHeaders != null && objGridHeaders.InnerXml != "")
            {
                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    //Add Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();
                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.Name;

                    dtGridData.Columns.Add(dcGridColumn);

                }
            }
            //Add rows to be associated with Datasource of grid
            if (objGridData != null && objGridData.Count != 0)
            {
                foreach (XmlNode objNodes in objGridData)
                {
                    dr = dtGridData.NewRow();

                    iColumnIndex = 0;
                    foreach (XmlElement objElem in objNodes)
                    {
                        dr[iColumnIndex++] = objElem.InnerXml;
                    }
                    dtGridData.Rows.Add(dr);

                }
            }
            else
            {
                dr = dtGridData.NewRow();
                dtGridData.Rows.Add(dr);
            }



        }
        protected void Delete_ActLogSteupGrid_Click(object sender, ImageClickEventArgs e)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
                        <Message>
                        <Authorization></Authorization> 
                        <Call>
                        <Function>ClaimActLogAdaptor.Delete</Function> 
                        </Call>
                        <Document>
                        <control name='LogId'>" + hdnLogId.Value + @"</control> 
                        </Document>
                        </Message>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }
            AppHelper.CallCWSService(xmlNodeDoc.InnerXml);
            LoadActLogConfig();
        }
    }
}