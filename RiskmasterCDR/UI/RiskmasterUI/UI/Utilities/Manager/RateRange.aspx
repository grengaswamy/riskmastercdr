﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RateRange.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.Manager.RateRange" %>
 <%@ Register TagPrefix="uc4" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
 <%--<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <%-- removed "..." from title bar Neha Suresh Jain, 04/27/2010, MITS:20471--%>
    <%--<title>Add/Modify Range...</title>--%>
    <title>Add/Modify Range</title>
  
    <link rel="stylesheet" href="/RiskmasterUI/Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/system.css" type="text/css" />
    <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
    </style>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>
    <script language ="javascript" type="text/javascript" >
    
     function ShowHideRangeCtl(radioname, FirstList,SecondList,sMode) {
            var radio = document.getElementById(radioname);
            var CtlFirstlist = FirstList.split(',');
            var CtlSecondlist = SecondList.split(',');
            var Ctl=null;
            
            if (radio.checked == true) 
            {
                for(i=0;i<CtlFirstlist.length;i++)
                {
                    Ctl =document.getElementById(CtlFirstlist[i]);
                    Ctl.style.display = '';
                }
                for(i=0;i<CtlSecondlist.length;i++)
                {
                    Ctl =document.getElementById(CtlSecondlist[i]);
                    Ctl.style.display = 'none';
                }
            }
            else 
            {
                for(i=0;i<CtlFirstlist.length;i++)
                {
                    Ctl =document.getElementById(CtlFirstlist[i]);
                    Ctl.style.display = 'none';
                }
                for(i=0;i<CtlSecondlist.length;i++)
                {
                    Ctl =document.getElementById(CtlSecondlist[i]);
                    Ctl.style.display = '';
                }
            }
             //Changes Made by Neha Suresh Jain, 03/12/2010
             if((sMode !="edit" ) && (document.getElementById("mode").value != "edit" 
             && document.getElementById("rangeType").value == radio.defaultValue || (document.getElementById("Count").value == 1)))
             {
                Ctl =document.getElementById("Modifier");
                Ctl.value="";
                Ctl =document.getElementById("LowerBound");
                Ctl.value="";
                Ctl =document.getElementById("UpperBound");
                Ctl.value="";
             }
            return true;
        }
    
    function RateRangeDisplay()
    {
        ShowHideRangeCtl('Deductible_Radio','lbl_value','lbl_LowerBound,lbl_UpperBound,UpperBound',"edit");
        ShowHideRangeCtl('Range_Radio','lbl_LowerBound,lbl_UpperBound,UpperBound','lbl_value',"edit");
        return true;
    }
    </script> 

</head>
<body class="10pt" onload="CheckDiscountRangeWindow();RateRangeDisplay()">
    <form id="frmData" runat="server"   method="post">
     <table>
        <tr>
            <td colspan="2">
                <uc4:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
    <div class="toolbardrift" id="toolbardrift" name="toolbardrift">
        <table class="toolbar" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td align="center" valign="middle" height="32">
                        <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="ImageButton1" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                            onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" OnClientClick="return ValidateFieldsforCoverageRange();"
                            OnClick="save_Click" TabIndex="5" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    <br>
    <div class="msgheader" id="formtitle">
        Add/Modify Range...</div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Range Parameters
                                </td>
                            </tr>
                            <tr>
                                <asp:TextBox runat="server" Style="display: none" ID="RowId"></asp:TextBox>
                            </tr>
                            <tr>
                            <td colspan="2">
                                <asp:RadioButton GroupName="optRangegroup" Text="Deductible" value="Deductible" ID="Deductible_Radio"
                                      Checked="true"
                                       onclick="(ValidateFieldsforCoverage(), ShowHideRangeCtl('Deductible_Radio','lbl_value','lbl_LowerBound,lbl_UpperBound,UpperBound'));"
                                      runat="server" TabIndex="0" />
                                <asp:RadioButton onclick="(ValidateFieldsforCoverage(), ShowHideRangeCtl('Range_Radio','lbl_LowerBound,lbl_UpperBound,UpperBound','lbl_value'));"
                                      GroupName="optRangegroup" Text="Range" value="Range" ID="Range_Radio"
                                      runat="server" TabIndex="1"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_Amount" Text="Modifier:" class="required" />&nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" 
                                        ID="Modifier" MaxLength="11"
                                        onchange="setDataChanged(true);" TabIndex="2"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_LowerBound" Text="Lower Bound:" class="required" Style="display: none;"/>
                                    <asp:Label runat="server" ID="lbl_value" Text="Deductible:" class="required"  />&nbsp;&nbsp;
                                </td>
                                <td>
                               <%-- Start:Nitin Goel,02/08/2010 MITS :18231--%>
                                   <%-- <asp:TextBox  MaxLength="11" runat="server" size="30"
                                        ID="LowerBound" onchange="setDataChanged(true);"  ></asp:TextBox>--%>
                                         <asp:TextBox  MaxLength="11" runat="server" size="30" onblur="numLostFocus(this);"
                                        ID="LowerBound" onchange="setDataChanged(true);" TabIndex="3"  ></asp:TextBox>
                               <%-- End:Nitin Goel,02/08/2010 MITS :18231--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_UpperBound" Text="Upper Bound:" 
                                        Style="display: none;"/>&nbsp;&nbsp;
                                </td>
                                <td>
                                <%-- Start:Nitin Goel,02/08/2010 MITS :18231--%>
                                    <%--<asp:TextBox runat="server" size="30" ID="UpperBound" MaxLength="11" rmxforms="currency"
                                         onchange="setDataChanged(true);"  Style="display: none;"></asp:TextBox>--%>
                                         <asp:TextBox runat="server" size="30" ID="UpperBound" MaxLength="11" rmxforms="currency"
                                         onblur="numLostFocus(this);" onchange="setDataChanged(true);"  Style="display: none;" TabIndex="4"></asp:TextBox>
                                <%-- End:Nitin Goel,02/08/2010 MITS :18231--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Style="display: none" ID="FormMode"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Style="display: none" ID="FormValid"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                     <asp:TextBox Style="display: none" runat="server" ID="gridname"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="mode"></asp:TextBox>
                    <%--Start: added by Neha Suresh Jain, 03/12/2010--%>
                    <asp:TextBox Style="display: none" runat="server" ID="Count"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="rangeType"></asp:TextBox>
                    <%--End: Neha Suresh Jain--%>
                    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtvalidate" ></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtData" ></asp:TextBox>
   <%--                 <input type="text" name="" value="" id="SysViewType" style="display: none"><input
                        type="text" name="" value="" id="SysCmd" style="display: none"><input type="text"
                            name="" value="" id="SysCmdConfirmSave" style="display: none"><input type="text"
                                name="" value="" id="SysCmdQueue" style="display: none"><input type="text" name=""
                                    value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate"><input type="text"
                                        name="" value="" id="SysClassName" style="display: none" rmxforms:value=""><input
                                            type="text" name="" value="" id="SysSerializationConfig" style="display: none"><input
                                                type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId"><input
                                                    type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId"><input
                                                        type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="DiscountRange"><input
                                                            type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value=""><input
                                                                type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="DiscountRange"><input
                                                                    type="text" name="" value="LowerBound|Amount|" id="SysRequired" style="display: none">--%>
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
   <%-- <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />--%>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-5" id="SysWindowId" />
   
    </form>
</body>
</html>
