﻿
/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
* 06/04/2014 | 33371   | ajohari2   | Added new page for Policy search data setup 
**********************************************************************************************/

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.BusinessHelpers;
using System.IO;
using System.ServiceModel;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class PolicySearchDataSetup : NonFDMBasePageCWS
    {
        //MITS 33371 ajohari2: Start
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();

        string sRowId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    selectedid.Text = AppHelper.GetQueryStringValue("selectedid");
                    sRowId = selectedid.Text;

                    PopulateControls();
                }
                else
                {
                    sRowId = selectedid.Text;

                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("PolicySearchDataSetupAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);
                        XmlDocument objXml = new XmlDocument();
                        objXml.LoadXml(sCWSresponse);

                        string policySystemId = Convert.ToString(objXml.SelectSingleNode("//control[@name='PolSys']").InnerText);
                        string secGroupId = Convert.ToString(objXml.SelectSingleNode("//control[@name='SecGroup']").InnerText);

                        PolSys.SelectedValue = policySystemId;
                        SecGroup.SelectedValue = getValue(secGroupId, 5);
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void PopulateControls()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            PolicySystemList oResponse = null;
            DataSet oDS = new DataSet();
            DataSet oDSOrgSet = new DataSet();
            string[] sList;
            string sValue = string.Empty;
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            try
            {
                //Preparing XML to send to service
                XElement oMessageElement = LoadOrgSetTemplate();
                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                if (oFDMPageDom != null)
                {
                    oDSOrgSet.ReadXml(new StringReader(oFDMPageDom.InnerXml));
                    SecGroup.DataSource = oDSOrgSet.Tables[3];
                    SecGroup.DataTextField = "GroupName";
                    SecGroup.DataValueField = "RowId";
                    SecGroup.DataBind();
                }

                oResponse = objHelper.GetPolicySystemList();
                if (oResponse != null && !string.IsNullOrEmpty(oResponse.ResponseXML))
                {
                    oDS.ReadXml(new StringReader(oResponse.ResponseXML));
                    ListItem item = null;
                    //JIRA RMA-10571 ajohari2: Start
                    for (int i = 0; i < oDS.Tables["option"].Rows.Count; i++)
                    {
                        item = new ListItem();
                        item.Text = oDS.Tables["option"].Rows[i]["option_text"].ToString();
                        item.Value = oDS.Tables["option"].Rows[i]["value"].ToString();
                        PolSys.Items.Add(item);
                    }
                    //JIRA RMA-10571 ajohari2: End
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oResponse != null)
                    oResponse = null;
            }

        }

        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            ErrorControl1.errorDom = p_sReturn;
            Exception ex = new Exception("Service Error");
            if (ErrorControl1.errorFlag == true)
                throw ex;
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }

        /// <summary>
        /// CWS request message template for Org Set
        /// </summary>
        /// <returns></returns>
        private XElement LoadOrgSetTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>ORGSECAdaptor.LoadOrgSet</Function> 
                      </Call>
                      <Document>
                          <PassToWebService>
                              <ConfRec></ConfRec>
                              <OrgEntitySecurityList>
                                  <listhead>
                                    <GroupName>a</GroupName>
                                    <GroupStatus>Group Status</GroupStatus>
                                  </listhead>
                              </OrgEntitySecurityList>
                              <AdminUserID /> 
                              <AdminPwd /> 
                              <OracleRole /> 
                          </PassToWebService>
                          <RowId></RowId> 
                      </Document>
                </Message>
            ");
            return oTemplate;
        }


        protected void btnOk_Click(object sender, EventArgs e)
        {

            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("PolicySearchDataSetupAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {

            }

        }

        private XElement GetMessageTemplate()
        {
            string strPolSys = string.Empty;
            string strAgentCode = string.Empty;
            string strMCO = string.Empty;
            string strPCO = string.Empty;
            string strBusUnit = string.Empty;
            string strBusSeg = string.Empty;
            string strSecGroup = string.Empty;

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><PolicySearchDataMapping>");

            sXml = sXml.Append("<control name='PolSys' type='id'>");
            sXml = sXml.Append(PolSys.SelectedValue);
            sXml = sXml.Append("</control>");
            
            sXml = sXml.Append("<control name='AgentCode' type='text'>");
            sXml = sXml.Append(AgentCode.Text);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='MCO' type='text'>");
            sXml = sXml.Append(MCO.Text);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='PCO' type='text'>");
            sXml = sXml.Append(PCO.Text);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='BusUnit' type='text'>");
            sXml = sXml.Append(BusUnit.Text);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='BusSeg' type='text'>");
            sXml = sXml.Append(BusSeg.Text);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='SecGroup' type='id'>");
            sXml = sXml.Append(SecGroup.SelectedValue);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='RowId' type='id'>");
            sXml = sXml.Append(sRowId);
            sXml = sXml.Append("</control>");
            
            sXml = sXml.Append("</PolicySearchDataMapping></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }

        ///// <summary>
        ///// Used to add "0" if not having MaxCount
        ///// </summary>
        ///// <param name="fieldValue"></param>
        ///// <param name="maxCount"></param>
        ///// <param name="fieldname"></param>
        ///// <returns></returns>
        public string getValue(string fieldValue, int maxCount)
        {
            string tempFieldValue = "";
            int lengthOfFieldValue = fieldValue.Length;
            if (lengthOfFieldValue < maxCount)
            {
                int checkCount = 0;
                while (checkCount < (maxCount - lengthOfFieldValue))
                {
                    tempFieldValue += "0";
                    checkCount++;
                }
            }
            fieldValue = tempFieldValue + fieldValue;
            return fieldValue;
        }
        //MITS 33371 ajohari2: End
    }

}
