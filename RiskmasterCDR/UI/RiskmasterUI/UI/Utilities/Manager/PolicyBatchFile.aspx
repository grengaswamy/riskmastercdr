﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyBatchFile.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.Manager.PolicyBatchFile" EnableViewStateMac="false"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Policy System Web Settings</title>
    <link href="../../../Content/zpcal/themes/system.css" rel="stylesheet" type="text/css" />
    <link href="../../../App_Themes/RMX_Default/rmnet.css" rel="stylesheet" type="text/css" />
    <script src="../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../Scripts/WaitDialog.js" type="text/javascript"></script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server" method="post">
    <div>
        <table>
            <tr>
                <td colspan="2">
                    <uc:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <div class="msgheader" id="formtitle">
            Policy System Batch File Update
        </div>
        <div>
            <div>
                <table>
                    <tr>
                        <td>
                            Policy System Name
                        </td>
                        <td>
                            <asp:DropDownList ID="lstPolicySys" type="combobox" rmxref="/Instance/Document/PolicySystems/PolicyList" 
                                itemsetref="/Instance/Document/PolicySystems/PolicyList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstPolicySys_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="completerow" style="HEIGHT:400px; WIDTH:99.5%; OVERFLOW: auto">
                <dg:UserControlDataGrid runat="server" ID="TransDetailsrid" GridName="TransDetailsrid"
                    GridTitle="" Target="/Document/PolicySystems/TransactionList" Ref="/Instance/Document/form//control[@name='TransDetailsrid']"
                    Unique_Id="transid" ShowRadioButton="False" Width="99%" Height="30%" HideNodes="|tag|transid|"
                    ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="340" HideButtons="" 
                    Type="Grid"  />
                <asp:TextBox Style="display: none" runat="server" ID="TransDetailsridSelectedId"
                    RMXType="id" />
                <asp:TextBox Style="display: none" runat="server" ID="TransDetailsrid_RowDeletedFlag"
                    RMXType="id" Text="false" />
                <asp:TextBox Style="display: none" runat="server" ID="TransDetailsrid_Action" RMXType="id" />
                <asp:TextBox Style="display: none" runat="server" ID="TransDetailsrid_RowAddedFlag"
                    RMXType="id" Text="false" />
            </div>
            <asp:TextBox Style="display: none" runat="server" ID="HdnTransId" Text="" rmxref="Instance/Document/PolicySystems/TransSplitIds"/>
             <asp:TextBox Style="display: none" runat="server" ID="SelectedPolId" Text="" />
       
        </div>
        <div>
            <asp:Button ID="btnCreateFile" runat="server" Text="Create Batch File" CssClass="button" OnClick="btnCreateFile_Click" />
        </div>
    </form>
</body>
</html>
