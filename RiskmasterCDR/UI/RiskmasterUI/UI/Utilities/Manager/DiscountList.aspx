﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiscountList.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.DiscountList" %>

<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="/RiskmasterUI/Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/system.css" type="text/css" />
    <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
    </style>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/drift.js"></script>

</head>
<body class="10pt" onload="pageLoaded();">
    <form id="frmData" method="post" runat="server">
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" /><div class="msgheader"
        id="formtitle">
    </div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Select discount
                                    <asp:TextBox runat="server" ID="tb_LOB_Code" Style="display: none" RMXref="/Instance/Document/PassToWebService/LOBCode[ @name ='LobCode']"></asp:TextBox>
                                    <asp:TextBox runat="server" ID="tb_State_Code" Style="display: none" RMXref="/Instance/Document/PassToWebService/StateCode[ @name ='StateCode']"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="500px">
                                    <b></b>
                                    <table width="100%">
                                        <tr>
                                            <td width="80%">
                                                <div style="&#xa; width: 500px; height: 400px; &#xa; &#xa;">
                                                    <dg:UserControlDataGrid runat="server" ID="DiscountListGrid" GridName="DiscountListGrid"
                                                        HideButtons="New|Edit|Delete" Target="/Document/PassToWebService/DiscountList"
                                                        Ref="/Instance/Document/form//control[@name='DiscountListGrid']" Unique_Id="RowId"
                                                        Width="680px" Height="150px" HideNodes="|RowId|UseVolumeDiscount|" ShowHeader="True" LinkColumn="Discount Name"
                                                        PopupWidth="500px" PopupHeight="250px" Type="Grid" RowDataParam="listrow" />
                                                    <asp:TextBox ID="tbDiscountListGrid" Style="display: none" runat="server" rmxignoreset="true"
                                                        RMXref="/Instance/Document/PassToWebService/DiscountList[ @name ='txtDiscountListGrid']"></asp:TextBox>
                                                    <asp:TextBox ID="tbDiscountListDiscountName" Style="display: none" runat="server"
                                                        Text="Discount Name" RMXref="/Instance/Document/PassToWebService/DiscountList/listhead/DiscountName[ @name ='txtDiscountListGridDiscountName']"></asp:TextBox>
                                                    <asp:TextBox ID="tbDiscountListState" Style="display: none" runat="server" Text="State"
                                                        RMXref="/Instance/Document/PassToWebService/DiscountList/listhead/State[ @name ='txtDiscountListGridState']"></asp:TextBox>
                                                    <asp:TextBox type="date" ID="tbDiscountListEffectiveDate" Style="display: none" runat="server"
                                                        Text="Effective Date" RMXref="/Instance/Document/PassToWebService/DiscountList/listhead/EffectiveDate[ @name ='txtDiscountListGridEffectiveDate']"></asp:TextBox>
                                                    <asp:TextBox type="date" ID="tbDiscountListExpirationDate" Style="display: none" runat="server"
                                                        Text="Expiration Date" RMXref="/Instance/Document/PassToWebService/DiscountList/listhead/ExpirationDate[ @name ='txtDiscountListGridExpirationDate']"></asp:TextBox>
                                                    <asp:TextBox ID="tbDiscountListUseVolumeDiscount" Style="display: none" runat="server" Text="UseVolumeDiscount"
                                                        RMXref="/Instance/Document/PassToWebService/DiscountList/listhead/UseVolumeDiscount[ @name ='txtDiscountListGridUseVolumeDiscount']"></asp:TextBox>
                                                    <asp:TextBox ID="tbreserveGridRowID" Style="display: none" runat="server" Text="RowId"
                                                        RMXref="/Instance/Document/PassToWebService/DiscountList/listhead/RowId[ @name ='txtDiscountListGridRowId']"></asp:TextBox>
                                                    <asp:TextBox Style="display: none" runat="server" ID="DiscountListGrid_Action" Text="" />
                                                </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="text" name="" value="" id="SysViewType" style="display: none" />
                    <input type="text" name="" value="" id="SysCmd" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdConfirmSave" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdQueue" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate" />
                    <input type="text" name="" value="" id="SysClassName" style="display: none" rmxforms:value="" />
                    <input type="text" name="" value="" id="SysSerializationConfig" style="display: none" />
                    <input type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId" />
                    <input type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId" />
                    <input type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="DiscountList />
                    <input type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value="" />
                    <input type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="DiscountList" />
                    <input type="text" name="" value="" id="SysRequired" style="display: none" />
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-3" id="SysWindowId"></form>
</body>
</html>
