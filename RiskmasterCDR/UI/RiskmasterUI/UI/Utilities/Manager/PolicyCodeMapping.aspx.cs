﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Data.SqlClient;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessHelpers;
using Riskmaster.UI.PolicyInterfaceService;
using System.IO;
using System.ServiceModel;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class PolicyCodeMapping : NonFDMBasePageCWS
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] sSelCodeType;
            if (!IsPostBack)
            {
                //rupal:start
                GetPolicySystem();                
                /*
                string Data = "";
                bool bReturnStatus = false;                
                bReturnStatus = CallCWS("PolicyCodeMappingAdaptor.Get", null, out Data, true, true);
                if (bReturnStatus)
                {
                    ErrorControl1.errorDom = Data;
                    //BindGridData(Data);
                }*/
                GetCodeTypes();
                GetCodeDetails();
                sSelCodeType = lstCodeType.SelectedValue.Split('|');
                if(sSelCodeType != null && sSelCodeType.Length == 2)
                    if (sSelCodeType[1] == "1") //INCLUDE_CLAIM_TYPE=1 , so include claim type drop down
                    {
                        trClaimType.Style.Add("display", "");
                        GetClaimTypes();
                    }
                    else
                    {
                        trClaimType.Style.Add("display", "none");
                    }
                //rupal:end
            }
        }

        //rupal:start
        private void GetPolicySystem()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            PolicySystemList oResponse = null;
            DataSet oDS = new DataSet();
            try
            {
                oResponse = objHelper.GetPolicySystemList();
                if (oResponse != null && !string.IsNullOrEmpty(oResponse.ResponseXML))
                {
                    oDS.ReadXml(new StringReader(oResponse.ResponseXML));
                    ListItem item = null;
                    for (int i = 0; i < oDS.Tables[1].Rows.Count; i++)
                    {
                        item = new ListItem();
                        item.Text = oDS.Tables[1].Rows[i]["option_text"].ToString();
                        item.Value = oDS.Tables[1].Rows[i]["value"].ToString() + "_" + oDS.Tables[1].Rows[i]["policy_system_type"].ToString();
                        lstPolicySystem.Items.Add(item);
                    }                    
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oResponse != null)
                    oResponse = null;
            }

        }

        private void GetCodeTypes()
        {
            try
            {
                XElement XmlTemplate = null;
                string sPolCode = "";
                XmlTemplate = GetMessageTemplateForCodeTypes();
                string Data = "";
                bool bReturnStatus = false;
                string[] sSelCodeType;
                lstRMCodes.Visible = true;
                lstPolSysCodes.Visible = true;
                txtRMCode.Visible = true;
                txtPolSysCode.Visible = true;
                addSPolicybtn.Visible = true;
                removePolicybtn.Visible = true;
                //txtCodeTypeId.Text = lstCodeType.SelectedValue;
                sSelCodeType = lstCodeType.SelectedValue.Split('|');
                txtCodeTypeId.Text = sSelCodeType[0];
                bReturnStatus = CallCWS("PolicyCodeMappingAdaptor.Get", XmlTemplate, out Data, false, true);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        //rupal:end

        private XElement GetMessageTemplateForCodeDetails()
        {

            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;
            //rupal:start
            string sPolSystemID = string.Empty;
            string sPolSysTypeID = string.Empty;
            string sCodeType = "0";
            string []sSelCodeType;
            if (lstCodeType.Items.Count > 0)
            {
                sSelCodeType = lstCodeType.SelectedValue.Split('|');
                sCodeType = sSelCodeType[0];  // lstCodeType.SelectedValue;
            }
            GetPolicySystemSelectedValue(ref sPolSystemID, ref sPolSysTypeID);
            //rupal:end
            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>PolicyCodeMappingAdaptor.GetCodeDetails</Function></Call>");
            sXML.Append("<Document><PolicyCodeMapping>");
            sXML.Append("<SelectedCodeType>" + sCodeType + "</SelectedCodeType>");
            //rupal:start
            sXML.Append("<PolicySystemTypeCode>" + sPolSysTypeID + "</PolicySystemTypeCode>");
            //rupal:end
            // aaggarwal29: Code mapping changes start
            sXML.Append("<PolicySystemID>" + sPolSystemID + "</PolicySystemID>");
            //aaggarwal29: Code mapping changes end
            sXML.Append("</PolicyCodeMapping></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }

        //rupal:start
        private XElement GetMessageTemplateForCodeTypes()
        {
            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;
            string sPolSystemID = string.Empty;
            string sPolSysTypeID = string.Empty;
            GetPolicySystemSelectedValue(ref sPolSystemID, ref sPolSysTypeID);
            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>PolicyCodeMappingAdaptor.Get</Function></Call>");
            sXML.Append("<Document><PolicySystemTypeCode>");
            sXML.Append(sPolSysTypeID);
            sXML.Append("</PolicySystemTypeCode></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }

        private void GetPolicySystemSelectedValue(ref string sPolSystemID, ref string sPolSysTypeID)
        {
            if (lstPolicySystem.Items.Count > 0)
            {
                string[] sarrPolSys = lstPolicySystem.SelectedValue.Split('_');
                if (sarrPolSys.Length == 2)
                {
                    sPolSystemID = sarrPolSys[0];
                    sPolSysTypeID = sarrPolSys[1];
                }
            }
        }
        //rupal:end

        private XElement GetMessageTemplateForDeletion()
        {
            //            XElement oTemplate = XElement.Parse(@"
            //                                                    <Message>
            //                                                      <Authorization></Authorization> 
            //                                                     <Call>
            //                                                      <Function>PolicyCodeMappingAdaptor.DeleteCodeMapping</Function> 
            //                                                      </Call>
            //                                                        <Document>
            //                                                            <PolicyCodeMapping>
            //                                                                 <PolicyCode></PolicyCode>
            //                                                                <DeleteMapping></DeleteMapping> 
            //                                                            </PolicyCodeMapping>
            //                                                        </Document>
            //                                                    </Message>");

            //            return oTemplate;

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("PolicyCodeMappingAdaptor.DeleteCodeMapping");
            sXml = sXml.Append("</Function></Call><Document><PolicyCodeMapping><DeleteCode>");
            sXml = sXml.Append("<DeletedCode>" + hdnSelected.Text + "</DeletedCode>");



            sXml = sXml.Append("</DeleteCode></PolicyCodeMapping></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }
       
        private XElement GetMessageTemplateForSave()
        {
            //            XElement oTemplate = XElement.Parse(@"
            //                                                    <Message>
            //                                                      <Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization> 
            //                                                     <Call>
            //                                                      <Function>PolicyCodeMappingAdaptor.SavePolicyCodeInfo</Function> 
            //                                                      </Call>
            //                                                        <Document>
            //                                                            <PolicyCodeMapping>
            //                                                                    <PolMapValues></PolMapValues>
            //                                                             </PolicyCodeMapping>
            //                                                        </Document>
            //                                                    </Message>");
            //if (mode.Text.ToLower() == "edit")
            //{
            //    string strRowId = AppHelper.GetQueryStringValue("selectedid");
            //    RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            //}
            //rupal:start
            string sPolSystemID = string.Empty;
            string sPolSysTypeID = string.Empty;
            string[] sSelCodeType;
            GetPolicySystemSelectedValue(ref sPolSystemID, ref sPolSysTypeID);
            //rupal:end
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("PolicyCodeMappingAdaptor.SavePolicyCodeInfo");
            sXml = sXml.Append("</Function></Call><Document><PolicyCodeMapping><AddCode>");
            sXml = sXml.Append("<RMCode>" + lstRMCodes.SelectedValue + "</RMCode>");
            sXml = sXml.Append("<PolSysCode>" + lstPolSysCodes.SelectedValue + "</PolSysCode>");

            sSelCodeType = lstCodeType.SelectedValue.Split('|');
            //sXml = sXml.Append("<CodeType>" + lstCodeType.SelectedValue + "</CodeType>");
            sXml = sXml.Append("<CodeType>" + sSelCodeType[0] + "</CodeType>");

            sXml = sXml.Append("<PolicySystemId>" + sPolSystemID + "</PolicySystemId>");
            if (sSelCodeType.Length == 2 && sSelCodeType[1] == "1")
            {
                // has claim type value, send it
                sXml = sXml.Append("<ClaimTypeCd>" + lstClaimTypes.SelectedValue + "</ClaimTypeCd>");
            }
            else
            {
                sXml = sXml.Append("<ClaimTypeCd>0</ClaimTypeCd>");
            }
            sXml = sXml.Append("</AddCode></PolicyCodeMapping></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

            //   return oTemplate;
        }

        private void BindGridData(string data)
        {
            try
            {
                hdValidateKey.Value = string.Empty;
                XmlDocument xmldoc = new XmlDocument();
                string []sSelCodeType;
                xmldoc.LoadXml(data);
                DataTable dtGridData = new DataTable();
                DataRow objRow = null;
                dtGridData.Columns.Add("CBId");
                dtGridData.Columns.Add("rmcode");
                dtGridData.Columns.Add("polsyscode");
                dtGridData.Columns.Add("ClaimCd");


                XmlNodeList xmlNodeList = xmldoc.SelectNodes("//MappedCodes");
                if (xmlNodeList.Count > 0)
                {
                    foreach (XmlNode objNodes in xmlNodeList)
                    {
                        objRow = dtGridData.NewRow();
                        string sRowId = objNodes.Attributes["RowId"].Value.ToString();
                        string sRMCode = objNodes.Attributes["RMCode"].Value.ToString();
                        string sPolSysCode = objNodes.Attributes["PolSysCode"].Value.ToString();
                        string sClaimCd = objNodes.Attributes["ClaimCd"].Value.ToString();
                        string sValidateKey = objNodes.Attributes["Key"].Value.ToString();
                        objRow["CBId"] = sRowId;
                        objRow["rmcode"] = sRMCode;
                        objRow["polsyscode"] = sPolSysCode;
                        objRow["ClaimCd"] = sClaimCd;
                        hdValidateKey.Value = hdValidateKey.Value + "," + sValidateKey;
                        dtGridData.Rows.Add(objRow);
                    }
                }

                objRow = dtGridData.NewRow();
                objRow["CBId"] = "";
                objRow["rmcode"] = "";
                objRow["polsyscode"] = "";
                objRow["ClaimCd"] = "";
                dtGridData.Rows.Add(objRow);

                sSelCodeType = lstCodeType.SelectedValue.Split('|');
                
                if (sSelCodeType.Length==2 && sSelCodeType[1] == "1")
                {
                    // INCLUDE_CLAIM_TYPE =1 in PS_MAP_TABLES, so make Claim type column as visible
                    gvPolicyCodeMappingGrid.Columns[2].Visible = true;
                }
                else
                {
                    gvPolicyCodeMappingGrid.Columns[2].Visible = false;
                }
                

                gvPolicyCodeMappingGrid.DataSource = dtGridData;
                gvPolicyCodeMappingGrid.DataBind();
                gvPolicyCodeMappingGrid.Rows[gvPolicyCodeMappingGrid.Rows.Count - 1].Visible = false;

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        //rupal:start,
        private void GetCodeDetails()
        {
            try
            {
                lstRMCodes.Items.Clear();
                lstPolSysCodes.Items.Clear();
                XElement XmlTemplate = null;
                string sPolCode = "";
                XmlTemplate = GetMessageTemplateForCodeDetails();
                string Data = "";
                bool bReturnStatus = false;
                string[] sSelCodeType;
                lstRMCodes.Visible = true;
                lstPolSysCodes.Visible = true;
                txtRMCode.Visible = true;
                txtPolSysCode.Visible = true;
                addSPolicybtn.Visible = true;
                removePolicybtn.Visible = true;
                //txtCodeTypeId.Text = lstCodeType.SelectedValue;
                sSelCodeType = lstCodeType.SelectedValue.Split('|');
                txtCodeTypeId.Text = sSelCodeType[0];

                bReturnStatus = CallCWS("PolicyCodeMappingAdaptor.GetCodeDetails", XmlTemplate, out Data, false, true);

                if (bReturnStatus)
                {
                    BindGridData(Data);
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //rupal:end      

        protected void addPolicy_btn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            XElement XmlTemplate = null;
            string[] sSelCodeType;
            try
            {
                XmlTemplate = GetMessageTemplateForSave();
                string Data = "";
                bool bReturnStatus = false;
                txtRMCodeId.Text = lstPolSysCodes.SelectedValue.ToString();
                txtPolSysCodeId.Text = lstRMCodes.SelectedValue.ToString();

                sSelCodeType = lstCodeType.SelectedValue.Split('|');
                //txtCodeTypeId.Text = lstCodeType.SelectedValue.ToString();
                txtCodeTypeId.Text = sSelCodeType[0];
                bReturnStatus = CallCWS("PolicyCodeMappingAdaptor.SavePolicyCodeInfo", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }

                    else
                    {
                        XmlTemplate = GetMessageTemplateForCodeDetails();
                        while (lstRMCodes.Items.Count > 0)
                            lstRMCodes.Items.RemoveAt(0);
                        while (lstPolSysCodes.Items.Count > 0)
                            lstPolSysCodes.Items.RemoveAt(0);
                        bReturnStatus = CallCWS("PolicyCodeMappingAdaptor.GetCodeDetails", XmlTemplate, out Data, false, true);
               
                        BindGridData(Data);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void removePolicybtn_Click(object sender, EventArgs e)
        {
            try
            {
                XElement XmlTemplate = null;
                string sPolCode = "";
                XmlTemplate = GetMessageTemplateForDeletion();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("PolicyCodeMappingAdaptor.DeleteCodeMapping", XmlTemplate, out Data, true, false);
                XmlTemplate = GetMessageTemplateForCodeDetails();
                bReturnStatus = CallCWS("PolicyCodeMappingAdaptor.GetCodeDetails", XmlTemplate, out Data, false, true);
               
                if (bReturnStatus)
                {
                    BindGridData(Data);

                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void lstCodeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetCodeDetails();
            string[] sSelCodeType;

            sSelCodeType = lstCodeType.SelectedValue.Split('|');

            if (sSelCodeType.Length == 2 && sSelCodeType[1] == "1") // include_claim_type=1, so add claim type drop down
            {
                GetClaimTypes();
                trClaimType.Style.Add("display", "");
            }
            else
            {
                trClaimType.Style.Add("display", "none");
            }
        }

        protected void lstPolicySystem_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //lstCodeType.Items.Clear();
                //XElement XmlTemplate = null;
                //string sPolCode = "";
                //XmlTemplate = GetMessageTemplateForCodeDetails();
                //string Data = "";
                //bool bReturnStatus = false;
                //lstRMCodes.Visible = true;
                //lstPolSysCodes.Visible = true;
                //txtRMCode.Visible = true;
                //txtPolSysCode.Visible = true;
                //addSPolicybtn.Visible = true;
                //removePolicybtn.Visible = true;
                //txtCodeTypeId.Text = lstCodeType.SelectedValue;
                //bReturnStatus = CallCWS("PolicyCodeMappingAdaptor.GetCodeDetails", XmlTemplate, out Data, false, true);

                //if (bReturnStatus)
                //{
                //    // BindDropList(Data);
                //    BindGridData(Data);


                //}
                GetCodeTypes();
                GetCodeDetails();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void GetClaimTypes()
        {
            try
            {
                XElement XmlTemplate = null;
                string sPolCode = "";
                XmlTemplate = GetMessageTemplateForClaimTypes();
                string Data = "";
                bool bReturnStatus = false;
                string[] sSelCodeType;
                lstRMCodes.Visible = true;
                lstPolSysCodes.Visible = true;  
                txtRMCode.Visible = true;
                txtPolSysCode.Visible = true;
                addSPolicybtn.Visible = true;
                removePolicybtn.Visible = true;
                sSelCodeType = lstCodeType.SelectedValue.Split('|');
                txtCodeTypeId.Text = sSelCodeType[0];   //lstCodeType.SelectedValue;
                bReturnStatus = CallCWS("PolicyCodeMappingAdaptor.GetClaimTypes", XmlTemplate, out Data, false, true);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplateForClaimTypes()
        {
            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;
            string sPolSystemID = string.Empty;
            string sPolSysTypeID = string.Empty;
            GetPolicySystemSelectedValue(ref sPolSystemID, ref sPolSysTypeID);
            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>PolicyCodeMappingAdaptor.GetClaimTypes</Function></Call>");
            sXML.Append("<Document><ClaimTypes><PolicySystemTypeCode>");
            sXML.Append(sPolSysTypeID);
            sXML.Append("</PolicySystemTypeCode>");
            sXML.Append("</ClaimTypes></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }

        //private void BindDropList(string p_sData)
        //{
        //    XmlDocument xmldoc = new XmlDocument();
        //    xmldoc.LoadXml(p_sData);
        //    DataTable dtGridData = new DataTable();
        //    DataRow objRow = null;
        //    DataSet dSet = null;
        //    XmlNode objNode = xmldoc.SelectSingleNode("ResultMessage/Document/RMCodes");
        //    if (objNode != null)
        //    {
        //        dSet = new DataSet();
        //        dSet.ReadXml(new XmlNodeReader(xmldoc));

        //        lstRMCodes.DataSource = dSet;
        //        lstRMCodes.DataTextField = "";
        //        lstRMCodes.DataValueField = "";
        //        lstRMCodes.DataBind();

        //    }
            
        //    objNode = null;
        //    dSet = null;
        //    objNode = xmldoc.SelectSingleNode("ResultMessage/Document/PolSysCodes");
        //    if (objNode != null)
        //    {
        //        dSet = new DataSet();
        //        dSet.ReadXml(new XmlNodeReader(xmldoc));

        //        lstRMCodes.DataSource = dSet;
        //        lstRMCodes.DataTextField = "";
        //        lstRMCodes.DataValueField = "";
        //        lstRMCodes.DataBind();

        //    }
 
        //}

    }
}