﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using Riskmaster.AppHelpers;
using System.Xml;
using System.Text;
using System.Xml.XPath;

namespace Riskmaster.UI.Utilities.Manager
{
  public partial class MultiCurrency : NonFDMBasePageCWS
  {
    XElement XmlTemplate = null;
    string sCWSresponse = "";

    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        if (!IsPostBack)
        {
          gridname.Text = AppHelper.GetQueryStringValue("gridname");
          mode.Text = AppHelper.GetQueryStringValue("mode");
          selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
          if (mode.Text.ToLower() != "add")
          {
            XmlTemplate = GetMessageTemplate();
            CallCWS("MultiCurrencyAdaptor.GetSelectedMultiCurrencyInfo", XmlTemplate, out sCWSresponse, true, true);
          }
        }

        else
        {
          TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
          txtPostBackParent.Text = "Done";

          Control c = DatabindingHelper.GetPostBackControl(this.Page);

          if (c == null)
          {
            XmlTemplate = GetMessageTemplate();
            CallCWS("MultiCurrencyAdaptor.GetSelectedMultiCurrencyInfo", XmlTemplate, out sCWSresponse, true, true);
            XmlDocument objReturnXml = new XmlDocument();
            objReturnXml.LoadXml(sCWSresponse);
            string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
            if (sMsgStatus == "Success")
            {
              XElement rootElement = XElement.Parse(sCWSresponse);
              if (rootElement.XPathSelectElement("//displaycolumn/control[@name='MultiCurrencyId']") != null)
              {
                MultiCurrencyIDLoadvalue.Text = rootElement.XPathSelectElement("//displaycolumn/control[@name='MultiCurrencyId']").Value;
              }
            }
          }

        }
      }
      catch (Exception ee)
      {
        ErrorHelper.logErrors(ee);
        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
      }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {

      try
      {
        if (SourceCurrency.CodeText != "" && SourceCurrency.CodeText != "0" && DestinationCurrency.CodeText != "" && DestinationCurrency.CodeText != "0")
        {
          XmlTemplate = GetMessageTemplate();
          CallCWS("MultiCurrencyAdaptor.Save", XmlTemplate, out sCWSresponse, true, false);
          XmlDocument objReturnXml = new XmlDocument();
          objReturnXml.LoadXml(sCWSresponse);
          string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
          if (sMsgStatus == "Success")
          {
            ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
          }
        }
      }
      catch (Exception ee)
      {
        ErrorHelper.logErrors(ee);
        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
      }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
      btnOk_Click(sender, e);
    }

    private XElement GetMessageTemplate()
    {
      string strMultiCurrencyId = string.Empty;

      if (mode.Text.ToLower() == "edit")
      {
        strMultiCurrencyId = AppHelper.GetQueryStringValue("selectedid");
      }
      StringBuilder sXml = new StringBuilder("<Message>");
      sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
      sXml = sXml.Append("<Call><Function></Function></Call><Document>");
      sXml = sXml.Append("<form name='MultiCurrencySetup' title='MultiCurrency'>");
      sXml = sXml.Append("<group name='MultiCurrencySetup' title='MultiCurrency'>");
      sXml = sXml.Append("<displaycolumn>");
      sXml = sXml.Append("<control name='CurrencyRowId' type='id'>");
      sXml = sXml.Append(strMultiCurrencyId);
      sXml = sXml.Append("</control>");
      sXml = sXml.Append("<control name='SourceCurrencyId' type='id'>");
      sXml = sXml.Append("</control>");
      sXml = sXml.Append("<control name='DestinationCurrencyId' type='id'>");
      sXml = sXml.Append("</control>");
      sXml = sXml.Append("</displaycolumn>");
      sXml = sXml.Append("</group></form>");
      sXml = sXml.Append("</Document>");
      sXml = sXml.Append("</Message>");
      XElement oElement = XElement.Parse(sXml.ToString());
      return oElement;
    }
  }
}