﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class GridParameterSetup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //MITS 34919  - rkulavil begin
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("GridParameterSetup.aspx"), "GridParameterSetupValidations",
                                                ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "GridParameterSetupValidations", sValidationResources, true);
                //MITS 34919  - rkulavil end
                if (!IsPostBack)
                {

                    XmlDocument xmlDoc = GetInputXML("SupplementalGridParameterSetupAdaptor.Get");
                    BindControls(AppHelper.CallCWSService(xmlDoc.InnerXml));

                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void BindControls(string sOutXml)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(sOutXml);

            minRows.Text = (xmlDoc.SelectSingleNode("ResultMessage/Document/GridParameters/MinRows")).InnerXml;
            maxRows.Text = (xmlDoc.SelectSingleNode("ResultMessage/Document/GridParameters/MaxRows")).InnerXml;
            minCols.Text = (xmlDoc.SelectSingleNode("ResultMessage/Document/GridParameters/MinCols")).InnerXml;
            maxCols.Text = (xmlDoc.SelectSingleNode("ResultMessage/Document/GridParameters/MaxCols")).InnerXml;

        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlDocument xmlDoc = GetInputXML("SupplementalGridParameterSetupAdaptor.Save");
                AppHelper.CallCWSService(xmlDoc.InnerXml);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
        }

        private XmlDocument GetInputXML(string sFunctionToCall)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            xmlNodeDoc.LoadXml(@"
                    <Message>
                    <Authorization></Authorization> 
                    <Call>
                    <Function>" + sFunctionToCall + @"</Function> 
                    </Call>
                    <Document>
                    <GridParameters>
                    <MinRows>" + minRows.Text +@"</MinRows> 
                    <MaxRows>"+ maxRows.Text +@"</MaxRows> 
                    <MinCols>"+ minCols.Text +@"</MinCols> 
                    <MaxCols>"+ maxCols.Text +@"</MaxCols> 
                    </GridParameters>
                    </Document>
                    </Message>
                    ");
            return xmlNodeDoc;
        }
    }
}
