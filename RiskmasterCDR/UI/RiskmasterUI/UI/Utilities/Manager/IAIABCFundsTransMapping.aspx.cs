﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class IAIABCFundsTransMapping : NonFDMBasePageCWS
    {
        private bool bReturnStatus;
        private string sCWSresponse = "";
        private DataTable dtGridData = null;
        private XmlDocument oFDMPageDom = null;
        private XmlNode BenefitList = null;
        private XmlNode PaidToDateList = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CallOnPageLoad();
                
            }
        }

        protected void SaveBenefitMapping(object sender, EventArgs e)
        {
            try
            {

                XElement oMessageElement = GetMessageTemplate();

                oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='IAIABCBenefits']").Value = lstBenefits.SelectedValue;
                oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='TransactionTypeBenefits']").Value = lstTransactionTypeBenefits.SelectedValue;

                bReturnStatus = CallCWS("IAIABCFundsTransMappingAdaptor.SaveBenefits", oMessageElement, out sCWSresponse, false, false);

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            // need to call the same functions as on page load to display changes in grid

            if(bReturnStatus) 
                CallOnPageLoad();

        }

        protected void DeleteBenefitMapping(object sender, EventArgs e)
        {
            try
            {
                XElement oMessageElement = GetMessageTemplate();

                oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='IAIABCBenefitsList']").Value = this.hdnSelectedBenefits.Text;

                bReturnStatus = CallCWS("IAIABCFundsTransMappingAdaptor.DeleteBenefitsMapping", oMessageElement, out sCWSresponse, false, false);

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
            if(bReturnStatus)
                CallOnPageLoad();

            
        }

        protected void SavePaidToDateMapping(object sender, EventArgs e)
        {
            try
            {
                XElement oMessageElement = GetMessageTemplate();

                oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='IAIABCPaidToDate']").Value = lstPaidToDate.SelectedValue;
                oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='TransactionTypePaidToDate']").Value = lstTransactionTypePaidToDate.SelectedValue;

                bReturnStatus = CallCWS("IAIABCFundsTransMappingAdaptor.SavePaidToDate", oMessageElement, out sCWSresponse, false, false);

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            // need to call the same functions as on page load to display changes in grid

            if(bReturnStatus)
                CallOnPageLoad();
                                
        }


        protected void DeletePaidToDateMapping(object sender, EventArgs e)
        {
            try
            {
                XElement oMessageElement = GetMessageTemplate();

                oMessageElement.XPathSelectElement("./Document/FundsMapping/control[@name='IAIABCPaidToDateList']").Value = this.hdnSelectedPaidToDate.Text;

                bReturnStatus = CallCWS("IAIABCFundsTransMappingAdaptor.DeletePaidToDateMapping", oMessageElement, out sCWSresponse, false, false);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            if(bReturnStatus)
                CallOnPageLoad();
                                            
        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
              <Call>
               <Function></Function> 
              </Call>
              <Document>
                <FundsMapping>
                    <control name='IAIABCBenefits'></control> 
                    <control name='TransactionTypeBenefits'></control> 
                    <control name='IAIABCPaidToDate'></control> 
                    <control name='TransactionTypePaidToDate'></control> 
                    <control name='IAIABCBenefitsList' rmxignoreget='true' rmxignoreset='true'></control> 
                    <control name='IAIABCPaidToDateList' rmxignoreget='true' rmxignoreset='true'></control>
                    <control name='flagInputXML'>1</control> 
                </FundsMapping>
              </Document>
           </Message>
            ");

            return oTemplate;
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            XElement objNewElm = null;
            

            if (Xelement.XPathSelectElement("./Document/FundsMapping/control[@name='flagInputXML']")!=null && Xelement.XPathSelectElement("./Document/FundsMapping/control[@name='flagInputXML']").Value == "1")
            {
            }
            else
            {
                Xelement.XPathSelectElement("//control[@name='IAIABCBenefitsList']").Value = "";
                Xelement.XPathSelectElement("//control[@name='IAIABCPaidToDateList']").Value = "";
                
                    XElement IAIABCFund = Xelement.XPathSelectElement("//control[@name='IAIABCBenefitsList']");

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "BenefitsRowId");
                    objNewElm.Value = "Row Id";
                    IAIABCFund.Add(objNewElm);

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "IAIABCBenefitType");
                    objNewElm.Value = "IAIABC Benefit Type";
                    IAIABCFund.Add(objNewElm);

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "IAIABCTransactionType");
                    objNewElm.Value = "IAIABC Transaction Type";
                    IAIABCFund.Add(objNewElm);

                    //for second grid

                    XElement IAIABCTransaction = Xelement.XPathSelectElement("//control[@name='IAIABCPaidToDateList']");

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "PaidToDatRowId");
                    objNewElm.Value = "Row Id";
                    IAIABCTransaction.Add(objNewElm);

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "IAIABCPTDType");
                    objNewElm.Value = "IAIABC PTD Type";
                    IAIABCTransaction.Add(objNewElm);

                    objNewElm = new XElement("rowhead");
                    objNewElm.SetAttributeValue("colname", "IAIABCPaidToDatTransactionType");
                    objNewElm.Value = "IAIABC Transaction Type";
                    IAIABCTransaction.Add(objNewElm);

                    
           }
           
        }

        protected void BindGridToData()
        {
            int lCount=1;
            string checkValue = "";
            string benefitName = "";
            string transactionName = "";

            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            dtGridData = new DataTable();

            dtGridData.Columns.Add("CheckValue");
            dtGridData.Columns.Add("benefitName");
            dtGridData.Columns.Add("transactionName");

            DataRow objRow = null;

            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("Document/form/group/displaycolumn/control[@name='IAIABCBenefitsList']/listrow");

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();

                lCount = 1;

                XmlNodeList xmlChildElements = objNodes.ChildNodes;
                
                foreach (XmlNode objChildNode in xmlChildElements)
                {
                    switch (lCount)
                    {
                        case 1:
                            checkValue = objChildNode.Attributes["value"].Value.ToString();
                            break;
                        case 2:
                            benefitName = objChildNode.Attributes["title"].Value.ToString();
                            break;
                        case 3:
                            transactionName = objChildNode.Attributes["title"].Value.ToString();
                            break;
                    }
                    lCount = lCount + 1;
                }
                objRow["CheckValue"] = checkValue;
                objRow["benefitName"] = benefitName ;
                objRow["transactionName"] = transactionName;
                dtGridData.Rows.Add(objRow);
            }

            GdIAIABCBenefitMapping.DataSource = dtGridData;
            GdIAIABCBenefitMapping.DataBind();
            BeforePrerender("1");

            // To show the headers if the there is no data in the grid
            if (dtGridData.Rows.Count == 0)
            {
                dtGridData.Rows.Add(dtGridData.NewRow());
                GdIAIABCBenefitMapping.DataSource = dtGridData;
                GdIAIABCBenefitMapping.DataBind();
                GdIAIABCBenefitMapping.Rows[0].Visible = false;
            }
            dtGridData.Dispose();

            // function for second grid
            lCount = 1;
            checkValue = "";
            string ptdName = "";
            string transactionType = "";
            dtGridData = new DataTable();

            dtGridData.Columns.Add("CheckValue");
            dtGridData.Columns.Add("ptdName");
            dtGridData.Columns.Add("transactionType");

            objRow = null;

            xmlNodeList = oFDMPageDom.SelectNodes("Document/form/group/displaycolumn/control[@name='IAIABCPaidToDateList']/listrow");

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();

                lCount = 1;

                XmlNodeList xmlChildElements = objNodes.ChildNodes;

                foreach (XmlNode objChildNode in xmlChildElements)
                {
                    switch (lCount)
                    {
                        case 1:
                            checkValue = objChildNode.Attributes["value"].Value.ToString();
                            break;
                        case 2:
                            ptdName = objChildNode.Attributes["title"].Value.ToString();
                            break;
                        case 3:
                            transactionType = objChildNode.Attributes["title"].Value.ToString();
                            break;
                    }
                    lCount = lCount + 1;
                }
                objRow["CheckValue"] = checkValue;
                objRow["ptdName"] = ptdName;
                objRow["transactionType"] = transactionType;
                dtGridData.Rows.Add(objRow);
            }

            GdIAIABCPaidMapping.DataSource = dtGridData;
            GdIAIABCPaidMapping.DataBind();
            BeforePrerender("2");

            // To show the headers if the there is no data in the grid
            if (dtGridData.Rows.Count == 0)
            {
                dtGridData.Rows.Add(dtGridData.NewRow());
                GdIAIABCPaidMapping.DataSource = dtGridData;
                GdIAIABCPaidMapping.DataBind();
                GdIAIABCPaidMapping.Rows[0].Visible = false;
            }


        }

        protected void BeforePrerender( string GdValue)
        {
            if (dtGridData != null)
            {

                if (dtGridData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        if (GdValue == "1")
                        {
                            Label benefitName = ((Label)(GdIAIABCBenefitMapping.Rows[i].FindControl("benefitName")));
                            benefitName.Text = dtGridData.Rows[i]["benefitName"].ToString();

                            Label transactionName = ((Label)(GdIAIABCBenefitMapping.Rows[i].FindControl("transactionName")));
                            transactionName.Text = dtGridData.Rows[i]["transactionName"].ToString();
                        }
                        else
                        {
                            Label ptdName = ((Label)(GdIAIABCPaidMapping.Rows[i].FindControl("ptdName")));
                            ptdName.Text = dtGridData.Rows[i]["ptdName"].ToString();

                            Label transactionType = ((Label)(GdIAIABCPaidMapping.Rows[i].FindControl("transactionType")));
                            transactionType.Text = dtGridData.Rows[i]["transactionType"].ToString();
                        }
                    }
                }
            }
        }

        protected void CallOnPageLoad()
        {
            try
            {
                bReturnStatus = CallCWS("IAIABCFundsTransMappingAdaptor.Get", null, out sCWSresponse, true, true);

                if (bReturnStatus)
                {
                    BindGridToData();
                }
                // setting the option value="" for first value of dropdowns
                if(this.lstBenefits.Items.Count > 0)
                    this.lstBenefits.Items[0].Value = "";

                if (this.lstPaidToDate.Items.Count > 0)
                    this.lstPaidToDate.Items[0].Value = "";

                if (this.lstTransactionTypeBenefits.Items.Count > 0)    
                    this.lstTransactionTypeBenefits.Items[0].Value = "";

                if (this.lstTransactionTypePaidToDate.Items.Count > 0)
                    this.lstTransactionTypePaidToDate.Items[0].Value = "";

                BenefitList = Data.SelectSingleNode("//BenefitListValues");
                this.hdnBenefits.Text = BenefitList.InnerText.ToString();

                PaidToDateList = Data.SelectSingleNode("//PaidToDateListValues");
                this.hdnPaidToDate.Text = PaidToDateList.InnerText.ToString();
                
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
