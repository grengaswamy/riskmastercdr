﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Drawing.Printing;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class ClaimLetterSetup : NonFDMBasePageCWS
    {
        private XElement XmlTemplate = null;
        //XElement oMessageElement = null;
        private bool bReturnStatus = false;
        private string sreturnValue = string.Empty;
        private XmlDocument XmlDoc = new XmlDocument();
        private string sNode = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //PrintDocument oPrintDocument = new PrintDocument();
                if (!IsPostBack)
                {
                    //oMessageElement = GetMessageTemplate();
                    bReturnStatus = CallCWS("ClaimLetterAdaptor.Get", XmlTemplate, out sreturnValue, true, true);

                    if (bReturnStatus)
                    {
                        ErrorControl1.errorDom = sreturnValue;
                        //Printing not needed for now.
                        //XmlDoc.LoadXml(sreturnValue);
                        //ddlPrinters.Items.Clear();
                        //ddlPrinters.Items.Add("");
                        //foreach (string printer in PrinterSettings.InstalledPrinters)
                        //{

                        //    ListItem item = new ListItem(printer);
                        //    ddlPrinters.Items.Add(printer);
                        //}
                        //sNode = null;
                        //sNode = (string)XmlDoc.SelectSingleNode("//Adjuster/control[@name='PrinterName']").InnerText;
                        //ddlPrinters.SelectedValue = sNode;
                    }
                }
                else
                {
                    txtIncAdjusters.Text = lstExcAdjusters.SelectedValue;
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                UpdateListItems();
                bReturnStatus = CallCWS("ClaimLetterAdaptor.Save", null, out sreturnValue, true, false);
                if (bReturnStatus)
                {
                    bReturnStatus = CallCWSFunction("ClaimLetterAdaptor.Get");
                    ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        //private XElement GetMessageTemplate()
        //{
        //    StringBuilder sXml = new StringBuilder("<Message>");
        //    sXml = sXml.Append("<Authorization></Authorization>");
        //    sXml = sXml.Append("<Call><Function>AdjusterTransferAdaptor.GetAvailableClaims</Function></Call>");
        //    sXml = sXml.Append("<Document><Adjuster><control name=\"ExcAdjusters\">");
        //    sXml = sXml.Append("</control>");
        //    sXml = sXml.Append("<control name=\"ExcAdjusters\" >");
        //    sXml = sXml.Append(txtExcAdjusters.Text);
        //    sXml = sXml.Append("</control>");
        //    sXml = sXml.Append("<control name=\"EmailFrom\" >");
        //    sXml = sXml.Append("</control>");
        //    sXml = sXml.Append("</Adjuster></Document></Message>");
        //    XElement oTemplate = XElement.Parse(sXml.ToString());
        //    return oTemplate;
        //}

        protected void UpdateListItems()
        {
            lstExcAdjusters.Items.Clear();
            lstIncAdjusters.Items.Clear();
            txtIncAdjusters.Text = "";
            txtExcAdjusters.Text = "";

            string[] splitter = { "|^|" };//MITS 35350 
            string svalues = string.Empty;
            string[] lsthndValues = hdnExcAdjusters.Value.Split(splitter,StringSplitOptions.None);
            if (lsthndValues.Length > 1)
            {
                char[] splitterEqual = { '=' };
                for (int Icount = 0; Icount < lsthndValues.Length - 1; Icount++)
                {
                    string[] svalue = lsthndValues[Icount].Split(splitterEqual);
                    if (svalue.Length > 1)
                    {
                        ListItem listitem = new ListItem(svalue[1], svalue[0]);
                        try
                        {
                            if (!lstExcAdjusters.Items.Contains(listitem))
                            {
                                lstExcAdjusters.Items.Add(listitem);
                                svalues += svalue[0] + " ";

                            }

                            if (lstIncAdjusters.Items.Contains(listitem))
                            {
                                lstIncAdjusters.Items.Remove(listitem);

                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                        lstExcAdjusters.Items[Icount].Selected = true;
                    }
                }
                if (svalues.Length > 0)
                {
                    txtExcAdjusters.Text = svalues.Substring(0, svalues.Length - 1);
                }
                svalues = "";
                hdnExcAdjusters.Value = "";
            }


            lsthndValues = hdnIncAdjusters.Value.Split(splitter,StringSplitOptions.None);
            if (lsthndValues.Length > 1)
            {
                char[] splitterEqual = { '=' };
                for (int Icount = 0; Icount < lsthndValues.Length - 1; Icount++)
                {
                    string[] svalue = lsthndValues[Icount].Split(splitterEqual);
                    if (svalue.Length > 1)
                    {
                        ListItem listitem = new ListItem(svalue[1], svalue[0]);
                        try
                        {
                            if (lstExcAdjusters.Items.Contains(listitem))
                            {
                                lstExcAdjusters.Items.Remove(listitem);

                            }
                            if (!lstIncAdjusters.Items.Contains(listitem))
                            {
                                lstIncAdjusters.Items.Add(listitem);
                                svalues += svalue[0] + " ";
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                        lstIncAdjusters.Items[Icount].Selected = true;
                    }
                }
                if (svalues.Length > 0)
                {
                    txtIncAdjusters.Text = svalues.Substring(0, svalues.Length - 1);
                }
                svalues = "";
                hdnIncAdjusters.Value = "";
            }
        }
        
        //Printing not needed for now.
        //protected void ddlPrinters_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PrintDocument oPrintDocument = new PrintDocument();

        //        oPrintDocument.PrinterSettings.PrinterName = ddlPrinters.SelectedValue;
        //        //ddlPbfc.Items.Clear();
        //        //ddlpbfeob.Items.Clear();
        //        //foreach (PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
        //        //{
        //        //    ListItem item = new ListItem(oPaperSource.SourceName, oPaperSource.RawKind.ToString());
        //        //    ddlPbfc.Items.Add(item);
        //        //    ddlpbfeob.Items.Add(item);
        //        //}
        //    }
        //    catch (Exception ee)
        //    {
        //        ErrorHelper.logErrors(ee);
        //        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        //        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        //        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
        //    }
        //}
    }
}