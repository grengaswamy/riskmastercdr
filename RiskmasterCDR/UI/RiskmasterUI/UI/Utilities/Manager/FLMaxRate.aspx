<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FLMaxRate.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.FLMaxRate" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Add/Modify FL Max Rate</title>
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Add/Modify FL Max Rate" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <%--<div class="Selected" id="TABSFLMaxRate">
        <span>
            <asp:Label runat="server" name="lblFLMaxRate" ID="lblFLMaxRate" Text="Max Rate" />
        </span>
    </div>--%>
    <br />
    <asp:TextBox Style="display: none" runat="server" ID="MaxRateId" RMXRef="/Instance/Document//FLMaxRate/control[@name='MaxRateId']" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <div id="div_Year" class="completerow">
        <asp:Label runat="server" class="required" ID="lblYear" Text="Year:" />
        <span class="formw">
            <asp:TextBox runat="server" ID="Year" RMXRef="/Instance/Document//FLMaxRate/control[@name='Year']"
                TabIndex="1" AutoPostBack="false" onchange="setDataChanged(true);return OnYearChange();" />
        </span>
    </div>
    <div id="div_MaxRate" class="completerow">
        <asp:Label runat="server" class="required" ID="lbl_MaxRate" Text="MaxRate:" />
        <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);return OnMaxRateChange();"
                ID="MaxRate" RMXRef="/Instance/Document//FLMaxRate/control[@name='MaxRate']"
                TabIndex="2" />
        </span>
    </div>
    <br />
    <br />
    <br />
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return FLMaxRate_onOk();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return FLMaxRate_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
