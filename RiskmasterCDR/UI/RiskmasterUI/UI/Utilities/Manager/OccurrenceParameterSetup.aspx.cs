using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Text;
using System.Xml.XPath;
using System.Xml;
namespace Riskmaster.UI.Utilities.Manager
{
    public partial class OccurrenceParameterSetup : NonFDMBasePageCWS
    {
        private XmlDocument oFDMPageDom = new XmlDocument();
        XElement oMessageElement = null;
        public string sReturn = "";
        public string sCWSresponse = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bool bReturnStatus = false;
                oMessageElement = GetMessageTemplate();
                bReturnStatus = CallCWS("OccurrenceParmsAdaptor.Get", oMessageElement, out sCWSresponse, false, true);
            }

        }


        protected void Save(object sender, EventArgs e)
        {
            try
            {
                ListBox lstoldvalues = null;
                TextBox tbnewvalues = null;
                bool bReturnStatus = false;
                string Data = "";
                // for medication
                lstoldvalues = (ListBox)Medication.FindControl("multicode");
                tbnewvalues = (TextBox)Medication.FindControl("multicode_lst");
                dochange(lstoldvalues, tbnewvalues);
                lstoldvalues.Items.Clear();

                // for sentinel
                lstoldvalues = (ListBox)Sentinel.FindControl("multicode");
                tbnewvalues = (TextBox)Sentinel.FindControl("multicode_lst");
                dochange(lstoldvalues, tbnewvalues);
                lstoldvalues.Items.Clear();

                //for equipment
                lstoldvalues = (ListBox)Equipment.FindControl("multicode");
                tbnewvalues = (TextBox)Equipment.FindControl("multicode_lst");
                dochange(lstoldvalues, tbnewvalues);
                lstoldvalues.Items.Clear();

                //for fall event
                lstoldvalues = (ListBox)FallEventCateg.FindControl("multicode");
                tbnewvalues = (TextBox)FallEventCateg.FindControl("multicode_lst");
                dochange(lstoldvalues, tbnewvalues);
                lstoldvalues.Items.Clear();


                bReturnStatus = CallCWS("OccurrenceParmsAdaptor.Save", oMessageElement, out Data, true, false);
                tbnewvalues = (TextBox)Medication.FindControl("multicode_lst");
                tbnewvalues.Text = "";
                tbnewvalues = (TextBox)Equipment.FindControl("multicode_lst");
                tbnewvalues.Text = "";
                tbnewvalues = (TextBox)FallEventCateg.FindControl("multicode_lst");
                tbnewvalues.Text = "";
                tbnewvalues = (TextBox)Sentinel.FindControl("multicode_lst");
                tbnewvalues.Text = "";

                if (bReturnStatus)
                {
                    oMessageElement = GetMessageTemplate();
                    CallCWS("OccurrenceParmsAdaptor.Get", oMessageElement, out sCWSresponse, false, true);
                }

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void dochange(ListBox lbold, TextBox tbnew)
        {
           
                string[] arrCodes = { "" };
                arrCodes = new string[1];
                if (tbnew.Text.IndexOf(" ") > 0)
                    arrCodes = tbnew.Text.Split(' ');
                else
                    arrCodes[0] = tbnew.Text;

                for (int i = 0; i < arrCodes.Length; i++)
                {
                    ListItem lsitem = lbold.Items.FindByValue(arrCodes[0].ToString());
                    if (lsitem != null)
                    {
                        if (lsitem.Value != "")
                        {
                            lbold.Items.Clear();
                            break;
                        }
                    }
                }

                if (lbold.Items.Count == 1 && arrCodes[0].ToString() == "0")
                {
                    lbold.Items.Clear();
                }

                //MITS 27919 hlv 4/19/2012 begin
                if (lbold.Items.Count > 0 && arrCodes.Length > 0 && arrCodes[0].Equals("0"))
                {
                    lbold.Items.Clear();
                }
                //MITS 27919 hlv 4/19/2012 end

                for (int i = 0; i < lbold.Items.Count; i++)
                {

                    if (tbnew.Text.Trim() == "")
                    {
                        tbnew.Text = lbold.Items[i].Value.ToString();
                        
                    }
                    else
                    {
                        tbnew.Text = tbnew.Text + " " + lbold.Items[i].Value.ToString();
                    }
                }
            
        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>OccurrenceParmsAdaptor.Get</Function></Call>");
            sXml = sXml.Append("<Document><form><group><displaycolumn><control name=\"FacilityName\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FromAdjuster\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FacilityAddress\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"Address2\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"City\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"StateZip\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"HCFANumber\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"ContactPerson\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FacilityPhone\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FallEventCateg\" type=\"codelist\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"Medication\" type=\"codelist\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"Sentinel\" type=\"codelist\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"Equipment\" type=\"codelist\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</displaycolumn></group></form></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
