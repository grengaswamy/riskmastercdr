﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

///////////////////////////////
// Developed By: abansal23
// Completed On: 6th Feb, 2009
///////////////////////////////
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class TierList : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tb_LOB_Code.Text = AppHelper.GetQueryStringValue("lobcode");
            tb_State_Code.Text = AppHelper.GetQueryStringValue("statecode");
            //rsushilaggar MITS 25274 date 06/24/2011
            tb_Parent_Code.Text = AppHelper.GetQueryStringValue("parentcode");
            NonFDMCWSPageLoad("TierListAdaptor.Get");
        }
    }
}
