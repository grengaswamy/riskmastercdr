﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SystemAdminLogin.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.SystemAdminLogin.SystemAdminLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Business Entity Security</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css">

    <script language="JavaScript" src="../../../Scripts/form.js"></script>

    <script type="text/javascript">
        var m_FormSubmitted = false;
        function ValidateOra1() {
            if (!m_FormSubmitted) {
                if (document.forms[0].txtAdminUserIDOracleRole.value == "") {
                    alert("Please enter the Admin UserID");
                    document.forms[0].txtAdminUserIDOracleRole.focus();
                    return false;
                }

                if (document.forms[0].txtPassword.value == "") {
                    alert("Please enter the Admin Password");
                    document.forms[0].txtPassword.focus();
                    return false;
                }
                if (document.forms[0].txtAdminUserIDOracleRole.value == "") {
                    alert("There is no role specified it is compulsory to enter a existing role.");
                    document.forms[0].txtAdminUserIDOracleRole.focus();
                    return false;
                }
                m_FormSubmitted = true;
                return true;
            }
            else {
                alert("You already submitted this form. Please wait for server to respond.");
                return false;
            }
        }

        function ValidateOra2() {
            if (!m_FormSubmitted) {
                if (document.forms[0].txtAdminUserIDOracleRole.value == "") {
                    alert("There is no role specified it is compulsory to enter a existing role.");
                    document.forms[0].txtAdminUserIDOracleRole.focus();
                    return false;
                }
                m_FormSubmitted = true;
                return true;
            }
            else {
                alert("You already submitted this form. Please wait for server to respond.");
                return false;
            }
        }
        function Validate() {
            if (!m_FormSubmitted) {
                if (document.forms[0].txtAdminUserIDOracleRole.value == "") {
                    alert("Please enter the Admin UserID");
                    document.forms[0].txtAdminUserIDOracleRole.focus();
                    return false;
                }

                if (document.forms[0].txtPassword.value == "") {
                    alert("Please enter the Admin Password");
                    document.forms[0].txtPassword.focus();
                    return false;
                }
                m_FormSubmitted = true;
                return true;
            }
            else {
                alert("You already submitted this form. Please wait for server to respond.");
                return false;
            }

        }
    </script>
</head>
<body>
    <form id="frmData" runat="server">
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""><table border="0"
        cellspacing="0" celpadding="0" width="70%" align="center">
          <input type="hidden" name="$node^46" value="" id="IsAdmin" runat="server" />
    <input type="hidden" name="$node^46" value="" id="ShowOnlyRole" runat="server" />
    <input type="hidden" name="$node^46" value="" id="ViewEnhanceBES" runat="server" />
    <input type="hidden" name="$node^46" value="" id="DatabaseLoginInfo" runat="server" />
        <tr>
            <td colspan="2" class="msgheader">
                Database Login Info
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center"><%=DatabaseLoginInfo.Value%>
                <%--Please login to an Oracle account with DBA privileges. This account information
                will only be used to create a new user account for the purpose of Business Entity
                Security.--%>
            </td>
        </tr>
        <tr>
            <td class="group" colspan="2">
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="required" align="right">
            <asp:Label ID="lblTitle" runat ="server"></asp:Label>
                <%--Oracle Role for BES User:--%>
            </td>
            <td class="required">
                <input type="text" name="$node^42" value="" id="txtAdminUserIDOracleRole" runat ="server">
            </td>
        </tr>
        <%if (ShowOnlyRole.Value != "True")
          { %>
         <tr>
            <td class="required" align="right">
            <asp:Label ID="lblTitle2" runat ="server"></asp:Label>
                <%--Oracle Role for BES User:--%>
            </td>
            <td class="required">
            <!--nadim to change password field-->
                <input type="password" name="$node^42" value="" id="txtPassword" runat ="server" autocomplete="off"/>
            </td>
        </tr>
        <%} %>
        <tr>
            <td class="group" colspan="2">
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" id ="btnOK" name="$action^setvalue%26node-ids%268%26content%26OK" value="&nbsp;&nbsp;OK&nbsp;&nbsp;"
                    class="button" runat ="server" onserverclick="btnOK_click">
                <input type="submit" id ="btnCancel" name="$action^setvalue%26node-ids%268%26content%26Cancel"
                        value="&nbsp;&nbsp;Cancel&nbsp;&nbsp;" class="button" runat ="server" onclick="window.close();">
            </td>
        </tr>
    </table>
    <input type="hidden" name="$node^5" value="" id="SysWindowId"><input type="hidden"
        name="$instance" value="H4sIAAAAAAAAAJ1Ty27bMBA8p19B6G4zcXoyZAEpnINRBwn8KHKlqZVNhCJVkorkv++SsmXJjwat&#xA;TuLO7Cx3dxjPlHVMcSB1LpUd12IS7ZwrxpRWVTWsHofabOno/v6Rvs8Ul2UK0YFaGMhEfYM+onWm&#xA;TW6PZPi8QXygSBjAJyjXkuuyR8Zgugn8uixS5tob6DrrEbXZgFZDrnOKEC2M5mCtNq1w8Ve6KEAK&#xA;1ao3DXzVX/Lt7u6O4Bfn4Fg4hWMIWW4A1ECkCTXCfuTMOjB08bJ2QgonwNLlHkP5U5oLNddboWJ6&#xA;yulrlRaMj3LLY3o8nJXDboX2ubS9VgAqoVJd+YSYnv5beAXW/YR9I308BIS2TTV3EGcVGXdYkJ5F&#xA;50J9oMR5GDfhBlzLMr/ICJA2KU7nGoJLZwO3L+AC3WErYAzuOMmYtIDz64T65CZ4Gg2OUZwOy2Z4&#xA;M5XpDqUWY9HYnuzQ75NoOKTvL3N6fDaDFeSFRE8O1jNv1KiTO9W8zNHX/V38ElA9q51P/vG8PFvU&#xA;Ei//quR+oSUkK1NiP71QvyHvQKJYDpPo3EcRccLJFiABGZIF/C6FgRRhXWxK57RChz9ExJYFPg50&#xA;M7n6xVujy+JQKxR44lyXyvlxtbWmuKYNs0ACgwTslmKrzLVyRsuD9hQsn7MN4JX8vieRxIPU2H9b&#xA;xY/4nzRD72t8MbPpUdVB7SKSCWNdJkCmYQYHfc8ks+k4Ssz3wo5ielD7n6pvVdoveajxxqyt0O9X&#xA;inQWfFv81TAuwVviqnwDE48TNAlBoxHfFpb7sp2Yhl1fwjH1fuu8nq6/4/ZFJH8AJs1TIlMGAAA=">
    </form>
</body>
</html>
