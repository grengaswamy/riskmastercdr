﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
//using Riskmaster.Models;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using System.IO;

namespace Riskmaster.UI.Utilities.SystemAdminLogin
{
    public partial class SystemAdminLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                XmlDocument oFDMPageDom = new XmlDocument();
                string sReturn = "";

                
                IsAdmin.Value = (string)Context.Items["IsAdmin"];
                ShowOnlyRole.Value = (string)Context.Items["ShowOnlyRole"];

                if (ShowOnlyRole.Value == "True")
                {
                    btnOK.Attributes.Add("onclick", "return ValidateOra2();");
                }
                else if (ViewEnhanceBES.Value == "yes")
                {
                    btnOK.Attributes.Add("onclick", "return ValidateOra1();");
                }
                else
                {
                    btnOK.Attributes.Add("onclick", "return Validate();");
                }

                //Preparing XML to send to service
                XElement oMessageElement = SystemAdminGetTemplate();
                //Modify XML 

                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                ViewEnhanceBES.Value = oFDMPageDom.SelectSingleNode("//ViewEnhanceBES").InnerText;
                DatabaseLoginInfo.Value = oFDMPageDom.SelectSingleNode("//control[@name='DescLabe1']").Attributes["title"].Value;
                if (ShowOnlyRole.Value != "True")
                {
                    lblTitle.Text =  oFDMPageDom.SelectSingleNode("//control[@name='AdminUserID']").Attributes["title"].Value;
                    txtAdminUserIDOracleRole.Value = oFDMPageDom.SelectSingleNode("//control[@name='AdminUserID']").InnerText;

                    lblTitle2.Text = oFDMPageDom.SelectSingleNode("//control[@name='AdminPwd']").Attributes["title"].Value;
                    txtPassword.Value = oFDMPageDom.SelectSingleNode("//control[@name='AdminPwd']").InnerText;
                }
                if (ViewEnhanceBES.Value == "yes")
                {
                    lblTitle.Text = oFDMPageDom.SelectSingleNode("//control[@name='OracleRole']").Attributes["title"].Value;
                    txtAdminUserIDOracleRole.Value = oFDMPageDom.SelectSingleNode("//control[@name='OracleRole']").InnerText;
                }
            }
        }
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }
        /// <summary>
        /// CWS request message template for Org Set
        /// </summary>
        /// <returns></returns>
        private XElement SystemAdminGetTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>SystemAdminLoginAdaptor.Get</Function> 
                      </Call>
                      <Document>
                        <Document>
			                <ViewEnhanceBES></ViewEnhanceBES>
			                <ShowOnlyRole></ShowOnlyRole>
				            <form name='SystemAdminLogin' title='System Admin. Required' topbuttons='1' supp=''>
                                <group name='LoginAccountInfo' title='Database Login Info'>
                                    <control name='DescLabe1' type='labelonly' title=''></control>
                                    <control name='AdminUserID' type='text' firstfield='1' title='User ID:'></control>
                                    <control name='AdminPwd' type='text' title='Password:'></control>
			                        <control name='OracleRole' type='text' title='Oracle Role for BES User:'></control>
                                </group>
                            </form>
                        </Document>
			          </Document>
                </Message>


            ");
             
            return oTemplate;
        }
        protected void btnOK_click(object sender, EventArgs e)
        {
            string AdminUserID ="";
            string AdminPwd = "";
            string OracleRole ="";
            if (ShowOnlyRole.Value != "True")
            {
                AdminUserID = txtAdminUserIDOracleRole.Value;
                AdminPwd = txtPassword.Value; 
            }
            else if(ViewEnhanceBES.Value == "yes")
            {
                OracleRole = txtAdminUserIDOracleRole.Value;
            }
            //Canged by nadim for passing oracle role
            Response.Redirect("~/UI/Utilities/LoadOrgSet/LoadOrgSet.aspx?AdminUserID=" + AdminUserID + "&AdminPwd=" + AdminPwd + "&OracleRole=" + OracleRole);
        }
    }
}
