﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.ToolsDesigners
{
    public partial class AddEditAutoDiaryFilter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           try
           {
               if (!IsPostBack)
               {
                   if (Request.QueryString["selectedid"] != null)
                   {
                       ViewState["selectedid"] = Request.QueryString["selectedid"];
                   }
                   else
                   {
                       ViewState["selectedid"] = "";
                   }

                   if (Request.QueryString["templateid"] != null)
                   {
                       ViewState["templateid"] = Request.QueryString["templateid"];
                   }
                   else
                   {
                       ViewState["templateid"] = "";
                   }

                   if (Request.QueryString["filtername"] != null)
                   {
                       ViewState["filtername"] = Request.QueryString["filtername"].Replace("^^^", "(").Replace("@@@", ")");
                   }
                   else
                   {
                       ViewState["filtername"] = "";
                   }

                   if (Request.QueryString["mode"] != null)
                   {
                       ViewState["mode"] = Request.QueryString["mode"];
                   }
                   else
                   {
                       ViewState["mode"] = "";
                   }

                   if (Request.QueryString["sqlfill"] != null)
                   {
                       ViewState["sqlfill"] = Request.QueryString["sqlfill"];
                       ViewState["sqlfill"] = ViewState["sqlfill"].ToString().Replace("`", "'").Replace("^^^", "(").Replace("@@@", ")");
                   }
                   else
                   {
                       ViewState["sqlfill"] = "";
                   }

                   if (Request.QueryString["filtertype"] != null)
                   {
                       ViewState["filtertype"] = Request.QueryString["filtertype"];
                   }
                   else
                   {
                       ViewState["filtertype"] = "";
                   }

                   if (Request.QueryString["defvalue"] != null)
                   {
                       ViewState["defvalue"] = Request.QueryString["defvalue"];
                   }
                   else
                   {
                       ViewState["defvalue"] = "";
                   }

                   if (Request.QueryString["data"] != null)
                   {
                       ViewState["data"] = Request.QueryString["data"];
                   }
                   else
                   {
                       ViewState["data"] = "";
                   }

                   hdFilterType.Value = ViewState["filtertype"].ToString();
                   hdData.Value = ViewState["data"].ToString();
                   if (ViewState["mode"].ToString() == "edit")
                   {
                       this.Page.Title = "Edit Auto Diary Filter";
                   }
                   else
                   {
                       this.Page.Title = "Add Auto Diary Filter";
                   }

                   //rupal:start, MITS 26244
                   if (Request.QueryString["database"] != null)
                   {
                       ViewState["database"] = Request.QueryString["database"];
                   }
                   else
                   {
                       ViewState["database"] = "";
                   }
                   //rupal:end

                   ShowDesiredStyle();
               }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void ShowDesiredStyle()
        {
            if (hdFilterType.Value == "1") //show checkbox
            {
                divForCheckBox.Visible = true;
                divForList.Visible = false;
                divForTextBox.Visible = false;

                lblForChkData.Text = ViewState["filtername"].ToString();
            }
            else if (hdFilterType.Value == "2") //show ListBox
            {
                divForCheckBox.Visible = false;
                divForList.Visible = true;
                divForTextBox.Visible = false;

                lblFilterNameForList.Text = ViewState["filtername"].ToString();
                lblFilterNameForList1.Text = ViewState["filtername"].ToString();

                FillFilterListBoxes(); 
            }
            else if ((hdFilterType.Value == "0") || (hdFilterType.Value == "3") || (hdFilterType.Value == "5")) //show textbox
            {
                divForCheckBox.Visible = false;
                divForList.Visible = false;
                divForTextBox.Visible = true;

                lblFilterNameForTextBox.Text = ViewState["filtername"].ToString();

                if (!string.IsNullOrEmpty(ViewState["data"].ToString()))
                {
                    txtData.Value = ViewState["data"].ToString();
                }
                else
                {
                    txtData.Value = ViewState["defvalue"].ToString();
                }
            }
        }

        private void FillFilterListBoxes()
        {
            XmlDocument addEditFilterDiaryDoc = null;

            addEditFilterDiaryDoc = GetAddEditDiaryFilter();
            if (addEditFilterDiaryDoc != null)
            {
                FillAvlFilterBodyParts(addEditFilterDiaryDoc.SelectSingleNode("ResultMessage/Document/WPAAutoDiaryFilterSetup/AvlBodyParts"));
                
                FillSelectedFilterBodyParts(addEditFilterDiaryDoc.SelectSingleNode("ResultMessage/Document/WPAAutoDiaryFilterSetup/SelBodyParts"));
            }
        }

        private void FillAvlFilterBodyParts(XmlNode avlFilterBodyPartsNode)
        {
            XmlNodeList avlOptionsList = null;

            if (avlFilterBodyPartsNode != null)
            {
                avlOptionsList = avlFilterBodyPartsNode.SelectNodes("option");

                Available.Items.Clear();
                foreach (XmlNode optionNode in avlOptionsList)
                {
                    Available.Items.Add(new ListItem(optionNode.InnerText,optionNode.Attributes["value"].Value));
                }
            }
        }

        private void FillSelectedFilterBodyParts(XmlNode selFilterBodyPartsNode)
        {
            XmlNodeList selOptionsList = null;

            if (selFilterBodyPartsNode != null)
            {
                selOptionsList = selFilterBodyPartsNode.SelectNodes("option");

                Selected.Items.Clear();
                foreach (XmlNode optionNode in selOptionsList)
                {
                    Selected.Items.Add(new ListItem(optionNode.InnerText, optionNode.Attributes["value"].Value));
                }
            }
        }

        private XmlNode GetInputDocForAddEditDiaryFilter()
        {
            string messageTemplageString = string.Empty;
            XmlDocument messageDoc = null;
            XmlNode templateIdNode = null;
            XmlNode idNode = null;
            XmlNode filterNameNode = null;
            XmlNode sqlFillNode = null;
            XmlNode filterTypeNode = null;
            XmlNode defValueNode = null;
            XmlNode dataNode = null;
            XmlNode modeNode = null;
            //rupal, MITS 26244
            XmlNode databaseNode = null;

            messageTemplageString = GetAddEditAutoDiaryFilterTemplate(); 
            messageDoc = new XmlDocument();
            messageDoc.LoadXml(messageTemplageString);

            templateIdNode = messageDoc.SelectSingleNode("WPAAutoDiaryFilterSetup/templateid");

            if (templateIdNode != null)
            {
                templateIdNode.InnerText = ViewState["templateid"].ToString();
            }

            idNode = messageDoc.SelectSingleNode("WPAAutoDiaryFilterSetup/id");
            if (idNode != null)
            {
                idNode.InnerText = ViewState["selectedid"].ToString();
            }


            filterNameNode = messageDoc.SelectSingleNode("WPAAutoDiaryFilterSetup/filtername");
            if (filterNameNode != null)
            {
                filterNameNode.InnerText = ViewState["filtername"].ToString();
            }

            sqlFillNode = messageDoc.SelectSingleNode("WPAAutoDiaryFilterSetup/SQLFill");
            if (sqlFillNode != null)
            {
                sqlFillNode.InnerText = ViewState["sqlfill"].ToString();
            }

            filterTypeNode = messageDoc.SelectSingleNode("WPAAutoDiaryFilterSetup/FilterType");
            if (filterTypeNode != null)
            {
                filterTypeNode.InnerText = ViewState["filtertype"].ToString();
            }

            defValueNode = messageDoc.SelectSingleNode("WPAAutoDiaryFilterSetup/DefValue");
            if (defValueNode != null)
            {
                defValueNode.InnerText = ViewState["defvalue"].ToString();
            }

            dataNode = messageDoc.SelectSingleNode("WPAAutoDiaryFilterSetup/data");
            if (dataNode != null)
            {
                dataNode.InnerText = ViewState["data"].ToString();
            }

            modeNode = messageDoc.SelectSingleNode("WPAAutoDiaryFilterSetup/mode");
            if (modeNode != null)
            {
                modeNode.InnerText = ViewState["mode"].ToString();
            }
            //rupal:start, MITS 26244
            databaseNode = messageDoc.SelectSingleNode("WPAAutoDiaryFilterSetup/Database");
            if (databaseNode != null)
            {
                databaseNode.InnerText = ViewState["database"].ToString();
            }
            //rupal:end

            return messageDoc.SelectSingleNode("/");
        }

        private string GetAddEditAutoDiaryFilterTemplate()
        {
            //rupal: updated for MITS 26244, added new tag <Database>
            XElement oTemplate = XElement.Parse(@"
                <WPAAutoDiaryFilterSetup>
                  <AvlBodyParts></AvlBodyParts>
                  <SelBodyParts></SelBodyParts>
                  <templateid></templateid>
                  <id></id>
                  <filtername></filtername>
                  <SQLFill></SQLFill>
                  <FilterType></FilterType>
                  <DefValue></DefValue>
                  <data></data>
                  <mode></mode>
                  <Database></Database>
                </WPAAutoDiaryFilterSetup>
            ");
            return oTemplate.ToString();
        }

        private XmlDocument GetAddEditDiaryFilter()
        {
            string cwsOutput = string.Empty;
            XmlDocument diaryDoc = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            
            try
            {
                serviceMethodToCall = "WPAAutoDiaryFilterSetupAdaptor.Get";

                inputDocNode = GetInputDocForAddEditDiaryFilter();
                cwsOutput = CallCWSFunctionFromAppHelper(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(cwsOutput);

                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region ContactingWebService
            private string GetMessageTemplate()
            {
                XElement oTemplate = XElement.Parse(@"
                        <Message>
                          <Authorization></Authorization>
                          <Call>
                            <Function></Function>
                          </Call>
                          <Document>
                          </Document>
                        </Message>
                    ");

                return oTemplate.ToString();
            }

            public string CallCWSFunctionFromAppHelper(XmlNode inputDocumentNode, string serviceMethodToCall)
            {
                XmlNode tempNode = null;
                string oMessageElement = string.Empty;
                string responseCWS = string.Empty;

                XmlDocument messageTemplateDoc = null;

                try
                {
                    oMessageElement = GetMessageTemplate();
                    messageTemplateDoc = new XmlDocument();
                    messageTemplateDoc.LoadXml(oMessageElement);

                    if (inputDocumentNode != null)
                    {
                        tempNode = messageTemplateDoc.SelectSingleNode("Message/Document");
                        tempNode.InnerXml = inputDocumentNode.OuterXml;
                    }

                    if (!string.IsNullOrEmpty(serviceMethodToCall))
                    {
                        tempNode = messageTemplateDoc.SelectSingleNode("Message/Call/Function");
                        tempNode.InnerText = serviceMethodToCall;
                    }

                    //get webservice call from Apphelper.cs
                    responseCWS = AppHelper.CallCWSService(messageTemplateDoc.InnerXml);

                    return responseCWS;
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(ex.Message);
                }
        }
        #endregion
    }
}
