﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.ToolsDesigners
{
    public partial class AutoDiaryWorkActivity : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["selectedid"] != null)
                    {
                        ViewState["selectedid"] = Request.QueryString["selectedid"];
                    }
                    else
                    {
                        ViewState["selectedid"] = "";
                    }

                    FillWorkActivities();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void FillWorkActivities()
        {
            XmlDocument autoWorkActivityDoc = null;
            XmlNodeList workActivityList = null;

            autoWorkActivityDoc = GetAutoDiaryWorkActivities();
            if (autoWorkActivityDoc != null)
            {
                AvailableActivities.Items.Clear();
                workActivityList =  autoWorkActivityDoc.SelectNodes("ResultMessage/Document/WorkActivities/activity");

                foreach (XmlNode workActivity in workActivityList)
                {
                    AvailableActivities.Items.Add(new ListItem(workActivity.InnerText,workActivity.Attributes["id"].Value)); 
                }
            }

            this.Page.ClientScript.RegisterStartupScript(typeof(string), "fillSelectedActivities", "fillSelectedActivities()", true);

            //this.Page.RegisterStartupScript("fillSelectedActivities","fillSelectedActivities()"); 
        }

        private XmlNode GetInputDocForWorkActivites()
        {
            XmlDocument messageDoc = null;
            string messageTemplageString;

            messageTemplageString = GetAutodiaryWorkActivitiesTemplate();
            messageDoc = new XmlDocument();
            messageDoc.LoadXml(messageTemplageString);

            messageDoc.SelectSingleNode("AutoDiaryWorkActivites/DiaryId").InnerText = ViewState["selectedid"].ToString();

            return messageDoc.SelectSingleNode("/");
        }
        
        private XmlDocument GetAutoDiaryWorkActivities()
        {
            string cwsOutput = string.Empty;
            XmlDocument diaryDoc = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            
            try
            {
                serviceMethodToCall = "WPAAutoDiarySetupAdaptor.GetAutoDiaryWorkActivities";
                inputDocNode = GetInputDocForWorkActivites();
                
                cwsOutput = CallCWSFunctionFromAppHelper(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(cwsOutput);
                
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetAutodiaryWorkActivitiesTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <AutoDiaryWorkActivites>
                  <DiaryId></DiaryId >
                  <AvailableActivities></AvailableActivities>
                  <SelectedActivities ></SelectedActivities>
                  <WebserviceData></WebserviceData>
                </AutoDiaryWorkActivites>
            ");
            return oTemplate.ToString();
        }
        
        #region ContactingWebService

            private string GetMessageTemplate()
            {
                XElement oTemplate = XElement.Parse(@"
                            <Message>
                              <Authorization></Authorization>
                              <Call>
                                <Function></Function>
                              </Call>
                              <Document>
                              </Document>
                            </Message>
                        ");

                return oTemplate.ToString();
            }

            public string CallCWSFunctionFromAppHelper(XmlNode inputDocumentNode, string serviceMethodToCall)
            {
                XmlNode tempNode = null;
                string oMessageElement = string.Empty;
                string responseCWS = string.Empty;

                XmlDocument messageTemplateDoc = null;

                try
                {
                    oMessageElement = GetMessageTemplate();
                    messageTemplateDoc = new XmlDocument();
                    messageTemplateDoc.LoadXml(oMessageElement);

                    if (inputDocumentNode != null)
                    {
                        tempNode = messageTemplateDoc.SelectSingleNode("Message/Document");
                        tempNode.InnerXml = inputDocumentNode.OuterXml;
                    }

                    if (!string.IsNullOrEmpty(serviceMethodToCall))
                    {
                        tempNode = messageTemplateDoc.SelectSingleNode("Message/Call/Function");
                        tempNode.InnerText = serviceMethodToCall;
                    }

                    //get webservice call from Apphelper.cs
                    responseCWS = AppHelper.CallCWSService(messageTemplateDoc.InnerXml);

                    return responseCWS;
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(ex.Message);
                }
            }

        #endregion

    }
}
