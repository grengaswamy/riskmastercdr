﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditAutoDiaryFilter.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.AddEditAutoDiaryFilter" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Auto Diary Filter</title>
    <script language="javascript" >
                    function AddFilter(mode)
					{
						var optionRank;
						var optionObject;
						
						selectObject=document.getElementById('Selected');
						
						if (mode=="selected")
						{
							//Add selected Available Values
							select=document.getElementById('Available');
							for(var x = 0; x < select.options.length ; x++)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
									
									//remove from available list	
									removeOption('Available',select.options[x].value);							
								}
							}
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('Available');
							if (select.options.length > 0)
							{
								for(var x = 0; x < select.options.length ; x++)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
								}
								select.options.length=0;
							}
						}
						return false;
					}
					
					function RemoveFilter(mode)
					{
						var optionRank;
						var optionObject;
						
						selectObject=document.getElementById('Available');
						
						if (mode=="selected")
						{
							//Add selected Available Values
							select=document.getElementById('Selected');
							for(var x = 0; x < select.options.length ; x++)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
									
									//remove from available list	
									removeOption('Selected',select.options[x].value);							
								}
							}
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('Selected');
							if (select.options.length > 0)
							{
								for(var x = 0; x < select.options.length ; x++)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
								}
								select.options.length=0;
							}
						}
						return false;
					}
					
					function removeOption(selectName,id)
					{
						select=document.getElementById(selectName);
						Ids=new Array();
						Names=new Array();

						for(var x = 0; x < select.options.length ; x++)
						{
							if(select.options[x].value!=id)
							{
								Ids[Ids.length] = select.options[x].value;
								Names[Names.length]=select.options[x].text;
							}
						}

						select.options.length = Ids.length;

						for(var x = 0;x < select.options.length; x++)
						{
							select.options[x].text=Names[x];
							select.options[x].value=Ids[x];
						}
					}
					
					function SelectedFilters()
					{
                        if (document.forms[0].hdFilterType.value=="1")
						{
							if (document.forms[0].chkData.checked==true)
								document.forms[0].hdData.value="1";
							else
								document.forms[0].hdData.value="0";
						}
						else if (document.forms[0].hdFilterType.value=="2")
						{
							allvalue=",";
							ctrl=document.getElementById('Selected');
	
							for(var f=0; f < ctrl.options.length; f++)
							{
								allvalue=allvalue + ctrl.options[f].value + ",";
							}
							ctrl=document.getElementById('hdData');
							ctrl.value=allvalue;
						}
						else if (document.forms[0].hdFilterType.value=="0" || document.forms[0].hdFilterType.value=="3" || document.forms[0].hdFilterType.value=="5")
						{	
							document.forms[0].hdData.value=document.forms[0].txtData.value;								
						}
						
                        window.opener.RefreshPageAfterAddEditFilter(document.forms[0].hdData.value);
						window.close();
						
						return true;
					}
					
					function IsNumeric()
					{
					    var k = window.event.keyCode; if (!((k > 47 && k < 58) || k == 46 || k == 45)) { window.event.returnValue = false; if (window.event.preventDefault) window.event.preventDefault(); }
					}
					
					function checkWindow()
					{
						if (document.forms[0].FormMode.value=="close")
						{
						    window.opener.document.location.reload(true);
                            window.close();
						}
					}
	</script>
</head>
<body>
    <form id="frmData" runat="server">
        <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
             <td colspan="2">
                <div id="divForCheckBox" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                       <td align="center">
                            <input type="checkbox" id="chkData" runat="server"  value="1" />
                            <asp:Label ID="lblForChkData" runat="server" Text=""></asp:Label>
                            <br/>
                       </td> 
                    </tr>
                </table> 
              </div>
                <div id="divForList" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                       <tr>
                            <td width="40%">
			                    Available&nbsp;<asp:Label ID="lblFilterNameForList" runat="server"></asp:Label><br/>
			                    <select size="10" style="width:90%" id="Available" runat="server" ondblclick="return AddFilter('selected');">
			                    </select>
                            <td width="20%" valign="middle" align="center">
                                <input type="button" value="Add &gt;" class="button"  style="width:80px" 
                                    onclick="return AddFilter('selected');; "/>
                                <br><br>
                                <input type="button" value="Add All &gt;" class="button" style="width:80px" onclick="return AddFilter('all');; "/>
                                <br><br>
                                <input type="button" value="< Remove" class="button" style="width:80px" onclick="return RemoveFilter('selected');; "/>
                                <br><br>
                                <input type="button" value="< Remove All" class="button" style="width:80px" onclick="return RemoveFilter('all');; "/>
                            </td>
                            <td width="40%">
					            Selected&nbsp;<asp:Label ID="lblFilterNameForList1" runat="server"></asp:Label>
					            <br/>
					            <select size="10" style="width:90%" id="Selected" runat="server" ondblclick="return RemoveFilter('selected');"></select>
				            </td>
                       </tr>
                    </table>
                </div>
                <div id="divForTextBox" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
				        <tr>
					        <td align="center">
					            <asp:Label ID="lblFilterNameForTextBox" runat="server"></asp:Label>
					            <br />
					            <input type="text" id="txtData" runat="server" onkeypress="IsNumeric();" value=""/>
					            <br /><br />
					        </td> 
					    </tr> 
				    </table> 
              </div>
             </td>
            </tr>
            <tr>
             <td class="group" colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="hidden" value="2" runat="server" id="hdFilterType">
                    <input type="hidden" value="" runat="server"  id="hdData">
                </td>
            </tr>
            <tr>
                 <td colspan="2" align="center">
                    <input type="button" value="Ok" class="button" style="width:80px" onclick="return SelectedFilters();; ">  
                    <input type="button" value="Cancel" class="button" style="width:80px" onclick="javascript:window.close();return false;; ">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
