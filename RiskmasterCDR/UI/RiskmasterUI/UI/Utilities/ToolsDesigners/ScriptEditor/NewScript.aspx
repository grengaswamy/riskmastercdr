﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="NewScript.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.ScriptEditor.NewScript" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Add New Script</title>
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/ScriptEditor.js"></script>
</head>
<body class="10pt" width="100%">
    <form id="frmData" runat="server">
    <table border="0" width="100%">
		<tr>
			<td class="msgheader" colspan="4" bgcolor="#D5CDA4">Add New Script</td>							
		</tr>
		<tr width="40%"><br/>
		    <td width="10%"></td>
			<td width="30%">Script Type :</td>
			<td width="60%">
			<asp:DropDownList ID="cboScriptType" runat="server" rmxref="/Instance/Document/ScriptType" onchange="FilterObjectTypes();">
			    <asp:ListItem Value="" Text="" />
			    <asp:ListItem Value="1" Text="Validation" />
			    <asp:ListItem Value="2" Text="Calculation" />
			    <asp:ListItem Value="3" Text="Initialization" />
			    <asp:ListItem Value="4" Text="Before Save" />
			    <asp:ListItem Value="5" Text="After Save" />
			    <asp:ListItem Value="6" Text="Before Delete" />
			</asp:DropDownList>
			</td> 
		</tr>
		<tr>
			<td></td>
			<td valign="top">Object Type :</td>
			<td>
			    <asp:ListBox ID="cboObjectType" runat="server" rmxref="/Instance/Document/ObjectType" />
			</td>
		</tr>
		<tr>
			<td></td>
			<td colspan="2">
				<br/>
				<asp:Button  class="button" id="btnNewScriptOk" Text =" OK " runat="server" OnClientClick ="return NewScriptOk();" Width ="50px" />
			</td>
		</tr>  
	</table>  
    </form>
</body>
</html>
