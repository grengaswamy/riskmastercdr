﻿<%@ Page Language="C#" AutoEventWireup="True" ValidateRequest="false" CodeBehind="ScriptView.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.ScriptEditor.ScriptView" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reports</title>
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <%-- RMA-6037 - vkumar258 - Starts --%>
    <%-- <script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>--%>
    <link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js" >        { var i; } </script>
    <%-- RMA-6037 - vkumar258 - End --%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/ScriptEditor.js"></script>
    <style type="text/css">
        .scriptTextBox
        {
            position:absolute;
            top: 17%;
            left: 0%;
            width: 97%;
            height: 80%;
            margin: 0px 0px 0px 0px;
            padding: 0.1em 0.5em 0.1em 0.5em;
        }
    </style>
</head>
<body onload="ScriptViewOnLoad();">
    <form id="frmData" name="frmData" runat="server" method="post" onsubmit="return OnFormSubmit();" >
        <table>
            <tr>
              <td colspan="2">
                <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                
               </td>
            </tr> 
        </table>  
          <asp:TextBox RMXref="/Instance/Document/ScriptEditor/LoadScriptRoot/SelectedTab" id="hTabName" runat="server" style="display:none"/>
          <asp:TextBox RMXref="/Instance/Document/ScriptEditor/LoadScriptRoot/FunctionToCall" id="hFunctionToCall" runat="server" style="display:none"/>
		  <asp:TextBox name="SysFocusFields" id="SysFocusFields" runat="server" style="display:none"/>
		  <asp:TextBox RMXref="/Instance/Document/ScriptEditor/LoadScriptRoot/RowId" id="RowId" runat="server" style="display:none"/>
		  <asp:TextBox RMXref="/Instance/Document/ScriptEditor/LoadScriptRoot/ScriptType" id="ScriptType" runat="server" style="display:none"/>
		  <asp:TextBox RMXref="/Instance/Document/ScriptEditor/LoadScriptRoot/ObjectName" id="ObjectName" runat="server" style="display:none"/>
        <div class="msgheader" id="div_formtitle" runat="server">
            <asp:label id="formtitle" runat="server" text="Script Editor" /><asp:label id="formsubtitle"
                runat="server" text="" />
        </div>
        <div id="Div1" class="tabGroup" runat="server">
            <div class="Selected" nowrap="true" runat="server" name="TABSProperties" id="TABSProperties">
                <a class="Selected" href="#" onclick="tabChange(this.name);return false;" runat="server" rmxref=""
                    name="Properties" id="LINKTABSProperties">Properties</a>
            </div>
            <div id="Div2" class="tabSpace" runat="server">
                &nbsp;&nbsp;</div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSSourceCode" id="TABSSourceCode">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server" rmxref=""
                    name="SourceCode" id="LINKTABSSourceCode">SourceCode</a>
            </div> 
            <div class="tabSpace">
            &nbsp;&nbsp;</div>
        </div> 
        <table id="FORMTABProperties" border="0" cellspacing="0" cellpadding="0" >
        <tr align="left">
            <td>Author:&nbsp;&nbsp;</td>
            <td>  
                <asp:TextBox ID="author" runat="server" onchange="setDataChanged(true);parent.setDataChanged(true);" rmxref="/Instance/Document/ScriptEditor/LoadScriptRoot/Author"/>               
            </td> 
        </tr> 
        <tr align="left">
            <td>Created on:&nbsp;&nbsp;</td>
            <td>  
                <asp:TextBox runat="server" FormatAs="date" onchange="setDataChanged(true);parent.setDataChanged(true);" id="createddate" RMXType="date" rmxref="/Instance/Document/ScriptEditor/LoadScriptRoot/CreatedOn" />
                    <%--vkumar258 - RMA-6037 - Starts --%>
                    <%--<input type="button" class="DateLookupControl" name="createddatebtn" />
                <script type="text/javascript">
                    Zapatec.Calendar.setup(
					    {
					        inputField: "createddate",
					        ifFormat: "%m/%d/%Y",
					        button: "createddatebtn"
					    }
					    );
			    </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#createddate").datepicker({
                                showOn: "button",
                                buttonImage: "../../../../Images/calendar.gif",
                               // buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                        });
                    </script>
                    <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <tr align="left">
                <td>Modified By:&nbsp;&nbsp;</td>
                <td>
                    <asp:textbox id="modifiedby" runat="server" onchange="setDataChanged(true);parent.setDataChanged(true);" rmxref="/Instance/Document/ScriptEditor/LoadScriptRoot/ModifiedBy" />
                </td>
            </tr>
            <tr align="left">
                <td>Modified on:&nbsp;&nbsp;</td>
                <td>
                    <asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);parent.setDataChanged(true);" id="modifiedon" rmxtype="date" rmxref="/Instance/Document/ScriptEditor/LoadScriptRoot/ModifiedOn" />
                    <%--vkumar258 - RMA-6037 - Starts --%>
                    <%--<input type="button" class="DateLookupControl" name="modifiedonbtn" />
                <script type="text/javascript">
                    Zapatec.Calendar.setup(
					    {
					        inputField: "modifiedon",
					        ifFormat: "%m/%d/%Y",
					        button: "modifiedonbtn"
					    }
					    );
			    </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#modifiedon").datepicker({
                                showOn: "button",
                                buttonImage: "../../../../Images/calendar.gif",
                                //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                        });
                    </script>
                    <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" style="display: none;" id="FORMTABSourceCode">
            <tr align="left">
				<td>
				    <asp:TextBox TextMode="MultiLine" id="txtScript" onchange="setDataChanged(true);parent.setDataChanged(true);" rmxref="/Instance/Document/ScriptEditor/LoadScriptRoot/ScriptText" CssClass="scriptTextBox" runat="server"/>
				</td>
			</tr>
        </table> 
        <uc1:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading"/>
    </form>
</body>
</html>
