﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ToolBar.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.ScriptEditor.ToolBar" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title></title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/ScriptEditor.js"></script>
    </head>
<body class="10pt" onload="Javascript : ToolBarOnLoad()">
   <form name="frmToolbar" title="" runat="server" >
       <div  >
           <td align="center" valign="middle" HEIGHT="32" WIDTH="28">    
                <asp:imagebutton runat="server" onclientclick="return SaveScript();return false;" 
                        src="../../../../Images/save.gif" width="28" height="28" border="0" id="save" title="Save Changed Script" alternatetext="Save Changed Script"
                        onmouseover="this.src='../../../../Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../../../Images/save2.gif';this.style.zoom='100%'" /> 
           </td> 
           <td align="center" valign="middle" HEIGHT="32" WIDTH="28">    
                        <asp:imagebutton runat="server" onclientclick="RefreshPage();return false;" 
                        src="../../../../Images/refresh.gif" width="28" height="28" border="0" id="refresh" title="Cancel any changes and Reload data" alternatetext="Cancel any changes and Reload data"
                        onmouseover="this.src='../../../../Images/refresh1.gif';this.style.zoom='110%'" onmouseout="this.src='../../../../Images/refresh.gif';this.style.zoom='100%'" />    
           </td> 
           <td align="center" valign="middle" HEIGHT="32" WIDTH="28">  
                        <asp:imagebutton runat="server" onclientclick="DeleteScript();return false;"
                        src="../../../../Images/delete.gif" width="28" height="28" border="0" id="delete" title="Delete Selected Script" alternatetext="Delete Selected Script"
                        onmouseover="this.src='../../../../Images/delete2.gif';this.style.zoom='110%'" onmouseout="this.src='../../../../Images/delete.gif';this.style.zoom='100%'" />    

           </td> 
           <td align="center" valign="middle" HEIGHT="32" WIDTH="28">
                        <asp:imagebutton runat="server" onclientclick="AddNewScript();return false;" 
                        src="../../../../Images/new.gif" width="28" height="28" border="0" id="Addnew" title="Add New Script" alternatetext="Add New Script"
                        onmouseover="this.src='../../../../Images/new2.gif';this.style.zoom='110%'" onmouseout="this.src='../../../../Images/new.gif';this.style.zoom='100%'" />    

           </td>
       </div>    
              <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted">
       </asp:TextBox> 
	</form>
</body>
</html>


