﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScriptEditor.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.ScriptEditor.ScriptEditor" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1"  runat="server">
    <title>Script Editor</title>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/ScriptEditor.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="form1" runat="server">
    <div>
    <iframe id="ToolBar" runat="server" align="top" height="50px" width="100%" scrolling="no" src="ToolBar.aspx">
    </iframe>
    <iframe id="LeftTree" runat="server" align="left" height="350px" width="24.2%" scrolling="auto" src="LeftTreeLoad.aspx">
    </iframe>
    <%--aravi5 Fixes for Jira Id: 7043 Script editor page is not loading properly--%>
    <iframe id="MainFrame" runat="server" align="right" height="350px" width="73%" scrolling="no" src="NoData.aspx" style="border: none;">
    </iframe>
    </div>
    </form>
</body>
</html>