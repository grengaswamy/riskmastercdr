﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;

namespace Riskmaster.UI.Utilities.ToolsDesigners.ScriptEditor
{
    public partial class LeftTreeLoad : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //registring onclick event for treeview
                //this event would handle all kinds of client side manipulations 
                TreeScript.Attributes.Add("onclick", "return OnTreeClick(event)");

                
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";
                XmlDocument objXml = new XmlDocument();
                DataTable objTable = new DataTable();
                XslCompiledTransform commentXSL = new XslCompiledTransform();
                String sScriptType = String.Empty;
                TreeNode treenode = new TreeNode();
                String sSelected = String.Empty;
                //Bkuzhanthaim : RMA-7043 :Script editor page is not loading properly in  Chrome
                if (AppHelper.GetQueryStringValue("hdnAction") == "postback")
                {
                    DeleteScript(AppHelper.GetQueryStringValue("DeleteRowid"));
                }
                if (!IsPostBack)
                {
                    FocusScriptId.Text = AppHelper.GetQueryStringValue("rowid");  
                    XmlTemplate = GetMessageTemplate(FocusScriptId.Text, FocusScriptType.Text);
                    bReturnStatus = CallCWSFunction("ScriptEditorAdaptor.LoadTreeXml", out sreturnValue, XmlTemplate);
                    if (bReturnStatus)
                    {
                        sSelected = AppHelper.GetQueryStringValue("rowid");  
                        objXml.LoadXml(sreturnValue);
                        TreeNode childNode = null;
                        XmlNodeList scriptNodeList = objXml.SelectNodes("//ScriptEditorTreeRoot/TreeNode");
                        foreach (XmlNode scriptNode in scriptNodeList)
                        {
                            switch (scriptNode.Attributes["ScriptType"].Value)
                            {
                                case "1": ValidationList.Text = scriptNode.Attributes["SelectedObjectsList"].Value;
                                    break;

                                case "2": CalculationList.Text = scriptNode.Attributes["SelectedObjectsList"].Value;
                                    break;

                                case "3": InitializationList.Text = scriptNode.Attributes["SelectedObjectsList"].Value;
                                    break;

                                case "4": BeforeSaveList.Text = scriptNode.Attributes["SelectedObjectsList"].Value;
                                    break;

                                case "5": AfterSaveList.Text = scriptNode.Attributes["SelectedObjectsList"].Value;
                                    break;

                                case "6": BeforeDeleteList.Text = scriptNode.Attributes["SelectedObjectsList"].Value;
                                    break;

                            }
                            
                            if (scriptNode.HasChildNodes)
                            {
                                foreach (XmlNode xNode in scriptNode.ChildNodes)
                                {
                                    childNode = new TreeNode(xNode.SelectSingleNode("@ObjectName").Value, xNode.SelectSingleNode("@RowId").Value);
                                    childNode.ImageUrl = "~/Images/book02.gif";
                                    sScriptType = xNode.SelectSingleNode("@ScriptType").Value;
                                    TreeNode searchNode = TreeScript.FindNode("0").ChildNodes[Convert.ToInt32(sScriptType) - 1];
                                    if (xNode.SelectSingleNode("@RowId").Value == sSelected)
                                    {
                                        childNode.Selected = true;
                                    }
                                    searchNode.ChildNodes.Add(childNode);
                                    if (xNode.SelectSingleNode("@RowId").Value == sSelected)
                                    {
                                        searchNode.ExpandAll();
                                    }
                                }
                            }

                        }
                    }

                }
                else
                {
                    if (hdnAction.Value == "Delete")
                    {
                        DeleteScript(RowID.Value);
                    }
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
               // ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate(string sScripId, string sScriptType)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ScriptEditor>");
            sXml = sXml.Append("<FocusScriptId >");
            sXml = sXml.Append(sScripId);
            sXml = sXml.Append("</FocusScriptId><FocusScriptType>");
            sXml = sXml.Append(sScriptType);
            sXml = sXml.Append("</FocusScriptType></ScriptEditor></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void DeleteScript(string sRowID)
        {
            bool bReturnStatus = false;

            string sReturn = string.Empty;
            StringBuilder sXml = new StringBuilder("<Message><Authorization></Authorization><Call><Function></Function></Call><Document><ScriptEditor>");
            sXml = sXml.Append("<RowId>" + sRowID + "</RowId>");
            sXml = sXml.Append("</ScriptEditor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            bReturnStatus = CallCWS("ScriptEditorAdaptor.DeleteScript", oTemplate, out sReturn, false, false);
        }
    }
}
