﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.ToolsDesigners.ScriptEditor
{
    public partial class ScriptView : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sFunctionname = string.Empty;
                string sReturn = string.Empty;
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes
                if (hFunctionToCall.Text == "ScriptEditorAdaptor.SaveScript")
                {
                    sFunctionname = hFunctionToCall.Text;
                    bReturnStatus = CallCWS(sFunctionname, null, out sReturn, true, true);
                }
                else
                {
                    sFunctionname = AppHelper.GetQueryStringValue("functiontocall");
                    RowId.Text = AppHelper.GetQueryStringValue("RowId");
                    ScriptType.Text = AppHelper.GetQueryStringValue("scripttype");
                    ObjectName.Text = AppHelper.GetQueryStringValue("objectname");
                    XmlTemplate = GetMessageTemplate(RowId.Text, ScriptType.Text, ObjectName.Text, sFunctionname);
                    bReturnStatus = CallCWS(sFunctionname, XmlTemplate, out sReturn, false, true);
                }
                
                if (bReturnStatus)
                {
                    formsubtitle.Text = CreateTitle(ScriptType.Text, ObjectName.Text);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate(string sRowID, string sScriptType, string sObjectName, string sFunctionName)
        {
            StringBuilder sXml = new StringBuilder("<Message><Authorization></Authorization><Call><Function></Function></Call><Document><ScriptEditor>");
            if (sFunctionName == "ScriptEditorAdaptor.LoadScript")
            {
                sXml = sXml.Append("<RowId>" + RowId.Text + "</RowId>");
            }
            else if (sFunctionName == "ScriptEditorAdaptor.NewScript")
            {
                sXml = sXml.Append("<ObjectName>" + sObjectName + "</ObjectName><ScriptType>" + sScriptType + "</ScriptType>" );
            }
            sXml = sXml.Append("</ScriptEditor></Document></Message>"); 
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        private string CreateTitle( string sScriptId, string sObjectName )
        {
            string sScriptType = string.Empty;
            switch (sScriptId )
            {
                case "0" :
                        sScriptType ="Script Editor";
                        break;
                case "1" :
                        sScriptType = "Validation";
                        break;
                case "2" :
                        sScriptType = "Calculation";
                        break;
                case "3" :
                        sScriptType = "Initialization";
                        break;
                case "4" :
                        sScriptType = "Before Save";
                        break;
                case "5" :
                        sScriptType = "After Save";
                        break;
                case "6":
                        sScriptType = "Before Delete";
                        break;

            }
            StringBuilder sSubTitle = new StringBuilder();
            sSubTitle = sSubTitle.Append(" [ " + sScriptType + " ][ " + sObjectName +" ]");
            return sSubTitle.ToString();  
        }

       
    }
}
