﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using System.Text;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners
{
    public partial class InitialPreFillFolderUtilityPage : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    XElement XmlTemplate = null;
                    XmlDocument xmlServiceDoc = new XmlDocument();
                    bool bReturnStatus = false;
                    string sCWSresponse;
                    string sDocumentPath;

                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWSFunction("AcrosoftUtilitiesAdaptor.VerifyAcrosoftStatus", out sCWSresponse, XmlTemplate);
                    xmlServiceDoc.LoadXml(sCWSresponse);

                    if ((xmlServiceDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
                    {
                        if ((xmlServiceDoc.SelectSingleNode("//CurrentStatus").InnerText != "True"))
                        {
                            lblStatusMessage.Text = "MCM is disabled. Please contact RISKMASTER sales if you are interested in purchasing the RISKMASTER MCM Interface.";
                            btnOk.Visible = false;
                        }
                        else
                        {
                            lblDocumentPathType_label.Text = "Document Path Type : ";
                            lblDocumentPath_label.Text = "Document Path : ";
                            lblStatusMessage.Text = "This Utility would copy all existing documents from your document path to the MCM server. Kindly verify the information below and press Continue.";
                            XmlNode objnode = xmlServiceDoc.SelectSingleNode("ResultMessage/Document/AcrosoftStatus/DocumentPathType");
                            XmlNode objPathNode = xmlServiceDoc.SelectSingleNode("ResultMessage/Document/AcrosoftStatus/DocumentPath");
                            sDocumentPath = objPathNode.InnerText;
                            if (objnode.InnerText == "0")
                            {
                                lblDocumentPathType.Text = "File Server";
                                lblDocumentPath.Text = sDocumentPath;
                            }
                            else
                            {
                                lblDocumentPathType.Text = "Database Server";
                                if (sDocumentPath != "")
                                {
                                    string[] sDatabase = sDocumentPath.Split(new Char[] { ';' });
                                    lblDocumentPath.Text = sDatabase[0];
                                }
                            }
                            lblNote.Text = "Note: This may take a minute or two... Please do not close this window till you get a pdf report on the upload.";
                        }

                    }
                }
                else
                {
                    if (Mode.Value == "Submit")
                    {
                        PreFillFolders();
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

      

        private void PreFillFolders()
        {
            try
            {
                XElement XmlTemplate = null;
                bool bReturnStatus = false;
                string sCWSresponse;
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWSFunction("AcrosoftUtilitiesAdaptor.PreFillFolders", out sCWSresponse, XmlTemplate);
                XmlTemplate = XElement.Parse(sCWSresponse.ToString());

                XElement oElement = XmlTemplate.XPathSelectElement("//AcrosoftUploadReport/Report");
                if (oElement != null)
                {
                    string sFileContent = oElement.Value;
                    byte[] byteOrg = Convert.FromBase64String(sFileContent);

                    Response.Clear();
                    Response.Charset = "";
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "inline;filename=MCMUploadDocumentReport.pdf");
                    Response.BinaryWrite(byteOrg);
                    Response.End();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<Form>");
            sXml = sXml.Append("</Form></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

    }
}
