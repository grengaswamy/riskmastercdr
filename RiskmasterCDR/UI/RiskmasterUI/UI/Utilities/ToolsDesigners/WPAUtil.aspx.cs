﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.Utilities.ToolsDesigners
{
    public partial class WPAUtil : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument userDoc = null;
            XmlNode ownersNode = null;

            try
            {
                //**ksahu5-ML-MITS34183 Start **
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                   
                }

                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("WPAUtil.aspx"), "WPAUtilValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "WPAUtilValidations", sValidationResources, true);

                //**ksahu5-ML-MITS34183 End **
                if (!IsPostBack)
                {
                    userDoc = GetWPAUtilUsersList();

                    if (userDoc != null)
                    {
                        ownersNode = userDoc.SelectSingleNode("ResultMessage/Document/WPAUtil/Owners");

                        PopulateListControlsWithUersList(ownersNode, lstFromUsers);

                        PopulateListControlsWithUersList(ownersNode, lstToUsers);

                        PopulateListControlsWithUersList(ownersNode, lstUsers);
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnGoTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                TransferDiary();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnGoPurge_Click(object sender, EventArgs e)
        {
            try
            {
                PurgeDiary();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        #region Get and Populate WPA Util Users List

            private XmlDocument GetWPAUtilUsersList()
            {
                string cwsOutput = string.Empty;
                XmlDocument diaryDoc = null;
                XmlNode inputDocNode = null;
                string serviceMethodToCall = string.Empty;

                try
                {
                    serviceMethodToCall = "WPAUtilAdaptor.Get";
                    inputDocNode = GetInputDocForWPAUtil();

                    cwsOutput = CallCWSFunctionFromAppHelper(inputDocNode, serviceMethodToCall);

                    diaryDoc = new XmlDocument();
                    diaryDoc.LoadXml(cwsOutput);

                    return diaryDoc;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            private XmlNode GetInputDocForWPAUtil()
            {
                XmlDocument messageDoc = null;
                string messageTemplageString = string.Empty;

                messageTemplageString = GetWPAUtilTemplate();
                messageDoc = new XmlDocument();
                messageDoc.LoadXml(messageTemplageString);

                return messageDoc.SelectSingleNode("/");
            }

            private string GetWPAUtilTemplate()
            {
                XElement oTemplate = XElement.Parse(@"
                   <WPAUtil /> 
                 ");
                return oTemplate.ToString();
            }

            private void PopulateListControlsWithUersList(XmlNode ownersNode,ListControl lstControl)
            {
                XmlNodeList usersNodeList = null;
                ListItem lstItem = null;

                if (ownersNode != null)
                {
                    usersNodeList = ownersNode.SelectNodes("User");
                    lstControl.Items.Clear();

                    foreach (XmlNode userNode in usersNodeList)
                    {
                        lstItem = new ListItem();

                        lstItem.Value = userNode.Attributes["userid"].Value;
                        lstItem.Text = userNode.InnerText;

                        lstControl.Items.Add(lstItem);
                    }
                }
            }

        #endregion

        #region Transfer Diary

            private string GetTransferDiaryTemplate()
            {
                XElement oTemplate = XElement.Parse(@"
                   <WPAUtil>
                      <Transfer>
                        <FromUser></FromUser>
                        <ToUser></ToUser>
                        <DiaryStatus>
                          <Open></Open>
                          <Close></Close>
                        </DiaryStatus>
                         <Dates>
                          <From></From>
                          <To></To>
                        </Dates>
                      </Transfer>   
                    </WPAUtil>
                 ");
                return oTemplate.ToString();
            }

            private XmlNode GetInputDocToTransferDiary()
            {
                XmlDocument messageDoc = null;
                string messageTemplageString = string.Empty;

                messageTemplageString = GetTransferDiaryTemplate();
                messageDoc = new XmlDocument();
                messageDoc.LoadXml(messageTemplageString);

                if(lstFromUsers.SelectedItem != null)
                {
                    messageDoc.SelectSingleNode("//FromUser").InnerText = lstFromUsers.SelectedItem.Value;
                }

                if (lstToUsers.SelectedItem != null)
                {
                    messageDoc.SelectSingleNode("//ToUser").InnerText = lstToUsers.SelectedItem.Value;
                }

                if(chkTransferOpen.Checked)
                {
                    messageDoc.SelectSingleNode("//DiaryStatus//Open").InnerText = "True";
                }
                
                if(chkTransferClose.Checked)
                {
                    messageDoc.SelectSingleNode("//DiaryStatus//Close").InnerText = "True";
                }

                messageDoc.SelectSingleNode("//Dates//From").InnerText = txtTransferFromDate.Value;
                
                messageDoc.SelectSingleNode("//Dates//To").InnerText = txtTransferToDate.Value; 


                return messageDoc.SelectSingleNode("/");
            }

            private XmlDocument TransferDiary()
            {
                string cwsOutput = string.Empty;
                XmlDocument diaryDoc = null;
                XmlNode inputDocNode = null;
                string serviceMethodToCall = string.Empty;

                try
                {
                    serviceMethodToCall = "WPAUtilAdaptor.SaveDiaryTransfer";
                    inputDocNode = GetInputDocToTransferDiary();

                    cwsOutput = CallCWSFunctionFromAppHelper(inputDocNode, serviceMethodToCall);

                    diaryDoc = new XmlDocument();
                    diaryDoc.LoadXml(cwsOutput);

                    return diaryDoc;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        #endregion

        #region Purge Diary

            private string GetPurgeDiaryTemplate()
            {
                XElement oTemplate = XElement.Parse(@"
                    <WPAUtil>
                        <Purge>
                            <User />
                            <DiaryStatus>
                                <Open></Open>
                                <Close />
                            </DiaryStatus>
                            <Dates>
                                <From />
                                <To />
                            </Dates>
                        </Purge>
                    </WPAUtil>
                 ");

                return oTemplate.ToString();
            }

            private XmlNode GetInputDocToPurgeDiary()
            {
                XmlDocument messageDoc = null;
                string messageTemplageString = string.Empty;
                System.Text.StringBuilder  selectedUsersId  = null;

                messageTemplageString = GetPurgeDiaryTemplate();

                messageDoc = new XmlDocument();
                messageDoc.LoadXml(messageTemplageString);

                selectedUsersId = new System.Text.StringBuilder();

                foreach (ListItem lstItem in lstUsers.Items)
                {
                    if (lstItem.Selected == true)
                    {
                        selectedUsersId.Append(lstItem.Value + " ");
                    }
                }

                messageDoc.SelectSingleNode("//User").InnerText = selectedUsersId.ToString().Trim();
                
                if (chkPurgeOpen.Checked)
                {
                    messageDoc.SelectSingleNode("//DiaryStatus//Open").InnerText = "True";
                }

                if (chkPurgeClose.Checked)
                {
                    messageDoc.SelectSingleNode("//DiaryStatus//Close").InnerText = "True";
                }

                messageDoc.SelectSingleNode("//Dates//From").InnerText = txtPurgeFromDate.Value; 

                messageDoc.SelectSingleNode("//Dates//To").InnerText = txtPurgeToDate.Value;


                return messageDoc.SelectSingleNode("/");
            }

            private XmlDocument PurgeDiary()
            {
                string cwsOutput = string.Empty;
                XmlDocument diaryDoc = null;
                XmlNode inputDocNode = null;
                string serviceMethodToCall = string.Empty;

                try
                {
                    serviceMethodToCall = "WPAUtilAdaptor.SaveDiaryPurge";
                    inputDocNode = GetInputDocToPurgeDiary();

                    cwsOutput = CallCWSFunctionFromAppHelper(inputDocNode, serviceMethodToCall);

                    diaryDoc = new XmlDocument();
                    diaryDoc.LoadXml(cwsOutput);

                    return diaryDoc;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        #endregion

        #region ContactingWebService

            private string GetMessageTemplate()
            {
                XElement oTemplate = XElement.Parse(@"
                        <Message>
                          <Authorization></Authorization>
                          <Call>
                            <Function></Function>
                          </Call>
                          <Document>
                          </Document>
                        </Message>
                    ");

                return oTemplate.ToString();
            }

            public string CallCWSFunctionFromAppHelper(XmlNode inputDocumentNode, string serviceMethodToCall)
            {
                XmlNode tempNode = null;
                string oMessageElement = string.Empty;
                string responseCWS = string.Empty;

                XmlDocument messageTemplateDoc = null;

                try
                {
                    oMessageElement = GetMessageTemplate();
                    messageTemplateDoc = new XmlDocument();
                    messageTemplateDoc.LoadXml(oMessageElement);

                    if (inputDocumentNode != null)
                    {
                        tempNode = messageTemplateDoc.SelectSingleNode("Message/Document");
                        tempNode.InnerXml = inputDocumentNode.OuterXml;
                    }

                    if (!string.IsNullOrEmpty(serviceMethodToCall))
                    {
                        tempNode = messageTemplateDoc.SelectSingleNode("Message/Call/Function");
                        tempNode.InnerText = serviceMethodToCall;
                    }

                    //get webservice call from Apphelper.cs
                    responseCWS = AppHelper.CallCWSService(messageTemplateDoc.InnerXml);

                    return responseCWS;
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(ex.Message);
                }
            }

        #endregion
            
    }
}
