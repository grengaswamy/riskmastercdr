﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WPAAutoDiary.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.WPAAutoDiary" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>WPA Auto Diary Setup</title>
    <script type="text/javascript" src="../../../Scripts/form.js"></script>
    
    <script language="javascript">
    
        function SetImageOnMouseOver(selectedImageControlId) 
        {
            var selectedControl = document.getElementById(selectedImageControlId);

            if (selectedImageControlId == 'New_WPAAutoDiaryList') 
            {
                selectedControl.src = '../../../Images/new2.gif';
            }
            else if (selectedImageControlId == 'Edit_WPAAutoDiaryList')
            {
                selectedControl.src = '../../../Images/edittoolbar2.gif';
            }
            else if (selectedImageControlId == 'Delete_WPAAutoDiaryList') 
            {
                selectedControl.src = '../../../Images/delete2.gif';
            }
        }

        function SetImageOnMouseOut(selectedImageControlId) 
        {
            var selectedControl = document.getElementById(selectedImageControlId);

            if (selectedImageControlId == 'New_WPAAutoDiaryList') 
            {
                selectedControl.src = '../../../Images/new.gif';
            }
            else if (selectedImageControlId == 'Edit_WPAAutoDiaryList') 
            {
                selectedControl.src = '../../../Images/edittoolbar.gif';
            }
            else if (selectedImageControlId == 'Delete_WPAAutoDiaryList') 
            {
                selectedControl.src = '../../../Images/delete.gif';
            }
        }
        
    </script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
        <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
        <div class="msgheader" id="formtitle">
        <%--YUktiML Changes Start--%>
            <%--WPA Auto Diary Setup--%>
            <asp:Label runat="server" ID="lblWPADiary" Text="<%$ Resources:lblWPADiary %>" />
        <%--Yukti-ML Changes End--%>
        </div>
         <table border="0" cellspacing="0" cellpadding="0" width="100%" 
            style="height: 500px">
             <tr>
                <td width="70%" align="left" valign="top">
                    <br />
                    <%--Yukti-ML Changes Start, MITS 34221--%>
                        <%--Current Auto Diary Definitions--%>
                    <asp:Label runat="server" ID="lblCurrentAutoDiary" Text="<%$ Resources:lblCurrentAutoDiary %>" />
                    <%--Yukti-ML Changes End--%>
      			    <br />
                    <asp:ListBox ID="WPAAutoDiaryList" runat="server" Width="95%"  Visible="true" ondblclick="return openGridAddEditWindow(&#34;WPAAutoDiaryList&#34;,&#34;edit&#34;,600,500);" Rows="20"></asp:ListBox>
                </td>
                <td width="40%" valign="top"><br>
                    <table width="100%" valign="top" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                            <%--Yukti-ML Changes Start, MITS 34221--%>
                                <%--<input type="image"  src="../../../Images/new.gif" alt="" id="New_WPAAutoDiaryList" onmouseover="SetImageOnMouseOver('New_WPAAutoDiaryList')" onmouseout="SetImageOnMouseOut('New_WPAAutoDiaryList')" title="New"   onclick="return openGridAddEditWindow('WPAAutoDiaryList','add','600','560')" />--%>
                                <input type="image" runat="server"  src="../../../Images/new.gif" alt="" id="New_WPAAutoDiaryList" onmouseover="SetImageOnMouseOver('New_WPAAutoDiaryList')" onmouseout="SetImageOnMouseOut('New_WPAAutoDiaryList')" title="<%$ Resources:ttNew %>"   onclick="return openGridAddEditWindow('WPAAutoDiaryList', 'add', '600', '560')" />
                                <br />
                                <%--<input type="image" src="../../../Images/edittoolbar.gif" alt="" id="Edit_WPAAutoDiaryList" onmouseover="SetImageOnMouseOver('Edit_WPAAutoDiaryList')" onmouseout="SetImageOnMouseOut('Edit_WPAAutoDiaryList')" title="Edit" onclick="return openGridAddEditWindow('WPAAutoDiaryList','edit','600','500')" />--%>
                                <input type="image" runat="server" src="../../../Images/edittoolbar.gif" alt="" id="Edit_WPAAutoDiaryList" onmouseover="SetImageOnMouseOver('Edit_WPAAutoDiaryList')" onmouseout="SetImageOnMouseOut('Edit_WPAAutoDiaryList')" title="<%$ Resources:ttEdit %>" onclick="return openGridAddEditWindow('WPAAutoDiaryList', 'edit', '600', '500')" />
                                <br />
                                    <%--<asp:ImageButton ImageUrl="../../../Images/delete.gif" ID="Delete_WPAAutoDiaryList" runat="server" onmouseover="SetImageOnMouseOver('Delete_WPAAutoDiaryList')" onmouseout="SetImageOnMouseOut('Delete_WPAAutoDiaryList')" title="Delete" 
                                    OnClientClick="return validateGridForDeletion('WPAAutoDiaryList')" onclick="Delete_WPAAutoDiaryList_Click" />--%>
                                    <asp:ImageButton ImageUrl="../../../Images/delete.gif" ID="Delete_WPAAutoDiaryList" runat="server" onmouseover="SetImageOnMouseOver('Delete_WPAAutoDiaryList')" onmouseout="SetImageOnMouseOut('Delete_WPAAutoDiaryList')" title="<%$ Resources:ttDelete %>" 
                                    OnClientClick="return validateGridForDeletion('WPAAutoDiaryList')" onclick="Delete_WPAAutoDiaryList_Click" />
                                <%--Yukti-ML Changes End--%>
                                <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
