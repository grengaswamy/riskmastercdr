﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.ToolsDesigners.JurisdictionalOptions
{
    public partial class FROIOptions : NonFDMBasePageCWS
    {
        private string sCWSresponse = "";
        private XElement oMessageElement = null;
        private string message = string.Empty; 
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Label lblError = (Label)ErrorControl.FindControl("lblError");
            lblError.Text = "";

            if (!IsPostBack)
            {
                try
                {
                    oMessageElement = GetMessageTemplate("GetFROIOptions");
                    CallCWS("FROIOptionsAdaptor.GetFROIOptions", oMessageElement, out sCWSresponse, true, true);
                    if (Int32.Parse(RecordCount.Text) > 100)
                        lblmessage.Visible = true;
                    else
                        lblmessage.Visible = false;

                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    //ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
            else
            {
                message = "";
                if (hdnAction.Text == "LoadValue" || hdnAction.Text == "SaveNew")
                {
                    oMessageElement = GetMessageTemplate("GetFROIOptions");
                    CallCWS("FROIOptionsAdaptor.GetFROIOptions", oMessageElement, out sCWSresponse, true, true);
                    if (Int32.Parse(RecordCount.Text) > 100)
                        lblmessage.Visible = true;
                    else
                        lblmessage.Visible = false;
                    hdnAction.Text = "";
                }
                
                
            }
            
        }

        protected void DeleteOptions(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            oMessageElement = GetMessageTemplate("Delete");
            bReturnStatus = CallCWS("FROIOptionsAdaptor.Delete", oMessageElement, out sCWSresponse, true, true);
            if (bReturnStatus)
            {
                hdnKey.Text = "";//MGaba2:MITS 19613
                oMessageElement = GetMessageTemplate("GetFROIOptions");
                CallCWS("FROIOptionsAdaptor.GetFROIOptions", oMessageElement, out sCWSresponse, true, true);
                if (Int32.Parse(RecordCount.Text) > 100)
                    lblmessage.Visible = true;
                else
                    lblmessage.Visible = false;
            }

        }

      
        protected void Save(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            oMessageElement = GetMessageTemplate("Save");
            bReturnStatus = CallCWS("FROIOptionsAdaptor.Save", oMessageElement, out sCWSresponse, true, true);
            if (bReturnStatus)
            {
                 XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sCWSresponse);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {
                        oMessageElement = GetMessageTemplate("GetFROIOptions");
                        bReturnStatus = CallCWS("FROIOptionsAdaptor.GetFROIOptions", oMessageElement, out sCWSresponse, true, true);
                        if (Int32.Parse(RecordCount.Text) > 100)
                            lblmessage.Visible = true;
                        else
                            lblmessage.Visible = false;
                    }
                    else
                        if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                        {
                            message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                            Exception ex = new Exception(message);
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                        }
            }
        }
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                oMessageElement = GetMessageTemplate("GetFROIOptions");
               CallCWS("FROIOptionsAdaptor.GetFROIOptions", oMessageElement, out sCWSresponse, true, true);
               if (Int32.Parse(RecordCount.Text) > 100)
                   lblmessage.Visible = true;
               else
                   lblmessage.Visible = false;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                //ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void JurisState_Click(object sender, EventArgs e)
        {
            CallCWSFunction("CodesListAdaptor.GetCodes");
        }

        protected void EntityOrgHier_Click(object sender, EventArgs e)
        {
            CallCWSFunction("OrgHierarchyAdaptor.GetOrgHierarchyXml");
        }

        private XElement GetMessageTemplate(string sMethodName)
        {
            XElement oTemplate = XElement.Parse(@"
                <Message>
                <Authorization></Authorization> 
                <Call>
                    <Function>FROIOptionsAdaptor."+ sMethodName+ @"</Function> 
                </Call>
                <Document>
                    <FROI>
                        <GetValueForKey>" + hdnKey.Text+ @"</GetValueForKey>             
                        <RecordCount></RecordCount>
                        <SelOrgHier codeid=''></SelOrgHier>
                        <JurisState codeid=''></JurisState>
                        <OptionsList>
                            <listhead>
                                <Jurisdiction>Jurisdictions</Jurisdiction> 
                                <Entity>Entity</Entity> 
                                <Org-Level>Org-Level</Org-Level> 
                                <Key>key</Key>
                            </listhead>
                        </OptionsList>
                        <FROIFormsList>
                            <listhead>
                                <FormName>formname</FormName>
                                <FormID>id</FormID>
                            </listhead>
                        </FROIFormsList>
                </FROI>
            </Document>
        </Message>
        ");
            return oTemplate;
        }
    }
}
