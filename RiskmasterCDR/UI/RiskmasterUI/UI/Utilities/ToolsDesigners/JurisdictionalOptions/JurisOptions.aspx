﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JurisOptions.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.JurisdictionalOptions.JurisOptions" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Jurisdictional Forms Options</title>
    <link rel="stylesheet" href="../../../../Content/rmnet.css" type="text/css"/>
    <script language="javascript" type="text/javascript" src="../../../../Scripts/froi.js"></script>
    <script language="javascript" type="text/javascript" src="../../../../Scripts/form.js"></script>
    <script language="javascript" type="text/javascript">
        function SetHdnValue(radio) {
            document.forms[0].hdnKey.value = radio.value;
            document.forms[0].submit();
        }

        function OnJuriLoad() {
            var gridElementRadio = document.getElementsByName('rdoJuris');
            if (document.getElementById('hdnKey').value == "") {
                gridElementRadio[0].checked = true;
            }
            else {
                for (var i = 0; i < gridElementRadio.length; i++) {

                    if (gridElementRadio[i].value == document.forms[0].hdnKey.value) {
                        gridElementRadio[i].checked = true;
                    }
                }
            }

            PageLoad();

        }

        function DeleteJurisRecord() {

            var bDel = false;
            bDel = confirm('Are you sure you want to delete the record?');
            if (bDel) {
                var gridElementsRadio = document.getElementsByName('rdoJuris');

                if (gridElementsRadio != null) {
                    for (var i = 0; i < gridElementsRadio.length; i++) {
                        var gridName = gridElementsRadio[i].name;
                        if (gridElementsRadio[i].checked) {
                            if (gridElementsRadio[i].value == "k-1;-1") {
                                alert("Can not delete the double default record.");
                                return false;
                            }
                        }
                    }
                }
                document.forms[0].hdnAction.value = "Delete";
                return true;
            }
            else
                return false;

        }

        function AddNew() {

            var sToSend = "";
            var gridElementsRadio = document.getElementsByName('rdoJuris');
            
            if (gridElementsRadio != null) {
                for (var i = 0; i < gridElementsRadio.length; i++) {
                   
                        sToSend = sToSend + gridElementsRadio[i].value.substring(1) + "|";
                   
                }
            }

            sToSend = sToSend.substring(0, sToSend.length - 1);
            sToSend = sToSend + "&amp;form=" + document.getElementById('hdnForm').value;
            window.open('FROIOptionsNew.aspx?list=' + sToSend, 'FROIOptionsNew', 'width=400,height=300,top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=yes');
            document.getElementById('hdnAction').value = 'SaveNew';
            return false;
        
        }
        
    
    </script>
</head>
<body onload="Javascript:OnJuriLoad();parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <input type="hidden" name="hTabName"><div id="toolbardrift" name="toolbardrift" class="toolbardrift">
    <table border="0" CLASS="toolbar" CELLPADDING="0" CELLSPACING="0">
     <tr>
      <td align="center" valign="middle" HEIGHT="32">
      <asp:ImageButton name="Save" runat="server" ImageUrl="~/Images/save.gif"  alt="" 
              id="Save" 
              onmouseover="javascript:document.all['Save'].src='../../../../Images/save2.gif'" 
              onmouseout="javascript:document.all['Save'].src='../../../../Images/save.gif'" 
              title="Save"  OnClientClick="return ValidateAndSave();return false;" onclick="Save_Click" /></td>
     </tr>
    </table>
   </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="msgheader" id="formtitle">Jurisdictional Forms Options</div>
    <table border="0">
    <tr>
     <td>
      <td><input type="text" runat="server" name="$node^9" value="" style="display:none" id="hdnAction"></td>
      <td><asp:TextBox runat="server" rmxref="/Instance/Document/Juris/GetValueForKey"  Text="" style="display:none" id="hdnKey"></asp:TextBox></td>
      <td><asp:TextBox runat="server" rmxref="/Instance/Document/Juris/Form"  Text="JURIS" style="display:none" id="hdnForm"></asp:TextBox></td>
      <table border="0" cellspacing="0" cellpadding="0">
       <tr>
        <td class="Selected" nowrap="true" name="TABScriteria" id="TABScriteria"><a class="Selected" HREF="#" onClick="tabChange(this.name);" name="criteria" id="LINKTABScriteria"><span style="text-decoration:none">Criteria Settings</span></a></td>
        <td nowrap="true" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
        <td valign="top" nowrap="true"></td>
       </tr>
      </table>
     <%-- <div class="singletopborder" style="">--%>
       <table border="0" cellspacing="0" cellpadding="0" width="100%" >
        <tr>
         <td>
          <table border="0" cellspacing="0" cellpadding="0" width="100%" name="FORMTABcriteria" id="FORMTABcriteria">
           <td colspan=2>
        <table>
        <tr><td>Jurisdiction</td><td>Entity</td></tr>
        <tr><td>
        <asp:TextBox runat="server" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="JurisState" RMXRef="/Instance/Document/Juris/JurisState" RMXType="orgh" name="JurisState" cancelledvalue="" />
        <asp:button runat="server" class="CodeLookupControl" Text="" id="JurisStatebtn" onclientclick="return selectCode('STATES','JurisState');" OnClick="JurisState_Click" />
        <asp:TextBox style="display:none" runat="server" id="JurisState_cid" rmxref = "/Instance/Document/Juris/JurisState/@codeid" cancelledvalue="" />
        </td><td>
        <asp:TextBox runat="server" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="EntityOrgHier" RMXRef="/Instance/Document/Juris/SelOrgHier" RMXType="orgh" name="EntityOrgHier" cancelledvalue="" />
        <asp:button runat="server" class="CodeLookupControl" Text="" id="EntityOrgHierbtn" onclientclick="return selectCode('orgh','EntityOrgHier','ALL');" onclick="EntityOrgHier_Click" />
        <asp:TextBox style="display:none" runat="server" id="EntityOrgHier_cid" cancelledvalue="" rmxref ="/Instance/Document/Juris/SelOrgHier/@codeid" />
        </td><td>
        <%--Mgaba2:MITS 19613--%>
        <asp:button class="button" runat="server" Text="Filter" ID="btnFilter" OnClick="btnFilter_Click" OnClientClick="fnMakeKeyValueBlank();"/>
        </td></tr>
        </table>
        </td>
        </tr>
        <tr><td colspan="2"><asp:label class="errortext1" runat="server" id="lblmessage" ForeColor="Red" Visible="false" text="Resultset is very large to load.Please filter for Jurisdiction/Entity"/></td></tr>
           <tr >
            <td colspan="2"><b>Jurisdiction and Entity Combinations</b>
               <asp:GridView ID="JurisGrid" runat="server"  allowPaging="false" Width="100%"  AutoGenerateColumns="false"  
                            BorderColor="#003399">
                      <HeaderStyle CssClass="colheader3" />
		              <AlternatingRowStyle CssClass="data2" />
		              <Columns>
		                  <asp:TemplateField  ItemStyle-CssClass="data"   >
                                    <ItemTemplate>
                                      <input type="radio" width="5%"  id="rdoJuris" onclick="SetHdnValue(this);" value='<%# DataBinder.Eval(Container, "DataItem.Key")%>' name="rdoJuris" />
                                    </ItemTemplate> 

<ItemStyle CssClass="data"></ItemStyle>
                          </asp:TemplateField> 
                          <asp:TemplateField  HeaderText="Jurisdictions" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" width="" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Jurisdiction")%>' ></asp:Label>
                                    </ItemTemplate> 

<HeaderStyle HorizontalAlign="Left" CssClass="headerlink2" ForeColor="White"></HeaderStyle>
                          </asp:TemplateField>
                          <asp:TemplateField  HeaderText="Entity" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" width="" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Entity")%>' ></asp:Label>
                                    </ItemTemplate> 

<HeaderStyle HorizontalAlign="Left" CssClass="headerlink2" ForeColor="White"></HeaderStyle>
                          </asp:TemplateField>
                          <asp:TemplateField  HeaderText="Org-Level" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblSysTableName" width="" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Org-Level")%>' ></asp:Label>
                                    </ItemTemplate> 

<HeaderStyle HorizontalAlign="Left" CssClass="headerlink2" ForeColor="White"></HeaderStyle>
                          </asp:TemplateField>  
		              </Columns> 
                      
                           
               </asp:GridView>
          </td>
          </tr>
         <tr id="Tr13">
            <td colspan="2">
                 <script type="text/javascript" src="">{var i;}</script>
                 <asp:Button class="button"  runat="server" type="buttonscript" id="NewOpt" OnClientClick="Javascript: return AddNew();" Text="New"/>
                 <script type="text/javascript" src="">{var i;}</script>
                 <asp:Button class="button"  runat="server" type="buttonscript" id="DelOpt" 
                     OnClientClick="Javascript:return DeleteJurisRecord();"  Text="Delete" 
                     onclick="DelOpt_Click"/>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="Tr14">
            <td colspan="2">&nbsp;</td>
            </tr>
           <tr id="Tr15">
            <td colspan="2">Select Which Description Will Appear On The Report (Where Applicable):</td>
           </tr>
           <tr id="Tr16">
            <td colspan="2">
            <asp:RadioButton runat="server" type="radio" value="1" GroupName="Desc" name="Description"  id="OshaDesc" Checked="true" Text="OSHA Description" rmxref="/Instance/Document/Juris/Description"/></td>
           </tr>
           <tr id="Tr17">
            <td colspan="2"><asp:RadioButton runat="server" value="0" type="radio" GroupName="Desc" name="Description"  id="EventDesc" Text="Event Description" rmxref="/Instance/Document/Juris/Description"/></td>
           </tr>
           <tr id="Tr18">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr >
            <td colspan="2">Claim Number Options</td>
           </tr>
           <tr >
            <td colspan="2">
            <asp:RadioButton runat="server"  type="radio" value="1" GroupName="ClNoOpt"  id="JurisSupp" rmxref="/Instance/Document/Juris/CarrClmNumOpt"  Text="Use Jurisdictional Supp Field" /></td>
           </tr>
           <tr >
            <td colspan="2"><asp:RadioButton runat="server" value="0" type="radio" GroupName="ClNoOpt"   id="RMClaim" rmxref="/Instance/Document/Juris/CarrClmNumOpt" Text="Use RISKMASTER Claim Number" /></td>
           </tr>
           <tr >
            <td colspan="2"><asp:RadioButton runat="server" value="2" type="radio" GroupName="ClNoOpt" id="RMClaimNotJurisSupp" rmxref="/Instance/Document/Juris/CarrClmNumOpt" Text="Use RISKMASTER Claim Number if Jurisdictional Supp Field is Empty" /></td>
           </tr>
           <tr >
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="Tr1">
            <td colspan="2">Claims Administrator (Where Applicable):</td>
           </tr>
           <tr id="Tr2">
            <td colspan="2">
                <asp:RadioButton runat="server" type="radio" GroupName="ClaimsAdmin" name="ClaimAdmin" value="2" onclick="Javascript:Enable()" id="ClmAdm2" rmxref="/Instance/Document/Juris/ClaimAdministrator" Text="Client/Company Name (Org.Hiearchy):"/>
                <label style="width:10px"></label>
                <asp:DropDownList runat="server" type="combobox" name="ClmAdminOrgHier" id="ClmAdminOrgHier" onchange="setDataChanged(true);" rmxref="/Instance/Document/Juris/ClAdmOrgLvl">
                 <asp:ListItem value="1005">Client</asp:ListItem>
                 <asp:ListItem value="1006">Company</asp:ListItem>
                 <asp:ListItem value="1007">Operation</asp:ListItem>
                 <asp:ListItem value="1008">Region</asp:ListItem>
                 <asp:ListItem value="1009">Division</asp:ListItem>
                 <asp:ListItem value="1010">Location</asp:ListItem>
                 <asp:ListItem value="1011">Facility</asp:ListItem>
                 <asp:ListItem value="1012">Department</asp:ListItem>
               </asp:DropDownList>
            </td>
           </tr>
           <tr id="Tr3">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="ClaimsAdmin" name="ClaimAdmin" type="radio" value="5" onclick="Javascript:Enable()" id="ClmAdm5" checked="true" Text="TPA (Third Party Administrator):" rmxref="/Instance/Document/Juris/ClaimAdministrator"/>
                <label style="width:10px"></label>
                <asp:TextBox runat="server" name="ClmAdmTPA" type="eidlookup" tableid="CLAIM_ADMIN_TPA" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="ClmAdmTPA" cancelledvalue="" rmxref="/Instance/Document/Juris/ClAdmTPAEID"/>
                <asp:Button class="EllipsisControl"  runat="server" text="..." id="ClmAdmTPAbtn" OnClientClick=" return lookupData('ClmAdmTPA','CLAIM_ADMIN_TPA',4,'ClmAdmTPA',2)"/>
                <asp:TextBox runat="server" name="" style="display:none" id="ClmAdmTPA_cid" cancelledvalue="" rmxref="/Instance/Document/Juris/ClAdmTPAEID/@codeid"/>
            </td>
           </tr>
           <tr id="Tr4">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="ClaimsAdmin" name="" value="4" onclick="Javascript:Enable()" id="ClmAdm4" Text="WC Claim Administrator - (Default):" rmxref="/Instance/Document/Juris/ClaimAdministrator"/>
                <label style="width:10px"></label>
                <asp:TextBox runat="server" name="" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="ClmAdmDef" cancelledvalue="" rmxref="/Instance/Document/Juris/ClAdmDefEID"/>
                <asp:Button class="EllipsisControl"  runat="server" Text="..." id="ClmAdmDefbtn" OnClientClick="return lookupData('ClmAdmDef','WC_DEF_CLAIM_ADMIN',4,'ClmAdmDef',2)"/>
                <asp:TextBox runat="server" name=""  style="display:none" id="ClmAdmDef_cid" cancelledvalue="" rmxref="/Instance/Document/Juris/ClAdmDefEID/@codeid"/>
            </td>
           </tr>
           <tr id="Tr5">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="ClaimsAdmin" name="" value="3" onclick="Javascript:Enable()" id="ClmAdm3"  Text="Current Adjuster" rmxref="/Instance/Document/Juris/ClaimAdministrator"/>
            </td>
           </tr>
           <tr id="Tr6">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="ClaimsAdmin" name="" value="1" onclick="Javascript:Enable()" id="ClmAdm1" Text="Nothing" rmxref="/Instance/Document/Juris/ClaimAdministrator"/>
            </td>
           </tr>
           <tr id="Tr7">
            <td colspan="2">&nbsp;</td>
           </tr>
          <tr id="Tr8">
            <td colspan="2">Carrier</td>
           </tr>
           <tr id="Tr9">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="Carrier" name="" value="1" onclick="Javascript:Enable()" id="Carrier1" Text="Client/Company Name (Org.Hiearchy):" rmxref="/Instance/Document/Juris/Carrier" />
                <label style="width:10px"></label>
                <asp:DropDownList runat="server" name="" id="CarrierOrgHier" onchange="setDataChanged(true);" type="combobox" rmxref="/Instance/Document/Juris/CarrierOrgLevel">
                    <asp:ListItem value="1005">Client</asp:ListItem>
                    <asp:ListItem value="1006">Company</asp:ListItem>
                    <asp:ListItem value="1007">Operation</asp:ListItem>
                    <asp:ListItem value="1008">Region</asp:ListItem>
                    <asp:ListItem value="1009">Division</asp:ListItem>
                    <asp:ListItem value="1010">Location</asp:ListItem>
                    <asp:ListItem value="1011">Facility</asp:ListItem>
                    <asp:ListItem value="1012">Department</asp:ListItem>
                </asp:DropDownList>
            </td>
           </tr>
           <tr id="Tr10">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="Carrier" name="" value="2" onclick="Javascript:Enable()" id="Carrier2" Text="Default Carrier:" rmxref="/Instance/Document/Juris/Carrier"/>
                <label style="width:10px"></label>
                <asp:TextBox runat="server" type="eidlookup" name="" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="CarrierDef" cancelledvalue="" rmxref="/Instance/Document/Juris/CarrierDefEID" />
                <asp:Button class="EllipsisControl"  runat="server" type="button" text="..." id="CarrierDefbtn" OnClientClick="return lookupData('CarrierDef','INSURERS',4,'CarrierDef',2)" />
                <asp:TextBox runat="server" type="text" name="" style="display:none" id="CarrierDef_cid" cancelledvalue="" rmxref="/Instance/Document/Juris/CarrierDefEID/@codeid" />
            </td>
           </tr>
           <tr id="Tr11">
            <td colspan="2">
                <asp:RadioButton  runat="server" type="radio" GroupName="Carrier" name="" value="3" onclick="Javascript:Enable()" id="Carrier3" checked="true" Text="Linked By Policy Number" rmxref="/Instance/Document/Juris/Carrier" />
            </td>
           </tr>
           <tr id="Tr12">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr >
            <td colspan="2">Financial Reporting (EDI and Forms)</td>
           </tr>
           <tr >
            <td colspan="2">
            <asp:RadioButton  runat="server" GroupName="grpFReport" value="0" rmxref="/Instance/Document/Juris/CheckStatusOpt" onclick="" id="OnlyPrinted" Text="Use only Funds records with 'Printed' status" /></td>
           </tr>
           <tr >
            <td colspan="2"><asp:RadioButton  runat="server" GroupName="grpFReport" rmxref="/Instance/Document/Juris/CheckStatusOpt" value="1" onclick="" id="PrintedOrReleased" Text="Use Funds records with 'Printed' or 'Released' status" /></td>
           </tr>
           <tr >
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr >
            <td colspan="2">Attach To Claim By Default</td>
           </tr>
           <tr >
            <td colspan="2">
            <asp:RadioButton  runat="server" rmxref="/Instance/Document/Juris/AttachToClaim" GroupName="grpAttClaim" value="0" onclick="" id="rbtAttachToClaim" Text="Yes" /></td>
           </tr>
           <tr >
            <td colspan="2"><asp:RadioButton  runat="server" rmxref="/Instance/Document/Juris/AttachToClaim" GroupName="grpAttClaim" value="1" onclick="" id="rbt1AttachToClaim" Text="No" /></td>
           </tr>
           <tr >
            <td colspan="2">&nbsp;</td>
           </tr>
          <tr id="Tr19">
            <td colspan="2">Source of Preparer Information:</td>
           </tr>
           <tr id="Tr20">
            <td colspan="2">
             </td>
           </tr>
           <!--mits 24622; adding another radio button for adjuster; tanwar2; 29th Dec2011-->
           <tr id="Tr28">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PreparerInfo" type="radio" name="PreparerInfo" value="5" onclick="" id="pi1" rmxref="/Instance/Document/Juris/PreparerInfo" Text="Adjuster" /></td>
           </tr>
           <!--end of mits 24622-->
           <tr id="Tr21">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PreparerInfo" type="radio" name="PreparerInfo" value="3" onclick="" id="pi2" rmxref="/Instance/Document/Juris/PreparerInfo" Text="Claim Created By" /></td>
           </tr>
           <tr id="Tr22">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PreparerInfo" type="radio" name="PreparerInfo" value="2" onclick="" id="pi3" rmxref="/Instance/Document/Juris/PreparerInfo" Text="Claim Last Edited By" /></td>
           </tr>
           <tr id="Tr23">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PreparerInfo" type="radio" name="PreparerInfo" value="1" onclick="" id="pi4" rmxref="/Instance/Document/Juris/PreparerInfo" Text="Workstation User" /></td>
           </tr>
           <tr id="Tr24">
            <td colspan="2">
          </td>
          </tr>
          
           <tr id="Tr25">
            <td colspan="2">&nbsp;</td>
           </tr>
          <tr id="Tr26">
            <td colspan="2">Select The Organization Hierarchy Level for Employer Information:</td>
           </tr>
           <tr id="Tr27">
            <td><asp:DropDownList runat="server"  name="EmployerInfo" id="EmployerInfo" onchange="setDataChanged(true);" rmxref="/Instance/Document/Juris/EmployerLevel" >
              <asp:ListItem value="1005">Client</asp:ListItem>
              <asp:ListItem value="1006">Company</asp:ListItem>
              <asp:ListItem value="1007">Operation</asp:ListItem>
              <asp:ListItem value="1008">Region</asp:ListItem>
              <asp:ListItem value="1009">Division</asp:ListItem>
              <asp:ListItem value="1010">Location</asp:ListItem>
              <asp:ListItem value="1011">Facility</asp:ListItem>
              <asp:ListItem value="1012" selected="True">Department</asp:ListItem></asp:DropDownList></td>
           </tr>
           <tr >
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr >
            <td colspan="2">Select TPA:</td>
           </tr>
           <tr >
            <tr id="Tr29">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="TPA" type="radio" name="BlankTPA" value="0" onclick="Javascript:Enable()" id="BlankTPA" Text="Leave Blank" Checked ="true" rmxref="/Instance/Document/Juris/TpaUseOpt" /></td>
           </tr>
           <%--MITS 29587 Added Radio Button Option for TPA ; sgupta243 ; 28/08/2012 --%>
           <tr id="Tr30">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="TPA" type="radio" name="DefaultTPA" value="1" onclick="Javascript:Enable()" id="DefaultTPA" checked="true" Text="Use A Default TPA" rmxref="/Instance/Document/Juris/TpaUseOpt" /> 
            <label style="width:10px"></label>
            <asp:textBox runat="server" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="TPALookup" cancelledvalue="" name="TPALookup" type="eidlookup" tableid="CLAIM_ADMIN_TPA" rmxref="/Instance/Document/Juris/TPAEID" />
            
            <asp:Button class="EllipsisControl"  runat="server" type="button" Text="..." id="TPALookupbtn" OnClientClick="return lookupData('TPALookup','CLAIM_ADMIN_TPA',4,'TPALookup',2)" />
            <asp:TextBox runat="server" value="0" style="display:none" id="TPALookup_cid" cancelledvalue="" rmxref="/Instance/Document/Juris/TPAEID/@codeid" /></td>
           
           </tr>
          <%-- <tr >
            <td>
            <asp:TextBox runat="server" size="30" onblur="codeLostFocus(this.id);" id="TPA" rmxref="/Instance/Document/Juris/TPAEID"  cancelledvalue=""></asp:TextBox>
            <input type="button" class="EllipsisControl" value="..." id="TPAbtn" onclick="lookupData('TPA','CLAIM_ADMIN_TPA',4,'TPA',2)">
            <asp:TextBox runat="server" value="0" style="display:none" id="TPA_cid" cancelledvalue="" rmxref="/Instance/Document/Juris/TPAEID/@codeid"></asp:TextBox>
            </td>
           </tr>--%>
            <%--MITS 29587 END--%>

          </table>
         </td>
         <td valign="top"></td>
        </tr>
       </table>
      <%--</div>--%>
      <table>
       <tr>
        <td><asp:TextBox style="display:none" runat="server" id="RecordCount" rmxref = "/Instance/Document/Juris/RecordCount" RMXType="id" /></td>
       </tr>
      </table>
    </form>
</body>
</html>
