﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.Utilities.ToolsDesigners.JurisdictionalOptions
{
    public partial class FROIHistory : NonFDMBasePageCWS
    {
        protected IEnumerable result = null;
        private XElement rootElement = null;
        private XmlDocument oNonFDMPageDom = null;
        private string sCWSresponse = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string formid = Request.QueryString["formid"];
                try
                {
                    XElement oMessageElement = GetMessageTemplate(formid);
                    
                    CallCWS("FROIOptionsAdaptor.GetFROIHistory", oMessageElement, out sCWSresponse, false, true);
               
                    oNonFDMPageDom = new XmlDocument();
                
                    oNonFDMPageDom = Data;

                    rootElement = XElement.Parse(oNonFDMPageDom.OuterXml);

                    result = from resultset in rootElement.XPathSelectElements("FROIHistory/List/listrow")
                         select resultset;
            }
                 catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }

            }
            
        }

        private XElement GetMessageTemplate(string formid)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>FROIOptionsAdaptor.GetFROIHistory</Function></Call><Document><FROIHistory><List><listhead><ClaimNumber>Claim Number</ClaimNumber>");
            sXml = sXml.Append("<DatePrinted>Date Printed</DatePrinted><TimePrinted>Time Printed</TimePrinted>");
            sXml = sXml.Append("<UserID>User ID</UserID></listhead></List><FormID>");
            sXml = sXml.Append(formid);
            sXml = sXml.Append("</FormID></FROIHistory></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
