﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.ToolsDesigners.JurisdictionalOptions
{
    public partial class FROIOptionsNew : NonFDMBasePageCWS
    {
        private string sList = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            sList = Request.QueryString["list"];
            ExistingList.Text = sList;

            //Added by abhishek to handel juris options
            if (Request.QueryString["form"] != "")
            {
                FormName.Text = Request.QueryString["form"];
            }
        }

        protected void EntityOrgHier_Click(object sender, EventArgs e)
        {          
           CallCWSFunction("OrgHierarchyAdaptor.GetOrgHierarchyXml");
        }

        protected void Okbtn_Click(object sender, EventArgs e)
        {
           CallCWSFunction("FROIOptionsAdaptor.New");
        }

        protected void JurisState_Click(object sender, EventArgs e)
        {
           CallCWSFunction("CodesListAdaptor.GetCodes");
        }

    }
}
