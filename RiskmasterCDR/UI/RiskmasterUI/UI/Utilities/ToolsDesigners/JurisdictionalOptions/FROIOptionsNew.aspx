﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FROIOptionsNew.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.JurisdictionalOptions.FROIOptionsNew" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Add New Setting</title>
    <uc:CommonTasks id="CommonTasks1" runat="server" />
    <script type="text/javascript" src="../../../../Scripts/froi.js"></script>
  </head>
 <body onload="checkWindow();">
  <form id="frmData" runat ="server" name="frmData" method="post" >
  <uc1:ErrorControl id="ErrorControl2" runat="server" />
  <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
  <table width="100%" cellspacing="0" cellpadding="0">
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td>Do you want to use all Org-Hieararchy Entities?</td>
    </tr>
    <tr>
     <td>
        <asp:RadioButton runat = "server" GroupName="OrgHier" name="" value="-1" id="allorg" onclick="Javascript:ToggleEntity()" checked="true" Text = "All" rmxref = "Instance/Document/form/OrgHierEntity"/><br/>
        <asp:RadioButton runat = "server" name="" GroupName="OrgHier" value="0" id="selectorg" onclick="Javascript:ToggleEntity()" Text = "Select Entity" rmxref = "Instance/Document/form/OrgHierEntity" />&nbsp;&nbsp;   
        <asp:TextBox runat="server" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="EntityOrgHier" disabled="true" RMXRef="/Instance/Document/form/SelOrgHier" RMXType="orgh" name="EntityOrgHier" cancelledvalue="" />
        <asp:button runat="server" class="CodeLookupControl" Text="" id="EntityOrgHierbtn" onclientclick="return selectCode('orgh','EntityOrgHier','ALL');" disabled = "true" onclick="EntityOrgHier_Click" />
        <asp:TextBox style="display:none" runat="server" id="EntityOrgHier_cid" cancelledvalue="" rmxref ="/Instance/Document/form/SelOrgHier/@codeid" />

      </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td>Do you want to use all Jurisdictions?</td>
    </tr>
    <tr>
     <td>
        <asp:RadioButton runat = "server" GroupName="Juris" name="" value="-1" id="alljuris" onclick="Javascript:ToggleEntity();" checked="true" Text = "All" rmxref = "Instance/Document/form/Jurisdictions"/><br/>
        <asp:RadioButton runat = "server" GroupName ="Juris" name="" value="0" id="selectjuris" onclick="Javascript:ToggleEntity();" Text = "Select Jurisdiction" />&nbsp;&nbsp;      
        <asp:TextBox runat="server" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="JurisState" RMXRef="/Instance/Document/form/JurisState" RMXType="orgh" name="JurisState" cancelledvalue="" disabled = "true" />
        <asp:button runat="server" class="CodeLookupControl" Text="" id="JurisStatebtn" onclientclick="return selectCode('STATES','JurisState');" OnClick="JurisState_Click" disabled ="true" />
        <asp:TextBox style="display:none" runat="server" id="JurisState_cid" rmxref = "/Instance/Document/form/JurisState/@codeid" cancelledvalue="" />
    </td>
    </tr>
    <tr>
     <td><br/><hr/>
     </td>
    </tr>
  
    <tr>
     <td>
        <asp:Button runat ="server" id= "OKButton" class="button" Text ="OK" onClientclick="Javascript:return SaveNew();" OnClick="Okbtn_Click" style="width:60px"/>
      	&nbsp;
      	<asp:Button runat = "server" id="CancelButton" class="button" Text = "Cancel" onClientClick="window.close();" style="width:60px" />
     </td>
    </tr>
   </table>
   <asp:TextBox runat = "server" style ="display:none" name="" value="" id="hdnAction"/>
   <asp:TextBox runat = "server" style ="display:none" name="" value="" id="FormMode" rmxref="/Instance/Document/form/FormMode" />
   <asp:TextBox runat = "server" style ="display:none" name="" value="FROI" id="FormName" rmxref="/Instance/Document/form/FormName" />
   <asp:TextBox runat = "server" style ="display:none" name="" id="ExistingList" rmxref="/Instance/Document/form/ExistingRecords"></asp:TextBox>
   <input type="hidden" name="" value="" id="SysWindowId" />
   </form>
 </body>
</html>

