﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FROIHistory.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.JurisdictionalOptions.FROIHistory" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Forms Print History</title>
    <uc:CommonTasks id="CommonTasks1" runat="server" />
    <script type="text/javascript" src="../../../../Scripts/froi.js"></script>
  </head>
 <body onload="checkWindow();">
 <form id="frmData" name="frmData" method="post" runat="server">
    <uc1:ErrorControl id="ErrorControl" runat="server" />
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
    <table width="100%">
     <tr>
      <td width="80%">
        <div style="&#xA;width:100%;height:150px; overflow:auto&#34;&#xA;">
        <table width="100%" class="singleborder" cellspacing="0" cellpadding="0">
         <tr class="msgheader">
          <td width="31.666666666666666667%">
          </td>
         </tr>
         <tr class="msgheader">
          <td width="31.666666666666666667%">Claim Number</td>
          <td width="31.666666666666666667%">Date Printed</td>
          <td width="31.666666666666666667%">Time Printed</td>
          <td width="31.666666666666666667%">User ID</td>
         </tr>
         <%int i = 0;
       if (result != null)
       {
           foreach (XElement item in result)
           {
               string rowclass = "";
               if ((i % 2) == 1) rowclass = "rowlight";
               else rowclass = "rowdark";
               i++;
               
     %>
     <%--Bijender has changed for Mits 15669--%>
     <tr class="<% =rowclass %>">
        <td>
            <% if (item.Element("ClaimNumber").Value != null)
            ClaimNumber.Text = item.Element("ClaimNumber").Value; %>
            <asp:Label ID="ClaimNumber" runat="server"></asp:Label>
        </td>
        <td>
            <% if (item.Element("DatePrinted").Value != null)
            DatePrinted.Text = item.Element("DatePrinted").Value; %>
            <asp:Label ID="DatePrinted" runat="server"></asp:Label>
        </td>
        <td>
            <% if (item.Element("TimePrinted").Value != null)
            TimePrinted.Text = item.Element("TimePrinted").Value; %>
            <asp:Label ID="TimePrinted" runat="server"></asp:Label>
        </td>
        <td>
        <% if (item.Element("TimePrinted").Value != null)
            UserID.Text = item.Element("UserID").Value; %>
            <asp:Label ID="UserID" runat="server"></asp:Label>
        </td>
       </tr>
      <%--Bijender End Mits 15669--%>
     <% }
       }     %>
     </table>
       </div>
      </td>
     </tr>
     <tr>
     <td><asp:Button runat="server" class="button" ID="btnClose" Text="Close" OnClientClick="window.close();" style="width:60px" /></td>
    </tr>
    </table>
   <asp:TextBox runat="server" type="hidden" style="display:none" name="" value="80" id="hdnformid" rmxref="/Instance/Document/FROIHistory/FormID" />
   <input type="hidden" name="" value="" id="SysWindowId"/>
  </form>
 </body>
</html>