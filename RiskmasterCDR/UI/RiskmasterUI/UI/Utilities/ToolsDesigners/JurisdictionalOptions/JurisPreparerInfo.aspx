﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JurisPreparerInfo.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.JurisdictionalOptions.JurisPreparerInfo" %>
    <%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head id="Head1" runat="server">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   
  <title>JurisPreparerInfo</title>
  <uc:CommonTasks id="CommonTasks1" runat="server" />
  </head>
 <body onload="parent.MDIScreenLoaded();">
  <form id="wsrp_rewrite_form_1" runat="server" method="post" >
    <uc1:ErrorControl id="ErrorControl2" runat="server" />
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""/>
    <table width="100%" border="0">
    <tr>
     <td>
      										Preparer Name:
      									
     </td>
     <td><asp:TextBox ID="preparerName" runat ="server" rmxref="/Instance/Document/JurisPreparer/preparerName" type="text" value=""/></td>
    </tr>
    <tr>
     <td>
      										Preparer Title:
      									
     </td>
     <td><asp:Textbox id ="preparerTitle" runat ="server" value="" type = "text" rmxref ="/Instance/Document/JurisPreparer/preparerTitle" /></td>
    </tr>
    <tr>
     <td>
      										Preparer Phone:
      									
     </td>
     <td><asp:Textbox type="phone" id ="preparerPhone" runat ="server" value="" rmxref = "/Instance/Document/JurisPreparer/preparerPhone" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" /></td>
    </tr>
    <tr>
     <td colspan="2">
        <asp:Button ID="btnSave" onclick="Save" class="button" runat ="server" text = "Save" value="Save" />
     </td>
    </tr>
    <tr>
     <td nowrap="1">
     <asp:CheckBox ID="rememberInfo" type="checkbox" runat = "server" value="-1" rmxref="/Instance/Document/JurisPreparer/rememberInfo" />Remember
                this information and don't prompt me again.
     </td>
    </tr>
   </table>
   <input type="hidden" name="" value="rmx-widget-handle-4" id="SysWindowId"/></form>
 </body>
</html>