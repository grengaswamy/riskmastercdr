﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.HistoryDesigner
{
    public partial class HistoryDesigner : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsCallback && !Page.IsPostBack)
                {
                    TVFieldsSelection.Attributes.Add("onclick", "return OnTreeClick(event)");
                    TVFieldsSelection.FindNode("0").Expand();                                       
                }                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        private XElement GetXmlIn()
        {           
            char sDelimiter='~';
            StringBuilder  sbXml = new StringBuilder();
            bool bCreatedTableNode = false;
            bool bSelected = false;


            sbXml.AppendFormat("<Message><Authorization></Authorization><Call><Function>{0}</Function></Call><Document><Form>", hdFunctionToCall.Value );
            if (hdFunctionToCall.Value  == "HistoryDesignerAdaptor.Save")
            {
                sbXml.Append("<Tables>");
                foreach (TreeNode objTableNode in TVFieldsSelection.Nodes[0].ChildNodes)
                {
                    bCreatedTableNode = false;

                    string[] sArrTableValue = objTableNode.Value.Split(sDelimiter);
                    bSelected = sArrTableValue[1] == "-1" ? true : false;

                    if (objTableNode.Checked != bSelected)
                    {
                        sbXml.AppendFormat("<Table id='{0}' Selected='{1}' >", sArrTableValue[0], objTableNode.Checked ? -1 : 0);
                        bCreatedTableNode = true;
                    }

                    foreach (TreeNode objColNode in objTableNode.ChildNodes)
                    {
                        string[] sArrColumnValue = objColNode.Value.Split(sDelimiter);
                        bSelected = sArrColumnValue[1] == "-1" ? true : false;
                        if (objColNode.ShowCheckBox == true && objColNode.Checked != bSelected)
                        {
                            if (bCreatedTableNode == false)
                            {
                                sbXml.AppendFormat("<Table id='{0}'>", sArrTableValue[0]);
                                bCreatedTableNode = true;
                            }
                            sbXml.AppendFormat("<Column id='{0}' Selected='{1}' />", sArrColumnValue[0], objColNode.Checked ? -1 : 0);
                        }
                    }
                    if (bCreatedTableNode == true)
                    {
                        sbXml.Append("</Table>");
                    }
                }                
                sbXml.Append("</Tables>");                
                TVFieldsSelection.Nodes[0].ChildNodes.Clear(); //Clearing the tree
            }         
            else
            {
            sbXml.AppendFormat("<SelectedTableId>{0}</SelectedTableId>", hdnSelectedTableId.Value); 
            }
            sbXml.AppendFormat("<DataChanged>{0}</DataChanged>", hdnDataChanged.Value);
            sbXml.Append("</Form></Document></Message>");
          
            return XElement.Parse(sbXml.ToString());
        }

        
        private void ResettingHiddenControls()
        {
            hdFunctionToCall.Value = "HistoryDesignerAdaptor.GetTables";           
        }        

        private void GenerateTree(TreeNode oParentNode)
        {
            string sValue = string.Empty;
            bool bSelectedFlag = false;
            bool bPrimaryFlag=false ;
            bool bReportFlag = false;
            string sDelimiter = "~";
            XElement objXmlIn=null ;
            string sResponse =string.Empty ;
            XElement objResponse = null;
            try
            {
                objXmlIn = GetXmlIn();
                CallCWS(hdFunctionToCall.Value, objXmlIn,out sResponse, false, false);
                objResponse = XElement.Parse(sResponse);

                XElement oDataChanged = objResponse.XPathSelectElement("//DataChanged");
                if (oDataChanged != null)
                    hdnDataChanged.Value = oDataChanged.Value;

                XElement oParent = objResponse.XPathSelectElement("//Root");
                if (oParent != null)
                {                    
                    foreach(XElement oElement in oParent.Elements()) 
                    {
                        bPrimaryFlag = false;
                        bReportFlag = false;
                        bSelectedFlag = false;

                        if (oElement.Name == "Column")
                        {
                            if (oElement.Attribute("PrimaryKey").Value   == "-1")
                                bPrimaryFlag = true;
                            else
                                bPrimaryFlag = false;                            
                        }

                        if (oElement.Attribute("Track_FLAG") != null)
                        {
                            bSelectedFlag = oElement.Attribute("Track_FLAG").Value == "-1" ? true : false;
                        }

                        if (oElement.Attribute("RequiredForReport_FLAG") != null)
                        {
                            bReportFlag = Convert.ToBoolean(oElement.Attribute("RequiredForReport_FLAG").Value);
                        }

                        sValue = oElement.Attribute("Id").Value + sDelimiter + oElement.Attribute("Track_FLAG").Value;

                        AddChildNode(oParentNode, sValue, oElement.Attribute("Name").Value, bSelectedFlag, bPrimaryFlag, bReportFlag);
                       
                    }                    
                }                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void AddChildNode(TreeNode oParentNode, string p_Value, string p_sName, bool  p_bSelectedFlag, bool p_bPrimaryFlag,bool p_bReportFlag)
        {
            TreeNode childNode = null;
            try
            {
                childNode = new TreeNode(p_sName, p_Value.ToString());
                if (oParentNode.Depth == 0)
                {
                    childNode.PopulateOnDemand = true;
                }
                else
                {
                    childNode.PopulateOnDemand = false;
                    childNode.SelectAction = TreeNodeSelectAction.None; 
                }
                
                childNode.Target = "_self";                

                if (p_bPrimaryFlag)
                {   
                    childNode.ImageUrl ="../../../../Images/primary_key.png";                   
                }
                else
                {
                    childNode.Checked = p_bSelectedFlag;
                }

                if (p_bReportFlag)
                {   //TO Do: Change the image
                    childNode.ImageUrl = "../../../../Images/Tick3.gif";                    
                }
                else
                {
                    childNode.Checked = p_bSelectedFlag;
                }

                if (p_bPrimaryFlag || p_bReportFlag)
                {
                    childNode.ShowCheckBox = false;
                }
                else
                {
                    childNode.ShowCheckBox = true ;
                }
                oParentNode.ChildNodes.Add(childNode);

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {                     
            hdFunctionToCall.Value   = "HistoryDesignerAdaptor.Save";            
            GenerateTree(TVFieldsSelection.Nodes[0]);
            ResettingHiddenControls();
        }        

        protected void TVFieldsSelection_TreeNodePopulate(object sender, TreeNodeEventArgs e)
        {  
                if (e.Node.Value != "0")
                {
                    hdnSelectedTableId.Value  = e.Node.Value.Substring(0, e.Node.Value.IndexOf('~'));
                    hdFunctionToCall.Value  = "HistoryDesignerAdaptor.GetColumns";
                }
                GenerateTree(e.Node);         
        }

        protected void btnSelectColumns_Click(object sender, ImageClickEventArgs e)
        {
            TreeNode tnTable = TVFieldsSelection.SelectedNode;
            tnTable.Checked = true;
            if (tnTable.Expanded == false)
            {
                tnTable.Expand();
            }
            foreach (TreeNode tnColumn in tnTable.ChildNodes)
            {                 
                tnColumn.Checked = true; 
            }
        }

        protected void btnDeselectColumns_Click(object sender, ImageClickEventArgs e)
        {
            TreeNode tnTable = TVFieldsSelection.SelectedNode;
            tnTable.Checked = false;
            if (tnTable.Expanded == false)
            {
                tnTable.Expand();
            }
            foreach (TreeNode tnColumn in tnTable.ChildNodes)
            {
                tnColumn.Checked = false;
            }
        }        
    }
}
