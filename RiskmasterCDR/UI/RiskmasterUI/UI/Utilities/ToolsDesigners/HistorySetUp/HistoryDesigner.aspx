﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HistoryDesigner.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.HistoryDesigner.HistoryDesigner" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="../../../Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>History SetUp</title>

    <script language="JavaScript" type="text/javascript" src="../../../../Scripts/HistoryDesigner.js"></script>

    <script language="JavaScript" type="text/javascript" src="../../../../Scripts/WaitDialog.js">        { var i; }</script>

</head>
<body onload="parent.MDIScreenLoaded();pageLoaded();">
    <form id="frmData" runat="server">
    <table width="90%">
        <tr>
            <td class="msgheader">
                History Tracking SetUp : Selection of Tables and Columns
            </td>
        </tr>
        <tr>
            <td>
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <div style="height: 40px;">
                    Please ensure that History Tracking executable is configured through Task Manager
                    for asynchronous processing to make the History tracking setup in effect.
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="image">
                    <asp:ImageButton ID="Save" onMouseOver="this.src='../../../../Images/tb_save_mo.png';this.style.zoom='110%'"
                        onMouseOut="this.src='../../../../Images/tb_save_active.png';this.style.zoom='100%'"
                        src="../../../../Images/tb_save_active.png" class="bold" ToolTip="Save" runat="server"
                        onclick="Save_Click" OnClientClick=" return fnSaveTree();" />
                </div>
                <div class="image">
                <asp:ImageButton ID="btnSelectColumns" onMouseOver="this.src='../../../../Images/tb_grantmodules_mo.png';this.style.zoom='110%'"
                        onMouseOut="this.src='../../../../Images/tb_grantmodules_active.png';this.style.zoom='100%'"
                        src="../../../../Images/tb_grantmodules_active.png" class="bold" ToolTip="Select All Columns of the table" runat="server"
                         onclick="btnSelectColumns_Click" OnClientClick="return SelectOrDeselectChildColumns();" />                    
                </div>
                <div class="image">
                <asp:ImageButton ID="btnDeselectColumns" onMouseOver="this.src='../../../../Images/tb_blockmodules_mo.png';this.style.zoom='110%'"
                        onMouseOut="this.src='../../../../Images/tb_blockmodules_active.png';this.style.zoom='100%'"
                        src="../../../../Images/tb_blockmodules_active.png" class="bold" 
                        ToolTip="Deselect All Columns of the table" runat="server" 
                        onclick="btnDeselectColumns_Click" OnClientClick="return SelectOrDeselectChildColumns();"/>
                    
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="height: 20px;">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%; height: 270px;" class="divScroll">
                    <asp:TreeView ID="TVFieldsSelection" PopulateNodesFromClient="true" ShowLines="true"
                        ShowExpandCollapse="true" runat="server"
                        SelectedNodeStyle-Font-Bold="true" SelectedNodeStyle-BackColor="LightBlue" ForeColor="Black"
                        Target="_self" EnableViewState="true" ExpandDepth="1" 
                        OnTreeNodePopulate="TVFieldsSelection_TreeNodePopulate" >
                        <Nodes>
                            <asp:TreeNode Text="Riskmaster" Value="0" PopulateOnDemand="true" ShowCheckBox="false" SelectAction ="None" ></asp:TreeNode>
                        </Nodes>
                    </asp:TreeView>
                </div>
            </td>
        </tr>
    </table>
    
    
    <asp:HiddenField ID="hdFunctionToCall" runat="server" Value="HistoryDesignerAdaptor.GetTables"/>
    <asp:HiddenField runat="server" ID="hdnSelectedTableId" />    
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    <asp:HiddenField ID ="hdnDataChanged" runat ="server" Value="false"/>
    </form>
</body>
</html>
