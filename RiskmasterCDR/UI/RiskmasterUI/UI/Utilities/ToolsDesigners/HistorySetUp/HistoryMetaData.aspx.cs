﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Cache; 

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.HistorySetUp
{
    public partial class HistoryMetaData : NonFDMBasePageCWS 
    {     
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement oXETableList = null;            
            string sValue = string.Empty;
            string sText = string.Empty;
            XElement oXmlIn=null ;
            try
            {
                if (!IsPostBack)
                {
                    //Retrieve Xml from Cache and bind to Combo            
                    if (CacheCommonFunctions.CheckIfKeyExists("MetaDataTableList", AppHelper.ClientId) && CacheCommonFunctions.RetreiveValueFromCache<object>("MetaDataTableList", AppHelper.ClientId) != null)
                    {
                        oXETableList = XElement.Parse(CacheCommonFunctions.RetreiveValueFromCache<object>("MetaDataTableList", AppHelper.ClientId).ToString());
                    }
                    else
                    {
                        string sResponse = string.Empty;
                        oXmlIn = GetMessageTemplateToGetTables();
                        bool bReturnStatus = CallCWS("HistoryMetaDataAdaptor.GetTables", oXmlIn, out sResponse, false, false);
                        XElement oXOut = XElement.Parse(sResponse);

                        oXETableList = oXOut.XPathSelectElement("//TableList");
                        if (oXETableList != null)
                        {
                            CacheCommonFunctions.AddValue2Cache<object>("MetaDataTableList", AppHelper.ClientId, oXETableList);
                        }
                    }

                    // Bind Combo with Grid
                    if (oXETableList != null)
                    {
                        foreach (XElement oXETable in oXETableList.Elements())
                        {
                            sValue = oXETable.Attribute("Id").Value;
                            sText = oXETable.Attribute("UserPrompt").Value;
                            ddlTableName.Items.Add(new ListItem(sText, sValue));
                        }
                    }
                }
            
                    ViewState["SelectedTableId"] = ddlTableName.SelectedValue; 
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } 
        }       

        protected void ddlTableName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTableName.SelectedValue == "0")
                {
                    pnlColumns.Visible = false;
                    pnlLabels.Visible = false;
                }
                else
                {
                    pnlColumns.Visible = true;
                    pnlLabels.Visible = true;
                }

                ViewState["SelectedTableId"] = ddlTableName.SelectedValue;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string sResponse = string.Empty;
            XElement objXmlIn = null;
            try
            {
                objXmlIn = GetMessageTemplateForSave();
                bool bReturnStatus = CallCWS("HistoryMetaDataAdaptor.Save", objXmlIn, out sResponse, false, false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected override void LoadViewState(object savedState)
        {
            try
            {
                base.LoadViewState(savedState);
                AddingControlsToPanel();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }        

        private XElement GetMessageTemplateToGetColumns()
        {
            StringBuilder sbXml = new StringBuilder();
            sbXml.AppendFormat("<Message><Authorization></Authorization><Call><Function>HistoryMetaDataAdaptor.GetColumnsInfo</Function></Call><Document><Form>");
            sbXml.AppendFormat("<SelectedTableId>{0}</SelectedTableId>", Request["ddlTableName$TextBox"]);
            sbXml.Append("</Form></Document></Message>");
            return(new XElement(XElement.Parse(sbXml.ToString())));
        }       

        private XElement GetMessageTemplateToGetTables()
        {
            StringBuilder sbXml = new StringBuilder();
            sbXml.Append("<Message><Authorization></Authorization><Call><Function>HistoryMetaDataAdaptor.GetTables</Function></Call>");
            sbXml.Append("<Document><Form></Form></Document></Message>");
            return (new XElement(XElement.Parse(sbXml.ToString())));
        }

        private XElement GetMessageTemplateForSave()
        {
            StringBuilder sbXml = new StringBuilder();

            sbXml.Append("<Message><Authorization></Authorization><Call><Function>HistoryMetaDataAdaptor.Save</Function></Call><Document><Form>");
            sbXml.AppendFormat("<Table>{0}</Table>", ddlTableName.SelectedValue);
            sbXml.Append("<Columns>");

            for (int i = 1; i <= pnlColumns.Controls.Count / 5; i++)
            {
                sbXml.Append("<Column Id='");
                TextBox txtColumnId = (TextBox)pnlColumns.FindControl("txtColumnId_" + ddlTableName.SelectedItem + "_" + i);
                if (txtColumnId != null)
                {
                    sbXml.Append(txtColumnId.Text);
                }
                sbXml.Append("'");

                sbXml.Append(" UserPrompt='");
                TextBox txtUserPrompt = (TextBox)pnlColumns.FindControl("txtUsrPrompt_" + ddlTableName.SelectedItem + "_" + i);
                if (txtUserPrompt != null)
                {
                    sbXml.Append(txtUserPrompt.Text);
                }
                sbXml.Append("'");

                sbXml.Append(" RMDataType='");
                DropDownList ddlRMDataType = (DropDownList)pnlColumns.FindControl("ddlRMDataType_" + ddlTableName.SelectedItem + "_" + i);
                if (ddlRMDataType != null)
                {
                    sbXml.Append(ddlRMDataType.SelectedValue);
                }
                sbXml.Append("'");

                sbXml.Append(" SystemTableName='");
                TextBox txtSystemTableName = (TextBox)pnlColumns.FindControl("txtSystemTableName_" + ddlTableName.SelectedItem + "_" + i);
                if (txtSystemTableName != null)
                {
                    sbXml.Append(txtSystemTableName.Text);
                }
                sbXml.Append("'");

                sbXml.Append("/>");
            }
            sbXml.Append("</Columns></Form></Document></Message>");

            return (new XElement(XElement.Parse(sbXml.ToString())));
        }

        private void AddingControlsToPanel()
        {
            XElement objXmlIn = null;
            string sResponse = string.Empty;
            XElement objXOut = null;
            XElement objXRoot = null;
            try
            {                
                    pnlColumns.Controls.Clear();
                    string sTableName = Request["ddlTableName$TextBox"];
                    objXmlIn = GetMessageTemplateToGetColumns();
                    bool bReturnStatus = CallCWS("HistoryMetaDataAdaptor.GetColumnsInfo", objXmlIn, out sResponse, false, false);
                    objXOut = XElement.Parse(sResponse);
                    objXRoot = objXOut.XPathSelectElement("//Root");
                    int iCount = 0;

                    foreach (XElement objCol in objXRoot.Elements())
                    {
                        ++iCount;

                        TextBox txtColumnId = new TextBox();
                        txtColumnId.ID = "txtColumnId_" + sTableName + "_" + iCount;
                        txtColumnId.Text = objCol.Attribute("Id").Value;
                        txtColumnId.Attributes.Add("style", "display:none");
                        pnlColumns.Controls.Add(txtColumnId);

                        Label lblColumnName = new Label();
                        lblColumnName.ID = "lblColName_" + sTableName + "_" + iCount;
                        lblColumnName.Text = objCol.Attribute("ColumnName").Value;
                        lblColumnName.Width = new Unit(27, UnitType.Percentage);
                        pnlColumns.Controls.Add(lblColumnName);

                        TextBox txtUserPrompt = new TextBox();
                        txtUserPrompt.ID = "txtUsrPrompt_" + sTableName + "_" + iCount;
                        txtUserPrompt.Text = objCol.Attribute("UserPrompt").Value;
                        txtUserPrompt.Width = new Unit(22, UnitType.Percentage);
                        pnlColumns.Controls.Add(txtUserPrompt);

                        DropDownList ddlRMDataType = new DropDownList();
                        ddlRMDataType.ID = "ddlRMDataType_" + sTableName + "_" + iCount;

                        ddlRMDataType.Items.Add(new ListItem("Text", "0"));
                        ddlRMDataType.Items.Add(new ListItem("Number", "1"));
                        ddlRMDataType.Items.Add(new ListItem("Currency", "2"));
                        ddlRMDataType.Items.Add(new ListItem("Date", "3"));
                        ddlRMDataType.Items.Add(new ListItem("Time", "4"));
                        ddlRMDataType.Items.Add(new ListItem("Code", "6"));
                        ddlRMDataType.Items.Add(new ListItem("State", "9"));
                        ddlRMDataType.Items.Add(new ListItem("ClaimLookUp", "10"));
                        ddlRMDataType.Items.Add(new ListItem("CheckBox", "101"));
                        ddlRMDataType.Items.Add(new ListItem("ComboBox", "105"));
                        ddlRMDataType.Items.Add(new ListItem("CodeList", "14"));
                        ddlRMDataType.Items.Add(new ListItem("EventLookUp", "12"));
                        ddlRMDataType.Items.Add(new ListItem("Entity", "8"));
                        ddlRMDataType.Items.Add(new ListItem("EntityList", "16"));
                        ddlRMDataType.Items.Add(new ListItem("VehicleLookUp", "13"));
                        ddlRMDataType.Items.Add(new ListItem("MultiState", "15"));
                        ddlRMDataType.Items.Add(new ListItem("RadioButton", "103"));
                        ddlRMDataType.Items.Add(new ListItem("BankLookUp", "104"));
                        ddlRMDataType.Items.Add(new ListItem("EnhancedPolicyLookUp", "107"));
                        ddlRMDataType.Items.Add(new ListItem("Unknown", "-1"));
                        ddlRMDataType.Items.Add(new ListItem("CodeLookUp", "108"));
                        ddlRMDataType.Items.Add(new ListItem("Id", "109"));
                        ddlRMDataType.Items.Add(new ListItem("DateTime", "106"));
                        ddlRMDataType.Items.Add(new ListItem("PolicyNumberLookUp", "110"));

                        ddlRMDataType.SelectedIndex = ddlRMDataType.Items.IndexOf(ddlRMDataType.Items.FindByValue(objCol.Attribute("RMDataType").Value));
                        pnlColumns.Controls.Add(ddlRMDataType);

                        TextBox txtSystemTableName = new TextBox();
                        txtSystemTableName.ID = "txtSystemTableName_" + sTableName + "_" + iCount;
                        txtSystemTableName.Text = objCol.Attribute("SystemTableName").Value;
                        txtSystemTableName.Width = new Unit(22, UnitType.Percentage);
                        pnlColumns.Controls.Add(txtSystemTableName);
                    }                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
