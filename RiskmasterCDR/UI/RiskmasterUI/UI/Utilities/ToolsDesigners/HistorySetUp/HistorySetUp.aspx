﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HistorySetUp.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.HistoryDesigner.HistoryTracking" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="../../../Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>History SetUp</title>    
    <script language="JavaScript" type="text/javascript" src="../../../../Scripts/WaitDialog.js">        { var i; }</script>
     <script language="JavaScript" type="text/javascript" src="../../../../Scripts/HistoryDesigner.js"></script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <table>
        <tr>
            <td class="msgheader">
                History Tracking SetUp
            </td>
        </tr>
        <tr>
            <td>
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <div style="height: 20px;">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="height: 50px;">
                    History Tracking System enables you to track history of changes in selected fields
                    of selected tables of RISKMASTER.</div>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
            <td>
                <div style="height: 20px;">
                    For activating the History Tracking feature, click on the Setup Button.
                </div>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td>
                <div style="height: 70px;">
                    <asp:Button runat="server" ID="btnEnable" Text="Enable History Tracking SetUp" CssClass="button"
                        OnClick="btnEnable_Click" Width="200px" OnClientClick="pleaseWait.Show();"/></div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="height: 20px;">
                    To Disable History Tracking click the button below
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="height: 70px;">
                    <asp:Button runat="server" ID="btnDisable" Text="Disable History Tracking" CssClass="button"
                        OnClick="btnDisable_Click" Width="200px" OnClientClick="return fnDisableHistoryTracking();" />
                </div>
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="hdnEnableFlag" rmxref="/Instance/Document/Document/form/HistoryEnableFlag" />    
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
