﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HistoryMetaData.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.HistorySetUp.HistoryMetaData" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
     <table width="90%">
        <tr>
            <td class="msgheader">
                History Tracking MetaData SetUp
            </td>
        </tr>
        <tr>
            <td>
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        </table>
    <div>
        <div class="image">
            <asp:ImageButton ID="Save" onMouseOver="this.src='../../../../Images/tb_save_mo.png';this.style.zoom='110%'"
                onMouseOut="this.src='../../../../Images/tb_save_active.png';this.style.zoom='100%'"
                src="../../../../Images/tb_save_active.png" class="bold" ToolTip="Save" runat="server"
                OnClick="btnSave_Click" />
        </div>
        <br />
        <br />
        <asp:Label runat="server" ID="lblTableName" Text ="Select Table : " />
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" />
        <cc1:ComboBox ID="ddlTableName" runat="server" AutoCompleteMode="SuggestAppend" TabIndex="1"
            OnSelectedIndexChanged="ddlTableName_SelectedIndexChanged" AutoPostBack="true" >
        </cc1:ComboBox>
        <br />
        <br />
        <asp:Panel ID ="pnlLabels" runat="server" Visible ="false"  >
        <asp:Label runat="server" ID="lblColName" Text ="Column Name" Width ="26%" />
         <asp:Label runat="server" ID="lblUserPrompt" Text ="User Prompt" Width ="20%" />
          <asp:Label runat="server" ID="lblRMDataType" Text ="RMX Data Type" Width ="20%" />
           <asp:Label runat="server" ID="lblSysTableName" Text ="System Table Name" Width ="22%" />
           </asp:Panel>
        <asp:Panel ID="pnlColumns" runat="server" Height="65%" ScrollBars="Auto" Style="width: 93%"
            BorderColor="LightGray" BorderStyle="Ridge" BorderWidth="2px" Visible ="false" >
        </asp:Panel>
    </div>
    </form>
</body>
</html>
