﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.HistoryDesigner
{
    public partial class HistoryTracking : NonFDMBasePageCWS
    {
        const bool ENABLE_HISTORY = true;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    NonFDMCWSPageLoad("HistorySetUpAdaptor.GetHistoryFlag");
                    if (hdnEnableFlag.Text == ENABLE_HISTORY.ToString())
                    {
                        btnDisable.Enabled = true;
                        btnEnable.Text = "History Tracking SetUp";
                    }
                    else
                    {
                        btnDisable.Enabled = false;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnEnable_Click(object sender, EventArgs e)
        {
            string sOutResponse = string.Empty;
            try
            {
                if (btnEnable.Text == "Enable History Tracking SetUp")
                {
                    hdnEnableFlag.Text = ENABLE_HISTORY.ToString();
                    CallCWSFunction("HistorySetUpAdaptor.UpdateHistoryFlags", out sOutResponse);
                }

                if (sOutResponse == string.Empty || ErrorHelper.IsCWSCallSuccess(sOutResponse))
                {
                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/HistorySetUp/HistoryDesigner.aspx");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnDisable_Click(object sender, EventArgs e)
        {
            try
            {
                hdnEnableFlag.Text = (!ENABLE_HISTORY).ToString();

                NonFDMCWSPageLoad("HistorySetUpAdaptor.UpdateHistoryFlags");

                btnDisable.Enabled = false;
                btnEnable.Text = "Enable History Tracking SetUp";
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
