﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.Common;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.Text;

namespace Riskmaster.UI.Utilities.ToolsDesigners.AutoMailMergeSetUp
{
    public class PersonInvolvedCodes
    {
        public string Name
        {
            get;
            set;
        }
        public string ID
        {
            get;
            set;
        }
    }
    public partial class PersonInvolvedList : NonFDMBasePageCWS
    {
        private const int m_MAX_RECORD_PER_PAGE = 10;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            int iCount = 0;
            int iPageValue = 0;
            bool bIsSuccess = false;
            bool bReturnStatus = false;
            string sFormName = string.Empty;
            string sreturnValue = string.Empty;
            XElement xmlTemplate = null;
            PersonInvolvedCodes objPersonInvolvedCodes = null;
            List<PersonInvolvedCodes> codes = new List<PersonInvolvedCodes>();
            HiddenField txtPageNumber = (HiddenField)this.FindControl("pagenumber");
            if (txtPageNumber.Value == "")
            {
                pagenumber.Value = "1";
                
            }
            else
            {
                pagenumber.Value = txtPageNumber.Value;
                iPageValue = Conversion.CastToType<int>(txtPageNumber.Value, out bIsSuccess);
            }
            

            xmlTemplate = GetMessageTemplate();
            bReturnStatus = CallCWSFunction("AutoMailMergeAdaptor.GetPeopleInvolved", out sreturnValue, xmlTemplate);
            XmlDocument objPerInvList = new XmlDocument();
            objPerInvList.LoadXml(sreturnValue);

            XmlNode personInvolvedNode = null;
            personInvolvedNode = objPerInvList.SelectSingleNode("ResultMessage/Document/Document/GetPersonInvolvedList");

            pagecount.Value = objPerInvList.SelectSingleNode("ResultMessage/Document/Document/PageCount").Attributes["PageCount"].InnerText;
            
            XmlNodeList personInvolvedNodeList = null;
            personInvolvedNodeList = personInvolvedNode.SelectNodes("PersonList");

            if (personInvolvedNodeList != null)
            {
                foreach (XmlNode levelNode in personInvolvedNodeList)
                {
                    objPersonInvolvedCodes = new PersonInvolvedCodes();
                    displayname.Value = "Person Involved Added";
                    objPersonInvolvedCodes.ID = levelNode.Attributes["ID"].Value;
                    objPersonInvolvedCodes.Name = levelNode.Attributes["Name"].Value;
                    codes.Add(objPersonInvolvedCodes);
                }
            }
            else
            {
                displayname.Value = "";
                pagenumber.Value = "0";
                pagecount.Value = "0";
            }

            dgUARCodes.DataSource = codes;
            dgUARCodes.DataBind();


        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><Document>");
            sXml = sXml.Append("<GetPersonInvolvedList></GetPersonInvolvedList>");
            sXml = sXml.Append("<PageCount></PageCount>");
            sXml = sXml.Append("</Document></Document></Message>");

            XElement oTemplate = XElement.Parse(Convert.ToString(sXml));
            return oTemplate;
        }

        /// <summary>
        /// The Function Binds each row to the Data Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgUARCodes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink rowHyperLink = null;
                rowHyperLink = new HyperLink();
                rowHyperLink.NavigateUrl = "#";
                String sName = string.Empty;
                String sDesc = string.Empty;
                String sId = string.Empty;
                if (DataBinder.Eval(e.Row.DataItem, "Name") == null)
                {
                    sName = string.Empty;
                }
                else
                {
                    sName = DataBinder.Eval(e.Row.DataItem, "Name").ToString();
                }

                if (DataBinder.Eval(e.Row.DataItem, "ID") == null)
                {
                    sId = string.Empty;
                }
                else
                {
                    sId = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                }

                rowHyperLink.Attributes.Add("onclick", "javascript:selPeopleCode('" + AppHelper.escapeStrings(sName) + "','" + sId + "')");
                rowHyperLink.Text = sName;
                rowHyperLink.CssClass = "HeaderNavy";
                e.Row.Cells[0].Controls.Add(rowHyperLink);
                e.Row.Cells[0].CssClass = "Bold2";
            }
        }
    }

    
}