﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.AutoMailMergeSetUp
{
    public partial class AddEditAutoMailMergeAdditionalselection : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["selectedid"] != null)
                    {
                        ViewState["selectedFilterID"] = Request.QueryString["selectedid"];
                    }
                    else
                    {
                        ViewState["selectedFilterID"] = "";
                    }

                    if (Request.QueryString["templateid"] != null)
                    {
                        ViewState["templateid"] = Request.QueryString["templateid"];
                    }
                    else
                    {
                        ViewState["templateid"] = "";
                    }

                    if (Request.QueryString["filtername"] != null)
                    {
                        ViewState["selectedfiltername"] = Request.QueryString["filtername"].Replace("^^^", "(").Replace("@@@", ")");
                    }
                    else
                    {
                        ViewState["selectedfiltername"] = "";
                    }

                    if (Request.QueryString["mode"] != null)
                    {
                        ViewState["mode"] = Request.QueryString["mode"];
                    }
                    else
                    {
                        ViewState["mode"] = "";
                    }


                    if (ViewState["mode"].ToString() == "edit")
                    {
                        this.Page.Title = "Edit Auto Mail Merge Filter";
                    }
                    else
                    {
                        this.Page.Title = "Add Auto mail Merge Filter";
                    }

                    //rupal:start, MITS 26244
                    if (Request.QueryString["selectedtemplateFormID"] != null)
                    {
                        ViewState["selectedtemplateFormID"] = Request.QueryString["selectedtemplateFormID"];
                    }
                    else
                    {
                        ViewState["selectedtemplateFormID"] = "";
                    }

                    if (Request.QueryString["data"] != null)
                    {
                        ViewState["data"] = Request.QueryString["data"];
                    }
                    else
                    {
                        ViewState["data"] = "";
                    }

                    lblFilterNameForList.Text = ViewState["selectedfiltername"].ToString();
                    lblFilterNameForList1.Text = ViewState["selectedfiltername"].ToString();

                    FillFilterListBoxes();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }


        private void FillFilterListBoxes()
        {
            XmlDocument addEditFilterMailMergeDoc = null;

            addEditFilterMailMergeDoc = GetAddEditAdditionalMailMergeFilter();
            if (addEditFilterMailMergeDoc != null)
            {
                FillAvlFilterBodyParts(addEditFilterMailMergeDoc.SelectSingleNode("ResultMessage/Document/WPAAutoMailMergeFilterSetup/AvlBodyParts"));

                FillSelectedFilterBodyParts(addEditFilterMailMergeDoc.SelectSingleNode("ResultMessage/Document/WPAAutoMailMergeFilterSetup/SelBodyParts"));
            }
        }

        private XmlDocument GetAddEditAdditionalMailMergeFilter()
        {
            string cwsOutput = string.Empty;
            XmlDocument mailDoc = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "AutoMailMergeAdaptor.GetAdditionalFilterData";

                inputDocNode = GetInputDocForAddEditMailMergeFilter();
                cwsOutput = CallCWSFunctionFromAppHelper(inputDocNode, serviceMethodToCall);

                mailDoc = new XmlDocument();
                mailDoc.LoadXml(cwsOutput);

                return mailDoc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private XmlNode GetInputDocForAddEditMailMergeFilter()
        {
            string messageTemplageString = string.Empty;
            XmlDocument messageDoc = null;
            XmlNode templateIdNode = null;
            XmlNode selectedTemplateFormidNode = null;
            XmlNode filterNameNode = null;
            XmlNode filterIDNode = null;
            XmlNode modeNode = null;
            XmlNode dataNode = null;
            //rupal, MITS 26244

            messageTemplageString = GetAddEditAutomailMergeAdditionalFilterTemplate();
            messageDoc = new XmlDocument();
            messageDoc.LoadXml(messageTemplageString);

            templateIdNode = messageDoc.SelectSingleNode("WPAAutoMailMergeFilterSetup/templateid");

            if (templateIdNode != null)
            {
                templateIdNode.InnerText = ViewState["templateid"].ToString();
            }

            selectedTemplateFormidNode = messageDoc.SelectSingleNode("WPAAutoMailMergeFilterSetup/selectedformtemplateid");
            if (selectedTemplateFormidNode != null)
            {
                selectedTemplateFormidNode.InnerText = ViewState["selectedtemplateFormID"].ToString();
            }

            filterNameNode = messageDoc.SelectSingleNode("WPAAutoMailMergeFilterSetup/filtername");
            if (filterNameNode != null)
            {
                filterNameNode.InnerText = ViewState["selectedfiltername"].ToString();
            }

            filterIDNode = messageDoc.SelectSingleNode("WPAAutoMailMergeFilterSetup/filterID");
            if (filterIDNode != null)
            {
                filterIDNode.InnerText = ViewState["selectedFilterID"].ToString();
            }

            modeNode = messageDoc.SelectSingleNode("WPAAutoMailMergeFilterSetup/mode");
            if (modeNode != null)
            {
                modeNode.InnerText = ViewState["mode"].ToString();
            }

            dataNode = messageDoc.SelectSingleNode("WPAAutoMailMergeFilterSetup/data");
            if (dataNode != null)
            {
                dataNode.InnerText = ViewState["data"].ToString();
            }
            return messageDoc.SelectSingleNode("/");
        }

        private string GetAddEditAutomailMergeAdditionalFilterTemplate()
        {
            //rupal: updated for MITS 26244, added new tag <Database>
            XElement oTemplate = XElement.Parse(@"
                <WPAAutoMailMergeFilterSetup>
                  <AvlBodyParts></AvlBodyParts>
                  <SelBodyParts></SelBodyParts>
                  <templateid></templateid>
                  <selectedformtemplateid></selectedformtemplateid>
                  <filterID></filterID>
                  <filtername></filtername>
                  <mode></mode>
                  <data></data>
                </WPAAutoMailMergeFilterSetup>
            ");
            return oTemplate.ToString();
        }

        private void FillAvlFilterBodyParts(XmlNode avlFilterBodyPartsNode)
        {
            XmlNodeList avlOptionsList = null;
            string sCategoryID = string.Empty;
            string sCategoryDesc = string.Empty;

            if (avlFilterBodyPartsNode != null)
            {
                avlOptionsList = avlFilterBodyPartsNode.SelectNodes("Option");

                Available.Items.Clear();
                List<string> lstdatavalue = null;
                string data = ViewState["data"].ToString();
                if (!string.IsNullOrEmpty(data))
                {
                    lstdatavalue = new List<string>();
                    string[] dataList = data.Split(',');
                    foreach (string datavalue in dataList)
                    {
                        if (!string.IsNullOrEmpty(datavalue))
                        {
                            lstdatavalue.Add(datavalue);
                        }
                    }
                }
                
                foreach (XmlNode optionNode in avlOptionsList)
                {
                    sCategoryID = optionNode.Attributes["CategoryID"].Value;
                    sCategoryDesc = optionNode.Attributes["Description"].Value;
                    if (lstdatavalue != null)
                    {
                        if (!lstdatavalue.Contains(sCategoryID))
                        {
                            Available.Items.Add(new ListItem(sCategoryDesc,sCategoryID));
                        }
                    }
                    else
                    {
                        Available.Items.Add(new ListItem(sCategoryDesc, sCategoryID));
                    }
                    
                }
            }
        }

        private void FillSelectedFilterBodyParts(XmlNode selFilterBodyPartsNode)
        {
            XmlNodeList selOptionsList = null;
            string sCategoryID = string.Empty;
            string sCategoryDesc = string.Empty;

            if (selFilterBodyPartsNode != null)
            {
                selOptionsList = selFilterBodyPartsNode.SelectNodes("Option");

                Selected.Items.Clear();
                foreach (XmlNode optionNode in selOptionsList)
                {
                    
                    sCategoryID = optionNode.Attributes["CategoryID"].Value;
                    
                    sCategoryDesc = optionNode.Attributes["Description"].Value;
                    Selected.Items.Add(new ListItem(sCategoryDesc, sCategoryID));
                }
            }
        }

        #region ContactingWebService

        public string CallCWSFunctionFromAppHelper(XmlNode inputDocumentNode, string serviceMethodToCall)
        {
            XmlNode tempNode = null;
            string oMessageElement = string.Empty;
            string responseCWS = string.Empty;

            XmlDocument messageTemplateDoc = null;

            try
            {
                oMessageElement = GetMessageTemplate();
                messageTemplateDoc = new XmlDocument();
                messageTemplateDoc.LoadXml(oMessageElement);

                if (inputDocumentNode != null)
                {
                    tempNode = messageTemplateDoc.SelectSingleNode("Message/Document");
                    tempNode.InnerXml = inputDocumentNode.OuterXml;
                }

                if (!string.IsNullOrEmpty(serviceMethodToCall))
                {
                    tempNode = messageTemplateDoc.SelectSingleNode("Message/Call/Function");
                    tempNode.InnerText = serviceMethodToCall;
                }

                //get webservice call from Apphelper.cs
                responseCWS = AppHelper.CallCWSService(messageTemplateDoc.InnerXml);

                return responseCWS;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private string GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                        <Message>
                          <Authorization></Authorization>
                          <Call>
                            <Function></Function>
                          </Call>
                          <Document>
                          </Document>
                        </Message>
                    ");

            return oTemplate.ToString();
        }

        #endregion
    }
}