﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PersonInvolvedList.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.AutoMailMergeSetUp.PersonInvolvedList" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script language="JavaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>
    <title>Please Select Person Involved </title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body>
    <form id="frmData" runat="server">
    <br />
    <asp:HiddenField ID ="pagecount" runat ="server" />
    <asp:HiddenField ID ="pagenumber" runat ="server" />
    <asp:HiddenField ID ="nextpage" runat ="server" />
    <asp:HiddenField ID ="prevpage" runat ="server" />
    <asp:HiddenField ID="displayname" runat ="server" /> 
    <table width="95%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td>
                <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="errortext"></td>
        </tr>
    </table>
    <table width="95%" cellspacing="0" cellpadding="0">
        <%if (string.IsNullOrEmpty(displayname.Value)) displayname.Value = " No Person Involved Found";%>
        <tr>
            <td class="ctrlgroup"><%=displayname.Value%></td>
        </tr>
    </table>
    <br/>
    <%if (Convert.ToInt32(pagecount.Value)>1)   %>
    <%{ %>
    <table width="95%" cellspacing="0" cellpadding="1" border="0">
        <tr>
            <td colspan="4" class="headertext2">Page <%=Convert.ToInt32(pagenumber.Value)%> of <%=Convert.ToInt32(pagecount.Value)%>
            </td>
            <td nowrap="" colspan="2" align="right" class="headertext2">
      								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
      									<%if (Convert.ToInt32(pagenumber.Value) > 1) %>
      										<%{
                                                 prevpage.Value = (Convert.ToInt32(pagenumber.Value) - 1).ToString();  
                                            %>   
      										<a class="headerlink2" href="#" onclick="getUarPage(1)">First &gt;</a> 
      										&nbsp;&nbsp;|&nbsp;
      										<a class="headerlink2" href="#" onclick="getUarPage('<%=prevpage.Value%>')">&lt; Prev</a>
       										<%}%>
       										<%else %>
       										<%{%>
       										<a class="headerlink2" style="FONT-WEIGHT: bold;" disabled>First
       										&nbsp;&nbsp;|&nbsp;
      										&lt; Prev&nbsp;&nbsp;</a>
       										<%}%> 
      									
      									<%if (Convert.ToInt32(pagenumber.Value) < Convert.ToInt32(pagecount.Value)) %>
      										<%{
                                                 nextpage.Value = (Convert.ToInt32(pagenumber.Value) + 1).ToString();
      										%>   
      								        <a class="headerlink2" href="#" onclick="getUarPage('<%=nextpage.Value%>')">  |  Next &gt;</a>
      									    &nbsp;&nbsp;|&nbsp;&nbsp;
      									    <%nextpage.Value =pagecount.Value; %>
      								        <a class="headerlink2" href="#" onclick="getUarPage('<%=nextpage.Value%>')">Last</a>
      									    &nbsp;
      									    <%}%>
       										<%else %>
       										<%{%>
       										<a class="headerlink2" style="FONT-WEIGHT: bold;" disabled>| Next &gt;
       										&nbsp;&nbsp;|&nbsp;&nbsp;
       										Last
       										&nbsp;|</a>
       										<%}%>
      							
     </td>
        </tr>
     </table>  
     <%} %>  
    <asp:GridView ID="dgUARCodes" AutoGenerateColumns="False" runat="server" 
        Font-Bold="True" CellPadding="0"  PagerSettings-Mode="NumericFirstLast"
        GridLines="None" CellSpacing="3" Width="95%" OnRowDataBound ="dgUARCodes_RowDataBound"
         >
        <PagerStyle CssClass="headertext2" ForeColor="White" />
        <HeaderStyle CssClass="msgheader"/>
        <PagerSettings Mode="NumericFirstLast"></PagerSettings>
        <FooterStyle CssClass="headertext2" />
        <RowStyle CssClass="Bold2" />
        <Columns>
            <asp:TemplateField HeaderText="Person Involved Type" HeaderStyle-HorizontalAlign="Left" >
                
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:TemplateField>
            
        </Columns>
        <EmptyDataTemplate>
        <table cellspacing="3" width="95%">
            <tr >
				<td class="msgheader">Vehicle Name</td>
				
			</tr>
        </table>
        </EmptyDataTemplate> 
        <HeaderStyle BackColor="#B8CCE4" Font-Bold="True" ForeColor="White" />
        <FooterStyle BackColor="#B8CCE4" Font-Bold="True" ForeColor="White" />
    </asp:GridView>
    <%if (Convert.ToInt32(pagecount.Value)>1)   %>
    <%{ %>
    <table width="95%" cellspacing="0" cellpadding="1" border="0">
        <tr>
            <td colspan="4" class="headertext2">Page <%=Convert.ToInt32(pagenumber.Value)%> of <%=Convert.ToInt32(pagecount.Value)%>
            </td>
            <td nowrap="" colspan="2" align="right" class="headertext2">
      								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
      									<%if (Convert.ToInt32(pagenumber.Value) > 1) %>
      										<%{
                                                 prevpage.Value = (Convert.ToInt32(pagenumber.Value) - 1).ToString();  
                                            %>   
      										<a class="headerlink2" href="#" onclick="getUarPage(1)">First &gt;</a> 
      										&nbsp;&nbsp;|&nbsp;
      										<a class="headerlink2" href="#" onclick="getUarPage('<%=prevpage.Value%>')">&lt; Prev</a>
       										<%}%>
       										<%else %>
       										<%{%>
       										<a class="headerlink2" style="FONT-WEIGHT: bold;" disabled>First &gt;
       										&nbsp;&nbsp;|&nbsp;
      										&lt; Prev&nbsp;&nbsp;|</a>
       										<%}%> 
      									
      									<%if (Convert.ToInt32(pagenumber.Value) < Convert.ToInt32(pagecount.Value)) %>
      										<%{
                                                 nextpage.Value = (Convert.ToInt32(pagenumber.Value) + 1).ToString();
      										%>   
      								        <a class="headerlink2" href="#" onclick="getUarPage('<%=nextpage.Value%>')">Next &gt;</a> 
      									    &nbsp;&nbsp;|&nbsp;&nbsp;
      									    <%nextpage.Value =pagecount.Value; %>
      								        <a class="headerlink2" href="#" onclick="getUarPage('<%=nextpage.Value%>')">Last</a>
      									    &nbsp;
      									    <%}%>
       										<%else %>
       										<%{%>
       										<a class="headerlink2" style="FONT-WEIGHT: bold;" disabled>| Next &gt;
       										&nbsp;&nbsp;|&nbsp;&nbsp;
       										Last</a>
       										&nbsp;
       										<%}%>
      							
     </td>
        </tr>
     </table>  
     <%} %>  
    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Please wait while we process your request.." />
    <asp:HiddenField ID="hdnSortExpression" runat="server" Value="1" />
    <asp:TextBox style="display:none" runat="server" ID="UARList" />
        
    <asp:TextBox style="display:none" runat="server" ID="UARAddedList"  />
    </form>
</body>
</html>
