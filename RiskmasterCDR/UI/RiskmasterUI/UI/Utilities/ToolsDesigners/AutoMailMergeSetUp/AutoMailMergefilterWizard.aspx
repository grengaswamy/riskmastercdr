﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoMailMergefilterWizard.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.AutoMailMergeSetUp.AutoMailMergefilterWizard" ValidateRequest="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Auto Mail Merge Setup</title>   
     <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
     
    <script type="text/javascript" src="../../../../Scripts/form.js"></script>
    <script src="../../../Scripts/cul.js" type="text/javascript"></script>
        <%--vkumar258 - RMA-6037 - Starts --%>
<%--<script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>--%>
<link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>
    <script language="javascript">
        function closeAutoMailMergeWindow() {
            if (confirm('Are you sure you want to Cancel Auto Mail Merge Setup?'))
                window.close();

            return false;
        }
        function OpenAddEditOnDoubleClick(selectedMode) {
            __doPostBack('tCtrlAutoMailMergeWizard_UpatePaneForAutoFilterStep2', 'FiltersDoubleClickedMode=' + selectedMode);
        }

        function OpenAddEditTemplateOnDoubleClick(selectedMode) {
            __doPostBack('tCtrlAutoMailMergeWizard_UpatePaneForAutoFilterStep2', 'FiltersDoubleClickedMode=' + selectedMode);
        }
        

        function RefreshPageAfterAddEditFilter(updatedDataValue) {           
            __doPostBack('tCtrlAutoMailMergeWizard_UpatePaneForAutoFilterStep2', updatedDataValue);
        }

        function RefreshPageAfterAddEditTemplateFilter(updatedDataValue) {
            __doPostBack('tCtrlAutoMailMergeWizard_UpatePaneForAutoFilterStep2', updatedDataValue);
        }

        function OpenAddEditAutoMailMergeFilter(url) {
            window.open(url, "AddEditAutoMailMerge",
									"width=600,height=300,top=" + (screen.availHeight - 300) / 2 + ",left=" + (screen.availWidth - 500) / 2 + ",resizable=yes,scrollbars=yes");
        }

        function OpenAddEditAutoMailMergeTemplateFilter(url) {
            window.open(url, "AddEditAutoMailMergeAddSelection",
									"width=600,height=300,top=" + (screen.availHeight - 300) / 2 + ",left=" + (screen.availWidth - 500) / 2 + ",resizable=yes,scrollbars=yes");
        }

    </script>
</head>
<body>
<form id="frmData" runat="server">
    <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
    <asp:HiddenField ID="hdnScreenAction" runat="server" Value="" />
    <asp:Label ID="lblMessgeTitle" runat="server" CssClass="group" Width="99%" Visible="false" Text=""></asp:Label>
    <div>
    <asp:Wizard ID="CtrlAutoMailMergeWizard"  NavigationStyle-HorizontalAlign="Center" runat="server"  DisplaySideBar="false"  Width="98%"  
            Height="90%" ActiveStepIndex="0" Visible=false oncancelbuttonclick="CtrlAutoMailMergeWizard_CancelButtonClick" 
            onnextbuttonclick="CtrlAutoMailMergeWizard_NextButtonClick" 
            onpreviousbuttonclick="CtrlAutoMailMergeWizard_PreviousButtonClick"> 
                <StartNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="closeAutoMailMergeWindow();return false;" />
                  <asp:Button class="button" ID="StepPreviousButton" disabled="true" runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton" runat="server" Text="Finish" CausesValidation="True" OnClick="CtrlAutoMailMergeWizard_FinishButtonClick" /> 
                </StartNavigationTemplate>  
                <StepNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="closeAutoMailMergeWindow();return false;" />
                  <asp:Button class="button" ID="StepPreviousButton"  runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton"  runat="server" Text="Finish" CausesValidation="True" OnClick="CtrlAutoMailMergeWizard_FinishButtonClick" />
               </StepNavigationTemplate>
               <FinishNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="closeAutoMailMergeWindow();return false;" />
                  <asp:Button class="button" ID="StepPreviousButton"  runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" disabled="false" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton" runat="server"  OnClick="CtrlAutoMailMergeWizard_FinishButtonClick" Text="Finish" CausesValidation="True" /> 
               </FinishNavigationTemplate>
               <WizardSteps>

               <asp:WizardStep ID="AutoFilterStep1" runat="server" Title="">
                        <asp:HiddenField ID="hdnAutoFilterStep1Visited" runat="server" Value="" />
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width= "30%" valign="center">
                                    <img src="../../../../Images/automailmergesetup.gif" />
                                </td>
                                <td width= "70%" align="left" valign="center">The auto mail merge setup wizard will walk you through the <br>process of creating/updating your auto mail merge definitions.<br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">Which auto mail merge templates do you wish to use? <br>
                                    <asp:DropDownList ID="cboTemplate" runat="server" Width="335px"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">What do you want to call this auto mail merge definition? <br>
                                        <asp:TextBox ID="txtMailMergeName" Text="" size="60" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td width="40%" valign="top">
         								        Do not process mail merge templates for records that were created before this date.<br>
         								        <input type="text"  runat="server" id="txtMailMergeDate" size="15" onblur="dateLostFocus(this.id);"/><!--Modified by mdhamija MITS 27606-->
         								                            <%--vkumar258 - RMA_6037- Starts--%>
<%-- <input type="button"  class="DateLookupControl" id="txtCompletedOnbtn" value=""/>
                                                <script type="text/javascript">
                                                    Zapatec.Calendar.setup({ inputField: "CtrlAutoMailMergeWizard_txtMailMergeDate", ifFormat: "%m/%d/%Y", button: "txtCompletedOnbtn" });      
                                                </script>--%>
                                               <script type="text/javascript">
                                                   $(function () {
                                                       $("#CtrlAutoMailMergeWizard_txtMailMergeDate").datepicker({
                                                           showOn: "button",
                                                           buttonImage: "../../../../Images/calendar.gif",
                                                           //buttonImageOnly: true,
                                                           showOtherMonths: true,
                                                           selectOtherMonths: true,
                                                           changeYear: true
                                                       }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                                   });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                                            </td>                                            
                                       </tr>
                                    </table>
                                </td>
                            </tr>
                           <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                 <td colspan="2">&nbsp;</td>
                            </tr>
                        </table>
                    </asp:WizardStep>
                     
               <asp:WizardStep ID="AutoFilterStep2" runat="server" Title="">
                       <asp:HiddenField ID="hdnAutoFilterStep2Visited" runat="server" Value="" />
                       <asp:HiddenField ID="hdnAutoFilterStep2" runat="server" Value="" />
                       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                       <asp:UpdatePanel ID="UpatePaneForAutoFilterStep2" OnLoad="UpatePaneForAutoFilterStep2_Load" runat="server">
                        <ContentTemplate> 
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width= "30%" valign="middle">
                                    <img src="../../../../Images/automailmergesetup.gif" />
                                </td>
                                <td width= "70%" align="left" valign="middle">
                                    By applying criteria or filters you can define auto merge definitions
                                    <br>
                                    that reflect your business rules.  You can set criteria that 
                                    <br>
                                    will be used to determine what records will be candidates for 
                                    <br>
                                    auto mail merge generation.  NOTE: Broad selection of criteria 
                                    <br>
                                    can result in a large number of auto mail merge documents being 
                                    <br>
                                    generated.
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                        
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                            <td width="40%">
         						                Available Filter(s)<br>
                                                <asp:ListBox ID="AvailableFilters" runat="server" size="10" style="width:90%" Height="190px" ondblclick="return OpenAddEditOnDoubleClick('add');"></asp:ListBox>
                                            </td>
                                            <td width="20%" valign="middle" align="center">
                                                <asp:Button ID="btnAddAvlFilter" runat="server" Text="Add &gt;" onclick="btnAddAvlFilter_Click"  CssClass="button" Width="80"/>
                                                <br><br>
                                                <asp:Button ID="btnEditAvlFilter" runat="server" Text="Edit &gt;" onclick="btnEditAvlFilter_Click"  CssClass="button" Width="80"/>
                                                <br><br>
                                                <asp:Button ID="btnRemoveAvlFilter" runat="server" Text="< Remove" onclick="btnRemoveAvlFilter_Click"    CssClass="button" Width="80"/>
                                            </td>
                                            <td width="40%">
         							            Selected Filter(s)<br>
                                                <asp:ListBox ID="SelectedFilters" runat="server" size="10" style="width:90%" Height="190px" ondblclick="return OpenAddEditOnDoubleClick('edit');"></asp:ListBox>
                                            </td> 
                                       </tr>
                                      </table>
                                      <asp:HiddenField ID="hdnTemplateId"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnId"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnFilterName"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSqlFill"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnFilterType"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnData"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnMode" runat="server" Value="" />
                                      <asp:HiddenField ID="hdnDefValue" runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelectedLevelNode" runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelectedFiltersNode" runat="server" Value="" />
                                      <!--rupal-->
                                      <asp:HiddenField ID="hdnDatabase" runat="server" Value="" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
      						    </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:WizardStep> 

               <asp:WizardStep ID="SelectTemplateFilterStep" runat="server" Title="">
                       <asp:HiddenField ID="hdnAutoFilterStep3Visited" runat="server" Value="" />
                       <asp:HiddenField ID="hdnAutoFilterStep3" runat="server" Value="" />
                        <%--<asp:UpdatePanel ID="UpatePaneForAutoFilterStep3" OnLoad="UpatePaneForAutoFilterStep3_Load" runat="server" UpdateMode="Conditional">--%>
                        <ContentTemplate> 
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width= "30%" valign="middle">
                                    <img src="../../../../Images/automailmergesetup.gif" />
                                </td>
                                <td width= "70%" align="left" valign="middle">
                                    When the system generates mail merge documents it needs to
                                    <br>
                                    know which mail merge templates will be used. Select which 
                                    <br>
                                    templates will be used by this auto mail merge definition.Based
                                    <br>
                                    on the template selected, additional business rules may be 
                                    <br>
                                    available to further define the specific conditions under which 
                                    <br>
                                    the system generates mail merge documents. 
                                </td>
                            </tr>

                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                             <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                 <td colspan="2" align="center">Which mail merge template will be used? <br>
                                    <asp:DropDownList ID="cbomailmergetemp" runat="server" Width="335px" OnSelectedIndexChanged= "cbomailmergetemp_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                        
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                            <td width="40%">
         						                Available Filter(s)<br>
                                                <asp:ListBox ID="lstAvTempFilter" runat="server" size="10" style="width:90%" Height="190px" ondblclick="return OpenAddEditTemplateOnDoubleClick('add');"></asp:ListBox>
                                            </td>
                                            <td width="20%" valign="middle" align="center">
                                                <asp:Button ID="btnAddFilter" runat="server" Text="Add &gt;" onclick="btnAddAvltempFilter_Click"  CssClass="button" Width="80"/>
                                                <br><br>
                                                <asp:Button ID="btnEditFilter" runat="server" Text="Edit &gt;" onclick="btnEditAvltempFilter_Click"  CssClass="button" Width="80"/>
                                                <br><br>
                                                <asp:Button ID="BtnRemoveFilter" runat="server" Text="< Remove" onclick="btnRemoveAvltempFilter_Click"    CssClass="button" Width="80"/>
                                            </td>
                                            <td width="40%">
         							            Selected Filter(s)<br>
                                                <asp:ListBox ID="lstSelTempFilter" runat="server" size="10" style="width:90%" Height="190px" ondblclick="return OpenAddEditTemplateOnDoubleClick('edit');"></asp:ListBox>
                                            </td> 
                                       </tr>
                                      </table>
                                      <asp:HiddenField ID="hdnTemplateFormID"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelAdditionalFilterId"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelAdditionalFilterName"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelMode" runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelectedTemplateLevelNode"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelectedTemplateFiltersNode"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnTemplateData"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelTemplateFilterID" runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelTemplateFilterName" runat="server" Value="" />

                                      <asp:HiddenField ID="hdneditTemplateID"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdneditTemplateFormID"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdneditSelAdditionalFilterId"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdneditSelAdditionalFilterName"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdneditSelMode" runat="server" Value="" />
                                      <asp:HiddenField ID="hdneditSelectedTemplateLevelNode"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdneditSelectedTemplateFiltersNode"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdneditTemplateData"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdneditSelTemplateFilterID" runat="server" Value="" />
                                      <asp:HiddenField ID="hdneditSelTemplateFilterName" runat="server" Value="" />
                                      <asp:HiddenField ID="hdnselectedvalue" runat="server" Value="" />


                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
      						    </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                      <%--  </asp:UpdatePanel>--%>
                    </asp:WizardStep>

               <asp:WizardStep ID="EntityRole" runat="server" Title="">
                       <asp:HiddenField ID="hdnSelectEntityRoleStepVisited" runat="server" Value="" />
                        <%--<asp:UpdatePanel ID="UpatePaneForAutoFilterStep2" OnLoad="UpatePaneForAutoFilterStep2_Load" runat="server">--%>
                        <asp:UpdatePanel ID="UpatePaneForSelectEntityRoleStep" runat="server">
                        <ContentTemplate> 
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width= "30%" valign="middle">
                                    <img src="../../../../Images/automailmergesetup.gif" />
                                </td>
                                <td width= "70%" align="left" valign="middle">
                                    This step will allow you to limit the information included in 
                                    <br>
                                    merged documents to reflect that of only specific entity roles or 
                                    <br>
                                    types and/or specific types and statuses of other information.
                                    <br>
                                    The selections below are enabled based on the template(s) 
                                    <br>
                                    you selected in Step 3.You may further refine the information on
                                    <br>
                                    system-generated merged letters by selecting specific roles or 
                                    <br>
                                    types for the enabled entities.
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                            <td width="45%">
         						                 Person(s) Involved Types<br />
                                                 <asp:ListBox ID="lstPersonInvolued" runat="server" size="10" style="width:70%" Height="70px" Enabled="false"></asp:ListBox>
                                                 <asp:TextBox id="lstPersonInvolued_lst" style="display:none" runat ="server" RMXType=""/>
                                                 <asp:Button ID="btnPersonCodeLookup" runat="server" CssClass="CodeLookupControl" Enabled="false" OnClientClick="return  getPersonInvolued();"/>
                                                 <asp:Button ID="btnPersonRemove" runat="server" CssClass="BtnRemove" Enabled="false" OnClientClick=" return deleteSelectedPeople()"/>
                                            </td>
                                            <td width="10%" valign="middle" align="center">
                                            <td width="45%">
                                                Defendant Types<br>
                                                <uc:MultiCode runat="server" ID="lstDefendantType" width="200px" height="88px"  CodeTable="DEFENDANT_TYPE" ControlName="lstDefendantType" RMXType="codelist" Enabled="false"/>
                                                <%--<asp:ListBox ID="lstDefendantType" runat="server" size="10" style="width:70%" Height="70px" Enabled="false" SelectionMode="Multiple"></asp:ListBox>
                                                <input type="button" class="CodeLookupControl" id="btnDefendantCodeLookup" value="" runat="server" disabled ="disabled" onclick="return selectCode('DEFENDANT_TYPE','CtrlAutoMailMergeWizard_lstDefendantType');"/>
                                                <input type="button" class="BtnRemove" id="btnDefendantRemove" value="" runat="server" disabled ="disabled" onclick=" return true"/>--%>
                                            </td>
                                       </tr>
                                      </table>
                                </td>
                            </tr>

                            <tr>
                            <td colspan="2">
                               <table cellspacing="0" cellpadding="0" width="100%">
                               <tr>
                                <td width="45%">
                                    Claimant Types<br>
                                    <uc:MultiCode runat="server" ID="lstClaimantType" width="200px" height="88px"  CodeTable="CLAIMANT_TYPE" ControlName="lstClaimantType" RMXType="codelist" Enabled="false"/>
                                    <%--<asp:ListBox ID="lstClaimantType" runat="server" size="10" style="width:70%" Height="70px" Enabled="false"></asp:ListBox>
                                    <input type="button" class="CodeLookupControl" id="btnClaimantCodeLookup" value="" runat="server" disabled ="disabled" onclick=" return true"/>
                                    <input type="button" class="BtnRemove" id="btnClaimantRemove" value="" runat="server" disabled ="disabled" onclick=" return true"/>--%>
                                </td>
                                 <td width="10%" valign="middle" align="center">
                                <td width="45%">
         						   ExpertWitness Types<br>
                                                <uc:MultiCode runat="server" ID="lstExpertType" width="200px" height="88px"  CodeTable="EXPERT_TYPE" ControlName="lstExpertType" RMXType="codelist" Enabled="false"/>
                                                <%--<asp:ListBox ID="ListBox4" runat="server" size="10" style="width:70%" Height="70px"></asp:ListBox>
                                                <input type="button" class="CodeLookupControl" id="Button5" value="" runat="server"/>
                                                <input type="button" class="BtnRemove" id="Button6" value="" runat="server"/>--%>
                                </td>
                                </tr>
                            </table>
                            </td>
                            </tr>

                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>

                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
      						    </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                      <asp:HiddenField ID="hdnPersonInvolvedValue"  runat="server" Value="" />
      								  <asp:HiddenField ID="hdnClaimantvalues"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnExpertvalues"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnDefendantvalues"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnAdjustervalue"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnCasemanagervalue"  runat="server" Value="" />
      						    </td>
                            </tr>
                            <tr>
                            </tr>
                             
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:WizardStep>

               <asp:WizardStep ID="SelectPrinterStep" runat="server" Title="">
                       <asp:HiddenField ID="hdnSelectPrinterStepVisited" runat="server" Value="" />
                        <input type="hidden" runat="server" value="" id="hdnMailMergeId" />
                        <%--<asp:UpdatePanel ID="UpatePaneForAutoFilterStep2" OnLoad="UpatePaneForAutoFilterStep2_Load" runat="server">--%>
                        <asp:UpdatePanel ID="UpatePaneForSelectPrinterStep" runat="server">
                        <ContentTemplate> 
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width= "30%" valign="middle">
                                    <img src="../../../../Images/automailmergesetup.gif" />
                                </td>
                                <td width= "70%" align="left" valign="middle">
                                    This screen allows you to select a designated printer for the
                                    <br>
                                    system to use when printing the completed merged 
                                    <br>
                                    documents. If no selection is made here, the default printer 
                                    <br>
                                    defined in the Line of Business Parameter Setup area of
                                    <br>
                                    Utilities will be used.
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                            <td width="50%">
         						                Following Printers are attached with the Server:<br>
                                            </td>
                                            <td width="50%">
                                                <asp:DropDownList ID="cboprinter" runat="server" Width="335px" AutoPostBack='true' onselectedindexchanged="cboprinter_SelectedIndexChanged"></asp:DropDownList>
                                            </td>
                                       </tr>
                                      </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                            <td width="50%">
         						                Please Choose Paper Bin For Documents<br>
                                            </td>
                                            <td width="50%">
                                                <asp:DropDownList ID="cboPaperBin" runat="server" Width="335px"></asp:DropDownList>
                                            </td>
                                            </tr>
                                      </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
      						    </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>

                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        <asp:HiddenField ID="hdnprinter" runat="server"  Value="" />
                                      <asp:HiddenField ID="hdnpaper"  runat="server" Value="" />
      						    </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:WizardStep>

               </WizardSteps>
       </asp:Wizard>
    <asp:Wizard ID="CtrlBestPracticeMailMergeWizard" runat="server" NavigationStyle-HorizontalAlign="Center" DisplaySideBar="false" 
             ActiveStepIndex="0" Height="90%"  
            Width="98%" oncancelbuttonclick="CtrlBestPracticeMailMergeWizard_CancelButtonClick" 
            onfinishbuttonclick="CtrlBestPracticeMailMergeWizard_FinishButtonClick" 
            onnextbuttonclick="CtrlBestPracticeMailMergeWizard_NextButtonClick" 
            onpreviousbuttonclick="CtrlBestPracticeMailMergeWizard_PreviousButtonClick"> 
                <StartNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="Javascript:return window.close();" />
                  <asp:Button class="button" ID="StepPreviousButton" disabled="true" runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton" disabled="true" runat="server" CommandName="Finish" Text="Finish" CausesValidation="True" OnClientClick="FinishClick();return false;" /> 
                </StartNavigationTemplate>  
                <StepNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="Javascript:return window.close();" />
                  <asp:Button class="button" ID="StepPreviousButton"  runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton" disabled="true" runat="server" CommandName="Finish" Text="Finish" CausesValidation="True" OnClientClick="FinishClick();return false;" /> 
               </StepNavigationTemplate>
               <FinishNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="CancelClick();return false;" />
                  <asp:Button class="button" ID="StepPreviousButton"  runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" disabled="true" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton" runat="server"  CommandName="MoveComplete" Text="Finish" CausesValidation="True" /> 
               </FinishNavigationTemplate>
                <WizardSteps>
                    <asp:WizardStep ID="WizardStep1"  runat="server" Title="Step1">
                         <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width= "30%" valign="center">
                                    <img src="../../../../Images/automailmergesetup.gif" />
                                </td>
                                 <td width= "70%" align="left" class="AutoDiarySetupTitleBig" valign="center">
                                    Work<br><br>Processing<br><br>Administrator<br><br>
                                 </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="AutoDiarySetupTitleSmall">
                                    Best Practices for Risk and Claims Management
                                 </td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                        </table>
                    </asp:WizardStep>
                    <asp:WizardStep ID="SpecificWizardSelectionStep"  runat="server" Title="Step1">
                    </asp:WizardStep>
                    <asp:WizardStep ID="BestPracticeWizardStep1"  runat="server" Title="Step1">
                    </asp:WizardStep> 
                    <asp:WizardStep ID="BestPracticeWizardStep2"  runat="server" Title="Step1">
                    </asp:WizardStep> 
                    <asp:WizardStep ID="BestPracticeWizardStep3"  runat="server" Title="Step1">
                    </asp:WizardStep> 
                    <asp:WizardStep ID="WizardStep5"  runat="server" Title="Step1">
                    </asp:WizardStep> 
                </WizardSteps> 
            </asp:Wizard>
    </div>
    </form>
</body>
</html>
