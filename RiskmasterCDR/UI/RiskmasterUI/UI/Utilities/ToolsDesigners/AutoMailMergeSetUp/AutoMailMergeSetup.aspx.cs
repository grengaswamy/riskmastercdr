﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.AutoMailMergeSetUp
{
    public partial class AutoMailMergeSetup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    RefreshListBox();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement AutoMailMergeListTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <Message>
                  <Authorization></Authorization>
                  <Call>
                    <Function>AutoMailMergeAdaptor.Get</Function>
                  </Call>
                  <Document>
                    <AutoMailMergeList>
                    </AutoMailMergeList>
                  </Document>
                </Message>
            ");

            return oTemplate;
        }

        private XElement DeleteAutoMailMergeTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            
            <Message>
                  <Authorization></Authorization>
                  <Call>
                    <Function>AutoMailMergeAdaptor.Delete</Function>
                  </Call>
                  <Document>
                    <AutoMailMergeId></AutoMailMergeId> 
                  </Document>
                </Message>
            ");

            return oTemplate;
        }

        private string GetInputXmlForAutoMailMergeList()
        {
            XElement objInputMessageTemplate = null;

            objInputMessageTemplate = AutoMailMergeListTemplate();
            return objInputMessageTemplate.ToString();
        }

        private string GetInputXmlForDeleteMailMerge()
        {
            XElement objInputMessageTemplate = null;

            objInputMessageTemplate = DeleteAutoMailMergeTemplate();
            objInputMessageTemplate.XPathSelectElement("./Document/AutoMailMergeId").Value = AutoMailMergeList.SelectedItem.Value;

            return objInputMessageTemplate.ToString();
        }

        private XmlDocument ProcessRequestForAutoMailMerge(string inputMessageForProcess)
        {
            string sProcessResponse = string.Empty;
            XmlDocument autoMailMergeDoc = null;

            sProcessResponse = AppHelper.CallCWSService(inputMessageForProcess);

            if (sProcessResponse != string.Empty)
            {
                autoMailMergeDoc = new XmlDocument();
                autoMailMergeDoc.LoadXml(sProcessResponse);
            }
            return autoMailMergeDoc;
        }

        private void PopulateListBoxFromAutoMailMergeList(XmlNode autoMailMergeListNode)
        {
            XmlNodeList listRowNodeList = null;
            ListItem autoMailMergeListItem = null;

            if (autoMailMergeListNode != null)
            {
                listRowNodeList = autoMailMergeListNode.SelectNodes("List");

                AutoMailMergeList.Items.Clear();

                foreach (XmlNode rowNode in listRowNodeList)
                {
                    autoMailMergeListItem = new ListItem();
                    autoMailMergeListItem.Text = rowNode.Attributes["AutoName"].Value;
                    autoMailMergeListItem.Value = rowNode.Attributes["RowId"].Value;
                    AutoMailMergeList.Items.Add(autoMailMergeListItem);
                }
            }
        }

        private void DeleteSelectedMailMerge()
        {
            string sInputMessageProcess = string.Empty;

            sInputMessageProcess = GetInputXmlForDeleteMailMerge();
            ProcessRequestForAutoMailMerge(sInputMessageProcess);
            RefreshListBox();
        }

        private void RefreshListBox()
        {
            XmlDocument autoMailMergeListDoc = null;
            XmlNode autoMailMergeListNode = null;
            string sInputMessageForProcess = string.Empty;

            sInputMessageForProcess = GetInputXmlForAutoMailMergeList();
            autoMailMergeListDoc = ProcessRequestForAutoMailMerge(sInputMessageForProcess);

            if (autoMailMergeListDoc != null)
            {
                autoMailMergeListNode = autoMailMergeListDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeList");
                PopulateListBoxFromAutoMailMergeList(autoMailMergeListNode);
            }
        }

        protected void Delete_AutoMailMergeList_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DeleteSelectedMailMerge();
                RefreshListBox();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

    }
}