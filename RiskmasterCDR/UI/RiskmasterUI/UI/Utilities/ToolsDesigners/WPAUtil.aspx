﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WPAUtil.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.WPAUtil" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WPA Diary Utilities</title>
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <!--**ksahu5-ML-MITS34183 Start **--> 
    <script src="../../../Scripts/form.js" type="text/javascript"></script>

    <%--<script type="text/javascript" src="../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>--%>
    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <!--**ksahu5-ML-MITS34183 End **--> 
    <script type="text/javascript">

        function startUp() {
            document.forms[0].lstFromUsers.selectedIndex = -1;
            document.forms[0].lstToUsers.selectedIndex = -1;
            document.forms[0].chkTransferOpen.checked = true;
            document.forms[0].chkPurgeOpen.checked = true;
        }


        function PurgeDiary() {
            if (document.forms[0].lstUsers.value == "") {
                //ksahu5-ML-MITS34183 Start 
                // alert("Diary owner is not selected.");
                alert(WPAUtilValidations.PDDiaryOwnerCheck);
                return false;
            }
            if ((document.forms[0].chkPurgeClose.checked == false) && (document.forms[0].chkPurgeOpen.checked == false)) {
                //alert("Diary status did not specified.");
                alert(WPAUtilValidations.PDDDiaryStatusCheck);
                return false;
            }
            if (trim(document.forms[0].txtPurgeFromDate.value) == '') {
                //alert("Date range was not closed.");
                alert(WPAUtilValidations.PDFromDateRangeCheck);
                document.forms[0].txtPurgeFromDate.focus();
                return false;
            }
            if (trim(document.forms[0].txtPurgeToDate.value) == '') {
                //alert("Date range was not closed.");
                alert(WPAUtilValidations.PDToDateRangeCheck);
                //ksahu5-ML-MITS34183 End 
                document.forms[0].txtPurgeToDate.focus();
                return false;
            }
            //ksahu5: ML Change start: MITS 34183
            //zalam 05/22/2008 Mits:-12303 Start
            /*var sFromDate = trim(document.forms[0].txtPurgeFromDate.value);
            var sToDate = trim(document.forms[0].txtPurgeToDate.value);

            // akaushik5 Changed for MITS 31595 Starts
            var dFromDate = getDateObject(sFromDate, "/");
            var dToDate = getDateObject(sToDate, "/");
            var dFromDate = formatDate(sFromDate);
            var dToDate = formatDate(sToDate);
             //akaushik5 Changed for MITS 31595 Ends
            if (dFromDate.getTime() > dToDate.getTime()) {
                alert("From Date cannot be greater than To Date");
                return false;
            }*/
            //zalam 05/22/2008 Mits:-12303 End
            //var target = $("#txtPurgeFromDate");
            //var sectarget = $("#txtPurgeToDate");
            //var inst = $.datepicker._getInst(target[0]);
            //var secinst = $.datepicker._getInst(sectarget[0]);
            //fdate = inst.selectedDay;
            //fmonth = inst.selectedMonth;
            //fyear = inst.selectedYear;

            //secdate = secinst.selectedDay;
            //secmonth = secinst.selectedMonth;
            //secyear = secinst.selectedYear;
            //var sFromDate = new Date();
            //sFromDate.setFullYear(fyear, fmonth, fdate);

            //var sToDate = new Date();
            //sToDate.setFullYear(secyear, secmonth, secdate);

            var sFromDate = new Date($('#txtPurgeFromDate').val());    // RMA-15060    pmanoharan5
            var sToDate = new Date($('#txtPurgeToDate').val());        // RMA-15060    pmanoharan5    

            if (sFromDate > sToDate) {
                alert(WPAUtilValidations.PDDateCompare);
                return false;
            }
            //ksahu5: ML Change End MITS 34183

            return true;
        }
        

        function TransferDiary() {
           if (document.forms[0].lstFromUsers.value == "") {
                //ksahu5-ML-MITS34183 Start 
                // alert("Diary From owner is not selected.");
                alert(WPAUtilValidations.TDDiaryFromOwnerCheck);
                return false;
            }
            if (document.forms[0].lstToUsers.value == "") {
                // alert("Diary To owner is not selected.");
                alert(WPAUtilValidations.TDDiaryToOwnerCheck);
                return false;
            }
            if (trim(document.forms[0].lstToUsers.value) == trim(document.forms[0].lstFromUsers.value)) {
                //alert("Two Transfer Parties cannot be the same.");
                alert(WPAUtilValidations.TDTransferpartyCheck);
                return false;
            }
            if ((document.forms[0].chkTransferClose.checked == false) && (document.forms[0].chkTransferOpen.checked == false)) {
                //alert("Diary status did not specified.");
                alert(WPAUtilValidations.TDDiaryStatusCheck);
                return false;
            }
            if (trim(document.forms[0].txtTransferFromDate.value) == '') {
                // alert("Date range was not closed.");
                alert(WPAUtilValidations.TDFromDateCheck);
                document.forms[0].txtTransferFromDate.focus();
                return false;
            }
            if (trim(document.forms[0].txtTransferToDate.value) == '') {
                // alert("Date range was not closed.");
                alert(WPAUtilValidations.TDToDateCheck);
                //ksahu5-ML-MITS34183 End 
                document.forms[0].txtTransferToDate.focus();
                return false;
            }
            //Parijat: Mits 11870
            //ksahu5: ML Change Start: MITS 34183
           /* var sFromDate = trim(document.forms[0].txtTransferFromDate.value);
            var sToDate = trim(document.forms[0].txtTransferToDate.value);
            // akaushik5 Changed for MITS 31595 Starts
            // var dFromDate = getDateObject(sFromDate, "/");
            // var dToDate = getDateObject(sToDate, "/");
            var dFromDate = formatDate(sFromDate);
            var dToDate = formatDate(sToDate);
            // akaushik5 Changed for MITS 31595 Ends
            if (dFromDate.getTime() > dToDate.getTime()) {
            //   alert("From Date cannot be greater than To Date");
                return false;
            }*/
            
            //var target = $("#txtTransferFromDate");
            //var sectarget = $("#txtTransferToDate");
            //var inst = $.datepicker._getInst(target[0]);
            //var secinst = $.datepicker._getInst(sectarget[0]);
            //fdate = inst.selectedDay;
            //fmonth = inst.selectedMonth;
            //fyear = inst.selectedYear;

            //secdate = secinst.selectedDay;
            //secmonth = secinst.selectedMonth;
            //secyear = secinst.selectedYear;
            //var sFromDate = new Date();
            //sFromDate.setFullYear(fyear, fmonth, fdate);

            //var sToDate = new Date();
            //sToDate.setFullYear(secyear, secmonth, secdate);

            var sFromDate = new Date($('#txtTransferFromDate').val());    // RMA-15060    pmanoharan5
            var sToDate = new Date($('#txtTransferToDate').val());        // RMA-15060    pmanoharan5   

            if (sFromDate > sToDate) {
                alert(WPAUtilValidations.TDDateCompare);
                return false;
            }
            //ksahu5: ML Change End MITS 34183
            return true;
        }

        // akaushik5 Added for MITS 31595 Starts
        function formatDate(strDate) {
            var arrDate = strDate.split('/');
            var formatedString = arrDate[1] + "/" + (parseInt(arrDate[0],10) - 1).toString() + "/" + arrDate[2];
            return getDateObject(formatedString, "/");
        }
        // akaushik5 Added for MITS 31595 Starts

        function TabSelected(ctrl) {
            if (ctrl.className = "Selected") {
                document.forms[0].SysTabSelected.value = ctrl.name + "**" + ctrl.className;
            }
        }


        function selectSelectedTab() {

            if (trim(document.forms[0].SysTabSelected.value) == "") {
                return;
            }
            for (i = 0; i < document.all.length; i++) {
                if (document.all[i].id != null) {
                    if (document.all[i].id.substring(0, 7) == 'FORMTAB')
                        document.all[i].style.display = "none";
                    if (document.all[i].id.substring(0, 4) == 'TABS')
                        document.all[i].className = "NotSelected";
                    if (document.all[i].id.substring(0, 8) == 'LINKTABS')
                        document.all[i].className = "NotSelected1";
                }
            }
            arr = document.forms[0].SysTabSelected.value.split("**");
            ctrl = document.getElementById(arr[0]);
            ctrl.className = arr[1];
            eval('document.all.FORMTAB' + arr[0] + '.style.display=""');
            eval('document.all.TABS' + arr[0] + '.className="Selected"');
            eval('document.all.LINKTABS' + arr[0] + '.className="Selected"');
        }

    </script>

</head>
<body onload="loadTabList();parent.MDIScreenLoaded();" style="width: 98%">
    <form id="frmData" runat="server">
    <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
    <input type="text" name="SysFocusFields" id="SysFocusFields" style="display: none" />
    <input type="hidden" runat="server" value="" id="SysTabSelected" />
    <input type="hidden" name="hTabName" />
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="msgheader" colspan="4">
            <%-- **ksahu5-ML-MITS34183 Start **    --%>
               <%-- Diary Utilities--%>
                <asp:Label ID="lblDiaryUtilities" runat="server" Text="<%$ Resources:lblDiaryUtilities %>"></asp:Label>
             <%--**ksahu5-ML-MITS34183 End ** --%>
            </td>
        </tr>
    </table>
    <table border="0" cellspacing="0" celpadding="0">
        <tr>
            <td class="Selected" nowrap="true" name="TABSTransferDiary" id="TABSTransferDiary">
                <a class="Selected" href="#" onclick="tabChange(this.name);TabSelected(this);return false;"
                    name="TransferDiary" id="LINKTABSTransferDiary"><span style="text-decoration: none">
                      <%-- **ksahu5-ML-MITS34183 Start **   --%>
                        <%--Transfer Diary--%>
                        <asp:Label ID="lblTransferDiary" runat="server" Text="<%$ Resources:lblTransferDiary %>"/>
                          <%-- **ksahu5-ML-MITS34183 End **   --%>
                        </span> </a>
                       
            </td>
            <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                &nbsp;&nbsp;
            </td>
            <td class="NotSelected" nowrap="true" name="TABSPurgeDiary" id="TABSPurgeDiary">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);TabSelected(this);return false;"
                    name="PurgeDiary" id="LINKTABSPurgeDiary"><span style="text-decoration: none">
                   <%-- **ksahu5-ML-MITS34183 Start **    --%>   
                 <%-- Purge Diary--%>
                  <asp:Label ID="lblPurgeDiary" runat="server" Text="<%$ Resources:lblPurgeDiary %>"></asp:Label>
                    <%-- **ksahu5-ML-MITS34183 End **    --%>
                    </span> </a>
            </td>
        </tr>
    </table>
    <div class="singletopborder">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%" name="FORMTABTransferDiary"
                        id="FORMTABTransferDiary" style="">
                        <tr>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0" width="80%">
                                    <tr>
                                        <td>
                                            <table border="0" width="48%" align="left">
                                                <tr>
                                                    <td width="100%" align="left">
                                                      <%--**ksahu5-ML-MITS34183 Start ** --%>
                                                        <%--From Owner :--%>
                                                         <asp:Label ID="lblFromOwner" runat="server" Text="<%$ Resources:lblFromOwner %>"></asp:Label>
                                                       <%-- **ksahu5-ML-MITS34183 End ** --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:DropDownList ID="lstFromUsers" Width="150px" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <img src="../../../Images/ArrowWPAUtil1.jpg" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                      <%--**ksahu5-ML-MITS34183 Start ** --%>
                                                        <%--To Owner :--%>
                                                        <asp:Label ID="lblToOwner" runat="server" Text="<%$ Resources:lblToOwner %>"></asp:Label>
                                                      <%-- **ksahu5-ML-MITS34183 End ** --%>
                                                     </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:DropDownList Width="150px" ID="lstToUsers" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table border="0" style="width: 75%">
                                                <tr width="100%">
                                                    <td class="ctrlgroup" colspan="2">
                                                   <%--  **ksahu5-ML-MITS34183 Start ** --%>
                                                       <%-- Diary Status--%>
                                                        <asp:Label ID="lblTDDiaryStatus" runat="server" Text="<%$ Resources:lblTDDiaryStatus %>"></asp:Label>
                                                      <%-- **ksahu5-ML-MITS34183 End ** --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                   <%--  **ksahu5-ML-MITS34183 Start ** --%>
                                                     <%--    Open Diary :--%>
                                                     <asp:Label ID="lblTDOpenDiary" runat="server" Text="<%$ Resources:lblTDOpenDiary %>"></asp:Label>
                                                     <input type="checkbox" value="True" id="chkTransferOpen" runat="server" onchange="ApplyBool(this);"/>
                                                    <%-- **ksahu5-ML-MITS34183 End ** --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                      <%-- **ksahu5-ML-MITS34183 Start **--%> 
                                                       <%-- Close Diary :--%>
                                                        <asp:Label ID="lblTDCloseDiary" runat="server" Text="<%$ Resources:lblTDCloseDiary %>"></asp:Label>
                                                        <input type="checkbox" value="True" id="chkTransferClose" onchange="ApplyBool(this);"
                                                            runat="server"/>
                                                 <%--     **ksahu5-ML-MITS34183 End ** --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ctrlgroup" colspan="2">
                                                    <%-- **ksahu5-ML-MITS34183 Start ** --%>
                                                      <%--  Due Date Range--%>
                                                       <asp:Label ID="lblTDDueDateRange" runat="server" Text="<%$ Resources:lblTDDueDateRange %>"></asp:Label>
                                                 <%--    **ksahu5-ML-MITS34183 End ** --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    <%-- **ksahu5-ML-MITS34183 Start ** --%>
                                                        <%--From :--%>
                                                          <asp:Label ID="lblTDFrom" runat="server" Text="<%$ Resources:lblTDFrom %>"></asp:Label>
                                                     <!--**ksahu5-ML-MITS34183 End **--> 
                                                       
                                                      <%--  <input type="text" value="" size="28" id="txtTransferFromDate" runat="server"
                                                            onblur="dateLostFocus(this.id);" /><input type="button" class="button" value="..."
                                                                id="btnTransferFromDate" />

                                                        <script type="text/javascript">
                                                            //Ankit-MITS 31595-change date format to ddmmyyyy from mmddyyyy
                                                            // akaushik5 MITS 31595 Reverted back the changes
                                                            Zapatec.Calendar.setup(
																		{
																		    inputField: "txtTransferFromDate",
																		    ifFormat: "%m/%d/%Y",
																		    button: "btnTransferFromDate",
																		    position: [460, 220]
																		}
																		);
                                                            //Ankit-End Changes
                                                        </script>--%>
                                                        <input type="text" value="" size="28" id="txtTransferFromDate" runat="server"
                                                            onblur="dateLostFocus(this.id);" />
                                                             <script type="text/javascript">
                                                                 $(function () {
                                                                     $("#txtTransferFromDate").datepicker({
                                                                         showOn: "button",
                                                                         buttonImage: "../../../Images/calendar.gif",
                                                                         //buttonImageOnly: true,
                                                                         showOtherMonths: true,
                                                                         selectOtherMonths: true,
                                                                         changeYear: true
                                                                     }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                                                 });
                                                            </script>
                                                         <!--**ksahu5-ML-MITS34183 End **--> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                       <%--**ksahu5-ML-MITS34183 Start ** --%>
                                                      <%--  To &nbsp;&nbsp;&nbsp;:--%>
                                                         <asp:Label ID="lblTDTo" runat="server" Text="<%$ Resources:lblTDTo %>"></asp:Label>

                                                       <%-- **ksahu5-ML-MITS34183 End **--%> 
                                                       <%--  **ksahu5-ML-MITS34183 Start **--%> 
                                                      <%--  <input type="text" value="" size="28" onblur="dateLostFocus(this.id);"
                                                            runat="server" id="txtTransferToDate" />
                                                        <input type="button" class="button" value="..." name="btnTransferToDate" />

                                                        <script type="text/javascript">
                                                            //Ankit-MITS 31595-change date format to ddmmyyyy from mmddyyyy
                                                            // akaushik5 MITS 31595 Reverted back the changes
                                                            Zapatec.Calendar.setup(
																		{
																		    inputField: "txtTransferToDate",
																		    ifFormat: "%m/%d/%Y",
																		    button: "btnTransferToDate",
																		    position: [460, 247]
																		}
																		);
															//Ankit-End Changes        
                                                        </script>--%>

                                                        <input type="text" value="" size="28" onblur="dateLostFocus(this.id);"
                                                            runat="server" id="txtTransferToDate" />

                                                            <script type="text/javascript">
                                                                $(function () {
                                                                    $("#txtTransferToDate").datepicker({
                                                                        showOn: "button",
                                                                        buttonImage: "../../../Images/calendar.gif",
                                                                        //buttonImageOnly: true,
                                                                        showOtherMonths: true,
                                                                        selectOtherMonths: true,
                                                                        changeYear: true
                                                                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                                                });
                                                            </script>
                                                      <%--   **ksahu5-ML-MITS34183 End ** --%>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table width="50%">
                                    <tr>
                                        <td align="center">
                                        <%--    <asp:Button ID="btnGoTransfer" runat="server" OnClientClick="return TransferDiary();"
                                                Text=" Go " CssClass="button" OnClick="btnGoTransfer_Click" />--%>

                                          <%-- **ksahu5-ML-MITS34183 Start ** --%>
                                         <asp:Button ID="btnGoTransfer" runat="server" OnClientClick="return TransferDiary();"
                                         Text="<%$ Resources:btnGoTransfer %>" CssClass="button" OnClick="btnGoTransfer_Click" />
                                   <%--       **ksahu5-ML-MITS34183 End ** --%>

                                        </td>
                                    </tr>
                                </table>
                                <%--This button is commented by Nitin for Mits 20602 because now in R6 opening--%>
                                <%--and closing of winddow is managed by MDI tree so no need of Cancel button --%>
                                <%--<asp:Button ID="btnCancel" runat="server" OnClientClick="Close();" Text="Cancel" CssClass="button" />--%>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellspacing="0" celpadding="0" width="100%" name="FORMTABPurgeDiary"
                        style="display: none;" id="FORMTABPurgeDiary">
                        <tr>
                            <td>
                                <table border="0" cellspacing="0" celpadding="0" width="80%">
                                    <tr>
                                        <td>
                                            <table width="50%" align="left">
                                                <tr>
                                                    <td width="100%" align="left">
                                                   <%--  **ksahu5-ML-MITS34183 Start ** --%>
                                                        <%--Diary Owner :--%>
                                                       <asp:Label ID="lblDiaryOwner" runat="server" Text="<%$ Resources:lblDiaryOwner %>"></asp:Label>
                                                  <%--   **ksahu5-ML-MITS34183 End ** --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:ListBox ID="lstUsers" runat="server" SelectionMode="Multiple" Width="150px">
                                                        </asp:ListBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="80%" align="left">
                                                <tr>
                                                    <td>
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td class="ctrlgroup" colspan="2">
                                                                    <%-- **ksahu5-ML-MITS34183 Start ** --%>
                                                                  <%--  Diary Status--%>
                                                                   <asp:Label ID="lblPDDiaryStatus" runat="server" Text="<%$ Resources:lblPDDiaryStatus %>"></asp:Label>
                                                                <%--    **ksahu5-ML-MITS34183 End ** --%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                              <%--   **ksahu5-ML-MITS34183 Start ** --%>
                                                                    <%--Open Diary :--%>
                                                                     <asp:Label ID="lblPDOpenDiary" runat="server" Text="<%$ Resources:lblPDOpenDiary %>"></asp:Label>
                                                                   <%--  **ksahu5-ML-MITS34183 End **--%> 
                                                                    <input type="checkbox" value="True" id="chkPurgeOpen" runat="server" onchange="ApplyBool(this);" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                 <%-- **ksahu5-ML-MITS34183 Start **--%> 
                                                                  <%--  Close Diary :--%>
                                                                   <asp:Label ID="lblPDCloseDiary" runat="server" Text="<%$ Resources:lblPDCloseDiary %>"></asp:Label>

                                                                <%--   **ksahu5-ML-MITS34183 End ** --%>
                                                                    <input type="checkbox" value="True" id="chkPurgeClose" runat="server" onchange="ApplyBool(this);" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="ctrlgroup" colspan="2">
                                                            <%--    **ksahu5-ML-MITS34183 Start ** --%>
                                                                   <%-- Due Date Range--%>
                                                                    <asp:Label ID="lblPDDueDateRange" runat="server" Text="<%$ Resources:lblPDDueDateRange %>"></asp:Label>
                                                              <%--  **ksahu5-ML-MITS34183 End **   --%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                               <%--   **ksahu5-ML-MITS34183 Start ** --%>
                                                                    <%-- From :--%>
                                                                    <asp:Label ID="lblPDFrom" runat="server" Text="<%$ Resources:lblPDFrom %>"></asp:Label>
                                                                       <%--**ksahu5-ML-MITS34183 End ** --%>
                                                                       <%--**ksahu5-ML-MITS34183 Start ** --%>
                                                                    <%--<input type="text" value="" size="30" runat="server" onblur="dateLostFocus(this.id);"
                                                                        id="txtPurgeFromDate" /><input type="button" class="button" value="..." name="btnPurgeFromDate">

                                                                    <script type="text/javascript">
                                                                        Zapatec.Calendar.setup(
																		                    {
																		                        inputField: "txtPurgeFromDate",
																		                        ifFormat: "%m/%d/%Y",
																		                        button: "btnPurgeFromDate",
																		                        position: [460, 220]
																		                    }
																		                    );
                                                                    </script>
--%>

                                                                 <input type="text" value="" size="30" runat="server" onblur="dateLostFocus(this.id);" id="txtPurgeFromDate" />
                                                                  <script type="text/javascript">
                                                                      $(function () {
                                                                          $("#txtPurgeFromDate").datepicker({
                                                                              showOn: "button",
                                                                              buttonImage: "../../../Images/calendar.gif",
                                                                              //buttonImageOnly: true,
                                                                              showOtherMonths: true,
                                                                              selectOtherMonths: true,
                                                                              changeYear: true
                                                                          }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                                                      });
                                                            </script>
                                                        <%-- **ksahu5-ML-MITS34183 End ** --%>
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                 <%--**ksahu5-ML-MITS34183 Start ** --%>
                                                                   <%-- To :--%>
                                                                    <asp:Label ID="lblPDTo" runat="server" Text="<%$ Resources:lblPDTo %>"></asp:Label>
                                                                <%-- **ksahu5-ML-MITS34183 End ** --%>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                    <%-- **ksahu5-ML-MITS34183 Start **--%> 
                                                                   <%-- <input type="text" value="" size="30" onblur="dateLostFocus(this.id);" runat="server"
                                                                        id="txtPurgeToDate" /><input type="button" class="button" value="..." name="btnPurgeToDate" />

                                                                    <script type="text/javascript">
                                                                        Zapatec.Calendar.setup(
																		                    {
																		                        inputField: "txtPurgeToDate",
																		                        ifFormat: "%m/%d/%Y",
																		                        button: "btnPurgeToDate",
																		                        position: [460, 247]
																		                    }
																		                    );
                                                                    </script>--%>
                                                                    <input type="text" value="" size="30" onblur="dateLostFocus(this.id);" runat="server"
                                                                        id="txtPurgeToDate" />
                                                                     <script type="text/javascript">
                                                                         $(function () {
                                                                             $("#txtPurgeToDate").datepicker({
                                                                                 showOn: "button",
                                                                                 buttonImage: "../../../Images/calendar.gif",
                                                                                // buttonImageOnly: true,
                                                                                 showOtherMonths: true,
                                                                                 selectOtherMonths: true,
                                                                                 changeYear: true
                                                                             }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                                                         });
                                                                  </script>
                                                                  <%-- **ksahu5-ML-MITS34183 End **--%> 

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table width="50%">
                                    <tr>
                                        <td align="center">
                                        <%-- **ksahu5-ML-MITS34183 Start ** --%>
                                          <%--  <asp:Button ID="btnGoPurge" Text=" Go " runat="server" CssClass="button" OnClientClick="return PurgeDiary();"
                                                OnClick="btnGoPurge_Click" />--%>
                                            <asp:Button ID="btnGoPurge" Text="<%$ Resources:btnGoPurge %>" runat="server" CssClass="button" OnClientClick="return PurgeDiary();"
                                                OnClick="btnGoPurge_Click" />
                                          <%-- **ksahu5-ML-MITS34183 End ** --%>
                                        </td>
                                    </tr>
                                </table>
                                <%--This button is commented by Nitin for Mits 20602 because now in R6 opening--%>
                                <%--and closing of winddow is managed by MDI tree so no use of Cancel button --%>
                                <%--<asp:Button ID="btnCancelPurge" Text="Cancel" runat="server" CssClass="button" OnClientClick="return PurgeDiary();" />--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript">
        startUp();
        selectSelectedTab();
    </script>

    </form>
</body>
</html>
