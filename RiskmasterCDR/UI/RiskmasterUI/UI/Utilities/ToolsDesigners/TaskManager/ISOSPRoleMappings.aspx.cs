﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
//using Riskmaster.UI.DataIntegratorService;
using System.Data;
using Riskmaster.Models;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class ISOSPRoleMappingGrid : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();
        private bool bLoadOnEdit { get; set; }
        private String[] arrtmp = { };



        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                mode.Text = AppHelper.GetQueryStringValue("mode");
               
                if (!IsPostBack)
                {
                    if (mode.Text == "edit")
                    {
                        bLoadOnEdit = true;
                    }
                    else
                    {
                        bLoadOnEdit = false;
                    }

                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    
                    if (mode.Text == "add" || bLoadOnEdit)
                    {
                        string strSelectedRowId = AppHelper.GetQueryStringValue("selectedid");
                        if (!string.IsNullOrEmpty(strSelectedRowId))
                        {
                            arrtmp = strSelectedRowId.Split('|');                            
                        }
                        GetSPRoleTypes();
                    }
                   
                }

               
            }
            catch (Exception ee)
            {

                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void GetSPRoleTypes()
        {
            DataSet ds = new DataSet();
            //DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
            DataIntegratorModel objDIModel = new DataIntegratorModel();
            try
            {
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }
                //var strXML = objDIService.GetrmASPRoleAndISOSPRoleServiceFunc(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                var strXML = AppHelper.GetResponse<string>("RMService/DAIntegration/getrmasproleandisosproleservicefunc", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);

                System.IO.StringReader strrdr = new System.IO.StringReader(strXML);
                ds.ReadXml(strrdr);

                //bind 1st dropdownlist

                ddlrmASPRoleType.DataSource = ds.Tables[0];
                ddlrmASPRoleType.DataTextField = "SHORT_CODE";
                ddlrmASPRoleType.DataValueField = "CODE_ID";
                ddlrmASPRoleType.DataBind();
                ddlrmASPRoleType.Items.Insert(0, "--Select--");

                //bind 2nd dropdownlist

                ddlISOSPRoleType.DataSource = ds.Tables[1];
                ddlISOSPRoleType.DataTextField = "SHORT_CODE";
                ddlISOSPRoleType.DataValueField = "CODE_ID";
                ddlISOSPRoleType.DataBind();
                ddlISOSPRoleType.Items.Insert(0, "--Select--");

                if (bLoadOnEdit)
                {
                    ddlrmASPRoleType.Enabled = false;
                    ddlrmASPRoleType.SelectedValue = arrtmp[1];
                    ddlISOSPRoleType.SelectedValue = arrtmp[2];
                    txtISORowID.Text = Convert.ToString(arrtmp[0]);
                    return;
                }
            }
            catch (Exception ex)
            {
                //no need to handle
            }
            finally
            {
                //objDIService.Close();
                if (objDIModel != null)
                    objDIModel = null;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            XmlTemplate = GetMessageTemplate();
            string sMsgStatus = "";

            if (ddlrmASPRoleType.SelectedIndex > 0 && ddlISOSPRoleType.SelectedIndex > 0)
            {
                CallCWS("DataIntegratorAdaptor.ISOSaveSPRoleMapping", XmlTemplate, out sCWSresponse, false, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
            }


            if (sMsgStatus == "Success")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            }

        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ISOSPRoleMappings>");

            sXml = sXml.Append("<control name='rmA Service Proivder Type' type='int'>");
            if (ddlrmASPRoleType.SelectedIndex > 0)
            {
                sXml = sXml.Append(ddlrmASPRoleType.SelectedValue);
            }
            else
            {
                sXml = sXml.Append(0);
            }

            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='ISO Service Proivder Type' type='int'>");
            if (ddlISOSPRoleType.SelectedIndex > 0)
            {
                sXml = sXml.Append(ddlISOSPRoleType.SelectedValue);
            }
            else
            {
                sXml = sXml.Append(0);
            }
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='ISO_ROW_ID' type='int'>");
            if (!(String.IsNullOrEmpty(txtISORowID.Text)))
            {
                sXml = sXml.Append(Convert.ToInt32(txtISORowID.Text));
            }
            else
            {
                sXml = sXml.Append(0);
            }
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("</ISOSPRoleMappings></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
    }
}