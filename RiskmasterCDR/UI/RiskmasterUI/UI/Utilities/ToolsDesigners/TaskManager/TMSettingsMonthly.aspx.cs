﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-4042         | ajohari2   | Underwriters - EFT Payments Part 2
 * 05/19/2015 | RMA-4606         | nshah28   | Import third party Currency Exchange Rates from flat file
 **********************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class TMSettingsMonthly : NonFDMBasePageCWS
    {
        //Start averma62 MITS 32386
        XElement oMessageElement = null;
        private string sreturnValue = string.Empty;
        //End averma62 MITS 32386
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                XmlDocument objxmldoc = null;
                string sReturn = string.Empty;
                XmlDocument XmlDoc = new XmlDocument();
                DataTable dtSettings = null;
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                    AppHelper.TimeClientScript(sCulture, this);
                }
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("TMSettingsMonthly.aspx"), "TMSettingValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "TMSettingValidations", sValidationResources, true);
                if (!IsPostBack)
                {
                    //lblTaskTypeText.Text = AppHelper.GetQueryStringValue("TaskTypetxt");
                    //lblScheduleTypeText.Text = AppHelper.GetQueryStringValue("ScheduleTypetxt");
                    lblTaskTypeText.Text = HttpUtility.ParseQueryString(Request.RawUrl).Get("TaskTypetxt");
                    lblScheduleTypeText.Text = HttpUtility.ParseQueryString(Request.RawUrl).Get("ScheduleTypetxt");
                    hdTaskType.Text = AppHelper.GetQueryStringValue("TaskType");
                    lblTaskName.Text = AppHelper.GetQueryStringValue("TaskName");
                    hdTaskName.Text = AppHelper.GetQueryStringValue("TaskName");
                    hdIsDataIntegratorTask.Text = AppHelper.GetQueryStringValue("IsDataIntegratorTask");
                    hdSystemModuleName.Text = AppHelper.GetQueryStringValue("SystemModuleName");
                    hdScheduleTypeId.Value = "4";

                    //PJS :for Edit functionality
                    if (AppHelper.GetQueryStringValue("action") == "Edit" && !(hdnaction.Text == "SaveMonthlySettings"))
                    {
                        hdnScheduleId.Text = AppHelper.GetQueryStringValue("ScheduleId");
                        hdnaction.Text = "EditMonthly";
                    }
                    if (hdSystemModuleName.Text == "FASScheduler") GetClaimStatus();  //averma62 MITS 32386
                    //JIRA RMA-4606 nshah28 start
                    if (hdSystemModuleName.Text == "CurrencyExchangeInterface")
                    {
                        XmlTemplate = GetMessageTemplateForDataSource();
                        
                        //CallCWS("SecurityAdaptor.GetXMLData", XmlTemplate, out sReturn,true, false);
                        CallCWS("TaskManagementAdaptor.GetDocPath", XmlTemplate, out sReturn, false, false);
                        XmlDoc.LoadXml(sReturn);
                        if (XmlDoc.SelectSingleNode("//docpath") != null)
                        {

                            txtFilePath.Text = XmlDoc.SelectSingleNode("//docpath").InnerText;
                        }

                    }
                    //RMA-4606 End
                }
                //nsachdeva2 - MITS:26428 - 12/28/2011
                if (hdnDeteleSetting.Value == "true" && ((DataTable)ViewState["PrintSettings"]) != null)
                {
                    string[] ArrIds = null;
                    if (hdnId.Value != "")
                    {
                        ArrIds = hdnId.Value.Split('-');
                    }
                    dtSettings = ((DataTable)ViewState["PrintSettings"]);
                    foreach (DataRow dr in dtSettings.Rows)
                    {
                        if (ArrIds.Length > 0)
                        {
                            if ((dr.ItemArray[1].ToString() == ArrIds[0]) && (dr.ItemArray[3].ToString() == ArrIds[1]))
                            {
                                dtSettings.Rows.Remove(dr);
                                break;
                            }
                        }
                    }
                    GridView1.DataSource = dtSettings;
                    GridView1.DataBind();
                    ViewState["PrintSettings"] = dtSettings;
                    hdnDeteleSetting.Value = "false";
                }
                else if (hdnPrintSettings.Value != "")
                {
                    bool bInSuccess = false;
                    string[] ArrReturnList = hdnPrintSettings.Value.Split(';');
                    string[] ArrEdit = null;
                    if ((DataTable)ViewState["PrintSettings"] != null)
                    {
                        dtSettings = (DataTable)ViewState["PrintSettings"];
                    }
                    else
                    {
                        dtSettings = GetPrintSettingsTable();
                    }
					// npadhy JIRA 6418 we have increased the array index by 1, as we need both Distribution Type Id and Desc
                    if (ArrReturnList.Length == 13)//JIRA:4042 : ajohari2
                    {
                        if (ArrReturnList[10] != "")
                        {
                            ArrEdit = ArrReturnList[10].Split('-');
                            foreach (DataRow dr in dtSettings.Rows)
                            {
                                if (ArrReturnList.Length > 0)
                                {
                                    if ((dr.ItemArray[1].ToString() == ArrEdit[0]) && (dr.ItemArray[3].ToString() == ArrEdit[1]))
                                    {
                                        dtSettings.Rows.Remove(dr);
                                        break;
                                    }
                                }
                            }
                        }
                    }
					// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                    //dtSettings.Rows.Add(ArrReturnList[0], Conversion.CastToType<int>(ArrReturnList[1], out bInSuccess), ArrReturnList[2], Conversion.CastToType<int>(ArrReturnList[3], out bInSuccess), ArrReturnList[9], Conversion.CastToType<int>(ArrReturnList[4], out bInSuccess), ArrReturnList[5], ArrReturnList[6], ArrReturnList[7], ArrReturnList[8]);
                    //dtSettings.Rows.Add(ArrReturnList[0], Conversion.CastToType<int>(ArrReturnList[1], out bInSuccess), ArrReturnList[2], Conversion.CastToType<int>(ArrReturnList[3], out bInSuccess), ArrReturnList[9], Conversion.CastToType<int>(ArrReturnList[4], out bInSuccess), ArrReturnList[5], ArrReturnList[6], ArrReturnList[11], ArrReturnList[7], ArrReturnList[8]);////JIRA:4042 START: ajohari2
                    dtSettings.Rows.Add(ArrReturnList[0], Conversion.CastToType<int>(ArrReturnList[1], out bInSuccess), ArrReturnList[2], Conversion.CastToType<int>(ArrReturnList[3], out bInSuccess), ArrReturnList[9], Conversion.CastToType<int>(ArrReturnList[4], out bInSuccess), ArrReturnList[5], ArrReturnList[6], ArrReturnList[12], Conversion.CastToType<int>(ArrReturnList[11], out bInSuccess), ArrReturnList[7], ArrReturnList[8]);
					// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                    hdnPrintSettings.Value = "";
                    ViewState["PrintSettings"] = dtSettings;
                    GridView1.DataSource = dtSettings;
                    GridView1.DataBind();
                }
                //End - MITS:26428 - 12/28/2011
                else if (hdnaction.Text == "SaveMonthlySettings")
                {
                    //Start averma62 MITS 32386
                    UpdateRelatedLossComponent();
                    UpdateRelatedCTypeComponent();
                    //End averma62 MITS 32386
                    hdTaskName.Text = lblTaskName.Text;
                    bReturnStatus = CallCWS("TaskManagementAdaptor.SaveMonthlySettings", XmlTemplate, out sReturn, true, false);
                    //changed by nadim for BES                 
                    objxmldoc = new XmlDocument();
                    objxmldoc.LoadXml(sReturn);
                    if (objxmldoc.SelectSingleNode("//MsgStatusCd") != null)
                    {
                        if (!(objxmldoc.SelectSingleNode("//MsgStatusCd").InnerText == "Error"))
                        {
                            Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx");
                        }
                    }    //changed by nadim for BES 
                }
                else if (hdnaction.Text == "EditMonthly" && hdnRepeat.Value != "true" )
                {
                    bReturnStatus = CallCWS("TaskManagementAdaptor.EditMonthly", XmlTemplate, out sReturn, true, true);

                    //Start averma62 MITS 32386
                    string sreturnOutPut = sReturn.Replace("\"", "'");
                    XElement oEl = XElement.Parse(sreturnOutPut);
                    string sModuleName = string.Empty; //JIRA RMA-4606 nshah28 
                    if (oEl.XPathSelectElement("./Document/Details/TaskType") != null)
                    {
                        if (oEl.XPathSelectElement("./Document/Details/SystemModuleName").Value == "FASScheduler")
                        {
                            //GetClaimStatus();
                            //if (oEl.XPathSelectElement("//control[@name='FASPassword']") != null)
                            //{
                            txtFASPassword.Attributes.Add("value", oEl.XPathSelectElement("//control[@name='FASPassword']").Value);
                            //}
                        }
                        //JIRA RMA-4606 nshah28 start(adding script to show hide div according to condition)
                        if (oEl.XPathSelectElement("./Document/Details/SystemModuleName") != null)
                        {
                            if (oEl.XPathSelectElement("./Document/Details/SystemModuleName").Value == "CurrencyExchangeInterface")
                            {
                                sModuleName = "CurrencyExchangeInterface";
                                if (oEl.XPathSelectElement("//FTPPassword") != null)
                                {
                                    txtFTPPass.Attributes.Add("value", oEl.XPathSelectElement("//FTPPassword").Value);
                                }

                                if (oEl.XPathSelectElement("./Document/Details/FileSource") != null)
                                {
                                    if (oEl.XPathSelectElement("./Document/Details/FileSource").Value == "1")
                                    {
                                        ClientScript.RegisterStartupScript(this.GetType(), "showhidediv", "javascript:document.getElementById('divFTP').style.display='none'; document.getElementById('tblSharedFilePath').style.display=''", true);

                                    }
                                    if (oEl.XPathSelectElement("./Document/Details/FileSource").Value == "2")
                                    {
                                        ClientScript.RegisterStartupScript(this.GetType(), "showhidediv", "javascript:document.getElementById('tblSharedFilePath').style.display='none'; document.getElementById('divFTP').style.display=''", true);
                                    }
                                }

                            }
                        }
                        //JIRA RMA-4606 nshah28 end
                    }
                    oEl = null;
                    //End averma62 MITS 32386
                    //  if (!string.IsNullOrEmpty(txtArgs.Text)) //commenting this and introducing new condition JIRA RMA-4606 nshah28 
                    if (!string.IsNullOrEmpty(txtArgs.Text) && sModuleName != "CurrencyExchangeInterface")
                    {
                        string[] sTemp = txtArgs.Text.Split();
                        tbNoOfOverDueDays.Text = sTemp[3];
                        chkSysDiary.Checked = Convert.ToBoolean(sTemp[4]);
                        chkEmailNotify.Checked = Convert.ToBoolean(sTemp[5]);
                        chkBoth.Checked = Convert.ToBoolean(sTemp[6]);
                    }
                }
                else if (string.IsNullOrEmpty(hdnaction.Text) && (hdSystemModuleName.Text.Equals("PolicySystemUpdate") || hdSystemModuleName.Text.Equals("ClaimBalancing")))
                {
                    bReturnStatus = CallCWS("TaskManagementAdaptor.GetPolicySystems", XmlTemplate, out sReturn, true, true);

                }
            }
            catch (Exception ee)
            {
               
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        //Start averma62 MITS 32386
        private void UpdateRelatedLossComponent()
        {
            string svalues = string.Empty;
            string[] lsthndValues = txtRelatedComponents.Text.Split(',');
            ListItem lstItem = null;

            try
            {
                foreach (ListItem lstItemvalue in lstRelatedLossComponents.Items)
                {
                    lstAvailableLossComponents.Items.Add(lstItemvalue);

                }
                lstRelatedLossComponents.Items.Clear();
                for (int Icount = 0; Icount < lsthndValues.Length; Icount++)
                {

                    lstItem = lstAvailableLossComponents.Items.FindByValue(lsthndValues[Icount]);
                    if (lstAvailableLossComponents.Items.Contains(lstItem))
                    {
                        lstRelatedLossComponents.Items.Add(lstItem);
                        lstAvailableLossComponents.Items.Remove(lstItem);
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void UpdateRelatedCTypeComponent()
        {
            string svalues = string.Empty;
            string[] lsthndValues = txtRelatedCTypeComponents.Text.Split(',');
            ListItem lstItem = null;

            try
            {
                foreach (ListItem lstItemvalue in lstRelatedCTypeComponents.Items)
                {
                    lstAvailableCTypeComponents.Items.Add(lstItemvalue);

                }
                lstRelatedCTypeComponents.Items.Clear();
                for (int Icount = 0; Icount < lsthndValues.Length; Icount++)
                {

                    lstItem = lstAvailableCTypeComponents.Items.FindByValue(lsthndValues[Icount]);
                    if (lstAvailableCTypeComponents.Items.Contains(lstItem))
                    {
                        lstRelatedCTypeComponents.Items.Add(lstItem);
                        lstAvailableCTypeComponents.Items.Remove(lstItem);
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// GetClaimTypeTemplate
        /// </summary>
        /// <returns></returns>
        private XElement GetClaimStatusTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>FASAdaptor.GetClaimStatus</Function></Call>");
            sXml = sXml.Append("<Document><Details>");
            sXml = sXml.Append("<control name=\"lstAvailableLossComponents\"  >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"lstAvailableCTypeComponents\"  >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FASServer\"  >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FASUserId\"  >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FASPassword\"  >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FASFolder\"  >");
            sXml = sXml.Append("</control>");
            //Asharma326 MITS 32386 Starts
            sXml = sXml.Append("<control name=\"FASSharedLocation\"  >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FASFileLocation\"  >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"RMALocation\"  >");
            sXml = sXml.Append("</control>");
            //Asharma326 MITS 32386 Ends
            sXml = sXml.Append("</Details></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        protected void GetClaimStatus()
        {
            oMessageElement = GetClaimStatusTemplate();
            CallCWS("FASAdaptor.GetClaimStatus", oMessageElement, out sreturnValue, false, true);
            string sreturnOutPut = sreturnValue.Replace("\"", "'");
            XElement oEl = XElement.Parse(sreturnOutPut);
            txtFASPassword.Attributes.Add("value", oEl.XPathSelectElement("//control[@name='FASPassword']").Value);
        }
        //End averma62 MITS 32386

        protected void MoveToOptionset(object sender, EventArgs e)
        {
            string sTransferToPage = string.Empty;

            sTransferToPage = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/" + hdSystemModuleName.Text + ".aspx";

            Server.Transfer(sTransferToPage);
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;
            StringBuilder sbIds = null;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                sbIds = new StringBuilder();
                sbIds.Append(DataBinder.Eval(e.Row.DataItem, "AccountId").ToString());
                sbIds.Append("-");
                sbIds.Append(DataBinder.Eval(e.Row.DataItem, "StockId").ToString());
                sbIds.Append("-");
                sbIds.Append(DataBinder.Eval(e.Row.DataItem, "OrgId").ToString());
                sbIds.Append("-");
                sbIds.Append(DataBinder.Eval(e.Row.DataItem, "Order").ToString());
                sbIds.Append("-");
                sbIds.Append(DataBinder.Eval(e.Row.DataItem, "CombinedPayment").ToString());
                sbIds.Append("-");
                sbIds.Append(DataBinder.Eval(e.Row.DataItem, "AutoPayment").ToString());
                sbIds.Append("-");
                sbIds.Append(DataBinder.Eval(e.Row.DataItem, "RptType").ToString());
                sbIds.Append("-");
                sbIds.Append(DataBinder.Eval(e.Row.DataItem, "Org").ToString());
				// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:4042 START: ajohari2
                //sbIds.Append("-");
                //sbIds.Append(DataBinder.Eval(e.Row.DataItem, "EFTPayment").ToString());
                //JIRA:4042 End: 
                sbIds.Append("-");
                sbIds.Append(DataBinder.Eval(e.Row.DataItem, "DistributionTypeId").ToString()); 
				// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment

                javascriptSelectRow = "SelectGridRow('" + sbIds.ToString() + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }
        /// <summary>
        /// override the base class function to change the xml values
        /// </summary>
        /// <param name="Xelement">xml</param>
        public override void ModifyXml(ref XElement Xelement)
        {
            if (((DataTable)ViewState["PrintSettings"]) != null)
            {
                XmlDocument xmlDoc = new XmlDocument();
                using (XmlReader reader = Xelement.CreateReader())
                {
                    xmlDoc.Load(reader);
                }
                XmlNode objXmlNode = xmlDoc.SelectSingleNode("Message/Document/Details/UserArguments");
                XmlElement xmlEle = (XmlElement)objXmlNode;
                DataTable dt = (DataTable)ViewState["PrintSettings"];
                StringWriter sw = new StringWriter();
                dt.WriteXml(sw, XmlWriteMode.IgnoreSchema);
                string sXMl = string.Empty;
                sXMl = sw.ToString();
                xmlEle.InnerXml = sXMl;
                Xelement = XElement.Parse(xmlDoc.InnerXml);
            }
        }
        public override void ModifyControl(XElement Xelement)
        {
            if (hdnaction.Text == "EditMonthly" && ((DataTable)ViewState["PrintSettings"]) == null )
            {
                XmlDocument xmlDoc = new XmlDocument();

                using (XmlReader reader = Xelement.CreateReader())
                {
                    xmlDoc.Load(reader);
                }

                XmlNode objXmlNode = xmlDoc.SelectSingleNode("//DocumentElement");
                if (objXmlNode != null)
                {
                    xmlDoc.LoadXml(objXmlNode.OuterXml);

                    DataSet dsSettings = ConvertXmlDocToDataSet(xmlDoc);
                    if (dsSettings != null)
                    {
                        DataTable dtSettings = dsSettings.Tables["PrintSettings"];
                        GridView1.DataSource = dtSettings;
                        GridView1.DataBind();
                        ViewState["PrintSettings"] = dtSettings;
                    }
                }
            }
        }
        private DataSet ConvertXmlDocToDataSet(XmlDocument resultDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(resultDoc));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        public DataTable GetPrintSettingsTable()
        {
            DataTable dtPrintSettings = new DataTable();
            dtPrintSettings.Columns.Add("Account", typeof(string));
            dtPrintSettings.Columns.Add("AccountId", typeof(Int32));
            dtPrintSettings.Columns.Add("ChkStock", typeof(string));
            dtPrintSettings.Columns.Add("StockId", typeof(Int32));
            dtPrintSettings.Columns.Add("Org", typeof(string));
            dtPrintSettings.Columns.Add("OrgId", typeof(Int32));
            dtPrintSettings.Columns.Add("Order", typeof(string));
            dtPrintSettings.Columns.Add("CombinedPayment", typeof(Boolean));
			// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //JIRA:4042 START: ajohari2
            //dtPrintSettings.Columns.Add("EFTPayment", typeof(Boolean));
            //JIRA:4042 End: 
            dtPrintSettings.Columns.Add("DistributionType", typeof(string));
            dtPrintSettings.Columns.Add("DistributionTypeId", typeof(Int32));
			// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            dtPrintSettings.Columns.Add("AutoPayment", typeof(Boolean));
            dtPrintSettings.Columns.Add("RptType", typeof(string));
            GridView1.DataSource = dtPrintSettings;
            GridView1.DataBind();
            dtPrintSettings.TableName = "PrintSettings";
            ViewState["PrintSettings"] = dtPrintSettings;
            return dtPrintSettings;
        }

        //JIRA RMA-4606 nshah28 start
        public XElement GetMessageTemplateForDataSource()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization>
                <Call>
                    <Function>TaskManagementAdaptor.GetDocPath</Function>
                </Call>
                <Document>
                    <Details>
					    <docpath />
                        </Details>
                </Document>
            </Message>
            ");
            return oTemplate;

        }
        //RMA-4606 end
    }
}
