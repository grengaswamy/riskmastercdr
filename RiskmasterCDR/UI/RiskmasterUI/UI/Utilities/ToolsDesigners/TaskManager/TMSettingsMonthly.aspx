﻿<%--/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-4042         | ajohari2   | Underwriters - EFT Payments Part 2
 **********************************************************************************************/--%>
<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="TMSettingsMonthly.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.TMSettingsMonthly" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc2" TagName="UserControlGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register Src="../../../Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc1" %>
    <%@ Register Src="../../../Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Schedule a Task</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />




    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/TMSettings.js"></script>
    
    <link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <script type="text/javascript">

        function removeOption(selectName, id) {
            select = document.getElementById(selectName);
            selecttxtObject = document.getElementById('txtRelatedComponents');
            Ids = new Array();
            Names = new Array();

            for (var x = 0; x < select.options.length; x++) {
                if (select.options[x].value != id) {
                    Ids[Ids.length] = select.options[x].value;
                    Names[Names.length] = select.options[x].text;

                }
            }

            select.options.length = Ids.length;
            if (selectName == 'lstRelatedLossComponents') {
                selecttxtObject.value = ""
            }
            for (var x = 0; x < select.options.length; x++) {
                select.options[x].text = Names[x];
                select.options[x].value = Ids[x];
                if (selectName == 'lstRelatedLossComponents') {
                    if (selecttxtObject.value == "") {
                        selecttxtObject.value = Ids[x];
                    }
                    else {
                        selecttxtObject.value = selecttxtObject.value + ',' + Ids[x];
                    }
                }
            }
        }

        function removeOptionCType(selectName, id) {
            select = document.getElementById(selectName);
            selecttxtObject = document.getElementById('txtRelatedCTypeComponents');
            Ids = new Array();
            Names = new Array();

            for (var x = 0; x < select.options.length; x++) {
                if (select.options[x].value != id) {
                    Ids[Ids.length] = select.options[x].value;
                    Names[Names.length] = select.options[x].text;

                }
            }

            select.options.length = Ids.length;
            if (selectName == 'lstRelatedCTypeComponents') {
                selecttxtObject.value = ""
            }
            for (var x = 0; x < select.options.length; x++) {
                select.options[x].text = Names[x];
                select.options[x].value = Ids[x];
                if (selectName == 'lstRelatedCTypeComponents') {
                    if (selecttxtObject.value == "") {
                        selecttxtObject.value = Ids[x];
                    }
                    else {
                        selecttxtObject.value = selecttxtObject.value + ',' + Ids[x];
                    }
                }
            }
        }

        function AddFilter(mode) {
            var optionRank;
            var optionObject;

            selectObject = document.getElementById('lstRelatedLossComponents');
            selecttxtObject = document.getElementById('txtRelatedComponents');

            if (mode == "selected") {

                //Add selected Available Values
                select = document.getElementById('lstAvailableLossComponents');
                for (var x = select.options.length - 1; x >= 0 ; x--) {
                    if (select.options[x].selected == true) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);

                        optionRank = selectObject.options.length;
                        selectObject.options[optionRank] = optionObject;
                        if (selecttxtObject.value == "") {
                            selecttxtObject.value = select.options[x].value;
                        }
                        else {
                            selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;
                        }
                        //remove from available list	
                        removeOption('lstAvailableLossComponents', select.options[x].value);
                        setDataChanged(true);
                    }
                }
            }
            else {
                //Add All Available Values
                select = document.getElementById('lstAvailableLossComponents');

                if (select.options.length > 0) {
                    for (var x = select.options.length - 1; x >= 0; x--) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;
                        if (selecttxtObject.value == "") {
                            selecttxtObject.value = select.options[x].value;
                        }
                        else {
                            selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;
                        }
                        selectObject.options[optionRank] = optionObject;
                        setDataChanged(true);
                    }
                    select.options.length = 0;
                }
            }
            return false;
        }


        function AddFilterCType(mode) {
            var optionRank;
            var optionObject;

            selectObject = document.getElementById('lstRelatedCTypeComponents');
            selecttxtObject = document.getElementById('txtRelatedCTypeComponents');

            if (mode == "selected") {

                //Add selected Available Values
                select = document.getElementById('lstAvailableCTypeComponents');
                for (var x = select.options.length - 1; x >= 0 ; x--) {
                    if (select.options[x].selected == true) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);

                        optionRank = selectObject.options.length;
                        selectObject.options[optionRank] = optionObject;
                        if (selecttxtObject.value == "") {
                            selecttxtObject.value = select.options[x].value;
                        }
                        else {
                            selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;
                        }
                        //remove from available list	
                        removeOptionCType('lstAvailableCTypeComponents', select.options[x].value);
                        setDataChanged(true);
                    }
                }
            }
            else {
                //Add All Available Values
                select = document.getElementById('lstAvailableCTypeComponents');

                if (select.options.length > 0) {
                    for (var x = select.options.length - 1; x >= 0; x--) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;
                        if (selecttxtObject.value == "") {
                            selecttxtObject.value = select.options[x].value;
                        }
                        else {
                            selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;
                        }
                        selectObject.options[optionRank] = optionObject;
                        setDataChanged(true);
                    }
                    select.options.length = 0;
                }
            }
            return false;
        }


        function RemoveFilter(mode) {
            var optionRank;
            var optionObject;
            selectObject = document.getElementById('lstAvailableLossComponents');
            selecttxtObject = document.getElementById('txtRelatedComponents');
            if (mode == "selected") {
                //Add selected Available Values
                select = document.getElementById('lstRelatedLossComponents');
                for (var x = select.options.length - 1; x >= 0; x--) {
                    if (select.options[x].selected == true) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;
                        selectObject.options[optionRank] = optionObject;

                        //remove from available list	
                        removeOption('lstRelatedLossComponents', select.options[x].value);
                        setDataChanged(true);
                    }
                }
            }
            else {
                //ADD All Available Values
                select = document.getElementById('lstRelatedLossComponents');

                if (select.options.length > 0) {

                    for (var x = select.options.length - 1; x >= 0; x--) {

                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;


                        selectObject.options[optionRank] = optionObject;

                        selecttxtObject.value = "";

                        setDataChanged(true);


                    }
                    select.options.length = 0;
                }
            }
            return false;
        }

        function RemoveFilterCType(mode) {
            var optionRank;
            var optionObject;
            selectObject = document.getElementById('lstAvailableCTypeComponents');
            selecttxtObject = document.getElementById('txtRelatedCTypeComponents');
            if (mode == "selected") {
                //Add selected Available Values
                select = document.getElementById('lstRelatedCTypeComponents');
                for (var x = select.options.length - 1; x >= 0; x--) {
                    if (select.options[x].selected == true) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;
                        selectObject.options[optionRank] = optionObject;

                        //remove from available list	
                        removeOptionCType('lstRelatedCTypeComponents', select.options[x].value);
                        setDataChanged(true);
                    }
                }
            }
            else {
                //ADD All Available Values
                select = document.getElementById('lstRelatedCTypeComponents');

                if (select.options.length > 0) {

                    for (var x = select.options.length - 1; x >= 0; x--) {

                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;


                        selectObject.options[optionRank] = optionObject;

                        selecttxtObject.value = "";

                        setDataChanged(true);


                    }
                    select.options.length = 0;
                }
            }
            return false;
        }
        //asharma326 MITS 32386 FAS starts
        function ShowHidefFASdiv() {
            if (document.getElementById('txtFASFileLocation') != null) {
                var SelectedOption = document.getElementById('txtFASFileLocation').value;
                if ((SelectedOption != null) && (SelectedOption != "")) {
                    if (SelectedOption == "N") {
                        document.getElementById('tbodyFASNonediv').style.display = 'block';
                    }
                    else if (SelectedOption == "F") {
                        document.getElementById('tbodyFASSettings').style.display = 'block';
                    }
                    else if (SelectedOption == "S") {
                        document.getElementById('tbodyFASSharedDiv').style.display = 'block';
                    }
                }
            }
        }
        //asharma326 MITS 32386 FAS Ends
	</script>   

    <style type="text/css">
        .auto-style3 {
            width: 199px;
        }
        .auto-style4 {
            width: 83px;
        }
        .auto-style5 {
            width: 85px;
        }
        .auto-style6 {
            width: 276px;
        }
    </style>
</head>
<body onload="onTMLoaded();ShowHidefFASdiv();">
    <form id="frmData" runat="server">
    <!--nsachdeva2 - MITS:26428 - 12/28/2011 -->
    <input type="hidden" id="hdnId" runat="server" />
    <input type="hidden" id="hdnPrintSettings" runat="server" />
    <input type="hidden" id="hdnDeteleSetting" runat="server" />
    <input type="hidden" id="hdnRepeat" runat="server" />
    <!--End - MITS:26428 - 12/28/2011 -->
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="msgheader" colspan="4">
          
                <asp:Label ID="lblScheduleTask" runat="server" Text="<%$ Resources:lblScheduleTask %>"></asp:Label>
            </td>
        </tr>
        <tr height="10">
                <td></td>
        </tr>
        <tr height="10">
                <td></td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <!-- Shruti starts-->
        <tr>
            <td align="left" widhth="20%">
                           <asp:Label ID="lblTaskType" runat="server" Text="<%$ Resources:lblTaskType %>"></asp:Label>
            </td>
            <td width="80%" align="left">
                <b>
                    <asp:Label ID="lblTaskTypeText" runat="server" rmxref="/Instance/Document/Details/TaskTypeText" />
                </b>
            </td>
        </tr>
        <tr height="10">
                <td></td>
        </tr>

        <tr>
            <td align="left" width="20%">
               <asp:Label ID="lblTaskNameHeader" runat="server" Text="<%$ Resources:lblTaskNameHeader %>"></asp:Label>
            </td>
            <td width="80%" align="left">
                <b>
                    <asp:Label ID="lblTaskName" runat="server" rmxref="/Instance/Document/Details/TaskNameLabel"/>
                </b>
            </td>
        </tr>
        
        <tr height="10">
                <td></td>
        </tr>


        <%if (hdnaction.Text == "EditMonthly")
          { %>
        <tr>
            <td align="left">
                <asp:Label ID="lblSdlTypeHeader" runat="server" Text="<%$ Resources:lblSdlTypeHeader %>"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ScheduleType" runat="server" rmxref="/Instance/Document/Details/ScheduleTypeId" ItemSetRef="/Instance/Document/Details/ScheduleType" onchange="return ScheduleTypeChanged();"></asp:DropDownList>           
            </td>
        </tr>
        <tr height="20">
                <td></td>
        </tr>
        <%}
          else
          { %>
        
        <tr>
            <td align="left">
               <asp:Label ID="lblSchedulelTypeHdr" runat="server" Text="<%$ Resources:lblSdlTypeHeader %>"></asp:Label>
            </td>
            <td align="left">
                <b>
                    <asp:Label ID="lblScheduleTypeText" runat="server" rmxref="/Instance/Document/Details/ScheduleTypeText" />
                </b>
            </td>
        </tr>
        <tr height="20">
                <td></td>
        </tr>
        <%} %>
        <tr width="100%" height="20">
            <td class="ctrlgroup2" colspan="10">
                &#160;           
                <asp:Label ID="lblStartMonthDtlHdr" runat="server" Text="<%$ Resources:lblStartMonthDtlHdr %>"></asp:Label>
            </td>
        </tr>
        <tr height="20">
                <td></td>
        </tr>
        <tr>
            <td align="left">
				<asp:Label ID="lblMonthHdr" Font-Underline="True" runat="server" Text="<%$ Resources:lblMonthHdr %>"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="Month" runat="server" rmxref="/Instance/Document/Details/Month" >
                    <asp:ListItem Text="<%$ Resources:liJanuary %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liFebruary %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liMarch %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liApril %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liMay %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liJune %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liJuly %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liAugust %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liSeptember %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liOctober %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liNovember %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liDecember %>" Value="12"></asp:ListItem>
                    <%-- --**ksahu5-ML-MITS33898** End--%>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left">
                    <asp:Label ID="lblDayOfMonthHdr" Font-Underline="True" runat="server" Text="<%$ Resources:lblDayOfMonthHdr %>"></asp:Label>               
            </td>
            <td>
                <asp:TextBox ID="DayOfMonth" rmxref="/Instance/Document/Details/DayOfMonth" onblur="numLostFocus(this);"
                    onchange="AllowIntOnly(this);" size="12" runat="server" />
            </td>
                <td>&#160;&#160;
            </td>
        </tr>
        <tr>
            <td align="left">
           
                 <asp:Label ID="lblTime" runat="server" Font-Underline="True" Text="<%$ Resources:lblTime %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" size="12" formatas="time" onchange="setDataChanged(true);"
                    ID="txtTime" rmxref="/Instance/Document/Details/Time" rmxtype="time" onblur="timeLostFocus(this.id);" />
            </td>
                <td>&#160;&#160;
            </td>
        </tr>
        <tr height="10">
                <td></td>
        </tr>
        <tr>
            <td align="left" id="tdAdditionalParams">
            
              <asp:Label ID="lblAdditionalParameters" runat="server" Text="<%$ Resources:lblAdditionalParameters %>"></asp:Label>
            </td>
            <td>
                <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkAdd_Param" rmxref="/Instance/Document/Details/bParams"
                    rmxtype="checkbox" onclick="AddParams(this);" />
            </td>
        </tr>
        <tr height="10">
                <td></td>
        </tr>
        <tr>
            <td align="left" id="tdArgLabel">
                    <%--kverma6- MITS 33898 Start --%>
                    <%--Arguments:--%>
                    <asp:Label ID="lblArgLabel" runat="server" Text="<%$ Resources:lblArgLabel %>"></asp:Label>
                    <%--kverma6- MITS 33898 End --%>
            </td>
            <td>
                <asp:TextBox ID="txtArgs" runat="server" rmxref="/Instance/Document/Details/Arguments" size="50" />
            </td>
                <td>&#160;&#160;
            </td>
        </tr>
        <%if (hdTaskType.Text == "2")
          { %>
        <tr width="100%" height="20">
            <td class="ctrlgroup2" colspan="10">
                &#160;
                <asp:Label ID="lblFinancialHistoryHdr" runat="server" Text="<%$ Resources:lblFinancialHistoryHdr %>"></asp:Label>
            </td>
        </tr>
        <tr height="20">
                <td></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkZBFinHist_Param"
                    rmxref="/Instance/Document/Details/ZeroBasedFinHist" rmxtype="checkbox" onclick="CheckFinHistParams();"
                    Text="<%$ Resources:chkZeroBaseFinHist %>" />
            </td>
                <td></td>
        </tr>
        <tr height="10">
                <td></td>
        </tr>
        <tr id="trZeroBasedOptions">
            <td>
                <asp:RadioButton ID="optClaimBasedFinHist" runat="server" onclick="SelectClaimBasedFinHist();"
                    rmxref="/Instance/Document/Details/ClaimBasedFinHist" Text="<%$ Resources:rbtnClaimBasedFinHist%>" value="1" />
            </td>
            <td>
                <asp:RadioButton ID="optEventBasedFinHist" runat="server" onclick="SelectEventBasedFinHist();"
                    rmxref="/Instance/Document/Details/EventBasedFinHist" Text="<%$ Resources:rbtnEventBasedFinHist%>" value="2" />
            </td>
        </tr>
        <tr height="10">
                <td></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkRecFinHist_Param"
                    rmxref="/Instance/Document/Details/RecreateFinHist" rmxtype="checkbox" Text="<%$ Resources:chkRecFinHist%>" />
            </td>
                <td></td>
        </tr>
        <tr height="10">
                <td></td>
        </tr>
        <%--akaushik5 Commented for MITS 33420 Strats--%>
        <%--<tr>
            <td colspan="2">
                <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkCreateLog_Param"
                    rmxref="/Instance/Document/Details/CreateLog" rmxtype="checkbox" Text="Generate Log in user's temp directory" />
            </td>
            <td>
            </td>
        </tr>--%>
        <%--akaushik5 Commented for MITS 33420 Ends--%>
        <%} %>
        <%if (hdTaskType.Text == "3")
          { %>
        <tr width="100%" height="20">
            <td class="ctrlgroup2" colspan="10">
                &#160; 
               <asp:Label ID="lblBillingSchedule" runat="server" Text="<%$ Resources:lblBillingSchedule%>"></asp:Label>
            </td>
        </tr>
        <tr height="20">
                <td></td>
        </tr>
        <tr>
            <td align="left">
            
                <asp:Label ID="lblGenerate" runat="server" Font-Underline="True" Text="<%$ Resources:lblGenerate%>"></asp:Label> 
            </td>
            <td colspan="2">
                <asp:DropDownList ID="BillOption" runat="server" rmxref="/Instance/Document/Details/BillingOption">
                    <asp:ListItem Text="<%$ Resources:liInstallment %>" Value="Installment"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liInvoice %>" Value="Invoice"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:liNotice %>" Value="Notice"></asp:ListItem>
                </asp:DropDownList>
            </td>
                <td></td>
        </tr>
        <%} %>
        
                      <%if (hdSystemModuleName.Text == "ProcessOverDueDiary")
                  { %>
        <tr width="100%" height="20">
            <td class="ctrlgroup2" colspan="10">
             
               <asp:Label runat="server" ID="lblOverDueDiaryHdr" Text="<%$ Resources:lblOverDueDiaryHdr %>"/>
            </td>
        </tr>
     
        <tr>
            <td align="left">
           
               <asp:Label runat="server" ID="lblNoOfDaysOverdue" Text= "<%$ Resources:lblNoOfDaysOverdue %>" />
                <br />
              
                <asp:Label runat="server" ID="lblDiaryNotification" Text="<%$ Resources:lblDiaryNotification %>" />
                <br />
             
                <asp:Label runat="server" ID="lblSetDueDate" Text="<%$ Resources:lblSetDueDate %>" />
            </td>
            <td>
            <asp:TextBox ID="tbNoOfOverDueDays" runat="server" rmxref="/Instance/Document/Details/OverDueDays"></asp:TextBox>
            </td>
            </tr>
            <tr>
            <td>
           
             <asp:Label runat="server" ID="lblOverDueNote" Text="<%$ Resources:lblDiaryOverDueNote %>" />
            <br />
           
            <asp:Label runat="server" ID="lblSentAs" Text="<%$ Resources:lblSentAs %>" />
            </td>
            
           
            <td colspan="2">
               
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkSysDiary"
                    rmxref="/Instance/Document/Details/CreateSysDiary" rmxtype="checkbox"  Text="<%$ Resources:chkCreateSysDiary %>" onclick="onCheckChanged(this)" />

            <br />
            
                            <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkEmailNotify"
                    rmxref="/Instance/Document/Details/SendEmailNotify" rmxtype="checkbox" Text="<%$ Resources:chkEmailNotify %>"  onclick="onCheckChanged(this)"/>
            <br />
            
                            <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkBoth"
                    rmxref="/Instance/Document/Details/BothDiaryAndEmail" rmxtype="checkbox" Text="<%$ Resources:chkBothDiaryAndEmail %>" onclick="onCheckChanged(this)"/>
            <br />



            
         
            </td>
                <td></td>
        </tr>
        <%} %>
             <%if (hdSystemModuleName.Text == "PolicySystemUpdate")
                  { %>
        <tr width="100%" height="20">
            <td class="ctrlgroup2" colspan="10">
                &#160; 
                <asp:Label runat="server" ID="lblPolicySysFinHdr" Text="<%$ Resources:lblPolicySysFinHdr %>" />
                   <%-- --**ksahu5-ML-MITS33898** End--%>
            </td>
        </tr>
     
        <tr>
            <td align="left">
             
                <asp:Label runat="server" ID="lblSelectPolicy" Text="<%$ Resources:lblSelectPolicy %>" />
            </td>
            <td>
           <asp:DropDownList ID="PolicySystems" runat="server" rmxref="/Instance/Document/Details/PolicySystemList/PolicySystems" ItemSetRef="/Instance/Document/Details/PolicySystemList/PolicySystems"></asp:DropDownList>
            </td>
            </tr>
            <tr>
                <td align="left">
                  
                    <u> <asp:Label runat="server" ID="lblPolDate" Text="<%$ Resources:lblPolDate %>" /> </u>
                    <br />
                   
                    <asp:Label runat="server" ID="lblPolRecUpload" Text="<%$ Resources:lblPolRecUpload %>" />
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="dtActivity" RMXRef="/Instance/Document/Details/dtActivity"
                    RMXType="date" onblur="dateLostFocus(this.id);"/>
                   

                    <script type="text/javascript">
                         $(function () {
                             $("#dtActivity").datepicker({
                                 showOn: "button",
                                 buttonImage: "../../../../Images/calendar.gif",
                                 //buttonImageOnly: true,
                                 showOtherMonths: true,
                                 selectOtherMonths: true,
                                 changeYear: true
                             }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                         });
                    </script>
                </td>
            </tr>

        <%} %>

        
           <%if (hdSystemModuleName.Text == "ClaimBalancing")
                  { %>
        <tr width="100%" height="20">
            <td class="ctrlgroup2" colspan="10">
                &#160;
                <asp:Label runat="server" ID="lblClmParam" Text="<%$ Resources:lblClmParam %>" />
            </td>
        </tr>
     
        <tr>
            <td align="left">
               
                <asp:Label runat="server" ID="lblSelectPolicy1" Text="<%$ Resources:lblSelectPolicy%>" />
            </td>
            <td>
           <asp:DropDownList ID="PolicySystemsClaimsBalancing" runat="server" rmxref="/Instance/Document/Details/PolicySystemList/PolicySystemsClaimsBalancing" ItemSetRef="/Instance/Document/Details/PolicySystemList/PolicySystems"></asp:DropDownList>
            </td>
            </tr>

            <tr>
                <td align="left">
                    
                    <asp:Label runat="server" ID="lblDtClm" Text="<%$ Resources:lblDtClm %>" />
                    <br />
                    
                    <asp:Label runat="server" ID="lblClmBal" Text="<%$ Resources:lblClmBal %>" />
                    <br />
                  
                   <asp:Label runat="server" ID="lblClmBal1" Text="<%$ Resources:lblClmBal1 %>" />
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="txtClmBalanceDate" RMXRef="/Instance/Document/Details/ClmBalanceDate"
                    RMXType="date" onblur="dateLostFocus(this.id);"/>
                   

                    <script type="text/javascript">
                        $(function () {
                            $("#txtClmBalanceDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../../../Images/calendar.gif",
                                //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                        });
                    </script>
                </td>
            </tr>

            <tr>
                <td align="left">
               
                <asp:Label runat="server" ID="lblSelClmType" Text="<%$ Resources:lblSelClmType %>" />
                </td>
                <td>
                    <uc5:CodeLookUp runat="server" CodeTable="CLAIM_TYPE" ControlName="ClaimBalancingClaimType" ID="ClaimBalancingClaimType"   RMXref="/Instance/Document/Details/ClaimBalance/ClaimType[@name = 'ClaimBalancingClaimType']"/>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="SendExceltoEmail"
                    rmxref="/Instance/Document/Details/SendEmail" rmxtype="checkbox" Text="<%$ Resources:chkSendExceltoEmail %>"  onclick="onCheckChanged(this)"/>
                </td>
            </tr>
        <%} %>

        <%--Ankit Start for Point Balancing --%>
        <%if (hdSystemModuleName.Text == "BES" || hdSystemModuleName.Text == "ClaimBalancing")
          { %>
        <tr width="100%" height="20">
            <td class="ctrlgroup2" colspan="10">
                &#160; 
                <asp:Label runat= "server" ID="lblBESSchedParamHdr" Text="<%$ Resources:lblBESSchedParamHdr %>" />
            </td>
        </tr>
        <tr height="20">
                <td></td>
        </tr>
        <tr>
            <td align="left">
           
                 <asp:Label runat="server" ID="lblAdminUserId" font-underline="true" Text="<%$ Resources:lblAdminUserId %>" />
            </td>
            <td>
                <asp:TextBox runat="server" size="12" onchange="setDataChanged(true);"
                    ID="txtLogin" rmxref="/Instance/Document/Details/AdminLogin" />
            </td>
                <td>&#160;&#160;
            </td>
        </tr>
        <tr>
            <td align="left">
               
                <asp:Label runat="server" ID="lblAdminPwd" font-underline="true" Text="<%$ Resources:lblAdminPwd %>" />
            </td>
            <td>
                <asp:TextBox runat="server" size="12" onchange="setDataChanged(true);"
                    ID="txtPassword" TextMode="Password" rmxref="/Instance/Document/Details/AdminPassword" autocomplete="off"/>
            </td>
                <td>&#160;&#160;
            </td>
        </tr>
        <tr height="10">
                <td></td>
        </tr>
        <%} %>
         <%--JIRA RMA-4606 nshah28 start--%>
            <% if (hdSystemModuleName.Text == "CurrencyExchangeInterface")
               { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160;
                    <asp:label id="lblFileSource" runat="server" text="<%$ Resources:lblFileSource %>"></asp:label>
                    <%--Choose File Source--%>
                </td>
            </tr>
            <tr height="20">
                <td>
                   </td>
            </tr>

            <tr>
                <td align="left" id="tdSharedFilePath">
                    <asp:label id="lblSharedFilePath" runat="server" text="<%$ Resources:lblSharedFilePath %>"></asp:label>
                    <%--Shared File Path--%>
                </td>
                <td>
                    <asp:radiobutton id="rdoSharedFilePath" runat="server" groupname="rdoFileSource" checked="true"  onclick="ShowHideControlsForFileSource(this);" rmxref="/Instance/Document/Details/FileSource" value="1"></asp:radiobutton>
                </td>
            </tr>

            <tr>
                <td align="left" id="tdFTP">
                    <asp:label id="lblFTP" runat="server" text="<%$ Resources:lblFTP %>"></asp:label>
                </td>
                <td>
                    <asp:radiobutton id="rdoFTP" runat="server" groupname="rdoFileSource"  onclick="ShowHideControlsForFileSource(this);" rmxref="/Instance/Document/Details/FileSource" value="2"></asp:radiobutton>
                </td>
            </tr>
        <tr></tr>
            <%--Section for Shared File Path--%>
           <div id="divSharedFilePath">
            <table id="tblSharedFilePath" width="100%">
                <tbody>
                   <tr width="100%" height="20">
                        <td class="ctrlgroup2" colspan="10">&#160;
                    <asp:label id="lblEnterSharedFilePath" runat="server" text="<%$ Resources:lblEnterSharedFilePath %>"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFilePath" runat="server" text="<%$ Resources:lblFilePath %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="50"
                                id="txtFilePath" rmxref="/Instance/Document/Details/FilePath" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFileName" runat="server" text="<%$ Resources:lblFileName %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="20"
                                id="txtFileName" rmxref="/Instance/Document/Details/FileName" />
                        </td>
                    </tr>
                   <tr height="10">
                        <td class="auto-style8" />
                    </tr>
                    </tbody>
               </table>
           </div>
               
            <%--Section for FTP --%>
            <div id="divFTP" style="display: none">
                <table width="100%">
                    <tbody>
                    <tr width="100%" height="20">
                        <td class="ctrlgroup2" colspan="10">&#160;
                    <asp:label id="lblEnterFTP" runat="server" text="<%$ Resources:lblEnterFTP %>"></asp:label>
                        </td>
                    </tr>
                    <tr height="20">
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPServer" runat="server" text="<%$ Resources:lblFTPServer %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="50"
                                id="txtFTPServer" rmxref="/Instance/Document/Details/FTPServerName" />

                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPPort" runat="server" text="<%$ Resources:lblFTPPort %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="20" onchange="setDataChanged(true);"
                                id="txtFTPPort" rmxref="/Instance/Document/Details/FTPPort" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPUserName" runat="server" text="<%$ Resources:lblFTPUserName %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="20" onchange="setDataChanged(true);"
                                id="txtFTPUserName" rmxref="/Instance/Document/Details/FTPUserName" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPPass" runat="server" text="<%$ Resources:lblFTPPass %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="20" onchange="setDataChanged(true);" type="text"
                                id="txtFTPPass" textmode="Password" rmxref="/Instance/Document/Details/FTPPassword" />
                        </td>
                    </tr>

                        <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPFilePath" runat="server" text="<%$ Resources:lblFTPFilePath %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="50"
                                id="txtFTPFilePath" rmxref="/Instance/Document/Details/FTPFilePath" />
                        </td>
                    </tr>

                         <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPFileName" runat="server" text="<%$ Resources:lblFTPFileName %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="20"
                                id="txtFTPFileName" rmxref="/Instance/Document/Details/FTPFileName" />
                        </td>
                    </tr>

                    <tr height="10">
                        <td class="auto-style8" />
                    </tr>
                        </tbody>
                </table>
            </div>
            <br /> <br />
            <% } %>
            <%--JIRA RMA-4606 nshah28 End--%>
         <% if (hdSystemModuleName.Text == "PrintBatchFroi")
          { %>
          <tr width="100%" height="20">
            <td class="ctrlgroup2" colspan="10">
                &#160;
                <asp:Label runat="server" ID="lblFROIAcord" Text="<%$ Resources:lblFROIAcord %>" />
            </td>
        </tr>
        <tr height="20">
                <td></td>
        </tr>
        <tr>
            <td colspan="2">
             &#160;
          <asp:Label runat="server" ID="lblSelectFROI" Text="<%$ Resources:lblSelectFROI %>" />
               &#160;&#160;
                <asp:CheckBox runat="server"  onchange="ApplyBool(this);" ID="CheckBox1"
                    rmxref="/Instance/Document/Details/IsFroiBatch" rmxtype="checkbox"  />
            </td>
                <td></td>
        </tr>
         <tr>
            <td colspan="2">
            &#160; 
             <asp:Label runat="server" ID="lblSelectACORD" Text= "<%$ Resources:lblSelectACORD %>"/>
                <asp:CheckBox runat="server"  onchange="ApplyBool(this);" ID="CheckBox2"
                    rmxref="/Instance/Document/Details/IsAcordBatch" rmxtype="checkbox" />
            </td>
                <td></td>
        </tr>
        
        <%} %>
        <!--nsachdeva2 - MITS:26428 - 12/28/2011 -->
        <% if (hdSystemModuleName.Text == "PrintCheckBatch")
          { %>
          <tr width="100%" height="20">
            <td class="ctrlgroup2" colspan="10">
                &#160;
                  <asp:Label runat="server" ID="lblPrintCheck" Text="<%$ Resources:lblPrintCheck %>" />
            </td>
        </tr>
        <tr height="20">
                <td></td>
        </tr>
        <tr>
            <td colspan="2">
                    <div id="divGrid" style="overflow: auto; height: 470px">
		           <asp:GridView ID="GridView1" runat="server" AllowPaging="false" Width="100%"  AutoGenerateColumns="false" OnRowDataBound="GridView1_RowDataBound"  
                            GridLines="None"   >
		              <HeaderStyle CssClass="msgheader" />
		              <AlternatingRowStyle CssClass="data2" />
		              <Columns>
		                  <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                      <input type="radio" id="selectrdo" name="ChkBatchPrintOTGrid"  />
                                    </ItemTemplate> 
                          </asp:TemplateField> 
                         <asp:TemplateField HeaderText="<%$ Resources:gvHdrBankAccount %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblAccount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Account")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrChkStock %>"  HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblChkStock" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChkStock")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrOrgHier %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblOrg" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Org")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrOrderField %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblOrder" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Order")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrCombinedPayment %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblCombinedPay" runat="server" Text='<%# (Boolean.Parse(DataBinder.Eval(Container,"DataItem.CombinedPayment").ToString())) ? "Yes" : "No" %>'  dataformatstring="{0:Yes/No}"></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrAutoPay %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White"  >
                                    <ItemTemplate>
                                      <asp:Label ID="lblAutoPay" runat="server" Text='<%# (Boolean.Parse(DataBinder.Eval(Container,"DataItem.AutoPayment").ToString())) ? "Yes" : "No" %>' dataformatstring="{0:Yes/No}"></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrRptType %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblRptType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RptType")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <%--npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment--%>
                            <%--//JIRA:4042 START: ajohari2--%>
                           <%--<asp:TemplateField HeaderText="<%$ Resources:gvHdrEFTPayment %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblEFTPayment" runat="server" Text='<%# (Boolean.Parse(DataBinder.Eval(Container,"DataItem.EFTPayment").ToString())) ? "Yes" : "No" %>'  dataformatstring="{0:Yes/No}"></asp:Label>
                                     </ItemTemplate> 
                          </asp:TemplateField> --%> 
                          <%--//JIRA:4042 End: --%>

                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrDistributionType %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblDistributionType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DistributionType")%>' ></asp:Label>
                                       <asp:Label ID="lblDistributionTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DistributionTypeId")%>' style="display:none" ></asp:Label>
                                     </ItemTemplate> 
                          </asp:TemplateField> 
                          <%--npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment--%>						  
                      </Columns> 
		           </asp:GridView>
		           </div>

            </td>
            <td width="5%" valign="top" colspan="1">
           
               <asp:ImageButton runat="server" src="../../../../Images/new.gif" alt="" 
                       id="New_SettingsGrid" 
                       onmouseover="javascript:document.all['New_SettingsGrid'].src='../../../../Images/new2.gif'" 
                       onmouseout="javascript:document.all['New_SettingsGrid'].src='../../../../Images/new.gif'" 
                       title="<%$ Resources:ttNew %>" 
                       OnClientClick="return openGridWindowAddEdit('ChkBatchPrintOTGrid','Add');"
                       /><br />
              <asp:ImageButton runat="server" src="../../../../Images/edittoolbar.gif" alt="" 
                       id="Edit_SettingsGrid" 
                       onmouseover="javascript:document.all['Edit_SettingsGrid'].src='../../../../Images/edittoolbar2.gif'" 
                       onmouseout="javascript:document.all['Edit_SettingsGrid'].src='../../../../Images/edittoolbar.gif'" 
                       title="<%$ Resources:ttEdit %>" 
                       OnClientClick="return openGridWindowAddEdit('ChkBatchPrintOTGrid','Edit');"
                       /><br />
		        <asp:ImageButton runat="server" src="../../../../Images/delete.gif" alt="" 
                       id="Delete_SettingsGrid" 
                       onmouseover="javascript:document.all['Delete_SettingsGrid'].src='../../../../Images/delete2.gif'" 
                       onmouseout="javascript:document.all['Delete_SettingsGrid'].src='../../../../Images/delete.gif'" 
                       title="<%$ Resources:ttDelete %>" 
                       OnClientClick="return GridForDeletion('ChkBatchPrintOTGrid');"
                       />
            </td>            
        </tr>
         <%} %>
     <!--End - MITS:26428 - 12/28/2011 -->
        <%--<Start averma62  MITS 32386 FAS Filters>--%>
        <%if (hdSystemModuleName.Text == "FASScheduler")
          { %>
        <div>
            <table width="100%">
                <tr height="20">
                    <td colspan="5" class="ctrlgroup2">&#160; 
                        <asp:Label runat="server" ID="lblFASSchParam" Text="<%$ Resources:lblFASSchParam %>" />
                    </td>
                </tr>
                <tr height="10">
                    <td class="auto-style3" />
                </tr>
                <div id="FASSettings" runat="server">
                    <tbody id="tbodyFASSettings" style="display: none">
                        <tr id="">
                            <td>
                                <asp:Label runat="server" ID="lblFASServer" Text="<%$ Resources:lblFASServer %>" />  
                            </td>
                            <td>
                                <div title="" style="padding: 0px; margin: 0px">
                                    <asp:TextBox ID="txtFASServer" Enabled="false" size="20" value="" onchange="setDataChanged(true);" rmxref="/Instance/Document/Details/control[@name ='FASServer']"
                                        type="text" runat="server"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr id="Tr1">
                            <td>
                                 <asp:Label runat="server" ID="lblFASUserId" Text="<%$ Resources:lblFASUserId %>" />
                            </td>
                            <td>
                                <div title="" style="padding: 0px; margin: 0px">
                                    <asp:TextBox ID="txtFASUserId" Enabled="false" size="20" value="" onchange="setDataChanged(true);" rmxref="/Instance/Document/Details/control[@name ='FASUserId']"
                                        type="text" runat="server"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr id="Tr2">
                            <td>
                                <asp:Label runat="server" ID="lblFASPassword" Text="<%$ Resources:lblFASPassword %>" />
                            </td>
                            <td>
                                <div title="" style="padding: 0px; margin: 0px">
                                    <asp:TextBox ID="txtFASPassword" Enabled="false" size="20" TextMode="Password" EnableViewState="true" type="text" onchange="setDataChanged(true);" value=""
                                        rmxref="/Instance/Document/Details/control[@name ='FASPassword']"
                                        runat="server"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr id="Tr3">
                            <td>
                                <asp:Label runat="server" ID="lblFASFolder" Text="<%$ Resources:lblFASFolder %>" />
                            </td>
                            <td>
                                <div title="" style="padding: 0px; margin: 0px">
                                    <asp:TextBox ID="txtFASFolder" Enabled="false" size="20" type="text" onchange="setDataChanged(true);" value=""
                                        rmxref="/Instance/Document/Details/control[@name ='FASFolder']"
                                        runat="server"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </div>
                <%--asharma326 MITS 32386 FAS starts--%>
                <div id="FASSharedDiv">
                    <tbody id="tbodyFASSharedDiv" style="display: none">
                        <tr id="trSharedLocation">
                            <td>
                                <asp:Label runat="server" ID="lblSharedLocation" Text="<%$ Resources:lblSharedLocation %>" />
                            </td>
                            <td>
                                <div style="padding: 0px; margin: 0px">
                                    <asp:TextBox ID="txtFASSharedLocation" size="20" Enabled="false" type="text"
                                        rmxref="/Instance/Document/Details/control[@name ='FASSharedLocation']"
                                        runat="server"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </div>
                <tr id="trfilelocation" style="display: none">
                    <td>
                        <asp:Label runat="server" ID="lblFileLocation" Text="<%$ Resources:lblFileLocation %>" />
                    </td>
                    <td>
                        <div style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="txtFASFileLocation" size="20" type="text"
                                rmxref="/Instance/Document/Details/control[@name ='FASFileLocation']"
                                runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <div id="FASNonediv">
                    <tbody id="tbodyFASNonediv" style="display: none">
                        <tr id="trNone">
                            <td>
                                <asp:Label runat="server" ID="lblRMALocation" Text="<%$ Resources:lblRMALocation %>" />
                            </td>
                            <td>
                                <div style="padding: 0px; margin: 0px" disabled="false">
                                    <asp:TextBox ID="txtRMALocation" size="20" type="text"
                                        rmxref="/Instance/Document/Details/control[@name ='RMALocation']"
                                        runat="server"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </div>
                <%--asharma326 MITS 32386 FAS Ends--%>
            </table>
        </div>
        <tr>
            <table>
                <tr>
                    <td width="20%" align="left" valign="top" colspan="3">
                        <asp:Label ID="lblCode2" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td width="5%" nowrap="true">
                        <asp:Label runat="server" ID="lblAvlClmStatus" Text="<%$ Resources:lblAvlClmStatus %>" /> 
                    </td>
                    <td class="style1"></td>
                    <td width="*" nowrap="true">
                        <asp:Label runat="server" ID="lblRtdClmStatus" Text="<%$ Resources:lblRtdClmStatus %>" /> 
                    </td>
                </tr>
                <tr>
                    <td width="5%" nowrap="true" valign="top">
                        <asp:ListBox type="combobox"
                            rmxref="/Instance/Document/Details/control[@name='lstAvailableLossComponents']"
                            rmxignoreset="true" runat="server"
                            ID="lstAvailableLossComponents" TabIndex="1"
                            size="10" Height="150px" Style="margin-top: 0px" Width="300px"
                            SelectionMode="Multiple"></asp:ListBox>
                        <asp:HiddenField ID="hndPagetype" Value="N" runat="server" />
                        <asp:TextBox runat="server" rmxref="/Instance/Document/Details/control[@name='txtRelatedComponents']" ID="txtRelatedComponents" Style="display: none"></asp:TextBox>
                    </td>
                    <td valign="top" align="center" class="style1">
                        <asp:Button runat="server" ID="btnAddAll" Text="&gt;&gt;" class="button" TabIndex="2"
                            Style="width: 95" OnClientClick="return AddFilter('all');" Height="26px"
                            Width="50px" UseSubmitBehavior="False" /><br />
                        <br />
                        <asp:Button runat="server" ID="btnAddSelected" Text="&gt;" class="button" TabIndex="2"
                            Style="width: 95" OnClientClick="return AddFilter('selected');" Height="26px"
                            Width="50px" UseSubmitBehavior="False" /><br />
                        <br />
                        <asp:Button runat="server" ID="btnRemoveSelected" Text="&lt;" class="button" TabIndex="4"
                            Style="width: 95; margin-left: 0px;"
                            OnClientClick="return RemoveFilter('selected');" Height="26px"
                            Width="50px" UseSubmitBehavior="False" /><br />
                        <br />
                        <asp:Button runat="server" ID="btnRemoveAll" Text="&lt;&lt;" class="button" TabIndex="4"
                            Style="width: 95; margin-left: 0px;"
                            OnClientClick="return RemoveFilter('all');" Height="26px"
                            Width="50px" UseSubmitBehavior="False" /><br />
                        <br />
                    </td>
                    <td nowrap="true" width="*" valign="top">
                        <asp:ListBox
                            type="combobox" rmxref="/Instance/Document/Details/control[@name='lstRelatedLossComponents']"
                            runat="server" TabIndex="6"
                            ID="lstRelatedLossComponents" rmxignoreset="true"
                            size="10" Height="150px" Style="margin-top: 0px" Width="300px" SelectionMode="Multiple" EnableViewState="true"></asp:ListBox>
                    </td>
                </tr>
            </table>
        </tr>
        <tr height="10">
            <td class="auto-style3" />
        </tr>
        <tr>
            <table>
                <tr>
                    <td width="20%" align="left" valign="top" colspan="3">
                        <asp:Label ID="lblCode3" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td width="5%" nowrap="true">
                        <asp:Label runat="server" ID="lblAvlClmType" Text="<%$ Resources:lblAvlClmType %>" />
                    </td>
                    <td class="style1"></td>
                    <td width="*" nowrap="true">
                         <asp:Label runat="server" ID="lblRldClmType" Text="<%$ Resources:lblRldClmType %>" />
                    </td>
                </tr>
                <tr>
                    <td width="5%" nowrap="true" valign="top">
                        <asp:ListBox type="combobox"
                            rmxref="/Instance/Document/Details/control[@name='lstAvailableCTypeComponents']"
                            rmxignoreset="true" runat="server"
                            ID="lstAvailableCTypeComponents" TabIndex="1"
                            size="10" Height="150px" Style="margin-top: 0px" Width="300px"
                            SelectionMode="Multiple"></asp:ListBox>
                        <asp:HiddenField ID="hndPagetypeCType" Value="N" runat="server" />
                        <asp:TextBox runat="server" rmxref="/Instance/Document/Details/control[@name='txtRelatedCTypeComponents']" ID="txtRelatedCTypeComponents" Style="display: none"></asp:TextBox>
                    </td>
                    <td valign="top" align="center" class="style1">
                        <asp:Button runat="server" ID="Button1" Text="&gt;&gt;" class="button" TabIndex="2"
                            Style="width: 95" OnClientClick="return AddFilterCType('all');" Height="26px"
                            Width="50px" UseSubmitBehavior="False" /><br />
                        <br />
                        <asp:Button runat="server" ID="Button2" Text="&gt;" class="button" TabIndex="2"
                            Style="width: 95" OnClientClick="return AddFilterCType('selected');" Height="26px"
                            Width="50px" UseSubmitBehavior="False" /><br />
                        <br />
                        <asp:Button runat="server" ID="Button3" Text="&lt;" class="button" TabIndex="4"
                            Style="width: 95; margin-left: 0px;"
                            OnClientClick="return RemoveFilterCType('selected');" Height="26px"
                            Width="50px" UseSubmitBehavior="False" /><br />
                        <br />
                        <asp:Button runat="server" ID="Button4" Text="&lt;&lt;" class="button" TabIndex="4"
                            Style="width: 95; margin-left: 0px;"
                            OnClientClick="return RemoveFilterCType('all');" Height="26px"
                            Width="50px" UseSubmitBehavior="False" /><br />
                        <br />
                    </td>
                    <td nowrap="true" width="*" valign="top">
                        <asp:ListBox
                            type="combobox" rmxref="/Instance/Document/Details/control[@name='lstRelatedCTypeComponents']"
                            runat="server" TabIndex="6"
                            ID="lstRelatedCTypeComponents" rmxignoreset="true"
                            size="10" Height="150px" Style="margin-top: 0px" Width="300px" SelectionMode="Multiple" EnableViewState="true"></asp:ListBox>
                    </td>
                </tr>
            </table>
        </tr>
        <tr height="10">
            <td class="auto-style3"></td>
        </tr>
        <div>
            <table>
                <tr>
                    <td class="auto-style3">
                       <asp:Label runat="server" ID="lblDtClm1" Text="<%$ Resources:lblDtClm %>" />
                    </td>
                    <td class="auto-style4">
                        <asp:Label runat="server" ID="lblFrom2" Text="<%$ Resources:lblFrom %>" />
                    </td>
                    <td class="auto-style6">
                        <asp:TextBox runat="server" FormatAs="date" ID="txtFrmDate" RMXRef="/Instance/Document/Details/FromDate"
                            RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="7" />
                       
                        <script type="text/javascript">
                            $(function () {
                                $("#txtFrmDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "8");
                            });
                        </script>
                    </td>
                    <td class="auto-style5">
                        <asp:Label runat="server" ID="lblTo" Text="<%$ Resources:lblTo %>" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" FormatAs="date" ID="txtToDate" RMXRef="/Instance/Document/Details/ToDate"
                            RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="9" />
                      
                        <script type="text/javascript">
                             $(function () {
                                 $("#txtToDate").datepicker({
                                     showOn: "button",
                                     buttonImage: "../../../../Images/calendar.gif",
                                     //buttonImageOnly: true,
                                     showOtherMonths: true,
                                     selectOtherMonths: true,
                                     changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "10");
                             });
                        </script>
                    </td>
                </tr>
                <tr height="10">
                    <td class="auto-style3" />
                </tr>
                <tr>
                    <td class="auto-style3">
                         <asp:Label runat="server" ID="lblDtClmUpdated" Text="<%$ Resources:lblDtClmUpdated %>" />
                    </td>
                    <td class="auto-style4">
                      <asp:Label runat="server" ID="lblFrom1" Text="<%$ Resources:lblFrom %>" />
                    </td>
                    <td class="auto-style6">
                        <asp:TextBox runat="server" FormatAs="date" ID="txtUFrmDate" RMXRef="/Instance/Document/Details/UFromDate"
                            RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="11" />
                       
                        <script type="text/javascript">
                             $(function () {
                                 $("#txtUFrmDate").datepicker({
                                     showOn: "button",
                                     buttonImage: "../../../../Images/calendar.gif",
                                     //buttonImageOnly: true,
                                     showOtherMonths: true,
                                     selectOtherMonths: true,
                                     changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "12");
                             });
                        </script>
                    </td>
                    <td class="auto-style5">
                        <asp:Label runat="server" ID="lblTo1" Text="<%$ Resources:lblTo %>" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" FormatAs="date" ID="txtUToDate" RMXRef="/Instance/Document/Details/UToDate"
                            RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="13" />
                       
                        <script type="text/javascript">
                            $(function () {
                                $("#txtUToDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../../../Images/calendar.gif",
                                   // buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "14");
                            });
                        </script>
                    </td>
                </tr>
                <tr height="10">
                    <td class="auto-style3" />
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <%} %>
        <%-- <End averma62  MITS 32386 FAS Filters>--%>
            <%--akaushik5 Added for MITS 36381 Starts--%>
        <%if (hdSystemModuleName.Text.Equals("Resbal"))
          { %>
            <tr width="100%" height="20">
            <td class="ctrlgroup2" colspan="5">
                &#160; 
               <%--kverma6- MITS 33898 Start --%>
               <%-- Reserve Balance Related Parameters:--%>
               <asp:Label ID="lblReserveBalanceParamMonthly" runat="server" Text="<%$ Resources:lblReserveBalanceParamMonthly %>"/>
               <%--kverma6- MITS 33898 End --%>
            </td>
        </tr>
        <tr height="20">
            <td class="auto-style14">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:RadioButton ID="optResbalAllClaims" runat="server" onclick="SelectResbalOptions(this);"
                    rmxref="/Instance/Document/Details/ResbalOption" Text="<%$ Resources:rbtnOptResbalAllClaims %>" value="1" /> <%--kverma6- MITS 33898  --%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:RadioButton ID="optResbalSingleClaim" runat="server" onclick="SelectResbalOptions(this);"
                    rmxref="/Instance/Document/Details/ResbalOption" Text="<%$ Resources:rbtnOptResbalSingleClaim %>" value="2" /><%--kverma6- MITS 33898  --%>
            </td>
        </tr>
        <tr id="trResbalSingleClaim" style="display:none">
            <td align="left" >
                <%--kverma6- MITS 33898  --%>
               <%-- Claim Number:--%>
                <asp:Label ID="lblClmNumber" runat="server" Text="<%$ Resources:lblClmNumber %>"/>
                <%--kverma6- MITS 33898 End --%>
            </td>
            <td class="auto-style8">
                <asp:TextBox ID="txtClaimNumber" runat="server" 
                    rmxref="/Instance/Document/Details/ResbalClaimNumber"/>
            </td>
        </tr>
            
        <tr>
            <td colspan="2">
                <asp:RadioButton ID="optResbalOnClaimDate" runat="server" onclick="SelectResbalOptions(this);"
                    rmxref="/Instance/Document/Details/ResbalOption" Text="<%$ Resources:rbtnOptResbalOnClaimDate %>" value="3" /> <%--kverma6- MITS 33898 --%>
            </td>
        </tr>
        <tr id="trResbalClaimDate" style="display:none">
            <td colspan="2">
                <table width="100%">
                    <tr>
                    <td width ="20%"> <asp:Label ID="lblFrom" runat="server" Text="<%$ Resources:lblFrom %>"/> </td> <%--kverma6- MITS 33898 --%>
                    <td class="auto-style8">
                        <asp:TextBox runat="server" FormatAs="date" ID="txtResbalFromDate" RMXRef="/Instance/Document/Details/FromDate"
                            RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="21" />
                       <%-- <input type="button" class="DateLookupControl" name="resbalfrmdatebtn" />

                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
                            {
                                inputField: "txtResbalFromDate",
                                ifFormat: "%m/%d/%Y",
                                button: "resbalfrmdatebtn"
                            }
                            );
                        </script>--%>
                         <script type="text/javascript">
                             $(function () {
                                 $("#txtResbalFromDate").datepicker({
                                     showOn: "button",
                                     buttonImage: "../../../../Images/calendar.gif",
                                     //buttonImageOnly: true,
                                     showOtherMonths: true,
                                     selectOtherMonths: true,
                                     changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "22");
                             });
                        </script>
                    </td>
                </tr>
            <tr>
            <td width ="20%"><asp:Label ID="lblTo2" runat="server" Text="<%$ Resources:lblTo %>"/> </td><%--kverma6- MITS 33898 --%>
            <td class="auto-style8">
                <asp:TextBox runat="server" FormatAs="date" ID="txtResbalToDate" RMXRef="/Instance/Document/Details/ToDate"
                    RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="23" />
              <%--  <input type="button" class="DateLookupControl" name="resbaltodatebtn" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
                    {
                        inputField: "txtResbalToDate",
                        ifFormat: "%m/%d/%Y",
                        button: "resbaltodatebtn"
                    }
                    );
                </script>--%>
                 <script type="text/javascript">
                     $(function () {
                         $("#txtResbalToDate").datepicker({
                             showOn: "button",
                             buttonImage: "../../../../Images/calendar.gif",
                             //buttonImageOnly: true,
                             showOtherMonths: true,
                             selectOtherMonths: true,
                             changeYear: true
                         }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "24");
                     });
                </script>
            </td>
        </tr>
                    </table>
                </td>
            </tr>
        <%} %>
        <%--akaushik5 Added for MITS 36381 Ends--%>
        <tr height="20">
                <td></td>
        </tr>
        <tr>
                <td>&#160;&#160;
            </td>
            <td align="left" colspan="2">
                <asp:Button ID="btnOptionset" Text="<%$ Resources:btnOptionset %>" class="button" OnClientClick="return OpenOptionset('4');" OnClick="MoveToOptionset" runat="server" />
                &#160;&#160;
                <asp:Button ID="btnSave" Text="<%$ Resources:btnSave %>" class="button" OnClientClick="SaveMonthlySettings();return false;" runat="server" />
                &#160;&#160;
                <asp:Button ID="btnCancel" Text="<%$ Resources:btnCancel %>" class="button" OnClientClick="OnCancel();return false;" runat="server" />
            </td>
        </tr>
    </table>
    <asp:TextBox value="" Style="display: none" ID="hdnaction" runat="server" />
    <asp:TextBox value="" Style="display: none" ID="hdnScheduleId" runat="server" rmxref="/Instance/Document/Details/ScheduleId"/>
    <asp:TextBox value="" Style="display: none" ID="hdnsaved" rmxref="/Instance/Document/Details/saved" runat="server" />
    <asp:TextBox value="" Style="display: none" ID="hdTaskType" rmxref="/Instance/Document/Details/TaskType"
        runat="server" />
    <asp:TextBox value="" Style="display: none" ID="hdUserArguments" rmxref="/Instance/Document/Details/UserArguments" runat="server" />
    <asp:TextBox value="" Style="display: none" ID="hdTaskName" rmxref="/Instance/Document/Details/TaskName" runat="server" />
    <asp:TextBox value="" Style="display: none" ID="hdIsDataIntegratorTask" runat="server" rmxref="/Instance/Document/Details/IsDataIntegratorTask" />
    <asp:TextBox value="" Style="display: none" ID="hdOptionsetId" runat="server" rmxref="/Instance/Document/Details/OptionsetId" />
    <asp:HiddenField ID="hdScheduleTypeId" runat="server"/>
    <asp:TextBox value="" Style="display: none" ID="hdSystemModuleName" runat="server" rmxref="/Instance/Document/Details/SystemModuleName" />
    </form>
</body>
</html>
