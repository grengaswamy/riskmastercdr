﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Riskmaster.Common;


namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public class CommonFunctions
    {
        /// <summary>
        /// Optionset Id.
        /// </summary>
        private static int m_iOptionsetId = 0;

        public static string GetTaskManagerXml(HttpRequest Request)
        {
            string sXml = string.Empty;
            XElement objTempElement = null;
            object objTempControl = null;
            int iScheduleType = 0;

            XElement objXElement = GetMessageTemplate();

            objTempControl = Request.Form["lblTaskTypeText"];
            if (objTempControl != null)
            {
                objTempElement = objXElement.XPathSelectElement("./TaskTypeText");
                objTempElement.Value = objTempControl.ToString();
            }

            objTempControl = Request.Form["hdTaskName"];
            if (objTempControl != null)
            {
                objTempElement = objXElement.XPathSelectElement("./TaskNameLabel");
                objTempElement.Value = objTempControl.ToString();

                objTempElement = objXElement.XPathSelectElement("./TaskName");
                objTempElement.Value = objTempControl.ToString();
            }

            objTempControl = Request.Form["hdScheduleTypeId"];
            if (objTempControl != null)
            {

                objTempElement = objXElement.XPathSelectElement("./ScheduleTypeId");
                objTempElement.Value = objTempControl.ToString();
                iScheduleType = Conversion.ConvertObjToInt(objTempControl);
            }

            objTempControl = Request.Form["lblScheduleTypeText"];
            if (objTempControl != null)
            {
                objTempElement = objXElement.XPathSelectElement("./ScheduleTypeText");
                objTempElement.Value = objTempControl.ToString();
            }

            objTempControl = Request.Form["txtDate"];
            if (objTempControl != null)
            {
                objTempElement = objXElement.XPathSelectElement("./Date");
                objTempElement.Value = objTempControl.ToString();
            }

            objTempControl = Request.Form["txtTime"];
            if (objTempControl != null)
            {
                objTempElement = objXElement.XPathSelectElement("./Time");
                objTempElement.Value = objTempControl.ToString();
            }

            objTempControl = Request.Form["hdnScheduleId"];
            if (objTempControl != null)
            {
                objTempElement = objXElement.XPathSelectElement("./ScheduleId");
                objTempElement.Value = objTempControl.ToString();
            }

            objTempControl = Request.Form["hdTaskType"];
            if (objTempControl != null)
            {
                objTempElement = objXElement.XPathSelectElement("./TaskType");
                objTempElement.Value = objTempControl.ToString();
            }

            objTempControl = Request.Form["hdIsDataIntegratorTask"];
            if (objTempControl != null)
            {
                objTempElement = objXElement.XPathSelectElement("./IsDataIntegratorTask");
                objTempElement.Value = objTempControl.ToString();
            }

            objTempControl = Request.Form["hdOptionsetId"];
            if (objTempControl != null)
            {
                objTempElement = objXElement.XPathSelectElement("./OptionsetId");
                objTempElement.Value = objTempControl.ToString();
                m_iOptionsetId = Riskmaster.Common.Conversion.ConvertObjToInt(objTempControl);
            }

            switch (iScheduleType)
            {
                case 1:
                    break;
                case 2:
                    objTempControl = Request.Form["IntervalType"];
                    if (objTempControl != null)
                    {
                        objXElement.Add(new XElement("IntervalType", objTempControl.ToString()));
                        objXElement.Add(new XElement("IntervalTypeId", objTempControl.ToString()));
                    }

                    //objTempControl = Request.Form["hdnIntervalTypeId"];
                    //if (objTempControl != null)
                    //{
                    //    objXElement.Add(new XElement("IntervalTypeId", ""));
                    //}

                    objTempControl = Request.Form["Interval"];
                    if (objTempControl != null)
                    {
                        objXElement.Add(new XElement("Interval", objTempControl.ToString()));
                    }

                    break;
                case 3:
                    SetCheckBoxValue(Request, "chkMon_Run", objXElement);
                    SetCheckBoxValue(Request, "chkTue_Run", objXElement);
                    SetCheckBoxValue(Request, "chkWed_Run", objXElement);
                    SetCheckBoxValue(Request, "chkThu_Run", objXElement);
                    SetCheckBoxValue(Request, "chkFri_Run", objXElement);
                    SetCheckBoxValue(Request, "chkSat_Run", objXElement);
                    SetCheckBoxValue(Request, "chkSun_Run", objXElement);

                    break;
                case 4:
                    objTempControl = Request.Form["Month"];
                    if (objTempControl != null)
                    {
                        objXElement.Add(new XElement("Month", objTempControl.ToString()));
                    }

                    objTempControl = Request.Form["DayOfMonth"];
                    if (objTempControl != null)
                    {
                        objXElement.Add(new XElement("DayOfMonth", objTempControl.ToString()));
                    }

                    break;
                case 5:
                    SetCheckBoxValue(Request, "chkJan_Run", objXElement);
                    SetCheckBoxValue(Request, "chkFeb_Run", objXElement);
                    SetCheckBoxValue(Request, "chkMar_Run", objXElement);
                    SetCheckBoxValue(Request, "chkApr_Run", objXElement);
                    SetCheckBoxValue(Request, "chkMay_Run", objXElement);
                    SetCheckBoxValue(Request, "chkJun_Run", objXElement);
                    SetCheckBoxValue(Request, "chkJul_Run", objXElement);
                    SetCheckBoxValue(Request, "chkAug_Run", objXElement);
                    SetCheckBoxValue(Request, "chkSep_Run", objXElement);
                    SetCheckBoxValue(Request, "chkOct_Run", objXElement);
                    SetCheckBoxValue(Request, "chkNov_Run", objXElement);
                    SetCheckBoxValue(Request, "chkDec_Run", objXElement);

                    break;
                default:
                    break;
            }

            return objXElement.ToString();
        }

        /// <summary>
        /// Optionset Id.
        /// </summary>
        public static int OptionsetId
        {
            get
            {
                return m_iOptionsetId;
            }
            set
            {
                m_iOptionsetId = value;
            }
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private static XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <Details>
                  <TaskTypeText></TaskTypeText>
                  <TaskNameLabel></TaskNameLabel>
                  <ScheduleTypeId/>
                  <ScheduleTypeText></ScheduleTypeText>
                  <Date></Date>
                  <Time></Time>
                  <bParams>False</bParams>
                  <Arguments></Arguments>
                  <ScheduleId></ScheduleId>
                  <saved></saved>
                  <TaskType></TaskType>
                  <TaskName></TaskName>
                  <OptionsetId />
                  <IsDataIntegratorTask />
                   <SystemModuleName/>
                </Details>
            ");

            return oTemplate;
        }

        private static void SetCheckBoxValue(HttpRequest Request, string p_sElementName, XElement p_objXElement)
        {
            object objTempControl = null;

            string sXmlElement = p_sElementName.Replace("chk", "");

            objTempControl = Request.Form[p_sElementName];

            if (objTempControl == null)
            {
                p_objXElement.Add(new XElement(sXmlElement, "False"));
            }
            else
            {
                p_objXElement.Add(new XElement(sXmlElement, "True"));
            }
        }
    }

}
