<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DDSSettings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.DDSSettings" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>DDS Settings</title>
    <link href="../../../../Content/dhtml-div.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js">{var i;}  </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}  </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}  </script>--%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/DataIntegrator.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/utilities.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/tmsettings.js"></script>
    <%--vkumar258 - RMA-6037 - Starts --%>
    <%--<script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
    <link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
    <script type="text/javascript" src="../../Scripts/calendar-alias.js"></script>
    
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <style type="text/css">
        .style2
        {
            border-style: none;
            border-color: inherit;
            border-width: medium;
            FONT-WEIGHT: bold;
            COLOR: #ffffff;
            BACKGROUND-COLOR: #95B3D7;
            float: left;
        }

        .style3
        {
            width: 627px;
        }

        .style4
        {
            width: 540px;
        }

        .style5
        {
            width: 121px;
        }
        .style6
        {
            width: 403px;
        }

    </style>
    <%--<script language='javascript' type="text/javascript">
        function AddNewRow() {
            debugger;
            var str1 = "";
            var str2 = "";
            var str3 = "";
            var table = document.getElementById('mapping');
            var tr = document.createElement('TR');
            var td1 = document.createElement('TD');
            var td2 = document.createElement('TD');
            var td3 = document.createElement('TD');
            var i = table.rows;
            str1 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + i;
            td1.innerTEXT = str1;
            td1.setAttribute('class', 'style6');
            td1.setAttribute('id', i);
            tr.appendChild(td1);
            str2 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' Text='' AutoPostBack='true' />";
            td2.innerHTML = str2;
            td2.setAttribute('class', 'style5');
            td2.setAttribute('id', 'chkMapped' + i);
            tr.appendChild(td2);
            
            td3.innerHTML = str3;
            td3.setAttribute('class', 'style4');
            td3.setAttribute('id','Reserve_Map' + i );
            tr.appendChild(td3);
            table.appendChild(tr);
            
        }
    </script>--%>
</head>

<body class="10pt">
    <form id="frmData" name="frmData" method="post" runat="server" enctype="multipart/form-data" >
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td colspan="2">
              <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>    
        <tr>
            <td>
            <asp:ScriptManager ID="ScriptManager1" runat ="server" />
            </td>
        </tr>
    </table>
    <div>
        <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="TextBox9" />
        <div class="msgheader" id="div_formtitle" runat="server">
            DDS Optionset
            <asp:Label ID="formsubtitle" runat="server" Text="" />
        </div>
        <div id="Div1" class="errtextheader" runat="server">
            <asp:Label ID="formdemotitle" runat="server" Text="" />
        </div>
    </div>
    <br />
    <table border="0">
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="NotSelected" nowrap="true" name="TABSSettings" id="TABSSettings">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="Settings"
                                id="LINKTABSSettings"><span style="text-decoration: none">General</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSAdvanced" id="TABSAdvanced">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="Advanced"
                                id="LINKTABSAdvanced"><span style="text-decoration: none">Advanced</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSReserve" id="TABSReserve">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="Reserve"
                                id="LINKTABSReserve"><span style="text-decoration: none">Reserve Mappings</span></a>
                        </td>
                    </tr>    
                 </table>
            </td>
        </tr>
    </table>
    <div>                    
        <table width=100% border="0" cellspacing="0" cellpadding="0" name="FORMTABSettings" id="FORMTABSettings">
            <br />
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblUserOption"
                        Text="Optionset Name:      " Font-Size="Small" Font-Bold="True" 
                    Font-Underline="True"></asp:Label>
                    <asp:TextBox ID="txtOptionName" runat="server" Width="20%" Font-Size="Small" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>                                                                                                  
                    <asp:CheckBox ID="gPerform_Verification" runat="server" Text="Verify data prior to import." />
                </td>
            </tr>                                            
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>                                               
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td class="msgheader" width="100%" style="background-color:#C5D2E5;">- Available 
                                Files</td>     
                        </tr>                                            
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="EventCSVFilesPanel" runat ="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                    <ContentTemplate>
                                         <table align="left" style="width: 50%">
                                              <tr>
                                               <td class="style2" colspan="2">
                                               <asp:CheckBox ID="gEvents_Available" runat="server" Text="Event" AutoPostBack="true" OnCheckedChanged="chkEvent_CheckedChanged"/>
                                               <%--<asp:CheckBox ID="gEvents_Available" runat="server" Text="Event" />--%>
                                               </td>
                                              </tr>
                                              <tr id="EventCSVFiles" runat="server" visible="false">
                                              <%--<tr id="EventCSVFiles" runat="server" >--%>
                                                <td style="width:40%;">
                                                     Event CSV:
                                                </td>
                                                <td align="left" style="width:60%;">      
                                                     <asp:FileUpload ID="EventCSVUpload" runat="server" />
                                                </td>
                                              </tr>
                                         </table> 
                                     </ContentTemplate>
                                     <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="gEvents_Available" EventName="CheckedChanged" /> 
                                     </Triggers>
                                 </asp:UpdatePanel>
                           </td>
                        </tr>
                    
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="MedCSVFilesPanel" runat ="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <table align="left" style="width: 50%">
                          <tr>
                           <td class="style2" colspan="2">
                           <asp:CheckBox ID="gMed_Watch_Available" runat="server" Text="Medwatch" AutoPostBack="true" OnCheckedChanged="chkMedwatch_CheckedChanged"/>
                           </td>
                          </tr>
                          <tr id="MedCSVFiles" runat="server" visible="false">
                            <td style="width:40%;">
                                 Medwatch CSV:
                            </td>
                            <td align="left" style="width:60%;">
                                 <asp:FileUpload ID="MedCSVUpload" runat="server"/>
                            </td>
                          </tr>
                         </table> 
                                 </ContentTemplate>
                                 <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gMed_Watch_Available" EventName="CheckedChanged" /> 
                                 </Triggers>
                                 </asp:UpdatePanel>
                            </td>
                        </tr>
                    
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="ClaimCSVFilesPanel" runat ="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                    <ContentTemplate>
                                         <table align="left" style="width: 50%">
                                          <tr>
                                           <td class="style2" colspan="2">
                                           <asp:CheckBox ID="gClaims_Available" runat="server" Text="Claim" AutoPostBack="true" OnCheckedChanged="chkClaim_CheckedChanged"/>
                                           </td>
                                          </tr>
                                          <tr id="ClaimCSVFiles" runat="server" visible="false">
                                            <td style="width:40%;">
                                                 Claim CSV:
                                            </td>
                                            <td align="left" style="width:60%;">
                                                 <asp:FileUpload ID="ClaimCSVUpload" runat="server"/>
                                            </td>
                                          </tr>
                                         </table> 
                                     </ContentTemplate>
                                     <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="gClaims_Available" EventName="CheckedChanged" /> 
                                     </Triggers>
                                 </asp:UpdatePanel>
                            </td>
                        </tr>
                    
                        <tr>
                            <td>
                            <asp:UpdatePanel ID="DisCSVFilesPanel" runat ="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>                                                
                             <table align="left" style="width: 50%">
                              <tr>
                                   <td class="style2" colspan="2">
                                   <asp:CheckBox ID="gShort_Term_Disability_Available" runat="server" Text="Short Term Disability" AutoPostBack="true" OnCheckedChanged="chkDisability_CheckedChanged"/>
                                   </td>
                              </tr>
                              <tr id="DisplanCSVFiles" runat="server" visible="false">
                                <td style="width:40%;">
                                     ST Dis Plan CSV:
                                </td>     
                                <td align="left" style="width:60%;">
                                     <asp:FileUpload ID="DisPlanCSVUpload" runat="server"/>
                                </td>
                              </tr>
                              <tr id="DisclassCSVFiles" runat="server" visible="false">
                                <td style="width:40%;">
                                     ST Dis Class CSV:
                                </td>
                                <td align="left" style="width:60%;">
                                     <asp:FileUpload ID="DisclassCSVUpload" runat="server"/>
                                </td>
                              </tr>                                                  
                             </table> 
                             </ContentTemplate>
                             <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="gShort_Term_Disability_Available" EventName="CheckedChanged" /> 
                             </Triggers>                                                 
                             </asp:UpdatePanel>
                           </td>
                        </tr>
                    
                        <tr>
                            <td>
                                 <asp:UpdatePanel ID="AdjusterCSVFilesPanel" runat ="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                    <ContentTemplate>                                                  
                                         <table align="left" style="width: 50%">
                                              <tr>
                                               <td class="style2" colspan="2">
                                               <asp:CheckBox ID="gAdjuster_Notes_Available" runat="server" Text="Adjuster Notes" AutoPostBack="true" OnCheckedChanged="chkAdjuster_CheckedChanged"/>
                                               </td>
                                              </tr>
                                              <tr id="AdjusterCSVFiles" runat="server" visible="false">
                                                <td style="width:40%;">
                                                     Adjuster Notes CSV:
                                                </td>
                                                <td align="left" style="width:60%;">
                                                     <asp:FileUpload ID="AdjusterCSVUpload" runat="server"/>
                                                </td>
                                              </tr>
                                         </table> 
                                     </ContentTemplate>
                                     <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="gAdjuster_Notes_Available" EventName="CheckedChanged" /> 
                                     </Triggers>                                                   
                                 </asp:UpdatePanel>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                 <asp:UpdatePanel ID="PaymentsPanel" runat ="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                    <ContentTemplate>                                                 
                                         <table align="left" style="width: 50%">
                                          <tr>
                                           <td class="style2" colspan="2">
                                           <asp:CheckBox ID="gPayments_Available" runat="server" Text="Payments" AutoPostBack="true" OnCheckedChanged="chkPayments_CheckedChanged"/>
                                           </td>
                                          </tr>
                                          <tr id="PaymentsCSVFiles" runat="server" visible="false">
                                            <td style="width:40%;">
                                                 Payments CSV:
                                            </td>
                                            <td align="left" style="width:60%;">
                                                 <asp:FileUpload ID="PaymentsCSVUpload" runat="server"/>
                                            </td>
                                          </tr>
                                          <tr id="BankAcc" runat="server" visible="false">
                                            <td style="width:40%;">
                                                <asp:Label runat="server" ID="lblBankAccount" Text="Payment Bank Account:"></asp:Label>
                                            </td>
                                            <td align="left" style="width:60%;">  
                                                <asp:DropDownList ID="gPayment_Bank_Account" runat="server"/>
                                            </td>
                                          </tr>
                                         </table> 
                                    </ContentTemplate>
                                     <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="gPayments_Available" EventName="CheckedChanged" /> 
                                     </Triggers>                                                  
                                 </asp:UpdatePanel>
                            </td>
                        </tr>
                    
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="EnhNotesCSVFilesPanel" runat ="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                    <ContentTemplate>                                                
                                         <table align="left" style="width: 50%">
                                              <tr>
                                               <td class="style2" colspan="2">
                                               <asp:CheckBox ID="gEnhanced_Notes_Available" runat="server" Text="Enhanced Notes" AutoPostBack="true" OnCheckedChanged="chkEnhNotes_CheckedChanged"/>
                                               </td>
                                              </tr>
                                              <tr id="EnhNotesCSVFiles" runat="server" visible="false">
                                                <td style="width:40%;">
                                                     Enhanced Notes CSV:
                                                </td>
                                                <td align="left" style="width:60%;">                                                          
                                                     <asp:FileUpload ID="EnhNotesCSVUpload" runat="server"/>
                                                </td>
                                              </tr>
                                         </table> 
                                     </ContentTemplate>
                                     <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="gEnhanced_Notes_Available" EventName="CheckedChanged" /> 
                                     </Triggers>                                                   
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td><br /></td></tr>
            <tr>
                <td>
                 <table width="100%" align="left">
                  <tr>
                   <td class="msgheader" width="100%" style="background-color:#C5D2E5;">- Global 
                       Options
                   </td>
                   </tr>
                 </table>
                 </td> 
            </tr>
            <tr>
                <td>
                    <table style="width:90%;">
                        <tr>
                            <td style="width:50%;">
                                <asp:CheckBox ID="gAllow_New_Claims" runat="server" 
                                    Text="Allow Creation of New Claims" />
                            </td>
                            <td style="width:50%;">
                                <asp:CheckBox ID="gSet_Adjuster_As_Current" runat="server" 
                                    Text="Set Adjuster as Current Adjuster" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                                <asp:CheckBox ID="gAllow_New_Codes" runat="server" 
                                    Text="Allow Creation of New Codes"  />
                            </td>
                            <td style="width:50%;">
                                <asp:CheckBox ID="gDont_Check_Duplicate_Payments" runat="server" 
                                    Text="Don't Check Duplicate Payments" />
                            </td>
                         </tr>
                        <tr>
                            <td style="width:50%;">
                                <asp:CheckBox ID="gAllow_New_Employees" runat="server" 
                                    Text="Allow Creation of New Employees"  />
                            </td>    
                            <td style="width:50%;">
                                <asp:CheckBox ID="gAttach_Trans_Id_To_Check_No" runat="server" 
                                    Text="Attach Trans ID to Check Number"/>
                            </td>
                         </tr>
                        <tr>
                            <td style="width:50%;">
                               <asp:CheckBox ID="gAllow_New_Department" runat="server" 
                                    Text="Allow Creation of New Department"/>
                            </td>
                            <td style="width:50%;">
                               <asp:CheckBox ID="gUse_Tax_Id_In_Entity_Matches" runat="server" 
                                    Text="Use Tax ID in Entity Matches"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                               <asp:CheckBox ID="gValidate_Supplementals" runat="server" 
                                    Text="Import Supplemental Data"/>
                            </td>
                             <td style="width:50%;">
                               <asp:CheckBox ID="gNew_Entity_If_TaxId_Empty_In_Match" runat="server" 
                                    Text="Create New Event PI Entity if data Tax ID is empty for match including Tax ID"/>
                            </td>                             
                        </tr>
                    </table> 
                </td> 
            </tr>                                              
            <tr id="Tr5">
                <td colspan=3>
                 <table align="left" style="width: 100%">
                    <tr>
                       <td class="msgheader" width="100%" style="background-color:#C5D2E5;">- GC/VA Options
                       </td>
                   </tr>
                 </table>
                </td>
             </tr>
            <tr>
                <td>
                  <table style="width:90%;">
                  <tr>
                    <td style="width:50%;">
                        <asp:RadioButton ID="gId_Claimant_By_Name_GCVA"
                            Text="Identify Claimants Based on Claimant Name" Checked="true"
                            runat="server" GroupName="IdentifyClaimant" />
                    </td>
                    <td style="width:50%;">
                        <asp:CheckBox ID="gUpdate_Claimant_Name_GCVA" runat="server" 
                            Text="Update Claimant Name" />
                    </td> 
                  </tr>       
                  <tr>
                    <td style="width:50%;">
                        <asp:RadioButton ID="gId_Claimant_By_Suffix_GCVA"
                            Text="Identify Claimants Based on Claim Suffix" 
                            runat="server" GroupName="IdentifyClaimant" />
                    </td>
                    <td style="width:50%;">
                        <asp:CheckBox ID="gCreate_Entity_If_Tax_ID_Empty_GCVA" runat="server"                                                             
                            Text="Create New Entity if Tax ID is empty and Identify Claimants Based on Claim Suffix" />
                    </td>
                   </tr>  
                   </table> 
                </td>
            </tr>
            <tr>
                <td>
                 <table align="left" style="width: 100%">
                  <tr>
                   <td class="msgheader" width="100%" style="background-color:#C5D2E5;">- WC Options
                   </td>
                   </tr>
                   <tr>
                   <td style="width:50%;">
                        <asp:CheckBox ID="gUpdate_Claimant_Name_WC" runat="server"
                                    text="Update Claimant Name" />
                   </td>
                   </tr>
                 </table>
                 </td>
             </tr>
        </table>
                                     
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABAdvanced" id="FORMTABAdvanced" >
            <br />
            <tr>
                <td>
                    Employee Match By:
                </td>
                <td>
                    <asp:CheckBox ID="gEmployee_Match_By_Name" runat ="server" Text="Name"/>
                </td>
                <td>
                    <asp:CheckBox ID="gEmployee_Match_By_Tax_Id" runat ="server" Text="Tax ID"/>
                </td>
                <td>
                    <asp:CheckBox ID="gEmployee_Match_By_Emp_Num" runat ="server" Text="Employee Number"/>
                </td>                                                                        
            </tr>
                                   
            <tr>
                <td>
                    Non-Employee Entity Match By:
                </td>
                <td>
                    <asp:RadioButton ID="gNon_Emp_Match_By_Name" runat ="server" Text="Name Only" GroupName="Non-EmployeeEntityMatch"/>
                </td>
                <td>
                    <asp:RadioButton ID="gNon_Emp_Match_By_Both" runat ="server" Text="Name and Tax ID" GroupName="Non-EmployeeEntityMatch"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                </td>
                <td>
                    <asp:RadioButton ID="gNon_Emp_Match_By_Tax_Id" runat ="server" Text="Tax ID Only" GroupName="Non-EmployeeEntityMatch"/>
                </td>
            </tr>
                                   
            <tr>
               <td>
                   Maximum Validation Errors Allowed:
               </td>
               <td>
                    <asp:TextBox ID="gMax_Errors" runat="server" Width="116px" />
               </td>
            </tr>
                                   
            <tr>
               <td> 
                   Date Field Validation Range:
               </td>
               <td>
                    <input type="text" name="gValid_Start_Date" runat="server" id="gValid_Start_Date" size="10" onblur="dateLostFocus(this.id);"/>
                        <%--vkumar258 - RMA-6037 - Starts --%>
                        <%--<input type="button" class="DateLookupControl" id="btnStartDate" value="..."/>
                    <script type="text/javascript">
                    Zapatec.Calendar.setup(
                    {
                        inputField: "gValid_Start_Date",
                        ifFormat: "%m/%d/%Y",
                        button: "btnStartDate"
                    }
                    );
                    </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#gValid_Start_Date").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                            });
                        </script>
                        <%--vkumar258 - RMA_6037- End--%>
                    </td>
                    <td align="center">to </td>
                    <td>
                        <input type="text" name="gValid_End_Date" runat="server" id="gValid_End_Date" size="10" onblur="dateLostFocus(this.id);" />
                        <%--vkumar258 - RMA-6037 - Starts --%>
                        <%-- <input type="button" class="DateLookupControl" id="btnEndDate" value="..."/>
                    <script type="text/javascript">
                       Zapatec.Calendar.setup(
                    {
                        inputField: "gValid_End_Date",
                        ifFormat: "%m/%d/%Y",
                        button: "btnEndDate"
                    }
                    );
                    </script>      --%>
                        <script type="text/javascript">
                            $(function () {
                                $("#gValid_End_Date").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                            });
                        </script>
                        <%--vkumar258 - RMA_6037- End--%>                            
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:checkbox id="gRecalculate_Reserves" runat="server" text="Recalculate Reserves After Upload" />
                    </td>
                </tr>

                <%-- "Removed as per MITS 29551" <tr>
               <td>
                    <asp:CheckBox ID="gUpdate_Claimant_Details" runat="server" 
                       Text="Update Claimant Details on Claims Upload" />
               </td>
            </tr> --%>
        </table>
            
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABReserve" id="FORMTABReserve">
            <br />
		    <tr>
			    <td class="style3">
		            <table border="0" cellspacing="0" cellpadding="0" style="width: 99%">
			            <tr>
		                    <td align="center" class="style6">Line of Business:&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:DropDownList ID="lstLOB" runat="server" 
                                     OnSelectedIndexChanged="lstLOB_SelectedIndexChanged" AutoPostBack = "true">
                                </asp:DropDownList>
                                <br />
                                <br />
                            </td>
                        </tr>
	                    <tr>
	                        <td>
	                            <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
	                                    <ContentTemplate>
                                            <asp:GridView ID="GV_ReserveMapping" runat="server" AutoGenerateColumns="False" Width="100%" 
                                                OnRowDataBound="GV_OnRowDataBound" ondatabound="GV_OnDataBound">
                                                <Columns >
                                                    <asp:BoundField HeaderText="Bucket" DataField="Bucket"  >
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Mapped">
                                                        <ItemTemplate >
                                                        <asp:CheckBox runat="server" ID="chkMapped" value='<%# DataBinder.Eval(Container.DataItem,"Mapped")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Reserve Type" >
                                                        <ItemTemplate>
                                                        <asp:DropDownList ID="lstReserveType" runat="server" ></asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>  
                                            				
						                    <asp:Button runat="server" ID="btnAddNew" Text="Add New Reserve Mapping" onclick="btnAddNewRow_Click" class ="button" />
				                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="lstLOB" EventName="SelectedIndexChanged" /> 
                                            <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="click" /> 
                                        </Triggers> 
                                 </asp:UpdatePanel>	
					        </td>
		                </tr>
		                <tr>
		                    <td></td>
		                </tr>
                     </table>
                </td>
            </tr>
        </table>                     
                    
        <table>
            <tr>
                <td style="text-align: center">
                    <asp:Button ID="btnSave" class ="button" runat="server" Text="Save" 
                        onclick="btnSave_Click" />
                    <asp:Button ID="btnCancel" class ="button" runat="server" onclick="btnCancel_Click" Text="Cancel" />
                </td>                    
            </tr>
        </table>       
    </div>
                
        <input type="hidden" name="hTabName" id="hTabName" runat="server" value="Settings" />
    <asp:HiddenField ID="hdTaskManagerXml" runat="server" />
    <asp:HiddenField ID="hdOptionsetId" runat="server" />
    <asp:HiddenField ID="hdnaction" runat="server" />
    <asp:HiddenField ID="gUse_Transactions" runat="server" />
    <asp:HiddenField ID="hValidate_Supplementals" runat="server" />
    </form>
</body>
</html>


