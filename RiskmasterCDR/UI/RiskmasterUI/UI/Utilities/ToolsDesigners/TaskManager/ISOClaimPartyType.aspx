<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ISOClaimPartyType.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.ISOClaimPartyType" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">    
    <title>ISO Claim Party Type Mapping with rmA MMSEA Claim Party Type</title>
    <link rel="stylesheet" href="../../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js">        { var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/supportscreens.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js">        { var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <script language="javascript" type="text/javascript">

        function ValidateDropDownSelection() {
            var ddlrmAPropType = document.getElementById('ddlrmAClaimParty');
            var ddlISOPropType = document.getElementById('ddlISOClaimParty');
            var ClientError = document.getElementById('ClientError');
            if (ddlrmAPropType != null && ddlrmAPropType.selectedIndex == 0) {
                debugger;
                if (ClientError != null) {
                    ClientError.style.display = 'block';
                    ClientError.innerHTML = "Please Select rmA Claim Party Type."
                    return false;
                }

            }
            if (ddlISOPropType != null && ddlISOPropType.selectedIndex == 0) {
                if (ClientError != null) {
                    ClientError.style.display = 'block';
                    ClientError.innerHTML = "Please Select ISO Claim Party Type."
                    return false;
                }

            }

        }

    </script>
</head>
<body>
    <form name="frmData" id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="ISO Role in the claim for beneficiary of a deceased claimant (CMS Reporting only)" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    &nbsp;
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/ISOAdditionalClaimant/control[@name ='RowId']" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" RMXRef="/Instance/Document/ISOAdditionalClaimant/control[@name ='mode']" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    &nbsp;
    <table style="width: 100%;">
        <tr id="div_rmAProp" class="full" >
            <td class="required" Width="160px" Font-Names="Times New Roman" Font-Size="Medium">
                rmA Claim Party Type
            </td>
            <td>
                <asp:DropDownList ID="ddlrmAClaimParty" runat="server" Width="250px"
                RMXRef="/Instance/Document/ISOAdditionalClaimant/control[@name ='rmA Additional Claimant Codes']" >
            </asp:DropDownList>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr id="div_ISOProp" >
        <td class="required"  Width="160px" Font-Names="Times New Roman" Font-Size="Medium">
            ISO Claim Party Type
             
            </td>
            <td>
        <span>
            <asp:DropDownList ID="ddlISOClaimParty" runat="server" Width="250px"
                RMXRef="/Instance/Document/ISOAdditionalClaimant/control[@name ='ISO Additional Claimant Codes']" >
            </asp:DropDownList>
        </span>
        </td>
         <td>
                &nbsp;
            </td>

        </tr>
        <tr  class="formButton" id="div_btnOk" align="center">
            
            <td align="right">
                 <asp:Button class="button" runat="server" ID="btnSave" Text="Save" Width="75px" OnClick="btnSave_Click" 
                 OnClientClick="return ValidateDropDownSelection();"/>
            </td>
            <td align="left">            
            <input type="button" value="Cancel" class="button" onclick="self.close()" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="line-height: 1px">
        <td colspan="3"></td>
        </tr>
        <tr >
        <td><asp:TextBox ID="txtISORowID"  runat="server" Visible="False"></asp:TextBox></td>
            
        <td >
            <asp:Label ID="ClientError" runat="server" Text="" ForeColor="#FF3300" Font-Bold="True"></asp:Label></td>
        <td></td>
        </tr>
    </table> 
   
    
    
    </form>
</body>
</html>
