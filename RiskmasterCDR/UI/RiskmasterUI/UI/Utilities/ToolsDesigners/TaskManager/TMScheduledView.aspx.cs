﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class TMScheduledView : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sReturn = string.Empty;
                string PageID = RMXResourceProvider.PageId("TMScheduledView.aspx");
                //ksahu5 MITS 33897 start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, PageID, "TMSettingValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "TMSettingValidations", sValidationResources, true);
                //ksahu5 MITS 33897 End
                TaskListGrid.LinkColumn = AppHelper.GetResourceValue(PageID, "gvHdrTaskName", "0");
				//Deb : MITS 32214
                if (hdnaction.Text == "Delete")
                {
                    XmlTemplate = GetMessageTemplate(hdnSelectedRow.Text);
                    bReturnStatus = CallCWS("TaskManagementAdaptor.DeleteSchedule", XmlTemplate, out sReturn, false, false);
					
				    hdnaction.Text = string.Empty;
                    XmlTemplate = GetMessageTemplate("");
                    bReturnStatus = CallCWS("TaskManagementAdaptor.GetScheduledJobs", XmlTemplate, out sReturn, false, true);
                }
                if (!IsPostBack || hdnaction.Text == "Refresh")
                {
                    XmlTemplate = GetMessageTemplate("");
                    bReturnStatus = CallCWS("TaskManagementAdaptor.GetScheduledJobs", XmlTemplate, out sReturn, false, true);
                    hdnRefresh.Text = "true";
                }
                //Deb : MITS 32214
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate(string sScheduleId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><Status><ScheduleId>");
            sXml = sXml.Append(sScheduleId);
            //ksahu5 MITS 33897 start
            //sXml = sXml.Append("</ScheduleId></Status></Document></Message>");
            sXml = sXml.Append("</ScheduleId><LangCodeID>");
            sXml = sXml.Append(AppHelper.GetLanguageCode());
            sXml = sXml.Append("</LangCodeID><PageID>");
            sXml = sXml.Append(RMXResourceProvider.PageId("TMScheduledView.aspx"));
            sXml = sXml.Append("</PageID></Status></Document></Message>");
            //ksahu5 MITS 33897 End
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
