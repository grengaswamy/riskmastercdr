<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PSOSettings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.PSOSettings" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>PSO Option Setting Page</title>
    <link href="../../../../Content/dhtml-div.css" rel="stylesheet" type="text/css" />
   
       <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script src="../../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/utilities.js"></script>
<%--    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>--%>
<%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">    { var i; } </script>  
    
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>--%>
    <script src="../../../../Scripts/TMSettings.js" type="text/javascript"></script>
     <link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    
    <style type="text/css">
        .style3
        {
            text-decoration: underline;
            width: 250px;
        }
        .style5
        {
            width: 337px;
        }
        .style9
        {
            width: 250px;
        }
        
    </style>
    
</head>
<body>
    <form id="frmData" name="frmData" method="post" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td colspan="2">
              <uc1:ErrorControl ID="ErrorControl1" runat="server" />
              
            </td>
            </tr>
    </table>
    
    <div>
                    
        <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="TextBox1" />
        <div class="msgheader" id="div_formtitle" runat="server">
<%--        Yukti-ML Changes Start
            <asp:Label ID="Label1" runat="server" Text="PSO Optionset" />--%>
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:lblLabel1 %>" />
            <asp:Label ID="formsubtitle" runat="server" Text="" />
        </div>
        
    </div>
            <td class="style4">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                                            <td class="Selected" nowrap="true" name="TABSexportsetting" 
                                                id="TABSexportsetting">
                            <span style="text-decoration: none"><%--Export Optionset --%>
                           <%-- Yukti-ML Changes Start--%>
                           <asp:Label runat="server" ID="lblExpOptionSet" Text="<%$ Resources:lblExpOptionSet %>" />
                           <%--Yukti-ML Changes End--%>
                            </span>
                        </td>
                    </tr>
                    </table>
                 </td>
                 
                 <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
                    <tr>
                        <td align="left" width="20%" style="font-weight: bold">
                            <%--Yukti-ML Changes Start
                            Optionset Name--%>
                            <asp:Label runat="server" ID="lblOptionSetName" Text="<%$ Resources:lblOptionSetName %>" />
                            <%--Yukti-ML Changes--%>
                            </td>
                        <td width="80%" align="left">
                        
                            <asp:TextBox ID="txtExportOptionset" runat="server" Width="204px"></asp:TextBox>
                        
                        &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" style="font-weight: bold">
                        <%--YUkti-ML Changes Start
                            Provider Id--%>
                            <asp:Label runat="server" ID="lblProviderId" Text="<%$ Resources:lblProviderId %>" />
                       <%-- Yukti-ML Changes End--%>
                            </td>
                        <td width="80%" align="left">
                        
                            <asp:TextBox ID="txtProviderId" runat="server" Width="204px"></asp:TextBox>
                        
                        &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" style="font-weight: bold">
                       <%-- Yukti-ML Changes Start
                            PSO ID--%>
                            <asp:Label runat="server" ID="lblPSOId" Text="<%$ Resources:lblPSOId %>" />
                        <%--Yukti-ML Changes End--%>
                            </td>
                        <td width="80%" align="left">
                        
                            <asp:TextBox ID="txtPSOId" runat="server" Width="204px"></asp:TextBox>
                        
                        &nbsp;
                        
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" >
                       <%-- YUkti-ML Changes Start
                            Company--%>
                            <asp:Label runat="server" ID="lblCompany" Text="<%$ Resources:lblCompany %>" />
                          <%--  Yukti-Ml Changes End--%>
                            
                            </td>
                        <td width="80%" align="left">
                            <%--Add Company Control here--%>                      
                        <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" 
                                onchange="lookupTextChanged(this);" id="txtCompany" RMXType="orgh" 
                                name="txtCompany" Width="204px"/>
                        <asp:button runat="server" class="CodeLookupControl" id="txtCompanybtn" onclientclick="return selectCode('orgh','txtCompany','Company');" />
                        <asp:TextBox style="display:none" runat="server" id="txtCompany_cid" />
                        
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" >
                        <%--Yukti-ML Changes Start
                            Event Date From--%>
                            <asp:Label runat="server" ID="lblEventDtFrom" Text="<%$ Resources:lblEventDtFrom %>" />
                            </td>
                        <td width="80%" align="left">
                        
                            <asp:TextBox runat="server" FormatAs="date" ID="txtEvtDateFrom" RMXType="date" onblur="dateLostFocus(this.id);" Width="204px" />
                            <%--Yukti-ML Changes Start--%>
                         <%-- <asp:Button class="DateLookupControl" runat="server" ID="btnEvtDateFrom"/>

                            <script type="text/javascript">
                                Zapatec.Calendar.setup({ inputField: "txtEvtDateFrom", ifFormat: "%m/%d/%Y", button: "btnEvtDateFrom" });      
                            </script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#txtEvtDateFrom").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../../../Images/calendar.gif",
                                        //buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                });
                    </script>
                        <%--YUkti-ML Changes End--%>
                        &nbsp;
                        
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" >
                        <%--Yukti-ML Changes Start--%>
                            <%--Event Date To--%>
                            <asp:Label runat="server" ID="lblEventDtTo" Text ="<%$ Resources:lblEventDtTo %>" />
                        <%--Yukti-ML Changes End--%>                            
                            </td>
                        <td width="80%" align="left">
                        
                            <asp:TextBox runat="server" FormatAs="date" ID="txtEvtDateTo" RMXType="date" onblur="dateLostFocus(this.id);" Width="204px" />
                           <%-- Yukti-ML Changes Start
                          <asp:Button class="DateLookupControl" runat="server" ID="btnEvtDateTo"/>

                            <script type="text/javascript">
                                Zapatec.Calendar.setup({ inputField: "txtEvtDateTo", ifFormat: "%m/%d/%Y", button: "btnEvtDateTo" });      
                            </script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#txtEvtDateTo").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../../../Images/calendar.gif",
                                       // buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                });
                            </script>
                        <%--YUkti-ML Changes End--%>
                        &nbsp;
                        
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" >
                        <%--Yukti-ML chnages Start
                            Event Date Added From--%>
                            <asp:Label runat="server" ID="lblEvDtAddedFrom" Text="<%$ Resources:lblEvDtAddedFrom %>" />
                            </td>
                        <td width="80%" align="left">
                        
                            <asp:TextBox runat="server" FormatAs="date" ID="txtEvtDateAddedFrm" RMXType="date" onblur="dateLostFocus(this.id);" Width="204px" />
                            <%--Yukti-ML changes Start--%>
                          <%--<asp:Button class="DateLookupControl" runat="server" ID="btnEvtDateAddedFrm"/>

                            <script type="text/javascript">
                                Zapatec.Calendar.setup({ inputField: "txtEvtDateAddedFrm", ifFormat: "%m/%d/%Y", button: "btnEvtDateAddedFrm" });      
                            </script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#txtEvtDateAddedFrm").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../../../Images/calendar.gif",
                                        //buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                });
                            </script>
                        
                        &nbsp;
                        <%--YUkti-ML Channges End--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                        <%--Yukti-ML Changes Start
                            Event Date Added To--%>
                            <asp:Label runat="server" ID="lblEvtDateAddedTo" Text="<%$ Resources:lblEvtDateAddedTo %>" />
                            <%--YUkti-ML Changes End--%>
                            </td>
                        <td width="80%" align="left">
                        
                            <asp:TextBox runat="server" FormatAs="date" ID="txtEvtDateAddedTo" RMXType="date" onblur="dateLostFocus(this.id);" Width="204px" />
                          <%--<asp:Button class="DateLookupControl" runat="server" ID="btnEvtDateAddedTo"/>

                            <script type="text/javascript">
                                Zapatec.Calendar.setup({ inputField: "txtEvtDateAddedTo", ifFormat: "%m/%d/%Y", button: "btnEvtDateAddedTo" });      
                            </script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#txtEvtDateAddedTo").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../../../Images/calendar.gif",
                                       // buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                });
                            </script>
                        
                        &nbsp;
                        
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" >
                        <%--Yukti-Ml Changes Start
                            Updated By User--%>
                            <asp:Label runat="server" ID="lblUserUpdated" Text="<%$ Resources:lblUserUpdated %>" />
                            <%--Yukt-ML Changes End--%>
                            </td>
                        <td width="80%" align="left">
                        
                            <asp:TextBox ID="txtUpdByUser" runat="server" Width="204px" ></asp:TextBox>
                        
                        &nbsp;
                        
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" >
                       <%-- Yukti-ML Changes Start
                            Event Numbers--%>
                            <asp:Label runat="server" ID="lblEventNum" Text="<%$ Resources:lblEventNum %>" />
                            <%--YUkti-ML Changes End--%>
                            </td>
                        <td width="80%" align="left">
                        
                            <asp:TextBox ID="txtEventNumbers" runat="server" Width="204px" ></asp:TextBox>
                        
                        &nbsp;
                        <%--<span>(This will override all other criteria selected.)</span>--%>
                        <span><asp:Label runat="server" ID="lblOverrideCriteria" Text="<%$ Resources:lblOverrideCriteria %>"/></span>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                        <%--Yukti-ML Changes Start
                            Email Notice--%>
                            <asp:Label runat="server" ID="lblEmailNotice" Text="<%$ Resources:lblEmailNotice %>" />
                            <%--Yukti-ML Changes End--%>
                            </td>
                        <td width="80%" align="left">
                        
                            <asp:TextBox ID="txtEmailNotice" runat="server" onchange="psoEmailLostFocus(this.id);" Width="204px" ></asp:TextBox>
                        
                        &nbsp;
                        
                        </td>
                    </tr>
                    
                    <tr>
                    <td align="left" width="20%">
                    <%--YUkti-ML Changes Start--%>
                            <%--<asp:CheckBox ID="chkTestExport" runat="server" Text="Test Export" />--%>
                            <asp:CheckBox ID="chkTestExport" runat="server" Text="<%$ Resources:chkTestExport %>" />
                    <%--Yukti-ML Changes End--%>

                   </td>
                   <tr>
                        <td class="style9">
                <%--<asp:Button ID="btnSaveExport" runat="server" Class="button" Text="Save"  OnClick="btnSave_Click" />--%>
                <asp:Button ID="btnSaveExport" runat="server" Class="button" Text="<%$ Resources:btnSaveExport %>"  OnClick="btnSave_Click" />
                            <%--<asp:Button ID="btnExportCancel" runat="server" Text="Cancel" 
                    class="button" OnClientClick="OnCancel();return false;" />--%>
                    <asp:Button ID="btnExportCancel" runat="server" Text="<%$ Resources:btnExportCancel %>" 
                    class="button" OnClientClick="OnCancel();return false;" />
                        </td>
                        <td class="style5">
                        
                            
                        
                            &nbsp;</td>
                    </tr>
                </table>
        <asp:HiddenField ID="hdTaskManagerXml" runat="server" />
    <asp:HiddenField ID="hdOptionsetId" runat="server" />
    
    </form>
</body>
</html>
