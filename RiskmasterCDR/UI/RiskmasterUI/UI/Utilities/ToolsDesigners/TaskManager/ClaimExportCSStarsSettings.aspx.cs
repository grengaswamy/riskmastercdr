﻿//******************************************************************************
//Developer: Subhendu Sahoo
//Date: 7/29/2011
//Description: New Template added into RMX TaskManager for DI CLAIM EXPORT JOB.
//******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Riskmaster.Models;    //ipuri 12/07/2014

// Developer :- Subhendu | Module - DA CLAIM EXPORT | New Template integrated into RMX Taskmanager
namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class CSStarExport : NonFDMBasePageCWS
    {
        private const string m_sModuleName = "CLAIM_EXPORT_CSStars";
        private int m_iOptionSetID;
        private XElement XmlTemplate = null;
        private string sCWSresponse = "";
            
        protected void Page_Load(object sender, EventArgs e)
        {
            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes


            if (!IsPostBack)
            {
                //Extract Job schedule details from Taskmanager DB.
                hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));
                RetrieveSettings(0);                
            }
            else 
            {
                if (hdOptionset.Value == "" || hdOptionset.Value == null)
                {
                    hdOptionset.Value = "0";
                }
                m_iOptionSetID = Convert.ToInt32(hdOptionset.Value);
                if (OrgRelatedInfoGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = OrgRelatedInfoSelectedId.Text;
                    XmlTemplate = GetMessageTemplate(selectedRowId);
                    CallCWS("DataIntegratorAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                    OrgRelatedInfoGrid_RowDeletedFlag.Text = "false";
                }
                string strSelectedRowId = AppHelper.GetQueryStringValue("selectedid");
                XmlTemplate = GetMessageTemplate();
                CallCWSFunctionBind("DataIntegratorAdaptor.Get", out sCWSresponse , XmlTemplate);
            }            
            if (CommonFunctions.OptionsetId > 0)
            {
                txtOptionsetName.Enabled = false;
            }
            else
            {
                txtOptionsetName.Enabled = true;
            }
            ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('" + hTabName.Value + "')};", true);
        }
        //Function : Retrieve setting from DATA_INTEGRATOR_TEMPLATES Table
        protected void RetrieveSettings(int iOptionsetId)
        {
            //DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            DataIntegratorModel objDIModel = null;
            BusinessAdaptorErrors err = null;

            
            
            try
            {
                objDIModel = new DataIntegratorModel();
                err = new BusinessAdaptorErrors();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }

                m_iOptionSetID = iOptionsetId == 0? CommonFunctions.OptionsetId : iOptionsetId;
                hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                hdOptionset.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                objDIModel.OptionSetID = m_iOptionSetID;
                objDIModel.ModuleName = m_sModuleName;
                
                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.RetrieveSettings(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End

                if (objDIModel.OptionSetID != 0)
                {
                    txtOptionsetName.Enabled = false;
                }

                txtOptionsetName.Text = objDIModel.OptionSetName;

                Dictionary<string, string> ClaimExport_Parms = new Dictionary<string, string>();
                ClaimExport_Parms = objDIModel.Parms;

                //Retrieve - Claim Office & TPA Identification Code.
                txtclaimoffice.Text = ClaimExport_Parms["Claim_Office"];
                txtidentificationcode.Text = ClaimExport_Parms["TPA_Identification_Code"];

                //Retrieve - Claim Export format
                //ddlExtractFormat.SelectedValue = ClaimExport_Parms["Export_Format"];

                //Retrieve - Multiple Claim Numbers
                /*
                if (ClaimExport_Parms["Use_Selected_Claim_No"] == "1")
                {
                    chkboxmultipleclaimno.Checked = true;
                    fuuploadclaimdata.Enabled = true;                
                }
                else
                {
                    chkboxmultipleclaimno.Checked = false;
                    fuuploadclaimdata.Enabled = false;
                }*/
                
                //Retrieve - Financial Date Range
                string sDate = "";
                if (m_iOptionSetID == 0)
                {
                    string sCurMonth = hdTaskManagerXml.Value.Substring(hdTaskManagerXml.Value.IndexOf("&lt;Date&gt;") + 12,10).ToString();
                    string sPrevMonth = "";
                    string sYear = sCurMonth.Substring(6, 4);
                    if ((Convert.ToInt32(sCurMonth.Substring(0, 2)) - 1).ToString() == "0")
                    {
                        sPrevMonth = "12";
                        sYear = (Convert.ToInt32(sYear) - 1).ToString();
                    }
                    else
                    {
                        sPrevMonth = (Convert.ToInt32(sCurMonth.Substring(0, 2)) - 1).ToString();
                    }
                    txtFinFromDate1.Value = sPrevMonth.PadLeft(2,'0') + "/" + "01" + "/" + sYear;

                    switch (sPrevMonth)
                    {
                        case "1":
                        case "3":
                        case "5":
                        case "7":
                        case "8":
                        case "10":
                        case "12":
                            txtFinToDate1.Value = sPrevMonth.PadLeft(2, '0') + "/" + "31" + "/" + sYear;
                            break;
                        case "4":
                        case "6":
                        case "9":
                        case "11":
                            txtFinToDate1.Value = sPrevMonth.PadLeft(2, '0') + "/" + "30" + "/" + sYear;
                            break;
                        case "2":
                            if (Convert.ToInt32(sYear) % 4 == 0)
                            {
                                txtFinToDate1.Value = sPrevMonth.PadLeft(2, '0') + "/" + "29" + "/" + sYear;
                            }
                            else
                            {
                                txtFinToDate1.Value = sPrevMonth.PadLeft(2, '0') + "/" + "28" + "/" + sYear;
                            }
                            break;
                    }                    
                }
                else
                {
                    if (ClaimExport_Parms["Financial_Date_From"] != "")
                    {
                        sDate = ClaimExport_Parms["Financial_Date_From"];
                        txtFinFromDate1.Value = sDate.Substring(4, 2) + "/" + sDate.Substring(6, 2) + "/" + sDate.Substring(0, 4);
                    }

                    if (ClaimExport_Parms["Financial_Date_To"] != "")
                    {
                        sDate = ClaimExport_Parms["Financial_Date_To"];
                        txtFinToDate1.Value = sDate.Substring(4, 2) + "/" + sDate.Substring(6, 2) + "/" + sDate.Substring(0, 4);
                    }
                }
                                
                //Retrieve - LOB, Claim type code and Reserve code
                    //Bind Claim type code
                    lbclaimtypecode_GL.DataSource = objDIModel.dsClaimTypeGC;
                    lbclaimtypecode_GL.DataTextField = objDIModel.dsClaimTypeGC.Tables[0].Columns[0].ToString();
                    lbclaimtypecode_GL.DataValueField = objDIModel.dsClaimTypeGC.Tables[0].Columns[1].ToString();
                    lbclaimtypecode_GL.DataBind();
                    lbclaimtypecode_GL.Items.Insert(0, "All Sub Claim Types");
                    //Bind Reserve type code
                    lbreservetype_GL.DataSource = objDIModel.dsReserveTypes;
                    lbreservetype_GL.DataTextField = objDIModel.dsReserveTypes.Tables[0].Columns[1].ToString();
                    lbreservetype_GL.DataValueField = objDIModel.dsReserveTypes.Tables[0].Columns[0].ToString();
                    lbreservetype_GL.DataBind();
                    lbreservetype_GL.Items.Insert(0, "All Reserve Types");
                    
                if (ClaimExport_Parms["Line_Of_Business_GL"] == "1")
                {
                    chkboxlob_GL.Checked = true;
                    lbclaimtypecode_GL.Enabled = true;
                    lbreservetype_GL.Enabled = true;

                    if (objDIModel.sClaimTypeGC != "" && objDIModel.OptionSetID > 0)
                    {
                        string[] sClaimTypeGC = objDIModel.sClaimTypeGC.Split(',');

                        foreach (string sClaimTypeCode in sClaimTypeGC)
                        {
                            int iCount = 0;

                            for (iCount = 0; iCount < lbclaimtypecode_GL.Items.Count; iCount++)
                            {
                                if (sClaimTypeCode.Contains(lbclaimtypecode_GL.Items[iCount].Value))
                                {
                                    lbclaimtypecode_GL.Items[iCount].Selected = true;
                                }
                            }                            
                        }
                        if (lbclaimtypecode_GL.Items[0].Selected)
                        {
                            lbclaimtypecode_GL.Items[0].Selected = false;
                        }
                    }

                    if (objDIModel.sReserveTypeGC != "" && objDIModel.OptionSetID > 0)
                    {
                        string[] sReserveTypeGC = objDIModel.sReserveTypeGC.Split(',');

                        foreach (string sReserveType in sReserveTypeGC)
                        {
                            int iCount = 0;

                            for (iCount = 0; iCount < lbreservetype_GL.Items.Count; iCount++)
                            {
                                if (sReserveType.Contains(lbreservetype_GL.Items[iCount].Value))
                                {
                                    lbreservetype_GL.Items[iCount].Selected = true;
                                }
                            }
                        }
                        if (lbreservetype_GL.Items[0].Selected)
                        {
                            lbreservetype_GL.Items[0].Selected = false;
                        }
                    }
                }
                else
                {
                    chkboxlob_GL.Checked = false;
                    lbclaimtypecode_GL.Enabled = false;
                    lbreservetype_GL.Enabled = false;
                }

                //Policy Supp - Temporary hidden-Start
                ////Retrieve - Policy Supplemental Field
                //ddlsuppfield.DataSource = objDIModel.dsPolicySupp;
                //ddlsuppfield.DataTextField = objDIModel.dsPolicySupp.Tables[0].Columns[0].ToString();
                //ddlsuppfield.DataValueField = objDIModel.dsPolicySupp.Tables[0].Columns[0].ToString();
                //ddlsuppfield.DataBind();
                //ddlsuppfield.Items.Insert(0, "Select");
                //if (ClaimExport_Parms["Use_Supplementals"] == "1")
                //{
                //    chkboxsuppfield.Checked = true;
                //    ddlsuppfield.Enabled = true;
                //    if (ClaimExport_Parms["Policy_Code"] != "")
                //    {
                //        ddlsuppfield.SelectedValue = ClaimExport_Parms["Policy_Code"];
                //    }
                //}
                //else
                //{
                //    chkboxsuppfield.Checked = false;
                //    ddlsuppfield.Enabled = false;
                //    ddlsuppfield.Items[0].Selected = true;
                //}
                //Policy Supp - Temporary hidden-End
                

                //if (ClaimExport_Parms["Policy_Code"] != "")
                //{
                //    ddlsuppfield.SelectedValue = ClaimExport_Parms["Policy_Code"];
                //}
                //else
                //{
                //    ddlsuppfield.Items[0].Selected = true;
                //}

                //Retrieve - Email Addresses
                txtemailforerror.Text = ClaimExport_Parms["Error_Log_Emails"];
                txtemailforprocess.Text = ClaimExport_Parms["Process_Log_Emails"];

                // Retrieve Org Hie + Date OF Claim + Active Org Details
                //XmlDocument XmlDoc = new XmlDocument();
                try
                {        
                        XmlTemplate = GetMessageTemplate();
                        CallCWSFunctionBind("DataIntegratorAdaptor.Get", out sCWSresponse, XmlTemplate);                    
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
                //end
            }
            catch (Exception e)
            {
                err.Add("DataIntegrator.Error", e.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void chkboxlob_GL_CheckedChanged(object sender, EventArgs e)
        {
            if (chkboxlob_GL.Checked == true)
            {
                lbclaimtypecode_GL.Enabled = true;
                lbreservetype_GL.Enabled = true;
                lbclaimtypecode_GL.Items[0].Selected = true;
                lbreservetype_GL.Items[0].Selected = true;
                int i;
                for (i = 1; i < lbclaimtypecode_GL.Items.Count - 1; i++)
                {
                    lbclaimtypecode_GL.Items[i].Selected = false;
                }
                for (i = 1; i < lbreservetype_GL.Items.Count - 1; i++)
                {
                    lbreservetype_GL.Items[i].Selected = false;
                }                
            }
            else
            {
                lbclaimtypecode_GL.Enabled = false;
                lbreservetype_GL.Enabled = false;
            }
        }

        protected void OK_Click(object sender, EventArgs e)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            try
            {
                SaveSettings();
            }
            catch (Exception ex)
            {
                err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //Function : Save setting into DATA_INTEGRATOR Table
        public void SaveSettings()
        {
            string sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
            m_iOptionSetID = Conversion.ConvertObjToInt(hdOptionsetId.Value);
            //m_iOptionSetID = Conversion.ConvertObjToInt(hdOptionset.Value);

            //DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            DataIntegratorModel objDIModel = new DataIntegratorModel();

            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            bool bErr = false;
            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }

            string sText ="";
            //Validations Start
            if ((m_iOptionSetID == 0) && (txtOptionsetName.Text == ""))
            {
                err.Add("DataIntagrator.Error", "Optionset Name is blank.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }
            sText = txtclaimoffice.Text.Trim();
            if (sText.Length == 0)
            {
                err.Add("DataIntagrator.Error", "Please enter CLAIM OFFICE Code.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }
            sText = "";
            sText = txtidentificationcode.Text.Trim();
            if (sText.Length == 0)
            {
                err.Add("DataIntagrator.Error", "Please enter TPA IDENTIFICATION Code.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }
            if (txtFinFromDate1.Value == "" || txtFinToDate1.Value == "")
            {
                err.Add("DataIntagrator.Error", "Please enter Financial Date Range.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }
            //Policy Supp - Temporary hidden-Start
            //if (chkboxsuppfield.Checked == true && ddlsuppfield.Items[0].Selected == true)
            //{
            //    err.Add("DataIntagrator.Error", "Please select a field from Policy_Supp.", BusinessAdaptorErrorType.Message);
            //    bErr = true;
            //}
            //Policy Supp - Temporary hidden-End
            //if (chkboxlob_GL.Checked == true && (lbclaimtypecode_GL.SelectedIndex == -1 || lbreservetype_GL.SelectedIndex == -1 ) )
            //{
            //    err.Add("DataIntagrator.Error", "Please select atleast one Claim type Code or Reserve type code.", BusinessAdaptorErrorType.Message);
            //    bErr = true;
            //}
            ////Condition - If OptionsetID already used.--Last Validation
            //if (objDIModel.OptionSetID == -1)
            //{
            //    err.Add("DataIntagrator.Error", "Optionset Name is already used.", BusinessAdaptorErrorType.Message);
            //    bErr = true;
            //}
            if (bErr == true)
            {
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
            int iOptionSetID;
            int iCount = 0;
            string sClaimtypecode = "";
            string sReservetypecode = "";
            string sDate = "";
            string sTemp = "";
            
            Dictionary<string, string> ClaimExport_Parms = new Dictionary<string, string>();

            //Save Setting - Claim office and Identification codes
            ClaimExport_Parms.Add("Claim_Office", txtclaimoffice.Text);
            ClaimExport_Parms.Add("TPA_Identification_Code", txtidentificationcode.Text);
            
            //Save Setting - Export format
            //ClaimExport_Parms.Add("Export_Format",ddlExtractFormat.SelectedItem.Text);
            ClaimExport_Parms.Add("Export_Format", "CS Stars");

            //Save Setting - Multiple claim Selection - Not Complete
            /*MITS 26773 bschroeder: code commented out.
            if (chkboxmultipleclaimno.Checked == true)
            {
                ClaimExport_Parms.Add("Use_Selected_Claim_No", "1");            
            }
            else
            { */
                ClaimExport_Parms.Add("Use_Selected_Claim_No", "0");
            //  END MITS 26773}

            //Save Setting - Financial date Range
            if (txtFinFromDate1.Value.Length == 10 && txtFinToDate1.Value.Length == 10)
            {
                sDate = "";
                sDate = txtFinFromDate1.Value.Substring(6, 4) + txtFinFromDate1.Value.Substring(0, 2) + txtFinFromDate1.Value.Substring(3, 2);
                ClaimExport_Parms.Add("Financial_Date_From", sDate);
                sDate = "";
                sDate = txtFinToDate1.Value.Substring(6, 4) + txtFinToDate1.Value.Substring(0, 2) + txtFinToDate1.Value.Substring(3, 2);
                ClaimExport_Parms.Add("Financial_Date_To", sDate);
            }
            else 
            {
                ClaimExport_Parms.Add("Financial_Date_From", "");
                ClaimExport_Parms.Add("Financial_Date_To", "");
            }
            
            //Save Setting - LOB, Claim Type codes and Reserve Codes
            if (chkboxlob_GL.Checked == true)
            {
                ClaimExport_Parms.Add("Line_Of_Business_GL", "1");
                for (iCount = 0; iCount < lbclaimtypecode_GL.Items.Count; iCount++)
                {
                    if (lbclaimtypecode_GL.Items[0].Selected == true)
                    {
                        if (iCount > 0)
                        {
                            sClaimtypecode = sClaimtypecode + lbclaimtypecode_GL.Items[iCount].Value.ToString() + ",";
                        }
                    }
                    else if (lbclaimtypecode_GL.Items[iCount].Selected == true)
                    {
                        sClaimtypecode = sClaimtypecode + lbclaimtypecode_GL.Items[iCount].Value.ToString() + ",";
                    }
                }
                if (sClaimtypecode != "")
                {
                    sClaimtypecode = sClaimtypecode.Substring(0, sClaimtypecode.Length - 1);
                }                
                else
                {
                    err.Add("DataIntagrator.Error", "Please select Claim Types for General Claims LOB.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                ClaimExport_Parms.Add("CLAIM_TYPE_CODE_GL",sClaimtypecode);

                for ((iCount) = 0; iCount < lbreservetype_GL.Items.Count; iCount++)
                {
                    if (lbreservetype_GL.Items[0].Selected == true)
                    {
                        if (iCount > 0)
                        {
                            sReservetypecode = sReservetypecode + lbreservetype_GL.Items[iCount].Value.ToString() + ",";
                        }
                    }
                    else if (lbreservetype_GL.Items[iCount].Selected == true)
                    {
                        sReservetypecode = sReservetypecode + lbreservetype_GL.Items[iCount].Value.ToString() + ",";
                    }
                }                
                if (sReservetypecode != "")
                {
                    sReservetypecode = sReservetypecode.Substring(0, sReservetypecode.Length - 1);
                }                
                else
                {
                    err.Add("DataIntagrator.Error", "Please select Reserve Types for General Claims LOB.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                ClaimExport_Parms.Add("RESERVE_CODE_GL",sReservetypecode);
            }
            else
            {
                ClaimExport_Parms.Add("Line_Of_Business_GL", "0");
                ClaimExport_Parms.Add("CLAIM_TYPE_CODE_GL",sClaimtypecode);
                ClaimExport_Parms.Add("RESERVE_CODE_GL",sReservetypecode);
            }
            //Policy Supp - Temporary hidden-Start
            ////Save Setting - Policy Supplemental Field
            //if (chkboxsuppfield.Checked == true)
            //{
            //    ClaimExport_Parms.Add("Use_Supplementals", "1");
            //    ClaimExport_Parms.Add("Policy_Code", ddlsuppfield.SelectedItem.Text);
            //}
            //else
            //{
            //    ClaimExport_Parms.Add("Use_Supplementals", "0");
            //    ClaimExport_Parms.Add("Policy_Code", "");
            //}
            //Policy Supp - Temporary hidden-End
            
            
            //Save Setting - Email Addresses
            ClaimExport_Parms.Add("Error_Log_Emails", txtemailforerror.Text);
            ClaimExport_Parms.Add("Process_Log_Emails", txtemailforprocess.Text);
            //Save Setting - Inactive Flag for Org Hierarchy.
            //ClaimExport_Parms.Add("InactiveOrgHie", hdOrgInactive.Value.TrimEnd(';'));

            objDIModel.OptionSetName = txtOptionsetName.Text;
            objDIModel.ModuleName = m_sModuleName;
            objDIModel.OptionSetID = m_iOptionSetID;
            objDIModel.TaskManagerXml = sTaskManagerXml;

            objDIModel.Parms = ClaimExport_Parms;
            
            //ipuri REST Service Conversion 12/06/2014 Start
            //objDIModel = objDIService.SaveSettings(objDIModel);
            objDIModel.ClientId = AppHelper.ClientId;

            objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
            //ipuri REST Service Conversion 12/06/2014 End
            iOptionSetID = objDIModel.OptionSetID;

            //Condition - If OptionsetID already used.--Last Validation
            if (objDIModel.OptionSetID == -1)
            {                
                err.Add("DataIntagrator.Error", "Optionset Name is already used.", BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }

            //Validations End
            if (objDIModel.OptionSetID > 0)
            {
                Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(hdOptionset.Value) > 0)
            {
                int iOptionsetID = Convert.ToInt32(hdOptionset.Value);
                //DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
                
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document><ClaimExportOrgList>");
                sXml = sXml.Append("<control name='Optionset'>");
                sXml = sXml.Append(iOptionsetID);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("</ClaimExportOrgList></Document>");
                sXml = sXml.Append("</Message>");
                XElement oElement = XElement.Parse(sXml.ToString());

                CallCWS("DataIntegratorAdaptor.CleanDataGridonCancel", oElement, out sCWSresponse, false, false);
                //objDIService.CleanDataGridonCancel(iOptionsetID);
                               
            }
            Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx");
        }

        //Function Create XML Template for Retreiving Org Related Info
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<ClaimExportOrgList><listhead>");
            sXml = sXml.Append("<Active type='bool'>Active</Active>");
            sXml = sXml.Append("<OrgId>Org Hierarchy</OrgId>");
            sXml = sXml.Append("<FromDate type='date'>Claim From Date</FromDate>");
            sXml = sXml.Append("<ToDate type='date'>Claim To Date</ToDate>");
            sXml = sXml.Append("<RowId>RowId</RowId>");
            if (m_iOptionSetID == 0)
            {
                sXml = sXml.Append("<OptionSet>0</OptionSet>");                
            }
            else 
            {
                sXml = sXml.Append("<OptionSet>" + m_iOptionSetID + "</OptionSet>");                
            }
            

            sXml = sXml.Append("</listhead></ClaimExportOrgList>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        //Function Create XML Template for Deleting a selected Org Related Info
        private XElement GetMessageTemplate(string selectedRowId)
        {
            string strOptionset = selectedRowId.Substring(0, selectedRowId.IndexOf("|"));
            int i= selectedRowId.Split('|').Length - 1;
            string[] arrtmp = selectedRowId.Split('|');
            string strOrgId = arrtmp[i];
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ClaimExportOrgList>");
            sXml = sXml.Append("<control name='Optionset'>");
            sXml = sXml.Append(strOptionset);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Org Hierarchy'>");
            sXml = sXml.Append(strOrgId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</ClaimExportOrgList></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }        
    }
}