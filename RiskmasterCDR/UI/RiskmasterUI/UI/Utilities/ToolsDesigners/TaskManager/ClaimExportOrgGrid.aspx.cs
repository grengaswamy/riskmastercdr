﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.Utilities.ToolsDesigners.TaskManager
{
    //public partial class ClaimExportOrgGrid : System.Web.UI.Page
    public partial class ClaimExportOrgGrid : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes
            try
            {
                mode.Text = AppHelper.GetQueryStringValue("mode");
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    //mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    //ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'> fTakeOptionsetIDfromParentPage();</script>");
                }

                else
                {
                    
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("DataIntegratorAdaptor.GetCEOrg", XmlTemplate, out sCWSresponse, false, true);
                    }
                }
            }
            catch (Exception ee)
            {

                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {

            try
            {

                //validation-start
                //BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                //bool bErr = false;

                //string sTmp = "";
                //if (!(string.IsNullOrEmpty(Org_orglist.Text)))
                //    sTmp = Org_orglist.Text;
                //if (!(string.IsNullOrEmpty(OrgId.Text)))
                //    sTmp = OrgId.Text;

                ////if (!(string.IsNullOrEmpty(sTmp)))
                //if( sTmp.Length == 0 || sTmp == "" || sTmp == null)
                //{
                //    err.Add("DataIntagrator.Error", "Provide single Org Hierarchy", BusinessAdaptorErrorType.Message);
                //    bErr = true;
                //}
                //if (!(string.IsNullOrEmpty(FromDate.Text)))
                //{
                //    err.Add("DataIntagrator.Error", "Provide Claim From Date", BusinessAdaptorErrorType.Message);
                //    bErr = true;
                //}
                //if (!(string.IsNullOrEmpty(ToDate.Text)))
                //{
                //    err.Add("DataIntagrator.Error", "Provide Claim To Date", BusinessAdaptorErrorType.Message);
                //    bErr = true;
                //}
                //if (bErr == true)
                //{
                //    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                //    return;
                //}
                //validation-end
                //mode.Text = "add";
                XmlTemplate = GetMessageTemplate();
                CallCWS("DataIntegratorAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                string sOptionset = objReturnXml.SelectSingleNode("//Document/ClaimExportOrgList").LastChild.InnerText;
                hdOptionsetpopup.Value = objReturnXml.SelectSingleNode("//Document/ClaimExportOrgList").LastChild.InnerText;
                if (sMsgStatus == "Success")
                {
                    //ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'> fPassOptionsetIDtoParentPage();</script>");
                    //ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "<script type='text/javascript'> var hdOptionset = document.getElementById("hdOptionset"); hdOptionset.Value = sOptionset ;</script>");
                    //this.Page.ClientScript.RegisterStartupScript(typeof(string), "ClaimExportSettings", "window.opener.document.getElementById('hdOptionset').Value = " + sOptionset +";", true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplate()
        {
            string strClaimToDate = string.Empty;
            string strClaimFromDate = string.Empty;
            string strOrgId = string.Empty;
            
            int iOptionsetID = 0;
            if (mode.Text.ToLower() == "edit")
            {
                {
                    Org.Enabled = false;
                    Orgbtn.Enabled = false;
                }

                string strSelectedRowId = AppHelper.GetQueryStringValue("selectedid");

                iOptionsetID = Convert.ToInt32(strSelectedRowId.Substring(0, strSelectedRowId.IndexOf("|")));
                strSelectedRowId = strSelectedRowId.Substring(strSelectedRowId.IndexOf("|") + 1);

                if (!string.IsNullOrEmpty(FromDate.Text))
                {
                    strClaimFromDate = FromDate.Text;
                }
                else
                {
                    strClaimFromDate = strSelectedRowId.Substring(0, strSelectedRowId.IndexOf("|"));
                    //strSelectedRowId = strSelectedRowId.Substring(9);
                    strSelectedRowId = strSelectedRowId.Substring(strSelectedRowId.IndexOf("|") + 1);
                }

                if (!string.IsNullOrEmpty(ToDate.Text))
                {
                    strClaimToDate = ToDate.Text;
                }
                else
                {
                    strClaimToDate = strSelectedRowId.Substring(0, strSelectedRowId.IndexOf("|"));
                    //strSelectedRowId = strSelectedRowId.Substring(9);
                    strSelectedRowId = strSelectedRowId.Substring(strSelectedRowId.IndexOf("|") + 1);
                }

                int iCount = strSelectedRowId.Split('|').Length - 1;
                if (iCount != 1)
                {
                    Org_ceorglist.Text = OrgId.Text;
                    OrgId.Text = "";
                }
                else
                {
                    if (strSelectedRowId.Substring(0, strSelectedRowId.IndexOf("|")) == "YES")
                        cbOrgActive.Checked = true;
                    else
                        cbOrgActive.Checked = false;

                    strOrgId = strSelectedRowId.Substring(strSelectedRowId.IndexOf("|") + 1);
                    OrgId.Text = strOrgId; //current OrgId
                    Org.Text = Org.Text.Replace("&amp;", "&").Replace("&apos;", "'");
                }
            }
            else
            {
                //ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javacript'> fTakeOptionsetIDfromParentPage();</script>");
                if (AppHelper.GetQueryStringValue("hdOptionsetpopup") == "")
                {
                    iOptionsetID = 0;
                }
                else
                {
                    iOptionsetID = Convert.ToInt32(AppHelper.GetQueryStringValue("hdOptionsetpopup"));
                }
                if (!(string.IsNullOrEmpty(FromDate.Text)))
                {
                    strClaimFromDate = FromDate.Text;
                }
                else
                { 
                    strClaimFromDate = "19000101";
                }
                if (!(string.IsNullOrEmpty(ToDate.Text)))
                {
                    strClaimToDate = ToDate.Text;
                }
                else
                {
                    string sYear = DateTime.Now.Year.ToString();
                    string sMonth = (DateTime.Now.Month - 1).ToString();
                    string sDate = string.Empty;

                    switch (sMonth)
                    {
                        case "1":
                        case "3":
                        case "5":
                        case "7":
                        case "8":
                        case "10":
                        case "12":
                            sDate = "31";
                            break;
                        case "4":
                        case "6":
                        case "9":
                        case "11":
                            sDate = "30";
                            break;
                        case "2":
                            if (Convert.ToInt32(sYear) % 4 == 0)
                                sDate = "29";
                            else
                                sDate = "28";
                            break;
                    }
                    if (Convert.ToInt32(sMonth) == 0)
                    {
                        sMonth = "12";
                        sYear = (Convert.ToInt32(sYear) - 1).ToString();
                    }
                    strClaimToDate = sYear + sMonth.PadLeft(2, '0') + sDate;
                }
                Org.Text = Org.Text.Replace("&", "&amp;").Replace("'", "&apos;");
                }

            
            //if (strOrgId == "")
            //{
            //    strOrgId = Org_orglist.Text;
            //}

            Org.Text = XmlConvert.EncodeLocalName(Org.Text);

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ClaimExportOrgList>");

            sXml = sXml.Append("<control name='Active' type='bool'>");
            sXml = sXml.Append(cbOrgActive.Checked);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='Org Hierarchy' currentid='" + OrgId.Text + "' orgid='" + strOrgId + "' type='orglist'>");
            //sXml = sXml.Append(Org.Text);
            //sXml = sXml.Append(Server.HtmlEncode(Org.Text));
            sXml = sXml.Append((Org.Text));
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Org_orglist' type='hidden'>");
            //Subhendu
            sXml = sXml.Append(Org_ceorglist.Text);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='Claim From Date' type='date'>");
            sXml = sXml.Append(strClaimFromDate);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='Claim To Date' type='date'>");
            sXml = sXml.Append(strClaimToDate);
            sXml = sXml.Append("</control>");
            
            sXml = sXml.Append("<control name='RowId' type='id'>");
            sXml = sXml.Append(RowId.Text);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='OptionSet'>");
            sXml = sXml.Append(iOptionsetID);
            //sXml = sXml.Append("<OptionSet>0</OptionSet>");
            sXml = sXml.Append("</control>");
                        
            sXml = sXml.Append("</ClaimExportOrgList></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //btnOk_Click(sender, e);
            //ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.close();</script>");
        }
    }
}