﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessHelpers;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.ServiceHelpers;
//Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
using Riskmaster.UI.DataIntegratorService;        //ipuri 12/06/2014 SOA Service discontinued
using System.IO;
using Riskmaster.Models;    //ipuri 12/06/2014


namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{

    public partial class DIPositivePay : System.Web.UI.Page
    {
        private int OptionSetID; // this will be pased in from task manager.

        private const string ModuleName = "POSITIVE_PAY";


        // is used to see if the option set name is all ready used in the data base 




        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (hdnaction.Value == "SaveOptionset")
            {
                Saveoptionset();
                hdnaction.Value = "";
                
            }

            if (!IsPostBack)
            {
                //OptionSetID = 4;// this come from task manager just used to test right now 
                //Vsoni5 : MITS 22565 : Exception thrown by Service layer will be displayed on UI instead of hard coded error message                
                BusinessAdaptorErrors objAdaptorErrors = null;
                
                    //hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(PreviousPage));
                    hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));

                    OptionSetID = CommonFunctions.OptionsetId;

                    hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //Riskmaster.UI.DataIntegratorService.DataIntegratorModel Model = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                    Riskmaster.Models.PPAccountList AccountList = new Riskmaster.Models.PPAccountList();
                    //DataIntegratorService.DataIntegratorServiceClient GetSetting = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
                    Riskmaster.Models.DataIntegratorModel Model = new Riskmaster.Models.DataIntegratorModel();
                    //ipuri REST Service Conversion 12/06/2014 End
                    objAdaptorErrors = new BusinessAdaptorErrors();
                    if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                    {
                        AccountList.Token = AppHelper.GetSessionId();
                    }

                    AccountList.AccountID = new List<string>();
                    AccountList.AccountNameNumber = new List<string>();

                    //gets the account information from the database and puts it in to the acount drop down box
                    //AccountList = GetSetting.GetAccountList(AccountList);
                    //ipuri REST Service Conversion 12/06/2014 Start
                    AccountList.ClientId = AppHelper.ClientId;
                    AccountList = AppHelper.GetResponse<Riskmaster.Models.PPAccountList>("RMService/DAIntegration/getaccountlist", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, AccountList);
                    //ipuri REST Service Conversion 12/06/2014 End
                    PopulateAccountDropDown(AccountList);

                    if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                    {
                        Model.Token = AppHelper.GetSessionId();
                    }//if

                    Model.ModuleName = ModuleName; //sets the moduleName 
                    Model.OptionSetID = OptionSetID; //sets optsion set id

                try
                {
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //Model = GetSetting.RetrieveSettings(Model);// get the seeting for the UI from the data base
                    Model.ClientId = AppHelper.ClientId;

                    Model = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, Model);
					//ipuri REST Service Conversion 12/06/2014 End
                    
                }
                catch (Exception exp)
                {
                //Vsoni5 : MITS 22565 : Exception thrown by Service Layer will be shown on UI.
                   objAdaptorErrors.Add("DataIntegrator.Error", exp.Message, BusinessAdaptorErrorType.Message);
                   ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(objAdaptorErrors);
                }
                if (Model.Parms != null && Model.Parms.Count != 0)
                    {
                        SettingUIfeilds(Model);// sets the fields in the UI
                    }
                    else
                    {
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('Export')};", true);
                    }
                    if (OptionSetID == 0)
                    {
                        txtExportOptionSetName.Enabled = true;
                        txtImportOptionsetName.Enabled = true;

                    }
                    else
                    {
                        txtExportOptionSetName.Enabled = false;
                        txtImportOptionsetName.Enabled = false;

                    }                
            }
            // makes sure that the tab selected is the one being displyaed on the screen.
            ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('" + hTabName.Value + "')};", true);
        }


        private void PopulateAccountDropDown(Riskmaster.Models.PPAccountList AccountList)
        { // add the a listItem for each item in accountlist to the bank account drop down box.
            for (int i = 0; i < AccountList.AccountID.Count; i++)
            {
                ddlBankAccount.Items.Add(new ListItem(AccountList.AccountNameNumber[i], AccountList.AccountID[i]));
            }

        }

        protected void ddlBankFormat_OnSelectedIndexChanged(object sender, EventArgs e)
        {// this make the lsbAccountName list box, the cmdAdd button and cmdDelete button visable or on visable depending on what bank format you selected.
            if (ddlBankFormat.Text == "" || ddlBankFormat.Text == "Corporate Claim" || ddlBankFormat.Text == "Wachovia" || ddlBankFormat.Text == "Northern Trust")
            {
                cmdAdd.Visible = false;
                lsbAccountName.Visible = false;
               btnDelete.Visible = false;
            }
            else
            {
                cmdAdd.Visible = true;
                lsbAccountName.Visible = true;
                btnDelete.Visible = true;

            }
        }

        private void SettingUIfeilds(Riskmaster.Models.DataIntegratorModel Model)
        {//set the fields in the UI based on what was in the data base.

            string sTmp = string.Empty;
            string ListItemValue = string.Empty;
            string sToDate = string.Empty;
            string sFromDate = string.Empty;
            string sManuallyChangeDate = string.Empty;
            
            //txtExportOptionSetName.Text = Model.OptionSetName;
            //txtImportOptionsetName.Text = Model.OptionSetName;
            if (Model.OptionSetID != 0)
            {
                txtImportOptionsetName.Enabled = false;
                txtExportOptionSetName.Enabled = false;
            }
            if (Model.Parms["Positive_Pay_Type"] == "EXPORT")
            {//if the type of positive pay is expot
                txtExportOptionSetName.Text = Model.OptionSetName;
                List<string> AccountList = new List<string>();
               
                ddlBankFormat.SelectedItem.Value = Model.Parms["BankFormat"];
                txtTargetFileName.Text = Model.Parms["Target_File"];
                sTmp = Model.Parms["Account_ID"];
                //convertes the account string to a list
                sTmp = sTmp.Replace("'", "");
                AccountList = GetAccountString(sTmp);
                sFromDate = Model.Parms["From_Date"];
                    fromcheckdate.Text = Conversion.GetDBDateFormat(sFromDate, "d");
                sToDate = Model.Parms["To_Date"];
                    tocheckdate.Text = Conversion.GetDBDateFormat(sToDate, "d");
                    sManuallyChangeDate = Model.Parms["ManuallyChangeDateRange"];
                    if (sManuallyChangeDate == "1")
                    {
                        ckbManually.Checked = true;
                    }
                    else
                    {
                        ckbManually.Checked = false;
                    }

                if (ddlBankFormat.Text == "" || ddlBankFormat.Text == "Corporate Claim" || ddlBankFormat.Text == "Wachovia" || ddlBankFormat.Text == "Northern Trust")
                {// the bank format only allows one account then cmdAdd, lsbaccountname and cmddelete are set tot false visible so you dont see them 
                    // and in the account list there will only be one vules so you get the accountlist indes 0 
                    ddlBankAccount.SelectedValue = AccountList[0];
                    cmdAdd.Visible = false;
                    lsbAccountName.Visible = false;
                    btnDelete.Visible = false;

                }
                else
                {// when bank format allows more then one account
                    cmdAdd.Visible = true;
                    lsbAccountName.Visible = true;
                    btnDelete.Visible = true;
                    foreach (string AccountID in AccountList)
                    {// takes all the items in account list and finds that vaule in ddlBankAccount then add a listItem to lsbAccountName

                        ListItemValue = ddlBankAccount.Items.FindByValue(AccountID).Text;
                        lsbAccountName.Items.Add(new ListItem(ListItemValue, AccountID));
                    }
                }
                // makes sure the right tabe is displayed screen
                ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('Export')};", true);
             }
            else
            {//if the type of positive pay is impotes 
                //if the 
                txtImportOptionsetName.Text = Model.OptionSetName;
                ddlFileFormat.SelectedValue = Model.Parms["AccountLen"];
                // makes sure the right tabe is diplayed on the screen 
                ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('Import')};", true);
            }



            // the to and from date are here for use later on

        }


        private List<string> GetAccountString(string AccountString)
        {//makes a list of all the number in the string ther is a , between each number
            List<string> AccountList = new List<string>();

            foreach (string sTmp in AccountString.Split(','))
            {
                AccountList.Add(sTmp);
            }
            return AccountList;
        }

        protected void Saveoptionset()
        {
            string sPositivePayType = string.Empty;
            string sBankFormat = string.Empty;
            string sFileName = string.Empty;
            string sAccountIds = string.Empty;
            string sAccountLen = string.Empty;
            string sToDate = string.Empty;
            string sFromDate = string.Empty;
            string sManuallyChangeDate = string.Empty;
            bool bdateFlag = true;
            bool bsaveflag = true;
            string sDateErrormessage = string.Empty;
            int iOptionSetID;
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            /// Vsoni5 : 01/29/2011 : MITS 23439
            /// //ipuri REST Service Conversion 12/06/2014 Start
            //kkaur25-SOA start
            Riskmaster.UI.DataIntegratorService.DataIntegratorModel ModelS = null; 
            Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient SaveSetting = null;
            //kkaur25-SOA end
            Riskmaster.Models.DataIntegratorModel Model = null;
            //ipuri REST Service Conversion 12/06/2014 End
            try
            {   //KKAUR25-SOA CHANGE START
                ModelS = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                SaveSetting = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
                //KKAUR25-SOA CHANGE END
                Model = new Riskmaster.Models.DataIntegratorModel();

                string sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);

                OptionSetID = Conversion.ConvertObjToInt(hdOptionsetId.Value);

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    Model.Token = AppHelper.GetSessionId();
                }

                Model.ModuleName = ModuleName;

                Model.OptionSetID = OptionSetID;

                Model.TaskManagerXml = sTaskManagerXml;
                // sets all the lables to false
                if (hTabName.Value == "Export")
                {//for exports 
                    sPositivePayType = "EXPORT";
                    Model.ImportFlage = false;
                    Model.OptionSetName = txtExportOptionSetName.Text;

                    sBankFormat = ddlBankFormat.SelectedItem.Text;

                    sFileName = txtTargetFileName.Text;

                    //format date string to yyyymmdd
                    sFromDate = Conversion.GetDate(fromcheckdate.Text);

                    sToDate = Conversion.GetDate(tocheckdate.Text);
                    if (ckbManually.Checked == true)
                    {
                        sManuallyChangeDate = "1";
                    }
                    else
                    {
                        sManuallyChangeDate = "0";
                    }
                    //checks the dates


                    // makes sure that a Account Name is selected and if one not selected the lable is made visable and the setting will not be saved to the database 
                    if (lsbAccountName.Items.Count == 0 || ddlBankFormat.Text == "Corporate Claim" || ddlBankFormat.Text == "Wachovia" || ddlBankFormat.Text == "Northern Trust")
                    {// if the bankformat only allows one account then the ddlbanaccount is cheacked for the account

                        if (ddlBankAccount.SelectedItem.Text != "")
                        {
                            sAccountIds = ddlBankAccount.SelectedItem.Value;
                        }
                    }
                    else
                    {
                        if (!checkforDoubleAccountID() && ddlBankAccount.SelectedItem.Text != "")
                        {
                            sAccountIds = ddlBankAccount.SelectedItem.Value;
                        }
                        foreach (ListItem sTmp in lsbAccountName.Items)
                        {// put the value of the itmes in lsbAccountname in to a string. the value are the account ID
                            if (sAccountIds == "")
                            {// for the 1st number in the string 
                                sAccountIds = sTmp.Value;

                            }
                            else
                            {// in between each value there is a '','' the resone for this is cause in the job a a "IN" command is used 
                                // and we want the vluse to have singel qualtes arount it like this 'value' and then spirated by a , 
                                // but in SQL you have to have the extra ' or it wont work.
                                sAccountIds = sAccountIds + "'',''" + sTmp.Value;
                            }

                        }

                    }

                    if (sAccountIds == "")
                    {
                        bsaveflag = false;
                    }
                    sAccountIds = "''" + sAccountIds + "''";
                }

                else
                {//for import
                    sPositivePayType = "IMPORT";

                    Model.ImportFlage = true;
                    Model.OptionSetName = txtImportOptionsetName.Text;
                    sAccountLen = ddlFileFormat.SelectedItem.Value;
                    sFileName = Filetoupload();
                }
                // set the vulse that will be saved database
                Model.Parms = new Dictionary<string, string>();
                Model.Parms.Add("Positive_Pay_Type", sPositivePayType);
                Model.Parms.Add("AccountLen", sAccountLen);
                Model.Parms.Add("BankFormat", sBankFormat);
                Model.Parms.Add("Account_ID", sAccountIds);
                Model.Parms.Add("ManuallyChangeDateRange", sManuallyChangeDate);
                // the to and from date are here for use later on
                Model.Parms.Add("From_Date", sFromDate);
                Model.Parms.Add("To_Date", sToDate);
                Model.Parms.Add("Target_File", sFileName);

                //ipuri REST Service Conversion 12/06/2014 Start
                //Model = SaveSetting.SaveSettings(Model);
                Model.ClientId = AppHelper.ClientId;
                Model = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, Model);
                //ipuri REST Service Conversion 12/06/2014 End


                if (Model.OptionSetID == -1)
                {//makse sure the optionset name is not all ready used
                    //BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add("DataIntagrator.Error", "Optionset name already exists.", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
                else if (Model.ImportFlage == true)
                {
                    //put were you want the file to be saved
                    sFileName = Model.Parms["Target_File"] + "." + Model.OptionSetID;
                    //fulFileToProcess.PostedFile.SaveAs(Model.FilePath + sFileName);

                    //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                    //kkaur25 soa change start
                    Riskmaster.Models.DAImportFile objDAImportFile = new Riskmaster.Models.DAImportFile();
                    Riskmaster.UI.DataIntegratorService.DAImportFile objDAImportFileS = new Riskmaster.UI.DataIntegratorService.DAImportFile();
                    //kkaur25 soa change send
                    objDAImportFile.Token = AppHelper.GetSessionId();
                    Stream data = fulFileToProcess.PostedFile.InputStream;
                    objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                    objDAImportFile.fileName = sFileName;
                    objDAImportFile.filePath = Model.FilePath;

                    //-----add sgupta320 9815
                    objDAImportFile.ModuleName = "POSITIVE_PAY";
                    objDAImportFile.DocumentType = "Import";
                    objDAImportFile.OptionsetId = Model.OptionSetID;
                    //-----end

                    //ipuri REST Service Conversion 12/06/2014 Start
                    //SaveSetting.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                    objDAImportFile.ClientId = AppHelper.ClientId;

                    //kkaur25-SOA change start
                    objDAImportFileS.FileContents = objDAImportFile.FileContents;
                    objDAImportFileS.fileName = sFileName;
                    objDAImportFileS.filePath = Model.FilePath;
                    objDAImportFileS.Token = AppHelper.Token;
                    objDAImportFileS.ClientId = AppHelper.ClientId;
                    objDAImportFileS.ModuleName = "POSITIVE_PAY";
                    objDAImportFileS.DocumentType = "Import";
                    objDAImportFileS.OptionsetId = Model.OptionSetID;
                    //Call SOAP/XML service
                    SaveSetting.UploadDAImportFile(objDAImportFileS);

                   

                    //AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                    //ipuri REST Service Conversion 12/06/2014 End
                    //Vsoni : End of MITS 22699.

                }



                if (Model.OptionSetID > 0)
                {
                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx",false);
                }
            }
            catch (Exception ex)
            {
                //npradeepshar 01/17/2010 MITS 23305 : Log the error into log file as well display it on UI
                err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                /// Vsoni5 : 01/29/2011 : MITS 23439
                try
                {
                    if (OptionSetID <= 0 && Model.OptionSetID > 0)
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //SaveSetting.SaveSettingsCleanup(Model);
                        Model.ClientId = AppHelper.ClientId;
                    AppHelper.GetResponse("RMService/DAIntegration/SaveSettingsCleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, Model);
                    //ipuri REST Service Conversion 12/06/2014 End
                        
                }
                catch (Exception eCleanup)
                {
                }

                return;
            }
        }

        protected void cmdAdd_onClick(object sender, EventArgs e)
        {// at the selected item in the drop down box to the list box and make sure there not doubles

            if (!checkforDoubleAccountID() && ddlBankAccount.Text != "")
            {
                lsbAccountName.Items.Add(new ListItem(ddlBankAccount.SelectedItem.Text, ddlBankAccount.SelectedItem.Value));
            }
        }

        private bool checkforDoubleAccountID()
        {
            foreach (ListItem List in lsbAccountName.Items)

                if (List.Value == ddlBankAccount.SelectedValue)
                {
                    return true;
                }

            return false;
        }

        private string Filetoupload()
        {//uploade the import file to the folder.
            string filepath = fulFileToProcess.PostedFile.FileName;
            string file = string.Empty;

            // npadhy : FileUpload.PostedFile.FileName contains full path of client in case of IE, but Chrome and Firefox consider it as a security breach and will only contain the file Name.
            if (filepath.Contains("\\"))
            {
                string pat = @"\\(?:.+)\\(.+)\.(.+)";
                Regex r = new Regex(pat);
                //run
                Match m = r.Match(filepath);
                string file_ext = m.Groups[2].Captures[0].ToString();
                string filename = m.Groups[1].Captures[0].ToString();
                file = filename + "." + file_ext;
            }
            else
            {
                file = filepath;
            }
             
            return file;

        }

        
        protected void btnDelete_Click(object sender, EventArgs e)
        {//deletes item from the list box.
            if (lsbAccountName.SelectedIndex != -1)
            {
                lsbAccountName.Items.Remove(new ListItem(lsbAccountName.SelectedItem.Text, lsbAccountName.SelectedItem.Value));
            }
        }



 

  
        
    }



}
