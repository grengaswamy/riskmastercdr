﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RM1099Preferences.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.RM1099Preferences" %>

<%@ Register Src="~/UI/Shared/Controls/MultiCodeLookup.ascx" TagName="MultiCode"
    TagPrefix="uc" %>
    
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../../Content/dhtml-div.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="javascript" src="../../../../Scripts/form.js" type="text/javascript" ></script>
    <script src="../../../../Scripts/form.js"type="text/javascript"></script>
       <%--vkumar258 - RMA-6037 - Starts --%>

     <%--<script src="../../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript">{var i;} </script>
    
    <script src="../../../../Scripts/zapatec/zpcal/src/calendar.js" type="text/javascript">{var i;}</script>

    <script src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js" type="text/javascript">{var i;}</script>--%>
<link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>
    <title>RM1099 Preferences</title>
    <style type="text/css">
        #tblFilters
        {
            border-color : Blue;
        }
        .tblfilters tbody tr td
        {
            width:255px;
        }
        
        .style1
        {
            width: 432px;
        }
        .style6
        {
        }
        /*        .style24
        {
            width: 100%;
        }*/
        .style25
        {
            width: 340px;
        }
        .style28
        {
            text-decoration: underline;
        }
    </style>

    <script language="javascript" type="text/javascript">       
    function browseFile()
    {
//        document.getElementById('FU_Payee').click();
    }
    function setFile()
    {
//        if (document.getElementById('FU_Payee').value != '')
//        {
//            document.getElementById('TxtPayeeFile').value =
//            document.getElementById('FU_Payee').value;
//        }
//        if (document.getElementById('FU_Payer').value != '')
//        {
//            document.getElementById('TxtPayerFile').value =
//            document.getElementById('FU_Payer').value;
//        }
//        if (document.getElementById('FU_Data').value != '')
//        {
//            document.getElementById('TxtDataFile').value =
//            document.getElementById('FU_Data').value;
//        }
//        if (document.getElementById('FU_Log').value != '')
//        {
//            document.getElementById('TxtLogFile').value =
//            document.getElementById('FU_Log').value;
//        }
    }
   
//Tanu : MITS 17006
function ExporPaymentsConfirm()
{
    var ret = false;
    if(document.getElementById('ChkList1_0').checked == true)//11/20/2009 Anand.
    {
	    ret = confirm('Please consult the users guide before choosing this option.  This option should only be used if absolutely necessary!.  Are You sure you wish to use this option?');
	}
	if(ret==true)
	{
	    document.getElementById('ChkList1_0').checked = true;
	}
	else
	{
	    document.getElementById('ChkList1_0').checked = false;
	}
	return ret;
}

//Vsoni5 MITS 25787 : Ignore Tax ID Errors
function IgnoreTaxIDConfirm()
{
   var ret = false;
    if(document.getElementById('ChkList2_1').checked == true)//11/20/2009 Anand.
    {
	    ret = confirm('Please consult the product support department before selecting this option.'  + 
                      ' This option should only be used if absolutely necessary!.  This will not export' +
                      ' payments to payees that do not have Tax IDs in RISKMASTER.  Click OK if you are sure you wish to use this option');
	}
	if(ret==true)
	{   
	    document.getElementById('ChkList2_1').checked = true;
	}
	else
	{
	    document.getElementById('ChkList2_1').checked = false;
	}
	return ret; 
}


//vgupta20 -- This validation check is not required.

//function ExportPayeesConfirm()
//{
//    var ret = false;
//	ret = confirm('Please consult the user guide before choosing this option.  This option should only be used if absolutely necessary!.  Are You sure you wish to use this option?');
//	if(ret==true)
//	{
//	    document.getElementById('ChkList2_2').checked = true;
//	}
//	else
//	{
//	    document.getElementById('ChkList2_2').checked = false;
//	}
//	return ret; 
//}
function ValidateEffectiveDateTrig()
 {  // Added by Anand : mits 17000
    //var sEffectiveDate = document.getElementById("TxtDateFrom").value; // it refers to From Date
    //var sExpirationDate = document.getElementById("TxtDateTo").value;  // it refers to To Date
    //var sEffdate = sEffectiveDate.substring(6, 10) + sEffectiveDate.substring(0, 2) + sEffectiveDate.substring(3, 5);
    //var sExpDate = sExpirationDate.substring(6, 10) + sExpirationDate.substring(0, 2) + sExpirationDate.substring(3, 5);
     var target = $("#TxtDateFrom");
    var sectarget = $("#TxtDateTo");
    var inst = $.datepicker._getInst(target[0]);
    var secinst = $.datepicker._getInst(sectarget[0]);

    fdate = inst.selectedDay;
    fmonth = inst.selectedMonth;
    fyear = inst.selectedYear;

    secdate = secinst.selectedDay;
    secmonth = secinst.selectedMonth;
    secyear = secinst.selectedYear;

    var sEffdate = new Date();
    sEffdate.setFullYear(fyear, fmonth, fdate);

    var sExpDate = new Date();
    sExpDate.setFullYear(secyear, secmonth, secdate);

    if (sectarget != "" && sExpDate < sEffdate) {
        alert("From date must be less than To date.");
        document.getElementById("JavaScriptcheck").value="Error";
       return false;
    }
    else{ //Vsoni5 : 4/06/2010 :MITS 17531
        document.getElementById("JavaScriptcheck").value="Corrected"
        return true;
    }    
    return true;   
}
function ExportSpecificReserveConfirm()
{
    //Hassan - 01/15/2010 - MITS 17006
    var flgChkList1_0 = document.getElementById('ChkList1_0').checked
    //alert('flgChkList1_0=' + flgChkList1_0);
    //Anand 11/20/2009: Prompt not required.
    var ret = false;
    if (flgChkList1_0 == true) {
        ret = confirm('This option may not produce the results you expect for payments that do not link to a claim.  Reserve Type may not be recorded in the database for payments that do not link to a claim.  Are you sure you wish to export by specific reserve types?');
        if (ret == true) {
            document.getElementById('OptSelect_2').checked = true;
        }
        else {
            document.getElementById('OptSelect_2').checked = false;
        }
    }
    return ret;
    
}

function DisableValidateTaxYear()
{
   // retrieve instance of our checkbox
   var chkDateRange = document.getElementById('ChkDtRange');
   // enable/disable all validators on page based on checkbox state
   //ValidatorsEnabled(!chkDateRange.checked);
}
//function ValidatorsEnabled(state)
//{
//   ValidatorEnable(document.getElementById('RequiredFieldValidator1'), state);
//}

//Hassan - 01/19/2010 - MITS 17486
function ValidateDateActivityLog() {
    //var sfromdate = document.forms[0].TxtDateFrom.value;
    //var stodate = document.forms[0].TxtDateTo.value;
    //var stxtFromDate = sfromdate.substring(6, 10) + sfromdate.substring(0, 2) + sfromdate.substring(3, 5);
    //var stxtToDate = stodate.substring(6, 10) + stodate.substring(0, 2) + stodate.substring(3, 5);

    var target = $("#TxtDateFrom");
    var sectarget = $("#TxtDateTo");
    var inst = $.datepicker._getInst(target[0]);
    var secinst = $.datepicker._getInst(sectarget[0]);
  
    fdate = inst.selectedDay;
    fmonth = inst.selectedMonth;
    fyear = inst.selectedYear;

    secdate = secinst.selectedDay;
    secmonth = secinst.selectedMonth;
    secyear = secinst.selectedYear;

    var stxtFromDate = new Date();
    stxtFromDate.setFullYear(fyear, fmonth, fdate);

    var stxtToDate = new Date();
    stxtToDate.setFullYear(secyear, secmonth, secdate);
    if (stxtToDate != "" && stxtFromDate > stxtToDate) {
        alert("From Date must be less than To Date");
//        document.forms[0].TxtDateFrom.value = "";
//        document.forms[0].TxtDateTo.value = "";
    }
}

//Hassan - 01/18/2010 - MITS 17531
// Year validation (year must be >= 1900)
function validateYearToImport(dateRangeYear) {
      //Vsoni5 - 0325/2010 - MITS 17531 year validation added for date range text boxes.
      var sYear = "";
      if (dateRangeYear.id == "TxtFiscalYear") // Get year for "Tax Year to export"
      {
        sYear = dateRangeYear.value;
      }
      else // Get year for From/To Date Range
      {
        sYear = dateRangeYear.value;
        if (sYear != '')
        {
            sYear = sYear.substring(6, 10);
        }
      }

    if (sYear < 1900) {
        dateRangeYear.value = "";
        return false;
    }
    return true;   
}

// Vsoni5 : MITS 25786 : Added this function to set the Roll Up Payments checkbox on the UI
function setRollUp(isrollUp) 
{
    if (isrollUp == true || isrollUp == "True")
        document.getElementById("chkTotalPaid").disabled = false;
    else {
        document.getElementById("chkTotalPaid").disabled = true;
        document.getElementById("chkTotalPaid").checked = false;
        setExportPayeeTTLPaid(false);
    }
}

// Vsoni5 : MITS 25786 : Added this function to set the "Export Payments Total Paid >=" checkbox on the UI
function setExportPayeeTTLPaid(isExportPayeeTTLPaidChecked) {
    if (isExportPayeeTTLPaidChecked)
        document.getElementById("TxtTotalPaid").disabled = false;
    else {
        document.getElementById("TxtTotalPaid").disabled = true;
        document.getElementById("TxtTotalPaid").value = "";
    }
}

function toggleFilters(disableFilters) {
    if (disableFilters == false || disableFilters == "False") {
        document.getElementById("tblFilters").disabled = false;
        
//        toggleExpReserveType(document.getElementById("chkBxExpReserveType").checked);
//        toggleExpTransType(document.getElementById("chkBxExpTransType").checked);
//        toggleExpLOB(document.getElementById("chkBxExpLOB").checked);
    }
    else {        
        //document.getElementById("chkBxExpReserveType").checked = false;
//        toggleExpReserveType(document.getElementById("chkBxExpReserveType").checked);
//        
//       // document.getElementById("chkBxExpTransType").checked = false;
//        toggleExpTransType(document.getElementById("chkBxExpTransType").checked);
//        
//       // document.getElementById("chkBxExpLOB").checked = false;
//        toggleExpLOB(document.getElementById("chkBxExpLOB").checked);
        document.getElementById("tblFilters").disabled = true;
    }
}

function toggleExpTransType(enableExpTransType) {
    if (enableExpTransType) {
        document.getElementById("ddlExpTransType").disabled = false;
        document.getElementById("lstBxExpTransType").disabled = false;
    }
    else {
        document.getElementById("ddlExpTransType").disabled = true;
        document.getElementById("lstBxExpTransType").disabled = true;
    }
}

function toggleExpReserveType(enableExpReserveType) {
    if (enableExpReserveType) {
        document.getElementById("ddlExpReserveType").disabled = false;
        document.getElementById("lstBxExpReserveType").disabled = false;
    }
    else {
        document.getElementById("ddlExpReserveType").disabled = true;
        document.getElementById("lstBxExpReserveType").disabled = true;
    }
}

function toggleExpLOB(enableExpLOB) {
    if (enableExpLOB) {
        document.getElementById("ddlExpLOB").disabled = false;
        document.getElementById("lstBxExpLOB").disabled = false;
    }
    else {
        document.getElementById("ddlExpLOB").disabled = true;
        document.getElementById("lstBxExpLOB").disabled = true;
    }
}

//Vsoni5 : MITS 25790 : Raise a Partial PostBack event to Synchronize Payer and Bank Account List.
// This function is called in form.js file's CodeSelected function.
function SyncPayerAndBankAccts() {
    __doPostBack('updPnlTransBoxes', '');
}
    </script>


</head>
<body  onload="setRollUp('<%= chkBxRollUpPymt.Checked %>'); toggleFilters('<%= chkBxExpAllPymtTxYr.Checked%>');">
    <form id="frmData" name="frmData" onmouseenter="setFile()" runat="server">
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:Label ID="Label1" runat="server" Text="1099 Optionset" />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </div>
            <div id="Div1" class="errtextheader" runat="server">
                <asp:Label ID="formdemotitle" runat="server" Text="" />
            </div>
    <table style="width: 655px; height: 111px;" >
        <tr>
            <td class="style6" colspan="2">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="style6" style="text-decoration: underline; font-weight: bold;">
                Optionset Name:
            </td>
            <td class="style1">
                <asp:TextBox ID="TxtOptionSetName" runat="server" onMouseEnter="setFile()" Width="131px"
                    MaxLength="50" TabIndex="1"></asp:TextBox>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style6" style="text-decoration: underline; font-weight: bold;">
                Tax Year to Export:
            </td>
            <td class="style1">
                <asp:TextBox ID="TxtFiscalYear" runat="server" Width="131px"
                    MaxLength="4" TabIndex="2" onchange="return numLostFocus(this);" onblur="return validateYearToImport(this);" ></asp:TextBox>
                &nbsp;
                </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style6">
                <asp:CheckBox ID="ChkDtRange" runat="server" Text="Specify Date Range:" AutoPostBack="True"
                    OnCheckedChanged="ChkDtRange_CheckedChanged" TabIndex="3" 
                    Font-Underline="True" Font-Bold="True" />
            </td>
            <td class="style1">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style6">
                &nbsp;Date From:
            </td>
            <td class="style1">
                <asp:TextBox ID="TxtDateFrom" tabIndex="4" runat="server" FormatAs="date" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);validateYearToImport(this);" size="10" Width="131px"  ></asp:TextBox>
      <%--vkumar258 - RMA_6037- Starts--%>
                <%--<asp:Button ID="btnCalendarFrom" runat="server" Class="DateLookupControl" Height="22px"
                    Width="22px" TabIndex="4" />

                <script type="text/javascript"> 
                        Zapatec.Calendar.setup(       {  inputField : "TxtDateFrom",       ifFormat : "%m/%d/%Y",       button : "btnCalendarFrom"       }       );      
                    </script>--%>
                  <div id="divFromDate" runat="server" class="sameline">
                <script type="text/javascript">$(function () { $("#TxtDateFrom").datepicker({ showOn: "button", buttonImage: "../../../../Images/calendar.gif", showOtherMonths: true, selectOtherMonths: true, changeYear: true }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "5"); });
          </script></div>
   <%--vkumar258 - RMA_6037- End--%>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style6">
                &nbsp;Date To:
            </td>
            <td class="style1">
                <asp:TextBox ID="TxtDateTo" tabIndex="6"  runat="server" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);validateYearToImport(this);" size="10"  FormatAs="date" Width="131px" ></asp:TextBox>
                     <%--vkumar258 - RMA_6037- Starts--%>

                 <%--<asp:Button ID="btnCalendarTo" runat="server" CssClass="DateLookupControl" value=""
                    Height="22px" Width="22px" TabIndex="5"/>

                <script type="text/javascript">
                    Zapatec.Calendar.setup({inputField: "TxtDateTo", ifFormat: "%m/%d/%Y", button: "btnCalendarTo" });  
                </script>--%>
                  <div id="divToDate" runat="server" class="sameline">
                <script type="text/javascript">$(function () { $("#TxtDateTo").datepicker({ showOn: "button", buttonImage: "../../../../Images/calendar.gif", showOtherMonths: true, selectOtherMonths: true, changeYear: true }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "7"); });
                                                        </script></div>
                                                           <%--vkumar258 - RMA_6037- End--%>

            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <table>
    <tr>
    <td>
            <table class="style24"  >
                <tr>
                <td colspan="2">
                <asp:UpdatePanel ID="updPnlPayerAndBankAccts" runat="server">
                <ContentTemplate>
                <table>
                    <tr>
                        <td class="style25">
                            <span class="style28" style="text-decoration: underline; font-weight: bold">Payers To Export Data For</span><span>:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="BtnOrgHier" runat="server" CssClass="button" Height="24px" OnClientClick="return lookupData('LstPayer','ORGANIZATIONS',4,'LstPayer',3);"
                                TabIndex="10" Text="Org Hierarchy" value="..." Width="86px" 
                                 />
                        </td>
                        <td id="tdChkExpBkAccts" runat="server">                           
                            <asp:CheckBox ID="chkBankAccount" runat="server" AutoPostBack="True" OnCheckedChanged="chkBankAccount_CheckedChanged"
                                Text="Export Specific Bank Accounts" TabIndex="11"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="style25">
                            <asp:ListBox ID="LstPayer" runat="server" Height="80px" Width="294px"></asp:ListBox>
                            <asp:ImageButton ID="BtnDelPayer" runat="server" ImageUrl="~/Images/240.GIF" Style="height: 16px"
                                OnClick="BtnDelPayer_Click" />
                            <asp:TextBox runat="server" Style="display: none" ID="LstPayer_lst" />
                        </td>
                        <td id="tdLstExpBkAccts" runat="server">
                            <asp:ListBox ID="LstBankAcc" runat="server" Height="80px" Width="290px" 
                                SelectionMode="Multiple"></asp:ListBox>
                            <asp:ImageButton ID="BtnDelAccount" runat="server" ImageUrl="~/Images/240.GIF" Style="height: 16px;
                                width: 16px;" OnClick="BtnDelAccount_Click" />
                        </td>
                    </tr>
                </table>
                </ContentTemplate>
                </asp:UpdatePanel>
                </td>                
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>

                <tr >
                    <td  valign="top">
                        <asp:CheckBox ID="chkPrintedPaymentsOnly" runat="server" 
                            style="margin-left:5px;" TabIndex="12" Text="Export Printed Payments Only" />
                        <asp:CheckBoxList ID="ChkList1" runat="server" TabIndex="13" 
                            CellSpacing="5" onselectedindexchanged="ChkList1_SelectedIndexChanged" >
                            <asp:ListItem>Export Payments Without Claim link</asp:ListItem> 
                            <asp:ListItem>Exclude Payments To Org Hierarchy</asp:ListItem>
                            <asp:ListItem>Exclude Payments To Claimants</asp:ListItem>
                            <asp:ListItem>Exclude Deleted Entities</asp:ListItem>
                            <asp:ListItem>Export Payees based on RM 1099 Reportable Flag</asp:ListItem>
                            <asp:ListItem>Include EntityID into Payee File</asp:ListItem>
                            
                        </asp:CheckBoxList>
                    </td>
                    <td valign="top">
                        <asp:CheckBox ID="chkBxRollUpPymt" runat="server" Text="RollUp Payments on Export"  
                        style="margin-left:5px;" onclick="setRollUp(this.checked)"/>
                        <br />
                        <asp:CheckBox ID="chkTotalPaid" runat="server"  TabIndex="14" 
                            Text="Export Payees with Total Paid =&gt;"  style="margin-left:5px;" onclick="setExportPayeeTTLPaid(this.checked)"/>
                        &nbsp;<asp:TextBox ID="TxtTotalPaid" runat="server" 
                             onblur=";currencyLostFocus(this);" onchange=";setDataChanged(true);" max="9999999999.99" ></asp:TextBox>
                    
                        <asp:CheckBoxList ID="ChkList2" runat="server" Width="316px"
                            OnSelectedIndexChanged="ChkList2_SelectedIndexChanged" TabIndex="15" 
                            CellSpacing="5" >
                            <asp:ListItem>Use double quotes</asp:ListItem>
                            <asp:ListItem>Ignore Tax ID errors</asp:ListItem>
                            </asp:CheckBoxList>
                    </td> </tr>
                    <tr>
                    <td class="style25">
                    
                    </td>
                    </tr>

        </table>
                <table>
                <tr>
                    <td >
                        <asp:CheckBox ID="chkBxExpAllPymtTxYr" Text="Export All Payments For Tax Year" 
                            runat="server" onClick="toggleFilters(this.checked)" Checked="True"/>
                    </td>

                </tr>
                </table>
                <!-- Export Filters Section- Start -->
                <table id ="tblFilters" class="tblfilters">
                <tr> 
                    <td>
                        <asp:UpdatePanel ID="updPnlTT" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <tr>
                                        <td>Export Specific Transaction Types</td>
                                    </tr>
                                    <tr>
                                      <td>
                                           <asp:DropDownList ID="ddlExpTransType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlExpTransType_SelectedIndexChanged"
                                            Width="230px" TabIndex="18" onprerender="ddlExpTransType_PreRender"></asp:DropDownList>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>
                                            <asp:ListBox ID="lstBxExpTransType" runat="server" Height="82px" Width="230px"></asp:ListBox>
                                            <asp:ImageButton ID="imgBtnDelTransTypes" runat="server" ImageUrl="~/Images/240.GIF" 
                                              style="margin-bottom:8px" onclick="imgBtnDelTransTypes_Click"  />
                                      </td>
                                     </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="updPnlRT" runat="server" UpdateMode="Conditional">
                            <ContentTemplate runat="server">
                                <table>
                                    <tr>
                                        <td>Export Specific Reserve Types</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlExpReserveType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlExpReserveType_SelectedIndexChanged"
                                        Width="230px" TabIndex="18" onprerender="ddlExpReserveType_PreRender"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <asp:ListBox ID="lstBxExpReserveType" runat="server" Height="82px" Width="230px"></asp:ListBox>
                                        <asp:ImageButton ID="imgBtnDelResTypes" runat="server" ImageUrl="~/Images/240.GIF" 
                                                style="margin-bottom:8px" onclick="imgBtnDelResTypes_Click"/>                                   
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate runat="server">
                            <table>
                                <tr>
                                    <td>Export Specific LOB</td>
                                </tr>
                                <tr>
                                  <td>
                                    <asp:DropDownList ID="ddlExpLOB" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlExpLOB_SelectedIndexChanged"
                                    Width="230px" TabIndex="18" onprerender="ddlExpLOB_PreRender"></asp:DropDownList>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <asp:ListBox ID="lstBxExpLOB" runat="server" Height="82px" Width="230px"></asp:ListBox>
                                    <asp:ImageButton ID="imgBtnDelLOB" runat="server" ImageUrl="~/Images/240.GIF" 
                                          style="margin-bottom:8px" onclick="imgBtnDelLOB_Click"/>
                                  </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </td>                    
                </tr>

                <tr>
                <td></td><td></td><td></td>
                </tr>
                </table>
                <!-- Export Filters Section- End -->
                <asp:UpdatePanel ID="updPnlTransBoxes" runat="server">
                <ContentTemplate>
                <table  id="tblBoxes" class="tblfilters">
                <tr id = "tr">
                    <td>
                        <asp:Label ID="lblReserveTypeBox6" runat="server" class="label" 
                            Text="Box 6 Medical Reserve Type" />
                    </td>
                    <td>
                        &nbsp;<asp:Label ID="lbl_TransTypes13" runat="server" class="label" Text="Box 13 Transaction Type" />
                        &nbsp;
                    </td>
                    <td>
                        Box 14 Transaction Type
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlReserveTypeBox6" runat="server" AutoPostBack="True" 
                            Height="22px" onselectedindexchanged="ddlReserveTypeBox6_SelectedIndexChanged" 
                            TabIndex="16" Width="230px" onprerender="ddlReserveTypeBox6_PreRender">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBox13" runat="server" AutoPostBack="True" Height="22px"
                            OnSelectedIndexChanged="ddlBox13_SelectedIndexChanged" Width="230px" 
                            TabIndex="19" onprerender="ddlBox13_PreRender">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBox14" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBox14_SelectedIndexChanged"
                            Width="230px" TabIndex="20" onprerender="ddlBox14_PreRender" >
                        </asp:DropDownList>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <asp:ListBox ID="lstReserveTypeBox6" runat="server" Width="230px"></asp:ListBox>
                        <asp:ImageButton ID="imgbtnReserveTypeBox6" runat="server" 
                            ImageUrl="~/Images/240.GIF" OnClick="imgbtnReserveTypeBox6_Click" />
                    </td>
                    <td>
                        <asp:ListBox ID="LstBox13" runat="server" Width="230px"></asp:ListBox>
                        <asp:ImageButton ID="BtnDelBox13" runat="server" ImageUrl="~/Images/240.GIF" OnClick="BtnDelBox13_Click" />
                    </td>
                    <td>
                        <asp:ListBox ID="LstBox14" runat="server" Width="230px"></asp:ListBox>
                        <asp:ImageButton ID="BtnDelBox14" runat="server" ImageUrl="~/Images/240.GIF" OnClick="BtnDelBox14_Click" />
                    </td>
                    
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </td>
    </tr>
    </table>
    <table>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="BtnSave" CssClass="button" runat="server" Text="Save" OnClientClick="return ValidateEffectiveDateTrig();" OnClick="BtnSave_Click"
                    Width="49px" TabIndex="21" />
                &nbsp;<asp:Button ID="BtnCancel" class="button" runat="server" Text="Cancel" Width="49px"
                    OnClick="BtnCancel_Click" TabIndex="22" CausesValidation="False" />
            </td>
        </tr>
    </table>
    <br />
    <asp:HiddenField ID="JavaScriptcheck" runat="server" />
    <asp:HiddenField ID="SysFormName" runat="server" Value="RM1099Setting" />
    <asp:HiddenField ID="hdTaskManagerXml" runat="server" />
    <asp:HiddenField ID="hdOptionsetId" runat="server" />
    </form>
</body>

</html>
