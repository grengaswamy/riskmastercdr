﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessHelpers;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.ServiceHelpers;
//Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
using Riskmaster.UI.DataIntegratorService;     //kkaur25 Reverting back to SOA Service
using System.IO;
using Riskmaster.Models;    //ipuri 12/06/2014


namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class DDSSettings : System.Web.UI.Page
    {
        private const string m_sModuleName = "DDS"; //Modules Name
        private int m_iOptionSetID; // this will be passed in from task manager.
        private string sEventFileName = string.Empty;
        private string sMedwatchFileName = string.Empty;
        private string sClaimFileName = string.Empty;
        private string sDisPlanFileName = string.Empty;
        private string sDisClassFileName = string.Empty;
        private string sAdjusterFileName = string.Empty;
        private string sPaymentFileName = string.Empty;
        private string sEnhNotesFileName = string.Empty;
        private DataSet dsReserveList = null;


        protected void Page_Load(object sender, EventArgs e)
        {
            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes
            if (!IsPostBack)
            {
                ViewState["LstReserveMapping"] = null;
                hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));
                m_iOptionSetID = CommonFunctions.OptionsetId;
                hdOptionsetId.Value = Common.Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                RetrieveSettings(); //Retrieve Settings from data integrator tables.
            }

            if (CommonFunctions.OptionsetId > 0)
            {
                txtOptionName.Enabled = false;
            }
            else
            {
                txtOptionName.Enabled = true;
            }
            //Register scripts for Tab control
            ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('" + hTabName.Value + "')};", true);

            //Hide Verification Flag
            gPerform_Verification.Visible = false;

        }

        protected void RetrieveSettings()
        {
            DataSet dsLOBList = null;
            
            //Vsoni5 : MITS 22565 : Exception thrown by Service Layer will be shown on UI.
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            //ipuri REST Service Conversion 12/06/2014 Start
            //DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            Riskmaster.Models.DataIntegratorModel objDIModel = new Riskmaster.Models.DataIntegratorModel();
            //ipuri REST Service Conversion 12/06/2014 End
            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }

            bool isError = false;

            try
            {
                m_iOptionSetID = CommonFunctions.OptionsetId;

                hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);

                //objDIModel.OptionSetName = "ISOfirst";
                objDIModel.OptionSetID = m_iOptionSetID;
                objDIModel.ModuleName = m_sModuleName;

                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.RetrieveSettings(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End
                if (objDIModel.OptionSetID != 0)
                {
                    txtOptionName.Enabled = false;
                }
                txtOptionName.Text = objDIModel.OptionSetName;

                Dictionary<string, string> DDS_Setting = new Dictionary<string, string>();
                DDS_Setting = objDIModel.Parms;
                UpdateControlsCollection(ref DDS_Setting, this.Form.Controls);
                
                dsLOBList = objDIModel.dsLOB;
                ViewState["LOBList"] = dsLOBList;
                lstLOB.DataSource = dsLOBList;
                lstLOB.DataValueField = dsLOBList.Tables[0].Columns[1].ToString();
                lstLOB.DataTextField = dsLOBList.Tables[0].Columns[1].ToString();
                //npradeepshar 03/24/2011 merged form r6
                //rsushilaggar MITS 21553 DATE 20-Sep-2010
                if (objDIModel.OptionSetID != 0)
                {
                    int i = 0;
                    foreach (DataRow row in dsLOBList.Tables[0].Rows)
                    {
                        // Vsoni5 : MITS 24657 : 04/08/2011 : Added Row count check to avoid null exception in case of edit job.
                        if (objDIModel.dsReserveMappings.Tables[0].Rows.Count > 0 && row["CODE_ID"].ToString().Trim() == objDIModel.dsReserveMappings.Tables[0].Rows[0]["LOB_CODE"].ToString().Trim())
                        {
                            break;
                        }
                        i = i + 1;
                    }
                    lstLOB.SelectedIndex = i;
                }
                //End rsushilaggar
                lstLOB.DataBind();
                ViewState["LOB"] = lstLOB.SelectedValue.Substring(0, lstLOB.SelectedValue.IndexOf(" ")); ;
                dsReserveList = objDIModel.dsReserveTypes;
                ViewState["ReserveType"] = dsReserveList;
                //npradeepshar merged on 03/24/2011
                //rsushilaggar MITS 21553 DATE 20-Sep-2010
                if (objDIModel.OptionSetID != 0)
                    ViewState["ReserveMappings"] = objDIModel.dsReserveMappings;
                //end rsushilaggar
                bindgrid(dsReserveList);
            }
            catch (Exception exp)
            {
                //Vsoni5 : MITS 22565 : Exception thrown by Service Layer will be shown on UI.
                isError = true;
                err.Add("DataIntegrator.Error", exp.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            UpdateViewState4ReserveMappings();
            SaveSettings(); 
        }

        protected void SaveSettings()
        {
            string sFileName = string.Empty;
            //kkaur25 Reverting back to SOA Service Conversion 11/06/2015 Start
            Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModelS = null;
            Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient objDIService = null;
            Riskmaster.Models.DataIntegratorModel objDIModel = null;
            //kkaur25 Reverting back to SOA Service Conversion 11/06/2015 End
            BusinessAdaptorErrors err = null;
            bool bErr = false;
            bool bVerifyCSVPath = false;
            string sTemp = string.Empty;
            string sTempLOB = string.Empty;
            string sTaskManagerXML = string.Empty;
            int iOptionSetId = 0;
            int iNoOfImportedAreas = 0;
            //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
            //kkaur25 Reverting back to SOA Service Conversion 11/06/2015 Start            
		     Riskmaster.Models.DAImportFile objDAImportFile = null;
			 Riskmaster.UI.DataIntegratorService.DAImportFile objDAImportFileS = new Riskmaster.UI.DataIntegratorService.DAImportFile();
            //kkaur25 Reverting back to SOA Service Conversion 11/06/2015 End
            Stream sTempStream = null;
            //Vsoni : End of MITS 22699.

            try
            {
                sTaskManagerXML = Server.HtmlDecode(hdTaskManagerXml.Value);
                iOptionSetId = Common.Conversion.ConvertObjToInt(hdOptionsetId.Value);
                objDIModelS = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
	            objDIModel = new Riskmaster.Models.DataIntegratorModel();
            //kkaur25 Reverting back to SOA Service Conversion 11/06/2015 Start 
                objDIModelS = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                objDIService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
          //kkaur25 Reverting back to SOA Service Conversion 11/06/2015 End
                err = new BusinessAdaptorErrors();
                //objDAImportFile = new DataIntegratorService.DAImportFile();     //ipuri
                objDAImportFile = new Riskmaster.Models.DAImportFile();
                sTemp = txtOptionName.Text;
                if (sTemp == "")
                {
                    err.Add("DataIntegrator.Error", "OptionSet Name cannot be blank", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                // mpandey24 05/20/2014 : MITS 36425 starts
                if (gEvents_Available.Checked == false && gMed_Watch_Available.Checked == false && gClaims_Available.Checked == false && gShort_Term_Disability_Available.Checked == false && gAdjuster_Notes_Available.Checked == false && gPayments_Available.Checked == false && gEnhanced_Notes_Available.Checked == false)  
                {
                    err.Add("DataIntegrator.Error", "Select at least one Import area or Available Files", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                // MITS 36425 ends.
                bVerifyCSVPath = VerifyCSVUpload(ref err);
                if (bVerifyCSVPath)
                    objDIModel.ImportFlage = true;

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                    //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                    objDAImportFile.Token = AppHelper.GetSessionId();
                }
                Dictionary<string, string> DDS_Setting = new Dictionary<string, string>();

                UpdateFlagsCollection(ref DDS_Setting, this.Form.Controls);
                //DIS_Setting.Add("Dairy_To_Users", "1");
                DDS_Setting.Add("gDiary_To_Users", "");
                
                //Vsoni5 : MITS 24648 : 04/15/2011 : Below line commented as its not required.
                //gValidate_Supplementals key will be added automatically in UpdateFlagsCollection function as per the check box selection.
                //DDS_Setting.Add("gValidate_Supplementals", "0"); // to be implemented later 

                DDS_Setting.Add("gUse_Transactions", "0");
                DDS_Setting.Add("gValid_Start_Date", gValid_Start_Date.Value);
                DDS_Setting.Add("gValid_End_Date", gValid_End_Date.Value);
                DDS_Setting.Add("gMax_Errors", gMax_Errors.Text);
                
                //vsoni5 : 24263 : this block of code is not required.
                //if (!DDS_Setting.ContainsKey("gNon_Emp_Match_By_Name"))
                    //DDS_Setting.Add("gNon_Emp_Match_By_Name","0");
                //if (!DDS_Setting.ContainsKey("gNon_Emp_Match_By_Tax_Id"))
                  //  DDS_Setting.Add("gNon_Emp_Match_By_Tax_Id", "0");

                //mjha3 : MITS 22707,22708,22709,22710,22711,22712,22713,21539
                // 8 new XML nodes added in criteria XML for the DDS import file names corresponding to the different modules.
                if (objDIModel.ImportFlage)
                {
                        DDS_Setting.Add("gEVENT_IMPORTFILE_NAME", sEventFileName + ".csv");
                        DDS_Setting.Add("gMEDWATCH_IMPORTFILE_NAME", sMedwatchFileName + ".csv");
                        DDS_Setting.Add("gCLAIM_IMPORTFILE_NAME", sClaimFileName + ".csv");
                        DDS_Setting.Add("gSTD_PLAN_IMPORTFILE_NAME", sDisPlanFileName + ".csv");
                        DDS_Setting.Add("gSTD_CLASS_IMPORTFILE_NAME", sDisClassFileName + ".csv");
                        DDS_Setting.Add("gADJUSTER_IMPORTFILE_NAME", sAdjusterFileName + ".csv");
                        DDS_Setting.Add("gPAYMENT_IMPORTFILE_NAME", sPaymentFileName + ".csv");
                        DDS_Setting.Add("gENHANCE_NOTES_IMPORTFILE_NAME", sEnhNotesFileName + ".csv");
                }
                objDIModel.Parms = DDS_Setting;
                objDIModel.OptionSetID = iOptionSetId;
                objDIModel.ModuleName = m_sModuleName;
                objDIModel.TaskManagerXml = sTaskManagerXML;

                if (DDS_Setting.ContainsKey("lstLOB"))
                {
                    sTempLOB = DDS_Setting["lstLOB"];
                    sTempLOB = sTempLOB.Substring(0, sTempLOB.IndexOf(" "));
                    switch (sTempLOB)
                    {
                        case "GC":
                            objDIModel.LOB = 241;
                            break;
                        case "VA":
                            objDIModel.LOB = 242;
                            break;
                        case "WC":
                            objDIModel.LOB = 243;
                            break;
                        case "DI":
                            objDIModel.LOB = 844;
                            break;
                        case "PC":
                            objDIModel.LOB = 845;
                            break;
                    }
                }
              
                sTemp = sTemp.Replace("'", "");
                if (sTemp.Length > 50)
                {
                    sTemp = sTemp.Substring(1, 50);
                }
                objDIModel.OptionSetName = sTemp;
                UpdateReserveMappingsInObject(ref objDIModel);

                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.SaveSettings(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End
                if (objDIModel.OptionSetID == -1)
                {
                    err.Add("DataIntegrator.Error", "Optionset name already exists", BusinessAdaptorErrorType.Message);
                    bErr = true;
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
                else if (objDIModel.ImportFlage)
                {
                    //put were you want the file to be saved
                    if (sEventFileName != "" && sEventFileName != null)
                    {
                        sFileName = sEventFileName + ".csv." + objDIModel.OptionSetID ;
                        //EventCSVUpload.PostedFile.SaveAs(objDIModel.FilePath + sFileName);
                        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                        sTempStream = EventCSVUpload.PostedFile.InputStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(sTempStream);
                        objDAImportFile.fileName = sFileName;
                        objDAImportFile.filePath = objDIModel.FilePath;
                        //ayadav38 MITS 36669 jira 9815-PMT DOCUMENT STORAGE TABLE
                        objDAImportFile.ModuleName = "DDS";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objDIModel.OptionSetID;
                        //ayadav38 MITS 36669-PMT DOCUMENT STORAGE TABLE end
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objDIService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                        objDAImportFile.ClientId = AppHelper.ClientId;
                         //kkaur25 Reverting back to SOA Service Conversion start               
                       objDAImportFileS.FileContents = objDAImportFile.FileContents; 
                       objDAImportFileS.fileName = sFileName;
                       objDAImportFileS.filePath = objDIModel.FilePath;
 				       objDAImportFileS.Token = AppHelper.Token;
                       objDAImportFileS.ClientId = AppHelper.ClientId;
                       objDAImportFileS.ModuleName = "DDS";
                       objDAImportFileS.DocumentType = "Import";
                       objDAImportFileS.OptionsetId = objDIModel.OptionSetID;
                       objDIService.UploadDAImportFile(objDAImportFileS);
               
                        // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                
				//kkaur25 Reverting back to SOA Service Conversion End

                        //ipuri REST Service Conversion 12/06/2014 End

                        //Vsoni5 : End of MITS 22699
                    }
                    //start: rsushilaggar 06/Jul/2010 modified the code to change the name of file upload control 
                    if (sMedwatchFileName != "" && sMedwatchFileName != null)
                    {
                        sFileName = sMedwatchFileName + ".csv." + objDIModel.OptionSetID;
                        //MedCSVUpload.PostedFile.SaveAs(objDIModel.FilePath + sFileName);
                        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                        sTempStream = MedCSVUpload.PostedFile.InputStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(sTempStream);
                        objDAImportFile.fileName = sFileName;
                        objDAImportFile.filePath = objDIModel.FilePath;
                        //ayadav38 MITS 36669- jira 9815 PMT DOCUMENT STORAGE TABLE
                        objDAImportFile.ModuleName = "DDS";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objDIModel.OptionSetID;
                        //ayadav38 MITS 36669-PMT DOCUMENT STORAGE TABLE end
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objDIService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                        objDAImportFile.ClientId = AppHelper.ClientId;
                        //kkaur25 Reverting back to SOA Service Conversion start               
                objDAImportFileS.FileContents = objDAImportFile.FileContents; 
                objDAImportFileS.fileName = sFileName;
                objDAImportFileS.filePath = objDIModel.FilePath;
 				objDAImportFileS.Token = AppHelper.Token;
                objDAImportFileS.ClientId = AppHelper.ClientId;
                objDAImportFileS.ModuleName = "DDS";
                objDAImportFileS.DocumentType = "Import";
                objDAImportFileS.OptionsetId = objDIModel.OptionSetID;
                objDIService.UploadDAImportFile(objDAImportFileS);
               
                        // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                
				//kkaur25 Reverting back to SOA Service Conversion End
                      //  AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                        //ipuri REST Service Conversion 12/06/2014 End

                        //Vsoni5 : End of MITS 22699
                    }
                    if (sClaimFileName != "" && sClaimFileName != null)
                    {
                        sFileName = sClaimFileName + ".csv." + objDIModel.OptionSetID ;
                        //ClaimCSVUpload.PostedFile.SaveAs(objDIModel.FilePath + sFileName);
                        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                        sTempStream = ClaimCSVUpload.PostedFile.InputStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(sTempStream);
                        objDAImportFile.fileName = sFileName;
                        objDAImportFile.filePath = objDIModel.FilePath;
                        //ayadav38 MITS 36669-  jira 9815 PMT DOCUMENT STORAGE TABLE
                        objDAImportFile.ModuleName = "DDS";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objDIModel.OptionSetID;
                        //ayadav38 MITS 36669-PMT DOCUMENT STORAGE TABLE end
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objDIService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                        objDAImportFile.ClientId = AppHelper.ClientId;
						    //kkaur25 Reverting back to SOA Service Conversion start               
                objDAImportFileS.FileContents = objDAImportFile.FileContents; 
                objDAImportFileS.fileName = sFileName;
                objDAImportFileS.filePath = objDIModel.FilePath;
 				objDAImportFileS.Token = AppHelper.Token;
                objDAImportFileS.ClientId = AppHelper.ClientId;
                objDAImportFileS.ModuleName = "DDS";
                objDAImportFileS.DocumentType = "Import";
                objDAImportFileS.OptionsetId = objDIModel.OptionSetID;
                objDIService.UploadDAImportFile(objDAImportFileS);
               
                        // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                
				//kkaur25 Reverting back to SOA Service Conversion End

                       // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                        //ipuri REST Service Conversion 12/06/2014 End

                        //Vsoni5 : End of MITS 22699
                    }
                    if (sDisPlanFileName != "" && sDisPlanFileName != null)
                    {
                        sFileName = sDisPlanFileName + ".csv." + objDIModel.OptionSetID;
                        //DisPlanCSVUpload.PostedFile.SaveAs(objDIModel.FilePath + sFileName);
                        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                        sTempStream = DisPlanCSVUpload.PostedFile.InputStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(sTempStream);
                        objDAImportFile.fileName = sFileName;
                        objDAImportFile.filePath = objDIModel.FilePath;
                        //ayadav38 MITS 36669- jira 9815 PMT DOCUMENT STORAGE TABLE
                        objDAImportFile.ModuleName = "DDS";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objDIModel.OptionSetID;
                        //ayadav38 MITS 36669-PMT DOCUMENT STORAGE TABLE end
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objDIService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                        objDAImportFile.ClientId = AppHelper.ClientId;

              //kkaur25 Reverting back to SOA Service Conversion start               
                objDAImportFileS.FileContents = objDAImportFile.FileContents; 
                objDAImportFileS.fileName = sFileName;
                objDAImportFileS.filePath = objDIModel.FilePath;
 				objDAImportFileS.Token = AppHelper.Token;
                objDAImportFileS.ClientId = AppHelper.ClientId;
                objDAImportFileS.ModuleName = "DDS";
                objDAImportFileS.DocumentType = "Import";
                objDAImportFileS.OptionsetId = objDIModel.OptionSetID;
                objDIService.UploadDAImportFile(objDAImportFileS);
               
                        // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                
				//kkaur25 Reverting back to SOA Service Conversion End

                        //Vsoni5 : End of MITS 22699
                    }
                    if (sDisClassFileName != "" && sDisClassFileName != null)
                    {
                        sFileName = sDisClassFileName + ".csv." + objDIModel.OptionSetID;
                        //DisclassCSVUpload.PostedFile.SaveAs(objDIModel.FilePath + sFileName);
                        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                        sTempStream = DisclassCSVUpload.PostedFile.InputStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(sTempStream);
                        objDAImportFile.fileName = sFileName;
                        objDAImportFile.filePath = objDIModel.FilePath;
                        //ayadav38 MITS 36669  jira 9815-PMT DOCUMENT STORAGE TABLE
                        objDAImportFile.ModuleName = "DDS";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objDIModel.OptionSetID;
                        //ayadav38 MITS 36669-PMT DOCUMENT STORAGE TABLE end
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objDIService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                        objDAImportFile.ClientId = AppHelper.ClientId;

                        //kkaur25 Reverting back to SOA Service Conversion start               
                objDAImportFileS.FileContents = objDAImportFile.FileContents; 
                objDAImportFileS.fileName = sFileName;
                objDAImportFileS.filePath = objDIModel.FilePath;
 				objDAImportFileS.Token = AppHelper.Token;
                objDAImportFileS.ClientId = AppHelper.ClientId;
                objDAImportFileS.ModuleName = "DDS";
                objDAImportFileS.DocumentType = "Import";
                objDAImportFileS.OptionsetId = objDIModel.OptionSetID;
                objDIService.UploadDAImportFile(objDAImportFileS);
               
                        // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                
				//kkaur25 Reverting back to SOA Service Conversion End

                       // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                        //ipuri REST Service Conversion 12/06/2014 End

                        //Vsoni5 : End of MITS 22699
                    }
                    if (sAdjusterFileName != "" && sAdjusterFileName != null)
                    {
                        sFileName = sAdjusterFileName + ".csv." + objDIModel.OptionSetID;
                        //AdjusterCSVUpload.PostedFile.SaveAs(objDIModel.FilePath + sFileName);
                        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                        sTempStream = AdjusterCSVUpload.PostedFile.InputStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(sTempStream);
                        objDAImportFile.fileName = sFileName;
                        objDAImportFile.filePath = objDIModel.FilePath;
                        //ayadav38 MITS 36669- jira 9815 PMT DOCUMENT STORAGE TABLE
                        objDAImportFile.ModuleName = "DDS";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objDIModel.OptionSetID;
                        //ayadav38 MITS 36669-PMT DOCUMENT STORAGE TABLE end
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objDIService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                        
                        objDAImportFile.ClientId = AppHelper.ClientId;
                        //kkaur25 Reverting back to SOA Service Conversion start               
                objDAImportFileS.FileContents = objDAImportFile.FileContents; 
                objDAImportFileS.fileName = sFileName;
                objDAImportFileS.filePath = objDIModel.FilePath;
 				objDAImportFileS.Token = AppHelper.Token;
                objDAImportFileS.ClientId = AppHelper.ClientId;
                objDAImportFileS.ModuleName = "DDS";
                objDAImportFileS.DocumentType = "Import";
                objDAImportFileS.OptionsetId = objDIModel.OptionSetID;
                objDIService.UploadDAImportFile(objDAImportFileS);
               
                        // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                
				//kkaur25 Reverting back to SOA Service Conversion End


                      //  AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                        //ipuri REST Service Conversion 12/06/2014 End

                        //Vsoni5 : End of MITS 22699
                    }
                    if (sPaymentFileName != "" && sPaymentFileName != null)
                    {
                        sFileName = sPaymentFileName + ".csv." + objDIModel.OptionSetID ;
                        //PaymentsCSVUpload.PostedFile.SaveAs(objDIModel.FilePath + sFileName);
                        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                        sTempStream = PaymentsCSVUpload.PostedFile.InputStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(sTempStream);
                        objDAImportFile.fileName = sFileName;
                        objDAImportFile.filePath = objDIModel.FilePath;
                        //ayadav38 MITS 36669- jira 9815 PMT DOCUMENT STORAGE TABLE
                        objDAImportFile.ModuleName = "DDS";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objDIModel.OptionSetID;
                        //ayadav38 MITS 36669-PMT DOCUMENT STORAGE TABLE end
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objDIService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                        objDAImportFile.ClientId = AppHelper.ClientId;

                        //kkaur25 Reverting back to SOA Service Conversion start               
                objDAImportFileS.FileContents = objDAImportFile.FileContents; 
                objDAImportFileS.fileName = sFileName;
                objDAImportFileS.filePath = objDIModel.FilePath;
 				objDAImportFileS.Token = AppHelper.Token;
                objDAImportFileS.ClientId = AppHelper.ClientId;
                objDAImportFileS.ModuleName = "DDS";
                objDAImportFileS.DocumentType = "Import";
                objDAImportFileS.OptionsetId = objDIModel.OptionSetID;
                objDIService.UploadDAImportFile(objDAImportFileS);
               
                        // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                
				//kkaur25 Reverting back to SOA Service Conversion End

                        //AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                        //ipuri REST Service Conversion 12/06/2014 End
                        //Vsoni5 : End of MITS 22699
                    }
                    if (sEnhNotesFileName != "" && sEnhNotesFileName != null)
                    {
                        sFileName = sEnhNotesFileName + ".csv." + objDIModel.OptionSetID ;
                        //EnhNotesCSVUpload.PostedFile.SaveAs(objDIModel.FilePath + sFileName);
                        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                        sTempStream = EnhNotesCSVUpload.PostedFile.InputStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(sTempStream);
                        objDAImportFile.fileName = sFileName;
                        objDAImportFile.filePath = objDIModel.FilePath;
                        //ayadav38 MITS 36669  jira 9815-PMT DOCUMENT STORAGE TABLE
                        objDAImportFile.ModuleName = "DDS";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objDIModel.OptionSetID;
                        //ayadav38 MITS 36669-PMT DOCUMENT STORAGE TABLE end
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objDIService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                        objDAImportFile.ClientId = AppHelper.ClientId;
                        //kkaur25 Reverting back to SOA Service Conversion start               
                objDAImportFileS.FileContents = objDAImportFile.FileContents; 
                objDAImportFileS.fileName = sFileName;
                objDAImportFileS.filePath = objDIModel.FilePath;
 				objDAImportFileS.Token = AppHelper.Token;
                objDAImportFileS.ClientId = AppHelper.ClientId;
                objDAImportFileS.ModuleName = "DDS";
                objDAImportFileS.DocumentType = "Import";
                objDAImportFileS.OptionsetId = objDIModel.OptionSetID;
                objDIService.UploadDAImportFile(objDAImportFileS);
               
                        // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                
				//kkaur25 Reverting back to SOA Service Conversion End
                      //  AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                        //ipuri REST Service Conversion 12/06/2014 End
                        //Vsoni5 : End of MITS 22699
                    }
                    //end: rsushilaggar 06/Jul/2010 modified the code to change the name of file upload control 
                }
                if (objDIModel.OptionSetID > 0)
                {
                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx",false);
                }
            }
            catch (Exception eRM)
            {
                if (eRM.Message != "VerifyCSVUpload")
                {
                    err.Add("DataIntegrator.Error", eRM.Message, BusinessAdaptorErrorType.Message);
                    /// Vsoni5 : 01/29/2011 : MITS 23364
                    try
                    {
                        if (iOptionSetId <= 0 && objDIModel.OptionSetID > 0)
                            //ipuri REST Service Conversion 12/06/2014 Start
                            //objDIService.SaveSettingsCleanup(objDIModel);
                            objDIModel.ClientId = AppHelper.ClientId;
                        AppHelper.GetResponse("RMService/DAIntegration/savesettingscleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                        //ipuri REST Service Conversion 12/06/2014 End
                            
                    }
                    catch (Exception eCleanup)
                    {                        
                    } 
                }
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void UpdateFlagsCollection(ref Dictionary<string, string> DDS_Setting, ControlCollection oControls)
        {
            string sControlType = string.Empty;
            string sControlName = string.Empty;

            foreach (Control oControl in oControls)
            {
                if (oControl.Controls.Count == 0)
                {
                    sControlType = oControl.GetType().ToString();
                    sControlType = sControlType.Substring(sControlType.LastIndexOf(".") + 1);
                    sControlName = oControl.ID;
                    
                    switch (sControlType)
                    {
                        case "CheckBox":
                            if (((CheckBox)oControl).Checked)
                                DDS_Setting.Add(sControlName, "1");
                            else
                                DDS_Setting.Add(sControlName, "0");
                            break;
                        case "RadioButton":
                            switch (sControlName)
                            {
                                // Vsoni5 : MITS 24623 : 04/07/2011 : Added both the search keys in all 3 cases.
                                case "gNon_Emp_Match_By_Name": //"Add" and "Add_Update" radiobuttons are mutually exclusive
                                    if (((RadioButton)oControl).Checked)
                                    {
                                        DDS_Setting.Add("gNon_Emp_Match_By_Name", "1");
                                        DDS_Setting.Add("gNon_Emp_Match_By_Tax_Id", "0");
                                    }
                                    break;
                                case "gNon_Emp_Match_By_Both": //"Add" and "Add_Update" radiobuttons are mutually exclusive
                                    if (((RadioButton)oControl).Checked)
                                    {
                                        DDS_Setting.Add("gNon_Emp_Match_By_Name", "1");
                                        DDS_Setting.Add("gNon_Emp_Match_By_Tax_Id", "1");
                                    }
                                    break;
                                case "gNon_Emp_Match_By_Tax_Id": //"Add" and "Add_Update" radiobuttons are mutually exclusive
                                    if (((RadioButton)oControl).Checked)
                                    {
                                        DDS_Setting.Add("gNon_Emp_Match_By_Name", "0");
                                        DDS_Setting.Add("gNon_Emp_Match_By_Tax_Id", "1");
                                    }
                                    break;
                                default:
                                    if (((RadioButton)oControl).Checked)
                                        DDS_Setting.Add(sControlName, "1");
                                    else
                                        DDS_Setting.Add(sControlName, "0");
                                    break;
                            }
                            break;
                        case "DropDownList":
                            if (sControlName == "gPayment_Bank_Account")
                            {
                                DDS_Setting.Add("gPayment_Sub_Account", "");  
                            }
                            DDS_Setting.Add(sControlName, ((DropDownList)oControl).SelectedValue);
                            break;
                    }
                }
                else
                {
                    if (oControl.ID != "GV_ReserveMapping")
                    {
                        UpdateFlagsCollection(ref DDS_Setting, oControl.Controls);
                    }
                }
            }
        }


        private void UpdateControlsCollection(ref Dictionary<string, string> DDS_Setting, ControlCollection oControls)
        {
            string sControlType = string.Empty;
            string sControlName = string.Empty;

            foreach (Control oControl in oControls)
            {
                if (oControl.Controls.Count == 0)
                {
                    sControlType = oControl.GetType().ToString();
                    sControlType = sControlType.Substring(sControlType.LastIndexOf(".") + 1);
                    sControlName = oControl.ID;
                    
                    switch (sControlType)
                    {
                        case "CheckBox":
                            if (DDS_Setting.ContainsKey(sControlName))
                            {
                                if (DDS_Setting[sControlName] == "1")
                                    ((CheckBox)oControl).Checked = true;
                                else
                                    ((CheckBox)oControl).Checked = false;
                            }
                            break;
                        case "RadioButton":
                            if (DDS_Setting.ContainsKey(sControlName))
                            {
                                if (DDS_Setting[sControlName] == "1")
                                    ((RadioButton)oControl).Checked = true;
                                else
                                    ((RadioButton)oControl).Checked = false;
                            }
                            break;
                    }
                }
                else
                {
                    UpdateControlsCollection(ref DDS_Setting, oControl.Controls);
                }
            }

            if (DDS_Setting.ContainsKey("gValid_Start_Date"))
                gValid_Start_Date.Value = DDS_Setting["gValid_Start_Date"];
            if (DDS_Setting.ContainsKey("gValid_End_Date"))
                gValid_End_Date.Value = DDS_Setting["gValid_End_Date"];
            if (DDS_Setting.ContainsKey("gMax_Errors"))
                gMax_Errors.Text = DDS_Setting["gMax_Errors"];

            if (DDS_Setting.ContainsKey("gNon_Emp_Match_By_Name") && DDS_Setting.ContainsKey("gNon_Emp_Match_By_Tax_Id"))
            {
                if (DDS_Setting["gNon_Emp_Match_By_Name"] == "1" && DDS_Setting["gNon_Emp_Match_By_Tax_Id"] == "1")
                {
                    gNon_Emp_Match_By_Both.Checked = true;
                    gNon_Emp_Match_By_Name.Checked = false;
                    gNon_Emp_Match_By_Tax_Id.Checked = false;
                }
            }
        }



        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {
            if (gEvents_Available.Checked)
                EventCSVFiles.Visible = true;
            else
                EventCSVFiles.Visible = false;
        }

        protected void chkClaim_CheckedChanged(object sender, EventArgs e)
        {
            if (gClaims_Available.Checked)
                ClaimCSVFiles.Visible = true;            
            else
                ClaimCSVFiles.Visible = false;            
        }

        protected void chkMedwatch_CheckedChanged(object sender, EventArgs e)
        {
            if (gMed_Watch_Available.Checked)
                MedCSVFiles.Visible = true;
            else
                MedCSVFiles.Visible = false;
        }

        protected void chkDisability_CheckedChanged(object sender, EventArgs e)
        {
            if (gShort_Term_Disability_Available.Checked)
            {
                DisplanCSVFiles.Visible = true;
                DisclassCSVFiles.Visible = true;
            }
            else
            {
                DisplanCSVFiles.Visible = false;
                DisclassCSVFiles.Visible = false;
            }
        }

        protected void chkAdjuster_CheckedChanged(object sender, EventArgs e)
        {
            if (gAdjuster_Notes_Available.Checked)
                AdjusterCSVFiles.Visible = true;
            else
                AdjusterCSVFiles.Visible = false;
        }

        protected void chkPayments_CheckedChanged(object sender, EventArgs e)
        {
            if (gPayments_Available.Checked)
            {
                PaymentsCSVFiles.Visible = true;
                BankAcc.Visible = true;

                //Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient objDIService = null;
                //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModel = null;
                Riskmaster.Models.DataIntegratorModel objDIModel = null;
                string sSelectedVal = string.Empty;
                DataSet dsFieldList = null;
                BusinessAdaptorErrors berr = null;
                bool bErr = false;
                try
                {
                    berr = new BusinessAdaptorErrors();
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objDIModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                    //objDIService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
                    objDIModel = new Riskmaster.Models.DataIntegratorModel();
                    //ipuri REST Service Conversion 12/06/2014 End
                    if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                    {
                        objDIModel.Token = AppHelper.GetSessionId();
                    }

                    objDIModel.ModuleName = m_sModuleName;

                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objDIModel = objDIService.GetBankAccountList(objDIModel);
                    objDIModel.ClientId = AppHelper.ClientId;
                    objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/getbankaccountList", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                    //ipuri REST Service Conversion 12/06/2014 End
                    dsFieldList = objDIModel.dsBankAccounts;

                    gPayment_Bank_Account.DataSource = dsFieldList;
                    gPayment_Bank_Account.DataValueField = dsFieldList.Tables[0].Columns[0].ToString();
                    gPayment_Bank_Account.DataTextField = dsFieldList.Tables[0].Columns[1].ToString();
                    gPayment_Bank_Account.DataBind();
                    //rsushilaggar MITS 21462 04-Jul-2010
                    if (dsFieldList.Tables[0].Columns[0].ColumnName != "ACCOUNT_ID")
                    {
                        lblBankAccount.Text = "Payment Bank Sub Account:";
                    }
                    else
                    {
                        lblBankAccount.Text = "Payment Bank Account:";
                    }
                }
                catch (Exception eRm)
                {
                    bErr = true;
                }
      


            }
            else
            {
                PaymentsCSVFiles.Visible = false;
                BankAcc.Visible = false;
            }
        }

        protected void chkEnhNotes_CheckedChanged(object sender, EventArgs e)
        {
            if (gEnhanced_Notes_Available.Checked)
                EnhNotesCSVFiles.Visible = true;
            else
                EnhNotesCSVFiles.Visible = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx");
        }

        private bool IsValidExtension(string sFileName)
        {
            string sFileExt = string.Empty;
            bool bValid = true;
            sFileExt = sFileName.Substring(sFileName.LastIndexOf(".") + 1, sFileName.Length - sFileName.LastIndexOf(".") - 1);
            if (sFileExt.ToLower() != "csv")  
            {
                bValid = false;
            }
            return bValid;
        }

        private bool VerifyCSVUpload(ref BusinessAdaptorErrors err)
        {
            bool bIsValidated = true;
            BusinessAdaptorErrors err1 =  new BusinessAdaptorErrors();
            if (gEvents_Available.Checked)
            {
                if (EventCSVUpload.HasFile) // to-do for all csv !!!!
                {
                    sEventFileName = EventCSVUpload.PostedFile.FileName;
                    if (!IsValidExtension(sEventFileName))
                    {
                        //throw new Exception("Unrecognized Database Format:" + sEventFileName + " for Event CSV File. Only Files with Extension .csv accepted");
                        err.Add("DataIntegrator.Error", "Unrecognized Database Format:" + sEventFileName + " for Event CSV File. Only Files with Extension .csv accepted", BusinessAdaptorErrorType.Message);
                        bIsValidated = false;
                    
                    }
                    sEventFileName = sEventFileName.Substring(sEventFileName.LastIndexOf("\\") + 1, sEventFileName.Length - sEventFileName.LastIndexOf("\\") - 1);
                    sEventFileName = sEventFileName.Substring(0, sEventFileName.IndexOf("."));
                    
                    // Vsoni5 - MITS 23364. Added module specific name as prefix with the import file name
                    if (!String.IsNullOrEmpty(sEventFileName))
                    {
                        sEventFileName = "Event_" + sEventFileName;
                    }
                }
                else
                {
                    //throw new Exception("Event CSV File Path cannot be blank");
                    err.Add("DataIntegrator.Error", "Event CSV File Path cannot be blank / File size cannot be 0 bytes", BusinessAdaptorErrorType.Message);
                    bIsValidated = false;
                    //ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
            if (gMed_Watch_Available.Checked)
            {
                if (MedCSVUpload.HasFile) // to-do for all csv !!!!
                {
                    sMedwatchFileName = MedCSVUpload.PostedFile.FileName;
                    if (!IsValidExtension(sMedwatchFileName))
                    {
                        //throw new Exception("Unrecognized Database Format:" + sMedwatchFileName + " for MedWatch CSV File. Only Files with Extension .csv accepted");
                        err.Add("DataIntegrator.Error", "Unrecognized Database Format:" + sMedwatchFileName + " for MedWatch CSV File. Only Files with Extension .csv accepted", BusinessAdaptorErrorType.Message);
                        bIsValidated = false;
                    }
                    sMedwatchFileName = sMedwatchFileName.Substring(sMedwatchFileName.LastIndexOf("\\") + 1, sMedwatchFileName.Length - sMedwatchFileName.LastIndexOf("\\") - 1);
                    sMedwatchFileName = sMedwatchFileName.Substring(0, sMedwatchFileName.IndexOf("."));

                    // Vsoni5 - MITS 23364. Added module specific name as prefix with the import file name
                    if (!String.IsNullOrEmpty(sMedwatchFileName))
                    {
                        sMedwatchFileName = "MedWatch_" + sMedwatchFileName;
                    }
                }
                else
                {
                    //throw new Exception("Medwatch CSV File Path cannot be blank");  
                    err.Add("DataIntegrator.Error", "Medwatch CSV File Path cannot be blank / File size cannot be 0 bytes", BusinessAdaptorErrorType.Message);
                    bIsValidated = false;
                }
            }
            if (gClaims_Available.Checked)
            {
                if (ClaimCSVUpload.HasFile) // to-do for all csv !!!!
                {
                    sClaimFileName = ClaimCSVUpload.PostedFile.FileName;
                    if (!IsValidExtension(sClaimFileName))
                    {
                        //throw new Exception("Unrecognized Database Format:" + sClaimFileName + " for Claim CSV File. Only Files with Extension .csv accepted");
                        err.Add("DataIntegrator.Error", "Unrecognized Database Format:" + sClaimFileName + " for Claim CSV File. Only Files with Extension .csv accepted", BusinessAdaptorErrorType.Message);
                        bIsValidated = false;
                    }
                    sClaimFileName = sClaimFileName.Substring(sClaimFileName.LastIndexOf("\\") + 1, sClaimFileName.Length - sClaimFileName.LastIndexOf("\\") - 1);
                    sClaimFileName = sClaimFileName.Substring(0, sClaimFileName.IndexOf("."));

                    // Vsoni5 - MITS 23364. Added module specific name as prefix with the import file name
                    if (!String.IsNullOrEmpty(sClaimFileName))
                    {
                        sClaimFileName = "Claim_" + sClaimFileName;
                    }
                }
                else
                {
                    //throw new Exception("Claim CSV File Path cannot be blank"); 
                    err.Add("DataIntegrator.Error", "Claim CSV File Path cannot be blank / File size cannot be 0 bytes", BusinessAdaptorErrorType.Message);
                    bIsValidated = false;
                }
            }
            if (gShort_Term_Disability_Available.Checked)
            {
                if (!DisPlanCSVUpload.HasFile && !DisclassCSVUpload.HasFile)
                {
                   err.Add("DataIntegrator.Error", "Disability Plan/Class CSV File Path cannot be blank / File size cannot be 0 bytes", BusinessAdaptorErrorType.Message);
                   //err.Add("DataIntegrator.Error", "Disability Class CSV File Path cannot be blank / File size cannot be 0 bytes", BusinessAdaptorErrorType.Message);
                   bIsValidated = false;
                }
                else
                {
                    if (DisPlanCSVUpload.HasFile) // to-do for all csv !!!!
                    {
                        sDisPlanFileName = DisPlanCSVUpload.PostedFile.FileName;
                        if (!IsValidExtension(sDisPlanFileName))
                        {
                            //throw new Exception("Unrecognized Database Format:" + sDisPlanFileName + " for Disability Plan CSV File. Only Files with Extension .csv accepted");
                            err.Add("DataIntegrator.Error", "Unrecognized Database Format:" + sDisPlanFileName + " for Disability Plan CSV File. Only Files with Extension .csv accepted", BusinessAdaptorErrorType.Message);
                            bIsValidated = false;
                        }
                        sDisPlanFileName = sDisPlanFileName.Substring(sDisPlanFileName.LastIndexOf("\\") + 1, sDisPlanFileName.Length - sDisPlanFileName.LastIndexOf("\\") - 1);
                        sDisPlanFileName = sDisPlanFileName.Substring(0, sDisPlanFileName.IndexOf("."));

                        // Vsoni5 - MITS 23364. Added module specific name as prefix with the import file name
                        if (!String.IsNullOrEmpty(sDisPlanFileName))
                        {
                            sDisPlanFileName = "Disability_Plan_" + sDisPlanFileName ;
                        }
                    }

                    if (DisclassCSVUpload.HasFile) // to-do for all csv !!!!
                    {
                        sDisClassFileName = DisclassCSVUpload.PostedFile.FileName;
                        if (!IsValidExtension(sDisClassFileName))
                        {
                            //throw new Exception("Unrecognized Database Format:" + sDisClassFileName + " for Disability Class CSV File. Only Files with Extension .csv accepted");
                            err.Add("DataIntegrator.Error", "Unrecognized Database Format:" + sDisClassFileName + " for Disability Class CSV File. Only Files with Extension .csv accepted", BusinessAdaptorErrorType.Message);
                            bIsValidated = false;
                        }
                        sDisClassFileName = sDisClassFileName.Substring(sDisClassFileName.LastIndexOf("\\") + 1, sDisClassFileName.Length - sDisClassFileName.LastIndexOf("\\") - 1);
                        sDisClassFileName = sDisClassFileName.Substring(0, sDisClassFileName.IndexOf("."));

                        // Vsoni5 - MITS 23364. Added module specific name as prefix with the import file name
                        if (!String.IsNullOrEmpty(sDisClassFileName))
                        {
                            sDisClassFileName = "Disability_Class_" + sDisClassFileName ;
                        }
                    }
                }
                
            }
            if (gAdjuster_Notes_Available.Checked)
            {
                if (AdjusterCSVUpload.HasFile) // to-do for all csv !!!!
                {
                    sAdjusterFileName = AdjusterCSVUpload.PostedFile.FileName;
                    if (!IsValidExtension(sAdjusterFileName))
                    {
                        //throw new Exception("Unrecognized Database Format:" + sAdjusterFileName + " for Adjuster Notes CSV File. Only Files with Extension .csv accepted");
                        err.Add("DataIntegrator.Error", "Unrecognized Database Format:" + sAdjusterFileName + " for Adjuster Notes CSV File. Only Files with Extension .csv accepted", BusinessAdaptorErrorType.Message);
                        bIsValidated = false;
                    }
                    sAdjusterFileName = sAdjusterFileName.Substring(sAdjusterFileName.LastIndexOf("\\") + 1, sAdjusterFileName.Length - sAdjusterFileName.LastIndexOf("\\") - 1);
                    sAdjusterFileName = sAdjusterFileName.Substring(0, sAdjusterFileName.IndexOf("."));

                    // Vsoni5 - MITS 23364. Added module specific name as prefix with the import file name
                    if (!String.IsNullOrEmpty(sAdjusterFileName))
                    {
                        sAdjusterFileName = "Disability_Adjuster_" + sAdjusterFileName;
                    }
                }
                else
                {
                    //throw new Exception("Adjuster Notes CSV File Path cannot be blank"); 
                    err.Add("DataIntegrator.Error", "Adjuster Notes CSV File Path cannot be blank / File size cannot be 0 bytes", BusinessAdaptorErrorType.Message);
                    bIsValidated = false;
                }
            }
            if (gPayments_Available.Checked)
            {
                if (PaymentsCSVUpload.HasFile) // to-do for all csv !!!!
                {
                    sPaymentFileName = PaymentsCSVUpload.PostedFile.FileName;
                    if (!IsValidExtension(sPaymentFileName))
                    {
                        //throw new Exception("Unrecognized Database Format:" + sPaymentFileName + " for Payments CSV File. Only Files with Extension .csv accepted");
                        err.Add("DataIntegrator.Error", "Unrecognized Database Format:" + sPaymentFileName + " for Payments CSV File. Only Files with Extension .csv accepted", BusinessAdaptorErrorType.Message);
                        bIsValidated = false;
                    }
                    sPaymentFileName = sPaymentFileName.Substring(sPaymentFileName.LastIndexOf("\\") + 1, sPaymentFileName.Length - sPaymentFileName.LastIndexOf("\\") - 1);
                    sPaymentFileName = sPaymentFileName.Substring(0, sPaymentFileName.IndexOf("."));

                    // Vsoni5 - MITS 23364. Added module specific name as prefix with the import file name
                    if (!String.IsNullOrEmpty(sPaymentFileName))
                    {
                        sPaymentFileName = "Payments_" + sPaymentFileName;
                    }
                }
                else
                {
                    //throw new Exception("Payments CSV File Path cannot be blank"); 
                    err.Add("DataIntegrator.Error", "Payments CSV File Path cannot be blank / File size cannot be 0 bytes", BusinessAdaptorErrorType.Message);
                    bIsValidated = false;
                }
            }
            if (gEnhanced_Notes_Available.Checked)
            {
                if (EnhNotesCSVUpload.HasFile) // to-do for all csv !!!!
                {
                    sEnhNotesFileName = EnhNotesCSVUpload.PostedFile.FileName;
                    if (!IsValidExtension(sEnhNotesFileName))
                    {
                        //throw new Exception("Unrecognized Database Format:" + sEnhNotesFileName + " for Enhanced Notes CSV File. Only Files with Extension .csv accepted");
                        err.Add("DataIntegrator.Error", "Unrecognized Database Format:" + sEnhNotesFileName + " for Enhanced Notes CSV File. Only Files with Extension .csv accepted", BusinessAdaptorErrorType.Message);
                        bIsValidated = false;
                    }
                    sEnhNotesFileName = sEnhNotesFileName.Substring(sEnhNotesFileName.LastIndexOf("\\") + 1, sEnhNotesFileName.Length - sEnhNotesFileName.LastIndexOf("\\") - 1);
                    sEnhNotesFileName = sEnhNotesFileName.Substring(0, sEnhNotesFileName.IndexOf("."));

                    // Vsoni5 - MITS 23364. Added module specific name as prefix with the import file name
                    if (!String.IsNullOrEmpty(sEnhNotesFileName))
                    {
                        sEnhNotesFileName = "Enhanced_" + sEnhNotesFileName;
                    }
                }
                else
                {
                    //throw new Exception("Enhanced Notes CSV File Path cannot be blank"); 
                    err.Add("DataIntegrator.Error", "Enhanced Notes CSV File Path cannot be blank / File size cannot be 0 bytes", BusinessAdaptorErrorType.Message);
                    bIsValidated = false;
                }
            }

            if (!string.IsNullOrEmpty(gValid_Start_Date.Value))
            {
                if (string.IsNullOrEmpty(gValid_End_Date.Value))
                {
                    err.Add("DataIntegrator.Error", "Date Field Validation Range: To date cannot be blank", BusinessAdaptorErrorType.Message);
                    bIsValidated = false;
                }
                else
                {
                    DateTime fromDate = Convert.ToDateTime(gValid_Start_Date.Value);
                    DateTime toDate = Convert.ToDateTime(gValid_End_Date.Value);
                    if (toDate.Subtract(fromDate).Days < 0)
                    {
                        err.Add("DataIntegrator.Error", "Date Field Validation Range: To date cannot be < from date", BusinessAdaptorErrorType.Message);
                        bIsValidated = false;
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(gValid_End_Date.Value))
                {
                    err.Add("DataIntegrator.Error", "Date Field Validation Range: From date cannot be blank", BusinessAdaptorErrorType.Message);
                    bIsValidated = false;
                }
            }

            if (!bIsValidated)
            {
                throw new Exception("VerifyCSVUpload");
            }
            return true;
        }

        /// <summary>
        /// This function is used for getting reserve types for the selected LOB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lstLOB_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient objDIService = null;
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModel = null;
           Riskmaster.Models. DataIntegratorModel objDIModel = null;
            UpdateViewState4ReserveMappings();

            string sSelectedVal = string.Empty;
            DataSet dsReserveList = null;
            BusinessAdaptorErrors berr = null;
            bool bErr = false;
            string sTempLOB = string.Empty;
            try
            {
                berr = new BusinessAdaptorErrors();
                //objDIModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                //objDIService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
                objDIModel = new Riskmaster.Models.DataIntegratorModel();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }

                sTempLOB = lstLOB.SelectedValue;
                sTempLOB = sTempLOB.Substring(0, sTempLOB.IndexOf(" "));
                switch (sTempLOB)
                {
                    case "GC":
                        objDIModel.LOB = 241;
                        break;
                    case "VA":
                        objDIModel.LOB = 242;
                        break;
                    case "WC":
                        objDIModel.LOB = 243;
                        break;
                    case "DI":
                        objDIModel.LOB = 844;
                        break;
                    case "PC":
                        objDIModel.LOB = 845;
                        break;
                }
                ViewState["LOB"] = sTempLOB;
                objDIModel.ModuleName = m_sModuleName;
                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.GetReserveList4LOB(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;
                objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/getreservelist4lob", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End
                dsReserveList = objDIModel.dsReserveTypes;
                ViewState["ReserveType"] = dsReserveList;
                bindgrid(dsReserveList);
            }
            catch (Exception eRm)
            {
                bErr = true;
            }





        }

        /// <summary>
        /// This fuinction is used for binding the data to the Reserve Mappings grid.
        /// </summary>
        /// <param name="dsReserveList"></param>
        private void bindgrid(DataSet dsReserveList)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn();
            dc.ColumnName = "Bucket";
            dc.DataType = System.Type.GetType("System.String");
            dt.Columns.Add(dc);
            dc = new DataColumn();
            dc.ColumnName = "Mapped";
            dt.Columns.Add(dc);
            dc = new DataColumn();
            dc.ColumnName = "ReserveType";
            dt.Columns.Add(dc);
            //npradeepshar 03/24/2011 merged 21553 in R7
            int iAvailableMappings = 0;
            int iSelectedMappings = 0;
            if (ViewState["LstReserveMapping"] != null)
            {
               // j = ((List<LobReserveMappings>)ViewState["LstReserveMapping"]).Count(c => c.sLOB == ViewState["LOB"].ToString());
                iAvailableMappings = ((List<LobReserveMappings>)ViewState["LstReserveMapping"]).Count(c => c.sLOB == ViewState["LOB"].ToString());
            }
            //rsushilaggar MITS 21553 DATE 20-Sep-2010
            if (ViewState["ReserveMappings"] != null)
            {
                DataTable dt2 = ((DataSet)ViewState["LOBList"]).Tables[0];
                string sLobCode = string.Empty;
                foreach (DataRow dsRow in dt2.Rows)
                {
                    if (dsRow["SHORT_CODE"].ToString() == lstLOB.SelectedValue)
                    {
                        sLobCode = dsRow["CODE_ID"].ToString();
                        break;
                    }
                }
                DataTable dt1 = ((DataSet)ViewState["ReserveMappings"]).Tables[0];
                foreach (DataRow dr in dt1.Rows)
                {
                    if (dr["LOB_CODE"].ToString() == sLobCode)
                    {
                        iSelectedMappings = iSelectedMappings + 1;
                    }
                }

            }
            //end rsushilaggar//end of merging

            if (dsReserveList != null && dsReserveList.Tables != null && dsReserveList.Tables.Count > 0)
            {//npradeepshar 03/24/2011 merged 21553 in R7
                for (int i = 0; i < 4 || i < iAvailableMappings || i < iSelectedMappings; i++)
                {
                    if (i < 4 || i < iAvailableMappings || i < iSelectedMappings)
                    {//end
                        DataRow dr = dt.NewRow();
                        dr["Bucket"] = Convert.ToChar(65 + i);
                        dr["Mapped"] = "1";
                        dr["ReserveType"] = "";
                        dt.Rows.Add(dr);
                    }
                    else
                        break;
                }
                if (dsReserveList.Tables[0].Rows.Count < 5)
                {
                    btnAddNew.Enabled = false;
                }
                else
                {
                    btnAddNew.Enabled = true;
                }

            }
            
            GV_ReserveMapping.DataSource = dt;
            GV_ReserveMapping.DataBind();
 
        }

        /// <summary>
        /// This function is used to bind the data with the drop down list for each row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GV_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            DropDownList dd = (DropDownList)row.FindControl("lstReserveType");
            //npradeepshar  21553 merge on R7
            CheckBox chkBox = (CheckBox)row.FindControl("chkMapped");
            if (dd != null)
            {
                if (ViewState["ReserveType"] != null)
                {
                    dsReserveList = (DataSet)ViewState["ReserveType"];
                    dd.DataSource = dsReserveList;
                    dd.DataTextField = dsReserveList.Tables[0].Columns[1].ToString();
                    dd.DataValueField = dsReserveList.Tables[0].Columns[1].ToString();
                    dd.DataBind();
                    //npradeepshar  21553 merge on R7 starts
                    //rsushilaggar MITS 21553 DATE 20-Sep-2010
                    DataTable dt = ((DataSet)ViewState["LOBList"]).Tables[0];
                    string sLobCode = string.Empty;
                    foreach (DataRow dsRow in dt.Rows)
                    {
                        if (dsRow["SHORT_CODE"].ToString() == lstLOB.SelectedValue)
                        {
                            sLobCode = dsRow["CODE_ID"].ToString();
                            break;
                        }
                    }
                    if (ViewState["ReserveMappings"] != null)
                    {
                        DataSet ds = ((DataSet)ViewState["ReserveMappings"]);
                        foreach (DataRow dataRow in ds.Tables[0].Rows)
                        {
                            if (row.Cells[0].Text == dataRow["BUCKET"].ToString() && sLobCode == dataRow["LOB_CODE"].ToString())
                            {
                                chkBox.Checked = true;
                                ListItem item = new ListItem(dataRow["SHORT_CODE"].ToString(), dataRow["SHORT_CODE"].ToString());
                                dd.SelectedIndex = dd.Items.IndexOf(item);
                            }
                        }
                    }
                    //end rsushilaggar
                    //npradeepshar  21553 merge on R7 ends
                }
            }

        }

        /// <summary>
        /// This function is used to add a new row in the grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewRow_Click(object sender, EventArgs e)
        {
            UpdateViewState4ReserveMappings();

            //DataTable dt = (DataTable)GridView1.DataSource;
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn();
            dc.ColumnName = "Bucket";
            dt.Columns.Add(dc);
            dc = new DataColumn();
            dc.ColumnName = "Mapped";
            dt.Columns.Add(dc);
            dc = new DataColumn();
            dc.ColumnName = "ReserveType";
            dt.Columns.Add(dc);

            int i = GV_ReserveMapping.Rows.Count;
            for (int j = 0; j < i; j++)
            {
                DataRow dr = dt.NewRow();
                dr["Bucket"] = GV_ReserveMapping.Rows[j].Cells[0].Text;
                dr["Mapped"] = GV_ReserveMapping.Rows[j].Cells[1].Text;
                dr["ReserveType"] = "";
                dt.Rows.Add(dr);
            }
            if (dt != null)
            {
                DataRow dr = dt.NewRow();
                dr["Bucket"] = Convert.ToChar(i + 65);
                dr["Mapped"] = i;
                dr["ReserveType"] = "";
                dt.Rows.Add(dr);
                GV_ReserveMapping.DataSource = dt;
                GV_ReserveMapping.DataBind();
            }

            if (ViewState["ReserveType"] != null)
            {
                dsReserveList = (DataSet)ViewState["ReserveType"];
                if (dt.Rows.Count == dsReserveList.Tables[0].Rows.Count)
                {
                    btnAddNew.Enabled = false;
                }
                else
                {
                    btnAddNew.Enabled = true;
                }
            }
        }

        /// <summary>
        /// this function is used to persist the state of the mapping grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GV_OnDataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GV_ReserveMapping.Rows)
            {
                if (ViewState["LstReserveMapping"] != null)
                {
                    LobReserveMappings obj = ((List<LobReserveMappings>)ViewState["LstReserveMapping"]).Find(c => c.sLOB == ViewState["LOB"].ToString() && c.iOption == (row.Cells[0].Text[0]-64));
                    if (obj != null)
                    {
                        DropDownList ddType = (DropDownList)row.FindControl("lstReserveType");
                        if (ddType != null)
                        {
                            ddType.SelectedValue = obj.sReserveType;
                        }
                        CheckBox chkbox = (CheckBox)row.FindControl("chkMapped");
                        if (chkbox != null)
                        {
                            chkbox.Checked = obj.bIsChecked;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// this function is used to put all the mappings data in the DataIntegratorModal object to save.
        /// </summary>
        /// <param name="objDIModel"></param>
        private void UpdateReserveMappingsInObject(ref Riskmaster.Models.DataIntegratorModel objDIModel)
        {
            if (ViewState["LstReserveMapping"] != null)
            {
                DataTable dt = new DataTable();
                DataColumn dc = new DataColumn();
                dc.ColumnName = "Bucket";
                dt.Columns.Add(dc);
                dc = new DataColumn();
                dc.ColumnName = "Mapped";
                dt.Columns.Add(dc);
                dc = new DataColumn();
                dc.ColumnName = "ReserveType";
                dt.Columns.Add(dc);
                dc = new DataColumn();
                dc.ColumnName = "LOB";
                dt.Columns.Add(dc);
                List<LobReserveMappings> lstReserveMappings = (List<LobReserveMappings>)ViewState["LstReserveMapping"];
                List<LobReserveMappings> lstCheckedMappings = lstReserveMappings.FindAll(c => c.bIsChecked == true);
                foreach (LobReserveMappings objReserve in lstCheckedMappings)
                {
                    DataRow dr = dt.NewRow();
                    switch (objReserve.sLOB)
                    {
                        case "GC":
                            dr["LOB"] = 241;
                            break;
                        case "VA":
                            dr["LOB"] = 242;
                            break;
                        case "WC":
                            dr["LOB"] = 243;
                            break;
                        case "DI":
                            dr["LOB"] = 844;
                            break;
                        case "PC":
                            dr["LOB"] = 845;
                            break;
                    }
                    dr["Mapped"] = objReserve.bIsChecked.ToString();
                    dr["ReserveType"] = objReserve.sReserveType;
                    dr["Bucket"] = (Convert.ToChar(objReserve.iOption+64)).ToString();
                    dt.Rows.Add(dr);
                }

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);

                objDIModel.dsReserveMappings = ds;
            }
        }

        /// <summary>
        /// This function is used to update the view state for having updated mappings data .
        /// </summary>
        private void UpdateViewState4ReserveMappings()
        {
            List<LobReserveMappings> lstLobReserveMappings = new List<LobReserveMappings>();
            if (ViewState["LOB"] != null)
            {
                if (ViewState["LstReserveMapping"] != null)
                {
                    lstLobReserveMappings = (List<LobReserveMappings>)ViewState["LstReserveMapping"];
                    lstLobReserveMappings.RemoveAll(c => c.sLOB == ViewState["LOB"].ToString());
                }
                if (GV_ReserveMapping.Rows != null && GV_ReserveMapping.Rows.Count > 0)
                {
                    foreach (GridViewRow row in GV_ReserveMapping.Rows)
                    {
                        LobReserveMappings objReserve = new LobReserveMappings();
                        objReserve.sLOB = ViewState["LOB"].ToString();
                        objReserve.iOption = Convert.ToInt32(row.Cells[0].Text[0]) - 64;
                        DropDownList ddType = (DropDownList)row.FindControl("lstReserveType");
                        if (ddType != null)
                        {
                            objReserve.sReserveType = ddType.SelectedValue;
                        }
                        CheckBox chkbox = (CheckBox)row.FindControl("chkMapped");
                        if (chkbox != null)
                        {
                            objReserve.bIsChecked = chkbox.Checked;
                        }
                        lstLobReserveMappings.Add(objReserve);
                    }
                }
                ViewState["LstReserveMapping"] = lstLobReserveMappings;
            }
        }
    }

    /// <summary>
    /// Author- rsushilaggar 28/06/2010
    /// This is a temp class. It has been created to hold the mappings data b/w postbacks.
    /// </summary>
    [Serializable]
    public class LobReserveMappings
    {
        public string sLOB { get; set; }
        public bool bIsChecked { get; set; }
        public string sReserveType { get; set; }
        public int iOption { get; set; }
    }
}
