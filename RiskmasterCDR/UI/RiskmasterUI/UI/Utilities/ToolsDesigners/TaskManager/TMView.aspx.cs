﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class TMView : NonFDMBasePageCWS
    {
        private string pageID = RMXResourceProvider.PageId("TMView.aspx"); //jira 31
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                XElement objTempElement = null;
                string sReturn = string.Empty;
                string PageID = RMXResourceProvider.PageId("TMView.aspx");//MITS 33942 ksahu5
                int iTotalRows = 0;
                int iPageSize = 0;
                JobListGrid.LinkColumn = AppHelper.GetResourceValue(PageID, "gvHdrJobState", "0");//MITS 33942 ksahu5
                ArchJobListGrid.LinkColumn = AppHelper.GetResourceValue(PageID, "gvHdrJobState", "0");//MITS 33942 ksahu5
         
                if (hdnaction.Text == "KillTask")
                {
                    XmlTemplate = GetMessageTemplate(hdnSelectedRow.Text);
                    bReturnStatus = CallCWS("TaskManagementAdaptor.KillTask", XmlTemplate, out sReturn, false, false);

                    RefreshJobs(ref bReturnStatus, XmlTemplate, ref sReturn, ref iTotalRows, ref iPageSize);
                }

                if (hdnaction.Text == "Refresh")
                {
                    XmlTemplate = GetMessageTemplate(hdnSelectedRow.Text);

                    RefreshJobs(ref bReturnStatus, XmlTemplate, ref sReturn, ref iTotalRows, ref iPageSize);
                    hdnRefresh.Text = "true";
                }

                if (!IsPostBack)
                {
                    XmlTemplate = GetMessageTemplate(hdnSelectedRow.Text);

                    RefreshJobs(ref bReturnStatus, XmlTemplate, ref sReturn, ref iTotalRows, ref iPageSize);
                    hdnRefresh.Text = "true";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void RefreshJobs(ref bool bReturnStatus, XElement XmlTemplate, ref string sReturn, ref int iTotalRows, ref int iPageSize)
        {
            bReturnStatus = CallCWS("TaskManagementAdaptor.GetJobs", XmlTemplate, out sReturn, false, true);

            iTotalRows = Conversion.ConvertObjToInt(hdTotalRows.Text);
            iPageSize = Conversion.ConvertObjToInt(hdPageSize.Text);

            hdCurrentPage.Value = hdPageNumber.Text;

            if (iTotalRows == 0)
            {
                lnkFirstTop.Visible = false;
                lnkLastTop.Visible = false;
                lnkPrevTop.Visible = false;
                lnkNextTop.Visible = false;
            }
            else
            {
                PagerSettings(iTotalRows, iPageSize);
                // Page specific customization.
                CustomizePage();
            }
        }

        private void PagerSettings(int iTotalRows, int iPageSize)
        {
            //lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Text;
            lblPageRangeTop.Text = AppHelper.GetResourceValue(this.pageID, "lblPage1", "0") + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + hdTotalPages.Text;//jira 31
            if (iPageSize > iTotalRows) // There is only 1 page.
            {
                //lblPagerTop.Text = 1 + " - " + iTotalRows + " of " + iTotalRows + " records ";
                lblPagerTop.Text = 1 + " - " + iTotalRows + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + iTotalRows + " " + AppHelper.GetResourceValue(this.pageID, "lblrecords", "0") + " ";//jira 31

                lnkFirstTop.Enabled = false;
                lnkLastTop.Enabled = false;
                lnkPrevTop.Enabled = false;
                lnkNextTop.Enabled = false;
            }
            else
            {
                //lblPagerTop.Text = 1 + " - " + iPageSize + " of " + iTotalRows + " records ";
                lblPagerTop.Text = 1 + " - " + iPageSize + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + iTotalRows + " " + AppHelper.GetResourceValue(this.pageID, "lblrecords", "0") + " ";//jira 31

                lnkFirstTop.Enabled = false;
                lnkPrevTop.Enabled = false;
            }
        }

        private int GetGridCellIndex(GridView p_grdGridView, string p_sHeaderText)
        {
            int iIndex = 0;

            for (int iHeaderCount = 0; iHeaderCount < p_grdGridView.HeaderRow.Cells.Count; iHeaderCount++)
            {
                if (p_grdGridView.HeaderRow.Cells[iHeaderCount].Text.ToLower() == p_sHeaderText.ToLower())
                {
                    iIndex = iHeaderCount;
                    break;
                }
            }

            return iIndex;
        }

        private XElement GetMessageTemplate(string sSelectedRow)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><form><SelectedRow>");
            sXml = sXml.Append(sSelectedRow);
            // ksahu5 -- MITS 33942 Start
            //sXml = sXml.Append("</SelectedRow><PageNumber/></form></Document></Message>");
            sXml = sXml.Append("</SelectedRow><LangCodeID>");
            sXml = sXml.Append(AppHelper.GetLanguageCode());
            sXml = sXml.Append("</LangCodeID><PageID>");
            sXml = sXml.Append(RMXResourceProvider.PageId("TMView.aspx"));
            sXml = sXml.Append("</PageID><PageNumber/></form></Document></Message>");
            // // ksahu5 -- MITS 33942 Start End
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void LinkCommand_Click(object sender, CommandEventArgs e)
        {
            XElement objTempElement = null;
            XElement objXmlIn = null;
            XElement objResultXml = null;
            string sSortColumn = string.Empty;
            string sSortOrder = string.Empty;
            int iCurrentPage = 0;
            int iPreviousPage = 0;
            int iPageSize = Conversion.ConvertObjToInt(hdPageSize.Text);
            int iTotalRows = Conversion.ConvertObjToInt(hdTotalRows.Text);

            string sReturn = string.Empty;
            bool bReturn = false;
            //if (Request.Form["hdCriteriaXml"] != null)
            //    objXmlCriteria = XElement.Parse(Server.HtmlDecode(Request.Form["hdCriteriaXml"]));

            //if (Request.Form["hdSortColumn"] != null)
            //    sSortColumn = Request.Form["hdSortColumn"];

            //if (Request.Form["hdSortOrder"] != null)
            //    sSortOrder = Request.Form["hdSortOrder"];

            hdCurrentPage.Value = hdPageNumber.Text;

            objXmlIn = GetMessageTemplate(hdnSelectedRow.Text);

            lnkFirstTop.Enabled = true;
            lnkLastTop.Enabled = true;
            lnkPrevTop.Enabled = true;
            lnkNextTop.Enabled = true;

            switch (e.CommandName.ToUpper())
            {
                case "FIRST":
                    {
                        iCurrentPage = 0;

                        //lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Text;
                        lblPageRangeTop.Text = AppHelper.GetResourceValue(this.pageID, "lblPage1", "0") + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + hdTotalPages.Text;//jira 31

                       // lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " of " + iTotalRows + " records ";
                        lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + iTotalRows + " " + AppHelper.GetResourceValue(this.pageID, "lblrecords", "0") + " ";
                        
                        lnkFirstTop.Enabled = false;
                        lnkPrevTop.Enabled = false;

                        objTempElement = objXmlIn.XPathSelectElement("./Document/form/PageNumber");
                        objTempElement.Value = "1";
                        break;
                    }
                case "LAST":
                    {
                        iCurrentPage = Conversion.ConvertStrToInteger(hdTotalPages.Text) - 1;


                        //lblPageRangeTop.Text = "Page " + hdTotalPages.Text + " of " + hdTotalPages.Text;
                        lblPageRangeTop.Text = AppHelper.GetResourceValue(this.pageID, "lblPage", "0") + " " + hdTotalPages.Text + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + hdTotalPages.Text;
                        //lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " records ";
                        lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + iTotalRows + " " + AppHelper.GetResourceValue(this.pageID, "lblrecords", "0") + " ";
                        
                        lnkLastTop.Enabled = false;
                        lnkNextTop.Enabled = false;

                        objTempElement = objXmlIn.XPathSelectElement("./Document/form/PageNumber");
                        objTempElement.Value = hdTotalPages.Text;
                        break;
                    }
                case "PREV":
                    {
                        iCurrentPage = Conversion.ConvertStrToInteger(hdCurrentPage.Value);

                        iPreviousPage = iCurrentPage - 1;

                        if (iPreviousPage == 1)
                        {
                            lnkFirstTop.Enabled = false;
                            lnkPrevTop.Enabled = false;
                        }

                       // lblPageRangeTop.Text = "Page " + iPreviousPage + " of " + hdTotalPages.Text;
                        lblPageRangeTop.Text = AppHelper.GetResourceValue(this.pageID, "lblPage", "0") + " " + iPreviousPage + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + hdTotalPages.Text;
                        //lblPagerTop.Text = (((iPreviousPage - 1) * iPageSize) + 1) + " - " + ((((iPreviousPage - 1) * iPageSize) + iPageSize)) + " of " + iTotalRows + " records ";
                        lblPagerTop.Text = (((iPreviousPage - 1) * iPageSize) + 1) + " - " + ((((iPreviousPage - 1) * iPageSize) + iPageSize)) + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + iTotalRows + " " + AppHelper.GetResourceValue(this.pageID, "lblrecords", "0") + " ";


                        objTempElement = objXmlIn.XPathSelectElement("./Document/form/PageNumber");
                        objTempElement.Value = Conversion.ConvertObjToStr(iCurrentPage - 1);
                        break;
                    }
                case "NEXT":
                    {
                        iCurrentPage = Conversion.ConvertStrToInteger(hdCurrentPage.Value);

                        if ((iCurrentPage + 1) == Conversion.ConvertObjToInt(hdTotalPages.Text))
                        {
                            lnkLastTop.Enabled = false;
                            lnkNextTop.Enabled = false;
                        }

                        //lblPageRangeTop.Text = "Page " + (iCurrentPage + 1) + " of " + hdTotalPages.Text;
                        lblPageRangeTop.Text = AppHelper.GetResourceValue(this.pageID, "lblPage", "0") + " " + (iCurrentPage + 1) + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + hdTotalPages.Text;

                        if (((iCurrentPage * iPageSize) + iPageSize) > iTotalRows) // Last Page
                        {
                            //lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " of " + iTotalRows + " records ";
                            lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + iTotalRows + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + iTotalRows + " " + AppHelper.GetResourceValue(this.pageID, "lblrecords", "0") + " ";
                        }
                        else
                        {
                            //lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " of " + iTotalRows + " records ";
                            lblPagerTop.Text = ((iCurrentPage * iPageSize) + 1) + " - " + (((iCurrentPage * iPageSize) + iPageSize)) + " " + AppHelper.GetResourceValue(this.pageID, "lblof", "0") + " " + iTotalRows + " " + AppHelper.GetResourceValue(this.pageID, "lblrecords", "0") + " ";
                        }

                        objTempElement = objXmlIn.XPathSelectElement("./Document/form/PageNumber");
                        objTempElement.Value = Conversion.ConvertObjToStr(iCurrentPage + 1);
                        break;
                    }
            }

            bReturn = CallCWS("TaskManagementAdaptor.GetJobs", objXmlIn, out sReturn, false, true);

            // Page specific customization.
            CustomizePage();
        }

        private void CustomizePage()
        {
            //ksahu5 - ML Changes - MITS 33942 -PageId and Lang ID 
            string sPageID = string.Empty;
            sPageID = RMXResourceProvider.PageId("TMView.aspx");
            //MITS 33942 end

            const string DATA_INTEGRATOR_COLUMN = "dataintegratorjob_Hidden";
            const string JOB_STATE_ID_COLUMN = "jobstateid_Hidden";
            const string JOB_STATE_COLUMN = "Job State";
            //const string JOB_FILES_COLUMN = "Job Files";//ksahu5 MITS 33942
            string JOB_FILES_COLUMN = RMXResourceProvider.GetSpecificObject("gvHdrJobFiles", sPageID, "0");//ksahu5 MITS 33942



           

            GridView objArchJobList = (GridView)ArchJobListGrid.GridView;
            GridView objJobList = (GridView)JobListGrid.GridView;

            bool bJobFiles = false;

            if (GetGridCellIndex(objArchJobList, JOB_FILES_COLUMN) > 0)
            {
                bJobFiles = true;
            }

            if (!bJobFiles)
            {
                BoundField objJobFiles = new BoundField();
                //objJobFiles.HeaderText = "Job Files";//ksahu5 MITS 33942
                objJobFiles.HeaderText = RMXResourceProvider.GetSpecificObject("gvHdrJobFiles", sPageID, "0");//ksahu5 MITS 33942
                objArchJobList.Columns.Add(objJobFiles);
                objArchJobList.DataBind();
                objArchJobList.HeaderRow.Cells[objArchJobList.Columns.Count - 1].Wrap = false;
            }

            bool bIsDataIntegratorJob = false;
            string sJobStateId = string.Empty;
            bool bIndexRetrieved = false;
            int iDataIntegratorCellIndex = 0;
            int iJobStateIdCellIndex = 0;
            int iJobStateCellIndex = 0;
            int iJobFilesCellIndex = 0;

            for (int iCount = 0; iCount < objArchJobList.Rows.Count; iCount++)
            {
                objArchJobList.Rows[iCount].Cells[objArchJobList.Columns.Count - 1].Wrap = false;

                // Retreive the index for the data integrator and job id columns, we need to make
                // the calls below just once.
                if (!bIndexRetrieved)
                {
                    iDataIntegratorCellIndex = GetGridCellIndex(objArchJobList, DATA_INTEGRATOR_COLUMN);
                    iJobStateIdCellIndex = GetGridCellIndex(objArchJobList, JOB_STATE_ID_COLUMN);
                    iJobStateCellIndex = GetGridCellIndex(objArchJobList, JOB_STATE_COLUMN);
                    iJobFilesCellIndex = GetGridCellIndex(objArchJobList, JOB_FILES_COLUMN);
                    bIndexRetrieved = true;
                }

                bIsDataIntegratorJob = Conversion.ConvertObjToBool(objArchJobList.Rows[iCount].Cells[iDataIntegratorCellIndex].Text);

                sJobStateId = objArchJobList.Rows[iCount].Cells[iJobStateIdCellIndex].Text;

                switch (sJobStateId)
                {
                    case "1":
                    case "2":
                    case "5":
                        objArchJobList.Rows[iCount].Cells[iJobStateCellIndex].ForeColor = System.Drawing.Color.Blue;
                        break;
                    case "3":
                        objArchJobList.Rows[iCount].Cells[iJobStateCellIndex].ForeColor = System.Drawing.Color.Green;
                        break;
                    case "4":
                    case "6":
                    case "7":
                    case "8":
                        objArchJobList.Rows[iCount].Cells[iJobStateCellIndex].ForeColor = System.Drawing.Color.Red;
                        break;
                    default:
                        break;
                }

                if (bIsDataIntegratorJob)
                {
                    //objArchJobList.Rows[iCount].Cells[iJobFilesCellIndex].Text = "<div align='center'><input type='image' name='jobfiles' id='jobfiles' onclick='GetJobFiles();' src='../../../../Images/tb_attach_mo.png' alt='Job Files' style='height:28px;width:28px;border-width:0px;' /></div>";
                    objArchJobList.Rows[iCount].Cells[iJobFilesCellIndex].Text = String.Format("<div align='center'><input type='image' name='jobfiles' id='jobfiles' onclick='GetJobFiles({0});' src='../../../../Images/tb_ziplock_active.png' alt='Job Files' style='height:28px;width:28px;border-width:0px;' /></div>", objArchJobList.Rows[iCount].Cells[13].Text);
                }
            }

            bIndexRetrieved = false;

            for (int iCount = 0; iCount < objJobList.Rows.Count; iCount++)
            {
                if (!bIndexRetrieved)
                {
                    iJobStateIdCellIndex = GetGridCellIndex(objJobList, JOB_STATE_ID_COLUMN);
                    iJobStateCellIndex = GetGridCellIndex(objJobList, JOB_STATE_COLUMN);
                    bIndexRetrieved = true;
                }

                sJobStateId = objJobList.Rows[iCount].Cells[iJobStateIdCellIndex].Text;

                switch (sJobStateId)
                {
                    case "1":
                    case "2":
                    case "5":
                        objJobList.Rows[iCount].Cells[iJobStateCellIndex].ForeColor = System.Drawing.Color.Blue;
                        break;
                    case "3":
                        objJobList.Rows[iCount].Cells[iJobStateCellIndex].ForeColor = System.Drawing.Color.Green;
                        break;
                    case "4":
                    case "6":
                    case "7":
                        objJobList.Rows[iCount].Cells[iJobStateCellIndex].ForeColor = System.Drawing.Color.Red;
                        break;
                    default:
                        break;
                }
            }
        }

    }
}
