<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ISOSetting.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.ISOMappingClass" EnableViewState="true"
    ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%--ipuri Mits:30917 Start--%>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%--ipuri Mits:30917 End--%>

<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%--ipuri Mits:30917 Start--%>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%--ipuri Mits:30917 End--%>

<%--npadhy RMACLOUD-2418 - ISO Compatible with Cloud--%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ISO Data Integrator Settings</title>
    <link href="../../../../Content/dhtml-div.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
       <%--vkumar258 - RMA-6037 - Starts --%>
<%-- <script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js">{var i;}  </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}  </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}  </script>--%>
<link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/DataIntegrator.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/utilities.js"></script>
    <%--npadhy RMACLOUD-2418 - ISO Compatible with Cloud--%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>
    <%--ipuri Mits:30917 Start--%>
    <script src="../../../../Scripts/cul.js" type="text/javascript"></script>
    <%--ipuri Mits:30917 End--%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/tmsettings.js"></script>
    <%--<script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js"></script>   <%-- ipuri Mits:34373--%>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            text-align: left;
            width: 779px;
        }

        .style3
        {
            width: 203px;
        }

        .style4
        {
            width: 260px;
        }

        .styleEmptyTd
        {
            width: 150px;
        }

        .styleEmpty
        {
            width: 225px;
        }

        .auto-style2
        {
            font-weight: bold;
            text-align: left;
            width: 234px;
        }

        .auto-style3
        {
            width: 234px;
        }

        .auto-style4
        {
            width: 177px;
        }

        .auto-style5
        {
            width: 202px;
        }

        .auto-style6
        {
            font-weight: bold;
            text-align: left;
            width: 624px;
        }

        .auto-style7
        {
            width: 524px;
        }
        .sameline {
    display: inline;
}
    </style>
        <%--ipuri Mits:30917 Start--%>
        <script language="javascript" type="text/javascript">

            <%-- ipuri Mits:34373 Start--%>
            $('document').ready(function () {
                EnableDisableStates();

                //if ($('#CheckBox22')) {
                //    if ($('#CheckBox22').is(':checked')) {
                //        $('#chkExcludeStates').removeAttr('disabled');
                //        $('#chkIncludeStates').removeAttr('disabled');
                //    }
                //    else {
                //        $('#chkExcludeStates').attr('disabled', true);
                //        $('#chkIncludeStates').attr('disabled', true);
                //    }
                //}
            });
            <%-- ipuri Mits:34373 End--%>

            function SetValueToHidden() {
                document.getElementById('hdntxtClaimMatchDocText').value = document.getElementById('txtClaimMatchDocText').value;
            }
            function ChkSubmissorUncheck() {
                alert("This field is mandatory.");
                var SubmissorAdjuster = document.getElementById('rbtSubmissorAdjuster');
                if (SubmissorAdjuster.checked == true) {
                    document.getElementById('hdSROtherUserID').value = "";
                    document.getElementById('hdSROtherUserName').value = "";
                }
            }

            function OnSave() {
                var chkIndex = document.getElementById('chkIndex');
                var chkError = document.getElementById('chkError');
                if (chkIndex.checked == false && chkError.checked == false) {
                    alert("Please select ISO Claim Match Report or ISO Submission Rejection Report.");
                }
            }
            function SetImport() {
                var hdISOType = document.getElementById('hdISOType');
                hdISOType.value = "";
                hdISOType.value = "Import";
            }

            function setFile() {
                if (document.getElementById('FUIndexFile').value != '') {
                    document.getElementById('txtIndexFile').value =
                    document.getElementById('FUIndexFile').value;
                    document.getElementById('txtIndexFile').disabled = false;
                }
                if (document.getElementById('FUErrorFile').value != '') {
                    document.getElementById('txtErrorFile').value =
                    document.getElementById('FUErrorFile').value;
                    document.getElementById('txtErrorFile').disabled = false;
                }
            }
            function SetListValues() {
                var hdCMOtherUserID = document.getElementById('hdCMOtherUserID');
                var hdCMOtherUserName = document.getElementById('hdCMOtherUserName');
                var hdSROtherUserID = document.getElementById('hdSROtherUserID');
                var hdSROtherUserName = document.getElementById('hdSROtherUserName');

                var CMOtherUserID = document.getElementById('ClaimMatchUserId');
                var CMOtherUserName = document.getElementById('ClaimMatchUserName');
                var SROtherUserID = document.getElementById('SubmissionUserId');
                var SROtherUserName = document.getElementById('SubmissionUserName');

                hdCMOtherUserID.value = "";
                hdCMOtherUserID.value = CMOtherUserID.value;
                hdCMOtherUserName.value = "";
                hdCMOtherUserName.value = CMOtherUserName.value;
                hdSROtherUserID.value = "";
                hdSROtherUserID.value = SROtherUserID.value;
                hdSROtherUserName.value = "";
                hdSROtherUserName.value = SROtherUserName.value;

            }

            function EnableDisableControls(CheckboxClaimMatch) {
                if (CheckboxClaimMatch.checked) {
                    document.getElementById('lblClaimMatchDocText').disabled = false;
                    document.getElementById('lblClaimMatchDocType').disabled = false;
                    document.getElementById('ddlClaimMatchDocType').disabled = false;
                    document.getElementById('txtClaimMatchDocText').disabled = false;
                } else {
                    document.getElementById('lblClaimMatchDocText').disabled = true;
                    document.getElementById('lblClaimMatchDocType').disabled = true;
                    document.getElementById('ddlClaimMatchDocType').disabled = true;
                    document.getElementById('txtClaimMatchDocText').disabled = true;
                    document.getElementById('txtClaimMatchDocText').value = '';
                    document.getElementById('ddlClaimMatchDocType').selectedIndex = 0;
                }
            }

            <%-- ipuri Mits:34373 Start--%>
            function EnableDisableStates() {
                if ($('#CheckBox22')) {
                    var chkExcludeStates = document.getElementById('chkExcludeStates');
                    var chkIncludeStates = document.getElementById('chkIncludeStates');
                    var lStates = document.getElementById('lstStates');

                    if ($('#CheckBox22').is(':checked')) {
                        if (chkExcludeStates.checked == true) {
                            document.getElementById('chkExcludeStates').disabled = false;
                            document.getElementById('chkIncludeStates').disabled = true;
                            document.getElementById('chkIncludeStates').checked = false;
                        }
                        else if (chkIncludeStates.checked == true) {
                            document.getElementById('chkIncludeStates').disabled = false;
                            document.getElementById('chkExcludeStates').disabled = true;
                            document.getElementById('chkExcludeStates').checked = false;
                        }
                        else {
                            document.getElementById('chkExcludeStates').disabled = false;
                            document.getElementById('chkIncludeStates').disabled = false;
                            //document.getElementById('lstStates').disabled = true;
                        }
                        if (chkExcludeStates.checked == true || chkIncludeStates.checked == true) {
                            document.getElementById('lstStates').disabled = false;
                        }
                        else {
                            document.getElementById('lstStates').disabled = true;
                            if (lStates != null) {
                                lStates.selectedIndex = -1;
                            }
                        }
                    }
                    else {
                        if (lStates != null) {
                            lStates.selectedIndex = -1;
                            document.getElementById('lstStates').disabled = true;
                            document.getElementById('chkExcludeStates').disabled = true;
                            document.getElementById('chkIncludeStates').disabled = true;
                        }
                    }

                }
            }

            <%-- ipuri Mits:34373 End--%>
        </script>
        <%--ipuri Mits:30917 End--%>
</head>

<body class="10pt">
    <form id="frmData" name="frmData" method="post" onMouseEnter="setFile()" runat="server"  > <%--ipuri Mits:30917--%>
    <asp:ScriptManager ID="ScriptManager1" runat="server" /> <%--ipuri Mits:30917--%>
    <%--onload="onLoad();"--%>
   <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td colspan="2">
              <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
            </tr>
        <tr height="10">
            <td>
            </td>
        </tr>
    </table>
    <div>
        <asp:HiddenField runat="server" ID="HiddenField1" Value="" />
        <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="TextBox9" />
        <div class="msgheader" id="div_formtitle" runat="server">
            <asp:Label ID="Label2" runat="server" Text="ISO Optionset" />
            <asp:Label ID="formsubtitle" runat="server" Text="" />
        </div>
        <div id="Div1" class="errtextheader" runat="server">
            <asp:Label ID="formdemotitle" runat="server" Text="" />
        </div>
    </div>
    <br />
    <table border="0">
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="NotSelected" nowrap="true" name="TABSSettings" id="TABSSettings">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" name="Settings"
                            <%--Developer - Iti : MITS 30917 : Start--%>
                                id="LINKTABSSettings"><span style="text-decoration: none">ISO Export Settings</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        
                        <td class="NotSelected" nowrap="true" name="TABSImport" id="TABSImport">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);SetImport();" name="Import"
                                id="LINKTABSImport"><span style="text-decoration: none">ISO Import Settings</span></a>
                        </td>
                        
                        <%--Developer - Iti : MITS 30917 : End--%>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSMappings" id="TABSMappings">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" name="Mappings"
                                id="LINKTABSMappings"><span style="text-decoration: none">ISO Loss Type 
                            Mappings</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSClaimantSettings" id="TABSClaimantSettings">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" name="ClaimantSettings"
                                id="LINKTABSClaimantSettings">
                            <span style="text-decoration: none; text-align: left;">Claimant Type Mappings</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSPropertyMappings" id="TABSPropertyMappings">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" name="PropertyMappings"
                                id="LINKTABSPropertyMappings"><span style="text-decoration: none">ISO Property Type
                                    Mappings</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <%--Developer - Subhendu : MITS 30839 : Start--%>
                        <%--<td class="NotSelected" nowrap="true" name="TABSOtherSettings" id="TABSOtherSettings">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" name="OtherSettings"
                                id="LINKTABSOtherSettings">
                            <span style="text-decoration: none; text-align: left;">Reset Options</span></a>
                        </td>--%>
                        <%--Developer - Subhendu : MITS 30839 : End--%>
            </td>
        </tr>
        <tr>
       <td class="NotSelected" nowrap="true" name="TABSISOClaimPartyType" id="TABSISOClaimPartyType">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" name="ISOClaimPartyType"
                                id="LINKTABSISOClaimPartyType"><span style="text-decoration: none">Additional Claimant Type</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>

            <!--mihtesham MITS 35711 Start -->
                        <td class="NotSelected" nowrap="true" name="TABSSPRoleMappings" id="TABSSPRoleMappings">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" name="SPRoleMappings"
                                id="LINKTABSSPRoleMappings"><span style="text-decoration: none">ISO Service Provider Mappings</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
            <!--mihtesham MITS 35711 End -->
                        <%-- npadhy RMACLOUD-2418 - ISO Compatible with Cloud--%>
                        <%-- We might need to move the Attchments section to a Tab. So commenting the code for now.--%>
                        <%--<td class="NotSelected" nowrap="nowrap" name="TABSAttachments" id="TABSAttachments">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" name="Attachments"
                                id="LINKTABSAttachments"><span style="text-decoration: none">Upload Attachments</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>--%>

        </tr>
                </table>
                <div class="singletopborder" 
                    style="position: relative; left: 0; top: 0; width: 800px; height: 826px;">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABSettings"
                                    id="FORMTABSettings"  style="">
                                    <tr><td>
                                        <!--<asp:Panel ID="pnlSettings" runat="server" Height="599px" Width="720px" 
                                            Font-Size="Small">-->
                                            <asp:Label ID="lblUserOption" runat="server" ForeColor="Black" 
                                                Text="Optionset Name:" Font-Size="Small" Font-Bold="True" 
                                            Font-Underline="True"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:TextBox ID="txtOptionName" runat="server" Width="219px" Font-Size="Small" 
                                                Height="18px" ></asp:TextBox>
                                            &nbsp;<asp:Label ID="Label3" runat="server" ForeColor="#FF3300" 
                                                style="font-weight: 700" Text="Optionset Name already exists..." 
                                                Visible="False"></asp:Label>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td colspan="2">
                                                        <b>Report Preparation: </b>    </td>                        </tr>
                                                <tr>
                                                    <td>
                                                        <u>ISO Assigned Customer Code:</u></td>
                                                    <td width="50%">
                                                        <asp:TextBox ID="TextBox1" runat="server" Font-Size="Small" Height="18px" 
                                                            Width="80%" MaxLength="9" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Preparing Company Name:</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox2" runat="server" Font-Size="Small" Height="18px" 
                                                            Width="80%" MaxLength="40"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" Text="Use Default Insured Name:" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox3" runat="server" Font-Size="Small" Height="18px" 
                                                            Width="80%" MaxLength="70"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Address Line 1:</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox4" runat="server" Font-Size="Small" Height="18px" 
                                                            Width="80%" MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Address Line 2:</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox5" runat="server" Font-Size="Small" Height="18px" 
                                                            Width="80%" MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        City:</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox6" runat="server" Font-Size="Small" Height="18px" 
                                                            Width="80%" MaxLength="25"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        State:</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox7" runat="server" Font-Size="Small" Height="18px" 
                                                            Width="30%" MaxLength="2"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td colspan="2" style ="text-decoration: underline">
                                                        <b>Select Reporting Action:</b></td>
                                                    <td style="text-decoration: underline">
                                                        <b>Claim Status Criteria</b><b style="text-align: left">:</b></td>
                                                    <td>
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td class="">
                                                        <asp:CheckBox ID="CheckBox2" runat="server" Text="Initial Reporting" />
                                                        <br />
                                                        <asp:CheckBox ID="CheckBox3" runat="server" 
                                                            Text="Supplemental Reporting (Replacement)"  />
                                                    </td>
                                                    <td colspan="3" style="text-align: left">
                                                        <asp:CheckBox ID="CheckBox16" runat="server" Text="Open Claims" />
                                                        <br />
                                                        <asp:CheckBox ID="CheckBox17" runat="server" Text="Closed Claims" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td colspan="4">
                                                        &nbsp;&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <b style="text-align: left">To Report Loss or Injuries:</b></td>
                                                </tr>
                                                <tr>
                                                    <td class="" align="center" class="style5">
                                                        <%-- <asp:CheckBox ID="CheckBox10" runat="server" Text="General Claims(GC)" /> --%>
                                                        <span>General Claims</span>
                                                        <asp:DropDownList ID="ddlLossInjuryGC" runat="server"></asp:DropDownList>
                                                    </td>
                                                    <td style="text-align: center">
                                                        <%--<asp:CheckBox ID="CheckBox11" runat="server" 
                                                            Text="Vehicle Accident Claims(VA)" Enabled="False" />--%>
                                                        <span>Vehicle Accident Claims</span>
                                                        <asp:DropDownList ID="ddlLossInjuryVC" runat="server"></asp:DropDownList>
                                                    </td>
                                                    <td style="text-align: center">
                                                        <%--<asp:CheckBox ID="CheckBox12" runat="server" 
                                                            Text="Workers' Compensation Claims(WC)" />--%>
                                                        <span>Workers' Compensation Claims</span>
                                                        <asp:DropDownList ID="ddlLossInjuryWC" runat="server"></asp:DropDownList>
                                                    </td>
                                                    <%--Subhendu 11/01/2010 Added new checkbox for Property Claims Event Desciption(MITS 22785)--%>
                                                    <td style="text-align: center">
                                                        <%--<asp:CheckBox ID="CheckBox25" runat="server" Text="Property Claims(PC)" 
                                                            Enabled="False" />--%>
                                                         <span>Property Claims</span>
                                                        <asp:DropDownList ID="ddlLossInjuryPC" runat="server"></asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td class="style3">
                                                        &nbsp;&nbsp;</td>
                                                    <td colspan="2" style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        <b style="text-align: left">Would Like To Process Single Claim Only:</b></td>
                                                    <td colspan="2" style="text-align: left">
                                                        <b>To Report Policy Number</b><b style="text-align: left">:</b></td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        <asp:CheckBox ID="CheckBox14" runat="server" Text="Yes" 
                                                            oncheckedchanged="CheckBox14_CheckedChanged" AutoPostBack="True" />
                                                        &nbsp;&nbsp;
                                                        <asp:TextBox ID="TextBox8" runat="server" Font-Size="Small" Height="18px" 
                                                            Width="219px" MaxLength="25" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="text-align: left">
                                                        <asp:CheckBox ID="CheckBox19" runat="server" 
                                                            Text="Substitute Claim Number for Policy" Enabled="False" />
                                                    </td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td class="style1">
                                                        &nbsp;&nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="">
                                                        <b style="text-align: left">Claim Criteria:</b></td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="">
                                                        <div>
                                                        <asp:CheckBox ID="CheckBox18" runat="server" Text="Use Date of Claim Range" 
                                                            AutoPostBack="True" oncheckedchanged="CheckBox18_CheckedChanged" />
                                                        :&nbsp;<asp:Label ID="Label4" runat="server" Text="From"></asp:Label>
                                                        &nbsp;
                                                        <input type="text"  runat="server" id="txtFromDate" size="10" 
                                                            onblur="dateLostFocus(this.id);" onclick="return txtFromDate_onclick()" 
                                                            disabled="disabled" />

                                                         <%--vkumar258 - RMA-6037 - Starts --%>
                                                         <%--<input type="button"  class="DateLookupControl" id="txtCompletedOnbtn1" value="" runat="server"/>
                                                        <script type="text/javascript">
                                                            Zapatec.Calendar.setup({ inputField: "txtFromDate", ifFormat: "%m/%d/%Y", button: "txtCompletedOnbtn1" });
                                                        </script>--%>
                                                      <div id="divFromDate" runat="server" class="sameline">
                                                        <script type="text/javascript">
                                                            $(function () {
                                                                $("#txtFromDate").datepicker({
                                                                    showOn: "button",
                                                                    buttonImage: "../../../../Images/calendar.gif",
                                                                    showOtherMonths: true,
                                                                    selectOtherMonths: true,
                                                                    changeYear: true
                                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                                            });
                                                        </script></div>
                                                           <%--vkumar258 - RMA_6037- End--%>
                                                        <asp:Label ID="Label5" runat="server" Text="To"></asp:Label>
                                                        &nbsp;
                                                        <input type="text"  runat="server" id="txtToDate" size="10" 
                                                            onblur="dateLostFocus(this.id);" disabled="disabled"/>
                                                       
                                                        <%--vkumar258 - RMA-6037 - Starts --%>
                                                         <%--<input type="button"  class="DateLookupControl" id="txtCompletedOnbtn2" value="" runat=server/>
                                                        <script type="text/javascript">
                                                            Zapatec.Calendar.setup({ inputField: "txtToDate", ifFormat: "%m/%d/%Y", button: "txtCompletedOnbtn2" });
                                                        </script>--%>
                                                       <div id="divToDate" runat="server" class="sameline">
                                                        <script type="text/javascript">
                                                            $(function () {
                                                                $("#txtToDate").datepicker({
                                                                    showOn: "button",
                                                                    buttonImage: "../../../../Images/calendar.gif",
                                                                    showOtherMonths: true,
                                                                    selectOtherMonths: true,
                                                                    changeYear: true
                                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                                            });
                                                        </script>
                                                           </div>
                                                            </div>
                                                           <%--vkumar258 - RMA_6037- End--%>
                     
                                                    </td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                            <table style="width:100%; height: 145px;">
                                                <tr>
                                                    <td class="auto-style6">
                                                        &nbsp;&nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style7" style="text-decoration: underline">
                                                        <b style="text-align: left">Line of Business Criteria:</b></td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style7">
                                                        <asp:CheckBox ID="CheckBox20" runat="server" Text="General Claims" 
                                                            AutoPostBack="True" oncheckedchanged="CheckBox20_CheckedChanged" />
                                                    </td>
                                                    <td style="text-align: left">
                                                        <asp:CheckBox ID="CheckBox21" runat="server" 
                                                            Text="Vehicle Accident Claims" AutoPostBack="True" 
                                                            oncheckedchanged="CheckBox21_CheckedChanged" Enabled="False" />
                                                    </td>
                                                    <td style="width:1px"></td>
                                                    <td style="text-align: left">
                                                        <asp:CheckBox ID="CheckBox22" runat="server" 
                                                            Text="Workers' Compensation Claims" AutoPostBack="True" 
                                                            oncheckedchanged="CheckBox22_CheckedChanged" />
                                                    </td>
                                                    <td style="width:1px"></td>
                                                    <%--Subhendu 11/01/2010 Added new checkbox for Property Claims -Claim Types(MITS 22785)--%>
                                                    <td style="text-align: left">
                                                        <asp:CheckBox ID="Chkbox_propClaim" runat="server" AutoPostBack="True" 
                                                            oncheckedchanged="PropClaims_CheckedChanged" Text="Property Claims" 
                                                            Enabled="False" />
                                                    </td>
                                                    <td style="width:1px"></td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style7">
                                                        <asp:ListBox ID="ListBox1" runat="server" Width="181px" 
                                                            SelectionMode="Multiple" ></asp:ListBox>
                                                    </td>
                                                    <td style="text-align: left">
                                                        <asp:ListBox ID="ListBox2" runat="server" Width="181px" 
                                                            SelectionMode="Multiple" Enabled="False" ></asp:ListBox>
                                                    </td>
                                                    <td style="width:1px"></td>
                                                    <td style="text-align: left">
                                                        <asp:ListBox ID="ListBox3" runat="server" Width="181px" 
                                                            SelectionMode="Multiple" ></asp:ListBox>
                                                    </td>
                                                    <td style="width:1px"></td>
                                                    <%--Subhendu 11/01/2010 Added new ListBox for Property Claims(MITS 22785)--%>
                                                     <td style="text-align: left">
                                                        <asp:ListBox ID="ListBox4" runat="server" Width="181px" 
                                                            SelectionMode="Multiple" Enabled="False" ></asp:ListBox>
                                                    </td>
                                                    <td style="width:1px"></td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style6">
                                                        &nbsp;&nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style6" valign="top">
                                                        <asp:CheckBox ID="chkExcludeStates" runat="server" Text="Exclude States:" onclick ="EnableDisableStates();" />
                                                            <%--ipuri Mits:34373--AutoPostBack="True" oncheckedchanged="chkExcludeStates_CheckedChanged" />--%>
                                                        <br />
                                                        <br />
                                                         <%--ssahoo24 Added new checkbox for Include states(MITS 20512 and 22524)--%>
                                                        <asp:CheckBox ID="chkIncludeStates" runat="server" Text="Include States:" onclick ="EnableDisableStates();"/>
                                                            <%--ipuri Mits:34373 --oncheckedchanged="chkExcludeStates_CheckedChanged" AutoPostBack="True" --%>
                                                            
                                                    </td>
                                                    <td style="text-align: left">
                                                        <asp:ListBox ID="lstStates" runat="server" SelectionMode="Multiple" 
                                                            Width="100%"></asp:ListBox>
                                                    </td>
                                                    <%--ssahoo24 Added new checkbox for eligible ISO submission--%>
                                                    <%--ipuri Mits 34500
                                                     <td style="text-align: left" valign="top">
                                                        <asp:CheckBox ID="ChkEligibleISOSub" runat="server" Font-Bold="True" 
                                                            Text="Eligible for ISO Submission" />
                                                    </td> --%>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td class="auto-style6">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        <%--ipuri start 12/09 MITS-34500--%>
                                        <table width="100%">
                                            <tr>
                                                <td style="text-align: left;width:33%" class="auto-style3">
                                                        <asp:CheckBox ID="ChkEligibleISOSub" runat="server" Font-Bold="True" 
                                                            Text="Eligible for ISO Submission" />
                                                 </td>
                                                <td style="text-align: left;width:33%" class="auto-style4">
                                                    <asp:Checkbox ID="ChkCMSReporting" runat="server" Font-BOld="True" Text="Report to CMS" />
                                                </td>
                                                <td style="text-align: left;width:33%" class="auto-style5">
                                                        <asp:Checkbox ID="ChkRequestRecalInfoIndicator" runat="server" Font-Bold="True" Text="Search NHTSA for Recalls" />
                                                </td>
                                            </tr>
                                            <tr>
                                                 <%--agupta298 PMC GAP08 RMA#5500 --%>

                                                    <td style="text-align: left;width:33%" class="auto-style2">
                                                        <asp:CheckBox ID="ChkIncludeNMVTIS" runat="server" Font-Bold="True" Text="Include NMVTIS" /></td>
                                                    <td colspan="3" style="text-align: left;width:66%" class="auto-style5">
                                                        <b>NMVTIS Reporting ID:</b>&nbsp;<asp:TextBox ID="txtNMVTISReportingID" runat="server" Font-Size="Small" Height="18px" Width="50%" MaxLength="40"/></td>
                                                    
                                                    <%--<td style="text-align: left" class="auto-style5">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>--%>

                                                 <%--agupta298 PMC GAP08 RMA#5500 --%>
                                                </tr>
                                        </table>
                                        <table>
                                                <tr>
                                                    <td class="" >
                                                        <b style="text-align:left">Search Party in CSLN/OCSE database indicator: </b>
                                                        <br />
                                                        <asp:DropDownList ID="ddSearchParty" runat="server">
                                                        <asp:ListItem Selected="True" Text="--Select--" Value="--Select--"></asp:ListItem>
                                                        <asp:ListItem Text="Search party in Child Support Lien Network database" Value="Y"></asp:ListItem>
                                                        <asp:ListItem Text="Search party in Office of Child Support Enforcement database" Value="O"></asp:ListItem>
                                                        <asp:ListItem Text="Search party in both CSLN and OCSE databases" Value="B"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                             <tr>
                                                    <td class="style1">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                    <td style="text-align: left">
                                                        &nbsp;</td>
                                                </tr>
                                        </table>
                                        <%--ipuri end--%>
                                            <table style="width:100%; height: 31px;">
                                                <tr>
                                                    <td style="text-align: center">
                                                        <asp:Button ID="OK" class ="button" runat="server" style="width: 200px" 
                                                            Text="Save Settings" onclick="OK_Click" />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Button ID="Cancel" class ="button" runat="server" OnClientClick="OnCancel();return false;" Text="Cancel" 
                                                            Width="81px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        <!--</asp:Panel>-->
                                        <br />
                                    
                                    </td></tr>
                                </table>

                                <%--Developer - Iti : MITS 30917 : Start--%>
                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABImport"
                                    id="FORMTABImport" style="display:none">
                                    <tr>
                                        <td colspan="6" class="style1">
                                                       <asp:Label ID="lblOptName" runat="server" ForeColor="Black" 
                                                       Text="Optionset Name" Font-Size="Small" Font-Bold="True" 
                                                       Font-Underline="True"></asp:Label>
                                                       &nbsp;&nbsp;&nbsp;
                                                       <asp:TextBox ID="txtOptName" runat="server" Width="219px" Font-Size="Small" 
                                                       Height="18px" ></asp:TextBox>
                                                       &nbsp;
                                                       <asp:Label ID="lblOptName1" runat="server" ForeColor="#FF3300" 
                                                       style="font-weight: 700" Text="Optionset Name already exists..." 
                                                       Visible="False"></asp:Label> 
                                                       <br />
                                                       <br />
                                          </td>
                                      </tr>
                                      <tr>
                                              <td colspan="2" class="style1">
                                                 <asp:Label ID="lblFileLoc" runat="server" ForeColor="Black" 
                                                 Text="File Location" Font-Size="12" Font-Bold="True" 
                                                 ></asp:Label>
                                                  <br />
                                                  <br />
                                              </td>
                                            </tr>
                                            <tr> <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                    <asp:CheckBox ID="chkIndex" runat="server" onclick ="EnableDisableControls(this),SetListValues();" 
                                                            AutoPostBack="true" oncheckedchanged="chkIndex_CheckedChanged" TabIndex="0" />
                                                    </td>
                                                    <td>
                                                    ISO Claim Match Report
                                                    </td>
                                                    <td>
                                                     <asp:UpdatePanel ID="UpdatePanelIndex" runat="server" UpdateMode="Conditional" >
                                                 <ContentTemplate>
                                                 &nbsp;&nbsp;&nbsp;
                                                 <asp:TextBox ID="txtIndexFile" runat="server" Width="374px" enabled="false" onkeydown="return false;" 
                                                 onpaste="return false;" ></asp:TextBox>
                                                 <asp:FileUpload ID="FUIndexFile" runat="server" class="button" 
                                                         style="vertical-align:bottom" enabled="false" width="89px" TabIndex="1"/> <%--aravi5 12022--%>
                                                 </ContentTemplate>
                                                 <Triggers >
                                                <asp:AsyncPostBackTrigger ControlID="chkIndex" EventName="checkedchanged" />
                                                </Triggers>
                                                 </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    <asp:CheckBox ID="chkError" runat="server" onclick ="SetListValues();" 
                                                            AutoPostBack="true" OnCheckedChanged="chkError_CheckedChanged" TabIndex="2" />
                                                    </td>
                                                    <td>
                                                    ISO Submission Rejection Report
                                                    </td>
                                                    <td>
                                                   <asp:UpdatePanel ID="UpdatePanelError" runat="server" UpdateMode="Conditional">
                                                 <ContentTemplate>
                                                 &nbsp;&nbsp;&nbsp;
                                                 <asp:TextBox ID="txtErrorFile" runat="server" Width="374px" enabled="false" onkeydown="return false;" 
                                                 onpaste="return false;" ></asp:TextBox>
                                                 <asp:FileUpload ID="FUErrorFile" runat="server" class="button" 
                                                            style="vertical-align:bottom" enabled="false" width="89px" TabIndex="3" /> <%--aravi5 12022--%>
                                                 </ContentTemplate>
                                                 <Triggers >
                                                <asp:AsyncPostBackTrigger ControlID="chkError" EventName="CheckedChanged" />
                                                </Triggers>
                                                 </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                           </td>
                                               </tr>
                                               <tr><td> </td></tr>
                                                <%-- Developer Shubhanjali --%>
                                                <tr>
                                                    <td>
                                                        <br />
                                                        <asp:Label ID="Label1" runat="server" Text="Document Settings" Font-Size="12" Font-Bold="True"></asp:Label>
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                <td>
                                                <table>
                                                <tr>
                                                    <td class="style5"><asp:Label ID="lblClaimMatchDocType" runat="server" Text="Claim Match Report Document Type" Enabled="false"/></td>
                                                    <td class="styleEmpty"></td>
                                                    <td><asp:Label ID="lblClaimMatchDocText" runat="server" Text="Claim Match Report Document Text" Enabled="false"/></td>
                                                    
                                                </tr>                                                
                                                <tr>
                                                    <td class="style5"><asp:DropDownList ID="ddlClaimMatchDocType"  runat="server" Width="219px" Enabled="false"></asp:DropDownList></td>                                                    
                                                    <td class="styleEmpty"></td>
                                                    <td><asp:TextBox ID="txtClaimMatchDocText" runat="server" Width="219px" 
                                                            Font-Size="Small" onblur="SetValueToHidden();"
                                                       Height="18px" style="margin:0" Enabled="false"></asp:TextBox></td>
                                                    
                                                </tr>
                                                <tr><td class="style5"><asp:HiddenField ID="hdntxtClaimMatchDocText" runat="server" /></td></tr>                                                                                                                                            
                                                </table>
                                                </td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                  <br />
                                                   <asp:Label ID="lblProcessISO" runat="server" Text="Diary Settings" 
                                                   Font-Size="12" Font-Bold="True"></asp:Label>
                                                   <br />
                                                   <br />
                                             
                                                 </td>
                                               </tr> 
                                               <tr> 
                                                 <td>
                                                  <asp:UpdatePanel ID="UpdatePanelLOB" runat="server" UpdateMode="Always">
                                                 <ContentTemplate>
                                               <table>
                                               <%--<asp:UpdatePanel ID="UpdatePanelLOB" runat="server" UpdateMode="Always">
                                                 <ContentTemplate>--%>
                                              <tr> 
                                                 <td>
                                                 <asp:CheckBox ID="chkClaimMatch"  name='chkClaimMatch' runat="server"  
                                                         AutoPostBack="true" oncheckedchanged="ClaimMatch_CheckedChanged" onclick="SetListValues();"
                                                         Text="Create Diary for Claim Match Reports" Enabled= "false" 
                                                         TabIndex="4" style="margin:0"/>
                                                 </td>
                                                 <td class="styleEmptyTd"></td>
                                                 <td>
                                                 <asp:CheckBox ID="chkboxSubmissor" name='chkboxSubmissor' runat="server" 
                                                         Text="Create Diary for Submisson Rejections" Enabled= "false" 
                                                         AutoPostBack="true" oncheckedchanged="Submissor_CheckedChanged" 
                                                         onclick="SetListValues();" TabIndex="7" style="margin:0"/>
                                                 </td>
                                              </tr>
                                              <tr> <td></td></tr>
                                              <tr>
                                                 <td>
                                                     <asp:RadioButton ID="rbtClaimAdjuster" GroupName="GP2" runat="server" 
                                                         onclick ="SetListValues();" AutoPostBack="True" Text="Adjuster" 
                                                         Enabled= "false" oncheckedchanged="GP2_CheckedChanged" TabIndex="5" />
                                                 </td>
                                                 <td class="styleEmptyTd"></td>
                                                 <td>
                                                     <asp:RadioButton ID="rbtSubmissorAdjuster" GroupName="GP1" runat="server" 
                                                         onclick ="SetListValues();" AutoPostBack="True" Text="Adjuster" 
                                                         Enabled= "false" oncheckedchanged="GP1_CheckedChanged" TabIndex="8" style="margin:0" />
                                                 </td>
                                               </tr>
                                                 <tr><td></td></tr>
                                               <tr> 
                                                  <td>
                                                    <asp:RadioButton ID="rbtClaimUser" GroupName="GP2" runat="server" 
                                                          Text="Other User(s)" Enabled= "false"
                                                    Font-Bold="True" onclick ="SetListValues();" AutoPostBack="True" 
                                                          oncheckedchanged="GP2_CheckedChanged" TabIndex="6" style="margin:0"/>
                                                  </td>
                                                  <td class="styleEmptyTd"></td>
                                                  <td>
                                                     <asp:RadioButton ID="rbtSubmissorUser" GroupName="GP1" runat="server" 
                                                          Text="Other User(s)" Enabled= "false"
                                                     Font-Bold="True" onclick ="SetListValues();" AutoPostBack="True" 
                                                          oncheckedchanged="GP1_CheckedChanged" TabIndex="9" style="margin:0"/>
                                                  </td>
                                               </tr>
                                               <tr><td></td></tr>
                                               <%--</ContentTemplate>
                                                <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="chkError" EventName="CheckedChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="chkIndex" EventName="CheckedChanged" />
                                                </Triggers>
                                                </asp:UpdatePanel>--%>
                                               <tr> 
                                                    
                                                    <td>
                                                    <select multiple="true" id="lstClaimOtherUser" size="3" runat="server" style="width: 190px;
                                                            height: 71px" disabled="true">
                                                        </select>
                                                        <input type="button" class="CodeLookupControl" value="" runat="server" id="AutoFillUseridbtn1" onclick="AddCustomizedListUser('weblinksetup', 'lstClaimOtherUser', 'ClaimMatchUserId', 'ClaimMatchUserName')"  disabled="true" />
                                                        <input type="button" class="BtnRemove" value="" runat="server" id="OtherUseridbtndel1" style="width: 21px"
                                                            onclick="DelCustomizedListUser('weblinksetup', 'lstClaimOtherUser', 'ClaimMatchUserId', 'ClaimMatchUserName')" disabled="true" />
                                                        <asp:TextBox Style="display: none" runat="server"  ID="ClaimMatchUserId"
                                                            Text="" rmxref="Instance/Document/form/group/WebLink/SelectedUsersID" rmxtype="hidden" />
                                                        <asp:TextBox Style="display: none" runat="server"  ID="ClaimMatchUserName"
                                                            Text="" rmxref="Instance/Document/form/group/WebLink/SelectedUsers" rmxtype="hidden" />
                                                      </td>
                                                      <td class="styleEmptyTd"></td>
                                                      <td>
                                                    <select multiple="true" id="lstSubmissorOtherUser" size="3" runat="server" style="width: 190px;
                                                            height: 71px" disabled="true">
                                                        </select>
                                                        <input type="button" class="CodeLookupControl" value="" runat="server" id="AutoFillUseridbtn"
                                                            onclick="AddCustomizedListUser('weblinksetup', 'lstSubmissorOtherUser', 'SubmissionUserId', 'SubmissionUserName')" disabled="true" />
                                                        <input type="button" class="BtnRemove" value="" runat="server" id="OtherUseridbtndel" style="width: 21px"
                                                            onclick="DelCustomizedListUser('weblinksetup', 'lstSubmissorOtherUser', 'SubmissionUserId', 'SubmissionUserName')" disabled="true" />
                                                        <asp:TextBox Style="display: none" runat="server"  ID="SubmissionUserId"
                                                            Text="" rmxref="Instance/Document/form/group/WebLink/SelectedUsersID" rmxtype="hidden" />
                                                        <asp:TextBox Style="display: none" runat="server"  ID="SubmissionUserName"
                                                            Text="" rmxref="Instance/Document/form/group/WebLink/SelectedUsers" rmxtype="hidden" />
                                                      </td>

                                                 </tr> 
                                                 <tr><td>&nbsp;</td></tr>
                                                 <tr>                                                    
                                                    <td><asp:Label ID="lblClaimMatchDiaryType" runat="server" Text="Claim Match Report Diary Type" Enabled="false"/></td>
                                                    <td class="styleEmptyTd"></td>
                                                    <td><asp:Label ID="lblSubRejDiaryType" runat="server" Text="Submission Rejection Diary Type" Enabled="false"/></td>                                                    
                                                 </tr>
                                                 <tr>
                                                    <td><asp:DropDownList ID="ddlClaimMatchDiaryType" runat="server" Width="219px" Enabled="false"></asp:DropDownList></td>
                                                    <td></td>
                                                    <td><asp:DropDownList ID="ddlSubRejDiaryType" runat="server" Width="219px" Enabled="false"></asp:DropDownList></td>
                                                 </tr>
                                                 <tr><td>&nbsp;</td></tr>
                                                 <tr>
                                                    <td><asp:Label ID="lblClaimMatchDiaryText" runat="server" Text="Claim Match Report Diary Text" Enabled="false"/></td>                                                    
                                                    <td></td>
                                                    <td><asp:Label ID="lblSubRejDiaryText" runat="server" Text="Submission Rejection Diary Text" Enabled="false"/></td>                                                    
                                                 </tr>
                                                 <tr >
                                                  <td><asp:TextBox ID="txtClaimMatchDiaryText" runat="server" Width="219px" Font-Size="Small" 
                                                       Height="18px" Enabled="false" MaxLength="2000" style="margin:0px"></asp:TextBox></td>                                                    
                                                    <td></td>
                                                    <td><asp:TextBox ID="txtSubRejDiaryText" runat="server" Width="219px" Font-Size="Small" 
                                                       Height="18px" Enabled="false" MaxLength="2000" style="margin:0px"></asp:TextBox></td> 
                                                 </tr>
                                                 <tr>
                                                   <td>
                                                   <br />
                                                   <asp:Label ID="Label6" runat="server" Text="Enhanced Notes Settings" 
                                                   Font-Size="12" Font-Bold="True"></asp:Label>
                                                   <br />
                                                   <br />                                             
                                                  </td>
                                                 </tr>
                                                 <tr>
                                                    <td style="margin:0">
                                                    <div style="margin:0">
                                                    <asp:CheckBox ID="chkClaimMatchForEnhanced" runat="server" onclick ="SetListValues();" 
                                                            AutoPostBack="true" oncheckedchanged="chkClaimMatchForEnhanced_CheckedChanged" TabIndex="0" Enabled= "false" Text="Create Enhanced Notes for Claim Match Reports" style="margin:0"/>
                                                    </div>
                                                    </td>                                                    
                                                    <td></td>
                                                    <td>
                                                        <asp:CheckBox ID="chkSubmissorForEnhanced" runat="server" onclick ="SetListValues();" 
                                                            AutoPostBack="true" oncheckedchanged="chkSubmissorForEnhanced_CheckedChanged" TabIndex="0" Enabled= "false" Text="Create Enhanced Notes for Submission Rejections" style="margin:0"/>
                                                    </td>
                                                 </tr>
                                                 <tr><td>&nbsp;</td></tr>
                                                 <tr>
                                                    <td><asp:Label ID="lblClaimMatchEnhancedNotesType" runat="server" Text="Claim Match Enhanced Notes Type" Enabled="false"/></td>                                                    
                                                    <td></td>
                                                    <td><asp:Label ID="lblSubRejEnhancedNotesType" runat="server" Text="Submission Rejection Enhanced Notes Type" Enabled="false"/></td>                                                    
                                                 </tr>
                                                 <tr>
                                                    <td><asp:DropDownList ID="ddlClaimMatchEnhancedNotes" runat="server" Enabled="false"></asp:DropDownList></td>
                                                    <td></td>
                                                    <td><asp:DropDownList ID="ddlSubRejEnhancedNotes" runat="server" Enabled="false"></asp:DropDownList></td>
                                                 </tr>
                                                 <tr><td>&nbsp;</td></tr>
                                                 <tr>
                                                    <td><asp:Label ID="lblClaimMatchEnhancedNotesText" runat="server" Text="Claim Match Report Enhanced Notes Text" Enabled="false"/></td>                                                    
                                                    <td></td>
                                                    <td><asp:Label ID="lblSubRejEnhancedNotestText" runat="server" Text="Submission Rejection Enhanced Notes Text" Enabled="false"/></td>                                                    
                                                 </tr>
                                                 <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtClaimMatchEnhancedNotes" runat="server" Width="219px" Font-Size="Small" 
                                                       Height="18px" Enabled="false" style="margin:0"></asp:TextBox>
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <asp:TextBox ID="txtSubRejEnhancedNotes" runat="server" Width="219px" Font-Size="Small" 
                                                       Height="18px" Enabled="false" style="margin:0"></asp:TextBox>
                                                    </td>
                                                 </tr>
                                              </table>
                                                </ContentTemplate>
                                                <%--<Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="chkError" EventName="CheckedChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="chkIndex" EventName="CheckedChanged" />
                                                </Triggers>--%>
                                                </asp:UpdatePanel>
                                                </td>
                                                </tr>
                                                <%-- npadhy RMACLOUD-2418 - ISO Compatible with Cloud--%>
                                                <%-- Provide the functionality to upload the attachments from UI. This would only be available only in case of Cloud--%>
                                                <div id="divAttachments" runat="server">
                                                   <tr>
                                                       <td>
                                                        <br />
                                                        <asp:Label ID="Label7" runat="server" Text="Upload Files as Attachments" 
                                                        Font-Size="12" Font-Bold="True"></asp:Label>
                                                        <br />
                                                        <br />                                             
                                                      </td>
                                                 </tr>
                                                    <tr>
                                                        <td>
                                                                <asp:Label ID="lbSuccess" runat="server" Visible="false" Text="The  Files have been successfully uploaded" Style="font-weight: bold;
                                                                            color: blue"></asp:Label>
       
                                                        </td>
                                                        </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="udpAttach" runat="server" UpdateMode="Conditional" >
                                                                <ContentTemplate>
                                                                    <CuteWebUI:UploadAttachments  ID="UploadDocumentAttachments" runat="server" InsertText="Click here to upload Files" 
                                                                            MultipleFilesUpload="True">
                                                                    </CuteWebUI:UploadAttachments>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <input type="hidden" value="" id="filename" runat="server">
                                                    <uc2:PleaseWaitDialog ID="pleaseWait" runat="server"  CustomMessage="Uploading"/>
                                                </div>
                                                <tr>
                                                <td>
                                                  <table style="width: 100%; height: 31px;">
                                                        <tr>
                                                            <td style="text-align: center">
                                                                <asp:Button ID="Button1" class="button" runat="server" Style="width: 100px"
                                                                    Text="Save Settings" OnClientClick="SetListValues();"
                                                                    OnClick="Save_Click" TabIndex="10" />
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                         <asp:Button ID="Button2" class="button" runat="server"
                                                             OnClientClick="OnCancel();return false;" Text="Cancel" Width="81px"
                                                                     TabIndex="11" Style="width: 100px" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%--<table style="width: 840px; height: 31px;">
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center">
                                                      <asp:Button ID="btnSaveImport" class ="button" runat="server" style="width: 100px" 
                                                                    Text="Save Settings" OnClientClick="SetListValues();" 
                                                                onclick ="Save_Click" TabIndex="10" />
                                                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                      <asp:Button ID="btnCancelImport" class ="button" runat="server" 
                                                                OnClientClick="OnCancel();return false;" Text="Cancel" Width="81px" 
                                                                TabIndex="11" />
                                                    </td>
                                                    </tr>
                                                    </table>--%>
                                                </td>
                                                </tr>
                                            </table>
                                           
                                <%--Developer - Iti : MITS 30917 : End--%>

                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABMappings"
                                    id="FORMTABMappings" style="display:none">
                                    
<%--                                    <tr><td>
                                       <table style="">
                                            <tr>--%>
                                            <tr>
                                             
                                                <td colspan="6">
               
                                                        <div>
                
            <div>
            <!-- mkaran2 : Added pagination on the ISO mapping setup grid MITS 34134-->
                <span>
                    <dg:UserControlDataGrid runat="server" ID="ISOMappingInfoGrid" GridName="ISOMappingInfoGrid"  GridTitle="Select ISO Loss Type mapping :" Target="./Document/ISOMappingList" Unique_Id="RowId" ShowRadioButton="true" Width="750px" Height="250px" hidenodes="|RowId|" ShowHeader="True" LinkColumn="" PopupWidth="515" PopupHeight="500" Type="GridAndButtons" RowDataParam="listrow"  AllowPaging="true" PageSize="25" />
                </span>
            </div>
            
                <asp:TextBox style="display:none" runat="server" id="ISOMappingInfoGridSelectedId"  RMXType="id" />
                <asp:TextBox style="display:none" runat="server" id="ISOMappingInfoGrid_RowDeletedFlag"  RMXType="id" Text="false" />
                <asp:TextBox style="display:none" runat="server" id="ISOMappingInfoGrid_Action"  RMXType="id" />
                <asp:TextBox style="display:none" runat="server" id="ISOMappingInfoGrid_RowAddedFlag"  RMXType="id" Text="false"  />
        </div>

        <%--mihtesham to be removed --%>
<%--      <div id = "div_ClaimType">
        <asp:Label runat="server" class="required" ID="lbl_ClaimType" Text="Claim Type: " Width="160px" />
           <%--<asp:ListBox ID="lstRMClaimType" runat="server" onselectedindexchanged="lstRMClaimType_SelectedIndexChanged" style="height: 70px" Width="212px" AutoPostBack="True" Height="175px" Rows="10"></asp:ListBox>--%>

                                                </td>
                                              
                                           </tr>
                                        
                                      </table>
     
                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABClaimantSettings"
                                    id="FORMTABClaimantSettings" style="display:none">
                                    <tr><td>
                                        &nbsp;</td></tr>
                                    <tr><td>
                                        <table style="">
                                            <tr>
                                                <td>
                                                    <b style="text-align: left">Claimant Type</b></td>
                                                <td>
                                                    &nbsp;</td>
                                                <td class="style4">
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="6" valign="top">
                                                            <asp:ListBox ID="lstClaimantType" runat="server" 
                                                                onselectedindexchanged="lstClaimantType_SelectedIndexChanged" style="height: 70px" 
                                                                Width="212px" AutoPostBack="True" Height="175px" Rows="10"></asp:ListBox>
                                                        </td>
                                                        <td rowspan="6">
                                                            &nbsp;</td>
                                                        <td class="" valign="top">
                                                            <b>Current Setting:</b></td>
                                                        <td valign="top">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style4" valign="top">
                                                            Mapped As:</td>
                                                        <td valign="top">
                                                            <asp:Label ID="lblClaimantType" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style4" valign="top">
                                                            &nbsp;</td>
                                                        <td valign="top">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="" valign="top">
                                                            <b>Modify Settings:</b></td>
                                                        <td valign="top">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style4" valign="top" colspan="2">
                                                            <asp:RadioButtonList ID="rbtClmtMapping" runat="server" 
                                                                onselectedindexchanged="rbtClmtMapping_SelectedIndexChanged" 
                                                                AutoPostBack="True">
                                                                <asp:ListItem>UnMapped</asp:ListItem>
                                                                <asp:ListItem>Business</asp:ListItem>
                                                                <asp:ListItem>Individual</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    </table>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td class="style4" valign="top" colspan="2">
                                            <asp:Label ID="lblClaimantStatus" runat="server" ForeColor="#FF3300" 
                                                Visible="False"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                      <td style="text-align:center">
                                          <asp:Button ID="BtnClaimantSave" class="button" runat="server" Text="Save" 
                                              Enabled="False" onclick="BtnClaimantSave_Click" />
                                          &nbsp;&nbsp;&nbsp;&nbsp;
                                          <asp:Button ID="btnClaimantCancel" class="button" runat="server" OnClientClick="OnCancel();return false;" Text="Cancel" />
                                       </td>
                                    </tr>
                                </table>
                                 <!-- sagarwal54 -->
                    <table border="0" cellspacing="0" cellpadding="0" name="FORMTABPropertyMappings"
                        id="FORMTABPropertyMappings" style="display: none">
                        <tr>
                            <td colspan="6">
                                <div>
                                    <div>
                                        <span>
                                            <dg:UserControlDataGrid runat="server" ID="ISOPopertyMappingGrid" GridName="ISOPopertyMappingGrid"
                                                GridTitle="Select ISO Property Type mapping :" Target="./Document/IsoPropertyList"
                                                Unique_Id="RowId" ShowRadioButton="true" Width="750px" Height="250px" HideNodes="|RowId|"
                                                ShowHeader="True" LinkColumn="" PopupWidth="515" PopupHeight="500" Type="GridAndButtons"
                                                RowDataParam="listrow" AllowPaging="false" PageSize="25" />
                                        </span>
                                    </div>
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOPopertyMappingGridSelectedId" RMXType="id" />
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOPopertyMappingGrid_RowDeletedFlag" RMXType="id" Text="false" />
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOPopertyMappingGrid_Action" RMXType="id" />
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOPopertyMappingGrid_RowAddedFlag" RMXType="id" Text="false" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!-- mihtesham MITS 35711 start -->
                    <table border="0" cellspacing="0" cellpadding="0" name="FORMTABSPRoleMappings"
                        id="FORMTABSPRoleMappings" >
                        <tr>
                            <td colspan="6">
                                <div>
                                    <div>
                                        <span>
                                            <dg:UserControlDataGrid runat="server" ID="ISOSPRoleMappingGrid" GridName="ISOSPRoleMappingGrid"
                                                GridTitle="Select ISO Service Provider Role mapping :" Target="./Document/ISOSPRoleList"
                                                Unique_Id="RowId" ShowRadioButton="true" Width="750px" Height="250px" HideNodes="|RowId|"
                                                ShowHeader="True" LinkColumn="" PopupWidth="515" PopupHeight="500" Type="GridAndButtons"
                                                RowDataParam="listrow" AllowPaging="false" PageSize="25" />
                                        </span>
                                    </div>
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOSPRoleMappingGridSelectedId" RMXType="id" />
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOSPRoleMappingGrid_RowDeletedFlag" RMXType="id" Text="false" />
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOSPRoleMappingGrid_Action" RMXType="id" />
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOSPRoleMappingGrid_RowAddedFlag" RMXType="id" Text="false" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!-- mihtesham MITS 35711 End -->
                    <table border="0" cellspacing="0" cellpadding="0" name="FORMTABISOClaimPartyType"
                        id="FORMTABISOClaimPartyType" style="display: none">
                        <tr>
                            <td colspan="6">
                                <div>
                                    <div>
                                        <span>
                                            <dg:UserControlDataGrid runat="server" ID="ISOClaimPartyTypeGrid" GridName="ISOClaimPartyTypeGrid"
                                                GridTitle="Select Additional Claimant Type mapping :" Target="./Document/ClaimParty"
                                                Unique_Id="RowId" ShowRadioButton="true" Width="750px" Height="250px" HideNodes="|RowId|"
                                                ShowHeader="True" LinkColumn="" PopupWidth="515" PopupHeight="500" Type="GridAndButtons"
                                                RowDataParam="listrow" AllowPaging="false" PageSize="25" />
                                        </span>
                                    </div>
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOClaimPartyTypeGridSelectedId" RMXType="id" />
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOClaimPartyType_RowDeletedFlag" RMXType="id" Text="false" />
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOClaimPartyType_Action" RMXType="id" />
                                    <asp:TextBox Style="display: none" runat="server" ID="ISOClaimPartyType_RowAddedFlag" RMXType="id" Text="false" />
                                </div>
                            </td>
                        </tr>
                    </table>
                                <%-- npadhy RMACLOUD-2418 - ISO Compatible with Cloud--%>
                        	    <%-- We might need to move the Attchments section to a Tab. So commenting the code for now.--%>
                                <%--<table border="0" cellspacing="0" cellpadding="0" name="FORMTABAttachments"
                                    id="FORMTABAttachments" style="display: none">
                                    
                                </table>--%>
                            </td>
                        </tr>

                    </table>
                    <input type="hidden" name="hTabName" id="hTabName"  runat="server"  value="Settings" />
                </div>
    <asp:HiddenField ID="hdTaskManagerXml" runat="server" />
    <asp:HiddenField ID="hdOptionsetId" runat="server" />
    <asp:HiddenField ID="hdnaction" runat="server" />
    <asp:HiddenField ID="hdOptionset" runat="server" />  <%-- mihtesham --%>
    <asp:HiddenField ID="bIsCarrier" runat="server" />
    <asp:HiddenField ID="hdClaimLOB" runat="server" />
    <%--ipuri Mits:30917 Start--%>
    <asp:HiddenField ID="hdCMOtherUserID" runat="server" />
    <asp:HiddenField ID="hdCMOtherUserName" runat="server" />
    <asp:HiddenField ID="hdSROtherUserID" runat="server" />
    <asp:HiddenField ID="hdSROtherUserName" runat="server" />
    <asp:HiddenField ID="hdISOType" runat="server" />   
    <%--ipuri Mits:30917 End--%> 
    </form>
</body>
</html>

