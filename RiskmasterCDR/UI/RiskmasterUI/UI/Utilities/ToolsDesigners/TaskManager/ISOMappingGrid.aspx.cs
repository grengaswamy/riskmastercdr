﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
//using Riskmaster.UI.DataIntegratorService;    //ipuri 12/06/2014 SOA Service discontinued
using Riskmaster.Models;    //ipuri 12/06/2014



namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    //public partial class ISOMappingGrid : System.Web.UI.Page
    public partial class ISOMappingGrid : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();
        private string m_connectionString;
        //mihtesham start
        static int iLOB;
        static int iNLOB;

        static int iPSN; //Developer – abharti5 |MITS 36676|
        static bool bPolicySystemInterface;
        // Developer - Subhendu : MITS 30839 : Start
        static string sRecord;
        static string strPolicyCodeTmp;
        // Developer - Subhendu : MITS 30839 : Start
        //mihtesham end

        //mihtesham start
        private bool m_isCarrier;
        //mihtesham end
        public string connectionString
        {
            set { m_connectionString = value; }
            get { return m_connectionString; }
        }

        private bool bLoadOnEdit { get; set; }

        private String[] arrtmp = { };
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                mode.Text = AppHelper.GetQueryStringValue("mode");

                if (AppHelper.GetQueryStringValue("isCarrier") == "-1")
                {
                    m_isCarrier = true;
                }
                else
                {
                    m_isCarrier = false;
                }
                
                if (!IsPostBack)
                {
                    if (mode.Text == "edit")
                    {
                        bLoadOnEdit = true;
                    }
                    else
                    {
                        bLoadOnEdit = false;
                    }

                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    //mihtesham start
                    if (mode.Text == "add" || bLoadOnEdit)
                    {
                        string strSelectedRowId = AppHelper.GetQueryStringValue("selectedid");
                        if (!string.IsNullOrEmpty(strSelectedRowId))
                        {
                            arrtmp = strSelectedRowId.Split('|');
                        }

                        if (m_isCarrier == true)
                        {
                            //Developer – abharti5 |MITS 36676| start
                            //PolicyLOB();
                            PolicySystemNames();
                            //Developer – abharti5 |MITS 36676| end
                        }
                        else
                        {
                            ddlPolicySystemNames.Enabled = false; //Developer – abharti5 |MITS 36676|
                            Claim_LOB();
                        }
                    }

                    //mihtesham end

               }

                else
                {

                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        string strSelectedRowId = AppHelper.GetQueryStringValue("selectedid");                        
                    }
                }
            }
            catch (Exception ee)
            {

                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        //Developer – abharti5 |MITS 36676| start
        protected void PolicySystemNames()
        {
            if (m_isCarrier == true)
            {
                //ipuri REST Service Conversion 12/06/2014 Start
                //DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
                //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                DataIntegratorModel objDIModel = new DataIntegratorModel();
                //ipuri REST Service Conversion 12/06/2014 End
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }
                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.GetPolicySystemNames(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getpolicysystemnames", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
				//ipuri REST Service Conversion 12/06/2014 End
                //sagarwal54 MITS 37006 start
                bPolicySystemInterface = objDIModel.bPolicySystemInterface;
                //sagarwal54 MITS 37006 end
                if (objDIModel.dsPolicySystemNames != null && bPolicySystemInterface == true)
                {
                    ddlPolicySystemNames.Enabled = true;
                    ddlPolicySystemNames.DataSource = objDIModel.dsPolicySystemNames;
                    ddlPolicySystemNames.DataTextField = objDIModel.dsPolicySystemNames.Tables[0].Columns[1].ToString();
                    ddlPolicySystemNames.DataValueField = objDIModel.dsPolicySystemNames.Tables[0].Columns[0].ToString();
                    ddlPolicySystemNames.DataBind();
                    ddlPolicySystemNames.Items.Insert(0, "--Select--");
                    if (bLoadOnEdit)
                    {
                        ddlPolicySystemNames.SelectedValue = arrtmp[0];
                        ddlPolicySystemNames.Enabled = false;
                        ddlPolicySystemNames_SelectedIndexChanged(this, null);
                        return;
                    }
                }
                btnSave.Enabled = false;
                ddlPolicyLOB.Enabled = false;
                ddlCoverage.Enabled = false;
                ddlISOClaimType.Enabled = false;
                ddlTypeOfLoss.Enabled = false;
                ddlDisabilityCode.Enabled = false;
                ddlISOPolicyType.Enabled = false;
                ddlISOCoverageType.Enabled = false;
                ddlISOLossType.Enabled = false;
                
                
                if (bPolicySystemInterface == false)
                {
                    ddlPolicySystemNames.Enabled = false;                  
                    ddlPolicySystemNames.Items.Insert(0, "--Select--");
                    if (bLoadOnEdit)
                    {
                        ddlPolicySystemNames.SelectedValue = arrtmp[0];
                        ddlPolicySystemNames.Enabled = false;
                        ddlPolicySystemNames_SelectedIndexChanged(this, null);
                        return;
                    }
                    else
                    {
                        ddlPolicySystemNames_SelectedIndexChanged(this, null);
                    }
                }
                
               
            }
        }




        protected void Claim_LOB()
        {
            ddlPolicyLOB.Enabled = true;
            ddlPolicyLOB.Items.Insert(0, "-- Select --");
            ddlPolicyLOB.Items.Insert(1, "GC - General Claims");
            ddlPolicyLOB.Items.Insert(2, "VA - Vehicle Accident Claims");
            ddlPolicyLOB.Items.Insert(3, "WC - Workers' Compensation");
            ddlPolicyLOB.Items.Insert(4, "PC - Property Claims");

            if (bLoadOnEdit)
            {
                switch (arrtmp[2]) //Developer – abharti5 |MITS 36676|
                {
                    case "241":
                        ddlPolicyLOB.SelectedValue = "GC - General Claims";
                        break;
                    case "242":
                        ddlPolicyLOB.SelectedValue = "VA - Vehicle Accident Claims";
                        break;
                    case "243":
                        ddlPolicyLOB.SelectedValue = "WC - Workers' Compensation";
                        break;
                    case "845":
                        ddlPolicyLOB.SelectedValue = "PC - Property Claims";
                        break;
                    default:
                        break;
                }
                ddlPolicyLOB.Enabled = false;
                ddlPolicyLOB_SelectedIndexChanged(this, null);
                ddlPolicyLOB.Enabled = false;
                return;
            }

            btnSave.Enabled = false;
            ddlCoverage.Enabled = false;
            ddlISOClaimType.Enabled = false;
            ddlTypeOfLoss.Enabled = false;
            ddlDisabilityCode.Enabled = false;
            ddlISOPolicyType.Enabled = false;
            ddlISOCoverageType.Enabled = false;
            ddlISOLossType.Enabled = false;
        }


        //Developer – abharti5 |MITS 36676| start
        protected void ddlPolicySystemNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataIntegratorServiceClient objDIService = null;
            DataIntegratorModel objDIModel = null;

            if (((m_isCarrier == true))) // ddlPolicySystemNames.SelectedIndex != 0 &&  sagarwal54
            {
                //objDIService = new DataIntegratorServiceClient();
                objDIModel = new DataIntegratorModel();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }

                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.GetPolicyLOB(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getpolicylob", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End

                if (objDIModel.dsPolicyLOB != null)
                {
                    ddlPolicyLOB.Enabled = true;
                    ddlPolicyLOB.DataSource = objDIModel.dsPolicyLOB;
                    ddlPolicyLOB.DataTextField = objDIModel.dsPolicyLOB.Tables[0].Columns[1].ToString();
                    ddlPolicyLOB.DataValueField = objDIModel.dsPolicyLOB.Tables[0].Columns[0].ToString();
                    ddlPolicyLOB.DataBind();
                    ddlPolicyLOB.Items.Insert(0, "--Select--");
                    if (bLoadOnEdit)
                    {
                        ddlPolicyLOB.SelectedValue = arrtmp[6];
                        ddlPolicyLOB.Enabled = false;
                        ddlPolicyLOB_SelectedIndexChanged(this, null);
                        return;
                    }
                }
            }

            else if (m_isCarrier == false)
            {
                //Non Carrier clients

                //objDIService = new DataIntegratorServiceClient();
                objDIModel = new DataIntegratorModel();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }

                if (ddlPolicyLOB.SelectedIndex != 0)
                {


                    if (ddlPolicyLOB.Items[1].Selected == true)
                    {
                        objDIModel.LOB = 241;
                        iNLOB = 241;
                    }
                    else if (ddlPolicyLOB.Items[2].Selected == true)
                    {
                        objDIModel.LOB = 242;
                        iNLOB = 242;
                    }
                    else if (ddlPolicyLOB.Items[3].Selected == true)
                    {
                        objDIModel.LOB = 243;
                        iNLOB = 243;
                    }
                    else if (ddlPolicyLOB.Items[4].Selected == true)
                    {
                        objDIModel.LOB = 845;
                        iNLOB = 845;
                    }
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objDIModel = objDIService.GetISOLossMappings(objDIModel);
                    objDIModel.ClientId = AppHelper.ClientId;

                    objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getisolossmappings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                    //ipuri REST Service Conversion 12/06/2014 End

                    if (objDIModel.dsPolicyLOB != null)
                    {
                        ddlPolicyLOB.Enabled = true;
                        ddlISOClaimType.DataSource = objDIModel.dsRMClaimType;
                        ddlISOClaimType.DataTextField = objDIModel.dsRMClaimType.Tables[0].Columns[0].ToString();
                        ddlISOClaimType.DataValueField = objDIModel.dsRMClaimType.Tables[0].Columns[1].ToString();
                        ddlISOClaimType.DataBind();
                        ddlISOClaimType.Items.Insert(0, "--Select--");
                        if (bLoadOnEdit)
                        {
                            ddlPolicyLOB.SelectedValue = arrtmp[6];
                            ddlCoverage.Enabled = false;
                            ddlPolicyLOB_SelectedIndexChanged(this, null);
                            ddlISOClaimType.Enabled = false;
                            return;
                        }
                    }
                }
            }


            if (ddlPolicySystemNames.SelectedIndex == 0 && bPolicySystemInterface == true) //sagarwal54 MITS 37006
            {
                ddlPolicyLOB.Enabled = false;
                ddlPolicyLOB.Items.Clear();
                ddlPolicyLOB.Items.Insert(0, "--Select--");
            }

            if (m_isCarrier == true)
            {
                ddlCoverage.Enabled = false;
                ddlCoverage.Items.Clear();
                ddlCoverage.Items.Insert(0, "--Select--");
            }


            ddlCoverage.Enabled = false;
            ddlCoverage.Items.Clear();
            ddlCoverage.Items.Insert(0, "--Select--");

            ddlTypeOfLoss.Enabled = false;
            ddlTypeOfLoss.Items.Clear();
            ddlTypeOfLoss.Items.Insert(0, "--Select--");

            ddlDisabilityCode.Enabled = false;
            ddlDisabilityCode.Items.Clear();
            ddlDisabilityCode.Items.Insert(0, "--Select--");

            ddlISOPolicyType.Enabled = false;
            ddlISOPolicyType.Items.Clear();
            ddlISOPolicyType.Items.Insert(0, "--Select--");

            ddlISOCoverageType.Enabled = false;
            ddlISOCoverageType.Items.Clear();
            ddlISOCoverageType.Items.Insert(0, "--Select--");

            ddlISOLossType.Enabled = false;
            ddlISOLossType.Items.Clear();
            ddlISOLossType.Items.Insert(0, "--Select--");

            btnSave.Enabled = false;

        }

        //Developer – abharti5 |MITS 36676| end


        protected void ddlPolicyLOB_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataIntegratorServiceClient objDIService = null;
            DataIntegratorModel objDIModel = null;

            if ((ddlPolicyLOB.SelectedIndex != 0 && (m_isCarrier == true)))
            {

                //objDIService = new DataIntegratorServiceClient();
                objDIModel = new DataIntegratorModel();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }

                iLOB = Convert.ToInt32(ddlPolicyLOB.SelectedItem.Value);
                objDIModel.iPolicyType = iLOB;

                //Developer – abharti5 |MITS 36676| start
                if (String.Compare(ddlPolicySystemNames.SelectedItem.Value, "--Select--", true) != 0) //sagarwal54 MITS 37006
                {
                    iPSN = Convert.ToInt32(ddlPolicySystemNames.SelectedItem.Value);
                }
                else

                    iPSN = 0;
               
                objDIModel.iPolicySystemId = iPSN;
                //Developer – abharti5 |MITS 36676| end

                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.GetPolicyClaimType(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getpolicyclaimtype", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End

                if (objDIModel.dsPolicyClmtTypeMapping != null)
                {
                    ddlISOClaimType.Enabled = true;
                    ddlISOClaimType.DataSource = objDIModel.dsPolicyClmtTypeMapping;
                    ddlISOClaimType.DataTextField = objDIModel.dsPolicyClmtTypeMapping.Tables[0].Columns[1].ToString();
                    ddlISOClaimType.DataValueField = objDIModel.dsPolicyClmtTypeMapping.Tables[0].Columns[0].ToString();
                    ddlISOClaimType.DataBind();
                    ddlISOClaimType.Items.Insert(0, "--Select--");
                    if (bLoadOnEdit)
                    {
                        //ddlISOClaimType.SelectedValue = arrtmp[0]; - original
                        ddlISOClaimType.SelectedValue = arrtmp[1]; //Developer – abharti5 |MITS 36676|
                        ddlISOClaimType.Enabled = false;
                        ddlISOClaimType_SelectedIndexChanged(this, null);
                        return;
                    }
                }
                }

            else if (m_isCarrier == false)
            {
                //Non Carrier clients

                //objDIService = new DataIntegratorServiceClient();
                objDIModel = new DataIntegratorModel();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }

                if (ddlPolicyLOB.SelectedIndex != 0)
                {


                    if (ddlPolicyLOB.Items[1].Selected == true)
                    {
                        objDIModel.LOB = 241;
                        iNLOB = 241;
                    }
                    else if (ddlPolicyLOB.Items[2].Selected == true)
                    {
                        objDIModel.LOB = 242;
                        iNLOB = 242;
                    }
                    else if (ddlPolicyLOB.Items[3].Selected == true)
                    {
                        objDIModel.LOB = 243;
                        iNLOB = 243;
                    }
                    else if (ddlPolicyLOB.Items[4].Selected == true)
                    {
                        objDIModel.LOB = 845;
                        iNLOB = 845;
                    }
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objDIModel = objDIService.GetISOLossMappings(objDIModel);
                    objDIModel.ClientId = AppHelper.ClientId;

                    objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getisolossmappings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                    //ipuri REST Service Conversion 12/06/2014 End

                    if (objDIModel.dsRMClaimType != null)
                    {
                        ddlISOClaimType.Enabled = true;
                        ddlISOClaimType.DataSource = objDIModel.dsRMClaimType;
                        ddlISOClaimType.DataTextField = objDIModel.dsRMClaimType.Tables[0].Columns[0].ToString();
                        ddlISOClaimType.DataValueField = objDIModel.dsRMClaimType.Tables[0].Columns[1].ToString();
                        ddlISOClaimType.DataBind();
                        ddlISOClaimType.Items.Insert(0, "--Select--");
                        if (bLoadOnEdit)
                        {
                            //ddlISOClaimType.SelectedValue = arrtmp[0]; - original 
                            ddlISOClaimType.SelectedValue = arrtmp[1]; //Developer – abharti5 |MITS 36676|
                            ddlCoverage.Enabled = false;
                            ddlISOClaimType_SelectedIndexChanged(this, null);
                            ddlISOClaimType.Enabled = false;
                            return;
                        }
                    }
                }
            }


                if (ddlPolicyLOB.SelectedIndex == 0)
                {
                    ddlISOClaimType.Enabled = false;
                    ddlISOClaimType.Items.Clear();
                    ddlISOClaimType.Items.Insert(0, "--Select--");
                }

                if (m_isCarrier == true)
                {
                    ddlCoverage.Enabled = false;
                    ddlCoverage.Items.Clear();
                    ddlCoverage.Items.Insert(0, "--Select--");
                }

            
                ddlCoverage.Enabled = false;
                ddlCoverage.Items.Clear();
                ddlCoverage.Items.Insert(0, "--Select--");

                ddlTypeOfLoss.Enabled = false;
                ddlTypeOfLoss.Items.Clear();
                ddlTypeOfLoss.Items.Insert(0, "--Select--");

                ddlDisabilityCode.Enabled = false;
                ddlDisabilityCode.Items.Clear();
                ddlDisabilityCode.Items.Insert(0, "--Select--");

                ddlISOPolicyType.Enabled = false;
                ddlISOPolicyType.Items.Clear();
                ddlISOPolicyType.Items.Insert(0, "--Select--");

                ddlISOCoverageType.Enabled = false;
                ddlISOCoverageType.Items.Clear();
                ddlISOCoverageType.Items.Insert(0, "--Select--");

                ddlISOLossType.Enabled = false;
                ddlISOLossType.Items.Clear();
                ddlISOLossType.Items.Insert(0, "--Select--");

                btnSave.Enabled = false;
            }

        
        protected void ddlISOClaimType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataIntegratorServiceClient objDIService = new DataIntegratorServiceClient();
            DataIntegratorModel objDIModel = new DataIntegratorModel();

            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }

            if (ddlPolicyLOB.SelectedIndex != 0 && m_isCarrier == true)
            {
                iLOB = Convert.ToInt32(ddlPolicyLOB.SelectedItem.Value);
                objDIModel.iPolicyType = iLOB;

                //Developer – abharti5 |MITS 36676| start
                if (String.Compare(ddlPolicySystemNames.SelectedItem.Value, "--Select--", true) != 0) //sagarwal54 MITS 37006
                {
                    iPSN = Convert.ToInt32(ddlPolicySystemNames.SelectedItem.Value);
                }
                
                objDIModel.iPolicySystemId = iPSN;
                //Developer – abharti5 |MITS 36676| end
                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.GetPolicyCoverageType(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getpolicycoveragetype", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End

                if (objDIModel.dsPolicyCoverageMapping != null)
                {
                    ddlCoverage.Enabled = true;
                    ddlCoverage.DataSource = objDIModel.dsPolicyCoverageMapping;
                    ddlCoverage.DataTextField = objDIModel.dsPolicyCoverageMapping.Tables[0].Columns[1].ToString();
                    ddlCoverage.DataValueField = objDIModel.dsPolicyCoverageMapping.Tables[0].Columns[0].ToString();
                    ddlCoverage.DataBind();
                    ddlCoverage.Items.Insert(0, "--Select--");
                    if (bLoadOnEdit)
                    {
                        //ddlCoverage.SelectedValue = arrtmp[6]; - original
                        ddlCoverage.SelectedValue = arrtmp[7];  //Developer – abharti5 |MITS 36676|
                        ddlCoverage.Enabled = false;
                        ddlCoverage_SelectedIndexChanged(this, null);
                        return;
                    }
                }
                btnSave.Enabled = false;
            }
            else
            {
                //ISO Policy Type, Coverage type and Loss type is to be loaded for non carrier
                //objDIService = new DataIntegratorServiceClient();
                objDIModel = new DataIntegratorModel();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }

                if (ddlISOClaimType.SelectedIndex != 0)
                {
                    objDIModel.LOB = iNLOB;
                    //objDIModel.LOB = 9999;
                    objDIModel.RMClaimTypeID = Convert.ToInt32(ddlISOClaimType.SelectedValue.ToString());
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objDIModel = objDIService.GetRMClaimTypeMapping(objDIModel);
                    objDIModel.ClientId = AppHelper.ClientId;

                    objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getrmclaimtypemapping", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                    //ipuri REST Service Conversion 12/06/2014 End
                    // Developer - Subhendu : MITS 30839 : Start
                    sRecord = objDIModel.sISORecordType;
                    // Developer - Subhendu : MITS 30839 : End
                    if (objDIModel.dsISOPolicyType != null)
                    {
                        ddlISOPolicyType.Enabled = true;
                        ddlISOPolicyType.DataSource = objDIModel.dsISOPolicyType;
                        ddlISOPolicyType.DataTextField = objDIModel.dsISOPolicyType.Tables[0].Columns[0].ToString();
                        ddlISOPolicyType.DataValueField = objDIModel.dsISOPolicyType.Tables[0].Columns[1].ToString();
                        ddlISOPolicyType.DataBind();
                        ddlISOPolicyType.Items.Insert(0, "--Select--");
                        if (bLoadOnEdit)
                        {
                            // Developer - Subhendu : MITS 30839 : Start
                            //ddlISOPolicyType.SelectedValue = arrtmp[2] + "(" +sRecord+")"; - original
                            // Developer - Subhendu : MITS 30839 : End
                            //ddlISOPolicyType.Enabled = false;
                            ddlISOPolicyType.SelectedValue = arrtmp[3] + "(" + sRecord + ")"; //Developer – abharti5 |MITS 36676|
                            ddlISOPolicyType_SelectedIndexChanged(this, null);
                            return;
                        }
                    }

                    ddlISOCoverageType.Enabled = false;
                    ddlISOCoverageType.Items.Clear();
                    ddlISOCoverageType.Items.Insert(0, "--Select--");

                    ddlISOLossType.Enabled = false;
                    ddlISOLossType.Items.Clear();
                    ddlISOLossType.Items.Insert(0, "--Select--");
                }
            }


            if (ddlISOClaimType.SelectedIndex == 0 && m_isCarrier == true)
            {
                ddlCoverage.Enabled = false;
                ddlCoverage.Items.Clear();
                ddlCoverage.Items.Insert(0, "--Select--");
            }

            if (ddlISOClaimType.SelectedIndex == 0 && m_isCarrier == false)
            {
                ddlISOPolicyType.Enabled = false;
                ddlISOPolicyType.Items.Clear();
                ddlISOPolicyType.Items.Insert(0, "--Select--");

            }

            if (m_isCarrier == true)
            {
                ddlTypeOfLoss.Enabled = false;
                ddlTypeOfLoss.Items.Clear();
                ddlTypeOfLoss.Items.Insert(0, "--Select--");

                ddlDisabilityCode.Enabled = false;
                ddlDisabilityCode.Items.Clear();
                ddlDisabilityCode.Items.Insert(0, "--Select--");

                ddlISOPolicyType.Enabled = false;
                ddlISOPolicyType.Items.Clear();
                ddlISOPolicyType.Items.Insert(0, "--Select--");
            }

            ddlISOCoverageType.Enabled = false;
            ddlISOCoverageType.Items.Clear();
            ddlISOCoverageType.Items.Insert(0, "--Select--");

            ddlISOLossType.Enabled = false;
            ddlISOLossType.Items.Clear();
            ddlISOLossType.Items.Insert(0, "--Select--");
            
            btnSave.Enabled = false;
        }


        protected void ddlCoverage_SelectedIndexChanged(object sender, EventArgs e)
        {

            //DataIntegratorServiceClient objDIService = new DataIntegratorServiceClient();
            DataIntegratorModel objDIModel = new DataIntegratorModel();
            Boolean bIsWCType = false;//Variable to Check for Type of Loss or Disability Code

            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }

           // if ((ddlPolicyLOB.SelectedIndex != 0 && (m_isCarrier == true)) && (ddlISOClaimType.SelectedIndex != 0) && (ddlCoverage.SelectedIndex != 0)) - original
            //sagarwal54 MITS 37006
            if ((ddlPolicyLOB.SelectedIndex != 0 && (m_isCarrier == true)) && (ddlISOClaimType.SelectedIndex != 0) && (ddlCoverage.SelectedIndex != 0)) //Developer – abharti5 |MITS 36676|
            {                
                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.GetWCPolicyType(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getwcpolicytype", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End
                foreach (DataRow drRow in objDIModel.dsWCPolicyType.Tables[0].Rows)
                {
                    if (ddlPolicyLOB.SelectedItem.Value == drRow[0].ToString())
                    {
                        bIsWCType = true;
                        break;
                    }
                    else
                    {
                        bIsWCType = false;
                    }
                }
                // Developer - Subhendu : MITS 30839 : Start
                objDIModel.iPolicyType = Convert.ToInt32(ddlPolicyLOB.SelectedItem.Value);
                objDIModel.iCovCode = Convert.ToInt32(ddlCoverage.SelectedItem.Value);
                
                // Developer - Subhendu : MITS 30839 : End

                //sagarwal54 MITS 37006 start
                if (ddlPolicySystemNames.SelectedIndex != 0) 
                {
                    objDIModel.iPolicySystemId = Convert.ToInt32(ddlPolicySystemNames.SelectedValue); //Developer – abharti5 |MITS 36676| 
                }
                //sagarwal54 MITS 37006 end
                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.GetPolicyLOB(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getpolicylob", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End

                if (bIsWCType == true)//For Disablity Code
                {
                    //objDIModel = objDIService.GetPolicyLOB(objDIModel);
                    if ((ddlISOClaimType.SelectedIndex != 0) && (ddlPolicyLOB.SelectedIndex != 0))
                    {
                        if (objDIModel.dsDisablityCode != null)
                        {
                            ddlDisabilityCode.Enabled = true;
                            ddlDisabilityCode.DataSource = objDIModel.dsDisablityCode;
                            ddlDisabilityCode.DataTextField = objDIModel.dsDisablityCode.Tables[0].Columns[1].ToString();
                            ddlDisabilityCode.DataValueField = objDIModel.dsDisablityCode.Tables[0].Columns[0].ToString();
                            ddlDisabilityCode.DataBind();
                            ddlDisabilityCode.Items.Insert(0, "--Select--");
                            if (bLoadOnEdit)
                            {
                                //ddlDisabilityCode.SelectedValue = arrtmp[8]; - original
                                ddlDisabilityCode.SelectedValue = arrtmp[9]; //Developer – abharti5 |MITS 36676|
                                ddlDisabilityCode.Enabled = false;
                                ddlDisabilityCode_SelectedIndexChanged(this, null);
                                return;
                            }
                        }
                    }



                    if (ddlCoverage.SelectedIndex == 0)
                    {
                        ddlDisabilityCode.Enabled = false;
                        ddlDisabilityCode.Items.Clear();
                        ddlDisabilityCode.Items.Insert(0, "--Select--");
                    }

                    ddlTypeOfLoss.Enabled = false;
                    ddlTypeOfLoss.Items.Clear();
                    ddlTypeOfLoss.Items.Insert(0, "--Select--");

                    ddlISOPolicyType.Enabled = false;
                    ddlISOPolicyType.Items.Clear();
                    ddlISOPolicyType.Items.Insert(0, "--Select--");

                    ddlISOCoverageType.Enabled = false;
                    ddlISOCoverageType.Items.Clear();
                    ddlISOCoverageType.Items.Insert(0, "--Select--");

                    ddlISOLossType.Enabled = false;
                    ddlISOLossType.Items.Clear();
                    ddlISOLossType.Items.Insert(0, "--Select--");
                

                

                }
                else//For TypeOfLoss
                {
                    //objDIModel = objDIService.GetPolicyLOB(objDIModel);
                    if ((ddlISOClaimType.SelectedIndex != 0) && (ddlPolicyLOB.SelectedIndex != 0))
                    {
                        if (objDIModel.dsLossCode != null)
                        {
                            ddlTypeOfLoss.Enabled = true;
                            ddlTypeOfLoss.DataSource = objDIModel.dsLossCode;
                            ddlTypeOfLoss.DataTextField = objDIModel.dsLossCode.Tables[0].Columns[1].ToString();
                            ddlTypeOfLoss.DataValueField = objDIModel.dsLossCode.Tables[0].Columns[0].ToString();
                            ddlTypeOfLoss.DataBind();
                            ddlTypeOfLoss.Items.Insert(0, "--Select--");
                            if (bLoadOnEdit)
                            {
                                //ddlTypeOfLoss.SelectedValue = arrtmp[7]; - original
                                ddlTypeOfLoss.SelectedValue = arrtmp[8];  //Developer – abharti5 |MITS 36676|
                                ddlTypeOfLoss.Enabled = false;
                                ddlTypeOfLoss_SelectedIndexChanged(this, null);
                                return;
                            }
                        }
                    }

                    if (ddlCoverage.SelectedIndex == 0)
                    {
                        ddlTypeOfLoss.Enabled = false;
                        ddlTypeOfLoss.Items.Clear();
                        ddlTypeOfLoss.Items.Insert(0, "--Select--");
                    }

                    ddlDisabilityCode.Enabled = false;
                    ddlDisabilityCode.Items.Clear();
                    ddlDisabilityCode.Items.Insert(0, "--Select--");

                    ddlISOPolicyType.Enabled = false;
                    ddlISOPolicyType.Items.Clear();
                    ddlISOPolicyType.Items.Insert(0, "--Select--");

                    ddlISOCoverageType.Enabled = false;
                    ddlISOCoverageType.Items.Clear();
                    ddlISOCoverageType.Items.Insert(0, "--Select--");

                    ddlISOLossType.Enabled = false;
                    ddlISOLossType.Items.Clear();
                    ddlISOLossType.Items.Insert(0, "--Select--");

                }
            }

            //ipuri 04/26/2013 Mits:30402 start
            else
            {
                if (ddlCoverage.SelectedIndex == 0)
                {
                    ddlDisabilityCode.Enabled = false;
                    ddlDisabilityCode.Items.Clear();
                    ddlDisabilityCode.Items.Insert(0, "--Select--");
                }

                ddlTypeOfLoss.Enabled = false;
                ddlTypeOfLoss.Items.Clear();
                ddlTypeOfLoss.Items.Insert(0, "--Select--");

                ddlISOPolicyType.Enabled = false;
                ddlISOPolicyType.Items.Clear();
                ddlISOPolicyType.Items.Insert(0, "--Select--");

                ddlISOCoverageType.Enabled = false;
                ddlISOCoverageType.Items.Clear();
                ddlISOCoverageType.Items.Insert(0, "--Select--");

                ddlISOLossType.Enabled = false;
                ddlISOLossType.Items.Clear();
                ddlISOLossType.Items.Insert(0, "--Select--");
            }
            //ipuri 04/26/2013 Mits:30402 end
            btnSave.Enabled = false;
        }
       

        protected void ddlDisabilityCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if ((ddlPolicySystemNames.SelectedIndex != 0 && (m_isCarrier == true)) && (ddlPolicyLOB.SelectedIndex != 0) && (m_isCarrier == true) && (ddlISOClaimType.SelectedIndex != 0) && ddlDisabilityCode.SelectedIndex != 0) //Developer – abharti5 |MITS 36676|
            if ((ddlPolicyLOB.SelectedIndex != 0) && (m_isCarrier == true) && (ddlISOClaimType.SelectedIndex != 0) && ddlDisabilityCode.SelectedIndex != 0)            // sagarwal54 MITS 37006
            {
                //DataIntegratorServiceClient objDIService = null;
                DataIntegratorModel objDIModel = null;


                //objDIService = new DataIntegratorServiceClient();
                objDIModel = new DataIntegratorModel();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }

                objDIModel.iPolicyType = Convert.ToInt32(ddlPolicyLOB.SelectedValue);
                //objDIModel.LOB = 9999;
                hdLOBpopup.Value = objDIModel.LOB.ToString();
                objDIModel.RMClaimTypeID = Convert.ToInt32(ddlISOClaimType.SelectedValue.ToString());
                // Developer - Subhendu : MITS 30839 : Start
                objDIModel.iCovCode = Convert.ToInt32(ddlCoverage.SelectedValue.ToString());
                objDIModel.iLossTypeCode = 0;
                // Developer - Subhendu : MITS 30839 : End
                objDIModel.iDisabilityCode = Convert.ToInt32(ddlDisabilityCode.SelectedValue.ToString());
                
                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.GetRMClaimTypeMapping(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getrmclaimtypemapping", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End
                // Developer - Subhendu : MITS 30839 : Start
                sRecord = objDIModel.sISORecordType;
                // Developer - Subhendu : MITS 30839 : End
                if (objDIModel.dsISOPolicyType != null)
                {
                    ddlISOPolicyType.Enabled = true;
                    ddlISOPolicyType.DataSource = objDIModel.dsISOPolicyType;
                    ddlISOPolicyType.DataTextField = objDIModel.dsISOPolicyType.Tables[0].Columns[0].ToString();
                    ddlISOPolicyType.DataValueField = objDIModel.dsISOPolicyType.Tables[0].Columns[1].ToString();
                    ddlISOPolicyType.DataBind();
                    ddlISOPolicyType.Items.Insert(0, "--Select--");
                    if (bLoadOnEdit)
                    {
                        // Developer - Subhendu : MITS 30839 : Start
                        //ddlISOPolicyType.SelectedValue = arrtmp[2]+"("+sRecord+")"; - original
                        // Developer - Subhendu : MITS 30839 : End

                        ddlISOPolicyType.SelectedValue = arrtmp[3] + "(" + sRecord + ")";//Developer – abharti5 |MITS 36676|

                        ddlISOPolicyType.Enabled = true;
                        ddlISOPolicyType_SelectedIndexChanged(this, null);
                        return;
                    }
                }
            }

                if (ddlDisabilityCode.SelectedIndex == 0)
                {
                    ddlISOPolicyType.Enabled = false;
                    ddlISOPolicyType.Items.Clear();
                    ddlISOPolicyType.Items.Insert(0, "--Select--");
                }

                ddlTypeOfLoss.Enabled = false;
                ddlTypeOfLoss.Items.Clear();
                ddlTypeOfLoss.Items.Insert(0, "--Select--");


                ddlISOCoverageType.Enabled = false;
                ddlISOCoverageType.Items.Clear();
                ddlISOCoverageType.Items.Insert(0, "--Select--");

                ddlISOLossType.Enabled = false;
                ddlISOLossType.Items.Clear();
                ddlISOLossType.Items.Insert(0, "--Select--");

                btnSave.Enabled = false;        //ipuri 04/26/2013 Mits:30402
            
        }

        protected void ddlTypeOfLoss_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if ((ddlPolicySystemNames.SelectedIndex != 0 && (m_isCarrier == true)) && (ddlPolicyLOB.SelectedIndex != 0) && (m_isCarrier == true) && (ddlISOClaimType.SelectedIndex != 0) && ddlTypeOfLoss.SelectedIndex != 0) //Developer – abharti5 |MITS 36676|
            if ((ddlPolicyLOB.SelectedIndex != 0) && (m_isCarrier == true) && (ddlISOClaimType.SelectedIndex != 0) && ddlTypeOfLoss.SelectedIndex != 0) //sagarwal54 MITS 37006 09/02/2014
            {
                //DataIntegratorServiceClient objDIService = null;
                DataIntegratorModel objDIModel = null;

                //objDIService = new DataIntegratorServiceClient();
                objDIModel = new DataIntegratorModel();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }

                objDIModel.iPolicyType = Convert.ToInt32(ddlPolicyLOB.SelectedValue);
                //objDIModel.LOB = objDIService.GetPolicyClaimLOBType(objDIModel);
                //objDIModel.LOB = 9999;
                hdLOBpopup.Value = objDIModel.LOB.ToString();
                objDIModel.RMClaimTypeID = Convert.ToInt32(ddlISOClaimType.SelectedValue.ToString());
                // Developer - Subhendu : MITS 30839 : Start
                objDIModel.iCovCode = Convert.ToInt32(ddlCoverage.SelectedValue.ToString());
                // Developer - Subhendu : MITS 30839 : End
                objDIModel.iLossTypeCode = Convert.ToInt32(ddlTypeOfLoss.SelectedValue.ToString());
                // Developer - Subhendu : MITS 30839 : Start
                objDIModel.iDisabilityCode = 0;
                // Developer - Subhendu : MITS 30839 : End
                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.GetRMClaimTypeMapping(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getrmclaimtypemapping", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End

                // Developer - Subhendu : MITS 30839 : Start
                sRecord = objDIModel.sISORecordType;
                // Developer - Subhendu : MITS 30839 : End
                if (objDIModel.dsISOPolicyType != null)
                {
                    ddlISOPolicyType.Enabled = true;
                    ddlISOPolicyType.DataSource = objDIModel.dsISOPolicyType;
                    ddlISOPolicyType.DataTextField = objDIModel.dsISOPolicyType.Tables[0].Columns[0].ToString();
                    ddlISOPolicyType.DataValueField = objDIModel.dsISOPolicyType.Tables[0].Columns[1].ToString();
                    ddlISOPolicyType.DataBind();
                    ddlISOPolicyType.Items.Insert(0, "--Select--");
                    if (bLoadOnEdit)
                    {
                        // Developer - Subhendu : MITS 30839 : Start
                        // ddlISOPolicyType.SelectedValue = arrtmp[2]+"("+sRecord+")"; - original
                        // Developer - Subhendu : MITS 30839 : End
                        ddlISOPolicyType.SelectedValue = arrtmp[3] + "(" + sRecord + ")"; //Developer – abharti5 |MITS 36676|
                        ddlISOPolicyType.Enabled = true;
                        ddlISOPolicyType_SelectedIndexChanged(this, null);
                        return;
                    }
                }
            }

                if (ddlTypeOfLoss.SelectedIndex == 0)
                {
                    ddlISOPolicyType.Enabled = false;
                    ddlISOPolicyType.Items.Clear();
                    ddlISOPolicyType.Items.Insert(0, "--Select--");
                }

                ddlDisabilityCode.Enabled = false;
                ddlDisabilityCode.Items.Clear();
                ddlDisabilityCode.Items.Insert(0, "--Select--");

                ddlISOCoverageType.Enabled = false;
                ddlISOCoverageType.Items.Clear();
                ddlISOCoverageType.Items.Insert(0, "--Select--");

                ddlISOLossType.Enabled = false;
                ddlISOLossType.Items.Clear();
                ddlISOLossType.Items.Insert(0, "--Select--");

                btnSave.Enabled = false;        //ipuri 04/26/2013 Mits:30402

        }

        protected void ddlISOPolicyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataIntegratorServiceClient objDIService = null;
            DataIntegratorModel objDIModel = null;

            //objDIService = new DataIntegratorServiceClient();
            objDIModel = new DataIntegratorModel();

            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }
            
            if (m_isCarrier == true)
            {
                objDIModel.iPolicyType = Convert.ToInt32(ddlPolicyLOB.SelectedValue);
                //objDIModel.LOB = objDIService.GetPolicyClaimLOBType(objDIModel);
                //objDIModel.LOB = 9999;
                hdLOBpopup.Value = objDIModel.LOB.ToString();
            }
            else
            {
                // Developer - Subhendu : MITS 30839 : Start
                //objDIModel.LOB = iNLOB;
                //objDIModel.LOB = 9999;
                // Developer - Subhendu : MITS 30839 : End
            }
            
            objDIModel.RMClaimTypeID = Convert.ToInt32(ddlISOClaimType.SelectedValue.ToString());
            objDIModel.sISOPolicyCode = ddlISOPolicyType.SelectedValue;
            if (ddlTypeOfLoss.SelectedIndex > 0)
            {
                objDIModel.iLossTypeCode = Convert.ToInt32(ddlTypeOfLoss.SelectedValue.ToString());
            }
            if (ddlDisabilityCode.SelectedIndex > 0)
            {
                objDIModel.iDisabilityCode = Convert.ToInt32(ddlDisabilityCode.SelectedValue.ToString());
            }
            // Developer - Subhendu : MITS 30839 : Start
            //sRecord = (ddlISOPolicyType.SelectedItem.Text.IndexOf("UC01") == -1 ? "UV01" : "UC01");

            //Sagarwal54 02/20/2014 MITS 35386 Start
            if (ddlISOPolicyType.SelectedItem.Text.IndexOf("UC01") == -1)
            {
                if (ddlISOPolicyType.SelectedItem.Text.IndexOf("UV01") == -1)
                {
                    sRecord = "UP01";
                }
                else
                {
                    sRecord = "UV01";
                }
            }
            else
            {
                sRecord = "UC01";
            }
            //Sagarwal54 02/20/2014 MITS 35386 End
            objDIModel.sISORecordType = (ddlISOPolicyType.SelectedValue.IndexOf("UC01") == -1 ? "UV01" : "UC01");
            // Developer - Subhendu : MITS 30839 : End

            //ipuri REST Service Conversion 12/06/2014 Start
            //objDIModel = objDIService.GetCoverageTypeMapping(objDIModel);
            objDIModel.ClientId = AppHelper.ClientId;

            objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getcoveragetypemapping", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
            //ipuri REST Service Conversion 12/06/2014 End
            

                if (objDIModel.dsISOCoverageType != null)
                {
                    ddlISOCoverageType.Enabled = true;
                    ddlISOCoverageType.DataSource = objDIModel.dsISOCoverageType;
                    ddlISOCoverageType.DataTextField = objDIModel.dsISOCoverageType.Tables[0].Columns[0].ToString();
                    ddlISOCoverageType.DataValueField = objDIModel.dsISOCoverageType.Tables[0].Columns[1].ToString();
                    ddlISOCoverageType.DataBind();
                    ddlISOCoverageType.Items.Insert(0, "--Select--");
                    if (bLoadOnEdit)
                    {
                        //ddlISOCoverageType.SelectedValue = arrtmp[3]; - original
                        ddlISOCoverageType.SelectedValue = arrtmp[4]; //Developer – abharti5 |MITS 36676|
                        ddlISOCoverageType.Enabled = true;
                        ddlISOCoverageType_SelectedIndexChanged(this, null);
                        btnSave.Enabled = false;
                        return;
                    }
                }

                if (ddlISOPolicyType.SelectedIndex == 0)
                {
                    ddlISOCoverageType.Enabled = false;
                    ddlISOCoverageType.Items.Clear();
                    ddlISOCoverageType.Items.Insert(0, "--Select--");
                }
                ddlISOLossType.Enabled = false;
                ddlISOLossType.Items.Clear();
                ddlISOLossType.Items.Insert(0, "--Select--");
                btnSave.Enabled = false;        //ipuri 04/26/2013 Mits:30402
           }

        protected void ddlISOCoverageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataIntegratorServiceClient objDIService = new DataIntegratorServiceClient();
            DataIntegratorModel objDIModel = new DataIntegratorModel();
            
            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }
            
            if (m_isCarrier == true)
            {
                objDIModel.iPolicyType = Convert.ToInt32(ddlPolicyLOB.SelectedValue);
                //objDIModel.LOB = objDIService.GetPolicyClaimLOBType(objDIModel);
                //objDIModel.LOB = 9999;
                hdLOBpopup.Value = objDIModel.LOB.ToString();
            }
            else
            {
                // Developer - Subhendu : MITS 30839 : Start
                //objDIModel.LOB = iNLOB;
                //objDIModel.LOB = 9999;
                // Developer - Subhendu : MITS 30839 : End
            }
            
            objDIModel.RMClaimTypeID = Convert.ToInt32(ddlISOClaimType.SelectedValue.ToString());
            objDIModel.sISOPolicyCode = ddlISOPolicyType.SelectedValue;
            // Developer - Subhendu : MITS 30839 : Start
            objDIModel.sISORecordType = sRecord;
            // Developer - Subhendu : MITS 30839 : End
            objDIModel.sISOCoverageCode = ddlISOCoverageType.SelectedValue;
            if (ddlTypeOfLoss.SelectedIndex > 0)
            {
                objDIModel.iLossTypeCode = Convert.ToInt32(ddlTypeOfLoss.SelectedValue.ToString());
            }
            if (ddlDisabilityCode.SelectedIndex > 0)
            {
                objDIModel.iDisabilityCode = Convert.ToInt32(ddlDisabilityCode.SelectedValue.ToString());
            }

            //ipuri REST Service Conversion 12/06/2014 Start
            //objDIModel = objDIService.GetLossTypeMapping(objDIModel);
            objDIModel.ClientId = AppHelper.ClientId;

            objDIModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/getlosstypemapping", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
            //ipuri REST Service Conversion 12/06/2014 End
            

                if (objDIModel.dsISOLossType != null)
                {
                    ddlISOLossType.Enabled = true;
                    ddlISOLossType.DataSource = objDIModel.dsISOLossType;
                    ddlISOLossType.DataTextField = objDIModel.dsISOLossType.Tables[0].Columns[0].ToString();
                    ddlISOLossType.DataValueField = objDIModel.dsISOLossType.Tables[0].Columns[1].ToString();
                    ddlISOLossType.DataBind();
                    ddlISOLossType.Items.Insert(0, "--Select--");
                    if (bLoadOnEdit)
                    {
                        //ddlISOLossType.SelectedValue = arrtmp[4]; original
                        ddlISOLossType.SelectedValue = arrtmp[5]; //Developer – abharti5 |MITS 36676|
                        ddlISOLossType.Enabled = true;
                        ddlISOLossType_SelectedIndexChanged1(this, null);
                        btnSave.Enabled = false;
                    }
                }

                if (ddlISOCoverageType.SelectedIndex == 0 )
                {
                    ddlISOLossType.Enabled = false;
                    ddlISOLossType.Items.Clear();
                    ddlISOLossType.Items.Insert(0, "--Select--");
                }
                btnSave.Enabled = false;        //ipuri 04/26/2013 Mits:30402
                
         }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if ((ddlISOPolicyType.SelectedIndex != 0) && (ddlISOLossType.SelectedIndex != 0) && (ddlISOCoverageType.SelectedIndex != 0))
            {
                //DataIntegratorServiceClient objDIService = null;
                DataIntegratorModel objDIModel = null;

                //objDIService = new DataIntegratorServiceClient();
                objDIModel = new DataIntegratorModel();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }

                if (m_isCarrier == true)
                {
                    objDIModel.iPolicyType = Convert.ToInt32(ddlPolicyLOB.SelectedValue);
                    //objDIModel.LOB = objDIService.GetPolicyClaimLOBType(objDIModel);
                    ///objDIModel.LOB = 9999;
                    hdLOBpopup.Value = objDIModel.LOB.ToString();
                }
                else
                {
                    objDIModel.LOB = iNLOB;
                }
                objDIModel.RMClaimTypeID = Convert.ToInt32(ddlISOClaimType.SelectedValue.ToString());
                // Developer - Subhendu : MITS 30839 : Start
                strPolicyCodeTmp = ddlISOPolicyType.SelectedValue;
                strPolicyCodeTmp = strPolicyCodeTmp.Substring(0, strPolicyCodeTmp.IndexOf('('));
                objDIModel.sISOPolicyCode = strPolicyCodeTmp;
                // Developer - Subhendu : MITS 30839 : End
                objDIModel.sISOCoverageCode = ddlISOCoverageType.SelectedValue;
                objDIModel.sISOLossCode = ddlISOLossType.SelectedValue;
                if (ddlTypeOfLoss.SelectedIndex > 0)
                {
                    objDIModel.iLossTypeCode = Convert.ToInt32(ddlTypeOfLoss.SelectedValue.ToString());
                }
                if (ddlDisabilityCode.SelectedIndex > 0)
                {
                    objDIModel.iDisabilityCode = Convert.ToInt32(ddlDisabilityCode.SelectedValue.ToString());
                }
                XmlTemplate = GetMessageTemplate();
                CallCWS("DataIntegratorAdaptor.ISOSave", XmlTemplate, out sCWSresponse, false, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                
                if (sMsgStatus == "Success")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");                    
                }                
            }
            else
            {
            }
        }

        protected void ddlISOLossType_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (ddlISOLossType.Enabled == true)
            {
                if (ddlISOLossType.SelectedIndex != 0)
                {
                    btnSave.Enabled = true;
                }
                else if (ddlISOLossType.SelectedIndex == 0)
                {
                    btnSave.Enabled = false;
                }
            }
            else
            {
                btnSave.Enabled = false;
            }
            bLoadOnEdit = false;
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ISOMappingList>");

            if (m_isCarrier == true)
            {
                //Developer – abharti5 |MITS 36676| start
                sXml = sXml.Append("<control name='PSN' type='int'>");
                if (ddlPolicySystemNames.SelectedIndex != 0) //sagarwal54 MITS 37006
                {
                    sXml = sXml.Append(ddlPolicySystemNames.SelectedValue);
                }
                else
                {
                    sXml = sXml.Append(String.Empty);
                }
                sXml = sXml.Append("</control>");
                //Developer – abharti5 |MITS 36676| end

                sXml = sXml.Append("<control name='LOB' type='int'>");
                sXml = sXml.Append(ddlPolicyLOB.SelectedValue);
                sXml = sXml.Append("</control>");

                sXml = sXml.Append("<control name='Claim Type' type='int'>");
                sXml = sXml.Append(ddlISOClaimType.SelectedValue);
                sXml = sXml.Append("</control>");

                sXml = sXml.Append("<control name='Coverage' type='int'>");
                sXml = sXml.Append(ddlCoverage.SelectedValue);
                sXml = sXml.Append("</control>");

                sXml = sXml.Append("<control name='Type Of Loss' type='int'>");
                if (ddlTypeOfLoss.SelectedIndex > 0)
                {
                    sXml = sXml.Append(ddlTypeOfLoss.SelectedValue);
                }
                else
                {
                    sXml = sXml.Append(0);
                }

                sXml = sXml.Append("</control>");


                sXml = sXml.Append("<control name='Disability Code' type='int'>");
                if (ddlDisabilityCode.SelectedIndex > 0)
                {
                    sXml = sXml.Append(ddlDisabilityCode.SelectedValue);
                }
                else
                {
                    sXml = sXml.Append(0);
                }
                sXml = sXml.Append("</control>");

                sXml = sXml.Append("<control name='ISO Policy Type' type='int'>");
                // Developer - Subhendu : MITS 30839 : Start
                sXml = sXml.Append(strPolicyCodeTmp);
                // Developer - Subhendu : MITS 30839 : End
                sXml = sXml.Append("</control>");
                
                sXml = sXml.Append("<control name='ISO Coverage Type' type='int'>");
                sXml = sXml.Append(ddlISOCoverageType.SelectedValue);
                sXml = sXml.Append("</control>");

                sXml = sXml.Append("<control name='ISO Loss Type' type='int'>");
                sXml = sXml.Append(ddlISOLossType.SelectedValue);
                sXml = sXml.Append("</control>");

                sXml = sXml.Append("<control name='hdClaimLOB'>");
                // Developer - Subhendu : MITS 30839 : Start
                sXml = sXml.Append(0);
                sXml = sXml.Append("</control>");
                
                sXml = sXml.Append("<control name='ISO Record Type'>");
                sXml = sXml.Append(sRecord);
                sXml = sXml.Append("</control>");
                // Developer - Subhendu : MITS 30839 : End

                if (string.IsNullOrEmpty(RowId.Text))
                {
                    RowId.Text = "0";
                }
                sXml = sXml.Append("<control name='RowId' type='id'>");
                sXml = sXml.Append(RowId.Text);
                sXml = sXml.Append("</control>");
            }
            else
            {
                sXml = sXml.Append("<control name='hdClaimLOB' type='int'>");
                sXml = sXml.Append(iNLOB);
                sXml = sXml.Append("</control>");

                sXml = sXml.Append("<control name='Claim Type' type='int'>");
                sXml = sXml.Append(ddlISOClaimType.SelectedValue);
                sXml = sXml.Append("</control>");

                sXml = sXml.Append("<control name='ISO Policy Type' type='int'>");
                // Developer - Subhendu : MITS 30839 : Start
                sXml = sXml.Append(strPolicyCodeTmp);
                // Developer - Subhendu : MITS 30839 : End
                sXml = sXml.Append("</control>");


                sXml = sXml.Append("<control name='ISO Coverage Type' type='int'>");
                sXml = sXml.Append(ddlISOCoverageType.SelectedValue);
                sXml = sXml.Append("</control>");

                sXml = sXml.Append("<control name='ISO Loss Type' type='int'>");
                sXml = sXml.Append(ddlISOLossType.SelectedValue);
                sXml = sXml.Append("</control>");

                // Developer - Subhendu : MITS 30839 : Start
                sXml = sXml.Append("<control name='ISO Record Type'>");
                sXml = sXml.Append(sRecord);
                sXml = sXml.Append("</control>");
                // Developer - Subhendu : MITS 30839 : End
                if (string.IsNullOrEmpty(RowId.Text))
                {
                    RowId.Text = "0";
                }
                sXml = sXml.Append("<control name='RowId' type='id'>");
                sXml = sXml.Append(RowId.Text);
                sXml = sXml.Append("</control>");
            }
            sXml = sXml.Append("</ISOMappingList></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.close();</script>");
        }


    }      

 }
 
