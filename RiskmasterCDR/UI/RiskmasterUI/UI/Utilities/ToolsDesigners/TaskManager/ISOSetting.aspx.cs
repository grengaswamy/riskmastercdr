﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessHelpers;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.ServiceHelpers;
using System.Text;
using System.Xml;
using Riskmaster.UI.DataIntegratorService;  //kkaur25 SOA changes
using System.IO;    //ipuri
using Riskmaster.Models;
// npadhy RMACLOUD-2418 - ISO Compatible with Cloud
// Include the namespace for CuteWebUI which will upload the files in Case of cloud
using CuteWebUI;


//******************************************************************************
//Developer: Ankur Saxena
//Date: 4/3/2009
//Description: This page is for ISO settings being used by Data integrator jobs. 
//              This page is integrated with RMX task manager.
//******************************************************************************
namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    //public partial class ISOSetting : System.Web.UI.Page
    public partial class ISOMappingClass : NonFDMBasePageCWS
    
    {
        #region Private Variables
        private const string m_sModuleName = "ISO"; //Modules Name
        private int m_iOptionSetID; // this will be pased in from task manager.

        //mihtesham

        private XElement XmlTemplate = null;
        private string sCWSresponse= "";
        //ipuri start
        private static string sIndexFileName = string.Empty;
        private static string sErrorFileName = string.Empty;
        private static string sUserName = string.Empty;
        private static string sClUser = string.Empty;
        private static string sSbUserName = string.Empty;
        private static string sSbUser = string.Empty;
        private static string sIndexFileDir = string.Empty;
        private static string sErrorFileDir = string.Empty;
        //public static bool bError = false;
        //string s = string.Empty;
        //string s1 = string.Empty;
        //ipuri End
        #endregion


        //******************************************************************************
        //Developer: Ankur Saxena
        //Date: 4/3/2009
        //Description: Function will be called everytime when page load happens.
        //******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes
            if (!IsPostBack)
            {
                hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));
                RetrieveSettings(); //Retrieve Settings from data integrator tables.
               
            }

            else
            {
                if (hdOptionset.Value == "" || hdOptionset.Value == null)
                {
                    hdOptionset.Value = "0";
                }
                m_iOptionSetID = Convert.ToInt32(hdOptionset.Value);
                if (ISOMappingInfoGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = ISOMappingInfoGridSelectedId.Text;
                    XmlTemplate = GetMessageTemplate(selectedRowId);
                    CallCWS("DataIntegratorAdaptor.ISODelete", XmlTemplate, out sCWSresponse, false, false);
                    ISOMappingInfoGrid_RowDeletedFlag.Text = "false";
                }
                if (ISOPopertyMappingGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = ISOPopertyMappingGridSelectedId.Text;
                    XmlTemplate = GetMessageTemplateForPropType(selectedRowId);
                    CallCWS("DataIntegratorAdaptor.ISODeletePropertyMapping", XmlTemplate, out sCWSresponse, false, false);
                    ISOPopertyMappingGrid_RowDeletedFlag.Text = "false";
                }

                //sagarwal54 MITS 35704 start
                if (ISOClaimPartyType_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = ISOClaimPartyTypeGridSelectedId.Text;
                    XmlTemplate = GetMessageTemplateForAddClaimant(selectedRowId);
                    CallCWS("DataIntegratorAdaptor.ISODeleteAdditionalClaimantMapping", XmlTemplate, out sCWSresponse, false, false);
                    ISOClaimPartyType_RowDeletedFlag.Text = "false";
                }

                //sagarwal54 MITS 35704 End
                //MIHTESHAM MITS 35711 START

                if (ISOSPRoleMappingGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = ISOSPRoleMappingGridSelectedId.Text;
                    XmlTemplate = GetMessageTemplateForSPRoleType(selectedRowId);
                    CallCWS("DataIntegratorAdaptor.ISODeleteSPRoleMapping", XmlTemplate, out sCWSresponse, false, false);
                    ISOSPRoleMappingGrid_RowDeletedFlag.Text = "false";
                }

                //MIHTESHAM MITS 35711 END
                string strSelectedRowId = AppHelper.GetQueryStringValue("selectedid");
                XmlTemplate = GetMessageTemplate();
                CallCWSFunctionBind("DataIntegratorAdaptor.ISOGet", out sCWSresponse, XmlTemplate);

                //sagarwal54 MITS 35386 start                
                if (hTabName.Value == "PropertyMappings" || hTabName.Value == "ISOClaimPartyType" || hTabName.Value == "SPRoleMappings")
                {
                    HttpContext.Current.Session["hTabName"] = hTabName.Value;
                if ((this.Request.Params["__EVENTTARGET"] != null) && !((this.Request.Params["__EVENTTARGET"].Contains("Next"))
                                                                    || (this.Request.Params["__EVENTTARGET"].Contains("Last"))
                                                                    || (this.Request.Params["__EVENTTARGET"].Contains("Prev"))
                                                                    || (this.Request.Params["__EVENTTARGET"].Contains("First"))))
                {                    
                    XmlTemplate = GetMessageTemplateForPropType();
                    CallCWSFunctionBind("DataIntegratorAdaptor.ISOGetPropType", out sCWSresponse, XmlTemplate);

                    //sagarwal54 MITS 35386 end

                    //sagarwal54 MITS 35704 start
                    XmlTemplate = GetMessageTemplateForAdditionalClaimant();
                    CallCWSFunctionBind("DataIntegratorAdaptor.ISOGetPartyToClaimTypes", out sCWSresponse, XmlTemplate);

                    //mihtesham MITS 35711 start
                    XmlTemplate = GetMessageTemplateForSPRoleType();
                    CallCWSFunctionBind("DataIntegratorAdaptor.ISOGetSPRoleType", out sCWSresponse, XmlTemplate);



                    //mihtesham MITS 35711 end

                    string script = "<script>window.location.href=window.location.href;</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "RefereshParentPage", script);
                    //sagarwal54 MITS 35704 end
                    }
                }
               

                //ipuri Mits:30917 start
                if (hdISOType.Value == "Import")
                {
                    RetrieveListSettings(); //Retrieve List Settings for Claim Match Report and Submission Rejection
                }
                //ipuri End
                //sagarwal54 MITS 33168 start
                if (chkIndex.Checked == true)
                {
                    lblClaimMatchDocText.Enabled = true;
                    lblClaimMatchDocType.Enabled = true;
                    txtClaimMatchDocText.Enabled = true;
                    ddlClaimMatchDocType.Enabled = true;
                }
                else
                {
                    lblClaimMatchDocText.Enabled = false;
                    lblClaimMatchDocType.Enabled = false;
                    txtClaimMatchDocText.Enabled = false;
                    ddlClaimMatchDocType.Enabled = false;
                }
                //sagarwal54 MITS 33168 end
            }
            //Register scripts for Tab control
            if (!(hTabName.Value == "PropertyMappings" || hTabName.Value == "ISOClaimPartyType" || hTabName.Value == "SPRoleMappings"))
            {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('" + hTabName.Value + "')};", true);
            }
            //ipuri Mits:34373 Start
            if (chkExcludeStates.Checked == false && chkIncludeStates.Checked == false)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "scr", "document.getElementById('lstStates').disabled=true;", true);
            }
            //ipuri Mits 34373 End
           
            //Enable/disable OptionSet Name on the UI.
            if (CommonFunctions.OptionsetId > 0)
            {
                txtOptionName.Enabled = false;
                    txtOptName.Enabled = false; //ipuri Mits:30917
            }
            else
            {
                txtOptionName.Enabled = true;
                txtOptName.Enabled = true;  //ipuri Mits:30917
            }

            // npadhy JIRA RMACLOUD-2418 - ISO Compatible with Cloud
            // If we have cloud enabled then Show the Upload Attachments Tab And Associate appropriate settings with it
            if (AppHelper.ClientId > 0)
            {
                // Associate Properties for Upload Attachments
                UploadDocumentAttachments.RemoveButtonText = "Remove";
                UploadDocumentAttachments.CancelText = "Cancel";
                UploadDocumentAttachments.UploadingMsg = "Uploading...";
                UploadDocumentAttachments.TableHeaderTemplate = UploadDocumentAttachments.TableHeaderTemplate.Substring(0, 29) + "Files to upload" + "</td>";
                UploadDocumentAttachments.CancelAllMsg = "Cancel all Uploads";
            }
            else
            {
                // npadhy JIRA RMACLOUD-2418 - ISO Compatible with Cloud
                // We mighty move the Attachment div to a new Tab. In that case we will use the following statement
                // So commention the code for now.
                // Hide the Tab
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "HideAttachments", " $('document').ready(function () {$('#TABSAttachments').hide();});", true);

                // Hide the section
                divAttachments.Visible = false;
            }
        }
        
        //ipuri Mits:30917 Start : Function to retrieve Claim Match and Submission List on postback
        protected void RetrieveListSettings()
        {
            ClaimMatchUserId.Text = string.Empty;
            ClaimMatchUserId.Text = hdCMOtherUserID.Value.Trim();
            //ClaimMatchUserId.Text = hdCMOtherUserID.Value.TrimStart(',');
            //ClaimMatchUserId.Text = hdCMOtherUserID.Value.TrimEnd(',');
            ClaimMatchUserId.Text = ClaimMatchUserId.Text.Replace(" ", ",");

            ClaimMatchUserName.Text = string.Empty;
            ClaimMatchUserName.Text = hdCMOtherUserName.Value.Trim();
            //ClaimMatchUserName.Text = hdCMOtherUserName.Value.TrimStart(',');
            //ClaimMatchUserName.Text = hdCMOtherUserName.Value.TrimEnd(',');
            ClaimMatchUserName.Text = ClaimMatchUserName.Text.Replace(" ", ",");
            
            SubmissionUserId.Text = string.Empty;
            SubmissionUserId.Text = hdSROtherUserID.Value.Trim();
            //SubmissionUserId.Text = hdSROtherUserID.Value.TrimStart(',');
            //SubmissionUserId.Text = hdSROtherUserID.Value.TrimEnd(',');
            SubmissionUserId.Text = SubmissionUserId.Text.Replace(" ", ",");

            SubmissionUserName.Text = string.Empty;
            SubmissionUserName.Text = hdSROtherUserName.Value.Trim();
            //SubmissionUserName.Text = hdSROtherUserName.Value.TrimStart(',');
            //SubmissionUserName.Text = hdSROtherUserName.Value.TrimEnd(',');
            SubmissionUserName.Text = SubmissionUserName.Text.Replace(" ", ",");

            if (chkClaimMatch.Checked == true && rbtClaimUser.Checked == true)
            {
                //sClUser = ClaimMatchUserId.Text.Replace(" ", ",");
                //sUserName = ClaimMatchUserName.Text.Replace(" ", ",");
                if (!string.IsNullOrEmpty(ClaimMatchUserId.Text))
                {
                    string[] aClUser = ClaimMatchUserId.Text.Split(',');
                    string[] aUserName = ClaimMatchUserName.Text.Split(',');
                    Dictionary<string, string> aClaimList = new Dictionary<string, string>();

                    for (int i = 0; i < aUserName.Length; i++)
                    {
                        aClaimList[aClUser[i]] = aUserName[i];
                    }

                    lstClaimOtherUser.Items.Clear();
                    lstClaimOtherUser.DataSource = aClaimList;
                    lstClaimOtherUser.DataTextField = "Value";
                    lstClaimOtherUser.DataValueField = "Key";
                    lstClaimOtherUser.DataBind();
                }
            }
            
            if (chkboxSubmissor.Checked == true && rbtSubmissorUser.Checked == true)
            {
                if (!string.IsNullOrEmpty(SubmissionUserId.Text))
                {
                    string[] aSbUser = SubmissionUserId.Text.Split(',');
                    string[] aSbUserName = SubmissionUserName.Text.Split(',');
                    Dictionary<string, string> aSubmissorList = new Dictionary<string, string>();

                    for (int i = 0; i < aSbUserName.Length; i++)
                    {
                        aSubmissorList[aSbUser[i]] = aSbUserName[i];
                    }

                    lstSubmissorOtherUser.Items.Clear();
                    lstSubmissorOtherUser.DataSource = aSubmissorList;
                    lstSubmissorOtherUser.DataTextField = "Value";
                    lstSubmissorOtherUser.DataValueField = "Key";
                    lstSubmissorOtherUser.DataBind();
                }
            }
            //if (bError == true)
            //{
            //    if (chkError.Checked == true && chkboxSubmissor.Checked == false)
            //    {
            //        chkboxSubmissor.Enabled = true;
            //        rbtSubmissorAdjuster.Enabled = true;
            //        rbtSubmissorUser.Enabled = true;
            //        chkboxSubmissor.Checked = true;
            //        if (string.IsNullOrEmpty(SubmissionUserId.Text))
            //        {
            //            rbtSubmissorAdjuster.Checked = true;
            //        }
            //        else
            //        {
            //            string[] aSbUser = SubmissionUserId.Text.Split(',');
            //            string[] aSbUserName = SubmissionUserName.Text.Split(',');
            //            Dictionary<string, string> aSubmissorList = new Dictionary<string, string>();

            //            for (int i = 0; i < aSbUserName.Length; i++)
            //            {
            //                aSubmissorList[aSbUser[i]] = aSbUserName[i];
            //            }

            //            lstSubmissorOtherUser.Items.Clear();
            //            lstSubmissorOtherUser.DataSource = aSubmissorList;
            //            lstSubmissorOtherUser.DataTextField = "Value";
            //            lstSubmissorOtherUser.DataValueField = "Key";
            //            lstSubmissorOtherUser.DataBind();
            //        }
            //    }
            //    bError = false;
            //}

            
        }
        //ipuri end


        //******************************************************************************
        //Developer: Ankur Saxena
        //Date: 4/3/2009
        //Description: Function will retrieve settings from data integrator table and fill them on UI.
        //******************************************************************************
        protected void RetrieveSettings()
        {
            //DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
            Riskmaster.Models.DataIntegratorModel objDIModel = new Riskmaster.Models.DataIntegratorModel();
            //Vsoni5 : MITS 22565 : Exception handling done for Error display on UI and error logging in service error log.
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();

            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }

            bool isError = false;

            try
            {
                m_iOptionSetID = CommonFunctions.OptionsetId;

                hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);

                objDIModel.OptionSetID = m_iOptionSetID;
                objDIModel.ModuleName = m_sModuleName;

                //objDIModel = objDIService.RetrieveSettings(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);

                //ihtesham-start
                hdClaimLOB.Value = objDIModel.LOB.ToString();
                //ihtesham-end

                //if (objDIModel.OptionSetID != 0)
                //{
                //    txtOptionName.Enabled = false;
                //}
                //txtOptionName.Text = objDIModel.OptionSetName;

                ListBox1.DataSource = objDIModel.dsClaimTypeGC;
                ListBox1.DataTextField = objDIModel.dsClaimTypeGC.Tables[0].Columns[0].ToString();
                ListBox1.DataValueField = objDIModel.dsClaimTypeGC.Tables[0].Columns[1].ToString();
                ListBox1.DataBind();
                ListBox1.Items.Insert(0,"All Sub Claim Types");

                ListBox2.DataSource = objDIModel.dsClaimTypeVA;
                ListBox2.DataTextField = objDIModel.dsClaimTypeVA.Tables[0].Columns[0].ToString();
                ListBox2.DataValueField = objDIModel.dsClaimTypeVA.Tables[0].Columns[1].ToString();
                ListBox2.DataBind();
                ListBox2.Items.Insert(0, "All Sub Claim Types");

                ListBox3.DataSource = objDIModel.dsClaimTypeWC;
                ListBox3.DataTextField = objDIModel.dsClaimTypeWC.Tables[0].Columns[0].ToString();
                ListBox3.DataValueField = objDIModel.dsClaimTypeWC.Tables[0].Columns[1].ToString();
                ListBox3.DataBind();
                ListBox3.Items.Insert(0, "All Sub Claim Types");

                //subhendu 11/01/2010 MITS 22785 : Retrieve the default setting for the Claim type listbox in Property claim
                ListBox4.DataSource = objDIModel.dsClaimTypePC;
                ListBox4.DataTextField = objDIModel.dsClaimTypePC.Tables[0].Columns[0].ToString();
                ListBox4.DataValueField = objDIModel.dsClaimTypePC.Tables[0].Columns[1].ToString();
                ListBox4.DataBind();
                ListBox4.Items.Insert(0, "All Sub Claim Types");

                lstClaimantType.DataSource = objDIModel.dsClaimantType;
                lstClaimantType.DataTextField = objDIModel.dsClaimantType.Tables[0].Columns[0].ToString();
                lstClaimantType.DataValueField = objDIModel.dsClaimantType.Tables[0].Columns[1].ToString();
                lstClaimantType.DataBind();

                lstStates.DataSource = objDIModel.dsISOJuris;
                lstStates.DataTextField = objDIModel.dsISOJuris.Tables[0].Columns[0].ToString();
                lstStates.DataValueField = objDIModel.dsISOJuris.Tables[0].Columns[1].ToString();
                lstStates.DataBind();
                //lstStates.Enabled = false;	//ipuri Mits-34373
                //sagarwal54 08/23/2013 MITS 33168 Start
                ddlLossInjuryGC.DataSource = objDIModel.dsLossInjury;
                ddlLossInjuryGC.DataTextField = objDIModel.dsLossInjury.Tables[0].Columns[0].ToString();
                ddlLossInjuryGC.DataValueField = objDIModel.dsLossInjury.Tables[0].Columns[1].ToString();
                ddlLossInjuryGC.DataBind();
                ddlLossInjuryGC.Items.Insert(0, "--Select--");

                ddlLossInjuryVC.DataSource = objDIModel.dsLossInjury;
                ddlLossInjuryVC.DataTextField = objDIModel.dsLossInjury.Tables[0].Columns[0].ToString();
                ddlLossInjuryVC.DataValueField = objDIModel.dsLossInjury.Tables[0].Columns[1].ToString();
                ddlLossInjuryVC.DataBind();
                ddlLossInjuryVC.Items.Insert(0, "--Select--");

                ddlLossInjuryWC.DataSource = objDIModel.dsLossInjury;
                ddlLossInjuryWC.DataTextField = objDIModel.dsLossInjury.Tables[0].Columns[0].ToString();
                ddlLossInjuryWC.DataValueField = objDIModel.dsLossInjury.Tables[0].Columns[1].ToString();
                ddlLossInjuryWC.DataBind();
                ddlLossInjuryWC.Items.Insert(0, "--Select--");

                ddlLossInjuryPC.DataSource = objDIModel.dsLossInjury;
                ddlLossInjuryPC.DataTextField = objDIModel.dsLossInjury.Tables[0].Columns[0].ToString();
                ddlLossInjuryPC.DataValueField = objDIModel.dsLossInjury.Tables[0].Columns[1].ToString();
                ddlLossInjuryPC.DataBind();
                ddlLossInjuryPC.Items.Insert(0, "--Select--");
                
                ddlClaimMatchDocType.DataSource = objDIModel.dsDocumentType;
                ddlClaimMatchDocType.DataTextField = objDIModel.dsDocumentType.Tables[0].Columns[0].ToString();
                ddlClaimMatchDocType.DataValueField = objDIModel.dsDocumentType.Tables[0].Columns[1].ToString();
                ddlClaimMatchDocType.DataBind();
                ddlClaimMatchDocType.Items.Insert(0, "--Select--");

                ddlClaimMatchEnhancedNotes.DataSource = objDIModel.dsEnhancedNoteType;
                ddlClaimMatchEnhancedNotes.DataTextField = objDIModel.dsEnhancedNoteType.Tables[0].Columns[0].ToString();
                ddlClaimMatchEnhancedNotes.DataValueField = objDIModel.dsEnhancedNoteType.Tables[0].Columns[1].ToString();
                ddlClaimMatchEnhancedNotes.DataBind();
                ddlClaimMatchEnhancedNotes.Items.Insert(0, "--Select--");

                ddlSubRejEnhancedNotes.DataSource = objDIModel.dsEnhancedNoteType;
                ddlSubRejEnhancedNotes.DataTextField = objDIModel.dsEnhancedNoteType.Tables[0].Columns[0].ToString();
                ddlSubRejEnhancedNotes.DataValueField = objDIModel.dsEnhancedNoteType.Tables[0].Columns[1].ToString();
                ddlSubRejEnhancedNotes.DataBind();
                ddlSubRejEnhancedNotes.Items.Insert(0, "--Select--");

                ddlClaimMatchDiaryType.DataSource = objDIModel.dsDiaryType;
                ddlClaimMatchDiaryType.DataTextField = objDIModel.dsDiaryType.Tables[0].Columns[0].ToString();
                ddlClaimMatchDiaryType.DataValueField = objDIModel.dsDiaryType.Tables[0].Columns[1].ToString();
                ddlClaimMatchDiaryType.DataBind();
                ddlClaimMatchDiaryType.Items.Insert(0, "--Select--");

                ddlSubRejDiaryType.DataSource = objDIModel.dsDiaryType;
                ddlSubRejDiaryType.DataTextField = objDIModel.dsDiaryType.Tables[0].Columns[0].ToString();
                ddlSubRejDiaryType.DataValueField = objDIModel.dsDiaryType.Tables[0].Columns[1].ToString();
                ddlSubRejDiaryType.DataBind();
                ddlSubRejDiaryType.Items.Insert(0, "--Select--");

                //sagarwal54 08/23/2013 MITS 33168 End

                CheckBox2.Checked = true;
                ListBox1.Enabled = false;
                ListBox2.Enabled = false;
                ListBox3.Enabled = false;
                ListBox4.Enabled = false; //subhendu 11/01/2010 MITS 22785 : Added for Property claims.

                Dictionary<string, string> ISO_Parms = new Dictionary<string, string>();
                ISO_Parms = objDIModel.Parms;
                
                //mihtesham - start
                bIsCarrier.Value = ((objDIModel.bIsCarrier) ? -1 : 0).ToString();
                //mihtesham - end

                #region Export
                //ipuri start Mits:30917
                if (ISO_Parms["ISO_Type"] == "Export" || ISO_Parms["ISO_Type"] == "EXPORT") //sagarwal54 MITS 28149
                {
                    hTabName.Value = "Settings";
                    if (objDIModel.OptionSetID != 0)
                    {
                        txtOptionName.Enabled = false;
                    }
                    txtOptionName.Text = objDIModel.OptionSetName;
                //ipuri end
				
                TextBox1.Text = ISO_Parms["CompanyCIBNumber"];
                TextBox2.Text = ISO_Parms["CompanyName"];

                if (ISO_Parms["UseDefaultInsuredName"] == "1")
                    CheckBox1.Checked = true;
                else
                    CheckBox1.Checked = false;

                TextBox3.Text = ISO_Parms["DefaultInsuredName"];
                TextBox4.Text = ISO_Parms["DefaultInsuredAddr1"];
                TextBox5.Text = ISO_Parms["DefaultInsuredAddr2"];
                TextBox6.Text = ISO_Parms["DefaultInsuredCity"];
                TextBox7.Text = ISO_Parms["DefaultInsuredState"];

                if (ISO_Parms["InitialAction"] == "1")
                    CheckBox2.Checked = true;
                else
                    CheckBox2.Checked = false;

                if (ISO_Parms["ReplaceAction"] == "1")
                    CheckBox3.Checked = true;
                else
                    CheckBox3.Checked = false;

                if (objDIModel.bIsCarrier == false)
                {
                    CheckBox19.Enabled = true;
                    if (ISO_Parms["SubClaimnoForPolicy"] == "1")
                        CheckBox19.Checked = true;
                    else
                        CheckBox19.Checked = false;
                }
                //sagarwal54 08/23/2013 MITS 33168 Start
                if (!(String.IsNullOrEmpty(ISO_Parms["LossInjuryGC"] )))
                {
                    ddlLossInjuryGC.SelectedValue = ISO_Parms["LossInjuryGC"];
                }

                if (objDIModel.bIsCarrier == false)
                {
                    ddlLossInjuryVC.Enabled = true;

                    if (!(String.IsNullOrEmpty(ISO_Parms["LossInjuryVA"])))
                    { ddlLossInjuryVC.SelectedValue = ISO_Parms["LossInjuryVA"]; }
                }
                else
                {
                    ddlLossInjuryVC.Enabled = false;
                }

                if (!(String.IsNullOrEmpty(ISO_Parms["LossInjuryWC"])))
                {
                    ddlLossInjuryWC.SelectedValue = ISO_Parms["LossInjuryWC"];
                }

                if (objDIModel.bIsCarrier == false)
                {
                    ddlLossInjuryPC.Enabled = true;
                    if (!(String.IsNullOrEmpty(ISO_Parms["LossInjuryPC"])))  //subhendu 11/01/2010 MITS 22785 : Retriving the UI default setting(To Report Loss or Injuries Use Event Description In:) for Property claim.
                    { ddlLossInjuryPC.SelectedValue = ISO_Parms["LossInjuryPC"]; }
                }
                else
                {
                    ddlLossInjuryPC.Enabled = false;
                }

                //sagarwal54 08/23/2013 MITS 33168 End
                if (ISO_Parms["ProcessSingleClaim"] == "1")
                {
                    CheckBox14.Checked = true;
                    TextBox8.Text = ISO_Parms["SingleClaimNo"];
                    //ipuri 08/08/2013 Mits:32648 Start
                    CheckBox18.Enabled = false;       //Claim Criteria
                    CheckBox18.Checked = false;
                    txtFromDate.Disabled = true;
                    //ML changes by Sravan Starts RMA-6037
                   // txtCompletedOnbtn1.Disabled = true;  
                    txtToDate.Disabled = true;
                    // txtCompletedOnbtn2.Disabled = true;  
                    divFromDate.Visible = false;//
                    divToDate.Visible = false;
                    //ML changes by Sravan End RMA-6037
                    txtFromDate.Value = "";            
                    txtToDate.Value = "";  
                    CheckBox20.Enabled = false;         //General claims
                    CheckBox20.Checked = false;
                    ListBox1.Enabled = false;
                    CheckBox21.Enabled = false;         //Vehicle Accident
                    CheckBox21.Checked = false;
                    ListBox2.Enabled = false;
                    CheckBox22.Enabled = false;         //Workers compensation
                    CheckBox22.Checked = false;
                    ListBox3.Enabled = false;
                    Chkbox_propClaim.Enabled = false;       //Property claim
                    Chkbox_propClaim.Checked = false;
                    ListBox4.Enabled = false;
                    chkExcludeStates.Enabled = false;
                    chkExcludeStates.Checked = false;
                    chkIncludeStates.Enabled = false;
                    chkIncludeStates.Checked = false;
                    //lstStates.Enabled = false;   //ipuri 12/13/2013 Mits 34373
                    ChkEligibleISOSub.Enabled = false;
                    ChkEligibleISOSub.Checked = false;
                    //ipuri 08/08/2013 Mits:32648 End
                }
                else
                {
                    CheckBox14.Checked = false;
                    TextBox8.Text = "";
                    //ipuri 08/08/2013 Mits:32648 Start
                    CheckBox18.Enabled = true;       //Claim Criteria
                    txtFromDate.Disabled = false;
                   // txtCompletedOnbtn1.Disabled = false;  //vkumar258 - RMA-6037
                    txtToDate.Disabled = false;
                    //txtCompletedOnbtn2.Disabled = false;  //vkumar258 - RMA-6037
                    divFromDate.Visible = true;
                    divToDate.Visible = true;
                    CheckBox20.Enabled = true;         //General claims
                    for (int iIndex = 0; iIndex < ListBox1.Items.Count; iIndex++)
                    {
                        ListBox1.Items[iIndex].Selected = false;
                    }
                    if (bIsCarrier.Value != "-1")
                    {
                        CheckBox21.Enabled = true;         //Vehicle Accident
                        for (int iIndex = 0; iIndex < ListBox2.Items.Count; iIndex++)
                        {
                            ListBox2.Items[iIndex].Selected = false;
                        }
                        Chkbox_propClaim.Enabled = true;       //Property claim
                        for (int iIndex = 0; iIndex < ListBox4.Items.Count; iIndex++)
                        {
                            ListBox4.Items[iIndex].Selected = false;
                        }
                    }

                    CheckBox22.Enabled = true;         //Workers compensation
                    for (int iIndex = 0; iIndex < ListBox3.Items.Count; iIndex++)
                    {
                        ListBox3.Items[iIndex].Selected = false;
                    }
                    //chkExcludeStates.Enabled = true;  //ipuri 01/07/2014 Mits 34373
                    //chkIncludeStates.Enabled = true;  //ipuri 01/07/2014 Mits 34373
                    for (int iIndex = 0; iIndex < lstStates.Items.Count; iIndex++)
                    {
                        lstStates.Items[iIndex].Selected = false;
                    }
                    ChkEligibleISOSub.Enabled = true;
                    //ipuri 08/08/2013 Mits:32648 End
                }

                if (ISO_Parms["UseDateOfClaimRange"] == "1")
                {
                    CheckBox18.Checked = true;
                    //ipuri Mits:33399 start
                    txtFromDate.Disabled = false;
                    txtToDate.Disabled = false;
                    //txtCompletedOnbtn1.Disabled = false;  //vkumar258 - RMA-6037
                    //txtCompletedOnbtn2.Disabled = false;   //vkumar258 - RMA-6037
                    //ipuri Mits:33399 end
                    divFromDate.Visible = true;
                    divToDate.Visible = true;
                }
                else
                {
                    CheckBox18.Checked = false;
                    //ipuri Mits:33399 start
                    txtFromDate.Disabled = true;
                    txtToDate.Disabled = true;
                    // txtCompletedOnbtn1.Disabled = true; //vkumar258 - RMA-6037
                    //txtCompletedOnbtn2.Disabled = true; //vkumar258 - RMA-6037
                    //ipuri Mits:33399 start
                    divFromDate.Visible = false;
                    divToDate.Visible = false;
                }

                string sDate = "";

                if (ISO_Parms["ClaimDateFrom"] != "")
                {
                    sDate = ISO_Parms["ClaimDateFrom"];
                    txtFromDate.Value = sDate.Substring(4, 2) + "/" + sDate.Substring(6, 2) + "/" + sDate.Substring(0, 4);
                }

                if (ISO_Parms["ClaimDateTo"] != "")
                {
                    sDate = ISO_Parms["ClaimDateTo"];
                    txtToDate.Value = sDate.Substring(4, 2) + "/" + sDate.Substring(6, 2) + "/" + sDate.Substring(0, 4);
                }

                if (ISO_Parms["OpenCloseClaims"] == "0")
                {
                    CheckBox16.Checked = true;
                    CheckBox17.Checked = true;
                }
                else if (ISO_Parms["OpenCloseClaims"] == "1")
                {
                    CheckBox16.Checked = true;
                    CheckBox17.Checked = false;
                }
                else if (ISO_Parms["OpenCloseClaims"] == "2")
                {
                    CheckBox16.Checked = false;
                    CheckBox17.Checked = true;
                }
                else
                {
                    CheckBox16.Checked = false;
                    CheckBox17.Checked = false;
                }

                if (ISO_Parms["LOBGC"] == "1")
                {
                    CheckBox20.Checked = true;
                    ListBox1.Enabled = true;
                }
                else
                {
                    CheckBox20.Checked = false;
                    ListBox1.Enabled = false;
                }

                if (objDIModel.bIsCarrier == false)
                {
                    if (ISO_Parms["ProcessSingleClaim"] != "1")     //ipuri 08/12/2013 Mits:32648
                    {
                        CheckBox21.Enabled = true;
                    }
                    if (ISO_Parms["LOBVA"] == "1")
                    {
                        CheckBox21.Checked = true;
                        ListBox2.Enabled = true;
                    }
                    else
                    {
                        CheckBox21.Checked = false;
                        ListBox2.Enabled = false;
                    }
                }

                if (ISO_Parms["LOBWC"] == "1")
                {
                    CheckBox22.Checked = true;
                    ListBox3.Enabled = true;
                }
                else
                {
                    CheckBox22.Checked = false;
                    ListBox3.Enabled = false;
                }

                if (objDIModel.bIsCarrier == false)
                {
                    if (ISO_Parms["ProcessSingleClaim"] != "1")     //ipuri 08/12/2013 Mits:32648
                    {
                        Chkbox_propClaim.Enabled = true;
                    }
                    if (ISO_Parms["LOBPC"] == "1")  //subhendu 11/01/2010 MITS 22785 : Retrieving the UI default setting(Line of Business Criteria:) for property claim
                    {
                        Chkbox_propClaim.Checked = true;
                        ListBox4.Enabled = true;
                    }
                    else
                    {
                        Chkbox_propClaim.Checked = false;
                        ListBox4.Enabled = false;
                    }
                }

                if (ISO_Parms["ClaimSubTypeGC"] != "")
                {
                    string[] sClaimSubTypeGC = ISO_Parms["ClaimSubTypeGC"].Split(',');

                    foreach (string sClaimType in sClaimSubTypeGC)
                    {
                        int iIndex = 0;

                        for (iIndex = 0; iIndex < ListBox1.Items.Count; iIndex++)
                        {
                            if (ListBox1.Items[iIndex].Value == sClaimType)
                            {
                                ListBox1.Items[iIndex].Selected = true;
                            }
                        }
                    }
                }
                else
                {
                    ListBox1.Items[0].Selected = true;
                }
                
                if (ISO_Parms["ClaimSubTypeVA"] != "")
                {
                    string[] sClaimSubTypeVA = ISO_Parms["ClaimSubTypeVA"].Split(',');

                    foreach (string sClaimType in sClaimSubTypeVA)
                    {
                        int iIndex = 0;

                        for (iIndex = 0; iIndex < ListBox2.Items.Count; iIndex++)
                        {
                            if (ListBox2.Items[iIndex].Value == sClaimType)
                            {
                                ListBox2.Items[iIndex].Selected = true;
                            }
                        }
                    }
                }
                else
                {
                    ListBox2.Items[0].Selected = true;
                }

                if (ISO_Parms["ClaimSubTypeWC"] != "")
                {
                    string[] sClaimSubTypeWC = ISO_Parms["ClaimSubTypeWC"].Split(',');

                    foreach (string sClaimType in sClaimSubTypeWC)
                    {
                        int iIndex = 0;

                        for (iIndex = 0; iIndex < ListBox3.Items.Count; iIndex++)
                        {
                            if (ListBox3.Items[iIndex].Value == sClaimType)
                            {
                                ListBox3.Items[iIndex].Selected = true;
                            }
                        }
                    }
                }
                else
                {
                    ListBox3.Items[0].Selected = true;
                }


                if (ISO_Parms["ClaimSubTypePC"] != "")  //subhendu 11/01/2010 MITS 22785 : Retrieving the default selected Claim type code from listbox for Property claims.
                {
                    string[] sClaimSubTypePC = ISO_Parms["ClaimSubTypePC"].Split(',');

                    foreach (string sClaimType in sClaimSubTypePC)
                    {
                        int iIndex = 0;

                        for (iIndex = 0; iIndex < ListBox4.Items.Count; iIndex++)
                        {
                            if (ListBox4.Items[iIndex].Value == sClaimType)
                            {
                                ListBox4.Items[iIndex].Selected = true;
                            }
                        }
                    }
                }
                else
                {
                    ListBox4.Items[0].Selected = true;
                }

                if (ISO_Parms["ExcludeStates"] != "")
                {
                    // Developer - Subhendu : MITS 30839 : Start
                    chkExcludeStates.Checked = true;
                    //lstStates.Enabled = true;		ipuri Mits 34373
                    // Developer - Subhendu : MITS 30839 : End
                    //chkIncludeStates.Enabled = false;   //ipuri 08/22/2013 Mits:32648
                    string[] sExcludeStates = ISO_Parms["ExcludeStates"].Split(',');

                    foreach (string sState in sExcludeStates)
                    {
                        int iIndex = 0;

                        for (iIndex = 0; iIndex < lstStates.Items.Count; iIndex++)
                        {
                            if (lstStates.Items[iIndex].Value == sState)
                            {
                                lstStates.Items[iIndex].Selected = true;
                            }
                        }
                    }                    
                }
                else if (chkIncludeStates.Checked == false)     //ipuri 01/06/2014 Mits:34373 Start
                {
                    //chkExcludeStates.Checked = false;
                    //chkExcludeStates.Enabled = false;
                    //lstStates.Enabled = false;		
                    if (CheckBox22.Checked == true)
                    {
                        chkExcludeStates.Checked = false;
                    }
                    else
                    {
                        chkExcludeStates.Checked = false;
                        //chkExcludeStates.Enabled = false;
                    }
                    //ipuri 01/06/2014 Mits:34373 End
                }
                
                if (ISO_Parms["IncludeStates"] != "")// subhendu 9/28/2010 mits-20512 and 22524 added to retrive setting for include states
                {
                    // Developer - Subhendu : MITS 30839 : Start
                    chkIncludeStates.Checked = true;
                    //lstStates.Enabled = true;	//ipuri Mits:34373
                    // Developer - Subhendu : MITS 30839 : End
                    //chkExcludeStates.Enabled = false;   //ipuri 08/22/2013 Mits:32648
                    string[] sIncludeStates = ISO_Parms["IncludeStates"].Split(',');

                    foreach (string sState in sIncludeStates)
                    {
                        int iIndex = 0;

                        for (iIndex = 0; iIndex < lstStates.Items.Count; iIndex++)
                        {
                            if (lstStates.Items[iIndex].Value == sState)
                            {
                                lstStates.Items[iIndex].Selected = true;
                            }
                        }
                    }                                        
                }
                else if (chkExcludeStates.Checked == false)
                {
                    //ipuri 12/13/2013 Mits:34373 Start
                    if (CheckBox22.Checked == true)
                    {
                        chkIncludeStates.Checked = false;
                        }
                    else
                    {
                        chkIncludeStates.Checked = false;
                        //chkIncludeStates.Enabled = false;
                        }
                    //ipuri 12/13/2013 Mits:34373 End
                }
                if (ISO_Parms["EligibleISOSubmission"] == "1")  //subhendu 07/01/2010 MITS-(21241,21356)
                {
                    ChkEligibleISOSub.Checked = true;
                }
                else
                {
                    ChkEligibleISOSub.Checked = false;
                }

                //ipuri 12/11/2013 MITS-34500 Start

                if (ISO_Parms["CMSReporting"] == "1")
                    ChkCMSReporting.Checked = true;
                else
                    ChkCMSReporting.Checked = false;

                if (ISO_Parms["RequestRecalInfoIndicator"] == "Y")  
                {
                    ChkRequestRecalInfoIndicator.Checked = true;
                }
                else
                {
                    ChkRequestRecalInfoIndicator.Checked = false;
                }
                if (!(String.IsNullOrEmpty(ISO_Parms["SearchParty"])))
                {
                    ddSearchParty.SelectedValue = ISO_Parms["SearchParty"];
                }
                //ipuri 12/11/2013 MITS-34500 End

                //Added by agupta298 PMC GAP08 - RMA#5500 - Start
                if (ISO_Parms.ContainsKey("IncludeNMVTIS"))
                {
                    if (ISO_Parms["IncludeNMVTIS"] == "1")
                        ChkIncludeNMVTIS.Checked = true;
                    else
                        ChkIncludeNMVTIS.Checked = false;
                }
                else
                    ChkIncludeNMVTIS.Checked = false;

                if (ISO_Parms.ContainsKey("NMVTISReportingID"))
                {
                    txtNMVTISReportingID.Text = ISO_Parms["NMVTISReportingID"];
                }
                else
                    txtNMVTISReportingID.Text = string.Empty;
                //Added by agupta298 PMC GAP08 - RMA#5500  - End

                // makes sure that the tab selected is the one being displyaed on the screen.
                var hTabNameSession = (string)HttpContext.Current.Session["hTabName"];
                if (!string.IsNullOrEmpty(hTabNameSession))
                {
                    hTabName.Value = hTabNameSession;
                    HttpContext.Current.Session["hTabName"] = "";
                }
                ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('" + hTabName.Value + "')};", true);

                }

                #endregion

                #region Import
                //ipuri start Mits:30917 : Retrieve Import Settings
                else if (ISO_Parms["ISO_Type"] == "Import" || ISO_Parms["ISO_Type"] == "IMPORT") ////sagarwal54 MITS 28149
                {
                    hTabName.Value = "Import";
                    hdISOType.Value = "Import";

                    if (objDIModel.OptionSetID != 0)
                    {
                        txtOptName.Enabled = false;
                    }
                    txtOptName.Text = objDIModel.OptionSetName;

                    txtIndexFile.Text = ISO_Parms["IndexSourceFile"] + ISO_Parms["ISO_Import_Index_File_Name"];
                    txtErrorFile.Text = ISO_Parms["ErrorSourceFile"] + ISO_Parms["ISO_Import_Error_File_Name"];
                    
                    sIndexFileDir = ISO_Parms["IndexSourceFile"];
                    sIndexFileName = ISO_Parms["ISO_Import_Index_File_Name"];
                    sErrorFileDir = ISO_Parms["ErrorSourceFile"];
                    sErrorFileName = ISO_Parms["ISO_Import_Error_File_Name"];

                    if (ISO_Parms["ISO_Import_Index_File_Name"] != "")
                    {
                        chkIndex.Checked = true;
                        chkClaimMatch.Enabled = true;
                        FUIndexFile.Enabled = true;
                    }
                    else
                    {
                        chkIndex.Checked = false;
                        chkClaimMatch.Enabled = false;
                        FUIndexFile.Enabled = false;
                        txtIndexFile.Enabled = false;
                    }
                    if (ISO_Parms["ISO_Import_Error_File_Name"] != "")
                    {
                        chkError.Checked = true;
                        chkboxSubmissor.Enabled = true;
                        rbtSubmissorAdjuster.Enabled = true;
                        rbtSubmissorUser.Enabled = true;
                        FUErrorFile.Enabled = true;
                        chkboxSubmissor.Checked = true;
                    }
                    else
                    {
                        chkError.Checked = false;
                        chkboxSubmissor.Enabled = false;
                        rbtSubmissorAdjuster.Enabled = false;
                        rbtSubmissorUser.Enabled = false;
                        FUErrorFile.Enabled = false;
                        txtErrorFile.Enabled = false;
                        chkboxSubmissor.Checked = false;
                    }
                    if (ISO_Parms["Claim_Match"] == "0")
                    {
                        chkClaimMatch.Checked = false;
                        rbtClaimAdjuster.Enabled = false;
                        rbtClaimUser.Enabled = false;
                        lstClaimOtherUser.Disabled = true;
                        AutoFillUseridbtn1.Disabled = true;
                        OtherUseridbtndel1.Disabled = true;
                    }
                    else
                    {
                        chkClaimMatch.Enabled = true;
                        chkClaimMatch.Checked = true;
                        rbtClaimAdjuster.Enabled = true;
                        rbtClaimUser.Enabled = true;
                    }
                    
                    if (ISO_Parms["Submissor"] == "0")
                    {
                        chkboxSubmissor.Checked = false;
                        rbtSubmissorAdjuster.Enabled = false;
                        rbtSubmissorUser.Enabled = false;
                        lstSubmissorOtherUser.Disabled = true;
                        AutoFillUseridbtn.Disabled = true;
                        OtherUseridbtndel.Disabled = true;
                    }
                    else
                    {
                        chkboxSubmissor.Checked = true;
                        chkboxSubmissor.Enabled = true;
                        rbtSubmissorAdjuster.Enabled = true;
                        rbtSubmissorUser.Enabled = true;
                    }
                    if (ISO_Parms["Claim_Match_Adjuster"] == "0")
                    {
                        rbtClaimAdjuster.Checked = false;
                    }
                    else
                    {
                        rbtClaimAdjuster.Checked = true;
                    }
                    if (ISO_Parms["Submissor_Adjuster"] == "0")
                    {
                        rbtSubmissorAdjuster.Checked = false;
                    }
                    else
                    {
                        rbtSubmissorAdjuster.Checked = true;
                    }
                    if (ISO_Parms["Claim_Match_OtherUser"] == "0")
                    {
                        rbtClaimUser.Checked = false;
                        lstClaimOtherUser.Disabled = true;
                        AutoFillUseridbtn1.Disabled = true;
                        OtherUseridbtndel1.Disabled = true;
                    }
                    else
                    {
                        rbtClaimUser.Checked = true;
                        lstClaimOtherUser.Disabled = false;
                        AutoFillUseridbtn1.Disabled = false;
                        OtherUseridbtndel1.Disabled = false;
                    }
                    if (ISO_Parms["Submissor_OtherUser"] == "0")
                    {
                        rbtSubmissorUser.Checked = false;
                        lstSubmissorOtherUser.Disabled = true;
                        AutoFillUseridbtn.Disabled = true;
                        OtherUseridbtndel.Disabled = true;
                    }
                    else
                    {
                        rbtSubmissorUser.Checked = true;
                        lstSubmissorOtherUser.Disabled = false;
                        AutoFillUseridbtn.Disabled = false;
                        OtherUseridbtndel.Disabled = false;
                    }
                    if (ISO_Parms["Claim_Match_OtherUserList"] != "")
                    {
                        chkClaimMatch.Checked = true;
                        rbtClaimUser.Checked = true;
                        lstClaimOtherUser.Disabled = false;
                        AutoFillUseridbtn1.Disabled = false;
                        OtherUseridbtndel1.Disabled = false;

                        lstClaimOtherUser.DataSource = objDIModel.dsUserid;
                        lstClaimOtherUser.DataTextField = objDIModel.dsUserid.Tables[0].Columns[0].ToString();
                        lstClaimOtherUser.DataValueField = objDIModel.dsUserid.Tables[0].Columns[1].ToString();
                        lstClaimOtherUser.DataBind();

                        for (int i = 0; i < objDIModel.dsUserid.Tables[0].Rows.Count; i++)
                        {
                            ClaimMatchUserName.Text = ClaimMatchUserName.Text + " " + objDIModel.dsUserid.Tables[0].Rows[i]["LOGIN_NAME"].ToString();
                        }

                        ClaimMatchUserId.Text = ISO_Parms["Claim_Match_OtherUserList"];
                        
                    }
                    else
                    {
                        rbtClaimUser.Checked = false;
                    }
                    if (ISO_Parms["Submissor_OtherUserList"] != "")
                    {
                        chkboxSubmissor.Checked = true;
                        rbtSubmissorUser.Checked = true;
                        lstSubmissorOtherUser.Disabled = false;
                        AutoFillUseridbtn.Disabled = false;
                        OtherUseridbtndel.Disabled = false;
                        lstSubmissorOtherUser.DataSource = objDIModel.dsSubmissoUserid;
                        lstSubmissorOtherUser.DataTextField = objDIModel.dsSubmissoUserid.Tables[0].Columns[0].ToString();
                        lstSubmissorOtherUser.DataValueField = objDIModel.dsSubmissoUserid.Tables[0].Columns[1].ToString();
                        lstSubmissorOtherUser.DataBind();
                        
                        for (int i = 0; i < objDIModel.dsSubmissoUserid.Tables[0].Rows.Count; i++)
                        {
                            SubmissionUserName.Text = SubmissionUserName.Text + " " + objDIModel.dsSubmissoUserid.Tables[0].Rows[i]["LOGIN_NAME"].ToString();
                        }

                        SubmissionUserId.Text = ISO_Parms["Submissor_OtherUserList"];
                        
                    }
                    else
                    {
                        rbtSubmissorUser.Checked = false;
                    }
                    //sagarwal54 08/23/2013 MITS 33168 Start                                      
                    
                    if (String.Compare(ISO_Parms["Claim_Match"],"1") == 0)
                    {                     
                        ddlClaimMatchDiaryType.Enabled = true;
                        ddlClaimMatchDiaryType.SelectedValue = ISO_Parms["DiaryTypeClaimMatch"];
                        txtClaimMatchDiaryText.Enabled = true;
                        txtClaimMatchDiaryText.Text = ISO_Parms["DiaryTextClaimMatch"];
                        lblClaimMatchDiaryType.Enabled = true;
                        lblClaimMatchDiaryText.Enabled = true;

                    }

                    if (!(String.IsNullOrEmpty(ISO_Parms["ISO_Import_Index_File_Name"])))
                    {
                        lblClaimMatchDocType.Enabled = true;
                        lblClaimMatchDocText.Enabled = true;
                        ddlClaimMatchDocType.Enabled = true;
                        ddlClaimMatchDocType.SelectedValue = ISO_Parms["DocTypeClaimMatch"];
                        txtClaimMatchDocText.Enabled = true;
                        txtClaimMatchDocText.Text = ISO_Parms["DocTitleClaimMatch"];
                        chkClaimMatchForEnhanced.Enabled = true;
                        hdntxtClaimMatchDocText.Value = ISO_Parms["DocTitleClaimMatch"];
                    }
                    else
                    {
                        lblClaimMatchDocText.Enabled = false;
                        lblClaimMatchDocType.Enabled = false;
                        ddlClaimMatchDocType.Enabled = false;
                        txtClaimMatchDocText.Enabled = false;
                    }

                    if (!(String.IsNullOrEmpty(ISO_Parms["ISO_Import_Error_File_Name"])))
                    {
                        chkSubmissorForEnhanced.Enabled = true;
                    }

                    if (String.Compare(ISO_Parms["Submissor"], "1") == 0)
                    {
                        ddlSubRejDiaryType.Enabled = true;
                        ddlSubRejDiaryType.SelectedValue = ISO_Parms["DiaryTypeRejection"];
                        txtSubRejDiaryText.Enabled = true;
                        txtSubRejDiaryText.Text = ISO_Parms["DiaryTextRejection"];
                        lblSubRejDiaryText.Enabled = true;
                        lblSubRejDiaryType.Enabled = true;
                    }

                    
                    if (String.Compare(ISO_Parms["CreateEnhancedNoteClaimMatch"],"1") == 0)
                    {
                        chkClaimMatchForEnhanced.Enabled = true;
                        chkClaimMatchForEnhanced.Checked = true;
                        ddlClaimMatchEnhancedNotes.Enabled = true;
                        ddlClaimMatchEnhancedNotes.SelectedValue = ISO_Parms["EnhNoteTypeClaimMatch"];
                        txtClaimMatchEnhancedNotes.Enabled = true;
                        txtClaimMatchEnhancedNotes.Text = ISO_Parms["EnhNoteTextClaimMatch"];
                        lblClaimMatchEnhancedNotesType.Enabled = true;
                        lblClaimMatchEnhancedNotesText.Enabled = true;
                    }

                    if (String.Compare(ISO_Parms["CreateEnhancedNoteRejection"],"1") == 0)
                    {
                        chkSubmissorForEnhanced.Enabled = true;
                        chkSubmissorForEnhanced.Checked = true;
                        ddlSubRejEnhancedNotes.Enabled = true;
                        ddlSubRejEnhancedNotes.SelectedValue = ISO_Parms["EnhNoteTypeRejection"];
                        txtSubRejEnhancedNotes.Enabled = true;
                        txtSubRejEnhancedNotes.Text = ISO_Parms["EnhNoteTextRejection"];
                        lblSubRejEnhancedNotesType.Enabled = true;
                        lblSubRejEnhancedNotestText.Enabled = true;
                    }
                    //sagarwal54 08/23/2013 MITS 33168 End
                }
                #endregion
                //ipuri end
            }
            catch (Exception ee)
            {
                //Vsoni5 : MITS 22565 : Exception thrown by Service Layer will be shown on UI.
                err.Add("DataIntegrator.Error", ee.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }

            // Retrieve ISO Mapping
            //mihtesham
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWSFunctionBind("DataIntegratorAdaptor.ISOGet", out sCWSresponse, XmlTemplate);                    
                //sagarwal54 MITS 35386 start
                XmlTemplate = GetMessageTemplateForPropType();
                CallCWSFunctionBind("DataIntegratorAdaptor.ISOGetPropType", out sCWSresponse, XmlTemplate);
                
                //sagarwal54 MITS 35386 end


                //mihtesham MITS 35711 start
                XmlTemplate = GetMessageTemplateForSPRoleType();
                CallCWSFunctionBind("DataIntegratorAdaptor.ISOGetSPRoleType", out sCWSresponse, XmlTemplate);

                //mihtesham MITS 35711 end
                //sagarwal54 MITS 35704 start
                XmlTemplate = GetMessageTemplateForAdditionalClaimant();
                CallCWSFunctionBind("DataIntegratorAdaptor.ISOGetPartyToClaimTypes", out sCWSresponse, XmlTemplate);
                //sagarwal54 MITS 35704 end
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
            //end
        }
        //sagarwal54 MITS 35386 start
        private XElement GetMessageTemplateForPropType()
        {
            string XML = @"<Message>
                          <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                          <Call>
                            <Function></Function>
                          </Call>
                          <Document>
                            <IsoPropertyList>
                              <listhead>
                                <RMA_PROPERTY_TYPE>rmA Property Type</RMA_PROPERTY_TYPE>
                                <ISO_PROPERTY_TYPE>ISO Property Type</ISO_PROPERTY_TYPE>
                                <RowId>RowId</RowId>
                              </listhead>
                            </IsoPropertyList>
                          </Document>
                        </Message>";
            XElement oElement = XElement.Parse(XML);
            return oElement;
        }

        // xml for deleting an entry in grid
        private XElement GetMessageTemplateForPropType(string selectedRowId)
        {
            string[] arrtmp = selectedRowId.Split('|');
            string strrmAPropType = arrtmp[1];
            string strISOPropType = arrtmp[2];            
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><IsoPropertyList>");            
            sXml = sXml.Append("<control name='rmA Property Type'>");
            sXml = sXml.Append(strrmAPropType);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='ISO Property Type'>");
            sXml = sXml.Append(strISOPropType);
            sXml = sXml.Append("</control>");            
            sXml = sXml.Append("</IsoPropertyList></Document>");
            sXml = sXml.Append("</Message>");

            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }

        //sagarwal54 MITS 35386 end

        private XElement GetMessageTemplateForSPRoleType()
        {
            string XML = @"<Message>
                          <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                          <Call>
                            <Function></Function>
                          </Call>
                          <Document>
                            <ISOSPRoleList>
                              <listhead>
                                <RMA_PROVIDER_TYPE>rmA Service Provider Roles</RMA_PROVIDER_TYPE>
                                <ISO_PROVIDER_TYPE>ISO Service Provider Roles</ISO_PROVIDER_TYPE>
                                <RowId>RowId</RowId>
                              </listhead>
                              <CurrentPage>1</CurrentPage>
                              <hdnPageSize>25</hdnPageSize>
                              <hdTotalPages>0</hdTotalPages>
                            </ISOSPRoleList>
                          </Document>
                        </Message>";
            XElement oElement = XElement.Parse(XML);
            return oElement;
        }

        // xml for deleting an entry in grid
        private XElement GetMessageTemplateForSPRoleType(string selectedRowId)
        {
            string[] arrtmp = selectedRowId.Split('|');
            string strrmARoleType = arrtmp[1];
            string strISORoleType = arrtmp[2];
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ISOSPRoleList>");
            sXml = sXml.Append("<control name='rmA Service Proivder Roles'>");
            sXml = sXml.Append(strrmARoleType);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='ISO Service Proivder Roles'>");
            sXml = sXml.Append(strISORoleType);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</ISOSPRoleList></Document>");
            sXml = sXml.Append("</Message>");

            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }

        //mihtesham MITS 35711 end

        //sagarwal54 MITS 35704 start
        private XElement GetMessageTemplateForAdditionalClaimant()
        {
            string XML = @"<Message>
                          <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                          <Call>
                            <Function></Function>
                          </Call>
                          <Document>
                            <ClaimParty>
                              <listhead>
                                <RMA_CLAIM_PARTY_TYPE>rmA Role Codes</RMA_CLAIM_PARTY_TYPE>
                                <ISO_CLAIM_PARTY_TYPE>ISO Role Codes</ISO_CLAIM_PARTY_TYPE>
                                <RowId>RowId</RowId>
                              </listhead>
                            </ClaimParty>
                          </Document>
                        </Message>";
            XElement oElement = XElement.Parse(XML);
            return oElement;
        }

        private XElement GetMessageTemplateForAddClaimant(string selectedRowId)
        {
            string[] arrtmp = selectedRowId.Split('|');
            string strrmAPropType = arrtmp[1];
            string strISOPropType = arrtmp[2];
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ClaimParty>");
            sXml = sXml.Append("<control name='rmA Additional Claimant Type'>");
            sXml = sXml.Append(strrmAPropType);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='ISO Additional Claimant Type'>");
            sXml = sXml.Append(strISOPropType);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</ClaimParty></Document>");
            sXml = sXml.Append("</Message>");

            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }

        //sagarwal54 MITS 35704 end

        //******************************************************************************
        //Developer: Ankur Saxena
        //Date: 4/3/2009
        //Description: Function will save setting into data integrator table.
        //******************************************************************************
        protected void OK_Click(object sender, EventArgs e)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();

            try
            {
                SaveSettings();
            }
            catch (Exception ex)
            {
                //npradeepshar 01/12/2010 MITS 23561 : Log the error into log file as well display it on UI
                err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        //npradeepshar 01/12/2010 MITS 23561
        /// <summary>
        /// Function will save setting into data integrator table.
        /// </summary>
        public void SaveSettings()
        {
            string sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
            m_iOptionSetID = Conversion.ConvertObjToInt(hdOptionsetId.Value);

            BusinessAdaptorErrors err = new BusinessAdaptorErrors();    //vgupta20 08/10/2009 - initiate the error handler. Club all errors
            bool bErr = false;

            if ((m_iOptionSetID == 0) && (txtOptionName.Text == ""))
            {
                err.Add("DataIntagrator.Error", "Optionset Name is blank.", BusinessAdaptorErrorType.Message);
                bErr = true;

            }

            if (TextBox1.Text.Length < 9)
            {
                err.Add("DataIntagrator.Error", "Please enter your complete 9 Character Customer Code.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }

            if (CheckBox1.Checked == true)
            {
                if (TextBox3.Text == "")
                {
                    err.Add("DataIntagrator.Error", "Please enter default insured name.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                if (TextBox4.Text == "")
                {
                    err.Add("DataIntagrator.Error", "Please enter default insured Address for Line1.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                if (TextBox6.Text == "")
                {
                    err.Add("DataIntagrator.Error", "Please enter default insured City.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                if (TextBox7.Text == "")
                {
                    err.Add("DataIntagrator.Error", "Please enter default insured State.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }                               
            }

            if ((CheckBox14.Checked == true) && (TextBox8.Text == ""))
            {
                err.Add("DataIntagrator.Error", "If you like to report on single claim then please enter a claim number", BusinessAdaptorErrorType.Message);
                bErr = true;
            }

            //ipuri 05/22/2013 Mits: 29099 Start

            //if ((CheckBox18.Checked == true) && ((txtFromDate.Value == "") || (txtToDate.Value == "")))
            //{
            //    err.Add("DataIntagrator.Error", "Please enter From and To Dates for Claim date range.", BusinessAdaptorErrorType.Message);
            //    bErr = true;
            //}

            if (CheckBox18.Checked == true)
            {
                if ((txtFromDate.Value == "") || (txtToDate.Value == ""))
                {
                    err.Add("DataIntagrator.Error", "Please enter From and To Dates for Claim date range.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                else
                {
                    DateTime fromdt = Convert.ToDateTime(txtFromDate.Value);
                    DateTime todt = Convert.ToDateTime(txtToDate.Value);
                    if (fromdt.Date > todt.Date)
                    {
                        err.Add("DataIntagrator.Error", "From Date cannot be later than the To Date", BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }
                }
            }
            //ipuri 05/22/2013 Mits: 29099 End

            if ((CheckBox2.Checked == false) && (CheckBox3.Checked == false))              //vgupta20 08/10/2009
            {
                err.Add("DataIntagrator.Error", "Please select a Reporting Action.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }

            if ((CheckBox16.Checked == false) && (CheckBox17.Checked == false))              //vgupta20 08/10/2009
            {
                err.Add("DataIntagrator.Error", "Please select a Claim Criteria.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }
            //subhendu 11/01/2010 MITS 22785 : Property claim checkbox criteria added to the following IF loop.
            if ((CheckBox20.Checked == false) && (CheckBox21.Checked == false) && (CheckBox22.Checked == false) && (Chkbox_propClaim.Checked == false))      //vgupta20 08/10/2009
            {
                //Start: Shubhanjali 04/23/2013 : MITS 28149
                if (CheckBox14.Checked == false)
                {
                    err.Add("DataIntagrator.Error", "Please select a Line of Business Criteria.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                //End: Shubhanjali 04/23/2013 : MITS 28149
            }
            //sagarwal54 09/09/2013 MITS 33168 start
            if (CheckBox20.Checked == true && ddlLossInjuryGC.SelectedIndex == 0)
            {
                err.Add("DataIntagrator.Error", "Please select a value for General Claims in 'To Report Loss or Injuries:'.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }
            if (CheckBox21.Checked == true &&  ddlLossInjuryVC.SelectedIndex == 0)
            {
                err.Add("DataIntagrator.Error", "Please select a value for Vehicle Claims in 'To Report Loss or Injuries:'.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }
            if (CheckBox22.Checked == true && ddlLossInjuryWC.SelectedIndex == 0)
            {
                err.Add("DataIntagrator.Error", "Please select a value for Workers' Compensation Claims in 'To Report Loss or Injuries:'.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }
            if (Chkbox_propClaim.Checked == true && ddlLossInjuryPC.SelectedIndex == 0)
            {
                err.Add("DataIntagrator.Error", "Please select a value for Property Claims in 'To Report Loss or Injuries:'.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }
            //sagarwal54 09/09/2013 MITS 33168 End
			//ipuri Mits:33399 Start
            if (bErr == true)
            {
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                bErr = false;
                return;
            }
			//ipuri Mits:33399 end
           //kkaur25 SOA changes
            DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
            Riskmaster.Models.DataIntegratorModel objDIModel = new Riskmaster.Models.DataIntegratorModel();
            Riskmaster.Models.DataIntegratorModel objDIModelS = new Riskmaster.Models.DataIntegratorModel();
          //kkaur25 SOA changes end
            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }

            bool isError = false;
            int iOptionSetID;


            Dictionary<string, string> ISO_Parms = new Dictionary<string, string>();

            //
            // Populate ISO_Parms Dictionary
            //
            //Iti Mits:30917 Start : Adding Import Nodes to the XML 
            
            ISO_Parms.Add("ISO_Type", "Export");
            ISO_Parms.Add("IndexSourceFile", "");
            ISO_Parms.Add("ISO_Import_Index_File_Name", "");
            ISO_Parms.Add("ErrorSourceFile", "");
            ISO_Parms.Add("ISO_Import_Error_File_Name", "");
            ISO_Parms.Add("Claim_Match", "0");
            ISO_Parms.Add("Submissor", "0");
            ISO_Parms.Add("Claim_Match_Adjuster", "0");
            ISO_Parms.Add("Submissor_Adjuster", "0");
            ISO_Parms.Add("Claim_Match_OtherUser", "0");
            ISO_Parms.Add("Submissor_OtherUser", "0");
            ISO_Parms.Add("Claim_Match_OtherUserList", "");
            ISO_Parms.Add("Submissor_OtherUserList", "");
            //sagarwal54 08/23/2013 MITS 33168 Start
            ISO_Parms.Add("CreateEnhancedNoteClaimMatch", "0");
            ISO_Parms.Add("EnhNoteTypeClaimMatch", "0");
            ISO_Parms.Add("EnhNoteTextClaimMatch", "");
            ISO_Parms.Add("CreateEnhancedNoteRejection", "0");
            ISO_Parms.Add("EnhNoteTypeRejection", "0");
            ISO_Parms.Add("EnhNoteTextRejection", "");
            ISO_Parms.Add("DiaryTypeClaimMatch", "0");
            ISO_Parms.Add("DiaryTextClaimMatch", "");
            ISO_Parms.Add("DiaryTypeRejection", "0");
            ISO_Parms.Add("DiaryTextRejection", "");
            ISO_Parms.Add("DocTypeClaimMatch", "0");
            ISO_Parms.Add("DocTitleClaimMatch", "");
            //sagarwal54 08/23/2013 MITS 33168 End
            //Iti Mits:30917 End

            ISO_Parms.Add("CompanyCIBNumber", TextBox1.Text);
            ISO_Parms.Add("CompanyName", TextBox2.Text);

            if (CheckBox1.Checked == true)
                ISO_Parms.Add("UseDefaultInsuredName", "1");
            else
                ISO_Parms.Add("UseDefaultInsuredName", "0");


            ISO_Parms.Add("DefaultInsuredName", TextBox3.Text);
            ISO_Parms.Add("DefaultInsuredAddr1", TextBox4.Text);
            ISO_Parms.Add("DefaultInsuredAddr2", TextBox5.Text);
            ISO_Parms.Add("DefaultInsuredCity", TextBox6.Text);
            ISO_Parms.Add("DefaultInsuredState", TextBox7.Text);

            if (CheckBox2.Checked == true)
                ISO_Parms.Add("InitialAction", "1");
            else
                ISO_Parms.Add("InitialAction", "0");

            if (CheckBox3.Checked == true)
                ISO_Parms.Add("ReplaceAction", "1");
            else
                ISO_Parms.Add("ReplaceAction", "0");

            if (CheckBox19.Checked == true)
                ISO_Parms.Add("SubClaimnoForPolicy", "1");
            else
                ISO_Parms.Add("SubClaimnoForPolicy", "0");
            //sagarwal54 08/23/2013 MITS 33168 Start  
            if (ddlLossInjuryGC.SelectedIndex > 0)
            {
                ISO_Parms.Add("LossInjuryGC", ddlLossInjuryGC.SelectedItem.Value);
            }
            else
            {
                ISO_Parms.Add("LossInjuryGC", "");
            }

            if (ddlLossInjuryPC.SelectedIndex > 0)
            {
                ISO_Parms.Add("LossInjuryPC", ddlLossInjuryPC.SelectedItem.Value);
            }
            else
            {
                ISO_Parms.Add("LossInjuryPC", "");
            }
            if (ddlLossInjuryVC.SelectedIndex > 0)
            {
                ISO_Parms.Add("LossInjuryVA", ddlLossInjuryVC.SelectedItem.Value);
            }
            else
            {
                ISO_Parms.Add("LossInjuryVA", "");
            }
            if (ddlLossInjuryWC.SelectedIndex > 0)
            {
                ISO_Parms.Add("LossInjuryWC", ddlLossInjuryWC.SelectedItem.Value);
            }
            else
            {
                ISO_Parms.Add("LossInjuryWC", "");
            }
            //sagarwal54 08/23/2013 MITS 33168 End
            ISO_Parms.Add("UseEventDescDI", "0");

            if (CheckBox14.Checked == true)
            {
                ISO_Parms.Add("ProcessSingleClaim", "1");
                ISO_Parms.Add("SingleClaimNo", TextBox8.Text);
            }
            else
            {
                ISO_Parms.Add("ProcessSingleClaim", "0");
                ISO_Parms.Add("SingleClaimNo", "");
            }

            if (CheckBox18.Checked == true)
            {
                ISO_Parms.Add("UseDateOfClaimRange", "1");

                string dDate = "";
                dDate = txtFromDate.Value.Substring(6, 4) + txtFromDate.Value.Substring(0, 2) + txtFromDate.Value.Substring(3, 2);
                ISO_Parms.Add("ClaimDateFrom", dDate);

                dDate = txtToDate.Value.Substring(6, 4) + txtToDate.Value.Substring(0, 2) + txtToDate.Value.Substring(3, 2);
                ISO_Parms.Add("ClaimDateTo", dDate);
            }
            else
            {
                ISO_Parms.Add("UseDateOfClaimRange", "0");
                ISO_Parms.Add("ClaimDateFrom", "");
                ISO_Parms.Add("ClaimDateTo", "");
            }

            if ((CheckBox16.Checked == true) && (CheckBox17.Checked == true))
            {
                ISO_Parms["OpenCloseClaims"] = "0";
            }
            else if ((CheckBox16.Checked == true) && (CheckBox17.Checked == false))
            {
                ISO_Parms["OpenCloseClaims"] = "1";
            }
            else if ((CheckBox16.Checked == false) && (CheckBox17.Checked == true))
            {
                ISO_Parms["OpenCloseClaims"] = "2";
            }
            else
            {
                ISO_Parms["OpenCloseClaims"] = "0";
            }

            int iIndex = 0;
            string sClaimType = "";

            if (CheckBox20.Checked == true)
            {
                ISO_Parms.Add("LOBGC", "1");

                for (iIndex = 0; iIndex < ListBox1.Items.Count; iIndex++)
                {
                    if (ListBox1.Items[0].Selected == true)
                    {
                        if (iIndex > 0)
                        {
                            sClaimType = sClaimType + ListBox1.Items[iIndex].Value.ToString() + ",";
                        }
                    }
                    else if (ListBox1.Items[iIndex].Selected == true)
                    {
                        sClaimType = sClaimType + ListBox1.Items[iIndex].Value.ToString() + ",";
                    }
                }
                if (sClaimType != "")
                {
                    sClaimType = sClaimType.Substring(0, sClaimType.Length - 1);
                }
                else
                {
                    err.Add("DataIntagrator.Error", "Please select Claim Types for General Claims LOB.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                ISO_Parms.Add("ClaimSubTypeGC", sClaimType);
            }
            else
            {
                ISO_Parms.Add("LOBGC", "0");
                ISO_Parms.Add("ClaimSubTypeGC", sClaimType);
            }

            sClaimType = "";

            if (CheckBox21.Checked == true)
            {
                ISO_Parms.Add("LOBVA", "1");
                for (iIndex = 0; iIndex < ListBox2.Items.Count; iIndex++)
                {
                    if (ListBox2.Items[0].Selected == true)
                    {
                        if (iIndex > 0)
                        {
                            sClaimType = sClaimType + ListBox2.Items[iIndex].Value.ToString() + ",";
                        }
                    }
                    else if (ListBox2.Items[iIndex].Selected == true)
                    {
                        sClaimType = sClaimType + ListBox2.Items[iIndex].Value.ToString() + ",";
                    }
                }
                if (sClaimType != "")
                {
                    sClaimType = sClaimType.Substring(0, sClaimType.Length - 1);
                }
                else
                {
                    err.Add("DataIntagrator.Error", "Please select Claim Types for Vehicle Accident Claims LOB.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                ISO_Parms.Add("ClaimSubTypeVA", sClaimType);
            }
            else
            {
                ISO_Parms.Add("LOBVA", "0");
                ISO_Parms.Add("ClaimSubTypeVA", sClaimType);
            }

            sClaimType = "";

            if (CheckBox22.Checked == true)
            {
                ISO_Parms.Add("LOBWC", "1");
                for (iIndex = 0; iIndex < ListBox3.Items.Count; iIndex++)
                {
                    if (ListBox3.Items[0].Selected == true)
                    {
                        if (iIndex > 0)
                        {
                            sClaimType = sClaimType + ListBox3.Items[iIndex].Value.ToString() + ",";
                        }
                    }
                    else if (ListBox3.Items[iIndex].Selected == true)
                    {
                        sClaimType = sClaimType + ListBox3.Items[iIndex].Value.ToString() + ",";
                    }
                }
                if (sClaimType != "")
                {
                    sClaimType = sClaimType.Substring(0, sClaimType.Length - 1);
                }
                else
                {
                    err.Add("DataIntagrator.Error", "Please select Claim Types for Workers' Compensation Claims LOB.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                ISO_Parms.Add("ClaimSubTypeWC", sClaimType);
            }
            else
            {
                ISO_Parms.Add("LOBWC", "0");
                ISO_Parms.Add("ClaimSubTypeWC", sClaimType);
            }

            //subhendu 11/01/2010 MITS 22785: Creating new nodes LOBPC and ClaimSubTypePC for property claims which is passed to xml_string.
            sClaimType = "";

            if (Chkbox_propClaim.Checked == true)
            {
                ISO_Parms.Add("LOBPC", "1");
                for (iIndex = 0; iIndex < ListBox4.Items.Count; iIndex++)
                {
                    if (ListBox4.Items[0].Selected == true)
                    {
                        if (iIndex > 0)
                        {
                            sClaimType = sClaimType + ListBox4.Items[iIndex].Value.ToString() + ",";
                        }
                    }
                    else if (ListBox4.Items[iIndex].Selected == true)
                    {
                        sClaimType = sClaimType + ListBox4.Items[iIndex].Value.ToString() + ",";
                    }
                }
                if (sClaimType != "")
                {
                    sClaimType = sClaimType.Substring(0, sClaimType.Length - 1);
                }
                else
                {
                    err.Add("DataIntagrator.Error", "Please select Claim Types for Property Claims LOB.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                ISO_Parms.Add("ClaimSubTypePC", sClaimType);
            }
            else
            {
                ISO_Parms.Add("LOBPC", "0");
                ISO_Parms.Add("ClaimSubTypePC", sClaimType);
            }


            ISO_Parms.Add("LOBDI", "0");
            ISO_Parms.Add("ClaimSubTypeDI", "");

            //TODO
            string sState = "";

            if (chkExcludeStates.Checked == true)
            {
                for (iIndex = 0; iIndex < lstStates.Items.Count; iIndex++)
                {
                    if (lstStates.Items[iIndex].Selected == true)
                    {
                        sState = sState + lstStates.Items[iIndex].Value.ToString() + ",";
                    }
                }
                if (sState != "")
                {
                    sState = sState.Substring(0, sState.Length - 1);
                }
                else
                {
                    //ipuri 05/22/2013 Mits:29100
                    err.Add("DataIntagrator.Error", "Please select at least one state to be excluded.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
            }

            ISO_Parms.Add("ExcludeStates", sState);
            sState = "";
            if (chkIncludeStates.Checked == true)// subhendu 9/28/2010 - MITS 20512  and 22524 Added to save the Include states elements to xml string.
            {
                for (iIndex = 0; iIndex < lstStates.Items.Count; iIndex++)
                {
                    if (lstStates.Items[iIndex].Selected == true)
                    {
                        sState = sState + lstStates.Items[iIndex].Value.ToString() + ",";
                    }
                }
                if (sState != "")
                {
                    sState = sState.Substring(0, sState.Length - 1);
                }
                else
                {
                    //ipuri 05/22/2013 Mits:29101
                    err.Add("DataIntagrator.Error", "Please select at least one state to be included.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
            }

            ISO_Parms.Add("IncludeStates", sState);
            if (ChkEligibleISOSub.Checked == true)	// subhendu 07/01/2010 MITS-(21241,21356) 
                ISO_Parms.Add("EligibleISOSubmission", "1");
            else
                ISO_Parms.Add("EligibleISOSubmission", "0");

            //ipuri 12/11/2013 MITS:34500 Start

            if (ChkCMSReporting.Checked == true)
                ISO_Parms.Add("CMSReporting", "1");
            else
                ISO_Parms.Add("CMSReporting", "0");

            if (ChkRequestRecalInfoIndicator.Checked == true) 
                ISO_Parms.Add("RequestRecalInfoIndicator", "Y");
            else
                ISO_Parms.Add("RequestRecalInfoIndicator", "N");
            if (ddSearchParty.SelectedIndex > 0)
            {
                ISO_Parms.Add("SearchParty", ddSearchParty.SelectedItem.Value);
            }
            else
            {
                ISO_Parms.Add("SearchParty", "");
            }
            //ipuri 12/11/2013 MITS:34500 End

            //Added by agupta298 PMC GAP08 - RMA#5500 - Start
            if (ChkIncludeNMVTIS.Checked == true)
                ISO_Parms.Add("IncludeNMVTIS", "1");
            else
                ISO_Parms.Add("IncludeNMVTIS", "0");

            ISO_Parms.Add("NMVTISReportingID", txtNMVTISReportingID.Text);
            //Added by agupta298 PMC GAP08 - RMA#5500 - End
            string UserName = AppHelper.GetUserLoginName();
            ISO_Parms.Add("Username", UserName); //subhendu -9/15/2010 MITS 22012,21811 - This parameter is added to pass the User name(used for login) into the xml string. 

            if (bErr == true)       //vgupta20 08/05/2009 - club all errors
            {
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                bErr = false;
                return;
            }

            objDIModel.OptionSetName = txtOptionName.Text;
            objDIModel.ModuleName = m_sModuleName;
            objDIModel.OptionSetID = m_iOptionSetID;
            objDIModel.TaskManagerXml = sTaskManagerXml;

            objDIModel.Parms = ISO_Parms;
            //objDIModel = objDIService.SaveSettings(objDIModel);
            objDIModel.ClientId = AppHelper.ClientId;

            objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
            iOptionSetID = objDIModel.OptionSetID;
            
            if (objDIModel.OptionSetID == -1)
            {//makse sure the optionset name is not all ready used
                err.Add("DataIntagrator.Error", "Optionset Name is already used.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }

            if (bErr == true)       //vgupta20 08/05/2009 - club all errors and return page if any errors found
            {
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }

            if (objDIModel.OptionSetID > 0)
            {
                Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx");
            }
        }


        //ipuri Mits:30917 start
        #region SaveImport
        //Function will save import setting into data integrator table.
        private void SaveImportSetting()
        {
            string sTaskManagerXml = string.Empty;
            
            m_iOptionSetID = Conversion.ConvertObjToInt(hdOptionsetId.Value);

            BusinessAdaptorErrors err = new BusinessAdaptorErrors(); 
            bool bErr = false;
            //bError = false;

            sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
            if ((m_iOptionSetID == 0) && (txtOptName.Text == ""))
            {
                err.Add("DataIntagrator.Error", "Optionset Name is blank.", BusinessAdaptorErrorType.Message);
                bErr = true;

            }

            if (chkIndex.Checked == false && chkError.Checked == false)
            {
                err.Add("DataIntagrator.Error", "Please select ISO Claim Match Report or ISO Submission Rejection Report.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }

            if (chkIndex.Checked == true && (txtIndexFile.Text != ""))
            {  
                string sFileName = txtIndexFile.Text.Substring(txtIndexFile.Text.LastIndexOf("\\") + 1);
                string ext = sFileName.Substring(sFileName.LastIndexOf(".") + 1);
                if (ext.ToUpper() != "TXT")
                {
                    err.Add("DataIntagrator.Error", "ISO Claim Match Report file format not supported. Only text files are permitted.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
            }
            //sagarwal54 08/23/2013 MITS 33168 Start
            if (chkIndex.Checked == true)
            {
                if (ddlClaimMatchDocType.SelectedIndex == 0)
                {
                    err.Add("DataIntagrator.Error", "Claim Match Document Type required when ISO Claim Match Report option is selected", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                if ( string.IsNullOrEmpty(hdntxtClaimMatchDocText.Value) || string.IsNullOrEmpty(txtClaimMatchDocText.Text) )
                {
                    err.Add("DataIntagrator.Error", "Claim Match Document Title required when ISO Claim Match Report option is selected.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                else
                {
                    txtClaimMatchDocText.Text = hdntxtClaimMatchDocText.Value;
                }
            }
            else
            {
                ddlClaimMatchDocType.SelectedIndex = 0;
                txtClaimMatchDocText.Text = "";
            }
                        
            if (chkClaimMatch.Checked == true)
            {
                if (txtClaimMatchDiaryText.Text == "" || txtClaimMatchDiaryText.Text == null)
                {
                    err.Add("DataIntagrator.Error", "Claim Match Diary Text required when the Create Diary for Claim Match Reports option is selected.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                if ( ddlClaimMatchDiaryType.SelectedIndex == 0)
                {
                    err.Add("DataIntagrator.Error", "Claim Match Diary Type required when the Create Diary for Claim Match Reports option is selected.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
            }

            if  ( chkboxSubmissor.Checked)
            {
                if (txtSubRejDiaryText.Text == "" || txtSubRejDiaryText.Text == null)
                {
                    err.Add("DataIntagrator.Error", "Submission Rejection Diary Text required when the Create Diary for Submission Rejection Reports option is selected.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                if ( ddlSubRejDiaryType.SelectedIndex == 0)
                {
                    err.Add("DataIntagrator.Error", "Submission Rejection Diary Type required when the Create Diary for Submission Rejection Reports option is selected.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
            }

            if (chkClaimMatchForEnhanced.Checked)
            {
                if (txtClaimMatchEnhancedNotes.Text == "" || txtClaimMatchEnhancedNotes.Text == null)
                {
                    err.Add("DataIntagrator.Error", "Claim Match Enhanced Note Text required when the Create Enhanced Note for Claim Match Reports option is selected.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                if (ddlClaimMatchEnhancedNotes.SelectedIndex == 0)
                {
                    err.Add("DataIntagrator.Error", "Claim Match Enhanced Note Type required when the Create Enhanced Note for Claim Match Reports option is selected.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
            }

            if (chkSubmissorForEnhanced.Checked)
            {
                if (txtSubRejEnhancedNotes.Text == "" || txtSubRejEnhancedNotes.Text == null)
                {
                    err.Add("DataIntagrator.Error", "Submission Rejection Enhanced Note Text required when the Create Diary for Submission Rejection Reports option is selected.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                if (ddlSubRejEnhancedNotes.SelectedIndex == 0)
                {
                    err.Add("DataIntagrator.Error", "Submission Rejection Enhanced Note Type required when the Create Diary for Submission Rejection Reports option is selected.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
            }
            //sagarwal54 08/23/2013 MITS 33168 End
            if (chkError.Checked == true && (txtErrorFile.Text != ""))
            {
                string sFileName = txtErrorFile.Text.Substring(txtErrorFile.Text.LastIndexOf("\\") + 1);
                string ext = sFileName.Substring(sFileName.LastIndexOf(".") + 1);
                if (ext.ToUpper() != "TXT")
                {
                    err.Add("DataIntagrator.Error", "ISO Submission Rejection Report file format not supported. Only text files are permitted.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
            }
            if (chkIndex.Checked == true && string.IsNullOrEmpty(txtIndexFile.Text))
            {
                err.Add("DataIntagrator.Error", "ISO Claim Match Report is missing.", BusinessAdaptorErrorType.Message);
                bErr = true;

            }
            
                        
            if (chkError.Checked == true)
            {
                if (string.IsNullOrEmpty(txtErrorFile.Text))
                {
                    err.Add("DataIntagrator.Error", "ISO Submission Rejection Report is missing.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                if (chkboxSubmissor.Checked == false)
                {
                    err.Add("DataIntagrator.Error", "Diary for Submisson Rejection not selected", BusinessAdaptorErrorType.Message);
                    bErr = true;
                    // bError = true;
                }
            }

            
            if (chkClaimMatch.Checked == true && rbtClaimUser.Checked == true)
            {
                if (string.IsNullOrEmpty(ClaimMatchUserId.Text))
                {
                    err.Add("DataIntagrator.Error", "At least one user must be selected when the Claim Match Other User(s) option is selected.", BusinessAdaptorErrorType.Message);
                    lstClaimOtherUser.Items.Clear();
                    hdCMOtherUserID.Value = "";
                    ClaimMatchUserId.Text = "";
                    hdCMOtherUserName.Value = "";
                    ClaimMatchUserName.Text = "";
                    bErr = true;
                }
            }
            //if (chkError.Checked == true && rbtSubmissorUser.Checked == false)
            //{
            //    err.Add("DataIntagrator.Error", "Field 'Select D", BusinessAdaptorErrorType.Message);
            //    bErr = true;
            //}
            if (chkboxSubmissor.Checked == true && rbtSubmissorUser.Checked == true)
            {
                if (string.IsNullOrEmpty(SubmissionUserId.Text))
                {
                    err.Add("DataIntagrator.Error", "At least one user must be selected when the Submission Rejection Other User(s) option is selected.", BusinessAdaptorErrorType.Message);
                    lstSubmissorOtherUser.Items.Clear();
                    hdSROtherUserID.Value = "";
                    SubmissionUserId.Text = "";
                    hdSROtherUserName.Value = "";
                    SubmissionUserName.Text = "";
                    bErr = true;
                }
            }

            //error desc
            //actual

            //DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
            Riskmaster.Models.DataIntegratorModel objDIModel = new Riskmaster.Models.DataIntegratorModel();
            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }

            bool isError = false;
            int iOptionSetID;


            Dictionary<string, string> ISO_Parms = new Dictionary<string, string>();

            //
            // Populate ISO_Parms Dictionary
            //
            if (bErr == false)
            {
                ISO_Parms.Add("ISO_Type", "Import");


                sIndexFileName = (m_iOptionSetID == 0 || !(chkIndex.Checked)) ? string.Empty : sIndexFileName;
                sIndexFileDir = (m_iOptionSetID == 0 || !(chkIndex.Checked)) ? string.Empty : sIndexFileDir;
                sErrorFileName = (m_iOptionSetID == 0 || !(chkError.Checked)) ? string.Empty : sErrorFileName;
                sErrorFileDir = (m_iOptionSetID == 0 || !(chkError.Checked)) ? string.Empty : sErrorFileDir;

                //if (chkIndex.Checked== true && FUIndexFile.HasFile)
                if (chkIndex.Checked == true && txtIndexFile.Text != "")
                {
                    //sIndexFileName = FUIndexFile.PostedFile.FileName.Substring(FUIndexFile.PostedFile.FileName.LastIndexOf("\\") + 1);
                    sIndexFileName = txtIndexFile.Text.Substring(txtIndexFile.Text.LastIndexOf("\\") + 1);
                    if (!string.IsNullOrEmpty(txtIndexFile.Text))
                    {
                        int iIndexFile = txtIndexFile.Text.LastIndexOf("\\");
                        sIndexFileDir = txtIndexFile.Text.Remove(iIndexFile + 1);
                    }
                }
                // if (chkError.Checked==true && FUErrorFile.HasFile)
                if (chkError.Checked == true && txtErrorFile.Text != "")
                {
                    //sErrorFileName = FUErrorFile.PostedFile.FileName.Substring(FUErrorFile.PostedFile.FileName.LastIndexOf("\\") + 1);
                    sErrorFileName = txtErrorFile.Text.Substring(txtErrorFile.Text.LastIndexOf("\\") + 1);
                    if (!string.IsNullOrEmpty(txtErrorFile.Text))
                    {
                        int iErrorFile = txtErrorFile.Text.LastIndexOf("\\");
                        sErrorFileDir = txtErrorFile.Text.Remove(iErrorFile + 1);
                    }
                }

                ISO_Parms.Add("IndexSourceFile", sIndexFileDir);
                ISO_Parms.Add("ISO_Import_Index_File_Name", sIndexFileName);
                ISO_Parms.Add("ErrorSourceFile", sErrorFileDir);
                ISO_Parms.Add("ISO_Import_Error_File_Name", sErrorFileName);
                if (chkClaimMatch.Checked == true)
                    ISO_Parms.Add("Claim_Match", "1");
                else
                    ISO_Parms.Add("Claim_Match", "0");

                if (chkboxSubmissor.Checked == true)
                    ISO_Parms.Add("Submissor", "1");
                else
                    ISO_Parms.Add("Submissor", "0");

                if (rbtClaimAdjuster.Checked == true)
                    ISO_Parms.Add("Claim_Match_Adjuster", "1");
                else
                    ISO_Parms.Add("Claim_Match_Adjuster", "0");

                if (rbtSubmissorAdjuster.Checked == true)
                    ISO_Parms.Add("Submissor_Adjuster", "1");
                else
                    ISO_Parms.Add("Submissor_Adjuster", "0");

                if (rbtClaimUser.Checked == true)
                    ISO_Parms.Add("Claim_Match_OtherUser", "1");
                else
                    ISO_Parms.Add("Claim_Match_OtherUser", "0");

                if (rbtSubmissorUser.Checked == true)
                    ISO_Parms.Add("Submissor_OtherUser", "1");
                else
                    ISO_Parms.Add("Submissor_OtherUser", "0");

                int iIndex = 0;
                if (chkClaimMatch.Checked == true && rbtClaimUser.Checked == true)
                {
                    if (!string.IsNullOrEmpty(ClaimMatchUserId.Text))
                    {
                        sClUser = ClaimMatchUserId.Text.Replace(" ", ",");
                        sUserName = ClaimMatchUserName.Text.Replace(" ", ",");
                        ISO_Parms.Add("Claim_Match_OtherUserList", sClUser);
                    }
                }
                else
                {
                    ISO_Parms.Add("Claim_Match_OtherUserList", "");
                }

                if (chkboxSubmissor.Checked == true && rbtSubmissorUser.Checked == true)
                {
                    if (!string.IsNullOrEmpty(SubmissionUserId.Text))
                    {
                        sSbUser = SubmissionUserId.Text.Replace(" ", ",");
                        ISO_Parms.Add("Submissor_OtherUserList", sSbUser);
                    }
                }
                else
                {
                    ISO_Parms.Add("Submissor_OtherUserList", "");
                }
                //sagarwal54 08/23/2013 MITS 33168 Start
              
                if (chkClaimMatchForEnhanced.Checked)
                {
                    ISO_Parms.Add("CreateEnhancedNoteClaimMatch", "1");
                    if (ddlClaimMatchEnhancedNotes.SelectedIndex > 0)
                    {
                        ISO_Parms.Add("EnhNoteTypeClaimMatch", ddlClaimMatchEnhancedNotes.SelectedValue);

                    }
                    else
                    {
                        ISO_Parms.Add("EnhNoteTypeClaimMatch", "");
                    }
                    if (!string.IsNullOrEmpty(txtClaimMatchEnhancedNotes.Text))
                    {
                        ISO_Parms.Add("EnhNoteTextClaimMatch", txtClaimMatchEnhancedNotes.Text);
                    }
                    else
                    {
                        ISO_Parms.Add("EnhNoteTextClaimMatch", "");
                    }
                }
                else
                {
                    ISO_Parms.Add("CreateEnhancedNoteClaimMatch", "0");
                    ISO_Parms.Add("EnhNoteTypeClaimMatch", "");
                    ISO_Parms.Add("EnhNoteTextClaimMatch", "");
                }

                if (chkSubmissorForEnhanced.Checked)
                {
                    ISO_Parms.Add("CreateEnhancedNoteRejection", "1");
                    if (ddlSubRejEnhancedNotes.SelectedIndex > 0)
                    {
                        ISO_Parms.Add("EnhNoteTypeRejection", ddlSubRejEnhancedNotes.SelectedValue);

                    }
                    else
                    {
                        ISO_Parms.Add("EnhNoteTypeRejection", "");
                    }
                    if (!string.IsNullOrEmpty(txtSubRejEnhancedNotes.Text))
                    {
                        ISO_Parms.Add("EnhNoteTextRejection", txtSubRejEnhancedNotes.Text);
                    }
                    else
                    {
                        ISO_Parms.Add("EnhNoteTextRejection", "");
                    }
                }
                else
                {
                    ISO_Parms.Add("CreateEnhancedNoteRejection", "0");
                    ISO_Parms.Add("EnhNoteTypeRejection", "");
                    ISO_Parms.Add("EnhNoteTextRejection", "");
                }

                if (ddlClaimMatchDiaryType.SelectedIndex > 0)
                {
                    ISO_Parms.Add("DiaryTypeClaimMatch", ddlClaimMatchDiaryType.SelectedValue);
                }
                else
                {
                    ISO_Parms.Add("DiaryTypeClaimMatch", "");
                }

                if (!string.IsNullOrEmpty(txtClaimMatchDiaryText.Text))
                {
                    ISO_Parms.Add("DiaryTextClaimMatch", txtClaimMatchDiaryText.Text);
                }
                else
                {
                    ISO_Parms.Add("DiaryTextClaimMatch", "");
                }

                if (ddlSubRejDiaryType.SelectedIndex > 0)
                {
                    ISO_Parms.Add("DiaryTypeRejection", ddlSubRejDiaryType.SelectedValue);
                }
                else
                {
                    ISO_Parms.Add("DiaryTypeRejection", "");
                }

                if (!string.IsNullOrEmpty(txtSubRejDiaryText.Text))
                {
                    ISO_Parms.Add("DiaryTextRejection", txtSubRejDiaryText.Text);
                }
                else
                {
                    ISO_Parms.Add("DiaryTextRejection", "");
                }


                if (ddlClaimMatchDocType.SelectedIndex > 0)
                {
                    ISO_Parms.Add("DocTypeClaimMatch", ddlClaimMatchDocType.SelectedValue);
                }
                else
                {
                    ISO_Parms.Add("DocTypeClaimMatch", "");
                }
                if (!string.IsNullOrEmpty(txtClaimMatchDocText.Text))
                {
                    ISO_Parms.Add("DocTitleClaimMatch", txtClaimMatchDocText.Text);
                }

                else
                {
                    ISO_Parms.Add("DocTitleClaimMatch", "");
                }

                //sagarwal54 08/23/2013 MITS 33168 End
                //Export Nodes start

                ISO_Parms.Add("CompanyCIBNumber", "");
                ISO_Parms.Add("CompanyName", "");
                ISO_Parms.Add("UseDefaultInsuredName", "0");
                ISO_Parms.Add("DefaultInsuredName", "");
                ISO_Parms.Add("DefaultInsuredAddr1", "");
                ISO_Parms.Add("DefaultInsuredAddr2", "");
                ISO_Parms.Add("DefaultInsuredCity", "");
                ISO_Parms.Add("DefaultInsuredState", "");
                ISO_Parms.Add("InitialAction", "0");
                ISO_Parms.Add("ReplaceAction", "0");
                ISO_Parms.Add("SubClaimnoForPolicy", "0");
                //sagarwal54 08/23/2013 MITS 33168 Start
                ISO_Parms.Add("LossInjuryGC", "0");
                ISO_Parms.Add("LossInjuryPC", "0");
                ISO_Parms.Add("LossInjuryVA", "0");
                ISO_Parms.Add("LossInjuryWC", "0");
                //sagarwal54 08/23/2013 MITS 33168 End
                ISO_Parms.Add("UseEventDescDI", "0");
                ISO_Parms.Add("ProcessSingleClaim", "0");
                ISO_Parms.Add("SingleClaimNo", "");
                ISO_Parms.Add("UseDateOfClaimRange", "0");
                ISO_Parms.Add("ClaimDateFrom", "");
                ISO_Parms.Add("ClaimDateTo", "");
                ISO_Parms.Add("OpenCloseClaims", "0");
                ISO_Parms.Add("LOBGC", "0");
                ISO_Parms.Add("ClaimSubTypeGC", "0");
                ISO_Parms.Add("LOBVA", "0");
                ISO_Parms.Add("ClaimSubTypeVA", "");
                ISO_Parms.Add("LOBWC", "0");
                ISO_Parms.Add("ClaimSubTypeWC", "");
                ISO_Parms.Add("LOBPC", "0");
                ISO_Parms.Add("ClaimSubTypePC", "");
                ISO_Parms.Add("LOBDI", "0");
                ISO_Parms.Add("ClaimSubTypeDI", "");
                ISO_Parms.Add("ExcludeStates", "");
                ISO_Parms.Add("IncludeStates", "");
                ISO_Parms.Add("EligibleISOSubmission", "");
                //ipuri 12/11/2013 Mits: 34500 Start
                ISO_Parms.Add("CMSReporting", ""); 
                ISO_Parms.Add("RequestRecalInfoIndicator", "");     
                ISO_Parms.Add("SearchParty", "0");
                //ipuri 12/11/2013 Mits: 34500 End
				string UserName = AppHelper.GetUserLoginName();
                ISO_Parms.Add("Username", UserName);

                //Export Nodes end

                objDIModel.OptionSetName = txtOptName.Text;
                objDIModel.ModuleName = m_sModuleName;
                objDIModel.OptionSetID = m_iOptionSetID;
                objDIModel.TaskManagerXml = sTaskManagerXml;

                objDIModel.Parms = ISO_Parms;
                objDIModel.ImportFlage = true;
                //objDIModel = objDIService.SaveSettings(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
            
                iOptionSetID = objDIModel.OptionSetID;
            }

            if (objDIModel.OptionSetID == -1)
            {
                //makse sure the optionset name is not all ready used
                err.Add("DataIntagrator.Error", "Optionset Name is already used.", BusinessAdaptorErrorType.Message);
                bErr = true;
            }

            if (bErr == true)
            {
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                txtIndexFile.Text = "";
                txtErrorFile.Text = "";
                RetrieveListSettings();
                return;
            }

            try
            {

                if (objDIModel.OptionSetID > 0)
                {
                    if (chkIndex.Checked == true)
                    {

                        //kkaur25 SOA changes
                        Riskmaster.Models.DAImportFile objDAImportFile = new Riskmaster.Models.DAImportFile() ;  
                        Riskmaster.UI.DataIntegratorService.DAImportFile objDAImportFileS = new Riskmaster.UI.DataIntegratorService.DAImportFile();
                        //kkaur25 SOA changes end


                        objDAImportFile.Token = AppHelper.GetSessionId();
                        Stream data = FUIndexFile.PostedFile.InputStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                        objDAImportFile.fileName = sIndexFileName + "." + objDIModel.OptionSetID;
                        objDAImportFile.filePath = objDIModel.FilePath;

                        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
                        // We need ModuleName, DocumentType and optionsetId while retrieving the Import Files from DB
                        // So saving these attributes while uploading the Import Files to DB
                        objDAImportFile.ModuleName = "ISO";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objDIModel.OptionSetID;
                        objDAImportFile.ClientId = AppHelper.ClientId;
                       
                       //kkaur25 SOA changes
                        DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();                                     
                        objDAImportFileS.FileContents = objDAImportFile.FileContents;
                        objDAImportFileS.fileName = objDAImportFile.fileName;
                        objDAImportFileS.filePath = objDIModel.FilePath;
                        objDAImportFileS.Token = AppHelper.Token;
                        objDAImportFileS.ClientId = AppHelper.ClientId;
                        objDAImportFileS.ModuleName = "ISO";
                        objDAImportFileS.DocumentType = "Import";
                        objDAImportFileS.OptionsetId = objDIModel.OptionSetID;

                        objDIService.UploadDAImportFile(objDAImportFileS);

                     // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile); 
                        
                       //kkaur25 SOA changes end

                        //Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);                    
                    }

                    if (chkError.Checked == true)
                    {
                      
                        //kkaur25 SOA changes
                        Riskmaster.Models.DAImportFile objDAImportFile = new Riskmaster.Models.DAImportFile();
                        Riskmaster.UI.DataIntegratorService.DAImportFile objDAImportFileS = new Riskmaster.UI.DataIntegratorService.DAImportFile();
                        //kkaur25 SOA changes

                        
                        objDAImportFile.Token = AppHelper.GetSessionId();
                        Stream data = FUErrorFile.PostedFile.InputStream;
                        //StreamReader srdata = new StreamReader(txtErrorFile.Text);
                        //Stream data = srdata.BaseStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                        objDAImportFile.fileName = sErrorFileName + "." + objDIModel.OptionSetID;
                        objDAImportFile.filePath = objDIModel.FilePath;

                        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
                        // We need ModuleName, DocumentType and optionsetId while retrieving the Import Files from DB
                        // So saving these attributes while uploading the Import Files to DB
                        objDAImportFile.ModuleName = "ISO";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objDIModel.OptionSetID;

                        objDAImportFile.ClientId = AppHelper.ClientId;

                        //kkaur25 SOA changes
                        DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();    
                        objDAImportFileS.FileContents = objDAImportFile.FileContents;
                        objDAImportFileS.fileName = objDAImportFile.fileName;
                        objDAImportFileS.filePath = objDIModel.FilePath;
                        objDAImportFileS.Token = AppHelper.Token;
                        objDAImportFileS.ClientId = AppHelper.ClientId;
                        objDAImportFileS.ModuleName = "ISO";
                        objDAImportFileS.DocumentType = "Import";
                        objDAImportFileS.OptionsetId = objDIModel.OptionSetID;
                        objDIService.UploadDAImportFile(objDAImportFileS);

               // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile); 

                        //kkaur25 SOA changes end

                      //  AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile); 
                      
                        
                        //Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);

                    }

                    // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
                    // In Case of cloud the user will not have access to copy and paste the files on server.
                    // So providing a mechanism so that user can browse the files on his/her machine and upload to server.
                    // Once the files are uploaded to server, we can save them in DB
                    UploadAttachments("ISO", objDIModel.OptionSetID, "Attachment");
                   Riskmaster.Models.DataIntegratorModel objDIModelS = new Riskmaster.Models.DataIntegratorModel();
                    UploadAttachments("ISO", objDIModelS.OptionSetID, "Attachment");

             Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
                }
            }
            catch (Exception Ex)
            {
                err.Add(Ex, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                try
                {
                    if (m_iOptionSetID <= 0 && objDIModel.OptionSetID > 0)
                    {
                        //objDIService.SaveSettingsCleanup(objDIModel);
                        objDIModel.ClientId = AppHelper.ClientId;
                        AppHelper.GetResponse("RMService/DAIntegration/savesettingscleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                    }
                }
                catch (Exception eCleanup)
                {
                }
            }
        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // If this function is required by other modules of DA then it should be moved to some common file.
        /// <summary>
        /// In Case of cloud the user will not have access to copy and paste the files on server.
        /// So providing a mechanism so that user can browse the files on his/her machine and upload to server.
        /// Once the files are uploaded to server, we can save them in DB
        /// </summary>
        /// <param name="p_sModuleName">Name of the Module. Can be ISO, MMSEA, etc</param>
        /// <param name="p_iOptionsetId">The files are associated with which OptionsetId</param>
        /// <param name="p_sDocType">Document Type Import or Attcahment</param>
        private void UploadAttachments(string p_sModuleName, int p_iOptionsetId, string p_sDocType)
        {
            if (AppHelper.ClientId > 0)
            {
                int iUploadedFiles = 0;
                Riskmaster.Models.DAImportFile oDAImportFile = null;
                try
                {
                    lbSuccess.Visible = false;
                    if (UploadDocumentAttachments.Items.Count > 0)
                    {
                        oDAImportFile = new Riskmaster.Models.DAImportFile();
                        oDAImportFile.Token = AppHelper.GetSessionId();
                        try
                        {
                            foreach (AttachmentItem item in UploadDocumentAttachments.Items)
                            {
                                Stream data = item.OpenStream();

                                oDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                                oDAImportFile.fileName = item.FileName + "." + p_iOptionsetId;
                                oDAImportFile.ClientId = AppHelper.ClientId;
                                oDAImportFile.ModuleName = p_sModuleName;
                                oDAImportFile.OptionsetId = p_iOptionsetId;
                                oDAImportFile.DocumentType = p_sDocType;
                                AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDAImportFile);
                                if (!string.IsNullOrEmpty(filename.Value))
                                {
                                    filename.Value += ",";
                                }
                                filename.Value += item.FileName;

                                iUploadedFiles++;
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                        finally
                        {
                            oDAImportFile = null;
                        }
                        if (iUploadedFiles == UploadDocumentAttachments.Items.Count)
                        {
                            lbSuccess.Visible = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
            else
                return;
        }
        #endregion
        //ipuri Mits:30917 end


        //******************************************************************************
        //Developer: Ankur Saxena
        //Date: 4/3/2009
        //Description: Function will enable/disable GC claim types.
        //******************************************************************************
        protected void CheckBox20_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox20.Checked == true)
            {
                ListBox1.Enabled = true;
            }
            else
            {
                ListBox1.Enabled = false;
                //ipuri 08/09/2013 Mits:33444 Start
                int iIndex = 0;
                for (iIndex = 0; iIndex < ListBox1.Items.Count; iIndex++)
                {
                    ListBox1.Items[iIndex].Selected = false;
                }
                ListBox1.Items[0].Selected = true;
                //ipuri 08/09/2013 Mits:33444 End
            }
        }

        //******************************************************************************
        //Developer: Ankur Saxena
        //Date: 4/3/2009
        //Description: Function will enable/disable VA claim types.
        //******************************************************************************
        protected void CheckBox21_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox21.Checked == true)
            {
                ListBox2.Enabled = true;
            }
            else
            {
                ListBox2.Enabled = false;
                //ipuri 08/09/2013 Mits:33444 Start
                int iIndex = 0;
                for (iIndex = 0; iIndex < ListBox2.Items.Count; iIndex++)
                {
                    ListBox2.Items[iIndex].Selected = false;
                }
                ListBox2.Items[0].Selected = true;
                //ipuri 08/09/2013 Mits:33444 End
            }
        }

        //******************************************************************************
        //Developer: Ankur Saxena
        //Date: 4/3/2009
        //Description: Function will enable/disable WC claim types.
        //******************************************************************************
        protected void CheckBox22_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox22.Checked == true)
            {
                ListBox3.Enabled = true;
                //ipuri 12/13/2013 Mits:34373 Start
                chkExcludeStates.Enabled = true;
                chkIncludeStates.Enabled = true;
				//ipuri 12/13/2013 Mits:34373 End
            }
            else
            {
                ListBox3.Enabled = false;
                //ipuri 08/09/2013 Mits:33444 Start
                int iIndex = 0;
                for (iIndex = 0; iIndex < ListBox3.Items.Count; iIndex++)
                {
                    ListBox3.Items[iIndex].Selected = false;
                }
                ListBox3.Items[0].Selected = true;
                //ipuri 08/09/2013 Mits:33444 End

                //ipuri 12/13/2013 Mits:34373 Start
                chkExcludeStates.Checked = false;
                chkIncludeStates.Checked = false;
                chkExcludeStates.Enabled = false;
                chkIncludeStates.Enabled = false;
				//ipuri 12/13/2013 Mits:34373 End
            }
        }

        //******************************************************************************
        //Developer: Ankur Saxena
        //Date: 4/3/2009
        //Description: Function will retrieve claimant mapping.
        //******************************************************************************
        protected void lstClaimantType_SelectedIndexChanged(object sender, EventArgs e)
        {   //kkaur25 SOA changes 
           // DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
            Riskmaster.Models.DataIntegratorModel objDIModel = new Riskmaster.Models.DataIntegratorModel();
           //kkaur25 SOA changes end
            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }

            objDIModel.iClaimantMapping = Convert.ToInt32(lstClaimantType.SelectedValue.ToString());
            //objDIModel = objDIService.GetClaimantTypeMapping(objDIModel);
            objDIModel.ClientId = AppHelper.ClientId;

            objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/getclaimanttypemapping", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);

            if (objDIModel.iClaimantMapping == 1)
            {
                lblClaimantType.Text = "Business";
            }
            else if (objDIModel.iClaimantMapping == 2)
            {
                lblClaimantType.Text = "Individual";
            }
            else
            {
                lblClaimantType.Text = "UnMapped";
            }

            lblClaimantStatus.Visible = false;
            BtnClaimantSave.Enabled = false;
            rbtClmtMapping.Items[0].Selected = false;
            rbtClmtMapping.Items[1].Selected = false;
            rbtClmtMapping.Items[2].Selected = false;
        }

        //******************************************************************************
        //Developer: Ankur Saxena
        //Date: 4/3/2009
        //Description: Function will enable/disable VA claim types.
        //******************************************************************************
        protected void rbtClmtMapping_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((rbtClmtMapping.Items[0].Selected == true) || (rbtClmtMapping.Items[1].Selected == true) || (rbtClmtMapping.Items[2].Selected == true))
            {
                BtnClaimantSave.Enabled = true;
            }
        }

        //******************************************************************************
        //Developer: Ankur Saxena
        //Date: 4/3/2009
        //Description: Function will enable/disable VA claim types.
        //******************************************************************************
        protected void BtnClaimantSave_Click(object sender, EventArgs e)
        {
          //  DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient(); //mkaur24
            Riskmaster.Models.DataIntegratorModel objDIModel = new Riskmaster.Models.DataIntegratorModel();

            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }

            if (rbtClmtMapping.Items[0].Selected == true)
            {
                objDIModel.iClaimantMapping = 0;
            }
            else if (rbtClmtMapping.Items[1].Selected == true)
            {
                objDIModel.iClaimantMapping = 1;
            }
            else
            {
                objDIModel.iClaimantMapping = 2;
            }

            objDIModel.iClmtTypeID = Convert.ToInt32(lstClaimantType.SelectedValue.ToString());
            //objDIModel = objDIService.SaveClaimantTypeMapping(objDIModel);
            objDIModel.ClientId = AppHelper.ClientId;

            objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/saveclaimanttypemapping", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);

            lblClaimantStatus.Visible = true;
            lblClaimantStatus.Text = "Claimant Type mapping saved successfully...";
            lblClaimantStatus.ForeColor = System.Drawing.Color.Blue;
        }

        //******************************************************************************
        //Developer: Ankur Saxena
        //Date: 4/3/2009
        //Description: Function will enable/disable VA claim types.
        //******************************************************************************
        protected void chkExcludeStates_CheckedChanged(object sender, EventArgs e)// Subhendu 9/28/2010 MITS-20512 and 22524The function is modified so as to accommodate the Include state conditions.
        {
            if (chkExcludeStates.Checked == true)
            {
                chkIncludeStates.Enabled = false;
                chkExcludeStates.Enabled = true;
				}
            else
            {
                if (chkIncludeStates.Checked == true)
                {
                    chkIncludeStates.Enabled = true;
                    chkExcludeStates.Enabled = false;
                }
                else
                {
                    chkIncludeStates.Enabled = true;
                    chkExcludeStates.Enabled = true;
                }
            }
            if (chkExcludeStates.Checked == true || chkIncludeStates.Checked == true)
            {
                lstStates.Enabled = true;
            }
            else
            {
                lstStates.Enabled = false;
                //ipuri 08/09/2013 Mits:33444 Start
                int iIndex = 0;
                for (iIndex = 0; iIndex < lstStates.Items.Count; iIndex++)
                {
                    lstStates.Items[iIndex].Selected = false;
                }
                //ipuri 08/09/2013 Mits:33444 End
            }
            
        }

        protected void CheckBox18_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox18.Checked == true)
            {
                txtFromDate.Disabled = false;
                txtToDate.Disabled = false;
                divFromDate.Visible = true;
                divToDate.Visible = true;
                //txtCompletedOnbtn1.Disabled = false;  //vkumar258 - RMA-6037
                //txtCompletedOnbtn2.Disabled = false;  //vkumar258 - RMA-6037
            }
            else
            {
                txtFromDate.Disabled = true;
                txtToDate.Disabled = true;
                divFromDate.Visible = false;
                divToDate.Visible = false;
                //txtCompletedOnbtn1.Disabled = true;  //vkumar258 - RMA-6037
                //txtCompletedOnbtn2.Disabled = true;  //vkumar258 - RMA-6037
                txtFromDate.Value = "";             //vgupta20 08/10/2009 -- date fields must get empty if checkbox is not selected
                txtToDate.Value = "";                
            }
        }

        protected void CheckBox14_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox14.Checked)
            {
                TextBox8.Enabled = true;
                //ipuri 08/08/2013 Mits:32648 Start
                CheckBox18.Enabled = false;       //Claim Criteria
                CheckBox18.Checked = false;
                txtFromDate.Disabled = true;
                // txtCompletedOnbtn1.Disabled = true; //vkumar258 - RMA-6037
                txtToDate.Disabled = true;
                //txtCompletedOnbtn2.Disabled = true; //vkumar258 - RMA-6037
                txtFromDate.Value = "";            
                txtToDate.Value = "";  
                CheckBox20.Enabled = false;         //General claims
                CheckBox20.Checked = false;
                //ipuri Mits 34373 start
                for (int iIndex = 0; iIndex < ListBox1.Items.Count; iIndex++)
                {
                    ListBox1.Items[iIndex].Selected = false;
                }
                //ipuri Mits 34373 end
                ListBox1.Enabled = false;
                CheckBox21.Enabled = false;         //Vehicle Accident
                CheckBox21.Checked = false;
                //ipuri Mits 34373 start
                for (int iIndex = 0; iIndex < ListBox2.Items.Count; iIndex++)
                {
                    ListBox2.Items[iIndex].Selected = false;
                }
                //ipuri Mits 34373 end
                ListBox2.Enabled = false;
                CheckBox22.Enabled = false;         //Workers compensation
                CheckBox22.Checked = false;
                //ipuri Mits 34373 start
                for (int iIndex = 0; iIndex < ListBox3.Items.Count; iIndex++)
                {
                    ListBox3.Items[iIndex].Selected = false;
                }
                //ipuri Mits 34373 end
                ListBox3.Enabled = false;
                Chkbox_propClaim.Enabled = false;       //Property claim
                Chkbox_propClaim.Checked = false;
                //ipuri Mits 34373 start
                for (int iIndex = 0; iIndex < ListBox4.Items.Count; iIndex++)
                {
                    ListBox4.Items[iIndex].Selected = false;
                }
                //ipuri Mits 34373 end
                ListBox4.Enabled = false;
                chkExcludeStates.Enabled = false;
                chkExcludeStates.Checked = false;
                chkIncludeStates.Enabled = false;
                chkIncludeStates.Checked = false;
                ClientScript.RegisterStartupScript(this.GetType(), "scr", "document.getElementById('lstStates').disabled=true;", true);		//ipuri Mits:34373
                //lstStates.Enabled = false;   
                ChkEligibleISOSub.Enabled = false;
                ChkEligibleISOSub.Checked = false;

            }
            //ipuri 08/08/2013 Mits:32648 End
            else
            {
                TextBox8.Enabled = false;
                TextBox8.Text = "";             //vgupta20 08/10/2009- empty text if checkbox is not selected
                //ipuri 08/08/2013 Mits:32648 Start
                CheckBox18.Enabled = true;       //Claim Criteria
                txtFromDate.Disabled = true;
                //txtCompletedOnbtn1.Disabled = true; //vkumar258 - RMA-6037
                txtToDate.Disabled = true;
                //txtCompletedOnbtn2.Disabled = true; //vkumar258 - RMA-6037
                CheckBox20.Enabled = true;         //General claims
                for (int iIndex = 0; iIndex < ListBox1.Items.Count; iIndex++)
                {
                    ListBox1.Items[iIndex].Selected = false;
                }
                ListBox1.Items[0].Selected = true;
                if (bIsCarrier.Value != "-1")
                {
                    CheckBox21.Enabled = true;         //Vehicle Accident
                    for (int iIndex = 0; iIndex < ListBox2.Items.Count; iIndex++)
                    {
                        ListBox2.Items[iIndex].Selected = false;
                    }
                    ListBox2.Items[0].Selected = true;
                    Chkbox_propClaim.Enabled = true;       //Property claim
                    for (int iIndex = 0; iIndex < ListBox4.Items.Count; iIndex++)
                    {
                        ListBox4.Items[iIndex].Selected = false;
                    }
                    ListBox4.Items[0].Selected = true;
                }
                
                CheckBox22.Enabled = true;         //Workers compensation
                for (int iIndex = 0; iIndex < ListBox3.Items.Count; iIndex++)
                {
                    ListBox3.Items[iIndex].Selected = false;
                }
                ListBox3.Items[0].Selected = true;
               
                //chkExcludeStates.Enabled = true;		//ipuri Mits 34373
                //chkIncludeStates.Enabled = true;		//ipuri Mits 34373
                for (int iIndex = 0; iIndex < lstStates.Items.Count; iIndex++)
                {
                    lstStates.Items[iIndex].Selected = false;
                }
                ChkEligibleISOSub.Enabled = true;
                //ipuri 08/08/2013 Mits:32648 End
            }
        }

        //subhendu 11/01/2010 MITS 22785 Function will enable/disable Property claims types.
        protected void PropClaims_CheckedChanged(object sender, EventArgs e)
        {
            if (Chkbox_propClaim.Checked == true)
            {
                ListBox4.Enabled = true;
            }
            else
            {
                ListBox4.Enabled = false;
                //ipuri 08/09/2013 Mits:33444 Start
                int iIndex = 0;
                for (iIndex = 0; iIndex < ListBox4.Items.Count; iIndex++)
                {
                    ListBox4.Items[iIndex].Selected = false;
                }
                ListBox4.Items[0].Selected = true;
                //ipuri 08/09/2013 Mits:33444 End
            }
        }

               //Function Create XML Template for Retreiving ISO Mapping
                //  mihtesham
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<ISOMappingList><listhead>");
            if (bIsCarrier.Value == "-1")
            {
                sXml = sXml.Append("<ddlPolicySystemNames>Policy System Names</ddlPolicySystemNames>"); //Developer – abharti5 |MITS 36676|
                sXml = sXml.Append("<ddlPolicyLOB>LOB</ddlPolicyLOB>");
                sXml = sXml.Append("<ddlISOClaimType>Claim Type</ddlISOClaimType>");
                sXml = sXml.Append("<ddlCoverage>Coverage Type</ddlCoverage>");
                sXml = sXml.Append("<ddlTypeOfLoss>Type Of Loss</ddlTypeOfLoss>");
                sXml = sXml.Append("<ddlDisabilityCode>Disability Code</ddlDisabilityCode>");
                sXml = sXml.Append("<ddlISOPolicyType>ISO Policy Type</ddlISOPolicyType>");
                sXml = sXml.Append("<ddlISOCoverageType>ISO Coverage Type</ddlISOCoverageType>");
                sXml = sXml.Append("<ddlISOLossType>ISO Loss Type</ddlISOLossType>");
                // Developer - Subhendu : MITS 30839 : Start
                sXml = sXml.Append("<ISORecordType>ISO Record Type</ISORecordType>");
                // Developer - Subhendu : MITS 30839 : End
                sXml = sXml.Append("<RowId>RowId</RowId>");
            }
            else
            {
                sXml = sXml.Append("<hdClaimLOB>LOB</hdClaimLOB>");
                sXml = sXml.Append("<ddlISOClaimType>Claim Type</ddlISOClaimType>");
                sXml = sXml.Append("<ddlISOPolicyType>ISO Policy Type</ddlISOPolicyType>");
                sXml = sXml.Append("<ddlISOCoverageType>ISO Coverage Type</ddlISOCoverageType>");
                sXml = sXml.Append("<ddlISOLossType>ISO Loss Type</ddlISOLossType>");
                // Developer - Subhendu : MITS 30839 : Start
                sXml = sXml.Append("<ISORecordType>ISO Record Type</ISORecordType>");
                // Developer - Subhendu : MITS 30839 : End
                sXml = sXml.Append("<RowId>RowId</RowId>");
            }

           // sXml = sXml.Append("</listhead></ISOMappingList>");
            //mkaran2 - MITS 34134 - Start
            sXml = sXml.Append("</listhead>");
            sXml = sXml.Append("<CurrentPage>1</CurrentPage>");
            sXml = sXml.Append("<hdnPageSize>25</hdnPageSize>");
            sXml = sXml.Append("<hdTotalPages>0</hdTotalPages>");
            sXml = sXml.Append("</ISOMappingList>");     
            //mkaran2 - MITS 34134 - End
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        //Function Create XML Template for Deleting a ISO Mapping
        private XElement GetMessageTemplate(string selectedRowId)
        {
            string[] arrtmp = selectedRowId.Split('|');
            //Developer – abharti5 |MITS 36676| start
            string strPSN = arrtmp[0];
            string strClaimType = arrtmp[1];
            string strLOB = arrtmp[2];
            string sCoverage = arrtmp[7];
            string strLoss = arrtmp[8];
            string strDisability = arrtmp[9];
            //Developer – abharti5 |MITS 36676| end

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ISOMappingList>");
            //mihtesham 

            //Developer – abharti5 |MITS 36676| start
            sXml = sXml.Append("<control name='Policy System Names'>");
            sXml = sXml.Append(strPSN);
            sXml = sXml.Append("</control>");
            //Developer – abharti5 |MITS 36676| end

            sXml = sXml.Append("<control name='Claim Type'>");
            sXml = sXml.Append(strClaimType);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='hdClaimLOB'>");
            sXml = sXml.Append(strLOB);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Coverage'>");
            sXml = sXml.Append(sCoverage);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Type Of Loss'>");
            sXml = sXml.Append(strLoss);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Disability Code'>");
            sXml = sXml.Append(strDisability);
            sXml = sXml.Append("</control>");
            
            sXml = sXml.Append("</ISOMappingList></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }

       //ipuri Mits:30917 Start

        //Function will save import setting into data integrator table.
        protected void Save_Click(object sender, EventArgs e)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();

            try
            {
                SaveImportSetting();
            }
            catch (Exception ex)
            {
                err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                RetrieveListSettings();
            }

        }

        protected void ClaimMatch_CheckedChanged(object sender, EventArgs e)
        {
            if (chkClaimMatch.Checked == true)
            {
                rbtClaimAdjuster.Enabled = true;
                rbtClaimUser.Enabled = true;
                rbtClaimAdjuster.Checked = true;
                //sagarwal54 08/23/2013 MITS 33168 Start
                ddlClaimMatchDiaryType.Enabled = true;
                lblClaimMatchDiaryType.Enabled = true;
                lblClaimMatchDiaryText.Enabled = true;
                txtClaimMatchDiaryText.Enabled = true;
                //sagarwal54 08/23/2013 MITS 33168 End
            }
            else
            {
                rbtClaimAdjuster.Enabled = false;
                rbtClaimUser.Enabled = false;
                rbtClaimAdjuster.Checked = false;
                rbtClaimUser.Checked = false;
                lstClaimOtherUser.Items.Clear();
                hdCMOtherUserID.Value = "";
                ClaimMatchUserId.Text = "";
                hdCMOtherUserName.Value = "";
                ClaimMatchUserName.Text = "";
                lstClaimOtherUser.Disabled = true;
                AutoFillUseridbtn1.Disabled = true;
                OtherUseridbtndel1.Disabled = true;
                //sagarwal54 08/23/2013 MITS 33168 Start
                lblClaimMatchDiaryText.Enabled = false;
                lblClaimMatchDiaryType.Enabled = false;
                ddlClaimMatchDiaryType.Enabled = false;
                txtClaimMatchDiaryText.Enabled = false;
                ddlClaimMatchDiaryType.SelectedIndex = 0;
                txtClaimMatchDiaryText.Text = "";
                //sagarwal54 08/23/2013 MITS 33168 End
            }
        }

        protected void Submissor_CheckedChanged(object sender, EventArgs e)
        {
            if (chkboxSubmissor.Checked == true)
            {
                rbtSubmissorAdjuster.Enabled = true;
                rbtSubmissorUser.Enabled = true;
                rbtSubmissorAdjuster.Checked = true;
                //sagarwal54 08/23/2013 MITS 33168 Start
                lblSubRejDiaryText.Enabled = true;
                lblSubRejDiaryType.Enabled = true;
                ddlSubRejDiaryType.Enabled = true;
                txtSubRejDiaryText.Enabled = true;
                //sagarwal54 08/23/2013 MITS 33168 End
            }
            else
            {
                rbtSubmissorAdjuster.Checked = false;
                rbtSubmissorUser.Checked = false;
                rbtSubmissorAdjuster.Enabled = false;
                rbtSubmissorUser.Enabled = false;
                lstSubmissorOtherUser.Items.Clear();
                hdSROtherUserID.Value = "";
                SubmissionUserId.Text = "";
                hdSROtherUserName.Value = "";
                SubmissionUserName.Text = "";
                lstSubmissorOtherUser.Disabled = true;
                AutoFillUseridbtn.Disabled = true;
                OtherUseridbtndel.Disabled = true;
                //sagarwal54 08/23/2013 MITS 33168 Start
                lblSubRejDiaryText.Enabled = false;
                lblSubRejDiaryType.Enabled = false;
                ddlSubRejDiaryType.Enabled = false;
                txtSubRejDiaryText.Enabled = false;
                ddlSubRejDiaryType.SelectedIndex = 0;
                txtSubRejDiaryText.Text = "";
                //sagarwal54 08/23/2013 MITS 33168 End
                //chkboxSubmissor.Checked = true;

                //if (hdSROtherUserID.Value.Trim() != "" && rbtSubmissorUser.Checked == true)
                //{
                //    rbtSubmissorAdjuster.Checked = false;
                //    rbtSubmissorUser.Checked = true;
                //    RetrieveListSettings();

                //}
                //else
                //{
                //    rbtSubmissorAdjuster.Checked = true;
                //    rbtSubmissorUser.Checked = false;
                //    lstSubmissorOtherUser.Items.Clear();
                //    hdSROtherUserID.Value = "";
                //    SubmissionUserId.Text = "";
                //    hdSROtherUserName.Value = "";
                //    SubmissionUserName.Text = "";
                //    AutoFillUseridbtn.Disabled = true;
                //    OtherUseridbtndel.Disabled = true;
                //}
            }
        }


        protected void chkError_CheckedChanged(object sender, EventArgs e)
        {
            if (chkError.Checked == true)
            {
                chkboxSubmissor.Enabled = true;
                rbtSubmissorAdjuster.Enabled = true;
                rbtSubmissorUser.Enabled = true;
                FUErrorFile.Enabled = true;
                txtErrorFile.Enabled = true;
                chkboxSubmissor.Checked = true;
                rbtSubmissorAdjuster.Checked = true;
                //sagarwal 08/23/2013 MITS 33168 Start
                chkSubmissorForEnhanced.Enabled = true;
                lblSubRejDiaryText.Enabled = true;
                lblSubRejDiaryType.Enabled = true;
                ddlSubRejDiaryType.Enabled = true;
                txtSubRejDiaryText.Enabled = true;
                //sagarwal 08/23/2013 MITS 33168 End
            }
            else
            {
                chkboxSubmissor.Enabled = false;
                rbtSubmissorAdjuster.Enabled = false;
                FUErrorFile.Enabled = false;
                txtErrorFile.Text = "";
                txtErrorFile.Enabled = false;
                rbtSubmissorUser.Enabled = false;
                rbtSubmissorAdjuster.Checked = false;
                chkboxSubmissor.Checked = false;
                rbtSubmissorUser.Checked = false;
                lstSubmissorOtherUser.Items.Clear();
                hdSROtherUserID.Value = "";
                SubmissionUserId.Text = "";
                hdSROtherUserName.Value = "";
                SubmissionUserName.Text = "";
                lstSubmissorOtherUser.Disabled = true;
                AutoFillUseridbtn.Disabled = true;
                OtherUseridbtndel.Disabled = true;              
                //sagarwal54 08/23/2013 MITS 33168 Start
                chkSubmissorForEnhanced.Enabled = false;
                lblSubRejDiaryText.Enabled = false;
                lblSubRejDiaryType.Enabled = false;
                ddlSubRejDiaryType.Enabled = false;
                txtSubRejDiaryText.Enabled = false;
                ddlSubRejEnhancedNotes.Enabled = false;
                txtSubRejEnhancedNotes.Enabled = false;
                ddlSubRejDiaryType.SelectedIndex = 0; //setting values to default when ISO Submission Rejection Report  option is unchecked
                txtSubRejDiaryText.Text = "";
                chkSubmissorForEnhanced.Checked = false;
                ddlSubRejEnhancedNotes.SelectedIndex = 0;
                txtSubRejEnhancedNotes.Text = "";
                //sagarwal54 08/23/2013 MITS 33168 End
            }
        }

        protected void chkIndex_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIndex.Checked == true)
            {
                chkClaimMatch.Enabled = true;
                FUIndexFile.Enabled = true;
                txtIndexFile.Enabled = true;
                //sagarwal54 08/23/2013 MITS 33168 Start
                //lblClaimMatchDocText.Enabled = true;
                //lblClaimMatchDocType.Enabled = true;
                //ddlClaimMatchDocType.Enabled = true;
                //txtClaimMatchDocText.Enabled = true;                
                chkClaimMatchForEnhanced.Enabled = true;
                //sagarwal54 08/23/2013 MITS 33168 End
            }
            else
            {
                chkClaimMatch.Enabled = false;
                rbtClaimAdjuster.Enabled = false;
                rbtClaimUser.Enabled = false;
                FUIndexFile.Enabled = false;
                txtIndexFile.Text = "";
                txtIndexFile.Enabled = false;
                chkClaimMatch.Checked = false;
                rbtClaimAdjuster.Checked = false;
                rbtClaimUser.Checked = false;
                lstClaimOtherUser.Items.Clear();
                hdCMOtherUserID.Value = "";
                ClaimMatchUserId.Text = "";
                hdCMOtherUserName.Value = "";
                ClaimMatchUserName.Text = "";
                lstClaimOtherUser.Disabled = true;
                AutoFillUseridbtn1.Disabled = true;
                OtherUseridbtndel1.Disabled = true;
                //sagarwal54 08/23/2013 MITS 33168 Start                
                chkClaimMatchForEnhanced.Enabled = false;
                lblClaimMatchDiaryText.Enabled = false;
                lblClaimMatchDiaryType.Enabled = false;
                ddlClaimMatchDiaryType.Enabled = false;
                txtClaimMatchDiaryText.Enabled = false;
                ddlClaimMatchEnhancedNotes.Enabled = false;
                txtClaimMatchEnhancedNotes.Enabled = false;
                ddlClaimMatchDocType.SelectedIndex = 0; //setting values to default when ISO Claim Match Report option is unchecked
                txtClaimMatchDocText.Text = "";
                hdntxtClaimMatchDocText.Value = "";
                ddlClaimMatchDiaryType.SelectedIndex = 0;
                txtClaimMatchDiaryText.Text = "";
                chkClaimMatchForEnhanced.Checked = false;
                ddlClaimMatchEnhancedNotes.SelectedIndex = 0;
                txtClaimMatchEnhancedNotes.Text = "";
                //sagarwal54 08/23/2013 MITS 33168 End
               
            }
        }

        protected void GP2_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtClaimUser.Checked == true)
            {
                lstClaimOtherUser.Items.Clear();
                lstClaimOtherUser.Disabled = false;
                AutoFillUseridbtn1.Disabled = false;
                OtherUseridbtndel1.Disabled = false;
            }
            else if(rbtClaimAdjuster.Checked==true)
            {
                lstClaimOtherUser.Items.Clear();
                hdCMOtherUserID.Value = "";
                ClaimMatchUserId.Text = "";
                hdCMOtherUserName.Value = "";
                ClaimMatchUserName.Text = "";
                lstClaimOtherUser.Disabled = true;
                AutoFillUseridbtn1.Disabled = true;
                OtherUseridbtndel1.Disabled = true;
            }
        }

        protected void GP1_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtSubmissorUser.Checked == true)
            {
                lstSubmissorOtherUser.Items.Clear();
                lstSubmissorOtherUser.Disabled = false;
                AutoFillUseridbtn.Disabled = false;
                OtherUseridbtndel.Disabled = false;
            }
            else if (rbtSubmissorAdjuster.Checked == true)
            {
                lstSubmissorOtherUser.Items.Clear();
                hdSROtherUserID.Value = "";
                SubmissionUserId.Text = "";
                hdSROtherUserName.Value = "";
                SubmissionUserName.Text = "";
                lstSubmissorOtherUser.Disabled = true;
                AutoFillUseridbtn.Disabled = true;
                OtherUseridbtndel.Disabled = true;
            }
        }

        //sagarwal54 08/23/2013 MITS 33168 Start
        protected void chkClaimMatchForEnhanced_CheckedChanged(object sender, EventArgs e)
        {
            if (chkClaimMatchForEnhanced.Checked == true)
            {
                lblClaimMatchEnhancedNotesText.Enabled = true;
                ddlClaimMatchEnhancedNotes.Enabled = true;
                lblClaimMatchEnhancedNotesType.Enabled = true;
                txtClaimMatchEnhancedNotes.Enabled = true;
            }
            else
            {
                lblClaimMatchEnhancedNotesText.Enabled = false;
                ddlClaimMatchEnhancedNotes.Enabled = false;
                lblClaimMatchEnhancedNotesType.Enabled = false;
                txtClaimMatchEnhancedNotes.Enabled = false;
                ddlClaimMatchEnhancedNotes.SelectedIndex = 0;
                txtClaimMatchEnhancedNotes.Text = "";
            }
        }

        protected void chkSubmissorForEnhanced_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSubmissorForEnhanced.Checked == true)
            {
                lblSubRejEnhancedNotestText.Enabled = true;
                ddlSubRejEnhancedNotes.Enabled = true;
                lblSubRejEnhancedNotesType.Enabled = true;
                txtSubRejEnhancedNotes.Enabled = true;
            }
            else
            {
                lblSubRejEnhancedNotestText.Enabled = false;
                ddlSubRejEnhancedNotes.Enabled = false;
                lblSubRejEnhancedNotesType.Enabled = false;
                txtSubRejEnhancedNotes.Enabled = false;
                ddlSubRejEnhancedNotes.SelectedIndex = 0;
                txtSubRejEnhancedNotes.Text = "";
            }
        }
        //sagarwal54 08/23/2013 MITS 33168 End
        //ipuri Mits:30917 End
        
    }
}
