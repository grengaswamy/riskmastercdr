﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TMView.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.TMView" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc2" TagName="UserControlGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register Src="../../../Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TM Jobs View</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />


    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/TMSettings.js"></script>

</head>
<body onload="OnLoad();parent.MDIScreenLoaded();">
    <form id="frmData" title="<%$ Resources:lblTMView %>" runat="server" >
    <table border="0">
        <tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <table border="0">
                    <tr>
                        <td class="ctrlgroup" colspan="2">
                           <!--**ksahu5-ML-MITS33942 Start**--> 
                            <%--Running Jobs--%>
                            <asp:Label ID="lblRunningJobs" runat="server" Text="<%$ Resources:lblRunningJobs %>"></asp:Label>
                           <!--**ksahu5-ML-MITS33942 End**--> 
                        </td>
                    </tr>
                    <tr>
                        <td width="780px">
                            <b></b>
                            <table width="100%">
                                <tr>
                                    <td width="80%">
                                        <uc2:UserControlGrid runat="server" ID="JobListGrid" GridName="JobListGrid" GridTitle=""
                                            Target="Document/Document/TaskInfoList" Ref="" Unique_Id="jobid" ShowRadioButton="true"
                                            Height="180px" Width="780px" HideNodes="|jobid|jobstateid|" ShowHeader="True" LinkType="Link" AllowSorting="true"
                                            PopupWidth="290px" PopupHeight="350px" Type="Grid" hidebuttons="New, Edit, Delete, Clone" OnClick="OnTaskSelect();" />  <!--**ksahu5-ML-MITS33942 **--> 
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="12">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2">
                                        <!--**ksahu5-ML-MITS33942 Start**--> 
                                       <%-- <asp:Button ID="btnKill" Text="Abort" Width="80px" class="button" OnClientClick="KillTask();"
                                            runat="server" />--%>
                                            <asp:Button ID="btnKill" Text="<%$ Resources:btnKill %>" Width="80px" class="button" OnClientClick="KillTask();"
                                            runat="server" />
                                         <!--**ksahu5-ML-MITS33942 End**-->  

                                    </td>
                                    <td colspan="2">
                                        <!--**ksahu5-ML-MITS33942 Start**--> 
                                       <%-- <asp:Button ID="btnRefresh" Text="Refresh" Width="100px" class="button" OnClientClick="RefreshTaskList();"
                                            runat="server" />--%>
                                            <asp:Button ID="btnRefresh"  Text="<%$ Resources:btnRefresh %>" Width="100px" class="button" OnClientClick="RefreshTaskList();"
                                            runat="server" />
                                        <!--**ksahu5-ML-MITS33942 End**-->  
                                    </td>
                                    <td>
                                        <asp:TextBox value="" Style="display: none" ID="hdnaction" runat="server" />
                                    </td>
                                    <td>
                                        <asp:TextBox value="" Style="display: none" ID="hdnSelectedRow" runat="server" />
                                    </td>
                                     <td>
                                        <asp:TextBox value="" Style="display: none" ID="hdnRefresh" runat="server" />
                                    </td>  
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ctrlgroup" colspan="2">
                            <!--**ksahu5-ML-MITS33942 Start**--> 
                            <%--Archived Jobs--%>
                             <asp:Label ID="lblArchievedJobs" runat="server" Text="<%$ Resources:lblArchievedJobs %>"></asp:Label>
                            <!--**ksahu5-ML-MITS33942 End**-->  
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td><asp:Label ID="lblPagerTop" runat="server"></asp:Label></td>
                                    <td align="right">
                                        <asp:Label ID="lblPageRangeTop" runat="server"></asp:Label>
                                         <!--**ksahu5-ML-MITS33942 Start**--> 
                                        <%--<asp:LinkButton id="lnkFirstTop"   ForeColor="black" runat="server"  OnCommand = "LinkCommand_Click" CommandName="First">First|</asp:LinkButton> 
                                        <asp:LinkButton id="lnkPrevTop" runat="server" ForeColor="black" OnCommand = "LinkCommand_Click" CommandName="Prev">Previous</asp:LinkButton>
                                        <asp:LinkButton id="lnkNextTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next">|Next</asp:LinkButton>
                                        <asp:LinkButton id="lnkLastTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last">|Last</asp:LinkButton>--%>
                                                                                
                                        <asp:LinkButton id="lnkFirstTop"   ForeColor="black" runat="server"  OnCommand = "LinkCommand_Click" CommandName="First" Text="<%$ Resources:lnkbtnFirstResrc %>"></asp:LinkButton> 
                                        <asp:LinkButton id="lnkPrevTop" runat="server" ForeColor="black" OnCommand = "LinkCommand_Click" CommandName="Prev" Text="<%$ Resources:lnkbtnPrevResrc %>"></asp:LinkButton>
                                        <asp:LinkButton id="lnkNextTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next" Text="<%$ Resources:lnkbtnNextResrc %>"></asp:LinkButton>
                                         <asp:LinkButton id="lnkLastTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last" Text="<%$ Resources:lnkbtnLastResrc %>"></asp:LinkButton>
                                        <!--**ksahu5-ML-MITS33942 End**-->  
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td width="780px">
                            <b></b>
                            <table width="100%">
                                <tr>
                                    <td width="80%">
                                        <uc2:UserControlGrid runat="server" ID="ArchJobListGrid" GridName="ArchJobListGrid" LinkType="Link"
                                            GridTitle="" Target="Document/Document/ArchivedTaskInfoList" Ref="" Unique_Id="jobid"
                                            ShowRadioButton="false" Height="180px" Width="780px" HideNodes="|jobid|dataintegratorjob|jobstateid|" ShowHeader="True"
                                             ImageUrl="|tb_attach_mo.png" PopupWidth="290px" PopupHeight="350px" Type="Grid" hidebuttons="New, Edit, Delete, Clone"/> <!--**ksahu5-ML-MITS33942 **--> 
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
    
    <asp:HiddenField ID="hdJobId" runat="server" />

    <asp:HiddenField ID="hdCurrentPage" runat="server" />

    <asp:TextBox style="display:none" id="hdTotalRows" runat="server" rmxignoreset="true" rmxref="./Document/Document/ArchivedTaskInfoList/@totalrecords"></asp:TextBox>
    <asp:TextBox style="display:none" id="hdPageSize" runat="server" rmxignoreset="true" rmxref="./Document/Document/ArchivedTaskInfoList/@pagesize"></asp:TextBox>
    <asp:TextBox style="display:none" id="hdPageNumber" runat="server" rmxignoreset="true" rmxref="./Document/Document/ArchivedTaskInfoList/@pagenumber"></asp:TextBox>
    <asp:TextBox style="display:none" id="hdTotalPages" runat="server" rmxignoreset="true" rmxref="./Document/Document/ArchivedTaskInfoList/@totalpages"></asp:TextBox>

    </form>
</body>
</html>
