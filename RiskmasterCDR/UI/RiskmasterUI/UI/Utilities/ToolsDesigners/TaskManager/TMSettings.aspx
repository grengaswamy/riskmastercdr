﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TMSettings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.TMSettings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc2" TagName="UserControlGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register Src="../../../Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Schedule a Task</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/TMSettings.js"></script>

</head>
<body onload="onPageLoaded();">
    <form id="frmData" runat="server">
    <table cellspacing="0" cellpadding="0" border="0" width="50%">
        <tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr height="10">
            <td>
            </td>
        </tr>
        <tr>
            <td align="left">
            <!--Yukti-ML Changes Start
               Task Type:!-->
               <asp:label ID= "lblTaskType" runat="server" Text ="<%$ Resources:lblTaskType %>" />
               <!--Yukti-ML Changes End !-->
            </td>
            <td>
                <asp:DropDownList ID="lstTaskType" runat="server" rmxref="/Instance/Document/Details/TaskType" ItemSetRef="/Instance/Document/Details/TaskType" onchange="return GetDisabled();"></asp:DropDownList>           
            </td>
        </tr>
        <tr height="10">
            <td>
            </td>
        </tr>
        <tr>
            <td align="left">
           <!-- Yukti-ML Changes Start
                Task Name: !-->
                <asp:Label runat="server" ID="lblTaskName" Text="<%$ Resources:lblTaskName %>" />
                <!--Yukti-ML Changes End-->
            </td>
            <td>
                <asp:TextBox ID="txtTaskName" runat="server"></asp:TextBox>           
            </td>
        </tr>
        <tr height="10">
            <td>
            </td>
        </tr>
         <tr>
            <td align="left">
           <!--Yukti-ML Changes Start
                Sub Task Name: !-->
                <asp:Label runat="server" ID="lblSubTaskName" Text="<%$ Resources:lblSubTaskName %>" />
            </td>
            <td>               
                <asp:DropDownList ID="lstSubTaskName" runat="server" rmxref="/Instance/Document/Details/SubTaskName" >
                <asp:ListItem value ="" Text=""></asp:ListItem>
              <%-- Yukti-ML Changes Start
                <asp:ListItem value ="1" Text="Setup and Populate Data"></asp:ListItem>
                <asp:ListItem value ="2" Text="Purge Data"></asp:ListItem>
                <asp:ListItem value ="3" Text="Setup,Populate and Purge"></asp:ListItem>--%>
                <asp:ListItem value ="1" Text="<%$ Resources:liSetupandPopulateData %>"></asp:ListItem>
                <asp:ListItem value ="2" Text="<%$ Resources:liPurgeData %>"></asp:ListItem>
                <asp:ListItem value ="3" Text="<%$ Resources:liSetupPopulateandPurge %>"></asp:ListItem>
                </asp:DropDownList>           
            </td>
        </tr>
        <tr height="10">
            <td>
            </td>
        </tr>
        <tr>
            <td align="left">
            <!--YUkti-ML Changes Start
                Schedule Type: !-->
                <asp:Label runat="server" ID="lblSchedType" Text= "<%$ Resources:lblSchedType %>" />
                <!--Yukti-ML Changes End !-->
            </td>
            <td>
                <asp:DropDownList ID="lstScheduleType" runat="server" rmxref="/Instance/Document/Details/ScheduleType" ItemSetRef="/Instance/Document/Details/ScheduleType" onchange="return MoveTo(this);"></asp:DropDownList>           
                <asp:DropDownList ID="lstScheduleTypeForHistoryTracking" runat="server" rmxref="/Instance/Document/Details/ScheduleType" onchange="return MoveTo(this);" >
                <asp:ListItem value ="" Text=""></asp:ListItem>
              <%-- YUkti-ML Changes Start
                <asp:ListItem value ="1" Text="OneTime"></asp:ListItem>
                <asp:ListItem value ="2" Text="Periodically"></asp:ListItem> --%>
                <asp:ListItem value ="1" Text="<%$ Resources:liOneTime %>"></asp:ListItem>
                <asp:ListItem value ="2" Text="<%$ Resources:liPeriodically %>"></asp:ListItem>
                </asp:DropDownList>           
            </td>
        </tr>
        <tr height="20">
            <td>
                &#160;
            </td>
        </tr>
        <tr>
            <td>
                &#160;
            </td>
            <td>
           <%--Yukti-ML Changes Start
                <asp:Button ID="btnCancel" Text="Cancel" Width="150px" class="button" OnClientClick="OnCancel();return false;" runat="server" /> --%>
                <asp:Button ID="btnCancel" Text="<%$ Resources:btnCancel %>" Width="150px" class="button" OnClientClick="OnCancel();return false;" runat="server" />
                <!--Yukti-ML Changes End !-->
            </td>
        </tr>
        <!-- Shruti ends-->
    </table>
    <asp:TextBox value="" Style="display: none" ID="hdnaction" runat="server" />
    <asp:TextBox value="" Style="display: none" ID="hdnScheduleType" RMXRef="/Instance/Document/ScheduleTypeText" runat="server" />
    <asp:TextBox value="" Style="display: none" ID="hdnTaskType" RMXRef="/Instance/Document/TaskTypeText" runat="server" />    
    <asp:TextBox value="" Style="display: none" ID="hdSubTaskNameText" RMXRef="/Instance/Document/SubTaskNameText" runat="server" />
    <asp:TextBox value="" Style="display: none" ID="hdDisabledStatusDetails" RMXRef="/Instance/Document/Details/DisableDetailedStatus" runat="server" />    
    <asp:TextBox value="" Style="display: none" ID="hdSystemModuleName" RMXRef="/Instance/Document/Details/SystemModuleName" runat="server" />    
    </form>
</body>
</html>
