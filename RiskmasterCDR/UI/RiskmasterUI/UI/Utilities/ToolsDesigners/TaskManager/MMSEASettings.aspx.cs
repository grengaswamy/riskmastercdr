﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessHelpers;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.ServiceHelpers;
//Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
using Riskmaster.UI.DataIntegratorService;    //ipuri 12/06/2014 SOA Service discontinued
using System.IO;
using Riskmaster.Models;    //ipuri 12/06/2014

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class MMSEASettings : System.Web.UI.Page
    {
        private int iOptionSetIDParam;
        private const string sModuleName = "MMSEA";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));
                iOptionSetIDParam = CommonFunctions.OptionsetId;
                hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                RetriveSettingsFromDB();
                SetExtraOptionFields();
            }

            ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('" + hTabName.Value + "')};", true);
        }
        //npradeepshar 01/12/2010 MITS 23301
        /// <summary>
        /// Function will save setting into data integrator table. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveSetting();
        }
        private void RetriveSettingsFromDB()
        {
			//ipuri REST Service Conversion 12/06/2014 Start
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objReturnModel = null;
            //DataIntegratorService.DataIntegratorServiceClient objService = null;
           Riskmaster.Models.DataIntegratorModel objReturnModel = null;
			//ipuri REST Service Conversion 12/06/2014 End
			
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            bool bErr = false;
            try
            {
                //objReturnModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();		//ipuri 12/06/2014
                objReturnModel = new Riskmaster.Models.DataIntegratorModel();
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objReturnModel.Token = AppHelper.GetSessionId();
                }
                //objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();	//ipuri 12/06/2014

                objReturnModel.OptionSetID = iOptionSetIDParam;


                objReturnModel.ModuleName = sModuleName;
                //Anand MITS 17486,17584.
                try
                {
					//ipuri REST Service Conversion 12/06/2014 Start
                    //objReturnModel = objService.RetrieveSettings(objReturnModel);
                    objReturnModel.ClientId = AppHelper.ClientId;

                    objReturnModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objReturnModel);
					//ipuri REST Service Conversion 12/06/2014 End
                }
                catch (Exception exp)
                {
                    //Vsoni5 : MITS 22565 : Exception thrown by Service layer will be displayed on UI instead of hard coded error message
                    err.Add("DataIntegrator.Error", exp.Message, BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                Dictionary<string, string> MMSEA_Setting = new Dictionary<string, string>();
                ddlExportRREId.DataSource = objReturnModel.dsRREId;
                ddlExportRREId.DataTextField = objReturnModel.dsRREId.Tables[0].Columns[0].ToString();
                ddlExportRREId.DataValueField = objReturnModel.dsRREId.Tables[0].Columns[0].ToString();
                ddlExportRREId.DataBind();
                ddlExportRREId.Items.Insert(0, "--Select--");

                ddlImportRREId.DataSource = objReturnModel.dsRREId;
                ddlImportRREId.DataTextField = objReturnModel.dsRREId.Tables[0].Columns[0].ToString();
                ddlImportRREId.DataValueField = objReturnModel.dsRREId.Tables[0].Columns[0].ToString();
                ddlImportRREId.DataBind();
                ddlImportRREId.Items.Insert(0, "--Select--");

                MMSEA_Setting = (Dictionary<string, string>)objReturnModel.Parms;

                if (MMSEA_Setting["TYPE_OF_FILE"] == "Export")
                {
                    hTabName.Value = "exportsetting";
                    if (objReturnModel.OptionSetName != null)
                    {
                        txtExportOptionset.Text = objReturnModel.OptionSetName;
                        txtExportOptionset.ReadOnly = true;
                    }
                    try
                    {
                        ddlExportRREId.Items.FindByText(MMSEA_Setting["RRE_ID"]).Selected = true;
                    }
                    catch
                    {
                        ddlExportRREId.SelectedIndex = 0;
                    }
                    try
                    {
                        ddlExportFileFormat.Items.FindByText(MMSEA_Setting["FILE_FORMAT"]).Selected = true;
                    }
                    catch
                    {
                        ddlExportFileFormat.SelectedIndex = 0;
                    }
                    if (MMSEA_Setting["TEST_EXPORT"] == "-1")
                        chkTestExport.Checked = true;
                    else
                        chkTestExport.Checked = false;

                    //mcapps3 08/03/2011 MITS 25536
                    try
                    {
                        if (MMSEA_Setting["PRIMARY_CLAIMANT"] == "-1")
                            chkPrimayClaimant.Checked = true;
                        else
                            chkPrimayClaimant.Checked = false;
                    }

                    catch
                    {
                        chkPrimayClaimant.Checked = false;
                    }
                    //mcapps3 08/03/2011 MITS 25634
                    try
                    {
                        if (MMSEA_Setting["TPOCS_THRESHOLD"] == "-1")

                            chkTPOCSThreshold.Checked = true;
                        else
                            chkTPOCSThreshold.Checked = false;
                    }
                    catch
                    {
                        chkTPOCSThreshold.Checked = false;
                    }


                }
                else if (MMSEA_Setting["TYPE_OF_FILE"] == "Import")
                {
                    hTabName.Value = "importsetting";
                    if (objReturnModel.OptionSetName != null)
                    {
                        txtImportOptionset.Text = objReturnModel.OptionSetName;
                        txtImportOptionset.ReadOnly = true;
                    }
                    try
                    {
                        ddlImportRREId.Items.FindByText(MMSEA_Setting["RRE_ID"]).Selected = true;
                    }
                    catch
                    {
                        ddlExportRREId.SelectedIndex = 0;
                    }
                    try
                    {
                        ddlImportFileFormat.Items.FindByText(MMSEA_Setting["FILE_FORMAT"]).Selected = true;
                    }
                    catch
                    {
                        ddlImportFileFormat.SelectedIndex = 0;
                    }

                }

            }
            catch (Exception ex)
            {
            }
            if (bErr == true)       //Anand MITS 17486,17584.
            {
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
        }
        /// <summary>
        /// Function to save UI Settings
        /// </summary>
        private void SaveSetting()
        {//npradeepshar MITS 23301 17/01/2010 Logs the error into log file as well display it on UI
            string sTaskManagerXml = string.Empty;
			//ipuri REST Service Conversion 12/06/2014 Start
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            //DataIntegratorService.DataIntegratorServiceClient objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
            Riskmaster.Models.DataIntegratorModel objModel = null;
			//ipuri REST Service Conversion 12/06/2014 End
            BusinessAdaptorErrors err = null;
            bool bErr = false;
            try
            {
                objModel = new Riskmaster.Models.DataIntegratorModel();
                err = new BusinessAdaptorErrors();
                sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
                iOptionSetIDParam = Conversion.ConvertObjToInt(hdOptionsetId.Value);
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objModel.Token = AppHelper.GetSessionId();
                }
                Dictionary<string, string> MMSEA_Setting = new Dictionary<string, string>();
                //Anand MITS 17486,17584.
                if (txtExportOptionset.Text == "")
                {
                    err.Add("DataIntegrator.Error", "Optionset Name can not be blank. ", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                //Anand MITS 17486,17584.
                if (ddlExportFileFormat.SelectedItem.Value == "--Select--")
                {
                    err.Add("DataIntegrator.Error", "Please choose a file format.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                //Anand MITS 17486,17584.
                if (ddlExportRREId.SelectedItem == null || ddlExportRREId.SelectedItem.Value == "--Select--")
                {
                    err.Add("DataIntegrator.Error", "Please choose a RRE ID.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                if (bErr == true)      //Anand MITS 17486,17584.
                {
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                MMSEA_Setting.Add("RRE_ID", ddlExportRREId.SelectedItem.Text);
                MMSEA_Setting.Add("TYPE_OF_FILE", "Export");
                MMSEA_Setting.Add("FILE_FORMAT", ddlExportFileFormat.SelectedItem.Text);
                if (chkTestExport.Checked)
                    MMSEA_Setting.Add("TEST_EXPORT", "-1");
                else
                    MMSEA_Setting.Add("TEST_EXPORT", "0");

                MMSEA_Setting.Add("IMPORTFILE_NAME", "");
                objModel.OptionSetName = txtExportOptionset.Text;

                //mcapps3 08/03/2011 MITS 25536
                if (chkPrimayClaimant.Checked)
                    MMSEA_Setting.Add("PRIMARY_CLAIMANT", "-1");
                else
                    MMSEA_Setting.Add("PRIMARY_CLAIMANT", "0");

                //mcapps3 08/04/2011 MITS 25634
                if (chkTPOCSThreshold.Checked && ddlExportFileFormat.Text == "Claim Input")
                    MMSEA_Setting.Add("TPOCS_THRESHOLD", "-1");
                else
                    MMSEA_Setting.Add("TPOCS_THRESHOLD", "0");
                
                //mcapps3 09/02/2011 MITS 25532
                MMSEA_Setting.Add("TIN_IMPORTFILE_NAME", "");


                objModel.Parms = MMSEA_Setting;
                objModel.OptionSetID = iOptionSetIDParam;
                objModel.ModuleName = sModuleName;

                objModel.TaskManagerXml = sTaskManagerXml;

				//ipuri REST Service Conversion 12/06/2014 Start
                //objModel = objService.SaveSettings(objModel);
                objModel.ClientId = AppHelper.ClientId;

                objModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
            	//ipuri REST Service Conversion 12/06/2014 End
                if (objModel.OptionSetID == -1)
                {
                    //Anand MITS 17486,17584.

                    err.Add("DataIntagrator.Error", "OptionSet Name already exists.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }

                if (objModel.OptionSetID > 0)
                {
                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
                }
            }
            catch (Exception ex)
            {
                //npradeepshar 03/25/2011: Removed the code form event and added to the method.MITS 23301
                //npradeepshar 01/12/2010 MITS 23301 : Log the error into log file as well display it on UI
                err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                /// Vsoni5 : 01/29/2011 : MITS 23347
                try
                {
                    if (iOptionSetIDParam <= 0 && objModel.OptionSetID > 0)
                        //objService.SaveSettingsCleanup(objModel);
                    objModel.ClientId = AppHelper.ClientId;

                    objModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettingscleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
            
                }
                catch (Exception eCleanup)
                {
                }
                return;
            }

        }

        protected void btnSaveImport_Click(object sender, EventArgs e)
        {
            SaveImportSetting();
        }

        private void SaveImportSetting()
        {
            string sTaskManagerXml = string.Empty;
			//ipuri REST Service Conversion 12/06/2014 Start
            //kkaur25 -SOA start
            Riskmaster.UI.DataIntegratorService.DataIntegratorModel objModelS = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            DataIntegratorService.DataIntegratorServiceClient objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
            Riskmaster.Models.DataIntegratorModel objModel = null;
            //kkaur25 -SOA end
			//ipuri REST Service Conversion 12/06/2014 End
            BusinessAdaptorErrors err = null;
            bool bErr = false;

            try
            {
                objModel = new Riskmaster.Models.DataIntegratorModel();
                err = new BusinessAdaptorErrors();

                sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
                iOptionSetIDParam = Conversion.ConvertObjToInt(hdOptionsetId.Value);
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objModel.Token = AppHelper.GetSessionId();
                }
                Dictionary<string, string> MMSEA_Setting = new Dictionary<string, string>();
                //Anand MITS 17486,17584.
                if (txtImportOptionset.Text == "")
                {
                    err.Add("DataIntegrator.Error", "Optionset Name can not be blank. ", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                //Anand MITS 17486,17584.
                if (ddlImportFileFormat.SelectedItem.Value == "--Select--")
                {
                    err.Add("DataIntegrator.Error", "Please choose a file format.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                //Anand MITS 17486,17584.
                if (ddlImportRREId.SelectedItem == null || ddlImportRREId.SelectedItem.Value == "--Select--")
                {
                    err.Add("DataIntegrator.Error", "Please choose a RRE ID.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                MMSEA_Setting.Add("RRE_ID", ddlImportRREId.SelectedItem.Text);
                MMSEA_Setting.Add("TYPE_OF_FILE", "Import");
                MMSEA_Setting.Add("FILE_FORMAT", ddlImportFileFormat.SelectedItem.Text);
                MMSEA_Setting.Add("TEST_EXPORT", "");

                string sFileName = FUImport.PostedFile.FileName.Substring(FUImport.PostedFile.FileName.LastIndexOf("\\") + 1);
                //mcapps3 09/02/2011 added sTinFileName for MITS 25532
                string sTinFileName = "";
                if (FUTinImport.Visible == true)
                {
                     sTinFileName = FUTinImport.PostedFile.FileName.Substring(FUTinImport.PostedFile.FileName.LastIndexOf("\\") + 1);
                }

                //Anand MITS 17486,17584.
                if (sFileName == null || sFileName == "")
                {
                    err.Add("DataIntegrator.Error", "Please choose a file to be imported.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                if (bErr == true)      //Anand MITS 17486,17584.
                {
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                MMSEA_Setting.Add("IMPORTFILE_NAME", sFileName);

                objModel.OptionSetName = txtImportOptionset.Text;
                //mcapps3 08/03/2011 MITS 25536
                MMSEA_Setting.Add("PRIMARY_CLAIMANT", "0");
                //mcapps3 08/04/2011 MITS 25634
                MMSEA_Setting.Add("TPOCS_THRESHOLD", "0");
                //mcapps3 09/02/2011 MITS 25532
                MMSEA_Setting.Add("TIN_IMPORTFILE_NAME", sTinFileName);

                objModel.Parms = MMSEA_Setting;
                objModel.OptionSetID = iOptionSetIDParam;
                objModel.ModuleName = sModuleName;
                objModel.ImportFlage = true;
                objModel.TaskManagerXml = sTaskManagerXml;
				//ipuri REST Service Conversion 12/06/2014 Start
                //objModel = objService.SaveSettings(objModel);

                objModel.ClientId = AppHelper.ClientId;

                objModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
            	//ipuri REST Service Conversion 12/06/2014 End
                if (objModel.OptionSetID == -1)
                {
                    //Anand MITS 17486,17584.
                    err.Add("DataIntagrator.Error", "OptionSet Name is already used.", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                //Vsoni5 : MITS 22699 : This block of code is no more required
                //else
                //{
                //    if (!String.IsNullOrEmpty(sFileName))
                //    {
                //        //Anand: Appended option set ID in the file name.
                //        sFileName = sFileName + "." + objModel.OptionSetID;
                //        //FUImport.PostedFile.SaveAs(objModel.FilePath + sFileName);
                //    }
                //    //Anand MITS 17487.
                //    //FUImport.SaveAs(objModel.FilePath + sFileName);
                //}

                //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer                
                if (objModel.OptionSetID > 0)
                {
                    //kkaur25-SOA start
                    Riskmaster.Models.DAImportFile objDAImportFile = new Riskmaster.Models.DAImportFile();
                    Riskmaster.UI.DataIntegratorService.DAImportFile objDAImportFileS = new Riskmaster.UI.DataIntegratorService.DAImportFile();
                    //kkaur25-SOA end     

                    objDAImportFile.Token = AppHelper.GetSessionId();
                    Stream data = FUImport.PostedFile.InputStream;
                    objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                    objDAImportFile.fileName = sFileName + "." + objModel.OptionSetID;
                    objDAImportFile.filePath = objModel.FilePath;
					//ipuri REST Service Conversion 12/06/2014 Start
                    //objService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                    //kkaur25 start- JIRA RMACLOUD 3070 MMSEA CLOUD EXE CHANGES
                    objDAImportFile.ModuleName = "MMSEA";
                    objDAImportFile.DocumentType = "Import";
                    objDAImportFile.OptionsetId = objModel.OptionSetID;
                    //kkaur25 end- JIRA RMACLOUD 3070 MMSEA CLOUD EXE CHANGES
                    objDAImportFile.ClientId = AppHelper.ClientId;

                    //kkaur25-SOA change start
                    objDAImportFileS.FileContents = objDAImportFile.FileContents;
                    objDAImportFileS.fileName = objDAImportFile.fileName;
                    objDAImportFileS.filePath = objModel.FilePath;
                    objDAImportFileS.Token = AppHelper.Token;
                    objDAImportFileS.ClientId = AppHelper.ClientId;
                    objDAImportFileS.ModuleName = "MMSEA";
                    objDAImportFileS.DocumentType = "Import";
                    objDAImportFileS.OptionsetId = objModel.OptionSetID;
                    //Call SOAP/XML service
                    objService.UploadDAImportFile(objDAImportFileS);
                    //kkaur25 SOA end

                    //AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile); 
					//ipuri REST Service Conversion 12/06/2014 End

                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
                    //mcapps3 01/24/2012  MITS 27183 added if statment to make sure there is a tin file to import 
                    if (sTinFileName.Length != 0)
                    {
                        Riskmaster.Models.DAImportFile objDATINImportFile = new Riskmaster.Models.DAImportFile();
                        Riskmaster.UI.DataIntegratorService.DAImportFile objDAImportFileT = new Riskmaster.UI.DataIntegratorService.DAImportFile();
                        objDATINImportFile.Token = AppHelper.GetSessionId();
                        //mcapps3 12/8/2011 MITS 26778 add functionality to move the TIN file to the RMX server   
                        Stream dataTIN = FUTinImport.PostedFile.InputStream;
                        objDATINImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(dataTIN);
                        objDATINImportFile.fileName = sTinFileName + "." + objModel.OptionSetID;
                        objDATINImportFile.filePath = objModel.FilePath;
						//ipuri REST Service Conversion 12/06/2014 Start
                        //objService.UploadDAImportFile(objDATINImportFile.Errors, objDATINImportFile.Token, objDATINImportFile.fileName, objDATINImportFile.filePath, objDATINImportFile.fileStream);
                        //kkaur25 start-JIRA RMACLOUD 3070  MMSEA CLOUD EXE CHANGES
                        objDATINImportFile.ModuleName = "MMSEA";
                        objDATINImportFile.DocumentType = "Import";
                        objDATINImportFile.OptionsetId = objModel.OptionSetID;
                        //kkaur25 end-JIRA RMACLOUD 3070  MMSEA CLOUD EXE CHANGES
                        objDATINImportFile.ClientId = AppHelper.ClientId;

                        //kkaur25-SOA change start
                        objDAImportFileT.FileContents = objDATINImportFile.FileContents;
                        objDAImportFileT.fileName = objDATINImportFile.fileName;
                        objDAImportFileT.filePath = objModel.FilePath;
                        objDAImportFileT.Token = AppHelper.Token;
                        objDAImportFileT.ClientId = AppHelper.ClientId;
                        objDAImportFileT.ModuleName = "MMSEA";
                        objDAImportFileT.DocumentType = "Import";
                        objDAImportFileT.OptionsetId = objModel.OptionSetID;
                        //Call SOAP/XML service
                        objService.UploadDAImportFile(objDAImportFileT);
                        //kkaur25 SOA end

                        //AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDATINImportFile); 
						//ipuri REST Service Conversion 12/06/2014 End
                        Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
}
                  }
            }
            catch (Exception Ex)
            {
                //Anand MITS 17486,17584.
                err.Add(Ex, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                /// Vsoni5 : 01/29/2011 : MITS 23347
                try
                {
                    if (iOptionSetIDParam <= 0 && objModel.OptionSetID > 0)
					//ipuri REST Service Conversion 12/06/2014 Start
                        //objService.SaveSettingsCleanup(objModel);
                        objModel.ClientId = AppHelper.ClientId;
                    AppHelper.GetResponse("RMService/DAIntegration/savesettingscleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                   //ipuri REST Service Conversion 12/06/2014 End
                }
                catch (Exception eCleanup)
                {
                }
            }
        }


        /// <summary>
        /// mcapps3  MITS 25634 09/17/2011
        /// The SetExtraOptionFields set the ddlExportFileFormat based on if it is a claim or query export.
        /// mcapps3 MITS 25532 added code to make fiel to browse do the the fine visible and un visible
        /// </summary>
        private void SetExtraOptionFields()
        {
            if (hTabName.Value == "exportsetting")
            {
                if (ddlExportFileFormat.Text == "Claim Input")
                {
                    Claim_TPOC.Visible = true;
                    Claim_TIN.Visible = false;
                }
                else if (ddlExportFileFormat.Text == "Query Input")
                {
                    Claim_TPOC.Visible = false;
                    Claim_TIN.Visible = false;
                }
                else
                {
                    Claim_TPOC.Visible = false;
                    Claim_TIN.Visible = false;
                }
       
            }
            else if (hTabName.Value == "importsetting")
            {
                if (ddlImportFileFormat.Text == "Claim")
                {
                    Claim_TPOC.Visible = false;
                    Claim_TIN.Visible = true;
                }
                else
                {
                    Claim_TPOC.Visible = false;
                    Claim_TIN.Visible = false;
                }

            }
            else
            {
                Claim_TPOC.Visible = false;
                Claim_TIN.Visible = false;
            }
        }

        /// <summary>
        /// mcapps3  MITS 25634 08/17/2011
        /// when the ddlExportFileFormat changes this the ddlExportFileFormat_OnSelectedIndexChanged runs. 
        /// </summary>
        protected void ddlExportFileFormat_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            SetExtraOptionFields();
        }
        /// mcapps3  MITS 25582 09/02/2011
        /// when the ddlImportFileFormat changes this the ddlImportFileFormat_OnSelectedIndexChanged runs. 
        /// </summary>

        protected void ddlImportFileFormat_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            SetExtraOptionFields();
        }

        
        
    }
}
