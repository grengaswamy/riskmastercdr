﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
//using Riskmaster.UI.DataIntegratorService;
using System.Data;
using Riskmaster.Models;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class ISOPropertyMappings : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();
        private bool bLoadOnEdit { get; set; }
        private String[] arrtmp = { };        
        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {
                mode.Text = AppHelper.GetQueryStringValue("mode");
               
                if (!IsPostBack)
                {
                    if (mode.Text == "edit")
                    {
                        bLoadOnEdit = true;
                    }
                    else
                    {
                        bLoadOnEdit = false;
                    }

                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    
                    if (mode.Text == "add" || bLoadOnEdit)
                    {
                        string strSelectedRowId = AppHelper.GetQueryStringValue("selectedid");
                        if (!string.IsNullOrEmpty(strSelectedRowId))
                        {
                            arrtmp = strSelectedRowId.Split('|');                            
                        }
                        GetPropTypes();
                    }
                   
                }

               
            }
            catch (Exception ee)
            {

                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        protected void GetPropTypes()
        {
            DataSet ds = new DataSet();
            //DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
            DataIntegratorModel objDIModel = new DataIntegratorModel();
            try
            {
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }
                //var strXML = objDIService.GetrmAPropTypeAndISOPropTypeServiceFunc(objDIModel);

                objDIModel.ClientId = AppHelper.ClientId;

                var strXML = AppHelper.GetResponse<string>("RMService/DAIntegration/getrmaproptypeandisoproptypeservicefunc", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);

                System.IO.StringReader strrdr = new System.IO.StringReader(strXML);
                ds.ReadXml(strrdr);

                //bind 1st dropdownlist
                ddlrmAPropType.DataSource = ds.Tables[0];
                ddlrmAPropType.DataTextField = "SHORT_CODE";
                ddlrmAPropType.DataValueField = "CODE_ID";
                ddlrmAPropType.DataBind();
                ddlrmAPropType.Items.Insert(0, "--Select--");

                //bind 2nd dropdownlist
                ddlISOPropType.DataSource = ds.Tables[1];
                ddlISOPropType.DataTextField = "SHORT_CODE";
                ddlISOPropType.DataValueField = "CODE_ID";
                ddlISOPropType.DataBind();
                ddlISOPropType.Items.Insert(0, "--Select--");                

                if (bLoadOnEdit)
                {
                    ddlrmAPropType.Enabled = false;
                    ddlrmAPropType.SelectedValue = arrtmp[1];
                    ddlISOPropType.SelectedValue = arrtmp[2];
                    txtISORowID.Text = Convert.ToString(arrtmp[0]);
                    return;
                }
            }
            catch (Exception ex)
            {
                //no need to handle
            }
            finally
            {
                //objDIService.Close();
                if (objDIModel != null)
                    objDIModel = null;
            }
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {                                                                            
            XmlTemplate = GetMessageTemplate();
            string sMsgStatus = "";
            if (ddlrmAPropType.SelectedIndex > 0 && ddlISOPropType.SelectedIndex > 0)
            {
                CallCWS("DataIntegratorAdaptor.ISOSavePropertyMapping", XmlTemplate, out sCWSresponse, false, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
            }
           

            if (sMsgStatus == "Success")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            }
            
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ISOPropertyMapping>");

            sXml = sXml.Append("<control name='rmA Property Type' type='int'>");
            if (ddlrmAPropType.SelectedIndex > 0)
            {
                sXml = sXml.Append(ddlrmAPropType.SelectedValue);
            }
            else
            {
                sXml = sXml.Append(0);
            }
            
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='ISO Property Type' type='int'>");
            if (ddlISOPropType.SelectedIndex > 0)
            {
                sXml = sXml.Append(ddlISOPropType.SelectedValue);
            }
            else
            {
                sXml = sXml.Append(0);
            }
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("<control name='ISO_ROW_ID' type='int'>");
            if (!(String.IsNullOrEmpty(txtISORowID.Text)))
            {
                sXml = sXml.Append(Convert.ToInt32(txtISORowID.Text));
            }
            else
            {
                sXml = sXml.Append(0);
            }
            sXml = sXml.Append("</control>");                 
                
            sXml = sXml.Append("</ISOPropertyMapping></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }    
       
    }
}