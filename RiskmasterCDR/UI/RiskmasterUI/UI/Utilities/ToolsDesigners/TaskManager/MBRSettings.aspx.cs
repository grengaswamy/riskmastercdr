﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessHelpers;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.ServiceHelpers;
//Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
using Riskmaster.UI.DataIntegratorService;    //ipuri 12/06/2014 SOA Service discontinued
using System.IO;
using Riskmaster.Models;    //ipuri 12/06/2014
// Neha RMACLOUD-9816 - MBR Compatible with Cloud
// Include the namespace for CuteWebUI which will upload the files in Case of cloud
using CuteWebUI;
//******************************************************************************
//Developer: Vivek Pandey
//Date: 4/3/2009
//Description: This page is for MBR settings being used by Data integrator jobs. 
//              This page is integrated with RMX task manager.
//******************************************************************************
namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class MBRSettings : System.Web.UI.Page
    {
        protected string sJurisdictionState1;
        protected string sJurisdictionState2;
        protected int iOptionSetIDParam;
        private const string sModuleName = "MBR";

        protected void Page_Load(object sender, EventArgs e)
        {
            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes
            if (!IsPostBack)
            {
                //iOptionSetIDParam = Convert.ToInt32(Request.QueryString["OptionSetId"]);
                hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));
                iOptionSetIDParam = CommonFunctions.OptionsetId;
                hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                RetriveSettingsFromDB();
            }
            //MITS 23900 02/23/2011 npradeepshar removed this and added an event for the postback of checkbox 
            //else if(IsPostBack && hTabName.Value != "exportsetting")//vchaturvedi2 MITS - 20923
            //{


            //}//
            ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('" + hTabName.Value + "')};", true);
 // Neha RMACLOUD-9816 - MBR Compatible with Cloud
            // If we have cloud enabled then Show the Upload Attachments Tab And Associate appropriate settings with it
            if (AppHelper.ClientId > 0)
            {
                // Associate Properties for Upload Attachments
                UploadDocumentAttachments.RemoveButtonText = "Remove";
                UploadDocumentAttachments.CancelText = "Cancel";
                UploadDocumentAttachments.UploadingMsg = "Uploading...";
                UploadDocumentAttachments.TableHeaderTemplate = UploadDocumentAttachments.TableHeaderTemplate.Substring(0, 29) + "Files to upload" + "</td>";
                UploadDocumentAttachments.CancelAllMsg = "Cancel all Uploads";
            }
            else
            {
                // Neha RMACLOUD-9816 - MBR Compatible with Cloud
                // We mighty move the Attachment div to a new Tab. In that case we will use the following statement
                // So commention the code for now.
                // Hide the Tab
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "HideAttachments", " $('document').ready(function () {$('#TABSAttachments').hide();});", true);

                // Hide the section
                divAttachments.Visible = false;
            }
        }

        /// <summary>
        /// Vsoni5 : MITS 25670 : Rearranged code flow for logical grouping of functionality and criteria settings.
        /// </summary>
        private void RetriveSettingsFromDB()
        {
            //ipuri REST Service Conversion 12/06/2014 Start
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objReturnModel = null;
            //DataIntegratorService.DataIntegratorServiceClient objService = null;
            Riskmaster.Models.DataIntegratorModel objReturnModel = null;
            //ipuri REST Service Conversion 12/06/2014 End

            bool isError = false;
            //Manika : Start : Vsoni5 : 07/13/2011 : MITS 24924           
            // Changed default Entity/People table names to match the user friendly display name
            // as per code review comments by Jeff.
            string sHospital = "Hospital";
            string sProvider = "Providers";
            string sOtherPeople = "Other People";
            string sPhysician = "Physicians";
            string sEntityTables = string.Empty;
            string sECPeople = string.Empty;
            string sLOBCodes = string.Empty;
            string[] sEntityTablesArray;
            string[] sPeopleTablesArray;
            // END --

            try
            {
                objReturnModel = new Riskmaster.Models.DataIntegratorModel();
                //objReturnModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();   //ipuri 12/06/2014
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objReturnModel.Token = AppHelper.GetSessionId();
                }
                //objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();   //ipuri 12/06/2014

                objReturnModel.OptionSetID = iOptionSetIDParam;
                objReturnModel.ModuleName = sModuleName;
                //ipuri REST Service Conversion 12/06/2014 Start
                //objReturnModel = objService.RetrieveSettings(objReturnModel);
                objReturnModel.ClientId = AppHelper.ClientId;

                objReturnModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objReturnModel);
                //ipuri REST Service Conversion 12/06/2014 End

                Dictionary<string, string> MBR_Setting = new Dictionary<string, string>();
                MBR_Setting = (Dictionary<string, string>)objReturnModel.Parms;

                //Bind Medical reserve dropdown.
                ddlTTMedical.DataSource = objReturnModel.dsMedicalReserve;
                ddlTTMedical.DataTextField = objReturnModel.dsMedicalReserve.Tables[0].Columns[1].ToString();
                ddlTTMedical.DataValueField = objReturnModel.dsMedicalReserve.Tables[0].Columns[0].ToString();
                ddlTTMedical.DataBind();
                ddlTTMedical.Items.Insert(0, "--Select--");
                //Bind Other reserve dropdown.
                ddlTTOther.DataSource = objReturnModel.dsOtherReserve;
                ddlTTOther.DataTextField = objReturnModel.dsOtherReserve.Tables[0].Columns[1].ToString();
                ddlTTOther.DataValueField = objReturnModel.dsOtherReserve.Tables[0].Columns[0].ToString();
                ddlTTOther.DataBind();
                ddlTTOther.Items.Insert(0, "--Select--");
                //Bind Jurisdictions ListBox
                lbStatesAll.DataSource = objReturnModel.dsJurisStates;
                lbStatesAll.DataTextField = objReturnModel.dsJurisStates.Tables[0].Columns[0].ToString();
                lbStatesAll.DataValueField = objReturnModel.dsJurisStates.Tables[0].Columns[0].ToString();
                lbStatesAll.DataBind();
                //Bind Entity type List                
                lbEntityCode.DataSource = objReturnModel.dsEntityCode;
                lbEntityCode.DataTextField = objReturnModel.dsEntityCode.Tables[0].Columns[0].ToString();
                lbEntityCode.DataValueField = objReturnModel.dsEntityCode.Tables[0].Columns[1].ToString();
                lbEntityCode.DataBind();
                //Bind People type List
                lbpeople.DataSource = objReturnModel.dsPeopleCode;
                lbpeople.DataTextField = objReturnModel.dsPeopleCode.Tables[0].Columns[0].ToString();
                lbpeople.DataValueField = objReturnModel.dsPeopleCode.Tables[0].Columns[1].ToString();
                lbpeople.DataBind();

                //VSONI5 : MITS 25674 : 08/16/2011 - Start
                //Bind LOB Code List
                lbLOBCode.DataSource = objReturnModel.dsLOB;
                lbLOBCode.DataValueField = objReturnModel.dsLOB.Tables[0].Columns[0].ToString();
                lbLOBCode.DataTextField = objReturnModel.dsLOB.Tables[0].Columns[1].ToString();
                lbLOBCode.DataBind();
                //VSONI5 : MITS 25674 : 08/16/2011 - End

                //Manika : Start : Vsoni5 : 07/13/2011 : MITS 24924    
                // select the Default selected Entity Code and People tables while retrieving settings for editing a job.
                if (objReturnModel.OptionSetID <= 0)
                {
                    if (lbEntityCode.Items.FindByText(sHospital) != null)
                        lbEntityCode.Items.FindByText(sHospital).Selected = true;

                    if (lbEntityCode.Items.FindByText(sProvider) != null)
                        lbEntityCode.Items.FindByText(sProvider).Selected = true;

                    if (lbpeople.Items.FindByText(sOtherPeople) != null)
                        lbpeople.Items.FindByText(sOtherPeople).Selected = true;

                    if (lbpeople.Items.FindByText(sPhysician) != null)
                        lbpeople.Items.FindByText(sPhysician).Selected = true;
                }
                else
                {
                    // select the chosen Entity Code tables and People Tables while retrieving settings for editing a job.
                    sEntityTables = MBR_Setting["EntityTableIds"];
                    sEntityTablesArray = sEntityTables.Split(new char[] { ',' });
                    sECPeople = MBR_Setting["PeopleTableIds"];
                    sPeopleTablesArray = sECPeople.Split(new char[] { ',' });

                    for (int iIndex = 0; iIndex < sEntityTablesArray.Length; iIndex++)
                    {
                        if (lbEntityCode.Items.FindByValue(sEntityTablesArray[iIndex]) != null)
                            lbEntityCode.Items.FindByValue(sEntityTablesArray[iIndex]).Selected = true;
                    }

                    for (int iIndex = 0; iIndex < sPeopleTablesArray.Length; iIndex++)
                    {
                        if (lbpeople.Items.FindByValue(sPeopleTablesArray[iIndex]) != null)
                            lbpeople.Items.FindByValue(sPeopleTablesArray[iIndex]).Selected = true;
                    }
                }
                //MITS 24924 END
                //Entity Export Settings - End

                try
                {
                    //Npradeepshar 03/23/2011 This part of the code will never execute - START
                    #region Unreachable code
                    if (!MBR_Setting.ContainsKey("Execution_Mode"))
                    {
                        hTabName.Value = "importsetting";

                        if (MBR_Setting["ImportPrintedChecks"] == "1")
                            chkbxPrintedChecks.Checked = true;
                        else
                            chkbxPrintedChecks.Checked = false;
                        if (MBR_Setting["MBRPaymentsPrinted"] == "1")
                            chkbxPrintedMedical.Checked = true;
                        else
                            chkbxPrintedMedical.Checked = false;

                        if (MBR_Setting["MBRPayments2Printed"] == "1")

                            chkbxPrintedOther.Checked = true;
                        else
                            chkbxPrintedOther.Checked = false;

                        //vsoni5 : MITS 25051 : 'Generate Process File' and 'Generate Exception File' Checkboxes removed from UI.
                        // This functionality will be enabled by default.
                        //if (MBR_Setting["GenerateExceptionFile"] == "1")
                        //    chkbxException.Checked = true;
                        //else
                        //    chkbxException.Checked = false;


                        if (MBR_Setting["ProcessClosedClaims"] == "1")
                            chkbxAllowClosedClaims.Checked = true;
                        else
                            chkbxAllowClosedClaims.Checked = false;

                        if (MBR_Setting["EnclFlag"] == "1")
                            chkBxEnclFlag.Checked = true;
                        else
                            chkBxEnclFlag.Checked = false;

                        if (MBR_Setting["SearchbySSN"] == "1")
                            chkbxSSN.Checked = true;
                        else
                            chkbxSSN.Checked = false;

                        if (MBR_Setting["GetfirstPayee"] == "1")
                            chkbxFirstEntity.Checked = true;
                        else
                            chkbxFirstEntity.Checked = false;

                        if (MBR_Setting["DoNotAddPayee"] == "1")
                            chkbxNewPayee.Checked = true;
                        else
                            chkbxNewPayee.Checked = false;

                        if (MBR_Setting["FilterFrozenClaims"] == "1")
                            chkbxFrozenClaims.Checked = true;
                        else
                            chkbxFrozenClaims.Checked = false;

                        if (MBR_Setting["UseSuffixCode"] == "1")
                            chkbxSuffix.Checked = true;
                        else
                            chkbxSuffix.Checked = false;

                        if (MBR_Setting["CheckInsuffRes"] == "1")
                            chkbxInsuffReserve.Checked = true;
                        else
                            chkbxInsuffReserve.Checked = false;

                        if (MBR_Setting["CheckImportReason"] == "1")
                            chkbxImportReason.Checked = true;
                        else
                            chkbxImportReason.Checked = false;

                        if (MBR_Setting["UseImportPayeeInfo"] == "1")
                            chkbxImportPayee.Checked = true;
                        else
                            chkbxImportPayee.Checked = false;

                        if (MBR_Setting["UsePayeeZipLookup"] == "1")
                            chkbxZIP.Checked = true;
                        else
                            chkbxZIP.Checked = false;

                        if (MBR_Setting["DuplicatesInclVoids"] == "1")
                            chkbxVoid.Checked = true;
                        else
                            chkbxVoid.Checked = false;

                        if (MBR_Setting["DuplicatesUseSvcDts"] == "1")
                            chkbxTransDate.Checked = true;
                        else
                            chkbxTransDate.Checked = false;

                        tbIDMedical.Text = MBR_Setting["CorvelFeeIdentifier"];
                        tbIDOther.Text = MBR_Setting["CorvelFeeIdentifier2"];
                        try
                        {
                            ddlTTMedical.Items.FindByText(MBR_Setting["CorvelFeeTransType"]).Selected = true;
                        }
                        catch
                        {
                            ddlTTMedical.SelectedIndex = 0;
                        }
                        try
                        {
                            ddlTTOther.Items.FindByText(MBR_Setting["CorvelFeeTransType2"]).Selected = true;
                        }
                        catch
                        {
                            ddlTTOther.SelectedIndex = 0;
                        }
                        try
                        {
                            ddlPaymentMedical.Items.FindByText(MBR_Setting["PostDateType1"]).Selected = true;
                        }
                        catch
                        {
                            ddlPaymentMedical.SelectedIndex = 0;
                        }
                        try
                        {
                            ddlPaymentOther.Items.FindByText(MBR_Setting["PostDateType2"]).Selected = true;
                        }
                        catch
                        {
                            ddlPaymentOther.SelectedIndex = 0;
                        }
                        sJurisdictionState1 = MBR_Setting["JurisdictionStates1"];
                        sJurisdictionState2 = MBR_Setting["JurisdictionStates2"];
                        string[] sJurisState1 = sJurisdictionState1.Split(',');
                        string[] sJurisState2 = sJurisdictionState2.Split(',');
                        foreach (string str in sJurisState1)
                        {
                            if (!string.IsNullOrEmpty(str))
                                lbStateMedical.Items.Add(str);
                        }
                        foreach (string str in sJurisState2)
                        {
                            if (!string.IsNullOrEmpty(str))
                                lbStateOther.Items.Add(str);
                        }
                        //Vsoni5 : MITS 25624 : Import Document Location Removed.
                        //txtImportFile.Text = MBR_Setting["ImportDOCPath"];

                        //vchaturvedi2 MITS-20850 - Display Last Run date  
                        lblExportDate.Text = Conversion.GetDBDTTMFormat(MBR_Setting["ExportDateOnUI"], "d", "t");
                    }
                }
                catch
                {
                }
                    #endregion
                //Npradeepshar 03/23/2011 This part of the code will never execute -- END
                if (MBR_Setting.ContainsKey("Execution_Mode"))
                {
                    if (String.Compare(MBR_Setting["Execution_Mode"], "export", true) == 0)
                    {
                        hTabName.Value = "exportsetting";
                        if (objReturnModel.OptionSetName != null)
                        {
                            txtOptionSetNameExport.Text = objReturnModel.OptionSetName;
                            txtOptionSetNameExport.ReadOnly = true;
                        }
                        //Export Type Selection
                        try
                        {
                            rdbExportType.Items.FindByValue(MBR_Setting["ExportType"]).Selected = true;
                        }
                        catch
                        {
                            rdbExportType.SelectedIndex = 0;
                        }
                        //Claim Export Settings - Start
                        //Vsoni5 : MITS 24981 - Start
                        // lblExportDate - Label will show Claim Export Last Run Time
                        //tbExpOverrideDate - Set the From date in this field on UI.
                        lblExportDate.Text = Conversion.GetDBDTTMFormat(objReturnModel.sLastRuntime, "d", "t");
                        tbExpOverrideDate.Text = Conversion.GetDBDateFormat(MBR_Setting["ExportDateOnUI"], "d");
                        //Vsoni5 : MITS 24981 - End

                        //VSONI5 : MITS 25674 : 08/16/2011 - Start
                        chkbxRemoveSSN.Checked = MBR_Setting["RemoveSSN"] == "1" ? true : false;
                        if (objReturnModel.OptionSetID > 0)
                        {
                            sLOBCodes = MBR_Setting["LOBCodes"].ToString();
                            foreach (String sLOB in sLOBCodes.Split(','))
                            {
                                if (lbLOBCode.Items.FindByValue(sLOB) != null)
                                    lbLOBCode.Items.FindByValue(sLOB).Selected = true;
                            }
                        }
                        //VSONI5 : MITS 25674 : 08/16/2011 - End
                        //Claim Export Settings - End

                        //Vsoni5 : MITS 25250
                        //Entity Export Settings - Start
                        chkbxIncludeSuffix.Checked = MBR_Setting["IncludeSuffix"] == "1" ? true : false;
                        chkbxSourceForSuffix.Checked = MBR_Setting["UpdateSuffix"] == "1" ? true : false;
                        chkbxUpdateEntity.Checked = MBR_Setting["UpdAllSuffixes"] == "1" ? true : false;
                        //chkbxUpdateCodes.Checked = MBR_Setting["UpdateCodes"] == "1" ? true : false;
                        //Vsoni5 : MITS 25250 

                    }
                    else
                    {
                        hTabName.Value = "importsetting";
                        if (objReturnModel.OptionSetName != null)
                        {
                            txtOptionSetName.Text = objReturnModel.OptionSetName;
                            txtOptionSetName.ReadOnly = true;
                        }
                        // Import file type selection - Start 
                        if (MBR_Setting["FileType"] != string.Empty && MBR_Setting["FileType"] != null)
                        {
                            rbFileType.Items.FindByValue(MBR_Setting["FileType"]).Selected = true;
                        }
                        txtFileUpload.Text = MBR_Setting["SourceFile"] + MBR_Setting["PipeFileName"];
                        // Import file type selection - End

                        //Data Import Setting - Start 
                        chkbxAllowClosedClaims.Checked = MBR_Setting["ProcessClosedClaims"] == "1" ? true : false;
                        chkBxEnclFlag.Checked = MBR_Setting["EnclFlag"] == "1" ? true : false;
                        chkbxFrozenClaims.Checked = MBR_Setting["FilterFrozenClaims"] == "1" ? true : false;
                        chkbxImportReason.Checked = MBR_Setting["CheckImportReason"] == "1" ? true : false;
                        //npradeepshar MITS 24392 Add check box Import Medical Data
                        chkbxImportMedData.Checked = MBR_Setting["CheckImportMedData"] == "1" ? true : false;
                        chkbxInsuffReserve.Checked = MBR_Setting["CheckInsuffRes"] == "1" ? true : false;
                        chkbxSuffix.Checked = MBR_Setting["UseSuffixCode"] == "1" ? true : false;
                        chkbxPrintedChecks.Checked = MBR_Setting["ImportPrintedChecks"] == "1" ? true : false;
                        chkbxProvider.Checked = MBR_Setting["CheckImportProviderInvoiceNumber"] == "1" ? true : false;
                        chkbxQueuedPayment.Checked = MBR_Setting["QueuedPayments"] == "1" ? true : false;
                        //Data Import Setting - End

                        // Payee Search Criteria - Start
                        chkbxFirstEntity.Checked = MBR_Setting["GetfirstPayee"] == "1" ? true : false;
                        chkbxSSN.Checked = MBR_Setting["SearchbySSN"] == "1" ? true : false;
                        chkbxZIP.Checked = MBR_Setting["UsePayeeZipLookup"] == "1" ? true : false;
                        chkbxNewPayee.Checked = MBR_Setting["DoNotAddPayee"] == "1" ? true : false;
                        chkbxImportPayee.Checked = MBR_Setting["UseImportPayeeInfo"] == "1" ? true : false;
                        // Payee Search Criteria - End

                        //Payment Duplicate Search - Start
                        chkbxInvoice.Checked = MBR_Setting["ExcludeInvoice"] == "1" ? true : false;
                        chkbxVoid.Checked = MBR_Setting["DuplicatesInclVoids"] == "1" ? true : false;
                        chkbxTransDate.Checked = MBR_Setting["DuplicatesUseSvcDts"] == "1" ? true : false;
                        //Payment Duplicate Search - End

                        ///***********Fee Payment SetUp- Start************///
                        chkbxUseFeePayment.Checked = MBR_Setting["UseFeePayment"] == "1" ? true : false;

                        ///Vsoni5 : MITS 25216 : Fee Payment UI changes.
                        if (MBR_Setting["UseFeePayment"] == "1")
                        {
                            sJurisdictionState1 = MBR_Setting["JurisdictionStates1"];
                            sJurisdictionState2 = MBR_Setting["JurisdictionStates2"];
                            string[] sJurisState1 = sJurisdictionState1.Split(',');
                            string[] sJurisState2 = sJurisdictionState2.Split(',');
                            /******  Medical Reserve setup - Start   ******/
                            // Add selected saved medical reserver states to Medical Jurisdiction ListBox
                            foreach (string str in sJurisState1)
                            {
                                if (!string.IsNullOrEmpty(str))
                                    lbStateMedical.Items.Add(str);
                            }
                            try
                            {
                                ddlTTMedical.Items.FindByText(MBR_Setting["CorvelFeeTransType"]).Selected = true;
                            }
                            catch
                            {
                                ddlTTMedical.SelectedIndex = 0;
                            }
                            try
                            {
                                ddlPaymentMedical.Items.FindByText(MBR_Setting["PostDateType1"]).Selected = true;
                            }
                            catch
                            {
                                ddlPaymentMedical.SelectedIndex = 0;
                            }
                            tbIDMedical.Text = MBR_Setting["CorvelFeeIdentifier"];
                            chkbxPrintedMedical.Checked = MBR_Setting["MBRPaymentsPrinted"] == "1" ? true : false;
                            /******  Medical Reserve setup - End   ******/

                            /******  Other Reserve setup - Start   ******/
                            // Add selected saved Other Reserve states to Medical Jurisdiction ListBox
                            foreach (string str in sJurisState2)
                            {
                                if (!string.IsNullOrEmpty(str))
                                    lbStateOther.Items.Add(str);
                            }
                            try
                            {
                                ddlTTOther.Items.FindByText(MBR_Setting["CorvelFeeTransType2"]).Selected = true;
                            }
                            catch
                            {
                                ddlTTOther.SelectedIndex = 0;
                            }

                            try
                            {
                                ddlPaymentOther.Items.FindByText(MBR_Setting["PostDateType2"]).Selected = true;
                            }
                            catch
                            {
                                ddlPaymentOther.SelectedIndex = 0;
                            }
                            tbIDOther.Text = MBR_Setting["CorvelFeeIdentifier2"];
                            chkbxPrintedOther.Checked = MBR_Setting["MBRPayments2Printed"] == "1" ? true : false;
                            /******  Other Reserve setup - End   ******/
                        }
                        //Enable/Disable Fee Payment Panel Medical Reserves
                        if (lbStateMedical.Items.Count > 0)
                        {
                            trMedicalReserve.Disabled = false;
                            tbIDMedical.ReadOnly = false;
                        }
                        else
                        {
                            trMedicalReserve.Disabled = true;
                            tbIDMedical.ReadOnly = true;
                        }
                        //Enable/Disable Fee Payment Panel Other Reserves
                        if (lbStateOther.Items.Count > 0)
                        {
                            trOtherReserve.Disabled = false;
                            tbIDOther.ReadOnly = false;
                        }
                        else
                        {
                            trOtherReserve.Disabled = true;
                            tbIDOther.ReadOnly = true;
                        }
                        //End MITS 25216
                        ///***********Fee Payment SetUp - End************///
                        //Import Document Location
                        //Vsoni5 : MITS 25624 : Import Document Location Removed.
                        //txtImportFile.Text = MBR_Setting["ImportDOCPath"];

                        //Claim Export From Date setup
                        //Vsoni5 : MITS 25250 : Set Last Export date and Export type on initial load
                        //Vsoni5 : MITS 24981 - Start
                        // lblExportDate - Label will show Claim Export Last Run Time
                        //tbExpOverrideDate - Set the From date in this field on UI.
                        lblExportDate.Text = Conversion.GetDBDTTMFormat(objReturnModel.sLastRuntime, "d", "t");
                        tbExpOverrideDate.Text = Conversion.GetDBDateFormat(MBR_Setting["ExportDateOnUI"], "d");
                        
                        //Vsoni5 : MITS 24981 - End
                        try
                        {
                            rdbExportType.Items.FindByValue(MBR_Setting["ExportType"]).Selected = true;
                        }
                        catch
                        {
                            rdbExportType.SelectedIndex = 0;
                        }

                        //vsoni5 : MITS 25051 : 'Generate Process File' and 'Generate Exception File' Checkboxes removed from UI.
                        // This functionality will be enabled by default.
                        //if (MBR_Setting["UseProcessLog"] == "1")
                        //    chkbxProcessLog.Checked = true;
                        //else
                        //    chkbxProcessLog.Checked = false;

                        //vsoni5 : MITS 25051 : 'Generate Process File' and 'Generate Exception File' Checkboxes removed from UI.
                        // This functionality will be enabled by default.
                        //if (MBR_Setting["GenerateExceptionFile"] == "1")
                        //    chkbxException.Checked = true;
                        //else
                        //    chkbxException.Checked = false;
                    }
                }
                //vchaturvedi2 MITS - 20971 MBR UI issues
                else
                {
                    rbFileType.Items.FindByValue("pipe").Selected = true;
                    FUSourceFile.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                //vchaturvedi2 - MITS 20971 applying error control
                //formdemotitle.Text = "Error in retrieving setting.";
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add("DataIntagrator.Error", "Error in retrieving setting.", BusinessAdaptorErrorType.Message);
                err.Add("DataIntagrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
        }

        /// <summary>
        /// Vsoni5 : MITS 25670 : Restructured validations and code flow for logical grouping of functionality and criteria settings.
        /// </summary>
        private void SaveSettingsToDB()
        {
            //vchaturvedi2 - MITS 20971 applying error control
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            bool bErr = false; //
            bool isError = false;
            int iOptionSetID;
            string sAttachmentFileName = string.Empty;
            string sFileName = string.Empty;
            string sFileDir = string.Empty;
            string sImportFileName = string.Empty;
            string sAttachementFileName = string.Empty;
            string sECTable = "";
            string sECPeople = "";
            string sLOB = string.Empty;
            //ipuri REST Service Conversion 12/06/2014 Start
            //kkaur25-rest to soa start
            Riskmaster.UI.DataIntegratorService.DataIntegratorModel objModelS = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            DataIntegratorService.DataIntegratorServiceClient objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
            Riskmaster.Models.DataIntegratorModel objModel = new Riskmaster.Models.DataIntegratorModel();
            //kkaur25-rest to soa end
            //ipuri REST Service Conversion 12/06/2014 End
            try
            {
                string sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
                iOptionSetIDParam = Conversion.ConvertObjToInt(hdOptionsetId.Value);
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objModel.Token = AppHelper.GetSessionId();
                }

                Dictionary<string, string> MBR_Setting = new Dictionary<string, string>();
                #region MBR Export Settings
                if (hTabName.Value == "exportsetting")
                {
                    // vchaturvedi2 MITS : 20851 Export File Path Not Required 
                    //if (!string.IsNullOrEmpty(txtExportPath.Text))
                    //{
                    //   sFileName = txtExportPath.Text.Substring(txtExportPath.Text.LastIndexOf("\\") + 1);
                    //    sFileDir = txtExportPath.Text.Remove(txtExportPath.Text.LastIndexOf("\\") + 1);
                    //}
                    // MBR_Setting.Add("ExportFile", sFileName);
                    //vchaturvedi2 - MITS 20971 MBR UI issues - validating field
                    //Vsoni5 : 05/16/2011 : MITS 24984 : Validation applied on Export type drop down.
                    //Start
                    if (string.IsNullOrEmpty(txtOptionSetNameExport.Text))
                    {
                        err.Add("DataIntagrator.Error", "Optionset name can not be blank.", BusinessAdaptorErrorType.Message);
                        isError = true;
                        //return;
                    }
                    //Vsoni5 : MITS 25250 : Export type dropdown removed hense this block of code is not required
                    //if (ddlExportType.SelectedIndex == 0)
                    //{
                    //    err.Add("DataIntagrator.Error", "Export Type must be selected.", BusinessAdaptorErrorType.Message);                     
                    //    isError = true;
                    //}

                    if (isError == true)
                    {
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        return;
                    }
                    //End
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                    #region MBR Export Settings XML
                    // Rearranged the XML nodes to match the UI settings order.

                    MBR_Setting.Add("Execution_Mode", "Export");

                    // ********* Import Settings - Start : Set all the criteria to default as it is Export job  *********/    
                    // Import file type selection - Start                    
                    MBR_Setting.Add("FileType", String.Empty);
                    MBR_Setting.Add("PipeFileName", String.Empty);
                    MBR_Setting.Add("SourceFile", String.Empty);
                    // Import file type selection - End

                    //Data Import Setting - Start 
                    MBR_Setting.Add("ProcessClosedClaims", "0");
                    MBR_Setting.Add("EnclFlag", "0");
                    MBR_Setting.Add("FilterFrozenClaims", "0");
                    MBR_Setting.Add("CheckImportReason", "0");

                    //npradeepshar MITS 24392 add check box Import medical data
                    MBR_Setting.Add("CheckImportMedData", "0");
                    MBR_Setting.Add("CheckInsuffRes", "0");
                    MBR_Setting.Add("UseSuffixCode", "0");
                    MBR_Setting.Add("ImportPrintedChecks", "0");
                    MBR_Setting.Add("CheckImportProviderInvoiceNumber", "0");
                    MBR_Setting.Add("QueuedPayments", "0");
                    //Data Import Setting - End

                    // Payee Search Criteria - Start
                    MBR_Setting.Add("GetfirstPayee", "0");
                    MBR_Setting.Add("SearchbySSN", "0");
                    MBR_Setting.Add("UsePayeeZipLookup", "0");
                    MBR_Setting.Add("DoNotAddPayee", "0");
                    MBR_Setting.Add("UseImportPayeeInfo", "0");
                    // Payee Search Criteria - End

                    //Payment Duplicate Search - Start
                    MBR_Setting.Add("ExcludeInvoice", "0");
                    MBR_Setting.Add("DuplicatesInclVoids", "0");
                    MBR_Setting.Add("DuplicatesUseSvcDts", "0");
                    //Payment Duplicate Search - End

                    //Fee Payment SetUp- Start
                    // VSONI5 : MITS 25216 : Fee Payment Set up
                    MBR_Setting.Add("UseFeePayment", "0");
                    /******  Medical Reserve setup - Start   ******/
                    MBR_Setting.Add("JurisdictionStates1", String.Empty);
                    MBR_Setting.Add("CorvelFeeTransType", String.Empty);
                    MBR_Setting.Add("PostDateType1", String.Empty);
                    MBR_Setting.Add("CorvelFeeIdentifier", String.Empty);
                    MBR_Setting.Add("MBRPaymentsPrinted", "0");
                    /******  Medical Reserve setup - End   ******/

                    /******  Other Reserve setup - Start   ******/
                    MBR_Setting.Add("JurisdictionStates2", String.Empty);
                    MBR_Setting.Add("CorvelFeeTransType2", String.Empty);
                    MBR_Setting.Add("PostDateType2", String.Empty);
                    MBR_Setting.Add("CorvelFeeIdentifier2", String.Empty);
                    MBR_Setting.Add("MBRPayments2Printed", "0");
                    /******  Other Reserve setup - End   ******/

                    //Import Document Location
                    //Vsoni5 : MITS 25624 : Import Document Location Removed.
                    //MBR_Setting.Add("ImportDOCPath", String.Empty);

                    // TO-DO Options - Start
                    MBR_Setting.Add("ClearGrid", "0");//To-DO
                    MBR_Setting.Add("BillDocumentNumberCheck", "0");//To-DO                    
                    MBR_Setting.Add("AppendLog", "0");
                    MBR_Setting.Add("PrintProcessLog", "0");//To-DO
                    // TO-DO Options - End

                    //vsoni5 : MITS 25051 : 'Generate Process File' and 'Generate Exception File' Checkboxes removed from UI.
                    // This functionality will be enabled by default.
                    //if (chkbxException.Checked)
                    //    MBR_Setting.Add("GenerateExceptionFile", "1");
                    //else
                    //    MBR_Setting.Add("GenerateExceptionFile", "0");

                    //vsoni5 : MITS 25051 : 'Generate Process File' and 'Generate Exception File' Checkboxes removed from UI.
                    // This functionality will be enabled by default.
                    //if (chkbxProcessLog.Checked)
                    //    MBR_Setting.Add("UseProcessLog", "1");
                    //else
                    //    MBR_Setting.Add("UseProcessLog", "0");

                    // ********* Import Settings - End  *********/    

                    // ********* Export Settings - Start  *********/
                    //Export Type
                    MBR_Setting.Add("ExportType", rdbExportType.SelectedValue);

                    // Claim Export Settings - Start
                    //Vsoni5 : 08/30/2011 : MITS 24981 : Removed conditional block, as the Export Date on UI will be 
                    //populated from override export date field only.
                    MBR_Setting.Add("ExportDateOnUI", Conversion.GetDate(tbExpOverrideDate.Text));
                    
                    // VSONI5 : MITS 25674 : 08/16/2010 : Added Remove SSN and LOB restriction functionality
                    MBR_Setting.Add("RemoveSSN", chkbxRemoveSSN.Checked ? "1" : "0");
                    foreach (ListItem lobCode in lbLOBCode.Items)
                    {
                        if (lobCode.Selected)
                            if (sLOB.Equals(String.Empty))
                                sLOB += lobCode.Value.ToString();
                            else
                                sLOB += "," + lobCode.Value.ToString();
                    }
                    MBR_Setting.Add("LOBCodes", sLOB);
                    //VSONI5 : MITS 25674 - End
                    // Claim Export Settings - End

                    //Entity Export Settings - Start
                    MBR_Setting.Add("IncludeSuffix", chkbxIncludeSuffix.Checked ? "1" : "0");
                    MBR_Setting.Add("UpdateSuffix", chkbxSourceForSuffix.Checked ? "1" : "0");
                    //Vsoni5 : MITS 25250
                    //MBR_Setting.Add("UpdateCodes", chkbxUpdateCodes.Checked ? "1" : "0");
                    MBR_Setting.Add("UpdAllSuffixes", chkbxUpdateEntity.Checked ? "1" : "0");

                    //Manika : Start : Vsoni5 : 07/13/2011 : MITS 24924
                    if (String.Compare(rdbExportType.SelectedValue, "entity", true) == 0)
                    {
                        // Append select Entity Code Table Ids in a comma separated string
                        foreach (ListItem lstECCode in lbEntityCode.Items)
                        {
                            if (lstECCode.Selected)
                            {
                                if (!String.IsNullOrEmpty(sECTable))
                                    sECTable = sECTable + "," + lstECCode.Value.ToString();
                                else
                                    sECTable += lstECCode.Value.ToString();
                            }
                        }

                        // Append selected People tables Ids in a comma separated string.
                        foreach (ListItem lstPeopleCode in lbpeople.Items)
                        {
                            if (lstPeopleCode.Selected)
                            {
                                if (!String.IsNullOrEmpty(sECPeople))
                                    sECPeople = sECPeople + "," + lstPeopleCode.Value.ToString();
                                else
                                    sECPeople += lstPeopleCode.Value.ToString();
                            }
                        }

                        if (sECPeople == "" && sECTable == "")
                        {
                            err.Add("DataIntagrator.Error", "Atleast one Entity Code Table or People Table must be selected.", BusinessAdaptorErrorType.Message);
                            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                            return;

                        }
                    }
                    MBR_Setting.Add("EntityTableIds", sECTable);
                    MBR_Setting.Add("PeopleTableIds", sECPeople);
                    //Manika : Vsoni5 : 07/13/2011 : MITS 24924 end
                    /***** Entity Export Settings - End ********/
                    #endregion MBR Export Settings XML

                    objModel.OptionSetName = txtOptionSetNameExport.Text;
                    objModel.ImportFlage = false;
                }
                #endregion #region MBR Export Settings

                # region MBR Import Settings
                else if (hTabName.Value == "importsetting")
                {
                    //MBR_Setting.Add("ExportFile", ""); -- vchaturvedi2 MITS : 20851 Export File Path Not Required 
                    //vchaturvedi2 - MITS 20971 MBR UI issues 
                    if (string.IsNullOrEmpty(txtOptionSetName.Text))
                    {
                        err.Add("DataIntagrator.Error", "Optionset name can not be blank.", BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }
                    //--23900 npradeepshar  02/23/2011 Validation added
                    if (rbFileType.SelectedIndex == -1)
                    {
                        err.Add("DataIntagrator.Error", "Select file setting - Access/Pipe ", BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }
                    else
                    {
                        if (rbFileType.SelectedItem.Value == "pipe" && string.IsNullOrEmpty(txtFileUpload.Text))
                        {
                            err.Add("DataIntagrator.Error", "Select a text file path.", BusinessAdaptorErrorType.Message);
                            bErr = true;
                        }
                    }

                    /// Validation of Fee payment options, if Use ee Payment checkbox is checked
                    /// // VSONI5 : MITS 25216 : Fee Payment Set up - Start
                    if (chkbxUseFeePayment.Checked)
                    {

                        if (lbStateMedical.Items.Count == 0 && lbStateOther.Items.Count == 0)
                        {
                            err.Add("DataIntagrator.Error", "Select at least one jurisdiction for Medical/Other reserve type.", BusinessAdaptorErrorType.Message);
                            bErr = true;
                        }
                        else
                        {
                            if (lbStateMedical.Items.Count > 0)
                            {
                                if (ddlTTMedical.SelectedIndex <= 0)
                                {
                                    err.Add("DataIntagrator.Error", "Select transaction type for medical reserve.", BusinessAdaptorErrorType.Message);
                                    bErr = true;
                                }

                                if (ddlPaymentMedical.SelectedIndex <= 0)
                                {
                                    err.Add("DataIntagrator.Error", "Select payment date for medical reserve.", BusinessAdaptorErrorType.Message);
                                    bErr = true;
                                }
                                if (tbIDMedical.Text.Trim() == String.Empty)
                                {
                                    err.Add("DataIntagrator.Error", "Enter Corvel Fee Identifier for medical reserve", BusinessAdaptorErrorType.Message);
                                    bErr = true;
                                }
                            }
                            if (lbStateOther.Items.Count > 0)
                            {
                                if (ddlTTOther.SelectedIndex <= 0)
                                {
                                    err.Add("DataIntagrator.Error", "Select transaction type for other reserve.", BusinessAdaptorErrorType.Message);
                                    bErr = true;
                                }
                                if (ddlPaymentOther.SelectedIndex <= 0)
                                {
                                    err.Add("DataIntagrator.Error", "Select payment date for other reserve.", BusinessAdaptorErrorType.Message);
                                    bErr = true;
                                }
                                if (tbIDOther.Text.Trim().Equals(String.Empty))
                                {
                                    err.Add("DataIntagrator.Error", "Enter Corvel Fee Identifier for other reserve.", BusinessAdaptorErrorType.Message);
                                    bErr = true;
                                }
                            }
                        }
                        // VSONI5 : MITS 25216 : Fee Payment Set up - Start
                    }//23900 end

                    if (bErr == true)
                    {
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        if (iOptionSetIDParam <= 0)
                            txtFileUpload.Text = String.Empty;
                        return;
                    }
                    #region MBR Settings XML
                    // Rearranged the XML nodes to match the UI settings order.
                    // Import file type selection
                    MBR_Setting.Add("Execution_Mode", "Import");
                    MBR_Setting.Add("FileType", rbFileType.SelectedValue.ToString());
                    if (!string.IsNullOrEmpty(txtFileUpload.Text))
                    {
                        int iIndex = txtFileUpload.Text.LastIndexOf("\\");
                        sFileName = txtFileUpload.Text.Substring(iIndex + 1);
                        sFileDir = txtFileUpload.Text.Remove(iIndex + 1);
                    }
                    MBR_Setting.Add("PipeFileName", sFileName);
                    MBR_Setting.Add("SourceFile", sFileDir);


                    //Data Import Setting- Start 
                    MBR_Setting.Add("ProcessClosedClaims", chkbxAllowClosedClaims.Checked ? "1" : "0");
                    MBR_Setting.Add("EnclFlag", chkBxEnclFlag.Checked ? "1" : "0");
                    MBR_Setting.Add("FilterFrozenClaims", chkbxFrozenClaims.Checked ? "1" : "0");
                    MBR_Setting.Add("CheckImportReason", chkbxImportReason.Checked ? "1" : "0");

                    //npradeepshar MITS 24392 add check box Import medical data
                    MBR_Setting.Add("CheckImportMedData", chkbxImportMedData.Checked ? "1" : "0");
                    MBR_Setting.Add("CheckInsuffRes", chkbxInsuffReserve.Checked ? "1" : "0");
                    MBR_Setting.Add("UseSuffixCode", chkbxSuffix.Checked ? "1" : "0");
                    MBR_Setting.Add("ImportPrintedChecks", chkbxPrintedChecks.Checked ? "1" : "0");
                    MBR_Setting.Add("CheckImportProviderInvoiceNumber", chkbxProvider.Checked ? "1" : "0");
                    MBR_Setting.Add("QueuedPayments", chkbxQueuedPayment.Checked ? "1" : "0");
                    //Data Import Setting - End

                    // Payee Search Criteria - Start
                    MBR_Setting.Add("GetfirstPayee", chkbxFirstEntity.Checked ? "1" : "0");
                    MBR_Setting.Add("SearchbySSN", chkbxSSN.Checked ? "1" : "0");
                    MBR_Setting.Add("UsePayeeZipLookup", chkbxZIP.Checked ? "1" : "0");
                    MBR_Setting.Add("DoNotAddPayee", chkbxNewPayee.Checked ? "1" : "0");
                    MBR_Setting.Add("UseImportPayeeInfo", chkbxImportPayee.Checked ? "1" : "0");
                    // Payee Search Criteria - End

                    //Payment Duplicate Search - Start
                    MBR_Setting.Add("ExcludeInvoice", chkbxInvoice.Checked ? "1" : "0");
                    MBR_Setting.Add("DuplicatesInclVoids", chkbxVoid.Checked ? "1" : "0");
                    MBR_Setting.Add("DuplicatesUseSvcDts", chkbxTransDate.Checked ? "1" : "0");
                    //Payment Duplicate Search - End

                    //Fee Payment SetUp- Start
                    // VSONI5 : MITS 25216 : Fee Payment Set up - Start
                    MBR_Setting.Add("UseFeePayment", chkbxUseFeePayment.Checked ? "1" : "0");
                    if (chkbxUseFeePayment.Checked)
                    {
                        /******  Medical Reserve setup - Start   ******/
                        //- Append selected Medical Jurisdiction state short codes in a comma seperated string.                    
                        foreach (ListItem lst in lbStateMedical.Items)
                        {
                            if (sJurisdictionState1 == null)
                                sJurisdictionState1 = lst.Text;
                            else
                                sJurisdictionState1 = sJurisdictionState1 + ',' + lst;
                        }

                        MBR_Setting.Add("JurisdictionStates1", sJurisdictionState1);
                        MBR_Setting.Add("CorvelFeeTransType", ddlTTMedical.SelectedItem.ToString());
                        MBR_Setting.Add("PostDateType1", ddlPaymentMedical.SelectedItem.ToString());
                        MBR_Setting.Add("CorvelFeeIdentifier", tbIDMedical.Text);
                        MBR_Setting.Add("MBRPaymentsPrinted", chkbxPrintedMedical.Checked ? "1" : "0");
                        /******  Medical Reserve setup - End   ******/

                        /******  Other Reserve setup - Start   ******/
                        //- Append selected Other Jurisdiction state short codes in a comma seperated string.                    
                        foreach (ListItem lst in lbStateOther.Items)
                        {
                            if (sJurisdictionState2 == null)
                                sJurisdictionState2 = lst.Text;
                            else
                                sJurisdictionState2 = sJurisdictionState2 + ',' + lst;
                        }

                        MBR_Setting.Add("JurisdictionStates2", sJurisdictionState2);
                        MBR_Setting.Add("CorvelFeeTransType2", ddlTTOther.SelectedItem.ToString());
                        MBR_Setting.Add("PostDateType2", ddlPaymentOther.SelectedItem.ToString());
                        MBR_Setting.Add("CorvelFeeIdentifier2", tbIDOther.Text);
                        MBR_Setting.Add("MBRPayments2Printed", chkbxPrintedOther.Checked ? "1" : "0");
                        /******   Other Reserve setup - End   ******/
                    }
                    else
                    {   // Set all the Fee payment options to default
                        /******  Medical Reserve setup - Start   ******/
                        MBR_Setting.Add("JurisdictionStates1", String.Empty);
                        MBR_Setting.Add("CorvelFeeTransType", String.Empty);
                        MBR_Setting.Add("PostDateType1", String.Empty);
                        MBR_Setting.Add("CorvelFeeIdentifier", String.Empty);
                        MBR_Setting.Add("MBRPaymentsPrinted", "0");
                        /******  Medical Reserve setup - End   ******/

                        /******  Other Reserve setup - Start   ******/
                        MBR_Setting.Add("JurisdictionStates2", String.Empty);
                        MBR_Setting.Add("CorvelFeeTransType2", String.Empty);
                        MBR_Setting.Add("PostDateType2", String.Empty);
                        MBR_Setting.Add("CorvelFeeIdentifier2", String.Empty);
                        MBR_Setting.Add("MBRPayments2Printed", "0");
                        /******  Other Reserve setup - End   ******/
                    }
                    // VSONI5 : MITS 25216 : Fee Payment Set up - End
                    //Import Document Location
                    //Vsoni5 : MITS 25624 : Import Document Location Removed.
                    //MBR_Setting.Add("ImportDOCPath", txtImportFile.Text);

                    // TO-DO Options - Start
                    MBR_Setting.Add("ClearGrid", "0");//To-DO
                    MBR_Setting.Add("BillDocumentNumberCheck", "0");//To-DO                    
                    MBR_Setting.Add("AppendLog", "0");
                    MBR_Setting.Add("PrintProcessLog", "0");//To-DO
                    // TO-DO Options - End

                    //vsoni5 : MITS 25051 : 'Generate Process File' and 'Generate Exception File' Checkboxes removed from UI.
                    // This functionality will be enabled by default.
                    //if (chkbxException.Checked)
                    //    MBR_Setting.Add("GenerateExceptionFile", "1");
                    //else
                    //    MBR_Setting.Add("GenerateExceptionFile", "0");

                    //vsoni5 : MITS 25051 : 'Generate Process File' and 'Generate Exception File' Checkboxes removed from UI.
                    // This functionality will be enabled by default.
                    //if (chkbxProcessLog.Checked)
                    //    MBR_Setting.Add("UseProcessLog", "1");
                    //else
                    //    MBR_Setting.Add("UseProcessLog", "0");

                    // ********* Export Settings - Start : Export criteria will be set to default as it is Import Job *********/
                    //Export Type
                    MBR_Setting.Add("ExportType", rdbExportType.SelectedValue);
                    // Claim Export Settings - Start
                    MBR_Setting.Add("ExportDateOnUI", String.Empty);
                    //VSONI5 : MITS 25674 : 08/16/2011 - Start
                    MBR_Setting.Add("RemoveSSN", "0");
                    MBR_Setting.Add("LOBCodes", String.Empty);
                    //VSONI5 : MITS 25674 : 08/16/2011 - End
                    // Claim Export Settings - End

                    //Entity Export Settings - Start
                    MBR_Setting.Add("IncludeSuffix", "0");
                    MBR_Setting.Add("UpdateSuffix", "0");
                    //MBR_Setting.Add("UpdateCodes", "0");
                    MBR_Setting.Add("UpdAllSuffixes", "0");
                    //Manika : Start : Vsoni5 : 07/13/2011 : MITS 24924
                    MBR_Setting.Add("EntityTableIds", String.Empty);
                    MBR_Setting.Add("PeopleTableIds", String.Empty);
                    //END
                    //Entity Export Settings - End
                    #endregion MBR Settings XML

                    //MBR_Setting.Add("FileDirectory", ""); -- vchaturvedi2 MITS : 20851 Export File Path Not Required 
                    //Vsoni5 : MITS 25250                   

                    objModel.OptionSetName = txtOptionSetName.Text;
                    if (FUSourceFile.HasFile)
                        sImportFileName = FUSourceFile.FileName;
                    //vchaturvedi2 - MITS 21420 - file not uploading from disk drives
                    //sImportFileName = Filetoupload(FUSourceFile.PostedFile.FileName);                         
                    //else if (!string.IsNullOrEmpty(txtFileUpload.Text)) 
                    // sImportFileName = Filetoupload(txtFileUpload.Text);
                    
                    //Vsoni5 : MITS 25624 : Import Document Location Removed.
                    //if (FUAttachment.HasFile)
                    //    sAttachementFileName = FUAttachment.FileName; //vchaturvedi2 - MITS 21420
                    
                    //sAttachementFileName = Filetoupload(FUAttachment.PostedFile.FileName);
                    // else if(!string.IsNullOrEmpty(txtImportFile.Text))
                    // sAttachementFileName = Filetoupload(txtImportFile.Text);
                    objModel.ImportFlage = true;
                }
                #endregion MBR Import Settings

                objModel.Parms = MBR_Setting;
                objModel.OptionSetID = iOptionSetIDParam;
                objModel.ModuleName = sModuleName;

                objModel.TaskManagerXml = sTaskManagerXml;
                //Vsoni5 : MITS 25119 : Import Access MDB file.
                //set the value of bUpdateFlag just in case of editing the job.
                if (objModel.OptionSetID > 0)
                {
                    objModel.bUpdateFlag = true;
                }
                //ipuri REST Service Conversion 12/06/2014 Start
                //objModel = objService.SaveSettings(objModel);
                objModel.ClientId = AppHelper.ClientId;

                objModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                //ipuri REST Service Conversion 12/06/2014 End
                if (objModel.OptionSetID == -1)
                {
                    // formdemotitle.Text = "Option set name already exists.";
                    //vchaturvedi2 - MITS 20971 applying error control 
                    err.Add("DataIntagrator.Error", "Option set name already exists.", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    txtFileUpload.Text = String.Empty;
                    return;
                    //err.Add("DataIntagrator.Error", "OptionSet Name is all ready used.", BusinessAdaptorErrorType.Message);
                }

                else if (objModel.ImportFlage == true)
                {
                    //vchaturvedi2 -MITS 20923 file upload not working
                    if (!string.IsNullOrEmpty(sImportFileName) && FUSourceFile.HasFile)
                    {
                        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                        //kkaur25 -REST to soa start
                        Riskmaster.Models.DAImportFile objDAImportFile = new Riskmaster.Models.DAImportFile();
                        Riskmaster.UI.DataIntegratorService.DAImportFile objDAImportFileS = new Riskmaster.UI.DataIntegratorService.DAImportFile();
                        //kkaur25 -REST to soa end
                        objDAImportFile.Token = AppHelper.GetSessionId();
                        Stream data = FUSourceFile.PostedFile.InputStream;
                        objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                        objDAImportFile.fileName = sImportFileName + "." + objModel.OptionSetID;
                        objDAImportFile.filePath = objModel.FilePath;
                        // Neha RMACLOUD-9816 - MBR Compatible with Cloud
                        // We need ModuleName, DocumentType and optionsetId while retrieving the Import Files from DB
                        // So saving these attributes while uploading the Import Files to DB
                        objDAImportFile.ModuleName = "MBR";
                        objDAImportFile.DocumentType = "Import";
                        objDAImportFile.OptionsetId = objModel.OptionSetID; //end
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                        objDAImportFile.ClientId = AppHelper.ClientId;


                        //kkaur25-SOA change start
                        objDAImportFileS.FileContents = objDAImportFile.FileContents;
                        objDAImportFileS.fileName = objDAImportFile.fileName;
                        objDAImportFileS.filePath = objModel.FilePath;
                        objDAImportFileS.Token = AppHelper.Token;
                        objDAImportFileS.ClientId = AppHelper.ClientId;
                        objDAImportFileS.ModuleName = "MBR";
                        objDAImportFileS.DocumentType = "Import";
                        objDAImportFileS.OptionsetId = objModel.OptionSetID;
                        //Call SOAP/XML service
                        objService.UploadDAImportFile(objDAImportFileS);
                        //kkaur25 SOA end


                        //AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                        //ipuri REST Service Conversion 12/06/2014 End
                        //Vsoni : End of MITS 22699.

                        //Vsoni5 : MITS 25119 : Import Access MDB file.
                        //Upload the mdb file on business layer and transfer the data into 
                        //HEADER AND LINEITEMS table of MS_ACCESS_STAGING database
                        objModel.sDAStagingDBImportFile = objModel.FilePath + sImportFileName + "." + objModel.OptionSetID;
                        if (String.Compare(rbFileType.SelectedValue, "access", true) == 0)
                        {
                            //ipuri REST Service Conversion 12/06/2014 Start
                            //objService.PeekDAStagingDatabase(objModel);
                            objModel.ClientId = AppHelper.ClientId;

                            //kkaur25 start UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder
                            if (objModel.ClientId > 0)
                            {
                                AppHelper.GetResponse("RMService/DAIntegration/RetrieveFile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                            }
                            //kkaur25 end UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder
                            AppHelper.GetResponse("RMService/DAIntegration/peekdastagingdatabase", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);

                            AppHelper.GetResponse("RMService/DAIntegration/updatedastagingdatabase", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                        
                            //objService.UpdateDAStagingDatabase(objModel);
                            //ipuri REST Service Conversion 12/06/2014 End
                        }
                    }
                    
                    //Vsoni5 : MITS 25624 : Import Document Location Removed.
                    //if (!string.IsNullOrEmpty(sAttachementFileName) && FUAttachment.HasFile)
                    //{
                    //    sAttachementFileName = sAttachementFileName + "." + objModel.OptionSetID;
                    //    FUAttachment.PostedFile.SaveAs(objModel.FilePath + sAttachementFileName);
                    //}//
                    
                    //Commented as of now - 04/06
                    // sAttachmentFileName = Filetoupload(objModel.OptionSetID);
                }
                if (objModel.OptionSetID > 0)
                {//npradeepshar MITS 23900 Added false to stop error logging
                   //Neha RMACLOUD-9816 - MBR Compatible with Cloud
                    UploadAttachments("MBR", objModel.OptionSetID, "Attachment");
                    //KKAUR25 soa change start
                    Riskmaster.Models.DataIntegratorModel objDIModelS = new Riskmaster.Models.DataIntegratorModel();
                    UploadAttachments("MBR", objDIModelS.OptionSetID, "Attachment");
                    ////KKAUR25 soa change end 
                    //Neha RMACLOUD-9816 - MBR Compatible with Cloud
                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
                }

            }
            catch (Exception Ex)
            {
                //vchaturvedi2 - MITS 20971 applying error control - validating field
                //formdemotitle.Text = "Error in saving Optionset";
                err.Add("DataIntagrator.Error", "Error in saving Optionset", BusinessAdaptorErrorType.Message);
                err.Add("DataIntagrator.Error", Ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                /// Vsoni5 : 01/29/2011 : MITS 23365
                try
                {
                    if (iOptionSetIDParam <= 0 && objModel.OptionSetID > 0)
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objService.SaveSettingsCleanup(objModel);
                        objModel.ClientId = AppHelper.ClientId;
                    AppHelper.GetResponse("RMService/DAIntegration/savesettingscleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                    //ipuri REST Service Conversion 12/06/2014 End
                }
                catch (Exception eCleanup)
                {
                }

                return;//
            }

        }
        // Neha RMACLOUD-9816 - MBR Compatible with Cloud
        // If this function is required by other modules of DA then it should be moved to some common file.
        /// <summary>
        /// In Case of cloud the user will not have access to copy and paste the files on server.
        /// So providing a mechanism so that user can browse the files on his/her machine and upload to server.
        /// Once the files are uploaded to server, we can save them in DB
        /// </summary>
        /// <param name="p_sModuleName">Name of the Module. Can be ISO, MMSEA, etc</param>
        /// <param name="p_iOptionsetId">The files are associated with which OptionsetId</param>
        /// <param name="p_sDocType">Document Type Import or Attcahment</param>
        private void UploadAttachments(string p_sModuleName, int p_iOptionsetId, string p_sDocType)
        {
            if (AppHelper.ClientId > 0)
            {
                int iUploadedFiles = 0;
                Riskmaster.Models.DAImportFile oDAImportFile = null;
                try
                {
                    lbSuccess.Visible = false;
                    if (UploadDocumentAttachments.Items.Count > 0)
                    {
                        oDAImportFile = new Riskmaster.Models.DAImportFile();
                        oDAImportFile.Token = AppHelper.GetSessionId();
                        try
                        {
                            foreach (AttachmentItem item in UploadDocumentAttachments.Items)
                            {
                                Stream data = item.OpenStream();

                                oDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                                oDAImportFile.fileName = item.FileName + "." + p_iOptionsetId;
                                oDAImportFile.ClientId = AppHelper.ClientId;
                                oDAImportFile.ModuleName = p_sModuleName;
                                oDAImportFile.OptionsetId = p_iOptionsetId;
                                oDAImportFile.DocumentType = p_sDocType;
                                AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDAImportFile);
                                if (!string.IsNullOrEmpty(filename.Value))
                                {
                                    filename.Value += ",";
                                }
                                filename.Value += item.FileName;

                                iUploadedFiles++;
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                        finally
                        {
                            oDAImportFile = null;
                        }
                        if (iUploadedFiles == UploadDocumentAttachments.Items.Count)
                        {
                            lbSuccess.Visible = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
            else
                return;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveSettingsToDB();
            ///Commneted this block of code as its causing forced selection of import file in case of Job Edit.
            ////vchaturvedi2 - MITS 20923 File upload not working
            //if (ErrorControl1.errorFlag == true)
            //{
            //    txtFileUpload.Text = "";
            //        //if (rbFileType.SelectedItem.Value == "pipe") 
            //                 //FUSourceFile.Enabled = true;
            //        //else FUSourceFile.Enabled = false;

            //}
        }

        //Vsoni5 : MITS 25119 : Import Access MDB file.
        // This function is not required hense commented
        ///// <summary>
        ///// MITS 23900 npradeepshar 02/23/2011 added event to the checkbox.
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void rbFileType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (rbFileType.SelectedItem.Value == "access")
        //    {
        //        FUSourceFile.Enabled = false;
        //        txtFileUpload.Enabled = false;
        //        txtFileUpload.Text = "";
        //    }
        //    else if (rbFileType.SelectedItem.Value == "pipe")
        //    {
        //        FUSourceFile.Enabled = true;
        //        txtFileUpload.Enabled = true;
        //    }

        //}


        /// <summary>
        /// VSONI5 : MITS 25216 
        /// This function will sort the Medical/Other Reserve Jurisdiction alphabetically.
        /// </summary>
        /// <param name="lbSortList"></param>
        private void SortListBox(ListBox lbSortList)
        {

            ArrayList alListItems = new ArrayList();
            foreach (ListItem lstItem in lbSortList.Items)
            {
                alListItems.Add(lstItem.Text);
            }
            alListItems.Sort();

            lbSortList.Items.Clear();

            for (int iIndex = 0; iIndex < alListItems.Count; iIndex++)
            {
                lbSortList.Items.Add(alListItems[iIndex].ToString());
            }
        }

        /// <summary>
        /// VSONI5 : MITS 25216 : Fee Payment Set Up
        /// This function will Remove the selected jurisdictions from jurisdiction list and add them to Jurisdiction Medical Reserve listbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddMedResState_Click(object sender, EventArgs e)
        {
            bool bAddResState = false;
            for (int iIndex = 0; iIndex < lbStatesAll.Items.Count; iIndex++)
            {
                if (lbStatesAll.Items[iIndex].Selected)
                {
                    if (lbStateMedical.Items.FindByText(lbStatesAll.Items[iIndex].Text) == null)
                    {
                        lbStateMedical.Items.Add(lbStatesAll.Items[iIndex]);
                        lbStatesAll.Items.Remove(lbStatesAll.Items[iIndex]);
                        iIndex--;
                        bAddResState = true;
                    }
                }
            }
            lbStateMedical.ClearSelection();
            lbStatesAll.ClearSelection();

            if (bAddResState)
                SortListBox(lbStateMedical);

            if (lbStateMedical.Items.Count > 0)
            {
                trMedicalReserve.Disabled = false;
                tbIDMedical.ReadOnly = false;
            }
        }

        /// <summary>
        /// /// VSONI5 : MITS 25216 : Fee Payment Set Up
        /// This function will Remove the selected jurisdictions from Jurisdiction Medical Reserve listbox and add them to jurisdiction list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelMedResState_Click(object sender, EventArgs e)
        {
            bool bAddResState = false;
            for (int iIndex = 0; iIndex < lbStateMedical.Items.Count; iIndex++)
            {
                if (lbStateMedical.Items[iIndex].Selected)
                {
                    lbStatesAll.Items.Add(lbStateMedical.Items[iIndex]);
                    lbStateMedical.Items.Remove(lbStateMedical.Items[iIndex]);
                    iIndex--;
                    bAddResState = true;
                }
            }
            lbStatesAll.ClearSelection();
            lbStateMedical.ClearSelection();

            if (lbStateMedical.Items.Count == 0)
            {
                trMedicalReserve.Disabled = true;
                ddlPaymentMedical.ClearSelection();
                ddlTTMedical.ClearSelection();
                tbIDMedical.Text = String.Empty;
                tbIDMedical.ReadOnly = true;
                chkbxPrintedMedical.Checked = false;
            }
            SortListBox(lbStatesAll);
        }

        /// <summary>
        /// VSONI5 : MITS 25216 : Fee Payment Set Up
        /// This function will Remove the selected jurisdictions from jurisdiction list and add them to Jurisdiction Other Reserve listbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddOtherResState_Click(object sender, EventArgs e)
        {
            bool bAddResState = false;
            for (int iIndex = 0; iIndex < lbStatesAll.Items.Count; iIndex++)
            {
                if (lbStatesAll.Items[iIndex].Selected)
                {
                    if (lbStateOther.Items.FindByText(lbStatesAll.Items[iIndex].Text) == null)
                    {
                        lbStateOther.Items.Add(lbStatesAll.Items[iIndex]);
                        lbStatesAll.Items.Remove(lbStatesAll.Items[iIndex]);
                        iIndex--;
                        bAddResState = true;
                    }
                }
            }
            lbStateOther.ClearSelection();
            lbStatesAll.ClearSelection();
            SortListBox(lbStateOther);

            if (lbStateOther.Items.Count > 0)
            {
                trOtherReserve.Disabled = false;
                tbIDOther.ReadOnly = false;
            }
        }

        /// <summary>
        /// /// VSONI5 : MITS 25216 : Fee Payment Set Up
        /// This function will Remove the selected jurisdictions from Jurisdiction Other Reserve listbox and add them to jurisdiction list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelOtherResState_Click(object sender, EventArgs e)
        {
            bool bAddResState = false;
            for (int iIndex = 0; iIndex < lbStateOther.Items.Count; iIndex++)
            {
                if (lbStateOther.Items[iIndex].Selected)
                {
                    lbStatesAll.Items.Add(lbStateOther.Items[iIndex]);
                    lbStateOther.Items.Remove(lbStateOther.Items[iIndex]);
                    iIndex--;
                    bAddResState = true;
                }
            }
            lbStatesAll.ClearSelection();
            lbStateOther.ClearSelection();
            if (lbStateOther.Items.Count == 0)
            {
                trOtherReserve.Disabled = true;
                ddlPaymentOther.ClearSelection();
                ddlTTOther.ClearSelection();
                tbIDOther.Text = String.Empty;
                tbIDOther.ReadOnly = true;
                chkbxPrintedOther.Checked = false;
            }
            SortListBox(lbStatesAll);
        }
    }
}
