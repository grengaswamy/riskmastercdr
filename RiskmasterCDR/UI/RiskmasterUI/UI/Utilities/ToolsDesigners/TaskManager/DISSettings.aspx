<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DISSettings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.DISSettings" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../../../../Content/dhtml-div.css" rel="stylesheet" type="text/css" />
   
       <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script src="../../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/utilities.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>

    <script src="../../../../Scripts/TMSettings.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

    <script type="text/javascript">

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                window.alert("Please enter numeric values only!");
                return false;
                
            }
            return true;
        }
        </script>
    
    </head>
<body>
    <form id="frmData" name="frmData" method="post" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td colspan="2">
              <uc1:ErrorControl ID="ErrorControl1" runat="server" />
              
            </td>
        <tr height="10">
            <td>
            </td>
        </tr>
    </table>
    <div>
    <asp:scriptmanager id="ScriptManager1" runat="server" AsyncPostBackTimeout="120"/>            
        <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="TextBox1" />
        <div class="msgheader" id="div_formtitle" runat="server">
            <asp:Label ID="Label1" runat="server" Text="DIS Optionset" />
            <asp:Label ID="formsubtitle" runat="server" Text="" />
        </div>
        <asp:Table ID="Table1" runat = "server" >
           <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="lbl_OptionsetName" runat="server" Text="Optionset Name" Font-Bold="true" Font-Underline="true"/>
                &nbsp;&nbsp;
                <asp:TextBox ID="txtOptionName" runat="server"></asp:TextBox>
            </asp:TableCell>
           </asp:TableRow>
           <asp:TableRow>
            <asp:TableCell>
            <asp:UpdatePanel ID="VerificationPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>

            <asp:CheckBox ID="Verification_Flag" runat="server"  Text="Verify the data before import"/>
             
             </ContentTemplate>
                </asp:UpdatePanel>
            </asp:TableCell>
           </asp:TableRow>

           <asp:TableRow>

            <asp:TableCell>                    
             <asp:CheckBox ID="chk2GBData" runat="server" Text="Select Heavy Access Database to Import From" AutoPostBack="true" OnCheckedChanged ="chk2GBData_CheckedChanged"></asp:CheckBox>           
                &nbsp;
                <asp:Label ID="lblBatchID" runat="server" Text="Batch ID" Font-Bold="true" Font-Underline="true" visible="false" />
                &nbsp;
                <asp:TextBox ID="txtBatchID" runat="server" visible="false" onkeypress="return isNumber(event)"></asp:TextBox>
            </asp:TableCell>
           </asp:TableRow>

               
           <%--<asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="lblBatchID" runat="server" Text="Batch ID" Font-Bold="true" Font-Underline="true" visible="true" />
                &nbsp;
                <asp:TextBox ID="txtBatchID" runat="server" visible="true" onkeypress="return isNumber(event)"></asp:TextBox>
            </asp:TableCell>
           </asp:TableRow>--%>
     
            
          <%-- <asp:TableRow><asp:TableCell></asp:TableCell></asp:TableRow>
           <asp:TableRow><asp:TableCell></asp:TableCell></asp:TableRow>--%>
           <asp:TableRow>
           <asp:TableCell>            
            <asp:Label ID="lbl_SelAccessDb" runat="server" Text="Select Access Database to Import From:" Font-Bold="true" Font-Underline="true"/>
            &nbsp;
            <asp:FileUpload ID="DBPath" runat="server"/> 
           </asp:TableCell>
           </asp:TableRow>
           <asp:TableRow><asp:TableCell></asp:TableCell></asp:TableRow>
           <asp:TableRow><asp:TableCell></asp:TableCell></asp:TableRow>
        </asp:Table>
        
        <asp:Table ID="ImportedAreas" runat = "server">
        <asp:TableHeaderRow>
            <asp:TableCell  Font-Bold ="true" ColumnSpan ="2">
                Select the areas you wish to Import
            </asp:TableCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:RadioButton ID="Employees_Flag" runat="server" GroupName="ModulesGroup"  Text="Employees" AutoPostBack="true" OnCheckedChanged ="Employees_Flag_CheckedChanged"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="OrgHier_Flag" runat="server" GroupName="ModulesGroup" Text ="Organization Hierarchy" AutoPostBack="true" OnCheckedChanged="OrgHier_Flag_CheckedChanged" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="Funds_Flag" runat="server" GroupName="ModulesGroup" Text = "Funds" AutoPostBack="true" OnCheckedChanged="Funds_Flag_CheckedChanged"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="FundsDeposit_Flag" runat="server" GroupName="ModulesGroup" Text = "Funds Deposit" AutoPostBack="true" OnCheckedChanged="FundsDeposit_Flag_CheckedChanged"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="Vehicles_Flag" runat="server" GroupName="ModulesGroup" Text = "Vehicles" AutoPostBack="true" OnCheckedChanged="Vehicles_Flag_CheckedChanged"/>
            </asp:TableCell>
             <asp:TableCell>
                <asp:RadioButton ID="Entities_Flag" runat="server" GroupName="ModulesGroup" Text = "Entities" AutoPostBack="true" OnCheckedChanged="Entities_Flag_CheckedChanged"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="Policies_Flag" runat="server" GroupName="ModulesGroup" Text = "Policies" AutoPostBack="true" OnCheckedChanged="Policies_Flag_CheckedChanged"/>
            </asp:TableCell>
       </asp:TableRow>
       <asp:TableRow>
            <asp:TableCell>
                <asp:RadioButton ID="Reserves_Flag" runat="server" GroupName="ModulesGroup" Text = "Reserves" AutoPostBack="true" OnCheckedChanged="Reserves_Flag_CheckedChanged"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="OrgExpo_Flag" runat="server" GroupName="ModulesGroup" Text = "Organization Exposure" AutoPostBack="true" OnCheckedChanged="OrgExpo_Flag_CheckedChanged"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="Patients_Flag" runat="server" GroupName="ModulesGroup" Text = "Patients" AutoPostBack="true" OnCheckedChanged="Patients_Flag_CheckedChanged"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="Physicians_Flag" runat="server" GroupName="ModulesGroup" Text = "Physicians" AutoPostBack="true" OnCheckedChanged="Physicians_Flag_CheckedChanged"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="MedicalStaff_Flag" runat="server" GroupName="ModulesGroup" Text = "Medical Staff" AutoPostBack="true" OnCheckedChanged="MedicalStaff_Flag_CheckedChanged"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="AdminTrack_Flag" runat="server" GroupName="ModulesGroup" Text = "Administrative Tracking" AutoPostBack="true" OnCheckedChanged="AdminTrack_Flag_CheckedChanged"/>
            </asp:TableCell>
       </asp:TableRow>
     </asp:Table>
        
 <asp:Table ID="Table3" runat = "server" Width="100%" >
     
      <asp:TableRow BackColor = "#C5D2E5">
        <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
         Employees
        </asp:TableCell>
      </asp:TableRow>
            
      <asp:TableRow>
         <asp:TableCell>
         <asp:UpdatePanel ID="EmployeeSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>
            <asp:Table ID="EmployeeSettings" runat="server">
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Employees_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Employees_Update_Blank_Zero" runat="server" Text = "Update Even if Blank or Zero"/>
            </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Employees_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Employees_Update_Address_Blank_Zero" runat="server" Text = "Update Address Even if Blank or Zero"/>
            </asp:TableCell>
            </asp:TableRow>
                                    
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Employees_Match_Employee_ID" runat="server" Text = "Match Only on Employee ID" AutoPostBack="true" OnCheckedChanged ="Employees_Match_Employee_ID_CheckedChanged"/>
            </asp:TableCell>
            </asp:TableRow>
                       
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Employees_Match_Entity_Id" runat="server" Text = "Match by Entity ID" AutoPostBack="true" OnCheckedChanged ="Employees_Match_Entity_Id_CheckedChanged"/>
            </asp:TableCell>
            </asp:TableRow> 
            
            </asp:Table>
        </ContentTemplate>
        </asp:UpdatePanel>
        </asp:TableCell>
      </asp:TableRow> 
    
      <asp:TableRow BackColor = "#C5D2E5">
        <asp:TableCell Font-Bold ="true"  ForeColor = "white" Width ="30%">
        Organization Hierarchy
        </asp:TableCell>
      </asp:TableRow>
              
      <asp:TableRow>
        <asp:TableCell>
        <asp:UpdatePanel ID="OrgHierSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>
            <asp:Table runat="server" ID="OrgHierSettings">
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="OrgHier_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="OrgHier_Update_Blank_Zero" runat="server" Text = "Update Even if Blank or Zero"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="OrgHier_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
        </asp:UpdatePanel>    
        </asp:TableCell>
      </asp:TableRow>
    
      <asp:TableRow BackColor = "#C5D2E5">
        <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
        Funds
        </asp:TableCell>
      </asp:TableRow>
            
      <asp:TableRow>
        <asp:TableCell>
        <asp:UpdatePanel ID="FundsSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>
            <asp:Table runat="server" ID="FundsSettings">
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Funds_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Funds_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
                        
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Funds_Open_Claims_Only" runat="server" Text = "Import Funds for Open Claims Only"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Funds_Check_Duplicate_Payments" runat="server" Text = "Check Duplicate Payments"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Funds_Accept_Duplicate_Check_Number" runat="server" Text = "Accept Duplicate Check Numbers"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Funds_Import_Cleared_Payments" runat="server" Text = "Import Cleared Payments too"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Funds_Match_Payee_Name" runat="server" Text = "Match Payee by Name" AutoPostBack="true" OnCheckedChanged="Funds_Match_Payee_Name_CheckedChanged"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Funds_Match_Payee_Tax_Id" runat="server" Text = "Match Payee by Tax ID" AutoPostBack="true" OnCheckedChanged="Funds_Match_Payee_Tax_Id_CheckedChanged"/>
            </asp:TableCell>
            </asp:TableRow>
                        
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Funds_Match_Entity_Id" runat="server" Text = "Match by Entity ID" AutoPostBack="true" OnCheckedChanged="Funds_Match_Entity_Id_CheckedChanged"/>
            </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Funds_Import_Multiple_Payees" runat="server" Text = "Import Multiple Payees"/>
            </asp:TableCell>
            </asp:TableRow>

           </asp:Table>
           </ContentTemplate>
           </asp:UpdatePanel>
       </asp:TableCell>
    </asp:TableRow>       
     
    <asp:TableRow BackColor = "#C5D2E5">
        <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
        Funds Deposit
        </asp:TableCell>
    </asp:TableRow>
            
    <asp:TableRow>
        <asp:TableCell>
        <asp:UpdatePanel ID="FundsDepositsSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>        
            <asp:Table runat="server" ID="FundsDepositsSettings">
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="FundsDeposit_Update_Blank_Zero" runat="server" Text = "Update Even if Blank or Zero"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="FundsDeposit_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
           </asp:Table>
           </ContentTemplate>
           </asp:UpdatePanel>
        </asp:TableCell>
     </asp:TableRow>  
     
     <asp:TableRow BackColor = "#C5D2E5">
         <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
         Vehicles
         </asp:TableCell>
     </asp:TableRow>
            
     <asp:TableRow>
         <asp:TableCell>
        <asp:UpdatePanel ID="VehiclesSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>         
            <asp:Table runat="server" ID="VehiclesSettings">
            <asp:TableRow>
            <asp:TableCell>           
            <asp:CheckBox ID="Vehicles_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Vehicles_Update_Blank_Zero" runat="server" Text = "Update Even if Blank or Zero"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Vehicles_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
           </asp:Table>
           </ContentTemplate>
           </asp:UpdatePanel>
        </asp:TableCell>
     </asp:TableRow>  
     
     <asp:TableRow BackColor = "#C5D2E5">
        <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
        Entities
        </asp:TableCell>
     </asp:TableRow>
            
     <asp:TableRow>
        <asp:TableCell>
        <asp:UpdatePanel ID="EntitiesSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>       
            <asp:Table runat="server" ID="EntitiesSettings">
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Entities_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Entities_Update_Blank_Zero" runat="server" Text = "Update Even if Blank or Zero"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Entities_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
              
              <%--<asp:TableRow>      csingh7 06/10/2010 Defect # 11 : Added : Start--%>          
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Entities_Match_By_TaxId" runat="server" Text = "Match By Tax ID"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Entities_Match_By_Name" runat="server" Text = "Match By Name"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Entities_Match_By_Abbrev" runat="server" Text = "Match By Abbrev"/>
            </asp:TableCell>
            </asp:TableRow>
            <%--<asp:TableRow>      csingh7 06/10/2010 Defect # 11 : Added : End--%>  
            
            <%--<asp:TableRow>      csingh7 06/10/2010 Defect # 11 : commented
            <asp:TableCell>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>Match By: </asp:TableCell>
                        
                        <asp:TableCell>
                        <asp:CheckBox ID="Entities_Match_By_TaxId" runat="server" Text = "Tax ID"/>
                        </asp:TableCell>
                        
                        <asp:TableCell>
                        <asp:CheckBox ID="Entities_Match_By_Name" runat="server" Text = "Name"/>
                        </asp:TableCell>
                        
                        <asp:TableCell>
                        <asp:CheckBox ID="Entities_Match_By_Abbrev" runat="server" Text = "Abbrev"/>
                        </asp:TableCell>  
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            </asp:TableRow>--%>
            </asp:Table>
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:TableCell>
     </asp:TableRow>  
     
     <asp:TableRow BackColor = "#C5D2E5">
        <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
        Policies
        </asp:TableCell>
     </asp:TableRow>
            
     <asp:TableRow>
        <asp:TableCell>
        <asp:UpdatePanel ID="PoliciesSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>        
            <asp:Table runat="server" ID="PoliciesSettings">
            <asp:TableRow>
            <asp:TableCell>            
            <asp:CheckBox ID="Policies_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Policies_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
          </ContentTemplate>
          </asp:UpdatePanel>
        </asp:TableCell>
     </asp:TableRow>

     <%--START :  mkaur24 : MITS 33475,33476, 33477  : DIS UI changes in sync with Desktop DIS and Carrier Settings Functionality.
                       --%>  
      <asp:TableRow>
        <asp:TableCell>                   
         <asp:UpdatePanel ID="PoliciesCarrierSettingsPanel" runat="server" UpdateMode="Conditional">   
               <ContentTemplate>  
                   <asp:Table runat="server" ID="PoliciesCarrierSettings">  
                        <asp:TableRow>
                        <asp:TableCell>            
                        <asp:CheckBox ID="Policies_Insured_EntityID" runat="server" Text = "Match Insured by Entity ID" AutoPostBack="true"  OnCheckedChanged="Policies_Insured_EntityID_CheckedChanged"/> 
                         </asp:TableCell>
                         </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell>
                        <asp:Label ID ="lbl_Policies_Insured_EntityID" runat="server" Text="Match Insured by: "/>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell>
                        <asp:CheckBox ID="Policies_Match_Insured_TaxID" runat="server"  Text ="TaxID"/>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:CheckBox ID="Policies_Match_Insured_Name" runat="server" style="padding-right:100px" Text = "Name"/>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:CheckBox ID="Policies_Match_Insured_Abbrev" runat="server" Text = "Abbrev"/>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell>            
                        <asp:CheckBox ID="Policies_Insurer_EntityID" runat="server" Text = "Match Insurer by Entity ID"  AutoPostBack="true" OnCheckedChanged="Policies_Insurer_EntityID_CheckedChanged"/> 
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell>
                        <asp:Label ID ="lbl_Policies_Insurer_EntityID" runat="server" Text="Match Insurer by: "/>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell>
                        <asp:CheckBox ID="Policies_Match_Insurer_TaxID" runat="server"  Text ="TaxID"/>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:CheckBox ID="Policies_Match_Insurer_Name" runat="server" style="padding-right:100px"  Text = "Name"/>   
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:CheckBox ID="Policies_Match_Insurer_Abbrev" runat="server"   Text = "Abbrev"/>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell>
                        <asp:CheckBox ID="Policies_Insured_Claimant" runat="server" Text = "Create Insured Claimant"/>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell>
                        <asp:CheckBox ID="Policies_Import_Additional_Interest" runat="server"  Text = "Import Additional Interest"  AutoPostBack="true" OnCheckedChanged="Policies_Import_Additional_Interest_CheckedChanged"/> <%-- Added by mkaur24 : 16/05/2014 :  MITS 33475,33476, 33477 --%>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell>
                         <asp:Label ID ="lbl_Policies_Import_Additional_Interest" runat="server" Text=" Match Additional Interests by:  " Visible="false" /> <%-- Added by mkaur24 : 16/05/2014 :  MITS 33475,33476, 33477 --%>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell>
                       <asp:CheckBox ID="Policies_Import_Additional_Interest_TaxID" runat="server" Text = "TaxID" Visible="false"/>  <%-- Added by mkaur24 : 16/05/2014 :  MITS 33475,33476, 33477 --%>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:CheckBox ID="Policies_Import_Additional_Interest_Name" runat="server" Text = "Name" Visible="false"/>  <%-- Added by mkaur24 : 16/05/2014 :  MITS 33475,33476, 33477 --%>
                        </asp:TableCell>
                        </asp:TableRow>
                       </asp:Table>
                   </ContentTemplate>
              </asp:UpdatePanel>                      
                 </asp:TableCell>
         </asp:TableRow>  
                <%-- END :  mkaur24 : MITS 33475 , 33476 , 33478  : DIS UI changes in sync with Desktop DIS and Carrier Settings Functionality.
                       --%>
     
     
     <asp:TableRow BackColor = "#C5D2E5">
         <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
         Reserves
         </asp:TableCell>
     </asp:TableRow>
            
     <asp:TableRow>
        <asp:TableCell>
        <asp:UpdatePanel ID="ReservesSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>        
            <asp:Table runat="server" ID="ReservesSettings">
<%--            <asp:TableRow>
            <asp:TableCell>   csingh7 : Commented for MITS 21268         
            <asp:CheckBox ID="Reserves_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>--%>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Reserves_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Reserves_Check_Duplicate_Reserves" runat="server" Text = "Check Duplicate Reserves"/>
            </asp:TableCell>
            </asp:TableRow>
           </asp:Table>
           </ContentTemplate>
           </asp:UpdatePanel>
        </asp:TableCell>
     </asp:TableRow>  
     
     <asp:TableRow BackColor = "#C5D2E5">
        <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
        Organization Exposure
        </asp:TableCell>
     </asp:TableRow>
            
     <asp:TableRow>
        <asp:TableCell>
        <asp:UpdatePanel ID="OrgExpoSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>        
            <asp:Table  runat="server" ID="OrgExpoSettings">
            <asp:TableRow>
            <asp:TableCell>            
            <asp:CheckBox ID="OrgExpo_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="OrgExpo_Update_Blank_Zero" runat="server" Text = "Update Even if Blank or Zero"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="OrgExpo_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="OrgExpo_New_Records_Only" runat="server" Text = "Assume New Records Only(No Update)"/>
            </asp:TableCell>
            </asp:TableRow>            
            
           </asp:Table>
           </ContentTemplate>
           </asp:UpdatePanel>
        </asp:TableCell>
     </asp:TableRow>  
     
     <asp:TableRow BackColor = "#C5D2E5">
         <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
         Patients
         </asp:TableCell>
     </asp:TableRow>
            
     <asp:TableRow>
        <asp:TableCell>
        <asp:UpdatePanel ID="PatientsSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>        
            <asp:Table runat="server" ID="PatientsSettings">
            <asp:TableRow>
            <asp:TableCell>            
            <asp:CheckBox ID="Patients_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>
             
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Patients_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
                        
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Patients_Validate_Data_Only" runat="server" Text = "Validate Data Only (No Upload)"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Patients_Match_Patient_Number" runat="server" Text = "Match Patient/Physician Number Only"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="Table2" runat="server">
                    <asp:TableRow>
                        <asp:TableCell> 
                        <asp:RadioButton ID="AcctNo" Checked="true" GroupName="Patients_Match_Patient" runat="server" Text="Match Patient Account Number" />
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:RadioButton ID="MedRcdNo" GroupName="Patients_Match_Patient" runat="server" Text="Medical Record Number" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            </asp:TableRow>
            
           </asp:Table>
           </ContentTemplate>
           </asp:UpdatePanel>
        </asp:TableCell>
     </asp:TableRow>  
     
     <asp:TableRow BackColor = "#C5D2E5">
         <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
         Physicians
         </asp:TableCell>
     </asp:TableRow>
            
     <asp:TableRow>
        <asp:TableCell>
        <asp:UpdatePanel ID="PhysiciansSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>        
            <asp:Table runat="server" ID="PhysiciansSettings">
            <asp:TableRow>
            <asp:TableCell>            
            <asp:CheckBox ID="Physicians_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Physicians_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
                        
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Physicians_Validate_Data" runat="server" Text = "Validate Data Only (No Upload)"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="Physicians_Match_Physicians_Number" runat="server" Text = "Match Physician Number Only"/>
            </asp:TableCell>
            </asp:TableRow>
  
           </asp:Table>
           </ContentTemplate>
           </asp:UpdatePanel>
        </asp:TableCell>
     </asp:TableRow>  
     
     <asp:TableRow BackColor = "#C5D2E5">
        <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
        Medical Staff
        </asp:TableCell>
     </asp:TableRow>
            
     <asp:TableRow>
        <asp:TableCell>
        <asp:UpdatePanel ID="MedicalStaffSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>        
            <asp:Table runat="server" ID="MedicalStaffSettings">
            <asp:TableRow>
            <asp:TableCell>            
            <asp:CheckBox ID="MedicalStaff_Import_Supp" runat="server" Text = "Import Supplemental Data"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="MedicalStaff_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="MedicalStaff_Validate_Data" runat="server" Text = "Validate Data Only (No Upload)"/>
            </asp:TableCell>
            </asp:TableRow>
                        
           </asp:Table>
           </ContentTemplate>
           </asp:UpdatePanel>
        </asp:TableCell>
     </asp:TableRow>  
     
     <asp:TableRow BackColor = "#C5D2E5">
        <asp:TableCell Font-Bold ="true"  ForeColor = "white"  Width ="30%" >
        Administrative Tracking
        </asp:TableCell>
     </asp:TableRow>
           
     <asp:TableRow>
         <asp:TableCell>
        <asp:UpdatePanel ID="AdminTrackSettingsPanel" runat="server" UpdateMode="Conditional">
         <ContentTemplate>         
            <asp:Table runat="server" ID="AdminTrackSettings">
                        
            <asp:TableRow>
            <asp:TableCell>
            <asp:CheckBox ID="AdminTrack_Create_New_Codes" runat="server" Text = "Allow Creation of New Codes"/>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
                <asp:table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>Select Admin Tracking Table: </asp:TableCell>
                        <asp:TableCell>
                        <asp:DropDownList ID="AdminTrack_Area" runat="server" AutoPostBack="true" OnSelectedIndexChanged="AdminTrack_Area_SelectionChanged"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:table>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                        <asp:RadioButton ID="Add" Checked="true" GroupName="AdminTrack_Add_Update" runat="server" Text="Always Add" AutoPostBack="true" OnCheckedChanged = "AdminTrack_Add_Update_CheckedChanged"/>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:RadioButton ID="Add_Update" GroupName="AdminTrack_Add_Update" runat="server" Text="Add or Update" AutoPostBack="true" OnCheckedChanged = "AdminTrack_Add_Update_CheckedChanged" onclick="alert('Please select unique field/column name from Match Field Dropdown')" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            </asp:TableRow>
            
            <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="AdminTrack_FieldSettings" runat="server">
                    <asp:TableRow>
                        <asp:TableCell>Match Field: </asp:TableCell>
                        <asp:TableCell>
                        <asp:DropDownList ID="AdminTrack_Match_Field" runat="server"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            </asp:TableRow>

           </asp:Table>
           </ContentTemplate>
           </asp:UpdatePanel>
        </asp:TableCell>
     </asp:TableRow>  
 </asp:Table>
         
 </div>
 <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />
 <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click" />
    
 <asp:HiddenField ID="hdTaskManagerXml" runat="server" />
 <asp:HiddenField ID="hdOptionsetId" runat="server" />
 </form>

</body>
</html>


