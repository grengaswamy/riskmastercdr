﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TMScheduledView.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.TMScheduledView" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc2" TagName="UserControlGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register Src="../../../Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Scheduled Tasks</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js"></script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/TMSettings.js"></script>

</head>
<body onload="self.setTimeout('RefreshTaskList()',60000);parent.MDIScreenLoaded();" >
    <form id="frmData" runat="server" title="<%$ Resources:lblViewScheduledTasks %>">
    <table border="0">
    	<tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <table border="0">
                    <tr>
                        <td class="ctrlgroup" colspan="2">
                        <%--YUkti-ML Changes Start
                            Task List--%>
                            <asp:Label runat="server" ID="lblTaskList" Text="<%$ Resources:lblTaskList %>" />
                        </td>
                    </tr>
                    <tr>
                        <td width="780px">
                            <b></b>
                            <table width="100%">
                                <tr>
                                    <td width="80%">
                                        <uc2:UserControlGrid runat="server" ID="TaskListGrid" GridName="TaskListGrid" GridTitle="" AllowSorting="true"
                                            Target="Document/Document/TaskInfoList" Ref="" Unique_Id="scheduleid" ShowRadioButton="true"
                                            Height="180px" Width="780px" HideNodes="|scheduletypeId|scheduleid|" ShowHeader="True" LinkType="Link"
                                            PopupWidth="290px" PopupHeight="350px" Type="GridAndButtons" HideButtons="New, Edit, Clone" TextColumn="scheduletypeId" ImgDeleteToolTip="<%$ Resources:ttDelete %>"/>  <!-- ksahu5 - ML Changes - MITS 33897 !-->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                     <tr height="10">
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="12">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2">
                                    <%--Yukti-Ml Changes Start
                                        <asp:Button ID="btnRefresh" Text="Refresh" Width="100px" class="button" OnClientClick="RefreshTaskList();"--%>
                                        <asp:Button ID="btnRefresh" Text="<%$ Resources:btnRefresh %>" Width="100px" class="button" OnClientClick="RefreshTaskList();"
                                            runat="server" />
                                        <%--Yukti-ML Changes End--%>
                                    </td>
                                    <td colspan="2">
                                    <%--Yukti-Ml Changes Start
                                        <asp:Button ID="btnAddSchedule" Text="Schedule A New Task" Width="150px" class="button"
                                            OnClientClick="AddSchedule();return false;" runat="server" />--%>
                                    <asp:Button ID="btnAddSchedule" Text="<%$ Resources:btnAddSchedule %>" Width="150px" class="button"
                                            OnClientClick="AddSchedule();return false;" runat="server" />
                                    <%--Yukti-ML Changes End--%>
                                    </td>
                                    <td>
                                        <asp:TextBox value="" Style="display: none" ID="hdnaction" runat="server" />
                                    </td>
                                    <td>
                                        <asp:TextBox value="" Style="display: none" ID="hdnSelectedRow" runat="server" />
                                    </td>     
                                    <td>
                                        <asp:TextBox value="" Style="display: none" ID="hdnRefresh" runat="server" />
                                    </td>                            
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
