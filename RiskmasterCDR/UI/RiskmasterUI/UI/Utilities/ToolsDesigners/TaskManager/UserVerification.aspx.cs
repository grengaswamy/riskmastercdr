﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class UserVerification : NonFDMBasePageCWS
    {
        private XmlDocument Model = null;
        private string InputXMLTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>UserVerificationAdaptor.GetVerificationTable</Function></Call><Document><UserVerification><ImportArea /><TableName /><DataOption /><TableData /><JobId></JobId><RowId /><StopVerify /><ModuleName /></UserVerification></Document></Message>";
        private string ImportXMLTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>UserVerificationAdaptor.GetImportArea</Function></Call><Document><UserVerification><ImportArea /><TableName /><DataOption /><TableData /><JobId></JobId><RowId /><ModuleName /></UserVerification></Document></Message>";
        private List<KeyValue> ColumnNames = null;
        private string m_sJobId = string.Empty;
        private string sErrorolumns = string.Empty;
        private string m_sModuleName = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            //string test = "funtion calltest(){ parent.MDIShowScreen('~/UI/Utilities/ToolsDesigners/TaskManager/TMView.aspx');}";
            //ClientScript.RegisterClientScriptBlock(this.GetType(), "test", test, true);
                
            m_sJobId = Request.QueryString["jobid"];
            m_sModuleName = Request.QueryString["modulename"];
            //m_sJobId = "9";
            //m_sModuleName = "DIS";
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            if (!Page.IsPostBack)
            {
                InputXMLTemplate = AppHelper.ChangeMessageValue(InputXMLTemplate, "//ModuleName", m_sModuleName);
                InputXMLTemplate = AppHelper.ChangeMessageValue(InputXMLTemplate, "//JobId", m_sJobId);
                string sReturn = AppHelper.CallCWSService(InputXMLTemplate);
                Model = new XmlDocument();
                Model.LoadXml(sReturn);
                 XmlElement XElem = (XmlElement)Model.SelectSingleNode("//MsgStatusCd");
                 if (XElem != null && XElem.InnerText == "Error")
                 {
                     XmlElement xMessage = (XmlElement)Model.SelectSingleNode("//ExtendedStatusDesc");
                     err.Add("UserVerification.Error", xMessage.InnerText, BusinessAdaptorErrorType.Message);
                     ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                 }
                 else
                 {
                     BindImportArea();
                     BindData();
                     BindDatatoGrid(GetTableDataTemplate(), null);
                 }
            }
        }

        /// <summary>
        /// Author - Iti Puri
        /// Date - 09 May 2014
        /// This function is called to bind the Import Area with the drop down.
        /// </summary>
        private void BindImportArea()
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            try
            {
                ImportXMLTemplate = AppHelper.ChangeMessageValue(ImportXMLTemplate, "//ModuleName", m_sModuleName);
                ImportXMLTemplate = AppHelper.ChangeMessageValue(ImportXMLTemplate, "//JobId", m_sJobId);
                string sIAReturn = AppHelper.CallCWSService(ImportXMLTemplate);
                Model = new XmlDocument();
                Model.LoadXml(sIAReturn);
                XmlElement ImportArea = (XmlElement)Model.SelectSingleNode("//Document/ImportArea");
                XmlDocument Impdoc = new XmlDocument();
                Impdoc.LoadXml(ImportArea.OuterXml);
                DataSet Impdataset = ConvertXmlDocToDataSet(Impdoc);
                if (Impdataset != null && Impdataset.Tables != null & Impdataset.Tables.Count > 0)
                {
                    lstImportArea.DataSource = Impdataset;
                    lstImportArea.DataTextField = Impdataset.Tables[0].Columns[0].ToString();
                    lstImportArea.DataValueField = Impdataset.Tables[0].Columns[0].ToString();
                    lstImportArea.DataBind();
                }
            }
            catch (Exception eRM)
            {
                err.Add("UserVerification.Error", eRM.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }


        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is called to bind the Table Name with the drop down.
        /// </summary>
        private void BindData()
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            try
            {
                if (lstImportArea.SelectedItem != null)
                {
                    string sImportArea = lstImportArea.SelectedItem.Text;
                    InputXMLTemplate = AppHelper.ChangeMessageValue(InputXMLTemplate, "//ImportArea", sImportArea);
                    InputXMLTemplate = AppHelper.ChangeMessageValue(InputXMLTemplate, "//JobId", m_sJobId);                    
                    string sReturn = AppHelper.CallCWSService(InputXMLTemplate);
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    XmlElement UserTable = (XmlElement)Model.SelectSingleNode("//Document/Table/TableData/NewDataSet/Table");
                    XmlDocument doc = new XmlDocument();
                    //doc.LoadXml(UserTable.OuterXml);
                    //XmlNode x = doc.SelectSingleNode("TableName");
                    doc.LoadXml(UserTable.InnerXml);
                    XmlNodeList xl = doc.GetElementsByTagName("TABLE_NAME");
                    string s = xl.Item(0).InnerText;
                    string[] sa = s.Split(',');
                    lstVerificationTable.Items.Clear();
                    for (int i = 0; i < sa.Length; i++)
                    {
                        lstVerificationTable.Items.Add(sa[i]);
                        lstVerificationTable.DataBind();
                    }

                    //   foreach(XmlNode n in xl)
                    //   {
                    //       lstVerificationTable.Items.Add(n.ToString());
                    //lstVerificationTable.DataBind();
                    //   }
                    //XmlNodeList xl = doc.SelectNodes("TABLE_NAME");
                    //for(int xlcnt=0;xlcnt<=xl.Count;xlcnt++)
                    //{
                    //    lstVerificationTable.Items.Add(xl[xlcnt].ToString());
                    //    lstVerificationTable.DataBind();
                    //}
                    //DataSet dataset = ConvertXmlDocToDataSet(doc);
                    //if (dataset != null && dataset.Tables != null & dataset.Tables.Count > 0)
                    //{
                    //    lstVerificationTable.DataSource = dataset;
                    //    lstVerificationTable.DataTextField = dataset.Tables[0].Columns[0].ToString();
                    //    lstVerificationTable.DataValueField = dataset.Tables[0].Columns[0].ToString();
                    //    lstVerificationTable.DataBind();
                    //}
                    //lstVerificationTable.Items.Add("Employee");
                    //lstVerificationTable.DataBind();
                }
                else
                {
                    lstImportArea.Items.Add("--Select--");
                    lstImportArea.DataBind();
                    lstVerificationTable.Items.Add("--Select--");
                    lstVerificationTable.DataBind();
                }
            }
            catch (Exception eRM)
            {
                err.Add("UserVerification.Error", eRM.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is called when we change the radio button 
        /// View All Data
        /// Edit Failed Rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RbtnViewData_CheckedChanged(object sender, EventArgs e)
        {
            gvJobData.PageIndex = 0;
            gvJobData.EditIndex = -1;
            txtSummary.Text = string.Empty;
            //BindImportArea();
            BindDatatoGrid(GetTableDataTemplate(), null);
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is called when we change the value in the drop down list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lstVerificationTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvJobData.PageIndex = 0;
            gvJobData.EditIndex = -1;
            txtSummary.Text = string.Empty;
            BindDatatoGrid(GetTableDataTemplate(), null);
        }

        protected void lstImportArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvJobData.PageIndex = 0;
            gvJobData.EditIndex = -1;
            txtSummary.Text = string.Empty;
            //BindDatatoGrid(GetTableDataTemplate(), null);
            BindData();
            RbtnViewData.Checked = true;
            RbtnViewError.Checked = false;
            BindDatatoGrid(GetTableDataTemplate(), null);
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is called to modified the XML template
        /// </summary>
        /// <returns></returns>
        private string GetTableDataTemplate()
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            try
            {
                string template = InputXMLTemplate;
                if (lstImportArea.SelectedItem != null)
                {
                    string sImportArea = lstImportArea.SelectedItem.Text;
                    string sTableName = lstVerificationTable.SelectedItem.Text;
                    template = AppHelper.ChangeMessageValue(template, "//ImportArea", sImportArea);
                    template = AppHelper.ChangeMessageValue(template, "//TableName", sTableName);
                    template = AppHelper.ChangeMessageValue(template, "//JobId", m_sJobId);
                    template = AppHelper.ChangeMessageValue(template, "//ModuleName", m_sModuleName);
                    template = AppHelper.ChangeMessageValue(template, "//DataOption", RbtnViewData.Checked ? "V" : "E");
                    return template;
                }
                string sdef="--Select--";
                return sdef;
            }
            catch (Exception eRM)
            {
                err.Add("UserVerification.Error", eRM.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return null;
            }

        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is called to get the data from the database and bind it with grid
        /// </summary>
        /// <param name="template"></param>
        /// <param name="p_sSortExpression"></param>
        private void BindDatatoGrid(string template, string p_sSortExpression)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            try
            {
                if (template != "--Select--")
                {
                    template = AppHelper.ChangeMessageValue(template, "//Function", "UserVerificationAdaptor.GetTableData");
                    string sReturn = AppHelper.CallCWSService(template);
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    XmlElement XElem = (XmlElement)Model.SelectSingleNode("//MsgStatusCd");
                    if (XElem != null && XElem.InnerText == "Error")
                    {
                        XmlElement xMessage = (XmlElement)Model.SelectSingleNode("//ExtendedStatusDesc");
                        err.Add("UserVerification.Error", xMessage.InnerText, BusinessAdaptorErrorType.Message);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }
                    else
                    {
                        XmlElement UserTable = (XmlElement)Model.SelectSingleNode("//Document");
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(UserTable.OuterXml);
                        DataSet dataset = ConvertXmlDocToDataSet(doc);
                        if (dataset != null && dataset.Tables != null & dataset.Tables.Count > 0)
                        {
                            DataView view = new DataView(dataset.Tables[0]);
                            if (!string.IsNullOrEmpty(p_sSortExpression))
                            {
                                view.Sort = p_sSortExpression;
                            }
                            else
                            {
                                view = dataset.Tables[0].DefaultView;
                            }
                            gvJobData.DataSource = view;
                            gvJobData.DataBind();
                            //hide columns start
                            //gvJobData.Columns[0].Visible = false;
                            //foreach (GridViewRow gvr in gvJobData.Rows)
                            //{
                            //    gvr.Cells[1].Visible = false;
                            //}
                            if (RbtnViewError.Checked)
                            {
                                lblMessage.Text = "Error Rows in the Table : " + gvJobData.Rows.Count;
                            }
                            else
                            {
                                lblMessage.Text = "Total number of records in the Table: " + gvJobData.Rows.Count;
                            }
                        }
                    }
                }
            }
            catch (Exception eRM)
            {
                err.Add("UserVerification.Error", eRM.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is called when we change the page index of the Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvJobData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvJobData.PageIndex = e.NewPageIndex;
            BindDatatoGrid(GetTableDataTemplate(), !string.IsNullOrEmpty(GridSortExpression.Text) ? GridSortExpression.Text + " " + GridSortDirection.Text : "");
            txtSummary.Text = string.Empty;
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is called when update the row in the Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvJobData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int RowId = e.RowIndex;
            BusinessAdaptorErrors err = null;
            try
            {
                err = new BusinessAdaptorErrors();
                GridViewRow row = gvJobData.Rows[RowId];
                string sRowId = string.Empty;
                
                XmlDocument doc = new XmlDocument();
                XmlElement Data = doc.CreateElement(lstVerificationTable.SelectedItem.Text);
                doc.AppendChild(Data);
                if (ViewState["ColumnNames"] != null)
                {
                    ColumnNames = (List<KeyValue>)ViewState["ColumnNames"];
                }
                if (ColumnNames != null && ColumnNames.Count > 0)
                {
                    for (int i = 1; i < row.Cells.Count; i++)
                    {
                        XmlElement elem = doc.CreateElement("ColumnData");
                        KeyValue column = ColumnNames[i - 1];

                        XmlAttribute attr1 = doc.CreateAttribute("Name");
                        attr1.Value = column.Key;
                        elem.Attributes.Append(attr1);

                        XmlAttribute attr3 = doc.CreateAttribute("DataType");
                        attr3.Value = column.Value;
                        elem.Attributes.Append(attr3);

                        XmlAttribute attr2 = doc.CreateAttribute("Value");
                        attr2.Value = row.Cells[i].Controls[0].ClientID;
                        TextBox text = (TextBox)row.Cells[i].Controls[0];
                        if (text != null)
                        {
                            //if (column.Value == "System.Int32")
                            if (column.Value == "System.Int32" || (column.Value == "System.Int16")) //ipuri
                            {
                                attr2.Value = !string.IsNullOrEmpty(text.Text) ? text.Text : "0";
                            }
                                //ipuri start
                            else if (column.Value == "System.Double") //ipuri
                            {
                                attr2.Value = !string.IsNullOrEmpty(text.Text) ? text.Text : "0.0";
                            }
                                //ipuri end
                            else
                            {
                                attr2.Value = text.Text;
                            }
                            int iResult;
                            if (column.Value == "System.Int32" && !string.IsNullOrEmpty(text.Text) && !int.TryParse(text.Text, out iResult))
                            {
                                err.Add("UserVerification.GridRowUpdating", "The Column '" + column.Key + "' is of " + column.Value + " type. Please provide valid input", BusinessAdaptorErrorType.Error);
                            }
                            else
                            {
                                if (column.Key == "DA_ROW_ID")
                                    sRowId = !string.IsNullOrEmpty(text.Text)?text.Text:"0";
                                elem.Attributes.Append(attr2);
                                Data.AppendChild(elem);
                            }
                        }
                    }
                }
                if (err.Count > 0)
                {
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
                else
                {
                    string template = GetTableDataTemplate();
                    template = AppHelper.ChangeMessageValue(template, "//Function", "UserVerificationAdaptor.UpdateTableData");
                    template = AppHelper.ChangeMessageXML(template, "//TableData", doc.InnerXml);
                    template = AppHelper.ChangeMessageValue(template, "//RowId", sRowId);
                    string sReturn = AppHelper.CallCWSService(template);
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    XmlElement XElem = (XmlElement)Model.SelectSingleNode("//MsgStatusCd");
                    if (XElem != null && XElem.InnerText == "Error")
                    {
                        XmlElement xMessage = (XmlElement)Model.SelectSingleNode("//ExtendedStatusDesc");
                        err.Add("UserVerification.Error", xMessage.InnerText, BusinessAdaptorErrorType.Message);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }
                    else
                    {
                        gvJobData.EditIndex = -1;
                        BindDatatoGrid(GetTableDataTemplate(), !string.IsNullOrEmpty(GridSortExpression.Text) ? GridSortExpression.Text + " " + GridSortDirection.Text : "");
                        txtSummary.Text = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                err.Add("UserVerification.GridRowUpdating", ex.Message, BusinessAdaptorErrorType.Error);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is called when we edit the row in the Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvJobData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            try
            {
                string sErrorTitle = string.Empty;
                string sErrorDesc = string.Empty;
                string sErrorTitleUpper = string.Empty;
                gvJobData.EditIndex = e.NewEditIndex;
                GridViewRow gRow = gvJobData.Rows[e.NewEditIndex];
                string sRowId = string.Empty;
                if (ViewState["ColumnNames"] != null)
                {
                    ColumnNames = (List<KeyValue>)ViewState["ColumnNames"];
                }
                if (ColumnNames != null && ColumnNames.Count > 0)
                {
                    for (int i = 1; i < gRow.Cells.Count; i++)
                    {
                        if (ColumnNames[i - 1].Key == "DA_ROW_ID")
                        {
                            sRowId = gRow.Cells[i].Text;
                            break;
                        }
                    }
                }
                string template = AppHelper.ChangeMessageValue(GetTableDataTemplate(), "//Function", "UserVerificationAdaptor.GetErrorDescription");
                template = AppHelper.ChangeMessageValue(template, "//RowId", sRowId);
                string sReturn = AppHelper.CallCWSService(template);
                Model = new XmlDocument();
                Model.LoadXml(sReturn);
                XmlElement XElem = (XmlElement)Model.SelectSingleNode("//MsgStatusCd");
                if (XElem != null && XElem.InnerText == "Error")
                {
                    XmlElement xMessage = (XmlElement)Model.SelectSingleNode("//ExtendedStatusDesc");
                    err.Add("UserVerification.Error", xMessage.InnerText, BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
                else
                {
                    XmlNode xmlErrorDescNode = Model.SelectSingleNode("//ErrorLogData/NewDataSet/Table/ERROR_DESC");
                    XmlNode xmlErrorTitleNode = Model.SelectSingleNode("//ErrorLogData/NewDataSet/Table/ERROR_TITLE");
                    XmlNode xmlErrorColumnNode = Model.SelectSingleNode("//ErrorLogData/NewDataSet/Table/ERROR_COLUMNS");
                    sErrorolumns = xmlErrorColumnNode != null ? xmlErrorColumnNode.InnerText : "";
                    //ipuri Start
                    sErrorTitle = xmlErrorTitleNode != null ? xmlErrorTitleNode.InnerText : "";
                    sErrorTitleUpper = sErrorTitle.ToUpper();
                    int iErrPos = sErrorTitleUpper.IndexOf("VALIDATION ERROR");
                    sErrorTitle= sErrorTitle.Substring(0,iErrPos);
                    sErrorTitle=sErrorTitle.TrimEnd(':','/',',','-',' ');
                    sErrorDesc = xmlErrorDescNode != null ? xmlErrorDescNode.InnerText : "";
                    //txtSummary.Text = xmlErrorDescNode != null ? xmlErrorDescNode.InnerText : "";
                    txtSummary.Text = sErrorTitle + ":" + "\r\n" + sErrorDesc.Replace("\\n", "\r\n") + ": " + sErrorolumns;
                    txtSummary.ReadOnly = true;
                    BindDatatoGrid(GetTableDataTemplate(), !string.IsNullOrEmpty(GridSortExpression.Text) ? GridSortExpression.Text + " " + GridSortDirection.Text : "");
                }
            }
            catch (Exception eRM)
            {
                err.Add(eRM, eRM.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is called when we click on cancel button to cancel the edit of the row in the Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvJobData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvJobData.EditIndex = -1;
            BindDatatoGrid(GetTableDataTemplate(), !string.IsNullOrEmpty(GridSortExpression.Text) ? GridSortExpression.Text + " " + GridSortDirection.Text : "");
            txtSummary.Text = string.Empty;
        }

        /// <summary>
        /// Author - Iti
        /// Date - 20 May 2014
        /// This function is called when we click on resume Task button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnResumetask_Click(object sender, EventArgs e)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            try
            {
                string template = string.Empty;
                template = GetTableDataTemplate();
                if (template != "--Select--")
                {
                    template = AppHelper.ChangeMessageValue(template, "//Function", "UserVerificationAdaptor.ResumeTask");
                    string sReturn = AppHelper.CallCWSService(template);
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    XmlElement XElem = (XmlElement)Model.SelectSingleNode("//MsgStatusCd");
                    if (XElem != null && XElem.InnerText == "Error")
                    {
                        XmlElement xMessage = (XmlElement)Model.SelectSingleNode("//ExtendedStatusDesc");
                        err.Add("UserVerification.Error", xMessage.InnerText, BusinessAdaptorErrorType.Message);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }
                    template = AppHelper.ChangeMessageValue(template, "//Function", "UserVerificationAdaptor.GetJobId");
                    string sReturnId = AppHelper.CallCWSService(template);
                    Model = new XmlDocument();
                    Model.LoadXml(sReturnId);
                    XmlElement XElemId = (XmlElement)Model.SelectSingleNode("//MsgStatusCd");
                    if (XElemId != null && XElemId.InnerText == "Error")
                    {
                        XmlElement xMessage = (XmlElement)Model.SelectSingleNode("//ExtendedStatusDesc");
                        err.Add("UserVerification.Error", xMessage.InnerText, BusinessAdaptorErrorType.Message);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }
                }
                btnResumetask.Enabled = false;
                //Server.Transfer("~/UI/Utilities/ToolsDesigners/TaskManager/TMView.aspx");
                //Response.Redirect("~/UI/Utilities/ToolsDesigners/TaskManager/TMView.aspx", true);
            }
            catch (Exception eRM)
            {
                err.Add("UserVerification.Error", eRM.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            //BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            //try
            //{
            //    string template = GetTableDataTemplate();
            //    template = AppHelper.ChangeMessageValue(template, "//Function", "UserVerificationAdaptor.ReVerifyJob");
            //    string sReturn = AppHelper.CallCWSService(template);
            //    Model = new XmlDocument();
            //    Model.LoadXml(sReturn);
            //    XmlElement XElem = (XmlElement)Model.SelectSingleNode("//MsgStatusCd");
            //    if (XElem != null && XElem.InnerText == "Error")
            //    {
            //        XmlElement xMessage = (XmlElement)Model.SelectSingleNode("//ExtendedStatusDesc");
            //        err.Add("UserVerification.Error", xMessage.InnerText, BusinessAdaptorErrorType.Message);
            //        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            //    }
            //}
            //catch (Exception eRM)
            //{
            //    err.Add("UserVerification.Error", eRM.Message, BusinessAdaptorErrorType.Message);
            //    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            //}
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is call when we click on abondon rask button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConfirmAbondon_Click(object sender, EventArgs e)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            string template = GetTableDataTemplate();
            template = AppHelper.ChangeMessageValue(template, "//Function", "UserVerificationAdaptor.AbondonJob");
            string sReturn = AppHelper.CallCWSService(template);
            Model = new XmlDocument();
            Model.LoadXml(sReturn);
            XmlElement XElem = (XmlElement)Model.SelectSingleNode("//MsgStatusCd");
                if (XElem != null && XElem.InnerText == "Error")
                {
                    XmlElement xMessage = (XmlElement)Model.SelectSingleNode("//ExtendedStatusDesc");
                    err.Add("UserVerification.Error", xMessage.InnerText, BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is called to get the dataset from the output xml.
        /// </summary>
        /// <param name="p_XmlDoc"></param>
        /// <returns>DataSet</returns>
        private new DataSet ConvertXmlDocToDataSet(XmlDocument p_XmlDoc)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            DataSet ds = null;
            try
            {
                ds = new DataSet();
                DataTable dt = new DataTable();
                int ColCount = gvJobData.Columns.Count;
                XmlNodeList objHeader = p_XmlDoc.SelectNodes("//TableHeader/Columns");
                ColumnNames = new List<KeyValue>();

                if (objHeader != null && objHeader.Count > 0)
                {
                    foreach (XmlNode node in objHeader)
                    {
                        KeyValue ColumnName = new KeyValue();
                        DataColumn dc = new DataColumn();
                        dc.ColumnName = node.InnerText;
                        dc.DataType = System.Type.GetType(node.Attributes["DataType"].Value);
                        dt.Columns.Add(dc);
                        ColumnName.Key = node.InnerText;
                        ColumnName.Value = node.Attributes["DataType"].Value;
                        ColumnNames.Add(ColumnName);
                    }
                    ViewState["ColumnNames"] = ColumnNames;
                }
                
                XmlElement objTableData = (XmlElement)p_XmlDoc.SelectSingleNode("//TableData");
                if (objTableData != null && objTableData.HasChildNodes)
                {
                    foreach (XmlNode node in objTableData.SelectNodes("//NewDataSet/Table"))
                    {
                        DataRow row = dt.NewRow();
                        foreach (XmlNode node2 in node.ChildNodes)
                        {
                            if (node2.InnerText.Length > 50)
                            {
                                if ((node2.InnerText.Length / 4) > Convert.ToInt32(node2.Name.Length))
                                {
                                    string sSpaces = string.Empty;
                                    for (int index2 = 0; index2 < node2.InnerText.Length / 8; index2++)
                                        //sSpaces += "_";
                                        sSpaces += "";  //kkaur25 JIRA 14403 
                                    if (dt.Columns[node2.Name] != null)
                                    {
                                        dt.Columns[node2.Name].ColumnName = sSpaces + node2.Name + sSpaces;
                                    }
                                    row[sSpaces + node2.Name + sSpaces] = node2.InnerText;
                                }
                            }
                            else
                            {
                                row[node2.Name] = node2.InnerText;
                            }
                        }
                        dt.Rows.Add(row);
                    }
                }
                ds.Tables.Add(dt);
            }
            catch (Exception eRM)
            {
                err.Add("UserVerification.Error", eRM.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            return ds;
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This function is used when we click on column name in the Grid to sort the data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvJobData_Sorting(object sender, GridViewSortEventArgs e)
        {
            gvJobData.PageIndex = 0;
            gvJobData.EditIndex = -1;
            txtSummary.Text = string.Empty;
            if (!string.IsNullOrEmpty(GridSortExpression.Text) && GridSortExpression.Text == e.SortExpression)
            {
                if (GridSortDirection.Text == "ASC")
                    GridSortDirection.Text = "DESC";
                else
                    GridSortDirection.Text = "ASC";
            }
            else
            {
                GridSortDirection.Text = "ASC";
            }
            BindDatatoGrid(GetTableDataTemplate(), e.SortExpression + " " + GridSortDirection.Text);
            GridSortExpression.Text = e.SortExpression;
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This is the event. It calls on every row bound in the grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvJobData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow gRow = e.Row;
            Control ctrl = gRow.FindControl("EditRow");
            if (ctrl != null)
            {
                LinkButton lnkButton = (LinkButton)ctrl;
                if (RbtnViewData.Checked)
                {
                    lnkButton.Enabled = false;
                }
            }

            for (int index = 1; index < gRow.Cells.Count; index++)
            {
                gRow.Cells[index].Attributes["style"] = "overflow:hidden";
                //ipuri Start
                //if (index < 6 && index != 0)
                //{
                    gRow.Cells[1].Visible = false;
                    gRow.Cells[2].Visible = false;
                    gRow.Cells[3].Visible = false;
                    gRow.Cells[4].Visible = false;
                    gRow.Cells[5].Visible = false;
                //}
                //ipuri End
            }
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This is a event. It call after data bound in the grid. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvJobData_DataBound(object sender, EventArgs e)
        {
            string[] sErrorColumnsList = sErrorolumns.Split(';');
            bool bIsColumnDisabled = true;
            if (ViewState["ColumnNames"] != null)
            {
                ColumnNames = (List<KeyValue>)ViewState["ColumnNames"];
            }
            if (gvJobData.EditIndex >= 0)
            {
                GridViewRow gRow = gvJobData.Rows[gvJobData.EditIndex];
                if (gRow != null)
                {
                    
                    for (int index = 1; index < gRow.Cells.Count; index++)
                    {
                        //ipuri start
                        //foreach (string sColumn in sErrorColumnsList)
                        //{
                        //    if (!string.IsNullOrEmpty(sColumn))
                        //    {
                        //        if (ColumnNames[index - 1].Key == sColumn.Trim())
                        //        {
                        //            bIsColumnDisabled = false;
                        //            break;
                        //        }
                        //    }
                        //}

                        //TextBox text = (TextBox)gRow.Cells[index].Controls[0];

                        //if (text != null)
                        //{
                        //    if (bIsColumnDisabled)
                        //    {
                        //        text.Enabled = false;
                        //    }
                        //    else
                        //    {
                        //        text.BorderColor = System.Drawing.Color.Red;
                        //        text.ForeColor = System.Drawing.Color.Red;
                        //    }
                        //}
                        //ipuri my changes
                        TextBox text = (TextBox)gRow.Cells[index].Controls[0];
                        if ((ColumnNames[index - 1].Key == "DA_ROW_ID") || (ColumnNames[index - 1].Key == "JOBID") || (ColumnNames[index - 1].Key == "INPUT_ROW_ID") || (ColumnNames[index - 1].Key == "INVALID_ROW") || (ColumnNames[index - 1].Key == "UPDATE_ROW"))
                        {
                            text.Enabled = false;
                        }
                        foreach (string sColumn in sErrorColumnsList)
                        {
                            if (!string.IsNullOrEmpty(sColumn))
                            {
                                if (ColumnNames[index - 1].Key == sColumn.Trim())
                                {
                                    text.BorderColor = System.Drawing.Color.Red;
                                    text.ForeColor = System.Drawing.Color.Red;
                                }
                            }
                        }
                        //ipuri end
                        bIsColumnDisabled = true;
                    }
                }
            }
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Date - 09 Jul 2010
        /// This is the event. It calls when we check/uncheck the Re-Verify check box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkReverify_CheckedChanged(object sender, EventArgs e)
        {
            //ipuri start
            //if (chkReverify.Checked)
            //    btnResumetask.Enabled = true;
            //else
            //    btnResumetask.Enabled = false;
            //ipuri end
        }

        /// <summary>
        /// Author - Iti Puri
        /// Date - 16 May 2014
        /// This is the event. It calls when we check/uncheck the Stop Verificayion check box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkStopVerify_CheckedChanged(object sender, EventArgs e)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            string template = GetTableDataTemplate();
            template = AppHelper.ChangeMessageValue(template, "//StopVerify", chkStopVerify.Checked ? "true" : "false");
            template = AppHelper.ChangeMessageValue(template, "//Function", "UserVerificationAdaptor.StopVerification");
            string sReturn = AppHelper.CallCWSService(template);
            Model = new XmlDocument();
            Model.LoadXml(sReturn);
            XmlElement XElem = (XmlElement)Model.SelectSingleNode("//MsgStatusCd");
            if (XElem != null && XElem.InnerText == "Error")
            {
                XmlElement xMessage = (XmlElement)Model.SelectSingleNode("//ExtendedStatusDesc");
                err.Add("UserVerification.Error", xMessage.InnerText, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

    }
    /// <summary>
    /// Author - Rahul Aggarwal
    /// Date - 09 Jul 2010
    /// This call is used to hold the Key value pair data.
    /// </summary>
    [Serializable]
    public class KeyValue
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
