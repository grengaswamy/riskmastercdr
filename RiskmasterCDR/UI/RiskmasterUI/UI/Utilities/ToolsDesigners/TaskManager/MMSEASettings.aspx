<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MMSEASettings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.MMSEASettings" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../../../../Content/dhtml-div.css" rel="stylesheet" type="text/css" />
   
       <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script src="../../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/utilities.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>

    <script src="../../../../Scripts/TMSettings.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    
    <style type="text/css">
        .style2
        {
            width: 113px;
        }
        .style3
        {
            text-decoration: underline;
            width: 113px;
        }
        .style4
        {
            width: 478px;
        }
        .style5
        {
            width: 337px;
        }
        .style7
        {
            text-decoration: underline;
            width: 116px;
        }
        .style8
        {
            width: 116px;
        }
        .style9
        {
            width: 362px;
        }
        
        .style10
        {
            width: 116px;
            height: 19px;
        }
        .style11
        {
            height: 19px;
        }
        
    </style>
    
</head>
<body>
    <form id="frmData" name="frmData" method="post" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td colspan="2">
              <uc1:ErrorControl ID="ErrorControl1" runat="server" />
              
            </td>
        <tr height="10">
            <td>
            </td>
        </tr>
    </table>
    <div>
    <div>
                    
        <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="TextBox1" />
        <div class="msgheader" id="div_formtitle" runat="server">
            <asp:Label ID="Label1" runat="server" Text="MMSEA Optionset" />
            <asp:Label ID="formsubtitle" runat="server" Text="" />
        </div>
        <div id="Div1" class="errtextheader" runat="server">
            <asp:Label ID="formdemotitle" runat="server" Text="" />
        </div>
        
    </div>
    </br>
    <table>
        <tr>
            
            <td class="style4">
                        <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                                            <td class="style10" nowrap="true" name="TABSexportsetting" 
                                                id="TABSexportsetting">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="exportsetting"
                                id="LINKTABSexportsetting"><span style="text-decoration: none">Export Optionset 
                            </span></a>
                        </td>
                        <!-- Vsoni5 : 12/14/2010 : MITS 23165: "return false;" added with onClick event for both the tabs to 
                        prevent the page to move up. -->
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;" class="style11">
                            &nbsp;&nbsp;
                        </td>
                        <td class="style10" nowrap="true" name="TABSimportsetting" id="TABSimportsetting">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="importsetting"
                                id="LINKTABSimportsetting"><span style="text-decoration: none">Import 
                            Optionset</span></a>
                        </td>
                                                
                        <td valign="top" nowrap="true" class="style11">
                        </td>
                    </tr>
                    
                </table>
                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABimportsetting"
                                    id="FORMTABimportsetting" style="display:none">
                    <tr>
                        <td class="style2">
                        
                            &nbsp;&nbsp;</td>
                        <td class="style5">
                        
                            &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3" style="font-weight: bold">
                        
                            Optionset Name</td>
                        <td class="style5">
                        
                            <asp:TextBox ID="txtImportOptionset" runat="server"></asp:TextBox>
                        
                        &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style3" style="font-weight: bold">
                        
                            RRE Id</td>
                        <td class="style5">
                        
                            <asp:DropDownList ID="ddlImportRREId" runat="server">
                            </asp:DropDownList>
                        
                        &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style3" style="font-weight: bold">
                        
                            File Format</td>
                        <td class="style5">
                        
                            <asp:DropDownList ID="ddlImportFileFormat" runat="server" AutoPostBack="True" OnSelectedIndexChanged = "ddlImportFileFormat_OnSelectedIndexChanged">
                                <asp:ListItem>--Select--</asp:ListItem>
                                <asp:ListItem>Claim</asp:ListItem>
                                <asp:ListItem>Query</asp:ListItem>
                            </asp:DropDownList>
                        
                        &nbsp;
                        
                        </td>
                    </tr>
                    <tr>
                        <td class="style3" style="font-weight: bold">
                        
                            Import File</td>
                        <td class="style5">
                            <!-- Vsoni5 : 04/05/2010 : MITS 17486: Events added to prevent user, entering import file name manually. -->
                            <input id="FUImport" type="file" runat="server"  oncontextmenu="return false;" onkeypress="return false;" onpaste="return false"/>                       
                        
                        
                        
                        </td>
 
                    </tr>
                    <tr id="Claim_TIN" runat="server" visible="true">
                        <td class="style3" style="font-weight: bold"> TIN Import File</td>
                        <td class="style5">
                            <input id="FUTinImport" type="file" runat="server"  oncontextmenu="return false;" onkeypress="return false;" onpaste="return false"/>                       
                
                        </td>
                     </tr>
                    <tr>
                        <td class="style2">
                        
                            &nbsp;</td>
                        <td class="style5">
                        
                            
                        
                            
                        
                            
                        
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                <!-- Vsoni5 : 04/05/2010 : MITS 17486: OnClientClick events handled to validate import file extension. -->       
                <asp:Button ID="btnSaveImport" runat="server" Class="button" Text="Save" 
                   CausesValidation="True" 
                    ValidationGroup="vImport" onclick="btnSaveImport_Click" OnClientClick="return validateFileType(document.forms[0].FUImport.value);" />
                            <asp:Button ID="btnImportCancel" runat="server" Text="Cancel" 
                    class="button" OnClientClick="OnCancel();return false;"  CausesValidation="false"/>
                        </td>
                        <td class="style5">
                        
                            
                        
                            &nbsp;</td>
                    </tr>
                </table>
                </br>
                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABexportsetting" id="FORMTABexportsetting"
                                   style="width: 449px">
                                   <tr>
                        <td class="style7" style="font-weight: bold">
                            
                            Optionset Name</td>
                        <td class="style9">
                            
                            <asp:TextBox ID="txtExportOptionset" runat="server"></asp:TextBox>
                            
                        &nbsp;
                        
                        </td>
                    </tr>
                    <tr>
                        <td class="style7" style="font-weight: bold">
                            
                            RRE Id</td>
                        <td class="style9">
                            
                            <asp:DropDownList ID="ddlExportRREId" runat="server">
                            </asp:DropDownList>
                        
                        &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style7" style="font-weight: bold">
                            
                            File Format</td>
                        <td class="style9">
                            
                            <asp:DropDownList ID="ddlExportFileFormat" runat="server" AutoPostBack="True" OnSelectedIndexChanged ="ddlExportFileFormat_OnSelectedIndexChanged">
                               <asp:ListItem>--Select--</asp:ListItem>
                                <asp:ListItem>Claim Input</asp:ListItem>
                                <asp:ListItem>Query Input</asp:ListItem>
                            </asp:DropDownList>
                        
                        &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style8">
                            
                            &nbsp;</td>
                        <td class="style9">
                            
                            <asp:CheckBox ID="chkTestExport" runat="server" Text="Test Export" />
                            
                        </td>
                    </tr>
                    <tr>
                        <td  class="style8" style="font-weight: bold">
                            Claimant Filters</td>
                        <td class="style13">
                            
                            <asp:CheckBox ID="chkPrimayClaimant" runat="server" Text="Export Only Primary Claimants" />
                            
                        </td>                                          
                    </tr>

                                                 <tr id="Claim_TPOC" runat="server" visible="true" >
                        <td  class="style10" style="font-weight: bold" >
                            Claim Option</td>
                        <td class="style11">
                            
                            <asp:CheckBox ID="chkTPOCSThreshold" runat="server" Text="Don&#8217;t Include TPOCs if under the Threshold" />
                            
                        </td>

                    </tr>
                    <tr>
                        <td class="style8">
                            
                <asp:Button ID="btnSave" runat="server" Class="button" Text="Save" 
                    onclick="btnSave_Click" CausesValidation="True" ValidationGroup="vExport" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    class="button" OnClientClick="OnCancel();return false;"  CausesValidation="false"/>
                        </td>
                        <td class="style9">
                            
                            &nbsp;</td>
                    </tr>
                </table>
                <input type="hidden" name="hTabName" id="hTabName"  runat="server"  value="exportsetting" />
            </td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
        </tr>
    </table>
        <asp:HiddenField ID="hdTaskManagerXml" runat="server" />
    <asp:HiddenField ID="hdOptionsetId" runat="server" />
    </div>
    </form>
</body>
</html>
