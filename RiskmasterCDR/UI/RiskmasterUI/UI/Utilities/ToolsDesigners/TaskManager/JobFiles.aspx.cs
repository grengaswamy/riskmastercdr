﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ServiceModel;
using System.IO;
//using Riskmaster.UI.DataIntegratorService;(dvatsa-cloud)
using Riskmaster.Models;
using Riskmaster.Common;


namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class JobFiles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //DataIntegratorServiceClient objDIClient = null;(dvatsa-cloud)
            JobFile objJobFile = null;
            JobFile objJobFileReturn = null;
            try
            {
                if (!IsPostBack)
                {
                    if (AppHelper.GetQueryStringValue("JobId") != "")
                    {
                        objJobFile = new JobFile();

                        if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                        {
                            objJobFile.Token = AppHelper.GetSessionId();
                        }

                        objJobFile.JobId = Conversion.ConvertObjToInt(AppHelper.GetQueryStringValue("JobId"));
                        
                        //Code change by dvatsa(cloud) -Start
                        //objDIClient = new DataIntegratorServiceClient();
                        //objJobFileReturn = objDIClient.RetrieveJobFiles(objJobFile);
                        objJobFile.ClientId = AppHelper.ClientId;
                        objJobFileReturn = AppHelper.GetResponse<JobFile>("RMService/DAIntegration/retrievejobfiles", AppHelper.HttpVerb.POST, "application/json", objJobFile);

                        //Code change by dvatsa(cloud) -End


                        using (MemoryStream ms = new MemoryStream(objJobFileReturn.FileContents))
                        {
                            long dataLengthToRead = ms.Length;
                            int blockSize = dataLengthToRead >= 5000 ? 5000 : (int)dataLengthToRead;
                            byte[] buffer = new byte[dataLengthToRead];

                            Response.Clear();

                            // Clear the content of the response
                            Response.ClearContent();
                            Response.ClearHeaders();

                            // Buffer response so that page is sent
                            // after processing is complete.
                            Response.BufferOutput = true;

                            // Add the file name and attachment,
                            // which will force the open/cance/save dialog to show, to the header
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + objJobFileReturn.FileName);

                            // bypass the Open/Save/Cancel dialog
                            //Response.AddHeader("Content-Disposition", "inline; fileid=" + doc.FileName);

                            // Add the file size into the response header
                            Response.AddHeader("Content-Length", ms.Length.ToString());

                            // Set the ContentType
                            Response.ContentType = "application/octet-stream";

                            // Write the document into the response
                            while (dataLengthToRead > 0 && Response.IsClientConnected)
                            {
                                Int32 lengthRead = ms.Read(buffer, 0, blockSize);
                                Response.OutputStream.Write(buffer, 0, lengthRead);
                                //Response.Flush(); // do not flush since BufferOutput = true
                                dataLengthToRead = dataLengthToRead - lengthRead;
                            }

                            Response.Flush();
                            Response.Close();
                        }

                        // End the response

                        Response.End();
                    }
                }
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objDIClient.Close();
            }
        }
    }
}
