﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TMStatusDetails.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.TMStatusDetails" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>TM Job Status Details</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/TMSettings.js"></script>
</head>
<body>
    <form id="frmData" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr id="tr_extComments">
             <%-- **ksahu5-ML-MITS33986** Start--%>
             <%-- <td class="caption" width="10%" style="font:Bold" >Status Details:</td>--%>

              <td class="caption" width="10%" style="font:Bold" >
              <asp:Label ID="lblStatusDetails" runat="server" Text="<%$ Resources:lblStatusDetails %>"></asp:Label>
              </td>
                
              <%-- **ksahu5-ML-MITS33986** End--%>
              <td>
                <asp:TextBox RMXRef="Instance/Document/Status/StatusDetails" runat="server"  ReadOnly="true" Columns="80" Rows="25" textmode="Multiline" id="txtextComments"/>
              </td>
            </tr>
    </table> 
    </form>
</body>
</html>
