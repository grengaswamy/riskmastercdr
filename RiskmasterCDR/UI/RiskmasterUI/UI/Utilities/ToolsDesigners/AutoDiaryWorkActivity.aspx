﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoDiaryWorkActivity.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.AutoDiaryWorkActivity" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<link href="Style/rmnet.css" rel="stylesheet" type="text/css"></link>
<script language="javaScript" src="../../../Scripts/form.js" type="text/javascript"></script>	
<head runat="server">
    <title>Work Activities</title>
    <script language="javascript">
				
					function fillSelectedActivities()
					{
						textboxSelectedId=window.opener.document.getElementById('CtrlAutoDiaryWizard_WorkActivityIdList');
						textboxSelectedName=window.opener.document.getElementById('CtrlAutoDiaryWizard_WorkActivityNameList');
						if (textboxSelectedId.value != '')
						{
							selectObject=document.getElementById('SelectedActivities');
							var arrId = new Array();
							var arrName = new Array();
							arrId=textboxSelectedId.value.split("|");
							arrName=textboxSelectedName.value.split("|");
							for(var i=0; i < arrId.length ;i++)
							{
								addOption(selectObject,arrName[i],arrId[i]);
							}
							
							removeAvailSelectedActivities();
						}
					}
					
					function addOption(selectObject,optionText,optionValue) 
					{
						var optionObject = new Option(optionText,optionValue);
						var optionRank = selectObject.options.length;
						selectObject.options[optionRank]=optionObject;
					} 
				
					function removeAvailSelectedActivities()
					{
						selectObject=document.getElementById('SelectedActivities');
						for(var i=0; i < selectObject.options.length ;i++)
						{
						    removeOption('AvailableActivities', selectObject.options[i].text); 	// Chnaged by Amitosh for mits 24245 (03/08/2011)
						}
					}

					function removeOption(selectName, text)		// Chnaged by Amitosh for mits 24245 (03/08/2011)
					{
						select=document.getElementById(selectName);
						Ids=new Array();
						Names=new Array();

						for(var x = 0; x < select.options.length ; x++)
						{
						    if (select.options[x].text != text)
							{
								Ids[Ids.length] = select.options[x].value;
								Names[Names.length]=select.options[x].text;
							}
						}

						select.length = Ids.length;

						for(var x = 0;x < select.options.length; x++)
						{
							select.options[x].text=Names[x];
							select.options[x].value=Ids[x];
						}
					}

					function AddActivity() {
					    
						var optionRank;
						var optionObject;
						var iSelectObjectLength;
						var bhasvalue = true;
                        selectObject = document.getElementById('SelectedActivities');
                        //Added By Amitosh for Mits 24245 (03/08/2011)
                        // To add unique work activities from 'CustomWorkActivity' and avoid dulicate work activities in diary
                        iSelectObjectLength = selectObject.length;
                        var customActivity = Trim(document.getElementById('CustomWorkActivity').value) ;
                        if (customActivity != '') {
                            if (iSelectObjectLength > 0) {
                                for (var i = 0; i < iSelectObjectLength; i++) {
                                    if (selectObject.options[i].innerText.toLowerCase() == customActivity.toLowerCase()) {
                                        document.getElementById('CustomWorkActivity').value = '';
                                        bhasvalue = false;
                                        break;
                                    }

                                }
                            }
                            if (bhasvalue) {
                                optionObject = new Option(customActivity, '0');
                                optionRank = selectObject.options.length;
                                selectObject.options[optionRank] = optionObject;
                                document.getElementById('CustomWorkActivity').value = '';
                            }
                        }

                        
                        // end Amitosh
						//Add selected Available Values
						select=document.getElementById('AvailableActivities');
						for(var x = 0; x < select.length ; x++)
						{
							if(select.options[x].selected == true)
							{
								//add to selected list
								optionObject = new Option(select.options[x].text,select.options[x].value);
								optionRank = selectObject.options.length;
								selectObject.options[optionRank]=optionObject;
								
								//remove from available list	
								removeOption('AvailableActivities',select.options[x].text);			// Chnaged by Amitosh for mits 24245 (03/08/2011)				
							}
						}
						return false;
					}
					
					
					function RemoveActivity()
					{
						var optionRank;
						var optionObject;
						selectObject=document.getElementById('AvailableActivities');
						select=document.getElementById('SelectedActivities');
						
						//Check selected Activity
						for(var x = 0; x < select.length ; x++)
						{
							if(select.options[x].selected == true)
							{
								if (select.options[x].value != 0)
								{
									//add to available list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
								}
								
								//remove from selected list	
					removeOption('SelectedActivities', select.options[x].text); 			// Chnaged by Amitosh for mits 24245 (03/08/2011)	
							}
						}
						return false;
					}
				
					function UpdateWorkActivity()
					{
						allvalue="";
						allname="";
						ctrl=document.getElementById('SelectedActivities');
	
						for(var f=0; f < ctrl.options.length; f++)
						{
							allvalue=allvalue + ctrl.options[f].value + "|";
							allname=allname + ctrl.options[f].text + "|";
						}
						allvalue=allvalue.substring(0,allvalue.length-1);
						ctrl=window.opener.document.getElementById('CtrlAutoDiaryWizard_WorkActivityIdList');
						ctrl.value=allvalue;
						
						allname = allname.substring(0,allname.length-1);
						ctrl = window.opener.document.getElementById('CtrlAutoDiaryWizard_WorkActivityNameList');
						ctrl.value = allname;
					
						window.opener.fillActivities();
						
						window.close();
						return false;
		}

		//rupal:start, MITS 26246
		function ResetSelectedIndex() {
		    var selectObject = document.getElementById('AvailableActivities');
		    selectObject.selectedIndex = -1;
		    return false;

		}
		//rupal:end

        // Ishan : MITS :28281
		function OnSelectionChanged() {
		    document.getElementById('CustomWorkActivity').value = '';
		}
        // Ishan end
					
				</script>
</head>
<body>
    <form id="frmData" runat="server">
        <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
             <td colspan="2">
              <table cellspacing="0" cellpadding="0" width="100%">
               <tr>
                <td width="40%">
         			Available Activities<br>
                    <asp:ListBox ID="AvailableActivities" size="10" style="width:90%"  
                        runat="server" ondblclick="return AddActivity();" Height="180px" onchange="OnSelectionChanged();" ></asp:ListBox>
         		</td>
                <td width="20%" valign="middle" align="center">
                    <input type="button" id="btnAddWorkActivity" value="Add &gt;" class="button" style="width:80px" onclick="return AddActivity();; ">
                    <br><br>
                    <input type="button" id="btnRemoveWorkActivity" value="< Remove" class="button" style="width:80px" onclick="return RemoveActivity();; ">
                </td>
                <td width="40%">
         			Selected Activities
         			<br>
                    <asp:ListBox style="width:90%" size="10" ID="SelectedActivities" runat="server" 
                        ondblclick="return RemoveActivity();" Height="180px"></asp:ListBox>
         		</td>
               </tr>
               <tr>
                <td colspan="3">&nbsp;</td>
               </tr>
               <tr>
                <td>
         		    Custom:&nbsp;&nbsp;&nbsp;<input type="text" id="CustomWorkActivity" name="CustomWorkActivity" size="26" onfocus="return ResetSelectedIndex();;" >
         		</td>
                <td colspan="2">&nbsp;</td>
               </tr>
              </table>
             </td>
            </tr>
            <tr>
             <td class="group" colspan="2"></td>
            </tr>
            <tr>
             <td colspan="2">
      			&nbsp;
             </td>
            </tr>
            <tr>
             <td colspan="2" align="center">
                <input type="button"  id="btnOk" value="Ok" class="button" style="width:80px" onclick="return UpdateWorkActivity();; "> 
                <input type="button" id="btnCancel"  value="Cancel" class="button" style="width:80px" onclick="javascript:window.close();return false;; ">
            </td>
            </tr>
         </table>
    </form>
</body>
</html>
