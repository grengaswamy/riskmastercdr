﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoDiaryfilterWizard.aspx.cs"  ValidateRequest="false" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.AutoDiaryfilterWizard" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>WPA Auto Diary Setup</title>   
     <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
     
    <script type="text/javascript" src="../../../Scripts/form.js"></script>
    <script src="../../../Scripts/cul.js" type="text/javascript"></script>
    <%--vkumar258 - RMA-6037 - Starts --%>
    <%--<script type="text/javascript" src="../../../Scripts/zapatec/utils/zapatec.js">{ var i; }</script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js">{ var i; }</script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js">{ var i; }</script>--%>

    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
    <script language="javascript">

                    function checkSelectionForAutoDiaryWizard()
					{
						AutoDiaryWizard=document.forms[0].AutoDiaryWizard;
						if (AutoDiaryWizard.checked)
						{
							document.forms[0].hdnScreenAction.value="AutoDiaryWizard";
						}
						else
						{
							document.forms[0].hdnScreenAction.value="BestPracticesWizard";
						}
						
                        return false;
					}
					
                    function OpenAddEditAutoDiaryFilter(url) {                                        
                        window.open(url,"AddEditAutoDiaryFilter",
									"width=600,height=300,top="+(screen.availHeight-300)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
                    }

                    function OpenAddEditOnDoubleClick(selectedMode) 
					{
                        __doPostBack('tCtrlAutoDiaryWizard_UpatePaneForAutoFilterStep2','FiltersDoubleClickedMode=' + selectedMode);
					}
					
					function RefreshPageAfterAddEditFilter(updatedDataValue)
					{
                        __doPostBack('tCtrlAutoDiaryWizard_UpatePaneForAutoFilterStep2', updatedDataValue);
                    }
					
                    function EnableDisableSuppFieldCombo()
					{
					//Changed by Saurabh Arora for MITS 18645:Start
                    /*if (document.forms[0].chkEventSupp.checked)
							document.forms[0].cboSuppField.disabled=false;
						else
							document.forms[0].cboSuppField.disabled=true;
					*/				
				
						if (document.forms[0].CtrlAutoDiaryWizard_chkEventSupp.checked==true)
							document.forms[0].CtrlAutoDiaryWizard_cboSuppField.disabled=false;
						else
							document.forms[0].CtrlAutoDiaryWizard_cboSuppField.disabled=true;
					//Changed by Saurabh Arora for MITS 18645:End
					}
					
					function UpdateSendOtherUsersGroups(mode)
					{
						// For Send Users
						allid="";
						allname="";
						ctrl=document.getElementById('CtrlAutoDiaryWizard_lstSendUsers');
						for(var f = 0; f < ctrl.options.length ; f++)
						{
							allid=allid + ctrl.options[f].value + "|";
							allname=allname + ctrl.options[f].text + "|";
						}
						allid = allid.substring(0,allid.length-1);
						ctrl = document.getElementById('CtrlAutoDiaryWizard_SendUsersIdList');
						ctrl.value = allid;
						allname = allname.substring(0,allname.length-1);
						ctrl = document.getElementById('CtrlAutoDiaryWizard_SendUsersNameList');
						ctrl.value = allname;
						
						// For Send Groups
						allid="";
						allname="";
						ctrl=document.getElementById('CtrlAutoDiaryWizard_lstSendGroups');
						for(var f = 0; f < ctrl.options.length ; f++)
						{
							allid=allid + ctrl.options[f].value + "|";
							allname=allname + ctrl.options[f].text + "|";
						}
						allid = allid.substring(0,allid.length-1);
						ctrl = document.getElementById('CtrlAutoDiaryWizard_SendGroupsIdList');
						ctrl.value = allid;
						allname = allname.substring(0,allname.length-1);
						ctrl = document.getElementById('CtrlAutoDiaryWizard_SendGroupsNameList');
						ctrl.value = allname;
					
						document.forms[0].Flag.value="1";
						
						if (mode=='finish')
							document.forms[0].FormMode.value="close";
						
						return true;
					}
					
					function AddItemsToSendOtherUsersGroupsList()
					{
						textboxUsersId=document.getElementById('CtrlAutoDiaryWizard_SendUsersIdList');
						textboxUsersName=document.getElementById('CtrlAutoDiaryWizard_SendUsersNameList');
						selectObject=document.getElementById('CtrlAutoDiaryWizard_lstSendUsers');
						selectObject.options.length=0;
						if (textboxUsersId.value != '')
						{
							var arrId = new Array();
							var arrName = new Array();
							arrId=textboxUsersId.value.split("|");
							arrName=textboxUsersName.value.split("|");
							for(var i=0; i < arrId.length ;i++)
							{
								addOption(selectObject,arrName[i],arrId[i]);
							}
						}
						
						textboxGroupsId=document.getElementById('CtrlAutoDiaryWizard_SendGroupsIdList');
						textboxGroupsName=document.getElementById('CtrlAutoDiaryWizard_SendGroupsNameList');
						selectObject=document.getElementById('CtrlAutoDiaryWizard_lstSendGroups');
						selectObject.options.length=0;
						if (textboxGroupsId.value != '')
						{
							var arrId = new Array();
							var arrName = new Array();
							arrId=textboxGroupsId.value.split("|");
							arrName=textboxGroupsName.value.split("|");
							for(var i=0; i < arrId.length ;i++)
							{
								addOption(selectObject,arrName[i],arrId[i]);
							}
						}
					}
					
					function addOption(selectObject,optionText,optionValue) 
					{
						if (!recheck (selectObject, optionText, optionValue))
						{
							var optionObject = new Option(optionText,optionValue);
							var optionRank = selectObject.options.length;
							selectObject.options[optionRank]=optionObject;
						}
					} 

					function recheck (selectObject, optionText, optionValue)
					{ 
						var sltbool = false;
						for (i=0; i < selectObject.options.length; i++)
						{
							if (optionText == selectObject.options[i].text ||  optionValue == selectObject.options[i].value )
							{
								sltbool = true;
								break;
							}
						} 
						return sltbool;
					}
				
                    function UpdateUsersGroups(mode)
					{
						// For Notify Users
						allid="";
						allname="";
						ctrl=document.getElementById('CtrlAutoDiaryWizard_lstUsersNotified');
						for(var f = 0; f < ctrl.options.length ; f++)
						{
							allid=allid + ctrl.options[f].value + "|";
							allname=allname + ctrl.options[f].text + "|";
						}
						allid = allid.substring(0,allid.length-1);
						ctrl = document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedIdList');
						ctrl.value = allid;
						allname = allname.substring(0,allname.length-1);
						ctrl = document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedNameList');
						ctrl.value = allname;
						
						// For Routed Users
						allid="";
						allname="";
						ctrl=document.getElementById('CtrlAutoDiaryWizard_lstUsersRouted');
						for(var f = 0; f < ctrl.options.length ; f++)
						{
							allid=allid + ctrl.options[f].value + "|";
							allname=allname + ctrl.options[f].text + "|";
						}
						allid = allid.substring(0,allid.length-1);
						ctrl = document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList');
						ctrl.value = allid;
						allname = allname.substring(0,allname.length-1);
						ctrl = document.getElementById('CtrlAutoDiaryWizard_UsersRoutedNameList');
						ctrl.value = allname;
					
						document.forms[0].Flag.value="1";
					
						if (mode=='finish')
							document.forms[0].FormMode.value="close";
							
						return true;
					}
					
					function AddItemsToUsersGroupsList()
					{
						textboxUsersId=document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedIdList');
						textboxUsersName=document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedNameList');
						selectObject=document.getElementById('CtrlAutoDiaryWizard_lstUsersNotified');
						selectObject.options.length=0;
						if (textboxUsersId.value != '')
						{
							var arrId = new Array();
							var arrName = new Array();
							arrId=textboxUsersId.value.split("|");
							arrName=textboxUsersName.value.split("|");
							for(var i=0; i < arrId.length ;i++)
							{
								addOption(selectObject,arrName[i],arrId[i]);
							}
						}
						
						textboxUsersId=document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList');
						textboxUsersName=document.getElementById('CtrlAutoDiaryWizard_UsersRoutedNameList');
						selectObject=document.getElementById('CtrlAutoDiaryWizard_lstUsersRouted');
						selectObject.options.length=0;
						if (textboxUsersId.value != '')
						{
							var arrId = new Array();
							var arrName = new Array();
							arrId=textboxUsersId.value.split("|");
							arrName=textboxUsersName.value.split("|");
							for(var i=0; i < arrId.length ;i++)
							{
								addOption(selectObject,arrName[i],arrId[i]);
							}
						}
					}
					
					function addOption(selectObject,optionText,optionValue) 
					{
						if (!recheck (selectObject, optionText, optionValue))
						{
							var optionObject = new Option(optionText,optionValue);
							var optionRank = selectObject.options.length;
							selectObject.options[optionRank]=optionObject;
						}
					} 

					function recheck (selectObject, optionText, optionValue)
					{ 
						var sltbool = false;
					    for (i=0; i < selectObject.options.length; i++)
                        {
                          if (optionText == selectObject.options[i].text ||  optionValue == selectObject.options[i].value )
                          {
                            sltbool = true;
                            break;
                          }
                        }
                          return sltbool;
                      }

                      function checkWindow()
                      {
                        if (document.forms[0].FormMode.value=="close")
                        {
                        window.opener.document.forms[0].submit();
                        window.close();
                        }
                      }
                      
         function HideLists(firstlist, secondlist, thirdlist, escllevelAll,escllevelHigh,escllevelImme,escllevelNone, firstlabel, hdnSmsRelationSelected)
         {
            flist=document.getElementById('CtrlAutoDiaryWizard_' + firstlist);
            slist=document.getElementById('CtrlAutoDiaryWizard_' + secondlist);
            tlist=document.getElementById('CtrlAutoDiaryWizard_' + thirdlist);
            //fcheck = document.getElementById('CtrlAutoDiaryWizard_' + firstcheck);
            esclAll = document.getElementById('CtrlAutoDiaryWizard_' + escllevelAll);
            esclHigh = document.getElementById('CtrlAutoDiaryWizard_' + escllevelHigh);
            esclImme = document.getElementById('CtrlAutoDiaryWizard_' + escllevelImme);
            esclNone = document.getElementById('CtrlAutoDiaryWizard_' + escllevelNone);
            flabel=document.getElementById('CtrlAutoDiaryWizard_' + firstlabel);
            flist.style.display='none';
            slist.style.display='none';
            tlist.style.display='none';
            //Change by kuladeep for mits:24661 Start
            //fcheck.disabled = false;
            esclAll.checked = true;
            esclAll.disabled = false;
            esclHigh.disabled = false;
            esclImme.disabled = false;
            esclNone.disabled = false;
            
            //Change by kuladeep for mits:24661 End
            flabel.style.display='';
            document.getElementById('CtrlAutoDiaryWizard_' + hdnSmsRelationSelected).value = 'true';
          }
          
          function DisplayLists(firstlist, secondlist, thirdlist, escllevelAll, escllevelHigh, escllevelImme, escllevelNone, firstlabel, hdnSmsRelationSelected) {
            flist=document.getElementById('CtrlAutoDiaryWizard_' + firstlist);
            slist=document.getElementById('CtrlAutoDiaryWizard_' + secondlist);
            tlist=document.getElementById('CtrlAutoDiaryWizard_' + thirdlist);
            //Change by kualdeep
            //fcheck= document.getElementById('CtrlAutoDiaryWizard_' + firstcheck);
            esclAll = document.getElementById('CtrlAutoDiaryWizard_' + escllevelAll);
            esclHigh = document.getElementById('CtrlAutoDiaryWizard_' + escllevelHigh);
            esclImme = document.getElementById('CtrlAutoDiaryWizard_' + escllevelImme);
            esclNone = document.getElementById('CtrlAutoDiaryWizard_' + escllevelNone);
            flabel=document.getElementById('CtrlAutoDiaryWizard_' + firstlabel);
            flist.style.display='';
            slist.style.display='';
            tlist.style.display='';
            //Change by kuladeep for mits:24661 Start
            //fcheck.checked=false;
            //fcheck.disabled=true;
            esclAll.checked = false;
            esclAll.disabled = true;
            esclHigh.disabled = true;
            esclImme.disabled = true;
            esclNone.disabled = true;
            //Add by kuladeep for mits:24661 End
            flabel.style.display='';
            
            document.getElementById('CtrlAutoDiaryWizard_' + hdnSmsRelationSelected).value = 'false';
          }
          
          function checkSecurityUser()
          {
            if(document.forms[0].Selected_User.checked == true)
                DisplayLists('lstUsersNotified', 'lstUsersNotifiedbtn', 'lstUsersNotifiedbtndel', 'Escalate_All_Lev', 'Escalate_High_Lev', 'Escalate_Imme_Lev', 'Escalate_None_Lev', 'lblEscalate');
            else
                HideLists('lstUsersNotified', 'lstUsersNotifiedbtn', 'lstUsersNotifiedbtndel', 'Escalate_All_Lev', 'Escalate_High_Lev', 'Escalate_Imme_Lev', 'Escalate_None_Lev', 'lblEscalate');
          }
            //Add function by kuladeep for check number Start mits:24661
            function numLostFocusNonNegative(objCtrl) {
                if (objCtrl.value.length == 0)
                    return false;
                if (objCtrl.value.indexOf('-') == 0) {
                    alert("Negative value not allowed for this field");
                    objCtrl.focus();
                    return false;
                }
                if (objCtrl.value == 0) {
                    alert("Value should be greater then zero for this field");
                    objCtrl.focus();
                    return false;
                }
                if (isNaN(parseFloat(objCtrl.value)))
                    objCtrl.value = "";
                else
                    objCtrl.value = parseFloat(objCtrl.value);

                if (objCtrl.getAttribute('min') != null) {
                    if (!(isNaN(parseFloat(objCtrl.getAttribute('min'))))) {
                        if (parseFloat(objCtrl.value) < parseFloat(objCtrl.getAttribute('min'))) {
                            objCtrl.value = 0;
                        }
                    }
                }

                return true;
            }
            //Add function by kuladeep for check number End mits:24661
          
          function selectallworkactivities()
		    {
			    if (document.forms[0].WorkActivityIdList.value == '')
			    {
				    allvalue="";
				    allname="";
				    ctrl=document.getElementById('CtrlAutoDiaryWizard_lstWorkActivity');

				    for(var f=0; f < ctrl.options.length; f++)
				    {
					    allvalue=allvalue + ctrl.options[f].value + "|";
					    allname=allname + ctrl.options[f].text + "|";
				    }
				    allvalue=allvalue.substring(0,allvalue.length-1);
				    ctrl=document.getElementById('CtrlAutoDiaryWizard_WorkActivityIdList');
				    ctrl.value=allvalue;
    				
				    allname = allname.substring(0,allname.length-1);
				    ctrl = document.getElementById('CtrlAutoDiaryWizard_WorkActivityNameList');
				    ctrl.value = allname;
			    }
		    }
					
					function openWorkActivityPage()
					{
					  
//						window.open('home?pg=riskmaster/RMUtilities/AutoDiaryWorkActivities&selectedid=' + document.forms[0].diaryId.value,'AutoDiaryWorkActivities',
//							'width=650,height=300,top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-650)/2+',resizable=no,scrollbars=yes');

                        window.open('AutoDiaryWorkActivity.aspx?selectedid=' + document.forms[0].CtrlAutoDiaryWizard_diaryId.value,'AutoDiaryWorkActivities',
							'width=650,height=300,top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-650)/2+',resizable=no,scrollbars=yes');
					
					
						return false;
					}
					
					function fillActivities()
					{
						selectObject=document.getElementById('CtrlAutoDiaryWizard_lstWorkActivity');
						
						textboxSelectedId=document.getElementById('CtrlAutoDiaryWizard_WorkActivityIdList');
						textboxSelectedName=document.getElementById('CtrlAutoDiaryWizard_WorkActivityNameList');
						if (textboxSelectedId.value != '')
						{
							var arrId = new Array();
							var arrName = new Array();
							arrId=textboxSelectedId.value.split("|");
							arrName=textboxSelectedName.value.split("|");
							selectObject.length=arrId.length;
							var optionObject;
							for(var i=0; i < arrId.length ;i++)
							{
								optionObject = new Option(arrName[i],arrId[i]);
								selectObject.options[i]=optionObject;
							}
						}
					}
					function checkForFillingWorkActivities()
					{
						textboxSelectedId=document.getElementById('CtrlAutoDiaryWizard_WorkActivityIdList');
						if (textboxSelectedId.value != '')
							fillActivities();
					}
					
					function SetCloseWindow()
					{
						document.forms[0].FormMode.value="close";
						return true;
					}
					
					function checkWindow()
					{
						if (document.forms[0].FormMode.value=="close")
						{
							window.opener.document.forms[0].submit();
							window.close();
						}
					}

					function closeAutoDiaryWindow() 
					{
					    if (confirm('Are you sure you want to Cancel Auto Diary Setup?'))
					        window.close();

					    return false;
					}
                    //amar mits 33773 starts
					function CheckField() {

					    var txtDiaryName = document.getElementById('CtrlAutoDiaryWizard_txtDiaryName');
					    if (txtDiaryName != null && txtDiaryName.value == '') {
					        alert("Please give a name to the Auto Diary Definition");
					        return false;
					    }
					    else {
					        return true;
                        }
					}
					//amar mits 33773 ends
	</script>
</head>
<body>
    <form id="frmData" runat="server">
    <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
    <asp:HiddenField ID="hdnScreenAction" runat="server" Value="" />
    <asp:Label ID="lblMessgeTitle" runat="server" CssClass="group" Width="99%" Visible="false" Text=""></asp:Label>
        <input type="hidden"  runat="server" value="" id="hdnUserStr"/>
        <input type="hidden"  runat="server" value="" id="hdnGroupStr" />
        <input type="text" runat="server" style="display:none" id="hdnDiaryLstGroups"/>
        <input type="text" runat="server" style="display:none" id="hdnDiaryLstUsers"/>
    <div>
        <asp:Wizard ID="CtrlAutoDiaryWizard"  NavigationStyle-HorizontalAlign="Center" runat="server"  DisplaySideBar="false"  Width="98%"  
            Height="90%" ActiveStepIndex="0" Visible=false oncancelbuttonclick="CtrlAutoDiaryWizard_CancelButtonClick" 
            onnextbuttonclick="CtrlAutoDiaryWizard_NextButtonClick" 
            onpreviousbuttonclick="CtrlAutoDiaryWizard_PreviousButtonClick"> 
                <StartNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="closeAutoDiaryWindow();return false;" />
                  <asp:Button class="button" ID="StepPreviousButton" disabled="true" runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" OnClientClick="return CheckField();" /> 
                  <asp:Button class="button" ID="StepFinishButton" runat="server" Text="Finish" CausesValidation="True" OnClick="CtrlAutoDiaryWizard_FinishButtonClick" /> 
                </StartNavigationTemplate>  
                <StepNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="closeAutoDiaryWindow();return false;" />
                  <asp:Button class="button" ID="StepPreviousButton"  runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True"   /> 
                  <asp:Button class="button" ID="StepFinishButton"  runat="server" Text="Finish" CausesValidation="True" OnClick="CtrlAutoDiaryWizard_FinishButtonClick" />
               </StepNavigationTemplate>
               <FinishNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="closeAutoDiaryWindow();return false;" />
                  <asp:Button class="button" ID="StepPreviousButton"  runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" disabled="true" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton" runat="server"  OnClick="CtrlAutoDiaryWizard_FinishButtonClick" Text="Finish" CausesValidation="True" /> 
               </FinishNavigationTemplate>
                <WizardSteps>
                    <asp:WizardStep ID="AutoFilterStep1" runat="server" Title="">
                        <asp:HiddenField ID="hdnAutoFilterStep1Visited" runat="server" Value="" />
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td valign="center">
                                    <img src="../../../Images/wpaautosetup.gif" />
                                </td>
                                <td align="left" valign="center">The auto diary setup wizard will walk you through the <br>process of creating/updating your auto diary definitions.<br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">Which auto diary template do you wish to use?
                                    <asp:DropDownList ID="cboTemplate" runat="server" Width="335px"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="required">What do you want to call this auto diary definition?<br>
                                        <asp:TextBox ID="txtDiaryName" Text="" size="60" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td width="40%" valign="top">
         								        Do not process diaries for records <br>that were created before this date.<br>
         								        <input type="text"  runat="server" id="txtDiaryDate" size="15" onblur="dateLostFocus(this.id);"/><!--Modified by mdhamija MITS 27606-->
         								                                                      <%--vkumar258 - RMA-6037 - Starts --%>
 <%-- <input type="button"  class="DateLookupControl" id="txtCompletedOnbtn" value=""/>
                                                <script type="text/javascript">
                                                    Zapatec.Calendar.setup({ inputField: "CtrlAutoDiaryWizard_txtDiaryDate", ifFormat: "%m/%d/%Y", button: "txtCompletedOnbtn" });      
                                                </script>--%>
                                        <script type="text/javascript">
                                            $(function () {
                                                $("#CtrlAutoDiaryWizard_txtDiaryDate").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "../../../Images/calendar.gif",
                                                   // buttonImageOnly: true,
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    changeYear: true
                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                            });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                                            </td>
                                            <td width="60%" valign="top">
         											By default, Due date is set to one day after WPA 
         											<br>
         											Processing is done. You can choose number of 
         											<br>
         											days after WPA processing in order to set the due 
         											<br>
         											date. Enter -1 to set Due date to the same date.
         											<br>
                                                    <asp:TextBox ID="txtDaysNoAfterDueDate" runat="server" Text=""></asp:TextBox>
         							        </td>
                                       </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								What priority level should be given to this diary?<br>
      								<asp:DropDownList ID="cboPriority" runat="server" Width="185px"></asp:DropDownList>
      							</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                 <td colspan="2">&nbsp;</td>
                            </tr>
                        </table>
                    </asp:WizardStep> 
                    <asp:WizardStep ID="AutoFilterStep2" runat="server" Title="">
                       <asp:HiddenField ID="hdnAutoFilterStep2Visited" runat="server" Value="" />
                       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UpatePaneForAutoFilterStep2" OnLoad="UpatePaneForAutoFilterStep2_Load" runat="server">
                        <ContentTemplate> 
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td valign="middle">
                                    <img src="../../../Images/wpaautosetup.gif" />
                                </td>
                                <td align="left" valign="middle">
                                    By applying criteria or filters you can define diary definitions
                                    <br>
                                    that reflect your business rules.  You can set criteria that 
                                    <br>
                                    will be used to determine what records will be candidates for 
                                    <br>
                                    auto diary generation.  NOTE: Broad selection of criteria 
                                    <br>
                                    can result in a large number of auto diaries being 
                                    <br>
                                    generated.
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                        
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                            <td width="40%">
         						                Available Filter(s)<br>
                                                <asp:ListBox ID="AvailableFilters" runat="server" size="10" style="width:90%" Height="190px" ondblclick="return OpenAddEditOnDoubleClick('add');"></asp:ListBox>
                                            </td>
                                            <td width="20%" valign="middle" align="center">
                                                <asp:Button ID="btnAddAvlFilter" runat="server" Text="Add &gt;" onclick="btnAddAvlFilter_Click"  CssClass="button" Width="80"/>
                                                <br><br>
                                                <asp:Button ID="btnEditAvlFilter" runat="server" Text="Edit &gt;" onclick="btnEditAvlFilter_Click"  CssClass="button" Width="80"/>
                                                <br><br>
                                                <asp:Button ID="btnRemoveAvlFilter" runat="server" Text="< Remove" onclick="btnRemoveAvlFilter_Click"    CssClass="button" Width="80"/>
                                            </td>
                                            <td width="40%">
         							            Selected Filter(s)<br>
                                                <asp:ListBox ID="SelectedFilters" runat="server" size="10" style="width:90%" Height="190px" ondblclick="return OpenAddEditOnDoubleClick('edit');"></asp:ListBox>
                                            </td> 
                                       </tr>
                                      </table>
                                      <asp:HiddenField ID="hdnTemplateId"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnId"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnFilterName"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSqlFill"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnFilterType"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnData"  runat="server" Value="" />
                                      <asp:HiddenField ID="hdnMode" runat="server" Value="" />
                                      <asp:HiddenField ID="hdnDefValue" runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelectedLevelNode" runat="server" Value="" />
                                      <asp:HiddenField ID="hdnSelectedFiltersNode" runat="server" Value="" />
                                      <!--rupal-->
                                      <asp:HiddenField ID="hdnDatabase" runat="server" Value="" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								                        &nbsp;
      						    </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:WizardStep> 
                    <asp:WizardStep ID="AutoFilterStep3" runat="server" Title="">
                        <asp:HiddenField ID="hdnAutoFilterStep3Visited" runat="server" Value="" />
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td valign="middle">
                                    <img src="../../../Images/wpaautosetup.gif" />
                                </td>
                                <td align="left" valign="middle">
      								                        When the system generates an auto diary it needs to know 
      								                        <br>
      								                        what user(s) will receive the diary as a task in their WPA 
      								                        <br>
      								                        Diary List.  Select what users will receive diaries generated 
      								                        <br>
      								                        by this auto diary definition.
      								                        <br>
      						    </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                       <tr>
                                            <td width="20%">&nbsp;</td>
                                            <td width="80%">
                                                <input type="checkbox"  id="chkUserCreated" value="1" runat="server" />Send diary to user that created record
                                            </td>
                                       </tr>
                                       <tr>
                                            <td>&nbsp;</td>
                                            <td><input type="checkbox" id="chkUserModified" value="1" runat="server" />Send diary to user that last modified record</td>
                                       </tr>
                                       <tr>
                                            <td>&nbsp;</td>
                                            <td><input id="chkAdjAssign" type="checkbox" value="1" runat="server" />Send diary to adjuster assigned to record</td>
                                       </tr>
                                       <tr>
                                            <td>&nbsp;</td>
                                            <td><input type="checkbox" id="chkAdjAssigSupervisor" value="1" runat="server"  >Send diary to the Supervisor of adjuster assigned to record</td>
                                       </tr>
                                       <tr id="trChkEventSupp" runat="server">
                                            <td>&nbsp;</td>
                                            <td><input type="checkbox" id="chkEventSupp" value="1" onclick="EnableDisableSuppFieldCombo()" runat="server">Send diary to user using Event supp field type Entity</td>
                                       </tr>
                                       <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:DropDownList ID="cboSuppField"  Width="250px" runat="server"></asp:DropDownList>
                                            </td>
                                       </tr>
                                       <tr>
                                            <td colspan="2">&nbsp;</td>
                                       </tr>
                                       <tr>
                                            <td colspan="2">&nbsp;</td>
                                       </tr>
                                       <tr>
                                            <td colspan="2">&nbsp;</td>
                                       </tr>
                                       <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <div id="UsersList">
                                                        Other User(s)/Group(s)<br>
                                                        <asp:ListBox SelectionMode="Multiple" Width="250px" ID="lstSendUsers" runat="server" ></asp:ListBox>
                                                        <input type="button" class="CodeLookupControl" id="lstSendUsersbtn" onclick="AddCustomizedListUser('autodiarysetup','CtrlAutoDiaryWizard_lstSendUsers','CtrlAutoDiaryWizard_SendUsersIdList','CtrlAutoDiaryWizard_SendUsersNameList')" value=""/>
                                                        <input type="button" class="BtnRemove" id="lstSendUsersbtndel" onclick="DelCustomizedListUser('autodiarysetup','CtrlAutoDiaryWizard_lstSendUsers','CtrlAutoDiaryWizard_SendUsersIdList','CtrlAutoDiaryWizard_SendUsersNameList')" value=""/>
                                                </div>
                                                <div id="GroupsList" style="display:none">
          										        Other Group(s)<br>
                                                        <asp:ListBox SelectionMode="Multiple" Width="250px" Height="100px" ID="lstSendGroups" runat="server" ></asp:ListBox>
          										        <input type="button" class="CodeLookupControl" id="lstSendGroupsbtn" onclick="selectCode('user_groups','lstSendGroups')" value="">
          										        <input type="button" class="BtnRemove" id="lstSendGroupsbtndel" onclick="deleteSelCode('lstSendGroups')" value="">
                                                </div>
          										        <%--<input type="hidden" value="" runat="server" id="SendUsersIdList">
          										        <input type="hidden" value="" runat="server" id="SendUsersNameList">
          										        <input type="hidden" value="" runat="server" id="SendGroupsIdList">
          										        <input type="hidden" value="" runat="server" id="SendGroupsNameList">--%>
                                                        <input type="text" runat="server" style="display:none" id="SendUsersIdList">
          										        <input type="text" runat="server" style="display:none" id="SendUsersNameList">
          										        <input type="text" runat="server" style="display:none" id="SendGroupsIdList">
          										        <input type="text" runat="server" style="display:none" id="SendGroupsNameList">
                                                        <input type="hidden" value="0" runat="server" id="Flag">
                                            </td>
                                       </tr>
                                  </table>
                             </td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                            </tr>
                        </table>
                    </asp:WizardStep> 
                    <asp:WizardStep ID="AutoFilterStep4" runat="server" Title="">
                        <asp:HiddenField ID="hdnAutoFilterStep4Visited" runat="server" Value="" />
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td valign="middle">
                                <img src="../../../Images/wpaautosetup.gif" />
                                </td>
                                <td align="left" valign="center">
      								                        This step will allow you to set triggers that determine how 
      								                        <br>
      								                        overdue diaries should be handled.  You can send 
      								                        <br>
      								                        notification that the diary is overdue to a single user or 
      								                        <br>
      								                        multiple users.  You can also re-route the diary to a single 
      								                        <br>
      								                        use or multiple users.  Both options allow you to set the
      								                        <br>
      								                        number of days before taking action on overdue diaries.
      								                        <br>
      						    </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                <%--Start:Add by kuladeep for mits:24661--%>
                                   <tr>
                                   <td>&nbsp;</td>
                                        <td>
                                        <input type="radio" value="2" runat="server" name="Escalate" id="Escalate_All_Lev"/>
                                        <asp:Label ID="lblEscalateAll"  runat="server" Text= "Escalate diary to all levels."></asp:Label>
                                        </td>
                                   </tr>
                                 
                                   <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <%--Changes by kuladeep for mits:24661--%>
                                           <%--<input type="checkbox" value="1" id="chkEscalate" runat="server" />--%>
                                            <input type="radio" value="1" runat="server" name="Escalate" id="Escalate_High_Lev"/>
                                            <asp:Label ID="lblEscalate"  runat="server" Text= "Escalate diary to the highest level."></asp:Label>
                                        </td>
                                   </tr>
                                   <tr>
                                   <td>&nbsp;</td>
                                        <td>
                                         <input type="radio" value="3" runat="server" name="Escalate" id="Escalate_Imme_Lev" />
                                        <asp:Label ID="lblEscalateImme"  runat="server" Text= "Escalate diary to the immediate level."></asp:Label>
                                        </td>
                                   </tr>
                                   <tr>
                                   <td>&nbsp;</td>
                                        <td>
                                         <input type="radio" value="4" runat="server" name="Escalate" id="Escalate_None_Lev"/>
                                        <asp:Label ID="lblEscalateNone"  runat="server" Text= "Escalate to none."></asp:Label>
                                        </td>
                                   </tr>
                                   <%--End:Add by kuladeep for mits:24661--%>
                                   <tr>
                                        <td colspan="2">&nbsp;</td>
                                   </tr>
                               <tr>
                                    <td width="20%">&nbsp;</td>
                                    <td width="80%"> Who should be notified when this diary's <br>completion is overdue?</td>
                               </tr>
                               <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <%--Change in Java script for mits:24661--%>
                                        <input type="radio" value="1" runat="server" name="RelationShip" id="SMS_Relationship"  onclick="HideLists('lstUsersNotified','lstUsersNotifiedbtn','lstUsersNotifiedbtndel','Escalate_All_Lev','Escalate_High_Lev','Escalate_Imme_Lev','Escalate_None_Lev','lblEscalate','hdnSms_RelationShipSelectedByUser')" />Security Management Relationship
                                        <input type="radio" value="0" name="RelationShip" runat="server" id="Selected_User" onclick="DisplayLists('lstUsersNotified','lstUsersNotifiedbtn','lstUsersNotifiedbtndel','Escalate_All_Lev','Escalate_High_Lev','Escalate_Imme_Lev','Escalate_None_Lev','lblEscalate','hdnSms_RelationShipSelectedByUser')" checked />Select Users: &nbsp;&nbsp;&nbsp;
                                        <asp:HiddenField ID="hdnSms_RelationShipSelectedByUser" runat="server" Value="" />
                                    </td>
                               </tr>
                               <tr>
                                    <td width="20%">&nbsp;</td>
                                    <td width="20%">
                                        <select  multiple size="3" style="width:248px; height: 70px;" runat="server" id="lstUsersNotified"></select>
                                        <input type="button" class="CodeLookupControl" id="lstUsersNotifiedbtn" runat="server" onclick="AddCustomizedListUser('autodiarysetup','CtrlAutoDiaryWizard_lstUsersNotified','CtrlAutoDiaryWizard_UsersNotifiedIdList','CtrlAutoDiaryWizard_UsersNotifiedNameList')" value="">
                                        <input type="button" class="BtnRemove" id="lstUsersNotifiedbtndel"  runat="server" onclick="DelCustomizedListUser('autodiarysetup','CtrlAutoDiaryWizard_lstUsersNotified','CtrlAutoDiaryWizard_UsersNotifiedIdList','CtrlAutoDiaryWizard_UsersNotifiedNameList')" value="">
                                        <input type="text"  value="" style="display:none" id="lstUsersNotified_lst">
                                    </td>
                               </tr>
                               <tr>
                                <td colspan="2">
                                 &nbsp;
                                 
                                </td>
                               </tr>
                               <tr>
                                    <td>&nbsp;</td>
                                    <td>How many days before notification is sent?
         						        <input type="text"  runat="server"  value="" id="NotificationDays" onblur="numLostFocusNonNegative(this);" size="3">
         						    </td>
                               </tr>
                               <tr>
                                <td colspan="2">&nbsp;</td>
                               </tr>
                               <tr>
                                    <td colspan="2">&nbsp;</td>
                               </tr>
                               <tr>
                                    <td>&nbsp;</td>
                                    <td>    Who should the diary be re-routed to when this<br>
         									diary's completion is overdue?<br>
         							    <select multiple size="3" style="width:248px; height: 70px;" id="lstUsersRouted" runat="server"></select>
         							    <input type="button" class="CodeLookupControl" id="lstUsersRoutedbtn" onclick="AddCustomizedListUser('autodiarysetup','CtrlAutoDiaryWizard_lstUsersRouted','CtrlAutoDiaryWizard_UsersRoutedIdList','CtrlAutoDiaryWizard_UsersRoutedNameList')" value="">
         							    <input type="button" class="BtnRemove" id="lstUsersRoutedbtndel" onclick="DelCustomizedListUser('autodiarysetup','CtrlAutoDiaryWizard_lstUsersRouted','CtrlAutoDiaryWizard_UsersRoutedIdList','CtrlAutoDiaryWizard_UsersRoutedNameList')" value="">
         							</td>
                               </tr>
                                <tr>
                                <td>&nbsp;</td>
                                <td>How many days before diary is re-routed?
                                    <input type="text" runat="server" value="" id="ReRoutedDays" size="3">
                                </td>
                               </tr>
                              </table>
                             </td>
                            </tr>
                            <tr>
                                 <td  colspan="2">
                                    <input type="hidden" value="" id="UsersNotifiedIdList" runat="server">
                                    <input type="hidden" value="" id="UsersNotifiedNameList" runat="server">
                                    <input type="hidden" value="" id="GroupsNotifiedIdList" runat="server">
                                    <input type="hidden" value="" id="GroupsNotifiedNameList" runat="server">
                                    <input type="hidden" value="" id="GroupsRoutedIdList" runat="server">
                                    <input type="hidden" value="" id="GroupsRoutedNameList" runat="server">
                                    <input type="hidden" value="" id="UsersRoutedIdList" runat="server">
                                    <input type="hidden"  value="" id="UsersRoutedNameList" runat="server">
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            
                            <tr>
                                 <td colspan="2" align="center"></td>
                            </tr>
                             <tr>
                                 <td colspan="2" align="center"></td>
                            </tr>
                             <tr>
                                 <td colspan="2" align="center"></td>
                            </tr>
                             <tr>
                                 <td colspan="2" align="center"></td>
                            </tr>
                            
                           </table>
                    </asp:WizardStep> 
                    <asp:WizardStep ID="AutoFilterStep5" runat="server" Title="">
                        <asp:HiddenField ID="hdnAutoFilterStep5Visited" runat="server" Value="" />
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                             <td valign="center">
                                <img src="../../../Images/wpaautosetup.gif" />
                            </td>
                             <td align="left" valign="center">
  								                            This step will allow you to define some general properties 
  								                            <br>
  								                            for this type of diary.  You can set the priority, estimated 
  								                            <br>
  								                            time, and export options.
  								                            <br></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td width="20%">&nbsp;</td>
                                    <td width="80%">
                                        What is the total estimated amount of time it <br>should take to complete this task?<br>
     								    <input type="text"  runat="server" value="" id="EstTime" size="6" />&nbsp;&nbsp;
     								    <input type="radio" runat="server" id="rdoMins" name="estTimeChk"  runat="server" value="0">Minute(s)
     								    <input type="radio" runat="server" id="rdoHrs" name="estTimeChk" runat="server" value="1">Hour(s)
     							    </td>
                               </tr>
                               <tr>
                                    <td colspan="2">&nbsp;</td>
                               </tr>
                               <tr>
                                    <td width="20%">&nbsp;</td>
                                    <td width="80%">Is the time associated with completion of this <br>type of diary billable?<br>
     									<input type="radio" id="rdoTimeAssoNo" name="TimeAssoFlag" runat="server" value="0">No
     									<input type="radio" id="rdoTimeAssoYes" name="TimeAssoFlag" runat="server" value="1">Yes
     								</td>
                               </tr>
                               <tr>
                                    <td colspan="2">&nbsp;</td>
                               </tr>
                               <tr>
                                    <td width="20%">&nbsp;</td>
                                    <td width="80%">
                                            <!--rupal, for R8 Auto Diary Enhancement-->
                                            <!--Do you want diaries of this type exported to<br>Schedule+?<br>-->
                                            Do you want to generate an email notification<br /> with information for diaries of this type?<br />
     										<input type="radio" id="rdoExportedNo" runat="server" value="0">No
     										<input type="radio" id="rdoExportedYes" runat="server" value="1">Yes
     								</td>
                               </tr>
                              </table>
                             </td>
                            </tr>
                            <tr>
                                 <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                 <td colspan="2">
                                        &nbsp;
                                </td>
                            </tr>
                        </table>
                    </asp:WizardStep> 
                    <asp:WizardStep ID="AutoFilterStep6" runat="server" Title="">
                        <asp:HiddenField ID="hdnAutoFilterStep6Visited" runat="server" Value="" />
                        <input type="hidden" runat="server" value="" id="diaryId" />
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td valign="center">
                                    <img src="../../../Images/wpaautosetup.gif" />
                                <td align="left" valign="center">
		                            This step will allow you to select any work activities and 
		                            <br>
		                            type in any text instructions that relate to performing this 
		                            <br>
		                            type of diary.  For example, this might contain instructions 
		                            <br>
		                            on filling out a form or where a form should be sent upon 
		                            <br>
		                            completion.
		                            <br>
      						    </td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
      								&nbsp;
                              	</td>
                            </tr>
                            <tr>
                                <td colspan="2">
					                What work activities do you want to associate with this auto diary definition?
					                <br>

                                    <asp:ListBox SelectionMode="Multiple"  ID="lstWorkActivity" runat="server" style="width:95%"></asp:ListBox>
					                <input type="button" class="CodeLookupControl" id="lstWorkActivitybtn" onclick="return openWorkActivityPage();" value="">
					                <input type="hidden" value="" id="WorkActivityIdList" runat="server">
					                <input type="hidden" value="" id="WorkActivityNameList" runat="server">
      							</td>
                            </tr>
                            <tr>
                                <td colspan="2">
      						        &nbsp;
                              							
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
		                            Enter any diary related instructions into the box below.
		                            <br>
		                            <textarea cols="20" rows="10" style="width:95%" id="Instruction" runat="server"></textarea>
		                        </td>
                            </tr>
                            <tr>
                                 <td colspan="2">
      								        &nbsp;
                                 </td>
                                </tr>
                                <tr>
                                    <td class="group" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
      								     &nbsp;
                                    </td>
                                </tr>
                        </table>
                    </asp:WizardStep> 
                </WizardSteps> 
        
        </asp:Wizard>
        <asp:Wizard ID="CtrlBestPracticeDiaryWizard" runat="server" NavigationStyle-HorizontalAlign="Center" DisplaySideBar="false" 
             ActiveStepIndex="0" Height="90%"  
            Width="98%" oncancelbuttonclick="CtrlBestPracticeDiaryWizard_CancelButtonClick" 
            onfinishbuttonclick="CtrlBestPracticeDiaryWizard_FinishButtonClick" 
            onnextbuttonclick="CtrlBestPracticeDiaryWizard_NextButtonClick" 
            onpreviousbuttonclick="CtrlBestPracticeDiaryWizard_PreviousButtonClick"> 
                <StartNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="Javascript:return window.close();" />
                  <asp:Button class="button" ID="StepPreviousButton" disabled="true" runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton" disabled="true" runat="server" CommandName="Finish" Text="Finish" CausesValidation="True" OnClientClick="FinishClick();return false;" /> 
                </StartNavigationTemplate>  
                <StepNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="Javascript:return window.close();" />
                  <asp:Button class="button" ID="StepPreviousButton"  runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton" disabled="true" runat="server" CommandName="Finish" Text="Finish" CausesValidation="True" OnClientClick="FinishClick();return false;" /> 
               </StepNavigationTemplate>
               <FinishNavigationTemplate>
                  <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="CancelClick();return false;" />
                  <asp:Button class="button" ID="StepPreviousButton"  runat="server" CausesValidation="True" CommandName="MovePrevious" Text="< Back" /> 
                  <asp:Button class="button" ID="StepNextButton" disabled="true" runat="server" CommandName="MoveNext" Text="Next >" CausesValidation="True" /> 
                  <asp:Button class="button" ID="StepFinishButton" runat="server"  CommandName="MoveComplete" Text="Finish" CausesValidation="True" /> 
               </FinishNavigationTemplate>
                <WizardSteps>
                    <asp:WizardStep ID="IntroductionStep"  runat="server" Title="Step1">
                         <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td valign="center">
                                    <img src="../../../Images/wpaautosetup.gif" />
                                </td>
                                 <td align="left" class="AutoDiarySetupTitleBig" valign="center">
                                    Work<br><br>Processing<br><br>Administrator<br><br>
                                 </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="AutoDiarySetupTitleSmall">
                                    Best Practices for Risk and Claims Management
                                 </td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                        </table>
                    </asp:WizardStep>
                    <asp:WizardStep ID="SpecificWizardSelectionStep"  runat="server" Title="Step1">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td valign="center">
                                    <img src="../../../Images/wpaautosetup.gif" />
                                </td>
                                <td align="left" valign="center">Select a method for creating new auto diary(s).To <br>create a single user-defined auto diary, select 'Auto 
                                    <br>Diary Wizard'.To create a group of pre-defined <br>auto diaries that are grouped by category, select<br> 'Best Practices Wizard'.<br><br>
                                </td>
                            </tr>
                            <tr>
                                 <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="AutoDiarySetupTitleSmall">
      							    Best Practices for Risk and Claims Management
                                </td>
                            </tr>
                            <tr>
                                 <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                 <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                 <td colspan="2">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select the method you wish to use for creating auto diary definition(s).
                                 </td>
                            </tr>
                            <tr>
                                 <td colspan="2">
                                    <input type="radio"  name="diaryWizard" value="1" id="rdoAutoDiaryWizard" checked runat="server">Auto Diary Wizard</td>
                            </tr>
                            <tr>
                             <td colspan="2">
                                    <input type="radio" name="diaryWizard" value="2" id="rdoBestPracticeWizard" runat="server">Best Practices Wizard</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="group" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center"></td>
                            </tr>
                        </table>
                    </asp:WizardStep>
                    <asp:WizardStep ID="BestPracticeWizardStep1"  runat="server" Title="Step1">
                    </asp:WizardStep> 
                    <asp:WizardStep ID="BestPracticeWizardStep2"  runat="server" Title="Step1">
                    </asp:WizardStep> 
                    <asp:WizardStep ID="BestPracticeWizardStep3"  runat="server" Title="Step1">
                    </asp:WizardStep> 
                    <asp:WizardStep ID="WizardStep5"  runat="server" Title="Step1">
                    </asp:WizardStep> 
                </WizardSteps> 
            </asp:Wizard>
    </div>
    </form>
</body>
</html>
