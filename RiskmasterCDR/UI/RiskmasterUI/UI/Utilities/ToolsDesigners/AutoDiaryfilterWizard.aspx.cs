﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.Utilities.ToolsDesigners
{
    public partial class AutoDiaryfilterWizard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument autoDiaryDoc = null;
            Button stepFinishedButton = null;

            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes

                if (!IsPostBack)
                {
                    if (Request.QueryString["defid"] != null)
                    {
                        ViewState["defid"] = Request.QueryString["defid"];
                    }
                    else
                    {
                        ViewState["defid"] = "";
                    }

                    autoDiaryDoc = GetAutoDiaryFilter();
                    ViewState["autoFilterDiaryDoc"] = autoDiaryDoc.OuterXml;

                    //setting style of finish button in case of edit
                    if (ViewState["defid"].ToString() != "")
                    {
                        if (CtrlAutoDiaryWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                        {
                            stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoDiaryWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StepFinishButton");
                            stepFinishedButton.Enabled = true;
                        }

                        ShowAutoFilterWizard();
                    }
                    else //in case of new 
                    {
                        if (CtrlAutoDiaryWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                        {
                            stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoDiaryWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StepFinishButton");
                            stepFinishedButton.Enabled = false;
                        }

                        ShowBestPracticeDiaryWizard();

                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void UpatePaneForAutoFilterStep2_Load(object sender, EventArgs e)
        {
           string  clientArgument = string.Empty;
           XmlDocument selectedFiltersDoc = null;
           XmlDocument selectedLevelDoc = null;
           XmlNode selectedLevelNode = null;
           XmlNode selectedFiltersNode = null;


           try
           {
               clientArgument = Request.Params.Get("__EVENTARGUMENT");

               if (!string.IsNullOrEmpty(clientArgument))
               {

                   if (!clientArgument.Contains("FiltersDoubleClickedMode"))
                   {
                       hdnData.Value = clientArgument;                       
                       selectedLevelDoc = new XmlDocument();
                       selectedLevelDoc.LoadXml(hdnSelectedLevelNode.Value);
                       //selectedLevelDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedLevelNode.Value));//Bharani - MITS : 35878                       
                       selectedLevelNode = selectedLevelDoc.SelectSingleNode("level");

                       if (selectedLevelNode.Attributes["data"] != null)
                       {
                           selectedLevelNode.Attributes["data"].Value = hdnData.Value;
                       }
                       else
                       {
                           XmlAttribute newDataAttribute = null;
                           newDataAttribute = selectedLevelDoc.CreateAttribute("data");
                           newDataAttribute.Value = hdnData.Value;

                           selectedLevelNode.Attributes.Append(newDataAttribute);
                       }

                       hdnSelectedLevelNode.Value = selectedLevelNode.OuterXml;
                       //hdnSelectedLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);//Bharani - MITS : 35878

                       selectedFiltersDoc = new XmlDocument();
                       selectedFiltersDoc.LoadXml(hdnSelectedFiltersNode.Value);
                       //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value));//Bharani - MITS : 35878

                       selectedLevelNode = null;

                       selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedFilters");
                       selectedLevelNode = selectedFiltersNode.SelectSingleNode("level[@templateid ='" + hdnTemplateId.Value + "' and @id = '" + hdnId.Value + "']");

                       if (selectedLevelNode == null)
                       {
                           selectedFiltersNode.InnerXml = selectedFiltersNode.InnerXml + AppHelper.HTMLCustomDecode(hdnSelectedLevelNode.Value);//Bharani - MITS : 35878
                           hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                           //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35878

                           //adding this item in SelectedFilters listbox
                           SelectedFilters.Items.Add(new ListItem(hdnFilterName.Value, hdnId.Value));

                           //removig this item from AvaibleFilters listbox
                           AvailableFilters.Items.Remove(new ListItem(hdnFilterName.Value, hdnId.Value));
                       }
                       else
                       {
                           selectedLevelNode.Attributes["data"].Value = hdnData.Value;
                           hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                           //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35878
                       }

                       hdnId.Value = "";
                       hdnFilterName.Value = "";
                       hdnSqlFill.Value = "";
                       hdnFilterType.Value = "";
                       hdnData.Value = "";
                       hdnMode.Value = "";
                       hdnDefValue.Value = "";
                       hdnSelectedLevelNode.Value = "";
                   }
                   else
                   {
                       string[] arrArg;
                       arrArg = clientArgument.Split('=');

                       UpdateControlsOnAddEditAutoFilter(arrArg[1]);
                   }
               }
           }
           catch (Exception ee)
           {
               ErrorHelper.logErrors(ee);
               BusinessAdaptorErrors err = new BusinessAdaptorErrors();
               err.Add(ee, BusinessAdaptorErrorType.SystemError);
               ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
           }
        }

        protected void UpateForAutoFilterStep2()
        {
            XmlDocument selectedFiltersDoc = null;
            XmlDocument selectedLevelDoc = null;
            XmlNode selectedLevelNode = null;
            XmlNode selectedFiltersNode = null;

            try
            {
                selectedLevelDoc = new XmlDocument();
                selectedLevelDoc.LoadXml(hdnSelectedLevelNode.Value);
                //selectedLevelDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedLevelNode.Value));//Bharani - MITS : 35878

                selectedLevelNode = selectedLevelDoc.SelectSingleNode("level");



                hdnSelectedLevelNode.Value = selectedLevelNode.OuterXml;
                //hdnSelectedLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);//Bharani - MITS : 35878

                selectedFiltersDoc = new XmlDocument();
                selectedFiltersDoc.LoadXml(hdnSelectedFiltersNode.Value);
                //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value));//Bharani - MITS : 35878

                selectedLevelNode = null;

                selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedFilters");
                selectedLevelNode = selectedFiltersNode.SelectSingleNode("level[@templateid ='" + hdnTemplateId.Value + "' and @id = '" + hdnId.Value + "']");

                if (selectedLevelNode == null)
                {
                    selectedFiltersNode.InnerXml = selectedFiltersNode.InnerXml + hdnSelectedLevelNode.Value;
                    //selectedFiltersNode.InnerXml = selectedFiltersNode.InnerXml + AppHelper.HTMLCustomDecode(hdnSelectedLevelNode.Value);//Bharani - MITS : 35878
                    hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                    //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35878

                    //adding this item in SelectedFilters listbox
                    SelectedFilters.Items.Add(new ListItem(hdnFilterName.Value, hdnId.Value));

                    //removig this item from AvaibleFilters listbox
                    AvailableFilters.Items.Remove(new ListItem(hdnFilterName.Value, hdnId.Value));
                }
                else
                {
                    //selectedLevelNode.Attributes["data"].Value = hdnData.Value;
                    hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                    //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35878
                }

                //hdnId.Value = "";
                //hdnFilterName.Value = "";
                //hdnSqlFill.Value = "";
                //hdnFilterType.Value = "";
                //hdnData.Value = "";
                //hdnMode.Value = "";
                //hdnDefValue.Value = "";
                //hdnSelectedLevelNode.Value = "";
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                hdnId.Value = "";
                hdnFilterName.Value = "";
                hdnSqlFill.Value = "";
                hdnFilterType.Value = "";
                hdnData.Value = "";
                hdnMode.Value = "";
                hdnDefValue.Value = "";
                hdnSelectedLevelNode.Value = "";

                selectedFiltersDoc = null;
                selectedLevelDoc = null;
                selectedLevelNode = null;
                selectedFiltersNode = null;
            }
        }
        #region AutoDiaryWizard

            #region Fetching AutoDiaryWizard Details From Webservice

                private XmlNode GetInputDocForAutoDiary()
                {
                    string messageTemplageString = string.Empty;

                    messageTemplageString = GetAutoDiaryFilterTemplate();

                    XmlDocument messageDoc = new XmlDocument();
                    messageDoc.LoadXml(messageTemplageString);

                    messageDoc.SelectSingleNode("DiaryWizard/DefId").InnerText = ViewState["defid"].ToString();

                    return messageDoc.SelectSingleNode("/"); 
                }

                private XmlDocument GetAutoDiaryFilter()
                {
                    string cwsResponse = string.Empty;
                    XmlDocument autoDiaryDoc = null;
                    XmlNode inputDocNode = null;
                    string serviceMethodToCall = string.Empty;

                    try
                    {
                        serviceMethodToCall = "WPAAutoDiarySetupAdaptor.Get";

                        inputDocNode = GetInputDocForAutoDiary();
                        cwsResponse = CallCWSFunctionFromAppHelper(inputDocNode, serviceMethodToCall);

                        autoDiaryDoc = new XmlDocument();
                        autoDiaryDoc.LoadXml(cwsResponse);

                        return autoDiaryDoc;
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException(ex.Message);
                    }
                }

                private string GetAutoDiaryFilterTemplate()
                {   
                    //rupal:r8 auto diary enh
                    //added ExportToEmail tag and removed <ExportFlag></ExportFlag>
                    XElement oTemplate = XElement.Parse(@"
                    <DiaryWizard name='WPAAutoDiarySetup'>
                      <DefId />
                      <DiaryTemplate selectedid='' selectedname=''></DiaryTemplate>
                      <DiaryName></DiaryName>
                      <DiaryDate></DiaryDate>
                      <NoOfDays></NoOfDays>
                      <PriorityLevel selectedid=''></PriorityLevel>
                      <DefIndex></DefIndex>
                      <Filters>
                        <AvlFilters></AvlFilters>
                        <SelectedFilters></SelectedFilters>
                      </Filters>
                      <SendDiariesTo>
                        <UserCreated></UserCreated>
                        <UserModified></UserModified>
                        <AdjAssig></AdjAssig>
                        <AdjAssigSupervisor></AdjAssigSupervisor>
                        <EventSuppUser></EventSuppUser>
                        <SuppField selectedid='' selectedname=''></SuppField>
                        <OtherUsers></OtherUsers>
                        <OtherGroups></OtherGroups>
                        <SendUsers></SendUsers>
                        <SendUsersIdList></SendUsersIdList>
                        <SendUsersNameList></SendUsersNameList>
                        <SendGroups></SendGroups>
                        <SendGroupsIdList></SendGroupsIdList>
                        <SendGroupsNameList></SendGroupsNameList>
                        <GroupsNotified></GroupsNotified>
                        <GroupsNotifiedIdList></GroupsNotifiedIdList>
                        <GroupsNotifiedNameList></GroupsNotifiedNameList>
                        <GroupsRouted></GroupsRouted>
                        <GroupsRoutedIdList></GroupsRoutedIdList>
                        <GroupsRoutedNameList></GroupsRoutedNameList>
                        <Flag></Flag>
                      </SendDiariesTo>
                      <Escalate_Highestlevel></Escalate_Highestlevel>
                      <Security_Mgmt_Relationship></Security_Mgmt_Relationship>
                      <UsersNotified>
                        <UsersNotifiedLevelList></UsersNotifiedLevelList>
                        <UsersNotifiedIdList></UsersNotifiedIdList>
                        <UsersNotifiedNameList></UsersNotifiedNameList>
                      </UsersNotified>
                      <NotificationDays></NotificationDays>
                      <UsersRouted>
                        <UsersRoutedLevels></UsersRoutedLevels>
                        <UsersRoutedIdList></UsersRoutedIdList>
                        <UsersRoutedNameList></UsersRoutedNameList>
                        <ReRoutedDays></ReRoutedDays>
                      </UsersRouted>
                      <EstTime></EstTime>
                      <MinsHrs></MinsHrs>
                      <TimeAssoFlag></TimeAssoFlag>                      
                      <ExportToEmailFlag></ExportToEmailFlag>   
                      <WorkActivity>
                        <WorkActivityLevelList></WorkActivityLevelList>
                        <WorkActivityIdList></WorkActivityIdList>
                        <WorkActivityNameList></WorkActivityNameList>
                      </WorkActivity>
                      <Instruction></Instruction>
                    </DiaryWizard>
                    ");
                    return oTemplate.ToString();
                }

                private XmlDocument GetStoredAutoDiaryDoc()
                {
                    string strAutoDiaryDoc = string.Empty;
                    XmlDocument autoDiaryDoc = null;

                    strAutoDiaryDoc = ViewState["autoFilterDiaryDoc"].ToString();

                    autoDiaryDoc = new XmlDocument();
                    autoDiaryDoc.LoadXml(strAutoDiaryDoc);

                    return autoDiaryDoc;
                }

            #endregion

            #region AutoDiaryWizard Step1

                private void PopulateAutoFitlerStep1Controls()
                {
                    XmlDocument autoDiaryDoc = null;
                    XmlNode diaryWizardNode = null;
                    XmlNode diaryTemplateNode = null;
                    XmlNodeList diaryTemplateNodeList = null;
                    ListItem cboTemplateItem = null;

                    if (hdnAutoFilterStep1Visited.Value == "")
                    {
                        autoDiaryDoc = GetStoredAutoDiaryDoc();
                        diaryWizardNode = autoDiaryDoc.SelectSingleNode("ResultMessage/Document/DiaryWizard");

                        diaryTemplateNode = diaryWizardNode.SelectSingleNode("DiaryTemplate");
                        diaryTemplateNodeList = diaryTemplateNode.SelectNodes("level");

                        if (diaryTemplateNodeList.Count > 0)
                        {
                            cboTemplate.Items.Clear();
                            foreach (XmlNode levelNode in diaryTemplateNodeList)
                            {
                                cboTemplateItem = new ListItem();
                                cboTemplateItem.Text = levelNode.Attributes["name"].Value;
                                cboTemplateItem.Value = levelNode.Attributes["value"].Value;
                                cboTemplate.Items.Add(cboTemplateItem);
                            }

                            if (diaryTemplateNode.Attributes["selectedid"].Value != "")
                            {
                                cboTemplate.SelectedValue = diaryTemplateNode.Attributes["selectedid"].Value;
                            }
                        }

                        //adding data for priority level 
                        cboPriority.Items.Clear();
                        cboPriority.Items.Add(new ListItem("Optional", "1"));
                        cboPriority.Items.Add(new ListItem("Important", "2"));
                        cboPriority.Items.Add(new ListItem("Required", "3"));

                        if (diaryWizardNode.SelectSingleNode("PriorityLevel").Attributes["selectedid"].Value != "0")
                        {
                            cboPriority.SelectedValue = diaryWizardNode.SelectSingleNode("PriorityLevel").Attributes["selectedid"].Value;
                        }


                        if (diaryWizardNode.SelectSingleNode("NoOfDays").InnerText != "")
                        {
                            txtDaysNoAfterDueDate.Text = diaryWizardNode.SelectSingleNode("NoOfDays").InnerText;
                        }
                        else
                        {
                            txtDaysNoAfterDueDate.Text = "0";
                        }

                        txtDiaryName.Text = diaryWizardNode.SelectSingleNode("DiaryName").InnerText;

                        txtDiaryDate.Value = diaryWizardNode.SelectSingleNode("DiaryDate").InnerText;

                        diaryId.Value = diaryWizardNode.SelectSingleNode("DefId").InnerText;
                    }
                    
                    hdnAutoFilterStep1Visited.Value = "true";

                }  //DiaryWizardstep3

            #endregion

            #region AutoDiaryWizard Step2

                private void PopulateAutoFilterStep2Controls()
                {
                    XmlDocument autoDiaryDoc = null;
                    XmlNodeList availableFilterNodeList = null;
                    XmlNode selectedFiltersNode = null;
                    XmlNodeList selectedFilterNodeList = null;
                    ListItem availableFilterItem = null;
                    ListItem selectedFilterItem = null;
                    
                    if (hdnTemplateId.Value != cboTemplate.SelectedItem.Value)
                    {
                        autoDiaryDoc = GetStoredAutoDiaryDoc();
                        availableFilterNodeList = autoDiaryDoc.SelectNodes("ResultMessage/Document/DiaryWizard/Filters/AvlFilters/level[@templateid ='" + cboTemplate.SelectedItem.Value + "']");

                        AvailableFilters.Items.Clear();

                        if (availableFilterNodeList.Count > 0)
                        {
                            foreach (XmlNode levelNode in availableFilterNodeList)
                            {
                                availableFilterItem = new ListItem();

                                availableFilterItem.Text = levelNode.Attributes["name"].Value;
                                availableFilterItem.Value = levelNode.Attributes["id"].Value;

                                AvailableFilters.Items.Add(availableFilterItem);
                            }

                        }

                        hdnSelectedFiltersNode.Value = "";
                        selectedFiltersNode = autoDiaryDoc.SelectSingleNode("ResultMessage/Document/DiaryWizard/Filters/SelectedFilters");
                        hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                        //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35878

                        SelectedFilters.Items.Clear();
                        if (AvailableFilters.Items.Count > 0)
                        {
                            selectedFilterNodeList = selectedFiltersNode.SelectNodes("level");
                            if (selectedFilterNodeList.Count > 0)
                            {
                                foreach (XmlNode levelNode in selectedFilterNodeList)
                                {
                                    selectedFilterItem = new ListItem();

                                    selectedFilterItem.Text = levelNode.Attributes["name"].Value;
                                    selectedFilterItem.Value = levelNode.Attributes["id"].Value;

                                    SelectedFilters.Items.Add(selectedFilterItem);
                                    AvailableFilters.Items.Remove(selectedFilterItem);
                                }
                            }
                        }
                    }

                    hdnTemplateId.Value = cboTemplate.SelectedItem.Value;
                } //DiaryWizardstep4

                protected void btnAddAvlFilter_Click(object sender, EventArgs e)
                {
                    UpdateControlsOnAddEditAutoFilter("add");
                }

                protected void btnEditAvlFilter_Click(object sender, EventArgs e)
                {
                    UpdateControlsOnAddEditAutoFilter("edit");
                }

                protected void btnRemoveAvlFilter_Click(object sender, EventArgs e)
                {
                    UpdateControlsOnAddEditAutoFilter("remove");
                }

                private void UpdateControlsOnAddEditAutoFilter(string mode)
                {
                    XmlDocument storedAutoDiaryDoc = null;
                    XmlDocument selectedFiltersDoc = null;
                    XmlNode selectedLevelNode = null;
                    XmlNode selectedFiltersNode = null;

                    string urlToOpen = string.Empty;
                    string scriptForOpen = string.Empty;

                    hdnMode.Value = mode;
                    if (mode == "add")
                    {
                        if (AvailableFilters.SelectedItem != null)
                        {
                            hdnFilterName.Value = AvailableFilters.SelectedItem.Text;
                            hdnId.Value = AvailableFilters.SelectedItem.Value;
                            hdnTemplateId.Value = cboTemplate.SelectedItem.Value;

                            storedAutoDiaryDoc = GetStoredAutoDiaryDoc();
                            selectedLevelNode = storedAutoDiaryDoc.SelectSingleNode("ResultMessage/Document/DiaryWizard/Filters/AvlFilters/level[@templateid ='" + hdnTemplateId.Value + "' and @id = '" + hdnId.Value + "']");

                            PopulateHiddenFieldsForAddEditAutoFilter(selectedLevelNode);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (mode == "edit")
                    {
                        if (SelectedFilters.SelectedItem != null)
                        {
                            hdnFilterName.Value = SelectedFilters.SelectedItem.Text;
                            hdnId.Value = SelectedFilters.SelectedItem.Value;
                            hdnTemplateId.Value = cboTemplate.SelectedItem.Value;

                            selectedFiltersDoc = new XmlDocument();
                            selectedFiltersDoc.LoadXml(hdnSelectedFiltersNode.Value);
                            //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value));//Bharani - MITS : 35878
                            selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedFilters");
                            selectedLevelNode = selectedFiltersNode.SelectSingleNode("level[@templateid ='" + hdnTemplateId.Value + "' and @id = '" + SelectedFilters.SelectedItem.Value + "']");

                            PopulateHiddenFieldsForAddEditAutoFilter(selectedLevelNode);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (mode == "remove")
                    {
                        if (SelectedFilters.SelectedItem != null)
                        {
                            hdnTemplateId.Value = cboTemplate.SelectedItem.Value;

                            selectedFiltersDoc = new XmlDocument();
                            selectedFiltersDoc.LoadXml(hdnSelectedFiltersNode.Value);
                            //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value));//Bharani - MITS : 35878
                            selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedFilters");
                            selectedLevelNode = selectedFiltersNode.SelectSingleNode("level[@templateid ='" + hdnTemplateId.Value + "' and @id = '" + SelectedFilters.SelectedItem.Value + "']");

                            if (selectedLevelNode != null)
                            {
                                selectedFiltersNode.RemoveChild(selectedLevelNode);

                                hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                                //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35878

                                AvailableFilters.Items.Add(SelectedFilters.SelectedItem);
                                SelectedFilters.Items.Remove(SelectedFilters.SelectedItem);
                                AvailableFilters.SelectedIndex = -1;
                            }
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }

                    if (hdnFilterType.Value != "6") //No option
                    {
                        hdnSqlFill.Value = hdnSqlFill.Value.Replace("'", "`").Replace("(", "^^^").Replace(")", "@@@");
                        hdnFilterName.Value = hdnFilterName.Value.Replace("(", "^^^").Replace(")", "@@@");
                        //rupal:r8 auto diary setup enh
                        //urlToOpen = "AddEditAutoDiaryFilter.aspx?selectedid=" + hdnId.Value + "&templateid=" + hdnTemplateId.Value + "&mode=" + hdnMode.Value + "&filtername=" + hdnFilterName.Value + "&sqlfill=" + hdnSqlFill.Value + "&filtertype=" + hdnFilterType.Value + "&defvalue=" + hdnDefValue.Value + "&data=" + hdnData.Value;
                        //urlToOpen = "AddEditAutoDiaryFilter.aspx?selectedid=" + hdnId.Value + "&templateid=" + hdnTemplateId.Value + "&mode=" + hdnMode.Value + "&filtername=" + hdnFilterName.Value + "&sqlfill=" + hdnSqlFill.Value + "&filtertype=" + hdnFilterType.Value + "&defvalue=" + hdnDefValue.Value + "&data=" + hdnData.Value + "&database=" + hdnDatabase.Value;
                        //asharma326 MITS 35669 
                        urlToOpen = "AddEditAutoDiaryFilter.aspx?selectedid=" + hdnId.Value + "&templateid=" + hdnTemplateId.Value + "&mode=" + hdnMode.Value + "&filtername=" + hdnFilterName.Value + "&filtertype=" + hdnFilterType.Value + "&defvalue=" + hdnDefValue.Value + "&data=" + hdnData.Value + "&database=" + hdnDatabase.Value;
                        //rupal:end
                        scriptForOpen = "OpenAddEditAutoDiaryFilter('" + urlToOpen + "');";
                        ScriptManager.RegisterStartupScript(UpatePaneForAutoFilterStep2, typeof(string), "AddEditAutoDiaryFilter", scriptForOpen, true);
                        hdnFilterName.Value = hdnFilterName.Value.Replace("^^^", "(").Replace("@@@", ")");
                        hdnSqlFill.Value = hdnSqlFill.Value.Replace("'", "`").Replace("^^^", "(").Replace("@@@", ")");
                    }
                    else
                    {
                        UpateForAutoFilterStep2();
                    }

                }

                private void PopulateHiddenFieldsForAddEditAutoFilter(XmlNode selectedLevelNode)
                {
                    XmlAttribute sqlFillAtb = null;
                    XmlAttribute filterTypeAtb = null;
                    XmlAttribute defValueAtb = null;
                    XmlAttribute dataNodeAtb = null;

                    if (selectedLevelNode != null)
                    {
                        hdnSelectedLevelNode.Value = selectedLevelNode.OuterXml;
                        //hdnSelectedLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml); //Bharani - MITS : 35878

                        sqlFillAtb = selectedLevelNode.Attributes["SQLFill"];
                        if (sqlFillAtb != null)
                        {
                            hdnSqlFill.Value = sqlFillAtb.Value;
                        }

                        filterTypeAtb = selectedLevelNode.Attributes["FilterType"];
                        if (filterTypeAtb != null)
                        {
                            hdnFilterType.Value = filterTypeAtb.Value;
                        }

                        defValueAtb = selectedLevelNode.Attributes["DefValue"];
                        if (defValueAtb != null)
                        {
                            hdnDefValue.Value = defValueAtb.Value;
                        }

                        dataNodeAtb = selectedLevelNode.Attributes["data"];
                        if (dataNodeAtb != null)
                        {
                            hdnData.Value = dataNodeAtb.Value;
                        }
                        //rupal:start, r8 auto diary setup enh
                        if (selectedLevelNode.Attributes["Database"] != null)
                        {
                            hdnDatabase.Value = selectedLevelNode.Attributes["Database"].Value;
                        }
                        //rupal:end
                    }
                }

            #endregion

            #region AutoDiaryWizard Step3
                private void PopulateAutoFilterStep3Controls()   //DiaryWizardstep5
                {
                    XmlDocument autoDiaryDoc = null;
                    XmlNode sendDiariesToNode = null;
                    XmlNode eventSuppNode = null;
                    XmlNodeList suppFieldNodeList = null;
                    ListItem suppFieldItem = null;
                    string[] arrSendUsersId;
                    string[] arrSendUsersName;
                    string[] arrSendGroupsId; //pkandhari Jira 6412
                    string[] arrSendGroupsName; //pkandhari Jira 6412
                    int iSelectedSupId = 0;
                    bool bIsScucess = false;

                    if (hdnAutoFilterStep3Visited.Value == "")
                    {
                        autoDiaryDoc = GetStoredAutoDiaryDoc();
                        sendDiariesToNode = autoDiaryDoc.SelectSingleNode("ResultMessage/Document/DiaryWizard/SendDiariesTo");

                        if (sendDiariesToNode.SelectSingleNode("UserCreated").InnerText == "1")
                        {
                            chkUserCreated.Checked = true;
                        }
                        else
                        {
                            chkUserCreated.Checked = false;
                        }

                        if (sendDiariesToNode.SelectSingleNode("UserModified").InnerText == "1")
                        {
                            chkUserModified.Checked = true;
                        }
                        else
                        {
                            chkUserModified.Checked = false;
                        }

                        if (sendDiariesToNode.SelectSingleNode("AdjAssig").InnerText == "1")
                        {
                            chkAdjAssign.Checked = true;
                        }
                        else
                        {
                            chkAdjAssign.Checked = false;
                        }


                        if (sendDiariesToNode.SelectSingleNode("AdjAssigSupervisor").InnerText == "1")
                        {
                            chkAdjAssigSupervisor.Checked = true;
                        }
                        else
                        {
                            chkAdjAssigSupervisor.Checked = false;
                        }
                    }

                    if (cboTemplate.SelectedItem.Value == "2")
                    {
                        trChkEventSupp.Visible = true;
                        cboSuppField.Visible = true;

                        if (hdnAutoFilterStep3Visited.Value == "")
                        {
                            eventSuppNode = sendDiariesToNode.SelectSingleNode("EventSuppUser");

                            if (eventSuppNode != null)
                            {

                                //mits 25439 -corrected XPath - tanwar2 - 4th Jan 2012 - start
                                //suppFieldNodeList = autoDiaryDoc.SelectNodes("SuppField/level");
                                suppFieldNodeList = autoDiaryDoc.SelectNodes("//SuppField/level");
                                //mits 25439 -corrected XPath - tanwar2 - 4th Jan 2012 - end

                                //mits 25439 - code modifications follows - Jan 4th, 2012 - tanwar2
                                //update the combo box
                                if (suppFieldNodeList.Count > 0)
                                {
                                    cboSuppField.Items.Clear();
                                    foreach (XmlNode levelNode in suppFieldNodeList)
                                    {
                                        suppFieldItem = new ListItem();

                                        suppFieldItem.Text = levelNode.InnerText;
                                        suppFieldItem.Value = levelNode.Attributes["id"].Value;

                                        cboSuppField.Items.Add(suppFieldItem);
                                    }
                                }

                                //in case the values are loaded from database ("Edit option is selected for Auto Diary")
                                if (eventSuppNode.InnerText == "1")
                                {
                                    if (autoDiaryDoc.SelectSingleNode("//SuppField") != null)
                                    {
                                        if (autoDiaryDoc.SelectSingleNode("//SuppField").Attributes["selectedid"] != null)
                                        {
                                            //parsing will fail in case the last chosen value of Supp items got changed in db
                                            //bool isParsed = Int32.TryParse
                                            //    (autoDiaryDoc.SelectSingleNode("//SuppField").Attributes["selectedid"].Value,
                                            //    out iSelectedSupId);
                                            iSelectedSupId = Conversion.CastToType<int>(Convert.ToString(autoDiaryDoc.SelectSingleNode("//SuppField").Attributes["selectedid"].Value), out bIsScucess);//MITS 25439 

                                            if (autoDiaryDoc.SelectSingleNode("//SuppField/level[@id='" +
                                                //iSelectedSupId.ToString() + "']") != null && isParsed == true) Commented for MITS 25439
                                                iSelectedSupId.ToString() + "']") != null)
                                            {
                                                cboSuppField.Enabled = true;
                                                chkEventSupp.Checked = true;
                                                cboSuppField.SelectedValue = iSelectedSupId.ToString();
                                            }
                                            else
                                            {
                                                cboSuppField.Enabled = false;
                                            }
                                        }
                                        else
                                        {
                                            cboSuppField.Enabled = false;
                                        }
                                    }
                                }
                                //mits 25439 - code modificaion ends - Jan 4th 2012 - tanwar2
                                else
                                {
                                    cboSuppField.Enabled = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        trChkEventSupp.Visible = false;
                        cboSuppField.Visible = false;
                    }


                    if (hdnAutoFilterStep3Visited.Value == "")
                    {
                        if (sendDiariesToNode.SelectSingleNode("SendUsersIdList").InnerText != "")
                        {
                            SendUsersIdList.Value = sendDiariesToNode.SelectSingleNode("SendUsersIdList").InnerText;
                        }

                        if (sendDiariesToNode.SelectSingleNode("SendUsersNameList").InnerText != "")
                        {
                            SendUsersNameList.Value = sendDiariesToNode.SelectSingleNode("SendUsersNameList").InnerText;
                        }
                        //pkandhari Jira 6412 starts
                        if (sendDiariesToNode.SelectSingleNode("SendGroupsIdList").InnerText != "")
                        {
                            SendGroupsIdList.Value = sendDiariesToNode.SelectSingleNode("SendGroupsIdList").InnerText;
                        }

                        if (sendDiariesToNode.SelectSingleNode("SendGroupsNameList").InnerText != "")
                        {
                            SendGroupsNameList.Value = sendDiariesToNode.SelectSingleNode("SendGroupsNameList").InnerText;
                        }

                        if (sendDiariesToNode.SelectSingleNode("GroupsNotifiedIdList").InnerText != "")
                        {
                            GroupsNotifiedIdList.Value = sendDiariesToNode.SelectSingleNode("GroupsNotifiedIdList").InnerText;
                        }

                        if (sendDiariesToNode.SelectSingleNode("GroupsNotifiedNameList").InnerText != "")
                        {
                            GroupsNotifiedNameList.Value = sendDiariesToNode.SelectSingleNode("GroupsNotifiedNameList").InnerText;
                        }

                        if (sendDiariesToNode.SelectSingleNode("GroupsRoutedIdList").InnerText != "")
                        {
                            GroupsRoutedIdList.Value = sendDiariesToNode.SelectSingleNode("GroupsRoutedIdList").InnerText;
                        }

                        if (sendDiariesToNode.SelectSingleNode("GroupsRoutedNameList").InnerText != "")
                        {
                            GroupsRoutedNameList.Value = sendDiariesToNode.SelectSingleNode("GroupsRoutedNameList").InnerText;
                        }
                        //pkandhari Jira 6412 ends
                    }

                    //Start: Sumit (10/25/2010)-  MITS# 22784:Added If clause to prevent empty values getting-in
                    if (SendUsersIdList.Value.Trim() != string.Empty && SendUsersNameList.Value.Trim() != string.Empty)
                    {
                        arrSendUsersId = SendUsersIdList.Value.Split('|');
                        arrSendUsersName = SendUsersNameList.Value.Split('|');

                        lstSendUsers.Items.Clear();
                        //skhare7: Mits 22083 first row was displying blank in list box
                        //Sumit - Added changed count from < to <=
                        for (int count = 0; count <= arrSendUsersId.GetUpperBound(0); count++)
                        {
                            lstSendUsers.Items.Add(new ListItem(arrSendUsersName[count], arrSendUsersId[count]));
                        }
                        //skhare7: Mits 22083 first row was displying blank in list box
                    }
                    //End:Sumit
                    //pkandhari Jira 6412 starts
                    if (SendGroupsIdList.Value.Trim() != string.Empty && SendGroupsNameList.Value.Trim() != string.Empty)
                    {
                        arrSendGroupsId = SendGroupsIdList.Value.Split('|');
                        arrSendGroupsName = SendGroupsNameList.Value.Split('|');

                        for (int count = 0; count <= arrSendGroupsName.GetUpperBound(0); count++)
                        {
                            lstSendUsers.Items.Add(new ListItem(arrSendGroupsName[count], arrSendGroupsId[count]));
                        }
                    }
                    //pkandhari Jira 6412 ends
                    hdnAutoFilterStep3Visited.Value = "true";
                }
            #endregion

            #region AutoDiaryWizard Step4
                private void PopulateAutoFilterStep4Controls()
                {
                    XmlDocument autoDiaryDoc = null;
                    XmlNode diaryWizardNode = null;

                    autoDiaryDoc = GetStoredAutoDiaryDoc();
                    diaryWizardNode = autoDiaryDoc.SelectSingleNode("ResultMessage/Document/DiaryWizard");

                    if (hdnAutoFilterStep4Visited.Value == "")
                    {
                        //Change by kuladeep for mits:24661 start
                        //if (diaryWizardNode.SelectSingleNode("Escalate_Highestlevel").InnerText == "1")
                        //{
                        //   // chkEscalate.Checked = true;
                        //}
                        //else
                        //{
                        //    //chkEscalate.Checked = false;
                        //}
                        if (diaryWizardNode.SelectSingleNode("Escalate_Highestlevel").InnerText == "1")
                        {
                            Escalate_High_Lev.Checked = true;
                        }
                        else if (diaryWizardNode.SelectSingleNode("Escalate_Highestlevel").InnerText == "2")
                        {
                            Escalate_All_Lev.Checked=true;
                        }
                        else if (diaryWizardNode.SelectSingleNode("Escalate_Highestlevel").InnerText == "3")
                        {
                            Escalate_Imme_Lev.Checked=true;
                        }
                        else if (diaryWizardNode.SelectSingleNode("Escalate_Highestlevel").InnerText == "4")
                        {
                            Escalate_None_Lev.Checked = true;
                        }
                        //Change by kuladeep for mits:24661 end

                        NotificationDays.Value = diaryWizardNode.SelectSingleNode("NotificationDays").InnerText;
                        ReRoutedDays.Value = diaryWizardNode.SelectSingleNode("UsersRouted/ReRoutedDays").InnerText;
                    }

                    FillUserNotifiedListBox(diaryWizardNode);
                    FillUsersRoutedListBox(diaryWizardNode);

                    hdnAutoFilterStep4Visited.Value = "true";
                }

                private void FillUserNotifiedListBox(XmlNode diaryWizardNode)
                {
                    string[] arrNotifyUsersId;
                    string[] arrNotifyUsersName;
                    string[] arrGroupsNotifiedId; //pkandhari Jira 6412
                    string[] arrGroupsNotifiedName; //pkandhari Jira 6412

                    if (hdnSms_RelationShipSelectedByUser.Value == "")
                    {
                        if (diaryWizardNode.SelectSingleNode("Security_Mgmt_Relationship").InnerText == "1")
                        {
                            ShowSecurity_Mgmt_RelationshipStyle();
                        }
                        else
                        {
                            HideSecurity_Mgmt_RelationshipStyle();
                        }
                        if (hdnAutoFilterStep4Visited.Value == "") //Values were getting lost on click of Back after proceeding to next step.
                        {

                            UsersNotifiedIdList.Value = diaryWizardNode.SelectSingleNode("UsersNotified/UsersNotifiedIdList").InnerText;
                            UsersNotifiedNameList.Value = diaryWizardNode.SelectSingleNode("UsersNotified/UsersNotifiedNameList").InnerText;
                            GroupsNotifiedIdList.Value = diaryWizardNode.SelectSingleNode("//SendDiariesTo/GroupsNotifiedIdList").InnerText; //pkandhari Jira 6412
                            GroupsNotifiedNameList.Value = diaryWizardNode.SelectSingleNode("//SendDiariesTo/GroupsNotifiedNameList").InnerText; //pkandhari Jira 6412
                        } 
                    }
                    else if (hdnSms_RelationShipSelectedByUser.Value == "true")
                    {
                        ShowSecurity_Mgmt_RelationshipStyle();
                    }
                    else if (hdnSms_RelationShipSelectedByUser.Value == "false")
                    {
                        HideSecurity_Mgmt_RelationshipStyle();
                    }
                    //rjhamb Mits 22354:User selected in Other Users field shown on 2nd row not first
                    if (UsersNotifiedIdList.Value.Trim() != string.Empty && UsersNotifiedNameList.Value.Trim() != string.Empty) //Mits 22354:Added If clause to prevent empty values getting-in
                    {
                        arrNotifyUsersId = UsersNotifiedIdList.Value.Split('|');
                        arrNotifyUsersName = UsersNotifiedNameList.Value.Split('|');

                        lstUsersNotified.Items.Clear();

                        for (int count = 0; count <= arrNotifyUsersId.GetUpperBound(0); count++)
                        {
                            lstUsersNotified.Items.Add(new ListItem(arrNotifyUsersName[count], arrNotifyUsersId[count]));
                        }
                    }
                    //pkandhari Jira 6412 starts
                    if (GroupsNotifiedIdList.Value.Trim() != string.Empty && GroupsNotifiedNameList.Value.Trim() != string.Empty) //Mits 22354:Added If clause to prevent empty values getting-in
                    {
                        arrGroupsNotifiedId = GroupsNotifiedIdList.Value.Split('|');
                        arrGroupsNotifiedName = GroupsNotifiedNameList.Value.Split('|');
                        
                        for (int count = 0; count <= arrGroupsNotifiedId.GetUpperBound(0); count++)
                        {
                            lstUsersNotified.Items.Add(new ListItem(arrGroupsNotifiedName[count], arrGroupsNotifiedId[count]));
                        }
                    }
                    //pkandhari Jira 6412 ends
                }

                private void FillUsersRoutedListBox(XmlNode diaryWizardNode)
                {
                    string[] arrUsersRoutedId;
                    string[] arrUsersRoutedName;
                    string[] arrGroupsRoutedId; //pkandhari Jira 6412
                    string[] arrGroupsRoutedName; //pkandhari Jira 6412

                    if (hdnAutoFilterStep4Visited.Value == "")
                    {
                        UsersRoutedIdList.Value = diaryWizardNode.SelectSingleNode("UsersRouted/UsersRoutedIdList").InnerText;
                        UsersRoutedNameList.Value = diaryWizardNode.SelectSingleNode("UsersRouted/UsersRoutedNameList").InnerText;
                        GroupsRoutedIdList.Value = diaryWizardNode.SelectSingleNode("//SendDiariesTo/GroupsRoutedIdList").InnerText;//pkandhari Jira 6412
                        GroupsRoutedNameList.Value = diaryWizardNode.SelectSingleNode("//SendDiariesTo/GroupsRoutedNameList").InnerText; //pkandhari Jira 6412
                    }
                    //rjhamb Mits 22354::User selected in Other Users field shown on 2nd row not first
                    if (UsersRoutedIdList.Value.Trim() != string.Empty && UsersRoutedNameList.Value.Trim() != string.Empty) //Mits 22354:Added If clause to prevent empty values getting-in
                    {
                        arrUsersRoutedId = UsersRoutedIdList.Value.Split('|');
                        arrUsersRoutedName = UsersRoutedNameList.Value.Split('|');

                        lstUsersRouted.Items.Clear();

                        for (int count = 0; count <= arrUsersRoutedId.GetUpperBound(0); count++)
                        {
                            lstUsersRouted.Items.Add(new ListItem(arrUsersRoutedName[count], arrUsersRoutedId[count]));
                        }
                    }
                    //pkandhari Jira 6412 starts
                    if (GroupsRoutedIdList.Value.Trim() != string.Empty && GroupsRoutedNameList.Value.Trim() != string.Empty)
                    {
                        arrGroupsRoutedId = GroupsRoutedIdList.Value.Split('|');
                        arrGroupsRoutedName = GroupsRoutedNameList.Value.Split('|');

                        for (int count = 0; count <= arrGroupsRoutedId.GetUpperBound(0); count++)
                        {
                            lstUsersRouted.Items.Add(new ListItem(arrGroupsRoutedName[count], arrGroupsRoutedId[count]));
                        }
                    }
                    //pkandhari Jira 6412 ends
                }
                    
                private void ShowSecurity_Mgmt_RelationshipStyle()
                {
                    SMS_Relationship.Checked = true;
                    Selected_User.Checked = false;

                    lstUsersNotified.Attributes.Add("style", "display:none");
                    lstUsersNotifiedbtn.Attributes.Add("style", "display:none");
                    lstUsersNotifiedbtndel.Attributes.Add("style", "display:none");
                    //Change by kualdeep for mits:24661 Start
                    //chkEscalate.Disabled = false;
                    //Add by kuladeep for mits:24661 End
                }

                private void HideSecurity_Mgmt_RelationshipStyle()
                {
                    SMS_Relationship.Checked = false;
                    Selected_User.Checked = true;

                    lstUsersNotified.Attributes.Add("style", "display:''");
                    lstUsersNotifiedbtn.Attributes.Add("style", "display:''");
                    lstUsersNotifiedbtndel.Attributes.Add("style", "display:''");
                    //Change by kualdeep for mits:24661 Start
                    //chkEscalate.Disabled = true;
                    Escalate_High_Lev.Attributes.Add("disabled", "true");
                    Escalate_All_Lev.Attributes.Add("disabled", "true");
                    Escalate_Imme_Lev.Attributes.Add("disabled", "true");
                    Escalate_None_Lev.Attributes.Add("disabled", "true");
                    //Add by kuladeep for mits:24661 End
                }

            #endregion

            #region AutoDiaryWizard Step5
                private void PopulateAutoFilterStep5Controls()
                {
                    XmlDocument autoDiaryDoc = null;
                    XmlNode diaryWizardNode = null;

                    if (hdnAutoFilterStep5Visited.Value == "")
                    {
                        autoDiaryDoc = GetStoredAutoDiaryDoc();
                        diaryWizardNode = autoDiaryDoc.SelectSingleNode("ResultMessage/Document/DiaryWizard");

                        EstTime.Value = diaryWizardNode.SelectSingleNode("EstTime").InnerText;
                        
                        if (diaryWizardNode.SelectSingleNode("MinsHrs").InnerText == "1")
                        {
                            // rdoMins.Checked = true; commented and added below mits 25270:atavaragiri
                            rdoHrs.Checked = true;
                        }
                        else
                        {
                            //rdoHrs.Checked = true; commented and added below mits 25270:atavaragiri
                            rdoMins.Checked = true;
                        }
                        
                        if (diaryWizardNode.SelectSingleNode("TimeAssoFlag").InnerText == "1")
                        {
                            rdoTimeAssoYes.Checked = true;
                        }
                        else
                        {
                            rdoTimeAssoNo.Checked = true;
                        }
                        
                        //rupal:start, r8 auto diary enh
                        /*
                        if (diaryWizardNode.SelectSingleNode("ExportFlag").InnerText == "1")
                        {
                            rdoExportedYes.Checked = true;
                        }
                        else
                        {
                            rdoExportedNo.Checked = true;
                        }
                        */
                        if (diaryWizardNode.SelectSingleNode("ExportToEmailFlag").InnerText == "1")
                        {
                            rdoExportedYes.Checked = true;
                        }
                        else
                        {
                            rdoExportedNo.Checked = true;
                        }
                        //rupal:end
                    }

                    hdnAutoFilterStep5Visited.Value = "true";
                }
            #endregion

            #region AutoDiaryWizard Step6

                private void PopulateAutoFilterStep6Controls()
                {
                    XmlDocument autoDiaryDoc = null;
                    XmlNode diaryWizardNode = null;
                    string[] arrWorkActivityId;
                    string[] arrWorkActivityName;
                    
                    if (hdnAutoFilterStep6Visited.Value == "")
                    {
                        autoDiaryDoc = GetStoredAutoDiaryDoc();
                        diaryWizardNode = autoDiaryDoc.SelectSingleNode("ResultMessage/Document/DiaryWizard");

                        WorkActivityIdList.Value = diaryWizardNode.SelectSingleNode("WorkActivity/WorkActivityIdList").InnerText;
                        WorkActivityNameList.Value = diaryWizardNode.SelectSingleNode("WorkActivity/WorkActivityNameList").InnerText;

                        Instruction.Value = diaryWizardNode.SelectSingleNode("Instruction").InnerText;
                    }
                        
                    arrWorkActivityId = WorkActivityIdList.Value.Split('|');
                    arrWorkActivityName = WorkActivityNameList.Value.Split('|');

                    lstWorkActivity.Items.Clear();

                    for (int count = 0; count <= arrWorkActivityId.GetUpperBound(0); count++)
                    {
                        lstWorkActivity.Items.Add(new ListItem(arrWorkActivityName[count], arrWorkActivityId[count]));
                    }

                    hdnAutoFilterStep6Visited.Value = "true";
                }

            #endregion

            #region Save AutoDiaryWizard

                private XmlNode GetFinalUpdatedAutoDiaryXmlOnFinish()
                {
                    XmlDocument autoDiaryDoc = null;
                    XmlDocument storedAutoDiaryDoc = null;
                    XmlNode storedDiaryWizardNode = null;
                    string autoDiaryTemplateString = string.Empty;
                    bool populateFromControls = false;

                    try
                    {
                        autoDiaryTemplateString = GetAutoDiaryFilterTemplate();
                        autoDiaryDoc = new XmlDocument();

                        autoDiaryDoc.LoadXml(autoDiaryTemplateString);

                        storedAutoDiaryDoc = GetStoredAutoDiaryDoc();
                        storedDiaryWizardNode = storedAutoDiaryDoc.SelectSingleNode("ResultMessage/Document/DiaryWizard"); 


                        if (ViewState["defid"].ToString() != "")
                        {
                            populateFromControls = false;

                            if (CtrlAutoDiaryWizard.ActiveStepIndex == 0)
                            {
                                UpdateOnFinishAutoFilterStep1(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep2(ref autoDiaryDoc, storedDiaryWizardNode, false);
                                UpdateOnFinishAutoFilterStep3(ref autoDiaryDoc, storedDiaryWizardNode, false);
                                UpdateOnFinishAutoFilterStep4(ref autoDiaryDoc, storedDiaryWizardNode, false);
                                UpdateOnFinishAutoFilterStep5(ref autoDiaryDoc, storedDiaryWizardNode, false);
                                UpdateOnFinishAutoFilterStep6(ref autoDiaryDoc, storedDiaryWizardNode, false);
                            }
                            else if (CtrlAutoDiaryWizard.ActiveStepIndex == 1)
                            {
                                UpdateOnFinishAutoFilterStep1(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep2(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep3(ref autoDiaryDoc, storedDiaryWizardNode, false);
                                UpdateOnFinishAutoFilterStep4(ref autoDiaryDoc, storedDiaryWizardNode, false);
                                UpdateOnFinishAutoFilterStep5(ref autoDiaryDoc, storedDiaryWizardNode, false);
                                UpdateOnFinishAutoFilterStep6(ref autoDiaryDoc, storedDiaryWizardNode, false);
                            }
                            else if (CtrlAutoDiaryWizard.ActiveStepIndex == 2)
                            {
                                UpdateOnFinishAutoFilterStep1(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep2(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep3(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep4(ref autoDiaryDoc, storedDiaryWizardNode, false);
                                UpdateOnFinishAutoFilterStep5(ref autoDiaryDoc, storedDiaryWizardNode, false);
                                UpdateOnFinishAutoFilterStep6(ref autoDiaryDoc, storedDiaryWizardNode, false);
                            }
                            else if (CtrlAutoDiaryWizard.ActiveStepIndex == 3)
                            {
                                UpdateOnFinishAutoFilterStep1(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep2(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep3(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep4(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep5(ref autoDiaryDoc, storedDiaryWizardNode, false);
                                UpdateOnFinishAutoFilterStep6(ref autoDiaryDoc, storedDiaryWizardNode, false);
                            }
                            else if (CtrlAutoDiaryWizard.ActiveStepIndex == 4)
                            {
                                UpdateOnFinishAutoFilterStep1(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep2(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep3(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep4(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep5(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep6(ref autoDiaryDoc, storedDiaryWizardNode, false);
                            }
                            else if (CtrlAutoDiaryWizard.ActiveStepIndex == 5)
                            {
                                UpdateOnFinishAutoFilterStep1(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep2(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep3(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep4(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep5(ref autoDiaryDoc, storedDiaryWizardNode, true);
                                UpdateOnFinishAutoFilterStep6(ref autoDiaryDoc, storedDiaryWizardNode, true);
                            }
                        }
                        else
                        {
                            UpdateOnFinishAutoFilterStep1(ref autoDiaryDoc, storedDiaryWizardNode, true);
                            UpdateOnFinishAutoFilterStep2(ref autoDiaryDoc, storedDiaryWizardNode, true);
                            UpdateOnFinishAutoFilterStep3(ref autoDiaryDoc, storedDiaryWizardNode, true);
                            UpdateOnFinishAutoFilterStep4(ref autoDiaryDoc, storedDiaryWizardNode, true);
                            UpdateOnFinishAutoFilterStep5(ref autoDiaryDoc, storedDiaryWizardNode, true);
                            UpdateOnFinishAutoFilterStep6(ref autoDiaryDoc, storedDiaryWizardNode, true);
                        }
                        
                        return autoDiaryDoc.SelectSingleNode("/");
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                private void UpdateOnFinishAutoFilterStep1(ref XmlDocument autoDiaryDoc, XmlNode storedDiaryWizardNode, bool populateFromControls)
                {
                    XmlNode diaryWizardNode = null;
                    XmlNode diaryTemplateNode = null;
                    XmlNode diaryNameNode = null;
                    XmlNode diaryDateNode = null;
                    XmlNode noOfDaysNode = null;
                    XmlNode priorityLevelNode = null;
                    XmlNode defIndexNode = null;
                    XmlNode defIdNode = null;

                    XmlNode storedDiaryTemplateNode = null;
                    XmlNode storedDiaryNameNode = null;
                    XmlNode storedDiaryDateNode = null;
                    XmlNode storedNoOfDaysNode = null;
                    XmlNode storedPriorityLevelNode = null;
                    XmlNode storedDefIndexNode = null;
                    
                    diaryWizardNode = autoDiaryDoc.SelectSingleNode("DiaryWizard");

                    if (diaryWizardNode != null)
                    {
                        defIdNode = diaryWizardNode.SelectSingleNode("DefId");
                        if (defIdNode != null)
                        {
                            defIdNode.InnerText = ViewState["defid"].ToString();
                        }
                        
                        diaryTemplateNode = diaryWizardNode.SelectSingleNode("DiaryTemplate");

                        if (diaryTemplateNode != null)
                        {
                            if (populateFromControls)
                            {
                                diaryTemplateNode.Attributes["selectedname"].Value = cboTemplate.SelectedItem.Text;
                                diaryTemplateNode.Attributes["selectedid"].Value = cboTemplate.SelectedItem.Value;
                            }
                            else //populate from stored doc
                            {
                                storedDiaryTemplateNode = storedDiaryWizardNode.SelectSingleNode("DiaryTemplate");

                                if (storedDiaryTemplateNode != null)
                                {
                                    diaryTemplateNode.Attributes["selectedname"].Value = storedDiaryTemplateNode.Attributes["selectedname"].Value;
                                    diaryTemplateNode.Attributes["selectedid"].Value = storedDiaryTemplateNode.Attributes["selectedid"].Value;
                                }
                            }
                        }

                        diaryNameNode = diaryWizardNode.SelectSingleNode("DiaryName");
                        if (diaryNameNode != null)
                        {
                            if (populateFromControls)
                            {
                                diaryNameNode.InnerText = txtDiaryName.Text;
                            }
                            else //populate from stored doc
                            {
                                storedDiaryNameNode = storedDiaryWizardNode.SelectSingleNode("DiaryName");
                                if (storedDiaryWizardNode != null)
                                {
                                    diaryNameNode.InnerText = storedDiaryNameNode.InnerText;
                                }
                            }
                        }

                        diaryDateNode = diaryWizardNode.SelectSingleNode("DiaryDate");
                        if (diaryDateNode != null)
                        {
                            if (populateFromControls)
                            {
                                diaryDateNode.InnerText = txtDiaryDate.Value;
                            }
                            else
                            {
                                storedDiaryDateNode = storedDiaryWizardNode.SelectSingleNode("DiaryDate");

                                if (storedDiaryDateNode != null)
                                {
                                    diaryDateNode.InnerText = storedDiaryDateNode.InnerText;
                                }
                            }
                        }

                        noOfDaysNode = diaryWizardNode.SelectSingleNode("NoOfDays");
                        if (noOfDaysNode != null)
                        {
                            if (populateFromControls)
                            {
                                noOfDaysNode.InnerText = txtDaysNoAfterDueDate.Text;
                            }
                            else
                            {
                                storedNoOfDaysNode = diaryWizardNode.SelectSingleNode("NoOfDays");
                            }
                        }

                        priorityLevelNode = diaryWizardNode.SelectSingleNode("PriorityLevel");

                        if (priorityLevelNode != null)
                        {
                            if (populateFromControls)
                            {
                                priorityLevelNode.Attributes["selectedid"].Value = cboPriority.SelectedItem.Value;
                            }
                            else
                            {
                                storedPriorityLevelNode = storedDiaryWizardNode.SelectSingleNode("PriorityLevel");

                                if (storedPriorityLevelNode != null)
                                {
                                    priorityLevelNode.Attributes["selectedid"].Value = storedPriorityLevelNode.Attributes["selectedid"].Value;
                                }
                            }
                        }

                        defIndexNode = diaryWizardNode.SelectSingleNode("DefIndex");
                        if (defIndexNode != null)
                        {
                            if (populateFromControls)
                            {
                                defIndexNode.InnerText = "0";
                            }
                            else
                            {
                                storedDefIndexNode = storedDiaryWizardNode.SelectSingleNode("DefIndex");
                                if (storedDefIndexNode != null)
                                {
                                    defIndexNode.InnerText = storedDefIndexNode.InnerText;
                                }
                            }
                        }
                    }
                }

                private void UpdateOnFinishAutoFilterStep2(ref XmlDocument autoDiaryDoc, XmlNode storedDiaryWizardNode, bool populateFromControls)
                {
                    XmlDocument hdnSelectedFiltersDoc = null;
                    XmlNode hdnSelectedNode = null;
                    XmlNode selectedFiltersNode = null;

                    XmlNode storedSelectedFiltersNOde = null;

                    selectedFiltersNode = autoDiaryDoc.SelectSingleNode("DiaryWizard/Filters/SelectedFilters");

                    if (populateFromControls)
                    {
                        //fetching selected filters from hdnField
                        hdnSelectedFiltersDoc = new XmlDocument();
                        hdnSelectedFiltersDoc.LoadXml(hdnSelectedFiltersNode.Value);
                        //hdnSelectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value)); //Bharani - MITS : 35878
                        hdnSelectedNode = hdnSelectedFiltersDoc.SelectSingleNode("SelectedFilters");
                        
                        selectedFiltersNode.InnerXml = hdnSelectedNode.InnerXml;
                    }
                    else
                    {
                        storedSelectedFiltersNOde = storedDiaryWizardNode.SelectSingleNode("Filters/SelectedFilters");
                        selectedFiltersNode.InnerXml = storedSelectedFiltersNOde.InnerXml;
                    }
                }

                private void UpdateOnFinishAutoFilterStep3(ref XmlDocument autoDiaryDoc, XmlNode storedDiaryWizardNode, bool populateFromControls)
                {
                    XmlNode sendDiariesToNode = null;
                    XmlNode userCreatedNode = null;
                    XmlNode userModifiedNode = null;
                    XmlNode adjAssignNode = null;
                    XmlNode adjAssignSupervisorNode = null;
                    XmlNode eventSuppNode = null;
                    XmlNode suppFieldNode = null;
                    XmlNode sendUserIdListNode = null;
                    XmlNode sendUserNameListNode = null;
                    //pkandhari Jira 6412 starts
                    XmlNode sendGroupsIdListNode = null;
                    XmlNode sendGroupsNameListNode = null;
                    XmlNode GroupsNotifiedIdListNode = null;
                    XmlNode GroupsNotifiedNameListNode = null;
                    XmlNode GroupsRoutedIdListNode = null;
                    XmlNode GroupsRoutedNameListNode = null;
                    //pkandhari Jira 6412 ends
                    XmlNode flagNode = null;

                    XmlNode storedSendDiariesToNode = null;

                    sendDiariesToNode = autoDiaryDoc.SelectSingleNode("DiaryWizard/SendDiariesTo");

                    if (sendDiariesToNode != null)
                    {
                        if (populateFromControls)
                        {
                            userCreatedNode = sendDiariesToNode.SelectSingleNode("UserCreated");
                            if (userCreatedNode != null)
                            {
                                if (chkUserCreated.Checked)
                                {
                                    userCreatedNode.InnerText = "1";
                                }
                            }

                            userModifiedNode = sendDiariesToNode.SelectSingleNode("UserModified");
                            if (userModifiedNode != null)
                            {
                                if (chkUserModified.Checked)
                                {
                                    userModifiedNode.InnerText = "1";
                                }
                            }

                            adjAssignNode = sendDiariesToNode.SelectSingleNode("AdjAssig");
                            if (adjAssignNode != null)
                            {
                                if (chkAdjAssign.Checked)
                                {
                                    adjAssignNode.InnerText = "1";
                                }
                            }

                            adjAssignSupervisorNode = sendDiariesToNode.SelectSingleNode("AdjAssigSupervisor");
                            if (adjAssignSupervisorNode != null)
                            {
                                if (chkAdjAssigSupervisor.Checked)
                                {
                                    adjAssignSupervisorNode.InnerText = "1";
                                }
                            }

                            if (chkEventSupp.Checked == true)
                            {
                                eventSuppNode = sendDiariesToNode.SelectSingleNode("EventSuppUser");

                                if (eventSuppNode != null)
                                {
                                    if (chkEventSupp.Checked)
                                    {
                                        eventSuppNode.InnerText = "1";
                                    }
                                }
                            }

                            if (chkEventSupp.Checked == true)
                            {
                                suppFieldNode = sendDiariesToNode.SelectSingleNode("SuppField");

                                if (suppFieldNode != null)
                                {
                                    suppFieldNode.Attributes["selectedname"].Value = cboSuppField.SelectedItem.Text;
                                    suppFieldNode.Attributes["selectedid"].Value = cboSuppField.SelectedItem.Value;
                                }
                            }

                            sendUserIdListNode = sendDiariesToNode.SelectSingleNode("SendUsersIdList");
                            if (sendUserIdListNode != null)
                            {
                                sendUserIdListNode.InnerText = SendUsersIdList.Value;
                            }


                            sendUserNameListNode = sendDiariesToNode.SelectSingleNode("SendUsersNameList");
                            if (sendUserNameListNode != null)
                            {
                                sendUserNameListNode.InnerText = SendUsersNameList.Value;
                            }
                            
                            //pkandhari Jira 6412 starts
                            sendGroupsIdListNode = sendDiariesToNode.SelectSingleNode("SendGroupsIdList");
                            if (sendGroupsIdListNode != null)
                            {
                                sendGroupsIdListNode.InnerText = SendGroupsIdList.Value;
                            }

                            sendGroupsNameListNode = sendDiariesToNode.SelectSingleNode("SendGroupsNameList");
                            if (sendGroupsNameListNode != null)
                            {
                                sendGroupsNameListNode.InnerText = SendGroupsNameList.Value;
                            }

                            GroupsNotifiedIdListNode = sendDiariesToNode.SelectSingleNode("GroupsNotifiedIdList");
                            if (GroupsNotifiedIdListNode != null)
                            {
                                GroupsNotifiedIdListNode.InnerText = GroupsNotifiedIdList.Value;
                            }

                            GroupsNotifiedNameListNode = sendDiariesToNode.SelectSingleNode("GroupsNotifiedNameList");
                            if (GroupsNotifiedNameListNode != null)
                            {
                                GroupsNotifiedNameListNode.InnerText = GroupsNotifiedNameList.Value;
                            }

                            GroupsRoutedIdListNode = sendDiariesToNode.SelectSingleNode("GroupsRoutedIdList");
                            if (GroupsRoutedIdListNode != null)
                            {
                                GroupsRoutedIdListNode.InnerText = GroupsRoutedIdList.Value;
                            }

                            GroupsRoutedNameListNode = sendDiariesToNode.SelectSingleNode("GroupsRoutedNameList");
                            if (GroupsRoutedNameListNode != null)
                            {
                                GroupsRoutedNameListNode.InnerText = GroupsRoutedNameList.Value;
                            }
                            //pkandhari Jira 6412 ends
                            flagNode = sendDiariesToNode.SelectSingleNode("Flag");
                            if (flagNode != null)
                            {
                                flagNode.InnerText = Flag.Value;
                            }
                        }
                        else  //populate from stored doc
                        {
                            storedSendDiariesToNode = storedDiaryWizardNode.SelectSingleNode("SendDiariesTo");

                            if (storedSendDiariesToNode != null)
                            {
                                // assigning inner xml of stored node to new node
                                sendDiariesToNode.InnerXml = storedSendDiariesToNode.InnerXml;
                            }
                        }
                    }
                }

                private void UpdateOnFinishAutoFilterStep4(ref XmlDocument autoDiaryDoc, XmlNode storedDiaryWizardNode, bool populateFromControls)
                {
                    XmlNode diaryWizardNode = null;
                    XmlNode escalateHighestLvlNode = null;
                    XmlNode security_Mgmt_RelationshipNode = null;
                    XmlNode userNotifiedNode = null;
                    XmlNode usersNotifiedIdListNode = null;
                    XmlNode usersNotifiedNameListNode = null;
                    XmlNode notificationDaysNode = null;
                    XmlNode usersRoutedNode = null;
                    XmlNode usersRoutedIdListNode = null;
                    XmlNode usersRoutedNameListNode = null;
                    XmlNode reRoutedDaysNode = null;

                    XmlNode storedEscalateHighestLvlNode = null;
                    XmlNode storedSecurity_Mgmt_RelationshipNode = null;
                    XmlNode storedUsersNotifiedNode = null;
                    XmlNode storedNotificationDaysNode = null;
                    XmlNode storedUsersRoutedNode = null;
                    
                    
                    diaryWizardNode = autoDiaryDoc.SelectSingleNode("DiaryWizard");
                    
                    if (diaryWizardNode != null)
                    {
                        escalateHighestLvlNode = diaryWizardNode.SelectSingleNode("Escalate_Highestlevel");
                        if (escalateHighestLvlNode != null)
                        {
                            if (populateFromControls)
                            {
                                //Change by kuladeep for mits:24661 Strat
                                //if (chkEscalate.Checked)
                                //{
                                //    escalateHighestLvlNode.InnerText = "1";
                                //}
                                if (Escalate_All_Lev.Checked)
                                {
                                    escalateHighestLvlNode.InnerText = "2";
                                }
                                else if (Escalate_High_Lev.Checked)
                                {
                                    escalateHighestLvlNode.InnerText = "1";
                                }
                                else if (Escalate_Imme_Lev.Checked)
                                {
                                    escalateHighestLvlNode.InnerText = "3";
                                }
                                else if (Escalate_None_Lev.Checked)
                                {
                                    escalateHighestLvlNode.InnerText = "4";
                                }
                                //Change by kuladeep for mits:24661 end
                            }
                            else
                            {
                                storedEscalateHighestLvlNode = storedDiaryWizardNode.SelectSingleNode("Escalate_Highestlevel");

                                if (storedEscalateHighestLvlNode != null)
                                {
                                    escalateHighestLvlNode.InnerText = storedEscalateHighestLvlNode.InnerText;
                                }
                            }
                        }

                        security_Mgmt_RelationshipNode = diaryWizardNode.SelectSingleNode("Security_Mgmt_Relationship");
                        if (security_Mgmt_RelationshipNode != null)
                        {
                            if (populateFromControls)
                            {
                                if (SMS_Relationship.Checked)
                                {
                                    security_Mgmt_RelationshipNode.InnerText = "1";
                                }
                            }
                            else
                            {
                                storedSecurity_Mgmt_RelationshipNode = storedDiaryWizardNode.SelectSingleNode("Security_Mgmt_Relationship");

                                if (storedSecurity_Mgmt_RelationshipNode != null)
                                {
                                    security_Mgmt_RelationshipNode.InnerText = storedSecurity_Mgmt_RelationshipNode.InnerText;
                                }
                            }
                        }


                        if (populateFromControls)
                        {
                            usersNotifiedIdListNode = diaryWizardNode.SelectSingleNode("UsersNotified/UsersNotifiedIdList");
                            if (usersNotifiedIdListNode != null)
                            {
                                usersNotifiedIdListNode.InnerText = UsersNotifiedIdList.Value;
                            }

                            usersNotifiedNameListNode = diaryWizardNode.SelectSingleNode("UsersNotified/UsersNotifiedNameList");
                            if (usersNotifiedNameListNode != null)
                            {
                                usersNotifiedNameListNode.InnerText = UsersNotifiedNameList.Value;
                            }

                            usersRoutedIdListNode = diaryWizardNode.SelectSingleNode("UsersRouted/UsersRoutedIdList");
                            if (usersRoutedIdListNode != null)
                            {
                                usersRoutedIdListNode.InnerText = UsersRoutedIdList.Value;
                            }

                            usersRoutedNameListNode = diaryWizardNode.SelectSingleNode("UsersRouted/UsersRoutedNameList");
                            if (usersRoutedNameListNode != null)
                            {
                                usersRoutedNameListNode.InnerText = UsersRoutedNameList.Value;
                            }

                            reRoutedDaysNode = diaryWizardNode.SelectSingleNode("UsersRouted/ReRoutedDays");
                            if (reRoutedDaysNode != null)
                            {
                                reRoutedDaysNode.InnerText = ReRoutedDays.Value;
                            }
                        }
                        else
                        {
                            userNotifiedNode = diaryWizardNode.SelectSingleNode("UsersNotified");

                            if (userNotifiedNode != null)
                            {
                                storedUsersNotifiedNode = storedDiaryWizardNode.SelectSingleNode("UsersNotified");

                                if (storedUsersNotifiedNode != null)
                                {
                                    userNotifiedNode.InnerXml = storedUsersNotifiedNode.InnerXml;
                                }
                            }

                            usersRoutedNode = diaryWizardNode.SelectSingleNode("UsersRouted");

                            if (usersRoutedNode != null)
                            {
                                storedUsersRoutedNode = storedDiaryWizardNode.SelectSingleNode("UsersRouted");

                                if (storedUsersRoutedNode != null)
                                {
                                    usersRoutedNode.InnerXml = storedUsersRoutedNode.InnerXml; 
                                }
                            }
                        }

                        notificationDaysNode = diaryWizardNode.SelectSingleNode("NotificationDays");
                        if (notificationDaysNode != null)
                        {
                            if (populateFromControls)
                            {
                                notificationDaysNode.InnerText = NotificationDays.Value;
                            }
                            else
                            {
                                storedNotificationDaysNode = storedDiaryWizardNode.SelectSingleNode("NotificationDays");

                                if(storedUsersRoutedNode != null)
                                {
                                    notificationDaysNode.InnerText = storedNotificationDaysNode.InnerText; 
                                }
                            }
                        }
                    }
                }

                private void UpdateOnFinishAutoFilterStep5(ref XmlDocument autoDiaryDoc, XmlNode storedDiaryWizardNode, bool populateFromControls)
                {
                    XmlNode diaryWizardNode = null;
                    XmlNode estTimeNode = null;
                    XmlNode minsHrNode = null;
                    XmlNode timeAssoFlagNode = null;
                    XmlNode exportFlagNode = null;
                    
                    XmlNode storedEstTimeNode = null;
                    XmlNode storedMinsHrNode = null;
                    XmlNode storedTimeAssoFlagNode = null;
                    XmlNode storedExportFlagNode = null;


                    diaryWizardNode = autoDiaryDoc.SelectSingleNode("DiaryWizard");

                    if (diaryWizardNode != null)
                    {
                        estTimeNode = diaryWizardNode.SelectSingleNode("EstTime");
                        if (estTimeNode != null)
                        {
                            if (populateFromControls)
                            {
                                estTimeNode.InnerText = EstTime.Value;
                            }
                            else
                            {
                                storedEstTimeNode = storedDiaryWizardNode.SelectSingleNode("EstTime");

                                if (storedEstTimeNode != null)
                                {
                                    estTimeNode.InnerText = storedEstTimeNode.InnerText;
                                }
                            }
                        }

                        minsHrNode = diaryWizardNode.SelectSingleNode("MinsHrs");
                        if (minsHrNode != null)
                        {
                            if (populateFromControls)
                            {
                                if (rdoMins.Checked)
                                {
                                    minsHrNode.InnerText = "0";
                                }
                                else if (rdoHrs.Checked)
                                {
                                    minsHrNode.InnerText = "1";
                                }
                            }
                            else
                            {
                                storedMinsHrNode = storedDiaryWizardNode.SelectSingleNode("MinsHrs");

                                if (storedMinsHrNode != null)
                                {
                                    minsHrNode.InnerText = storedMinsHrNode.InnerText;
                                }
                            }
                        }

                        timeAssoFlagNode = diaryWizardNode.SelectSingleNode("TimeAssoFlag");
                        if (timeAssoFlagNode != null)
                        {
                            if (populateFromControls)
                            {
                                if (rdoTimeAssoNo.Checked)
                                {
                                    timeAssoFlagNode.InnerText = "0";
                                }
                                else if (rdoTimeAssoYes.Checked)
                                {
                                    timeAssoFlagNode.InnerText = "1";
                                }
                            }
                            else
                            {
                                storedTimeAssoFlagNode = storedDiaryWizardNode.SelectSingleNode("TimeAssoFlag");

                                if (storedTimeAssoFlagNode != null)
                                {
                                    timeAssoFlagNode.InnerText = storedTimeAssoFlagNode.InnerText;
                                }
                            }
                        }
                          
                        //rupal:start,r8 auto diary enh
                        //export to email enh
                        exportFlagNode = diaryWizardNode.SelectSingleNode("ExportToEmailFlag");
                        //exportFlagNode = diaryWizardNode.SelectSingleNode("ExportFlag");
                        //rupal:end

                        if (exportFlagNode != null)
                        {
                            if (populateFromControls)
                            {
                                if (rdoExportedNo.Checked)
                                {
                                    exportFlagNode.InnerText = "0";
                                }
                                else if (rdoExportedYes.Checked)
                                {
                                    exportFlagNode.InnerText = "1";
                                }
                            }
                            else
                            {
                                //rupal:start,r8 auto diary enh
                                //export to email enh
                                //storedExportFlagNode = storedDiaryWizardNode.SelectSingleNode("ExportFlag");
                                storedExportFlagNode = storedDiaryWizardNode.SelectSingleNode("ExportToEmailFlag");
                                //rupal:end
                                
                                if (storedExportFlagNode != null)
                                {
                                    exportFlagNode.InnerText = storedExportFlagNode.InnerText;
                                }
                            }
                        }
                    }
                }

                private void UpdateOnFinishAutoFilterStep6(ref XmlDocument autoDiaryDoc, XmlNode storedDiaryWizardNode, bool populateFromControls)
                {
                    XmlNode diaryWizardNode = null;
                    XmlNode workActivityIdListNode = null;
                    XmlNode workActivityNameListNode = null;
                    XmlNode instructionsNode = null;
                    XmlNode workActivityNode = null;

                    XmlNode storedWorkActivityNode = null;
                    XmlNode storedInstructionNode = null;

                    diaryWizardNode = autoDiaryDoc.SelectSingleNode("DiaryWizard");

                    if (diaryWizardNode != null)
                    {
                        if (populateFromControls)
                        {
                            workActivityIdListNode = diaryWizardNode.SelectSingleNode("WorkActivity/WorkActivityIdList");
                            if (workActivityIdListNode != null)
                            {
                                workActivityIdListNode.InnerText = WorkActivityIdList.Value;
                            }

                            workActivityNameListNode = diaryWizardNode.SelectSingleNode("WorkActivity/WorkActivityNameList");
                            if (workActivityNameListNode != null)
                            {
                                workActivityNameListNode.InnerText = WorkActivityNameList.Value;
                            }

                            instructionsNode = diaryWizardNode.SelectSingleNode("Instruction");
                            if (instructionsNode != null)
                            {
                                instructionsNode.InnerText = Instruction.Value;
                            }
                        }
                        else
                        {
                            
                            workActivityNode = diaryWizardNode.SelectSingleNode("WorkActivity");

                            if (workActivityNode != null)
                            {
                                storedWorkActivityNode = storedDiaryWizardNode.SelectSingleNode("WorkActivity");

                                if (storedWorkActivityNode != null)
                                {
                                    workActivityNode.InnerXml = storedWorkActivityNode.InnerXml;
                                }
                            }

                            instructionsNode = diaryWizardNode.SelectSingleNode("Instruction");
                            if (instructionsNode != null)
                            {
                                storedInstructionNode = storedDiaryWizardNode.SelectSingleNode("Instruction");

                                if (storedInstructionNode != null)
                                {
                                    instructionsNode.InnerText = storedInstructionNode.InnerText;
                                }
                            }
                        }
                    }
                }
                
                private XmlDocument SaveAutoDiaryFinalData()
                {
                    //sgupta243 --Changes for the MITS 28668 
                   if(Instruction.Value.Contains("'"))
                   {
                    Instruction.Value =Instruction.Value.Replace("'","''");
                   }

                    string cwsResponse = string.Empty;
                    XmlDocument diaryDoc = null;
                    XmlNode inputDocNode = null;
                    string serviceMethodToCall = string.Empty;
                    
                    try
                    {
                        serviceMethodToCall = "WPAAutoDiarySetupAdaptor.Save";

                        inputDocNode = GetFinalUpdatedAutoDiaryXmlOnFinish();
                        cwsResponse = CallCWSFunctionFromAppHelper(inputDocNode, serviceMethodToCall);

                        diaryDoc = new XmlDocument();
                        diaryDoc.LoadXml(cwsResponse);

                        return diaryDoc;
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException(ex.Message);
                    }
                }

            #endregion

            #region ContactingWebService

                private string GetMessageTemplate()
                {
                    XElement oTemplate = XElement.Parse(@"
                        <Message>
                          <Authorization></Authorization>
                          <Call>
                            <Function></Function>
                          </Call>
                          <Document>
                          </Document>
                        </Message>
                    ");

                    return oTemplate.ToString();
                }

                public string CallCWSFunctionFromAppHelper(XmlNode inputDocumentNode, string serviceMethodToCall)
                {
                    XmlNode tempNode = null;
                    string oMessageElement = string.Empty;
                    string responseCWS = string.Empty;
                    
                    XmlDocument messageTemplateDoc = null;

                    try
                    {
                        oMessageElement = GetMessageTemplate();
                        messageTemplateDoc = new XmlDocument();
                        messageTemplateDoc.LoadXml(oMessageElement);

                        if (inputDocumentNode != null)
                        {
                            tempNode = messageTemplateDoc.SelectSingleNode("Message/Document");
                            tempNode.InnerXml = inputDocumentNode.OuterXml;
                        }

                        if (!string.IsNullOrEmpty(serviceMethodToCall))
                        {
                            tempNode = messageTemplateDoc.SelectSingleNode("Message/Call/Function");
                            tempNode.InnerText = serviceMethodToCall;
                        }

                        //get webservice call from Apphelper.cs
                        responseCWS = AppHelper.CallCWSService(messageTemplateDoc.InnerXml);

                        return responseCWS;
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException(ex.Message);
                    }
                }
        
            #endregion

        #endregion

        #region Wizard Control Navigation Buttons Events

            protected void CtrlAutoDiaryWizard_NextButtonClick(object sender, EventArgs e)
            {
                Button stepFinishedButton = null;

                try
                {
                    if (CtrlAutoDiaryWizard.ActiveStepIndex == 0)
                    {
                        PopulateAutoFilterStep2Controls();

                        //enabling and disabling Finish button
                        if (CtrlAutoDiaryWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                        {
                            stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoDiaryWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton");

                            if (ViewState["defid"].ToString() != "")
                            {
                                stepFinishedButton.Enabled = true;
                                
                                lblMessgeTitle.Text = "Edit Auto Diary - Step 2 of 6 (Business Rule Definition)";
                            }
                            else
                            {
                                stepFinishedButton.Enabled = false;
                                lblMessgeTitle.Text = "New Auto Diary - Step 2 of 6 (Business Rule Definition)";
                            }
                        }
                    }
                    else if (CtrlAutoDiaryWizard.ActiveStepIndex == 1)
                    {
                        PopulateAutoFilterStep3Controls();

                        //enabling and disabling Finish button
                        if (CtrlAutoDiaryWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                        {
                            stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoDiaryWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton");

                            if (ViewState["defid"].ToString() != "")
                            {
                                stepFinishedButton.Enabled = true;
                                lblMessgeTitle.Text = "Edit Auto Diary - Step 3 of 6 (Workflow Notification)";
                            }
                            else
                            {
                                stepFinishedButton.Enabled = false;
                                lblMessgeTitle.Text = "New Auto Diary - Step 3 of 6 (Workflow Notification)";
                            }
                        }
                    }
                    else if (CtrlAutoDiaryWizard.ActiveStepIndex == 2)
                    {
                        PopulateAutoFilterStep4Controls();

                        //enabling and disabling Finish button
                        if (CtrlAutoDiaryWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                        {
                            stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoDiaryWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton");

                            if (ViewState["defid"].ToString() != "")
                            {
                                stepFinishedButton.Enabled = true;
                                lblMessgeTitle.Text = "Edit Auto Diary - Step 4 of 6 (Escalation Management)";
                            }
                            else
                            {
                                stepFinishedButton.Enabled = false;
                                lblMessgeTitle.Text = "New Auto Diary - Step 4 of 6 (Escalation Management)";
                            }
                        }
                    }
                    else if (CtrlAutoDiaryWizard.ActiveStepIndex == 3)
                    {
                        PopulateAutoFilterStep5Controls();

                        //enabling and disabling Finish button
                        if (CtrlAutoDiaryWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                        {
                            stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoDiaryWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton");

                            if (ViewState["defid"].ToString() != "")
                            {
                                stepFinishedButton.Enabled = true;
                                lblMessgeTitle.Text = "Edit Auto Diary - Step 5 of 6 (Time & Task Management)";
                            }
                            else
                            {
                                stepFinishedButton.Enabled = false;
                                lblMessgeTitle.Text = "New Auto Diary - Step 5 of 6 (Time & Task Management)";
                            }
                        }
                    }
                    else if (CtrlAutoDiaryWizard.ActiveStepIndex == 4)
                    {
                        PopulateAutoFilterStep6Controls();

                        //enabling and disabling Finish button
                        if (CtrlAutoDiaryWizard.FindControl("FinishNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                        {
                            stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoDiaryWizard.FindControl("FinishNavigationTemplateContainerID").FindControl("StepFinishButton");

                            //if (AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value) == "<SelectedFilters></SelectedFilters>")//Bharani - MITS : 35878
                            if (hdnSelectedFiltersNode.Value == "<SelectedFilters></SelectedFilters>")
                            {
                                stepFinishedButton.Enabled = false;
                            }
                            else
                            {
                                stepFinishedButton.Enabled = true;
                            }

                            if (ViewState["defid"].ToString() != "")
                            {
                                lblMessgeTitle.Text = "Edit Auto Diary - Step 6 of 6 (Instructions/Procedures)";
                            }
                            else
                            {
                                lblMessgeTitle.Text = "New Auto Diary - Step 6 of 6 (Instructions/Procedures)";
                            }
                        }
                    }
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }


            protected void CtrlAutoDiaryWizard_PreviousButtonClick(object sender, EventArgs e)
            {
                try
                {
                    if (CtrlAutoDiaryWizard.ActiveStepIndex == 1)
                    {
                        PopulateAutoFitlerStep1Controls();

                        if (ViewState["defid"].ToString() != "")
                        {
                            lblMessgeTitle.Text = "Edit Auto Diary - Step 1 of 6 (Best Practice Scenerio)";
                        }
                        else
                        {
                            lblMessgeTitle.Text = "New Auto Diary - Step 1 of 6 (Best Practice Scenerio)";
                        }
                    }
                    else if (CtrlAutoDiaryWizard.ActiveStepIndex == 2)
                    {
                        PopulateAutoFilterStep2Controls();

                        if (ViewState["defid"].ToString() != "")
                        {
                            lblMessgeTitle.Text = "Edit Auto Diary - Step 2 of 6 (Business Rule Definition)";
                        }
                        else
                        {
                            lblMessgeTitle.Text = "New Auto Diary - Step 2 of 6 (Business Rule Definition)";
                        }
                    }
                    else if (CtrlAutoDiaryWizard.ActiveStepIndex == 3)
                    {
                        PopulateAutoFilterStep3Controls();

                        if (ViewState["defid"].ToString() != "")
                        {
                            lblMessgeTitle.Text = "Edit Auto Diary - Step 3 of 6 (Workflow Notification)";
                        }
                        else
                        {
                            lblMessgeTitle.Text = "New Auto Diary - Step 3 of 6 (Workflow Notification)";
                        }
                    }
                    else if (CtrlAutoDiaryWizard.ActiveStepIndex == 4)
                    {
                        PopulateAutoFilterStep4Controls();

                        if (ViewState["defid"].ToString() != "")
                        {
                            lblMessgeTitle.Text = "Edit Auto Diary - Step 4 of 6 (Escalation Management)";
                        }
                        else
                        {
                            lblMessgeTitle.Text = "New Auto Diary - Step 4 of 6 (Escalation Management)";
                        }
                    }
                    else if (CtrlAutoDiaryWizard.ActiveStepIndex == 5)
                    {
                        PopulateAutoFilterStep5Controls();

                        if (ViewState["defid"].ToString() != "")
                        {
                            lblMessgeTitle.Text = "Edit Auto Diary - Step 5 of 6 (Time & Task Management)";
                        }
                        else
                        {
                            lblMessgeTitle.Text = "New Auto Diary - Step 5 of 6 (Time & Task Management)";
                        }
                    }
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }

            protected void CtrlAutoDiaryWizard_FinishButtonClick(object sender, EventArgs e)
            {
                try
                {
                    SaveAutoDiaryFinalData();
                    //Change by kuladeep for mits:24453 Start
                    //Page.ClientScript.RegisterClientScriptBlock(typeof(string), "ee", "<Script>window.opener.document.location.reload(true);window.close();</Script>");
                    Page.ClientScript.RegisterClientScriptBlock(typeof(string), "ee", "<Script>window.opener.document.location.href=window.opener.document.location;window.close();</Script>");
                    //Change by kuladeep for mits:24453 End
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        
            protected void CtrlAutoDiaryWizard_CancelButtonClick(object sender, EventArgs e)
            {

            }

            private void ShowAutoFilterWizard()
            {
                CtrlAutoDiaryWizard.Visible = true;

                CtrlAutoDiaryWizard.ActiveStepIndex = 0;

                PopulateAutoFitlerStep1Controls();

                lblMessgeTitle.Visible = true;
                if (ViewState["defid"].ToString() != "")
                {
                    lblMessgeTitle.Text = "Edit Auto Diary - Step 1 of 6 (Best Practice Scenerio)";
                }
                else
                {
                    lblMessgeTitle.Text = "New Auto Diary - Step 1 of 6 (Best Practice Scenerio)";
                }

                CtrlBestPracticeDiaryWizard.Visible = false;
            }


            private void ShowBestPracticeDiaryWizard()
            {
                CtrlBestPracticeDiaryWizard.Visible = true;
                CtrlBestPracticeDiaryWizard.ActiveStepIndex = 0;
                CtrlAutoDiaryWizard.Visible = false;
                lblMessgeTitle.Visible = true;
                lblMessgeTitle.Text = "Auto Diary Creation - Step 1 of 6 (Introduction)";
            }
        
        #endregion

        #region BestPracticeDiaryWizard

            protected void CtrlBestPracticeDiaryWizard_NextButtonClick(object sender, EventArgs e)
            {
                if (CtrlBestPracticeDiaryWizard.ActiveStepIndex == 0)
                {
                    lblMessgeTitle.Text = "Auto Diary Creation - Step 2 of 6 (Creation Process)";
                }
                else if (CtrlBestPracticeDiaryWizard.ActiveStepIndex == 1)
                {
                    if (rdoAutoDiaryWizard.Checked == true)
                    {
                        ShowAutoFilterWizard();
                    }
                    else
                    {
                        ShowBestPracticeDiaryWizard();
                    }
                }
            }

            protected void CtrlBestPracticeDiaryWizard_PreviousButtonClick(object sender, EventArgs e)
            {
                if (CtrlBestPracticeDiaryWizard.ActiveStepIndex == 1)
                {
                    lblMessgeTitle.Text = "Auto Diary Creation - Step 1 of 6 (Introduction)";
                }
                else if (CtrlBestPracticeDiaryWizard.ActiveStepIndex == 2)
                {
                    lblMessgeTitle.Text = "Auto Diary Creation - Step 2 of 6 (Creation Process)";
                }
            }

            protected void CtrlBestPracticeDiaryWizard_CancelButtonClick(object sender, EventArgs e)
            {

            }

            protected void CtrlBestPracticeDiaryWizard_FinishButtonClick(object sender, EventArgs e)
            {

            }

        #endregion

    }
}
