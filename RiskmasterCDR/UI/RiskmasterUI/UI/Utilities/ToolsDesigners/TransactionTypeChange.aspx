<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransactionTypeChange.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.ToolsDesigners.TransactionTypeChange" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Transaction Type Change Utility</title>
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">{var i;}</script>
     <%--vkumar258 - RMA-6037 - Starts --%>

   <%-- <script type="text/javascript" src="../../../Scripts/zapatec/utils/zapatec.js">{var i;}</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}</script>--%>
<link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

    <script type="text/javascript" language="javascript">
    var ns, ie, ieversion;
				var browserName = navigator.appName;                   // detect browser 
				var browserVersion = navigator.appVersion;
				if (browserName == "Netscape")
				{
					ie=0;
					ns=1;
				}
				else		//Assume IE
				{
				ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
				ie=1;
				ns=0;
				}
    function onPageLoaded()
				{
					if (ie)
					{
						if ((eval("document.all.divForms")!=null) && (ieversion>=6))
						{
							eval("document.all.divForms").style.height=350;
						}
					}
					else
					{
						var o_divforms;
						o_divforms=document.getElementById("divForms");
						if (o_divforms!=null)
						{
							o_divforms.style.height=window.frames.innerHeight*0.70;
							o_divforms.style.width=window.frames.innerWidth*0.995;
						}
					}
				}
				function setSelectedValue(objField)
				{
				    var strControl=eval('document.forms[0].'+objField);
				    var strControlType=strControl.type;
				    var strControlName=strControl.id;
				    var strName=strControlName.substring(0,strControlName.indexOf("_"));
				    var strHiddenField=document.getElementById(strName+"_Changed");
                    
				    if(strControlType=='select-one')
				    {
                        for(cntItems=0;cntItems<strControl.options.length;cntItems++)
				        {
        					
						        if (strControl.options[cntItems].selected==true)
						        {	
							        if(strHiddenField.value=='')
							            strHiddenField.value= strControlName+ "|" + strControl.options[cntItems].value+";";
							         else
							         {
							            if(strHiddenField.value.indexOf(objField)!=-1)
							            {
							                    var iIndex=strHiddenField.value.indexOf(objField);
							                    var iLength=strHiddenField.value.indexOf(";",iIndex);
							                    var sReplaceText=strHiddenField.value.substring(iIndex,iLength+1);
							                    strHiddenField.value=strHiddenField.value.replace(sReplaceText,"");
							            }
							            strHiddenField.value= strHiddenField.value+strControlName+ "|" + strControl.options[cntItems].value+";"
							          }
							        break;
						        }	
        					
				        }
				        
				    }
				    else
				    {
				        if(strHiddenField.value=='')
				            strHiddenField.value= strControlName+ "|" + strControl.value+";";
				       else
				       {
				           if(strHiddenField.value.indexOf(objField)!=-1)
							                {
							                    var iIndex=strHiddenField.value.indexOf(objField);
							                    var iLength=strHiddenField.value.indexOf(";",iIndex);
							                    var sReplaceText=strHiddenField.value.substring(iIndex,iLength+1);
							                    strHiddenField.value=strHiddenField.value.replace(sReplaceText,"");
							                }
				                strHiddenField.value= strHiddenField.value+strControlName+ "|" + strControl.value+";";
				       }
				    }
				}
				
    </script>
    

</head>
<body onload="onPageLoaded();parent.MDIScreenLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
    <input type="hidden" id="action" runat="server" />
    <asp:HiddenField ID="SelectedId" runat="server" />
    <asp:TextBox ID="Payee" Style="display: none" runat="server" />
    <asp:TextBox Style="display: none" runat="server" ID="lstTransNam_Changed" />
    <asp:TextBox Style="display: none" runat="server" ID="txtFrom_Changed" />
    <asp:TextBox Style="display: none" runat="server" ID="txtTo_Changed" />
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td class="msgheader" colspan="4">
                Transaction Type Change Utility
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td align="center">
                <b>Payment Control Number:</b>&nbsp;&nbsp;
                <asp:TextBox runat="server" ID="txtSearch" onchange="javascript:m_LookupTextChanged=true;"
                    onblur="return lookupLostFocus(this);" size="30" RMXRef="/Instance/Document/TransTypeChange/PaymentCtlNum"></asp:TextBox>
                <asp:Button runat="server" ID="btnLookUp" Text="LookUp" CssClass="button" OnClientClick="javascript:document.forms[0].action.value='Search';" />
                <asp:TextBox runat="server" ID="txtSearch_cid" Style="display: none" RMXRef="/Instance/Document/TransTypeChange/PaymentCtlNumId"></asp:TextBox>
                <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="button" OnClientClick="javascript:m_LookupTextChanged=false;return lookupData('txtSearch','Payment',7,'',12)" />
            </td>
            <td width="*">
                &nbsp;
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="msgheader">
                Payee:
                <%=Payee.Text%>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="ctrlgroup">
                Transaction Detail:
            </td>
        </tr>
    </table>
    <div id="divForms" class="divScroll">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr><%-- bpaskova MITS 37587 added header titles--%>
                <td class="colheader5">
                    &nbsp;Transaction Type 
                </td>
                <td class="colheader5">
                    &nbsp;Reserve Data
                </td>
                <td class="colheader5">
                    &nbsp;From Date
                </td>
                <td class="colheader5">
                    &nbsp;To Date
                </td>
            </tr>
            <%
                if (RecordExists)
                {
                    int i = 0; foreach (XElement item in result)
                    {
                        string rowclass = "";
                        string rowid = "";
                        if ((i % 2) == 1) rowclass = "rowlight1";
                        else rowclass = "rowdark1";
                        i++;
            %>
            <tr class="<%=rowclass%>">
                <td>
                    <select id="lstTransNam_<%=item.Attribute("id").Value%>" onchange="setDataChanged(true);return setSelectedValue(this.id);">
                        <%foreach (var ele in item.XPathSelectElements("Code"))
                          {
                              SelectedId.Value = item.Attribute("transcode").Value;
                              if (ele.Attribute("id").Value == SelectedId.Value)
                              {%>
                        <option selected="selected" label="<%=ele.Attribute("title").Value%>" value="<%=ele.Attribute("id").Value%>" />
                        <%}
                              else
                              {%>
                        <option label="<%=ele.Attribute("title").Value%>" value="<%=ele.Attribute("id").Value%>" />
                        <%}


                          }%>
                    </select>
                </td>
                <td>
                    <%=item.Attribute("reservedesc").Value%>
                </td>
                <td>
                    <input type="text" id="txtFrom_<%=item.Attribute("id").Value%>" onchange="setDataChanged(true);return setSelectedValue(this.id);"
                        onblur="dateLostFocus(this.id);" value="<%=item.Attribute("fromdt").Value %>" />
                   <%-- <input type="button" id="dateFrom_<%=item.Attribute("id").Value%>" value="..." />--%><%--vkumar258 RMA-6037 --%>
                </td>
                <td>
                    <input type="text" id="txtTo_<%=item.Attribute("id").Value%>" onchange="setDataChanged(true);return setSelectedValue(this.id);"
                        onblur="dateLostFocus(this.id);" value="<%=item.Attribute("todt").Value %>" />
                    <%--<input type="button" id="dateTo_<%=item.Attribute("id").Value%>" value="..." />--%><%--vkumar258 RMA-6037 --%>
                </td>
            </tr>
            <% }
                }%>
            <%else
                {%>

            <script language="javascript" type="text/javascript">
                if (document.forms[0].action.value == "Search") 
                    alert("Payment with Control Number " + document.forms[0].txtSearch.value + " not found.");
            </script>

            <% }%>
        </table>
    </div>
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td align="center">
                <asp:Button runat="server" ID="btnSave" class="button" Text="Save Changes" OnClientClick="setDataChanged(false);parent.MDISetUnDirty('1');" OnClick="SaveChanges" />
            </td>
            <td width="*">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="*">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                NOTE: This utility only allows Transaction Types to be changed to another Transaction
                Type of the same Reserve parent.
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
