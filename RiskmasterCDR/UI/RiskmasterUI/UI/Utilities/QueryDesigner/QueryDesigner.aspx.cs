﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
//using Riskmaster.Models;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Utilities.QueryDesigner
{
    public partial class QueryDesigner : System.Web.UI.Page
    {
        private string sPageID = RMXResourceProvider.PageId("QueryDesigner.aspx");
        private string sSearchPageID = RMXResourceProvider.PageId("SearchLocalization.aspx");
        public string sLangId = AppHelper.GetLanguageCode(); 
         
       
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("QueryDesigner.aspx"), "QueryDesignerValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "QueryDesignerValidations", sValidationResources, true);
            XElement oLangId = null;
            XElement PageId = null;
            try
            {
                if (!Page.IsPostBack)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    string sReturn = "";

                    //Preparing XML to send to service
                    XElement oMessageElement = LoadSearchWizardTemplate();
                    //Modify XML 
                    oLangId = oMessageElement.XPathSelectElement("./Document/form/group/LangId");
                    if (oLangId != null)
                    {
                        oLangId.Value = sLangId;
                    }
                    PageId = oMessageElement.XPathSelectElement("./Document/form/group/PageId");
                    if (PageId != null)
                    {
                        PageId.Value = sSearchPageID;
                    }

                    CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

                    FillQueryDesigner(ref oFDMPageDom);

                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (oLangId != null)
                {
                    oLangId = null;
                }
                if (PageId != null)
                {
                    PageId = null;
                }
            }           
        }
        public void FillQueryDesigner(ref XmlDocument oFDMPageDom)
        {
            if (oFDMPageDom.SelectSingleNode("//SearchView") != null)
            {
                DataSet aDataSet = new DataSet();
                aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("SearchView")[0].OuterXml));
                rptSearchCategory.DataSource = aDataSet.Tables["SearchCategory"];
                ViewState["oFDMPageDom"] = oFDMPageDom.GetElementsByTagName("SearchView")[0].OuterXml;
                rptSearchCategory.DataBind();
                //Bijender Mits 15065
                hdnsearchname.Text = oFDMPageDom.GetElementsByTagName("SearchNames")[0].InnerText;                
                //Bijender End Mits 15065

            }
        }
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }
        /// <summary>
        /// CWS request message template for SearchWizard
        /// </summary>
        /// <returns></returns>
        private XElement LoadSearchWizardTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>QueryDesignerAdaptor.Get</Function> 
                      </Call>
                      <Document>
                        <form>
                            <group>
                                <ViewID/>
                                <SearchView/> 
                                <LangId></LangId>  
                                <PageId></PageId>
                            </group>
                        </form>
                      </Document>
                </Message>


            ");
            return oTemplate;
        }
        protected void rptSearchCategory_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            //ListItemType lt = e.Item.ItemType;
            //if (lt == ListItemType.Item || lt == ListItemType.AlternatingItem)
            //{
            //    System.Web.UI.WebControls.Image img = e.Item.FindControl("CompanyNameImage") as System.Web.UI.WebControls.Image;
            //    if (img != null)
            //    {
            //        DataRowView dv = e.Item.DataItem as DataRowView;
            //        if (dv != null)
            //        {
            //            string cName = dv["CompanyName"] as string;
            //            if (cName.Length > 30)
            //            {
            //                cName = "Too Long!";
            //                img.ImageUrl = "ImageMaker.ashx?CName=" + Server.UrlEncode(cName) + "&color=red";
            //            }
            //            else
            //                img.ImageUrl = "ImageMaker.ashx?CName=" + Server.UrlEncode(cName);

                        Repeater rptSearchNames = (Repeater)e.Item.FindControl("rptSearchNames");
                        if (rptSearchNames != null)
                        {
                            if (ViewState["oFDMPageDom"] != null)
                            {
                                XmlDocument oFDMPageDom = new XmlDocument();
                                oFDMPageDom.LoadXml(ViewState["oFDMPageDom"].ToString());
                                DataSet aDataSet = new DataSet();
                                aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("SearchCategory")[e.Item.ItemIndex].OuterXml));
                                rptSearchNames.DataSource = aDataSet.Tables["Search"];
                                //rptSearchNames.DataSource = dv.CreateChildView("CustomerOrders");
                                rptSearchNames.DataBind();
                            }
                        }
        //            }
        //        }
        //    }
        }

        protected void DeleteSearches(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            //Preparing XML to send to service
            XElement oMessageElement = LoadSearchWizardTemplate();
            //Modify XML 
            XElement oFunctionName = oMessageElement.XPathSelectElement("./Call/Function");
            if (oFunctionName != null)
            {
                oFunctionName.Value = "QueryDesignerAdaptor.Delete";
            }
            XElement oViewID = oMessageElement.XPathSelectElement("./Document/form/group/ViewID");
            if (oViewID != null)
                oViewID.Value = hdnDelete.Value;
            XElement oLangId = oMessageElement.XPathSelectElement("./Document/form/group/LangId");
            if (oLangId != null)
            {
                oLangId.Value = sLangId;
            }
            XElement PageId = oMessageElement.XPathSelectElement("./Document/form/group/PageId");
            if (PageId != null)
            {
                PageId.Value = sSearchPageID;
            }
            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
            FillQueryDesigner(ref oFDMPageDom);
        }
    }
}
