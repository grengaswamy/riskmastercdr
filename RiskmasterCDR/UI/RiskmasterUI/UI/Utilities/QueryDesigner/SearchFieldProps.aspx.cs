﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.UI.Utilities.QueryDesigner
{
    public partial class SearchFieldProps : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string selectedtype = AppHelper.GetQueryStringValue("selectedtype");
                if (selectedtype == "3" || selectedtype == "14")
                {
                    ListItem listval = new ListItem();
                    listval.Value = "3";
                    listval.Text ="Single-Code";
                    fieldprops.Items.Add(listval);

                    listval = new ListItem();
                    listval.Value = "14";
                    listval.Text = "Multi-Code";
                    fieldprops.Items.Add(listval);
                }
                else if (selectedtype == "6" || selectedtype == "15")
                {
                    ListItem listval = new ListItem();
                    listval.Value = "6";
                    listval.Text = "Single-State";
                    fieldprops.Items.Add(listval);

                    listval = new ListItem();
                    listval.Value = "15";
                    listval.Text = "Multi-State";
                    fieldprops.Items.Add(listval);
                }
                fieldprops.SelectedValue = selectedtype;
            }
        }
    }
}
