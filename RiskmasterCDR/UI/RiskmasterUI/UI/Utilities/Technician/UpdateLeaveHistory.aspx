<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateLeaveHistory.aspx.cs" Inherits="Riskmaster.UI.Utilities.Technician.UpdateLeaveHistory" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head id="Head1" runat="server">
  <title>Leave History Update</title>
  <link rel="stylesheet" href="../../../Content/system.css" type="text/css"/>
 </head>
 <body>
  <form id="frmdata" name="frmData" method="post" runat="server">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  <table border="1" cellspacing="0" cellpadding="0" width="70%" align="center">
    <tr>
     <td colspan="2" class="Bold1" align="center">Leave History Update Completed Successfully</td>
    </tr>
   </table><asp:TextBox visible="false" runat="server" type="hidden" name="" id="SysWindowId" rmxref="/Instance/Document/planid"/>
   </form>
 </body>
</html>