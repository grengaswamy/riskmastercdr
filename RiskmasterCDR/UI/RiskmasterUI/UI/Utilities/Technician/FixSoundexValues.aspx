﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FixSoundexValues.aspx.cs" Inherits="Riskmaster.UI.Utilities.Technician.FixSoundexValues" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Rebuild SOUNDEX Values</title>
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="form1" runat="server" method="post">
    <uc1:ErrorControl id="ErrorControl1" runat="server" />
    <table border="1" cellspacing="0" cellpadding="0" width="70%" align="center">
    <tr>
     <td colspan="2" class="Bold1" align="center">Rebuild of SOUNDEX values completed Successfully</td>
    </tr>
    </table>
    </form>
</body>
</html>
