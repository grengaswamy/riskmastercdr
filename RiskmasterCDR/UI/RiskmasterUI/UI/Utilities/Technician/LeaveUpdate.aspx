<%@ Page Language="C#" EnableViewState="true" AutoEventWireup="true" CodeBehind="LeaveUpdate.aspx.cs" Inherits="Riskmaster.UI.Utilities.Technician.LeaveUpdate" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head runat="server">
  <title>Transaction Type Change Utility</title>

  <link rel="stylesheet" href="../../../Content/system.css" type="text/css"/>
  <script  src="../../../Scripts/form.js" type="text/javascript"></script>
  <script type="text/javascript">
				var ns, ie, ieversion;
				var browserName = navigator.appName;                   // detect browser 
				var browserVersion = navigator.appVersion;
				if (browserName == "Netscape")
				{
					ie=0;
					ns=1;
				}
				else		//Assume IE
				{
				ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
				ie=1;
				ns=0;
				}
				function onPageLoaded()
				{
					if (ie)
					{
						if ((eval("document.all.divForms")!=null) && (ieversion>=6))
						{
							eval("document.all.divForms").style.height=350;
						}
					}
					else
					{
						var o_divforms;
						o_divforms=document.getElementById("divForms");
						if (o_divforms!=null)
						{
							o_divforms.style.height=window.frames.innerHeight*0.70;
							o_divforms.style.width=window.frames.innerWidth*0.995;
						}
					}
				}
				function UpdateLeaveHistory()
				{
					var iPlan = "";
					iPlan = document.forms[0].lstLeavePlan[document.forms[0].lstLeavePlan.selectedIndex].value;
					window.open("../Technician/UpdateLeaveHistory.aspx?planid="+ iPlan, "LeaveHistory","width=400,height=200"+",top="+(screen.availHeight-290)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
				}
				function UpdateLeaveRecords()
				{
					var iPlan = "";
					iPlan = document.forms[0].lstLeavePlan[document.forms[0].lstLeavePlan.selectedIndex].value;
					window.open("../Technician/UpdateLeaveRecords.aspx?planid="+ iPlan, "LeaveUpdate","width=400,height=200"+",top="+(screen.availHeight-290)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
				}
			</script>
 </head>
 <body onload="javascript:onPageLoaded();parent.MDIScreenLoaded()">
  <form id="frmData"  method="post" runat="server">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  
  <table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td class="msgheader" colspan="4">Update Leave History and Records</td>
    </tr>
   </table>
   <table cellspacing="0" cellpadding="0" border="0">
    <tr>
     <td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
    </tr>
    <tr>
     <td align="left">Optional : Update Selected Leave Plan &nbsp;</td>
     <td><asp:DropDownList type="combobox" runat="server" rmxref="/Instance/Document/LeavePlans" id="lstLeavePlan" ItemSetRef="/Instance/Document/LeavePlans/leaveplan" AutoPostBack="false" ></asp:DropDownList>
     </td>
    </tr>
    <tr>
     <td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
    </tr>
    <tr>
     <td colspan="2">
     <asp:Button runat="server" type="button" id="btnLeaveHist" Text="Update Leave History" class="button" onclientclick="UpdateLeaveHistory();"/>
     <asp:Button runat="server" type="button" id="btnLeaveRec" Text="Update Leave Records" class="button" onclientclick="UpdateLeaveRecords();"/>
     </td>
    </tr>
   </table>
  </form>
 </body>
</html>