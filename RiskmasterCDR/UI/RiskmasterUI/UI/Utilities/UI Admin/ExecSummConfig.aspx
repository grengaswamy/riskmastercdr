﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExecSummConfig.aspx.cs"
    Inherits="Riskmaster.UI.ExecSummConfig.ExecSummConfig" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Executive Summary Configuration</title>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/ExecutiveSummary.js"></script>

    <%--igupta3 Mits : 31732 Changes starts--%>

    <script type="text/javascript">
        function removeOption(selectName, id) {
            select = document.getElementById(selectName);
            selecttxtObject = document.getElementById('txtRelatedComponents');
            Ids = new Array();
            Names = new Array();

            for (var x = 0; x < select.options.length; x++) {
                if (select.options[x].value != id) {
                    Ids[Ids.length] = select.options[x].value;
                    Names[Names.length] = select.options[x].text;

                }
            }

            select.options.length = Ids.length;
            if (selectName == 'lstUsersGroups') {
                selecttxtObject.value = ""
            }
            for (var x = 0; x < select.options.length; x++) {
                select.options[x].text = Names[x];
                select.options[x].value = Ids[x];
                if (selectName == 'lstUsersGroups') {
                    if (selecttxtObject.value == "") {
                        selecttxtObject.value = Ids[x];
                    }
                    else {
                        selecttxtObject.value = selecttxtObject.value + ',' + Ids[x];
                    }
                }
            }
        }
        function AddFilter(mode) {

            var optionRank;
            var optionObject;

            selectObject = document.getElementById('lstUsersGroups');
            selecttxtObject = document.getElementById('txtRelatedComponents');

            if (mode == "selected") {

                //Add selected Available Values
                select = document.getElementById('cboAllUsersGroups');
                for (var x = select.options.length - 1; x >= 0; x--) {
                    if (select.options[x].selected == true) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);

                        optionRank = selectObject.options.length;
                        selectObject.options[optionRank] = optionObject;
                        if (selecttxtObject.value == "") {
                            selecttxtObject.value = select.options[x].value;
                        }
                        else {
                            selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;
                        }
                        //remove from available list	
                        removeOption('cboAllUsersGroups', select.options[x].value);
                        setDataChanged(true);
                    }
                }
            }
            else {
                //Add All Available Values
                select = document.getElementById('cboAllUsersGroups');

                if (select.options.length > 0) {
                    for (var x = select.options.length - 1; x >= 0; x--) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;
                        if (selecttxtObject.value == "") {
                            selecttxtObject.value = select.options[x].value;
                        }
                        else {
                            selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;
                        }
                        selectObject.options[optionRank] = optionObject;
                        setDataChanged(true);
                    }
                    select.options.length = 0;
                }
            }
            return false;
        }

        function RemoveFilter(mode) {

            var optionRank;
            var optionObject;
            selectObject = document.getElementById('cboAllUsersGroups');
            selecttxtObject = document.getElementById('txtRelatedComponents');
            if (mode == "selected") {
                //Add selected Available Values
                select = document.getElementById('lstUsersGroups');
                for (var x = select.options.length - 1; x >= 0; x--) {
                    if (select.options[x].selected == true) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;
                        selectObject.options[optionRank] = optionObject;

                        //remove from available list	
                        removeOption('lstUsersGroups', select.options[x].value);
                        setDataChanged(true);
                    }
                }
            }
            else {
                //ADD All Available Values
                select = document.getElementById('lstUsersGroups');

                if (select.options.length > 0) {

                    for (var x = select.options.length - 1; x >= 0; x--) {

                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;


                        selectObject.options[optionRank] = optionObject;

                        selecttxtObject.value = "";

                        setDataChanged(true);


                    }
                    select.options.length = 0;
                }
            }
            return false;
        }
					
				
                </script>
                   
    <style type="text/css">
          .style1
          {
              width: 3%;
          }
      </style>
       <%--igupta3 Mits : 31732 Changes ends--%>

</head>
<body onload="OnLoad();loadTabList();parent.MDIScreenLoaded()">
    <form id="frmData" runat="server">
    <div class="msgheader" id="formtitle">
        Executive Summary Configuration</div>
        
        <%--Bijender Start Mits 14894--%>
    <div class="tabGroup" id="TabsDivGroup" runat="server">
    
         <div class="Selected" nowrap="true" runat="server" name="TABSevents" id="TABSevents">
            <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="events" id="LINKTABSevents">Events</a>
         </div>
         <div class="tabSpace" runat="server" id="Div1">
            &nbsp;&nbsp;
         </div>
         
         <div class="NotSelected" nowrap="true" runat="server" name="TABSallclaims" id="TABSallclaims">
            <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="allclaims" id="LINKTABSallclaims">Claims (All)</a>
         </div>
         <div class="tabSpace" runat="server" id="TBSFundSettings">
              &nbsp;&nbsp;
         </div>
        
        <div class="NotSelected" nowrap="true" runat="server" name="TABSgcclaims" id="TABSgcclaims">
            <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="gcclaims" id="LINKTABSgcclaims">Claims (GC)</a>
         </div>
         
         <div class="tabSpace" runat="server" id="TBSEmployee">
             &nbsp;&nbsp;             
         </div>
        
         <div class="NotSelected" nowrap="true" runat="server" name="TABSvaclaims" id="TABSvaclaims">
            <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="vaclaims" id="LINKTABSvaclaims">Claims (VA)</a>
         </div>
          <div class="tabSpace" runat="server" id="TBSDiaries">
                &nbsp;&nbsp;
         </div>
         
         <div class="NotSelected" nowrap="true" runat="server" name="TABSwcclaims" id="TABSwcclaims">
            <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="wcclaims" id="LINKTABSwcclaims">Claims (WC)</a>
         </div>
          <div class="tabSpace" runat="server" id="Div3">
             &nbsp;&nbsp;
         </div>
         
         <div class="NotSelected" nowrap="true" runat="server" name="TABSdiclaims" id="TABSdiclaims">
            <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="diclaims" id="LINKTABSdiclaims">Claims (DI)</a>
         </div>
          <div class="tabSpace" runat="server" id="Div5">
              &nbsp;&nbsp;
         </div>         

          <%--Sumit - MITS# 18145 - 10/15/2009 Fields to be added for PC Claim--%>
         <div class="NotSelected" nowrap="true" runat="server" name="TABSpcclaims" id="TABSpcclaims">
            <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="pcclaims" id="LINKTABSpcclaims">Claims (PC)</a>
         </div>
          <div class="tabSpace" runat="server" id="Div6">
              &nbsp;&nbsp;
         </div>                  
         
         <%-- gbhatnagar - MITS# 21226 --%>
         <div class="NotSelected" nowrap="true" runat="server" name="TABSpolicy" id="TABSpolicy">
            <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="policy" id="LINKTABSpolicy">Policy Tracking</a>
         </div>
         
         <div class="tabSpace" runat="server" id="Div4">
              &nbsp;&nbsp;
         </div>         

         <div class="NotSelected" nowrap="true" runat="server" name="TABSpolicymgmt" id="TABSpolicymgmt">         
            <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="policymgmt" id="LINKTABSpolicymgmt">Policy Management</a>
         </div>
         <%-- gbhatnagar - MITS# 21226 --%>
          <div class="tabSpace" runat="server" id="Div2">
              &nbsp;&nbsp;
         </div>     
        
      </div>
      <%--Bijender End Mits 14894--%>
      
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
       
        <tr valign="top">
            <td width="100%" valign="top">
                <div style="position: relative; left: 0; top: 0; width: 950px; height: 280px; overflow: auto; vertical-align: top" class="singletopborder">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr valign="top">
                            <td valign="top">
                                <%--The Name of the Checkbox control should be chk + <Last Element of RMXRef> + <Second Last Element of RMXRef>
                                for Security Settings to work--%>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" name="FORMTABevents"
                                    id="FORMTABevents">
                                    <tr>
                                        <td width="18%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EventInformation"
                                                ID="chkEventInformationEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="events" />Event Information
                                        </td>
                                        <td width="18%" valign="top" nowrap=true>
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/MedwatchInfo"
                                                ID="chkMedwatchInfoEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="9" tab="events" />MedWatch Information
                                        </td>
                                        <td width="18%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/PersonInvolved"
                                                ID="chkPersonInvolvedEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="17" tab="events" />Person Involved
                                        </td>
                                        <%-- Merging Changes for MITS 12824 --%>
                                        <td width="18%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EnhancedNotes"
                                                ID="chkEnhancedNotesEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="16" tab="events" />EnhancedNotes
                                        </td>
                                        <%--rupal:start, r8 enh for case mgt --%>
                                         <td width="40%"  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/CaseManagementInformation"
                                                ID="chkCaseManagementInformationEvent"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="10" tab="wcclaims" />Case Management Information
                                        </td>
                                        <%--end:rupal --%>
                                    </tr>
                                    <tr>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EventDetail"
                                                ID="chkEventDetailEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="2" tab="events" />Event Detail
                                        </td>
                                        <td  valign="top" nowrap=true>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EquipIndicator"
                                                ID="chkEquipIndicatorEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="10" tab="events" />Equipment Indicator
                                        </td>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Patient"
                                                ID="chkPatientEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="18" tab="events" />Patient Involved
                                        </td>
                                        <%-- Merging Changes for MITS 12824 --%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/ActivityDate"
                                                ID="chkActivityDateEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="25" tab="events" />Activity Date
                                        </td>
                                        <%--rupal:start, r8 enh for case mgt --%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/CaseManagerInformation"
                                                ID="chkCaseManagerInformationEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="11" tab="wcclaims" />Case Manager Information
                                        </td>
                                        <%--end:rupal --%>
                                    </tr>
                                    <tr>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EventDatedText"
                                                ID="chkEventDatedTextEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="3" tab="events" />Event Dated Text
                                        </td>
                                        <td  valign="top" nowrap=true>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/MedicationIndicator"
                                                ID="chkMedicationIndicatorEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="11" tab="events" />Medication Indicator
                                        </td>
                                        <%--rupal:start --%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/PatientInvolvedInjuryLoss"
                                                ID="chkPatientInvolvedInjuryLossEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="events" />Injury Loss
                                        </td>
                                        <%--rupal:end %>
                                        <%-- Merging Changes for MITS 12824 --%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/DateCreated"
                                                ID="chkDateCreatedEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="26" tab="events" />Date Created
                                        </td>
                                        <%--rupal:start, r8 enh for case mgt --%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/CaseManagerNotes"
                                                ID="chkCaseManagerNotesEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="12" tab="wcclaims" />Case Manager Notes
                                        </td>
                                        <%--end:rupal --%>
                                    </tr>
                                    <tr>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/ReportedInfo"
                                                ID="chkReportedInfoEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="4" tab="events" />Reported Information
                                        </td>
                                        <td  valign="top" nowrap=true>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/ConcomitantProduct"
                                                ID="chkConcomitantProductEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="12" tab="events" />Concomitant Product
                                        </td>
                                        <%--rupal:start--%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Physicians"
                                                ID="chkPhysiciansEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="19" tab="events" />Physicians
                                        </td>
                                        <%--rupal:end --%>                     
                                        <%-- Merging Changes for MITS 12824 --%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EnteredBy"
                                                ID="chkEnteredByEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="27" tab="events" />Entered By
                                        </td>
                                          <%--rupal:start, r8 enh for case mgt --%>
                                       <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/RestrictedWork"
                                                ID="chkRestrictedWorkEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="13" tab="wcclaims" />Restricted Work
                                        </td>
                                        <%--end:rupal --%>
                                    </tr>
                                    <tr>
                                    <!--asingh263 MITS 30541 starts-->
                                        <td valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/HideTaxIdSSN"
                                                ID="chkHideTaxIdSSNEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="events" />Hide/Unhide SSN
                                        </td>
                                        <!--asingh263 MITs 30541 ends-->
                                        <%--  <td valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/FollowupInfo"
                                                ID="chkFollowUpInfoEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="5" tab="events" />Follow Up Information
                                        </td>--%>
                                        <td valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/MedwatchTest"
                                                ID="chkMedWatchTestEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="13" tab="events" />MedWatch Test
                                        </td>
                                        <%--rupal:start --%>                                         
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/PhysicianInjuryLoss"
                                                ID="chkPhysicianInjuryLossEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="events" />Injury Loss
                                        </td>
                                        <%--rupal:end --%>
                                        <%-- Merging Changes for MITS 12824 --%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/NoteType"
                                                ID="chkNoteTypeEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="28" tab="events" />Note Type
                                        </td>
                                        <%--rupal:start, r8 enh for case mgt --%>
                                      <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/WorkLoss"
                                                ID="chkWorkLossEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="14" tab="wcclaims" />Work Loss
                                        </td>
                                        <%--end:rupal --%>
                                    </tr>
                                    <tr>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/OshaInfo"
                                                ID="chkOshaInfoEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="6" tab="events" />OSHA Information
                                        </td>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/DiaryDetails"
                                                ID="chkDiaryDetailsEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="14" tab="events" />Diary Details
                                        </td>
                                        <%--rupal:start --%>     
                                          <td  valign="top" nowrap=true>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/OtherMedicalStaff"
                                                ID="chkOtherMedicalStaffEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="20" tab="events" />Other Medical Staff
                                        </td>
                                        
                                        <%--rupal:end --%>     
                                        <%-- Merging Changes for MITS 12293%-->
            <%--<td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/QualityManagement" id="chkQualityManagement"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="22" tab="events"/>Quality Management--%>                                       
                                        <%-- Merging Changes for MITS 12824 --%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/UserType"
                                                ID="chkUserTypeEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="28" tab="events" />User Type
                                        </td>
                                         <%--rupal:start, r8 enh for case mgt --%>
                                       <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/TreatmentPlan"
                                                ID="chkTreatmentPlanEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="15" tab="wcclaims" />Treatment Plan
                                        </td>
                                        <%--end:rupal --%>
                                    </tr>
                                    <tr>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/ClaimInfo"
                                                ID="chkClaimInfoEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="7" tab="events" />Claim Information
                                        </td>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Supp"
                                                ID="chkSuppEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="15" tab="events" />Supplemental Fields
                                        </td>
                                        <%--rupal:start --%>                                         
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/OtherMedicalStaffInjuryLoss"
                                                ID="chkOtherMedicalStaffInjuryLossEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="events" />Injury Loss
                                        </td>
                                        <%--rupal:end %>
                                        <%-- Merging Changes for MITS 12824 --%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/NoteText"
                                                ID="chkNoteTextEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="24" tab="events" />Note Text
                                        </td>
                                         <%--rupal:start, r8 enh for case mgt --%>
                                       <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/MedicalManagementSavings"
                                                ID="chkMedicalManagementSavingsEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="16" tab="wcclaims" />Medical Management Savings
                                        </td>
                                        <%--end:rupal --%>
                                    </tr>
                                    <tr>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/FallInfo"
                                                ID="chkFallInfoEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="8" tab="events" />Fall Information
                                        </td>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Comment"
                                                ID="chkCommentEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="16" tab="events" />Comments
                                        </td>
                                         <%--rupal:start --%>
                                        <td  valign="top" nowrap=true>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Employees"
                                                ID="chkEmployeesEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="21" tab="events" />Employees Involved
                                        </td>
                                          <%-- Zmohamamd MITS 31048--%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Subject"
                                                ID="chkSubjectEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="35" tab="events" />Subject
                                        </td>
                                        <%--rupal:end --%>
                                        <%--rupal:start, r8 enh for case mgt --%>
                                        <td  valign="top"></td>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Accommodations"
                                                ID="chkAccommodationsEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="17" tab="wcclaims" />Accommodations
                                        </td>
                                        <%--end:rupal --%>
                                    </tr>
                                    <tr>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/QualityManagement"
                                                ID="chkQualityManagementEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="24" tab="events" />Quality Management
                                        </td>
                                        <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Intervention"
                                                ID="chkInterventionEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="25" tab="events" />Intervention
                                        </td>
                                          <%--rupal:start --%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EmployeeInjuryLoss"
                                                ID="chkEmpInvolvedInjuryLossEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="events" />Injury Loss
                                        </td>
                                        <%--rupal:end --%>
                                         <%--rupal:start, r8 enh for case mgt --%>
                                        <td  valign="top"></td>
                                       <td  valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/VocationalRehabilitation"
                                                ID="chkVocationalRehabilitationEvent"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="18" tab="wcclaims" />Vocational Rehabilitation
                                        </td>
                                        <%--end:rupal --%>
                                       
                                    </tr>
                                    <%--rupal:start --%>
                                    <tr>
                                      
                                        <td valign="top" nowrap=true>
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/FollowupInfo"
                                                ID="chkFollowUpInfoEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="5" tab="events" />Follow Up Information
                                       
                                        </td>
                                        <td  valign="top"></td>
                                      <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/OtherPersons"
                                                ID="chkOtherPersonsEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="23" tab="events" />Other Persons
                                        </td>
                                    </tr>                                     
                                    <tr>
                                        <td valign="top">                                           
                                        </td>
                                        <td  valign="top"></td>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/OtherPersonsInjuryLoss"
                                                ID="chkOtherPersonInjuryLossEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="events" />Injury Loss
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">                                           
                                        </td>
                                        <td valign="top"></td>
                                        <td valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Witness"
                                                ID="chkWitnessEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="22" tab="events" />Witness Involved
                                        </td>
                                    </tr>                                     
                                    <tr>
                                        <td valign="top">                                           
                                        </td>
                                        <td valign="top"></td>
                                        <td valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/WitnessInjuryLoss"
                                                ID="chkWitnessInjuryLossEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="events" />Injury Loss
                                        </td>
                                    </tr>                                                                  
                                    <%--rupal:end --%>
                                    <%--Aman Driver Enh --%>
                                      <tr>
                                        <td valign="top">                                           
                                        </td>
                                        <td valign="top"></td>
                                        <td valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Driver"
                                                ID="chkDriverEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="22" tab="events" />Driver Involved
                                        </td>
                                    </tr>                                     
                                    <tr>
                                        <td valign="top">                                           
                                        </td>
                                        <td valign="top"></td>
                                        <td valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/DriverInjuryLoss"
                                                ID="chkDriverInjuryLossEvent" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="events" />Injury Loss
                                        </td>
                                    </tr>      
                                    <%--Aman Driver Enh --%>

                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" name="FORMTABallclaims"
                                    id="FORMTABallclaims" style="display: none;">
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/ClaimInformation"
                                                ID="chkClaimInformationAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="allclaims" />Claim Information
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/LitigationInformation"
                                                ID="chkLitigationInformationAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="11" tab="allclaims" />Litigation Information
                                        </td>
                                        <%-- Merging Changes for MITS 12824--%>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/EnhancedNotes"
                                                ID="chkEnhancedNotesAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="23" tab="allclaims" />Enhanced Notes
                                        </td>
                                        <%-- skhare7 R8 Enhancements--%>
                                        <%-- <td width="30%" valign="top">--%>
                                          <%--  <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/Subrogation"
                                                ID="chkSubrogationAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="27" tab="allclaims" />Subrogation--%>
                                     <%--   </td>--%>
                                          <%-- skhare7 R8 Enhancements--%>
                                    </tr>
                                    <tr>
                                     <%-- added by rkaur7 for MITS 16668--%>
                                        <td width="30%" valign="top">
                                            <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/PolicyStates"
                                                ID="chkPolicyStatesAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="2" tab="allclaims" />Policy States--%>
                                                <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/ClaimLevelReserves"
                                                ID="chkClaimLevelReservesAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="2" tab="allclaims" />Claim Level Reserves                                                

                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/AttorneyInformation"
                                                ID="chkAttorneyInformationAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="12" tab="allclaims" />Attorney Information
                                        </td>
                                        <%-- Merging Changes for MITS 12824--%>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/ActivityDate"
                                                ID="chkActivityDateAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="24" tab="allclaims" />Actvity Date
                                        </td>
                                         <%-- skhare7 R8 Enhancements--%>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/Arbitration"
                                                ID="chkArbitrationAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="28" tab="allclaims" />Arbitration
                                        </td>
                                         <%-- skhare7 R8 Enhancements--%>
                                    </tr>
                                    <tr>
                                    <td width="30%" valign="top">
                                    <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/PaymentHistory"
                                                ID="chkPaymentHistoryAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="3" tab="allclaims" />Payment History
                                        </td>                                       
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/ExpertWitness"
                                                ID="chkExpertWitnessAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="13" tab="allclaims" />Expert Witness
                                        </td>
                                        <%-- Merging Changes for MITS 12824--%>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/DateCreated"
                                                ID="chkDateCreatedAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="25" tab="allclaims" />Date Created
                                        </td>
                                         <%-- skhare7 R8 Enhancements--%>
                                         <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/Liability"
                                                ID="chkLiabilityAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="29" tab="allclaims" />Liability Loss
                                        </td>
                                         <%-- skhare7 R8 Enhancements--%>
                                    </tr>
                                    <tr>
                                     <td width="30%" valign="top">
                                          <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/FundsManagement"
                                                ID="chkFundsManagementAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="4" tab="allclaims" />Funds Management    
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/LitDemandOffer"
                                                ID="chkLitDemandOfferAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="14" tab="allclaims" />Demand Offer
                                        </td>
                                        <%-- Merging Changes for MITS 12824--%>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/EnteredBy"
                                                ID="chkEnteredByAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="26" tab="allclaims" />Entered By
                                        </td>
                                        <%--asingh263:MITS 30541:start --%>
                                        <td width="30%" valign="top" nowrap="true">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/ACHideTaxIdSSN"
                                                ID="chkACHideTaxIdSSNAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="allclaims" />Hide/Unhide SSN
                                        </td>
                                        <%--asingh263:MITS 30541:end --%>
                                    </tr>
                                    <tr>
                                    <td width="30%" valign="top">
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/TransactionDetail"
                                                ID="chkTransactionDetailAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="5" tab="allclaims" />Transaction Detail
                                        </td>
                                       
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/SubrogationInformation"
                                                ID="chkSubrogationInformationAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="15" tab="allclaims" />Subrogation Information
                                        </td>
                                        <%-- Merging Changes for MITS 12824--%>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/NoteType"
                                                ID="chkNoteTypeAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="27" tab="allclaims" />Note Type
                                        </td>
                                    </tr>
                                    <tr>
                                    
                                     <td width="30%" valign="top">
                                         <asp:CheckBox runat="server" name="$node^114" value="True" appearance="full" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/EventDetail"
                                                ID="chkEventDetailAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="6" tab="allclaims" />Event Detail 
                                        </td>
                                      
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/SubroDemandOffer"
                                                ID="chkSubroDemandOfferAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="16" tab="allclaims" />Demand Offer
                                        </td>
                                        <%-- Merging Changes for MITS 12824--%>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/UserType"
                                                ID="chkUserTypeAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="28" tab="allclaims" />User Type
                                        </td>
                                    </tr>
                                    <tr>
                                    
                                      <td width="30%" valign="top">
                                          <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/AdjusterInformation"
                                                ID="chkAdjusterInformationAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="8" tab="allclaims" />Adjuster Information 
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/DefendantInformation"
                                                ID="chkDefendantInformationAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="17" tab="allclaims" />Defendant Information
                                        </td>
                                        <%-- Merging Changes for MITS 12824--%>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/NoteText"
                                                ID="chkNoteTextAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="29" tab="allclaims" />Note Text
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/PersonsInvolved"
                                                ID="chkPersonsInvolvedAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="7" tab="allclaims" />Persons Involved
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/DefAttorneyInformation"
                                                ID="chkDefAttorneyInformationAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="18" tab="allclaims" />Attorney Information
                                        </td>
                                        <%-- Zmohamamd MITS 31048--%>
                                         <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/Subject"
                                                ID="chkSubjectAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="35" tab="allclaims" />Subject
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/AdjusterDatedText"
                                                ID="chkAdjusterDatedTextAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="9" tab="allclaims" />Adjuster Dated Text   
                                        </td>
                                       
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/ClaimInvolvementHistory"
                                                ID="chkClaimInvolvementHistoryAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="19" tab="allclaims" />Claim Involvement History
                                        </td>
                                    </tr>
                                    <tr>
                                     <td width="30%" valign="top">
                                           
                                     </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/EnteredUserFullName"
                                                ID="chkEnteredUserFullNameAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="10" tab="allclaims" />Entered User Full Name
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" appearance="full" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/DiaryDetails"
                                                ID="chkDiaryDetailsAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="20" tab="allclaims" />Diary Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/SupplementalFields"
                                                ID="chkSupplementalFieldsAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="21" tab="allclaims" />Supplemental Fields
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/Comments"
                                                ID="chkCommentsAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="22" tab="allclaims" />Comments
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" name="FORMTABgcclaims"
                                    id="FORMTABgcclaims" style="display: none;">


                                    <!--MITS 25743 Start-->
                                    
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimInformationAllClaims"
                                                ID="chkClaimInformationAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="1" tab="allclaims" />Claim Information
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/LitigationInformationAllClaims"
                                                ID="chkLitigationInformationAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="11" tab="allclaims" />Litigation Information
                                        </td>
                                  
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/EnhancedNotesAllClaims"
                                                ID="chkEnhancedNotesAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="23" tab="allclaims" />Enhanced Notes
                                        </td>
                                       
                                    </tr>
                                    <tr>
                                        
                                        <td width="30%" valign="top">
                                          
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimLevelReservesAllClaims"
                                                ID="chkClaimLevelReservesAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="2" tab="allclaims" />Claim Level Reserves
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/AttorneyInformationAllClaims"
                                                ID="chkAttorneyInformationAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="12" tab="allclaims" />Attorney Information
                                        </td>
                                      
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ActivityDateAllClaims"
                                                ID="chkActivityDateAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="24" tab="allclaims" />Actvity Date
                                        </td>
                                     
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ArbitrationAllClaims"
                                                ID="chkArbitrationAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="28" tab="allclaims" />Arbitration
                                        </td>
                                     
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/PaymentHistoryAllClaims"
                                                ID="chkPaymentHistoryAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="3" tab="allclaims" />Payment History
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ExpertWitnessAllClaims"
                                                ID="chkExpertWitnessAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="13" tab="allclaims" />Expert Witness
                                        </td>
                                 
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/DateCreatedAllClaims"
                                                ID="chkDateCreatedAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="25" tab="allclaims" />Date Created
                                        </td>
                                       
                                        <td width="30%" nowrap=true valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/LiabilityAllClaims"
                                                ID="chkLiabilityAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="29" tab="allclaims" />Liability Loss
                                        </td>
                                    
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/FundsManagementAllClaims"
                                                ID="chkFundsManagementAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="4" tab="allclaims" />Funds Management
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/LitDemandOfferAllClaims"
                                                ID="chkLitDemandOfferAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="14" tab="allclaims" />Demand Offer
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/EnteredByAllClaims"
                                                ID="chkEnteredByAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="26" tab="allclaims" />Entered By
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/TransactionDetailAllClaims"
                                                ID="chkTransactionDetailAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="5" tab="allclaims" />Transaction Detail
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/SubrogationInformationAllClaims"
                                                ID="chkSubrogationInformationAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="15" tab="allclaims" />Subrogation Information
                                        </td>
                                 
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/NoteTypeAllClaims"
                                                ID="chkNoteTypeAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="27" tab="allclaims" />Note Type
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" name="$node^114" value="True" appearance="full" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/EventDetailAllClaims"
                                                ID="chkEventDetailAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="6" tab="allclaims" />Event Detail
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/SubroDemandOfferAllClaims"
                                                ID="chkSubroDemandOfferAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="16" tab="allclaims" />Demand Offer
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/UserTypeAllClaims"
                                                ID="chkUserTypeAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="28" tab="allclaims" />User Type
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/AdjusterInformationAllClaims"
                                                ID="chkAdjusterInformationAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="8" tab="allclaims" />Adjuster Information
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/DefendantInformationAllClaims"
                                                ID="chkDefendantInformationAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="17" tab="allclaims" />Defendant Information
                                        </td>
                                        <%-- Merging Changes for MITS 12824--%>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/NoteTextAllClaims"
                                                ID="chkNoteTextAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="29" tab="allclaims" />Note Text
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/PersonsInvolvedAllClaims"
                                                ID="chkPersonsInvolvedAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="7" tab="allclaims" />Persons Involved
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/DefAttorneyInformationAllClaims"
                                                ID="chkDefAttorneyInformationAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="18" tab="allclaims" />Attorney Information
                                        </td>
                                        <%-- Zmohamamd MITS 31048--%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/SubjectAllClaims"
                                                ID="chkSubjectAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="35" tab="allclaims" />Subject
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/AdjusterDatedTextAllClaims"
                                                ID="chkAdjusterDatedTextAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="9" tab="allclaims" />Adjuster Dated Text
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimInvolvementHistoryAllClaims"
                                                ID="chkClaimInvolvementHistoryAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="19" tab="allclaims" />Claim Involvement History
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/EnteredUserFullNameAllClaims"
                                                ID="chkEnteredUserFullNameAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2"
                                                index="10" tab="allclaims" />Entered User Full Name
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" appearance="full" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/DiaryDetailsAllClaims"
                                                ID="chkDiaryDetailsAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="20" tab="allclaims" />Diary Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/SupplementalFieldsAllClaims"
                                                ID="chkSupplementFieldsAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="21" tab="allclaims" />Supplemental Fields
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/CommentsAllClaims"
                                                ID="chkCommentsAllClaimsGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="22" tab="allclaims" />Comments
                                        </td>
                                    </tr>
                                    <!--25743 end-->
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantInformation"
                                                ID="chkClaimantInformationGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="gcclaims" />Claimant Information
                                        </td>
                                         <%-- skhare7 R8 Enhancements--%>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/UnitLoss"
                                                ID="chkSubGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="8" tab="gcclaims" />Unit Loss
                                        </td>
                                        <%-- skhare7 R8 Enhancements--%>                                       
                                         
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/AttorneyInformation"
                                                ID="chkAttorneyInformationGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="gcclaims" />Attorney Information
                                        </td>
                                         <%-- skhare7 R8 Enhancements--%>
                                         <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/UnitSalvage"
                                                ID="chkProUnitSalvageGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="9" tab="gcclaims" />Salvage
                                        </td>
                                         <%-- skhare7 R8 Enhancements--%>                                        
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimInvolvementHistory"
                                                ID="chkClaimInvolvementHistoryGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="3" tab="gcclaims" />Claim Involvement History
                                        </td>
                                        <td width="40%" valign="top"></td>                                       
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantLevelReserves"
                                                ID="chkClaimantLevelReservesGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="4" tab="gcclaims" />Claimant Level Reserves
                                        </td>
                                          <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/PropertyLoss"
                                                ID="chkPropLossGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="10" tab="gcclaims" />Property Loss
                                        </td>                                      
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantPaymentHistory"
                                                ID="chkClaimantPaymentHistoryGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="5" tab="gcclaims" />Claimant Payment History
                                        </td>
                                         <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/PropertySalvage"
                                                ID="chkPropertySalvageGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="10" tab="gcclaims" />Salvage
                                        </td>                                       
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantFundsManagement"
                                                ID="chkClaimantFundsManagementGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="6" tab="gcclaims" />Claimant Funds Management
                                        </td>
                                        <td width="40%" valign="top"></td>                                        
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/TransactionDetail"
                                                ID="chkTransactionDetailGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="7" tab="gcclaims" />Transaction Detail
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr>  
									<%-- added by swati for WWIG MITS # 35363--%>
                                     <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/DemandOffer"
                                                ID="chkDemandOfferGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="6" tab="gcclaims" />Demand Offer
                                        </td>
                                        <td width="40%" valign="top"></td>                                        
                                    </tr>
                                   <%-- change end here by swati--%>                             
                                     <%-- Yukti , MITS 35772--%> 
                                     <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                            runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantUnitInfo"  
                                            ID="chkClaimantUnitInfoGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="9" tab="gcclaims" />Claimant Unit Information
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr>        
                                     <%-- Yukti , MITS 35772--%> 
									 <%-- added by gbindra for WWIG GAP 15 MITS # 34104--%>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/EnhancedNotesAllClaimant"
                                                ID="chkEnhancedNotesAllClaimantGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="7" tab="gcclaims" />Enhanced Notes
                                        </td>
                                        <td width="40%" valign="top"></td>                                        
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ActivityDateAllClaimant"
                                                ID="chkActivityDateAllClaimantGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="8" tab="gcclaims" />Activity Date
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/DateCreatedAllClaimant"
                                                ID="chkDateCreatedAllClaimantGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="9" tab="gcclaims" />Date Created
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/EnteredByAllClaimant"
                                                ID="chkEnteredByAllClaimantGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="10" tab="gcclaims" />Entered by
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/NoteTypeAllClaimant"
                                                ID="chkNoteTypeAllClaimantGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="gcclaims" />Note Type
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/UserTypeAllClaimant"
                                                ID="chkUserTypeAllClaimantGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="12" tab="gcclaims" />User Type
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/NoteTextAllClaimant"
                                                ID="chkNoteTextAllClaimantGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="13" tab="gcclaims" />Note Text
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/SubjectAllClaimant"
                                                ID="chkSubjectAllClaimantGCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="14" tab="gcclaims" />Subject
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                   <%-- added by gbindra for WWIG GAP 15 MITS # 34104 END--%>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" name="FORMTABvaclaims"
                                    id="FORMTABvaclaims" style="display: none;">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimantInformation"
                                                ID="chkClaimantInformationVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="vaclaims" />Claimant Information
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/VehicleAccident"
                                                ID="chkVehicleAccidentVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="9" tab="vaclaims" />Vehicle Accident
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/AttorneyInformation"
                                                ID="chkAttorneyInformationVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="vaclaims" />Attorney Information
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/UnitInformation"
                                                ID="chkUnitInformationVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="11" tab="vaclaims" />Unit Information
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimInvolvementHistory"
                                                ID="chkClaimInvolvementHistoryVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="3" tab="vaclaims" />Claim Involvement History
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimInvolvementHistoryUnit"
                                                ID="chkClaimInvolvementHistoryUnitVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="12" tab="vaclaims" />Claim Involvement History
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimantLevelReserves"
                                                ID="chkClaimantLevelReservesVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="4" tab="vaclaims" />Claimant Level Reserves
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/UnitLevelReserves"
                                                ID="chkUnitLevelReservesVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="13" tab="vaclaims" />Unit Level Reserves
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimantPaymentHistory"
                                                ID="chkClaimantPaymentHistoryVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="5" tab="vaclaims" />Claimant Payment History
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/PaymentHistoryForUnit"
                                                ID="chkPaymentHistoryForUnitVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="14" tab="vaclaims" />Payment History for Unit
                                        </td>
                                       
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimantFundsManagement"
                                                ID="chkClaimantFundsManagementVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="6" tab="vaclaims" />Claimant Funds Management
                                        </td>
                                      <%--  skhare7 R8 Enhancement--%>
                                         <td width="40%" valign="top">
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/VASalvage"
                                                ID="chkVASalvageVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="15" tab="vaclaims" />Unit Salvage
                                        </td>
                                      <%--  end--%>
                                        
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/TransactionDetail"
                                                ID="chkTransactionDetailVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="7" tab="vaclaims" />Transaction Detail
                                        </td>
                                          <%--Changed by Amitosh for mits 24431 (03/23/2011)--%>
                                       <%-- <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/TransactionDetailUnitLevel"
                                                ID="chkTransactionDetailUnitVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="3" index="14" tab="vaclaims" />Transaction Detail
                                        </td>--%>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/UnitLevelFunds"
                                                ID="chkUnitLevelFundsVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="16" tab="vaclaims" />Unit Level Funds
                                        </td>
                                      
                                    </tr>
                                    <tr>
                                    <%--<td></td>--%>
                                        <%-- Yukti , MITS 35772--%> 
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                            runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimantUnitInfo"  
                                            ID="chkClaimantUnitInfoVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="9" tab="vaclaims" />Claimant Unit Information
                                        </td>
                                        <td width="40%" valign="top"></td>                                                
                                     <%-- Yukti , MITS 35772--%>        
                                      <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/TransactionDetailUnitLevel"
                                                ID="chkTransactionDetailUnitLevelVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="3" index="17" tab="vaclaims" />Transaction Detail
                                        </td>
                                    </tr>
									
									<%-- added by swati for WWIG MITS # 35363--%>
                                     <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/DemandOffer"
                                                ID="chkDemandOfferVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="4" index="15" tab="vaclaims" />Demand Offer
                                        </td>
                                        <td width="40%" valign="top"></td>                                        
                                    </tr>
                                   <%-- change end here by swati--%>

                                   <%-- added by gbindra for WWIG GAP 15 MITS # 34104--%>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/EnhancedNotesAllClaimant"
                                                ID="chkEnhancedNotesAllClaimantVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="16" tab="vaclaims" />Enhanced Notes
                                        </td>
                                        <td width="40%" valign="top"></td>                                        
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ActivityDateAllClaimant"
                                                ID="chkActivityDateAllClaimantVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="17" tab="vaclaims" />Activity Date
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/DateCreatedAllClaimant"
                                                ID="chkDateCreatedAllClaimantVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="18" tab="vaclaims" />Date Created
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/EnteredByAllClaimant"
                                                ID="chkEnteredByAllClaimantVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="19" tab="vaclaims" />Entered by
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/NoteTypeAllClaimant"
                                                ID="chkNoteTypeAllClaimantVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="20" tab="vaclaims" />Note Type
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/UserTypeAllClaimant"
                                                ID="chkUserTypeAllClaimantVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="21" tab="vaclaims" />User Type
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/NoteTextAllClaimant"
                                                ID="chkNoteTextAllClaimantVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="22" tab="vaclaims" />Note Text
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/SubjectAllClaimant"
                                                ID="chkSubjectAllClaimantVAClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="23" tab="vaclaims" />Subject
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                   <%-- added by gbindra for WWIG GAP 15 MITS # 34104 END--%>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" name="FORMTABwcclaims"
                                    id="FORMTABwcclaims" style="display: none;">

                                     <!--25743 Starts WCClaims; -->
                                    
                                    <!--AllClaims-->
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/ClaimInformationAllClaims"
                                                ID="chkClaimInformationAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="1" tab="allclaims" />Claim Information
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/LitigationInformationAllClaims"
                                                ID="chkLitigationInformationAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="11" tab="allclaims" />Litigation Information
                                        </td>
                                     
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/EnhancedNotesAllClaims"
                                                ID="chkEnhancedNotesAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="23" tab="allclaims" />Enhanced Notes
                                        </td>
                                    
                                    </tr>
                                    <tr>
                                        
                                        <td width="30%" valign="top">
                                       
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/ClaimLevelReservesAllClaims"
                                                ID="chkClaimLevelReservesAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="2" tab="allclaims" />Claim Level Reserves
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/AttorneyInformationAllClaims"
                                                ID="chkAttorneyInformationAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="12" tab="allclaims" />Attorney Information
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/ActivityDateAllClaims"
                                                ID="chkActivityDateAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="24" tab="allclaims" />Actvity Date
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/ArbitrationAllClaims"
                                                ID="chkArbitrationAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="28" tab="allclaims" />Arbitration
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/PaymentHistoryAllClaims"
                                                ID="chkPaymentHistoryAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="3" tab="allclaims" />Payment History
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/ExpertWitnessAllClaims"
                                                ID="chkExpertWitnessAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="13" tab="allclaims" />Expert Witness
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/DateCreatedAllClaims"
                                                ID="chkDateCreatedAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="25" tab="allclaims" />Date Created
                                        </td>
                                        
                                        <td width="30%" nowrap=true valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/LiabilityAllClaims"
                                                ID="chkLiabilityAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="29" tab="allclaims" />Liability Loss
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/FundsManagementAllClaims"
                                                ID="chkFundsManagementAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="4" tab="allclaims" />Funds Management
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/LitDemandOfferAllClaims"
                                                ID="chkLitDemandOfferAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="14" tab="allclaims" />Demand Offer
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/EnteredByAllClaims"
                                                ID="chkEnteredByAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="26" tab="allclaims" />Entered By
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/TransactionDetailAllClaims"
                                                ID="chkTransactionDetailAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="5" tab="allclaims" />Transaction Detail
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/SubrogationInformationAllClaims"
                                                ID="chkSubrogationInformationAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="15" tab="allclaims" />Subrogation Information
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/NoteTypeAllClaims"
                                                ID="chkNoteTypeAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="27" tab="allclaims" />Note Type
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" name="$node^114" value="True" appearance="full" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/EventDetailAllClaims"
                                                ID="chkEventDetailAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="6" tab="allclaims" />Event Detail
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/SubroDemandOfferAllClaims"
                                                ID="chkSubroDemandOfferAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="16" tab="allclaims" />Demand Offer
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/UserTypeAllClaims"
                                                ID="chkUserTypeAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="28" tab="allclaims" />User Type
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/AdjusterInformationAllClaims"
                                                ID="chkAdjusterInformationAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="8" tab="allclaims" />Adjuster Information
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/DefendantInformationAllClaims"
                                                ID="chkDefendantInformationAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="17" tab="allclaims" />Defendant Information
                                        </td>
                                        
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/NoteTextAllClaims"
                                                ID="chkNoteTextAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="29" tab="allclaims" />Note Text
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/PersonsInvolvedAllClaims"
                                                ID="chkPersonsInvolvedAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="7" tab="allclaims" />Persons Involved
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/DefAttorneyInformationAllClaims"
                                                ID="chkDefAttorneyInformationAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="18" tab="allclaims" />Attorney Information
                                        </td>
                                        <%-- Zmohamamd MITS 31048--%>
                                        <td  valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/SubjectAllClaims"
                                                ID="chkSubjectAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="35" tab="allclaims" />Subject
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/AdjusterDatedTextAllClaims"
                                                ID="chkAdjusterDatedTextAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="9" tab="allclaims" />Adjuster Dated Text
                                        </td>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/ClaimInvolvementHistoryAllClaims"
                                                ID="chkClaimInvolvementHistoryAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1"
                                                index="19" tab="allclaims" />Claim Involvement History
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/EnteredUserFullNameAllClaims"
                                                ID="chkEnteredUserFullNameAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2"
                                                index="10" tab="allclaims" />Entered User Full Name
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" appearance="full" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/DiaryDetailsAllClaims"
                                                ID="chkDiaryDetailsAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="20" tab="allclaims" />Diary Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/SupplementalFieldsAllClaims"
                                                ID="chkSupplementalFieldsAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="21" tab="allclaims" />Supplemental Fields
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%" valign="top">
                                        </td>
                                        <td width="30%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/CommentsAllClaims"
                                                ID="chkCommentsAllClaimsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0"
                                                index="22" tab="allclaims" />Comments
                                        </td>
                                    </tr>
                                    <!--25743 End-->


                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/EmployeeInformation"
                                                ID="chkEmployeeInformationWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="wcclaims" />Employee Information
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/CaseManagementInformation"
                                                ID="chkCaseManagementInformationWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="10" tab="wcclaims" />Case Management Information
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/EmploymentInformation"
                                                ID="chkEmploymentInformationWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="wcclaims" />Employment Information
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/CaseManagerInformation"
                                                ID="chkCaseManagerInformationWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="11" tab="wcclaims" />Case Manager Information
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/DependentInformation"
                                                ID="chkDependentInformationWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="3" tab="wcclaims" />Dependent Information
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/CaseManagerNotes"
                                                ID="chkCaseManagerNotesWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="12" tab="wcclaims" />Case Manager Notes
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/RestrictedDays"
                                                ID="chkRestrictedDaysWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="4" tab="wcclaims" />Restricted Days
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/RestrictedWork"
                                                ID="chkRestrictedWorkWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="13" tab="wcclaims" />Restricted Work
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/LostDays"
                                                ID="chkLostDaysWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="5" tab="wcclaims" />Lost Days
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/WorkLoss"
                                                ID="chkWorkLossWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="14" tab="wcclaims" />Work Loss
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/EmployeeEventDetail"
                                                ID="chkEmployeeEventDetailWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="6" tab="wcclaims" />Employee Event Detail
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/TreatmentPlan"
                                                ID="chkTreatmentPlanWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="15" tab="wcclaims" />Treatment Plan
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/ClaimInvolvementHistory"
                                                ID="chkClaimInvolvementHistoryWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="7" tab="wcclaims" />Claim Involvement History
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/MedicalManagementSavings"
                                                ID="chkMedicalManagementSavingsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="16" tab="wcclaims" />Medical Management Savings
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/OSHAInformation"
                                                ID="chkOSHAInformationWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="8" tab="wcclaims" />OSHA Information
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/Accommodations"
                                                ID="chkAccommodationsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="17" tab="wcclaims" />Accommodations
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/JurisdictionalSupplementals"
                                                ID="chkJurisdictionalSupplementalsWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="9" tab="wcclaims" />Jurisdictional Supplementals
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/VocationalRehabilitation"
                                                ID="chkVocationalRehabilitationWCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="18" tab="wcclaims" />Vocational Rehabilitation
                                        </td>
                                    </tr>
                                    <!-- BOB Unit Stat enhancements -->
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/UnitStatInformation"
                                                ID="chkUnitStatInformation" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="19" tab="wcclaims" />Unit Stat Information
                                        </td>
                                        <td width="40%" valign="top">
                                            <%--<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/VocationalRehabilitation"
                                                ID="CheckBox2" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="18" tab="wcclaims" />Vocational Rehabilitation--%>
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" name="FORMTABdiclaims"
                                    id="FORMTABdiclaims" style="display: none;">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/EmployeeInformation"
                                                ID="chkEmployeeInformationDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="diclaims" />Employee Information
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/CaseManagementInformation"
                                                ID="chkCaseManagementInformationDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="9" tab="diclaims" />Case Management Information
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/EmploymentInformation"
                                                ID="chkEmploymentInformationDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="diclaims" />Employment Information
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/CaseManagerInformation"
                                                ID="chkCaseManagerInformationDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="10" tab="diclaims" />Case Manager Information
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/DependentInformation"
                                                ID="chkDependentInformationDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="3" tab="diclaims" />Dependent Information
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/CaseManagerNotes"
                                                ID="chkCaseManagerNotesDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="diclaims" />Case Manager Notes
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/RestrictedDays"
                                                ID="chkRestrictedDaysDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="4" tab="diclaims" />Restricted Days
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/RestrictedWork"
                                                ID="chkRestrictedWorkDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="12" tab="diclaims" />Restricted Work
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/LostDays"
                                                ID="chkLostDaysDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="5" tab="diclaims" />Lost Days
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/WorkLoss"
                                                ID="chkWorkLossDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="13" tab="diclaims" />Work Loss
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/EmployeeEventDetail"
                                                ID="chkEmployeeEventDetailDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="6" tab="diclaims" />Employee Event Detail
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/TreatmentPlan"
                                                ID="chkTreatmentPlanDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="14" tab="diclaims" />Treatment Plan
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/LeaveInfo"
                                                ID="chkLeaveInfoDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="7" tab="diclaims" />Leave Info
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/ClaimInvolvementHistory"
                                                ID="chkClaimInvolvementHistoryDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="8" tab="diclaims" />Claim Involvement History
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/MedicalManagementSavings"
                                                ID="chkMedicalManagementSavingsDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="15" tab="diclaims" />Medical Management Savings
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/PlanInfo"
                                                ID="chkPlanInfoDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="9" tab="diclaims" />Plan Info
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/Accommodations"
                                                ID="chkAccommodationsDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="16" tab="diclaims" />Accommodations
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/VocationalRehabilitation"
                                                ID="chkVocationalRehabilitationDIClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="10" tab="diclaims" />Vocational Rehabilitation
                                        </td>
                                    </tr>
                                </table>
                                
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" name="FORMTABpolicy"
                                    id="FORMTABpolicy" style="display: none;">
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/PolicyInformation"
                                                ID="chkPolicyInformationPolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="policy" />Policy Information
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/InsuredInformation"
                                                ID="chkInsuredInformationPolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="9" tab="policy" />Insured Information
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/PolicyNumber"
                                                ID="chkPolicyNumberPolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Policy Number
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/Insured"
                                                ID="chkInsuredPolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="10" tab="policy" />Insured
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/EffectiveDate"
                                                ID="chkEffectiveDatePolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="3" tab="policy" />Effective Date
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/Address"
                                                ID="chkAddressPolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="policy" />Address
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/ExpirationDate"
                                                ID="chkExpirationDatePolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="4" tab="policy" />Expiration Date
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/DepartmentAbbreviation"
                                                ID="chkDepartmentAbbreviationPolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="12" tab="policy" />Department Abbreviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/ClaimLimit"
                                                ID="chkClaimLimitPolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="5" tab="policy" />Claim Limit
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/InsuranceCompanyName"
                                                ID="chkInsuranceCompanyNamePolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="13" tab="policy" />Insurance Company Name
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/PolicyLimit"
                                                ID="chkPolicyLimitPolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="6" tab="policy" />Policy Limit
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/PercentageofAggregate"
                                                ID="chkPercentageofAggregatePolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="7" tab="policy" />Percentage of Aggregate
                                        </td>
                                    </tr>
                                    <tr>                                <%-- added by rkaur7 for MITS 16668--%>
                                        <%--<td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/PolicyStates"
                                                ID="chkPolicyStatesAllClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="2" tab="allclaims" />Policy States
                                        </td>--%>
                                        <%--Changed by Amitosh for mits 24325 (03/21/2011)--%>
                                        <td width="30%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Policy/PolicyStates"
                                                ID="chkPolicyStatesPolicy" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="2" tab="allclaims" />Policy States
                                        </td>
                                    </tr>
                                        

                                </table> 
                                                              
                               <%-- Sumit - MITS# 18145 -  10/15/2009 Fields to be added for PC Claim--%>
                               <%-- smahajan6 - MITS# 18230 -  12/30/2009 - Start --%>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" ALIGN="center" name="FORMTABpcclaims" id="FORMTABpcclaims" style="display:none;">
                                   <tr>
                                    <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/ClaimantInformation" id="chkClaimantInformationpcclaims"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="1" tab="pcclaims"/>Claimant Information
                                    </td>
                                    <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/PropertyInformation" id="chkPropertyInformationpcclaims"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="8" tab="pcclaims"/>Property Information
                                    </td>
                                   </tr>
                                   <tr>
                                    <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/AttorneyInformation" id="chkAttorneyInformationpcclaims"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="2" tab="pcclaims"/>Attorney Information
                                    </td>
                                    <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/COPEData" id="chkCOPEDatapcclaims"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="10" tab="pcclaims"/>COPE Data
                                    </td>
                                   </tr>
                                   <tr>
                                    <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/ClaimInvolvementHistory" id="chkClaimInvolvementHistorypcclaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="3" tab="pcclaims"/>Claim Involvement History
                                    </td>
                                    <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/OptionalCOPEData" id="chkOptionalCOPEDatapcclaims"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="11" tab="pcclaims"/>Optional COPE Data
                                    </td>            
                                   </tr>
                                   <tr>
                                    <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/ClaimantLevelReserves" id="chkClaimantLevelReservespcclaims"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="4" tab="pcclaims"/>Claimant Level Reserves
                                    </td>
                                    <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/Schedule" id="chkSchedulepcclaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="12" tab="pcclaims"/>Schedule
                                    </td> 
                                   </tr>
                                   <tr>
                                    <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/ClaimantPaymentHistory" id="chkClaimantPaymentHistorypcclaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="5" tab="pcclaims"/>Claimant Payment History
                                    </td>
                                   </tr>
                                   <tr>
                                    <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/ClaimantFundsManagement" id="chkClaimantFundsManagementpcclaims"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="6" tab="pcclaims"/>Claimant Funds Management
                                    </td>                    
                                   </tr>
                                   <tr>
                                    <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/TransactionDetail" id="chkTransactionDetailpcclaims"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="7" tab="pcclaims"/>Transaction Detail
                                    </td>
                                   </tr>
								   <%-- added by swati for WWIG MITS # 35363--%>
                                     <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/DemandOffer"
                                                ID="chkDemandOfferPCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="3" index="8" tab="pcclaims" />Demand Offer
                                        </td>
                                        <td width="40%" valign="top"></td>                                        
                                    </tr>
                                   <%-- change end here by swati--%>   
                                    <%-- Yukti , MITS 35772--%> 
                                     <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                            runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/ClaimantUnitInfo"  
                                            ID="chkClaimantUnitInfoPCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="9" tab="pcclaims" />Claimant Unit Information
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr>        
                                     <%-- Yukti , MITS 35772--%>  
									 <%-- added by gbindra for WWIG GAP 15 MITS # 34104--%>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/EnhancedNotesAllClaimant"
                                                ID="chkEnhancedNotesAllClaimantPCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="8" tab="pcclaims" />Enhanced Notes
                                        </td>
                                        <td width="40%" valign="top"></td>                                        
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/ActivityDateAllClaimant"
                                                ID="chkActivityDateAllClaimantPCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="9" tab="pcclaims" />Activity Date
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/DateCreatedAllClaimant"
                                                ID="chkDateCreatedAllClaimantPCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="10" tab="pcclaims" />Date Created
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/EnteredByAllClaimant"
                                                ID="chkEnteredByAllClaimantPCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="pcclaims" />Entered by
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/NoteTypeAllClaimant"
                                                ID="chkNoteTypeAllClaimantPCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="12" tab="pcclaims" />Note Type
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/UserTypeAllClaimant"
                                                ID="chkUserTypeAllClaimantPCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="13" tab="pcclaims" />User Type
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/NoteTextAllClaimant"
                                                ID="chkNoteTextAllClaimantPCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="14" tab="pcclaims" />Note Text
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PCClaims/SubjectAllClaimant"
                                                ID="chkSubjectAllClaimantPCClaims" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="15" tab="pcclaims" />Subject
                                        </td>
                                        <td width="40%" valign="top"></td>                                          
                                    </tr> 
                                   <%-- added by gbindra for WWIG GAP 15 MITS # 34104 END--%>
                                </table>
                                <%-- smahajan6 - MITS# 18230 -  12/30/2009 - End --%>
                               <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" name="FORMTABpolicymgmt"
                                    id="FORMTABpolicymgmt" style="display: none;">  
                                    
                                    <tr>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyInfo"
                                                ID="chkPolicyInfoPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="policy" />Policy Information
                                        </td>                                                                            



                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyCvgs"
                                                ID="chkPolicyCvgsPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="policy" />Policy Coverages
                                        </td>                                                                            
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyName"
                                                ID="chkPolicyNamePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Policy Name
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyCvgType"
                                                ID="chkPolicyCvgTypePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="1" tab="policy" />Coverage Type                                                
	                                    </td>                                                                            
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyNumber"
                                                    ID="chkPolicyNumberPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Policy Number
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyCvgDesc"
                                                ID="chkPolicyCvgDescPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="1" tab="policy" />Coverage Description                                                
                                       </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyStatus"
                                                ID="chkPolicyStatusPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Policy Status
                                        </td>
                                         <td width="40%" valign="top">
                                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyLimit"
                                                ID="chkPolicyLimitPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="1" tab="policy" />Policy Limit                                               
                                       </td>
                                        
                                    </tr>                                   
    
    
                                    <tr>
                                        <td width="40%" valign="top">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyType"
                                                ID="chkPolicyTypePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Policy Type
                                        </td>
                                        <td width="40%" valign="top">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/ClaimLimit"
                                                ID="chkClaimLimitPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="policy" />Claim Limit                                               
                                       </td>
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyState"
                                                ID="chkPolicyStatePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Policy State
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/OccurrenceLimit"
                                                ID="chkOccurrenceLimitPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="policy" />Occurrence Limit                                               
                                       </td>                                        
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/EffecDate"
                                                ID="chkEffecDatePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Effective Date
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/TotalPayment"
                                                ID="chkTotalPaymentPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="1" tab="policy" />Total Payment                                               
                                        </td>                                                                          
                                        
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/ExpiDate"
                                                ID="chkExpiDatePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Expiration Date
                                        </td>
                                        <td width="40%" valign="top">
                                            <asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyExposure"
                                                ID="chkPolicyExposurePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="0" index="1" tab="policy" />Policy Exposure                                               
                                        </td>
             
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/TotalBilledPremium"
                                                ID="chkTotalBilledPremiumPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Total Billed Premium
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/ExposureType"
                                                ID="chkExposureTypePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="1" tab="policy" />Exposure/UAR Type                                               
                                        </td>                                                                          
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyInsurer"
                                                ID="chkPolicyInsurerPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Policy Insurer
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/ExposureDesc"
                                                ID="chkExposureDescPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="1" tab="policy" />Exposure/UAR Description                                               
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td width="40%" valign="top">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyInsured"
                                                ID="chkPolicyInsuredPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="2" tab="policy" />Policy Insured
                                        </td>
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/ExposureAmount"
                                                ID="chkExposureAmountPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="1" tab="policy" />Amount                                               
                                        </td> 
                                    </tr>                                    
                                    <tr>
                                    <td width="40%" valign="top">
                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyInsuredName"
                                                ID="chkPolicyInsuredNamePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="policy" />Name
                                        </td> 
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/Rate"
                                                ID="chkRatePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="1" tab="policy" />Rate
                                        </td>    
                                    </tr>                                    
                                    <tr>                                    
                                     <td width="40%" valign="top">
                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                                                runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/PolicyInsuredAddress"
                                                ID="chkPolicyInsuredAddressPolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="2" index="11" tab="policy" />Address
                                        </td>    
                                        <td width="40%" valign="top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/PolicyMgmt/BaseRate"
                                                ID="chkBaseRatePolicyMgmt" onchange="ApplyBool(this);" onclientclick="CheckboxClick()"
                                                depth="1" index="1" tab="policy" />Base Rate
                                        </td>    
                                    </tr>                                    
                                    </table>                                                                 
                                    
                            
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlAdmin" runat="server">
                    <table border="0" cellspacing="0" cellpadding="0" width="30%" valign="top">
                         <tr>
                         <td width="5%" nowrap="true">Available Users:</td>
                         <td ></td>
                         <td width="*" nowrap="true" >Users To Be Configured:</td>
                         </tr>
                        <tr>
                        <%--igupta3 Mits : 31732 Changes starts--%>
                                <td width="5%" nowrap="true" valign="top">
                   <asp:ListBox   type="combobox" 
                          rmxref="/Instance/ExecutiveSummaryConfig/Users/User/Name[@name='cboAllUsersGroups']" 
                          rmxignoreset="true" runat="server" 
                          ID="cboAllUsersGroups"  TabIndex = "1"
                          size="10" Height="150px" style="margin-top: 0px" Width="300px" 
                          SelectionMode="Multiple"></asp:ListBox>
                           <asp:HiddenField  ID ="hndPagetype" value="N"  runat="server" />  
                  <asp:TextBox runat ="server" rmxref="/Instance/ExecutiveSummaryConfig/Users/User/Name[@name='txtRelatedComponents']"  ID="txtRelatedComponents" style="display:none"></asp:TextBox>                                                                    
                            </td>
                                                        
                            <td valign="top" align="center" class="style1">
                                
                   <asp:Button runat="server" ID="btnAddAll" Text="&gt;&gt;" class="button" TabIndex = "2"
                            style="width:95" OnClientClick="return AddFilter('all');" Height="26px" 
                          Width="50px" UseSubmitBehavior="False" /><br/><br/>
                   <asp:Button runat="server" ID="btnAddSelected" Text="&gt;" class="button" TabIndex = "2"
                          style="width:95" OnClientClick="return AddFilter('selected');" Height="26px" 
                          Width="50px" UseSubmitBehavior="False" /><br/><br/>
                  <asp:Button runat="server" ID="btnRemoveSelected" Text="&lt;" class="button" TabIndex = "4"
                          style="width:95; margin-left: 0px;" 
                          OnClientClick="return RemoveFilter('selected');" Height="26px" 
                          Width="50px" UseSubmitBehavior="False" /><br/><br/>
                      <asp:Button runat="server" ID="btnRemoveAll" Text="&lt;&lt;" class="button" TabIndex = "4"
                          style="width:95; margin-left: 0px;" 
                          OnClientClick="return RemoveFilter('all');" Height="26px" 
                          Width="50px" UseSubmitBehavior="False"/><br/><br/>
                 
                  </td>                 
                            <td width="5%" nowrap="true" valign="top">
                                <asp:ListBox 
                          type="combobox" rmxref="/Instance/ExecutiveSummaryConfig/Users/User/Name[@name='lstUsersGroups']"  
                          runat="server"   TabIndex = "6"
                           ID="lstUsersGroups" rmxignoreset="true"  
                          size="10" Height="150px" style="margin-top: 0px" Width="300px" SelectionMode="Multiple"  EnableViewState="true"></asp:ListBox>
                            </td>                          
                             
                            <%--<td valign="top">
                                 <asp:DropDownList runat="server" ID="cboAllUsersGroups" rmxtext="/Instance/ExecutiveSummaryConfig/Users/User/Name"
                                    rmxvalue="/Instance/ExecutiveSummaryConfig/Users/User/UserID" />
                                <asp:Button UseSubmitBehavior="false" runat="server" Text="Add" ID="btnAddUser" class="button"
                                    OnClientClick="if(!AddUserGroup()) return false;" OnClick="AddUserGroup" />
                            </td>
                            <td>
                                <asp:ListBox runat="server" ID="lstUsersGroups" rmxretainvalue="true" />
                                <asp:ImageButton ID="ImageButton1" runat="server" UseSubmitBehavior="false" src="../../../Images/delete3.gif"
                                    Width="20" Height="20" border="0" alt="Delete" title="Remove" OnClientClick="if(!DeleteUserGroup()) return false;"
                                    OnClick="DeleteUserGroup" />
                            </td>--%>
                            <%--igupta3 Mits : 31732 Changes ends--%>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0" width="80%" valign="top">
                    <tr>
                        <td>
                            <div class="small">
                                Use this page to select the information you want to see in Executive Summary Reports.
                                <br />
                                Once you have made all of your selections, click the "Save Configuration" button
                                to store the settings for use.
                            </div>
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" cellpadding="0" width="30%" valign="top">
                    <tr>
                        <td>
                            <asp:Button runat="server" UseSubmitBehavior="false" Text="Save Configuration" ID="btnSaveConfiguration"
                                class="button" OnClientClick="if(! Save()) return false; " OnClick="Save" />
                        </td>
                        <td>
                            <asp:Button runat="server" Text="Select All" ID="btnSelectAll" class="button" OnClientClick="return SelectAll();; " />
                        </td>
                        <td>
                            <asp:Button runat="server" Text="Unselect All" ID="btnUnselectAll" class="button"
                                OnClientClick="return UnselectAll();; " />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="hiddenSelectedUser" rmxretainvalue="true"
        rmxref="/Instance/Document/ExecutiveSummaryConfig/SelectedUser" Text="" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="IsAdminUser" rmxretainvalue="true"
        rmxref="/Instance/Document/ExecutiveSummaryConfig/IsAdminUser" Text="" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="hTabName" rmxref="/Instance/Document/ExecutiveSummaryConfig/SelectedTab"
        Text="" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="functiontocall" rmxref="/Instance/Document/ExecutiveSummaryConfig/FunctionToCall"
        Text="" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="UserId" rmxref="/Instance/Document/ExecutiveSummaryConfig/UserId"
        Text="" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="UserIDs" rmxref="/Instance/Document/ExecutiveSummaryConfig/UserIDs"
        Text="" rmxtype="hidden" />   
    </form>
</body>
</html>
