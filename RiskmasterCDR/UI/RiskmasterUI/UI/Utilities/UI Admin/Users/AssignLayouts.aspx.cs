﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class AssignLayouts : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    NonFDMCWSPageLoad("AssignLayoutsAdaptor.Get");
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }
        protected void Save(object sender, EventArgs e)
        {
            bool bReturnStatus = false;

            try
            {
                UpdateListItems();
                bReturnStatus = CallCWSFunction("AssignLayoutsAdaptor.Save");
                if (bReturnStatus)
                    bReturnStatus = CallCWSFunction("AssignLayoutsAdaptor.Get");
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
      }

        protected void UpdateListItems()
        {
            lstTabs.Items.Clear();
            lstTopDowns.Items.Clear();
            txtTopDown.Text = "";
            txtlstTabs.Text = "";
			
			//Bijender MITS #14652,03-12-2009 : Added rmxignoreget attribute to the listbox controls.
			lstTabs.Attributes.Add("rmxignoreget", "true");
            lstTopDowns.Attributes.Add("rmxignoreget", "true");
			//End 14652

            char[] splitter = { ';' };
            string svalues = string.Empty;
            string[] lsthndValues = hndlstTabs.Value.Split(splitter);
            if (lsthndValues.Length > 1)
            {
                char[] splitterEqual = { '=' };
                for (int Icount = 0; Icount < lsthndValues.Length - 1; Icount++)
                {
                    string[] svalue = lsthndValues[Icount].Split(splitterEqual);
                    if (svalue.Length > 1)
                    {
                        ListItem listitem = new ListItem(svalue[1], svalue[0]);
                        try
                        {
                            if (!lstTabs.Items.Contains(listitem))
                            {
                                lstTabs.Items.Add(listitem);
                                svalues += svalue[0] + " ";
                                
                            }

                            if (lstTopDowns.Items.Contains(listitem))
                            {
                                lstTopDowns.Items.Remove(listitem);
                                
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                        //lstTabs.Items[Icount].Selected = true;
                    }
                }
                if (svalues.Length > 0)
                {
                    txtlstTabs.Text = svalues.Substring(0, svalues.Length - 1);
                }
                svalues = "";
                hndlstTabs.Value = "";
            }


            lsthndValues = hndTopDown.Value.Split(splitter);
            if (lsthndValues.Length > 1)
            {
                char[] splitterEqual = { '=' };
                for (int Icount = 0; Icount < lsthndValues.Length - 1; Icount++)
                {
                    string[] svalue = lsthndValues[Icount].Split(splitterEqual);
                    if (svalue.Length > 1)
                    {
                        ListItem listitem = new ListItem(svalue[1], svalue[0]);
                        try
                        {
                            if (lstTabs.Items.Contains(listitem))
                            {
                                lstTabs.Items.Remove(listitem);
                               
                            }
                            if (!lstTopDowns.Items.Contains(listitem))
                            {
                                lstTopDowns.Items.Add(listitem);
                                svalues += svalue[0] + " ";
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                      //  lstTopDowns.Items[Icount].Selected = true;
                    }
                }
                if (svalues.Length > 0)
                {
                    txtTopDown.Text = svalues.Substring(0, svalues.Length - 1);
                }
                svalues = "";
                hndTopDown.Value = "";
            }
            


        }
        
    }
}
