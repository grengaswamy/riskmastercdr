<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportAccessMgmt.aspx.cs" Inherits="Riskmaster.UI.Utilities.ReportAccessMgmt" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat ="server">  
    
    <title>Report Access Permissions</title>    
    <script type ="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js">{var i;} </script>
    
    <script type="text/javascript">
				
				var ns, ie, ieversion;
				var browserName = navigator.appName;                   // detect browser 
				var browserVersion = navigator.appVersion;
				if (browserName == "Netscape")
				{
					ie=0;
					ns=1;
				}
				else		//Assume IE
				{
				ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
				ie=1;
				ns=0;
				}
				
				function DeleteAccess()
				{
				     var selected="";
					 if (SelectionValid())
					 {
						for(var i = 0; i<document.forms[0].elements.length;i++) 
						{
						if(document.forms[0].elements[i].type=='checkbox') 
							if(document.forms[0].elements[i].checked == true)
							{
								if (selected=="")
								{
									selected=document.forms[0].elements[i].value;
								}
								else
								{
								selected=selected+" "+document.forms[0].elements[i].value;
								}
							}
						}
						document.forms[0].hdnSelected.value=selected;
						
						 pleaseWait.Show();
                         return true;
					 }
					 else
					 {
					   return false;
					 }
				}
				function SelectionValid()
				{
					for(var i = 0; i<document.forms[0].elements.length;i++) 
						if(document.forms[0].elements[i].type=='checkbox') 
							if(document.forms[0].elements[i].checked == true)
								return true;

					return false;
				}
				
				function ShowPleaseWait()
				{
				     pleaseWait.Show();
                     return true;
				}
				function onPageLoaded()
				{
					if (ie)
					{
						if ((eval("document.all.divForms")!=null) && (ieversion>=6))
						{
							eval("document.all.divForms").style.height=410;
							//alert(window.frames.frameElement);
							//eval("document.all.divForms").style.height=window.frames.Height*0.70;
						}
					}
					else
					{
						var o_divforms;
						o_divforms=document.getElementById("divForms");
						if (o_divforms!=null)
						{
							o_divforms.style.height=window.frames.innerHeight*0.70;
							o_divforms.style.width=window.frames.innerWidth*0.995;
						}
					}
				}
				</script><script src="../../../../../Scripts/zapatec.js" type="text/javascript"></script>
				<script src="../../../../../Scripts/window.js" type="text/javascript"></script>
				<script src="../../../../../Scripts/dialog.js" type="text/javascript"></script>				
				<script type="text/javascript" src="../../../../../Scripts/zpgrid.js"></script>
				<script type="text/javascript" src="../../../../../Scripts/zpgrid-xml.js"></script>
				<script type="text/javascript" src="../../../../../Scripts/zpgrid-editable.js"></script>
				<script type="text/javascript" src="../../../../../Scripts/zpgrid-query.js"></script>
				
</head>

<body onload ="javascript:onPageLoaded();parent.MDIScreenLoaded();">
    <form id="frmData" width="90%" runat="server" >
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    
        
    <table width="98%" border="0" cellspacing="0">
        <tr>
            <td class="msgheader" colspan="4">
                <asp:Label ID="lblCurrentReprtPerm" runat="server" Text="<%$ Resources:lblCurrentReprtPerm %>"></asp:Label></td>
        </tr>
        <tr>
            <td><input type="hidden" value="" id="hdnSelected" />
            <input type="hidden" value="All Users" id="hdnFilterText" /></td>
        </tr>
   </table>
   
   <div id="divForms" style ="width:98%" class="divScroll">
        <table border="0" cellspacing="0" cellpadding="0">             
             <tr>
                <asp:GridView Width="100%" ID="grdReport" runat ="server" HeaderStyle-CssClass ="colheader5 " HeaderStyle-HorizontalAlign="Left" AutoGenerateColumns ="false" GridLines ="None" 
                 AlternatingRowStyle-CssClass="rowdark" >               
                    <Columns >
                        <asp:TemplateField >
                            <ItemTemplate >
                                <asp:CheckBox ID="chkReport"  runat ="server" />
                                <asp:TextBox ID ="txtReport" Text ='<%# Eval("value") %>' style="display:none" runat ="server" ></asp:TextBox>
                            </ItemTemplate>                        
                        </asp:TemplateField>                   
                    </Columns>  
                    
                    <Columns >                         
                        <asp:BoundField HeaderText ="<%$ Resources:gvHdrUser %>"  DataField ="users"/>
                        <asp:BoundField HeaderText ="<%$ Resources:gvHdrReportName %>"  DataField ="ReportName" />
                        <asp:BoundField HeaderText ="<%$ Resources:gvHdrReportDescription %>" DataField ="ReportDescription" />                    
                    </Columns>                                           
                </asp:GridView>
             </tr>
        </table>
   </div>
   
   <table border="0">
        <tr>
             <td><asp:Button ID ="btnRemoveAccess" runat ="server" type="button" 
                     Text="<%$ Resources:btnRemoveAccess %>" class="button" OnClientClick="return DeleteAccess();" 
                     onclick="btnRemoveAccess_Click" /></td>
             <td><asp:Button ID="btnSelectAll" runat ="server" type="button" class="button" Text="<%$ Resources:btnSelectAll %>" OnClientClick="for(var i = 0; i <document.forms[0].elements.length;i++) if(document.forms[0].elements[i].type=='checkbox') document.forms[0].elements[i].checked = true;" /></td>
             <td><asp:Button ID ="btnUnSelect" runat ="server" type="button" class="button" Text="<%$ Resources:btnUnSelect %>" OnClientClick="for(var i = 0; i<document.forms[0].elements.length;i++) if(document.forms[0].elements[i].type=='checkbox') document.forms[0].elements[i].checked = false;" /></td>
             
             <td><asp:Label ID="lblFilter" runat="server" Text="<%$ Resources:lblFilter %>"></asp:Label></td>
             <td>
                 <asp:DropDownList runat="server" rmxref="/Instance/Document/ReportsAccess/User" 
                     ItemSetRef="/Instance/Document/ReportsAccess/Filters" ID ="filter" AutoPostBack ="true" 
                     onselectedindexchanged="filter_SelectedIndexChanged" >
                 </asp:DropDownList>    
             </td>
        </tr>
   </table>    
    <asp:TextBox runat ="server" rmxref="/Instance/Document/ReportsAccess/Reports" rmxignoreget="true" ID ="txthndUsers" style="display:none"></asp:TextBox>
    
 </form>
    <asp:Label ID="lblNote1" runat="server" Text="<%$ Resources:lblNote1 %>"></asp:Label> 
  				<br />
                 <asp:Label ID="lblNote2" runat="server" Text="<%$ Resources:lblNote2 %>"></asp:Label> 
  				<br /><br />
  				<asp:Label ID="lblNote3" runat="server" Text="<%$ Resources:lblNote3 %>"></asp:Label>
  				<br />
                <asp:Label ID="lblNote4" runat="server" Text="<%$ Resources:lblNote4 %>"></asp:Label>
  				
                <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
</body>
</html>
