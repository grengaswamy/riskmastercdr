<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignLayouts.aspx.cs" EnableEventValidation ="false" ValidateRequest="false"  Inherits="Riskmaster.UI.Utilities.Manager.AssignLayouts" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
 <head runat="server">
      <title>Assign Layouts</title>
      <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
      <script type ="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js">{var i;} </script>
      
      <script type="text/javascript">
				function AddFilter(mode)
					{
						var optionRank;
						var optionObject;
												
						selectObject=document.getElementById('lstTabs');
												
						if (mode=="selected")
						{
							//Add selected Available Values
							select=document.getElementById('lstTopDowns');
							for(var x = 0; x < select.options.length ; x++)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
																		
									//remove from available list	
									removeOption('lstTopDowns',select.options[x].value);							
								}
							}							
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('lstTopDowns');
							if (select.options.length > 0)
							{
								for(var x = 0; x < select.options.length ; x++)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;									
								}
								select.options.length=0;
							}							
						}
						return false;
					}
					
					function RemoveFilter(mode)
					{
						var optionRank;
						var optionObject;
												
						selectObject=document.getElementById('lstTopDowns');
												
						if (mode=="selected")
						{
							//Add selected Available Values
							select=document.getElementById('lstTabs');
							for(var x = 0; x < select.options.length ; x++)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
																		
									//remove from available list	
									removeOption('lstTabs',select.options[x].value);							
								}
							}							
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('lstTabs');
							if (select.options.length > 0)
							{
								for(var x = 0; x < select.options.length ; x++)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;									
								}
								select.options.length=0;
							}							
						}
						return false;
					}
					
					function removeOption(selectName,id)
					{
						select=document.getElementById(selectName);
						Ids=new Array();
						Names=new Array();

						for(var x = 0; x < select.options.length ; x++)
						{
							if(select.options[x].value!=id)
							{
								Ids[Ids.length] = select.options[x].value;
								Names[Names.length]=select.options[x].text;
							}
						}

						select.options.length = Ids.length;

						for(var x = 0;x < select.options.length; x++)
						{
							select.options[x].text=Names[x];
							select.options[x].value=Ids[x];
						}						
					}				
		
			function SelectAll()
			{
			    hndlstTabs1= document.getElementById('hndlstTabs');
			    hndTopDown= document.getElementById('hndTopDown');
			    var values="";
			    		    
			    for(var f=0;f<document.forms[0].lstTopDowns.options.length;f++)
			    {
				   values += document.forms[0].lstTopDowns.options[f].value + "=" + document.forms[0].lstTopDowns.options[f].text + ";";
			    }
			    hndTopDown.value +=values;		   
			    	
			    values="";
			    for(var f=0;f<document.forms[0].lstTabs.options.length;f++)
			    {
				   values += document.forms[0].lstTabs.options[f].value + "=" + document.forms[0].lstTabs.options[f].text + ";";
			    }
			     hndlstTabs1.value  =values;
			     pleaseWait.Show();
			     return true;
			}	
					
   </script>
 </head> 
 <body onload="parent.MDIScreenLoaded();">
      <form id="frmData" name="frmData" runat="server" method="post" >
           <uc1:ErrorControl ID="ErrorControl1" runat="server" />
           <div>              
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td class="msgheader" colspan="3">Assign Layout to User(s)</td>
                    </tr>
                    
                    <tr>
                        <td colspan="3" nowrap="true"></td>
                    </tr>
                    
                    <tr>
                        <td width="5%" nowrap="true"><b>Top-down Layout</b></td>
                        <td width="5%"></td>
                        <td width="*" nowrap="true"><b>Tab Layout</b></td>
                    </tr>
                    
                    <tr>     
                        <td width="5%" nowrap="true">
                            <asp:ListBox rmxref="/Instance/Document/LayoutUsers/TopDownUsers" rmxignoreset="true" runat="server" ID="lstTopDowns" size="10" Height="159px" TabIndex="1"></asp:ListBox>
                            <asp:HiddenField  ID ="hndTopDown" runat="server" />  
                            <asp:TextBox runat ="server" rmxref="/Instance/Document/LayoutUsers/TopDownUsers" rmxignoreget="true" ID="txtTopDown" style="display :none"></asp:TextBox>
                        </td>
                        
                        <td width="40%" valign="middle" align="center">
                            <asp:Button runat="server" ID="btnAdd" type="submit" Text="Add &gt;"  class="button"  OnClientClick ="return AddFilter('selected');" style="width:80" TabIndex="2" Width ="80" />                               
                            <br /><br />
                            <asp:Button runat="server" ID="btnAddall" type="submit" Text="Add All &gt;" class="button"  OnClientClick="return AddFilter('all');" style="width:80" TabIndex="3" Width ="80" />
                            <br /><br />
                            <asp:Button runat="server" ID="btnRemove" type="submit" Text="< Remove" class="button" OnClientClick="return RemoveFilter('selected');" TabIndex="4" Width ="80"/>
                            <br /><br />
                            <asp:Button runat="server" ID="btnRemoveAll" type="submit" Text="< Remove All" class="button"  OnClientClick="return RemoveFilter('all');" style="width:80"  TabIndex="5" Width ="80"/>
                        </td>
                             
                        <td width="*" nowrap="true">
                            <asp:ListBox rmxref="/Instance/Document/LayoutUsers/TabLayoutUsers" rmxignoreset="true" runat="server" ID="lstTabs" size="10" Height="159px" TabIndex="6"></asp:ListBox>  
                            <asp:HiddenField  ID ="hndlstTabs" runat="server" />  
                            <asp:TextBox runat ="server" ID="txtlstTabs" rmxref="/Instance/Document/LayoutUsers/TabLayoutUsers" rmxignoreget="true" style="display :none"></asp:TextBox>
                        </td>                     
                    </tr>
                    
                    <tr>
                        <td class="group" colspan="3" nowrap="true"></td>
                    </tr>               
                    
                    <tr>
                        <td colspan="3" nowrap="true"></td>
                    </tr>
                        
                    <tr>
                        <td colspan="3" nowrap="true">
                            <asp:Button ID="btnsave" runat="server" type="submit" name="" Text="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" OnClientClick ="return SelectAll();" OnClick="Save" class="button" TabIndex="7"/>
                        </td>
                    </tr>
               </table>                         
               
               <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />             
           </div>           
       </form>
 </body>
</html>