﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using System.Xml;



namespace Riskmaster.UI.Utilities
{
    public partial class ReportAccessMgmt : NonFDMBasePageCWS 
    {
        private DataTable tbl = new DataTable("ReportMgmtTable");
        private string sCWSresponse = string.Empty;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bRetVal=false;
            try
            {
                if (!IsPostBack)
                {
                    filter.Attributes.Add("onchange", "ShowPleaseWait();");
                    //Bijender has changed for Mits 16049
                    //bRetVal = GetNonFDMCallCWS("ReportAccessMgmtAdaptor.Get");
                    bRetVal = GetNonFDMCallCWS("ReportAccessMgmtAdaptor.Get",true,true);
                    //End Mits 16049
                }
                
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        bool GetNonFDMCallCWS(string CWSFunctionName, bool bGetControlData, bool bSetControlData)
        { 
            XElement xRetCWSXML = null;
            bool bReturnStatusCWSCall = false;
            //Bijender has changed for Mits 16049
            //bReturnStatusCWSCall = CallCWS(CWSFunctionName, null, out sCWSresponse, true, true);
            bReturnStatusCWSCall = CallCWS(CWSFunctionName, null, out sCWSresponse, bGetControlData, bSetControlData);
            //End Mits 16049
             if (bReturnStatusCWSCall)
             {
                 XmlDocument oFDMPageDom = new XmlDocument();
                 oFDMPageDom.LoadXml(sCWSresponse);
                 if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                 {
                     XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                     XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);

                     xRetCWSXML = oMessageRespElement.XPathSelectElement("/ReportsAccess");

                     if (xRetCWSXML == null)
                     {
                         return true;
                     }

                     if (!tbl.Columns.Contains("users"))
                         tbl.Columns.Add(new DataColumn("users", typeof(string)));
                     if (!tbl.Columns.Contains("ReportName"))
                         tbl.Columns.Add(new DataColumn("ReportName", typeof(string)));
                     if (!tbl.Columns.Contains("ReportDescription"))
                         tbl.Columns.Add(new DataColumn("ReportDescription", typeof(string)));
                     if (!tbl.Columns.Contains("value"))
                         tbl.Columns.Add(new DataColumn("value", typeof(string)));

                     GetReportElements(grdReport, xRetCWSXML, "/ReportsAccess/Reports");
                     return true;
                 }
                
             }
             return false;
        }


        protected void GetReportElements(GridView grdReport, XElement Template, string node)
        {
                tbl.Clear();
                XElement xvalue = Template.XPathSelectElement(node);
                if (xvalue.HasElements)
                {
                    string sChildName = ((XElement)(xvalue.FirstNode)).Name.ToString();
                    string sValue = xvalue.ToString();

                    XElement oLB = XElement.Parse(sValue);

                    if (oLB.HasElements)
                    {
                        foreach (XElement oEle in oLB.Elements())
                        {
                            string sUser = "";
                            string sReportName = "";
                            string sReportDescription = "";
                            string sRepValue = "";

                            if (oEle.Attribute("User") != null)
                                sUser = oEle.Attribute("User").Value;
                            if (oEle.Attribute("ReportName") != null)
                                sReportName = oEle.Attribute("ReportName").Value;
                            if (oEle.Attribute("ReportDesc") != null)
                                sReportDescription = oEle.Attribute("ReportDesc").Value;
                            if (oEle.Attribute("value") != null)
                                sRepValue = oEle.Attribute("value").Value;

                            tbl.Rows.Add(sUser, sReportName, sReportDescription, sRepValue);
                        }

                        BindGridToDataSource(); 
                    }
                }
                else
                {
                    BindGridToDataSource(); 
                }
            }
        private void BindGridToDataSource()
        {
            if (tbl.Rows.Count != 0)
            {
                grdReport.DataSource = tbl;
                grdReport.DataBind();
            }
            else
            {
                tbl.Rows.Add(tbl.NewRow ());
                grdReport.DataSource = tbl;
                grdReport.DataBind();
                grdReport.Rows[0].Visible = false;
            }
        }

        protected void btnRemoveAccess_Click(object sender, EventArgs e)
        {
            bool bRetVal = false;

            try
            {
                GetCheckedCheckBox();
                //Bijender has changed for Mits 16049
                //bRetVal = GetNonFDMCallCWS("ReportAccessMgmtAdaptor.Save");
                bRetVal = GetNonFDMCallCWS("ReportAccessMgmtAdaptor.Save",true,false);
                //Ends Mits 16049
                txthndUsers.Text = string.Empty;
                if (bRetVal)
                {
                    //Bijender has changed for Mits 16049
                    //bRetVal = GetNonFDMCallCWS("ReportAccessMgmtAdaptor.Get");
                    bRetVal = GetNonFDMCallCWS("ReportAccessMgmtAdaptor.Get",true,true);
                    //End Mits 16049
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }   
        }

        private void GetCheckedCheckBox()
        {
            string value = string.Empty;

            for (int lcount = 0; lcount < grdReport.Rows.Count; lcount++)
            {
                //if (grdReport.Rows[lcount].RowType == DataRow)
                {
                    CheckBox chkbox = (CheckBox)grdReport.Rows[lcount].FindControl("chkReport");
                    TextBox txtbox = (TextBox) grdReport.Rows[lcount].FindControl("txtReport");
                    if (chkbox.Checked)
                    {
                        value += txtbox.Text + " ";
                    }
                }
            }

            if (value.Length > 0)
            {
                txthndUsers.Text = value.Substring(0, value.Length - 1);             

            }
        }

        protected void filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool bRetVal;
            try
            {
                ViewState["selectedvalue"] = filter.SelectedValue;
                //Bijender has changed for Mits 16049
                //bRetVal = GetNonFDMCallCWS("ReportAccessMgmtAdaptor.Get");
                bRetVal = GetNonFDMCallCWS("ReportAccessMgmtAdaptor.Get",true,true);
                //End Mits 16049

                filter.SelectedValue = ViewState["selectedvalue"].ToString();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }   

        }

    }

    }

