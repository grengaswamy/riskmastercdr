<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GrantReportAccess.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.GrantReportAccess" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
 <head id="Head1" runat="server">  
    <title>Grant Report Access Permissions</title>
    <script type ="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js">{var i;} </script>  
    <script type ="text/javascript" language ="javascript" >
        function ShowPleaseWait()
        {
             pleaseWait.Show();
             return true;
        }
    </script> 
 </head>
 
 <body onload="parent.MDIScreenLoaded();">
  <form id="frmData" runat="server" name="frmData" method="post" >
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
         <td class="msgheader" colspan="4">
             <asp:Label ID="lblGrantReprtPerm" runat="server" Text="<%$ Resources:lblGrantReprtPerm %>"></asp:Label></td>
    </tr>
    <tr>
         <td colspan="3" class="colheader5"><asp:Label ID="lblUsers" runat="server" Text="<%$ Resources:lblUsers %>"></asp:Label></td>
         <td class="colheader5"><asp:Label ID="lblReports" runat="server" Text="<%$ Resources:lblReports %>"></asp:Label></td>
    </tr>
    
    <tr>
        <td class="colheader"></td>
        <td>
            <asp:ListBox ID="lstUsers" runat="server" SelectionMode ="Multiple" rmxignoreset="true" rmxref="/Instance/Document/ReportsAccess/Users" Height ="200"></asp:ListBox>       
            <asp:TextBox id="txtUsers" runat ="server" rmxignoreget="true" rmxref="/Instance/Document/ReportsAccess/Users" style="display:none"></asp:TextBox>
       </td>      
       <td></td>
       <td>
       <!-- rsushilaggar 02/01/10 :MITS 14884 : Text not wrapping in Users Grant Additional Report Access Rights --> 
            <asp:ListBox ID ="lstReports" runat="server" SelectionMode ="Multiple" rmxignoreset="true" rmxref="/Instance/Document/ReportsAccess/Reports" Height ="200" Width="400" OnPreRender="lstReports_PreRender"></asp:ListBox>       
            <asp:TextBox ID="txtReports" runat="server" rmxignoreget="true" rmxref="/Instance/Document/ReportsAccess/Reports" style="display :none"></asp:TextBox>
       </td>
    </tr>
    
   </table>   
   <table border="0">
    <tr>
         <td>
            <asp:Button ID="btnGrantAccess" runat="server" type="submit" Text="<%$ Resources:btnGrantAccess %>" class="button" OnClientClick ="return ShowPleaseWait();" onclick="btnGrantAccess_Click" />
         </td>
         <td>
            <asp:Button ID ="btnRefresh" type="submit" runat="server" ref="/Instance/ui/action" Text="<%$ Resources:btnRefresh %>" OnClientClick ="return ShowPleaseWait();" class="button" onclick="btnRefresh_Click" />
         </td>
         <td>
            
         </td>
    </tr>
   </table>   
        <input type="hidden" name="" value="rmx-widget-handle-3" id="SysWindowId" />
        <input type="hidden" name="" value="" />
   </div>         
   <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
 </form>
 <br />
     <asp:Label ID="lblNote1" runat="server" Text="<%$ Resources:lblNote1 %>"></asp:Label>
   		<ol>
            <li><asp:Label ID="lblNote2" runat="server" Text="<%$ Resources:lblNote2 %>"></asp:Label></li>
            <li><asp:Label ID="lblNote3" runat="server" Text="<%$ Resources:lblNote3 %>"></asp:Label></li>
            <li><asp:Label ID="lblNote4" runat="server" Text="<%$ Resources:lblNote4 %>"></asp:Label></li>
        </ol>  
        <p> <asp:Label ID="lblNote5" runat="server" Text="<%$ Resources:lblNote5 %>"></asp:Label>
        </p>    
</body>
</html>
