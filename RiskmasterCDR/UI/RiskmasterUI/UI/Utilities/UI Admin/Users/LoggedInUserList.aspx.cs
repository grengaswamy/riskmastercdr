﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.Utilities.UI_Admin.Users
{
    public partial class LoggedInUserList : NonFDMBasePageCWS
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        XmlDocument oFDMPageDom = null;
        public string sCWSresponse = "";
        //protected XElement oMessageElement;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                XElement oMessageElement = GetMessageTemplate();

                CallCWS("LoggedInUserListAdaptor.Get", oMessageElement, out sCWSresponse, false, true);

                oFDMPageDom = new XmlDocument();

                oFDMPageDom = Data;

                rootElement = XElement.Parse(oFDMPageDom.OuterXml);

                result = from c in rootElement.XPathSelectElements("Sessions/option")
                         select c;
            }

        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
             <Call>
              <Function></Function> 
              </Call>
             <Document>
              <Sessions /> 
             </Document>
            </Message>


            ");

            return oTemplate;
        }

        protected void LogOut(object sender, EventArgs e)
        {
            XElement oMessageElement = GetMessageTemplate();

            oMessageElement.XPathSelectElement("./Document/Sessions").Value = this.hdnSelected.Text;

            
            CallCWS("LoggedInUserListAdaptor.DeleteSession", oMessageElement, out sCWSresponse, false, false);



            XElement oMessageElement1 = GetMessageTemplate();

            CallCWS("LoggedInUserListAdaptor.Get", oMessageElement1, out sCWSresponse, false, true);

            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            rootElement = XElement.Parse(oFDMPageDom.OuterXml);

            result = from c in rootElement.XPathSelectElements("Sessions/option")
                     select c;

        }
    }
}
