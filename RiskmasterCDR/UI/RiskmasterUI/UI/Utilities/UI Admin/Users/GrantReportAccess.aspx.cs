﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class GrantReportAccess : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                NonFDMCWSPageLoad("GrantReportAccessAdaptor.Get");
            }
        }

        protected void btnGrantAccess_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;

            try
            {
                GetSelectedListItem();
                bReturnStatus = CallCWSFunction("GrantReportAccessAdaptor.Save");
                if (bReturnStatus)
                    txtReports.Text = "";
                    txtUsers.Text = "";
                    bReturnStatus = CallCWSFunction("GrantReportAccessAdaptor.Get");
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void GetSelectedListItem()
        {
            string values = string.Empty;
            for (int lcount = 0; lcount < lstUsers.Items.Count; lcount++)
            {
                if (lstUsers.Items[lcount].Selected)
                {
                    values += lstUsers.Items[lcount].Value + " ";
                }
            }
            if (values.Length > 0)
            {
                txtUsers.Text = values.Substring(0, values.Length - 1);
            }
            values ="";
            for (int lcount = 0; lcount < lstReports.Items.Count; lcount++)
            {
                if (lstReports.Items[lcount].Selected)
                {
                    values += lstReports.Items[lcount].Value + " ";
                }
            }
            if (values.Length > 0)
            {
                txtReports.Text = values.Substring(0, values.Length - 1);
            }

        }

        
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            NonFDMCWSPageLoad("GrantReportAccessAdaptor.Get");
        }

        // rsushilaggar 02/01/10 :MITS 14884 : Text not wrapping in Users Grant Additional Report Access Rights
        //Solution: Added support for tool tip.
        protected void lstReports_PreRender(object sender, EventArgs e)
        {
            foreach (ListItem item in lstReports.Items)
            {
                item.Attributes.Add("title", item.Text);
            }

        }
    }
}
