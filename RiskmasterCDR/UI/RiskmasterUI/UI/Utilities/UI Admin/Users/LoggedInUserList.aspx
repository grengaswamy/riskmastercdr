<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoggedInUserList.aspx.cs" Inherits="Riskmaster.UI.Utilities.UI_Admin.Users.LoggedInUserList" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
 <head runat="server">
  <title>Users Currently logged into the Wizard</title>
  <link rel="stylesheet" href="../../../../Content/system.css" type="text/css"/>
  <script language="JavaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>
  <script type="text/javascript">
				var ns, ie, ieversion;
				var browserName = navigator.appName;                   // detect browser 
				var browserVersion = navigator.appVersion;
				if (browserName == "Netscape")
				{
					ie=0;
					ns=1;
				}
				else		//Assume IE
				{
				ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
				ie=1;
				ns=0;
				}
				function Logout(val)
				{
					document.forms[0].hdnSelected.value=val;
					//document.forms[0].submit();
				}
				function onPageLoaded()
				{
					if (ie)
					{
						if ((eval("document.all.divForms")!=null) && (ieversion>=6))
						{
							eval("document.all.divForms").style.height=450
							//alert(window.frames.frameElement);
							//eval("document.all.divForms").style.height=window.frames.Height*0.70;
						}
					}
					else
					{
						var o_divforms;
						o_divforms=document.getElementById("divForms");
						if (o_divforms!=null)
						{
							o_divforms.style.height=window.frames.innerHeight*0.70;
							o_divforms.style.width=window.frames.innerWidth*0.995;
						}
					}
				}
				
	</script>
 </head>
 <body onload="javascript:onPageLoaded();parent.MDIScreenLoaded();">
  <form id="frmData" method="post" runat="server">
  <uc1:ErrorControl id="ErrorControl1" runat="server" />
  <table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td class="msgheader" colspan="4">
         <asp:Label ID="lblCurrentlyLoggedinUsers" runat="server" Text="<%$ Resources:lblCurrentlyLoggedinUsers %>"></asp:Label></td>
    </tr>
    <tr>
     <td><asp:TextBox runat="server" style="display:none" name="hdnSelected" value="" id="hdnSelected" /></td>
    </tr>
   </table>
   <div id="divForms" class="divScroll">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td class="colheader5"> <asp:Label ID="lblUserName" runat="server" Text="<%$ Resources:lblUserName %>"></asp:Label></td>
      <td class="colheader5"> <asp:Label ID="lblEmail" runat="server" Text="<%$ Resources:lblEmail %>"></asp:Label></td>
      <td class="colheader5"> <asp:Label ID="lblDatabase" runat="server" Text="<%$ Resources:lblDatabase %>"></asp:Label></td>
      <td class="colheader5"> <asp:Label ID="lblLastAccess" runat="server" Text="<%$ Resources:lblLastAccess %>"></asp:Label></td>
      <td class="colheader5"> <asp:Label ID="lblPhoneNumber" runat="server" Text="<%$ Resources:lblPhoneNumber %>"></asp:Label></td>
      <td class="colheader5">&nbsp;</td>
     </tr>
     <%int i = 0;
       if (result != null)
       {
           
           //Parijat :Sorting 18824 for metro
           List<DateTime> ls = new List<DateTime>();
           List<XElement> listXelement = new List<XElement>();
           ArrayList arr = new ArrayList();
           foreach (XElement item1 in result)
           {

               ls.Add(Convert.ToDateTime(item1.Attribute("datetime").Value));
               arr.Add(Convert.ToDateTime(item1.Attribute("datetime").Value));
               listXelement.Add(item1);

           }
           ls.Sort();
           ls.Reverse();
           for (int x = 0; x <= ls.Count - 1; x++)
           {
               XElement item = listXelement[arr.IndexOf(ls[x])];
               //}
           //foreach (XElement item in result)
           //{
               string rowclass = "";
               if ((i % 2) == 1) rowclass = "rowlight";
               else rowclass = "rowdark";
               i++;
     %>
     
     <tr class="<% =rowclass %>">
        <td>
            <% UserName.Text = item.Attribute("name").Value; %>
            <asp:Label ID="UserName" runat="server"></asp:Label>
        </td>
        <td>
            <% Email_Link.Text = item.Attribute("email").Value; %>
            <%--avipinsrivas start : Worked for JIRA - 12206 --%>
            <asp:Label ID="Email_Link" runat="server"></asp:Label>
            <%--<asp:LinkButton ID="Email_Link" runat="server"></asp:LinkButton>--%>
        </td>
        <td>
            <% Database.Text = item.Attribute("database").Value; %>
            <asp:Label ID="Database" runat="server"></asp:Label>
        </td>
        <td>
            <% DateTime.Text = item.Attribute("datetime").Value; %>
            <asp:Label ID="DateTime" runat="server"></asp:Label>
        </td>
        <td>
            <% Phone.Text = item.Attribute("phone").Value; %>
            <asp:Label ID="Phone" runat="server"></asp:Label>
        </td>
        <td>
            <% 
               session.Text = item.Value;
               logout.OnClientClick = "Logout('" + session.Text + "');"; %>
            <asp:Button ID="logout" runat="server" Text="<%$ Resources:btnLogOut %>" OnClick="LogOut" />
            <asp:TextBox runat="server" ID="session" style="display:none" />
        </td>
     </tr>
     
     <% }
       }     %>
     
   </table>
   </div>
   <table border="0">
    <tr style="display:none">
        <% cmdBack.OnClientClick = "window.location='../Users/Users.aspx'"; %>
     <td><asp:Button runat="server" type="button" class="button" ID="cmdBack" Text="<%$ Resources:btnBack %>" /></td>
    </tr>
   </table>  
  </form>
 </body>
</html>
