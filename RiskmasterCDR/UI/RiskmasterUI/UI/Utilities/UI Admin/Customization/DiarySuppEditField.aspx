﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiarySuppEditField.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.DiarySuppEditField" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title></title>
    <script type="text/javascript">

        var pOptions = new Array();
        pOptions.length = 0;
        var globalIndex = -1;
        var m_DataChange = false;
        function Save() {

            if (trimIt(document.forms[0].txtCaption.value) == "") {

                alert("Please enter the Caption.");
                document.forms[0].txtCaption.focus();
                return false;
            }

            if (document.forms[0].txtCaption.value.search("-") >= 0) {
                //nadim MITS 7408
                if (document.forms[0].txtCaption.value.substr(0, 1) == "-") {
                    alert("'-' character cannot be put at first position");
                    return false;
                }
                //else
                //alert("'-' character cannot be part of field caption.");
                // return false;
                //MITS 7408
            }

            if (document.forms[0].txtCaption.value.search("[\[]") >= 0) {
                alert("'[' character cannot be part of field caption.");
                return false;
            }

            if (document.forms[0].txtCaption.value.search("[\]]") >= 0) {
                alert("']' character cannot be part of field caption.");
                return false;
            }

            if (window.opener != null) {
                window.opener.OnEditFieldOK();
            }
            return true;
        }

        function Cancel() {
            self.close();
        }

        function handleUnload() {
            if (window.opener != null)
                window.opener.onWndClose();
            return true;
        }
        //Geeta 02/02/07 : Modified for FMLA Grid Edit field 
        function handleLoad() {
            if (document.forms[0].txtCaption != null && document.forms[0].txtCaption.disabled == false)
                document.forms[0].txtCaption.focus();
            return true;
        }
        function trimIt(aString) {
            // RETURNS INPUT ARGUMENT WITHOUT ANY LEADING OR TRAILING SPACES
            return (aString.replace(/^ +/, '')).replace(/ +$/, '');
        }
        var sName, sCaption;

        //var sValueParent = window.opener.document.forms[0].lstFormList.options[window.opener.document.forms[0].lstFormList.selectedIndex].text;
        var sFieldCaption = window.opener.document.forms[0].FieldCaption.value;  


</script>
</head>
<body>
<form id="form1" runat="server">
    <div>
    <input type="hidden" id="fieldcaption" value="0">
    <table>
    <tr>
     <td class="msgheader" colspan="2">Edit&nbsp;<script type="text/javascript" language="javascript">window.document.write(sFieldCaption);</script></td>
    </tr>
    <tr>
     <td class="required">Caption:</td>
     <td><input type="text" size="22" id="txtCaption" maxlength="50"></td>
    </tr>
    <tr>
     <td colspan="2" align="center"><input type="button" class="button" value="  OK  " onClick="Save();"><input type="button" class="button" value="Cancel" onClick="Cancel();"></td>
    </tr>
   </table>
    </div>
    <script type="text/javascript" language="javascript">
        document.forms[0].txtCaption.value = window.opener.document.forms[0].FieldName.value;
        if (window.opener.document.forms[0].FieldCaption.value == "Field")
            document.forms[0].fieldcaption.value = "1";
    </script>
    </form>
</body>
</html>
