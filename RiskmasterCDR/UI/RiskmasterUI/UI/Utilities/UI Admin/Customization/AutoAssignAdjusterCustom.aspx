﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="AutoAssignAdjusterCustom.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.UI_Admin.Customization.AutoAssignAdjusterCustom" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Auto Assign Adjuster Settings</title>
    <link rel="stylesheet" href="../../../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>

    <script language="javaScript" src="../../../../Scripts/Utilities.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../../../Scripts/drift.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }</script>

</head>
<%--<body onload="pageLoaded();parent.MDIScreenLoaded()">--%>
<%--MITS 29071: Ishan--%>
<body onload="parent.MDIScreenLoaded()">
  
    <form id="frmData" runat="server">
    <asp:HiddenField ID="hdnURL" Value="" runat="server" />
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <div id="toolbardrift" class="toolbardrift">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
            <asp:imagebutton ID="Imagebutton1" runat="server" onclick="NavigateSave"
                src="../../../../Images/save.gif" alternatetext="<%$ Resources:imgbtnAltSave %>"
                validationgroup="vgSave" onmouseover="this.src='../../../../Images/tb_save_mo.png';this.style.zoom='110%'"
                onmouseout="this.src='../../../../Images/save.gif';this.style.zoom='100%'" style="height:28px;width:28px;border-width:0px;"/>
        </div>
    </div>
    <div class="msgheader2" id="formtitle">
        <asp:Label ID="lblAutoAssignAdjuster" runat="server" Text="<%$ Resources:lblAutoAssignAdjuster %>"></asp:Label>
    </div>
    <div class="customItemContainer">
        <div class="colheader3">
            <div class="customShowColumn"><asp:Label ID="lblShow" runat="server" Text="<%$ Resources:lblShow %>"></asp:Label></div>
        </div>
        <div class="datatd">
            <div class="customLeftColumn"><asp:Label ID="lblLOB" runat="server" Text="<%$ Resources:lblLOB %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="LOB" type="checkbox" rmxref="Instance/Document/AAACustomization/LOB" appearance="full" /></div>
        </div>
        <div class="datatd1">
            <div class="customLeftColumn"><asp:Label ID="lblDepartments" runat="server" Text="<%$ Resources:lblDepartments %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="Departments" type="checkbox" rmxref="Instance/Document/AAACustomization/Departments" appearance="full" /></div>
        </div>
        <div class="datatd">
            <div class="customLeftColumn"><asp:Label ID="lblClaimType" runat="server" Text="<%$ Resources:lblClaimType %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="ClaimType" type="checkbox" rmxref="Instance/Document/AAACustomization/ClaimType" appearance="full" /></div>
        </div>
        <div class="datatd1">
            <div class="customLeftColumn"><asp:Label ID="lblJurisdiction" runat="server" Text="<%$ Resources:lblJurisdiction %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="Jurisdiction" type="checkbox" rmxref="Instance/Document/AAACustomization/Jurisdiction" appearance="full" /></div>
        </div>
        <div class="datatd">
            <div class="customLeftColumn"><asp:Label ID="lblForceWorkItems" runat="server" Text="<%$ Resources:lblForceWorkItems %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="ForceWorkItems" type="checkbox" rmxref="Instance/Document/AAACustomization/ForceWorkItems" appearance="full" /></div>
        </div>
        <div class="datatd1">
            <div class="customLeftColumn"><asp:Label ID="lblWorkItems" runat="server" Text="<%$ Resources:lblWorkItems %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="WorkItems" type="checkbox" rmxref="Instance/Document/AAACustomization/WorkItems" appearance="full" /></div>
        </div>
        <div class="datatd">
            <div class="customLeftColumn"><asp:Label ID="lblSkipWorkItems" runat="server" Text="<%$ Resources:lblSkipWorkItems %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="SkipWorkItems" type="checkbox" rmxref="Instance/Document/AAACustomization/SkipWorkItems" appearance="full" /></div>
        </div>
        <div class="datatd1">
            <div class="customLeftColumn"><asp:Label ID="lblCurrentlyAvailable" runat="server" Text="<%$ Resources:lblCurrentlyAvailable %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="CurrentlyAvailable" type="checkbox" rmxref="Instance/Document/AAACustomization/CurrentlyAvailable" appearance="full" /></div>
        </div>
        <div class="datatd">
            <div class="customLeftColumn"><asp:Label ID="lblSameCriteria" runat="server" Text="<%$ Resources:lblSameCriteria %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="SeparateSameCriteria" type="checkbox" rmxref="Instance/Document/AAACustomization/SeparateSameCriteria" appearance="full" /></div>
        </div>
    </div>

    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    
    <input type="text" name="" value="" id="SysClassName" style="display: none" />
    <input type="text" name="" value="" id="SysCmd" style="display: none" />
    <input type="text" name="" value="" id="SysFormIdName" style="display: none" />
    <input type="text" name="" value="" id="SysFormPIdName" style="display: none" />
    <input type="text" name="" value="" id="SysFormName" style="display: none" />
    <input type="text" name="" value="" id="SysCmdQueue" style="display: none" />

    </form>
</body>
</html>
