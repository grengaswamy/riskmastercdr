﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.FDM;
using System.Xml;
using System.IO;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class ModifyPortletInfoForAll : GridPopupBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // aravi5 Modified for Jira Id: 7240 RMA Header Tab is not in proper design of rma standard in IE11 Full Mode
            btnOk.Attributes.Add("onclick", "javascript:Save()");
            btnCancel.Attributes.Add("onclick", "Close()");
            GridPopupPageload();
            if(IsPostBack)
            {
                //if (txtData.Text != "")
                //{
                //    XmlDocument objXMLDoc = new XmlDocument();
                //    //XmlTextReader xmlReader = new XmlTextReader(new StringReader(txtData.Text));
                //    string sData = txtData.Text;
                //    sData = sData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                //    objXMLDoc.LoadXml(sData);
                //    Name.Text = objXMLDoc.SelectSingleNode("//Name").InnerText;
                //    urlPortlet.Text = objXMLDoc.SelectSingleNode("//urlPortlet").InnerText;
                //} // if    
                if (urlPortlet.Text == "** URL Editing Locked **")
                {
                    urlPortlet.Visible = false;
                    tdUrlOfPortlet.Visible = false;
                } // if

            } // if
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            FormMode.Value = "close";
            base.btnOk_Click(sender,e);
        }
    }
}
