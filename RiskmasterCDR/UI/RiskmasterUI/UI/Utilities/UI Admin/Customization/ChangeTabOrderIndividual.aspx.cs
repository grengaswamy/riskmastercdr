﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.IO;
using System.Xml.Linq;
using Riskmaster.Models;
using Riskmaster.Cache;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class ChangeTabOrderIndividual : System.Web.UI.Page
    {
        XmlElement objLstRow = null;
        XmlElement objRowTxt = null;
        XmlElement objElement = null;
        XmlDocument p_objXmlDocument = new XmlDocument();
        XmlDocument xmlDoc;
        int iSequenceId = 0;
        string sChangedIDs = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            imgMoveUp.Attributes.Add("onclick", "javascript:return ChangeTabOrderUpdateInfo();");
            imgMoveDown.Attributes.Add("onclick", "javascript:return ChangeTabOrderUpdateInfo();");
            
            if (IsPostBack)
            {
                if (IndividualPortletXML.Text != "")
                {
                    p_objXmlDocument.LoadXml(IndividualPortletXML.Text);
                } // if
            } // if
            else
            {
                UserName.Text = AppHelper.GetQueryStringValue("username");
                xmlDoc = new XmlDocument();
                PortalData oPortalData = new PortalData();
                oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                string content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 

                xmlDoc.LoadXml(content);
                //xmlDoc.Load("C:/Customization.xml");
                //xmlDoc.Load(Server.MapPath("~/App_Data/Customization.xml"));
                IndividualPortletXML.Text = "<PortletSettings><IndividualPortletsList>";
                foreach (XmlNode node in xmlDoc.SelectNodes("/PortalSettings/Portlets[@LoginName='" + UserName.Text + "']/id"))
                {
                    iSequenceId++;
                    IndividualPortletXML.Text += "<option><SequenceId>" + iSequenceId.ToString() + "</SequenceId>";
                    IndividualPortletXML.Text += "<RowID>" + node.InnerText + "</RowID>";
                    string sName = xmlDoc.SelectSingleNode("/PortalSettings/IndividualPortletsInfo/PortletInfo[@id=" + node.InnerText + "]/@name").Value;
                    IndividualPortletXML.Text += "<Name>" + sName + "</Name>";
                    string sUrlPortlet = xmlDoc.SelectSingleNode("/PortalSettings/IndividualPortletsInfo/PortletInfo[@id=" + node.InnerText + "]").InnerText;
                    IndividualPortletXML.Text += "<urlPortlet>" + sUrlPortlet + "</urlPortlet>";
                    IndividualPortletXML.Text += "</option>";
                } // foreach

                IndividualPortletXML.Text += "</IndividualPortletsList></PortletSettings>";
                SequenceID.Text = iSequenceId.ToString();
                p_objXmlDocument.LoadXml(IndividualPortletXML.Text);
                GridView gvChangetabOrderGrid = (GridView)this.Form.FindControl("gvChangetabOrderGrid");
                if (iSequenceId != 0)
                {
                    DataSet usersRecordsSet = null;
                    usersRecordsSet = ConvertXmlDocToDataSet(p_objXmlDocument);

                    usersRecordsSet.Tables["option"].DefaultView.Sort = "SequenceId" + " " + "ASC";

                    gvChangetabOrderGrid.DataSource = usersRecordsSet.Tables["option"].DefaultView;
                } // if
                gvChangetabOrderGrid.DataBind();
            } // else

        }

        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(diaryDoc));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        protected void imgMoveUp_Click(object sender, ImageClickEventArgs e)
        {
            if (SelectedID.Text != "1" && SelectedID.Text != "0")
            {
                foreach (XmlNode node in p_objXmlDocument.SelectNodes("//option/SequenceId"))
                {
                    if (node.InnerText == (int.Parse(SelectedID.Text) - 1).ToString())
                    {
                        node.InnerText = SelectedID.Text;
                        node.ParentNode.NextSibling.FirstChild.InnerText = (int.Parse(SelectedID.Text) - 1).ToString();
                        break;
                    } // if
                    
                }

                DataSet usersRecordsSet = null;

                usersRecordsSet = ConvertXmlDocToDataSet(p_objXmlDocument);
                GridView gvChangetabOrderGrid = (GridView)this.Form.FindControl("gvChangetabOrderGrid");
                usersRecordsSet.Tables["option"].DefaultView.Sort = "SequenceId" + " " + "ASC";

                gvChangetabOrderGrid.DataSource = usersRecordsSet.Tables["option"].DefaultView;

                DataRow[] dr = usersRecordsSet.Tables["option"].Select("0=0", "SequenceId ASC");
                DataTable dt = new DataTable("option");
                foreach (DataColumn dc in usersRecordsSet.Tables["option"].Columns)
                {
                    dt.Columns.Add(dc.ColumnName);
                } // foreach

                foreach (DataRow drTemp in dr)
                {
                    dt.ImportRow(drTemp);
                    sChangedIDs += "<id>" + drTemp[1].ToString() + "</id>";
                } // foreach

                gvChangetabOrderGrid.DataBind();
                StringWriter objWriter = new StringWriter();
                dt.WriteXml(objWriter, true);
                string sChangedPortletXML = objWriter.ToString();
                XmlDocument objChangedPortletXML = new XmlDocument();
                objChangedPortletXML.LoadXml(sChangedPortletXML);
                string sOption = objChangedPortletXML.InnerXml.ToString();

                SortChanged.Text = "true";
                IndividualPortletXML.Text = sOption;

                xmlDoc = new XmlDocument();
                //PortalServiceClient PortalClient = new PortalServiceClient();
                // Rsolanki2: mits 23786: RMX portal setting should be saved to database instead of web server
                //string strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                //string content = PortalClient.ContentInfo(strSql);

                PortalData oPortalData = new PortalData();
                oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                string content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 

                xmlDoc.LoadXml(content);

                //xmlDoc.Load("C:/Customization.xml");
                //xmlDoc.Load(Server.MapPath("~/App_Data/Customization.xml"));
                xmlDoc.SelectSingleNode("/PortalSettings/Portlets[@LoginName='" + UserName.Text + "']").InnerXml = sChangedIDs;

                oPortalData.Content = xmlDoc.InnerXml.ToString();
                oPortalData.FileName = "CUSTOMIZE_PORTALINFO";
                oPortalData.Token = AppHelper.Token;
                AppHelper.GetResponse("RMService/Portal/Content", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oPortalData); 
                
                //PortalClient.ContentUpdate(xmlDoc.InnerXml.ToString(), "CUSTOMIZE_PORTALINFO");
                                
                //HttpContext.Current.Cache.Remove("RMXPortalSettings");    
                CacheCommonFunctions.RemoveValueFromCache("RMXPortalSettings", AppHelper.ClientId);

                //xmlDoc.Save("C:/Customization.xml");
                //xmlDoc.Save(Server.MapPath("~/App_Data/Customization.xml"));
            } // if            
        }

        protected void imgMoveDown_Click(object sender, ImageClickEventArgs e)
        {
            if (SelectedID.Text != SequenceID.Text && SelectedID.Text != "0")
            {
                foreach (XmlNode node in p_objXmlDocument.SelectNodes("//option/SequenceId"))
                {
                    if (node.InnerText == SelectedID.Text)
                    {
                        node.InnerText = (int.Parse(SelectedID.Text) + 1).ToString();
                        node.ParentNode.NextSibling.FirstChild.InnerText = int.Parse(SelectedID.Text).ToString();
                        break;
                    } // if
                    
                }

                DataSet usersRecordsSet = null;

                usersRecordsSet = ConvertXmlDocToDataSet(p_objXmlDocument);
                GridView gvChangetabOrderGrid = (GridView)this.Form.FindControl("gvChangetabOrderGrid");
                usersRecordsSet.Tables["option"].DefaultView.Sort = "SequenceId" + " " + "ASC";

                gvChangetabOrderGrid.DataSource = usersRecordsSet.Tables["option"].DefaultView;

                DataRow[] dr = usersRecordsSet.Tables["option"].Select("0=0", "SequenceId ASC");
                DataTable dt = new DataTable("option");
                foreach (DataColumn dc in usersRecordsSet.Tables["option"].Columns)
                {
                    dt.Columns.Add(dc.ColumnName);
                } // foreach

                foreach (DataRow drTemp in dr)
                {
                    dt.ImportRow(drTemp);
                    sChangedIDs += "<id>" + drTemp[1].ToString() + "</id>";
                } // foreach
                gvChangetabOrderGrid.DataBind();

                StringWriter objWriter = new StringWriter();
                dt.WriteXml(objWriter, true);
                string sChangedPortletXML = objWriter.ToString();
                XmlDocument objChangedPortletXML = new XmlDocument();
                objChangedPortletXML.LoadXml(sChangedPortletXML);
                string sOption = objChangedPortletXML.InnerXml.ToString();

                SortChanged.Text = "true";
                IndividualPortletXML.Text = sOption;
                xmlDoc = new XmlDocument();
                //PortalServiceClient PortalClient = new PortalServiceClient();
                
                // Rsolanki2: mits 23786: RMX portal setting should be saved to database instead of web server
                //string strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                //string content = PortalClient.ContentInfo(strSql);

                PortalData oPortalData = new PortalData();
                oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                string content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 
                

                xmlDoc.LoadXml(content);

                //xmlDoc.Load("C:/Customization.xml");
                //xmlDoc.Load(Server.MapPath("~/App_Data/Customization.xml"));
                xmlDoc.SelectSingleNode("/PortalSettings/Portlets[@LoginName='" + UserName.Text + "']").InnerXml = sChangedIDs;

                oPortalData.Content = xmlDoc.InnerXml.ToString();
                oPortalData.FileName = "CUSTOMIZE_PORTALINFO";
                oPortalData.Token = AppHelper.Token;
                AppHelper.GetResponse("RMService/Portal/Content", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oPortalData); 
                //PortalClient.ContentUpdate(xmlDoc.InnerXml.ToString(), "CUSTOMIZE_PORTALINFO");
                                
                //HttpContext.Current.Cache.Remove("RMXPortalSettings");
                CacheCommonFunctions.RemoveValueFromCache("RMXPortalSettings", AppHelper.ClientId);

                //xmlDoc.Save("C:/Customization.xml");
                //xmlDoc.Save(Server.MapPath("~/App_Data/Customization.xml"));
            }
        }
    }
}
