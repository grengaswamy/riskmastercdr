﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class DiarySuppHeaderConfig : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hdnCloseWindow.Value = "False";
                XElement messageElement = null;
                string sReturn = string.Empty;
                if (!IsPostBack)
                {
                    messageElement = GetDiarySuppConfigMessageTemplate();
                    sReturn = AppHelper.CallCWSService(messageElement.ToString());
                    BindPageControls(sReturn);
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void BindPageControls(string sreturnValue)
        {
            DataSet formEditRecordsSet = null;
            XmlDocument formEditXDoc = new XmlDocument();
            formEditXDoc.LoadXml(sreturnValue);
            formEditRecordsSet = ConvertXmlDocToDataSet(formEditXDoc);

            DropDownList cboDiarySuppFields = (DropDownList)this.Form.FindControl("cboDiarySuppFields");
            if (formEditRecordsSet.Tables["FormFieldOption"] != null)
            {
                cboDiarySuppFields.DataSource = formEditRecordsSet.Tables["FormFieldOption"].DefaultView;
                cboDiarySuppFields.DataTextField = "FormFieldOption_Text";
                cboDiarySuppFields.DataValueField = "value";
                cboDiarySuppFields.DataBind();
            }
            else
            {
                cboDiarySuppFields.Items.Clear();

            }

            ListBox lstDiarySuppList = (ListBox)this.Form.FindControl("lstDiarySuppList");
            if (formEditRecordsSet.Tables["FormListOption"] != null)
            {
                lstDiarySuppList.DataSource = formEditRecordsSet.Tables["FormListOption"].DefaultView;
                lstDiarySuppList.DataTextField = "FormListOption_Text";
                lstDiarySuppList.DataValueField = "value";
                lstDiarySuppList.DataBind();
            }
            else
            {
                lstDiarySuppList.Items.Clear();
            }

            formEditRecordsSet.Dispose();

        }

        protected void Save(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            try
            {
                string sReturn = string.Empty;
                XElement messageElement = null;
                messageElement = SetDiarySuppConfigMessageTemplate();
                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                ErrorControl1.errorDom = sReturn;

                XmlDocument xmlServiceDoc = new XmlDocument();
                xmlServiceDoc.LoadXml(sReturn);

                if ((xmlServiceDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
                {
                    hdnCloseWindow.Value = "True";
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetDiarySuppConfigMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>HeaderConfigAdaptor.GetDiarySuppHeaderConfig</Function></Call>");
            sXml = sXml.Append("<Document>");
            sXml = sXml.Append("<DiarySupplemental>");
            sXml = sXml.Append("<WPADiarySuppFields></WPADiarySuppFields>");
            sXml = sXml.Append("<WPADiarySuppFieldsCaption></WPADiarySuppFieldsCaption>");
            sXml = sXml.Append("</DiarySupplemental>");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }

        private XElement SetDiarySuppConfigMessageTemplate()
        {
            string[] arrFields;
            string[] arrCaptions;
            string[] arrFieldTypes;
            string[] arrFieldIDs;
            string sListOptions = string.Empty;
            XElement oTemplate = null;

            try
            {
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization></Authorization>");
                sXml = sXml.Append("<Call><Function>HeaderConfigAdaptor.SaveDiarySuppHeaderConfig</Function></Call>");
                sXml = sXml.Append("<Document>");
                sXml = sXml.Append("<DiarySupplemental>");
                if (!string.IsNullOrEmpty(fields.Text) && !string.IsNullOrEmpty(captions.Text))
                {
                    arrFields = fields.Text.Split('|');
                    arrCaptions = captions.Text.Split('|');
                    arrFieldTypes = fieldtypes.Text.Split('|');
                    arrFieldIDs = field_ids.Text.Split('|');
                    for (int iCtr = 0; iCtr < arrFields.Length - 1; iCtr++)
                    {
                        sXml = sXml.Append("<option value=\"" + arrFields[iCtr] + "\" type=\"" + arrFieldTypes[iCtr] + "\" field_id=\"" + arrFieldIDs[iCtr] + "\" >");
                        sXml = sXml.Append(arrCaptions[iCtr]);
                        sXml = sXml.Append("</option>");
                    }
                }
                sXml = sXml.Append("</DiarySupplemental>");
                sXml = sXml.Append("</Document>");
                sXml = sXml.Append("</Message>");
                oTemplate = XElement.Parse(sXml.ToString());

                return oTemplate;
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

            return oTemplate;
        }
    }
}